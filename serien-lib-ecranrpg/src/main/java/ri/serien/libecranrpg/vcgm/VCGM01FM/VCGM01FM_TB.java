
package ri.serien.libecranrpg.vcgm.VCGM01FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM01FM_TB extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] TBSNS_Value = { "D", "C" };
  private String[] TBSNS_Title = { "Débit", "Crédit" };
  private String[] TBTYP_Value = { "", "1", "2", "3", };
  private String[] TBTYP_Title = { "Rubrique", "Titre", "Total", "Calcul", };
  private String[] TBMAR_Value = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", };
  private String[] TBMAR_Title = { "aucune", "décalage de 1 à droite", "décalage de 2 à droite", "décalage de 3 à droite",
      "décalage de 4 à droite", "décalage de 5 à droite", "décalage de 6 à droite", "décalage de 7 à droite", "décalage de 8 à droite",
      "décalage de 9 à droite", };
  
  public VCGM01FM_TB(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TBSNS.setValeurs(TBSNS_Value, TBSNS_Title);
    TBMAR.setValeurs(TBMAR_Value, TBMAR_Title);
    TBTYP.setValeurs(TBTYP_Value, TBTYP_Title);
    TBGRA.setValeursSelection("1", "0");
    TBSSL.setValeursSelection("1", "0");
    TBEDT.setValeursSelection("1", "0");
    TPN12.setValeursSelection("1", " ");
    TPN11.setValeursSelection("1", " ");
    TPN10.setValeursSelection("1", " ");
    TPN9.setValeursSelection("1", " ");
    TPN8.setValeursSelection("1", " ");
    TPN7.setValeursSelection("1", " ");
    TPN6.setValeursSelection("1", " ");
    TPN5.setValeursSelection("1", " ");
    TPN4.setValeursSelection("1", " ");
    TPN3.setValeursSelection("1", " ");
    TPN2.setValeursSelection("1", " ");
    TPN1.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    panel1.setVisible(lexique.isTrue("N40"));
    panel2.setVisible(lexique.isTrue("N40"));
    panel3.setVisible(lexique.isTrue("N40"));
    
    

    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44_OBJ_45 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_45_OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46_OBJ_47 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    TBTYP = new XRiComboBox();
    TBLIB = new XRiTextField();
    OBJ_120_OBJ_120 = new JLabel();
    TBSNS = new XRiComboBox();
    OBJ_118_OBJ_118 = new JLabel();
    OBJ_119_OBJ_119 = new JLabel();
    panel1 = new JPanel();
    STL01 = new XRiTextField();
    STL02 = new XRiTextField();
    STL03 = new XRiTextField();
    STL04 = new XRiTextField();
    STL05 = new XRiTextField();
    OBJ_129_OBJ_129 = new JLabel();
    TBT01 = new XRiTextField();
    TBT02 = new XRiTextField();
    TBT03 = new XRiTextField();
    TBT04 = new XRiTextField();
    TBT05 = new XRiTextField();
    OBJ_128_OBJ_128 = new JLabel();
    TBO01 = new XRiTextField();
    TBO02 = new XRiTextField();
    TBO03 = new XRiTextField();
    TBO04 = new XRiTextField();
    panel2 = new JPanel();
    TPD1 = new XRiTextField();
    TPF1 = new XRiTextField();
    TPD2 = new XRiTextField();
    TPF2 = new XRiTextField();
    TPD3 = new XRiTextField();
    TPF3 = new XRiTextField();
    TPD4 = new XRiTextField();
    TPF4 = new XRiTextField();
    TPD5 = new XRiTextField();
    TPF5 = new XRiTextField();
    TPD6 = new XRiTextField();
    TPF6 = new XRiTextField();
    TPD7 = new XRiTextField();
    TPF7 = new XRiTextField();
    TPD8 = new XRiTextField();
    TPF8 = new XRiTextField();
    TPD9 = new XRiTextField();
    TPF9 = new XRiTextField();
    TPD10 = new XRiTextField();
    TPF10 = new XRiTextField();
    TPD11 = new XRiTextField();
    TPF11 = new XRiTextField();
    TPD12 = new XRiTextField();
    TPF12 = new XRiTextField();
    OBJ_127_OBJ_127 = new JLabel();
    OBJ_125_OBJ_125 = new JLabel();
    TPN1 = new XRiCheckBox();
    TPN2 = new XRiCheckBox();
    TPN3 = new XRiCheckBox();
    TPN4 = new XRiCheckBox();
    TPN5 = new XRiCheckBox();
    TPN6 = new XRiCheckBox();
    TPN7 = new XRiCheckBox();
    TPN8 = new XRiCheckBox();
    TPN9 = new XRiCheckBox();
    TPN10 = new XRiCheckBox();
    TPN11 = new XRiCheckBox();
    TPN12 = new XRiCheckBox();
    OBJ_126_OBJ_126 = new JLabel();
    panel3 = new JPanel();
    TBEDT = new XRiCheckBox();
    TBLAP = new XRiTextField();
    TBSSL = new XRiCheckBox();
    OBJ_123_OBJ_123 = new JLabel();
    TBGRA = new XRiCheckBox();
    label1 = new JLabel();
    label2 = new JLabel();
    TBLAV = new XRiTextField();
    TBMAR = new XRiComboBox();
    BTD = new JPopupMenu();
    OBJ_20 = new JMenuItem();
    OBJ_19 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44_OBJ_45 ----
          OBJ_44_OBJ_45.setText("Soci\u00e9t\u00e9");
          OBJ_44_OBJ_45.setName("OBJ_44_OBJ_45");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setName("INDETB");

          //---- OBJ_45_OBJ_46 ----
          OBJ_45_OBJ_46.setText("Code");
          OBJ_45_OBJ_46.setName("OBJ_45_OBJ_46");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setName("INDTYP");

          //---- OBJ_46_OBJ_47 ----
          OBJ_46_OBJ_47.setText("Ordre");
          OBJ_46_OBJ_47.setName("OBJ_46_OBJ_47");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(700, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(600, 572));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Ligne du tableau de bord");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

            //---- TBTYP ----
            TBTYP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TBTYP.setName("TBTYP");

            //---- TBLIB ----
            TBLIB.setComponentPopupMenu(BTD);
            TBLIB.setName("TBLIB");

            //---- OBJ_120_OBJ_120 ----
            OBJ_120_OBJ_120.setText("Sens normal");
            OBJ_120_OBJ_120.setName("OBJ_120_OBJ_120");

            //---- TBSNS ----
            TBSNS.setComponentPopupMenu(BTD);
            TBSNS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TBSNS.setName("TBSNS");

            //---- OBJ_118_OBJ_118 ----
            OBJ_118_OBJ_118.setText("Libell\u00e9");
            OBJ_118_OBJ_118.setName("OBJ_118_OBJ_118");

            //---- OBJ_119_OBJ_119 ----
            OBJ_119_OBJ_119.setText("Type");
            OBJ_119_OBJ_119.setName("OBJ_119_OBJ_119");

            //======== panel1 ========
            {
              panel1.setBorder(new CompoundBorder(
                new TitledBorder("Table de totalisation (ou postes de calcul)"),
                new EmptyBorder(5, 5, 5, 5)));
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- STL01 ----
              STL01.setName("STL01");
              panel1.add(STL01);
              STL01.setBounds(100, 45, 232, STL01.getPreferredSize().height);

              //---- STL02 ----
              STL02.setName("STL02");
              panel1.add(STL02);
              STL02.setBounds(100, 78, 232, STL02.getPreferredSize().height);

              //---- STL03 ----
              STL03.setName("STL03");
              panel1.add(STL03);
              STL03.setBounds(100, 111, 232, STL03.getPreferredSize().height);

              //---- STL04 ----
              STL04.setName("STL04");
              panel1.add(STL04);
              STL04.setBounds(100, 144, 232, STL04.getPreferredSize().height);

              //---- STL05 ----
              STL05.setName("STL05");
              panel1.add(STL05);
              STL05.setBounds(100, 177, 232, STL05.getPreferredSize().height);

              //---- OBJ_129_OBJ_129 ----
              OBJ_129_OBJ_129.setText("Libell\u00e9 associ\u00e9");
              OBJ_129_OBJ_129.setName("OBJ_129_OBJ_129");
              panel1.add(OBJ_129_OBJ_129);
              OBJ_129_OBJ_129.setBounds(100, 25, 230, 20);

              //---- TBT01 ----
              TBT01.setComponentPopupMenu(BTD);
              TBT01.setName("TBT01");
              panel1.add(TBT01);
              TBT01.setBounds(50, 45, 50, TBT01.getPreferredSize().height);

              //---- TBT02 ----
              TBT02.setComponentPopupMenu(BTD);
              TBT02.setName("TBT02");
              panel1.add(TBT02);
              TBT02.setBounds(50, 78, 50, TBT02.getPreferredSize().height);

              //---- TBT03 ----
              TBT03.setComponentPopupMenu(BTD);
              TBT03.setName("TBT03");
              panel1.add(TBT03);
              TBT03.setBounds(50, 111, 50, TBT03.getPreferredSize().height);

              //---- TBT04 ----
              TBT04.setComponentPopupMenu(BTD);
              TBT04.setName("TBT04");
              panel1.add(TBT04);
              TBT04.setBounds(50, 144, 50, TBT04.getPreferredSize().height);

              //---- TBT05 ----
              TBT05.setComponentPopupMenu(BTD);
              TBT05.setName("TBT05");
              panel1.add(TBT05);
              TBT05.setBounds(50, 177, 50, TBT05.getPreferredSize().height);

              //---- OBJ_128_OBJ_128 ----
              OBJ_128_OBJ_128.setText("Code");
              OBJ_128_OBJ_128.setName("OBJ_128_OBJ_128");
              panel1.add(OBJ_128_OBJ_128);
              OBJ_128_OBJ_128.setBounds(50, 25, 50, 20);

              //---- TBO01 ----
              TBO01.setToolTipText("Op\u00e9rateur");
              TBO01.setComponentPopupMenu(BTD);
              TBO01.setName("TBO01");
              panel1.add(TBO01);
              TBO01.setBounds(20, 60, 20, TBO01.getPreferredSize().height);

              //---- TBO02 ----
              TBO02.setToolTipText("Op\u00e9rateur");
              TBO02.setComponentPopupMenu(BTD);
              TBO02.setName("TBO02");
              panel1.add(TBO02);
              TBO02.setBounds(20, 93, 20, TBO02.getPreferredSize().height);

              //---- TBO03 ----
              TBO03.setToolTipText("Op\u00e9rateur");
              TBO03.setComponentPopupMenu(BTD);
              TBO03.setName("TBO03");
              panel1.add(TBO03);
              TBO03.setBounds(20, 126, 20, TBO03.getPreferredSize().height);

              //---- TBO04 ----
              TBO04.setToolTipText("Op\u00e9rateur");
              TBO04.setComponentPopupMenu(BTD);
              TBO04.setName("TBO04");
              panel1.add(TBO04);
              TBO04.setBounds(20, 159, 20, TBO04.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("Plage de comptes g\u00e9n\u00e9raux"));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- TPD1 ----
              TPD1.setComponentPopupMenu(BTD);
              TPD1.setName("TPD1");
              panel2.add(TPD1);
              TPD1.setBounds(30, 55, 70, TPD1.getPreferredSize().height);

              //---- TPF1 ----
              TPF1.setComponentPopupMenu(BTD);
              TPF1.setName("TPF1");
              panel2.add(TPF1);
              TPF1.setBounds(105, 55, 70, TPF1.getPreferredSize().height);

              //---- TPD2 ----
              TPD2.setComponentPopupMenu(BTD);
              TPD2.setName("TPD2");
              panel2.add(TPD2);
              TPD2.setBounds(30, 83, 70, TPD2.getPreferredSize().height);

              //---- TPF2 ----
              TPF2.setComponentPopupMenu(BTD);
              TPF2.setName("TPF2");
              panel2.add(TPF2);
              TPF2.setBounds(105, 83, 70, TPF2.getPreferredSize().height);

              //---- TPD3 ----
              TPD3.setComponentPopupMenu(BTD);
              TPD3.setName("TPD3");
              panel2.add(TPD3);
              TPD3.setBounds(30, 111, 70, TPD3.getPreferredSize().height);

              //---- TPF3 ----
              TPF3.setComponentPopupMenu(BTD);
              TPF3.setName("TPF3");
              panel2.add(TPF3);
              TPF3.setBounds(105, 111, 70, TPF3.getPreferredSize().height);

              //---- TPD4 ----
              TPD4.setComponentPopupMenu(BTD);
              TPD4.setName("TPD4");
              panel2.add(TPD4);
              TPD4.setBounds(30, 139, 70, TPD4.getPreferredSize().height);

              //---- TPF4 ----
              TPF4.setComponentPopupMenu(BTD);
              TPF4.setName("TPF4");
              panel2.add(TPF4);
              TPF4.setBounds(105, 139, 70, TPF4.getPreferredSize().height);

              //---- TPD5 ----
              TPD5.setComponentPopupMenu(BTD);
              TPD5.setName("TPD5");
              panel2.add(TPD5);
              TPD5.setBounds(30, 167, 70, TPD5.getPreferredSize().height);

              //---- TPF5 ----
              TPF5.setComponentPopupMenu(BTD);
              TPF5.setName("TPF5");
              panel2.add(TPF5);
              TPF5.setBounds(105, 167, 70, TPF5.getPreferredSize().height);

              //---- TPD6 ----
              TPD6.setComponentPopupMenu(BTD);
              TPD6.setName("TPD6");
              panel2.add(TPD6);
              TPD6.setBounds(30, 195, 70, TPD6.getPreferredSize().height);

              //---- TPF6 ----
              TPF6.setComponentPopupMenu(BTD);
              TPF6.setName("TPF6");
              panel2.add(TPF6);
              TPF6.setBounds(105, 195, 70, TPF6.getPreferredSize().height);

              //---- TPD7 ----
              TPD7.setComponentPopupMenu(BTD);
              TPD7.setName("TPD7");
              panel2.add(TPD7);
              TPD7.setBounds(30, 223, 70, TPD7.getPreferredSize().height);

              //---- TPF7 ----
              TPF7.setComponentPopupMenu(BTD);
              TPF7.setName("TPF7");
              panel2.add(TPF7);
              TPF7.setBounds(105, 223, 70, TPF7.getPreferredSize().height);

              //---- TPD8 ----
              TPD8.setComponentPopupMenu(BTD);
              TPD8.setName("TPD8");
              panel2.add(TPD8);
              TPD8.setBounds(30, 251, 70, TPD8.getPreferredSize().height);

              //---- TPF8 ----
              TPF8.setComponentPopupMenu(BTD);
              TPF8.setName("TPF8");
              panel2.add(TPF8);
              TPF8.setBounds(105, 251, 70, TPF8.getPreferredSize().height);

              //---- TPD9 ----
              TPD9.setComponentPopupMenu(BTD);
              TPD9.setName("TPD9");
              panel2.add(TPD9);
              TPD9.setBounds(30, 279, 70, TPD9.getPreferredSize().height);

              //---- TPF9 ----
              TPF9.setComponentPopupMenu(BTD);
              TPF9.setName("TPF9");
              panel2.add(TPF9);
              TPF9.setBounds(105, 279, 70, TPF9.getPreferredSize().height);

              //---- TPD10 ----
              TPD10.setComponentPopupMenu(BTD);
              TPD10.setName("TPD10");
              panel2.add(TPD10);
              TPD10.setBounds(30, 307, 70, TPD10.getPreferredSize().height);

              //---- TPF10 ----
              TPF10.setComponentPopupMenu(BTD);
              TPF10.setName("TPF10");
              panel2.add(TPF10);
              TPF10.setBounds(105, 307, 70, TPF10.getPreferredSize().height);

              //---- TPD11 ----
              TPD11.setComponentPopupMenu(BTD);
              TPD11.setName("TPD11");
              panel2.add(TPD11);
              TPD11.setBounds(30, 335, 70, TPD11.getPreferredSize().height);

              //---- TPF11 ----
              TPF11.setComponentPopupMenu(BTD);
              TPF11.setName("TPF11");
              panel2.add(TPF11);
              TPF11.setBounds(105, 335, 70, TPF11.getPreferredSize().height);

              //---- TPD12 ----
              TPD12.setComponentPopupMenu(BTD);
              TPD12.setName("TPD12");
              panel2.add(TPD12);
              TPD12.setBounds(30, 363, 70, TPD12.getPreferredSize().height);

              //---- TPF12 ----
              TPF12.setComponentPopupMenu(BTD);
              TPF12.setName("TPF12");
              panel2.add(TPF12);
              TPF12.setBounds(105, 363, 70, TPF12.getPreferredSize().height);

              //---- OBJ_127_OBJ_127 ----
              OBJ_127_OBJ_127.setText("Exclus");
              OBJ_127_OBJ_127.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_127_OBJ_127.setFont(OBJ_127_OBJ_127.getFont().deriveFont(OBJ_127_OBJ_127.getFont().getStyle() | Font.BOLD));
              OBJ_127_OBJ_127.setName("OBJ_127_OBJ_127");
              panel2.add(OBJ_127_OBJ_127);
              OBJ_127_OBJ_127.setBounds(182, 35, 40, OBJ_127_OBJ_127.getPreferredSize().height);

              //---- OBJ_125_OBJ_125 ----
              OBJ_125_OBJ_125.setText("D\u00e9but");
              OBJ_125_OBJ_125.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_125_OBJ_125.setFont(OBJ_125_OBJ_125.getFont().deriveFont(OBJ_125_OBJ_125.getFont().getStyle() | Font.BOLD));
              OBJ_125_OBJ_125.setName("OBJ_125_OBJ_125");
              panel2.add(OBJ_125_OBJ_125);
              OBJ_125_OBJ_125.setBounds(30, 35, 70, OBJ_125_OBJ_125.getPreferredSize().height);

              //---- TPN1 ----
              TPN1.setText("");
              TPN1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TPN1.setName("TPN1");
              panel2.add(TPN1);
              TPN1.setBounds(195, 59, 25, 20);

              //---- TPN2 ----
              TPN2.setText("");
              TPN2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TPN2.setName("TPN2");
              panel2.add(TPN2);
              TPN2.setBounds(195, 87, 25, 20);

              //---- TPN3 ----
              TPN3.setText("");
              TPN3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TPN3.setName("TPN3");
              panel2.add(TPN3);
              TPN3.setBounds(195, 115, 25, 20);

              //---- TPN4 ----
              TPN4.setText("");
              TPN4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TPN4.setName("TPN4");
              panel2.add(TPN4);
              TPN4.setBounds(195, 143, 25, 20);

              //---- TPN5 ----
              TPN5.setText("");
              TPN5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TPN5.setName("TPN5");
              panel2.add(TPN5);
              TPN5.setBounds(195, 171, 25, 20);

              //---- TPN6 ----
              TPN6.setText("");
              TPN6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TPN6.setName("TPN6");
              panel2.add(TPN6);
              TPN6.setBounds(195, 199, 25, 20);

              //---- TPN7 ----
              TPN7.setText("");
              TPN7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TPN7.setName("TPN7");
              panel2.add(TPN7);
              TPN7.setBounds(195, 227, 25, 20);

              //---- TPN8 ----
              TPN8.setText("");
              TPN8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TPN8.setName("TPN8");
              panel2.add(TPN8);
              TPN8.setBounds(195, 255, 25, 20);

              //---- TPN9 ----
              TPN9.setText("");
              TPN9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TPN9.setName("TPN9");
              panel2.add(TPN9);
              TPN9.setBounds(195, 283, 25, 20);

              //---- TPN10 ----
              TPN10.setText("");
              TPN10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TPN10.setName("TPN10");
              panel2.add(TPN10);
              TPN10.setBounds(195, 311, 25, 20);

              //---- TPN11 ----
              TPN11.setText("");
              TPN11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TPN11.setName("TPN11");
              panel2.add(TPN11);
              TPN11.setBounds(195, 339, 25, 20);

              //---- TPN12 ----
              TPN12.setText("");
              TPN12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TPN12.setName("TPN12");
              panel2.add(TPN12);
              TPN12.setBounds(195, 367, 25, 20);

              //---- OBJ_126_OBJ_126 ----
              OBJ_126_OBJ_126.setText("Fin");
              OBJ_126_OBJ_126.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_126_OBJ_126.setFont(OBJ_126_OBJ_126.getFont().deriveFont(OBJ_126_OBJ_126.getFont().getStyle() | Font.BOLD));
              OBJ_126_OBJ_126.setName("OBJ_126_OBJ_126");
              panel2.add(OBJ_126_OBJ_126);
              OBJ_126_OBJ_126.setBounds(105, 35, 70, OBJ_126_OBJ_126.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }

            //======== panel3 ========
            {
              panel3.setBorder(new CompoundBorder(
                new TitledBorder("Caract\u00e9ristiques d'\u00e9dition"),
                new EmptyBorder(5, 5, 5, 5)));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- TBEDT ----
              TBEDT.setText("Top d'occultation");
              TBEDT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TBEDT.setName("TBEDT");
              panel3.add(TBEDT);
              TBEDT.setBounds(20, 100, 129, 20);

              //---- TBLAP ----
              TBLAP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TBLAP.setName("TBLAP");
              panel3.add(TBLAP);
              TBLAP.setBounds(301, 30, 28, 30);

              //---- TBSSL ----
              TBSSL.setText("Soulign\u00e9");
              TBSSL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TBSSL.setName("TBSSL");
              panel3.add(TBSSL);
              TBSSL.setBounds(20, 70, 78, 20);

              //---- OBJ_123_OBJ_123 ----
              OBJ_123_OBJ_123.setText("Tabulation");
              OBJ_123_OBJ_123.setName("OBJ_123_OBJ_123");
              panel3.add(OBJ_123_OBJ_123);
              OBJ_123_OBJ_123.setBounds(20, 133, 69, 20);

              //---- TBGRA ----
              TBGRA.setText("Gras");
              TBGRA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TBGRA.setName("TBGRA");
              panel3.add(TBGRA);
              TBGRA.setBounds(210, 70, 54, 20);

              //---- label1 ----
              label1.setText("Lignes apr\u00e8s");
              label1.setName("label1");
              panel3.add(label1);
              label1.setBounds(210, 30, 90, 30);

              //---- label2 ----
              label2.setText("Lignes avant");
              label2.setName("label2");
              panel3.add(label2);
              label2.setBounds(20, 30, 90, 30);

              //---- TBLAV ----
              TBLAV.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TBLAV.setName("TBLAV");
              panel3.add(TBLAV);
              TBLAV.setBounds(110, 30, 28, 30);

              //---- TBMAR ----
              TBMAR.setName("TBMAR");
              panel3.add(TBMAR);
              TBMAR.setBounds(149, 130, 180, TBMAR.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }

            GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
            xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
            xTitledPanel1ContentContainerLayout.setHorizontalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_118_OBJ_118, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(TBLIB, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
                      .addGap(24, 24, 24)
                      .addComponent(OBJ_119_OBJ_119, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(TBTYP, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_120_OBJ_120, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(TBSNS, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 245, GroupLayout.PREFERRED_SIZE)
                      .addGap(20, 20, 20)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 355, GroupLayout.PREFERRED_SIZE)
                        .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 355, GroupLayout.PREFERRED_SIZE)))))
            );
            xTitledPanel1ContentContainerLayout.setVerticalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(13, 13, 13)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_118_OBJ_118, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(TBLIB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_119_OBJ_119, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(TBTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(9, 9, 9)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_120_OBJ_120, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(TBSNS, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
                  .addGap(8, 8, 8)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 415, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                      .addGap(15, 15, 15)
                      .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 220, GroupLayout.PREFERRED_SIZE))))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_20 ----
      OBJ_20.setText("Choix possibles");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);

      //---- OBJ_19 ----
      OBJ_19.setText("Aide en ligne");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44_OBJ_45;
  private XRiTextField INDETB;
  private JLabel OBJ_45_OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_46_OBJ_47;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private XRiComboBox TBTYP;
  private XRiTextField TBLIB;
  private JLabel OBJ_120_OBJ_120;
  private XRiComboBox TBSNS;
  private JLabel OBJ_118_OBJ_118;
  private JLabel OBJ_119_OBJ_119;
  private JPanel panel1;
  private XRiTextField STL01;
  private XRiTextField STL02;
  private XRiTextField STL03;
  private XRiTextField STL04;
  private XRiTextField STL05;
  private JLabel OBJ_129_OBJ_129;
  private XRiTextField TBT01;
  private XRiTextField TBT02;
  private XRiTextField TBT03;
  private XRiTextField TBT04;
  private XRiTextField TBT05;
  private JLabel OBJ_128_OBJ_128;
  private XRiTextField TBO01;
  private XRiTextField TBO02;
  private XRiTextField TBO03;
  private XRiTextField TBO04;
  private JPanel panel2;
  private XRiTextField TPD1;
  private XRiTextField TPF1;
  private XRiTextField TPD2;
  private XRiTextField TPF2;
  private XRiTextField TPD3;
  private XRiTextField TPF3;
  private XRiTextField TPD4;
  private XRiTextField TPF4;
  private XRiTextField TPD5;
  private XRiTextField TPF5;
  private XRiTextField TPD6;
  private XRiTextField TPF6;
  private XRiTextField TPD7;
  private XRiTextField TPF7;
  private XRiTextField TPD8;
  private XRiTextField TPF8;
  private XRiTextField TPD9;
  private XRiTextField TPF9;
  private XRiTextField TPD10;
  private XRiTextField TPF10;
  private XRiTextField TPD11;
  private XRiTextField TPF11;
  private XRiTextField TPD12;
  private XRiTextField TPF12;
  private JLabel OBJ_127_OBJ_127;
  private JLabel OBJ_125_OBJ_125;
  private XRiCheckBox TPN1;
  private XRiCheckBox TPN2;
  private XRiCheckBox TPN3;
  private XRiCheckBox TPN4;
  private XRiCheckBox TPN5;
  private XRiCheckBox TPN6;
  private XRiCheckBox TPN7;
  private XRiCheckBox TPN8;
  private XRiCheckBox TPN9;
  private XRiCheckBox TPN10;
  private XRiCheckBox TPN11;
  private XRiCheckBox TPN12;
  private JLabel OBJ_126_OBJ_126;
  private JPanel panel3;
  private XRiCheckBox TBEDT;
  private XRiTextField TBLAP;
  private XRiCheckBox TBSSL;
  private JLabel OBJ_123_OBJ_123;
  private XRiCheckBox TBGRA;
  private JLabel label1;
  private JLabel label2;
  private XRiTextField TBLAV;
  private XRiComboBox TBMAR;
  private JPopupMenu BTD;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
