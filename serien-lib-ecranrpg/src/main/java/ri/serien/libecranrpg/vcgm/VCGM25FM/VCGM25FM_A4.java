
package ri.serien.libecranrpg.vcgm.VCGM25FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM25FM_A4 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _MOD1_Top = { "MOD1", "MOD2", "MOD3", "MOD4", "MOD5", "MOD6", "MOD7", "MOD8", "MOD9", "MOD10", "MOD11", "MOD12", "MOD13",
      "MOD14", "MOD15", "MOD16", "MOD17", };
  private String[] _MOD1_Title = { "", };
  private String[][] _MOD1_Data = { { "TIT1", }, { "TIT2", }, { "TIT3", }, { "TIT4", }, { "TIT5", }, { "TIT6", }, { "TIT7", }, { "TIT8", },
      { "TIT9", }, { "TIT10", }, { "TIT11", }, { "TIT12", }, { "TIT13", }, { "TIT14", }, { "TIT15", }, { "TIT16", }, { "TIT17", }, };
  private int[] _MOD1_Width = { 273, };
  
  public VCGM25FM_A4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    RELCRE.setValeursSelection("OUI", "NON");
    MOD1.setAspectTable(_MOD1_Top, _MOD1_Title, _MOD1_Data, _MOD1_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RELTIT@ @LIBLET@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // Ajoute à la liste des oData les variables non liées directement à un composant graphique
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _MOD1_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    

    
    


    
    
    
    // RELCRE.setSelected(lexique.HostFieldGetData("RELCRE").equalsIgnoreCase("OUI"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @RELTIT@ - @LIBLET@"));
    
    

    
    p_bpresentation.setCodeEtablissement(RELSOC.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(RELSOC.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (RELCRE.isSelected())
    // lexique.HostFieldPutData("RELCRE", 0, "OUI");
    // else
    // lexique.HostFieldPutData("RELCRE", 0, "NON");
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void MOD1MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _MOD1_Top, "X", "ENTER", e);
    MOD1.setValeurTop("X");
    if (MOD1.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_24 = new JLabel();
    RELSOC = new XRiTextField();
    OBJ_25 = new JLabel();
    RELCOL = new XRiTextField();
    OBJ_26 = new JLabel();
    RELDEB = new XRiTextField();
    OBJ_29 = new JLabel();
    RELFIN = new XRiTextField();
    OBJ_30 = new JLabel();
    RELLIM = new XRiCalendrier();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel3 = new JPanel();
    panel1 = new JPanel();
    OBJ_75 = new JLabel();
    OBJ_78 = new JLabel();
    RELDEV = new XRiTextField();
    RELCRE = new XRiCheckBox();
    RELLET = new XRiTextField();
    panel2 = new JPanel();
    TRIREP = new XRiTextField();
    OBJ_86 = new JLabel();
    OBJ_27 = new JLabel();
    SELREP = new XRiTextField();
    SELCDP = new XRiTextField();
    SELSEL = new XRiTextField();
    OBJ_84 = new JLabel();
    OBJ_28 = new JLabel();
    OBJ_85 = new JLabel();
    OBJ_83 = new JLabel();
    TRIDEP = new XRiTextField();
    TRIS5 = new XRiTextField();
    TRIS4 = new XRiTextField();
    TRIS3 = new XRiTextField();
    TRIS2 = new XRiTextField();
    TRIS1 = new XRiTextField();
    panel4 = new JPanel();
    SCROLLPANE_MOD1 = new JScrollPane();
    MOD1 = new XRiTable();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@RELTIT@ @LIBLET@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_24 ----
          OBJ_24.setText("Soci\u00e9t\u00e9");
          OBJ_24.setName("OBJ_24");

          //---- RELSOC ----
          RELSOC.setComponentPopupMenu(BTD);
          RELSOC.setName("RELSOC");

          //---- OBJ_25 ----
          OBJ_25.setText("Collectif");
          OBJ_25.setName("OBJ_25");

          //---- RELCOL ----
          RELCOL.setComponentPopupMenu(BTD);
          RELCOL.setName("RELCOL");

          //---- OBJ_26 ----
          OBJ_26.setText("Auxiliaire de d\u00e9but");
          OBJ_26.setName("OBJ_26");

          //---- RELDEB ----
          RELDEB.setComponentPopupMenu(BTD);
          RELDEB.setName("RELDEB");

          //---- OBJ_29 ----
          OBJ_29.setText("Auxiliaire de fin");
          OBJ_29.setName("OBJ_29");

          //---- RELFIN ----
          RELFIN.setComponentPopupMenu(BTD);
          RELFIN.setName("RELFIN");

          //---- OBJ_30 ----
          OBJ_30.setText("Date limite");
          OBJ_30.setName("OBJ_30");

          //---- RELLIM ----
          RELLIM.setComponentPopupMenu(BTD);
          RELLIM.setName("RELLIM");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(RELSOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(RELCOL, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_26, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(RELDEB, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(RELFIN, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(RELLIM, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(RELSOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(RELCOL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(RELDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(RELFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(RELLIM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_24)
                  .addComponent(OBJ_25)
                  .addComponent(OBJ_26)
                  .addComponent(OBJ_29)
                  .addComponent(OBJ_30)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(990, 400));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //======== panel1 ========
            {
              panel1.setBorder(new TitledBorder("Options de retards paiement"));
              panel1.setOpaque(false);
              panel1.setName("panel1");

              //---- OBJ_75 ----
              OBJ_75.setText("Type de lettre \u00e0 \u00e9diter");
              OBJ_75.setName("OBJ_75");

              //---- OBJ_78 ----
              OBJ_78.setText("Comptes dont le code devise est");
              OBJ_78.setName("OBJ_78");

              //---- RELDEV ----
              RELDEV.setComponentPopupMenu(BTD);
              RELDEV.setName("RELDEV");

              //---- RELCRE ----
              RELCRE.setText("Cr\u00e9dits non point\u00e9s");
              RELCRE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              RELCRE.setName("RELCRE");

              //---- RELLET ----
              RELLET.setComponentPopupMenu(BTD);
              RELLET.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              RELLET.setName("RELLET");

              GroupLayout panel1Layout = new GroupLayout(panel1);
              panel1.setLayout(panel1Layout);
              panel1Layout.setHorizontalGroup(
                panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(26, 26, 26)
                    .addGroup(panel1Layout.createParallelGroup()
                      .addComponent(OBJ_78, GroupLayout.PREFERRED_SIZE, 204, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_75, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE))
                    .addGap(6, 6, 6)
                    .addGroup(panel1Layout.createParallelGroup()
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addComponent(RELLET, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addGap(43, 43, 43)
                        .addComponent(RELCRE, GroupLayout.PREFERRED_SIZE, 148, GroupLayout.PREFERRED_SIZE))
                      .addComponent(RELDEV, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(25, Short.MAX_VALUE))
              );
              panel1Layout.setVerticalGroup(
                panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(25, 25, 25)
                    .addGroup(panel1Layout.createParallelGroup()
                      .addComponent(OBJ_75)
                      .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(RELCRE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(RELLET, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                    .addGap(7, 7, 7)
                    .addGroup(panel1Layout.createParallelGroup()
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(OBJ_78))
                      .addComponent(RELDEV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
              );
            }
            panel3.add(panel1);
            panel1.setBounds(5, 5, 510, 146);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("Crit\u00e8res de tri ou code \u00e0 s\u00e9lectionner"));
              panel2.setOpaque(false);
              panel2.setName("panel2");

              //---- TRIREP ----
              TRIREP.setComponentPopupMenu(BTD);
              TRIREP.setName("TRIREP");

              //---- OBJ_86 ----
              OBJ_86.setText("Tri par repr\u00e9sentant");
              OBJ_86.setName("OBJ_86");

              //---- OBJ_27 ----
              OBJ_27.setText("ou");
              OBJ_27.setName("OBJ_27");

              //---- SELREP ----
              SELREP.setComponentPopupMenu(BTD);
              SELREP.setName("SELREP");

              //---- SELCDP ----
              SELCDP.setComponentPopupMenu(BTD);
              SELCDP.setName("SELCDP");

              //---- SELSEL ----
              SELSEL.setComponentPopupMenu(BTD);
              SELSEL.setName("SELSEL");

              //---- OBJ_84 ----
              OBJ_84.setText("ou");
              OBJ_84.setName("OBJ_84");

              //---- OBJ_28 ----
              OBJ_28.setText("ou");
              OBJ_28.setName("OBJ_28");

              //---- OBJ_85 ----
              OBJ_85.setText("Tri par d\u00e9partement");
              OBJ_85.setName("OBJ_85");

              //---- OBJ_83 ----
              OBJ_83.setText("Tri par crit\u00e8re de s\u00e9lection");
              OBJ_83.setName("OBJ_83");

              //---- TRIDEP ----
              TRIDEP.setComponentPopupMenu(BTD);
              TRIDEP.setName("TRIDEP");

              //---- TRIS5 ----
              TRIS5.setComponentPopupMenu(BTD);
              TRIS5.setName("TRIS5");

              //---- TRIS4 ----
              TRIS4.setComponentPopupMenu(BTD);
              TRIS4.setName("TRIS4");

              //---- TRIS3 ----
              TRIS3.setComponentPopupMenu(BTD);
              TRIS3.setName("TRIS3");

              //---- TRIS2 ----
              TRIS2.setComponentPopupMenu(BTD);
              TRIS2.setName("TRIS2");

              //---- TRIS1 ----
              TRIS1.setComponentPopupMenu(BTD);
              TRIS1.setName("TRIS1");

              GroupLayout panel2Layout = new GroupLayout(panel2);
              panel2.setLayout(panel2Layout);
              panel2Layout.setHorizontalGroup(
                panel2Layout.createParallelGroup()
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGroup(panel2Layout.createParallelGroup()
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(136, 136, 136)
                        .addComponent(TRIREP, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addGap(15, 15, 15)
                        .addComponent(OBJ_86, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)
                        .addGap(15, 15, 15)
                        .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
                        .addGap(13, 13, 13)
                        .addComponent(SELREP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(136, 136, 136)
                        .addComponent(TRIDEP, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addGap(15, 15, 15)
                        .addComponent(OBJ_85, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)
                        .addGap(15, 15, 15)
                        .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
                        .addGap(13, 13, 13)
                        .addComponent(SELCDP, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(TRIS1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addGap(7, 7, 7)
                        .addComponent(TRIS2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addGap(7, 7, 7)
                        .addComponent(TRIS3, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addGap(7, 7, 7)
                        .addComponent(TRIS4, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addGap(9, 9, 9)
                        .addComponent(TRIS5, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addGap(15, 15, 15)
                        .addComponent(OBJ_83, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)
                        .addGap(15, 15, 15)
                        .addComponent(OBJ_84, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
                        .addGap(13, 13, 13)
                        .addComponent(SELSEL, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)))
                    .addContainerGap(29, Short.MAX_VALUE))
              );
              panel2Layout.setVerticalGroup(
                panel2Layout.createParallelGroup()
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(43, 43, 43)
                    .addGroup(panel2Layout.createParallelGroup()
                      .addComponent(TRIREP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(SELREP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(panel2Layout.createParallelGroup()
                          .addComponent(OBJ_86)
                          .addComponent(OBJ_27))))
                    .addGap(7, 7, 7)
                    .addGroup(panel2Layout.createParallelGroup()
                      .addComponent(TRIDEP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(SELCDP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(panel2Layout.createParallelGroup()
                          .addComponent(OBJ_85)
                          .addComponent(OBJ_28))))
                    .addGap(7, 7, 7)
                    .addGroup(panel2Layout.createParallelGroup()
                      .addComponent(TRIS1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(TRIS2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(TRIS3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(TRIS4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(TRIS5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(OBJ_83))
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(OBJ_84, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(SELSEL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
              );
            }
            panel3.add(panel2);
            panel2.setBounds(5, 155, 510, 210);

            //======== panel4 ========
            {
              panel4.setBorder(new TitledBorder("Crit\u00e8res de retards paiement"));
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);

              //======== SCROLLPANE_MOD1 ========
              {
                SCROLLPANE_MOD1.setToolTipText("<HTML>Cr\u00e9ation ou modification<BR>d'un crit\u00e8re de relance</HTML>");
                SCROLLPANE_MOD1.setName("SCROLLPANE_MOD1");

                //---- MOD1 ----
                MOD1.setName("MOD1");
                MOD1.addMouseListener(new MouseAdapter() {
                  @Override
                  public void mouseClicked(MouseEvent e) {
                    MOD1MouseClicked(e);
                  }
                });
                SCROLLPANE_MOD1.setViewportView(MOD1);
              }
              panel4.add(SCROLLPANE_MOD1);
              SCROLLPANE_MOD1.setBounds(20, 40, 393, 290);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel4.getComponentCount(); i++) {
                  Rectangle bounds = panel4.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel4.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel4.setMinimumSize(preferredSize);
                panel4.setPreferredSize(preferredSize);
              }
            }
            panel3.add(panel4);
            panel4.setBounds(525, 5, 435, 360);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 965, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(11, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 370, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(15, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_24;
  private XRiTextField RELSOC;
  private JLabel OBJ_25;
  private XRiTextField RELCOL;
  private JLabel OBJ_26;
  private XRiTextField RELDEB;
  private JLabel OBJ_29;
  private XRiTextField RELFIN;
  private JLabel OBJ_30;
  private XRiCalendrier RELLIM;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel3;
  private JPanel panel1;
  private JLabel OBJ_75;
  private JLabel OBJ_78;
  private XRiTextField RELDEV;
  private XRiCheckBox RELCRE;
  private XRiTextField RELLET;
  private JPanel panel2;
  private XRiTextField TRIREP;
  private JLabel OBJ_86;
  private JLabel OBJ_27;
  private XRiTextField SELREP;
  private XRiTextField SELCDP;
  private XRiTextField SELSEL;
  private JLabel OBJ_84;
  private JLabel OBJ_28;
  private JLabel OBJ_85;
  private JLabel OBJ_83;
  private XRiTextField TRIDEP;
  private XRiTextField TRIS5;
  private XRiTextField TRIS4;
  private XRiTextField TRIS3;
  private XRiTextField TRIS2;
  private XRiTextField TRIS1;
  private JPanel panel4;
  private JScrollPane SCROLLPANE_MOD1;
  private XRiTable MOD1;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
