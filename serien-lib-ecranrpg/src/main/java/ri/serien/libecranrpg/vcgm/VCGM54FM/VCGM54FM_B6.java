
package ri.serien.libecranrpg.vcgm.VCGM54FM;
// Nom Fichier: pop_VCGM54FM_FMTB6_594.java

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGM54FM_B6 extends SNPanelEcranRPG implements ioFrame {
  
  private static final String BOUTON_VALEUR_INITIALE = "Valeurs initiales";
  
  public VCGM54FM_B6(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Barre des Boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    
    // - Ajouter un bouton personnalisé
    snBarreBouton.ajouterBouton(BOUTON_VALEUR_INITIALE, 'i', true);
    // -
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    pnlDifference.setVisible(isTrue("19"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Préimputation"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_VALEUR_INITIALE)) {
        lexique.HostScreenSendKey(this, "F5");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btChoixPossibleActionPerformed(ActionEvent e) {
    try {
      lexique.HostCursorPut(BTD.getInvoker().getName());
      lexique.HostScreenSendKey(this, "F4");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    snBarreBouton = new SNBarreBouton();
    sNPanelContenu1 = new SNPanelContenu();
    pnlCharges = new SNPanelTitre();
    lbCompte = new SNLabelUnite();
    lbMontant = new SNLabelUnite();
    lbLibelle = new SNLabelUnite();
    lbSection = new SNLabelUnite();
    lbAffaire = new SNLabelUnite();
    lbNature = new SNLabelUnite();
    CGE01 = new XRiTextField();
    MTE01 = new XRiTextField();
    LIE01 = new XRiTextField();
    WSAN01 = new XRiTextField();
    WAFF01 = new XRiTextField();
    WNAT01 = new XRiTextField();
    CGE02 = new XRiTextField();
    MTE02 = new XRiTextField();
    LIE02 = new XRiTextField();
    WSAN02 = new XRiTextField();
    WAFF02 = new XRiTextField();
    WNAT02 = new XRiTextField();
    CGE03 = new XRiTextField();
    MTE03 = new XRiTextField();
    LIE03 = new XRiTextField();
    WSAN03 = new XRiTextField();
    WAFF03 = new XRiTextField();
    WNAT03 = new XRiTextField();
    CGE04 = new XRiTextField();
    MTE04 = new XRiTextField();
    LIE04 = new XRiTextField();
    WSAN04 = new XRiTextField();
    WAFF04 = new XRiTextField();
    WNAT04 = new XRiTextField();
    CGE05 = new XRiTextField();
    MTE05 = new XRiTextField();
    LIE05 = new XRiTextField();
    WSAN05 = new XRiTextField();
    WAFF05 = new XRiTextField();
    WNAT05 = new XRiTextField();
    pnlTva = new SNPanelTitre();
    lbCompte2 = new SNLabelUnite();
    lbMontant2 = new SNLabelUnite();
    lbLibelle2 = new SNLabelUnite();
    C6CGT = new XRiTextField();
    C6MTT = new XRiTextField();
    C6LIT = new XRiTextField();
    pnlDifference = new SNPanel();
    sNLabelChamp1 = new SNLabelChamp();
    WDIER = new XRiTextField();
    BTD = new JPopupMenu();
    btChoixPossible = new JMenuItem();

    //======== this ========
    setPreferredSize(new Dimension(710, 500));
    setMinimumSize(new Dimension(710, 500));
    setMaximumSize(new Dimension(710, 500));
    setName("this");
    setLayout(new BorderLayout());

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //======== sNPanelContenu1 ========
    {
      sNPanelContenu1.setName("sNPanelContenu1");
      sNPanelContenu1.setLayout(new GridBagLayout());
      ((GridBagLayout)sNPanelContenu1.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)sNPanelContenu1.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
      ((GridBagLayout)sNPanelContenu1.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)sNPanelContenu1.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

      //======== pnlCharges ========
      {
        pnlCharges.setTitre("Charges");
        pnlCharges.setName("pnlCharges");
        pnlCharges.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlCharges.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0, 0};
        ((GridBagLayout)pnlCharges.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0};
        ((GridBagLayout)pnlCharges.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnlCharges.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

        //---- lbCompte ----
        lbCompte.setText("Compte");
        lbCompte.setPreferredSize(new Dimension(70, 30));
        lbCompte.setMinimumSize(new Dimension(70, 30));
        lbCompte.setName("lbCompte");
        pnlCharges.add(lbCompte, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- lbMontant ----
        lbMontant.setText("Montant");
        lbMontant.setPreferredSize(new Dimension(70, 30));
        lbMontant.setMinimumSize(new Dimension(70, 30));
        lbMontant.setName("lbMontant");
        pnlCharges.add(lbMontant, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- lbLibelle ----
        lbLibelle.setText("Libell\u00e9 de l'\u00e9criture");
        lbLibelle.setPreferredSize(new Dimension(70, 30));
        lbLibelle.setMinimumSize(new Dimension(70, 30));
        lbLibelle.setName("lbLibelle");
        pnlCharges.add(lbLibelle, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- lbSection ----
        lbSection.setText("Section");
        lbSection.setPreferredSize(new Dimension(50, 30));
        lbSection.setMinimumSize(new Dimension(50, 30));
        lbSection.setMaximumSize(new Dimension(50, 30));
        lbSection.setName("lbSection");
        pnlCharges.add(lbSection, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- lbAffaire ----
        lbAffaire.setText("Affaire");
        lbAffaire.setPreferredSize(new Dimension(60, 30));
        lbAffaire.setMinimumSize(new Dimension(60, 30));
        lbAffaire.setName("lbAffaire");
        pnlCharges.add(lbAffaire, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- lbNature ----
        lbNature.setText("Nature");
        lbNature.setPreferredSize(new Dimension(60, 30));
        lbNature.setMinimumSize(new Dimension(60, 30));
        lbNature.setName("lbNature");
        pnlCharges.add(lbNature, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- CGE01 ----
        CGE01.setFont(new Font("sansserif", Font.PLAIN, 14));
        CGE01.setPreferredSize(new Dimension(70, 30));
        CGE01.setMinimumSize(new Dimension(70, 30));
        CGE01.setMaximumSize(new Dimension(70, 30));
        CGE01.setComponentPopupMenu(BTD);
        CGE01.setName("CGE01");
        pnlCharges.add(CGE01, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- MTE01 ----
        MTE01.setFont(new Font("sansserif", Font.PLAIN, 14));
        MTE01.setPreferredSize(new Dimension(90, 30));
        MTE01.setMinimumSize(new Dimension(90, 30));
        MTE01.setMaximumSize(new Dimension(90, 30));
        MTE01.setName("MTE01");
        pnlCharges.add(MTE01, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- LIE01 ----
        LIE01.setFont(new Font("sansserif", Font.PLAIN, 14));
        LIE01.setPreferredSize(new Dimension(300, 30));
        LIE01.setMinimumSize(new Dimension(300, 30));
        LIE01.setMaximumSize(new Dimension(300, 30));
        LIE01.setName("LIE01");
        pnlCharges.add(LIE01, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- WSAN01 ----
        WSAN01.setFont(new Font("sansserif", Font.PLAIN, 14));
        WSAN01.setPreferredSize(new Dimension(50, 30));
        WSAN01.setMinimumSize(new Dimension(50, 30));
        WSAN01.setMaximumSize(new Dimension(50, 30));
        WSAN01.setComponentPopupMenu(BTD);
        WSAN01.setName("WSAN01");
        pnlCharges.add(WSAN01, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- WAFF01 ----
        WAFF01.setFont(new Font("sansserif", Font.PLAIN, 14));
        WAFF01.setPreferredSize(new Dimension(60, 30));
        WAFF01.setMinimumSize(new Dimension(60, 30));
        WAFF01.setMaximumSize(new Dimension(60, 30));
        WAFF01.setComponentPopupMenu(BTD);
        WAFF01.setName("WAFF01");
        pnlCharges.add(WAFF01, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- WNAT01 ----
        WNAT01.setFont(new Font("sansserif", Font.PLAIN, 14));
        WNAT01.setPreferredSize(new Dimension(60, 30));
        WNAT01.setMinimumSize(new Dimension(60, 30));
        WNAT01.setMaximumSize(new Dimension(60, 30));
        WNAT01.setComponentPopupMenu(BTD);
        WNAT01.setName("WNAT01");
        pnlCharges.add(WNAT01, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- CGE02 ----
        CGE02.setFont(new Font("sansserif", Font.PLAIN, 14));
        CGE02.setPreferredSize(new Dimension(70, 30));
        CGE02.setMinimumSize(new Dimension(70, 30));
        CGE02.setMaximumSize(new Dimension(70, 30));
        CGE02.setComponentPopupMenu(BTD);
        CGE02.setName("CGE02");
        pnlCharges.add(CGE02, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- MTE02 ----
        MTE02.setFont(new Font("sansserif", Font.PLAIN, 14));
        MTE02.setPreferredSize(new Dimension(90, 30));
        MTE02.setMinimumSize(new Dimension(90, 30));
        MTE02.setMaximumSize(new Dimension(90, 30));
        MTE02.setName("MTE02");
        pnlCharges.add(MTE02, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- LIE02 ----
        LIE02.setFont(new Font("sansserif", Font.PLAIN, 14));
        LIE02.setPreferredSize(new Dimension(300, 30));
        LIE02.setMinimumSize(new Dimension(300, 30));
        LIE02.setMaximumSize(new Dimension(300, 30));
        LIE02.setName("LIE02");
        pnlCharges.add(LIE02, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- WSAN02 ----
        WSAN02.setFont(new Font("sansserif", Font.PLAIN, 14));
        WSAN02.setPreferredSize(new Dimension(50, 30));
        WSAN02.setMinimumSize(new Dimension(50, 30));
        WSAN02.setMaximumSize(new Dimension(50, 30));
        WSAN02.setComponentPopupMenu(BTD);
        WSAN02.setName("WSAN02");
        pnlCharges.add(WSAN02, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- WAFF02 ----
        WAFF02.setFont(new Font("sansserif", Font.PLAIN, 14));
        WAFF02.setPreferredSize(new Dimension(60, 30));
        WAFF02.setMinimumSize(new Dimension(60, 30));
        WAFF02.setMaximumSize(new Dimension(60, 30));
        WAFF02.setComponentPopupMenu(BTD);
        WAFF02.setName("WAFF02");
        pnlCharges.add(WAFF02, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- WNAT02 ----
        WNAT02.setFont(new Font("sansserif", Font.PLAIN, 14));
        WNAT02.setPreferredSize(new Dimension(60, 30));
        WNAT02.setMinimumSize(new Dimension(60, 30));
        WNAT02.setMaximumSize(new Dimension(60, 30));
        WNAT02.setComponentPopupMenu(BTD);
        WNAT02.setName("WNAT02");
        pnlCharges.add(WNAT02, new GridBagConstraints(5, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- CGE03 ----
        CGE03.setFont(new Font("sansserif", Font.PLAIN, 14));
        CGE03.setPreferredSize(new Dimension(70, 30));
        CGE03.setMinimumSize(new Dimension(70, 30));
        CGE03.setMaximumSize(new Dimension(70, 30));
        CGE03.setComponentPopupMenu(BTD);
        CGE03.setName("CGE03");
        pnlCharges.add(CGE03, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- MTE03 ----
        MTE03.setFont(new Font("sansserif", Font.PLAIN, 14));
        MTE03.setPreferredSize(new Dimension(90, 30));
        MTE03.setMinimumSize(new Dimension(90, 30));
        MTE03.setMaximumSize(new Dimension(90, 30));
        MTE03.setName("MTE03");
        pnlCharges.add(MTE03, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- LIE03 ----
        LIE03.setFont(new Font("sansserif", Font.PLAIN, 14));
        LIE03.setPreferredSize(new Dimension(300, 30));
        LIE03.setMinimumSize(new Dimension(300, 30));
        LIE03.setMaximumSize(new Dimension(300, 30));
        LIE03.setName("LIE03");
        pnlCharges.add(LIE03, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- WSAN03 ----
        WSAN03.setFont(new Font("sansserif", Font.PLAIN, 14));
        WSAN03.setPreferredSize(new Dimension(50, 30));
        WSAN03.setMinimumSize(new Dimension(50, 30));
        WSAN03.setMaximumSize(new Dimension(50, 30));
        WSAN03.setComponentPopupMenu(BTD);
        WSAN03.setName("WSAN03");
        pnlCharges.add(WSAN03, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- WAFF03 ----
        WAFF03.setFont(new Font("sansserif", Font.PLAIN, 14));
        WAFF03.setPreferredSize(new Dimension(60, 30));
        WAFF03.setMinimumSize(new Dimension(60, 30));
        WAFF03.setMaximumSize(new Dimension(60, 30));
        WAFF03.setComponentPopupMenu(BTD);
        WAFF03.setName("WAFF03");
        pnlCharges.add(WAFF03, new GridBagConstraints(4, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- WNAT03 ----
        WNAT03.setFont(new Font("sansserif", Font.PLAIN, 14));
        WNAT03.setPreferredSize(new Dimension(60, 30));
        WNAT03.setMinimumSize(new Dimension(60, 30));
        WNAT03.setMaximumSize(new Dimension(60, 30));
        WNAT03.setComponentPopupMenu(BTD);
        WNAT03.setName("WNAT03");
        pnlCharges.add(WNAT03, new GridBagConstraints(5, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- CGE04 ----
        CGE04.setFont(new Font("sansserif", Font.PLAIN, 14));
        CGE04.setPreferredSize(new Dimension(70, 30));
        CGE04.setMinimumSize(new Dimension(70, 30));
        CGE04.setMaximumSize(new Dimension(70, 30));
        CGE04.setComponentPopupMenu(BTD);
        CGE04.setName("CGE04");
        pnlCharges.add(CGE04, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- MTE04 ----
        MTE04.setFont(new Font("sansserif", Font.PLAIN, 14));
        MTE04.setPreferredSize(new Dimension(90, 30));
        MTE04.setMinimumSize(new Dimension(90, 30));
        MTE04.setMaximumSize(new Dimension(90, 30));
        MTE04.setName("MTE04");
        pnlCharges.add(MTE04, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- LIE04 ----
        LIE04.setFont(new Font("sansserif", Font.PLAIN, 14));
        LIE04.setPreferredSize(new Dimension(300, 30));
        LIE04.setMinimumSize(new Dimension(300, 30));
        LIE04.setMaximumSize(new Dimension(300, 30));
        LIE04.setName("LIE04");
        pnlCharges.add(LIE04, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- WSAN04 ----
        WSAN04.setFont(new Font("sansserif", Font.PLAIN, 14));
        WSAN04.setPreferredSize(new Dimension(50, 30));
        WSAN04.setMinimumSize(new Dimension(50, 30));
        WSAN04.setMaximumSize(new Dimension(50, 30));
        WSAN04.setComponentPopupMenu(BTD);
        WSAN04.setName("WSAN04");
        pnlCharges.add(WSAN04, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- WAFF04 ----
        WAFF04.setFont(new Font("sansserif", Font.PLAIN, 14));
        WAFF04.setPreferredSize(new Dimension(60, 30));
        WAFF04.setMinimumSize(new Dimension(60, 30));
        WAFF04.setMaximumSize(new Dimension(60, 30));
        WAFF04.setComponentPopupMenu(BTD);
        WAFF04.setName("WAFF04");
        pnlCharges.add(WAFF04, new GridBagConstraints(4, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- WNAT04 ----
        WNAT04.setFont(new Font("sansserif", Font.PLAIN, 14));
        WNAT04.setPreferredSize(new Dimension(60, 30));
        WNAT04.setMinimumSize(new Dimension(60, 30));
        WNAT04.setMaximumSize(new Dimension(60, 30));
        WNAT04.setComponentPopupMenu(BTD);
        WNAT04.setName("WNAT04");
        pnlCharges.add(WNAT04, new GridBagConstraints(5, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- CGE05 ----
        CGE05.setFont(new Font("sansserif", Font.PLAIN, 14));
        CGE05.setPreferredSize(new Dimension(70, 30));
        CGE05.setMinimumSize(new Dimension(70, 30));
        CGE05.setMaximumSize(new Dimension(70, 30));
        CGE05.setComponentPopupMenu(BTD);
        CGE05.setName("CGE05");
        pnlCharges.add(CGE05, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- MTE05 ----
        MTE05.setFont(new Font("sansserif", Font.PLAIN, 14));
        MTE05.setPreferredSize(new Dimension(90, 30));
        MTE05.setMinimumSize(new Dimension(90, 30));
        MTE05.setMaximumSize(new Dimension(90, 30));
        MTE05.setName("MTE05");
        pnlCharges.add(MTE05, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- LIE05 ----
        LIE05.setFont(new Font("sansserif", Font.PLAIN, 14));
        LIE05.setPreferredSize(new Dimension(300, 30));
        LIE05.setMinimumSize(new Dimension(300, 30));
        LIE05.setMaximumSize(new Dimension(300, 30));
        LIE05.setName("LIE05");
        pnlCharges.add(LIE05, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- WSAN05 ----
        WSAN05.setFont(new Font("sansserif", Font.PLAIN, 14));
        WSAN05.setPreferredSize(new Dimension(50, 30));
        WSAN05.setMinimumSize(new Dimension(50, 30));
        WSAN05.setMaximumSize(new Dimension(50, 30));
        WSAN05.setComponentPopupMenu(BTD);
        WSAN05.setName("WSAN05");
        pnlCharges.add(WSAN05, new GridBagConstraints(3, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- WAFF05 ----
        WAFF05.setFont(new Font("sansserif", Font.PLAIN, 14));
        WAFF05.setPreferredSize(new Dimension(60, 30));
        WAFF05.setMinimumSize(new Dimension(60, 30));
        WAFF05.setMaximumSize(new Dimension(60, 30));
        WAFF05.setComponentPopupMenu(BTD);
        WAFF05.setName("WAFF05");
        pnlCharges.add(WAFF05, new GridBagConstraints(4, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- WNAT05 ----
        WNAT05.setFont(new Font("sansserif", Font.PLAIN, 14));
        WNAT05.setPreferredSize(new Dimension(60, 30));
        WNAT05.setMinimumSize(new Dimension(60, 30));
        WNAT05.setMaximumSize(new Dimension(60, 30));
        WNAT05.setComponentPopupMenu(BTD);
        WNAT05.setName("WNAT05");
        pnlCharges.add(WNAT05, new GridBagConstraints(5, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      sNPanelContenu1.add(pnlCharges, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== pnlTva ========
      {
        pnlTva.setTitre("TVA");
        pnlTva.setName("pnlTva");
        pnlTva.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlTva.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0, 0};
        ((GridBagLayout)pnlTva.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlTva.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnlTva.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //---- lbCompte2 ----
        lbCompte2.setText("Compte");
        lbCompte2.setPreferredSize(new Dimension(70, 30));
        lbCompte2.setMinimumSize(new Dimension(70, 30));
        lbCompte2.setName("lbCompte2");
        pnlTva.add(lbCompte2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- lbMontant2 ----
        lbMontant2.setText("Montant");
        lbMontant2.setPreferredSize(new Dimension(70, 30));
        lbMontant2.setMinimumSize(new Dimension(70, 30));
        lbMontant2.setName("lbMontant2");
        pnlTva.add(lbMontant2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- lbLibelle2 ----
        lbLibelle2.setText("Libell\u00e9 de l'\u00e9criture");
        lbLibelle2.setPreferredSize(new Dimension(70, 30));
        lbLibelle2.setMinimumSize(new Dimension(70, 30));
        lbLibelle2.setName("lbLibelle2");
        pnlTva.add(lbLibelle2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- C6CGT ----
        C6CGT.setFont(new Font("sansserif", Font.PLAIN, 14));
        C6CGT.setPreferredSize(new Dimension(70, 30));
        C6CGT.setMinimumSize(new Dimension(70, 30));
        C6CGT.setMaximumSize(new Dimension(70, 30));
        C6CGT.setComponentPopupMenu(BTD);
        C6CGT.setName("C6CGT");
        pnlTva.add(C6CGT, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- C6MTT ----
        C6MTT.setFont(new Font("sansserif", Font.PLAIN, 14));
        C6MTT.setPreferredSize(new Dimension(90, 30));
        C6MTT.setMinimumSize(new Dimension(90, 30));
        C6MTT.setMaximumSize(new Dimension(90, 30));
        C6MTT.setName("C6MTT");
        pnlTva.add(C6MTT, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- C6LIT ----
        C6LIT.setFont(new Font("sansserif", Font.PLAIN, 14));
        C6LIT.setPreferredSize(new Dimension(300, 30));
        C6LIT.setMinimumSize(new Dimension(300, 30));
        C6LIT.setMaximumSize(new Dimension(300, 30));
        C6LIT.setName("C6LIT");
        pnlTva.add(C6LIT, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));
      }
      sNPanelContenu1.add(pnlTva, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== pnlDifference ========
      {
        pnlDifference.setName("pnlDifference");
        pnlDifference.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlDifference.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlDifference.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlDifference.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnlDifference.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //---- sNLabelChamp1 ----
        sNLabelChamp1.setText("Diff\u00e9rence");
        sNLabelChamp1.setMinimumSize(new Dimension(100, 30));
        sNLabelChamp1.setMaximumSize(new Dimension(100, 30));
        sNLabelChamp1.setPreferredSize(new Dimension(85, 30));
        sNLabelChamp1.setName("sNLabelChamp1");
        pnlDifference.add(sNLabelChamp1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- WDIER ----
        WDIER.setPreferredSize(new Dimension(90, 30));
        WDIER.setMinimumSize(new Dimension(90, 30));
        WDIER.setMaximumSize(new Dimension(90, 30));
        WDIER.setName("WDIER");
        pnlDifference.add(WDIER, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      sNPanelContenu1.add(pnlDifference, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(sNPanelContenu1, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- btChoixPossible ----
      btChoixPossible.setText("Choix possibles");
      btChoixPossible.setFont(new Font("sansserif", Font.PLAIN, 14));
      btChoixPossible.setName("btChoixPossible");
      btChoixPossible.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btChoixPossibleActionPerformed(e);
        }
      });
      BTD.add(btChoixPossible);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu sNPanelContenu1;
  private SNPanelTitre pnlCharges;
  private SNLabelUnite lbCompte;
  private SNLabelUnite lbMontant;
  private SNLabelUnite lbLibelle;
  private SNLabelUnite lbSection;
  private SNLabelUnite lbAffaire;
  private SNLabelUnite lbNature;
  private XRiTextField CGE01;
  private XRiTextField MTE01;
  private XRiTextField LIE01;
  private XRiTextField WSAN01;
  private XRiTextField WAFF01;
  private XRiTextField WNAT01;
  private XRiTextField CGE02;
  private XRiTextField MTE02;
  private XRiTextField LIE02;
  private XRiTextField WSAN02;
  private XRiTextField WAFF02;
  private XRiTextField WNAT02;
  private XRiTextField CGE03;
  private XRiTextField MTE03;
  private XRiTextField LIE03;
  private XRiTextField WSAN03;
  private XRiTextField WAFF03;
  private XRiTextField WNAT03;
  private XRiTextField CGE04;
  private XRiTextField MTE04;
  private XRiTextField LIE04;
  private XRiTextField WSAN04;
  private XRiTextField WAFF04;
  private XRiTextField WNAT04;
  private XRiTextField CGE05;
  private XRiTextField MTE05;
  private XRiTextField LIE05;
  private XRiTextField WSAN05;
  private XRiTextField WAFF05;
  private XRiTextField WNAT05;
  private SNPanelTitre pnlTva;
  private SNLabelUnite lbCompte2;
  private SNLabelUnite lbMontant2;
  private SNLabelUnite lbLibelle2;
  private XRiTextField C6CGT;
  private XRiTextField C6MTT;
  private XRiTextField C6LIT;
  private SNPanel pnlDifference;
  private SNLabelChamp sNLabelChamp1;
  private XRiTextField WDIER;
  private JPopupMenu BTD;
  private JMenuItem btChoixPossible;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
