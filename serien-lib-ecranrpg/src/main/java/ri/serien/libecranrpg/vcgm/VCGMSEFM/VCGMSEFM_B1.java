
package ri.serien.libecranrpg.vcgm.VCGMSEFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGMSEFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  public VCGMSEFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDUSR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDUSR@")).trim());
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")));
    OBJ_156.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPAGE@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    SEJO10.setEnabled(lexique.isPresent("SEJO10"));
    SEJO09.setEnabled(lexique.isPresent("SEJO09"));
    SEJO08.setEnabled(lexique.isPresent("SEJO08"));
    SEJO07.setEnabled(lexique.isPresent("SEJO07"));
    SEJO06.setEnabled(lexique.isPresent("SEJO06"));
    SEJO05.setEnabled(lexique.isPresent("SEJO05"));
    SEJO04.setEnabled(lexique.isPresent("SEJO04"));
    SEJO03.setEnabled(lexique.isPresent("SEJO03"));
    SEJO02.setEnabled(lexique.isPresent("SEJO02"));
    SEJO01.setEnabled(lexique.isPresent("SEJO01"));
    SEJO20.setEnabled(lexique.isPresent("SEJO20"));
    SEJO19.setEnabled(lexique.isPresent("SEJO19"));
    SEJO18.setEnabled(lexique.isPresent("SEJO18"));
    SEJO17.setEnabled(lexique.isPresent("SEJO17"));
    SEJO16.setEnabled(lexique.isPresent("SEJO16"));
    SEJO15.setEnabled(lexique.isPresent("SEJO15"));
    SEJO14.setEnabled(lexique.isPresent("SEJO14"));
    SEJO13.setEnabled(lexique.isPresent("SEJO13"));
    SEJO12.setEnabled(lexique.isPresent("SEJO12"));
    SEJO11.setEnabled(lexique.isPresent("SEJO11"));
    SE2X10.setVisible(lexique.isPresent("SE2X10"));
    SE2X09.setVisible(lexique.isPresent("SE2X09"));
    SE2X08.setVisible(lexique.isPresent("SE2X08"));
    SE2X07.setVisible(lexique.isPresent("SE2X07"));
    SE2X06.setVisible(lexique.isPresent("SE2X06"));
    SE2X05.setVisible(lexique.isPresent("SE2X05"));
    SE2X04.setVisible(lexique.isPresent("SE2X04"));
    SE2X03.setVisible(lexique.isPresent("SE2X03"));
    SE2X02.setVisible(lexique.isPresent("SE2X02"));
    SE2X01.setVisible(lexique.isPresent("SE2X01"));
    SECX10.setEnabled(lexique.isPresent("SECX10"));
    SECX09.setEnabled(lexique.isPresent("SECX09"));
    SECX08.setEnabled(lexique.isPresent("SECX08"));
    SECX07.setEnabled(lexique.isPresent("SECX07"));
    SECX06.setEnabled(lexique.isPresent("SECX06"));
    SECX05.setEnabled(lexique.isPresent("SECX05"));
    SECX04.setEnabled(lexique.isPresent("SECX04"));
    SECX03.setEnabled(lexique.isPresent("SECX03"));
    SECX02.setEnabled(lexique.isPresent("SECX02"));
    SECX01.setEnabled(lexique.isPresent("SECX01"));
    
    

    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_68ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlNord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDUSR = new RiZoneSortie();
    OBJ_64 = new JLabel();
    INDETB = new RiZoneSortie();
    p_tete_droite = new JPanel();
    OBJ_156 = new JLabel();
    OBJ_155 = new JLabel();
    pnlSud = new SNPanelFond();
    pnlMenus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    pnlPrincipal = new SNPanelContenu();
    pnlContenu = new SNPanelContenu();
    pnlInterdits = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    SEJO11 = new XRiTextField();
    SEJO12 = new XRiTextField();
    SEJO13 = new XRiTextField();
    SEJO14 = new XRiTextField();
    SEJO15 = new XRiTextField();
    SEJO16 = new XRiTextField();
    SEJO17 = new XRiTextField();
    SEJO18 = new XRiTextField();
    SEJO19 = new XRiTextField();
    SEJO20 = new XRiTextField();
    SEJO01 = new XRiTextField();
    SEJO02 = new XRiTextField();
    SEJO03 = new XRiTextField();
    SEJO04 = new XRiTextField();
    SEJO05 = new XRiTextField();
    SEJO06 = new XRiTextField();
    SEJO07 = new XRiTextField();
    SEJO08 = new XRiTextField();
    SEJO09 = new XRiTextField();
    SEJO10 = new XRiTextField();
    xTitledPanel2 = new JXTitledPanel();
    SECX01 = new XRiTextField();
    SECX02 = new XRiTextField();
    SECX03 = new XRiTextField();
    SECX04 = new XRiTextField();
    SECX05 = new XRiTextField();
    SECX06 = new XRiTextField();
    SECX07 = new XRiTextField();
    SECX08 = new XRiTextField();
    SECX09 = new XRiTextField();
    SECX10 = new XRiTextField();
    SE2X01 = new XRiTextField();
    SE2X02 = new XRiTextField();
    SE2X03 = new XRiTextField();
    SE2X04 = new XRiTextField();
    SE2X05 = new XRiTextField();
    SE2X06 = new XRiTextField();
    SE2X07 = new XRiTextField();
    SE2X08 = new XRiTextField();
    SE2X09 = new XRiTextField();
    SE2X10 = new XRiTextField();
    OBJ_98 = new JLabel();
    OBJ_99 = new JLabel();
    OBJ_100 = new JLabel();
    OBJ_101 = new JLabel();
    OBJ_103 = new JLabel();
    OBJ_111 = new JLabel();
    OBJ_112 = new JLabel();
    OBJ_113 = new JLabel();
    OBJ_114 = new JLabel();
    OBJ_115 = new JLabel();
    OBJ_68 = new SNBoutonLeger();
    V06F = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlNord ========
    {
      pnlNord.setName("pnlNord");
      pnlNord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("S\u00e9curit\u00e9 de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      pnlNord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- OBJ_42 ----
          OBJ_42.setText("Utilisateur");
          OBJ_42.setName("OBJ_42");
          
          // ---- INDUSR ----
          INDUSR.setComponentPopupMenu(BTD);
          INDUSR.setOpaque(false);
          INDUSR.setText("@INDUSR@");
          INDUSR.setName("INDUSR");
          
          // ---- OBJ_64 ----
          OBJ_64.setText("Etablissement");
          OBJ_64.setName("OBJ_64");
          
          // ---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5)
                  .addGroup(p_tete_gaucheLayout.createParallelGroup()
                      .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(75, 75, 75).addComponent(INDUSR,
                          GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                      .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE))
                  .addGap(65, 65, 65).addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE).addGap(1, 1, 1)
                  .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDUSR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2)
                  .addGroup(p_tete_gaucheLayout.createParallelGroup()
                      .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
          
          // ---- OBJ_156 ----
          OBJ_156.setText("@WPAGE@");
          OBJ_156.setName("OBJ_156");
          p_tete_droite.add(OBJ_156);
          
          // ---- OBJ_155 ----
          OBJ_155.setText("Page    ");
          OBJ_155.setName("OBJ_155");
          p_tete_droite.add(OBJ_155);
        }
        barre_tete.add(p_tete_droite);
      }
      pnlNord.add(barre_tete);
    }
    add(pnlNord, BorderLayout.NORTH);
    
    // ======== pnlSud ========
    {
      pnlSud.setName("pnlSud");
      pnlSud.setLayout(new BorderLayout());
      
      // ======== pnlMenus ========
      {
        pnlMenus.setPreferredSize(new Dimension(170, 0));
        pnlMenus.setMinimumSize(new Dimension(170, 0));
        pnlMenus.setBackground(new Color(238, 239, 241));
        pnlMenus.setBorder(LineBorder.createGrayLineBorder());
        pnlMenus.setName("pnlMenus");
        pnlMenus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        pnlMenus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");
              
              // ---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);
            
            // ======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");
              
              // ---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);
            
            // ======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");
              
              // ---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        pnlMenus.add(scroll_droite, BorderLayout.NORTH);
      }
      pnlSud.add(pnlMenus, BorderLayout.EAST);
      
      // ======== pnlPrincipal ========
      {
        pnlPrincipal.setBackground(new Color(198, 198, 200));
        pnlPrincipal.setName("pnlPrincipal");
        pnlPrincipal.setLayout(new GridBagLayout());
        
        // ======== pnlContenu ========
        {
          pnlContenu.setPreferredSize(new Dimension(890, 490));
          pnlContenu.setBackground(new Color(239, 239, 222));
          pnlContenu.setMinimumSize(new Dimension(880, 600));
          pnlContenu.setName("pnlContenu");
          pnlContenu.setLayout(null);
          
          // ======== pnlInterdits ========
          {
            pnlInterdits.setOpaque(false);
            pnlInterdits.setName("pnlInterdits");
            pnlInterdits.setLayout(null);
            
            // ======== xTitledPanel1 ========
            {
              xTitledPanel1.setBorder(new DropShadowBorder());
              xTitledPanel1.setTitle("Journaux interdits");
              xTitledPanel1.setName("xTitledPanel1");
              Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
              xTitledPanel1ContentContainer.setLayout(null);
              
              // ---- SEJO11 ----
              SEJO11.setComponentPopupMenu(BTD);
              SEJO11.setName("SEJO11");
              xTitledPanel1ContentContainer.add(SEJO11);
              SEJO11.setBounds(430, 45, 30, SEJO11.getPreferredSize().height);
              
              // ---- SEJO12 ----
              SEJO12.setComponentPopupMenu(BTD);
              SEJO12.setName("SEJO12");
              xTitledPanel1ContentContainer.add(SEJO12);
              SEJO12.setBounds(469, 45, 30, SEJO12.getPreferredSize().height);
              
              // ---- SEJO13 ----
              SEJO13.setComponentPopupMenu(BTD);
              SEJO13.setName("SEJO13");
              xTitledPanel1ContentContainer.add(SEJO13);
              SEJO13.setBounds(508, 45, 30, SEJO13.getPreferredSize().height);
              
              // ---- SEJO14 ----
              SEJO14.setComponentPopupMenu(BTD);
              SEJO14.setName("SEJO14");
              xTitledPanel1ContentContainer.add(SEJO14);
              SEJO14.setBounds(547, 45, 30, SEJO14.getPreferredSize().height);
              
              // ---- SEJO15 ----
              SEJO15.setComponentPopupMenu(BTD);
              SEJO15.setName("SEJO15");
              xTitledPanel1ContentContainer.add(SEJO15);
              SEJO15.setBounds(586, 45, 30, SEJO15.getPreferredSize().height);
              
              // ---- SEJO16 ----
              SEJO16.setComponentPopupMenu(BTD);
              SEJO16.setName("SEJO16");
              xTitledPanel1ContentContainer.add(SEJO16);
              SEJO16.setBounds(625, 45, 30, SEJO16.getPreferredSize().height);
              
              // ---- SEJO17 ----
              SEJO17.setComponentPopupMenu(BTD);
              SEJO17.setName("SEJO17");
              xTitledPanel1ContentContainer.add(SEJO17);
              SEJO17.setBounds(664, 45, 30, SEJO17.getPreferredSize().height);
              
              // ---- SEJO18 ----
              SEJO18.setComponentPopupMenu(BTD);
              SEJO18.setName("SEJO18");
              xTitledPanel1ContentContainer.add(SEJO18);
              SEJO18.setBounds(703, 45, 30, SEJO18.getPreferredSize().height);
              
              // ---- SEJO19 ----
              SEJO19.setComponentPopupMenu(BTD);
              SEJO19.setName("SEJO19");
              xTitledPanel1ContentContainer.add(SEJO19);
              SEJO19.setBounds(742, 45, 30, SEJO19.getPreferredSize().height);
              
              // ---- SEJO20 ----
              SEJO20.setComponentPopupMenu(BTD);
              SEJO20.setName("SEJO20");
              xTitledPanel1ContentContainer.add(SEJO20);
              SEJO20.setBounds(781, 45, 30, SEJO20.getPreferredSize().height);
              
              // ---- SEJO01 ----
              SEJO01.setComponentPopupMenu(BTD);
              SEJO01.setName("SEJO01");
              xTitledPanel1ContentContainer.add(SEJO01);
              SEJO01.setBounds(40, 45, 30, SEJO01.getPreferredSize().height);
              
              // ---- SEJO02 ----
              SEJO02.setComponentPopupMenu(BTD);
              SEJO02.setName("SEJO02");
              xTitledPanel1ContentContainer.add(SEJO02);
              SEJO02.setBounds(79, 45, 30, SEJO02.getPreferredSize().height);
              
              // ---- SEJO03 ----
              SEJO03.setComponentPopupMenu(BTD);
              SEJO03.setName("SEJO03");
              xTitledPanel1ContentContainer.add(SEJO03);
              SEJO03.setBounds(118, 45, 30, SEJO03.getPreferredSize().height);
              
              // ---- SEJO04 ----
              SEJO04.setComponentPopupMenu(BTD);
              SEJO04.setName("SEJO04");
              xTitledPanel1ContentContainer.add(SEJO04);
              SEJO04.setBounds(157, 45, 30, SEJO04.getPreferredSize().height);
              
              // ---- SEJO05 ----
              SEJO05.setComponentPopupMenu(BTD);
              SEJO05.setName("SEJO05");
              xTitledPanel1ContentContainer.add(SEJO05);
              SEJO05.setBounds(196, 45, 30, SEJO05.getPreferredSize().height);
              
              // ---- SEJO06 ----
              SEJO06.setComponentPopupMenu(BTD);
              SEJO06.setName("SEJO06");
              xTitledPanel1ContentContainer.add(SEJO06);
              SEJO06.setBounds(235, 45, 30, SEJO06.getPreferredSize().height);
              
              // ---- SEJO07 ----
              SEJO07.setComponentPopupMenu(BTD);
              SEJO07.setName("SEJO07");
              xTitledPanel1ContentContainer.add(SEJO07);
              SEJO07.setBounds(274, 45, 30, SEJO07.getPreferredSize().height);
              
              // ---- SEJO08 ----
              SEJO08.setComponentPopupMenu(BTD);
              SEJO08.setName("SEJO08");
              xTitledPanel1ContentContainer.add(SEJO08);
              SEJO08.setBounds(313, 45, 30, SEJO08.getPreferredSize().height);
              
              // ---- SEJO09 ----
              SEJO09.setComponentPopupMenu(BTD);
              SEJO09.setName("SEJO09");
              xTitledPanel1ContentContainer.add(SEJO09);
              SEJO09.setBounds(352, 45, 30, SEJO09.getPreferredSize().height);
              
              // ---- SEJO10 ----
              SEJO10.setComponentPopupMenu(BTD);
              SEJO10.setName("SEJO10");
              xTitledPanel1ContentContainer.add(SEJO10);
              SEJO10.setBounds(391, 45, 30, SEJO10.getPreferredSize().height);
              
              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = xTitledPanel1ContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
                xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
              }
            }
            pnlInterdits.add(xTitledPanel1);
            xTitledPanel1.setBounds(5, 5, 854, 151);
            
            // ======== xTitledPanel2 ========
            {
              xTitledPanel2.setBorder(new DropShadowBorder());
              xTitledPanel2.setTitle("Comptes interdits");
              xTitledPanel2.setName("xTitledPanel2");
              Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
              xTitledPanel2ContentContainer.setLayout(null);
              
              // ---- SECX01 ----
              SECX01.setComponentPopupMenu(BTD);
              SECX01.setName("SECX01");
              xTitledPanel2ContentContainer.add(SECX01);
              SECX01.setBounds(65, 25, 60, SECX01.getPreferredSize().height);
              
              // ---- SECX02 ----
              SECX02.setComponentPopupMenu(BTD);
              SECX02.setName("SECX02");
              xTitledPanel2ContentContainer.add(SECX02);
              SECX02.setBounds(182, 25, 60, SECX02.getPreferredSize().height);
              
              // ---- SECX03 ----
              SECX03.setComponentPopupMenu(BTD);
              SECX03.setName("SECX03");
              xTitledPanel2ContentContainer.add(SECX03);
              SECX03.setBounds(65, 58, 60, SECX03.getPreferredSize().height);
              
              // ---- SECX04 ----
              SECX04.setComponentPopupMenu(BTD);
              SECX04.setName("SECX04");
              xTitledPanel2ContentContainer.add(SECX04);
              SECX04.setBounds(182, 58, 60, SECX04.getPreferredSize().height);
              
              // ---- SECX05 ----
              SECX05.setComponentPopupMenu(BTD);
              SECX05.setName("SECX05");
              xTitledPanel2ContentContainer.add(SECX05);
              SECX05.setBounds(65, 91, 60, SECX05.getPreferredSize().height);
              
              // ---- SECX06 ----
              SECX06.setComponentPopupMenu(BTD);
              SECX06.setName("SECX06");
              xTitledPanel2ContentContainer.add(SECX06);
              SECX06.setBounds(182, 91, 60, SECX06.getPreferredSize().height);
              
              // ---- SECX07 ----
              SECX07.setComponentPopupMenu(BTD);
              SECX07.setName("SECX07");
              xTitledPanel2ContentContainer.add(SECX07);
              SECX07.setBounds(65, 124, 60, SECX07.getPreferredSize().height);
              
              // ---- SECX08 ----
              SECX08.setComponentPopupMenu(BTD);
              SECX08.setName("SECX08");
              xTitledPanel2ContentContainer.add(SECX08);
              SECX08.setBounds(182, 124, 60, SECX08.getPreferredSize().height);
              
              // ---- SECX09 ----
              SECX09.setComponentPopupMenu(BTD);
              SECX09.setName("SECX09");
              xTitledPanel2ContentContainer.add(SECX09);
              SECX09.setBounds(65, 157, 60, SECX09.getPreferredSize().height);
              
              // ---- SECX10 ----
              SECX10.setComponentPopupMenu(BTD);
              SECX10.setName("SECX10");
              xTitledPanel2ContentContainer.add(SECX10);
              SECX10.setBounds(182, 157, 60, SECX10.getPreferredSize().height);
              
              // ---- SE2X01 ----
              SE2X01.setComponentPopupMenu(BTD);
              SE2X01.setName("SE2X01");
              xTitledPanel2ContentContainer.add(SE2X01);
              SE2X01.setBounds(377, 25, 60, SE2X01.getPreferredSize().height);
              
              // ---- SE2X02 ----
              SE2X02.setComponentPopupMenu(BTD);
              SE2X02.setName("SE2X02");
              xTitledPanel2ContentContainer.add(SE2X02);
              SE2X02.setBounds(494, 25, 60, SE2X02.getPreferredSize().height);
              
              // ---- SE2X03 ----
              SE2X03.setComponentPopupMenu(BTD);
              SE2X03.setName("SE2X03");
              xTitledPanel2ContentContainer.add(SE2X03);
              SE2X03.setBounds(377, 58, 60, SE2X03.getPreferredSize().height);
              
              // ---- SE2X04 ----
              SE2X04.setComponentPopupMenu(BTD);
              SE2X04.setName("SE2X04");
              xTitledPanel2ContentContainer.add(SE2X04);
              SE2X04.setBounds(494, 58, 60, SE2X04.getPreferredSize().height);
              
              // ---- SE2X05 ----
              SE2X05.setComponentPopupMenu(BTD);
              SE2X05.setName("SE2X05");
              xTitledPanel2ContentContainer.add(SE2X05);
              SE2X05.setBounds(377, 91, 60, SE2X05.getPreferredSize().height);
              
              // ---- SE2X06 ----
              SE2X06.setComponentPopupMenu(BTD);
              SE2X06.setName("SE2X06");
              xTitledPanel2ContentContainer.add(SE2X06);
              SE2X06.setBounds(494, 91, 60, SE2X06.getPreferredSize().height);
              
              // ---- SE2X07 ----
              SE2X07.setComponentPopupMenu(BTD);
              SE2X07.setName("SE2X07");
              xTitledPanel2ContentContainer.add(SE2X07);
              SE2X07.setBounds(377, 124, 60, SE2X07.getPreferredSize().height);
              
              // ---- SE2X08 ----
              SE2X08.setComponentPopupMenu(BTD);
              SE2X08.setName("SE2X08");
              xTitledPanel2ContentContainer.add(SE2X08);
              SE2X08.setBounds(494, 124, 60, SE2X08.getPreferredSize().height);
              
              // ---- SE2X09 ----
              SE2X09.setComponentPopupMenu(BTD);
              SE2X09.setName("SE2X09");
              xTitledPanel2ContentContainer.add(SE2X09);
              SE2X09.setBounds(377, 157, 60, SE2X09.getPreferredSize().height);
              
              // ---- SE2X10 ----
              SE2X10.setComponentPopupMenu(BTD);
              SE2X10.setName("SE2X10");
              xTitledPanel2ContentContainer.add(SE2X10);
              SE2X10.setBounds(494, 157, 60, SE2X10.getPreferredSize().height);
              
              // ---- OBJ_98 ----
              OBJ_98.setText("\u00e0");
              OBJ_98.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_98.setName("OBJ_98");
              xTitledPanel2ContentContainer.add(OBJ_98);
              OBJ_98.setBounds(125, 29, 55, 20);
              
              // ---- OBJ_99 ----
              OBJ_99.setText("\u00e0");
              OBJ_99.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_99.setName("OBJ_99");
              xTitledPanel2ContentContainer.add(OBJ_99);
              OBJ_99.setBounds(125, 128, 55, 20);
              
              // ---- OBJ_100 ----
              OBJ_100.setText("\u00e0");
              OBJ_100.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_100.setName("OBJ_100");
              xTitledPanel2ContentContainer.add(OBJ_100);
              OBJ_100.setBounds(125, 62, 55, 20);
              
              // ---- OBJ_101 ----
              OBJ_101.setText("\u00e0");
              OBJ_101.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_101.setName("OBJ_101");
              xTitledPanel2ContentContainer.add(OBJ_101);
              OBJ_101.setBounds(125, 161, 55, 20);
              
              // ---- OBJ_103 ----
              OBJ_103.setText("\u00e0");
              OBJ_103.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_103.setName("OBJ_103");
              xTitledPanel2ContentContainer.add(OBJ_103);
              OBJ_103.setBounds(125, 95, 55, 20);
              
              // ---- OBJ_111 ----
              OBJ_111.setText("\u00e0");
              OBJ_111.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_111.setName("OBJ_111");
              xTitledPanel2ContentContainer.add(OBJ_111);
              OBJ_111.setBounds(440, 29, 55, 20);
              
              // ---- OBJ_112 ----
              OBJ_112.setText("\u00e0");
              OBJ_112.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_112.setName("OBJ_112");
              xTitledPanel2ContentContainer.add(OBJ_112);
              OBJ_112.setBounds(440, 128, 55, 20);
              
              // ---- OBJ_113 ----
              OBJ_113.setText("\u00e0");
              OBJ_113.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_113.setName("OBJ_113");
              xTitledPanel2ContentContainer.add(OBJ_113);
              OBJ_113.setBounds(440, 62, 55, 20);
              
              // ---- OBJ_114 ----
              OBJ_114.setText("\u00e0");
              OBJ_114.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_114.setName("OBJ_114");
              xTitledPanel2ContentContainer.add(OBJ_114);
              OBJ_114.setBounds(440, 161, 55, 20);
              
              // ---- OBJ_115 ----
              OBJ_115.setText("\u00e0");
              OBJ_115.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_115.setName("OBJ_115");
              xTitledPanel2ContentContainer.add(OBJ_115);
              OBJ_115.setBounds(440, 95, 55, 20);
              
              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = xTitledPanel2ContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
                xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
              }
            }
            pnlInterdits.add(xTitledPanel2);
            xTitledPanel2.setBounds(5, 160, 854, 253);
            
            // ---- OBJ_68 ----
            OBJ_68.setText("Aller \u00e0 la page");
            OBJ_68.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_68.setName("OBJ_68");
            OBJ_68.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_68ActionPerformed(e);
              }
            });
            pnlInterdits.add(OBJ_68);
            OBJ_68.setBounds(new Rectangle(new Point(685, 420), OBJ_68.getPreferredSize()));
            
            // ---- V06F ----
            V06F.setComponentPopupMenu(BTD);
            V06F.setName("V06F");
            pnlInterdits.add(V06F);
            V06F.setBounds(834, 420, 25, V06F.getPreferredSize().height);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < pnlInterdits.getComponentCount(); i++) {
                Rectangle bounds = pnlInterdits.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = pnlInterdits.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              pnlInterdits.setMinimumSize(preferredSize);
              pnlInterdits.setPreferredSize(preferredSize);
            }
          }
          pnlContenu.add(pnlInterdits);
          pnlInterdits.setBounds(13, 14, 864, 462);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < pnlContenu.getComponentCount(); i++) {
              Rectangle bounds = pnlContenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = pnlContenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            pnlContenu.setMinimumSize(preferredSize);
            pnlContenu.setPreferredSize(preferredSize);
          }
        }
        pnlPrincipal.add(pnlContenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlSud.add(pnlPrincipal, BorderLayout.CENTER);
    }
    add(pnlSud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlNord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private RiZoneSortie INDUSR;
  private JLabel OBJ_64;
  private RiZoneSortie INDETB;
  private JPanel p_tete_droite;
  private JLabel OBJ_156;
  private JLabel OBJ_155;
  private SNPanelFond pnlSud;
  private JPanel pnlMenus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelContenu pnlPrincipal;
  private SNPanelContenu pnlContenu;
  private JPanel pnlInterdits;
  private JXTitledPanel xTitledPanel1;
  private XRiTextField SEJO11;
  private XRiTextField SEJO12;
  private XRiTextField SEJO13;
  private XRiTextField SEJO14;
  private XRiTextField SEJO15;
  private XRiTextField SEJO16;
  private XRiTextField SEJO17;
  private XRiTextField SEJO18;
  private XRiTextField SEJO19;
  private XRiTextField SEJO20;
  private XRiTextField SEJO01;
  private XRiTextField SEJO02;
  private XRiTextField SEJO03;
  private XRiTextField SEJO04;
  private XRiTextField SEJO05;
  private XRiTextField SEJO06;
  private XRiTextField SEJO07;
  private XRiTextField SEJO08;
  private XRiTextField SEJO09;
  private XRiTextField SEJO10;
  private JXTitledPanel xTitledPanel2;
  private XRiTextField SECX01;
  private XRiTextField SECX02;
  private XRiTextField SECX03;
  private XRiTextField SECX04;
  private XRiTextField SECX05;
  private XRiTextField SECX06;
  private XRiTextField SECX07;
  private XRiTextField SECX08;
  private XRiTextField SECX09;
  private XRiTextField SECX10;
  private XRiTextField SE2X01;
  private XRiTextField SE2X02;
  private XRiTextField SE2X03;
  private XRiTextField SE2X04;
  private XRiTextField SE2X05;
  private XRiTextField SE2X06;
  private XRiTextField SE2X07;
  private XRiTextField SE2X08;
  private XRiTextField SE2X09;
  private XRiTextField SE2X10;
  private JLabel OBJ_98;
  private JLabel OBJ_99;
  private JLabel OBJ_100;
  private JLabel OBJ_101;
  private JLabel OBJ_103;
  private JLabel OBJ_111;
  private JLabel OBJ_112;
  private JLabel OBJ_113;
  private JLabel OBJ_114;
  private JLabel OBJ_115;
  private SNBoutonLeger OBJ_68;
  private XRiTextField V06F;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
