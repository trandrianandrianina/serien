
package ri.serien.libecranrpg.vcgm.VCGM67FM;
// Nom Fichier: pop_VCGM67FM_FMTA3_1076.java

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;

/**
 * @author Stéphane Vénéri
 */
public class VCGM67FM_A3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM67FM_A3(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    CHX2.setValeursSelection("1", " ");
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    // CHX2.setVisible( lexique.isPresent("CHX2"));
    // CHX2.setSelected(lexique.HostFieldGetData("CHX2").equalsIgnoreCase("1"));
    
    // TODO Icones
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Validation des fichiers selectionnés"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // if (CHX2.isSelected())
    // lexique.HostFieldPutData("CHX2", 0, "1");
    // else
    // lexique.HostFieldPutData("CHX2", 0, " ");
  }
  
  private void VALActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="ENTER"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    CHX2 = new XRiCheckBox();
    BT_ENTER = new JButton();

    //======== this ========
    setName("this");

    //---- CHX2 ----
    CHX2.setText("Validation de l'injection des fichiers");
    CHX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    CHX2.setName("CHX2");

    //---- BT_ENTER ----
    BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_ENTER.setToolTipText("Ok");
    BT_ENTER.setName("BT_ENTER");
    BT_ENTER.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        VALActionPerformed(e);
      }
    });

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(35, 35, 35)
          .addComponent(CHX2, GroupLayout.PREFERRED_SIZE, 265, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(245, 245, 245)
          .addComponent(BT_ENTER, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(25, 25, 25)
          .addComponent(CHX2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
          .addGap(30, 30, 30)
          .addComponent(BT_ENTER, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
    );
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private XRiCheckBox CHX2;
  private JButton BT_ENTER;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
