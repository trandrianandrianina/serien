
package ri.serien.libecranrpg.vcgm.VCGM9TFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM9TFM_A1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM9TFM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_34_OBJ_34.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@*DATE@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    DEMSA2.setEnabled(lexique.isPresent("DEMSA2"));
    DEMSA1.setEnabled(lexique.isPresent("DEMSA1"));
    DEMDAF.setEnabled(lexique.isPresent("DEMDAF"));
    DEMDAD.setEnabled(lexique.isPresent("DEMDAD"));
    DEMCPF.setEnabled(lexique.isPresent("DEMCPF"));
    DEMCPD.setEnabled(lexique.isPresent("DEMCPD"));
    OBJ_34_OBJ_34.setVisible(lexique.isPresent("*DATE"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
    riMenu_bt4.setIcon(lexique.chargerImage("images/fonctions.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt13ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt16ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt17ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt18ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt19ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt20ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt21ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riSousMenu13 = new RiSousMenu();
    riSousMenu_bt13 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    riSousMenu16 = new RiSousMenu();
    riSousMenu_bt16 = new RiSousMenu_bt();
    riSousMenu17 = new RiSousMenu();
    riSousMenu_bt17 = new RiSousMenu_bt();
    riMenu4 = new RiMenu();
    riMenu_bt4 = new RiMenu_bt();
    riSousMenu18 = new RiSousMenu();
    riSousMenu_bt18 = new RiSousMenu_bt();
    riSousMenu19 = new RiSousMenu();
    riSousMenu_bt19 = new RiSousMenu_bt();
    riSousMenu20 = new RiSousMenu();
    riSousMenu_bt20 = new RiSousMenu_bt();
    riSousMenu21 = new RiSousMenu();
    riSousMenu_bt21 = new RiSousMenu_bt();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_20_OBJ_20 = new JLabel();
    DEMETB = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_40_OBJ_40 = new JLabel();
    OBJ_38_OBJ_38 = new JLabel();
    DEMDAD = new XRiTextField();
    OBJ_39_OBJ_39 = new JLabel();
    DEMDAF = new XRiTextField();
    OBJ_35_OBJ_35 = new JLabel();
    DEMSA1 = new XRiTextField();
    OBJ_37_OBJ_37 = new JLabel();
    DEMSA2 = new XRiTextField();
    OBJ_61_OBJ_61 = new JLabel();
    DEMCPD = new XRiTextField();
    OBJ_62_OBJ_62 = new JLabel();
    DEMCPF = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    OBJ_34_OBJ_34 = new JLabel();

    //======== scroll_droite ========
    {
      scroll_droite.setBackground(new Color(238, 239, 241));
      scroll_droite.setPreferredSize(new Dimension(16, 520));
      scroll_droite.setBorder(null);
      scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
      scroll_droite.setName("scroll_droite");

      //======== menus_haut ========
      {
        menus_haut.setMinimumSize(new Dimension(160, 520));
        menus_haut.setPreferredSize(new Dimension(160, 520));
        menus_haut.setBackground(new Color(238, 239, 241));
        menus_haut.setAutoscrolls(true);
        menus_haut.setName("menus_haut");
        menus_haut.setLayout(new VerticalLayout());

        //======== riMenu_V01F ========
        {
          riMenu_V01F.setMinimumSize(new Dimension(104, 50));
          riMenu_V01F.setPreferredSize(new Dimension(170, 50));
          riMenu_V01F.setMaximumSize(new Dimension(104, 50));
          riMenu_V01F.setName("riMenu_V01F");

          //---- riMenu_bt_V01F ----
          riMenu_bt_V01F.setText("@V01F@");
          riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
          riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
          riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
          riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
          riMenu_bt_V01F.setName("riMenu_bt_V01F");
          riMenu_V01F.add(riMenu_bt_V01F);
        }
        menus_haut.add(riMenu_V01F);

        //======== riSousMenu_consult ========
        {
          riSousMenu_consult.setName("riSousMenu_consult");

          //---- riSousMenu_bt_consult ----
          riSousMenu_bt_consult.setText("Consultation");
          riSousMenu_bt_consult.setToolTipText("Consultation");
          riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
          riSousMenu_consult.add(riSousMenu_bt_consult);
        }
        menus_haut.add(riSousMenu_consult);

        //======== riSousMenu_modif ========
        {
          riSousMenu_modif.setName("riSousMenu_modif");

          //---- riSousMenu_bt_modif ----
          riSousMenu_bt_modif.setText("Modification");
          riSousMenu_bt_modif.setToolTipText("Modification");
          riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
          riSousMenu_modif.add(riSousMenu_bt_modif);
        }
        menus_haut.add(riSousMenu_modif);

        //======== riSousMenu_crea ========
        {
          riSousMenu_crea.setName("riSousMenu_crea");

          //---- riSousMenu_bt_crea ----
          riSousMenu_bt_crea.setText("Cr\u00e9ation");
          riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
          riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
          riSousMenu_crea.add(riSousMenu_bt_crea);
        }
        menus_haut.add(riSousMenu_crea);

        //======== riSousMenu_suppr ========
        {
          riSousMenu_suppr.setName("riSousMenu_suppr");

          //---- riSousMenu_bt_suppr ----
          riSousMenu_bt_suppr.setText("Annulation");
          riSousMenu_bt_suppr.setToolTipText("Annulation");
          riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
          riSousMenu_suppr.add(riSousMenu_bt_suppr);
        }
        menus_haut.add(riSousMenu_suppr);

        //======== riSousMenuF_dupli ========
        {
          riSousMenuF_dupli.setName("riSousMenuF_dupli");

          //---- riSousMenu_bt_dupli ----
          riSousMenu_bt_dupli.setText("Duplication");
          riSousMenu_bt_dupli.setToolTipText("Duplication");
          riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
          riSousMenuF_dupli.add(riSousMenu_bt_dupli);
        }
        menus_haut.add(riSousMenuF_dupli);

        //======== riSousMenu_rappel ========
        {
          riSousMenu_rappel.setName("riSousMenu_rappel");

          //---- riSousMenu_bt_rappel ----
          riSousMenu_bt_rappel.setText("Rappel");
          riSousMenu_bt_rappel.setToolTipText("Rappel");
          riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
          riSousMenu_rappel.add(riSousMenu_bt_rappel);
        }
        menus_haut.add(riSousMenu_rappel);

        //======== riSousMenu_reac ========
        {
          riSousMenu_reac.setName("riSousMenu_reac");

          //---- riSousMenu_bt_reac ----
          riSousMenu_bt_reac.setText("R\u00e9activation");
          riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
          riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
          riSousMenu_reac.add(riSousMenu_bt_reac);
        }
        menus_haut.add(riSousMenu_reac);

        //======== riSousMenu_destr ========
        {
          riSousMenu_destr.setName("riSousMenu_destr");

          //---- riSousMenu_bt_destr ----
          riSousMenu_bt_destr.setText("Suppression");
          riSousMenu_bt_destr.setToolTipText("Suppression");
          riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
          riSousMenu_destr.add(riSousMenu_bt_destr);
        }
        menus_haut.add(riSousMenu_destr);

        //======== riMenu2 ========
        {
          riMenu2.setName("riMenu2");

          //---- riMenu_bt2 ----
          riMenu_bt2.setText("Options");
          riMenu_bt2.setName("riMenu_bt2");
          riMenu2.add(riMenu_bt2);
        }
        menus_haut.add(riMenu2);

        //======== riSousMenu6 ========
        {
          riSousMenu6.setName("riSousMenu6");

          //---- riSousMenu_bt6 ----
          riSousMenu_bt6.setText("text");
          riSousMenu_bt6.setName("riSousMenu_bt6");
          riSousMenu_bt6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt6ActionPerformed(e);
            }
          });
          riSousMenu6.add(riSousMenu_bt6);
        }
        menus_haut.add(riSousMenu6);

        //======== riSousMenu7 ========
        {
          riSousMenu7.setName("riSousMenu7");

          //---- riSousMenu_bt7 ----
          riSousMenu_bt7.setText("text");
          riSousMenu_bt7.setName("riSousMenu_bt7");
          riSousMenu_bt7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt7ActionPerformed(e);
            }
          });
          riSousMenu7.add(riSousMenu_bt7);
        }
        menus_haut.add(riSousMenu7);

        //======== riSousMenu8 ========
        {
          riSousMenu8.setName("riSousMenu8");

          //---- riSousMenu_bt8 ----
          riSousMenu_bt8.setText("text");
          riSousMenu_bt8.setName("riSousMenu_bt8");
          riSousMenu_bt8.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt8ActionPerformed(e);
            }
          });
          riSousMenu8.add(riSousMenu_bt8);
        }
        menus_haut.add(riSousMenu8);

        //======== riSousMenu9 ========
        {
          riSousMenu9.setName("riSousMenu9");

          //---- riSousMenu_bt9 ----
          riSousMenu_bt9.setText("text");
          riSousMenu_bt9.setName("riSousMenu_bt9");
          riSousMenu_bt9.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt9ActionPerformed(e);
            }
          });
          riSousMenu9.add(riSousMenu_bt9);
        }
        menus_haut.add(riSousMenu9);

        //======== riSousMenu10 ========
        {
          riSousMenu10.setName("riSousMenu10");

          //---- riSousMenu_bt10 ----
          riSousMenu_bt10.setText("text");
          riSousMenu_bt10.setName("riSousMenu_bt10");
          riSousMenu_bt10.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt10ActionPerformed(e);
            }
          });
          riSousMenu10.add(riSousMenu_bt10);
        }
        menus_haut.add(riSousMenu10);

        //======== riSousMenu11 ========
        {
          riSousMenu11.setName("riSousMenu11");

          //---- riSousMenu_bt11 ----
          riSousMenu_bt11.setText("text");
          riSousMenu_bt11.setName("riSousMenu_bt11");
          riSousMenu_bt11.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt11ActionPerformed(e);
            }
          });
          riSousMenu11.add(riSousMenu_bt11);
        }
        menus_haut.add(riSousMenu11);

        //======== riSousMenu12 ========
        {
          riSousMenu12.setName("riSousMenu12");

          //---- riSousMenu_bt12 ----
          riSousMenu_bt12.setText("text");
          riSousMenu_bt12.setName("riSousMenu_bt12");
          riSousMenu_bt12.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt12ActionPerformed(e);
            }
          });
          riSousMenu12.add(riSousMenu_bt12);
        }
        menus_haut.add(riSousMenu12);

        //======== riSousMenu13 ========
        {
          riSousMenu13.setName("riSousMenu13");

          //---- riSousMenu_bt13 ----
          riSousMenu_bt13.setText("text");
          riSousMenu_bt13.setName("riSousMenu_bt13");
          riSousMenu_bt13.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt13ActionPerformed(e);
            }
          });
          riSousMenu13.add(riSousMenu_bt13);
        }
        menus_haut.add(riSousMenu13);

        //======== riMenu3 ========
        {
          riMenu3.setName("riMenu3");

          //---- riMenu_bt3 ----
          riMenu_bt3.setText("Outils");
          riMenu_bt3.setName("riMenu_bt3");
          riMenu3.add(riMenu_bt3);
        }
        menus_haut.add(riMenu3);

        //======== riSousMenu14 ========
        {
          riSousMenu14.setName("riSousMenu14");

          //---- riSousMenu_bt14 ----
          riSousMenu_bt14.setText("text");
          riSousMenu_bt14.setName("riSousMenu_bt14");
          riSousMenu_bt14.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt14ActionPerformed(e);
            }
          });
          riSousMenu14.add(riSousMenu_bt14);
        }
        menus_haut.add(riSousMenu14);

        //======== riSousMenu15 ========
        {
          riSousMenu15.setName("riSousMenu15");

          //---- riSousMenu_bt15 ----
          riSousMenu_bt15.setText("text");
          riSousMenu_bt15.setName("riSousMenu_bt15");
          riSousMenu_bt15.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt15ActionPerformed(e);
            }
          });
          riSousMenu15.add(riSousMenu_bt15);
        }
        menus_haut.add(riSousMenu15);

        //======== riSousMenu16 ========
        {
          riSousMenu16.setName("riSousMenu16");

          //---- riSousMenu_bt16 ----
          riSousMenu_bt16.setText("text");
          riSousMenu_bt16.setName("riSousMenu_bt16");
          riSousMenu_bt16.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt16ActionPerformed(e);
            }
          });
          riSousMenu16.add(riSousMenu_bt16);
        }
        menus_haut.add(riSousMenu16);

        //======== riSousMenu17 ========
        {
          riSousMenu17.setName("riSousMenu17");

          //---- riSousMenu_bt17 ----
          riSousMenu_bt17.setText("text");
          riSousMenu_bt17.setName("riSousMenu_bt17");
          riSousMenu_bt17.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt17ActionPerformed(e);
            }
          });
          riSousMenu17.add(riSousMenu_bt17);
        }
        menus_haut.add(riSousMenu17);

        //======== riMenu4 ========
        {
          riMenu4.setName("riMenu4");

          //---- riMenu_bt4 ----
          riMenu_bt4.setText("Fonctions");
          riMenu_bt4.setName("riMenu_bt4");
          riMenu4.add(riMenu_bt4);
        }
        menus_haut.add(riMenu4);

        //======== riSousMenu18 ========
        {
          riSousMenu18.setName("riSousMenu18");

          //---- riSousMenu_bt18 ----
          riSousMenu_bt18.setText("text");
          riSousMenu_bt18.setName("riSousMenu_bt18");
          riSousMenu_bt18.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt18ActionPerformed(e);
            }
          });
          riSousMenu18.add(riSousMenu_bt18);
        }
        menus_haut.add(riSousMenu18);

        //======== riSousMenu19 ========
        {
          riSousMenu19.setName("riSousMenu19");

          //---- riSousMenu_bt19 ----
          riSousMenu_bt19.setText("text");
          riSousMenu_bt19.setName("riSousMenu_bt19");
          riSousMenu_bt19.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt19ActionPerformed(e);
            }
          });
          riSousMenu19.add(riSousMenu_bt19);
        }
        menus_haut.add(riSousMenu19);

        //======== riSousMenu20 ========
        {
          riSousMenu20.setName("riSousMenu20");

          //---- riSousMenu_bt20 ----
          riSousMenu_bt20.setText("text");
          riSousMenu_bt20.setName("riSousMenu_bt20");
          riSousMenu_bt20.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt20ActionPerformed(e);
            }
          });
          riSousMenu20.add(riSousMenu_bt20);
        }
        menus_haut.add(riSousMenu20);

        //======== riSousMenu21 ========
        {
          riSousMenu21.setName("riSousMenu21");

          //---- riSousMenu_bt21 ----
          riSousMenu_bt21.setText("text");
          riSousMenu_bt21.setName("riSousMenu_bt21");
          riSousMenu_bt21.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt21ActionPerformed(e);
            }
          });
          riSousMenu21.add(riSousMenu_bt21);
        }
        menus_haut.add(riSousMenu21);
      }
      scroll_droite.setViewportView(menus_haut);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Affichage historique des comptes analytiques");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_20_OBJ_20 ----
          OBJ_20_OBJ_20.setText("Soci\u00e9t\u00e9");
          OBJ_20_OBJ_20.setName("OBJ_20_OBJ_20");

          //---- DEMETB ----
          DEMETB.setComponentPopupMenu(BTD);
          DEMETB.setOpaque(false);
          DEMETB.setName("DEMETB");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(OBJ_20_OBJ_20, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(DEMETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_20_OBJ_20, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(DEMETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(400, 320));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Option d'analyse"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_40_OBJ_40 ----
            OBJ_40_OBJ_40.setText("P\u00e9riode d'analyse");
            OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");
            panel1.add(OBJ_40_OBJ_40);
            OBJ_40_OBJ_40.setBounds(30, 64, 124, 20);

            //---- OBJ_38_OBJ_38 ----
            OBJ_38_OBJ_38.setText("D\u00e9but");
            OBJ_38_OBJ_38.setName("OBJ_38_OBJ_38");
            panel1.add(OBJ_38_OBJ_38);
            OBJ_38_OBJ_38.setBounds(155, 40, 58, 20);

            //---- DEMDAD ----
            DEMDAD.setComponentPopupMenu(BTD);
            DEMDAD.setName("DEMDAD");
            panel1.add(DEMDAD);
            DEMDAD.setBounds(150, 60, 55, DEMDAD.getPreferredSize().height);

            //---- OBJ_39_OBJ_39 ----
            OBJ_39_OBJ_39.setText("Fin");
            OBJ_39_OBJ_39.setName("OBJ_39_OBJ_39");
            panel1.add(OBJ_39_OBJ_39);
            OBJ_39_OBJ_39.setBounds(225, 40, 30, 20);

            //---- DEMDAF ----
            DEMDAF.setComponentPopupMenu(BTD);
            DEMDAF.setName("DEMDAF");
            panel1.add(DEMDAF);
            DEMDAF.setBounds(215, 60, 55, DEMDAF.getPreferredSize().height);

            //---- OBJ_35_OBJ_35 ----
            OBJ_35_OBJ_35.setText("Section analytique d\u00e9but");
            OBJ_35_OBJ_35.setName("OBJ_35_OBJ_35");
            panel1.add(OBJ_35_OBJ_35);
            OBJ_35_OBJ_35.setBounds(30, 99, 150, 20);

            //---- DEMSA1 ----
            DEMSA1.setComponentPopupMenu(BTD);
            DEMSA1.setName("DEMSA1");
            panel1.add(DEMSA1);
            DEMSA1.setBounds(215, 95, 44, DEMSA1.getPreferredSize().height);

            //---- OBJ_37_OBJ_37 ----
            OBJ_37_OBJ_37.setText("Section analytique fin");
            OBJ_37_OBJ_37.setName("OBJ_37_OBJ_37");
            panel1.add(OBJ_37_OBJ_37);
            OBJ_37_OBJ_37.setBounds(30, 134, 145, 20);

            //---- DEMSA2 ----
            DEMSA2.setComponentPopupMenu(BTD);
            DEMSA2.setName("DEMSA2");
            panel1.add(DEMSA2);
            DEMSA2.setBounds(215, 130, 44, DEMSA2.getPreferredSize().height);

            //---- OBJ_61_OBJ_61 ----
            OBJ_61_OBJ_61.setText("Compte d\u00e9but");
            OBJ_61_OBJ_61.setName("OBJ_61_OBJ_61");
            panel1.add(OBJ_61_OBJ_61);
            OBJ_61_OBJ_61.setBounds(30, 169, 108, 20);

            //---- DEMCPD ----
            DEMCPD.setComponentPopupMenu(BTD);
            DEMCPD.setName("DEMCPD");
            panel1.add(DEMCPD);
            DEMCPD.setBounds(215, 165, 60, DEMCPD.getPreferredSize().height);

            //---- OBJ_62_OBJ_62 ----
            OBJ_62_OBJ_62.setText("Compte fin");
            OBJ_62_OBJ_62.setName("OBJ_62_OBJ_62");
            panel1.add(OBJ_62_OBJ_62);
            OBJ_62_OBJ_62.setBounds(30, 204, 90, 20);

            //---- DEMCPF ----
            DEMCPF.setComponentPopupMenu(BTD);
            DEMCPF.setName("DEMCPF");
            panel1.add(DEMCPF);
            DEMCPF.setBounds(215, 200, 60, DEMCPF.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 329, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(39, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 258, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(33, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Choix possibles");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }

    //---- OBJ_34_OBJ_34 ----
    OBJ_34_OBJ_34.setText("@*DATE@");
    OBJ_34_OBJ_34.setName("OBJ_34_OBJ_34");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiSousMenu riSousMenu13;
  private RiSousMenu_bt riSousMenu_bt13;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private RiSousMenu riSousMenu16;
  private RiSousMenu_bt riSousMenu_bt16;
  private RiSousMenu riSousMenu17;
  private RiSousMenu_bt riSousMenu_bt17;
  private RiMenu riMenu4;
  private RiMenu_bt riMenu_bt4;
  private RiSousMenu riSousMenu18;
  private RiSousMenu_bt riSousMenu_bt18;
  private RiSousMenu riSousMenu19;
  private RiSousMenu_bt riSousMenu_bt19;
  private RiSousMenu riSousMenu20;
  private RiSousMenu_bt riSousMenu_bt20;
  private RiSousMenu riSousMenu21;
  private RiSousMenu_bt riSousMenu_bt21;
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_20_OBJ_20;
  private XRiTextField DEMETB;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_40_OBJ_40;
  private JLabel OBJ_38_OBJ_38;
  private XRiTextField DEMDAD;
  private JLabel OBJ_39_OBJ_39;
  private XRiTextField DEMDAF;
  private JLabel OBJ_35_OBJ_35;
  private XRiTextField DEMSA1;
  private JLabel OBJ_37_OBJ_37;
  private XRiTextField DEMSA2;
  private JLabel OBJ_61_OBJ_61;
  private XRiTextField DEMCPD;
  private JLabel OBJ_62_OBJ_62;
  private XRiTextField DEMCPF;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_10;
  private JLabel OBJ_34_OBJ_34;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
