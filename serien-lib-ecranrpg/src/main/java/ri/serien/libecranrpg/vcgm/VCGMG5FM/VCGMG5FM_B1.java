
package ri.serien.libecranrpg.vcgm.VCGMG5FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.JButton;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGMG5FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  private final static String BOUTON_SASIE_LIBELLEE = "Saisie des libellés";
  
  public VCGMG5FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Bouton par défaut
    
    // Ajout
    initDiverses();
    
    sNBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    sNBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    sNBarreBouton.ajouterBouton(BOUTON_SASIE_LIBELLEE, 's', true);
    sNBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _WTX01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    if (lexique.HostFieldGetData("SNE01").trim().equals("+")) {
      SNE01.setIcon(lexique.chargerImage("images/plus_petit.png", true));
      LIE01.setForeground(Color.BLACK);
      MTE01.setForeground(Color.BLACK);
    }
    else {
      SNE01.setIcon(lexique.chargerImage("images/moins_petit.png", true));
      LIE01.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
      MTE01.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
    }
    if (lexique.HostFieldGetData("SNE02").trim().equals("+")) {
      SNE02.setIcon(lexique.chargerImage("images/plus_petit.png", true));
      LIE02.setForeground(Color.BLACK);
      MTE02.setForeground(Color.BLACK);
    }
    else {
      SNE02.setIcon(lexique.chargerImage("images/moins_petit.png", true));
      LIE02.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
      MTE02.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
    }
    if (lexique.HostFieldGetData("SNE03").trim().equals("+")) {
      SNE03.setIcon(lexique.chargerImage("images/plus_petit.png", true));
      LIE03.setForeground(Color.BLACK);
      MTE03.setForeground(Color.BLACK);
    }
    else {
      SNE03.setIcon(lexique.chargerImage("images/moins_petit.png", true));
      LIE03.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
      MTE03.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
    }
    if (lexique.HostFieldGetData("SNE04").trim().equals("+")) {
      SNE04.setIcon(lexique.chargerImage("images/plus_petit.png", true));
      LIE04.setForeground(Color.BLACK);
      MTE04.setForeground(Color.BLACK);
    }
    else {
      SNE04.setIcon(lexique.chargerImage("images/moins_petit.png", true));
      LIE04.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
      MTE04.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
    }
    if (lexique.HostFieldGetData("SNE05").trim().equals("+")) {
      SNE05.setIcon(lexique.chargerImage("images/plus_petit.png", true));
      LIE05.setForeground(Color.BLACK);
      MTE05.setForeground(Color.BLACK);
    }
    else {
      SNE05.setIcon(lexique.chargerImage("images/moins_petit.png", true));
      LIE05.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
      MTE05.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
    }
    if (lexique.HostFieldGetData("SNE06").trim().equals("+")) {
      SNE06.setIcon(lexique.chargerImage("images/plus_petit.png", true));
      LIE06.setForeground(Color.BLACK);
      MTE06.setForeground(Color.BLACK);
    }
    else {
      SNE06.setIcon(lexique.chargerImage("images/moins_petit.png", true));
      LIE06.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
      MTE06.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
    }
    if (lexique.HostFieldGetData("SNE07").trim().equals("+")) {
      SNE07.setIcon(lexique.chargerImage("images/plus_petit.png", true));
      LIE07.setForeground(Color.BLACK);
      MTE07.setForeground(Color.BLACK);
    }
    else {
      SNE07.setIcon(lexique.chargerImage("images/moins_petit.png", true));
      LIE07.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
      MTE07.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
    }
    if (lexique.HostFieldGetData("SNE08").trim().equals("+")) {
      SNE08.setIcon(lexique.chargerImage("images/plus_petit.png", true));
      LIE08.setForeground(Color.BLACK);
      MTE08.setForeground(Color.BLACK);
    }
    else {
      SNE08.setIcon(lexique.chargerImage("images/moins_petit.png", true));
      LIE08.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
      MTE08.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
    }
    if (lexique.HostFieldGetData("SNE09").trim().equals("+")) {
      SNE09.setIcon(lexique.chargerImage("images/plus_petit.png", true));
      LIE09.setForeground(Color.BLACK);
      MTE09.setForeground(Color.BLACK);
    }
    else {
      SNE09.setIcon(lexique.chargerImage("images/moins_petit.png", true));
      LIE09.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
      MTE09.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
    }
    if (lexique.HostFieldGetData("SNE10").trim().equals("+")) {
      SNE10.setIcon(lexique.chargerImage("images/plus_petit.png", true));
      LIE10.setForeground(Color.BLACK);
      MTE10.setForeground(Color.BLACK);
    }
    else {
      SNE10.setIcon(lexique.chargerImage("images/moins_petit.png", true));
      LIE10.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
      MTE10.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
    }
    if (lexique.HostFieldGetData("SNE11").trim().equals("+")) {
      SNE11.setIcon(lexique.chargerImage("images/plus_petit.png", true));
      LIE11.setForeground(Color.BLACK);
      MTE11.setForeground(Color.BLACK);
    }
    else {
      SNE11.setIcon(lexique.chargerImage("images/moins_petit.png", true));
      LIE11.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
      MTE11.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
    }
    if (lexique.HostFieldGetData("SNE12").trim().equals("+")) {
      SNE12.setIcon(lexique.chargerImage("images/plus_petit.png", true));
      LIE12.setForeground(Color.BLACK);
      MTE12.setForeground(Color.BLACK);
    }
    else {
      SNE12.setIcon(lexique.chargerImage("images/moins_petit.png", true));
      LIE12.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
      MTE12.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
    }
    if (lexique.HostFieldGetData("SNE13").trim().equals("+")) {
      SNE13.setIcon(lexique.chargerImage("images/plus_petit.png", true));
      LIE13.setForeground(Color.BLACK);
      MTE13.setForeground(Color.BLACK);
    }
    else {
      SNE13.setIcon(lexique.chargerImage("images/moins_petit.png", true));
      LIE13.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
      MTE13.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
    }
    if (lexique.HostFieldGetData("SNE14").trim().equals("+")) {
      SNE14.setIcon(lexique.chargerImage("images/plus_petit.png", true));
      LIE14.setForeground(Color.BLACK);
      MTE14.setForeground(Color.BLACK);
    }
    else {
      SNE14.setIcon(lexique.chargerImage("images/moins_petit.png", true));
      LIE14.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
      MTE14.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
    }
    if (lexique.HostFieldGetData("SNE15").trim().equals("+")) {
      SNE15.setIcon(lexique.chargerImage("images/plus_petit.png", true));
      LIE15.setForeground(Color.BLACK);
      MTE15.setForeground(Color.BLACK);
    }
    else {
      SNE15.setIcon(lexique.chargerImage("images/moins_petit.png", true));
      LIE15.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
      MTE15.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
    }
    if (lexique.HostFieldGetData("SNE16").trim().equals("+")) {
      SNE16.setIcon(lexique.chargerImage("images/plus_petit.png", true));
      LIE16.setForeground(Color.BLACK);
      MTE16.setForeground(Color.BLACK);
    }
    else {
      SNE16.setIcon(lexique.chargerImage("images/moins_petit.png", true));
      LIE16.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
      MTE16.setForeground(Constantes.COULEUR_LISTE_LETTRAGE);
    }
    
    ERE01.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    ERE02.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    ERE03.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    ERE04.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    ERE05.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    ERE06.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    ERE07.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    ERE08.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    ERE09.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    ERE10.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    ERE11.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    ERE12.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    ERE13.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    ERE14.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    ERE15.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    ERE16.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    
    ERE01.setVisible(!lexique.HostFieldGetData("ERE01").trim().equals(""));
    ERE02.setVisible(!lexique.HostFieldGetData("ERE02").trim().equals(""));
    ERE03.setVisible(!lexique.HostFieldGetData("ERE03").trim().equals(""));
    ERE04.setVisible(!lexique.HostFieldGetData("ERE04").trim().equals(""));
    ERE05.setVisible(!lexique.HostFieldGetData("ERE05").trim().equals(""));
    ERE06.setVisible(!lexique.HostFieldGetData("ERE06").trim().equals(""));
    ERE07.setVisible(!lexique.HostFieldGetData("ERE07").trim().equals(""));
    ERE08.setVisible(!lexique.HostFieldGetData("ERE08").trim().equals(""));
    ERE09.setVisible(!lexique.HostFieldGetData("ERE09").trim().equals(""));
    ERE10.setVisible(!lexique.HostFieldGetData("ERE10").trim().equals(""));
    ERE11.setVisible(!lexique.HostFieldGetData("ERE11").trim().equals(""));
    ERE12.setVisible(!lexique.HostFieldGetData("ERE12").trim().equals(""));
    ERE13.setVisible(!lexique.HostFieldGetData("ERE13").trim().equals(""));
    ERE14.setVisible(!lexique.HostFieldGetData("ERE14").trim().equals(""));
    ERE15.setVisible(!lexique.HostFieldGetData("ERE15").trim().equals(""));
    ERE16.setVisible(!lexique.HostFieldGetData("ERE16").trim().equals(""));
    
    LIE01.setVisible(!lexique.HostFieldGetData("LIE01").trim().equals(""));
    LIE02.setVisible(!lexique.HostFieldGetData("LIE02").trim().equals(""));
    LIE03.setVisible(!lexique.HostFieldGetData("LIE03").trim().equals(""));
    LIE04.setVisible(!lexique.HostFieldGetData("LIE04").trim().equals(""));
    LIE05.setVisible(!lexique.HostFieldGetData("LIE05").trim().equals(""));
    LIE06.setVisible(!lexique.HostFieldGetData("LIE06").trim().equals(""));
    LIE07.setVisible(!lexique.HostFieldGetData("LIE07").trim().equals(""));
    LIE08.setVisible(!lexique.HostFieldGetData("LIE08").trim().equals(""));
    LIE09.setVisible(!lexique.HostFieldGetData("LIE09").trim().equals(""));
    LIE10.setVisible(!lexique.HostFieldGetData("LIE10").trim().equals(""));
    LIE11.setVisible(!lexique.HostFieldGetData("LIE11").trim().equals(""));
    LIE12.setVisible(!lexique.HostFieldGetData("LIE12").trim().equals(""));
    LIE13.setVisible(!lexique.HostFieldGetData("LIE13").trim().equals(""));
    LIE14.setVisible(!lexique.HostFieldGetData("LIE14").trim().equals(""));
    LIE15.setVisible(!lexique.HostFieldGetData("LIE15").trim().equals(""));
    LIE16.setVisible(!lexique.HostFieldGetData("LIE16").trim().equals(""));
    
    SNE01.setVisible(!lexique.HostFieldGetData("LIE01").trim().equals(""));
    SNE02.setVisible(!lexique.HostFieldGetData("LIE02").trim().equals(""));
    SNE03.setVisible(!lexique.HostFieldGetData("LIE03").trim().equals(""));
    SNE04.setVisible(!lexique.HostFieldGetData("LIE04").trim().equals(""));
    SNE05.setVisible(!lexique.HostFieldGetData("LIE05").trim().equals(""));
    SNE06.setVisible(!lexique.HostFieldGetData("LIE06").trim().equals(""));
    SNE07.setVisible(!lexique.HostFieldGetData("LIE07").trim().equals(""));
    SNE08.setVisible(!lexique.HostFieldGetData("LIE08").trim().equals(""));
    SNE09.setVisible(!lexique.HostFieldGetData("LIE09").trim().equals(""));
    SNE10.setVisible(!lexique.HostFieldGetData("LIE10").trim().equals(""));
    SNE11.setVisible(!lexique.HostFieldGetData("LIE11").trim().equals(""));
    SNE12.setVisible(!lexique.HostFieldGetData("LIE12").trim().equals(""));
    SNE13.setVisible(!lexique.HostFieldGetData("LIE13").trim().equals(""));
    SNE14.setVisible(!lexique.HostFieldGetData("LIE14").trim().equals(""));
    SNE15.setVisible(!lexique.HostFieldGetData("LIE15").trim().equals(""));
    SNE16.setVisible(!lexique.HostFieldGetData("LIE16").trim().equals(""));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Caisse"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_SASIE_LIBELLEE)) {
        lexique.HostScreenSendKey(this, "F2");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    sNBarreBouton = new SNBarreBouton();
    snPanelPrincipal = new SNPanelContenu();
    pnlOperations = new SNPanelTitre();
    lbLibelle = new SNLabelUnite();
    lbSens = new SNLabelUnite();
    lbMontant = new SNLabelUnite();
    ERE01 = new SNLabelChamp();
    LIE01 = new XRiTextField();
    SNE01 = new SNLabelChamp();
    MTE01 = new XRiTextField();
    BT_PGUP = new JButton();
    ERE02 = new SNLabelChamp();
    LIE02 = new XRiTextField();
    SNE02 = new SNLabelChamp();
    MTE02 = new XRiTextField();
    ERE03 = new SNLabelChamp();
    LIE03 = new XRiTextField();
    SNE03 = new SNLabelChamp();
    MTE03 = new XRiTextField();
    ERE04 = new SNLabelChamp();
    LIE04 = new XRiTextField();
    SNE04 = new SNLabelChamp();
    MTE04 = new XRiTextField();
    ERE05 = new SNLabelChamp();
    LIE05 = new XRiTextField();
    SNE05 = new SNLabelChamp();
    MTE05 = new XRiTextField();
    ERE06 = new SNLabelChamp();
    LIE06 = new XRiTextField();
    SNE06 = new SNLabelChamp();
    MTE06 = new XRiTextField();
    ERE07 = new SNLabelChamp();
    LIE07 = new XRiTextField();
    SNE07 = new SNLabelChamp();
    MTE07 = new XRiTextField();
    ERE08 = new SNLabelChamp();
    LIE08 = new XRiTextField();
    SNE08 = new SNLabelChamp();
    MTE08 = new XRiTextField();
    ERE09 = new SNLabelChamp();
    LIE09 = new XRiTextField();
    SNE09 = new SNLabelChamp();
    MTE09 = new XRiTextField();
    ERE10 = new SNLabelChamp();
    LIE10 = new XRiTextField();
    SNE10 = new SNLabelChamp();
    MTE10 = new XRiTextField();
    LIE11 = new XRiTextField();
    SNE11 = new SNLabelChamp();
    MTE11 = new XRiTextField();
    LIE12 = new XRiTextField();
    SNE12 = new SNLabelChamp();
    MTE12 = new XRiTextField();
    LIE13 = new XRiTextField();
    SNE13 = new SNLabelChamp();
    MTE13 = new XRiTextField();
    LIE14 = new XRiTextField();
    SNE14 = new SNLabelChamp();
    MTE14 = new XRiTextField();
    LIE15 = new XRiTextField();
    SNE15 = new SNLabelChamp();
    MTE15 = new XRiTextField();
    LIE16 = new XRiTextField();
    SNE16 = new SNLabelChamp();
    MTE16 = new XRiTextField();
    sNLabelChamp1 = new SNLabelChamp();
    TOTCAI = new XRiTextField();
    BT_PGDOWN = new JButton();
    ERE11 = new SNLabelChamp();
    ERE12 = new SNLabelChamp();
    ERE13 = new SNLabelChamp();
    ERE14 = new SNLabelChamp();
    ERE15 = new SNLabelChamp();
    ERE16 = new SNLabelChamp();

    //======== this ========
    setMinimumSize(new Dimension(875, 622));
    setPreferredSize(new Dimension(875, 622));
    setMaximumSize(new Dimension(875, 622));
    setName("this");
    setLayout(new BorderLayout());

    //---- sNBarreBouton ----
    sNBarreBouton.setName("sNBarreBouton");
    add(sNBarreBouton, BorderLayout.SOUTH);

    //======== snPanelPrincipal ========
    {
      snPanelPrincipal.setPreferredSize(new Dimension(700, 520));
      snPanelPrincipal.setMinimumSize(new Dimension(700, 520));
      snPanelPrincipal.setMaximumSize(new Dimension(700, 520));
      snPanelPrincipal.setName("snPanelPrincipal");
      snPanelPrincipal.setLayout(new GridBagLayout());
      ((GridBagLayout)snPanelPrincipal.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)snPanelPrincipal.getLayout()).rowHeights = new int[] {0, 0};
      ((GridBagLayout)snPanelPrincipal.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)snPanelPrincipal.getLayout()).rowWeights = new double[] {1.0, 1.0E-4};

      //======== pnlOperations ========
      {
        pnlOperations.setAutoscrolls(true);
        pnlOperations.setBackground(new Color(239, 239, 222));
        pnlOperations.setName("pnlOperations");
        pnlOperations.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlOperations.getLayout()).columnWidths = new int[] {0, 0, 48, 0, 0, 0};
        ((GridBagLayout)pnlOperations.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        ((GridBagLayout)pnlOperations.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnlOperations.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

        //---- lbLibelle ----
        lbLibelle.setText("Libell\u00e9 de l'op\u00e9ration");
        lbLibelle.setName("lbLibelle");
        pnlOperations.add(lbLibelle, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- lbSens ----
        lbSens.setText("Sens");
        lbSens.setName("lbSens");
        pnlOperations.add(lbSens, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- lbMontant ----
        lbMontant.setText("Montant");
        lbMontant.setMinimumSize(new Dimension(100, 30));
        lbMontant.setPreferredSize(new Dimension(100, 30));
        lbMontant.setMaximumSize(new Dimension(100, 30));
        lbMontant.setName("lbMontant");
        pnlOperations.add(lbMontant, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- ERE01 ----
        ERE01.setText("text");
        ERE01.setPreferredSize(new Dimension(28, 28));
        ERE01.setMaximumSize(new Dimension(28, 28));
        ERE01.setMinimumSize(new Dimension(28, 28));
        ERE01.setName("ERE01");
        pnlOperations.add(ERE01, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE01 ----
        LIE01.setPreferredSize(new Dimension(500, 28));
        LIE01.setMinimumSize(new Dimension(500, 28));
        LIE01.setMaximumSize(new Dimension(500, 28));
        LIE01.setFont(new Font("sansserif", Font.PLAIN, 14));
        LIE01.setName("LIE01");
        pnlOperations.add(LIE01, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- SNE01 ----
        SNE01.setPreferredSize(new Dimension(28, 28));
        SNE01.setMinimumSize(new Dimension(28, 28));
        SNE01.setMaximumSize(new Dimension(28, 28));
        SNE01.setFont(new Font("sansserif", Font.PLAIN, 14));
        SNE01.setName("SNE01");
        pnlOperations.add(SNE01, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE01 ----
        MTE01.setPreferredSize(new Dimension(100, 28));
        MTE01.setMinimumSize(new Dimension(100, 28));
        MTE01.setMaximumSize(new Dimension(100, 28));
        MTE01.setFont(new Font("sansserif", Font.PLAIN, 14));
        MTE01.setName("MTE01");
        pnlOperations.add(MTE01, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- BT_PGUP ----
        BT_PGUP.setText("");
        BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGUP.setName("BT_PGUP");
        pnlOperations.add(BT_PGUP, new GridBagConstraints(4, 1, 1, 8, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 0), 0, 0));

        //---- ERE02 ----
        ERE02.setText("text");
        ERE02.setPreferredSize(new Dimension(28, 28));
        ERE02.setMaximumSize(new Dimension(28, 28));
        ERE02.setMinimumSize(new Dimension(28, 28));
        ERE02.setName("ERE02");
        pnlOperations.add(ERE02, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE02 ----
        LIE02.setMaximumSize(new Dimension(500, 28));
        LIE02.setMinimumSize(new Dimension(500, 28));
        LIE02.setPreferredSize(new Dimension(500, 28));
        LIE02.setFont(new Font("sansserif", Font.PLAIN, 14));
        LIE02.setName("LIE02");
        pnlOperations.add(LIE02, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- SNE02 ----
        SNE02.setMaximumSize(new Dimension(28, 28));
        SNE02.setMinimumSize(new Dimension(28, 28));
        SNE02.setPreferredSize(new Dimension(28, 28));
        SNE02.setFont(new Font("sansserif", Font.PLAIN, 14));
        SNE02.setName("SNE02");
        pnlOperations.add(SNE02, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE02 ----
        MTE02.setMaximumSize(new Dimension(100, 28));
        MTE02.setMinimumSize(new Dimension(100, 28));
        MTE02.setPreferredSize(new Dimension(100, 28));
        MTE02.setFont(new Font("sansserif", Font.PLAIN, 14));
        MTE02.setName("MTE02");
        pnlOperations.add(MTE02, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- ERE03 ----
        ERE03.setText("text");
        ERE03.setPreferredSize(new Dimension(28, 28));
        ERE03.setMaximumSize(new Dimension(28, 28));
        ERE03.setMinimumSize(new Dimension(28, 28));
        ERE03.setName("ERE03");
        pnlOperations.add(ERE03, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE03 ----
        LIE03.setMaximumSize(new Dimension(500, 28));
        LIE03.setMinimumSize(new Dimension(500, 28));
        LIE03.setPreferredSize(new Dimension(500, 28));
        LIE03.setFont(new Font("sansserif", Font.PLAIN, 14));
        LIE03.setName("LIE03");
        pnlOperations.add(LIE03, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- SNE03 ----
        SNE03.setMaximumSize(new Dimension(28, 28));
        SNE03.setMinimumSize(new Dimension(28, 28));
        SNE03.setPreferredSize(new Dimension(28, 28));
        SNE03.setFont(new Font("sansserif", Font.PLAIN, 14));
        SNE03.setName("SNE03");
        pnlOperations.add(SNE03, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE03 ----
        MTE03.setMaximumSize(new Dimension(100, 28));
        MTE03.setMinimumSize(new Dimension(100, 28));
        MTE03.setPreferredSize(new Dimension(100, 28));
        MTE03.setFont(new Font("sansserif", Font.PLAIN, 14));
        MTE03.setName("MTE03");
        pnlOperations.add(MTE03, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- ERE04 ----
        ERE04.setText("text");
        ERE04.setPreferredSize(new Dimension(28, 28));
        ERE04.setMaximumSize(new Dimension(28, 28));
        ERE04.setMinimumSize(new Dimension(28, 28));
        ERE04.setName("ERE04");
        pnlOperations.add(ERE04, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE04 ----
        LIE04.setMaximumSize(new Dimension(500, 28));
        LIE04.setMinimumSize(new Dimension(500, 28));
        LIE04.setPreferredSize(new Dimension(500, 28));
        LIE04.setFont(new Font("sansserif", Font.PLAIN, 14));
        LIE04.setName("LIE04");
        pnlOperations.add(LIE04, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- SNE04 ----
        SNE04.setMaximumSize(new Dimension(28, 28));
        SNE04.setMinimumSize(new Dimension(28, 28));
        SNE04.setPreferredSize(new Dimension(28, 28));
        SNE04.setFont(new Font("sansserif", Font.PLAIN, 14));
        SNE04.setName("SNE04");
        pnlOperations.add(SNE04, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE04 ----
        MTE04.setMaximumSize(new Dimension(100, 28));
        MTE04.setMinimumSize(new Dimension(100, 28));
        MTE04.setPreferredSize(new Dimension(100, 28));
        MTE04.setFont(new Font("sansserif", Font.PLAIN, 14));
        MTE04.setName("MTE04");
        pnlOperations.add(MTE04, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- ERE05 ----
        ERE05.setText("text");
        ERE05.setPreferredSize(new Dimension(28, 28));
        ERE05.setMaximumSize(new Dimension(28, 28));
        ERE05.setMinimumSize(new Dimension(28, 28));
        ERE05.setName("ERE05");
        pnlOperations.add(ERE05, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE05 ----
        LIE05.setMaximumSize(new Dimension(500, 28));
        LIE05.setMinimumSize(new Dimension(500, 28));
        LIE05.setPreferredSize(new Dimension(500, 28));
        LIE05.setFont(new Font("sansserif", Font.PLAIN, 14));
        LIE05.setName("LIE05");
        pnlOperations.add(LIE05, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- SNE05 ----
        SNE05.setPreferredSize(new Dimension(28, 28));
        SNE05.setMinimumSize(new Dimension(28, 28));
        SNE05.setMaximumSize(new Dimension(28, 28));
        SNE05.setFont(new Font("sansserif", Font.PLAIN, 14));
        SNE05.setName("SNE05");
        pnlOperations.add(SNE05, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE05 ----
        MTE05.setMaximumSize(new Dimension(100, 28));
        MTE05.setMinimumSize(new Dimension(100, 28));
        MTE05.setPreferredSize(new Dimension(100, 28));
        MTE05.setFont(new Font("sansserif", Font.PLAIN, 14));
        MTE05.setName("MTE05");
        pnlOperations.add(MTE05, new GridBagConstraints(3, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- ERE06 ----
        ERE06.setText("text");
        ERE06.setPreferredSize(new Dimension(28, 28));
        ERE06.setMaximumSize(new Dimension(28, 28));
        ERE06.setMinimumSize(new Dimension(28, 28));
        ERE06.setName("ERE06");
        pnlOperations.add(ERE06, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE06 ----
        LIE06.setMaximumSize(new Dimension(500, 28));
        LIE06.setMinimumSize(new Dimension(500, 28));
        LIE06.setPreferredSize(new Dimension(500, 28));
        LIE06.setFont(new Font("sansserif", Font.PLAIN, 14));
        LIE06.setName("LIE06");
        pnlOperations.add(LIE06, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- SNE06 ----
        SNE06.setMaximumSize(new Dimension(28, 28));
        SNE06.setMinimumSize(new Dimension(28, 28));
        SNE06.setPreferredSize(new Dimension(28, 28));
        SNE06.setFont(new Font("sansserif", Font.PLAIN, 14));
        SNE06.setName("SNE06");
        pnlOperations.add(SNE06, new GridBagConstraints(2, 6, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE06 ----
        MTE06.setMaximumSize(new Dimension(100, 28));
        MTE06.setMinimumSize(new Dimension(100, 28));
        MTE06.setPreferredSize(new Dimension(100, 28));
        MTE06.setFont(new Font("sansserif", Font.PLAIN, 14));
        MTE06.setName("MTE06");
        pnlOperations.add(MTE06, new GridBagConstraints(3, 6, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- ERE07 ----
        ERE07.setText("text");
        ERE07.setPreferredSize(new Dimension(28, 28));
        ERE07.setMaximumSize(new Dimension(28, 28));
        ERE07.setMinimumSize(new Dimension(28, 28));
        ERE07.setName("ERE07");
        pnlOperations.add(ERE07, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE07 ----
        LIE07.setMaximumSize(new Dimension(500, 28));
        LIE07.setMinimumSize(new Dimension(500, 28));
        LIE07.setPreferredSize(new Dimension(500, 28));
        LIE07.setFont(new Font("sansserif", Font.PLAIN, 14));
        LIE07.setName("LIE07");
        pnlOperations.add(LIE07, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- SNE07 ----
        SNE07.setPreferredSize(new Dimension(28, 28));
        SNE07.setMinimumSize(new Dimension(28, 28));
        SNE07.setMaximumSize(new Dimension(28, 28));
        SNE07.setFont(new Font("sansserif", Font.PLAIN, 14));
        SNE07.setName("SNE07");
        pnlOperations.add(SNE07, new GridBagConstraints(2, 7, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE07 ----
        MTE07.setMaximumSize(new Dimension(100, 28));
        MTE07.setMinimumSize(new Dimension(100, 28));
        MTE07.setPreferredSize(new Dimension(100, 28));
        MTE07.setFont(new Font("sansserif", Font.PLAIN, 14));
        MTE07.setName("MTE07");
        pnlOperations.add(MTE07, new GridBagConstraints(3, 7, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- ERE08 ----
        ERE08.setText("text");
        ERE08.setPreferredSize(new Dimension(28, 28));
        ERE08.setMaximumSize(new Dimension(28, 28));
        ERE08.setMinimumSize(new Dimension(28, 28));
        ERE08.setName("ERE08");
        pnlOperations.add(ERE08, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE08 ----
        LIE08.setMaximumSize(new Dimension(500, 28));
        LIE08.setMinimumSize(new Dimension(500, 28));
        LIE08.setPreferredSize(new Dimension(500, 28));
        LIE08.setFont(new Font("sansserif", Font.PLAIN, 14));
        LIE08.setName("LIE08");
        pnlOperations.add(LIE08, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- SNE08 ----
        SNE08.setMaximumSize(new Dimension(28, 28));
        SNE08.setMinimumSize(new Dimension(28, 28));
        SNE08.setPreferredSize(new Dimension(28, 28));
        SNE08.setFont(new Font("sansserif", Font.PLAIN, 14));
        SNE08.setName("SNE08");
        pnlOperations.add(SNE08, new GridBagConstraints(2, 8, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE08 ----
        MTE08.setMaximumSize(new Dimension(100, 28));
        MTE08.setMinimumSize(new Dimension(100, 28));
        MTE08.setPreferredSize(new Dimension(100, 28));
        MTE08.setFont(new Font("sansserif", Font.PLAIN, 14));
        MTE08.setName("MTE08");
        pnlOperations.add(MTE08, new GridBagConstraints(3, 8, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- ERE09 ----
        ERE09.setText("text");
        ERE09.setPreferredSize(new Dimension(28, 28));
        ERE09.setMaximumSize(new Dimension(28, 28));
        ERE09.setMinimumSize(new Dimension(28, 28));
        ERE09.setName("ERE09");
        pnlOperations.add(ERE09, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE09 ----
        LIE09.setMaximumSize(new Dimension(500, 28));
        LIE09.setMinimumSize(new Dimension(500, 28));
        LIE09.setPreferredSize(new Dimension(500, 28));
        LIE09.setFont(new Font("sansserif", Font.PLAIN, 14));
        LIE09.setName("LIE09");
        pnlOperations.add(LIE09, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- SNE09 ----
        SNE09.setPreferredSize(new Dimension(28, 28));
        SNE09.setMinimumSize(new Dimension(28, 28));
        SNE09.setMaximumSize(new Dimension(28, 28));
        SNE09.setFont(new Font("sansserif", Font.PLAIN, 14));
        SNE09.setName("SNE09");
        pnlOperations.add(SNE09, new GridBagConstraints(2, 9, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE09 ----
        MTE09.setMaximumSize(new Dimension(100, 28));
        MTE09.setMinimumSize(new Dimension(100, 28));
        MTE09.setPreferredSize(new Dimension(100, 28));
        MTE09.setFont(new Font("sansserif", Font.PLAIN, 14));
        MTE09.setName("MTE09");
        pnlOperations.add(MTE09, new GridBagConstraints(3, 9, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- ERE10 ----
        ERE10.setText("text");
        ERE10.setPreferredSize(new Dimension(28, 28));
        ERE10.setMaximumSize(new Dimension(28, 28));
        ERE10.setMinimumSize(new Dimension(28, 28));
        ERE10.setName("ERE10");
        pnlOperations.add(ERE10, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE10 ----
        LIE10.setMaximumSize(new Dimension(500, 28));
        LIE10.setMinimumSize(new Dimension(500, 28));
        LIE10.setPreferredSize(new Dimension(500, 28));
        LIE10.setFont(new Font("sansserif", Font.PLAIN, 14));
        LIE10.setName("LIE10");
        pnlOperations.add(LIE10, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- SNE10 ----
        SNE10.setMaximumSize(new Dimension(28, 28));
        SNE10.setMinimumSize(new Dimension(28, 28));
        SNE10.setPreferredSize(new Dimension(28, 28));
        SNE10.setFont(new Font("sansserif", Font.PLAIN, 14));
        SNE10.setName("SNE10");
        pnlOperations.add(SNE10, new GridBagConstraints(2, 10, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE10 ----
        MTE10.setMaximumSize(new Dimension(100, 28));
        MTE10.setMinimumSize(new Dimension(100, 28));
        MTE10.setPreferredSize(new Dimension(100, 28));
        MTE10.setFont(new Font("sansserif", Font.PLAIN, 14));
        MTE10.setName("MTE10");
        pnlOperations.add(MTE10, new GridBagConstraints(3, 10, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE11 ----
        LIE11.setMaximumSize(new Dimension(500, 28));
        LIE11.setMinimumSize(new Dimension(500, 28));
        LIE11.setPreferredSize(new Dimension(500, 28));
        LIE11.setFont(new Font("sansserif", Font.PLAIN, 14));
        LIE11.setName("LIE11");
        pnlOperations.add(LIE11, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- SNE11 ----
        SNE11.setPreferredSize(new Dimension(28, 28));
        SNE11.setMaximumSize(new Dimension(28, 28));
        SNE11.setMinimumSize(new Dimension(28, 28));
        SNE11.setFont(new Font("sansserif", Font.PLAIN, 14));
        SNE11.setName("SNE11");
        pnlOperations.add(SNE11, new GridBagConstraints(2, 11, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE11 ----
        MTE11.setMaximumSize(new Dimension(100, 28));
        MTE11.setMinimumSize(new Dimension(100, 28));
        MTE11.setPreferredSize(new Dimension(100, 28));
        MTE11.setFont(new Font("sansserif", Font.PLAIN, 14));
        MTE11.setName("MTE11");
        pnlOperations.add(MTE11, new GridBagConstraints(3, 11, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE12 ----
        LIE12.setMaximumSize(new Dimension(500, 28));
        LIE12.setMinimumSize(new Dimension(500, 28));
        LIE12.setPreferredSize(new Dimension(500, 28));
        LIE12.setFont(new Font("sansserif", Font.PLAIN, 14));
        LIE12.setName("LIE12");
        pnlOperations.add(LIE12, new GridBagConstraints(1, 12, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- SNE12 ----
        SNE12.setMinimumSize(new Dimension(28, 28));
        SNE12.setMaximumSize(new Dimension(28, 28));
        SNE12.setPreferredSize(new Dimension(28, 28));
        SNE12.setFont(new Font("sansserif", Font.PLAIN, 14));
        SNE12.setName("SNE12");
        pnlOperations.add(SNE12, new GridBagConstraints(2, 12, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE12 ----
        MTE12.setMaximumSize(new Dimension(100, 28));
        MTE12.setMinimumSize(new Dimension(100, 28));
        MTE12.setPreferredSize(new Dimension(100, 28));
        MTE12.setFont(new Font("sansserif", Font.PLAIN, 14));
        MTE12.setName("MTE12");
        pnlOperations.add(MTE12, new GridBagConstraints(3, 12, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE13 ----
        LIE13.setMaximumSize(new Dimension(500, 28));
        LIE13.setMinimumSize(new Dimension(500, 28));
        LIE13.setPreferredSize(new Dimension(500, 28));
        LIE13.setFont(new Font("sansserif", Font.PLAIN, 14));
        LIE13.setName("LIE13");
        pnlOperations.add(LIE13, new GridBagConstraints(1, 13, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- SNE13 ----
        SNE13.setPreferredSize(new Dimension(28, 28));
        SNE13.setMinimumSize(new Dimension(28, 28));
        SNE13.setMaximumSize(new Dimension(28, 28));
        SNE13.setFont(new Font("sansserif", Font.PLAIN, 14));
        SNE13.setName("SNE13");
        pnlOperations.add(SNE13, new GridBagConstraints(2, 13, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE13 ----
        MTE13.setMaximumSize(new Dimension(100, 28));
        MTE13.setMinimumSize(new Dimension(100, 28));
        MTE13.setPreferredSize(new Dimension(100, 28));
        MTE13.setFont(new Font("sansserif", Font.PLAIN, 14));
        MTE13.setName("MTE13");
        pnlOperations.add(MTE13, new GridBagConstraints(3, 13, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE14 ----
        LIE14.setMaximumSize(new Dimension(500, 28));
        LIE14.setMinimumSize(new Dimension(500, 28));
        LIE14.setPreferredSize(new Dimension(500, 28));
        LIE14.setFont(new Font("sansserif", Font.PLAIN, 14));
        LIE14.setName("LIE14");
        pnlOperations.add(LIE14, new GridBagConstraints(1, 14, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- SNE14 ----
        SNE14.setMaximumSize(new Dimension(28, 28));
        SNE14.setMinimumSize(new Dimension(28, 28));
        SNE14.setPreferredSize(new Dimension(28, 28));
        SNE14.setFont(new Font("sansserif", Font.PLAIN, 14));
        SNE14.setName("SNE14");
        pnlOperations.add(SNE14, new GridBagConstraints(2, 14, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE14 ----
        MTE14.setMaximumSize(new Dimension(100, 28));
        MTE14.setMinimumSize(new Dimension(100, 28));
        MTE14.setPreferredSize(new Dimension(100, 28));
        MTE14.setFont(new Font("sansserif", Font.PLAIN, 14));
        MTE14.setName("MTE14");
        pnlOperations.add(MTE14, new GridBagConstraints(3, 14, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE15 ----
        LIE15.setMaximumSize(new Dimension(500, 28));
        LIE15.setMinimumSize(new Dimension(500, 28));
        LIE15.setPreferredSize(new Dimension(500, 28));
        LIE15.setFont(new Font("sansserif", Font.PLAIN, 14));
        LIE15.setName("LIE15");
        pnlOperations.add(LIE15, new GridBagConstraints(1, 15, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- SNE15 ----
        SNE15.setPreferredSize(new Dimension(28, 28));
        SNE15.setMinimumSize(new Dimension(28, 28));
        SNE15.setMaximumSize(new Dimension(28, 28));
        SNE15.setFont(new Font("sansserif", Font.PLAIN, 14));
        SNE15.setName("SNE15");
        pnlOperations.add(SNE15, new GridBagConstraints(2, 15, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE15 ----
        MTE15.setMaximumSize(new Dimension(100, 28));
        MTE15.setMinimumSize(new Dimension(100, 28));
        MTE15.setPreferredSize(new Dimension(100, 28));
        MTE15.setFont(new Font("sansserif", Font.PLAIN, 14));
        MTE15.setName("MTE15");
        pnlOperations.add(MTE15, new GridBagConstraints(3, 15, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE16 ----
        LIE16.setMaximumSize(new Dimension(500, 28));
        LIE16.setMinimumSize(new Dimension(500, 28));
        LIE16.setPreferredSize(new Dimension(500, 28));
        LIE16.setFont(new Font("sansserif", Font.PLAIN, 14));
        LIE16.setName("LIE16");
        pnlOperations.add(LIE16, new GridBagConstraints(1, 16, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- SNE16 ----
        SNE16.setMaximumSize(new Dimension(28, 28));
        SNE16.setMinimumSize(new Dimension(28, 28));
        SNE16.setPreferredSize(new Dimension(28, 28));
        SNE16.setFont(new Font("sansserif", Font.PLAIN, 14));
        SNE16.setName("SNE16");
        pnlOperations.add(SNE16, new GridBagConstraints(2, 16, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE16 ----
        MTE16.setMaximumSize(new Dimension(100, 28));
        MTE16.setMinimumSize(new Dimension(100, 28));
        MTE16.setPreferredSize(new Dimension(100, 28));
        MTE16.setFont(new Font("sansserif", Font.PLAIN, 14));
        MTE16.setName("MTE16");
        pnlOperations.add(MTE16, new GridBagConstraints(3, 16, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- sNLabelChamp1 ----
        sNLabelChamp1.setText("Total des op\u00e9rations");
        sNLabelChamp1.setName("sNLabelChamp1");
        pnlOperations.add(sNLabelChamp1, new GridBagConstraints(1, 17, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 2), 0, 0));

        //---- TOTCAI ----
        TOTCAI.setMaximumSize(new Dimension(100, 28));
        TOTCAI.setMinimumSize(new Dimension(100, 28));
        TOTCAI.setPreferredSize(new Dimension(100, 28));
        TOTCAI.setFont(new Font("sansserif", Font.PLAIN, 14));
        TOTCAI.setName("TOTCAI");
        pnlOperations.add(TOTCAI, new GridBagConstraints(3, 17, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 2), 0, 0));

        //---- BT_PGDOWN ----
        BT_PGDOWN.setText("");
        BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGDOWN.setName("BT_PGDOWN");
        pnlOperations.add(BT_PGDOWN, new GridBagConstraints(4, 9, 1, 8, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 0), 0, 0));

        //---- ERE11 ----
        ERE11.setText("text");
        ERE11.setPreferredSize(new Dimension(28, 28));
        ERE11.setMaximumSize(new Dimension(28, 28));
        ERE11.setMinimumSize(new Dimension(28, 28));
        ERE11.setName("ERE11");
        pnlOperations.add(ERE11, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- ERE12 ----
        ERE12.setText("text");
        ERE12.setPreferredSize(new Dimension(28, 28));
        ERE12.setMaximumSize(new Dimension(28, 28));
        ERE12.setMinimumSize(new Dimension(28, 28));
        ERE12.setName("ERE12");
        pnlOperations.add(ERE12, new GridBagConstraints(0, 12, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- ERE13 ----
        ERE13.setText("text");
        ERE13.setPreferredSize(new Dimension(28, 28));
        ERE13.setMaximumSize(new Dimension(28, 28));
        ERE13.setMinimumSize(new Dimension(28, 28));
        ERE13.setName("ERE13");
        pnlOperations.add(ERE13, new GridBagConstraints(0, 13, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- ERE14 ----
        ERE14.setText("text");
        ERE14.setPreferredSize(new Dimension(28, 28));
        ERE14.setMaximumSize(new Dimension(28, 28));
        ERE14.setMinimumSize(new Dimension(28, 28));
        ERE14.setName("ERE14");
        pnlOperations.add(ERE14, new GridBagConstraints(0, 14, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- ERE15 ----
        ERE15.setText("text");
        ERE15.setPreferredSize(new Dimension(28, 28));
        ERE15.setMaximumSize(new Dimension(28, 28));
        ERE15.setMinimumSize(new Dimension(28, 28));
        ERE15.setName("ERE15");
        pnlOperations.add(ERE15, new GridBagConstraints(0, 15, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- ERE16 ----
        ERE16.setText("text");
        ERE16.setPreferredSize(new Dimension(28, 28));
        ERE16.setMaximumSize(new Dimension(28, 28));
        ERE16.setMinimumSize(new Dimension(28, 28));
        ERE16.setName("ERE16");
        pnlOperations.add(ERE16, new GridBagConstraints(0, 16, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));
      }
      snPanelPrincipal.add(pnlOperations, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(snPanelPrincipal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBarreBouton sNBarreBouton;
  private SNPanelContenu snPanelPrincipal;
  private SNPanelTitre pnlOperations;
  private SNLabelUnite lbLibelle;
  private SNLabelUnite lbSens;
  private SNLabelUnite lbMontant;
  private SNLabelChamp ERE01;
  private XRiTextField LIE01;
  private SNLabelChamp SNE01;
  private XRiTextField MTE01;
  private JButton BT_PGUP;
  private SNLabelChamp ERE02;
  private XRiTextField LIE02;
  private SNLabelChamp SNE02;
  private XRiTextField MTE02;
  private SNLabelChamp ERE03;
  private XRiTextField LIE03;
  private SNLabelChamp SNE03;
  private XRiTextField MTE03;
  private SNLabelChamp ERE04;
  private XRiTextField LIE04;
  private SNLabelChamp SNE04;
  private XRiTextField MTE04;
  private SNLabelChamp ERE05;
  private XRiTextField LIE05;
  private SNLabelChamp SNE05;
  private XRiTextField MTE05;
  private SNLabelChamp ERE06;
  private XRiTextField LIE06;
  private SNLabelChamp SNE06;
  private XRiTextField MTE06;
  private SNLabelChamp ERE07;
  private XRiTextField LIE07;
  private SNLabelChamp SNE07;
  private XRiTextField MTE07;
  private SNLabelChamp ERE08;
  private XRiTextField LIE08;
  private SNLabelChamp SNE08;
  private XRiTextField MTE08;
  private SNLabelChamp ERE09;
  private XRiTextField LIE09;
  private SNLabelChamp SNE09;
  private XRiTextField MTE09;
  private SNLabelChamp ERE10;
  private XRiTextField LIE10;
  private SNLabelChamp SNE10;
  private XRiTextField MTE10;
  private XRiTextField LIE11;
  private SNLabelChamp SNE11;
  private XRiTextField MTE11;
  private XRiTextField LIE12;
  private SNLabelChamp SNE12;
  private XRiTextField MTE12;
  private XRiTextField LIE13;
  private SNLabelChamp SNE13;
  private XRiTextField MTE13;
  private XRiTextField LIE14;
  private SNLabelChamp SNE14;
  private XRiTextField MTE14;
  private XRiTextField LIE15;
  private SNLabelChamp SNE15;
  private XRiTextField MTE15;
  private XRiTextField LIE16;
  private SNLabelChamp SNE16;
  private XRiTextField MTE16;
  private SNLabelChamp sNLabelChamp1;
  private XRiTextField TOTCAI;
  private JButton BT_PGDOWN;
  private SNLabelChamp ERE11;
  private SNLabelChamp ERE12;
  private SNLabelChamp ERE13;
  private SNLabelChamp ERE14;
  private SNLabelChamp ERE15;
  private SNLabelChamp ERE16;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
