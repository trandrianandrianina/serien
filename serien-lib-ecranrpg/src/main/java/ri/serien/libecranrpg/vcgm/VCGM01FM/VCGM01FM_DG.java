
package ri.serien.libecranrpg.vcgm.VCGM01FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM01FM_DG extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] DGNAT_Value = { "", "7", "8", };
  private String[] DGNAT_Title = { "", "Obligatoire", "Facultative", };
  private String[] DGANA_Value = { "", "7", "8", };
  private String[] DGANA_Title = { "", "Obligatoire", "Facultative", };
  
  public VCGM01FM_DG(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    DGNAT.setValeurs(DGNAT_Value, DGNAT_Title);
    DGANA.setValeurs(DGANA_Value, DGANA_Title);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    OBJ_47_OBJ_47.setVisible(lexique.isPresent("DGBLOX"));
    OBJ_49.setVisible(lexique.isTrue("16"));
    OBJ_50_OBJ_50.setVisible(lexique.isTrue("16"));
    OBJ_51_OBJ_51.setVisible(lexique.isTrue("N16"));
    DGDEV.setVisible(lexique.isTrue("16"));
    DGETP.setVisible(lexique.isTrue("N16"));
    DGFP1X.setVisible(lexique.isTrue("N06"));
    OBJ_87_OBJ_87.setVisible(DGFP1X.isVisible());
    DGDP2X.setVisible(lexique.isTrue("N07"));
    DGDF2X.setVisible(lexique.isTrue("N08"));
    OBJ_87_OBJ_88.setVisible(DGDF2X.isVisible());
    OBJ_85_OBJ_86.setVisible(DGDP2X.isVisible());
    
    // icones
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44_OBJ_45 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_45_OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46_OBJ_47 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    DGANA = new XRiComboBox();
    DGNAT = new XRiComboBox();
    DGNOM = new XRiTextField();
    DGAD1 = new XRiTextField();
    DVLIB = new XRiTextField();
    OBJ_46_OBJ_46 = new JLabel();
    OBJ_50_OBJ_50 = new JLabel();
    OBJ_51_OBJ_51 = new JLabel();
    OBJ_48_OBJ_48 = new JLabel();
    OBJ_47_OBJ_47 = new JLabel();
    OBJ_49_OBJ_49 = new JLabel();
    OBJ_57_OBJ_57 = new JLabel();
    OBJ_66_OBJ_66 = new JLabel();
    DGBLOX = new XRiTextField();
    DGETP = new XRiTextField();
    DG445 = new XRiTextField();
    DGEUR = new XRiTextField();
    DGCGE = new XRiTextField();
    panel1 = new JPanel();
    DGAA1 = new XRiTextField();
    DGAA2 = new XRiTextField();
    DGAA3 = new XRiTextField();
    DGAA4 = new XRiTextField();
    DGAA5 = new XRiTextField();
    DGAA6 = new XRiTextField();
    DGDEV = new XRiTextField();
    panel4 = new JPanel();
    OBJ_71_OBJ_71 = new JLabel();
    OBJ_73_OBJ_73 = new JLabel();
    OBJ_75_OBJ_75 = new JLabel();
    OBJ_78_OBJ_78 = new JLabel();
    DGDEBX = new XRiTextField();
    DGFINX = new XRiTextField();
    OBJ_77_OBJ_77 = new JLabel();
    DGMEX = new XRiTextField();
    DGMPR = new XRiTextField();
    panel2 = new JPanel();
    OBJ_85_OBJ_85 = new JLabel();
    OBJ_89_OBJ_89 = new JLabel();
    DGDE1X = new XRiTextField();
    DGFE1X = new XRiTextField();
    DGDP1X = new XRiTextField();
    DGFP1X = new XRiTextField();
    WVB1 = new XRiTextField();
    OBJ_83_OBJ_83 = new JLabel();
    OBJ_87_OBJ_87 = new JLabel();
    label1 = new JLabel();
    panel5 = new JPanel();
    OBJ_85_OBJ_86 = new JLabel();
    OBJ_89_OBJ_90 = new JLabel();
    OBJ_83_OBJ_84 = new JLabel();
    OBJ_87_OBJ_88 = new JLabel();
    label3 = new JLabel();
    DGDE2X = new XRiTextField();
    DGFE2X = new XRiTextField();
    DGDP2X = new XRiTextField();
    DGDF2X = new XRiTextField();
    WVB2 = new XRiTextField();
    OBJ_101_OBJ_101 = new JLabel();
    DGMAS = new XRiTextField();
    OBJ_103_OBJ_103 = new JLabel();
    DGNMH = new XRiTextField();
    separator1 = compFactory.createSeparator("Exercice comptable");
    DGICS = new XRiTextField();
    OBJ_49 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44_OBJ_45 ----
          OBJ_44_OBJ_45.setText("Soci\u00e9t\u00e9");
          OBJ_44_OBJ_45.setName("OBJ_44_OBJ_45");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setName("INDETB");

          //---- OBJ_45_OBJ_46 ----
          OBJ_45_OBJ_46.setText("Code");
          OBJ_45_OBJ_46.setName("OBJ_45_OBJ_46");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setName("INDTYP");

          //---- OBJ_46_OBJ_47 ----
          OBJ_46_OBJ_47.setText("Ordre");
          OBJ_46_OBJ_47.setName("OBJ_46_OBJ_47");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(780, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Description g\u00e9n\u00e9rale");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- DGANA ----
            DGANA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DGANA.setName("DGANA");
            xTitledPanel1ContentContainer.add(DGANA);
            DGANA.setBounds(178, 109, 187, DGANA.getPreferredSize().height);

            //---- DGNAT ----
            DGNAT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DGNAT.setName("DGNAT");
            xTitledPanel1ContentContainer.add(DGNAT);
            DGNAT.setBounds(178, 139, 187, DGNAT.getPreferredSize().height);

            //---- DGNOM ----
            DGNOM.setName("DGNOM");
            xTitledPanel1ContentContainer.add(DGNOM);
            DGNOM.setBounds(178, 13, 310, DGNOM.getPreferredSize().height);

            //---- DGAD1 ----
            DGAD1.setName("DGAD1");
            xTitledPanel1ContentContainer.add(DGAD1);
            DGAD1.setBounds(178, 45, 310, DGAD1.getPreferredSize().height);

            //---- DVLIB ----
            DVLIB.setName("DVLIB");
            xTitledPanel1ContentContainer.add(DVLIB);
            DVLIB.setBounds(225, 77, 210, DVLIB.getPreferredSize().height);

            //---- OBJ_46_OBJ_46 ----
            OBJ_46_OBJ_46.setText("Nom ou raison sociale");
            OBJ_46_OBJ_46.setName("OBJ_46_OBJ_46");
            xTitledPanel1ContentContainer.add(OBJ_46_OBJ_46);
            OBJ_46_OBJ_46.setBounds(12, 17, 145, 20);

            //---- OBJ_50_OBJ_50 ----
            OBJ_50_OBJ_50.setText("Devise locale");
            OBJ_50_OBJ_50.setName("OBJ_50_OBJ_50");
            xTitledPanel1ContentContainer.add(OBJ_50_OBJ_50);
            OBJ_50_OBJ_50.setBounds(12, 81, 132, 20);

            //---- OBJ_51_OBJ_51 ----
            OBJ_51_OBJ_51.setText("Soci\u00e9t\u00e9 pilote");
            OBJ_51_OBJ_51.setName("OBJ_51_OBJ_51");
            xTitledPanel1ContentContainer.add(OBJ_51_OBJ_51);
            OBJ_51_OBJ_51.setBounds(12, 81, 132, 20);

            //---- OBJ_48_OBJ_48 ----
            OBJ_48_OBJ_48.setText("Compl\u00e9ment de nom");
            OBJ_48_OBJ_48.setName("OBJ_48_OBJ_48");
            xTitledPanel1ContentContainer.add(OBJ_48_OBJ_48);
            OBJ_48_OBJ_48.setBounds(12, 49, 130, 20);

            //---- OBJ_47_OBJ_47 ----
            OBJ_47_OBJ_47.setText("Date de blocage");
            OBJ_47_OBJ_47.setName("OBJ_47_OBJ_47");
            xTitledPanel1ContentContainer.add(OBJ_47_OBJ_47);
            OBJ_47_OBJ_47.setBounds(520, 17, 105, 20);

            //---- OBJ_49_OBJ_49 ----
            OBJ_49_OBJ_49.setText("Trt 4.4.5");
            OBJ_49_OBJ_49.setName("OBJ_49_OBJ_49");
            xTitledPanel1ContentContainer.add(OBJ_49_OBJ_49);
            OBJ_49_OBJ_49.setBounds(520, 49, 105, 20);

            //---- OBJ_57_OBJ_57 ----
            OBJ_57_OBJ_57.setText("Analytique");
            OBJ_57_OBJ_57.setName("OBJ_57_OBJ_57");
            xTitledPanel1ContentContainer.add(OBJ_57_OBJ_57);
            OBJ_57_OBJ_57.setBounds(12, 112, 66, 20);

            //---- OBJ_66_OBJ_66 ----
            OBJ_66_OBJ_66.setText("Nature");
            OBJ_66_OBJ_66.setName("OBJ_66_OBJ_66");
            xTitledPanel1ContentContainer.add(OBJ_66_OBJ_66);
            OBJ_66_OBJ_66.setBounds(12, 142, 43, 20);

            //---- DGBLOX ----
            DGBLOX.setName("DGBLOX");
            xTitledPanel1ContentContainer.add(DGBLOX);
            DGBLOX.setBounds(660, 13, 45, DGBLOX.getPreferredSize().height);

            //---- DGETP ----
            DGETP.setName("DGETP");
            xTitledPanel1ContentContainer.add(DGETP);
            DGETP.setBounds(178, 77, 40, DGETP.getPreferredSize().height);

            //---- DG445 ----
            DG445.setName("DG445");
            xTitledPanel1ContentContainer.add(DG445);
            DG445.setBounds(685, 45, 20, DG445.getPreferredSize().height);

            //---- DGEUR ----
            DGEUR.setName("DGEUR");
            xTitledPanel1ContentContainer.add(DGEUR);
            DGEUR.setBounds(445, 77, 20, DGEUR.getPreferredSize().height);

            //---- DGCGE ----
            DGCGE.setName("DGCGE");
            xTitledPanel1ContentContainer.add(DGCGE);
            DGCGE.setBounds(470, 77, 20, DGCGE.getPreferredSize().height);

            //======== panel1 ========
            {
              panel1.setBorder(new TitledBorder("Axes"));
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- DGAA1 ----
              DGAA1.setName("DGAA1");
              panel1.add(DGAA1);
              DGAA1.setBounds(20, 30, 20, DGAA1.getPreferredSize().height);

              //---- DGAA2 ----
              DGAA2.setName("DGAA2");
              panel1.add(DGAA2);
              DGAA2.setBounds(44, 30, 20, DGAA2.getPreferredSize().height);

              //---- DGAA3 ----
              DGAA3.setName("DGAA3");
              panel1.add(DGAA3);
              DGAA3.setBounds(68, 30, 20, DGAA3.getPreferredSize().height);

              //---- DGAA4 ----
              DGAA4.setName("DGAA4");
              panel1.add(DGAA4);
              DGAA4.setBounds(92, 30, 20, DGAA4.getPreferredSize().height);

              //---- DGAA5 ----
              DGAA5.setName("DGAA5");
              panel1.add(DGAA5);
              DGAA5.setBounds(116, 30, 20, DGAA5.getPreferredSize().height);

              //---- DGAA6 ----
              DGAA6.setName("DGAA6");
              panel1.add(DGAA6);
              DGAA6.setBounds(140, 30, 20, DGAA6.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel1);
            panel1.setBounds(520, 110, 185, 75);

            //---- DGDEV ----
            DGDEV.setName("DGDEV");
            xTitledPanel1ContentContainer.add(DGDEV);
            DGDEV.setBounds(180, 77, 40, DGDEV.getPreferredSize().height);

            //======== panel4 ========
            {
              panel4.setBorder(new TitledBorder("Exercice  de d\u00e9marrage"));
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);

              //---- OBJ_71_OBJ_71 ----
              OBJ_71_OBJ_71.setText("Nombre de p\u00e9riodes");
              OBJ_71_OBJ_71.setName("OBJ_71_OBJ_71");
              panel4.add(OBJ_71_OBJ_71);
              OBJ_71_OBJ_71.setBounds(33, 42, 185, 28);

              //---- OBJ_73_OBJ_73 ----
              OBJ_73_OBJ_73.setText("D\u00e9but de la p\u00e9riode");
              OBJ_73_OBJ_73.setName("OBJ_73_OBJ_73");
              panel4.add(OBJ_73_OBJ_73);
              OBJ_73_OBJ_73.setBounds(388, 42, 150, 28);

              //---- OBJ_75_OBJ_75 ----
              OBJ_75_OBJ_75.setText("Dur\u00e9e de la p\u00e9riode");
              OBJ_75_OBJ_75.setName("OBJ_75_OBJ_75");
              panel4.add(OBJ_75_OBJ_75);
              OBJ_75_OBJ_75.setBounds(33, 82, 185, 28);

              //---- OBJ_78_OBJ_78 ----
              OBJ_78_OBJ_78.setText("Fin de la p\u00e9riode");
              OBJ_78_OBJ_78.setName("OBJ_78_OBJ_78");
              panel4.add(OBJ_78_OBJ_78);
              OBJ_78_OBJ_78.setBounds(388, 82, 150, 28);

              //---- DGDEBX ----
              DGDEBX.setName("DGDEBX");
              panel4.add(DGDEBX);
              DGDEBX.setBounds(543, 42, 45, DGDEBX.getPreferredSize().height);

              //---- DGFINX ----
              DGFINX.setName("DGFINX");
              panel4.add(DGFINX);
              DGFINX.setBounds(543, 82, 45, DGFINX.getPreferredSize().height);

              //---- OBJ_77_OBJ_77 ----
              OBJ_77_OBJ_77.setText("mois");
              OBJ_77_OBJ_77.setName("OBJ_77_OBJ_77");
              panel4.add(OBJ_77_OBJ_77);
              OBJ_77_OBJ_77.setBounds(263, 82, 55, 28);

              //---- DGMEX ----
              DGMEX.setName("DGMEX");
              panel4.add(DGMEX);
              DGMEX.setBounds(223, 42, 30, DGMEX.getPreferredSize().height);

              //---- DGMPR ----
              DGMPR.setName("DGMPR");
              panel4.add(DGMPR);
              DGMPR.setBounds(223, 82, 30, DGMPR.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel4.getComponentCount(); i++) {
                  Rectangle bounds = panel4.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel4.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel4.setMinimumSize(preferredSize);
                panel4.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel4);
            panel4.setBounds(20, 213, 685, 127);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("Exercice principal"));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- OBJ_85_OBJ_85 ----
              OBJ_85_OBJ_85.setText("P\u00e9riode en cours");
              OBJ_85_OBJ_85.setName("OBJ_85_OBJ_85");
              panel2.add(OBJ_85_OBJ_85);
              OBJ_85_OBJ_85.setBounds(14, 62, 111, 24);

              //---- OBJ_89_OBJ_89 ----
              OBJ_89_OBJ_89.setText("Budget d\u00e9finitif");
              OBJ_89_OBJ_89.setName("OBJ_89_OBJ_89");
              panel2.add(OBJ_89_OBJ_89);
              OBJ_89_OBJ_89.setBounds(14, 92, 111, 24);

              //---- DGDE1X ----
              DGDE1X.setName("DGDE1X");
              panel2.add(DGDE1X);
              DGDE1X.setBounds(129, 30, 45, DGDE1X.getPreferredSize().height);

              //---- DGFE1X ----
              DGFE1X.setName("DGFE1X");
              panel2.add(DGFE1X);
              DGFE1X.setBounds(194, 30, 45, DGFE1X.getPreferredSize().height);

              //---- DGDP1X ----
              DGDP1X.setName("DGDP1X");
              panel2.add(DGDP1X);
              DGDP1X.setBounds(129, 60, 45, DGDP1X.getPreferredSize().height);

              //---- DGFP1X ----
              DGFP1X.setName("DGFP1X");
              panel2.add(DGFP1X);
              DGFP1X.setBounds(194, 60, 45, DGFP1X.getPreferredSize().height);

              //---- WVB1 ----
              WVB1.setName("WVB1");
              panel2.add(WVB1);
              WVB1.setBounds(129, 90, 45, WVB1.getPreferredSize().height);

              //---- OBJ_83_OBJ_83 ----
              OBJ_83_OBJ_83.setText("\u00e0");
              OBJ_83_OBJ_83.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_83_OBJ_83.setName("OBJ_83_OBJ_83");
              panel2.add(OBJ_83_OBJ_83);
              OBJ_83_OBJ_83.setBounds(175, 32, 19, 24);

              //---- OBJ_87_OBJ_87 ----
              OBJ_87_OBJ_87.setText("\u00e0");
              OBJ_87_OBJ_87.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_87_OBJ_87.setName("OBJ_87_OBJ_87");
              panel2.add(OBJ_87_OBJ_87);
              OBJ_87_OBJ_87.setBounds(175, 62, 19, 24);

              //---- label1 ----
              label1.setText("du mois de");
              label1.setName("label1");
              panel2.add(label1);
              label1.setBounds(14, 32, 111, 24);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel2);
            panel2.setBounds(20, 345, 275, 130);

            //======== panel5 ========
            {
              panel5.setBorder(new TitledBorder("Exercice suivant"));
              panel5.setOpaque(false);
              panel5.setName("panel5");
              panel5.setLayout(null);

              //---- OBJ_85_OBJ_86 ----
              OBJ_85_OBJ_86.setText("P\u00e9riode en cours");
              OBJ_85_OBJ_86.setName("OBJ_85_OBJ_86");
              panel5.add(OBJ_85_OBJ_86);
              OBJ_85_OBJ_86.setBounds(14, 65, 111, 19);

              //---- OBJ_89_OBJ_90 ----
              OBJ_89_OBJ_90.setText("Budget d\u00e9finitif");
              OBJ_89_OBJ_90.setName("OBJ_89_OBJ_90");
              panel5.add(OBJ_89_OBJ_90);
              OBJ_89_OBJ_90.setBounds(14, 95, 111, 19);

              //---- OBJ_83_OBJ_84 ----
              OBJ_83_OBJ_84.setText("\u00e0");
              OBJ_83_OBJ_84.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_83_OBJ_84.setName("OBJ_83_OBJ_84");
              panel5.add(OBJ_83_OBJ_84);
              OBJ_83_OBJ_84.setBounds(175, 34, 25, 20);

              //---- OBJ_87_OBJ_88 ----
              OBJ_87_OBJ_88.setText("\u00e0");
              OBJ_87_OBJ_88.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_87_OBJ_88.setName("OBJ_87_OBJ_88");
              panel5.add(OBJ_87_OBJ_88);
              OBJ_87_OBJ_88.setBounds(175, 65, 25, 19);

              //---- label3 ----
              label3.setText("du mois de");
              label3.setName("label3");
              panel5.add(label3);
              label3.setBounds(14, 35, 111, 19);

              //---- DGDE2X ----
              DGDE2X.setName("DGDE2X");
              panel5.add(DGDE2X);
              DGDE2X.setBounds(129, 30, 45, DGDE2X.getPreferredSize().height);

              //---- DGFE2X ----
              DGFE2X.setName("DGFE2X");
              panel5.add(DGFE2X);
              DGFE2X.setBounds(199, 30, 45, DGFE2X.getPreferredSize().height);

              //---- DGDP2X ----
              DGDP2X.setName("DGDP2X");
              panel5.add(DGDP2X);
              DGDP2X.setBounds(129, 60, 45, DGDP2X.getPreferredSize().height);

              //---- DGDF2X ----
              DGDF2X.setName("DGDF2X");
              panel5.add(DGDF2X);
              DGDF2X.setBounds(199, 60, 45, DGDF2X.getPreferredSize().height);

              //---- WVB2 ----
              WVB2.setName("WVB2");
              panel5.add(WVB2);
              WVB2.setBounds(129, 90, 45, WVB2.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel5.getComponentCount(); i++) {
                  Rectangle bounds = panel5.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel5.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel5.setMinimumSize(preferredSize);
                panel5.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel5);
            panel5.setBounds(430, 345, 275, 130);

            //---- OBJ_101_OBJ_101 ----
            OBJ_101_OBJ_101.setText("Nombre de mois d'avance en saisie");
            OBJ_101_OBJ_101.setName("OBJ_101_OBJ_101");
            xTitledPanel1ContentContainer.add(OBJ_101_OBJ_101);
            OBJ_101_OBJ_101.setBounds(20, 488, 219, 22);

            //---- DGMAS ----
            DGMAS.setName("DGMAS");
            xTitledPanel1ContentContainer.add(DGMAS);
            DGMAS.setBounds(265, 485, 30, DGMAS.getPreferredSize().height);

            //---- OBJ_103_OBJ_103 ----
            OBJ_103_OBJ_103.setText("Nombre d'exercices \u00e0 conserver");
            OBJ_103_OBJ_103.setName("OBJ_103_OBJ_103");
            xTitledPanel1ContentContainer.add(OBJ_103_OBJ_103);
            OBJ_103_OBJ_103.setBounds(430, 488, 240, 22);

            //---- DGNMH ----
            DGNMH.setName("DGNMH");
            xTitledPanel1ContentContainer.add(DGNMH);
            DGNMH.setBounds(685, 485, 20, DGNMH.getPreferredSize().height);

            //---- separator1 ----
            separator1.setName("separator1");
            xTitledPanel1ContentContainer.add(separator1);
            separator1.setBounds(5, 185, 720, separator1.getPreferredSize().height);

            //---- DGICS ----
            DGICS.setName("DGICS");
            xTitledPanel1ContentContainer.add(DGICS);
            DGICS.setBounds(561, 79, 144, DGICS.getPreferredSize().height);

            //---- OBJ_49 ----
            OBJ_49.setText("ICS");
            OBJ_49.setName("OBJ_49");
            xTitledPanel1ContentContainer.add(OBJ_49);
            OBJ_49.setBounds(520, 81, 35, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 754, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 552, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44_OBJ_45;
  private XRiTextField INDETB;
  private JLabel OBJ_45_OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_46_OBJ_47;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private XRiComboBox DGANA;
  private XRiComboBox DGNAT;
  private XRiTextField DGNOM;
  private XRiTextField DGAD1;
  private XRiTextField DVLIB;
  private JLabel OBJ_46_OBJ_46;
  private JLabel OBJ_50_OBJ_50;
  private JLabel OBJ_51_OBJ_51;
  private JLabel OBJ_48_OBJ_48;
  private JLabel OBJ_47_OBJ_47;
  private JLabel OBJ_49_OBJ_49;
  private JLabel OBJ_57_OBJ_57;
  private JLabel OBJ_66_OBJ_66;
  private XRiTextField DGBLOX;
  private XRiTextField DGETP;
  private XRiTextField DG445;
  private XRiTextField DGEUR;
  private XRiTextField DGCGE;
  private JPanel panel1;
  private XRiTextField DGAA1;
  private XRiTextField DGAA2;
  private XRiTextField DGAA3;
  private XRiTextField DGAA4;
  private XRiTextField DGAA5;
  private XRiTextField DGAA6;
  private XRiTextField DGDEV;
  private JPanel panel4;
  private JLabel OBJ_71_OBJ_71;
  private JLabel OBJ_73_OBJ_73;
  private JLabel OBJ_75_OBJ_75;
  private JLabel OBJ_78_OBJ_78;
  private XRiTextField DGDEBX;
  private XRiTextField DGFINX;
  private JLabel OBJ_77_OBJ_77;
  private XRiTextField DGMEX;
  private XRiTextField DGMPR;
  private JPanel panel2;
  private JLabel OBJ_85_OBJ_85;
  private JLabel OBJ_89_OBJ_89;
  private XRiTextField DGDE1X;
  private XRiTextField DGFE1X;
  private XRiTextField DGDP1X;
  private XRiTextField DGFP1X;
  private XRiTextField WVB1;
  private JLabel OBJ_83_OBJ_83;
  private JLabel OBJ_87_OBJ_87;
  private JLabel label1;
  private JPanel panel5;
  private JLabel OBJ_85_OBJ_86;
  private JLabel OBJ_89_OBJ_90;
  private JLabel OBJ_83_OBJ_84;
  private JLabel OBJ_87_OBJ_88;
  private JLabel label3;
  private XRiTextField DGDE2X;
  private XRiTextField DGFE2X;
  private XRiTextField DGDP2X;
  private XRiTextField DGDF2X;
  private XRiTextField WVB2;
  private JLabel OBJ_101_OBJ_101;
  private XRiTextField DGMAS;
  private JLabel OBJ_103_OBJ_103;
  private XRiTextField DGNMH;
  private JComponent separator1;
  private XRiTextField DGICS;
  private JLabel OBJ_49;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
