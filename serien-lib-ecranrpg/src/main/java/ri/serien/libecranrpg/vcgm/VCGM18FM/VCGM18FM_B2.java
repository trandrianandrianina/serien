
package ri.serien.libecranrpg.vcgm.VCGM18FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Emmanuel MARCQ
 */
public class VCGM18FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM18FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    TCT01.setValeursSelection("X", "");
    TCT02.setValeursSelection("X", "");
    TCT03.setValeursSelection("X", "");
    TCT04.setValeursSelection("X", "");
    TCT05.setValeursSelection("X", "");
    TCT06.setValeursSelection("X", "");
    TCT07.setValeursSelection("X", "");
    TCT08.setValeursSelection("X", "");
    TCT09.setValeursSelection("X", "");
    TCT10.setValeursSelection("X", "");
    TCT11.setValeursSelection("X", "");
    TCT12.setValeursSelection("X", "");
    TCT13.setValeursSelection("X", "");
    TCT14.setValeursSelection("X", "");
    TCT15.setValeursSelection("X", "");
    TCT16.setValeursSelection("X", "");
    TCT17.setValeursSelection("X", "");
    TCT18.setValeursSelection("X", "");
    TCT19.setValeursSelection("X", "");
    TCT20.setValeursSelection("X", "");
    TCT21.setValeursSelection("X", "");
    TCT22.setValeursSelection("X", "");
    TCT23.setValeursSelection("X", "");
    TCT24.setValeursSelection("X", "");
    TCT25.setValeursSelection("X", "");
    TCT26.setValeursSelection("X", "");
    TCT27.setValeursSelection("X", "");
    TCT28.setValeursSelection("X", "");
    TCT29.setValeursSelection("X", "");
    TCT30.setValeursSelection("X", "");
    TCT31.setValeursSelection("X", "");
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    JOLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@JOLIB@")).trim());
    TCT01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ01@ @TJJ01@ @TCL01@")).trim());
    TCT02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ02@ @TJJ02@ @TCL02@")).trim());
    TCT03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ03@ @TJJ03@ @TCL03@")).trim());
    TCT04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ04@ @TJJ04@ @TCL04@")).trim());
    TCT05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ05@ @TJJ05@ @TCL05@")).trim());
    TCT06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ06@ @TJJ06@ @TCL06@")).trim());
    TCT07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ07@ @TJJ07@ @TCL07@")).trim());
    TCT08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ08@ @TJJ08@ @TCL08@")).trim());
    TCT09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ09@ @TJJ09@ @TCL09@")).trim());
    TCT10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ10@ @TJJ10@ @TCL10@")).trim());
    TCT11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ11@ @TJJ11@ @TCL11@")).trim());
    TCT12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ12@ @TJJ12@ @TCL12@")).trim());
    TCT13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ13@ @TJJ13@ @TCL13@")).trim());
    TCT14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ14@ @TJJ14@ @TCL14@")).trim());
    TCT15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ15@ @TJJ15@ @TCL15@")).trim());
    TCT16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ16@ @TJJ16@ @TCL16@")).trim());
    TCT17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ17@ @TJJ17@ @TCL17@")).trim());
    TCT18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ18@ @TJJ18@ @TCL18@")).trim());
    TCT19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ19@ @TJJ19@ @TCL19@")).trim());
    TCT20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ20@ @TJJ20@ @TCL20@")).trim());
    TCT21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ21@ @TJJ21@ @TCL21@")).trim());
    TCT22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ22@ @TJJ22@ @TCL22@")).trim());
    TCT23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ23@ @TJJ23@ @TCL23@")).trim());
    TCT24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ24@ @TJJ24@ @TCL24@")).trim());
    TCT25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ25@ @TJJ25@ @TCL25@")).trim());
    TCT26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ26@ @TJJ26@ @TCL26@")).trim());
    TCT27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ27@ @TJJ27@ @TCL27@")).trim());
    TCT28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ28@ @TJJ28@ @TCL28@")).trim());
    TCT29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ29@ @TJJ29@ @TCL29@")).trim());
    TCT30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ30@ @TJJ30@ @TCL30@")).trim());
    TCT31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TLJ31@ @TJJ31@ @TCL31@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PERIOD@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    panel1.setVisible(lexique.isTrue("21"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    panel1 = new JPanel();
    OBJ_20 = new JXTitledSeparator();
    JOLIB = new RiZoneSortie();
    OBJ_24 = new JLabel();
    XPARJO = new XRiTextField();
    panel2 = new JPanel();
    TCT01 = new XRiCheckBox();
    TCT02 = new XRiCheckBox();
    TCT03 = new XRiCheckBox();
    TCT04 = new XRiCheckBox();
    TCT05 = new XRiCheckBox();
    TCT06 = new XRiCheckBox();
    TCT07 = new XRiCheckBox();
    TCT08 = new XRiCheckBox();
    TCT09 = new XRiCheckBox();
    TCT10 = new XRiCheckBox();
    TCT11 = new XRiCheckBox();
    TCT12 = new XRiCheckBox();
    TCT13 = new XRiCheckBox();
    TCT14 = new XRiCheckBox();
    TCT15 = new XRiCheckBox();
    TCT16 = new XRiCheckBox();
    TCT17 = new XRiCheckBox();
    TCT18 = new XRiCheckBox();
    TCT19 = new XRiCheckBox();
    TCT20 = new XRiCheckBox();
    TCT21 = new XRiCheckBox();
    TCT22 = new XRiCheckBox();
    TCT23 = new XRiCheckBox();
    TCT24 = new XRiCheckBox();
    TCT25 = new XRiCheckBox();
    TCT26 = new XRiCheckBox();
    TCT27 = new XRiCheckBox();
    TCT28 = new XRiCheckBox();
    TCT29 = new XRiCheckBox();
    TCT30 = new XRiCheckBox();
    TCT31 = new XRiCheckBox();
    label1 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(650, 570));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_20 ----
            OBJ_20.setTitle("");
            OBJ_20.setName("OBJ_20");
            panel1.add(OBJ_20);
            OBJ_20.setBounds(5, 10, 585, OBJ_20.getPreferredSize().height);

            //---- JOLIB ----
            JOLIB.setText("@JOLIB@");
            JOLIB.setName("JOLIB");
            panel1.add(JOLIB);
            JOLIB.setBounds(235, 30, 235, 28);

            //---- OBJ_24 ----
            OBJ_24.setText("Code journal de banque");
            OBJ_24.setName("OBJ_24");
            panel1.add(OBJ_24);
            OBJ_24.setBounds(20, 30, 155, 28);

            //---- XPARJO ----
            XPARJO.setComponentPopupMenu(BTD);
            XPARJO.setName("XPARJO");
            panel1.add(XPARJO);
            XPARJO.setBounds(175, 30, 34, XPARJO.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder(" "));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- TCT01 ----
            TCT01.setText("@TLJ01@ @TJJ01@ @TCL01@");
            TCT01.setName("TCT01");
            panel2.add(TCT01);
            TCT01.setBounds(20, 35, 150, 20);

            //---- TCT02 ----
            TCT02.setText("@TLJ02@ @TJJ02@ @TCL02@");
            TCT02.setName("TCT02");
            panel2.add(TCT02);
            TCT02.setBounds(20, 56, 150, 20);

            //---- TCT03 ----
            TCT03.setText("@TLJ03@ @TJJ03@ @TCL03@");
            TCT03.setName("TCT03");
            panel2.add(TCT03);
            TCT03.setBounds(20, 77, 150, 20);

            //---- TCT04 ----
            TCT04.setText("@TLJ04@ @TJJ04@ @TCL04@");
            TCT04.setName("TCT04");
            panel2.add(TCT04);
            TCT04.setBounds(20, 98, 150, 20);

            //---- TCT05 ----
            TCT05.setText("@TLJ05@ @TJJ05@ @TCL05@");
            TCT05.setName("TCT05");
            panel2.add(TCT05);
            TCT05.setBounds(20, 119, 150, 20);

            //---- TCT06 ----
            TCT06.setText("@TLJ06@ @TJJ06@ @TCL06@");
            TCT06.setName("TCT06");
            panel2.add(TCT06);
            TCT06.setBounds(20, 140, 150, 20);

            //---- TCT07 ----
            TCT07.setText("@TLJ07@ @TJJ07@ @TCL07@");
            TCT07.setName("TCT07");
            panel2.add(TCT07);
            TCT07.setBounds(20, 161, 150, 20);

            //---- TCT08 ----
            TCT08.setText("@TLJ08@ @TJJ08@ @TCL08@");
            TCT08.setName("TCT08");
            panel2.add(TCT08);
            TCT08.setBounds(20, 182, 150, 20);

            //---- TCT09 ----
            TCT09.setText("@TLJ09@ @TJJ09@ @TCL09@");
            TCT09.setName("TCT09");
            panel2.add(TCT09);
            TCT09.setBounds(20, 203, 150, 20);

            //---- TCT10 ----
            TCT10.setText("@TLJ10@ @TJJ10@ @TCL10@");
            TCT10.setName("TCT10");
            panel2.add(TCT10);
            TCT10.setBounds(20, 224, 150, 20);

            //---- TCT11 ----
            TCT11.setText("@TLJ11@ @TJJ11@ @TCL11@");
            TCT11.setName("TCT11");
            panel2.add(TCT11);
            TCT11.setBounds(20, 245, 150, 20);

            //---- TCT12 ----
            TCT12.setText("@TLJ12@ @TJJ12@ @TCL12@");
            TCT12.setName("TCT12");
            panel2.add(TCT12);
            TCT12.setBounds(20, 266, 150, 20);

            //---- TCT13 ----
            TCT13.setText("@TLJ13@ @TJJ13@ @TCL13@");
            TCT13.setName("TCT13");
            panel2.add(TCT13);
            TCT13.setBounds(20, 287, 150, 20);

            //---- TCT14 ----
            TCT14.setText("@TLJ14@ @TJJ14@ @TCL14@");
            TCT14.setName("TCT14");
            panel2.add(TCT14);
            TCT14.setBounds(20, 308, 150, 20);

            //---- TCT15 ----
            TCT15.setText("@TLJ15@ @TJJ15@ @TCL15@");
            TCT15.setName("TCT15");
            panel2.add(TCT15);
            TCT15.setBounds(20, 329, 150, 20);

            //---- TCT16 ----
            TCT16.setText("@TLJ16@ @TJJ16@ @TCL16@");
            TCT16.setName("TCT16");
            panel2.add(TCT16);
            TCT16.setBounds(212, 35, 150, 20);

            //---- TCT17 ----
            TCT17.setText("@TLJ17@ @TJJ17@ @TCL17@");
            TCT17.setName("TCT17");
            panel2.add(TCT17);
            TCT17.setBounds(212, 56, 150, 20);

            //---- TCT18 ----
            TCT18.setText("@TLJ18@ @TJJ18@ @TCL18@");
            TCT18.setName("TCT18");
            panel2.add(TCT18);
            TCT18.setBounds(212, 77, 150, 20);

            //---- TCT19 ----
            TCT19.setText("@TLJ19@ @TJJ19@ @TCL19@");
            TCT19.setName("TCT19");
            panel2.add(TCT19);
            TCT19.setBounds(212, 98, 150, 20);

            //---- TCT20 ----
            TCT20.setText("@TLJ20@ @TJJ20@ @TCL20@");
            TCT20.setName("TCT20");
            panel2.add(TCT20);
            TCT20.setBounds(212, 119, 150, 20);

            //---- TCT21 ----
            TCT21.setText("@TLJ21@ @TJJ21@ @TCL21@");
            TCT21.setName("TCT21");
            panel2.add(TCT21);
            TCT21.setBounds(212, 140, 150, 20);

            //---- TCT22 ----
            TCT22.setText("@TLJ22@ @TJJ22@ @TCL22@");
            TCT22.setName("TCT22");
            panel2.add(TCT22);
            TCT22.setBounds(212, 161, 150, 20);

            //---- TCT23 ----
            TCT23.setText("@TLJ23@ @TJJ23@ @TCL23@");
            TCT23.setName("TCT23");
            panel2.add(TCT23);
            TCT23.setBounds(212, 182, 150, 20);

            //---- TCT24 ----
            TCT24.setText("@TLJ24@ @TJJ24@ @TCL24@");
            TCT24.setName("TCT24");
            panel2.add(TCT24);
            TCT24.setBounds(212, 203, 150, 20);

            //---- TCT25 ----
            TCT25.setText("@TLJ25@ @TJJ25@ @TCL25@");
            TCT25.setName("TCT25");
            panel2.add(TCT25);
            TCT25.setBounds(212, 224, 150, 20);

            //---- TCT26 ----
            TCT26.setText("@TLJ26@ @TJJ26@ @TCL26@");
            TCT26.setName("TCT26");
            panel2.add(TCT26);
            TCT26.setBounds(212, 245, 150, 20);

            //---- TCT27 ----
            TCT27.setText("@TLJ27@ @TJJ27@ @TCL27@");
            TCT27.setName("TCT27");
            panel2.add(TCT27);
            TCT27.setBounds(212, 266, 150, 20);

            //---- TCT28 ----
            TCT28.setText("@TLJ28@ @TJJ28@ @TCL28@");
            TCT28.setName("TCT28");
            panel2.add(TCT28);
            TCT28.setBounds(212, 287, 150, 20);

            //---- TCT29 ----
            TCT29.setText("@TLJ29@ @TJJ29@ @TCL29@");
            TCT29.setName("TCT29");
            panel2.add(TCT29);
            TCT29.setBounds(212, 308, 150, 20);

            //---- TCT30 ----
            TCT30.setText("@TLJ30@ @TJJ30@ @TCL30@");
            TCT30.setName("TCT30");
            panel2.add(TCT30);
            TCT30.setBounds(212, 329, 150, 20);

            //---- TCT31 ----
            TCT31.setText("@TLJ31@ @TJJ31@ @TCL31@");
            TCT31.setName("TCT31");
            panel2.add(TCT31);
            TCT31.setBounds(404, 35, 150, 20);

            //---- label1 ----
            label1.setText("@PERIOD@");
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD, label1.getFont().getSize() + 2f));
            label1.setName("label1");
            panel2.add(label1);
            label1.setBounds(15, 0, 440, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 580, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 600, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 580, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGap(12, 12, 12)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 365, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JPanel panel1;
  private JXTitledSeparator OBJ_20;
  private RiZoneSortie JOLIB;
  private JLabel OBJ_24;
  private XRiTextField XPARJO;
  private JPanel panel2;
  private XRiCheckBox TCT01;
  private XRiCheckBox TCT02;
  private XRiCheckBox TCT03;
  private XRiCheckBox TCT04;
  private XRiCheckBox TCT05;
  private XRiCheckBox TCT06;
  private XRiCheckBox TCT07;
  private XRiCheckBox TCT08;
  private XRiCheckBox TCT09;
  private XRiCheckBox TCT10;
  private XRiCheckBox TCT11;
  private XRiCheckBox TCT12;
  private XRiCheckBox TCT13;
  private XRiCheckBox TCT14;
  private XRiCheckBox TCT15;
  private XRiCheckBox TCT16;
  private XRiCheckBox TCT17;
  private XRiCheckBox TCT18;
  private XRiCheckBox TCT19;
  private XRiCheckBox TCT20;
  private XRiCheckBox TCT21;
  private XRiCheckBox TCT22;
  private XRiCheckBox TCT23;
  private XRiCheckBox TCT24;
  private XRiCheckBox TCT25;
  private XRiCheckBox TCT26;
  private XRiCheckBox TCT27;
  private XRiCheckBox TCT28;
  private XRiCheckBox TCT29;
  private XRiCheckBox TCT30;
  private XRiCheckBox TCT31;
  private JLabel label1;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
