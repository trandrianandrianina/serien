
package ri.serien.libecranrpg.vcgm.VCGM01AF;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM01AF_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", "WTP16", "WTP17", "WTP18", "WTP19", "WTP20", "WTP21", "WTP22", "WTP23", "WTP24", "WTP25", "WTP26",
      "WTP27", "WTP28", "WTP29", "WTP30", };
  private String[] _WTP01_Title = { "Libellé", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, { "LD16", }, { "LD17", }, { "LD18", },
      { "LD19", }, { "LD20", }, { "LD21", }, { "LD22", }, { "LD23", }, { "LD24", }, { "LD25", }, { "LD26", }, { "LD27", }, { "LD28", },
      { "LD29", }, { "LD30", }, };
  private int[] _WTP01_Width = { 519, };
  
  public VCGM01AF_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    // Ajoute à la liste des oData les variables non liées directement à un composant graphique
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _WTP01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    OBJ_71.setVisible(lexique.HostFieldGetData("LD07").contains("?"));
    OBJ_79.setVisible(lexique.HostFieldGetData("LD15").contains("?"));
    OBJ_77.setVisible(lexique.HostFieldGetData("LD13").contains("?"));
    OBJ_76.setVisible(lexique.HostFieldGetData("LD12").contains("?"));
    OBJ_74.setVisible(lexique.HostFieldGetData("LD10").contains("?"));
    OBJ_73.setVisible(lexique.HostFieldGetData("LD09").contains("?"));
    OBJ_72.setVisible(lexique.HostFieldGetData("LD08").contains("?"));
    OBJ_69.setVisible(lexique.HostFieldGetData("LD06").contains("?"));
    OBJ_68.setVisible(lexique.HostFieldGetData("LD05").contains("?"));
    OBJ_67.setVisible(lexique.HostFieldGetData("LD04").contains("?"));
    OBJ_66.setVisible(lexique.HostFieldGetData("LD03").contains("?"));
    OBJ_65.setVisible(lexique.HostFieldGetData("LD02").contains("?"));
    OBJ_64.setVisible(lexique.HostFieldGetData("LD01").contains("?"));
    OBJ_78.setVisible(lexique.HostFieldGetData("LD14").contains("?"));
    OBJ_75.setVisible(lexique.HostFieldGetData("LD11").contains("?"));
    OBJ_70.setVisible(lexique.HostFieldGetData("LD16").contains("?"));
    OBJ_80.setVisible(lexique.HostFieldGetData("LD26").contains("?"));
    OBJ_81.setVisible(lexique.HostFieldGetData("LD29").contains("?"));
    OBJ_82.setVisible(lexique.HostFieldGetData("LD17").contains("?"));
    OBJ_83.setVisible(lexique.HostFieldGetData("LD18").contains("?"));
    OBJ_84.setVisible(lexique.HostFieldGetData("LD19").contains("?"));
    OBJ_85.setVisible(lexique.HostFieldGetData("LD20").contains("?"));
    OBJ_86.setVisible(lexique.HostFieldGetData("LD21").contains("?"));
    OBJ_87.setVisible(lexique.HostFieldGetData("LD23").contains("?"));
    OBJ_88.setVisible(lexique.HostFieldGetData("LD24").contains("?"));
    OBJ_89.setVisible(lexique.HostFieldGetData("LD25").contains("?"));
    OBJ_90.setVisible(lexique.HostFieldGetData("LD27").contains("?"));
    OBJ_91.setVisible(lexique.HostFieldGetData("LD28").contains("?"));
    OBJ_92.setVisible(lexique.HostFieldGetData("LD30").contains("?"));
    OBJ_93.setVisible(lexique.HostFieldGetData("LD22").contains("?"));
    
    

    
    p_bpresentation.setCodeEtablissement(WETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WETB.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "1", "Enter");
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    WTP01.setValeurTop("?");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "E", "Enter");
    WTP01.setValeurTop("E");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD2.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD2.getInvoker().getName());
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _WTP01_Top, "1", "Enter", e);
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void OBJ_75ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP11", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_78ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP14", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_64ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP01", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_65ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP02", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_66ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP03", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_67ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP04", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_68ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP05", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_69ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP06", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_72ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP08", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_73ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP09", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_74ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP10", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_76ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP12", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_77ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP13", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_79ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP15", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_71ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP07", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_80ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP26", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_81ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP29", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_70ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP16", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_82ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP17", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_83ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP18", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_84ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP19", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_85ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP20", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_86ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP21", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_87ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP23", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_88ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP24", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_89ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP25", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_90ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP27", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_91ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP28", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_92ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP30", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_93ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP22", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_33_OBJ_33 = new JLabel();
    WETB = new XRiTextField();
    OBJ_34_OBJ_34 = new JLabel();
    WPAR = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    P_Centre = new JPanel();
    SCROLLPANE_LIST2 = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    panel1 = new JPanel();
    OBJ_75 = new SNBoutonDetail();
    OBJ_78 = new SNBoutonDetail();
    OBJ_64 = new SNBoutonDetail();
    OBJ_65 = new SNBoutonDetail();
    OBJ_66 = new SNBoutonDetail();
    OBJ_67 = new SNBoutonDetail();
    OBJ_68 = new SNBoutonDetail();
    OBJ_69 = new SNBoutonDetail();
    OBJ_72 = new SNBoutonDetail();
    OBJ_73 = new SNBoutonDetail();
    OBJ_74 = new SNBoutonDetail();
    OBJ_76 = new SNBoutonDetail();
    OBJ_77 = new SNBoutonDetail();
    OBJ_79 = new SNBoutonDetail();
    OBJ_71 = new SNBoutonDetail();
    OBJ_80 = new SNBoutonDetail();
    OBJ_81 = new SNBoutonDetail();
    OBJ_70 = new SNBoutonDetail();
    OBJ_82 = new SNBoutonDetail();
    OBJ_83 = new SNBoutonDetail();
    OBJ_84 = new SNBoutonDetail();
    OBJ_85 = new SNBoutonDetail();
    OBJ_86 = new SNBoutonDetail();
    OBJ_87 = new SNBoutonDetail();
    OBJ_88 = new SNBoutonDetail();
    OBJ_89 = new SNBoutonDetail();
    OBJ_90 = new SNBoutonDetail();
    OBJ_91 = new SNBoutonDetail();
    OBJ_92 = new SNBoutonDetail();
    OBJ_93 = new SNBoutonDetail();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    BTD2 = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_18 = new JMenuItem();
    OBJ_25 = new JMenuItem();
    OBJ_24 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_33_OBJ_33 ----
          OBJ_33_OBJ_33.setText("Soci\u00e9t\u00e9");
          OBJ_33_OBJ_33.setName("OBJ_33_OBJ_33");

          //---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setName("WETB");

          //---- OBJ_34_OBJ_34 ----
          OBJ_34_OBJ_34.setText("Code");
          OBJ_34_OBJ_34.setName("OBJ_34_OBJ_34");

          //---- WPAR ----
          WPAR.setComponentPopupMenu(BTD);
          WPAR.setName("WPAR");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_33_OBJ_33, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_34_OBJ_34, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(WPAR, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(WETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WPAR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_33_OBJ_33, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_34_OBJ_34, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(830, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== P_Centre ========
          {
            P_Centre.setBorder(new TitledBorder(""));
            P_Centre.setOpaque(false);
            P_Centre.setName("P_Centre");
            P_Centre.setLayout(null);

            //======== SCROLLPANE_LIST2 ========
            {
              SCROLLPANE_LIST2.setComponentPopupMenu(BTD2);
              SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD2);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST2.setViewportView(WTP01);
            }
            P_Centre.add(SCROLLPANE_LIST2);
            SCROLLPANE_LIST2.setBounds(50, 25, 685, 510);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            P_Centre.add(BT_PGUP);
            BT_PGUP.setBounds(750, 25, 25, 230);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            P_Centre.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(750, 305, 25, 230);

            //======== panel1 ========
            {
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- OBJ_75 ----
              OBJ_75.setText("");
              OBJ_75.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_75.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_75.setName("OBJ_75");
              OBJ_75.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_75ActionPerformed(e);
                }
              });
              panel1.add(OBJ_75);
              OBJ_75.setBounds(5, 175, 15, 15);

              //---- OBJ_78 ----
              OBJ_78.setText("");
              OBJ_78.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_78.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_78.setName("OBJ_78");
              OBJ_78.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_78ActionPerformed(e);
                }
              });
              panel1.add(OBJ_78);
              OBJ_78.setBounds(5, 223, 15, 15);

              //---- OBJ_64 ----
              OBJ_64.setText("");
              OBJ_64.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_64.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_64.setName("OBJ_64");
              OBJ_64.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_64ActionPerformed(e);
                }
              });
              panel1.add(OBJ_64);
              OBJ_64.setBounds(5, 15, 15, 15);

              //---- OBJ_65 ----
              OBJ_65.setText("");
              OBJ_65.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_65.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_65.setName("OBJ_65");
              OBJ_65.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_65ActionPerformed(e);
                }
              });
              panel1.add(OBJ_65);
              OBJ_65.setBounds(5, 31, 15, 15);

              //---- OBJ_66 ----
              OBJ_66.setText("");
              OBJ_66.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_66.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_66.setName("OBJ_66");
              OBJ_66.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_66ActionPerformed(e);
                }
              });
              panel1.add(OBJ_66);
              OBJ_66.setBounds(5, 47, 15, 15);

              //---- OBJ_67 ----
              OBJ_67.setText("");
              OBJ_67.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_67.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_67.setName("OBJ_67");
              OBJ_67.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_67ActionPerformed(e);
                }
              });
              panel1.add(OBJ_67);
              OBJ_67.setBounds(5, 63, 15, 15);

              //---- OBJ_68 ----
              OBJ_68.setText("");
              OBJ_68.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_68.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_68.setName("OBJ_68");
              OBJ_68.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_68ActionPerformed(e);
                }
              });
              panel1.add(OBJ_68);
              OBJ_68.setBounds(5, 79, 15, 15);

              //---- OBJ_69 ----
              OBJ_69.setText("");
              OBJ_69.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_69.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_69.setName("OBJ_69");
              OBJ_69.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_69ActionPerformed(e);
                }
              });
              panel1.add(OBJ_69);
              OBJ_69.setBounds(5, 95, 15, 15);

              //---- OBJ_72 ----
              OBJ_72.setText("");
              OBJ_72.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_72.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_72.setName("OBJ_72");
              OBJ_72.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_72ActionPerformed(e);
                }
              });
              panel1.add(OBJ_72);
              OBJ_72.setBounds(5, 127, 15, 15);

              //---- OBJ_73 ----
              OBJ_73.setText("");
              OBJ_73.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_73.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_73.setName("OBJ_73");
              OBJ_73.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_73ActionPerformed(e);
                }
              });
              panel1.add(OBJ_73);
              OBJ_73.setBounds(5, 143, 15, 15);

              //---- OBJ_74 ----
              OBJ_74.setText("");
              OBJ_74.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_74.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_74.setName("OBJ_74");
              OBJ_74.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_74ActionPerformed(e);
                }
              });
              panel1.add(OBJ_74);
              OBJ_74.setBounds(5, 159, 15, 15);

              //---- OBJ_76 ----
              OBJ_76.setText("");
              OBJ_76.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_76.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_76.setName("OBJ_76");
              OBJ_76.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_76ActionPerformed(e);
                }
              });
              panel1.add(OBJ_76);
              OBJ_76.setBounds(5, 191, 15, 15);

              //---- OBJ_77 ----
              OBJ_77.setText("");
              OBJ_77.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_77.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_77.setName("OBJ_77");
              OBJ_77.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_77ActionPerformed(e);
                }
              });
              panel1.add(OBJ_77);
              OBJ_77.setBounds(5, 207, 15, 15);

              //---- OBJ_79 ----
              OBJ_79.setText("");
              OBJ_79.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_79.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_79.setName("OBJ_79");
              OBJ_79.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_79ActionPerformed(e);
                }
              });
              panel1.add(OBJ_79);
              OBJ_79.setBounds(5, 239, 15, 15);

              //---- OBJ_71 ----
              OBJ_71.setText("");
              OBJ_71.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_71.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_71.setName("OBJ_71");
              OBJ_71.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_71ActionPerformed(e);
                }
              });
              panel1.add(OBJ_71);
              OBJ_71.setBounds(5, 111, 15, 15);

              //---- OBJ_80 ----
              OBJ_80.setText("");
              OBJ_80.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_80.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_80.setName("OBJ_80");
              OBJ_80.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_80ActionPerformed(e);
                }
              });
              panel1.add(OBJ_80);
              OBJ_80.setBounds(5, 415, 15, 15);

              //---- OBJ_81 ----
              OBJ_81.setText("");
              OBJ_81.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_81.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_81.setName("OBJ_81");
              OBJ_81.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_81ActionPerformed(e);
                }
              });
              panel1.add(OBJ_81);
              OBJ_81.setBounds(5, 463, 15, 15);

              //---- OBJ_70 ----
              OBJ_70.setText("");
              OBJ_70.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_70.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_70.setName("OBJ_70");
              OBJ_70.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_70ActionPerformed(e);
                }
              });
              panel1.add(OBJ_70);
              OBJ_70.setBounds(5, 255, 15, 15);

              //---- OBJ_82 ----
              OBJ_82.setText("");
              OBJ_82.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_82.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_82.setName("OBJ_82");
              OBJ_82.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_82ActionPerformed(e);
                }
              });
              panel1.add(OBJ_82);
              OBJ_82.setBounds(5, 271, 15, 15);

              //---- OBJ_83 ----
              OBJ_83.setText("");
              OBJ_83.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_83.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_83.setName("OBJ_83");
              OBJ_83.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_83ActionPerformed(e);
                }
              });
              panel1.add(OBJ_83);
              OBJ_83.setBounds(5, 287, 15, 15);

              //---- OBJ_84 ----
              OBJ_84.setText("");
              OBJ_84.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_84.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_84.setName("OBJ_84");
              OBJ_84.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_84ActionPerformed(e);
                }
              });
              panel1.add(OBJ_84);
              OBJ_84.setBounds(5, 303, 15, 15);

              //---- OBJ_85 ----
              OBJ_85.setText("");
              OBJ_85.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_85.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_85.setName("OBJ_85");
              OBJ_85.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_85ActionPerformed(e);
                }
              });
              panel1.add(OBJ_85);
              OBJ_85.setBounds(5, 319, 15, 15);

              //---- OBJ_86 ----
              OBJ_86.setText("");
              OBJ_86.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_86.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_86.setName("OBJ_86");
              OBJ_86.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_86ActionPerformed(e);
                }
              });
              panel1.add(OBJ_86);
              OBJ_86.setBounds(5, 335, 15, 15);

              //---- OBJ_87 ----
              OBJ_87.setText("");
              OBJ_87.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_87.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_87.setName("OBJ_87");
              OBJ_87.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_87ActionPerformed(e);
                }
              });
              panel1.add(OBJ_87);
              OBJ_87.setBounds(5, 367, 15, 15);

              //---- OBJ_88 ----
              OBJ_88.setText("");
              OBJ_88.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_88.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_88.setName("OBJ_88");
              OBJ_88.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_88ActionPerformed(e);
                }
              });
              panel1.add(OBJ_88);
              OBJ_88.setBounds(5, 383, 15, 15);

              //---- OBJ_89 ----
              OBJ_89.setText("");
              OBJ_89.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_89.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_89.setName("OBJ_89");
              OBJ_89.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_89ActionPerformed(e);
                }
              });
              panel1.add(OBJ_89);
              OBJ_89.setBounds(5, 399, 15, 15);

              //---- OBJ_90 ----
              OBJ_90.setText("");
              OBJ_90.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_90.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_90.setName("OBJ_90");
              OBJ_90.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_90ActionPerformed(e);
                }
              });
              panel1.add(OBJ_90);
              OBJ_90.setBounds(5, 431, 15, 15);

              //---- OBJ_91 ----
              OBJ_91.setText("");
              OBJ_91.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_91.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_91.setName("OBJ_91");
              OBJ_91.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_91ActionPerformed(e);
                }
              });
              panel1.add(OBJ_91);
              OBJ_91.setBounds(5, 447, 15, 15);

              //---- OBJ_92 ----
              OBJ_92.setText("");
              OBJ_92.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_92.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_92.setName("OBJ_92");
              OBJ_92.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_92ActionPerformed(e);
                }
              });
              panel1.add(OBJ_92);
              OBJ_92.setBounds(5, 479, 15, 15);

              //---- OBJ_93 ----
              OBJ_93.setText("");
              OBJ_93.setToolTipText("<HTML>Des informations sont<BR>pr\u00e9sentes pour ce param\u00e8tre</HTML>");
              OBJ_93.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_93.setName("OBJ_93");
              OBJ_93.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_93ActionPerformed(e);
                }
              });
              panel1.add(OBJ_93);
              OBJ_93.setBounds(5, 351, 15, 15);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            P_Centre.add(panel1);
            panel1.setBounds(20, 35, 28, 500);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < P_Centre.getComponentCount(); i++) {
                Rectangle bounds = P_Centre.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = P_Centre.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              P_Centre.setMinimumSize(preferredSize);
              P_Centre.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(P_Centre, GroupLayout.DEFAULT_SIZE, 804, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(P_Centre, GroupLayout.DEFAULT_SIZE, 552, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Aide en ligne");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("Choix possibles");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);
    }

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD2.add(CHOISIR);

      //---- OBJ_18 ----
      OBJ_18.setText("Edition");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_18);
      BTD2.addSeparator();

      //---- OBJ_25 ----
      OBJ_25.setText("Choix possibles");
      OBJ_25.setName("OBJ_25");
      OBJ_25.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_25ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_25);

      //---- OBJ_24 ----
      OBJ_24.setText("Aide en ligne");
      OBJ_24.setName("OBJ_24");
      OBJ_24.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_24ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_24);
    }

    //---- OBJ_17 ----
    OBJ_17.setText("Informations");
    OBJ_17.setName("OBJ_17");
    OBJ_17.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_17ActionPerformed(e);
      }
    });
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_33_OBJ_33;
  private XRiTextField WETB;
  private JLabel OBJ_34_OBJ_34;
  private XRiTextField WPAR;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel P_Centre;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPanel panel1;
  private SNBoutonDetail OBJ_75;
  private SNBoutonDetail OBJ_78;
  private SNBoutonDetail OBJ_64;
  private SNBoutonDetail OBJ_65;
  private SNBoutonDetail OBJ_66;
  private SNBoutonDetail OBJ_67;
  private SNBoutonDetail OBJ_68;
  private SNBoutonDetail OBJ_69;
  private SNBoutonDetail OBJ_72;
  private SNBoutonDetail OBJ_73;
  private SNBoutonDetail OBJ_74;
  private SNBoutonDetail OBJ_76;
  private SNBoutonDetail OBJ_77;
  private SNBoutonDetail OBJ_79;
  private SNBoutonDetail OBJ_71;
  private SNBoutonDetail OBJ_80;
  private SNBoutonDetail OBJ_81;
  private SNBoutonDetail OBJ_70;
  private SNBoutonDetail OBJ_82;
  private SNBoutonDetail OBJ_83;
  private SNBoutonDetail OBJ_84;
  private SNBoutonDetail OBJ_85;
  private SNBoutonDetail OBJ_86;
  private SNBoutonDetail OBJ_87;
  private SNBoutonDetail OBJ_88;
  private SNBoutonDetail OBJ_89;
  private SNBoutonDetail OBJ_90;
  private SNBoutonDetail OBJ_91;
  private SNBoutonDetail OBJ_92;
  private SNBoutonDetail OBJ_93;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JPopupMenu BTD2;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_25;
  private JMenuItem OBJ_24;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
