
package ri.serien.libecranrpg.vcgm.VCGM01FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM01FM_DP extends SNPanelEcranRPG implements ioFrame {
  
  
  public VCGM01FM_DP(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44_OBJ_45 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_45_OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46_OBJ_47 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_55_OBJ_55 = new JLabel();
    DPT01 = new XRiTextField();
    OBJ_55_OBJ_57 = new JLabel();
    OBJ_55_OBJ_58 = new JLabel();
    OBJ_55_OBJ_59 = new JLabel();
    OBJ_55_OBJ_60 = new JLabel();
    OBJ_55_OBJ_61 = new JLabel();
    OBJ_55_OBJ_62 = new JLabel();
    OBJ_55_OBJ_63 = new JLabel();
    OBJ_55_OBJ_64 = new JLabel();
    OBJ_55_OBJ_65 = new JLabel();
    OBJ_55_OBJ_56 = new JLabel();
    OBJ_55_OBJ_66 = new JLabel();
    DPT02 = new XRiTextField();
    DPT03 = new XRiTextField();
    DPT04 = new XRiTextField();
    DPT05 = new XRiTextField();
    DPT06 = new XRiTextField();
    DPT07 = new XRiTextField();
    DPT08 = new XRiTextField();
    DPT09 = new XRiTextField();
    NBJDP = new XRiTextField();
    DPC01 = new XRiTextField();
    DPC02 = new XRiTextField();
    DPC03 = new XRiTextField();
    DPC04 = new XRiTextField();
    DPC05 = new XRiTextField();
    DPC06 = new XRiTextField();
    DPC07 = new XRiTextField();
    DPC08 = new XRiTextField();
    DPC09 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44_OBJ_45 ----
          OBJ_44_OBJ_45.setText("Soci\u00e9t\u00e9");
          OBJ_44_OBJ_45.setName("OBJ_44_OBJ_45");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setName("INDETB");

          //---- OBJ_45_OBJ_46 ----
          OBJ_45_OBJ_46.setText("Code");
          OBJ_45_OBJ_46.setName("OBJ_45_OBJ_46");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setName("INDTYP");

          //---- OBJ_46_OBJ_47 ----
          OBJ_46_OBJ_47.setText("Ordre");
          OBJ_46_OBJ_47.setName("OBJ_46_OBJ_47");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 550));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(900, 550));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Taux de d\u00e9pr\u00e9cation des cr\u00e9ances");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- OBJ_55_OBJ_55 ----
            OBJ_55_OBJ_55.setText("Client terme");
            OBJ_55_OBJ_55.setName("OBJ_55_OBJ_55");
            xTitledPanel1ContentContainer.add(OBJ_55_OBJ_55);
            OBJ_55_OBJ_55.setBounds(350, 10, 125, 20);

            //---- DPT01 ----
            DPT01.setComponentPopupMenu(BTD);
            DPT01.setName("DPT01");
            xTitledPanel1ContentContainer.add(DPT01);
            DPT01.setBounds(360, 41, 45, DPT01.getPreferredSize().height);

            //---- OBJ_55_OBJ_57 ----
            OBJ_55_OBJ_57.setText("Provisions sur factures non \u00e9chues");
            OBJ_55_OBJ_57.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_55_OBJ_57.setName("OBJ_55_OBJ_57");
            xTitledPanel1ContentContainer.add(OBJ_55_OBJ_57);
            OBJ_55_OBJ_57.setBounds(45, 83, 260, 20);

            //---- OBJ_55_OBJ_58 ----
            OBJ_55_OBJ_58.setText("Provisions sur effets non encaiss\u00e9s");
            OBJ_55_OBJ_58.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_55_OBJ_58.setName("OBJ_55_OBJ_58");
            xTitledPanel1ContentContainer.add(OBJ_55_OBJ_58);
            OBJ_55_OBJ_58.setBounds(45, 121, 260, 20);

            //---- OBJ_55_OBJ_59 ----
            OBJ_55_OBJ_59.setText("Provisions sur retard  entre 0 et 30 jours");
            OBJ_55_OBJ_59.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_55_OBJ_59.setName("OBJ_55_OBJ_59");
            xTitledPanel1ContentContainer.add(OBJ_55_OBJ_59);
            OBJ_55_OBJ_59.setBounds(45, 159, 260, 20);

            //---- OBJ_55_OBJ_60 ----
            OBJ_55_OBJ_60.setText("Provisions sur retard  entre 31 et 60 jours");
            OBJ_55_OBJ_60.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_55_OBJ_60.setName("OBJ_55_OBJ_60");
            xTitledPanel1ContentContainer.add(OBJ_55_OBJ_60);
            OBJ_55_OBJ_60.setBounds(45, 197, 260, 20);

            //---- OBJ_55_OBJ_61 ----
            OBJ_55_OBJ_61.setText("Provisions sur retard  entre 61 et 75 jours");
            OBJ_55_OBJ_61.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_55_OBJ_61.setName("OBJ_55_OBJ_61");
            xTitledPanel1ContentContainer.add(OBJ_55_OBJ_61);
            OBJ_55_OBJ_61.setBounds(45, 235, 260, 20);

            //---- OBJ_55_OBJ_62 ----
            OBJ_55_OBJ_62.setText("Provisions sur retard  entre 76 et 90 jours");
            OBJ_55_OBJ_62.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_55_OBJ_62.setName("OBJ_55_OBJ_62");
            xTitledPanel1ContentContainer.add(OBJ_55_OBJ_62);
            OBJ_55_OBJ_62.setBounds(45, 273, 260, 20);

            //---- OBJ_55_OBJ_63 ----
            OBJ_55_OBJ_63.setText("Provisions sur retard  entre 91 et 120 jours");
            OBJ_55_OBJ_63.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_55_OBJ_63.setName("OBJ_55_OBJ_63");
            xTitledPanel1ContentContainer.add(OBJ_55_OBJ_63);
            OBJ_55_OBJ_63.setBounds(45, 311, 260, 20);

            //---- OBJ_55_OBJ_64 ----
            OBJ_55_OBJ_64.setText("Provisions sur retard  sup\u00e9rieur \u00e0 120 jours");
            OBJ_55_OBJ_64.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_55_OBJ_64.setName("OBJ_55_OBJ_64");
            xTitledPanel1ContentContainer.add(OBJ_55_OBJ_64);
            OBJ_55_OBJ_64.setBounds(45, 349, 260, 20);

            //---- OBJ_55_OBJ_65 ----
            OBJ_55_OBJ_65.setText("Nombre de jours d\u00e9lcaratifs");
            OBJ_55_OBJ_65.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_55_OBJ_65.setName("OBJ_55_OBJ_65");
            xTitledPanel1ContentContainer.add(OBJ_55_OBJ_65);
            OBJ_55_OBJ_65.setBounds(45, 419, 260, 20);

            //---- OBJ_55_OBJ_56 ----
            OBJ_55_OBJ_56.setText("Provisions sur factures \u00e0 \u00e9tablir");
            OBJ_55_OBJ_56.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_55_OBJ_56.setName("OBJ_55_OBJ_56");
            xTitledPanel1ContentContainer.add(OBJ_55_OBJ_56);
            OBJ_55_OBJ_56.setBounds(45, 45, 260, 20);

            //---- OBJ_55_OBJ_66 ----
            OBJ_55_OBJ_66.setText("Client comptant");
            OBJ_55_OBJ_66.setName("OBJ_55_OBJ_66");
            xTitledPanel1ContentContainer.add(OBJ_55_OBJ_66);
            OBJ_55_OBJ_66.setBounds(595, 10, 125, 20);

            //---- DPT02 ----
            DPT02.setComponentPopupMenu(BTD);
            DPT02.setName("DPT02");
            xTitledPanel1ContentContainer.add(DPT02);
            DPT02.setBounds(360, 79, 45, DPT02.getPreferredSize().height);

            //---- DPT03 ----
            DPT03.setComponentPopupMenu(BTD);
            DPT03.setName("DPT03");
            xTitledPanel1ContentContainer.add(DPT03);
            DPT03.setBounds(360, 117, 45, DPT03.getPreferredSize().height);

            //---- DPT04 ----
            DPT04.setComponentPopupMenu(BTD);
            DPT04.setName("DPT04");
            xTitledPanel1ContentContainer.add(DPT04);
            DPT04.setBounds(360, 155, 45, DPT04.getPreferredSize().height);

            //---- DPT05 ----
            DPT05.setComponentPopupMenu(BTD);
            DPT05.setName("DPT05");
            xTitledPanel1ContentContainer.add(DPT05);
            DPT05.setBounds(360, 193, 45, DPT05.getPreferredSize().height);

            //---- DPT06 ----
            DPT06.setComponentPopupMenu(BTD);
            DPT06.setName("DPT06");
            xTitledPanel1ContentContainer.add(DPT06);
            DPT06.setBounds(360, 231, 45, DPT06.getPreferredSize().height);

            //---- DPT07 ----
            DPT07.setComponentPopupMenu(BTD);
            DPT07.setName("DPT07");
            xTitledPanel1ContentContainer.add(DPT07);
            DPT07.setBounds(360, 269, 45, DPT07.getPreferredSize().height);

            //---- DPT08 ----
            DPT08.setComponentPopupMenu(BTD);
            DPT08.setName("DPT08");
            xTitledPanel1ContentContainer.add(DPT08);
            DPT08.setBounds(360, 307, 45, DPT08.getPreferredSize().height);

            //---- DPT09 ----
            DPT09.setComponentPopupMenu(BTD);
            DPT09.setName("DPT09");
            xTitledPanel1ContentContainer.add(DPT09);
            DPT09.setBounds(360, 345, 45, DPT09.getPreferredSize().height);

            //---- NBJDP ----
            NBJDP.setComponentPopupMenu(BTD);
            NBJDP.setName("NBJDP");
            xTitledPanel1ContentContainer.add(NBJDP);
            NBJDP.setBounds(360, 415, 45, NBJDP.getPreferredSize().height);

            //---- DPC01 ----
            DPC01.setComponentPopupMenu(BTD);
            DPC01.setName("DPC01");
            xTitledPanel1ContentContainer.add(DPC01);
            DPC01.setBounds(615, 41, 45, DPC01.getPreferredSize().height);

            //---- DPC02 ----
            DPC02.setComponentPopupMenu(BTD);
            DPC02.setName("DPC02");
            xTitledPanel1ContentContainer.add(DPC02);
            DPC02.setBounds(615, 75, 45, DPC02.getPreferredSize().height);

            //---- DPC03 ----
            DPC03.setComponentPopupMenu(BTD);
            DPC03.setName("DPC03");
            xTitledPanel1ContentContainer.add(DPC03);
            DPC03.setBounds(615, 115, 45, DPC03.getPreferredSize().height);

            //---- DPC04 ----
            DPC04.setComponentPopupMenu(BTD);
            DPC04.setName("DPC04");
            xTitledPanel1ContentContainer.add(DPC04);
            DPC04.setBounds(615, 150, 45, DPC04.getPreferredSize().height);

            //---- DPC05 ----
            DPC05.setComponentPopupMenu(BTD);
            DPC05.setName("DPC05");
            xTitledPanel1ContentContainer.add(DPC05);
            DPC05.setBounds(615, 190, 45, DPC05.getPreferredSize().height);

            //---- DPC06 ----
            DPC06.setComponentPopupMenu(BTD);
            DPC06.setName("DPC06");
            xTitledPanel1ContentContainer.add(DPC06);
            DPC06.setBounds(615, 225, 45, DPC06.getPreferredSize().height);

            //---- DPC07 ----
            DPC07.setComponentPopupMenu(BTD);
            DPC07.setName("DPC07");
            xTitledPanel1ContentContainer.add(DPC07);
            DPC07.setBounds(615, 265, 45, DPC07.getPreferredSize().height);

            //---- DPC08 ----
            DPC08.setComponentPopupMenu(BTD);
            DPC08.setName("DPC08");
            xTitledPanel1ContentContainer.add(DPC08);
            DPC08.setBounds(615, 305, 45, DPC08.getPreferredSize().height);

            //---- DPC09 ----
            DPC09.setComponentPopupMenu(BTD);
            DPC09.setName("DPC09");
            xTitledPanel1ContentContainer.add(DPC09);
            DPC09.setBounds(615, 345, 45, DPC09.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44_OBJ_45;
  private XRiTextField INDETB;
  private JLabel OBJ_45_OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_46_OBJ_47;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_55_OBJ_55;
  private XRiTextField DPT01;
  private JLabel OBJ_55_OBJ_57;
  private JLabel OBJ_55_OBJ_58;
  private JLabel OBJ_55_OBJ_59;
  private JLabel OBJ_55_OBJ_60;
  private JLabel OBJ_55_OBJ_61;
  private JLabel OBJ_55_OBJ_62;
  private JLabel OBJ_55_OBJ_63;
  private JLabel OBJ_55_OBJ_64;
  private JLabel OBJ_55_OBJ_65;
  private JLabel OBJ_55_OBJ_56;
  private JLabel OBJ_55_OBJ_66;
  private XRiTextField DPT02;
  private XRiTextField DPT03;
  private XRiTextField DPT04;
  private XRiTextField DPT05;
  private XRiTextField DPT06;
  private XRiTextField DPT07;
  private XRiTextField DPT08;
  private XRiTextField DPT09;
  private XRiTextField NBJDP;
  private XRiTextField DPC01;
  private XRiTextField DPC02;
  private XRiTextField DPC03;
  private XRiTextField DPC04;
  private XRiTextField DPC05;
  private XRiTextField DPC06;
  private XRiTextField DPC07;
  private XRiTextField DPC08;
  private XRiTextField DPC09;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
