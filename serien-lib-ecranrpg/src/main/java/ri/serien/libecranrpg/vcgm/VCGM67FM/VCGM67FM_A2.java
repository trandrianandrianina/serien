
package ri.serien.libecranrpg.vcgm.VCGM67FM;
// Nom Fichier: b_VCGM67FM_FMTA2_1075.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VCGM67FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", "WTP16", "WTP17", "WTP18", };
  private String[] _WTP01_Title = { "HLD01", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, { "LD16", }, { "LD17", }, { "LD18", }, };
  private int[] _WTP01_Width = { 663, };
  // private String[][] _LIST_Title_Data_Brut=null;
  
  public VCGM67FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    // _LIST_Title_Data_Brut = initTable(LIST, "Courier New", Font.BOLD, 12, "Courier New", Font.PLAIN, 12);
    // setDialog(true);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
    // setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    l_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    LIBSOC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBSOC@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
    OBJ_47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NOMIFS@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST, _LIST_Title_Data_Brut, _WTP01_Top);
    
    
    JO10.setVisible(lexique.isPresent("JO10"));
    JO09.setVisible(lexique.isPresent("JO09"));
    JO08.setVisible(lexique.isPresent("JO08"));
    JO07.setVisible(lexique.isPresent("JO07"));
    JO06.setVisible(lexique.isPresent("JO06"));
    JO05.setVisible(lexique.isPresent("JO05"));
    JO04.setVisible(lexique.isPresent("JO04"));
    JO03.setVisible(lexique.isPresent("JO03"));
    JO02.setVisible(lexique.isPresent("JO02"));
    JO01.setVisible(lexique.isPresent("JO01"));
    LIBSOC.setVisible(lexique.isPresent("LIBSOC"));
    
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    // TODO Icones
    BT_PGDOWN.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    BT_PGUP.setIcon(lexique.chargerImage("images/pgup20.png", true));
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    OBJ_37.setIcon(lexique.chargerImage("images/plus.png", true));
    OBJ_56.setIcon(lexique.chargerImage("images/moins.png", true));
    xH_Titre.setIcon(ManagerSessionClient.getInstance().chargerLogoEtablissement(INDSOC.getText()));
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - RELEVES DE BANQUES DANS IFS"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "1", "Enter");
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_FonctionsActionPerformed(ActionEvent e) {
    CMD.show(bt_Fonctions, 0, bt_Fonctions.getHeight());
  }
  
  private void OBJ_37ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("VALALL", 0, "1");
    lexique.HostFieldPutData("DESALL", 0, " ");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_56ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("VALALL", 0, " ");
    lexique.HostFieldPutData("DESALL", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void BT_PGUPActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGUP", false);
  }
  
  private void BT_PGDOWNActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGDOWN", false);
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1", false);
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@", false);
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@", false);
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@", false);
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@", false);
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@", false);
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@", false);
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _WTP01_Top, "1", "ENTER", e);
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    bt_Fonctions = new JButton();
    l_V01F = new JLabel();
    OBJ_43 = new JLabel();
    INDSOC = new XRiTextField();
    LIBSOC = new JLabel();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    P_Centre = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    OBJ_35 = new JPanel();
    OBJ_37 = new JButton();
    OBJ_56 = new JButton();
    OBJ_51 = new JPanel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    panel1 = new JPanel();
    OBJ_47 = new XRiTextField();
    panel2 = new JPanel();
    JO01 = new XRiTextField();
    JO02 = new XRiTextField();
    JO03 = new XRiTextField();
    JO04 = new XRiTextField();
    JO05 = new XRiTextField();
    JO06 = new XRiTextField();
    JO07 = new XRiTextField();
    JO08 = new XRiTextField();
    JO09 = new XRiTextField();
    JO10 = new XRiTextField();
    CellConstraints cc = new CellConstraints();

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Choix du Papier");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      CMD.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setText("Annuler");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      CMD.add(OBJ_9);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_13 ----
      OBJ_13.setText("Aide en ligne");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("Invite");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);
    }

    //======== this ========
    setPreferredSize(new Dimension(950, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("RELEVES DE BANQUES DANS IFS");
      xH_Titre.setDescription("FM@LOCGRP/+1/@");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 22));
        P_Infos.setPreferredSize(new Dimension(66, 35));
        P_Infos.setName("P_Infos");

        //---- bt_Fonctions ----
        bt_Fonctions.setText("Fonctions");
        bt_Fonctions.setName("bt_Fonctions");
        bt_Fonctions.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_FonctionsActionPerformed(e);
          }
        });

        //---- l_V01F ----
        l_V01F.setText("@V01F@");
        l_V01F.setFont(new Font("sansserif", Font.BOLD, 12));
        l_V01F.setName("l_V01F");

        //---- OBJ_43 ----
        OBJ_43.setText("Soci\u00e9t\u00e9");
        OBJ_43.setName("OBJ_43");

        //---- INDSOC ----
        INDSOC.setComponentPopupMenu(BTD);
        INDSOC.setName("INDSOC");

        //---- LIBSOC ----
        LIBSOC.setText("@LIBSOC@");
        LIBSOC.setName("LIBSOC");

        GroupLayout P_InfosLayout = new GroupLayout(P_Infos);
        P_Infos.setLayout(P_InfosLayout);
        P_InfosLayout.setHorizontalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, P_InfosLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(l_V01F, GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE)
              .addGap(18, 18, 18)
              .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(INDSOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(LIBSOC, GroupLayout.PREFERRED_SIZE, 231, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 602, Short.MAX_VALUE)
              .addComponent(bt_Fonctions, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
              .addContainerGap())
        );
        P_InfosLayout.setVerticalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGroup(P_InfosLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(bt_Fonctions)
                .addComponent(l_V01F, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                .addComponent(INDSOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(LIBSOC, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(200)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");
      P_Centre.setLayout(null);

      //======== SCROLLPANE_LIST ========
      {
        SCROLLPANE_LIST.setComponentPopupMenu(BTD);
        SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

        //---- WTP01 ----
        WTP01.setName("WTP01");
        WTP01.addMouseListener(new MouseAdapter() {
          @Override
          public void mouseClicked(MouseEvent e) {
            WTP01MouseClicked(e);
          }
        });
        SCROLLPANE_LIST.setViewportView(WTP01);
      }
      P_Centre.add(SCROLLPANE_LIST);
      SCROLLPANE_LIST.setBounds(30, 225, 1105, 315);

      //======== OBJ_35 ========
      {
        OBJ_35.setName("OBJ_35");
        OBJ_35.setLayout(null);
      }
      P_Centre.add(OBJ_35);
      OBJ_35.setBounds(45, -15, 812, 4);

      //---- OBJ_37 ----
      OBJ_37.setComponentPopupMenu(BTD);
      OBJ_37.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_37.setToolTipText("Choix global");
      OBJ_37.setName("OBJ_37");
      OBJ_37.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_37ActionPerformed(e);
        }
      });
      P_Centre.add(OBJ_37);
      OBJ_37.setBounds(1045, 175, 40, 40);

      //---- OBJ_56 ----
      OBJ_56.setComponentPopupMenu(BTD);
      OBJ_56.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_56.setToolTipText("D\u00e9s\u00e9lection globale");
      OBJ_56.setName("OBJ_56");
      OBJ_56.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_56ActionPerformed(e);
        }
      });
      P_Centre.add(OBJ_56);
      OBJ_56.setBounds(1090, 175, 40, 40);

      //======== OBJ_51 ========
      {
        OBJ_51.setName("OBJ_51");
        OBJ_51.setLayout(null);
      }
      P_Centre.add(OBJ_51);
      OBJ_51.setBounds(2, 445, 45, 4);

      //---- BT_PGUP ----
      BT_PGUP.setText("");
      BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
      BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_PGUP.setName("BT_PGUP");
      BT_PGUP.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_PGUPActionPerformed(e);
        }
      });
      P_Centre.add(BT_PGUP);
      BT_PGUP.setBounds(1140, 225, 25, 135);

      //---- BT_PGDOWN ----
      BT_PGDOWN.setText("");
      BT_PGDOWN.setToolTipText("Page suivante");
      BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_PGDOWN.setName("BT_PGDOWN");
      BT_PGDOWN.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_PGDOWNActionPerformed(e);
        }
      });
      P_Centre.add(BT_PGDOWN);
      BT_PGDOWN.setBounds(1140, 405, 25, 135);

      //======== panel1 ========
      {
        panel1.setBorder(new TitledBorder("Chemin de l'IFS trait\u00e9"));
        panel1.setName("panel1");

        //---- OBJ_47 ----
        OBJ_47.setText("@NOMIFS@");
        OBJ_47.setName("OBJ_47");

        GroupLayout panel1Layout = new GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
          panel1Layout.createParallelGroup()
            .addGroup(panel1Layout.createSequentialGroup()
              .addGap(109, 109, 109)
              .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 651, GroupLayout.PREFERRED_SIZE))
        );
        panel1Layout.setVerticalGroup(
          panel1Layout.createParallelGroup()
            .addGroup(panel1Layout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        );
      }
      P_Centre.add(panel1);
      panel1.setBounds(30, 120, 890, 100);

      //======== panel2 ========
      {
        panel2.setBorder(new TitledBorder("Codes journaux"));
        panel2.setName("panel2");

        //---- JO01 ----
        JO01.setComponentPopupMenu(BTD);
        JO01.setName("JO01");

        //---- JO02 ----
        JO02.setComponentPopupMenu(BTD);
        JO02.setName("JO02");

        //---- JO03 ----
        JO03.setComponentPopupMenu(BTD);
        JO03.setName("JO03");

        //---- JO04 ----
        JO04.setComponentPopupMenu(BTD);
        JO04.setName("JO04");

        //---- JO05 ----
        JO05.setComponentPopupMenu(BTD);
        JO05.setName("JO05");

        //---- JO06 ----
        JO06.setComponentPopupMenu(BTD);
        JO06.setName("JO06");

        //---- JO07 ----
        JO07.setComponentPopupMenu(BTD);
        JO07.setName("JO07");

        //---- JO08 ----
        JO08.setComponentPopupMenu(BTD);
        JO08.setName("JO08");

        //---- JO09 ----
        JO09.setComponentPopupMenu(BTD);
        JO09.setName("JO09");

        //---- JO10 ----
        JO10.setComponentPopupMenu(BTD);
        JO10.setName("JO10");

        GroupLayout panel2Layout = new GroupLayout(panel2);
        panel2.setLayout(panel2Layout);
        panel2Layout.setHorizontalGroup(
          panel2Layout.createParallelGroup()
            .addGroup(panel2Layout.createSequentialGroup()
              .addGap(109, 109, 109)
              .addComponent(JO01, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
              .addGap(14, 14, 14)
              .addComponent(JO02, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
              .addGap(14, 14, 14)
              .addComponent(JO03, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
              .addGap(14, 14, 14)
              .addComponent(JO04, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
              .addGap(14, 14, 14)
              .addComponent(JO05, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
              .addGap(14, 14, 14)
              .addComponent(JO06, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
              .addGap(14, 14, 14)
              .addComponent(JO07, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
              .addGap(14, 14, 14)
              .addComponent(JO08, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
              .addGap(14, 14, 14)
              .addComponent(JO09, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
              .addGap(14, 14, 14)
              .addComponent(JO10, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
        );
        panel2Layout.setVerticalGroup(
          panel2Layout.createParallelGroup()
            .addGroup(panel2Layout.createSequentialGroup()
              .addGap(25, 25, 25)
              .addGroup(panel2Layout.createParallelGroup()
                .addComponent(JO01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(JO02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(JO03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(JO04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(JO05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(JO06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(JO07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(JO08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(JO09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(JO10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
        );
      }
      P_Centre.add(panel2);
      panel2.setBounds(30, 5, 890, 100);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < P_Centre.getComponentCount(); i++) {
          Rectangle bounds = P_Centre.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = P_Centre.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        P_Centre.setMinimumSize(preferredSize);
        P_Centre.setPreferredSize(preferredSize);
      }
    }
    add(P_Centre, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private JButton bt_Fonctions;
  private JLabel l_V01F;
  private JLabel OBJ_43;
  private XRiTextField INDSOC;
  private JLabel LIBSOC;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  private JPanel P_Centre;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JPanel OBJ_35;
  private JButton OBJ_37;
  private JButton OBJ_56;
  private JPanel OBJ_51;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPanel panel1;
  private XRiTextField OBJ_47;
  private JPanel panel2;
  private XRiTextField JO01;
  private XRiTextField JO02;
  private XRiTextField JO03;
  private XRiTextField JO04;
  private XRiTextField JO05;
  private XRiTextField JO06;
  private XRiTextField JO07;
  private XRiTextField JO08;
  private XRiTextField JO09;
  private XRiTextField JO10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
