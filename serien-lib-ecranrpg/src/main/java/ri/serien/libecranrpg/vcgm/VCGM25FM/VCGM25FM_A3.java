
package ri.serien.libecranrpg.vcgm.VCGM25FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM25FM_A3 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] TOPLET_Value = { "1", "2", "3", };
  private String[] TOPLET_Title = { "On ne sélectionne pas les écritures lettrées SAUF si date de lettrage > date d'analyse",
      "On ne sélectionne pas les écritures lettrées", "On sélectionne les écritures lettrées", };
  
  public VCGM25FM_A3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TOPLET.setValeurs(TOPLET_Value, TOPLET_Title);
    EDTFAX.setValeursSelection("OUI", "NON");
    SOLGLI.setValeursSelection("OUI", "NON");
    RELEXI.setValeursSelection("OUI", "NON");
    RELED2.setValeursSelection("OUI", "NON");
    RELALP.setValeursSelection("OUI", "NON");
    RELTRI.setValeursSelection("OUI", "NON");
    RELCRE.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RELTIT@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    


    
    
    // TOPLET.setSelectedIndex(getIndice("TOPLET", TOPLET_Value));
    // SOLGLI.setSelected(lexique.HostFieldGetData("SOLGLI").equalsIgnoreCase("OUI"));
    // RELTRI.setSelected(lexique.HostFieldGetData("RELTRI").equalsIgnoreCase("OUI"));
    // RELALP.setSelected(lexique.HostFieldGetData("RELALP").equalsIgnoreCase("OUI"));
    // RELED2.setSelected(lexique.HostFieldGetData("RELED2").equalsIgnoreCase("OUI"));
    // RELEXI.setSelected(lexique.HostFieldGetData("RELEXI").equalsIgnoreCase("OUI"));
    // RELCRE.setSelected(lexique.HostFieldGetData("RELCRE").equalsIgnoreCase("OUI"));
    // EDTFAX.setSelected(lexique.HostFieldGetData("EDTFAX").equalsIgnoreCase("OUI"));
    
    riMenu_bt1.setIcon(lexique.chargerImage("images/options.png", true));
    
    

    
    p_bpresentation.setCodeEtablissement(RELSOC.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(RELSOC.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (SOLGLI.isSelected())
    // lexique.HostFieldPutData("SOLGLI", 0, "OUI");
    // else
    // lexique.HostFieldPutData("SOLGLI", 0, "NON");
    // if (RELTRI.isSelected())
    // lexique.HostFieldPutData("RELTRI", 0, "OUI");
    // else
    // lexique.HostFieldPutData("RELTRI", 0, "NON");
    // if (RELALP.isSelected())
    // lexique.HostFieldPutData("RELALP", 0, "OUI");
    // else
    // lexique.HostFieldPutData("RELALP", 0, "NON");
    // if (RELED2.isSelected())
    // lexique.HostFieldPutData("RELED2", 0, "OUI");
    // else
    // lexique.HostFieldPutData("RELED2", 0, "NON");
    // if (RELEXI.isSelected())
    // lexique.HostFieldPutData("RELEXI", 0, "OUI");
    // else
    // lexique.HostFieldPutData("RELEXI", 0, "NON");
    // if (RELCRE.isSelected())
    // lexique.HostFieldPutData("RELCRE", 0, "OUI");
    // else
    // lexique.HostFieldPutData("RELCRE", 0, "NON");
    // if (EDTFAX.isSelected())
    // lexique.HostFieldPutData("EDTFAX", 0, "OUI");
    // else
    // lexique.HostFieldPutData("EDTFAX", 0, "NON");
    // lexique.HostFieldPutData("TOPLET", 0, TOPLET_Value[TOPLET.getSelectedIndex()]);
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_24 = new JLabel();
    RELSOC = new XRiTextField();
    OBJ_25 = new JLabel();
    RELCOL = new XRiTextField();
    OBJ_26 = new JLabel();
    RELDEB = new XRiTextField();
    OBJ_27 = new JLabel();
    RELFIN = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu1 = new RiMenu();
    riMenu_bt1 = new RiMenu_bt();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_50 = new JLabel();
    RELLIM = new XRiCalendrier();
    OBJ_48 = new JLabel();
    DATDEB = new XRiCalendrier();
    panel2 = new JPanel();
    OBJ_56 = new JLabel();
    RELETA = new XRiTextField();
    RELCRE = new XRiCheckBox();
    TOPLET = new XRiComboBox();
    OBJ_67 = new JLabel();
    RELREP = new XRiTextField();
    RELTRI = new XRiCheckBox();
    RELALP = new XRiCheckBox();
    RELDEV = new XRiTextField();
    OBJ_69 = new JLabel();
    RELED2 = new XRiCheckBox();
    RELEXI = new XRiCheckBox();
    SOLGLI = new XRiCheckBox();
    EDTFAX = new XRiCheckBox();
    OBJ_68 = new JLabel();
    DATECH = new XRiCalendrier();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@RELTIT@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_24 ----
          OBJ_24.setText("Soci\u00e9t\u00e9");
          OBJ_24.setName("OBJ_24");

          //---- RELSOC ----
          RELSOC.setComponentPopupMenu(BTD);
          RELSOC.setName("RELSOC");

          //---- OBJ_25 ----
          OBJ_25.setText("Collectif");
          OBJ_25.setName("OBJ_25");

          //---- RELCOL ----
          RELCOL.setComponentPopupMenu(BTD);
          RELCOL.setName("RELCOL");

          //---- OBJ_26 ----
          OBJ_26.setText("Auxiliaire de d\u00e9but");
          OBJ_26.setName("OBJ_26");

          //---- RELDEB ----
          RELDEB.setComponentPopupMenu(BTD);
          RELDEB.setName("RELDEB");

          //---- OBJ_27 ----
          OBJ_27.setText("Auxiliaire de fin");
          OBJ_27.setName("OBJ_27");

          //---- RELFIN ----
          RELFIN.setComponentPopupMenu(BTD);
          RELFIN.setName("RELFIN");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(RELSOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(RELCOL, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_26, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(RELDEB, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(RELFIN, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(160, 160, 160))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(RELSOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(RELCOL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(RELDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(RELFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_24)
                  .addComponent(OBJ_25)
                  .addComponent(OBJ_26)
                  .addComponent(OBJ_27)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu1 ========
            {
              riMenu1.setName("riMenu1");

              //---- riMenu_bt1 ----
              riMenu_bt1.setText("Options");
              riMenu_bt1.setName("riMenu_bt1");
              riMenu1.add(riMenu_bt1);
            }
            menus_haut.add(riMenu1);

            //======== riSousMenu1 ========
            {
              riSousMenu1.setName("riSousMenu1");

              //---- riSousMenu_bt1 ----
              riSousMenu_bt1.setText("Exportation vers tableur");
              riSousMenu_bt1.setToolTipText("Exportation vers tableur");
              riSousMenu_bt1.setName("riSousMenu_bt1");
              riSousMenu_bt1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt1ActionPerformed(e);
                }
              });
              riSousMenu1.add(riSousMenu_bt1);
            }
            menus_haut.add(riSousMenu1);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(740, 550));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("S\u00e9lection de p\u00e9riode"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_50 ----
            OBJ_50.setText("Date limite");
            OBJ_50.setName("OBJ_50");
            panel1.add(OBJ_50);
            OBJ_50.setBounds(293, 50, 78, 20);

            //---- RELLIM ----
            RELLIM.setComponentPopupMenu(BTD);
            RELLIM.setName("RELLIM");
            panel1.add(RELLIM);
            RELLIM.setBounds(373, 46, 105, RELLIM.getPreferredSize().height);

            //---- OBJ_48 ----
            OBJ_48.setText("Date de d\u00e9but");
            OBJ_48.setName("OBJ_48");
            panel1.add(OBJ_48);
            OBJ_48.setBounds(38, 50, 93, 20);

            //---- DATDEB ----
            DATDEB.setComponentPopupMenu(BTD);
            DATDEB.setName("DATDEB");
            panel1.add(DATDEB);
            DATDEB.setBounds(138, 46, 105, DATDEB.getPreferredSize().height);

            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setBackground(new Color(214, 217, 223));
            panel2.setBorder(new TitledBorder("Option"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_56 ----
            OBJ_56.setText("Type de lettre ou cliquer droit pour choisir");
            OBJ_56.setName("OBJ_56");
            panel2.add(OBJ_56);
            OBJ_56.setBounds(30, 30, 248, 23);

            //---- RELETA ----
            RELETA.setComponentPopupMenu(BTD);
            RELETA.setName("RELETA");
            panel2.add(RELETA);
            RELETA.setBounds(275, 25, 24, RELETA.getPreferredSize().height);

            //---- RELCRE ----
            RELCRE.setText("S\u00e9lection des cr\u00e9dits non point\u00e9s");
            RELCRE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RELCRE.setName("RELCRE");
            panel2.add(RELCRE);
            RELCRE.setBounds(30, 60, 248, 23);

            //---- TOPLET ----
            TOPLET.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TOPLET.setName("TOPLET");
            panel2.add(TOPLET);
            TOPLET.setBounds(30, 95, 538, TOPLET.getPreferredSize().height);

            //---- OBJ_67 ----
            OBJ_67.setText("Code repr\u00e9sentant");
            OBJ_67.setName("OBJ_67");
            panel2.add(OBJ_67);
            OBJ_67.setBounds(30, 135, 248, 21);

            //---- RELREP ----
            RELREP.setComponentPopupMenu(BTD);
            RELREP.setName("RELREP");
            panel2.add(RELREP);
            RELREP.setBounds(275, 130, 34, RELREP.getPreferredSize().height);

            //---- RELTRI ----
            RELTRI.setText("Tri par repr\u00e9sentant");
            RELTRI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RELTRI.setName("RELTRI");
            panel2.add(RELTRI);
            RELTRI.setBounds(30, 260, 248, 21);

            //---- RELALP ----
            RELALP.setText("Tri par code aplhab\u00e9tique");
            RELALP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RELALP.setName("RELALP");
            panel2.add(RELALP);
            RELALP.setBounds(30, 290, 248, 21);

            //---- RELDEV ----
            RELDEV.setComponentPopupMenu(BTD);
            RELDEV.setName("RELDEV");
            panel2.add(RELDEV);
            RELDEV.setBounds(275, 160, 40, RELDEV.getPreferredSize().height);

            //---- OBJ_69 ----
            OBJ_69.setText("Comptes dont le code devise est");
            OBJ_69.setName("OBJ_69");
            panel2.add(OBJ_69);
            OBJ_69.setBounds(30, 165, 248, 21);

            //---- RELED2 ----
            RELED2.setText("Pr\u00e9sentation type de compte");
            RELED2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RELED2.setName("RELED2");
            panel2.add(RELED2);
            RELED2.setBounds(30, 195, 248, 21);

            //---- RELEXI ----
            RELEXI.setText("Non \u00e9dition du total exigible");
            RELEXI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RELEXI.setName("RELEXI");
            panel2.add(RELEXI);
            RELEXI.setBounds(30, 225, 248, 21);

            //---- SOLGLI ----
            SOLGLI.setText("Avec solde gliss\u00e9");
            SOLGLI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SOLGLI.setName("SOLGLI");
            panel2.add(SOLGLI);
            SOLGLI.setBounds(30, 325, 248, 21);

            //---- EDTFAX ----
            EDTFAX.setText("Envoi par email");
            EDTFAX.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EDTFAX.setName("EDTFAX");
            panel2.add(EDTFAX);
            EDTFAX.setBounds(30, 355, 275, 20);

            //---- OBJ_68 ----
            OBJ_68.setText("Factures \u00e9chues \u00e0 \u00e9ch\u00e9ance au");
            OBJ_68.setName("OBJ_68");
            panel2.add(OBJ_68);
            OBJ_68.setBounds(50, 380, 175, 21);

            //---- DATECH ----
            DATECH.setComponentPopupMenu(BTD);
            DATECH.setName("DATECH");
            panel2.add(DATECH);
            DATECH.setBounds(230, 375, 105, DATECH.getPreferredSize().height);

            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                  .addComponent(panel1, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 711, Short.MAX_VALUE)
                  .addComponent(panel2, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 711, Short.MAX_VALUE))
                .addContainerGap(15, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 96, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 411, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(15, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_24;
  private XRiTextField RELSOC;
  private JLabel OBJ_25;
  private XRiTextField RELCOL;
  private JLabel OBJ_26;
  private XRiTextField RELDEB;
  private JLabel OBJ_27;
  private XRiTextField RELFIN;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu1;
  private RiMenu_bt riMenu_bt1;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_50;
  private XRiCalendrier RELLIM;
  private JLabel OBJ_48;
  private XRiCalendrier DATDEB;
  private JPanel panel2;
  private JLabel OBJ_56;
  private XRiTextField RELETA;
  private XRiCheckBox RELCRE;
  private XRiComboBox TOPLET;
  private JLabel OBJ_67;
  private XRiTextField RELREP;
  private XRiCheckBox RELTRI;
  private XRiCheckBox RELALP;
  private XRiTextField RELDEV;
  private JLabel OBJ_69;
  private XRiCheckBox RELED2;
  private XRiCheckBox RELEXI;
  private XRiCheckBox SOLGLI;
  private XRiCheckBox EDTFAX;
  private JLabel OBJ_68;
  private XRiCalendrier DATECH;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
