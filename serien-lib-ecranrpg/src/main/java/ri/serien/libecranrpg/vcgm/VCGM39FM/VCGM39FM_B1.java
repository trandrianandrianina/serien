
package ri.serien.libecranrpg.vcgm.VCGM39FM;
// Nom Fichier: pop_VCGM39FM_FMTB1_815.java

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import com.jgoodies.forms.factories.DefaultComponentFactory;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VCGM39FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  // private String[] _LIST_Top=null;
  private String[] _D01_Title = { "Echéance", "Nb.", "Montant", "Simul. en cours", };
  private String[][] _D01_Data = { { "D01", "N01", "M01", "S01", }, { "D02", "N02", "M02", "S02", }, { "D03", "N03", "M03", "S03", },
      { "D04", "N04", "M04", "S04", }, { "D05", "N05", "M05", "S05", }, { "D06", "N06", "M06", "S06", }, { "D07", "N07", "M07", "S07", },
      { "D08", "N08", "M08", "S08", }, { "D09", "N09", "M09", "S09", }, { "D10", "N10", "M10", "S10", }, { "D11", "N11", "M11", "S11", },
      { "D12", "N12", "M12", "S12", }, };
  private int[] _D01_Width = { 61, 35, 112, 110, };
  // private String[][] _LIST_Title_Data_Brut=null;
  private int[] _LIST_Justification = { SwingConstants.LEFT, SwingConstants.RIGHT, SwingConstants.RIGHT, SwingConstants.RIGHT };
  
  public VCGM39FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    // _LIST_Title_Data_Brut = initTable(LIST, "Courier New", Font.BOLD, 12, "Courier New", Font.PLAIN, 12);
    // setDialog(true);
    
    // Ajout
    initDiverses();
    D01.setAspectTable(null, _D01_Title, _D01_Data, _D01_Width, false, _LIST_Justification, null, null, null);
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
    // setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    DefaultComponentFactory.setTextAndMnemonic((JLabel)separator1.getComponent(0), lexique.TranslationTable(interpreteurD.analyseExpression("@LIBDOM@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, _LIST_Title_Data_Brut, _LIST_Top, _LIST_Justification);
    
    
    
    
    // TODO Icones
    BT_PGDOWN.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    BT_PGUP.setIcon(lexique.chargerImage("images/pgup20.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    OBJ_21.setIcon(lexique.chargerImage("images/retour.png", true));
    
    // V07F
    
    // Titre
    // setTitle(???);
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void VALActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void BT_PGUPActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGUP", false);
  }
  
  private void BT_PGDOWNActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGDOWN", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    SCROLLPANE_LIST = new JScrollPane();
    D01 = new XRiTable();
    BT_ENTER = new JButton();
    OBJ_21 = new JButton();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    separator1 = compFactory.createSeparator("@LIBDOM@");
    OBJ_4 = new JMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setName("this");

    //======== SCROLLPANE_LIST ========
    {
      SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

      //---- D01 ----
      D01.setName("D01");
      SCROLLPANE_LIST.setViewportView(D01);
    }

    //---- BT_ENTER ----
    BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_ENTER.setToolTipText("Ok");
    BT_ENTER.setName("BT_ENTER");
    BT_ENTER.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        VALActionPerformed(e);
      }
    });

    //---- OBJ_21 ----
    OBJ_21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_21.setToolTipText("Retour");
    OBJ_21.setName("OBJ_21");
    OBJ_21.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_21ActionPerformed(e);
      }
    });

    //---- BT_PGUP ----
    BT_PGUP.setText("");
    BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
    BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_PGUP.setName("BT_PGUP");
    BT_PGUP.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        BT_PGUPActionPerformed(e);
      }
    });

    //---- BT_PGDOWN ----
    BT_PGDOWN.setText("");
    BT_PGDOWN.setToolTipText("Page suivante");
    BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_PGDOWN.setName("BT_PGDOWN");
    BT_PGDOWN.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        BT_PGDOWNActionPerformed(e);
      }
    });

    //---- separator1 ----
    separator1.setName("separator1");

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(25, 25, 25)
          .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 503, GroupLayout.PREFERRED_SIZE)
          .addGap(12, 12, 12)
          .addGroup(layout.createParallelGroup()
            .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
            .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)))
        .addGroup(layout.createSequentialGroup()
          .addGap(450, 450, 450)
          .addComponent(BT_ENTER, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
          .addGap(6, 6, 6)
          .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(15, 15, 15)
          .addComponent(separator1, GroupLayout.DEFAULT_SIZE, 553, Short.MAX_VALUE)
          .addContainerGap())
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(15, 15, 15)
          .addComponent(separator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          .addGap(11, 11, 11)
          .addGroup(layout.createParallelGroup()
            .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 222, GroupLayout.PREFERRED_SIZE)
            .addGroup(layout.createSequentialGroup()
              .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
              .addGap(30, 30, 30)
              .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)))
          .addGap(8, 8, 8)
          .addGroup(layout.createParallelGroup()
            .addComponent(BT_ENTER, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
            .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
    );

    //======== OBJ_4 ========
    {
      OBJ_4.setText("Commandes");
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("Annuler");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_6);

      //---- OBJ_7 ----
      OBJ_7.setText("Exploitation");
      OBJ_7.setName("OBJ_7");
      OBJ_4.add(OBJ_7);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_9 ----
      OBJ_9.setText("Aide en ligne");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      BTD.add(OBJ_9);

      //---- OBJ_10 ----
      OBJ_10.setText("Invite");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable D01;
  private JButton BT_ENTER;
  private JButton OBJ_21;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JComponent separator1;
  private JMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_7;
  private JPopupMenu BTD;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
