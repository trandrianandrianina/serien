
package ri.serien.libecranrpg.vcgm.VCGM58FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM58FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "HLD01", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 543, };
  
  public VCGM58FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Relances @LIBSEL@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_71_OBJ_71.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBGES@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _WTP01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    OBJ_71_OBJ_71.setVisible(lexique.isPresent("LIBGES"));
    
    btlien1.setVisible(!lexique.HostFieldGetData("LD01").substring(13, 14).trim().equalsIgnoreCase(""));
    btlien2.setVisible(!lexique.HostFieldGetData("LD02").substring(13, 14).trim().equalsIgnoreCase(""));
    btlien3.setVisible(!lexique.HostFieldGetData("LD03").substring(13, 14).trim().equalsIgnoreCase(""));
    btlien4.setVisible(!lexique.HostFieldGetData("LD04").substring(13, 14).trim().equalsIgnoreCase(""));
    btlien5.setVisible(!lexique.HostFieldGetData("LD05").substring(13, 14).trim().equalsIgnoreCase(""));
    btlien6.setVisible(!lexique.HostFieldGetData("LD06").substring(13, 14).trim().equalsIgnoreCase(""));
    btlien7.setVisible(!lexique.HostFieldGetData("LD07").substring(13, 14).trim().equalsIgnoreCase(""));
    btlien8.setVisible(!lexique.HostFieldGetData("LD08").substring(13, 14).trim().equalsIgnoreCase(""));
    btlien9.setVisible(!lexique.HostFieldGetData("LD09").substring(13, 14).trim().equalsIgnoreCase(""));
    btlien10.setVisible(!lexique.HostFieldGetData("LD10").substring(13, 14).trim().equalsIgnoreCase(""));
    btlien11.setVisible(!lexique.HostFieldGetData("LD11").substring(13, 14).trim().equalsIgnoreCase(""));
    btlien12.setVisible(!lexique.HostFieldGetData("LD12").substring(13, 14).trim().equalsIgnoreCase(""));
    btlien13.setVisible(!lexique.HostFieldGetData("LD13").substring(13, 14).trim().equalsIgnoreCase(""));
    btlien14.setVisible(!lexique.HostFieldGetData("LD14").substring(13, 14).trim().equalsIgnoreCase(""));
    btlien15.setVisible(!lexique.HostFieldGetData("LD15").substring(13, 14).trim().equalsIgnoreCase(""));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  Fichier relance @LIBSEL@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTDI.getInvoker().getName());
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTDI.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("6");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("7");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("8");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("F");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_27ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("T");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_28ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void btlien1ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP01", 0, "6");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btlien2ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP02", 0, "6");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btlien3ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP03", 0, "6");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btlien4ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP04", 0, "6");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btlien5ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP05", 0, "6");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btlien6ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP06", 0, "6");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btlien7ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP07", 0, "6");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btlien8ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP08", 0, "6");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btlien9ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP09", 0, "6");
    lexique.HostScreenSendKey(this, "ENTER");
    // WTP01.setValeurTop("L");
    // lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void btlien10ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP10", 0, "6");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btlien11ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP11", 0, "6");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btlien12ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP12", 0, "6");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btlien13ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP13", 0, "6");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btlien14ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP12", 0, "6");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btlien15ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP13", 0, "6");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_36_OBJ_36 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_37_OBJ_37 = new JLabel();
    RLDTRX = new XRiCalendrier();
    OBJ_38_OBJ_38 = new JLabel();
    NCGX = new XRiTextField();
    OBJ_39_OBJ_39 = new JLabel();
    INDNCA = new XRiTextField();
    OBJ_40_OBJ_40 = new JLabel();
    INDPCE = new XRiTextField();
    OBJ_41_OBJ_41 = new JLabel();
    INDRAN = new XRiTextField();
    OBJ_42_OBJ_42 = new JLabel();
    WETAT = new XRiTextField();
    OBJ_43_OBJ_43 = new JLabel();
    WGES = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    OBJ_71_OBJ_71 = new JLabel();
    panel1 = new JPanel();
    btlien1 = new SNBoutonDetail();
    btlien2 = new SNBoutonDetail();
    btlien3 = new SNBoutonDetail();
    btlien4 = new SNBoutonDetail();
    btlien5 = new SNBoutonDetail();
    btlien6 = new SNBoutonDetail();
    btlien7 = new SNBoutonDetail();
    btlien8 = new SNBoutonDetail();
    btlien9 = new SNBoutonDetail();
    btlien10 = new SNBoutonDetail();
    btlien11 = new SNBoutonDetail();
    btlien12 = new SNBoutonDetail();
    btlien13 = new SNBoutonDetail();
    btlien14 = new SNBoutonDetail();
    btlien15 = new SNBoutonDetail();
    BTDI = new JPopupMenu();
    OBJ_6 = new JMenuItem();
    OBJ_5 = new JMenuItem();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_21 = new JMenuItem();
    OBJ_22 = new JMenuItem();
    OBJ_23 = new JMenuItem();
    OBJ_24 = new JMenuItem();
    OBJ_25 = new JMenuItem();
    OBJ_26 = new JMenuItem();
    OBJ_27 = new JMenuItem();
    OBJ_28 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Relances @LIBSEL@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(950, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_36_OBJ_36 ----
          OBJ_36_OBJ_36.setText("Soci\u00e9t\u00e9");
          OBJ_36_OBJ_36.setName("OBJ_36_OBJ_36");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setName("INDETB");

          //---- OBJ_37_OBJ_37 ----
          OBJ_37_OBJ_37.setText("Date relance");
          OBJ_37_OBJ_37.setName("OBJ_37_OBJ_37");

          //---- RLDTRX ----
          RLDTRX.setComponentPopupMenu(null);
          RLDTRX.setName("RLDTRX");

          //---- OBJ_38_OBJ_38 ----
          OBJ_38_OBJ_38.setText("Collectif");
          OBJ_38_OBJ_38.setName("OBJ_38_OBJ_38");

          //---- NCGX ----
          NCGX.setComponentPopupMenu(null);
          NCGX.setName("NCGX");

          //---- OBJ_39_OBJ_39 ----
          OBJ_39_OBJ_39.setText("Auxiliaire");
          OBJ_39_OBJ_39.setName("OBJ_39_OBJ_39");

          //---- INDNCA ----
          INDNCA.setComponentPopupMenu(null);
          INDNCA.setName("INDNCA");

          //---- OBJ_40_OBJ_40 ----
          OBJ_40_OBJ_40.setText("Pi\u00e8ce");
          OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");

          //---- INDPCE ----
          INDPCE.setComponentPopupMenu(null);
          INDPCE.setName("INDPCE");

          //---- OBJ_41_OBJ_41 ----
          OBJ_41_OBJ_41.setText("R\u00e8glement");
          OBJ_41_OBJ_41.setName("OBJ_41_OBJ_41");

          //---- INDRAN ----
          INDRAN.setComponentPopupMenu(null);
          INDRAN.setName("INDRAN");

          //---- OBJ_42_OBJ_42 ----
          OBJ_42_OBJ_42.setText("S");
          OBJ_42_OBJ_42.setName("OBJ_42_OBJ_42");

          //---- WETAT ----
          WETAT.setToolTipText("Filtre / s\u00e9lection (F4 pour liste)");
          WETAT.setComponentPopupMenu(BTDI);
          WETAT.setName("WETAT");

          //---- OBJ_43_OBJ_43 ----
          OBJ_43_OBJ_43.setText("Rc");
          OBJ_43_OBJ_43.setName("OBJ_43_OBJ_43");

          //---- WGES ----
          WGES.setToolTipText("Responsable du compte dans l'entreprise");
          WGES.setComponentPopupMenu(null);
          WGES.setName("WGES");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_36_OBJ_36, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_37_OBJ_37, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(RLDTRX, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(50, 50, 50)
                    .addComponent(NCGX, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_38_OBJ_38, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(OBJ_39_OBJ_39, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INDNCA, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_40_OBJ_40, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDPCE, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(OBJ_41_OBJ_41, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDRAN, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_42_OBJ_42, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(WETAT, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_43_OBJ_43, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(WGES, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(RLDTRX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(NCGX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDNCA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDPCE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDRAN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WETAT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WGES, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_36_OBJ_36)
                  .addComponent(OBJ_37_OBJ_37)
                  .addComponent(OBJ_38_OBJ_38)
                  .addComponent(OBJ_39_OBJ_39)
                  .addComponent(OBJ_40_OBJ_40)
                  .addComponent(OBJ_41_OBJ_41)
                  .addComponent(OBJ_42_OBJ_42)
                  .addComponent(OBJ_43_OBJ_43)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Pr\u00e9vision encaissements");
              riSousMenu_bt6.setToolTipText("Pr\u00e9vision d'encaissements");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Total relances par niveau");
              riSousMenu_bt7.setToolTipText("Total des relances par niveau");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(780, 360));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("R\u00e9sultat de la recherche"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST.setViewportView(WTP01);
            }
            panel2.add(SCROLLPANE_LIST);
            SCROLLPANE_LIST.setBounds(45, 40, 655, 270);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel2.add(BT_PGUP);
            BT_PGUP.setBounds(710, 40, 25, 125);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel2.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(710, 185, 25, 125);

            //---- OBJ_71_OBJ_71 ----
            OBJ_71_OBJ_71.setText("@LIBGES@");
            OBJ_71_OBJ_71.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_71_OBJ_71.setForeground(Color.red);
            OBJ_71_OBJ_71.setName("OBJ_71_OBJ_71");
            panel2.add(OBJ_71_OBJ_71);
            OBJ_71_OBJ_71.setBounds(25, 310, 655, 20);

            //======== panel1 ========
            {
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- btlien1 ----
              btlien1.setText("");
              btlien1.setToolTipText("Bloc-notes");
              btlien1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              btlien1.setName("btlien1");
              btlien1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  btlien1ActionPerformed(e);
                }
              });
              panel1.add(btlien1);
              btlien1.setBounds(5, 25, 20, btlien1.getPreferredSize().height);

              //---- btlien2 ----
              btlien2.setText("");
              btlien2.setToolTipText("Bloc-notes");
              btlien2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              btlien2.setName("btlien2");
              btlien2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  btlien2ActionPerformed(e);
                }
              });
              panel1.add(btlien2);
              btlien2.setBounds(5, 41, 20, btlien2.getPreferredSize().height);

              //---- btlien3 ----
              btlien3.setText("");
              btlien3.setToolTipText("Bloc-notes");
              btlien3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              btlien3.setName("btlien3");
              btlien3.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  btlien3ActionPerformed(e);
                }
              });
              panel1.add(btlien3);
              btlien3.setBounds(5, 57, 20, btlien3.getPreferredSize().height);

              //---- btlien4 ----
              btlien4.setText("");
              btlien4.setToolTipText("Bloc-notes");
              btlien4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              btlien4.setName("btlien4");
              btlien4.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  btlien4ActionPerformed(e);
                }
              });
              panel1.add(btlien4);
              btlien4.setBounds(5, 73, 20, btlien4.getPreferredSize().height);

              //---- btlien5 ----
              btlien5.setText("");
              btlien5.setToolTipText("Bloc-notes");
              btlien5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              btlien5.setName("btlien5");
              btlien5.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  btlien5ActionPerformed(e);
                }
              });
              panel1.add(btlien5);
              btlien5.setBounds(5, 89, 20, btlien5.getPreferredSize().height);

              //---- btlien6 ----
              btlien6.setText("");
              btlien6.setToolTipText("Bloc-notes");
              btlien6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              btlien6.setName("btlien6");
              btlien6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  btlien6ActionPerformed(e);
                }
              });
              panel1.add(btlien6);
              btlien6.setBounds(5, 105, 20, btlien6.getPreferredSize().height);

              //---- btlien7 ----
              btlien7.setText("");
              btlien7.setToolTipText("Bloc-notes");
              btlien7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              btlien7.setName("btlien7");
              btlien7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  btlien7ActionPerformed(e);
                }
              });
              panel1.add(btlien7);
              btlien7.setBounds(5, 121, 20, btlien7.getPreferredSize().height);

              //---- btlien8 ----
              btlien8.setText("");
              btlien8.setToolTipText("Bloc-notes");
              btlien8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              btlien8.setName("btlien8");
              btlien8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  btlien8ActionPerformed(e);
                }
              });
              panel1.add(btlien8);
              btlien8.setBounds(5, 137, 20, btlien8.getPreferredSize().height);

              //---- btlien9 ----
              btlien9.setText("");
              btlien9.setToolTipText("Bloc-notes");
              btlien9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              btlien9.setName("btlien9");
              btlien9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  btlien9ActionPerformed(e);
                }
              });
              panel1.add(btlien9);
              btlien9.setBounds(5, 153, 20, btlien9.getPreferredSize().height);

              //---- btlien10 ----
              btlien10.setText("");
              btlien10.setToolTipText("Bloc-notes");
              btlien10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              btlien10.setName("btlien10");
              btlien10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  btlien10ActionPerformed(e);
                }
              });
              panel1.add(btlien10);
              btlien10.setBounds(5, 169, 20, btlien10.getPreferredSize().height);

              //---- btlien11 ----
              btlien11.setText("");
              btlien11.setToolTipText("Bloc-notes");
              btlien11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              btlien11.setName("btlien11");
              btlien11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  btlien11ActionPerformed(e);
                }
              });
              panel1.add(btlien11);
              btlien11.setBounds(5, 185, 20, btlien11.getPreferredSize().height);

              //---- btlien12 ----
              btlien12.setText("");
              btlien12.setToolTipText("Bloc-notes");
              btlien12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              btlien12.setName("btlien12");
              btlien12.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  btlien12ActionPerformed(e);
                }
              });
              panel1.add(btlien12);
              btlien12.setBounds(5, 201, 20, btlien12.getPreferredSize().height);

              //---- btlien13 ----
              btlien13.setText("");
              btlien13.setToolTipText("Bloc-notes");
              btlien13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              btlien13.setName("btlien13");
              btlien13.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  btlien13ActionPerformed(e);
                }
              });
              panel1.add(btlien13);
              btlien13.setBounds(5, 217, 20, btlien13.getPreferredSize().height);

              //---- btlien14 ----
              btlien14.setText("");
              btlien14.setToolTipText("Bloc-notes");
              btlien14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              btlien14.setName("btlien14");
              btlien14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  btlien14ActionPerformed(e);
                }
              });
              panel1.add(btlien14);
              btlien14.setBounds(5, 233, 20, btlien14.getPreferredSize().height);

              //---- btlien15 ----
              btlien15.setText("");
              btlien15.setToolTipText("Bloc-notes");
              btlien15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              btlien15.setName("btlien15");
              btlien15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  btlien15ActionPerformed(e);
                }
              });
              panel1.add(btlien15);
              btlien15.setBounds(5, 249, 20, btlien15.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            panel2.add(panel1);
            panel1.setBounds(15, 40, 30, 270);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 754, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 340, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(5, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTDI ========
    {
      BTDI.setName("BTDI");

      //---- OBJ_6 ----
      OBJ_6.setText("Choix possibles");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      BTDI.add(OBJ_6);

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTDI.add(OBJ_5);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_21 ----
      OBJ_21.setText("Modification");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);

      //---- OBJ_22 ----
      OBJ_22.setText("Interrogation");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);

      //---- OBJ_23 ----
      OBJ_23.setText("Bloc Note");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD.add(OBJ_23);

      //---- OBJ_24 ----
      OBJ_24.setText("R\u00e9vision");
      OBJ_24.setName("OBJ_24");
      OBJ_24.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_24ActionPerformed(e);
        }
      });
      BTD.add(OBJ_24);

      //---- OBJ_25 ----
      OBJ_25.setText("Total");
      OBJ_25.setName("OBJ_25");
      OBJ_25.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_25ActionPerformed(e);
        }
      });
      BTD.add(OBJ_25);

      //---- OBJ_26 ----
      OBJ_26.setText("Facture GVM");
      OBJ_26.setName("OBJ_26");
      OBJ_26.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_26ActionPerformed(e);
        }
      });
      BTD.add(OBJ_26);

      //---- OBJ_27 ----
      OBJ_27.setText("OD");
      OBJ_27.setName("OBJ_27");
      OBJ_27.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_27ActionPerformed(e);
        }
      });
      BTD.add(OBJ_27);
      BTD.addSeparator();

      //---- OBJ_28 ----
      OBJ_28.setText("Aide en ligne");
      OBJ_28.setName("OBJ_28");
      OBJ_28.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_28ActionPerformed(e);
        }
      });
      BTD.add(OBJ_28);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_36_OBJ_36;
  private XRiTextField INDETB;
  private JLabel OBJ_37_OBJ_37;
  private XRiCalendrier RLDTRX;
  private JLabel OBJ_38_OBJ_38;
  private XRiTextField NCGX;
  private JLabel OBJ_39_OBJ_39;
  private XRiTextField INDNCA;
  private JLabel OBJ_40_OBJ_40;
  private XRiTextField INDPCE;
  private JLabel OBJ_41_OBJ_41;
  private XRiTextField INDRAN;
  private JLabel OBJ_42_OBJ_42;
  private XRiTextField WETAT;
  private JLabel OBJ_43_OBJ_43;
  private XRiTextField WGES;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel2;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JLabel OBJ_71_OBJ_71;
  private JPanel panel1;
  private SNBoutonDetail btlien1;
  private SNBoutonDetail btlien2;
  private SNBoutonDetail btlien3;
  private SNBoutonDetail btlien4;
  private SNBoutonDetail btlien5;
  private SNBoutonDetail btlien6;
  private SNBoutonDetail btlien7;
  private SNBoutonDetail btlien8;
  private SNBoutonDetail btlien9;
  private SNBoutonDetail btlien10;
  private SNBoutonDetail btlien11;
  private SNBoutonDetail btlien12;
  private SNBoutonDetail btlien13;
  private SNBoutonDetail btlien14;
  private SNBoutonDetail btlien15;
  private JPopupMenu BTDI;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_5;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_24;
  private JMenuItem OBJ_25;
  private JMenuItem OBJ_26;
  private JMenuItem OBJ_27;
  private JMenuItem OBJ_28;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
