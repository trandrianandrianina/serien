
package ri.serien.libecranrpg.vcgm.VCGM50FM;
// Nom Fichier: i_VCGM50FM_F02B4_FMTF1_724.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM50FM_B7 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM50FM_B7(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDefaultButton(bouton_valider);
    
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDETA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETA@")).trim());
    LIBETA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBETA@")).trim());
    OBJ_48_OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("page @WPAGE@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  DECLARATION TVA"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    BTD = new JPopupMenu();
    OBJ_15 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_47_OBJ_47 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_48_OBJ_48 = new JLabel();
    INDETA = new RiZoneSortie();
    LIBETA = new RiZoneSortie();
    p_tete_droite = new JPanel();
    OBJ_48_OBJ_50 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    MT3601 = new XRiTextField();
    MT3701 = new XRiTextField();
    MT3801 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    MT3901 = new XRiTextField();
    MT4001 = new XRiTextField();
    MT4101 = new XRiTextField();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    MT4201 = new XRiTextField();
    MT4301 = new XRiTextField();
    label7 = new JLabel();
    label8 = new JLabel();
    WTT37R = new XRiTextField();
    WTT39R = new XRiTextField();
    WTT40R = new XRiTextField();
    WTT41R = new XRiTextField();
    WTT42R = new XRiTextField();
    WTT43R = new XRiTextField();
    label25 = new JLabel();
    label26 = new JLabel();
    panel2 = new JPanel();
    label15 = new JLabel();
    MT4401 = new XRiTextField();
    FLD001 = new XRiTextField();
    OBJ_48_OBJ_49 = new JLabel();
    panel3 = new JPanel();
    xTitledPanel2 = new JXTitledPanel();
    WTT45R = new XRiTextField();
    WTT46R = new XRiTextField();
    WTT47R = new XRiTextField();
    WTT48R = new XRiTextField();
    WTT49R = new XRiTextField();

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_15 ----
      OBJ_15.setText("Choix possibles");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);

      //---- OBJ_14 ----
      OBJ_14.setText("Aide en ligne");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("D\u00e9claration de TVA");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_47_OBJ_47 ----
          OBJ_47_OBJ_47.setText("Etablissement");
          OBJ_47_OBJ_47.setName("OBJ_47_OBJ_47");
          p_tete_gauche.add(OBJ_47_OBJ_47);
          OBJ_47_OBJ_47.setBounds(5, 5, 100, 21);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setText("@INDETB@");
          INDETB.setOpaque(false);
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(110, 3, 40, INDETB.getPreferredSize().height);

          //---- OBJ_48_OBJ_48 ----
          OBJ_48_OBJ_48.setText("Etat");
          OBJ_48_OBJ_48.setName("OBJ_48_OBJ_48");
          p_tete_gauche.add(OBJ_48_OBJ_48);
          OBJ_48_OBJ_48.setBounds(190, 5, 40, 21);

          //---- INDETA ----
          INDETA.setComponentPopupMenu(BTD);
          INDETA.setText("@INDETA@");
          INDETA.setOpaque(false);
          INDETA.setName("INDETA");
          p_tete_gauche.add(INDETA);
          INDETA.setBounds(240, 3, 64, INDETA.getPreferredSize().height);

          //---- LIBETA ----
          LIBETA.setText("@LIBETA@");
          LIBETA.setOpaque(false);
          LIBETA.setName("LIBETA");
          p_tete_gauche.add(LIBETA);
          LIBETA.setBounds(310, 3, 532, LIBETA.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- OBJ_48_OBJ_50 ----
          OBJ_48_OBJ_50.setText("page @WPAGE@");
          OBJ_48_OBJ_50.setFont(OBJ_48_OBJ_50.getFont().deriveFont(OBJ_48_OBJ_50.getFont().getStyle() | Font.BOLD, OBJ_48_OBJ_50.getFont().getSize() + 2f));
          OBJ_48_OBJ_50.setName("OBJ_48_OBJ_50");
          p_tete_droite.add(OBJ_48_OBJ_50);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Protection zones CGM");
              riSousMenu_bt6.setToolTipText("Push/Pull protection des zones g\u00e9n\u00e9r\u00e9es depuis CGM");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(980, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("B- DECOMPTE DES TAXES ASSIMILEES"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== xTitledPanel1 ========
            {
              xTitledPanel1.setTitle("Nature des taxes                                                                                                                                                                                                                        Net \u00e0 payer");
              xTitledPanel1.setBorder(new DropShadowBorder());
              xTitledPanel1.setName("xTitledPanel1");
              Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
              xTitledPanel1ContentContainer.setLayout(null);

              //---- MT3601 ----
              MT3601.setName("MT3601");
              xTitledPanel1ContentContainer.add(MT3601);
              MT3601.setBounds(720, 5, 135, MT3601.getPreferredSize().height);

              //---- MT3701 ----
              MT3701.setName("MT3701");
              xTitledPanel1ContentContainer.add(MT3701);
              MT3701.setBounds(720, 30, 135, MT3701.getPreferredSize().height);

              //---- MT3801 ----
              MT3801.setName("MT3801");
              xTitledPanel1ContentContainer.add(MT3801);
              MT3801.setBounds(720, 55, 135, MT3801.getPreferredSize().height);

              //---- label1 ----
              label1.setText("64 - Taxe sur les actes des huissiers justice (art 302bis Y )");
              label1.setName("label1");
              xTitledPanel1ContentContainer.add(label1);
              label1.setBounds(12, 5, 448, 28);

              //---- label2 ----
              label2.setText("65 -");
              label2.setName("label2");
              xTitledPanel1ContentContainer.add(label2);
              label2.setBounds(12, 30, 23, 28);

              //---- label3 ----
              label3.setText("66 - Taxe sur l'embarquement et le d\u00e9barquement de passagers en Corse (art 1599 vic.)");
              label3.setName("label3");
              xTitledPanel1ContentContainer.add(label3);
              label3.setBounds(12, 55, 573, 28);

              //---- MT3901 ----
              MT3901.setName("MT3901");
              xTitledPanel1ContentContainer.add(MT3901);
              MT3901.setBounds(720, 80, 135, MT3901.getPreferredSize().height);

              //---- MT4001 ----
              MT4001.setName("MT4001");
              xTitledPanel1ContentContainer.add(MT4001);
              MT4001.setBounds(720, 105, 135, MT4001.getPreferredSize().height);

              //---- MT4101 ----
              MT4101.setName("MT4101");
              xTitledPanel1ContentContainer.add(MT4101);
              MT4101.setBounds(720, 130, 135, MT4101.getPreferredSize().height);

              //---- label4 ----
              label4.setText("67 -");
              label4.setName("label4");
              xTitledPanel1ContentContainer.add(label4);
              label4.setBounds(12, 80, 23, 28);

              //---- label5 ----
              label5.setText("68 -");
              label5.setName("label5");
              xTitledPanel1ContentContainer.add(label5);
              label5.setBounds(12, 105, 23, 28);

              //---- label6 ----
              label6.setText("69 -");
              label6.setName("label6");
              xTitledPanel1ContentContainer.add(label6);
              label6.setBounds(12, 130, 23, 28);

              //---- MT4201 ----
              MT4201.setName("MT4201");
              xTitledPanel1ContentContainer.add(MT4201);
              MT4201.setBounds(720, 155, 135, MT4201.getPreferredSize().height);

              //---- MT4301 ----
              MT4301.setName("MT4301");
              xTitledPanel1ContentContainer.add(MT4301);
              MT4301.setBounds(720, 180, 135, MT4301.getPreferredSize().height);

              //---- label7 ----
              label7.setText("70 -");
              label7.setName("label7");
              xTitledPanel1ContentContainer.add(label7);
              label7.setBounds(12, 155, 23, 28);

              //---- label8 ----
              label8.setText("71  -");
              label8.setName("label8");
              xTitledPanel1ContentContainer.add(label8);
              label8.setBounds(12, 180, 28, 28);

              //---- WTT37R ----
              WTT37R.setName("WTT37R");
              xTitledPanel1ContentContainer.add(WTT37R);
              WTT37R.setBounds(35, 30, 420, WTT37R.getPreferredSize().height);

              //---- WTT39R ----
              WTT39R.setName("WTT39R");
              xTitledPanel1ContentContainer.add(WTT39R);
              WTT39R.setBounds(35, 80, 420, WTT39R.getPreferredSize().height);

              //---- WTT40R ----
              WTT40R.setName("WTT40R");
              xTitledPanel1ContentContainer.add(WTT40R);
              WTT40R.setBounds(35, 105, 420, WTT40R.getPreferredSize().height);

              //---- WTT41R ----
              WTT41R.setName("WTT41R");
              xTitledPanel1ContentContainer.add(WTT41R);
              WTT41R.setBounds(35, 130, 420, WTT41R.getPreferredSize().height);

              //---- WTT42R ----
              WTT42R.setName("WTT42R");
              xTitledPanel1ContentContainer.add(WTT42R);
              WTT42R.setBounds(35, 155, 420, WTT42R.getPreferredSize().height);

              //---- WTT43R ----
              WTT43R.setName("WTT43R");
              xTitledPanel1ContentContainer.add(WTT43R);
              WTT43R.setBounds(35, 180, 420, WTT43R.getPreferredSize().height);

              //---- label25 ----
              label25.setText("4206");
              label25.setFont(label25.getFont().deriveFont(label25.getFont().getStyle() | Font.BOLD));
              label25.setHorizontalAlignment(SwingConstants.RIGHT);
              label25.setName("label25");
              xTitledPanel1ContentContainer.add(label25);
              label25.setBounds(620, 7, 95, 25);

              //---- label26 ----
              label26.setText("4204");
              label26.setFont(label26.getFont().deriveFont(label26.getFont().getStyle() | Font.BOLD));
              label26.setHorizontalAlignment(SwingConstants.RIGHT);
              label26.setName("label26");
              xTitledPanel1ContentContainer.add(label26);
              label26.setBounds(620, 57, 95, 25);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = xTitledPanel1ContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
                xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
              }
            }
            panel1.add(xTitledPanel1);
            xTitledPanel1.setBounds(15, 30, 905, 245);

            //======== panel2 ========
            {
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- label15 ----
              label15.setText("TOTAL DES LIGNES 47 A 71");
              label15.setFont(label15.getFont().deriveFont(label15.getFont().getStyle() | Font.BOLD));
              label15.setName("label15");
              panel2.add(label15);
              label15.setBounds(10, 5, 448, 28);

              //---- MT4401 ----
              MT4401.setName("MT4401");
              panel2.add(MT4401);
              MT4401.setBounds(720, 5, 135, MT4401.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel2);
            panel2.setBounds(15, 275, 905, 35);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //---- FLD001 ----
          FLD001.setName("FLD001");

          //---- OBJ_48_OBJ_49 ----
          OBJ_48_OBJ_49.setText("Aller \u00e0 la page");
          OBJ_48_OBJ_49.setName("OBJ_48_OBJ_49");

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder("C- LIQUIDATION DES TAXES"));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //======== xTitledPanel2 ========
            {
              xTitledPanel2.setBorder(new DropShadowBorder());
              xTitledPanel2.setName("xTitledPanel2");
              Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
              xTitledPanel2ContentContainer.setLayout(null);

              //---- WTT45R ----
              WTT45R.setName("WTT45R");
              xTitledPanel2ContentContainer.add(WTT45R);
              WTT45R.setBounds(12, 5, 668, WTT45R.getPreferredSize().height);

              //---- WTT46R ----
              WTT46R.setName("WTT46R");
              xTitledPanel2ContentContainer.add(WTT46R);
              WTT46R.setBounds(12, 31, 668, WTT46R.getPreferredSize().height);

              //---- WTT47R ----
              WTT47R.setName("WTT47R");
              xTitledPanel2ContentContainer.add(WTT47R);
              WTT47R.setBounds(12, 57, 668, WTT47R.getPreferredSize().height);

              //---- WTT48R ----
              WTT48R.setName("WTT48R");
              xTitledPanel2ContentContainer.add(WTT48R);
              WTT48R.setBounds(12, 83, 668, WTT48R.getPreferredSize().height);

              //---- WTT49R ----
              WTT49R.setName("WTT49R");
              xTitledPanel2ContentContainer.add(WTT49R);
              WTT49R.setBounds(12, 109, 668, WTT49R.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = xTitledPanel2ContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
                xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
              }
            }
            panel3.add(xTitledPanel2);
            xTitledPanel2.setBounds(10, 30, 905, 170);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(OBJ_48_OBJ_49, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(FLD001, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18))
                  .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(panel3, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 324, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 215, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_48_OBJ_49, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addComponent(FLD001, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
          p_contenuLayout.linkSize(SwingConstants.VERTICAL, new Component[] {FLD001, OBJ_48_OBJ_49});
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 1, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu BTD;
  private JMenuItem OBJ_15;
  private JMenuItem OBJ_14;
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_47_OBJ_47;
  private RiZoneSortie INDETB;
  private JLabel OBJ_48_OBJ_48;
  private RiZoneSortie INDETA;
  private RiZoneSortie LIBETA;
  private JPanel p_tete_droite;
  private JLabel OBJ_48_OBJ_50;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JXTitledPanel xTitledPanel1;
  private XRiTextField MT3601;
  private XRiTextField MT3701;
  private XRiTextField MT3801;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private XRiTextField MT3901;
  private XRiTextField MT4001;
  private XRiTextField MT4101;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private XRiTextField MT4201;
  private XRiTextField MT4301;
  private JLabel label7;
  private JLabel label8;
  private XRiTextField WTT37R;
  private XRiTextField WTT39R;
  private XRiTextField WTT40R;
  private XRiTextField WTT41R;
  private XRiTextField WTT42R;
  private XRiTextField WTT43R;
  private JLabel label25;
  private JLabel label26;
  private JPanel panel2;
  private JLabel label15;
  private XRiTextField MT4401;
  private XRiTextField FLD001;
  private JLabel OBJ_48_OBJ_49;
  private JPanel panel3;
  private JXTitledPanel xTitledPanel2;
  private XRiTextField WTT45R;
  private XRiTextField WTT46R;
  private XRiTextField WTT47R;
  private XRiTextField WTT48R;
  private XRiTextField WTT49R;
  // JFormDesigner - End of variables declaration  //GEN-END:variables



}
