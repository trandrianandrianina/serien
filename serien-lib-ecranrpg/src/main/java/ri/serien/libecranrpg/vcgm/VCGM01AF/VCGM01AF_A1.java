
package ri.serien.libecranrpg.vcgm.VCGM01AF;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGM01AF_A1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM01AF_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - PERSONNALISATION COMPTA"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_19_OBJ_19 = new JLabel();
    SCAN = new XRiTextField();
    OBJ_17_OBJ_17 = new JLabel();
    ARG1 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(565, 180));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Recherche multi-crit\u00e8res"));
          panel1.setOpaque(false);
          panel1.setName("panel1");

          //---- OBJ_19_OBJ_19 ----
          OBJ_19_OBJ_19.setText("Cha\u00eene de caract\u00e8res");
          OBJ_19_OBJ_19.setName("OBJ_19_OBJ_19");

          //---- SCAN ----
          SCAN.setComponentPopupMenu(BTD);
          SCAN.setName("SCAN");

          //---- OBJ_17_OBJ_17 ----
          OBJ_17_OBJ_17.setText("Code param\u00e8tre");
          OBJ_17_OBJ_17.setName("OBJ_17_OBJ_17");

          //---- ARG1 ----
          ARG1.setComponentPopupMenu(BTD);
          ARG1.setName("ARG1");

          GroupLayout panel1Layout = new GroupLayout(panel1);
          panel1.setLayout(panel1Layout);
          panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(OBJ_17_OBJ_17, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE)
                    .addGap(49, 49, 49)
                    .addComponent(ARG1, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(OBJ_19_OBJ_19, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)
                    .addGap(19, 19, 19)
                    .addComponent(SCAN, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE))))
          );
          panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_17_OBJ_17, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ARG1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_19_OBJ_19, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(SCAN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 375, 160);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Choix possibles");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);

      //---- OBJ_9 ----
      OBJ_9.setText("Aide en ligne");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      BTD.add(OBJ_9);
    }

    //======== menus_haut ========
    {
      menus_haut.setMinimumSize(new Dimension(160, 520));
      menus_haut.setPreferredSize(new Dimension(160, 520));
      menus_haut.setBackground(new Color(238, 239, 241));
      menus_haut.setAutoscrolls(true);
      menus_haut.setName("menus_haut");
      menus_haut.setLayout(new VerticalLayout());

      //======== riMenu_V01F ========
      {
        riMenu_V01F.setMinimumSize(new Dimension(104, 50));
        riMenu_V01F.setPreferredSize(new Dimension(170, 50));
        riMenu_V01F.setMaximumSize(new Dimension(104, 50));
        riMenu_V01F.setName("riMenu_V01F");

        //---- riMenu_bt_V01F ----
        riMenu_bt_V01F.setText("@V01F@");
        riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
        riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
        riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
        riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
        riMenu_bt_V01F.setName("riMenu_bt_V01F");
        riMenu_V01F.add(riMenu_bt_V01F);
      }
      menus_haut.add(riMenu_V01F);

      //======== riSousMenu_consult ========
      {
        riSousMenu_consult.setName("riSousMenu_consult");

        //---- riSousMenu_bt_consult ----
        riSousMenu_bt_consult.setText("Consultation");
        riSousMenu_bt_consult.setToolTipText("Consultation");
        riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
        riSousMenu_consult.add(riSousMenu_bt_consult);
      }
      menus_haut.add(riSousMenu_consult);

      //======== riSousMenu_modif ========
      {
        riSousMenu_modif.setName("riSousMenu_modif");

        //---- riSousMenu_bt_modif ----
        riSousMenu_bt_modif.setText("Modification");
        riSousMenu_bt_modif.setToolTipText("Modification");
        riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
        riSousMenu_modif.add(riSousMenu_bt_modif);
      }
      menus_haut.add(riSousMenu_modif);

      //======== riSousMenu_crea ========
      {
        riSousMenu_crea.setName("riSousMenu_crea");

        //---- riSousMenu_bt_crea ----
        riSousMenu_bt_crea.setText("Cr\u00e9ation");
        riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
        riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
        riSousMenu_crea.add(riSousMenu_bt_crea);
      }
      menus_haut.add(riSousMenu_crea);

      //======== riSousMenu_suppr ========
      {
        riSousMenu_suppr.setName("riSousMenu_suppr");

        //---- riSousMenu_bt_suppr ----
        riSousMenu_bt_suppr.setText("Annulation");
        riSousMenu_bt_suppr.setToolTipText("Annulation");
        riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
        riSousMenu_suppr.add(riSousMenu_bt_suppr);
      }
      menus_haut.add(riSousMenu_suppr);

      //======== riSousMenu_rappel ========
      {
        riSousMenu_rappel.setName("riSousMenu_rappel");

        //---- riSousMenu_bt_rappel ----
        riSousMenu_bt_rappel.setText("Rappel");
        riSousMenu_bt_rappel.setToolTipText("Rappel");
        riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
        riSousMenu_rappel.add(riSousMenu_bt_rappel);
      }
      menus_haut.add(riSousMenu_rappel);

      //======== riSousMenu_reac ========
      {
        riSousMenu_reac.setName("riSousMenu_reac");

        //---- riSousMenu_bt_reac ----
        riSousMenu_bt_reac.setText("R\u00e9activation");
        riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
        riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
        riSousMenu_reac.add(riSousMenu_bt_reac);
      }
      menus_haut.add(riSousMenu_reac);

      //======== riSousMenu_destr ========
      {
        riSousMenu_destr.setName("riSousMenu_destr");

        //---- riSousMenu_bt_destr ----
        riSousMenu_bt_destr.setText("Suppression");
        riSousMenu_bt_destr.setToolTipText("Suppression");
        riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
        riSousMenu_destr.add(riSousMenu_bt_destr);
      }
      menus_haut.add(riSousMenu_destr);

      //======== riSousMenuF_dupli ========
      {
        riSousMenuF_dupli.setName("riSousMenuF_dupli");

        //---- riSousMenu_bt_dupli ----
        riSousMenu_bt_dupli.setText("Duplication");
        riSousMenu_bt_dupli.setToolTipText("Duplication");
        riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
        riSousMenuF_dupli.add(riSousMenu_bt_dupli);
      }
      menus_haut.add(riSousMenuF_dupli);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_19_OBJ_19;
  private XRiTextField SCAN;
  private JLabel OBJ_17_OBJ_17;
  private XRiTextField ARG1;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_9;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
