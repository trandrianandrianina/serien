
package ri.serien.libecranrpg.vcgm.VCGM01FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM01FM_ED extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] EDSNS_Value = { "*", "C", "D", };
  private String[] EDSNS_Title = { "Tous sens autorisés", "Crédit", "Débit", };
  
  public VCGM01FM_ED(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    EDSNS.setValeurs(EDSNS_Value, EDSNS_Title);
    EDS02.setValeursSelection("S", " ");
    EDS04.setValeursSelection("S", " ");
    EDS03.setValeursSelection("S", " ");
    EDS05.setValeursSelection("S", " ");
    EDS06.setValeursSelection("S", " ");
    EDS07.setValeursSelection("S", " ");
    EDS08.setValeursSelection("S", " ");
    EDS01.setValeursSelection("S", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    EDJ01_CHK.setSelected(lexique.HostFieldGetData("EDJ01").equalsIgnoreCase("**"));
    P_SEL0.setVisible(!EDJ01_CHK.isSelected());
    EDL01_CHK.setSelected(lexique.HostFieldGetData("EDL01").equalsIgnoreCase("*"));
    P_SEL1.setVisible(!EDL01_CHK.isSelected());
    EDR01_CHK.setSelected(lexique.HostFieldGetData("EDR01").equalsIgnoreCase("**"));
    P_SEL2.setVisible(!EDR01_CHK.isSelected());
    
    

    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    // riMenu_bt2.setIcon(lexique.getImage("images/options.png",true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    if (EDJ01_CHK.isSelected()) {
      lexique.HostFieldPutData("EDJ01", 0, "**");
    }
    if (EDL01_CHK.isSelected()) {
      lexique.HostFieldPutData("EDL01", 0, "*");
    }
    if (EDR01_CHK.isSelected()) {
      lexique.HostFieldPutData("EDR01", 0, "**");
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void EDJ01_CHKActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
    if (!EDJ01_CHK.isSelected()) {
      EDJ01.setText("");
    }
  }
  
  private void EDL01_CHKActionPerformed(ActionEvent e) {
    P_SEL1.setVisible(!P_SEL1.isVisible());
    if (!EDL01_CHK.isSelected()) {
      EDL01.setText("");
    }
  }
  
  private void EDR01_CHKActionPerformed(ActionEvent e) {
    P_SEL2.setVisible(!P_SEL2.isVisible());
    if (!EDR01_CHK.isSelected()) {
      EDR01.setText("");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44_OBJ_45 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_45_OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46_OBJ_47 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    EDSNS = new XRiComboBox();
    OBJ_86_OBJ_86 = new JLabel();
    panel2 = new JPanel();
    EDC01 = new XRiTextField();
    EDC02 = new XRiTextField();
    EDC03 = new XRiTextField();
    EDC04 = new XRiTextField();
    EDC05 = new XRiTextField();
    EDC06 = new XRiTextField();
    EDC07 = new XRiTextField();
    EDC08 = new XRiTextField();
    EDS01 = new XRiCheckBox();
    EDS08 = new XRiCheckBox();
    EDS07 = new XRiCheckBox();
    EDS06 = new XRiCheckBox();
    EDS05 = new XRiCheckBox();
    EDS03 = new XRiCheckBox();
    EDS04 = new XRiCheckBox();
    EDS02 = new XRiCheckBox();
    EDJ01_CHK = new JCheckBox();
    EDL01_CHK = new JCheckBox();
    P_SEL0 = new JPanel();
    OBJ_36_OBJ_36 = new JLabel();
    EDJ01 = new XRiTextField();
    EDJ02 = new XRiTextField();
    EDJ03 = new XRiTextField();
    EDJ04 = new XRiTextField();
    EDJ05 = new XRiTextField();
    P_SEL1 = new JPanel();
    OBJ_85_OBJ_85 = new JLabel();
    EDL01 = new XRiTextField();
    EDL02 = new XRiTextField();
    EDL03 = new XRiTextField();
    EDL04 = new XRiTextField();
    EDL05 = new XRiTextField();
    xTitledSeparator1 = new JXTitledSeparator();
    EDR01_CHK = new JCheckBox();
    P_SEL2 = new JPanel();
    EDR01 = new XRiTextField();
    EDR02 = new XRiTextField();
    EDR03 = new XRiTextField();
    EDR04 = new XRiTextField();
    EDR05 = new XRiTextField();
    OBJ_90_OBJ_90 = new JLabel();
    EDRGL = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44_OBJ_45 ----
          OBJ_44_OBJ_45.setText("Soci\u00e9t\u00e9");
          OBJ_44_OBJ_45.setName("OBJ_44_OBJ_45");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setName("INDETB");

          //---- OBJ_45_OBJ_46 ----
          OBJ_45_OBJ_46.setText("Code");
          OBJ_45_OBJ_46.setName("OBJ_45_OBJ_46");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setName("INDTYP");

          //---- OBJ_46_OBJ_47 ----
          OBJ_46_OBJ_47.setText("Ordre");
          OBJ_46_OBJ_47.setName("OBJ_46_OBJ_47");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(800, 480));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(786, 480));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Extraction des dettes");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- EDSNS ----
            EDSNS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EDSNS.setName("EDSNS");
            xTitledPanel1ContentContainer.add(EDSNS);
            EDSNS.setBounds(190, 119, 180, EDSNS.getPreferredSize().height);

            //---- OBJ_86_OBJ_86 ----
            OBJ_86_OBJ_86.setText("Sens \u00e9critures comptables");
            OBJ_86_OBJ_86.setName("OBJ_86_OBJ_86");
            xTitledPanel1ContentContainer.add(OBJ_86_OBJ_86);
            OBJ_86_OBJ_86.setBounds(20, 122, 165, 20);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("Collectifs \u00e0 s\u00e9lectionner"));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- EDC01 ----
              EDC01.setName("EDC01");
              panel2.add(EDC01);
              EDC01.setBounds(30, 37, 70, EDC01.getPreferredSize().height);

              //---- EDC02 ----
              EDC02.setName("EDC02");
              panel2.add(EDC02);
              EDC02.setBounds(30, 67, 70, EDC02.getPreferredSize().height);

              //---- EDC03 ----
              EDC03.setName("EDC03");
              panel2.add(EDC03);
              EDC03.setBounds(200, 37, 70, EDC03.getPreferredSize().height);

              //---- EDC04 ----
              EDC04.setName("EDC04");
              panel2.add(EDC04);
              EDC04.setBounds(200, 67, 70, EDC04.getPreferredSize().height);

              //---- EDC05 ----
              EDC05.setName("EDC05");
              panel2.add(EDC05);
              EDC05.setBounds(365, 37, 70, EDC05.getPreferredSize().height);

              //---- EDC06 ----
              EDC06.setName("EDC06");
              panel2.add(EDC06);
              EDC06.setBounds(365, 67, 70, EDC06.getPreferredSize().height);

              //---- EDC07 ----
              EDC07.setName("EDC07");
              panel2.add(EDC07);
              EDC07.setBounds(535, 37, 70, EDC07.getPreferredSize().height);

              //---- EDC08 ----
              EDC08.setName("EDC08");
              panel2.add(EDC08);
              EDC08.setBounds(535, 67, 70, EDC08.getPreferredSize().height);

              //---- EDS01 ----
              EDS01.setText("Solde");
              EDS01.setToolTipText("Cochez pour g\u00e9n\u00e9rer une dette d'un montant correspondant au solde du compte");
              EDS01.setName("EDS01");
              panel2.add(EDS01);
              EDS01.setBounds(105, 41, 80, 20);

              //---- EDS08 ----
              EDS08.setText("Solde");
              EDS08.setToolTipText("Cochez pour g\u00e9n\u00e9rer une dette d'un montant correspondant au solde du compte");
              EDS08.setName("EDS08");
              panel2.add(EDS08);
              EDS08.setBounds(610, 71, 80, 20);

              //---- EDS07 ----
              EDS07.setText("Solde");
              EDS07.setToolTipText("Cochez pour g\u00e9n\u00e9rer une dette d'un montant correspondant au solde du compte");
              EDS07.setName("EDS07");
              panel2.add(EDS07);
              EDS07.setBounds(610, 41, 80, 20);

              //---- EDS06 ----
              EDS06.setText("Solde");
              EDS06.setToolTipText("Cochez pour g\u00e9n\u00e9rer une dette d'un montant correspondant au solde du compte");
              EDS06.setName("EDS06");
              panel2.add(EDS06);
              EDS06.setBounds(440, 71, 80, 20);

              //---- EDS05 ----
              EDS05.setText("Solde");
              EDS05.setToolTipText("Cochez pour g\u00e9n\u00e9rer une dette d'un montant correspondant au solde du compte");
              EDS05.setName("EDS05");
              panel2.add(EDS05);
              EDS05.setBounds(440, 41, 80, 20);

              //---- EDS03 ----
              EDS03.setText("Solde");
              EDS03.setToolTipText("Cochez pour g\u00e9n\u00e9rer une dette d'un montant correspondant au solde du compte");
              EDS03.setName("EDS03");
              panel2.add(EDS03);
              EDS03.setBounds(270, 41, 80, 20);

              //---- EDS04 ----
              EDS04.setText("Solde");
              EDS04.setToolTipText("Cochez pour g\u00e9n\u00e9rer une dette d'un montant correspondant au solde du compte");
              EDS04.setName("EDS04");
              panel2.add(EDS04);
              EDS04.setBounds(270, 71, 80, 20);

              //---- EDS02 ----
              EDS02.setText("Solde");
              EDS02.setToolTipText("Cochez pour g\u00e9n\u00e9rer une dette d'un montant correspondant au solde du compte");
              EDS02.setName("EDS02");
              panel2.add(EDS02);
              EDS02.setBounds(105, 71, 80, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel2);
            panel2.setBounds(20, 280, 735, 120);

            //---- EDJ01_CHK ----
            EDJ01_CHK.setText("Tous les journaux");
            EDJ01_CHK.setName("EDJ01_CHK");
            EDJ01_CHK.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                EDJ01_CHKActionPerformed(e);
              }
            });
            xTitledPanel1ContentContainer.add(EDJ01_CHK);
            EDJ01_CHK.setBounds(20, 21, 160, EDJ01_CHK.getPreferredSize().height);

            //---- EDL01_CHK ----
            EDL01_CHK.setText("Tous les libell\u00e9s");
            EDL01_CHK.setName("EDL01_CHK");
            EDL01_CHK.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                EDL01_CHKActionPerformed(e);
              }
            });
            xTitledPanel1ContentContainer.add(EDL01_CHK);
            EDL01_CHK.setBounds(20, 78, 160, EDL01_CHK.getPreferredSize().height);

            //======== P_SEL0 ========
            {
              P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL0.setOpaque(false);
              P_SEL0.setName("P_SEL0");
              P_SEL0.setLayout(null);

              //---- OBJ_36_OBJ_36 ----
              OBJ_36_OBJ_36.setText("Codes journaux");
              OBJ_36_OBJ_36.setName("OBJ_36_OBJ_36");
              P_SEL0.add(OBJ_36_OBJ_36);
              OBJ_36_OBJ_36.setBounds(10, 15, 100, 20);

              //---- EDJ01 ----
              EDJ01.setName("EDJ01");
              P_SEL0.add(EDJ01);
              EDJ01.setBounds(160, 11, 30, EDJ01.getPreferredSize().height);

              //---- EDJ02 ----
              EDJ02.setName("EDJ02");
              P_SEL0.add(EDJ02);
              EDJ02.setBounds(195, 11, 30, EDJ02.getPreferredSize().height);

              //---- EDJ03 ----
              EDJ03.setName("EDJ03");
              P_SEL0.add(EDJ03);
              EDJ03.setBounds(230, 11, 30, EDJ03.getPreferredSize().height);

              //---- EDJ04 ----
              EDJ04.setName("EDJ04");
              P_SEL0.add(EDJ04);
              EDJ04.setBounds(260, 11, 30, EDJ04.getPreferredSize().height);

              //---- EDJ05 ----
              EDJ05.setName("EDJ05");
              P_SEL0.add(EDJ05);
              EDJ05.setBounds(295, 11, 30, EDJ05.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < P_SEL0.getComponentCount(); i++) {
                  Rectangle bounds = P_SEL0.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = P_SEL0.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                P_SEL0.setMinimumSize(preferredSize);
                P_SEL0.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(P_SEL0);
            P_SEL0.setBounds(190, 5, 365, 50);

            //======== P_SEL1 ========
            {
              P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL1.setOpaque(false);
              P_SEL1.setName("P_SEL1");
              P_SEL1.setLayout(null);

              //---- OBJ_85_OBJ_85 ----
              OBJ_85_OBJ_85.setText("Codes libell\u00e9s");
              OBJ_85_OBJ_85.setName("OBJ_85_OBJ_85");
              P_SEL1.add(OBJ_85_OBJ_85);
              OBJ_85_OBJ_85.setBounds(10, 15, 93, 20);

              //---- EDL01 ----
              EDL01.setName("EDL01");
              P_SEL1.add(EDL01);
              EDL01.setBounds(160, 11, 20, EDL01.getPreferredSize().height);

              //---- EDL02 ----
              EDL02.setName("EDL02");
              P_SEL1.add(EDL02);
              EDL02.setBounds(195, 11, 20, EDL02.getPreferredSize().height);

              //---- EDL03 ----
              EDL03.setName("EDL03");
              P_SEL1.add(EDL03);
              EDL03.setBounds(230, 11, 20, EDL03.getPreferredSize().height);

              //---- EDL04 ----
              EDL04.setName("EDL04");
              P_SEL1.add(EDL04);
              EDL04.setBounds(265, 11, 20, EDL04.getPreferredSize().height);

              //---- EDL05 ----
              EDL05.setName("EDL05");
              P_SEL1.add(EDL05);
              EDL05.setBounds(300, 11, 20, EDL05.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < P_SEL1.getComponentCount(); i++) {
                  Rectangle bounds = P_SEL1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = P_SEL1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                P_SEL1.setMinimumSize(preferredSize);
                P_SEL1.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(P_SEL1);
            P_SEL1.setBounds(190, 62, 365, 50);

            //---- xTitledSeparator1 ----
            xTitledSeparator1.setTitle("R\u00e8glements \u00e0 s\u00e9lectionner");
            xTitledSeparator1.setName("xTitledSeparator1");
            xTitledPanel1ContentContainer.add(xTitledSeparator1);
            xTitledSeparator1.setBounds(20, 165, 735, xTitledSeparator1.getPreferredSize().height);

            //---- EDR01_CHK ----
            EDR01_CHK.setText("Tous les r\u00e8glements");
            EDR01_CHK.setName("EDR01_CHK");
            EDR01_CHK.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                EDR01_CHKActionPerformed(e);
              }
            });
            xTitledPanel1ContentContainer.add(EDR01_CHK);
            EDR01_CHK.setBounds(20, 200, 160, EDR01_CHK.getPreferredSize().height);

            //======== P_SEL2 ========
            {
              P_SEL2.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL2.setOpaque(false);
              P_SEL2.setName("P_SEL2");
              P_SEL2.setLayout(null);

              //---- EDR01 ----
              EDR01.setName("EDR01");
              P_SEL2.add(EDR01);
              EDR01.setBounds(20, 10, 30, EDR01.getPreferredSize().height);

              //---- EDR02 ----
              EDR02.setName("EDR02");
              P_SEL2.add(EDR02);
              EDR02.setBounds(53, 10, 30, EDR02.getPreferredSize().height);

              //---- EDR03 ----
              EDR03.setName("EDR03");
              P_SEL2.add(EDR03);
              EDR03.setBounds(86, 10, 30, EDR03.getPreferredSize().height);

              //---- EDR04 ----
              EDR04.setName("EDR04");
              P_SEL2.add(EDR04);
              EDR04.setBounds(119, 10, 30, EDR04.getPreferredSize().height);

              //---- EDR05 ----
              EDR05.setName("EDR05");
              P_SEL2.add(EDR05);
              EDR05.setBounds(152, 10, 30, EDR05.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < P_SEL2.getComponentCount(); i++) {
                  Rectangle bounds = P_SEL2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = P_SEL2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                P_SEL2.setMinimumSize(preferredSize);
                P_SEL2.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(P_SEL2);
            P_SEL2.setBounds(190, 185, 210, 45);

            //---- OBJ_90_OBJ_90 ----
            OBJ_90_OBJ_90.setText("R\u00e8glement par d\u00e9faut");
            OBJ_90_OBJ_90.setName("OBJ_90_OBJ_90");
            xTitledPanel1ContentContainer.add(OBJ_90_OBJ_90);
            OBJ_90_OBJ_90.setBounds(20, 240, 132, 20);

            //---- EDRGL ----
            EDRGL.setName("EDRGL");
            xTitledPanel1ContentContainer.add(EDRGL);
            EDRGL.setBounds(190, 235, 30, EDRGL.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 774, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 452, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44_OBJ_45;
  private XRiTextField INDETB;
  private JLabel OBJ_45_OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_46_OBJ_47;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private XRiComboBox EDSNS;
  private JLabel OBJ_86_OBJ_86;
  private JPanel panel2;
  private XRiTextField EDC01;
  private XRiTextField EDC02;
  private XRiTextField EDC03;
  private XRiTextField EDC04;
  private XRiTextField EDC05;
  private XRiTextField EDC06;
  private XRiTextField EDC07;
  private XRiTextField EDC08;
  private XRiCheckBox EDS01;
  private XRiCheckBox EDS08;
  private XRiCheckBox EDS07;
  private XRiCheckBox EDS06;
  private XRiCheckBox EDS05;
  private XRiCheckBox EDS03;
  private XRiCheckBox EDS04;
  private XRiCheckBox EDS02;
  private JCheckBox EDJ01_CHK;
  private JCheckBox EDL01_CHK;
  private JPanel P_SEL0;
  private JLabel OBJ_36_OBJ_36;
  private XRiTextField EDJ01;
  private XRiTextField EDJ02;
  private XRiTextField EDJ03;
  private XRiTextField EDJ04;
  private XRiTextField EDJ05;
  private JPanel P_SEL1;
  private JLabel OBJ_85_OBJ_85;
  private XRiTextField EDL01;
  private XRiTextField EDL02;
  private XRiTextField EDL03;
  private XRiTextField EDL04;
  private XRiTextField EDL05;
  private JXTitledSeparator xTitledSeparator1;
  private JCheckBox EDR01_CHK;
  private JPanel P_SEL2;
  private XRiTextField EDR01;
  private XRiTextField EDR02;
  private XRiTextField EDR03;
  private XRiTextField EDR04;
  private XRiTextField EDR05;
  private JLabel OBJ_90_OBJ_90;
  private XRiTextField EDRGL;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
