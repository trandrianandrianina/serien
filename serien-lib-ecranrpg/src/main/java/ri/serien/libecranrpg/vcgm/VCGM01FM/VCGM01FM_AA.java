
package ri.serien.libecranrpg.vcgm.VCGM01FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM01FM_AA extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM01FM_AA(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    AAV06.setValeursSelection("X", " ");
    AAV05.setValeursSelection("X", " ");
    AAV04.setValeursSelection("X", " ");
    AAV03.setValeursSelection("X", " ");
    AAV02.setValeursSelection("X", " ");
    AAV01.setValeursSelection("X", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDTYP@")).trim());
    INDIND.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDIND@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    AAV06.setVisible(lexique.isTrue("45"));
    // AAV06.setSelected(lexique.HostFieldGetData("AAV06").equalsIgnoreCase("X"));
    AAV05.setVisible(lexique.isTrue("45"));
    // AAV05.setSelected(lexique.HostFieldGetData("AAV05").equalsIgnoreCase("X"));
    AAV04.setVisible(lexique.isTrue("45"));
    // AAV04.setSelected(lexique.HostFieldGetData("AAV04").equalsIgnoreCase("X"));
    AAV03.setVisible(lexique.isTrue("45"));
    // AAV03.setSelected(lexique.HostFieldGetData("AAV03").equalsIgnoreCase("X"));
    AAV02.setVisible(lexique.isTrue("45"));
    // AAV02.setSelected(lexique.HostFieldGetData("AAV02").equalsIgnoreCase("X"));
    AAV01.setVisible(lexique.isTrue("45"));
    // AAV01.setSelected(lexique.HostFieldGetData("AAV01").equalsIgnoreCase("X"));
    OBJ_49_OBJ_49.setVisible(lexique.isTrue("45"));
    OBJ_50_OBJ_50.setVisible(lexique.isTrue("45"));
    OBJ_56_OBJ_56.setVisible(lexique.isTrue("45"));
    OBJ_55_OBJ_55.setVisible(lexique.isTrue("45"));
    OBJ_54_OBJ_54.setVisible(lexique.isTrue("45"));
    OBJ_53_OBJ_53.setVisible(lexique.isTrue("45"));
    OBJ_52_OBJ_52.setVisible(lexique.isTrue("45"));
    OBJ_51_OBJ_51.setVisible(lexique.isTrue("45"));
    AAEDT.setVisible(lexique.isTrue("N45"));
    OBJ_47_OBJ_47.setVisible(lexique.isTrue("N45"));
    ;
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (AAV06.isSelected())
    // lexique.HostFieldPutData("AAV06", 0, "X");
    // else
    // lexique.HostFieldPutData("AAV06", 0, " ");
    // if (AAV05.isSelected())
    // lexique.HostFieldPutData("AAV05", 0, "X");
    // else
    // lexique.HostFieldPutData("AAV05", 0, " ");
    // if (AAV04.isSelected())
    // lexique.HostFieldPutData("AAV04", 0, "X");
    // else
    // lexique.HostFieldPutData("AAV04", 0, " ");
    // if (AAV03.isSelected())
    // lexique.HostFieldPutData("AAV03", 0, "X");
    // else
    // lexique.HostFieldPutData("AAV03", 0, " ");
    // if (AAV02.isSelected())
    // lexique.HostFieldPutData("AAV02", 0, "X");
    // else
    // lexique.HostFieldPutData("AAV02", 0, " ");
    // if (AAV01.isSelected())
    // lexique.HostFieldPutData("AAV01", 0, "X");
    // else
    // lexique.HostFieldPutData("AAV01", 0, " ");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44_OBJ_45 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_45_OBJ_46 = new JLabel();
    INDTYP = new RiZoneSortie();
    OBJ_46_OBJ_47 = new JLabel();
    INDIND = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    AALIB = new XRiTextField();
    OBJ_49_OBJ_49 = new JLabel();
    AAEDT = new XRiTextField();
    OBJ_47_OBJ_47 = new JLabel();
    OBJ_45_OBJ_45 = new JLabel();
    OBJ_50_OBJ_50 = new JLabel();
    OBJ_51_OBJ_51 = new JLabel();
    OBJ_52_OBJ_52 = new JLabel();
    OBJ_53_OBJ_53 = new JLabel();
    OBJ_54_OBJ_54 = new JLabel();
    OBJ_55_OBJ_55 = new JLabel();
    OBJ_56_OBJ_56 = new JLabel();
    AAV01 = new XRiCheckBox();
    AAV02 = new XRiCheckBox();
    AAV03 = new XRiCheckBox();
    AAV04 = new XRiCheckBox();
    AAV05 = new XRiCheckBox();
    AAV06 = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_44_OBJ_45 ----
          OBJ_44_OBJ_45.setText("Soci\u00e9t\u00e9");
          OBJ_44_OBJ_45.setName("OBJ_44_OBJ_45");
          p_tete_gauche.add(OBJ_44_OBJ_45);
          OBJ_44_OBJ_45.setBounds(5, 3, 52, 18);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(70, 0, 40, INDETB.getPreferredSize().height);

          //---- OBJ_45_OBJ_46 ----
          OBJ_45_OBJ_46.setText("Code");
          OBJ_45_OBJ_46.setName("OBJ_45_OBJ_46");
          p_tete_gauche.add(OBJ_45_OBJ_46);
          OBJ_45_OBJ_46.setBounds(140, 3, 45, 18);

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setOpaque(false);
          INDTYP.setText("@INDTYP@");
          INDTYP.setName("INDTYP");
          p_tete_gauche.add(INDTYP);
          INDTYP.setBounds(195, 0, 34, INDTYP.getPreferredSize().height);

          //---- OBJ_46_OBJ_47 ----
          OBJ_46_OBJ_47.setText("Ordre");
          OBJ_46_OBJ_47.setName("OBJ_46_OBJ_47");
          p_tete_gauche.add(OBJ_46_OBJ_47);
          OBJ_46_OBJ_47.setBounds(260, 3, 45, 18);

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setOpaque(false);
          INDIND.setText("@INDIND@");
          INDIND.setName("INDIND");
          p_tete_gauche.add(INDIND);
          INDIND.setBounds(310, 0, 60, INDIND.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_tete_gauche.getComponentCount(); i++) {
              Rectangle bounds = p_tete_gauche.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_tete_gauche.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_tete_gauche.setMinimumSize(preferredSize);
            p_tete_gauche.setPreferredSize(preferredSize);
          }
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(540, 280));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Axes analytiques");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

            //---- AALIB ----
            AALIB.setComponentPopupMenu(BTD);
            AALIB.setName("AALIB");

            //---- OBJ_49_OBJ_49 ----
            OBJ_49_OBJ_49.setText("Possibilit\u00e9s d'utilisation");
            OBJ_49_OBJ_49.setName("OBJ_49_OBJ_49");

            //---- AAEDT ----
            AAEDT.setComponentPopupMenu(BTD);
            AAEDT.setName("AAEDT");

            //---- OBJ_47_OBJ_47 ----
            OBJ_47_OBJ_47.setText("Edition");
            OBJ_47_OBJ_47.setName("OBJ_47_OBJ_47");

            //---- OBJ_45_OBJ_45 ----
            OBJ_45_OBJ_45.setText("Libell\u00e9");
            OBJ_45_OBJ_45.setName("OBJ_45_OBJ_45");

            //---- OBJ_50_OBJ_50 ----
            OBJ_50_OBJ_50.setText("Axes");
            OBJ_50_OBJ_50.setName("OBJ_50_OBJ_50");

            //---- OBJ_51_OBJ_51 ----
            OBJ_51_OBJ_51.setText("1");
            OBJ_51_OBJ_51.setName("OBJ_51_OBJ_51");

            //---- OBJ_52_OBJ_52 ----
            OBJ_52_OBJ_52.setText("2");
            OBJ_52_OBJ_52.setName("OBJ_52_OBJ_52");

            //---- OBJ_53_OBJ_53 ----
            OBJ_53_OBJ_53.setText("3");
            OBJ_53_OBJ_53.setName("OBJ_53_OBJ_53");

            //---- OBJ_54_OBJ_54 ----
            OBJ_54_OBJ_54.setText("4");
            OBJ_54_OBJ_54.setName("OBJ_54_OBJ_54");

            //---- OBJ_55_OBJ_55 ----
            OBJ_55_OBJ_55.setText("5");
            OBJ_55_OBJ_55.setName("OBJ_55_OBJ_55");

            //---- OBJ_56_OBJ_56 ----
            OBJ_56_OBJ_56.setText("6");
            OBJ_56_OBJ_56.setName("OBJ_56_OBJ_56");

            //---- AAV01 ----
            AAV01.setText("");
            AAV01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            AAV01.setName("AAV01");

            //---- AAV02 ----
            AAV02.setText("");
            AAV02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            AAV02.setName("AAV02");

            //---- AAV03 ----
            AAV03.setText("");
            AAV03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            AAV03.setName("AAV03");

            //---- AAV04 ----
            AAV04.setText("");
            AAV04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            AAV04.setName("AAV04");

            //---- AAV05 ----
            AAV05.setText("");
            AAV05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            AAV05.setName("AAV05");

            //---- AAV06 ----
            AAV06.setText("");
            AAV06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            AAV06.setName("AAV06");

            GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
            xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
            xTitledPanel1ContentContainerLayout.setHorizontalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(12, 12, 12)
                      .addComponent(OBJ_45_OBJ_45, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
                      .addGap(12, 12, 12)
                      .addComponent(AALIB, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(12, 12, 12)
                      .addComponent(OBJ_47_OBJ_47, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                      .addGap(12, 12, 12)
                      .addComponent(AAEDT, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(67, 67, 67)
                      .addComponent(OBJ_49_OBJ_49, GroupLayout.PREFERRED_SIZE, 168, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(12, 12, 12)
                      .addComponent(OBJ_50_OBJ_50, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                      .addGap(22, 22, 22)
                      .addComponent(OBJ_51_OBJ_51, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                      .addGap(12, 12, 12)
                      .addComponent(OBJ_52_OBJ_52, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(OBJ_53_OBJ_53, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(OBJ_54_OBJ_54, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                      .addGap(12, 12, 12)
                      .addComponent(OBJ_55_OBJ_55, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(OBJ_56_OBJ_56, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(61, 61, 61)
                      .addComponent(AAV01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(12, 12, 12)
                      .addComponent(AAV02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(AAV03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(AAV04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(12, 12, 12)
                      .addComponent(AAV05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(12, 12, 12)
                      .addComponent(AAV06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addContainerGap(132, Short.MAX_VALUE))
            );
            xTitledPanel1ContentContainerLayout.setVerticalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(13, 13, 13)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_45_OBJ_45, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(AALIB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(12, 12, 12)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_47_OBJ_47, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(AAEDT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(18, 18, 18)
                  .addComponent(OBJ_49_OBJ_49)
                  .addGap(4, 4, 4)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_50_OBJ_50, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_51_OBJ_51, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_52_OBJ_52, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_53_OBJ_53, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_54_OBJ_54, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_55_OBJ_55, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_56_OBJ_56, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(AAV01, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(AAV02, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(AAV03, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(AAV04, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(AAV05, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(AAV06, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addContainerGap(59, Short.MAX_VALUE))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44_OBJ_45;
  private RiZoneSortie INDETB;
  private JLabel OBJ_45_OBJ_46;
  private RiZoneSortie INDTYP;
  private JLabel OBJ_46_OBJ_47;
  private RiZoneSortie INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private XRiTextField AALIB;
  private JLabel OBJ_49_OBJ_49;
  private XRiTextField AAEDT;
  private JLabel OBJ_47_OBJ_47;
  private JLabel OBJ_45_OBJ_45;
  private JLabel OBJ_50_OBJ_50;
  private JLabel OBJ_51_OBJ_51;
  private JLabel OBJ_52_OBJ_52;
  private JLabel OBJ_53_OBJ_53;
  private JLabel OBJ_54_OBJ_54;
  private JLabel OBJ_55_OBJ_55;
  private JLabel OBJ_56_OBJ_56;
  private XRiCheckBox AAV01;
  private XRiCheckBox AAV02;
  private XRiCheckBox AAV03;
  private XRiCheckBox AAV04;
  private XRiCheckBox AAV05;
  private XRiCheckBox AAV06;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
