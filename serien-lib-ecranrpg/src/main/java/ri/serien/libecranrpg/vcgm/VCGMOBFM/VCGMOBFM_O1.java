
package ri.serien.libecranrpg.vcgm.VCGMOBFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGMOBFM_O1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGMOBFM_O1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    riMenu2.setEnabled(lexique.isTrue("N97"));
    riSousMenu6.setEnabled(lexique.isTrue("N97"));
    riSousMenu7.setEnabled(lexique.isTrue("N97"));
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Notes"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OB11 = new XRiTextField();
    OB12 = new XRiTextField();
    OB13 = new XRiTextField();
    OB14 = new XRiTextField();
    OB15 = new XRiTextField();
    OB16 = new XRiTextField();
    OB17 = new XRiTextField();
    OB18 = new XRiTextField();
    OB1 = new XRiTextField();
    OB2 = new XRiTextField();
    OB3 = new XRiTextField();
    OB4 = new XRiTextField();
    OB5 = new XRiTextField();
    OB6 = new XRiTextField();
    OB7 = new XRiTextField();
    OB8 = new XRiTextField();
    OB9 = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(665, 420));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Affacturage");
            riSousMenu_bt6.setToolTipText("Affacturage");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Journal banque effet");
            riSousMenu_bt7.setToolTipText("Journal banque effet");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Fiche de r\u00e9vision"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OB11 ----
          OB11.setBorder(null);
          OB11.setFont(new Font("Courier New", Font.PLAIN, 12));
          OB11.setName("OB11");
          panel1.add(OB11);
          OB11.setBounds(15, 210, 435, 24);

          //---- OB12 ----
          OB12.setBorder(null);
          OB12.setFont(new Font("Courier New", Font.PLAIN, 12));
          OB12.setName("OB12");
          panel1.add(OB12);
          OB12.setBounds(15, 230, 435, 24);

          //---- OB13 ----
          OB13.setBorder(null);
          OB13.setFont(new Font("Courier New", Font.PLAIN, 12));
          OB13.setName("OB13");
          panel1.add(OB13);
          OB13.setBounds(15, 250, 435, 24);

          //---- OB14 ----
          OB14.setBorder(null);
          OB14.setFont(new Font("Courier New", Font.PLAIN, 12));
          OB14.setName("OB14");
          panel1.add(OB14);
          OB14.setBounds(15, 270, 435, 24);

          //---- OB15 ----
          OB15.setBorder(null);
          OB15.setFont(new Font("Courier New", Font.PLAIN, 12));
          OB15.setName("OB15");
          panel1.add(OB15);
          OB15.setBounds(15, 290, 435, 24);

          //---- OB16 ----
          OB16.setBorder(null);
          OB16.setFont(new Font("Courier New", Font.PLAIN, 12));
          OB16.setName("OB16");
          panel1.add(OB16);
          OB16.setBounds(15, 310, 435, 24);

          //---- OB17 ----
          OB17.setBorder(null);
          OB17.setFont(new Font("Courier New", Font.PLAIN, 12));
          OB17.setName("OB17");
          panel1.add(OB17);
          OB17.setBounds(15, 330, 435, 24);

          //---- OB18 ----
          OB18.setBorder(null);
          OB18.setFont(new Font("Courier New", Font.PLAIN, 12));
          OB18.setName("OB18");
          panel1.add(OB18);
          OB18.setBounds(15, 350, 435, 24);

          //---- OB1 ----
          OB1.setBorder(null);
          OB1.setFont(new Font("Courier New", Font.PLAIN, 12));
          OB1.setName("OB1");
          panel1.add(OB1);
          OB1.setBounds(15, 30, 435, 24);

          //---- OB2 ----
          OB2.setBorder(null);
          OB2.setFont(new Font("Courier New", Font.PLAIN, 12));
          OB2.setName("OB2");
          panel1.add(OB2);
          OB2.setBounds(15, 50, 435, 24);

          //---- OB3 ----
          OB3.setBorder(null);
          OB3.setFont(new Font("Courier New", Font.PLAIN, 12));
          OB3.setName("OB3");
          panel1.add(OB3);
          OB3.setBounds(15, 70, 435, 24);

          //---- OB4 ----
          OB4.setBorder(null);
          OB4.setFont(new Font("Courier New", Font.PLAIN, 12));
          OB4.setName("OB4");
          panel1.add(OB4);
          OB4.setBounds(15, 90, 435, 24);

          //---- OB5 ----
          OB5.setBorder(null);
          OB5.setFont(new Font("Courier New", Font.PLAIN, 12));
          OB5.setName("OB5");
          panel1.add(OB5);
          OB5.setBounds(15, 110, 435, 24);

          //---- OB6 ----
          OB6.setBorder(null);
          OB6.setFont(new Font("Courier New", Font.PLAIN, 12));
          OB6.setName("OB6");
          panel1.add(OB6);
          OB6.setBounds(15, 130, 435, 24);

          //---- OB7 ----
          OB7.setBorder(null);
          OB7.setFont(new Font("Courier New", Font.PLAIN, 12));
          OB7.setName("OB7");
          panel1.add(OB7);
          OB7.setBounds(15, 150, 435, 24);

          //---- OB8 ----
          OB8.setBorder(null);
          OB8.setFont(new Font("Courier New", Font.PLAIN, 12));
          OB8.setName("OB8");
          panel1.add(OB8);
          OB8.setBounds(15, 170, 435, 24);

          //---- OB9 ----
          OB9.setBorder(null);
          OB9.setFont(new Font("Courier New", Font.PLAIN, 12));
          OB9.setName("OB9");
          panel1.add(OB9);
          OB9.setBounds(15, 190, 435, 24);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(15, 15, 465, 390);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField OB11;
  private XRiTextField OB12;
  private XRiTextField OB13;
  private XRiTextField OB14;
  private XRiTextField OB15;
  private XRiTextField OB16;
  private XRiTextField OB17;
  private XRiTextField OB18;
  private XRiTextField OB1;
  private XRiTextField OB2;
  private XRiTextField OB3;
  private XRiTextField OB4;
  private XRiTextField OB5;
  private XRiTextField OB6;
  private XRiTextField OB7;
  private XRiTextField OB8;
  private XRiTextField OB9;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
