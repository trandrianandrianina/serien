
package ri.serien.libecranrpg.vcgm.VCGM11FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGM11FM_L4 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] L1SNS_Value = { "", "D", "C", };
  private String[] L1SNS_Title = { "", "Débit", "Crédit", };
  private boolean dup = false;
  
  public VCGM11FM_L4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    // setRefreshPanelUnderMe(false);
    
    // Ajout
    initDiverses();
    L1SNS.setValeurs(L1SNS_Value, L1SNS_Title);
    L1VTL.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    W1SNS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W1SNS@")).trim());
    OBJ_61_OBJ_61.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W2SNS@")).trim());
    OBJ_27_OBJ_27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DV@")).trim());
    OBJ_28_OBJ_28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TAUX@")).trim());
    OBJ_29_OBJ_29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MONT@")).trim());
    OBJ_43_OBJ_43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CHPLB2@")).trim());
    OBJ_24_OBJ_24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMTT@")).trim());
    OBJ_42_OBJ_42.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CHPLB1@")).trim());
    OBJ_44_OBJ_44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CHPRG1@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    


    
    
    dup = lexique.HostFieldGetData("L1DUPX").trim().equals("*");
    OBJ_61_OBJ_61.setVisible(lexique.isPresent("USOLDE"));
    W1SNS.setVisible(lexique.isPresent("W1SNS"));
    OBJ_44_OBJ_44.setVisible(lexique.isPresent("CHPRG1"));
    OBJ_45_OBJ_45.setVisible(lexique.isPresent("CHPRG2"));
    OBJ_30_OBJ_30.setVisible(lexique.isPresent("L1QTE"));
    OBJ_42_OBJ_42.setVisible(lexique.isPresent("CHPLB1"));
    OBJ_24_OBJ_24.setVisible(lexique.isPresent("LIBMTT"));
    OBJ_43_OBJ_43.setVisible(
        !interpreteurD.analyseExpression("@CHPLB2@").trim().equalsIgnoreCase("................") & lexique.isPresent("CHPLB2"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Ligne de folio"));
    // );
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    if (dup) {
      lexique.HostFieldPutData("L1DUPX", 0, "*");
    }
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(18, 33);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(17, 9);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", true);
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "DUPLICATE", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_79ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    if (dup) {
      OBJ_19.setForeground((Color) lexique.getSystemDefaultColor("Label.Foreground"));
    }
    else {
      OBJ_19.setForeground(Color.GREEN);
    }
    dup = !dup;
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    WCOD = new XRiTextField();
    L1NLI = new XRiTextField();
    L1NCGX = new XRiTextField();
    L1NCA = new XRiTextField();
    L1DTJ = new XRiTextField();
    LIBCPT = new XRiTextField();
    A1CPL = new XRiTextField();
    A1RUE = new XRiTextField();
    A1CDP = new XRiTextField();
    A1VIL = new XRiTextField();
    USOLDE = new XRiTextField();
    OBJ_57_OBJ_57 = new JLabel();
    W1SOLD = new XRiTextField();
    OBJ_78_OBJ_78 = new JLabel();
    W1SNS = new RiZoneSortie();
    OBJ_61_OBJ_61 = new JLabel();
    OBJ_25_OBJ_26 = new JLabel();
    OBJ_25_OBJ_27 = new JLabel();
    OBJ_25_OBJ_28 = new JLabel();
    OBJ_25_OBJ_29 = new JLabel();
    panel4 = new JPanel();
    L1MTT = new XRiTextField();
    L1SNS = new XRiComboBox();
    L1DEV = new XRiTextField();
    OBJ_27_OBJ_27 = new JLabel();
    WCHGX = new XRiTextField();
    OBJ_28_OBJ_28 = new JLabel();
    L1MTD = new XRiTextField();
    OBJ_29_OBJ_29 = new JLabel();
    L1QTE = new XRiTextField();
    OBJ_30_OBJ_30 = new JLabel();
    L1CLB = new XRiTextField();
    L1PCE = new XRiTextField();
    L1LIB1 = new XRiTextField();
    L1LIB2 = new XRiTextField();
    W1RGL = new XRiTextField();
    L1NCPX = new XRiTextField();
    WLNCPT = new XRiTextField();
    L1VTL = new XRiCheckBox();
    OBJ_66_OBJ_66 = new JLabel();
    L1RFC = new XRiTextField();
    OBJ_43_OBJ_43 = new JLabel();
    OBJ_24_OBJ_24 = new JLabel();
    OBJ_42_OBJ_42 = new JLabel();
    OBJ_55_OBJ_55 = new JLabel();
    OBJ_41_OBJ_41 = new JLabel();
    OBJ_45_OBJ_45 = new JLabel();
    OBJ_25_OBJ_25 = new JLabel();
    OBJ_44_OBJ_44 = new JLabel();
    OBJ_40_OBJ_40 = new JLabel();
    OBJ_19 = new JButton();
    W1LIBS = new XRiTextField();
    L1DTRX = new XRiTextField();
    Lbl_datedefacture = new JLabel();
    Lbl_referencefacture = new JLabel();
    L1RPE = new XRiTextField();
    W1ECHX = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_14 = new JMenuItem();
    OBJ_15 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_79 = new JButton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1200, 255));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setAutoscrolls(true);
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 180));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setAutoscrolls(true);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Vue du compte");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Plan comptable");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("For\u00e7age en devises");
              riSousMenu_bt8.setToolTipText("For\u00e7age en saisie sur comptes en devises");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Demande de lettrage");
              riSousMenu_bt9.setToolTipText("Demande de lettrage en cours de saisie");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Bloc-notes");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);

            //======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");

              //---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Documents li\u00e9s");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed(e);
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder(""));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- WCOD ----
          WCOD.setComponentPopupMenu(BTD);
          WCOD.setName("WCOD");
          panel2.add(WCOD);
          WCOD.setBounds(15, 30, 24, WCOD.getPreferredSize().height);

          //---- L1NLI ----
          L1NLI.setComponentPopupMenu(BTD);
          L1NLI.setName("L1NLI");
          panel2.add(L1NLI);
          L1NLI.setBounds(55, 30, 44, L1NLI.getPreferredSize().height);

          //---- L1NCGX ----
          L1NCGX.setComponentPopupMenu(BTD);
          L1NCGX.setName("L1NCGX");
          panel2.add(L1NCGX);
          L1NCGX.setBounds(120, 30, 60, L1NCGX.getPreferredSize().height);

          //---- L1NCA ----
          L1NCA.setComponentPopupMenu(BTD);
          L1NCA.setName("L1NCA");
          panel2.add(L1NCA);
          L1NCA.setBounds(185, 30, 60, L1NCA.getPreferredSize().height);

          //---- L1DTJ ----
          L1DTJ.setComponentPopupMenu(BTD);
          L1DTJ.setName("L1DTJ");
          panel2.add(L1DTJ);
          L1DTJ.setBounds(295, 30, 28, L1DTJ.getPreferredSize().height);

          //---- LIBCPT ----
          LIBCPT.setComponentPopupMenu(BTD);
          LIBCPT.setEditable(false);
          LIBCPT.setName("LIBCPT");
          panel2.add(LIBCPT);
          LIBCPT.setBounds(15, 60, 310, LIBCPT.getPreferredSize().height);

          //---- A1CPL ----
          A1CPL.setComponentPopupMenu(BTD);
          A1CPL.setName("A1CPL");
          panel2.add(A1CPL);
          A1CPL.setBounds(15, 90, 310, A1CPL.getPreferredSize().height);

          //---- A1RUE ----
          A1RUE.setComponentPopupMenu(BTD);
          A1RUE.setName("A1RUE");
          panel2.add(A1RUE);
          A1RUE.setBounds(15, 125, 310, A1RUE.getPreferredSize().height);

          //---- A1CDP ----
          A1CDP.setComponentPopupMenu(BTD);
          A1CDP.setName("A1CDP");
          panel2.add(A1CDP);
          A1CDP.setBounds(15, 155, 52, A1CDP.getPreferredSize().height);

          //---- A1VIL ----
          A1VIL.setComponentPopupMenu(BTD);
          A1VIL.setName("A1VIL");
          panel2.add(A1VIL);
          A1VIL.setBounds(75, 155, 250, A1VIL.getPreferredSize().height);

          //---- USOLDE ----
          USOLDE.setComponentPopupMenu(BTD);
          USOLDE.setName("USOLDE");
          panel2.add(USOLDE);
          USOLDE.setBounds(155, 185, 116, USOLDE.getPreferredSize().height);

          //---- OBJ_57_OBJ_57 ----
          OBJ_57_OBJ_57.setText("Solde du compte");
          OBJ_57_OBJ_57.setName("OBJ_57_OBJ_57");
          panel2.add(OBJ_57_OBJ_57);
          OBJ_57_OBJ_57.setBounds(15, 188, 130, 23);

          //---- W1SOLD ----
          W1SOLD.setComponentPopupMenu(BTD);
          W1SOLD.setName("W1SOLD");
          panel2.add(W1SOLD);
          W1SOLD.setBounds(155, 185, 84, W1SOLD.getPreferredSize().height);

          //---- OBJ_78_OBJ_78 ----
          OBJ_78_OBJ_78.setText("Le");
          OBJ_78_OBJ_78.setName("OBJ_78_OBJ_78");
          panel2.add(OBJ_78_OBJ_78);
          OBJ_78_OBJ_78.setBounds(265, 35, 18, 22);

          //---- W1SNS ----
          W1SNS.setText("@W1SNS@");
          W1SNS.setHorizontalAlignment(SwingConstants.RIGHT);
          W1SNS.setName("W1SNS");
          panel2.add(W1SNS);
          W1SNS.setBounds(285, 190, 40, 20);

          //---- OBJ_61_OBJ_61 ----
          OBJ_61_OBJ_61.setText("@W2SNS@");
          OBJ_61_OBJ_61.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_61_OBJ_61.setName("OBJ_61_OBJ_61");
          panel2.add(OBJ_61_OBJ_61);
          OBJ_61_OBJ_61.setBounds(290, 190, 30, 20);

          //---- OBJ_25_OBJ_26 ----
          OBJ_25_OBJ_26.setText("C");
          OBJ_25_OBJ_26.setName("OBJ_25_OBJ_26");
          panel2.add(OBJ_25_OBJ_26);
          OBJ_25_OBJ_26.setBounds(20, 5, 20, 23);

          //---- OBJ_25_OBJ_27 ----
          OBJ_25_OBJ_27.setText("Ligne");
          OBJ_25_OBJ_27.setName("OBJ_25_OBJ_27");
          panel2.add(OBJ_25_OBJ_27);
          OBJ_25_OBJ_27.setBounds(60, 5, 34, 23);

          //---- OBJ_25_OBJ_28 ----
          OBJ_25_OBJ_28.setText("Compte");
          OBJ_25_OBJ_28.setName("OBJ_25_OBJ_28");
          panel2.add(OBJ_25_OBJ_28);
          OBJ_25_OBJ_28.setBounds(120, 5, 60, 23);

          //---- OBJ_25_OBJ_29 ----
          OBJ_25_OBJ_29.setText("Auxiliaire");
          OBJ_25_OBJ_29.setName("OBJ_25_OBJ_29");
          panel2.add(OBJ_25_OBJ_29);
          OBJ_25_OBJ_29.setBounds(185, 5, 60, 23);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel2);
        panel2.setBounds(10, 10, 350, 235);

        //======== panel4 ========
        {
          panel4.setBorder(new TitledBorder(""));
          panel4.setOpaque(false);
          panel4.setName("panel4");
          panel4.setLayout(null);

          //---- L1MTT ----
          L1MTT.setComponentPopupMenu(BTD);
          L1MTT.setName("L1MTT");
          panel4.add(L1MTT);
          L1MTT.setBounds(30, 45, 100, L1MTT.getPreferredSize().height);

          //---- L1SNS ----
          L1SNS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1SNS.setName("L1SNS");
          panel4.add(L1SNS);
          L1SNS.setBounds(130, 45, 81, L1SNS.getPreferredSize().height);

          //---- L1DEV ----
          L1DEV.setComponentPopupMenu(BTD);
          L1DEV.setName("L1DEV");
          panel4.add(L1DEV);
          L1DEV.setBounds(270, 45, 40, L1DEV.getPreferredSize().height);

          //---- OBJ_27_OBJ_27 ----
          OBJ_27_OBJ_27.setText("@DV@");
          OBJ_27_OBJ_27.setName("OBJ_27_OBJ_27");
          panel4.add(OBJ_27_OBJ_27);
          OBJ_27_OBJ_27.setBounds(270, 15, 28, 23);

          //---- WCHGX ----
          WCHGX.setComponentPopupMenu(BTD);
          WCHGX.setName("WCHGX");
          panel4.add(WCHGX);
          WCHGX.setBounds(310, 45, 90, WCHGX.getPreferredSize().height);

          //---- OBJ_28_OBJ_28 ----
          OBJ_28_OBJ_28.setText("@TAUX@");
          OBJ_28_OBJ_28.setName("OBJ_28_OBJ_28");
          panel4.add(OBJ_28_OBJ_28);
          OBJ_28_OBJ_28.setBounds(310, 15, 52, 23);

          //---- L1MTD ----
          L1MTD.setComponentPopupMenu(BTD);
          L1MTD.setName("L1MTD");
          panel4.add(L1MTD);
          L1MTD.setBounds(405, 45, 100, L1MTD.getPreferredSize().height);

          //---- OBJ_29_OBJ_29 ----
          OBJ_29_OBJ_29.setText("@MONT@");
          OBJ_29_OBJ_29.setName("OBJ_29_OBJ_29");
          panel4.add(OBJ_29_OBJ_29);
          OBJ_29_OBJ_29.setBounds(405, 15, 97, 23);

          //---- L1QTE ----
          L1QTE.setComponentPopupMenu(BTD);
          L1QTE.setName("L1QTE");
          panel4.add(L1QTE);
          L1QTE.setBounds(520, 40, 100, L1QTE.getPreferredSize().height);

          //---- OBJ_30_OBJ_30 ----
          OBJ_30_OBJ_30.setText("Quantit\u00e9");
          OBJ_30_OBJ_30.setName("OBJ_30_OBJ_30");
          panel4.add(OBJ_30_OBJ_30);
          OBJ_30_OBJ_30.setBounds(510, 15, 52, 23);

          //---- L1CLB ----
          L1CLB.setComponentPopupMenu(BTD);
          L1CLB.setName("L1CLB");
          panel4.add(L1CLB);
          L1CLB.setBounds(30, 105, 20, L1CLB.getPreferredSize().height);

          //---- L1PCE ----
          L1PCE.setComponentPopupMenu(BTD);
          L1PCE.setName("L1PCE");
          panel4.add(L1PCE);
          L1PCE.setBounds(55, 105, 68, L1PCE.getPreferredSize().height);

          //---- L1LIB1 ----
          L1LIB1.setComponentPopupMenu(BTD);
          L1LIB1.setName("L1LIB1");
          panel4.add(L1LIB1);
          L1LIB1.setBounds(130, 105, 110, L1LIB1.getPreferredSize().height);

          //---- L1LIB2 ----
          L1LIB2.setComponentPopupMenu(BTD);
          L1LIB2.setName("L1LIB2");
          panel4.add(L1LIB2);
          L1LIB2.setBounds(235, 105, 170, L1LIB2.getPreferredSize().height);

          //---- W1RGL ----
          W1RGL.setComponentPopupMenu(BTD);
          W1RGL.setName("W1RGL");
          panel4.add(W1RGL);
          W1RGL.setBounds(520, 105, 30, W1RGL.getPreferredSize().height);

          //---- L1NCPX ----
          L1NCPX.setComponentPopupMenu(BTD);
          L1NCPX.setName("L1NCPX");
          panel4.add(L1NCPX);
          L1NCPX.setBounds(30, 165, 70, L1NCPX.getPreferredSize().height);

          //---- WLNCPT ----
          WLNCPT.setComponentPopupMenu(BTD);
          WLNCPT.setToolTipText("Num\u00e9ro de compte de contrepartie \u00e0 r\u00e9percuter sur la ligne.  Types de r\u00e9percutions possibles.  \"1\" = La ligne suivante de contrepartie est propos\u00e9e automatiquement avec montant, code libell\u00e9, N\u00b0de pi\u00e8ce...  \"2\" = Ligne suivante de contrepartie g\u00e9n\u00e9r\u00e9e automatiquement avec montant, code libell\u00e9, n\u00b0de pi\u00e8ce...");
          WLNCPT.setName("WLNCPT");
          panel4.add(WLNCPT);
          WLNCPT.setBounds(105, 165, 18, WLNCPT.getPreferredSize().height);

          //---- L1VTL ----
          L1VTL.setText("Ventilation \u00e9criture");
          L1VTL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1VTL.setName("L1VTL");
          panel4.add(L1VTL);
          L1VTL.setBounds(130, 170, 135, L1VTL.getPreferredSize().height);

          //---- OBJ_66_OBJ_66 ----
          OBJ_66_OBJ_66.setText("R\u00e9f classement");
          OBJ_66_OBJ_66.setName("OBJ_66_OBJ_66");
          panel4.add(OBJ_66_OBJ_66);
          OBJ_66_OBJ_66.setBounds(367, 170, 96, 21);

          //---- L1RFC ----
          L1RFC.setComponentPopupMenu(BTD);
          L1RFC.setName("L1RFC");
          panel4.add(L1RFC);
          L1RFC.setBounds(471, 165, 110, L1RFC.getPreferredSize().height);

          //---- OBJ_43_OBJ_43 ----
          OBJ_43_OBJ_43.setText("@CHPLB2@");
          OBJ_43_OBJ_43.setName("OBJ_43_OBJ_43");
          panel4.add(OBJ_43_OBJ_43);
          OBJ_43_OBJ_43.setBounds(235, 85, 268, 22);

          //---- OBJ_24_OBJ_24 ----
          OBJ_24_OBJ_24.setText("@LIBMTT@");
          OBJ_24_OBJ_24.setName("OBJ_24_OBJ_24");
          panel4.add(OBJ_24_OBJ_24);
          OBJ_24_OBJ_24.setBounds(30, 15, 97, 23);

          //---- OBJ_42_OBJ_42 ----
          OBJ_42_OBJ_42.setText("@CHPLB1@");
          OBJ_42_OBJ_42.setName("OBJ_42_OBJ_42");
          panel4.add(OBJ_42_OBJ_42);
          OBJ_42_OBJ_42.setBounds(130, 85, 110, 22);

          //---- OBJ_55_OBJ_55 ----
          OBJ_55_OBJ_55.setText("Contrepartie/T");
          OBJ_55_OBJ_55.setName("OBJ_55_OBJ_55");
          panel4.add(OBJ_55_OBJ_55);
          OBJ_55_OBJ_55.setBounds(30, 140, 90, 22);

          //---- OBJ_41_OBJ_41 ----
          OBJ_41_OBJ_41.setText("N\u00b0Pi\u00e8ce");
          OBJ_41_OBJ_41.setName("OBJ_41_OBJ_41");
          panel4.add(OBJ_41_OBJ_41);
          OBJ_41_OBJ_41.setBounds(55, 85, 52, 22);

          //---- OBJ_45_OBJ_45 ----
          OBJ_45_OBJ_45.setText("Date ech.");
          OBJ_45_OBJ_45.setName("OBJ_45_OBJ_45");
          panel4.add(OBJ_45_OBJ_45);
          OBJ_45_OBJ_45.setBounds(555, 85, 90, 22);

          //---- OBJ_25_OBJ_25 ----
          OBJ_25_OBJ_25.setText("Sens");
          OBJ_25_OBJ_25.setName("OBJ_25_OBJ_25");
          panel4.add(OBJ_25_OBJ_25);
          OBJ_25_OBJ_25.setBounds(130, 15, 34, 23);

          //---- OBJ_44_OBJ_44 ----
          OBJ_44_OBJ_44.setText("@CHPRG1@");
          OBJ_44_OBJ_44.setName("OBJ_44_OBJ_44");
          panel4.add(OBJ_44_OBJ_44);
          OBJ_44_OBJ_44.setBounds(520, 85, 24, 22);

          //---- OBJ_40_OBJ_40 ----
          OBJ_40_OBJ_40.setText("C");
          OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");
          panel4.add(OBJ_40_OBJ_40);
          OBJ_40_OBJ_40.setBounds(30, 85, 12, 22);

          //---- OBJ_19 ----
          OBJ_19.setText("Dup");
          OBJ_19.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_19.setName("OBJ_19");
          OBJ_19.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_19ActionPerformed(e);
            }
          });
          panel4.add(OBJ_19);
          OBJ_19.setBounds(210, 40, 55, 35);

          //---- W1LIBS ----
          W1LIBS.setComponentPopupMenu(BTD);
          W1LIBS.setName("W1LIBS");
          panel4.add(W1LIBS);
          W1LIBS.setBounds(405, 105, 104, W1LIBS.getPreferredSize().height);

          //---- L1DTRX ----
          L1DTRX.setComponentPopupMenu(BTD);
          L1DTRX.setName("L1DTRX");
          panel4.add(L1DTRX);
          L1DTRX.setBounds(471, 196, 70, 28);

          //---- Lbl_datedefacture ----
          Lbl_datedefacture.setText("Date r\u00e9elle de facture");
          Lbl_datedefacture.setName("Lbl_datedefacture");
          panel4.add(Lbl_datedefacture);
          Lbl_datedefacture.setBounds(338, 200, 125, 21);

          //---- Lbl_referencefacture ----
          Lbl_referencefacture.setText("R\u00e9f\u00e9rence de la facture");
          Lbl_referencefacture.setName("Lbl_referencefacture");
          panel4.add(Lbl_referencefacture);
          Lbl_referencefacture.setBounds(30, 200, 135, 21);

          //---- L1RPE ----
          L1RPE.setComponentPopupMenu(BTD);
          L1RPE.setMinimumSize(new Dimension(164, 28));
          L1RPE.setMaximumSize(new Dimension(164, 28));
          L1RPE.setPreferredSize(new Dimension(164, 28));
          L1RPE.setName("L1RPE");
          panel4.add(L1RPE);
          L1RPE.setBounds(163, 196, 165, L1RPE.getPreferredSize().height);

          //---- W1ECHX ----
          W1ECHX.setName("W1ECHX");
          panel4.add(W1ECHX);
          W1ECHX.setBounds(555, 105, 85, 30);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel4.getComponentCount(); i++) {
              Rectangle bounds = panel4.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel4.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel4.setMinimumSize(preferredSize);
            panel4.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel4);
        panel4.setBounds(365, 10, 660, 235);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_14 ----
      OBJ_14.setText("Choix possibles");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);

      //---- OBJ_15 ----
      OBJ_15.setText("Duplication");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);

      //---- OBJ_13 ----
      OBJ_13.setText("Aide en ligne");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }

    //---- OBJ_79 ----
    OBJ_79.setText("");
    OBJ_79.setToolTipText("Recherche p\u00e9riode anglaise (445)");
    OBJ_79.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_79.setName("OBJ_79");
    OBJ_79.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_79ActionPerformed(e);
      }
    });
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiTextField WCOD;
  private XRiTextField L1NLI;
  private XRiTextField L1NCGX;
  private XRiTextField L1NCA;
  private XRiTextField L1DTJ;
  private XRiTextField LIBCPT;
  private XRiTextField A1CPL;
  private XRiTextField A1RUE;
  private XRiTextField A1CDP;
  private XRiTextField A1VIL;
  private XRiTextField USOLDE;
  private JLabel OBJ_57_OBJ_57;
  private XRiTextField W1SOLD;
  private JLabel OBJ_78_OBJ_78;
  private RiZoneSortie W1SNS;
  private JLabel OBJ_61_OBJ_61;
  private JLabel OBJ_25_OBJ_26;
  private JLabel OBJ_25_OBJ_27;
  private JLabel OBJ_25_OBJ_28;
  private JLabel OBJ_25_OBJ_29;
  private JPanel panel4;
  private XRiTextField L1MTT;
  private XRiComboBox L1SNS;
  private XRiTextField L1DEV;
  private JLabel OBJ_27_OBJ_27;
  private XRiTextField WCHGX;
  private JLabel OBJ_28_OBJ_28;
  private XRiTextField L1MTD;
  private JLabel OBJ_29_OBJ_29;
  private XRiTextField L1QTE;
  private JLabel OBJ_30_OBJ_30;
  private XRiTextField L1CLB;
  private XRiTextField L1PCE;
  private XRiTextField L1LIB1;
  private XRiTextField L1LIB2;
  private XRiTextField W1RGL;
  private XRiTextField L1NCPX;
  private XRiTextField WLNCPT;
  private XRiCheckBox L1VTL;
  private JLabel OBJ_66_OBJ_66;
  private XRiTextField L1RFC;
  private JLabel OBJ_43_OBJ_43;
  private JLabel OBJ_24_OBJ_24;
  private JLabel OBJ_42_OBJ_42;
  private JLabel OBJ_55_OBJ_55;
  private JLabel OBJ_41_OBJ_41;
  private JLabel OBJ_45_OBJ_45;
  private JLabel OBJ_25_OBJ_25;
  private JLabel OBJ_44_OBJ_44;
  private JLabel OBJ_40_OBJ_40;
  private JButton OBJ_19;
  private XRiTextField W1LIBS;
  private XRiTextField L1DTRX;
  private JLabel Lbl_datedefacture;
  private JLabel Lbl_referencefacture;
  private XRiTextField L1RPE;
  private XRiTextField W1ECHX;
  private JPopupMenu BTD;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_15;
  private JMenuItem OBJ_13;
  private JButton OBJ_79;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
