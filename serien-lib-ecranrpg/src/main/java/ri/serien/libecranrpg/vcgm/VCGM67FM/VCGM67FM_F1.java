
package ri.serien.libecranrpg.vcgm.VCGM67FM;
// Nom Fichier: pop_VCGM05FM_FMTERR_367.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class VCGM67FM_F1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM67FM_F1(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(OK);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    OK.setIcon(lexique.chargerImage("images/OK.png", true));
    button1.setIcon(lexique.chargerImage("images/retour.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("CONFIRMATION"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OKActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, "OUI");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void button1ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, "NON");
    lexique.HostScreenSendKey(this, "ENTER");
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_5 = new JLabel();
    OK = new JButton();
    button1 = new JButton();

    //======== this ========
    setName("this");
    setLayout(null);

    //---- OBJ_5 ----
    OBJ_5.setText("Veuillez confimer");
    OBJ_5.setHorizontalAlignment(SwingConstants.CENTER);
    OBJ_5.setName("OBJ_5");
    add(OBJ_5);
    OBJ_5.setBounds(15, 10, 310, 20);

    //---- OK ----
    OK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OK.setToolTipText("Oui");
    OK.setName("OK");
    OK.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OKActionPerformed(e);
      }
    });
    add(OK);
    OK.setBounds(100, 55, 56, 40);

    //---- button1 ----
    button1.setToolTipText("Non");
    button1.setName("button1");
    button1.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        button1ActionPerformed(e);
      }
    });
    add(button1);
    button1.setBounds(190, 55, 56, 40);

    setPreferredSize(new Dimension(340, 130));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel OBJ_5;
  private JButton OK;
  private JButton button1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
