
package ri.serien.libecranrpg.vcgm.VCGM73FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerListModel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.outil.font.CustomFont;

/**
 * @author Stéphane Vénéri
 */
public class VCGM73FM_DP extends SNPanelEcranRPG implements ioFrame {
  
  private String[] _TE01_Top = { "TE01", "TE02", "TE03", "TE04", "TE05", "TE06", "TE07", "TE08", "TE09", "TE10", "TE11", "TE12", "TE13",
      "TE14", "TE15", "TE16", "TE17", "TE18", "TE19", "TE20", };
  private String[] _TE01_Title = { "HLD01", };
  private String[][] _TE01_Data = { { "LE01", }, { "LE02", }, { "LE03", }, { "LE04", }, { "LE05", }, { "LE06", }, { "LE07", },
      { "LE08", }, { "LE09", }, { "LE10", }, { "LE11", }, { "LE12", }, { "LE13", }, { "LE14", }, { "LE15", }, { "LE16", }, { "LE17", },
      { "LE18", }, { "LE19", }, { "LE20", }, };
  private int[] _TE01_Width = { 800, };
  
  private Color[][] _LIST_Text_Color = new Color[20][1];
  private Color[][] _LIST_Fond_Color = new Color[20][1];
  private Color violet = new Color(155, 155, 255);
  private Color couleurRapp = new Color(53, 100, 25);
  private Color couleurLiti = new Color(255, 100, 0);
  private Font[][] police = new Font[20][1];
  private Font normal = CustomFont.loadFont("fonts/VeraMono.ttf", 12);// new Font("Courier New", Font.PLAIN, 12);
  private Font gras = CustomFont.loadFont("fonts/VeraMoBd.ttf", 12); // new Font("Courier New", Font.BOLD, 12);
  // calculatrice solde partiel
  private String[][] enTete;
  private int[] indexs = { 6, 7 };
  private ArrayList<String> donnees = null;
  
  public VCGM73FM_DP(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    LE01.setAspectTable(_TE01_Top, _TE01_Title, _TE01_Data, _TE01_Width, false, null, _LIST_Text_Color, _LIST_Fond_Color, police);
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_120.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIB@")).trim());
    WCLA1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCLA1@")).trim());
    WCLA2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCLA2@")).trim());
    OBJ_112.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W1ATT@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    CPSENS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CPSENS@")).trim());
    OBJ_231.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W1ATT@")).trim());
    OBJ_221.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDEVF@")).trim());
    OBJ_220.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1DEV@")).trim());
    OBJ_228.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSENS@")).trim());
    OBJ_236.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PDSNS2@")).trim());
    OBJ_229.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PDSNS1@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // String chaine=null;
    boolean in57 = false, in91 = false, in92 = false, in53 = false, in56 = false, in84 = false;
    JLabel[] tabRap = { rap01, rap02, rap03, rap04, rap05, rap06, rap07, rap08, rap09, rap10, rap11, rap12, rap13, rap14, rap15, rap16,
        rap17, rap18, rap19, rap20 };
    JLabel[] tabLit = { lit01, lit02, lit03, lit04, lit05, lit06, lit07, lit08, lit09, lit10, lit11, lit12, lit13, lit14, lit15, lit16,
        lit17, lit18, lit19, lit20 };
    
    // Couleurs de la liste (suivant indicateurs de 21 à 35) POUR les lignes de 1 à 15
    // LETTRAGE
    for (int i = 0; i < 15; i++) {
      
      if (lexique.isTrue(String.valueOf(i + 21))) {
        if (lexique.isTrue("(N56)")) {
          // si ligne de reprise
          if ((lexique.HostFieldGetData("LE" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))).contains("Reprise"))
              || (lexique.HostFieldGetData("LE" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))).contains("Position"))) {
            _LIST_Text_Color[i][0] = Constantes.COULEUR_LISTE_COMMENTAIRE;
            police[i][0] = gras;
          }
          // sinon ligne pointage
          else {
            _LIST_Text_Color[i][0] = violet;
            police[i][0] = gras;
          }
        }
        else {
          _LIST_Text_Color[i][0] = Color.BLACK;
          police[i][0] = gras;
        }
      }
      else {
        _LIST_Text_Color[i][0] = Color.BLACK;
        police[i][0] = normal;
      }
    }
    
    // Couleurs de la liste (suivant indicateurs de 41 à 45) POUR les lignes de 16 à 20
    // LETTRAGE
    for (int i = 0; i < 5; i++) {
      if (lexique.isTrue(String.valueOf(i + 41))) {
        if (lexique.isTrue("(N56)")) {
          // si ligne de reprise
          if ((lexique.HostFieldGetData("LE" + (i + 16)).contains("Reprise"))
              || (lexique.HostFieldGetData("LE" + (i + 16)).contains("Position"))) {
            _LIST_Text_Color[i + 15][0] = Constantes.COULEUR_LISTE_COMMENTAIRE;
            police[i + 15][0] = gras;
          }
          // sinon ligne pointage
          else {
            _LIST_Text_Color[i + 15][0] = violet;
            police[i + 15][0] = gras;
          }
        }
        else {
          _LIST_Text_Color[i + 15][0] = Color.BLACK;
          police[i + 15][0] = gras;
        }
      }
      
      else {
        _LIST_Text_Color[i + 15][0] = Color.BLACK;
        police[i + 15][0] = normal;
      }
    }
    
    // Couleurs de la liste si "r" en position 17 de la ligne (écriture rapprochée)
    for (int i = 0; i < 20; i++) {
      if ((lexique.HostFieldGetData("LE" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))).substring(16, 17).equalsIgnoreCase("r"))) {
        _LIST_Text_Color[i][0] = couleurRapp;
        tabRap[i].setVisible(true);
        
        if ((lexique.HostFieldGetData("V01F").equalsIgnoreCase("JUSTIFIC"))) {
          tabRap[i].setBounds(180, tabRap[i].getY(), tabRap[i].getWidth(), tabRap[i].getHeight());
        }
        else {
          tabRap[i].setBounds(166, tabRap[i].getY(), tabRap[i].getWidth(), tabRap[i].getHeight());
        }
      }
      else {
        tabRap[i].setVisible(false);
      }
    }
    
    // Couleurs de la liste si "l" en position 19 de la ligne (écriture en litige)
    for (int i = 0; i < 20; i++) {
      if ((lexique.HostFieldGetData("LE" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))).substring(18, 19).equalsIgnoreCase("l"))) {
        _LIST_Text_Color[i][0] = couleurLiti;
        tabLit[i].setVisible(true);
        if ((lexique.HostFieldGetData("V01F").equalsIgnoreCase("JUSTIFIC"))) {
          tabLit[i].setBounds(195, tabLit[i].getY(), tabLit[i].getWidth(), tabLit[i].getHeight());
        }
        else {
          tabLit[i].setBounds(181, tabLit[i].getY(), tabLit[i].getWidth(), tabLit[i].getHeight());
        }
      }
      // Couleurs de la liste si "!" en position 2 de la ligne (litige)
      else if ((lexique.HostFieldGetData("LE" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))).substring(2, 3).equalsIgnoreCase("!"))) {
        _LIST_Text_Color[i][0] = couleurLiti;
        police[i][0] = gras;
      }
      else {
        tabLit[i].setVisible(false);
      }
    }
    
    WCLA1.setVisible(lexique.isPresent("WCLA1"));
    WCLA2.setVisible(lexique.isPresent("WCLA2"));
    
    // Ajoute à la liste des oData les variables non liées directement à un composant graphique
    // majTable( LIST, LIST.get_LIST_Title_Data_Brut(), _TE01_Top, null, _LIST_Text_Color, _LIST_Fond_Color, police);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    LE01.setVisible(!interpreteurD.analyseExpression("@HLE04@").trim().equalsIgnoreCase("Crédit"));
    OBJ_54.setVisible(interpreteurD.analyseExpression("@V01F@").equalsIgnoreCase("JUSTIFIC"));
    OBJ_31.setVisible(interpreteurD.analyseExpression("@TEST@").equalsIgnoreCase("Lettrer"));
    // chaine = lexique.HostFieldGetData("INDNCG");
    
    CPSOCD.setVisible(lexique.isTrue("57"));
    OBJ_132.setVisible(CPSOCD.isVisible());
    CPSENS.setVisible(CPSOCD.isVisible());
    OBJ_112.setVisible(lexique.isTrue("(N57) AND (53) AND (N92)"));
    
    WPTG.setVisible(lexique.isTrue("(36) AND (57)"));
    OBJ_130.setVisible(WPTG.isVisible());
    WTEL1.setVisible(lexique.isTrue("(53) AND (N57)"));
    WTEL.setVisible(lexique.isTrue("37"));
    WLDEV.setVisible(!lexique.HostFieldGetData("WLDEV").trim().equals(""));
    OBJ_127.setVisible(WLDEV.isVisible());
    
    in53 = lexique.isTrue("53");
    in56 = lexique.isTrue("56");
    in57 = lexique.isTrue("57");
    in84 = lexique.isTrue("84");
    in91 = lexique.isTrue("91");
    in92 = lexique.isTrue("92");
    
    WLETDP.setVisible(lexique.isTrue("54"));
    OBJ_213.setVisible(WLETDP.isVisible());
    WTPTDB.setVisible(lexique.isTrue("(N38) AND (N84)"));
    WTDVDB.setVisible(lexique.isTrue("(N38) AND (84)"));
    OBJ_217.setVisible(lexique.isTrue("N38"));
    WTPTCR.setVisible(lexique.isTrue("(N39) AND (N84)"));
    WTDVCR.setVisible(lexique.isTrue("(N39) AND (84)"));
    OBJ_219.setVisible(lexique.isTrue("N39"));
    WTPTD1.setVisible(!in56 & !in84 & in57 & !lexique.HostFieldGetData("WTPTD1").trim().equalsIgnoreCase(""));
    WTPTDV.setVisible(!in56 & in84);
    WTPTD2.setVisible(in56 & !in84 & in57 & !lexique.HostFieldGetData("WTPTD2").trim().equalsIgnoreCase(""));
    WTPTDX.setVisible(in56 & in84);
    OBJ_221.setVisible(in56 & in84);
    OBJ_220.setVisible(!in56 & in84);
    
    panel2.setVisible(in57);
    panel3.setVisible(!in57);
    if (!in57) {
      PDPER.setVisible(in91);
      OBJ_212.setVisible(PDPER.isVisible());
      WTJDB.setVisible(in53 & !in92);
      OBJ_214.setVisible(WTJDB.isVisible());
      WTJCR.setVisible(WTJDB.isVisible());
      OBJ_216.setVisible(WTJDB.isVisible());
      WSOCD.setVisible(in53 & !in91);
      OBJ_228.setVisible(WSOCD.isVisible());
      
      OBJ_232.setVisible(in91 & !in92);
      PDCR2.setVisible(in91 & !in92);
      PDDB2.setVisible(in91 & !in92);
      PDSOL1.setVisible(in91 & !in92);
      PDSOL2.setVisible(in91 & !in92);
      OBJ_229.setVisible(PDSOL1.isVisible());
      OBJ_236.setVisible(PDSOL2.isVisible());
      
      OBJ_218.setVisible(WSOCD.isVisible() || PDSOL1.isVisible() || PDSOL2.isVisible());
    }
    else {
      OBJ_206.setVisible(in53);
    }
    
    WMOIX_SP.setVisible(lexique.isTrue("(N52)"));
    OBJ_242.setVisible(WMOIX_SP.isVisible());
    OBJ_241.setVisible(WMOIX_SP.isVisible());
    // lettrage et dellettrage
    BT_LET.setVisible(lexique.isTrue("((53) AND (57))"));
    BT_DEL.setVisible(lexique.isTrue("((54) AND (57))"));
    // desequilibre
    label3.setVisible(lexique.isTrue("((53) AND (57) AND (19))"));
    label4.setVisible(lexique.isTrue("((54) AND (57) AND (19)))"));
    WMOIX.setVisible(lexique.isTrue("((53) AND (57) AND (19)) OR ((54) AND (57) AND (19))"));
    
    OBJ_288.setVisible(lexique.HostFieldGetData("LE01").charAt(2) == 'o');
    OBJ_289.setVisible(lexique.HostFieldGetData("LE02").charAt(2) == 'o');
    OBJ_290.setVisible(lexique.HostFieldGetData("LE03").charAt(2) == 'o');
    OBJ_291.setVisible(lexique.HostFieldGetData("LE04").charAt(2) == 'o');
    OBJ_292.setVisible(lexique.HostFieldGetData("LE05").charAt(2) == 'o');
    OBJ_293.setVisible(lexique.HostFieldGetData("LE06").charAt(2) == 'o');
    OBJ_294.setVisible(lexique.HostFieldGetData("LE07").charAt(2) == 'o');
    OBJ_295.setVisible(lexique.HostFieldGetData("LE08").charAt(2) == 'o');
    OBJ_296.setVisible(lexique.HostFieldGetData("LE09").charAt(2) == 'o');
    OBJ_297.setVisible(lexique.HostFieldGetData("LE10").charAt(2) == 'o');
    OBJ_298.setVisible(lexique.HostFieldGetData("LE11").charAt(2) == 'o');
    OBJ_299.setVisible(lexique.HostFieldGetData("LE12").charAt(2) == 'o');
    OBJ_300.setVisible(lexique.HostFieldGetData("LE13").charAt(2) == 'o');
    OBJ_301.setVisible(lexique.HostFieldGetData("LE14").charAt(2) == 'o');
    OBJ_302.setVisible(lexique.HostFieldGetData("LE15").charAt(2) == 'o');
    OBJ_303.setVisible(lexique.HostFieldGetData("LE20").charAt(2) == 'o');
    OBJ_304.setVisible(lexique.HostFieldGetData("LE19").charAt(2) == 'o');
    OBJ_305.setVisible(lexique.HostFieldGetData("LE18").charAt(2) == 'o');
    OBJ_306.setVisible(lexique.HostFieldGetData("LE17").charAt(2) == 'o');
    OBJ_307.setVisible(lexique.HostFieldGetData("LE16").charAt(2) == 'o');
    
    /*
    if (lexique.HostFieldGetData("WSOGL").equalsIgnoreCase("X"))
    {
      OBJ_280.setToolTipText("Visualisation classique");
      OBJ_280.setIcon(lexique.getImage("images/facture.gif", true));
    }
    else
    {
      OBJ_280.setToolTipText("Visualisation des soldes glissés");
      OBJ_280.setIcon(lexique.getImage("images/facture1.gif", true));
    }
    */
    // Menu contextuel
    OBJ_41.setVisible(in53 & in57);
    OBJ_42.setVisible(lexique.isTrue("54"));
    OBJ_43.setVisible(in53 & !in57);
    OBJ_44.setVisible(in53 & !in57);
    OBJ_45.setVisible(in53 & !in57);
    OBJ_46.setVisible(in53 & !in57);
    OBJ_48.setVisible(in53 & !in57);
    OBJ_FOL.setVisible(in53 & !in57);
    OBJ_TRS.setVisible(in53 & !in57);
    OBJ_Echeancier.setVisible(in53 & !in57);
    riSousMenu8.setVisible(in53 & in57);
    
    // TODO Icones
    OBJ_95.setIcon(lexique.chargerImage("images/pfin20.png", true));
    OBJ_91.setIcon(lexique.chargerImage("images/pdeb20.png", true));
    
    
    
    OBJ_285.setIcon(lexique.chargerImage("images/fleche_pre.png", true));
    OBJ_118.setIcon(lexique.chargerImage("images/fleche_sui.png", true));
    OBJ_241.setIcon(lexique.chargerImage("images/plus_petit.png", true));
    OBJ_242.setIcon(lexique.chargerImage("images/moins_petit.png", true));
    
    // Titre
    // setTitle(interpreteurD.analyseExpression("COMPTE"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
    riMenu_bt4.setIcon(lexique.chargerImage("images/fonctions.png", true));
    riMenu_bt5.setIcon(lexique.chargerImage("images/fonctions.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void gestionDeLaCalculatrice() {
    enTete = new String[][] { { "Jo", "3" }, { "Folio", "5" }, { "Date", "9" }, { "C", "2" }, { "Libell\u00e9", "27" },
        { "N\u00b0 pi\u00e8ce", "36" }, { "D\u00e9bit", "14" }, { "Cr\u00e9dit", "14" }, { "Aff", "2" } };
    
    if (donnees == null) {
      donnees = new ArrayList<String>();
    }
    donnees.clear();
    
    int[] indices = LE01.getSelectedRows();
    
    String valeur = null;
    
    for (int i = 0; i < indices.length; i++) {
      valeur = "LE";
      
      if (indices[i] < 9) {
        valeur += "0";
      }
      
      int ii = indices[i] + 1;
      valeur += ii;
      
      donnees.add(lexique.HostFieldGetData(valeur));
    }
    
    lexique.openCalculatriceSP(enTete, donnees, indexs);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(4, 2);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WMOIX");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("TE01", 0, "?");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WDOC", 0, "1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(23, 20);
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void riSousMenu_bt16ActionPerformed(ActionEvent e) {
    gestionDeLaCalculatrice();
  }
  
  private void riSousMenu_bt18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(2, 1);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(2, 2);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_119ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WMOIX", 0, "*BA");
    // WMOIX.setText("*BA");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_285ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(1, 6);
    lexique.HostScreenSendKey(this, "F7");
  }
  
  private void OBJ_118ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(6, 6);
    lexique.HostScreenSendKey(this, "F7");
  }
  
  private void TE01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _TE01_Top, "1", "ENTER", e);
    if (LE01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void OBJ_91ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WMOIX", 0, "D    ");
    // WMOIX.setText("D ");
    lexique.HostScreenSendKey(this, "enter");
  }
  
  private void OBJ_95ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WMOIX", 0, "F    ");
    // WMOIX.setText("F ");
    lexique.HostScreenSendKey(this, "enter");
  }
  
  private void OBJ_288ActionPerformed(ActionEvent e) {
    LE01.setClearTop(false);
    lexique.HostFieldPutData("TE01", 0, "5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_289ActionPerformed(ActionEvent e) {
    LE01.setClearTop(false);
    lexique.HostFieldPutData("TE02", 0, "5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_290ActionPerformed(ActionEvent e) {
    LE01.setClearTop(false);
    lexique.HostFieldPutData("TE03", 0, "5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_291ActionPerformed(ActionEvent e) {
    LE01.setClearTop(false);
    lexique.HostFieldPutData("TE04", 0, "5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_292ActionPerformed(ActionEvent e) {
    LE01.setClearTop(false);
    lexique.HostFieldPutData("TE05", 0, "5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_293ActionPerformed(ActionEvent e) {
    LE01.setClearTop(false);
    lexique.HostFieldPutData("TE06", 0, "5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_294ActionPerformed(ActionEvent e) {
    LE01.setClearTop(false);
    lexique.HostFieldPutData("TE07", 0, "5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_295ActionPerformed(ActionEvent e) {
    LE01.setClearTop(false);
    lexique.HostFieldPutData("TE08", 0, "5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_296ActionPerformed(ActionEvent e) {
    LE01.setClearTop(false);
    lexique.HostFieldPutData("TE09", 0, "5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_297ActionPerformed(ActionEvent e) {
    LE01.setClearTop(false);
    lexique.HostFieldPutData("TE10", 0, "5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_298ActionPerformed(ActionEvent e) {
    LE01.setClearTop(false);
    lexique.HostFieldPutData("TE11", 0, "5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_299ActionPerformed(ActionEvent e) {
    LE01.setClearTop(false);
    lexique.HostFieldPutData("TE12", 0, "5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_300ActionPerformed(ActionEvent e) {
    LE01.setClearTop(false);
    lexique.HostFieldPutData("TE13", 0, "5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_301ActionPerformed(ActionEvent e) {
    LE01.setClearTop(false);
    lexique.HostFieldPutData("TE14", 0, "5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_302ActionPerformed(ActionEvent e) {
    LE01.setClearTop(false);
    lexique.HostFieldPutData("TE15", 0, "5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_303ActionPerformed(ActionEvent e) {
    LE01.setClearTop(false);
    lexique.HostFieldPutData("TE20", 0, "5");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_304ActionPerformed(ActionEvent e) {
    LE01.setClearTop(false);
    lexique.HostFieldPutData("TE19", 0, "5");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_305ActionPerformed(ActionEvent e) {
    LE01.setClearTop(false);
    lexique.HostFieldPutData("TE18", 0, "5");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_306ActionPerformed(ActionEvent e) {
    LE01.setClearTop(false);
    lexique.HostFieldPutData("TE17", 0, "5");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_307ActionPerformed(ActionEvent e) {
    LE01.setClearTop(false);
    lexique.HostFieldPutData("TE16", 0, "5");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_238ActionPerformed(ActionEvent e) {
    if (lexique.isTrue("(53) AND (57)")) {
      // WMOIX.setText("LET");
      lexique.HostFieldPutData("WMOIX", 0, "LET");
    }
    else if (lexique.isTrue("(54) AND (57)")) {
      lexique.HostFieldPutData("WMOIX", 0, "DLT");
    }
    // WMOIX.setText("DLT");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_241ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WMOIX", 0, "+" + WMOIX_SP.getValue());
    // WMOIX.setText("+" + WMOIX_SP.getValue());
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_242ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WMOIX", 0, "-" + WMOIX_SP.getValue());
    // WMOIX.setText("-" + WMOIX_SP.getValue());
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_125ActionPerformed(ActionEvent e) {
    // PcCommand(4, "Numeroteur.exe " +@"@WTEL@"+ " "+Chr(34)+@"@WLIB@"+Chr(34)+" "+"1 "+ @"@&PREFIXE@")
  }
  
  private void OBJ_31ActionPerformed(ActionEvent e) {
    Component composant = BTD3.getInvoker();
    if (composant instanceof XRiTextField) {
      lexique.HostFieldPutData(((XRiTextField) composant).getName(), 0, " *FOR");
      // ((XRiTextField)composant).setText(((XRiTextField)composant).getText() + " *FOR");
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void OBJ_32ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD3.getInvoker().getName());
  }
  
  private void OBJ_33ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD3.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_38ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_39ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_41ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _TE01_Top, "1", "Enter");
    LE01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_42ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _TE01_Top, "1", "Enter");
    LE01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_43ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _TE01_Top, "1", "Enter");
    LE01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_44ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _TE01_Top, "2", "Enter");
    LE01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_45ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _TE01_Top, "3", "Enter");
    LE01.setValeurTop("3");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_46ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _TE01_Top, "4", "Enter");
    LE01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_47ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _TE01_Top, "5", "Enter");
    LE01.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_48ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _TE01_Top, "F", "Enter");
    LE01.setValeurTop("F");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_FOLActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _TE01_Top, "S", "Enter");
    LE01.setValeurTop("S");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_TRSActionPerformed(ActionEvent e) {
    LE01.setValeurTop("T");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_49ActionPerformed(ActionEvent e) {
    LE01.setValeurTop("Y");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_50ActionPerformed(ActionEvent e) {
    gestionDeLaCalculatrice();
  }
  
  private void OBJ_51ActionPerformed(ActionEvent e) {
    LE01.setValeurTop("L");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_52ActionPerformed(ActionEvent e) {
    LE01.setValeurTop("E");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_54ActionPerformed(ActionEvent e) {
    LE01.setValeurTop("P");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_EcheancierActionPerformed(ActionEvent e) {
    LE01.setValeurTop("?");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_55ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTDL1.getInvoker().getName());
  }
  
  private void OBJ_56ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTDL1.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_92ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD2.getInvoker().getName());
  }
  
  private void OBJ_93ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD2.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt22ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(1, 1);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt23ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WMOIX", 0, "REF*");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt24ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19");
  }
  
  private void riSousMenu_bt25ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void riSousMenu_bt26ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19");
  }
  
  private void riSousMenu_bt27ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void riSousMenu_bt28ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void riSousMenu_bt29ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void riSousMenu_bt_dupliActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F17");
  }
  
  private void riSousMenu_bt30ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18");
  }
  
  private void WTS1FocusGained(FocusEvent e) {
    if (!WTS1.getText().trim().equals("")) {
      WTS1.setText(WTS1.getText() + " ");
    }
    WTS1.select(WTS1.getText().length(), WTS1.getText().length());
  }
  
  private void WTS2FocusGained(FocusEvent e) {
    if (!WTS2.getText().trim().equals("")) {
      WTS2.setText(WTS2.getText() + " ");
    }
    WTS2.select(WTS2.getText().length(), WTS2.getText().length());
  }
  
  private void WTS3FocusGained(FocusEvent e) {
    if (!WTS3.getText().trim().equals("")) {
      WTS3.setText(WTS3.getText() + " ");
    }
    WTS3.select(WTS3.getText().length(), WTS3.getText().length());
  }
  
  private void mi_NumPieceActionPerformed(ActionEvent e) {
    LE01.setValeurTop("C");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void riSousMenu_bt31ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void riSousMenu_bt32ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void OBJ_FOL2ActionPerformed(ActionEvent e) {
    LE01.setValeurTop("U");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void MILienUrlActionPerformed(ActionEvent e) {
    LE01.setValeurTop("9");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void AfficheLitigeActionPerformed(ActionEvent e) {
    LE01.setValeurTop("G");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    INDETB = new XRiTextField();
    INDNCG = new XRiTextField();
    INDNCA = new XRiTextField();
    OBJ_120 = new RiZoneSortie();
    OBJ_119 = new SNBoutonDetail();
    OBJ_285 = new JButton();
    OBJ_118 = new JButton();
    label1 = new JLabel();
    label2 = new JLabel();
    WCLA1 = new RiZoneSortie();
    WCLA2 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    OBJ_112 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_del = new RiSousMenu();
    riSousMenu_bt_del = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu22 = new RiSousMenu();
    riSousMenu_bt22 = new RiSousMenu_bt();
    riSousMenu30 = new RiSousMenu();
    riSousMenu_bt30 = new RiSousMenu_bt();
    riSousMenu31 = new RiSousMenu();
    riSousMenu_bt31 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    riSousMenu16 = new RiSousMenu();
    riSousMenu_bt16 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riMenu4 = new RiMenu();
    riMenu_bt4 = new RiMenu_bt();
    riSousMenu18 = new RiSousMenu();
    riSousMenu_bt18 = new RiSousMenu_bt();
    riSousMenu19 = new RiSousMenu();
    riSousMenu_bt19 = new RiSousMenu_bt();
    riMenu5 = new RiMenu();
    riMenu_bt5 = new RiMenu_bt();
    riSousMenu23 = new RiSousMenu();
    riSousMenu_bt23 = new RiSousMenu_bt();
    riSousMenu24 = new RiSousMenu();
    riSousMenu_bt24 = new RiSousMenu_bt();
    riSousMenu25 = new RiSousMenu();
    riSousMenu_bt25 = new RiSousMenu_bt();
    riSousMenu26 = new RiSousMenu();
    riSousMenu_bt26 = new RiSousMenu_bt();
    riSousMenu27 = new RiSousMenu();
    riSousMenu_bt27 = new RiSousMenu_bt();
    riSousMenu28 = new RiSousMenu();
    riSousMenu_bt28 = new RiSousMenu_bt();
    riSousMenu29 = new RiSousMenu();
    riSousMenu_bt29 = new RiSousMenu_bt();
    riSousMenu32 = new RiSousMenu();
    riSousMenu_bt32 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_127 = new JLabel();
    WLDEV = new XRiTextField();
    OBJ_130 = new JLabel();
    WPTG = new XRiTextField();
    OBJ_125 = new SNBoutonDetail();
    WTEL = new XRiTextField();
    OBJ_132 = new JLabel();
    CPSOCD = new XRiTextField();
    CPSENS = new JLabel();
    WTEL1 = new XRiTextField();
    xTitledPanel1 = new JXTitledPanel();
    panel4 = new JPanel();
    OBJ_288 = new SNBoutonDetail();
    OBJ_289 = new SNBoutonDetail();
    OBJ_290 = new SNBoutonDetail();
    OBJ_291 = new SNBoutonDetail();
    OBJ_292 = new SNBoutonDetail();
    OBJ_293 = new SNBoutonDetail();
    OBJ_294 = new SNBoutonDetail();
    OBJ_295 = new SNBoutonDetail();
    OBJ_296 = new SNBoutonDetail();
    OBJ_297 = new SNBoutonDetail();
    OBJ_298 = new SNBoutonDetail();
    OBJ_299 = new SNBoutonDetail();
    OBJ_300 = new SNBoutonDetail();
    OBJ_301 = new SNBoutonDetail();
    OBJ_302 = new SNBoutonDetail();
    OBJ_303 = new SNBoutonDetail();
    OBJ_304 = new SNBoutonDetail();
    OBJ_305 = new SNBoutonDetail();
    OBJ_306 = new SNBoutonDetail();
    OBJ_307 = new SNBoutonDetail();
    OBJ_91 = new JButton();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    OBJ_95 = new JButton();
    OBJ_231 = new JLabel();
    WTPTDB = new XRiTextField();
    WTDVDB = new XRiTextField();
    WTPTCR = new XRiTextField();
    WTDVCR = new XRiTextField();
    WTPTD2 = new XRiTextField();
    WTPTDX = new XRiTextField();
    WTPTD1 = new XRiTextField();
    WTPTDV = new XRiTextField();
    OBJ_219 = new JLabel();
    OBJ_217 = new JLabel();
    OBJ_221 = new JLabel();
    OBJ_220 = new JLabel();
    panel5 = new JPanel();
    WMOIX_SP = new JSpinner();
    BT_LET = new JButton();
    OBJ_241 = new JButton();
    OBJ_242 = new JButton();
    BT_DEL = new JButton();
    panel1 = new JPanel();
    WMOIX = new XRiTextField();
    label3 = new JLabel();
    label4 = new JLabel();
    panel2 = new JPanel();
    WTS1 = new XRiTextField();
    WTS2 = new XRiTextField();
    WTS3 = new XRiTextField();
    OBJ_206 = new JLabel();
    OBJ_213 = new JLabel();
    WLETDP = new XRiTextField();
    panel3 = new JPanel();
    PDPER = new XRiTextField();
    WTJDB = new XRiTextField();
    WTJCR = new XRiTextField();
    PDSOL1 = new XRiTextField();
    WSOCD = new XRiTextField();
    OBJ_228 = new JLabel();
    OBJ_232 = new JLabel();
    PDDB2 = new XRiTextField();
    PDCR2 = new XRiTextField();
    PDSOL2 = new XRiTextField();
    OBJ_236 = new JLabel();
    OBJ_229 = new JLabel();
    panel6 = new JPanel();
    OBJ_214 = new JLabel();
    OBJ_216 = new JLabel();
    OBJ_218 = new JLabel();
    panel7 = new JPanel();
    OBJ_212 = new JLabel();
    panel8 = new JPanel();
    rap01 = new JLabel();
    rap02 = new JLabel();
    rap03 = new JLabel();
    rap04 = new JLabel();
    rap05 = new JLabel();
    rap06 = new JLabel();
    rap07 = new JLabel();
    rap08 = new JLabel();
    rap09 = new JLabel();
    rap10 = new JLabel();
    rap11 = new JLabel();
    rap12 = new JLabel();
    rap13 = new JLabel();
    rap14 = new JLabel();
    rap15 = new JLabel();
    rap16 = new JLabel();
    rap17 = new JLabel();
    rap18 = new JLabel();
    rap19 = new JLabel();
    rap20 = new JLabel();
    lit01 = new JLabel();
    lit02 = new JLabel();
    lit03 = new JLabel();
    lit04 = new JLabel();
    lit05 = new JLabel();
    lit06 = new JLabel();
    lit07 = new JLabel();
    lit08 = new JLabel();
    lit09 = new JLabel();
    lit10 = new JLabel();
    lit11 = new JLabel();
    lit12 = new JLabel();
    lit13 = new JLabel();
    lit14 = new JLabel();
    lit15 = new JLabel();
    lit16 = new JLabel();
    lit17 = new JLabel();
    lit18 = new JLabel();
    lit19 = new JLabel();
    lit20 = new JLabel();
    SCROLLPANE_LIST = new JScrollPane();
    LE01 = new XRiTable();
    BTD3 = new JPopupMenu();
    OBJ_31 = new JMenuItem();
    OBJ_33 = new JMenuItem();
    OBJ_32 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_39 = new JMenuItem();
    OBJ_38 = new JMenuItem();
    BTDL1 = new JPopupMenu();
    OBJ_41 = new JMenuItem();
    OBJ_42 = new JMenuItem();
    OBJ_43 = new JMenuItem();
    OBJ_44 = new JMenuItem();
    OBJ_45 = new JMenuItem();
    OBJ_46 = new JMenuItem();
    OBJ_47 = new JMenuItem();
    OBJ_48 = new JMenuItem();
    OBJ_FOL = new JMenuItem();
    OBJ_FOL2 = new JMenuItem();
    mi_NumPiece = new JMenuItem();
    OBJ_TRS = new JMenuItem();
    OBJ_49 = new JMenuItem();
    OBJ_50 = new JMenuItem();
    AfficheLitige = new JMenuItem();
    OBJ_51 = new JMenuItem();
    OBJ_52 = new JMenuItem();
    OBJ_54 = new JMenuItem();
    OBJ_Echeancier = new JMenuItem();
    MILienUrl = new JMenuItem();
    OBJ_56 = new JMenuItem();
    OBJ_55 = new JMenuItem();
    BTD2 = new JPopupMenu();
    OBJ_93 = new JMenuItem();
    OBJ_92 = new JMenuItem();

    //======== this ========
    setPreferredSize(new Dimension(1370, 700));
    setInheritsPopupMenu(true);
    setMinimumSize(new Dimension(125, 74));
    setMaximumSize(new Dimension(1280, 800));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Visualisation des comptes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(1100, 40));
          p_tete_gauche.setMinimumSize(new Dimension(900, 40));
          p_tete_gauche.setMaximumSize(new Dimension(900, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- INDNCG ----
          INDNCG.setComponentPopupMenu(BTD);
          INDNCG.setName("INDNCG");

          //---- INDNCA ----
          INDNCA.setComponentPopupMenu(BTD);
          INDNCA.setName("INDNCA");

          //---- OBJ_120 ----
          OBJ_120.setText("@WLIB@");
          OBJ_120.setToolTipText("Informations compl\u00e9mentaires");
          OBJ_120.setFont(OBJ_120.getFont().deriveFont(OBJ_120.getFont().getStyle() | Font.BOLD));
          OBJ_120.setOpaque(false);
          OBJ_120.setName("OBJ_120");

          //---- OBJ_119 ----
          OBJ_119.setToolTipText("Informations compl\u00e9mentaires");
          OBJ_119.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_119.setName("OBJ_119");
          OBJ_119.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_119ActionPerformed(e);
            }
          });

          //---- OBJ_285 ----
          OBJ_285.setText("");
          OBJ_285.setToolTipText("Compte pr\u00e9c\u00e9dent");
          OBJ_285.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_285.setName("OBJ_285");
          OBJ_285.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_285ActionPerformed(e);
            }
          });

          //---- OBJ_118 ----
          OBJ_118.setText("");
          OBJ_118.setToolTipText("Compte suivant");
          OBJ_118.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_118.setName("OBJ_118");
          OBJ_118.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_118ActionPerformed(e);
            }
          });

          //---- label1 ----
          label1.setText("Soci\u00e9t\u00e9");
          label1.setName("label1");

          //---- label2 ----
          label2.setText("Num\u00e9ro de compte");
          label2.setName("label2");

          //---- WCLA1 ----
          WCLA1.setText("@WCLA1@");
          WCLA1.setToolTipText("Informations compl\u00e9mentaires");
          WCLA1.setFont(WCLA1.getFont().deriveFont(WCLA1.getFont().getStyle() | Font.BOLD));
          WCLA1.setOpaque(false);
          WCLA1.setName("WCLA1");

          //---- WCLA2 ----
          WCLA2.setText("@WCLA2@");
          WCLA2.setToolTipText("Informations compl\u00e9mentaires");
          WCLA2.setFont(WCLA2.getFont().deriveFont(WCLA2.getFont().getStyle() | Font.BOLD));
          WCLA2.setOpaque(false);
          WCLA2.setName("WCLA2");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(label1)
                .addGap(9, 9, 9)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(label2)
                .addGap(9, 9, 9)
                .addComponent(INDNCG, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INDNCA, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_119, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22)
                .addComponent(OBJ_120, GroupLayout.PREFERRED_SIZE, 276, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(OBJ_285, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(OBJ_118, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addComponent(WCLA1, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(WCLA2, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addComponent(label1, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addComponent(label2, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDNCG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDNCA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_119, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addComponent(OBJ_120, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_285, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_118, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addComponent(WCLA1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addComponent(WCLA2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- OBJ_112 ----
          OBJ_112.setText("@W1ATT@");
          OBJ_112.setForeground(Color.red);
          OBJ_112.setName("OBJ_112");
          p_tete_droite.add(OBJ_112);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Justification");
              riSousMenu_bt_consult.setToolTipText("Justification");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Historique");
              riSousMenu_bt_modif.setToolTipText("Historique");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("En cours");
              riSousMenu_bt_crea.setToolTipText("En cours");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Lettrage d'\u00e9critures");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_del ========
            {
              riSousMenuF_del.setName("riSousMenuF_del");

              //---- riSousMenu_bt_del ----
              riSousMenu_bt_del.setText("D\u00e9lettrage d'\u00e9critures");
              riSousMenu_bt_del.setToolTipText("Duplication");
              riSousMenu_bt_del.setName("riSousMenu_bt_del");
              riSousMenu_bt_del.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_dupliActionPerformed(e);
                }
              });
              riSousMenuF_del.add(riSousMenu_bt_del);
            }
            menus_haut.add(riSousMenuF_del);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Recherche d'\u00e9critures");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Options de visualisation");
              riSousMenu_bt7.setToolTipText("Options de visualisation");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Ech\u00e9ancier");
              riSousMenu_bt9.setToolTipText("Ech\u00e9ancier");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Plan comptable");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);

            //======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");

              //---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Fiche r\u00e9vision");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);

            //======== riSousMenu22 ========
            {
              riSousMenu22.setName("riSousMenu22");

              //---- riSousMenu_bt22 ----
              riSousMenu_bt22.setText("Ecritures sur ex. N-1");
              riSousMenu_bt22.setToolTipText("Ecritures sur exercice pr\u00e9c\u00e9dent");
              riSousMenu_bt22.setName("riSousMenu_bt22");
              riSousMenu_bt22.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt22ActionPerformed(e);
                }
              });
              riSousMenu22.add(riSousMenu_bt22);
            }
            menus_haut.add(riSousMenu22);

            //======== riSousMenu30 ========
            {
              riSousMenu30.setName("riSousMenu30");

              //---- riSousMenu_bt30 ----
              riSousMenu_bt30.setText("Lettrage du compte");
              riSousMenu_bt30.setToolTipText(" Demande de lettrage du compte");
              riSousMenu_bt30.setName("riSousMenu_bt30");
              riSousMenu_bt30.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt30ActionPerformed(e);
                }
              });
              riSousMenu30.add(riSousMenu_bt30);
            }
            menus_haut.add(riSousMenu30);

            //======== riSousMenu31 ========
            {
              riSousMenu31.setName("riSousMenu31");

              //---- riSousMenu_bt31 ----
              riSousMenu_bt31.setText("Edition");
              riSousMenu_bt31.setToolTipText("Edition du compte");
              riSousMenu_bt31.setName("riSousMenu_bt31");
              riSousMenu_bt31.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt31ActionPerformed(e);
                }
              });
              riSousMenu31.add(riSousMenu_bt31);
            }
            menus_haut.add(riSousMenu31);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Documents li\u00e9s");
              riSousMenu_bt14.setToolTipText("Documents li\u00e9s au compte");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);

            //======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");

              //---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Edition tableur");
              riSousMenu_bt15.setToolTipText("Edition tableur");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed(e);
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);

            //======== riSousMenu16 ========
            {
              riSousMenu16.setName("riSousMenu16");

              //---- riSousMenu_bt16 ----
              riSousMenu_bt16.setText("Calculette solde partiel");
              riSousMenu_bt16.setToolTipText("Calculette de solde partiel");
              riSousMenu_bt16.setName("riSousMenu_bt16");
              riSousMenu_bt16.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt16ActionPerformed(e);
                }
              });
              riSousMenu16.add(riSousMenu_bt16);
            }
            menus_haut.add(riSousMenu16);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Escompte de lettrage");
              riSousMenu_bt8.setToolTipText("Escompte de lettrage");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riMenu4 ========
            {
              riMenu4.setName("riMenu4");

              //---- riMenu_bt4 ----
              riMenu_bt4.setText("Relances");
              riMenu_bt4.setName("riMenu_bt4");
              riMenu4.add(riMenu_bt4);
            }
            menus_haut.add(riMenu4);

            //======== riSousMenu18 ========
            {
              riSousMenu18.setName("riSousMenu18");

              //---- riSousMenu_bt18 ----
              riSousMenu_bt18.setText("Relances (contr\u00f4le seul)");
              riSousMenu_bt18.setToolTipText("Relances client : contr\u00f4le seulement");
              riSousMenu_bt18.setName("riSousMenu_bt18");
              riSousMenu_bt18.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt18ActionPerformed(e);
                }
              });
              riSousMenu18.add(riSousMenu_bt18);
            }
            menus_haut.add(riSousMenu18);

            //======== riSousMenu19 ========
            {
              riSousMenu19.setName("riSousMenu19");

              //---- riSousMenu_bt19 ----
              riSousMenu_bt19.setText("Relances client ");
              riSousMenu_bt19.setToolTipText("Relances client avec mise \u00e0 jour de niveau");
              riSousMenu_bt19.setName("riSousMenu_bt19");
              riSousMenu_bt19.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt19ActionPerformed(e);
                }
              });
              riSousMenu19.add(riSousMenu_bt19);
            }
            menus_haut.add(riSousMenu19);

            //======== riMenu5 ========
            {
              riMenu5.setName("riMenu5");

              //---- riMenu_bt5 ----
              riMenu_bt5.setText("Affichages");
              riMenu_bt5.setName("riMenu_bt5");
              riMenu5.add(riMenu_bt5);
            }
            menus_haut.add(riMenu5);

            //======== riSousMenu23 ========
            {
              riSousMenu23.setName("riSousMenu23");

              //---- riSousMenu_bt23 ----
              riSousMenu_bt23.setText("Ecritures par r\u00e9f. lettrage");
              riSousMenu_bt23.setToolTipText("\u00e9critures par r\u00e9f\u00e9rence de lettrage");
              riSousMenu_bt23.setName("riSousMenu_bt23");
              riSousMenu_bt23.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt23ActionPerformed(e);
                }
              });
              riSousMenu23.add(riSousMenu_bt23);
            }
            menus_haut.add(riSousMenu23);

            //======== riSousMenu24 ========
            {
              riSousMenu24.setName("riSousMenu24");

              //---- riSousMenu_bt24 ----
              riSousMenu_bt24.setText(" En devise");
              riSousMenu_bt24.setName("riSousMenu_bt24");
              riSousMenu_bt24.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt24ActionPerformed(e);
                }
              });
              riSousMenu24.add(riSousMenu_bt24);
            }
            menus_haut.add(riSousMenu24);

            //======== riSousMenu25 ========
            {
              riSousMenu25.setName("riSousMenu25");

              //---- riSousMenu_bt25 ----
              riSousMenu_bt25.setText("Analyse budg\u00e9taire");
              riSousMenu_bt25.setToolTipText("Visualisation analyse budg\u00e9taire");
              riSousMenu_bt25.setName("riSousMenu_bt25");
              riSousMenu_bt25.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt25ActionPerformed(e);
                }
              });
              riSousMenu25.add(riSousMenu_bt25);
            }
            menus_haut.add(riSousMenu25);

            //======== riSousMenu26 ========
            {
              riSousMenu26.setName("riSousMenu26");

              //---- riSousMenu_bt26 ----
              riSousMenu_bt26.setText("Ecritures tri\u00e9es/ montant");
              riSousMenu_bt26.setToolTipText("On / Off affichage des \u00e9critures tri\u00e9es par montant");
              riSousMenu_bt26.setName("riSousMenu_bt26");
              riSousMenu_bt26.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt26ActionPerformed(e);
                }
              });
              riSousMenu26.add(riSousMenu_bt26);
            }
            menus_haut.add(riSousMenu26);

            //======== riSousMenu27 ========
            {
              riSousMenu27.setName("riSousMenu27");

              //---- riSousMenu_bt27 ----
              riSousMenu_bt27.setText("En cours administratif");
              riSousMenu_bt27.setToolTipText(" en cours administratif (client)");
              riSousMenu_bt27.setName("riSousMenu_bt27");
              riSousMenu_bt27.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt27ActionPerformed(e);
                }
              });
              riSousMenu27.add(riSousMenu_bt27);
            }
            menus_haut.add(riSousMenu27);

            //======== riSousMenu28 ========
            {
              riSousMenu28.setName("riSousMenu28");

              //---- riSousMenu_bt28 ----
              riSousMenu_bt28.setText("Fiche de r\u00e9vision");
              riSousMenu_bt28.setToolTipText("Fiche de r\u00e9vision");
              riSousMenu_bt28.setName("riSousMenu_bt28");
              riSousMenu_bt28.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt28ActionPerformed(e);
                }
              });
              riSousMenu28.add(riSousMenu_bt28);
            }
            menus_haut.add(riSousMenu28);

            //======== riSousMenu29 ========
            {
              riSousMenu29.setName("riSousMenu29");

              //---- riSousMenu_bt29 ----
              riSousMenu_bt29.setText("Ecr. lettr\u00e9es/non lettr\u00e9es");
              riSousMenu_bt29.setToolTipText("\u00e9critures lettr\u00e9es/non lettr\u00e9es");
              riSousMenu_bt29.setName("riSousMenu_bt29");
              riSousMenu_bt29.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt29ActionPerformed(e);
                }
              });
              riSousMenu29.add(riSousMenu_bt29);
            }
            menus_haut.add(riSousMenu29);

            //======== riSousMenu32 ========
            {
              riSousMenu32.setName("riSousMenu32");

              //---- riSousMenu_bt32 ----
              riSousMenu_bt32.setText("Plafond d'encours");
              riSousMenu_bt32.setToolTipText("Plafond d'encours");
              riSousMenu_bt32.setName("riSousMenu_bt32");
              riSousMenu_bt32.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt32ActionPerformed(e);
                }
              });
              riSousMenu32.add(riSousMenu_bt32);
            }
            menus_haut.add(riSousMenu32);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setPreferredSize(new Dimension(1100, 725));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new BorderLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1010, 623));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(1000, 623));
          p_contenu.setMaximumSize(new Dimension(1280, 800));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(new GridBagLayout());
          ((GridBagLayout)p_contenu.getLayout()).columnWidths = new int[] {1109, 0};
          ((GridBagLayout)p_contenu.getLayout()).rowHeights = new int[] {83, 0, 0, 0};
          ((GridBagLayout)p_contenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout)p_contenu.getLayout()).rowWeights = new double[] {0.0, 1.0, 1.0, 1.0E-4};

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setMinimumSize(new Dimension(1010, 62));
            xTitledPanel2.setPreferredSize(new Dimension(1010, 62));
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();

            //---- OBJ_127 ----
            OBJ_127.setText("Monnaie d'affichage");
            OBJ_127.setName("OBJ_127");

            //---- WLDEV ----
            WLDEV.setName("WLDEV");

            //---- OBJ_130 ----
            OBJ_130.setText("Lettrage");
            OBJ_130.setName("OBJ_130");

            //---- WPTG ----
            WPTG.setName("WPTG");

            //---- OBJ_125 ----
            OBJ_125.setText("");
            OBJ_125.setToolTipText("Composer le num\u00e9ro de t\u00e9l\u00e9phone");
            OBJ_125.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_125.setName("OBJ_125");
            OBJ_125.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_125ActionPerformed(e);
              }
            });

            //---- WTEL ----
            WTEL.setToolTipText("Num\u00e9ro de t\u00e9l\u00e9phone");
            WTEL.setName("WTEL");

            //---- OBJ_132 ----
            OBJ_132.setText("Solde");
            OBJ_132.setName("OBJ_132");

            //---- CPSOCD ----
            CPSOCD.setName("CPSOCD");

            //---- CPSENS ----
            CPSENS.setText("@CPSENS@");
            CPSENS.setName("CPSENS");

            //---- WTEL1 ----
            WTEL1.setToolTipText("Num\u00e9ro de t\u00e9l\u00e9phone");
            WTEL1.setName("WTEL1");

            GroupLayout xTitledPanel2ContentContainerLayout = new GroupLayout(xTitledPanel2ContentContainer);
            xTitledPanel2ContentContainer.setLayout(xTitledPanel2ContentContainerLayout);
            xTitledPanel2ContentContainerLayout.setHorizontalGroup(
              xTitledPanel2ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addComponent(OBJ_127, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addComponent(WLDEV, GroupLayout.PREFERRED_SIZE, 156, GroupLayout.PREFERRED_SIZE)
                  .addGap(4, 4, 4)
                  .addComponent(OBJ_125, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addComponent(WTEL, GroupLayout.PREFERRED_SIZE, 172, GroupLayout.PREFERRED_SIZE)
                  .addGap(8, 8, 8)
                  .addComponent(WTEL1, GroupLayout.PREFERRED_SIZE, 172, GroupLayout.PREFERRED_SIZE)
                  .addGap(13, 13, 13)
                  .addComponent(OBJ_130, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, 0)
                  .addComponent(WPTG, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addGap(30, 30, 30)
                  .addComponent(OBJ_132, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                  .addGap(10, 10, 10)
                  .addComponent(CPSOCD, GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE)
                  .addGap(6, 6, 6)
                  .addComponent(CPSENS, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
            );
            xTitledPanel2ContentContainerLayout.setVerticalGroup(
              xTitledPanel2ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_127, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addComponent(WLDEV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addComponent(OBJ_125, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addComponent(WTEL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addComponent(WTEL1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_130, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addComponent(WPTG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_132, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addComponent(CPSOCD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addComponent(CPSENS, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
            );
          }
          p_contenu.add(xTitledPanel2, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(5, 5, 5, 5), 0, 0));

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setMinimumSize(new Dimension(1000, 519));
            xTitledPanel1.setPreferredSize(new Dimension(1000, 519));
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //======== panel4 ========
            {
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);

              //---- OBJ_288 ----
              OBJ_288.setText("");
              OBJ_288.setToolTipText("Le bloc-notes associ\u00e9 \u00e0 cette ligne contient un texte");
              OBJ_288.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_288.setName("OBJ_288");
              OBJ_288.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_288ActionPerformed(e);
                }
              });
              panel4.add(OBJ_288);
              OBJ_288.setBounds(5, 20, 13, 16);

              //---- OBJ_289 ----
              OBJ_289.setText("");
              OBJ_289.setToolTipText("Le bloc-notes associ\u00e9 \u00e0 cette ligne contient un texte");
              OBJ_289.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_289.setName("OBJ_289");
              OBJ_289.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_289ActionPerformed(e);
                }
              });
              panel4.add(OBJ_289);
              OBJ_289.setBounds(5, 36, 13, 16);

              //---- OBJ_290 ----
              OBJ_290.setText("");
              OBJ_290.setToolTipText("Le bloc-notes associ\u00e9 \u00e0 cette ligne contient un texte");
              OBJ_290.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_290.setName("OBJ_290");
              OBJ_290.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_290ActionPerformed(e);
                }
              });
              panel4.add(OBJ_290);
              OBJ_290.setBounds(5, 52, 13, 16);

              //---- OBJ_291 ----
              OBJ_291.setText("");
              OBJ_291.setToolTipText("Le bloc-notes associ\u00e9 \u00e0 cette ligne contient un texte");
              OBJ_291.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_291.setName("OBJ_291");
              OBJ_291.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_291ActionPerformed(e);
                }
              });
              panel4.add(OBJ_291);
              OBJ_291.setBounds(5, 68, 13, 16);

              //---- OBJ_292 ----
              OBJ_292.setText("");
              OBJ_292.setToolTipText("Le bloc-notes associ\u00e9 \u00e0 cette ligne contient un texte");
              OBJ_292.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_292.setName("OBJ_292");
              OBJ_292.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_292ActionPerformed(e);
                }
              });
              panel4.add(OBJ_292);
              OBJ_292.setBounds(5, 84, 13, 16);

              //---- OBJ_293 ----
              OBJ_293.setText("");
              OBJ_293.setToolTipText("Le bloc-notes associ\u00e9 \u00e0 cette ligne contient un texte");
              OBJ_293.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_293.setName("OBJ_293");
              OBJ_293.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_293ActionPerformed(e);
                }
              });
              panel4.add(OBJ_293);
              OBJ_293.setBounds(5, 100, 13, 16);

              //---- OBJ_294 ----
              OBJ_294.setText("");
              OBJ_294.setToolTipText("Le bloc-notes associ\u00e9 \u00e0 cette ligne contient un texte");
              OBJ_294.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_294.setName("OBJ_294");
              OBJ_294.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_294ActionPerformed(e);
                }
              });
              panel4.add(OBJ_294);
              OBJ_294.setBounds(5, 116, 13, 16);

              //---- OBJ_295 ----
              OBJ_295.setText("");
              OBJ_295.setToolTipText("Le bloc-notes associ\u00e9 \u00e0 cette ligne contient un texte");
              OBJ_295.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_295.setName("OBJ_295");
              OBJ_295.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_295ActionPerformed(e);
                }
              });
              panel4.add(OBJ_295);
              OBJ_295.setBounds(5, 132, 13, 16);

              //---- OBJ_296 ----
              OBJ_296.setText("");
              OBJ_296.setToolTipText("Le bloc-notes associ\u00e9 \u00e0 cette ligne contient un texte");
              OBJ_296.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_296.setName("OBJ_296");
              OBJ_296.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_296ActionPerformed(e);
                }
              });
              panel4.add(OBJ_296);
              OBJ_296.setBounds(5, 148, 13, 16);

              //---- OBJ_297 ----
              OBJ_297.setText("");
              OBJ_297.setToolTipText("Le bloc-notes associ\u00e9 \u00e0 cette ligne contient un texte");
              OBJ_297.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_297.setName("OBJ_297");
              OBJ_297.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_297ActionPerformed(e);
                }
              });
              panel4.add(OBJ_297);
              OBJ_297.setBounds(5, 164, 13, 16);

              //---- OBJ_298 ----
              OBJ_298.setText("");
              OBJ_298.setToolTipText("Le bloc-notes associ\u00e9 \u00e0 cette ligne contient un texte");
              OBJ_298.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_298.setName("OBJ_298");
              OBJ_298.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_298ActionPerformed(e);
                }
              });
              panel4.add(OBJ_298);
              OBJ_298.setBounds(5, 180, 13, 16);

              //---- OBJ_299 ----
              OBJ_299.setText("");
              OBJ_299.setToolTipText("Le bloc-notes associ\u00e9 \u00e0 cette ligne contient un texte");
              OBJ_299.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_299.setName("OBJ_299");
              OBJ_299.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_299ActionPerformed(e);
                }
              });
              panel4.add(OBJ_299);
              OBJ_299.setBounds(5, 196, 13, 16);

              //---- OBJ_300 ----
              OBJ_300.setText("");
              OBJ_300.setToolTipText("Le bloc-notes associ\u00e9 \u00e0 cette ligne contient un texte");
              OBJ_300.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_300.setName("OBJ_300");
              OBJ_300.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_300ActionPerformed(e);
                }
              });
              panel4.add(OBJ_300);
              OBJ_300.setBounds(5, 212, 13, 16);

              //---- OBJ_301 ----
              OBJ_301.setText("");
              OBJ_301.setToolTipText("Le bloc-notes associ\u00e9 \u00e0 cette ligne contient un texte");
              OBJ_301.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_301.setName("OBJ_301");
              OBJ_301.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_301ActionPerformed(e);
                }
              });
              panel4.add(OBJ_301);
              OBJ_301.setBounds(5, 228, 13, 16);

              //---- OBJ_302 ----
              OBJ_302.setText("");
              OBJ_302.setToolTipText("Le bloc-notes associ\u00e9 \u00e0 cette ligne contient un texte");
              OBJ_302.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_302.setName("OBJ_302");
              OBJ_302.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_302ActionPerformed(e);
                }
              });
              panel4.add(OBJ_302);
              OBJ_302.setBounds(5, 244, 13, 16);

              //---- OBJ_303 ----
              OBJ_303.setText("");
              OBJ_303.setToolTipText("Le bloc-notes associ\u00e9 \u00e0 cette ligne contient un texte");
              OBJ_303.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_303.setName("OBJ_303");
              OBJ_303.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_303ActionPerformed(e);
                }
              });
              panel4.add(OBJ_303);
              OBJ_303.setBounds(5, 324, 13, 16);

              //---- OBJ_304 ----
              OBJ_304.setText("");
              OBJ_304.setToolTipText("Le bloc-notes associ\u00e9 \u00e0 cette ligne contient un texte");
              OBJ_304.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_304.setName("OBJ_304");
              OBJ_304.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_304ActionPerformed(e);
                }
              });
              panel4.add(OBJ_304);
              OBJ_304.setBounds(5, 308, 13, 16);

              //---- OBJ_305 ----
              OBJ_305.setText("");
              OBJ_305.setToolTipText("Le bloc-notes associ\u00e9 \u00e0 cette ligne contient un texte");
              OBJ_305.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_305.setName("OBJ_305");
              OBJ_305.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_305ActionPerformed(e);
                }
              });
              panel4.add(OBJ_305);
              OBJ_305.setBounds(5, 292, 13, 16);

              //---- OBJ_306 ----
              OBJ_306.setText("");
              OBJ_306.setToolTipText("Le bloc-notes associ\u00e9 \u00e0 cette ligne contient un texte");
              OBJ_306.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_306.setName("OBJ_306");
              OBJ_306.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_306ActionPerformed(e);
                }
              });
              panel4.add(OBJ_306);
              OBJ_306.setBounds(5, 276, 13, 16);

              //---- OBJ_307 ----
              OBJ_307.setText("");
              OBJ_307.setToolTipText("Le bloc-notes associ\u00e9 \u00e0 cette ligne contient un texte");
              OBJ_307.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_307.setName("OBJ_307");
              OBJ_307.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_307ActionPerformed(e);
                }
              });
              panel4.add(OBJ_307);
              OBJ_307.setBounds(5, 260, 13, 16);
            }
            xTitledPanel1ContentContainer.add(panel4);
            panel4.setBounds(0, 10, 20, 345);

            //---- OBJ_91 ----
            OBJ_91.setText("");
            OBJ_91.setToolTipText("D\u00e9but de la liste");
            OBJ_91.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_91.setMaximumSize(new Dimension(28, 30));
            OBJ_91.setMinimumSize(new Dimension(28, 30));
            OBJ_91.setPreferredSize(new Dimension(28, 30));
            OBJ_91.setName("OBJ_91");
            OBJ_91.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_91ActionPerformed(e);
              }
            });
            xTitledPanel1ContentContainer.add(OBJ_91);
            OBJ_91.setBounds(1020, 5, 25, 30);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setMaximumSize(new Dimension(28, 130));
            BT_PGUP.setMinimumSize(new Dimension(28, 130));
            BT_PGUP.setPreferredSize(new Dimension(28, 130));
            BT_PGUP.setName("BT_PGUP");
            xTitledPanel1ContentContainer.add(BT_PGUP);
            BT_PGUP.setBounds(1020, 40, 25, 134);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setMaximumSize(new Dimension(28, 130));
            BT_PGDOWN.setMinimumSize(new Dimension(28, 130));
            BT_PGDOWN.setPreferredSize(new Dimension(28, 130));
            BT_PGDOWN.setName("BT_PGDOWN");
            xTitledPanel1ContentContainer.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(1020, 180, 25, 135);

            //---- OBJ_95 ----
            OBJ_95.setText("");
            OBJ_95.setToolTipText("Fin de la liste");
            OBJ_95.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_95.setMaximumSize(new Dimension(28, 30));
            OBJ_95.setMinimumSize(new Dimension(28, 30));
            OBJ_95.setPreferredSize(new Dimension(28, 30));
            OBJ_95.setName("OBJ_95");
            OBJ_95.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_95ActionPerformed(e);
              }
            });
            xTitledPanel1ContentContainer.add(OBJ_95);
            OBJ_95.setBounds(1020, 320, 25, 30);

            //---- OBJ_231 ----
            OBJ_231.setText("@W1ATT@");
            OBJ_231.setName("OBJ_231");
            xTitledPanel1ContentContainer.add(OBJ_231);
            OBJ_231.setBounds(155, 414, 1, 20);

            //---- WTPTDB ----
            WTPTDB.setHorizontalAlignment(SwingConstants.RIGHT);
            WTPTDB.setPreferredSize(new Dimension(110, 28));
            WTPTDB.setMinimumSize(new Dimension(110, 28));
            WTPTDB.setMaximumSize(new Dimension(110, 28));
            WTPTDB.setName("WTPTDB");
            xTitledPanel1ContentContainer.add(WTPTDB);
            WTPTDB.setBounds(new Rectangle(new Point(175, 359), WTPTDB.getPreferredSize()));

            //---- WTDVDB ----
            WTDVDB.setHorizontalAlignment(SwingConstants.RIGHT);
            WTDVDB.setName("WTDVDB");
            xTitledPanel1ContentContainer.add(WTDVDB);
            WTDVDB.setBounds(175, 359, 110, WTDVDB.getPreferredSize().height);

            //---- WTPTCR ----
            WTPTCR.setHorizontalAlignment(SwingConstants.RIGHT);
            WTPTCR.setPreferredSize(new Dimension(110, 28));
            WTPTCR.setMinimumSize(new Dimension(110, 28));
            WTPTCR.setMaximumSize(new Dimension(110, 28));
            WTPTCR.setName("WTPTCR");
            xTitledPanel1ContentContainer.add(WTPTCR);
            WTPTCR.setBounds(new Rectangle(new Point(325, 355), WTPTCR.getPreferredSize()));

            //---- WTDVCR ----
            WTDVCR.setHorizontalAlignment(SwingConstants.RIGHT);
            WTDVCR.setName("WTDVCR");
            xTitledPanel1ContentContainer.add(WTDVCR);
            WTDVCR.setBounds(325, 359, 110, WTDVCR.getPreferredSize().height);

            //---- WTPTD2 ----
            WTPTD2.setHorizontalAlignment(SwingConstants.RIGHT);
            WTPTD2.setForeground(Color.red);
            WTPTD2.setToolTipText("WTPTD2");
            WTPTD2.setPreferredSize(new Dimension(110, 28));
            WTPTD2.setMinimumSize(new Dimension(110, 28));
            WTPTD2.setMaximumSize(new Dimension(110, 28));
            WTPTD2.setName("WTPTD2");
            xTitledPanel1ContentContainer.add(WTPTD2);
            WTPTD2.setBounds(new Rectangle(new Point(475, 359), WTPTD2.getPreferredSize()));

            //---- WTPTDX ----
            WTPTDX.setHorizontalAlignment(SwingConstants.RIGHT);
            WTPTDX.setName("WTPTDX");
            xTitledPanel1ContentContainer.add(WTPTDX);
            WTPTDX.setBounds(475, 359, 110, WTPTDX.getPreferredSize().height);

            //---- WTPTD1 ----
            WTPTD1.setHorizontalAlignment(SwingConstants.RIGHT);
            WTPTD1.setForeground(Color.red);
            WTPTD1.setToolTipText("WTPTD1");
            WTPTD1.setName("WTPTD1");
            xTitledPanel1ContentContainer.add(WTPTD1);
            WTPTD1.setBounds(475, 359, 90, WTPTD1.getPreferredSize().height);

            //---- WTPTDV ----
            WTPTDV.setHorizontalAlignment(SwingConstants.RIGHT);
            WTPTDV.setPreferredSize(new Dimension(90, 28));
            WTPTDV.setMinimumSize(new Dimension(90, 28));
            WTPTDV.setName("WTPTDV");
            xTitledPanel1ContentContainer.add(WTPTDV);
            WTPTDV.setBounds(new Rectangle(new Point(475, 364), WTPTDV.getPreferredSize()));

            //---- OBJ_219 ----
            OBJ_219.setText("Cr");
            OBJ_219.setForeground(Color.black);
            OBJ_219.setName("OBJ_219");
            xTitledPanel1ContentContainer.add(OBJ_219);
            OBJ_219.setBounds(440, 363, 16, 20);

            //---- OBJ_217 ----
            OBJ_217.setText("Db");
            OBJ_217.setForeground(Color.black);
            OBJ_217.setName("OBJ_217");
            xTitledPanel1ContentContainer.add(OBJ_217);
            OBJ_217.setBounds(290, 363, 21, 20);

            //---- OBJ_221 ----
            OBJ_221.setText("@WDEVF@");
            OBJ_221.setName("OBJ_221");
            xTitledPanel1ContentContainer.add(OBJ_221);
            OBJ_221.setBounds(590, 363, 35, 20);

            //---- OBJ_220 ----
            OBJ_220.setText("@L1DEV@");
            OBJ_220.setName("OBJ_220");
            xTitledPanel1ContentContainer.add(OBJ_220);
            OBJ_220.setBounds(635, 363, 30, 20);

            //======== panel5 ========
            {
              panel5.setOpaque(false);
              panel5.setPreferredSize(new Dimension(200, 115));
              panel5.setMinimumSize(new Dimension(200, 115));
              panel5.setName("panel5");
              panel5.setLayout(null);

              //---- WMOIX_SP ----
              WMOIX_SP.setToolTipText("Valeur de d\u00e9calage dans la liste");
              WMOIX_SP.setComponentPopupMenu(BTD);
              WMOIX_SP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              WMOIX_SP.setModel(new SpinnerListModel(new String[] {" ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "90", "91", "92", "93", "94", "95", "96", "97", "98", "99"}) {
                { setValue("1"); }
              });
              WMOIX_SP.setName("WMOIX_SP");
              panel5.add(WMOIX_SP);
              WMOIX_SP.setBounds(145, 79, 50, WMOIX_SP.getPreferredSize().height);

              //---- BT_LET ----
              BT_LET.setText("Validation du lettrage");
              BT_LET.setToolTipText("Lettrage");
              BT_LET.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_LET.setName("BT_LET");
              BT_LET.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_238ActionPerformed(e);
                }
              });
              panel5.add(BT_LET);
              BT_LET.setBounds(20, 5, 175, 26);

              //---- OBJ_241 ----
              OBJ_241.setToolTipText("D\u00e9calage en plus dans la liste");
              OBJ_241.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_241.setName("OBJ_241");
              OBJ_241.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_241ActionPerformed(e);
                }
              });
              panel5.add(OBJ_241);
              OBJ_241.setBounds(95, 78, 30, 30);

              //---- OBJ_242 ----
              OBJ_242.setToolTipText("D\u00e9calage en moins dans la liste");
              OBJ_242.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_242.setName("OBJ_242");
              OBJ_242.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_242ActionPerformed(e);
                }
              });
              panel5.add(OBJ_242);
              OBJ_242.setBounds(65, 78, 30, 30);

              //---- BT_DEL ----
              BT_DEL.setText("Validation du d\u00e9lettrage");
              BT_DEL.setToolTipText("Lettrage");
              BT_DEL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_DEL.setActionCommand("Validation du d\u00e9lettrage");
              BT_DEL.setName("BT_DEL");
              BT_DEL.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_238ActionPerformed(e);
                }
              });
              panel5.add(BT_DEL);
              BT_DEL.setBounds(20, 5, 175, 26);

              //======== panel1 ========
              {
                panel1.setOpaque(false);
                panel1.setName("panel1");

                //---- WMOIX ----
                WMOIX.setToolTipText("Code d'autorisation permettant un lettrage forc\u00e9");
                WMOIX.setComponentPopupMenu(BTD2);
                WMOIX.setName("WMOIX");

                //---- label3 ----
                label3.setText("Lettrage forc\u00e9");
                label3.setName("label3");

                //---- label4 ----
                label4.setText("D\u00e9lettrage forc\u00e9");
                label4.setName("label4");

                GroupLayout panel1Layout = new GroupLayout(panel1);
                panel1.setLayout(panel1Layout);
                panel1Layout.setHorizontalGroup(
                  panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addGroup(panel1Layout.createParallelGroup()
                        .addComponent(label4, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label3, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                      .addGap(5, 5, 5)
                      .addComponent(WMOIX, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                );
                panel1Layout.setVerticalGroup(
                  panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(11, 11, 11)
                      .addGroup(panel1Layout.createParallelGroup()
                        .addComponent(label4)
                        .addComponent(label3)))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(WMOIX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                );
              }
              panel5.add(panel1);
              panel1.setBounds(15, 35, 185, 40);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel5.getComponentCount(); i++) {
                  Rectangle bounds = panel5.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel5.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel5.setMinimumSize(preferredSize);
                panel5.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel5);
            panel5.setBounds(new Rectangle(new Point(850, 360), panel5.getPreferredSize()));

            //======== panel2 ========
            {
              panel2.setOpaque(false);
              panel2.setMinimumSize(new Dimension(820, 105));
              panel2.setPreferredSize(new Dimension(820, 105));
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- WTS1 ----
              WTS1.setComponentPopupMenu(BTD3);
              WTS1.setMinimumSize(new Dimension(810, 28));
              WTS1.setPreferredSize(new Dimension(810, 28));
              WTS1.setName("WTS1");
              WTS1.addFocusListener(new FocusAdapter() {
                @Override
                public void focusGained(FocusEvent e) {
                  WTS1FocusGained(e);
                }
              });
              panel2.add(WTS1);
              WTS1.setBounds(5, 25, 810, WTS1.getPreferredSize().height);

              //---- WTS2 ----
              WTS2.setComponentPopupMenu(BTD3);
              WTS2.setMinimumSize(new Dimension(810, 28));
              WTS2.setPreferredSize(new Dimension(810, 28));
              WTS2.setName("WTS2");
              WTS2.addFocusListener(new FocusAdapter() {
                @Override
                public void focusGained(FocusEvent e) {
                  WTS2FocusGained(e);
                }
              });
              panel2.add(WTS2);
              WTS2.setBounds(5, 50, 810, WTS2.getPreferredSize().height);

              //---- WTS3 ----
              WTS3.setComponentPopupMenu(BTD3);
              WTS3.setMinimumSize(new Dimension(810, 28));
              WTS3.setPreferredSize(new Dimension(810, 28));
              WTS3.setName("WTS3");
              WTS3.addFocusListener(new FocusAdapter() {
                @Override
                public void focusGained(FocusEvent e) {
                  WTS3FocusGained(e);
                }
              });
              panel2.add(WTS3);
              WTS3.setBounds(5, 75, 810, WTS3.getPreferredSize().height);

              //---- OBJ_206 ----
              OBJ_206.setText("Lettrage en cours");
              OBJ_206.setName("OBJ_206");
              panel2.add(OBJ_206);
              OBJ_206.setBounds(5, 5, 104, 18);

              //---- OBJ_213 ----
              OBJ_213.setText("Donnez la lettre");
              OBJ_213.setName("OBJ_213");
              panel2.add(OBJ_213);
              OBJ_213.setBounds(675, 5, 95, 18);

              //---- WLETDP ----
              WLETDP.setHorizontalAlignment(SwingConstants.LEFT);
              WLETDP.setToolTipText("Lettre utilis\u00e9e pour lettrer que vous recherchez");
              WLETDP.setName("WLETDP");
              panel2.add(WLETDP);
              WLETDP.setBounds(780, 0, 35, WLETDP.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel2);
            panel2.setBounds(20, 385, 821, panel2.getPreferredSize().height);

            //======== panel3 ========
            {
              panel3.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.ABOVE_TOP));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- PDPER ----
              PDPER.setComponentPopupMenu(BTD);
              PDPER.setName("PDPER");
              panel3.add(PDPER);
              PDPER.setBounds(20, 35, 60, PDPER.getPreferredSize().height);

              //---- WTJDB ----
              WTJDB.setHorizontalAlignment(SwingConstants.RIGHT);
              WTJDB.setName("WTJDB");
              panel3.add(WTJDB);
              WTJDB.setBounds(118, 35, 114, WTJDB.getPreferredSize().height);

              //---- WTJCR ----
              WTJCR.setHorizontalAlignment(SwingConstants.RIGHT);
              WTJCR.setName("WTJCR");
              panel3.add(WTJCR);
              WTJCR.setBounds(238, 35, 114, WTJCR.getPreferredSize().height);

              //---- PDSOL1 ----
              PDSOL1.setHorizontalAlignment(SwingConstants.RIGHT);
              PDSOL1.setName("PDSOL1");
              panel3.add(PDSOL1);
              PDSOL1.setBounds(359, 35, 114, PDSOL1.getPreferredSize().height);

              //---- WSOCD ----
              WSOCD.setHorizontalAlignment(SwingConstants.RIGHT);
              WSOCD.setName("WSOCD");
              panel3.add(WSOCD);
              WSOCD.setBounds(359, 35, 114, WSOCD.getPreferredSize().height);

              //---- OBJ_228 ----
              OBJ_228.setText("@WSENS@");
              OBJ_228.setHorizontalAlignment(SwingConstants.LEFT);
              OBJ_228.setName("OBJ_228");
              panel3.add(OBJ_228);
              OBJ_228.setBounds(522, 40, 42, 20);

              //---- OBJ_232 ----
              OBJ_232.setText("Exigible");
              OBJ_232.setForeground(Color.red);
              OBJ_232.setFont(OBJ_232.getFont().deriveFont(OBJ_232.getFont().getSize() + 2f));
              OBJ_232.setName("OBJ_232");
              panel3.add(OBJ_232);
              OBJ_232.setBounds(20, 70, 69, OBJ_232.getPreferredSize().height);

              //---- PDDB2 ----
              PDDB2.setHorizontalAlignment(SwingConstants.RIGHT);
              PDDB2.setName("PDDB2");
              panel3.add(PDDB2);
              PDDB2.setBounds(118, 65, 114, PDDB2.getPreferredSize().height);

              //---- PDCR2 ----
              PDCR2.setHorizontalAlignment(SwingConstants.RIGHT);
              PDCR2.setName("PDCR2");
              panel3.add(PDCR2);
              PDCR2.setBounds(238, 65, 114, PDCR2.getPreferredSize().height);

              //---- PDSOL2 ----
              PDSOL2.setHorizontalAlignment(SwingConstants.RIGHT);
              PDSOL2.setName("PDSOL2");
              panel3.add(PDSOL2);
              PDSOL2.setBounds(359, 65, 114, PDSOL2.getPreferredSize().height);

              //---- OBJ_236 ----
              OBJ_236.setText("@PDSNS2@");
              OBJ_236.setHorizontalAlignment(SwingConstants.LEFT);
              OBJ_236.setName("OBJ_236");
              panel3.add(OBJ_236);
              OBJ_236.setBounds(478, 70, 42, 20);

              //---- OBJ_229 ----
              OBJ_229.setText("@PDSNS1@");
              OBJ_229.setHorizontalAlignment(SwingConstants.LEFT);
              OBJ_229.setName("OBJ_229");
              panel3.add(OBJ_229);
              OBJ_229.setBounds(480, 40, 42, 20);

              //======== panel6 ========
              {
                panel6.setOpaque(false);
                panel6.setName("panel6");
                panel6.setLayout(null);

                //---- OBJ_214 ----
                OBJ_214.setText("Total d\u00e9bit");
                OBJ_214.setFont(OBJ_214.getFont().deriveFont(OBJ_214.getFont().getSize() + 2f));
                OBJ_214.setName("OBJ_214");
                panel6.add(OBJ_214);
                OBJ_214.setBounds(0, 5, 114, 20);

                //---- OBJ_216 ----
                OBJ_216.setText("Total cr\u00e9dit");
                OBJ_216.setFont(OBJ_216.getFont().deriveFont(OBJ_216.getFont().getSize() + 2f));
                OBJ_216.setName("OBJ_216");
                panel6.add(OBJ_216);
                OBJ_216.setBounds(120, 5, 114, 20);

                //---- OBJ_218 ----
                OBJ_218.setText("Solde");
                OBJ_218.setFont(OBJ_218.getFont().deriveFont(OBJ_218.getFont().getSize() + 2f));
                OBJ_218.setName("OBJ_218");
                panel6.add(OBJ_218);
                OBJ_218.setBounds(245, 5, 114, 20);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < panel6.getComponentCount(); i++) {
                    Rectangle bounds = panel6.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = panel6.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  panel6.setMinimumSize(preferredSize);
                  panel6.setPreferredSize(preferredSize);
                }
              }
              panel3.add(panel6);
              panel6.setBounds(120, 10, 365, 30);

              //======== panel7 ========
              {
                panel7.setOpaque(false);
                panel7.setName("panel7");
                panel7.setLayout(null);

                //---- OBJ_212 ----
                OBJ_212.setText("Totaux au");
                OBJ_212.setFont(OBJ_212.getFont().deriveFont(OBJ_212.getFont().getSize() + 2f));
                OBJ_212.setName("OBJ_212");
                panel7.add(OBJ_212);
                OBJ_212.setBounds(5, 5, 66, OBJ_212.getPreferredSize().height);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < panel7.getComponentCount(); i++) {
                    Rectangle bounds = panel7.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = panel7.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  panel7.setMinimumSize(preferredSize);
                  panel7.setPreferredSize(preferredSize);
                }
              }
              panel3.add(panel7);
              panel7.setBounds(15, 10, 80, 30);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel3);
            panel3.setBounds(20, 389, 614, 105);

            //======== panel8 ========
            {
              panel8.setName("panel8");
              panel8.setLayout(null);

              //---- rap01 ----
              rap01.setBackground(new Color(0, 102, 102));
              rap01.setOpaque(true);
              rap01.setToolTipText("Ecriture rapproch\u00e9e");
              rap01.setPreferredSize(new Dimension(8, 9));
              rap01.setMinimumSize(new Dimension(8, 9));
              rap01.setMaximumSize(new Dimension(8, 9));
              rap01.setName("rap01");
              panel8.add(rap01);
              rap01.setBounds(new Rectangle(new Point(140, 28), rap01.getPreferredSize()));

              //---- rap02 ----
              rap02.setBackground(new Color(0, 102, 102));
              rap02.setOpaque(true);
              rap02.setToolTipText("Ecriture rapproch\u00e9e");
              rap02.setPreferredSize(new Dimension(8, 9));
              rap02.setMinimumSize(new Dimension(8, 9));
              rap02.setMaximumSize(new Dimension(8, 9));
              rap02.setName("rap02");
              panel8.add(rap02);
              rap02.setBounds(new Rectangle(new Point(140, 44), rap02.getPreferredSize()));

              //---- rap03 ----
              rap03.setBackground(new Color(0, 102, 102));
              rap03.setOpaque(true);
              rap03.setToolTipText("Ecriture rapproch\u00e9e");
              rap03.setPreferredSize(new Dimension(8, 9));
              rap03.setMinimumSize(new Dimension(8, 9));
              rap03.setMaximumSize(new Dimension(8, 9));
              rap03.setName("rap03");
              panel8.add(rap03);
              rap03.setBounds(new Rectangle(new Point(140, 60), rap03.getPreferredSize()));

              //---- rap04 ----
              rap04.setBackground(new Color(0, 102, 102));
              rap04.setOpaque(true);
              rap04.setToolTipText("Ecriture rapproch\u00e9e");
              rap04.setPreferredSize(new Dimension(8, 9));
              rap04.setMinimumSize(new Dimension(8, 9));
              rap04.setMaximumSize(new Dimension(8, 9));
              rap04.setName("rap04");
              panel8.add(rap04);
              rap04.setBounds(new Rectangle(new Point(140, 76), rap04.getPreferredSize()));

              //---- rap05 ----
              rap05.setBackground(new Color(0, 102, 102));
              rap05.setOpaque(true);
              rap05.setToolTipText("Ecriture rapproch\u00e9e");
              rap05.setPreferredSize(new Dimension(8, 9));
              rap05.setMinimumSize(new Dimension(8, 9));
              rap05.setMaximumSize(new Dimension(8, 9));
              rap05.setName("rap05");
              panel8.add(rap05);
              rap05.setBounds(new Rectangle(new Point(140, 92), rap05.getPreferredSize()));

              //---- rap06 ----
              rap06.setBackground(new Color(0, 102, 102));
              rap06.setOpaque(true);
              rap06.setToolTipText("Ecriture rapproch\u00e9e");
              rap06.setPreferredSize(new Dimension(8, 9));
              rap06.setMinimumSize(new Dimension(8, 9));
              rap06.setMaximumSize(new Dimension(8, 9));
              rap06.setName("rap06");
              panel8.add(rap06);
              rap06.setBounds(new Rectangle(new Point(140, 108), rap06.getPreferredSize()));

              //---- rap07 ----
              rap07.setBackground(new Color(0, 102, 102));
              rap07.setOpaque(true);
              rap07.setToolTipText("Ecriture rapproch\u00e9e");
              rap07.setPreferredSize(new Dimension(8, 9));
              rap07.setMinimumSize(new Dimension(8, 9));
              rap07.setMaximumSize(new Dimension(8, 9));
              rap07.setName("rap07");
              panel8.add(rap07);
              rap07.setBounds(new Rectangle(new Point(140, 124), rap07.getPreferredSize()));

              //---- rap08 ----
              rap08.setBackground(new Color(0, 102, 102));
              rap08.setOpaque(true);
              rap08.setToolTipText("Ecriture rapproch\u00e9e");
              rap08.setPreferredSize(new Dimension(8, 9));
              rap08.setMinimumSize(new Dimension(8, 9));
              rap08.setMaximumSize(new Dimension(8, 9));
              rap08.setName("rap08");
              panel8.add(rap08);
              rap08.setBounds(new Rectangle(new Point(140, 140), rap08.getPreferredSize()));

              //---- rap09 ----
              rap09.setBackground(new Color(0, 102, 102));
              rap09.setOpaque(true);
              rap09.setToolTipText("Ecriture rapproch\u00e9e");
              rap09.setPreferredSize(new Dimension(8, 9));
              rap09.setMinimumSize(new Dimension(8, 9));
              rap09.setMaximumSize(new Dimension(8, 9));
              rap09.setName("rap09");
              panel8.add(rap09);
              rap09.setBounds(new Rectangle(new Point(140, 156), rap09.getPreferredSize()));

              //---- rap10 ----
              rap10.setBackground(new Color(0, 102, 102));
              rap10.setOpaque(true);
              rap10.setToolTipText("Ecriture rapproch\u00e9e");
              rap10.setPreferredSize(new Dimension(8, 9));
              rap10.setMinimumSize(new Dimension(8, 9));
              rap10.setMaximumSize(new Dimension(8, 9));
              rap10.setName("rap10");
              panel8.add(rap10);
              rap10.setBounds(new Rectangle(new Point(140, 172), rap10.getPreferredSize()));

              //---- rap11 ----
              rap11.setBackground(new Color(0, 102, 102));
              rap11.setOpaque(true);
              rap11.setToolTipText("Ecriture rapproch\u00e9e");
              rap11.setPreferredSize(new Dimension(8, 9));
              rap11.setMinimumSize(new Dimension(8, 9));
              rap11.setMaximumSize(new Dimension(8, 9));
              rap11.setName("rap11");
              panel8.add(rap11);
              rap11.setBounds(new Rectangle(new Point(140, 188), rap11.getPreferredSize()));

              //---- rap12 ----
              rap12.setBackground(new Color(0, 102, 102));
              rap12.setOpaque(true);
              rap12.setToolTipText("Ecriture rapproch\u00e9e");
              rap12.setPreferredSize(new Dimension(8, 9));
              rap12.setMinimumSize(new Dimension(8, 9));
              rap12.setMaximumSize(new Dimension(8, 9));
              rap12.setName("rap12");
              panel8.add(rap12);
              rap12.setBounds(new Rectangle(new Point(140, 204), rap12.getPreferredSize()));

              //---- rap13 ----
              rap13.setBackground(new Color(0, 102, 102));
              rap13.setOpaque(true);
              rap13.setToolTipText("Ecriture rapproch\u00e9e");
              rap13.setPreferredSize(new Dimension(8, 9));
              rap13.setMinimumSize(new Dimension(8, 9));
              rap13.setMaximumSize(new Dimension(8, 9));
              rap13.setName("rap13");
              panel8.add(rap13);
              rap13.setBounds(new Rectangle(new Point(140, 220), rap13.getPreferredSize()));

              //---- rap14 ----
              rap14.setBackground(new Color(0, 102, 102));
              rap14.setOpaque(true);
              rap14.setToolTipText("Ecriture rapproch\u00e9e");
              rap14.setPreferredSize(new Dimension(8, 9));
              rap14.setMinimumSize(new Dimension(8, 9));
              rap14.setMaximumSize(new Dimension(8, 9));
              rap14.setName("rap14");
              panel8.add(rap14);
              rap14.setBounds(new Rectangle(new Point(140, 236), rap14.getPreferredSize()));

              //---- rap15 ----
              rap15.setBackground(new Color(0, 102, 102));
              rap15.setOpaque(true);
              rap15.setToolTipText("Ecriture rapproch\u00e9e");
              rap15.setPreferredSize(new Dimension(8, 9));
              rap15.setMinimumSize(new Dimension(8, 9));
              rap15.setMaximumSize(new Dimension(8, 9));
              rap15.setName("rap15");
              panel8.add(rap15);
              rap15.setBounds(new Rectangle(new Point(140, 252), rap15.getPreferredSize()));

              //---- rap16 ----
              rap16.setBackground(new Color(0, 102, 102));
              rap16.setOpaque(true);
              rap16.setToolTipText("Ecriture rapproch\u00e9e");
              rap16.setPreferredSize(new Dimension(8, 9));
              rap16.setMinimumSize(new Dimension(8, 9));
              rap16.setMaximumSize(new Dimension(8, 9));
              rap16.setName("rap16");
              panel8.add(rap16);
              rap16.setBounds(new Rectangle(new Point(140, 268), rap16.getPreferredSize()));

              //---- rap17 ----
              rap17.setBackground(new Color(0, 102, 102));
              rap17.setOpaque(true);
              rap17.setToolTipText("Ecriture rapproch\u00e9e");
              rap17.setPreferredSize(new Dimension(8, 9));
              rap17.setMinimumSize(new Dimension(8, 9));
              rap17.setMaximumSize(new Dimension(8, 9));
              rap17.setName("rap17");
              panel8.add(rap17);
              rap17.setBounds(new Rectangle(new Point(140, 284), rap17.getPreferredSize()));

              //---- rap18 ----
              rap18.setBackground(new Color(0, 102, 102));
              rap18.setOpaque(true);
              rap18.setToolTipText("Ecriture rapproch\u00e9e");
              rap18.setPreferredSize(new Dimension(8, 9));
              rap18.setMinimumSize(new Dimension(8, 9));
              rap18.setMaximumSize(new Dimension(8, 9));
              rap18.setName("rap18");
              panel8.add(rap18);
              rap18.setBounds(new Rectangle(new Point(140, 300), rap18.getPreferredSize()));

              //---- rap19 ----
              rap19.setBackground(new Color(0, 102, 102));
              rap19.setOpaque(true);
              rap19.setToolTipText("Ecriture rapproch\u00e9e");
              rap19.setPreferredSize(new Dimension(8, 9));
              rap19.setMinimumSize(new Dimension(8, 9));
              rap19.setMaximumSize(new Dimension(8, 9));
              rap19.setName("rap19");
              panel8.add(rap19);
              rap19.setBounds(new Rectangle(new Point(140, 316), rap19.getPreferredSize()));

              //---- rap20 ----
              rap20.setBackground(new Color(0, 102, 102));
              rap20.setOpaque(true);
              rap20.setToolTipText("Ecriture rapproch\u00e9e");
              rap20.setPreferredSize(new Dimension(8, 9));
              rap20.setMinimumSize(new Dimension(8, 9));
              rap20.setMaximumSize(new Dimension(8, 9));
              rap20.setName("rap20");
              panel8.add(rap20);
              rap20.setBounds(new Rectangle(new Point(140, 332), rap20.getPreferredSize()));

              //---- lit01 ----
              lit01.setBackground(new Color(255, 51, 0));
              lit01.setOpaque(true);
              lit01.setToolTipText("Ecriture en litige");
              lit01.setPreferredSize(new Dimension(8, 9));
              lit01.setMinimumSize(new Dimension(8, 9));
              lit01.setMaximumSize(new Dimension(8, 9));
              lit01.setName("lit01");
              panel8.add(lit01);
              lit01.setBounds(new Rectangle(new Point(160, 28), lit01.getPreferredSize()));

              //---- lit02 ----
              lit02.setBackground(new Color(255, 51, 0));
              lit02.setOpaque(true);
              lit02.setToolTipText("Ecriture en litige");
              lit02.setPreferredSize(new Dimension(8, 9));
              lit02.setMinimumSize(new Dimension(8, 9));
              lit02.setMaximumSize(new Dimension(8, 9));
              lit02.setName("lit02");
              panel8.add(lit02);
              lit02.setBounds(new Rectangle(new Point(160, 44), lit02.getPreferredSize()));

              //---- lit03 ----
              lit03.setBackground(new Color(255, 51, 0));
              lit03.setOpaque(true);
              lit03.setToolTipText("Ecriture en litige");
              lit03.setPreferredSize(new Dimension(8, 9));
              lit03.setMinimumSize(new Dimension(8, 9));
              lit03.setMaximumSize(new Dimension(8, 9));
              lit03.setName("lit03");
              panel8.add(lit03);
              lit03.setBounds(new Rectangle(new Point(160, 60), lit03.getPreferredSize()));

              //---- lit04 ----
              lit04.setBackground(new Color(255, 51, 0));
              lit04.setOpaque(true);
              lit04.setToolTipText("Ecriture en litige");
              lit04.setPreferredSize(new Dimension(8, 9));
              lit04.setMinimumSize(new Dimension(8, 9));
              lit04.setMaximumSize(new Dimension(8, 9));
              lit04.setName("lit04");
              panel8.add(lit04);
              lit04.setBounds(new Rectangle(new Point(160, 76), lit04.getPreferredSize()));

              //---- lit05 ----
              lit05.setBackground(new Color(255, 51, 0));
              lit05.setOpaque(true);
              lit05.setToolTipText("Ecriture en litige");
              lit05.setPreferredSize(new Dimension(8, 9));
              lit05.setMinimumSize(new Dimension(8, 9));
              lit05.setMaximumSize(new Dimension(8, 9));
              lit05.setName("lit05");
              panel8.add(lit05);
              lit05.setBounds(new Rectangle(new Point(160, 92), lit05.getPreferredSize()));

              //---- lit06 ----
              lit06.setBackground(new Color(255, 51, 0));
              lit06.setOpaque(true);
              lit06.setToolTipText("Ecriture en litige");
              lit06.setPreferredSize(new Dimension(8, 9));
              lit06.setMinimumSize(new Dimension(8, 9));
              lit06.setMaximumSize(new Dimension(8, 9));
              lit06.setName("lit06");
              panel8.add(lit06);
              lit06.setBounds(new Rectangle(new Point(160, 108), lit06.getPreferredSize()));

              //---- lit07 ----
              lit07.setBackground(new Color(255, 51, 0));
              lit07.setOpaque(true);
              lit07.setToolTipText("Ecriture en litige");
              lit07.setPreferredSize(new Dimension(8, 9));
              lit07.setMinimumSize(new Dimension(8, 9));
              lit07.setMaximumSize(new Dimension(8, 9));
              lit07.setName("lit07");
              panel8.add(lit07);
              lit07.setBounds(new Rectangle(new Point(160, 124), lit07.getPreferredSize()));

              //---- lit08 ----
              lit08.setBackground(new Color(255, 51, 0));
              lit08.setOpaque(true);
              lit08.setToolTipText("Ecriture en litige");
              lit08.setPreferredSize(new Dimension(8, 9));
              lit08.setMinimumSize(new Dimension(8, 9));
              lit08.setMaximumSize(new Dimension(8, 9));
              lit08.setName("lit08");
              panel8.add(lit08);
              lit08.setBounds(new Rectangle(new Point(160, 140), lit08.getPreferredSize()));

              //---- lit09 ----
              lit09.setBackground(new Color(255, 51, 0));
              lit09.setOpaque(true);
              lit09.setToolTipText("Ecriture en litige");
              lit09.setPreferredSize(new Dimension(8, 9));
              lit09.setMinimumSize(new Dimension(8, 9));
              lit09.setMaximumSize(new Dimension(8, 9));
              lit09.setName("lit09");
              panel8.add(lit09);
              lit09.setBounds(new Rectangle(new Point(160, 156), lit09.getPreferredSize()));

              //---- lit10 ----
              lit10.setBackground(new Color(255, 51, 0));
              lit10.setOpaque(true);
              lit10.setToolTipText("Ecriture en litige");
              lit10.setPreferredSize(new Dimension(8, 9));
              lit10.setMinimumSize(new Dimension(8, 9));
              lit10.setMaximumSize(new Dimension(8, 9));
              lit10.setName("lit10");
              panel8.add(lit10);
              lit10.setBounds(new Rectangle(new Point(160, 172), lit10.getPreferredSize()));

              //---- lit11 ----
              lit11.setBackground(new Color(255, 51, 0));
              lit11.setOpaque(true);
              lit11.setToolTipText("Ecriture en litige");
              lit11.setPreferredSize(new Dimension(8, 9));
              lit11.setMinimumSize(new Dimension(8, 9));
              lit11.setMaximumSize(new Dimension(8, 9));
              lit11.setName("lit11");
              panel8.add(lit11);
              lit11.setBounds(new Rectangle(new Point(160, 188), lit11.getPreferredSize()));

              //---- lit12 ----
              lit12.setBackground(new Color(255, 51, 0));
              lit12.setOpaque(true);
              lit12.setToolTipText("Ecriture en litige");
              lit12.setPreferredSize(new Dimension(8, 9));
              lit12.setMinimumSize(new Dimension(8, 9));
              lit12.setMaximumSize(new Dimension(8, 9));
              lit12.setName("lit12");
              panel8.add(lit12);
              lit12.setBounds(new Rectangle(new Point(160, 204), lit12.getPreferredSize()));

              //---- lit13 ----
              lit13.setBackground(new Color(255, 51, 0));
              lit13.setOpaque(true);
              lit13.setToolTipText("Ecriture en litige");
              lit13.setPreferredSize(new Dimension(8, 9));
              lit13.setMinimumSize(new Dimension(8, 9));
              lit13.setMaximumSize(new Dimension(8, 9));
              lit13.setName("lit13");
              panel8.add(lit13);
              lit13.setBounds(new Rectangle(new Point(160, 220), lit13.getPreferredSize()));

              //---- lit14 ----
              lit14.setBackground(new Color(255, 51, 0));
              lit14.setOpaque(true);
              lit14.setToolTipText("Ecriture en litige");
              lit14.setPreferredSize(new Dimension(8, 9));
              lit14.setMinimumSize(new Dimension(8, 9));
              lit14.setMaximumSize(new Dimension(8, 9));
              lit14.setName("lit14");
              panel8.add(lit14);
              lit14.setBounds(new Rectangle(new Point(160, 236), lit14.getPreferredSize()));

              //---- lit15 ----
              lit15.setBackground(new Color(255, 51, 0));
              lit15.setOpaque(true);
              lit15.setToolTipText("Ecriture en litige");
              lit15.setPreferredSize(new Dimension(8, 9));
              lit15.setMinimumSize(new Dimension(8, 9));
              lit15.setMaximumSize(new Dimension(8, 9));
              lit15.setName("lit15");
              panel8.add(lit15);
              lit15.setBounds(new Rectangle(new Point(160, 252), lit15.getPreferredSize()));

              //---- lit16 ----
              lit16.setBackground(new Color(255, 51, 0));
              lit16.setOpaque(true);
              lit16.setToolTipText("Ecriture en litige");
              lit16.setPreferredSize(new Dimension(8, 9));
              lit16.setMinimumSize(new Dimension(8, 9));
              lit16.setMaximumSize(new Dimension(8, 9));
              lit16.setName("lit16");
              panel8.add(lit16);
              lit16.setBounds(new Rectangle(new Point(160, 268), lit16.getPreferredSize()));

              //---- lit17 ----
              lit17.setBackground(new Color(255, 51, 0));
              lit17.setOpaque(true);
              lit17.setToolTipText("Ecriture en litige");
              lit17.setPreferredSize(new Dimension(8, 9));
              lit17.setMinimumSize(new Dimension(8, 9));
              lit17.setMaximumSize(new Dimension(8, 9));
              lit17.setName("lit17");
              panel8.add(lit17);
              lit17.setBounds(new Rectangle(new Point(160, 284), lit17.getPreferredSize()));

              //---- lit18 ----
              lit18.setBackground(new Color(255, 51, 0));
              lit18.setOpaque(true);
              lit18.setToolTipText("Ecriture en litige");
              lit18.setPreferredSize(new Dimension(8, 9));
              lit18.setMinimumSize(new Dimension(8, 9));
              lit18.setMaximumSize(new Dimension(8, 9));
              lit18.setName("lit18");
              panel8.add(lit18);
              lit18.setBounds(new Rectangle(new Point(160, 300), lit18.getPreferredSize()));

              //---- lit19 ----
              lit19.setBackground(new Color(255, 51, 0));
              lit19.setOpaque(true);
              lit19.setToolTipText("Ecriture en litige");
              lit19.setPreferredSize(new Dimension(8, 9));
              lit19.setMinimumSize(new Dimension(8, 9));
              lit19.setMaximumSize(new Dimension(8, 9));
              lit19.setName("lit19");
              panel8.add(lit19);
              lit19.setBounds(new Rectangle(new Point(160, 316), lit19.getPreferredSize()));

              //---- lit20 ----
              lit20.setBackground(new Color(255, 51, 0));
              lit20.setOpaque(true);
              lit20.setToolTipText("Ecriture en litige");
              lit20.setPreferredSize(new Dimension(8, 9));
              lit20.setMinimumSize(new Dimension(8, 9));
              lit20.setMaximumSize(new Dimension(8, 9));
              lit20.setName("lit20");
              panel8.add(lit20);
              lit20.setBounds(new Rectangle(new Point(160, 332), lit20.getPreferredSize()));

              //======== SCROLLPANE_LIST ========
              {
                SCROLLPANE_LIST.setMinimumSize(new Dimension(1000, 355));
                SCROLLPANE_LIST.setPreferredSize(new Dimension(1000, 355));
                SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

                //---- LE01 ----
                LE01.setComponentPopupMenu(BTDL1);
                LE01.setName("LE01");
                LE01.addMouseListener(new MouseAdapter() {
                  @Override
                  public void mouseClicked(MouseEvent e) {
                    TE01MouseClicked(e);
                  }
                });
                SCROLLPANE_LIST.setViewportView(LE01);
              }
              panel8.add(SCROLLPANE_LIST);
              SCROLLPANE_LIST.setBounds(0, 0, 1000, 355);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel8.getComponentCount(); i++) {
                  Rectangle bounds = panel8.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel8.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel8.setMinimumSize(preferredSize);
                panel8.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel8);
            panel8.setBounds(20, 0, 1000, 355);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(xTitledPanel1, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(5, 5, 5, 5), 0, 0));
        }
        p_centrage.add(p_contenu, BorderLayout.CENTER);
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD3 ========
    {
      BTD3.setName("BTD3");

      //---- OBJ_31 ----
      OBJ_31.setText("Forcer");
      OBJ_31.setName("OBJ_31");
      OBJ_31.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_31ActionPerformed(e);
        }
      });
      BTD3.add(OBJ_31);
      BTD3.addSeparator();

      //---- OBJ_33 ----
      OBJ_33.setText("Choix possibles");
      OBJ_33.setName("OBJ_33");
      OBJ_33.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_33ActionPerformed(e);
        }
      });
      BTD3.add(OBJ_33);

      //---- OBJ_32 ----
      OBJ_32.setText("Aide en ligne");
      OBJ_32.setName("OBJ_32");
      OBJ_32.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_32ActionPerformed(e);
        }
      });
      BTD3.add(OBJ_32);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_39 ----
      OBJ_39.setText("Choix possibles");
      OBJ_39.setName("OBJ_39");
      OBJ_39.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_39ActionPerformed(e);
        }
      });
      BTD.add(OBJ_39);

      //---- OBJ_38 ----
      OBJ_38.setText("Aide en ligne");
      OBJ_38.setName("OBJ_38");
      OBJ_38.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_38ActionPerformed(e);
        }
      });
      BTD.add(OBJ_38);
    }

    //======== BTDL1 ========
    {
      BTDL1.setName("BTDL1");

      //---- OBJ_41 ----
      OBJ_41.setText("Lettrer");
      OBJ_41.setName("OBJ_41");
      OBJ_41.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_41ActionPerformed(e);
        }
      });
      BTDL1.add(OBJ_41);

      //---- OBJ_42 ----
      OBJ_42.setText("D\u00e9lettrer");
      OBJ_42.setName("OBJ_42");
      OBJ_42.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_42ActionPerformed(e);
        }
      });
      BTDL1.add(OBJ_42);

      //---- OBJ_43 ----
      OBJ_43.setText("Choisir");
      OBJ_43.setName("OBJ_43");
      OBJ_43.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_43ActionPerformed(e);
        }
      });
      BTDL1.add(OBJ_43);

      //---- OBJ_44 ----
      OBJ_44.setText("Modifier");
      OBJ_44.setName("OBJ_44");
      OBJ_44.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_44ActionPerformed(e);
        }
      });
      BTDL1.add(OBJ_44);

      //---- OBJ_45 ----
      OBJ_45.setText("Vue r\u00e8glement");
      OBJ_45.setName("OBJ_45");
      OBJ_45.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_45ActionPerformed(e);
        }
      });
      BTDL1.add(OBJ_45);

      //---- OBJ_46 ----
      OBJ_46.setText("Affectation r\u00e8glement");
      OBJ_46.setName("OBJ_46");
      OBJ_46.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_46ActionPerformed(e);
        }
      });
      BTDL1.add(OBJ_46);

      //---- OBJ_47 ----
      OBJ_47.setText("Bloc note");
      OBJ_47.setName("OBJ_47");
      OBJ_47.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_47ActionPerformed(e);
        }
      });
      BTDL1.add(OBJ_47);

      //---- OBJ_48 ----
      OBJ_48.setText("Vue facture");
      OBJ_48.setName("OBJ_48");
      OBJ_48.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_48ActionPerformed(e);
        }
      });
      BTDL1.add(OBJ_48);

      //---- OBJ_FOL ----
      OBJ_FOL.setText("Folios (s\u00e9quentiel)");
      OBJ_FOL.setName("OBJ_FOL");
      OBJ_FOL.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_FOLActionPerformed(e);
        }
      });
      BTDL1.add(OBJ_FOL);

      //---- OBJ_FOL2 ----
      OBJ_FOL2.setText("Folios (par n\u00b0 de pi\u00e8ce)");
      OBJ_FOL2.setName("OBJ_FOL2");
      OBJ_FOL2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_FOL2ActionPerformed(e);
        }
      });
      BTDL1.add(OBJ_FOL2);

      //---- mi_NumPiece ----
      mi_NumPiece.setText("Num\u00e9ro de pi\u00e8ce");
      mi_NumPiece.setName("mi_NumPiece");
      mi_NumPiece.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_NumPieceActionPerformed(e);
        }
      });
      BTDL1.add(mi_NumPiece);

      //---- OBJ_TRS ----
      OBJ_TRS.setText("G\u00e9n\u00e9ration d'une OD");
      OBJ_TRS.setName("OBJ_TRS");
      OBJ_TRS.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_TRSActionPerformed(e);
        }
      });
      BTDL1.add(OBJ_TRS);

      //---- OBJ_49 ----
      OBJ_49.setText("Exportation folio dans tableur");
      OBJ_49.setName("OBJ_49");
      OBJ_49.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_49ActionPerformed(e);
        }
      });
      BTDL1.add(OBJ_49);

      //---- OBJ_50 ----
      OBJ_50.setText("Calculette de solde partiel");
      OBJ_50.setName("OBJ_50");
      OBJ_50.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_50ActionPerformed(e);
        }
      });
      BTDL1.add(OBJ_50);

      //---- AfficheLitige ----
      AfficheLitige.setText("Affichage des litiges");
      AfficheLitige.setName("AfficheLitige");
      AfficheLitige.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          AfficheLitigeActionPerformed(e);
        }
      });
      BTDL1.add(AfficheLitige);

      //---- OBJ_51 ----
      OBJ_51.setText("G\u00e9n\u00e9ration d'un litige");
      OBJ_51.setName("OBJ_51");
      OBJ_51.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_51ActionPerformed(e);
        }
      });
      BTDL1.add(OBJ_51);

      //---- OBJ_52 ----
      OBJ_52.setText("Vue effet");
      OBJ_52.setName("OBJ_52");
      OBJ_52.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_52ActionPerformed(e);
        }
      });
      BTDL1.add(OBJ_52);

      //---- OBJ_54 ----
      OBJ_54.setText("Acc\u00e9s prorogation / non acc\u00e8s");
      OBJ_54.setName("OBJ_54");
      OBJ_54.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_54ActionPerformed(e);
        }
      });
      BTDL1.add(OBJ_54);

      //---- OBJ_Echeancier ----
      OBJ_Echeancier.setText("Ech\u00e9ancier");
      OBJ_Echeancier.setName("OBJ_Echeancier");
      OBJ_Echeancier.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_EcheancierActionPerformed(e);
        }
      });
      BTDL1.add(OBJ_Echeancier);

      //---- MILienUrl ----
      MILienUrl.setText("Lien URL");
      MILienUrl.setName("MILienUrl");
      MILienUrl.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          MILienUrlActionPerformed(e);
        }
      });
      BTDL1.add(MILienUrl);
      BTDL1.addSeparator();

      //---- OBJ_56 ----
      OBJ_56.setText("Choix possibles");
      OBJ_56.setName("OBJ_56");
      OBJ_56.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_56ActionPerformed(e);
        }
      });
      BTDL1.add(OBJ_56);

      //---- OBJ_55 ----
      OBJ_55.setText("Aide en ligne");
      OBJ_55.setName("OBJ_55");
      OBJ_55.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_55ActionPerformed(e);
        }
      });
      BTDL1.add(OBJ_55);
    }

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- OBJ_93 ----
      OBJ_93.setText("Choix possibles");
      OBJ_93.setName("OBJ_93");
      OBJ_93.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_93ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_93);

      //---- OBJ_92 ----
      OBJ_92.setText("Aide en ligne");
      OBJ_92.setName("OBJ_92");
      OBJ_92.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_92ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_92);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private XRiTextField INDETB;
  private XRiTextField INDNCG;
  private XRiTextField INDNCA;
  private RiZoneSortie OBJ_120;
  private SNBoutonDetail OBJ_119;
  private JButton OBJ_285;
  private JButton OBJ_118;
  private JLabel label1;
  private JLabel label2;
  private RiZoneSortie WCLA1;
  private RiZoneSortie WCLA2;
  private JPanel p_tete_droite;
  private JLabel OBJ_112;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_del;
  private RiSousMenu_bt riSousMenu_bt_del;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu22;
  private RiSousMenu_bt riSousMenu_bt22;
  private RiSousMenu riSousMenu30;
  private RiSousMenu_bt riSousMenu_bt30;
  private RiSousMenu riSousMenu31;
  private RiSousMenu_bt riSousMenu_bt31;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private RiSousMenu riSousMenu16;
  private RiSousMenu_bt riSousMenu_bt16;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiMenu riMenu4;
  private RiMenu_bt riMenu_bt4;
  private RiSousMenu riSousMenu18;
  private RiSousMenu_bt riSousMenu_bt18;
  private RiSousMenu riSousMenu19;
  private RiSousMenu_bt riSousMenu_bt19;
  private RiMenu riMenu5;
  private RiMenu_bt riMenu_bt5;
  private RiSousMenu riSousMenu23;
  private RiSousMenu_bt riSousMenu_bt23;
  private RiSousMenu riSousMenu24;
  private RiSousMenu_bt riSousMenu_bt24;
  private RiSousMenu riSousMenu25;
  private RiSousMenu_bt riSousMenu_bt25;
  private RiSousMenu riSousMenu26;
  private RiSousMenu_bt riSousMenu_bt26;
  private RiSousMenu riSousMenu27;
  private RiSousMenu_bt riSousMenu_bt27;
  private RiSousMenu riSousMenu28;
  private RiSousMenu_bt riSousMenu_bt28;
  private RiSousMenu riSousMenu29;
  private RiSousMenu_bt riSousMenu_bt29;
  private RiSousMenu riSousMenu32;
  private RiSousMenu_bt riSousMenu_bt32;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel2;
  private JLabel OBJ_127;
  private XRiTextField WLDEV;
  private JLabel OBJ_130;
  private XRiTextField WPTG;
  private SNBoutonDetail OBJ_125;
  private XRiTextField WTEL;
  private JLabel OBJ_132;
  private XRiTextField CPSOCD;
  private JLabel CPSENS;
  private XRiTextField WTEL1;
  private JXTitledPanel xTitledPanel1;
  private JPanel panel4;
  private SNBoutonDetail OBJ_288;
  private SNBoutonDetail OBJ_289;
  private SNBoutonDetail OBJ_290;
  private SNBoutonDetail OBJ_291;
  private SNBoutonDetail OBJ_292;
  private SNBoutonDetail OBJ_293;
  private SNBoutonDetail OBJ_294;
  private SNBoutonDetail OBJ_295;
  private SNBoutonDetail OBJ_296;
  private SNBoutonDetail OBJ_297;
  private SNBoutonDetail OBJ_298;
  private SNBoutonDetail OBJ_299;
  private SNBoutonDetail OBJ_300;
  private SNBoutonDetail OBJ_301;
  private SNBoutonDetail OBJ_302;
  private SNBoutonDetail OBJ_303;
  private SNBoutonDetail OBJ_304;
  private SNBoutonDetail OBJ_305;
  private SNBoutonDetail OBJ_306;
  private SNBoutonDetail OBJ_307;
  private JButton OBJ_91;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JButton OBJ_95;
  private JLabel OBJ_231;
  private XRiTextField WTPTDB;
  private XRiTextField WTDVDB;
  private XRiTextField WTPTCR;
  private XRiTextField WTDVCR;
  private XRiTextField WTPTD2;
  private XRiTextField WTPTDX;
  private XRiTextField WTPTD1;
  private XRiTextField WTPTDV;
  private JLabel OBJ_219;
  private JLabel OBJ_217;
  private JLabel OBJ_221;
  private JLabel OBJ_220;
  private JPanel panel5;
  private JSpinner WMOIX_SP;
  private JButton BT_LET;
  private JButton OBJ_241;
  private JButton OBJ_242;
  private JButton BT_DEL;
  private JPanel panel1;
  private XRiTextField WMOIX;
  private JLabel label3;
  private JLabel label4;
  private JPanel panel2;
  private XRiTextField WTS1;
  private XRiTextField WTS2;
  private XRiTextField WTS3;
  private JLabel OBJ_206;
  private JLabel OBJ_213;
  private XRiTextField WLETDP;
  private JPanel panel3;
  private XRiTextField PDPER;
  private XRiTextField WTJDB;
  private XRiTextField WTJCR;
  private XRiTextField PDSOL1;
  private XRiTextField WSOCD;
  private JLabel OBJ_228;
  private JLabel OBJ_232;
  private XRiTextField PDDB2;
  private XRiTextField PDCR2;
  private XRiTextField PDSOL2;
  private JLabel OBJ_236;
  private JLabel OBJ_229;
  private JPanel panel6;
  private JLabel OBJ_214;
  private JLabel OBJ_216;
  private JLabel OBJ_218;
  private JPanel panel7;
  private JLabel OBJ_212;
  private JPanel panel8;
  private JLabel rap01;
  private JLabel rap02;
  private JLabel rap03;
  private JLabel rap04;
  private JLabel rap05;
  private JLabel rap06;
  private JLabel rap07;
  private JLabel rap08;
  private JLabel rap09;
  private JLabel rap10;
  private JLabel rap11;
  private JLabel rap12;
  private JLabel rap13;
  private JLabel rap14;
  private JLabel rap15;
  private JLabel rap16;
  private JLabel rap17;
  private JLabel rap18;
  private JLabel rap19;
  private JLabel rap20;
  private JLabel lit01;
  private JLabel lit02;
  private JLabel lit03;
  private JLabel lit04;
  private JLabel lit05;
  private JLabel lit06;
  private JLabel lit07;
  private JLabel lit08;
  private JLabel lit09;
  private JLabel lit10;
  private JLabel lit11;
  private JLabel lit12;
  private JLabel lit13;
  private JLabel lit14;
  private JLabel lit15;
  private JLabel lit16;
  private JLabel lit17;
  private JLabel lit18;
  private JLabel lit19;
  private JLabel lit20;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable LE01;
  private JPopupMenu BTD3;
  private JMenuItem OBJ_31;
  private JMenuItem OBJ_33;
  private JMenuItem OBJ_32;
  private JPopupMenu BTD;
  private JMenuItem OBJ_39;
  private JMenuItem OBJ_38;
  private JPopupMenu BTDL1;
  private JMenuItem OBJ_41;
  private JMenuItem OBJ_42;
  private JMenuItem OBJ_43;
  private JMenuItem OBJ_44;
  private JMenuItem OBJ_45;
  private JMenuItem OBJ_46;
  private JMenuItem OBJ_47;
  private JMenuItem OBJ_48;
  private JMenuItem OBJ_FOL;
  private JMenuItem OBJ_FOL2;
  private JMenuItem mi_NumPiece;
  private JMenuItem OBJ_TRS;
  private JMenuItem OBJ_49;
  private JMenuItem OBJ_50;
  private JMenuItem AfficheLitige;
  private JMenuItem OBJ_51;
  private JMenuItem OBJ_52;
  private JMenuItem OBJ_54;
  private JMenuItem OBJ_Echeancier;
  private JMenuItem MILienUrl;
  private JMenuItem OBJ_56;
  private JMenuItem OBJ_55;
  private JPopupMenu BTD2;
  private JMenuItem OBJ_93;
  private JMenuItem OBJ_92;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
