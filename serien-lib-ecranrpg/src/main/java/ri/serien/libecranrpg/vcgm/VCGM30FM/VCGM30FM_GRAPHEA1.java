/*
 * Created by JFormDesigner on Wed Feb 24 16:24:25 CET 2010
 */

package ri.serien.libecranrpg.vcgm.VCGM30FM;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.WindowConstants;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.DefaultKeyedValues;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.general.DatasetUtilities;

import ri.serien.libswing.composantrpg.lexical.RiGraphe;
import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * @author Stéphane Vénéri
 * 
 * 
 *         GRAPHE DE COMPARAISON DE L'EVOLUTION DE 2 ANNEES
 * 
 */
public class VCGM30FM_GRAPHEA1 extends JFrame {
  
  
  private String annee = null;
  private String annee1 = null;
  private String[] donnee = null;
  private String[] donnee1 = null;
  private RiGraphe graphe = null;
  private String[] moisLib =
      { "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre" };
  
  /**
   * Constructeur
   * @param annee
   * @param donnee
   */
  public VCGM30FM_GRAPHEA1(String annee, String annee1, String[] donnee, String[] donnee1) {
    super();
    initComponents();
    
    this.annee = annee;
    this.annee1 = annee1;
    this.donnee = donnee;
    this.donnee1 = donnee1;
    
    initData();
    setSize(1000, 700);
    setBackground(SNCharteGraphique.COULEUR_FOND);
    setVisible(true);
  }
  
  /**
   * Initialise les données pour les graphes
   *
   */
  private void initData() {
    // Préparation des données
    
    DefaultKeyedValues data = new DefaultKeyedValues();
    DefaultKeyedValues data2 = new DefaultKeyedValues();
    for (int mois = 0; mois < 12; mois++) {
      data.addValue(moisLib[mois], Double.parseDouble(donnee[mois]));
      data2.addValue(moisLib[mois], Double.parseDouble(donnee1[mois]));
    }
    
    CategoryDataset dataset = DatasetUtilities.createCategoryDataset(annee, data);
    CategoryDataset dataset2 = DatasetUtilities.createCategoryDataset(annee1, data2);
    
    CategoryDataset[] d = { dataset, dataset2 };
    
    JFreeChart graphe = createChart(d);
    
    l_graphe.setIcon(new ImageIcon(graphe.createBufferedImage(l_graphe.getWidth(), l_graphe.getHeight())));
    
  }
  
  public static JFreeChart createChart(CategoryDataset[] datasets) {
    // create the chart...
    
    JFreeChart chart = ChartFactory.createBarChart("Comparaison entre l'année actuelle et l'année précédente", // chart
                                                                                                               // title
        "mois", // domain axis label
        " ", // range axis label
        datasets[0], // data
        PlotOrientation.VERTICAL, true, // include legend
        true, false);
    
    CategoryPlot plot = (CategoryPlot) chart.getPlot();
    CategoryAxis domainAxis = plot.getDomainAxis();
    
    domainAxis.setLowerMargin(0.02);
    domainAxis.setUpperMargin(0.02);
    domainAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_90);
    
    LineAndShapeRenderer renderer1 = new LineAndShapeRenderer();
    LineAndShapeRenderer renderer2 = new LineAndShapeRenderer();
    
    NumberAxis axis1 = new NumberAxis("");
    // axis2.setNumberFormatOverride(NumberFormat.getPercentInstance());
    plot.setRangeAxis(0, axis1);
    plot.setDataset(0, datasets[0]);
    plot.setRenderer(0, renderer1);
    plot.mapDatasetToRangeAxis(0, 0);
    plot.getRangeAxis(0).setVisible(false);
    axis1.setLabelPaint(Color.magenta);
    axis1.setTickLabelPaint(Color.magenta);
    
    NumberAxis axis2 = new NumberAxis("");
    // axis2.setNumberFormatOverride(NumberFormat.getPercentInstance());
    plot.setRangeAxis(1, axis2);
    plot.setDataset(1, datasets[1]);
    plot.setRenderer(1, renderer2);
    plot.mapDatasetToRangeAxis(1, 1);
    plot.getRangeAxis(1).setVisible(false);
    axis2.setLabelPaint(Color.red);
    axis2.setTickLabelPaint(Color.red);
    
    plot.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);
    
    // ChartUtilities.applyCurrentTheme(chart);
    
    return chart;
  }
  
  private void MI_CopierActionPerformed(ActionEvent e) {
    graphe.sendToClipBoard(l_graphe.getWidth(), l_graphe.getHeight());
  }
  
  private void MI_EnregistrerActionPerformed(ActionEvent e) {
    graphe.saveGraphe(null, l_graphe.getWidth(), l_graphe.getHeight());
  }
  
  private void thisComponentResized(ComponentEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    l_graphe = new JLabel();
    BTD = new JPopupMenu();
    MI_Copier = new JMenuItem();
    MI_Enregistrer = new JMenuItem();

    //======== this ========
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setTitle("Analyse des comptes");
    setIconImage(null);
    setBackground(new Color(238, 238, 210));
    setName("this");
    addComponentListener(new ComponentAdapter() {
      @Override
      public void componentResized(ComponentEvent e) {
        thisComponentResized(e);
      }
    });
    Container contentPane = getContentPane();

    //---- l_graphe ----
    l_graphe.setComponentPopupMenu(BTD);
    l_graphe.setName("l_graphe");

    GroupLayout contentPaneLayout = new GroupLayout(contentPane);
    contentPane.setLayout(contentPaneLayout);
    contentPaneLayout.setHorizontalGroup(
      contentPaneLayout.createParallelGroup()
        .addComponent(l_graphe, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 932, Short.MAX_VALUE)
    );
    contentPaneLayout.setVerticalGroup(
      contentPaneLayout.createParallelGroup()
        .addComponent(l_graphe, GroupLayout.DEFAULT_SIZE, 628, Short.MAX_VALUE)
    );
    pack();

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- MI_Copier ----
      MI_Copier.setText("Copier");
      MI_Copier.setName("MI_Copier");
      MI_Copier.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          MI_CopierActionPerformed(e);
        }
      });
      BTD.add(MI_Copier);

      //---- MI_Enregistrer ----
      MI_Enregistrer.setText("Enregistrer sous");
      MI_Enregistrer.setName("MI_Enregistrer");
      MI_Enregistrer.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          MI_EnregistrerActionPerformed(e);
        }
      });
      BTD.add(MI_Enregistrer);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel l_graphe;
  private JPopupMenu BTD;
  private JMenuItem MI_Copier;
  private JMenuItem MI_Enregistrer;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
