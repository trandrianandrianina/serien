
package ri.serien.libecranrpg.vcgm.VCGM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM03FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VCGM03FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDTYP@")).trim());
    INDIND.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDIND@")).trim());
    OBJ_85.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPAGE@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_56ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_29 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_78 = new JLabel();
    INDTYP = new RiZoneSortie();
    OBJ_79 = new JLabel();
    INDIND = new RiZoneSortie();
    p_tete_droite = new JPanel();
    panel1 = new JPanel();
    OBJ_84 = new JLabel();
    OBJ_85 = new RiZoneSortie();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    P_Centre = new JPanel();
    OBJ_24 = new JXTitledSeparator();
    OBJ_75 = new JLabel();
    OBJ_121 = new JLabel();
    OBJ_123 = new JLabel();
    OBJ_117 = new JLabel();
    OBJ_124 = new JLabel();
    OBJ_77 = new JLabel();
    OBJ_74 = new JLabel();
    OBJ_126 = new JLabel();
    OBJ_71 = new JLabel();
    OBJ_76 = new JLabel();
    OBJ_118 = new JLabel();
    OBJ_91 = new JLabel();
    OBJ_119 = new JLabel();
    OBJ_122 = new JLabel();
    OBJ_125 = new JLabel();
    OBJ_73 = new JLabel();
    OBJ_116 = new JLabel();
    OBJ_72 = new JLabel();
    OBJ_87 = new JLabel();
    OBJ_56 = new SNBouton();
    OBJ_89 = new JLabel();
    OBJ_120 = new JLabel();
    OBJ_156 = new JLabel();
    OBJ_94 = new JLabel();
    OBJ_154 = new JLabel();
    OBJ_155 = new JLabel();
    OBJ_153 = new JLabel();
    PST1 = new XRiTextField();
    PST2 = new XRiTextField();
    PST3 = new XRiTextField();
    PST4 = new XRiTextField();
    PST5 = new XRiTextField();
    PST6 = new XRiTextField();
    PST7 = new XRiTextField();
    PST8 = new XRiTextField();
    PST9 = new XRiTextField();
    PST10 = new XRiTextField();
    PST11 = new XRiTextField();
    PST12 = new XRiTextField();
    PST13 = new XRiTextField();
    PST14 = new XRiTextField();
    PST15 = new XRiTextField();
    PST16 = new XRiTextField();
    PST17 = new XRiTextField();
    PST18 = new XRiTextField();
    PST19 = new XRiTextField();
    PST20 = new XRiTextField();
    PST21 = new XRiTextField();
    PST22 = new XRiTextField();
    V06F = new XRiTextField();
    V06F1 = new XRiTextField();
    OBJ_86 = new JLabel();
    OBJ_88 = new JLabel();
    OBJ_90 = new JLabel();
    OBJ_92 = new JLabel();
    OBJ_93 = new JLabel();
    OBJ_95 = new JLabel();
    OBJ_97 = new JLabel();
    OBJ_98 = new JLabel();
    OBJ_99 = new JLabel();
    OBJ_100 = new JLabel();
    OBJ_101 = new JLabel();
    OBJ_105 = new JLabel();
    OBJ_106 = new JLabel();
    OBJ_107 = new JLabel();
    OBJ_108 = new JLabel();
    OBJ_109 = new JLabel();
    OBJ_110 = new JLabel();
    OBJ_111 = new JLabel();
    OBJ_112 = new JLabel();
    OBJ_113 = new JLabel();
    OBJ_114 = new JLabel();
    OBJ_115 = new JLabel();
    OBJ_149 = new JLabel();
    OBJ_150 = new JLabel();
    OBJ_151 = new JLabel();
    OBJ_152 = new JLabel();
    OBJ_127 = new JLabel();
    OBJ_128 = new JLabel();
    OBJ_129 = new JLabel();
    OBJ_130 = new JLabel();
    OBJ_131 = new JLabel();
    OBJ_132 = new JLabel();
    OBJ_133 = new JLabel();
    OBJ_134 = new JLabel();
    OBJ_135 = new JLabel();
    OBJ_136 = new JLabel();
    OBJ_137 = new JLabel();
    OBJ_138 = new JLabel();
    OBJ_139 = new JLabel();
    OBJ_140 = new JLabel();
    OBJ_141 = new JLabel();
    OBJ_142 = new JLabel();
    OBJ_143 = new JLabel();
    OBJ_144 = new JLabel();
    OBJ_145 = new JLabel();
    OBJ_146 = new JLabel();
    OBJ_147 = new JLabel();
    OBJ_148 = new JLabel();
    separator1 = compFactory.createSeparator("Divers");
    BTD = new JPopupMenu();
    OBJ_14 = new JMenuItem();
    OBJ_13 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1200, 620));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_29 ----
          OBJ_29.setText("Soci\u00e9t\u00e9");
          OBJ_29.setName("OBJ_29");
          p_tete_gauche.add(OBJ_29);
          OBJ_29.setBounds(5, 3, 52, 18);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setText("@INDETB@");
          INDETB.setOpaque(false);
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(65, 0, 40, INDETB.getPreferredSize().height);

          //---- OBJ_78 ----
          OBJ_78.setText("Code");
          OBJ_78.setName("OBJ_78");
          p_tete_gauche.add(OBJ_78);
          OBJ_78.setBounds(130, 3, 45, 18);

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setText("@INDTYP@");
          INDTYP.setOpaque(false);
          INDTYP.setName("INDTYP");
          p_tete_gauche.add(INDTYP);
          INDTYP.setBounds(180, 0, 34, INDTYP.getPreferredSize().height);

          //---- OBJ_79 ----
          OBJ_79.setText("Ordre");
          OBJ_79.setName("OBJ_79");
          p_tete_gauche.add(OBJ_79);
          OBJ_79.setBounds(245, 3, 45, 18);

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setText("@INDIND@");
          INDIND.setOpaque(false);
          INDIND.setName("INDIND");
          p_tete_gauche.add(INDIND);
          INDIND.setBounds(300, 0, 60, INDIND.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //======== panel1 ========
          {
            panel1.setMinimumSize(new Dimension(124, 24));
            panel1.setPreferredSize(new Dimension(124, 24));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_84 ----
            OBJ_84.setText("Page");
            OBJ_84.setName("OBJ_84");
            panel1.add(OBJ_84);
            OBJ_84.setBounds(45, 2, 45, 20);

            //---- OBJ_85 ----
            OBJ_85.setText("@WPAGE@");
            OBJ_85.setOpaque(false);
            OBJ_85.setComponentPopupMenu(null);
            OBJ_85.setName("OBJ_85");
            panel1.add(OBJ_85);
            OBJ_85.setBounds(90, 0, 34, OBJ_85.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_tete_droite.add(panel1);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 520));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== P_Centre ========
          {
            P_Centre.setOpaque(false);
            P_Centre.setName("P_Centre");
            P_Centre.setLayout(null);

            //---- OBJ_24 ----
            OBJ_24.setTitle("Legende");
            OBJ_24.setName("OBJ_24");
            P_Centre.add(OBJ_24);
            OBJ_24.setBounds(25, 405, 830, OBJ_24.getPreferredSize().height);

            //---- OBJ_75 ----
            OBJ_75.setText("Edition bordereaux remise en banque tri\u00e9s / montant");
            OBJ_75.setComponentPopupMenu(BTD);
            OBJ_75.setName("OBJ_75");
            P_Centre.add(OBJ_75);
            OBJ_75.setBounds(65, 300, 310, 20);

            //---- OBJ_121 ----
            OBJ_121.setText("Gestion crit\u00e8res s\u00e9lection sur plan comptable auxi.");
            OBJ_121.setComponentPopupMenu(BTD);
            OBJ_121.setName("OBJ_121");
            P_Centre.add(OBJ_121);
            OBJ_121.setBounds(490, 205, 300, 20);

            //---- OBJ_123 ----
            OBJ_123.setText("Tri des \u00e9ch\u00e9ances pour r\u00e9f\u00e9rence de classement");
            OBJ_123.setComponentPopupMenu(BTD);
            OBJ_123.setName("OBJ_123");
            P_Centre.add(OBJ_123);
            OBJ_123.setBounds(490, 265, 300, 20);

            //---- OBJ_117 ----
            OBJ_117.setText("Affichage justification compte de r\u00e9f. classement");
            OBJ_117.setComponentPopupMenu(BTD);
            OBJ_117.setName("OBJ_117");
            P_Centre.add(OBJ_117);
            OBJ_117.setBounds(490, 80, 300, 20);

            //---- OBJ_124 ----
            OBJ_124.setText("Annulation d'un effet (r\u00e8glement multi-clients)");
            OBJ_124.setComponentPopupMenu(BTD);
            OBJ_124.setName("OBJ_124");
            P_Centre.add(OBJ_124);
            OBJ_124.setBounds(490, 295, 300, 20);

            //---- OBJ_77 ----
            OBJ_77.setText("Mise \u00e0 jour date de valeur en rapprochement");
            OBJ_77.setComponentPopupMenu(BTD);
            OBJ_77.setName("OBJ_77");
            P_Centre.add(OBJ_77);
            OBJ_77.setBounds(65, 362, 310, 20);

            //---- OBJ_74 ----
            OBJ_74.setText("Affichage justification compte sur affectation");
            OBJ_74.setComponentPopupMenu(BTD);
            OBJ_74.setName("OBJ_74");
            P_Centre.add(OBJ_74);
            OBJ_74.setBounds(65, 269, 310, 20);

            //---- OBJ_126 ----
            OBJ_126.setText("Saisie diff\u00e9rence de r\u00e8glement syst\u00e9matique");
            OBJ_126.setComponentPopupMenu(BTD);
            OBJ_126.setName("OBJ_126");
            P_Centre.add(OBJ_126);
            OBJ_126.setBounds(490, 360, 300, 20);

            //---- OBJ_71 ----
            OBJ_71.setText("Utilisation comptabilit\u00e9 Reporting");
            OBJ_71.setComponentPopupMenu(BTD);
            OBJ_71.setName("OBJ_71");
            P_Centre.add(OBJ_71);
            OBJ_71.setBounds(65, 55, 310, 20);

            //---- OBJ_76 ----
            OBJ_76.setText("Relance client au niveau le plus \u00e9lev\u00e9");
            OBJ_76.setComponentPopupMenu(BTD);
            OBJ_76.setName("OBJ_76");
            P_Centre.add(OBJ_76);
            OBJ_76.setBounds(65, 331, 310, 20);

            //---- OBJ_118 ----
            OBJ_118.setText("Gestion des bons \u00e0 payer sur dettes");
            OBJ_118.setComponentPopupMenu(BTD);
            OBJ_118.setName("OBJ_118");
            P_Centre.add(OBJ_118);
            OBJ_118.setBounds(490, 110, 300, 20);

            //---- OBJ_91 ----
            OBJ_91.setText("Edition sp\u00e9cifique titre de paiement");
            OBJ_91.setName("OBJ_91");
            P_Centre.add(OBJ_91);
            OBJ_91.setBounds(65, 145, 310, 20);

            //---- OBJ_119 ----
            OBJ_119.setText("Lettrage automatique sur r\u00e9f\u00e9rence");
            OBJ_119.setComponentPopupMenu(BTD);
            OBJ_119.setName("OBJ_119");
            P_Centre.add(OBJ_119);
            OBJ_119.setBounds(490, 140, 300, 20);

            //---- OBJ_122 ----
            OBJ_122.setText("Annulation automatique des dettes");
            OBJ_122.setComponentPopupMenu(BTD);
            OBJ_122.setName("OBJ_122");
            P_Centre.add(OBJ_122);
            OBJ_122.setBounds(490, 235, 300, 20);

            //---- OBJ_125 ----
            OBJ_125.setText("Folio 2 facultatif sur \" GF \" TRCE");
            OBJ_125.setComponentPopupMenu(BTD);
            OBJ_125.setName("OBJ_125");
            P_Centre.add(OBJ_125);
            OBJ_125.setBounds(490, 325, 300, 20);

            //---- OBJ_73 ----
            OBJ_73.setText("Contr\u00f4le r\u00e8glement sur \u00e9ch\u00e9ance");
            OBJ_73.setComponentPopupMenu(BTD);
            OBJ_73.setName("OBJ_73");
            P_Centre.add(OBJ_73);
            OBJ_73.setBounds(65, 238, 310, 20);

            //---- OBJ_116 ----
            OBJ_116.setText("S\u00e9lection des cr\u00e9dits non point\u00e9s");
            OBJ_116.setComponentPopupMenu(BTD);
            OBJ_116.setName("OBJ_116");
            P_Centre.add(OBJ_116);
            OBJ_116.setBounds(490, 55, 300, 20);

            //---- OBJ_72 ----
            OBJ_72.setText("S\u00e9lection des crit\u00e8res de relance");
            OBJ_72.setComponentPopupMenu(BTD);
            OBJ_72.setName("OBJ_72");
            P_Centre.add(OBJ_72);
            OBJ_72.setBounds(65, 176, 310, 20);

            //---- OBJ_87 ----
            OBJ_87.setText("Gestion r\u00e8glement avec \u00e9tranger");
            OBJ_87.setName("OBJ_87");
            P_Centre.add(OBJ_87);
            OBJ_87.setBounds(65, 83, 310, 20);

            //---- OBJ_56 ----
            OBJ_56.setText("Aller \u00e0 la page");
            OBJ_56.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_56.setName("OBJ_56");
            OBJ_56.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_56ActionPerformed(e);
              }
            });
            P_Centre.add(OBJ_56);
            OBJ_56.setBounds(630, 440, 152, 28);

            //---- OBJ_89 ----
            OBJ_89.setText("Saisie r\u00e9f\u00e9rence tir\u00e9 sur effets");
            OBJ_89.setName("OBJ_89");
            P_Centre.add(OBJ_89);
            OBJ_89.setBounds(65, 114, 310, 20);

            //---- OBJ_120 ----
            OBJ_120.setText("R\u00e8glements multi-clients");
            OBJ_120.setComponentPopupMenu(BTD);
            OBJ_120.setName("OBJ_120");
            P_Centre.add(OBJ_120);
            OBJ_120.setBounds(490, 170, 300, 20);

            //---- OBJ_156 ----
            OBJ_156.setText("Voir Documentation");
            OBJ_156.setName("OBJ_156");
            P_Centre.add(OBJ_156);
            OBJ_156.setBounds(355, 446, 120, 20);

            //---- OBJ_94 ----
            OBJ_94.setText("Gestion code firme");
            OBJ_94.setName("OBJ_94");
            P_Centre.add(OBJ_94);
            OBJ_94.setBounds(65, 207, 310, 20);

            //---- OBJ_154 ----
            OBJ_154.setText("0 < Val < 9");
            OBJ_154.setName("OBJ_154");
            P_Centre.add(OBJ_154);
            OBJ_154.setBounds(153, 446, 63, 20);

            //---- OBJ_155 ----
            OBJ_155.setText("Voir Aide");
            OBJ_155.setName("OBJ_155");
            P_Centre.add(OBJ_155);
            OBJ_155.setBounds(258, 446, 58, 20);

            //---- OBJ_153 ----
            OBJ_153.setText("1= OUI");
            OBJ_153.setName("OBJ_153");
            P_Centre.add(OBJ_153);
            OBJ_153.setBounds(71, 446, 43, 20);

            //---- PST1 ----
            PST1.setComponentPopupMenu(BTD);
            PST1.setName("PST1");
            P_Centre.add(PST1);
            PST1.setBounds(378, 51, 21, PST1.getPreferredSize().height);

            //---- PST2 ----
            PST2.setComponentPopupMenu(BTD);
            PST2.setName("PST2");
            P_Centre.add(PST2);
            PST2.setBounds(378, 79, 20, PST2.getPreferredSize().height);

            //---- PST3 ----
            PST3.setComponentPopupMenu(BTD);
            PST3.setName("PST3");
            P_Centre.add(PST3);
            PST3.setBounds(378, 110, 20, PST3.getPreferredSize().height);

            //---- PST4 ----
            PST4.setComponentPopupMenu(BTD);
            PST4.setName("PST4");
            P_Centre.add(PST4);
            PST4.setBounds(378, 141, 20, PST4.getPreferredSize().height);

            //---- PST5 ----
            PST5.setComponentPopupMenu(BTD);
            PST5.setName("PST5");
            P_Centre.add(PST5);
            PST5.setBounds(378, 172, 21, PST5.getPreferredSize().height);

            //---- PST6 ----
            PST6.setComponentPopupMenu(BTD);
            PST6.setName("PST6");
            P_Centre.add(PST6);
            PST6.setBounds(378, 203, 20, PST6.getPreferredSize().height);

            //---- PST7 ----
            PST7.setComponentPopupMenu(BTD);
            PST7.setName("PST7");
            P_Centre.add(PST7);
            PST7.setBounds(378, 234, 21, PST7.getPreferredSize().height);

            //---- PST8 ----
            PST8.setComponentPopupMenu(BTD);
            PST8.setName("PST8");
            P_Centre.add(PST8);
            PST8.setBounds(378, 265, 21, PST8.getPreferredSize().height);

            //---- PST9 ----
            PST9.setComponentPopupMenu(BTD);
            PST9.setName("PST9");
            P_Centre.add(PST9);
            PST9.setBounds(378, 296, 21, PST9.getPreferredSize().height);

            //---- PST10 ----
            PST10.setComponentPopupMenu(BTD);
            PST10.setName("PST10");
            P_Centre.add(PST10);
            PST10.setBounds(378, 327, 21, PST10.getPreferredSize().height);

            //---- PST11 ----
            PST11.setComponentPopupMenu(BTD);
            PST11.setName("PST11");
            P_Centre.add(PST11);
            PST11.setBounds(378, 358, 21, PST11.getPreferredSize().height);

            //---- PST12 ----
            PST12.setComponentPopupMenu(BTD);
            PST12.setName("PST12");
            P_Centre.add(PST12);
            PST12.setBounds(790, 51, 21, PST12.getPreferredSize().height);

            //---- PST13 ----
            PST13.setComponentPopupMenu(BTD);
            PST13.setName("PST13");
            P_Centre.add(PST13);
            PST13.setBounds(790, 75, 21, PST13.getPreferredSize().height);

            //---- PST14 ----
            PST14.setComponentPopupMenu(BTD);
            PST14.setName("PST14");
            P_Centre.add(PST14);
            PST14.setBounds(790, 105, 21, PST14.getPreferredSize().height);

            //---- PST15 ----
            PST15.setComponentPopupMenu(BTD);
            PST15.setName("PST15");
            P_Centre.add(PST15);
            PST15.setBounds(790, 135, 21, PST15.getPreferredSize().height);

            //---- PST16 ----
            PST16.setComponentPopupMenu(BTD);
            PST16.setName("PST16");
            P_Centre.add(PST16);
            PST16.setBounds(790, 170, 21, PST16.getPreferredSize().height);

            //---- PST17 ----
            PST17.setComponentPopupMenu(BTD);
            PST17.setName("PST17");
            P_Centre.add(PST17);
            PST17.setBounds(790, 200, 21, PST17.getPreferredSize().height);

            //---- PST18 ----
            PST18.setComponentPopupMenu(BTD);
            PST18.setName("PST18");
            P_Centre.add(PST18);
            PST18.setBounds(790, 230, 21, PST18.getPreferredSize().height);

            //---- PST19 ----
            PST19.setComponentPopupMenu(BTD);
            PST19.setName("PST19");
            P_Centre.add(PST19);
            PST19.setBounds(790, 260, 21, PST19.getPreferredSize().height);

            //---- PST20 ----
            PST20.setComponentPopupMenu(BTD);
            PST20.setName("PST20");
            P_Centre.add(PST20);
            PST20.setBounds(790, 290, 21, PST20.getPreferredSize().height);

            //---- PST21 ----
            PST21.setComponentPopupMenu(BTD);
            PST21.setName("PST21");
            P_Centre.add(PST21);
            PST21.setBounds(790, 325, 21, PST21.getPreferredSize().height);

            //---- PST22 ----
            PST22.setComponentPopupMenu(BTD);
            PST22.setName("PST22");
            P_Centre.add(PST22);
            PST22.setBounds(790, 355, 21, PST22.getPreferredSize().height);

            //---- V06F ----
            V06F.setComponentPopupMenu(BTD);
            V06F.setName("V06F");
            P_Centre.add(V06F);
            V06F.setBounds(790, 440, 20, V06F.getPreferredSize().height);

            //---- V06F1 ----
            V06F1.setComponentPopupMenu(BTD);
            V06F1.setName("V06F1");
            P_Centre.add(V06F1);
            V06F1.setBounds(790, 440, 20, V06F1.getPreferredSize().height);

            //---- OBJ_86 ----
            OBJ_86.setText("01");
            OBJ_86.setName("OBJ_86");
            P_Centre.add(OBJ_86);
            OBJ_86.setBounds(45, 55, 18, 20);

            //---- OBJ_88 ----
            OBJ_88.setText("02");
            OBJ_88.setName("OBJ_88");
            P_Centre.add(OBJ_88);
            OBJ_88.setBounds(45, 83, 18, 20);

            //---- OBJ_90 ----
            OBJ_90.setText("03");
            OBJ_90.setName("OBJ_90");
            P_Centre.add(OBJ_90);
            OBJ_90.setBounds(45, 114, 18, 20);

            //---- OBJ_92 ----
            OBJ_92.setText("04");
            OBJ_92.setName("OBJ_92");
            P_Centre.add(OBJ_92);
            OBJ_92.setBounds(45, 145, 18, 20);

            //---- OBJ_93 ----
            OBJ_93.setText("05");
            OBJ_93.setName("OBJ_93");
            P_Centre.add(OBJ_93);
            OBJ_93.setBounds(45, 176, 18, 20);

            //---- OBJ_95 ----
            OBJ_95.setText("06");
            OBJ_95.setName("OBJ_95");
            P_Centre.add(OBJ_95);
            OBJ_95.setBounds(45, 207, 18, 20);

            //---- OBJ_97 ----
            OBJ_97.setText("07");
            OBJ_97.setName("OBJ_97");
            P_Centre.add(OBJ_97);
            OBJ_97.setBounds(45, 238, 18, 20);

            //---- OBJ_98 ----
            OBJ_98.setText("08");
            OBJ_98.setName("OBJ_98");
            P_Centre.add(OBJ_98);
            OBJ_98.setBounds(45, 269, 18, 20);

            //---- OBJ_99 ----
            OBJ_99.setText("09");
            OBJ_99.setName("OBJ_99");
            P_Centre.add(OBJ_99);
            OBJ_99.setBounds(45, 300, 18, 20);

            //---- OBJ_100 ----
            OBJ_100.setText("10");
            OBJ_100.setName("OBJ_100");
            P_Centre.add(OBJ_100);
            OBJ_100.setBounds(45, 331, 18, 20);

            //---- OBJ_101 ----
            OBJ_101.setText("11");
            OBJ_101.setName("OBJ_101");
            P_Centre.add(OBJ_101);
            OBJ_101.setBounds(45, 362, 18, 20);

            //---- OBJ_105 ----
            OBJ_105.setText("12");
            OBJ_105.setName("OBJ_105");
            P_Centre.add(OBJ_105);
            OBJ_105.setBounds(460, 55, 18, 20);

            //---- OBJ_106 ----
            OBJ_106.setText("13");
            OBJ_106.setName("OBJ_106");
            P_Centre.add(OBJ_106);
            OBJ_106.setBounds(460, 80, 18, 20);

            //---- OBJ_107 ----
            OBJ_107.setText("14");
            OBJ_107.setName("OBJ_107");
            P_Centre.add(OBJ_107);
            OBJ_107.setBounds(460, 110, 18, 20);

            //---- OBJ_108 ----
            OBJ_108.setText("15");
            OBJ_108.setName("OBJ_108");
            P_Centre.add(OBJ_108);
            OBJ_108.setBounds(460, 140, 18, 20);

            //---- OBJ_109 ----
            OBJ_109.setText("16");
            OBJ_109.setName("OBJ_109");
            P_Centre.add(OBJ_109);
            OBJ_109.setBounds(460, 170, 18, 20);

            //---- OBJ_110 ----
            OBJ_110.setText("17");
            OBJ_110.setName("OBJ_110");
            P_Centre.add(OBJ_110);
            OBJ_110.setBounds(460, 205, 18, 20);

            //---- OBJ_111 ----
            OBJ_111.setText("18");
            OBJ_111.setName("OBJ_111");
            P_Centre.add(OBJ_111);
            OBJ_111.setBounds(460, 235, 18, 20);

            //---- OBJ_112 ----
            OBJ_112.setText("19");
            OBJ_112.setName("OBJ_112");
            P_Centre.add(OBJ_112);
            OBJ_112.setBounds(460, 265, 18, 20);

            //---- OBJ_113 ----
            OBJ_113.setText("20");
            OBJ_113.setName("OBJ_113");
            P_Centre.add(OBJ_113);
            OBJ_113.setBounds(460, 295, 18, 20);

            //---- OBJ_114 ----
            OBJ_114.setText("21");
            OBJ_114.setName("OBJ_114");
            P_Centre.add(OBJ_114);
            OBJ_114.setBounds(460, 325, 18, 20);

            //---- OBJ_115 ----
            OBJ_115.setText("22");
            OBJ_115.setName("OBJ_115");
            P_Centre.add(OBJ_115);
            OBJ_115.setBounds(460, 360, 18, 20);

            //---- OBJ_149 ----
            OBJ_149.setText("a");
            OBJ_149.setName("OBJ_149");
            P_Centre.add(OBJ_149);
            OBJ_149.setBounds(47, 446, 13, 20);

            //---- OBJ_150 ----
            OBJ_150.setText("b");
            OBJ_150.setName("OBJ_150");
            P_Centre.add(OBJ_150);
            OBJ_150.setBounds(129, 446, 13, 20);

            //---- OBJ_151 ----
            OBJ_151.setText("c");
            OBJ_151.setName("OBJ_151");
            P_Centre.add(OBJ_151);
            OBJ_151.setBounds(233, 446, 13, 20);

            //---- OBJ_152 ----
            OBJ_152.setText("d");
            OBJ_152.setName("OBJ_152");
            P_Centre.add(OBJ_152);
            OBJ_152.setBounds(332, 446, 13, 20);

            //---- OBJ_127 ----
            OBJ_127.setText("c");
            OBJ_127.setComponentPopupMenu(BTD);
            OBJ_127.setName("OBJ_127");
            P_Centre.add(OBJ_127);
            OBJ_127.setBounds(407, 57, 12, OBJ_127.getPreferredSize().height);

            //---- OBJ_128 ----
            OBJ_128.setText("c");
            OBJ_128.setComponentPopupMenu(BTD);
            OBJ_128.setName("OBJ_128");
            P_Centre.add(OBJ_128);
            OBJ_128.setBounds(407, 85, 12, OBJ_128.getPreferredSize().height);

            //---- OBJ_129 ----
            OBJ_129.setText("c");
            OBJ_129.setComponentPopupMenu(BTD);
            OBJ_129.setName("OBJ_129");
            P_Centre.add(OBJ_129);
            OBJ_129.setBounds(407, 116, 12, OBJ_129.getPreferredSize().height);

            //---- OBJ_130 ----
            OBJ_130.setText("c");
            OBJ_130.setComponentPopupMenu(BTD);
            OBJ_130.setName("OBJ_130");
            P_Centre.add(OBJ_130);
            OBJ_130.setBounds(407, 147, 12, OBJ_130.getPreferredSize().height);

            //---- OBJ_131 ----
            OBJ_131.setText("c");
            OBJ_131.setComponentPopupMenu(BTD);
            OBJ_131.setName("OBJ_131");
            P_Centre.add(OBJ_131);
            OBJ_131.setBounds(407, 209, 12, OBJ_131.getPreferredSize().height);

            //---- OBJ_132 ----
            OBJ_132.setText("c");
            OBJ_132.setComponentPopupMenu(BTD);
            OBJ_132.setName("OBJ_132");
            P_Centre.add(OBJ_132);
            OBJ_132.setBounds(407, 178, 12, OBJ_132.getPreferredSize().height);

            //---- OBJ_133 ----
            OBJ_133.setText("c");
            OBJ_133.setComponentPopupMenu(BTD);
            OBJ_133.setName("OBJ_133");
            P_Centre.add(OBJ_133);
            OBJ_133.setBounds(407, 240, 12, OBJ_133.getPreferredSize().height);

            //---- OBJ_134 ----
            OBJ_134.setText("c");
            OBJ_134.setComponentPopupMenu(BTD);
            OBJ_134.setName("OBJ_134");
            P_Centre.add(OBJ_134);
            OBJ_134.setBounds(407, 271, 12, OBJ_134.getPreferredSize().height);

            //---- OBJ_135 ----
            OBJ_135.setText("c");
            OBJ_135.setComponentPopupMenu(BTD);
            OBJ_135.setName("OBJ_135");
            P_Centre.add(OBJ_135);
            OBJ_135.setBounds(407, 302, 12, OBJ_135.getPreferredSize().height);

            //---- OBJ_136 ----
            OBJ_136.setText("c");
            OBJ_136.setComponentPopupMenu(BTD);
            OBJ_136.setName("OBJ_136");
            P_Centre.add(OBJ_136);
            OBJ_136.setBounds(407, 333, 12, OBJ_136.getPreferredSize().height);

            //---- OBJ_137 ----
            OBJ_137.setText("c");
            OBJ_137.setComponentPopupMenu(BTD);
            OBJ_137.setName("OBJ_137");
            P_Centre.add(OBJ_137);
            OBJ_137.setBounds(407, 364, 12, OBJ_137.getPreferredSize().height);

            //---- OBJ_138 ----
            OBJ_138.setText("c");
            OBJ_138.setComponentPopupMenu(BTD);
            OBJ_138.setName("OBJ_138");
            P_Centre.add(OBJ_138);
            OBJ_138.setBounds(820, 57, 12, OBJ_138.getPreferredSize().height);

            //---- OBJ_139 ----
            OBJ_139.setText("c");
            OBJ_139.setComponentPopupMenu(BTD);
            OBJ_139.setName("OBJ_139");
            P_Centre.add(OBJ_139);
            OBJ_139.setBounds(820, 205, 12, OBJ_139.getPreferredSize().height);

            //---- OBJ_140 ----
            OBJ_140.setText("c");
            OBJ_140.setComponentPopupMenu(BTD);
            OBJ_140.setName("OBJ_140");
            P_Centre.add(OBJ_140);
            OBJ_140.setBounds(820, 235, 12, OBJ_140.getPreferredSize().height);

            //---- OBJ_141 ----
            OBJ_141.setText("c");
            OBJ_141.setComponentPopupMenu(BTD);
            OBJ_141.setName("OBJ_141");
            P_Centre.add(OBJ_141);
            OBJ_141.setBounds(820, 300, 12, OBJ_141.getPreferredSize().height);

            //---- OBJ_142 ----
            OBJ_142.setText("c");
            OBJ_142.setComponentPopupMenu(BTD);
            OBJ_142.setName("OBJ_142");
            P_Centre.add(OBJ_142);
            OBJ_142.setBounds(820, 360, 12, OBJ_142.getPreferredSize().height);

            //---- OBJ_143 ----
            OBJ_143.setText("c");
            OBJ_143.setComponentPopupMenu(BTD);
            OBJ_143.setName("OBJ_143");
            P_Centre.add(OBJ_143);
            OBJ_143.setBounds(820, 110, 12, OBJ_143.getPreferredSize().height);

            //---- OBJ_144 ----
            OBJ_144.setText("c");
            OBJ_144.setComponentPopupMenu(BTD);
            OBJ_144.setName("OBJ_144");
            P_Centre.add(OBJ_144);
            OBJ_144.setBounds(820, 80, 12, OBJ_144.getPreferredSize().height);

            //---- OBJ_145 ----
            OBJ_145.setText("c");
            OBJ_145.setComponentPopupMenu(BTD);
            OBJ_145.setName("OBJ_145");
            P_Centre.add(OBJ_145);
            OBJ_145.setBounds(820, 145, 12, OBJ_145.getPreferredSize().height);

            //---- OBJ_146 ----
            OBJ_146.setText("c");
            OBJ_146.setComponentPopupMenu(BTD);
            OBJ_146.setName("OBJ_146");
            P_Centre.add(OBJ_146);
            OBJ_146.setBounds(820, 175, 12, OBJ_146.getPreferredSize().height);

            //---- OBJ_147 ----
            OBJ_147.setText("c");
            OBJ_147.setComponentPopupMenu(BTD);
            OBJ_147.setName("OBJ_147");
            P_Centre.add(OBJ_147);
            OBJ_147.setBounds(820, 265, 12, OBJ_147.getPreferredSize().height);

            //---- OBJ_148 ----
            OBJ_148.setText("c");
            OBJ_148.setComponentPopupMenu(BTD);
            OBJ_148.setName("OBJ_148");
            P_Centre.add(OBJ_148);
            OBJ_148.setBounds(820, 330, 12, OBJ_148.getPreferredSize().height);

            //---- separator1 ----
            separator1.setName("separator1");
            P_Centre.add(separator1);
            separator1.setBounds(25, 15, 830, separator1.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < P_Centre.getComponentCount(); i++) {
                Rectangle bounds = P_Centre.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = P_Centre.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              P_Centre.setMinimumSize(preferredSize);
              P_Centre.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(P_Centre, GroupLayout.PREFERRED_SIZE, 860, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(P_Centre, GroupLayout.PREFERRED_SIZE, 485, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_14 ----
      OBJ_14.setText("Choix possibles");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);

      //---- OBJ_13 ----
      OBJ_13.setText("Aide en ligne");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_29;
  private RiZoneSortie INDETB;
  private JLabel OBJ_78;
  private RiZoneSortie INDTYP;
  private JLabel OBJ_79;
  private RiZoneSortie INDIND;
  private JPanel p_tete_droite;
  private JPanel panel1;
  private JLabel OBJ_84;
  private RiZoneSortie OBJ_85;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel P_Centre;
  private JXTitledSeparator OBJ_24;
  private JLabel OBJ_75;
  private JLabel OBJ_121;
  private JLabel OBJ_123;
  private JLabel OBJ_117;
  private JLabel OBJ_124;
  private JLabel OBJ_77;
  private JLabel OBJ_74;
  private JLabel OBJ_126;
  private JLabel OBJ_71;
  private JLabel OBJ_76;
  private JLabel OBJ_118;
  private JLabel OBJ_91;
  private JLabel OBJ_119;
  private JLabel OBJ_122;
  private JLabel OBJ_125;
  private JLabel OBJ_73;
  private JLabel OBJ_116;
  private JLabel OBJ_72;
  private JLabel OBJ_87;
  private SNBouton OBJ_56;
  private JLabel OBJ_89;
  private JLabel OBJ_120;
  private JLabel OBJ_156;
  private JLabel OBJ_94;
  private JLabel OBJ_154;
  private JLabel OBJ_155;
  private JLabel OBJ_153;
  private XRiTextField PST1;
  private XRiTextField PST2;
  private XRiTextField PST3;
  private XRiTextField PST4;
  private XRiTextField PST5;
  private XRiTextField PST6;
  private XRiTextField PST7;
  private XRiTextField PST8;
  private XRiTextField PST9;
  private XRiTextField PST10;
  private XRiTextField PST11;
  private XRiTextField PST12;
  private XRiTextField PST13;
  private XRiTextField PST14;
  private XRiTextField PST15;
  private XRiTextField PST16;
  private XRiTextField PST17;
  private XRiTextField PST18;
  private XRiTextField PST19;
  private XRiTextField PST20;
  private XRiTextField PST21;
  private XRiTextField PST22;
  private XRiTextField V06F;
  private XRiTextField V06F1;
  private JLabel OBJ_86;
  private JLabel OBJ_88;
  private JLabel OBJ_90;
  private JLabel OBJ_92;
  private JLabel OBJ_93;
  private JLabel OBJ_95;
  private JLabel OBJ_97;
  private JLabel OBJ_98;
  private JLabel OBJ_99;
  private JLabel OBJ_100;
  private JLabel OBJ_101;
  private JLabel OBJ_105;
  private JLabel OBJ_106;
  private JLabel OBJ_107;
  private JLabel OBJ_108;
  private JLabel OBJ_109;
  private JLabel OBJ_110;
  private JLabel OBJ_111;
  private JLabel OBJ_112;
  private JLabel OBJ_113;
  private JLabel OBJ_114;
  private JLabel OBJ_115;
  private JLabel OBJ_149;
  private JLabel OBJ_150;
  private JLabel OBJ_151;
  private JLabel OBJ_152;
  private JLabel OBJ_127;
  private JLabel OBJ_128;
  private JLabel OBJ_129;
  private JLabel OBJ_130;
  private JLabel OBJ_131;
  private JLabel OBJ_132;
  private JLabel OBJ_133;
  private JLabel OBJ_134;
  private JLabel OBJ_135;
  private JLabel OBJ_136;
  private JLabel OBJ_137;
  private JLabel OBJ_138;
  private JLabel OBJ_139;
  private JLabel OBJ_140;
  private JLabel OBJ_141;
  private JLabel OBJ_142;
  private JLabel OBJ_143;
  private JLabel OBJ_144;
  private JLabel OBJ_145;
  private JLabel OBJ_146;
  private JLabel OBJ_147;
  private JLabel OBJ_148;
  private JComponent separator1;
  private JPopupMenu BTD;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_13;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
