
package ri.serien.libecranrpg.vcgm.VCGM45FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VCGM45FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _LDR01_Title = { "", "", "Niv", "Montant", "Echéance", "Règlement", "Préimputation", };
  private String[][] _LDR01_Data = { { "LDR01", "DDR01", "NIV01", "MTT01", "ECH01", "RGL01", "PRI01", },
      { "LDR02", "DDR02", "NIV02", "MTT02", "ECH02", "RGL02", "PRI02", },
      { "LDR03", "DDR03", "NIV03", "MTT03", "ECH03", "RGL03", "PRI03", },
      { "LDR04", "DDR04", "NIV04", "MTT04", "ECH04", "RGL04", "PRI04", }, };
  private int[] _LDR01_Width = { 88, 63, 30, 109, 70, 79, 105, };
  // private String[] _LIST_Top=null;
  
  public VCGM45FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    LDR01.setAspectTable(null, _LDR01_Title, _LDR01_Data, _LDR01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_56.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1SNS@")).trim());
    OBJ_61.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1DEV@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // Ajoute à la liste des oData les variables non liées directement à un composant graphique
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _LIST_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    L1CLB.setEnabled(lexique.isPresent("L1CLB"));
    L1NLI.setEnabled(lexique.isPresent("L1NLI"));
    OBJ_56.setEnabled(lexique.isPresent("L1SNS"));
    L1NCA.setVisible(lexique.isPresent("L1NCA"));
    L1NCG.setVisible(lexique.isPresent("L1NCG"));
    L1RLV.setEnabled(lexique.isPresent("L1RLV"));
    OBJ_60.setEnabled(lexique.isPresent("L1MTD"));
    L1SAN.setVisible(lexique.isTrue("72"));
    OBJ_57.setEnabled(lexique.isPresent("L1QTE"));
    L1NAT.setVisible(lexique.isTrue("72"));
    OBJ_46.setEnabled(lexique.isPresent("L1PCE"));
    OBJ_61.setVisible(lexique.isPresent("L1DEV"));
    L1PCE.setEnabled(lexique.isPresent("L1PCE"));
    L1ACT.setVisible(lexique.isTrue("72"));
    OBJ_33.setEnabled(lexique.isPresent("L1RLV"));
    OBJ_42.setEnabled(lexique.isPresent("L1NCPX"));
    L1NCPX.setEnabled(lexique.isPresent("L1NCPX"));
    L1MTD.setEnabled(lexique.isPresent("L1MTD"));
    L1MTT.setEnabled(lexique.isPresent("L1MTT"));
    L1QTE.setEnabled(lexique.isPresent("L1QTE"));
    L1RFC.setEnabled(lexique.isPresent("L1RFC"));
    DVCHG.setEnabled(lexique.isPresent("DVCHG"));
    OBJ_34.setEnabled(lexique.isPresent("L1RFC"));
    L1LIB.setEnabled(lexique.isPresent("L1LIB"));
    LIBCPT.setVisible(lexique.isPresent("LIBCPT"));
    OBJ_43.setVisible(lexique.isTrue("72"));
    OBJ_44.setVisible(lexique.isTrue("72"));
    OBJ_45.setVisible(lexique.isTrue("72"));
    xTitledPanel2.setVisible(lexique.isTrue("74"));
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
    
    

    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    xTitledPanel2 = new JXTitledPanel();
    SCROLLPANE_OBJ_15 = new JScrollPane();
    LDR01 = new XRiTable();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_55 = new JLabel();
    OBJ_60 = new JLabel();
    L1MTT = new XRiTextField();
    L1MTD = new XRiTextField();
    OBJ_56 = new RiZoneSortie();
    OBJ_61 = new RiZoneSortie();
    DVCHG = new XRiTextField();
    L1QTE = new XRiTextField();
    OBJ_57 = new JLabel();
    OBJ_62 = new JLabel();
    xTitledPanel3 = new JXTitledPanel();
    L1LIB = new XRiTextField();
    OBJ_34 = new JLabel();
    OBJ_28 = new JLabel();
    L1RFC = new XRiTextField();
    L1NCPX = new XRiTextField();
    OBJ_42 = new JLabel();
    OBJ_33 = new JLabel();
    OBJ_44 = new JLabel();
    OBJ_45 = new JLabel();
    L1ACT = new XRiTextField();
    OBJ_43 = new JLabel();
    L1PCE = new XRiTextField();
    OBJ_46 = new JLabel();
    L1NAT = new XRiTextField();
    OBJ_25 = new JLabel();
    L1SAN = new XRiTextField();
    OBJ_35 = new JLabel();
    OBJ_26 = new JLabel();
    L1RLV = new XRiTextField();
    L1NCG = new XRiTextField();
    L1NCA = new XRiTextField();
    OBJ_27 = new JLabel();
    L1NLI = new XRiTextField();
    L1CLB = new XRiTextField();
    LIBCPT = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();
    OBJ_9 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(930, 570));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Bloc-notes");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== xTitledPanel2 ========
        {
          xTitledPanel2.setTitle("Ventilation \u00e9ch\u00e9ances");
          xTitledPanel2.setBorder(new DropShadowBorder());
          xTitledPanel2.setName("xTitledPanel2");
          Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
          xTitledPanel2ContentContainer.setLayout(null);

          //======== SCROLLPANE_OBJ_15 ========
          {
            SCROLLPANE_OBJ_15.setName("SCROLLPANE_OBJ_15");

            //---- LDR01 ----
            LDR01.setName("LDR01");
            SCROLLPANE_OBJ_15.setViewportView(LDR01);
          }
          xTitledPanel2ContentContainer.add(SCROLLPANE_OBJ_15);
          SCROLLPANE_OBJ_15.setBounds(40, 10, 650, 96);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel2ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(xTitledPanel2);
        xTitledPanel2.setBounds(10, 410, 740, 150);

        //======== xTitledPanel1 ========
        {
          xTitledPanel1.setBorder(new DropShadowBorder());
          xTitledPanel1.setTitle("Montant");
          xTitledPanel1.setName("xTitledPanel1");
          Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

          //---- OBJ_55 ----
          OBJ_55.setText("Montant");
          OBJ_55.setName("OBJ_55");

          //---- OBJ_60 ----
          OBJ_60.setText("Devise");
          OBJ_60.setName("OBJ_60");

          //---- L1MTT ----
          L1MTT.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
          L1MTT.setName("L1MTT");

          //---- L1MTD ----
          L1MTD.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
          L1MTD.setName("L1MTD");

          //---- OBJ_56 ----
          OBJ_56.setText("@L1SNS@");
          OBJ_56.setName("OBJ_56");

          //---- OBJ_61 ----
          OBJ_61.setText("@L1DEV@");
          OBJ_61.setName("OBJ_61");

          //---- DVCHG ----
          DVCHG.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
          DVCHG.setName("DVCHG");

          //---- L1QTE ----
          L1QTE.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
          L1QTE.setName("L1QTE");

          //---- OBJ_57 ----
          OBJ_57.setText("Quantit\u00e9");
          OBJ_57.setName("OBJ_57");

          //---- OBJ_62 ----
          OBJ_62.setText("Taux");
          OBJ_62.setName("OBJ_62");

          GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
          xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
          xTitledPanel1ContentContainerLayout.setHorizontalGroup(
            xTitledPanel1ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                    .addGap(13, 13, 13)
                    .addComponent(L1MTT, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                    .addGap(100, 100, 100)
                    .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                    .addComponent(L1QTE, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
                    .addGap(14, 14, 14)
                    .addComponent(L1MTD, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                    .addGap(100, 100, 100)
                    .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DVCHG, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE))))
          );
          xTitledPanel1ContentContainerLayout.setVerticalGroup(
            xTitledPanel1ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_55))
                  .addComponent(L1MTT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_57))
                  .addComponent(L1QTE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_60))
                  .addComponent(L1MTD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_62))
                  .addComponent(DVCHG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_contenu.add(xTitledPanel1);
        xTitledPanel1.setBounds(10, 270, 740, 130);

        //======== xTitledPanel3 ========
        {
          xTitledPanel3.setBorder(new DropShadowBorder());
          xTitledPanel3.setName("xTitledPanel3");
          Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();

          //---- L1LIB ----
          L1LIB.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
          L1LIB.setName("L1LIB");

          //---- OBJ_34 ----
          OBJ_34.setText("R\u00e9f\u00e9rence classement");
          OBJ_34.setName("OBJ_34");

          //---- OBJ_28 ----
          OBJ_28.setText("Libell\u00e9 du compte");
          OBJ_28.setFont(OBJ_28.getFont().deriveFont(OBJ_28.getFont().getStyle() | Font.BOLD));
          OBJ_28.setName("OBJ_28");

          //---- L1RFC ----
          L1RFC.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
          L1RFC.setName("L1RFC");

          //---- L1NCPX ----
          L1NCPX.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
          L1NCPX.setName("L1NCPX");

          //---- OBJ_42 ----
          OBJ_42.setText("Contre partie");
          OBJ_42.setName("OBJ_42");

          //---- OBJ_33 ----
          OBJ_33.setText("Relev\u00e9");
          OBJ_33.setName("OBJ_33");

          //---- OBJ_44 ----
          OBJ_44.setText("Affaire");
          OBJ_44.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_44.setName("OBJ_44");

          //---- OBJ_45 ----
          OBJ_45.setText("Nature");
          OBJ_45.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_45.setName("OBJ_45");

          //---- L1ACT ----
          L1ACT.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
          L1ACT.setName("L1ACT");

          //---- OBJ_43 ----
          OBJ_43.setText("Section");
          OBJ_43.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_43.setName("OBJ_43");

          //---- L1PCE ----
          L1PCE.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
          L1PCE.setName("L1PCE");

          //---- OBJ_46 ----
          OBJ_46.setText("N\u00b0 pi\u00e8ce");
          OBJ_46.setName("OBJ_46");

          //---- L1NAT ----
          L1NAT.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
          L1NAT.setName("L1NAT");

          //---- OBJ_25 ----
          OBJ_25.setText("N\u00b0 ligne");
          OBJ_25.setFont(OBJ_25.getFont().deriveFont(OBJ_25.getFont().getStyle() | Font.BOLD));
          OBJ_25.setName("OBJ_25");

          //---- L1SAN ----
          L1SAN.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
          L1SAN.setName("L1SAN");

          //---- OBJ_35 ----
          OBJ_35.setText("Libell\u00e9");
          OBJ_35.setName("OBJ_35");

          //---- OBJ_26 ----
          OBJ_26.setText("Compte");
          OBJ_26.setFont(OBJ_26.getFont().deriveFont(OBJ_26.getFont().getStyle() | Font.BOLD));
          OBJ_26.setName("OBJ_26");

          //---- L1RLV ----
          L1RLV.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
          L1RLV.setName("L1RLV");

          //---- L1NCG ----
          L1NCG.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
          L1NCG.setFont(L1NCG.getFont().deriveFont(L1NCG.getFont().getStyle() | Font.BOLD));
          L1NCG.setName("L1NCG");

          //---- L1NCA ----
          L1NCA.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
          L1NCA.setFont(L1NCA.getFont().deriveFont(L1NCA.getFont().getStyle() | Font.BOLD));
          L1NCA.setName("L1NCA");

          //---- OBJ_27 ----
          OBJ_27.setText("Tiers");
          OBJ_27.setFont(OBJ_27.getFont().deriveFont(OBJ_27.getFont().getStyle() | Font.BOLD));
          OBJ_27.setName("OBJ_27");

          //---- L1NLI ----
          L1NLI.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
          L1NLI.setFont(L1NLI.getFont().deriveFont(L1NLI.getFont().getStyle() | Font.BOLD));
          L1NLI.setName("L1NLI");

          //---- L1CLB ----
          L1CLB.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
          L1CLB.setName("L1CLB");

          //---- LIBCPT ----
          LIBCPT.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
          LIBCPT.setFont(LIBCPT.getFont().deriveFont(LIBCPT.getFont().getStyle() | Font.BOLD));
          LIBCPT.setName("LIBCPT");

          GroupLayout xTitledPanel3ContentContainerLayout = new GroupLayout(xTitledPanel3ContentContainer);
          xTitledPanel3ContentContainer.setLayout(xTitledPanel3ContentContainerLayout);
          xTitledPanel3ContentContainerLayout.setHorizontalGroup(
            xTitledPanel3ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                    .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(OBJ_26, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                    .addGap(8, 8, 8)
                    .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                    .addComponent(L1NLI, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                    .addGap(34, 34, 34)
                    .addComponent(L1NCG, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(L1NCA, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addGap(35, 35, 35)
                    .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
                    .addGap(20, 20, 20)
                    .addComponent(LIBCPT, GroupLayout.PREFERRED_SIZE, 339, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 61, GroupLayout.PREFERRED_SIZE)
                  .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                    .addComponent(L1LIB, GroupLayout.PREFERRED_SIZE, 220, GroupLayout.PREFERRED_SIZE)
                    .addComponent(L1CLB, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
                    .addGap(15, 15, 15)
                    .addComponent(L1RFC, GroupLayout.PREFERRED_SIZE, 92, GroupLayout.PREFERRED_SIZE)
                    .addGap(43, 43, 43)
                    .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
                    .addGap(11, 11, 11)
                    .addComponent(L1RLV, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                    .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
                    .addGap(121, 121, 121)
                    .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE)
                    .addGap(41, 41, 41)
                    .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
                    .addGap(24, 24, 24)
                    .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                    .addGap(17, 17, 17)
                    .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                    .addComponent(L1PCE, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
                    .addGap(107, 107, 107)
                    .addComponent(L1NCPX, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addGap(60, 60, 60)
                    .addComponent(L1SAN, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
                    .addGap(24, 24, 24)
                    .addComponent(L1ACT, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                    .addGap(17, 17, 17)
                    .addComponent(L1NAT, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE))))
          );
          xTitledPanel3ContentContainerLayout.setVerticalGroup(
            xTitledPanel3ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                  .addComponent(OBJ_25)
                  .addComponent(OBJ_26)
                  .addComponent(OBJ_27))
                .addGap(4, 4, 4)
                .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                  .addComponent(L1NLI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(L1NCG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(L1NCA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_28))
                  .addComponent(LIBCPT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addComponent(OBJ_35)
                .addGap(4, 4, 4)
                .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                  .addComponent(L1LIB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(L1CLB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_34))
                  .addComponent(L1RFC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_33))
                  .addComponent(L1RLV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                  .addComponent(OBJ_46)
                  .addComponent(OBJ_42)
                  .addComponent(OBJ_43)
                  .addComponent(OBJ_44)
                  .addComponent(OBJ_45))
                .addGap(4, 4, 4)
                .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                  .addComponent(L1PCE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(L1NCPX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(L1SAN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(L1ACT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(L1NAT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_contenu.add(xTitledPanel3);
        xTitledPanel3.setBounds(10, 10, 740, 250);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Choix possibles");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);

      //---- OBJ_9 ----
      OBJ_9.setText("Aide en ligne");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      BTD.add(OBJ_9);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel2;
  private JScrollPane SCROLLPANE_OBJ_15;
  private XRiTable LDR01;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_55;
  private JLabel OBJ_60;
  private XRiTextField L1MTT;
  private XRiTextField L1MTD;
  private RiZoneSortie OBJ_56;
  private RiZoneSortie OBJ_61;
  private XRiTextField DVCHG;
  private XRiTextField L1QTE;
  private JLabel OBJ_57;
  private JLabel OBJ_62;
  private JXTitledPanel xTitledPanel3;
  private XRiTextField L1LIB;
  private JLabel OBJ_34;
  private JLabel OBJ_28;
  private XRiTextField L1RFC;
  private XRiTextField L1NCPX;
  private JLabel OBJ_42;
  private JLabel OBJ_33;
  private JLabel OBJ_44;
  private JLabel OBJ_45;
  private XRiTextField L1ACT;
  private JLabel OBJ_43;
  private XRiTextField L1PCE;
  private JLabel OBJ_46;
  private XRiTextField L1NAT;
  private JLabel OBJ_25;
  private XRiTextField L1SAN;
  private JLabel OBJ_35;
  private JLabel OBJ_26;
  private XRiTextField L1RLV;
  private XRiTextField L1NCG;
  private XRiTextField L1NCA;
  private JLabel OBJ_27;
  private XRiTextField L1NLI;
  private XRiTextField L1CLB;
  private XRiTextField LIBCPT;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_9;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
