
package ri.serien.libecranrpg.vcgm.VCGM01FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM01FM_GF extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM01FM_GF(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    GFIMS.setValeurs("M", buttonGroup1);
    GFIMS_1.setValeurs("R");
    GFRGD1.setValeursSelection("1", "");
    GFRGC1.setValeursSelection("1", "");
    GFRGD2.setValeursSelection("1", "");
    GFRGC2.setValeursSelection("1", "");
    GFRGD3.setValeursSelection("1", "");
    GFRGC3.setValeursSelection("1", "");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    GFL01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@GFL01@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    GFL02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@GFL02@")).trim());
    OBJ_61_OBJ_61.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@GFL03@")).trim());
    OBJ_74_OBJ_74.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@GFL04@")).trim());
    OBJ_75_OBJ_75.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@GFL05@")).trim());
    OBJ_88_OBJ_88.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@GFL06@")).trim());
    OBJ_89_OBJ_89.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@GFL07@")).trim());
    OBJ_72_OBJ_72.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@GFC01@")).trim());
    OBJ_73_OBJ_73.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@GFC02@")).trim());
    OBJ_86_OBJ_86.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@GFC03@")).trim());
    OBJ_87_OBJ_87.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@GFC04@")).trim());
    OBJ_100_OBJ_100.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@GFC05@")).trim());
    OBJ_101_OBJ_101.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@GFC06@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    OBJ_101_OBJ_101.setVisible(lexique.isPresent("GFC06"));
    OBJ_100_OBJ_100.setVisible(lexique.isPresent("GFC05"));
    OBJ_87_OBJ_87.setVisible(lexique.isPresent("GFC04"));
    OBJ_86_OBJ_86.setVisible(lexique.isPresent("GFC03"));
    OBJ_73_OBJ_73.setVisible(lexique.isPresent("GFC02"));
    OBJ_72_OBJ_72.setVisible(lexique.isPresent("GFC01"));
    GFIMS_1.setEnabled(GFIMS.isEnabled());
    GFL01.setVisible(lexique.isPresent("GFL01"));
    OBJ_89_OBJ_89.setVisible(lexique.isPresent("GFL07"));
    OBJ_88_OBJ_88.setVisible(lexique.isPresent("GFL06"));
    OBJ_75_OBJ_75.setVisible(lexique.isPresent("GFL05"));
    OBJ_74_OBJ_74.setVisible(lexique.isPresent("GFL04"));
    OBJ_61_OBJ_61.setVisible(lexique.isPresent("GFL03"));
    
    

    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    // riMenu_bt2.setIcon(lexique.getImage("images/options.png",true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (GFIMS.isSelected())
    // lexique.HostFieldPutData("GFIMS", 0, "X");
    // else
    // lexique.HostFieldPutData("GFIMS", 0, " ");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44_OBJ_45 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_45_OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46_OBJ_47 = new JLabel();
    INDIND = new XRiTextField();
    GFL01 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    GFL02 = new RiZoneSortie();
    OBJ_61_OBJ_61 = new RiZoneSortie();
    OBJ_74_OBJ_74 = new RiZoneSortie();
    OBJ_75_OBJ_75 = new RiZoneSortie();
    OBJ_88_OBJ_88 = new RiZoneSortie();
    OBJ_89_OBJ_89 = new RiZoneSortie();
    OBJ_72_OBJ_72 = new RiZoneSortie();
    OBJ_73_OBJ_73 = new RiZoneSortie();
    OBJ_86_OBJ_86 = new RiZoneSortie();
    OBJ_87_OBJ_87 = new RiZoneSortie();
    OBJ_100_OBJ_100 = new RiZoneSortie();
    OBJ_101_OBJ_101 = new RiZoneSortie();
    OBJ_63_OBJ_63 = new JLabel();
    OBJ_77_OBJ_77 = new JLabel();
    OBJ_91_OBJ_91 = new JLabel();
    OBJ_67_OBJ_67 = new JLabel();
    OBJ_70_OBJ_70 = new JLabel();
    OBJ_81_OBJ_81 = new JLabel();
    OBJ_84_OBJ_84 = new JLabel();
    OBJ_95_OBJ_95 = new JLabel();
    OBJ_98_OBJ_98 = new JLabel();
    OBJ_66_OBJ_66 = new JLabel();
    OBJ_80_OBJ_80 = new JLabel();
    OBJ_94_OBJ_94 = new JLabel();
    OBJ_69_OBJ_69 = new JLabel();
    OBJ_83_OBJ_83 = new JLabel();
    OBJ_97_OBJ_97 = new JLabel();
    GFJO1 = new XRiTextField();
    GFJO2 = new XRiTextField();
    GFJO3 = new XRiTextField();
    GFCOD1 = new XRiTextField();
    GFCOC1 = new XRiTextField();
    GFCOD2 = new XRiTextField();
    GFCOC2 = new XRiTextField();
    GFCOD3 = new XRiTextField();
    GFCOC3 = new XRiTextField();
    GFRGD1 = new XRiCheckBox();
    GFRGC1 = new XRiCheckBox();
    GFRGD2 = new XRiCheckBox();
    GFRGC2 = new XRiCheckBox();
    GFRGD3 = new XRiCheckBox();
    GFRGC3 = new XRiCheckBox();
    OBJ_68_OBJ_68 = new JLabel();
    OBJ_71_OBJ_71 = new JLabel();
    OBJ_82_OBJ_82 = new JLabel();
    OBJ_85_OBJ_85 = new JLabel();
    OBJ_96_OBJ_96 = new JLabel();
    OBJ_99_OBJ_99 = new JLabel();
    separator1 = compFactory.createSeparator("D\u00e9bit");
    separator2 = compFactory.createSeparator("D\u00e9bit");
    separator3 = compFactory.createSeparator("D\u00e9bit");
    separator4 = compFactory.createSeparator("Cr\u00e9dit");
    separator5 = compFactory.createSeparator("Cr\u00e9dit");
    separator6 = compFactory.createSeparator("Cr\u00e9dit");
    label1 = new JLabel();
    GFIMS = new XRiRadioButton();
    GFIMS_1 = new XRiRadioButton();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    buttonGroup1 = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44_OBJ_45 ----
          OBJ_44_OBJ_45.setText("Soci\u00e9t\u00e9");
          OBJ_44_OBJ_45.setName("OBJ_44_OBJ_45");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setName("INDETB");

          //---- OBJ_45_OBJ_46 ----
          OBJ_45_OBJ_46.setText("Code");
          OBJ_45_OBJ_46.setName("OBJ_45_OBJ_46");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setName("INDTYP");

          //---- OBJ_46_OBJ_47 ----
          OBJ_46_OBJ_47.setText("Ordre");
          OBJ_46_OBJ_47.setName("OBJ_46_OBJ_47");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setName("INDIND");

          //---- GFL01 ----
          GFL01.setText("@GFL01@");
          GFL01.setOpaque(false);
          GFL01.setName("GFL01");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(GFL01, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(GFL01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(700, 480));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("G\u00e9n\u00e9ration automatique des folios");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- GFL02 ----
            GFL02.setText("@GFL02@");
            GFL02.setName("GFL02");
            xTitledPanel1ContentContainer.add(GFL02);
            GFL02.setBounds(170, 10, 225, GFL02.getPreferredSize().height);

            //---- OBJ_61_OBJ_61 ----
            OBJ_61_OBJ_61.setText("@GFL03@");
            OBJ_61_OBJ_61.setName("OBJ_61_OBJ_61");
            xTitledPanel1ContentContainer.add(OBJ_61_OBJ_61);
            OBJ_61_OBJ_61.setBounds(405, 10, 225, OBJ_61_OBJ_61.getPreferredSize().height);

            //---- OBJ_74_OBJ_74 ----
            OBJ_74_OBJ_74.setText("@GFL04@");
            OBJ_74_OBJ_74.setName("OBJ_74_OBJ_74");
            xTitledPanel1ContentContainer.add(OBJ_74_OBJ_74);
            OBJ_74_OBJ_74.setBounds(170, 127, 225, OBJ_74_OBJ_74.getPreferredSize().height);

            //---- OBJ_75_OBJ_75 ----
            OBJ_75_OBJ_75.setText("@GFL05@");
            OBJ_75_OBJ_75.setName("OBJ_75_OBJ_75");
            xTitledPanel1ContentContainer.add(OBJ_75_OBJ_75);
            OBJ_75_OBJ_75.setBounds(405, 127, 225, OBJ_75_OBJ_75.getPreferredSize().height);

            //---- OBJ_88_OBJ_88 ----
            OBJ_88_OBJ_88.setText("@GFL06@");
            OBJ_88_OBJ_88.setName("OBJ_88_OBJ_88");
            xTitledPanel1ContentContainer.add(OBJ_88_OBJ_88);
            OBJ_88_OBJ_88.setBounds(170, 269, 225, OBJ_88_OBJ_88.getPreferredSize().height);

            //---- OBJ_89_OBJ_89 ----
            OBJ_89_OBJ_89.setText("@GFL07@");
            OBJ_89_OBJ_89.setName("OBJ_89_OBJ_89");
            xTitledPanel1ContentContainer.add(OBJ_89_OBJ_89);
            OBJ_89_OBJ_89.setBounds(405, 269, 225, OBJ_89_OBJ_89.getPreferredSize().height);

            //---- OBJ_72_OBJ_72 ----
            OBJ_72_OBJ_72.setText("@GFC01@");
            OBJ_72_OBJ_72.setName("OBJ_72_OBJ_72");
            xTitledPanel1ContentContainer.add(OBJ_72_OBJ_72);
            OBJ_72_OBJ_72.setBounds(215, 98, 70, OBJ_72_OBJ_72.getPreferredSize().height);

            //---- OBJ_73_OBJ_73 ----
            OBJ_73_OBJ_73.setText("@GFC02@");
            OBJ_73_OBJ_73.setName("OBJ_73_OBJ_73");
            xTitledPanel1ContentContainer.add(OBJ_73_OBJ_73);
            OBJ_73_OBJ_73.setBounds(455, 98, 70, OBJ_73_OBJ_73.getPreferredSize().height);

            //---- OBJ_86_OBJ_86 ----
            OBJ_86_OBJ_86.setText("@GFC03@");
            OBJ_86_OBJ_86.setName("OBJ_86_OBJ_86");
            xTitledPanel1ContentContainer.add(OBJ_86_OBJ_86);
            OBJ_86_OBJ_86.setBounds(215, 210, 70, OBJ_86_OBJ_86.getPreferredSize().height);

            //---- OBJ_87_OBJ_87 ----
            OBJ_87_OBJ_87.setText("@GFC04@");
            OBJ_87_OBJ_87.setName("OBJ_87_OBJ_87");
            xTitledPanel1ContentContainer.add(OBJ_87_OBJ_87);
            OBJ_87_OBJ_87.setBounds(455, 210, 70, OBJ_87_OBJ_87.getPreferredSize().height);

            //---- OBJ_100_OBJ_100 ----
            OBJ_100_OBJ_100.setText("@GFC05@");
            OBJ_100_OBJ_100.setName("OBJ_100_OBJ_100");
            xTitledPanel1ContentContainer.add(OBJ_100_OBJ_100);
            OBJ_100_OBJ_100.setBounds(215, 362, 70, OBJ_100_OBJ_100.getPreferredSize().height);

            //---- OBJ_101_OBJ_101 ----
            OBJ_101_OBJ_101.setText("@GFC06@");
            OBJ_101_OBJ_101.setName("OBJ_101_OBJ_101");
            xTitledPanel1ContentContainer.add(OBJ_101_OBJ_101);
            OBJ_101_OBJ_101.setBounds(455, 362, 70, OBJ_101_OBJ_101.getPreferredSize().height);

            //---- OBJ_63_OBJ_63 ----
            OBJ_63_OBJ_63.setText("Code journal");
            OBJ_63_OBJ_63.setName("OBJ_63_OBJ_63");
            xTitledPanel1ContentContainer.add(OBJ_63_OBJ_63);
            OBJ_63_OBJ_63.setBounds(25, 96, 84, 28);

            //---- OBJ_77_OBJ_77 ----
            OBJ_77_OBJ_77.setText("Code journal");
            OBJ_77_OBJ_77.setName("OBJ_77_OBJ_77");
            xTitledPanel1ContentContainer.add(OBJ_77_OBJ_77);
            OBJ_77_OBJ_77.setBounds(25, 208, 84, 28);

            //---- OBJ_91_OBJ_91 ----
            OBJ_91_OBJ_91.setText("Code journal");
            OBJ_91_OBJ_91.setName("OBJ_91_OBJ_91");
            xTitledPanel1ContentContainer.add(OBJ_91_OBJ_91);
            OBJ_91_OBJ_91.setBounds(25, 360, 84, 28);

            //---- OBJ_67_OBJ_67 ----
            OBJ_67_OBJ_67.setText("Compte");
            OBJ_67_OBJ_67.setName("OBJ_67_OBJ_67");
            xTitledPanel1ContentContainer.add(OBJ_67_OBJ_67);
            OBJ_67_OBJ_67.setBounds(215, 72, 51, 26);

            //---- OBJ_70_OBJ_70 ----
            OBJ_70_OBJ_70.setText("Compte");
            OBJ_70_OBJ_70.setName("OBJ_70_OBJ_70");
            xTitledPanel1ContentContainer.add(OBJ_70_OBJ_70);
            OBJ_70_OBJ_70.setBounds(455, 72, 51, 26);

            //---- OBJ_81_OBJ_81 ----
            OBJ_81_OBJ_81.setText("Compte");
            OBJ_81_OBJ_81.setName("OBJ_81_OBJ_81");
            xTitledPanel1ContentContainer.add(OBJ_81_OBJ_81);
            OBJ_81_OBJ_81.setBounds(215, 184, 51, 26);

            //---- OBJ_84_OBJ_84 ----
            OBJ_84_OBJ_84.setText("Compte");
            OBJ_84_OBJ_84.setName("OBJ_84_OBJ_84");
            xTitledPanel1ContentContainer.add(OBJ_84_OBJ_84);
            OBJ_84_OBJ_84.setBounds(455, 184, 51, 26);

            //---- OBJ_95_OBJ_95 ----
            OBJ_95_OBJ_95.setText("Compte");
            OBJ_95_OBJ_95.setName("OBJ_95_OBJ_95");
            xTitledPanel1ContentContainer.add(OBJ_95_OBJ_95);
            OBJ_95_OBJ_95.setBounds(215, 331, 51, 26);

            //---- OBJ_98_OBJ_98 ----
            OBJ_98_OBJ_98.setText("Compte");
            OBJ_98_OBJ_98.setName("OBJ_98_OBJ_98");
            xTitledPanel1ContentContainer.add(OBJ_98_OBJ_98);
            OBJ_98_OBJ_98.setBounds(455, 331, 51, 26);

            //---- OBJ_66_OBJ_66 ----
            OBJ_66_OBJ_66.setText("NCo");
            OBJ_66_OBJ_66.setName("OBJ_66_OBJ_66");
            xTitledPanel1ContentContainer.add(OBJ_66_OBJ_66);
            OBJ_66_OBJ_66.setBounds(170, 72, 30, 26);

            //---- OBJ_80_OBJ_80 ----
            OBJ_80_OBJ_80.setText("NCo");
            OBJ_80_OBJ_80.setName("OBJ_80_OBJ_80");
            xTitledPanel1ContentContainer.add(OBJ_80_OBJ_80);
            OBJ_80_OBJ_80.setBounds(170, 184, 30, 26);

            //---- OBJ_94_OBJ_94 ----
            OBJ_94_OBJ_94.setText("NCo");
            OBJ_94_OBJ_94.setName("OBJ_94_OBJ_94");
            xTitledPanel1ContentContainer.add(OBJ_94_OBJ_94);
            OBJ_94_OBJ_94.setBounds(170, 331, 30, 26);

            //---- OBJ_69_OBJ_69 ----
            OBJ_69_OBJ_69.setText("Nco");
            OBJ_69_OBJ_69.setName("OBJ_69_OBJ_69");
            xTitledPanel1ContentContainer.add(OBJ_69_OBJ_69);
            OBJ_69_OBJ_69.setBounds(405, 72, 28, 26);

            //---- OBJ_83_OBJ_83 ----
            OBJ_83_OBJ_83.setText("Nco");
            OBJ_83_OBJ_83.setName("OBJ_83_OBJ_83");
            xTitledPanel1ContentContainer.add(OBJ_83_OBJ_83);
            OBJ_83_OBJ_83.setBounds(405, 184, 28, 26);

            //---- OBJ_97_OBJ_97 ----
            OBJ_97_OBJ_97.setText("Nco");
            OBJ_97_OBJ_97.setName("OBJ_97_OBJ_97");
            xTitledPanel1ContentContainer.add(OBJ_97_OBJ_97);
            OBJ_97_OBJ_97.setBounds(405, 331, 28, 26);

            //---- GFJO1 ----
            GFJO1.setComponentPopupMenu(BTD);
            GFJO1.setName("GFJO1");
            xTitledPanel1ContentContainer.add(GFJO1);
            GFJO1.setBounds(125, 96, 30, GFJO1.getPreferredSize().height);

            //---- GFJO2 ----
            GFJO2.setComponentPopupMenu(BTD);
            GFJO2.setName("GFJO2");
            xTitledPanel1ContentContainer.add(GFJO2);
            GFJO2.setBounds(125, 208, 30, GFJO2.getPreferredSize().height);

            //---- GFJO3 ----
            GFJO3.setComponentPopupMenu(BTD);
            GFJO3.setName("GFJO3");
            xTitledPanel1ContentContainer.add(GFJO3);
            GFJO3.setBounds(125, 360, 30, GFJO3.getPreferredSize().height);

            //---- GFCOD1 ----
            GFCOD1.setComponentPopupMenu(BTD);
            GFCOD1.setName("GFCOD1");
            xTitledPanel1ContentContainer.add(GFCOD1);
            GFCOD1.setBounds(170, 96, 40, GFCOD1.getPreferredSize().height);

            //---- GFCOC1 ----
            GFCOC1.setComponentPopupMenu(BTD);
            GFCOC1.setName("GFCOC1");
            xTitledPanel1ContentContainer.add(GFCOC1);
            GFCOC1.setBounds(405, 96, 40, GFCOC1.getPreferredSize().height);

            //---- GFCOD2 ----
            GFCOD2.setComponentPopupMenu(BTD);
            GFCOD2.setName("GFCOD2");
            xTitledPanel1ContentContainer.add(GFCOD2);
            GFCOD2.setBounds(170, 208, 40, GFCOD2.getPreferredSize().height);

            //---- GFCOC2 ----
            GFCOC2.setComponentPopupMenu(BTD);
            GFCOC2.setName("GFCOC2");
            xTitledPanel1ContentContainer.add(GFCOC2);
            GFCOC2.setBounds(405, 208, 40, GFCOC2.getPreferredSize().height);

            //---- GFCOD3 ----
            GFCOD3.setComponentPopupMenu(BTD);
            GFCOD3.setName("GFCOD3");
            xTitledPanel1ContentContainer.add(GFCOD3);
            GFCOD3.setBounds(170, 360, 40, GFCOD3.getPreferredSize().height);

            //---- GFCOC3 ----
            GFCOC3.setComponentPopupMenu(BTD);
            GFCOC3.setName("GFCOC3");
            xTitledPanel1ContentContainer.add(GFCOC3);
            GFCOC3.setBounds(405, 360, 40, GFCOC3.getPreferredSize().height);

            //---- GFRGD1 ----
            GFRGD1.setName("GFRGD1");
            xTitledPanel1ContentContainer.add(GFRGD1);
            GFRGD1.setBounds(335, 101, 20, GFRGD1.getPreferredSize().height);

            //---- GFRGC1 ----
            GFRGC1.setName("GFRGC1");
            xTitledPanel1ContentContainer.add(GFRGC1);
            GFRGC1.setBounds(570, 101, 20, GFRGC1.getPreferredSize().height);

            //---- GFRGD2 ----
            GFRGD2.setName("GFRGD2");
            xTitledPanel1ContentContainer.add(GFRGD2);
            GFRGD2.setBounds(335, 213, 20, GFRGD2.getPreferredSize().height);

            //---- GFRGC2 ----
            GFRGC2.setName("GFRGC2");
            xTitledPanel1ContentContainer.add(GFRGC2);
            GFRGC2.setBounds(570, 213, 20, GFRGC2.getPreferredSize().height);

            //---- GFRGD3 ----
            GFRGD3.setName("GFRGD3");
            xTitledPanel1ContentContainer.add(GFRGD3);
            GFRGD3.setBounds(335, 365, 20, GFRGD3.getPreferredSize().height);

            //---- GFRGC3 ----
            GFRGC3.setName("GFRGC3");
            xTitledPanel1ContentContainer.add(GFRGC3);
            GFRGC3.setBounds(570, 365, 20, GFRGC3.getPreferredSize().height);

            //---- OBJ_68_OBJ_68 ----
            OBJ_68_OBJ_68.setText("R");
            OBJ_68_OBJ_68.setName("OBJ_68_OBJ_68");
            xTitledPanel1ContentContainer.add(OBJ_68_OBJ_68);
            OBJ_68_OBJ_68.setBounds(339, 72, 13, 26);

            //---- OBJ_71_OBJ_71 ----
            OBJ_71_OBJ_71.setText("R");
            OBJ_71_OBJ_71.setName("OBJ_71_OBJ_71");
            xTitledPanel1ContentContainer.add(OBJ_71_OBJ_71);
            OBJ_71_OBJ_71.setBounds(574, 72, 13, 26);

            //---- OBJ_82_OBJ_82 ----
            OBJ_82_OBJ_82.setText("R");
            OBJ_82_OBJ_82.setName("OBJ_82_OBJ_82");
            xTitledPanel1ContentContainer.add(OBJ_82_OBJ_82);
            OBJ_82_OBJ_82.setBounds(339, 184, 13, 26);

            //---- OBJ_85_OBJ_85 ----
            OBJ_85_OBJ_85.setText("R");
            OBJ_85_OBJ_85.setName("OBJ_85_OBJ_85");
            xTitledPanel1ContentContainer.add(OBJ_85_OBJ_85);
            OBJ_85_OBJ_85.setBounds(574, 184, 13, 26);

            //---- OBJ_96_OBJ_96 ----
            OBJ_96_OBJ_96.setText("R");
            OBJ_96_OBJ_96.setName("OBJ_96_OBJ_96");
            xTitledPanel1ContentContainer.add(OBJ_96_OBJ_96);
            OBJ_96_OBJ_96.setBounds(339, 331, 13, 26);

            //---- OBJ_99_OBJ_99 ----
            OBJ_99_OBJ_99.setText("R");
            OBJ_99_OBJ_99.setName("OBJ_99_OBJ_99");
            xTitledPanel1ContentContainer.add(OBJ_99_OBJ_99);
            OBJ_99_OBJ_99.setBounds(574, 331, 13, 26);

            //---- separator1 ----
            separator1.setName("separator1");
            xTitledPanel1ContentContainer.add(separator1);
            separator1.setBounds(170, 50, 185, separator1.getPreferredSize().height);

            //---- separator2 ----
            separator2.setName("separator2");
            xTitledPanel1ContentContainer.add(separator2);
            separator2.setBounds(170, 162, 185, separator2.getPreferredSize().height);

            //---- separator3 ----
            separator3.setName("separator3");
            xTitledPanel1ContentContainer.add(separator3);
            separator3.setBounds(170, 309, 185, separator3.getPreferredSize().height);

            //---- separator4 ----
            separator4.setName("separator4");
            xTitledPanel1ContentContainer.add(separator4);
            separator4.setBounds(405, 50, 185, separator4.getPreferredSize().height);

            //---- separator5 ----
            separator5.setName("separator5");
            xTitledPanel1ContentContainer.add(separator5);
            separator5.setBounds(410, 162, 185, separator5.getPreferredSize().height);

            //---- separator6 ----
            separator6.setName("separator6");
            xTitledPanel1ContentContainer.add(separator6);
            separator6.setBounds(405, 309, 185, separator6.getPreferredSize().height);

            //---- label1 ----
            label1.setText("Comptabilisation \u00e0 la remise :");
            label1.setName("label1");
            xTitledPanel1ContentContainer.add(label1);
            label1.setBounds(25, 242, 185, 21);

            //---- GFIMS ----
            GFIMS.setText("\u00e0 la date de r\u00e8glement");
            GFIMS.setName("GFIMS");
            xTitledPanel1ContentContainer.add(GFIMS);
            GFIMS.setBounds(215, 242, 160, 21);

            //---- GFIMS_1 ----
            GFIMS_1.setText("\u00e0 la date d'\u00e9mission du titre");
            GFIMS_1.setName("GFIMS_1");
            xTitledPanel1ContentContainer.add(GFIMS_1);
            GFIMS_1.setBounds(405, 242, 225, 21);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 674, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 452, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }

    //---- buttonGroup1 ----
    buttonGroup1.add(GFIMS);
    buttonGroup1.add(GFIMS_1);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44_OBJ_45;
  private XRiTextField INDETB;
  private JLabel OBJ_45_OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_46_OBJ_47;
  private XRiTextField INDIND;
  private RiZoneSortie GFL01;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private RiZoneSortie GFL02;
  private RiZoneSortie OBJ_61_OBJ_61;
  private RiZoneSortie OBJ_74_OBJ_74;
  private RiZoneSortie OBJ_75_OBJ_75;
  private RiZoneSortie OBJ_88_OBJ_88;
  private RiZoneSortie OBJ_89_OBJ_89;
  private RiZoneSortie OBJ_72_OBJ_72;
  private RiZoneSortie OBJ_73_OBJ_73;
  private RiZoneSortie OBJ_86_OBJ_86;
  private RiZoneSortie OBJ_87_OBJ_87;
  private RiZoneSortie OBJ_100_OBJ_100;
  private RiZoneSortie OBJ_101_OBJ_101;
  private JLabel OBJ_63_OBJ_63;
  private JLabel OBJ_77_OBJ_77;
  private JLabel OBJ_91_OBJ_91;
  private JLabel OBJ_67_OBJ_67;
  private JLabel OBJ_70_OBJ_70;
  private JLabel OBJ_81_OBJ_81;
  private JLabel OBJ_84_OBJ_84;
  private JLabel OBJ_95_OBJ_95;
  private JLabel OBJ_98_OBJ_98;
  private JLabel OBJ_66_OBJ_66;
  private JLabel OBJ_80_OBJ_80;
  private JLabel OBJ_94_OBJ_94;
  private JLabel OBJ_69_OBJ_69;
  private JLabel OBJ_83_OBJ_83;
  private JLabel OBJ_97_OBJ_97;
  private XRiTextField GFJO1;
  private XRiTextField GFJO2;
  private XRiTextField GFJO3;
  private XRiTextField GFCOD1;
  private XRiTextField GFCOC1;
  private XRiTextField GFCOD2;
  private XRiTextField GFCOC2;
  private XRiTextField GFCOD3;
  private XRiTextField GFCOC3;
  private XRiCheckBox GFRGD1;
  private XRiCheckBox GFRGC1;
  private XRiCheckBox GFRGD2;
  private XRiCheckBox GFRGC2;
  private XRiCheckBox GFRGD3;
  private XRiCheckBox GFRGC3;
  private JLabel OBJ_68_OBJ_68;
  private JLabel OBJ_71_OBJ_71;
  private JLabel OBJ_82_OBJ_82;
  private JLabel OBJ_85_OBJ_85;
  private JLabel OBJ_96_OBJ_96;
  private JLabel OBJ_99_OBJ_99;
  private JComponent separator1;
  private JComponent separator2;
  private JComponent separator3;
  private JComponent separator4;
  private JComponent separator5;
  private JComponent separator6;
  private JLabel label1;
  private XRiRadioButton GFIMS;
  private XRiRadioButton GFIMS_1;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  private ButtonGroup buttonGroup1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
