
package ri.serien.libecranrpg.vcgm.VCGMSEFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGMSEFM_A1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGMSEFM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    SEACT.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    NCRI.setVisible(lexique.isTrue("98"));
    SENS.setVisible(lexique.isTrue("98"));
    OBJ_37.setVisible(lexique.isTrue("98"));
    OBJ_39.setVisible(lexique.isTrue("98"));
    // SEACT.setVisible( lexique.isPresent("SEACT"));
    // SEACT.setSelected(lexique.HostFieldGetData("SEACT").equalsIgnoreCase("OUI"));
    
    

    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (SEACT.isSelected())
    // lexique.HostFieldPutData("SEACT", 0, "OUI");
    // else
    // lexique.HostFieldPutData("SEACT", 0, "NON");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    pnlNord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    pnlSud = new SNPanelFond();
    pnlMenus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    pnlContenu = new SNPanelContenu();
    xTitledPanel1 = new JXTitledPanel();
    pnlDroits = new SNPanel();
    SEU002 = new XRiTextField();
    SEU001 = new XRiTextField();
    SEU003 = new XRiTextField();
    SEU004 = new XRiTextField();
    SEU006 = new XRiTextField();
    SEU005 = new XRiTextField();
    SEU007 = new XRiTextField();
    SEU008 = new XRiTextField();
    SEU010 = new XRiTextField();
    SEU009 = new XRiTextField();
    SEU013 = new XRiTextField();
    SEU012 = new XRiTextField();
    SEU011 = new XRiTextField();
    SEU017 = new XRiTextField();
    SEU015 = new XRiTextField();
    SEU016 = new XRiTextField();
    SEU014 = new XRiTextField();
    SEU020 = new XRiTextField();
    SEU019 = new XRiTextField();
    SEU018 = new XRiTextField();
    pnlSecurite = new SNPanel();
    SEACT = new XRiCheckBox();
    OBJ_37 = new JLabel();
    OBJ_39 = new JLabel();
    NCRI = new XRiTextField();
    SENS = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlNord ========
    {
      pnlNord.setName("pnlNord");
      pnlNord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("S\u00e9curit\u00e9 de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      pnlNord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      pnlNord.add(barre_tete);
    }
    add(pnlNord, BorderLayout.NORTH);

    //======== pnlSud ========
    {
      pnlSud.setName("pnlSud");
      pnlSud.setLayout(new BorderLayout());

      //======== pnlMenus ========
      {
        pnlMenus.setPreferredSize(new Dimension(170, 0));
        pnlMenus.setMinimumSize(new Dimension(170, 0));
        pnlMenus.setBackground(new Color(238, 239, 241));
        pnlMenus.setBorder(LineBorder.createGrayLineBorder());
        pnlMenus.setName("pnlMenus");
        pnlMenus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        pnlMenus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Tri");
              riSousMenu_bt6.setToolTipText("Tri par \u00e9tablissement ou code utilisateur");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        pnlMenus.add(scroll_droite, BorderLayout.NORTH);
      }
      pnlSud.add(pnlMenus, BorderLayout.LINE_END);

      //======== pnlContenu ========
      {
        pnlContenu.setPreferredSize(new Dimension(770, 380));
        pnlContenu.setBackground(new Color(239, 239, 222));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(null);

        //======== xTitledPanel1 ========
        {
          xTitledPanel1.setBorder(new DropShadowBorder());
          xTitledPanel1.setTitle("S\u00e9curit\u00e9");
          xTitledPanel1.setName("xTitledPanel1");
          Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

          //======== pnlDroits ========
          {
            pnlDroits.setBorder(new TitledBorder("Liste des utilisateurs ayant TOUS les droits sur CGM"));
            pnlDroits.setOpaque(false);
            pnlDroits.setName("pnlDroits");

            //---- SEU002 ----
            SEU002.setComponentPopupMenu(BTD);
            SEU002.setName("SEU002");

            //---- SEU001 ----
            SEU001.setComponentPopupMenu(BTD);
            SEU001.setName("SEU001");

            //---- SEU003 ----
            SEU003.setComponentPopupMenu(BTD);
            SEU003.setName("SEU003");

            //---- SEU004 ----
            SEU004.setComponentPopupMenu(BTD);
            SEU004.setName("SEU004");

            //---- SEU006 ----
            SEU006.setComponentPopupMenu(BTD);
            SEU006.setName("SEU006");

            //---- SEU005 ----
            SEU005.setComponentPopupMenu(BTD);
            SEU005.setName("SEU005");

            //---- SEU007 ----
            SEU007.setComponentPopupMenu(BTD);
            SEU007.setName("SEU007");

            //---- SEU008 ----
            SEU008.setComponentPopupMenu(BTD);
            SEU008.setName("SEU008");

            //---- SEU010 ----
            SEU010.setComponentPopupMenu(BTD);
            SEU010.setName("SEU010");

            //---- SEU009 ----
            SEU009.setComponentPopupMenu(BTD);
            SEU009.setName("SEU009");

            //---- SEU013 ----
            SEU013.setComponentPopupMenu(BTD);
            SEU013.setName("SEU013");

            //---- SEU012 ----
            SEU012.setComponentPopupMenu(BTD);
            SEU012.setName("SEU012");

            //---- SEU011 ----
            SEU011.setComponentPopupMenu(BTD);
            SEU011.setName("SEU011");

            //---- SEU017 ----
            SEU017.setComponentPopupMenu(BTD);
            SEU017.setName("SEU017");

            //---- SEU015 ----
            SEU015.setComponentPopupMenu(BTD);
            SEU015.setName("SEU015");

            //---- SEU016 ----
            SEU016.setComponentPopupMenu(BTD);
            SEU016.setName("SEU016");

            //---- SEU014 ----
            SEU014.setComponentPopupMenu(BTD);
            SEU014.setName("SEU014");

            //---- SEU020 ----
            SEU020.setComponentPopupMenu(BTD);
            SEU020.setName("SEU020");

            //---- SEU019 ----
            SEU019.setComponentPopupMenu(BTD);
            SEU019.setName("SEU019");

            //---- SEU018 ----
            SEU018.setComponentPopupMenu(BTD);
            SEU018.setName("SEU018");

            GroupLayout pnlDroitsLayout = new GroupLayout(pnlDroits);
            pnlDroits.setLayout(pnlDroitsLayout);
            pnlDroitsLayout.setHorizontalGroup(
              pnlDroitsLayout.createParallelGroup()
                .addGroup(pnlDroitsLayout.createSequentialGroup()
                  .addGap(16, 16, 16)
                  .addGroup(pnlDroitsLayout.createParallelGroup()
                    .addGroup(pnlDroitsLayout.createSequentialGroup()
                      .addComponent(SEU002, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addGap(25, 25, 25)
                      .addComponent(SEU001, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addGap(25, 25, 25)
                      .addComponent(SEU003, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addGap(25, 25, 25)
                      .addComponent(SEU004, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addGap(25, 25, 25)
                      .addComponent(SEU006, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlDroitsLayout.createSequentialGroup()
                      .addComponent(SEU005, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addGap(25, 25, 25)
                      .addComponent(SEU007, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addGap(25, 25, 25)
                      .addComponent(SEU008, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addGap(25, 25, 25)
                      .addComponent(SEU010, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addGap(25, 25, 25)
                      .addComponent(SEU009, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlDroitsLayout.createSequentialGroup()
                      .addComponent(SEU013, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addGap(25, 25, 25)
                      .addComponent(SEU012, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addGap(25, 25, 25)
                      .addComponent(SEU011, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addGap(25, 25, 25)
                      .addComponent(SEU017, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addGap(25, 25, 25)
                      .addComponent(SEU015, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlDroitsLayout.createSequentialGroup()
                      .addComponent(SEU016, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addGap(25, 25, 25)
                      .addComponent(SEU014, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addGap(25, 25, 25)
                      .addComponent(SEU020, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addGap(25, 25, 25)
                      .addComponent(SEU019, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addGap(25, 25, 25)
                      .addComponent(SEU018, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)))
                  .addContainerGap(23, Short.MAX_VALUE))
            );
            pnlDroitsLayout.setVerticalGroup(
              pnlDroitsLayout.createParallelGroup()
                .addGroup(pnlDroitsLayout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addGroup(pnlDroitsLayout.createParallelGroup()
                    .addComponent(SEU002, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SEU001, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SEU003, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SEU004, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SEU006, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addGroup(pnlDroitsLayout.createParallelGroup()
                    .addComponent(SEU005, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SEU007, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SEU008, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SEU010, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SEU009, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addGroup(pnlDroitsLayout.createParallelGroup()
                    .addComponent(SEU013, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SEU012, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SEU011, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SEU017, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SEU015, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addGroup(pnlDroitsLayout.createParallelGroup()
                    .addComponent(SEU016, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(pnlDroitsLayout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addGroup(pnlDroitsLayout.createParallelGroup()
                        .addComponent(SEU014, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(SEU020, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(SEU019, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(SEU018, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            );
          }

          //======== pnlSecurite ========
          {
            pnlSecurite.setOpaque(false);
            pnlSecurite.setName("pnlSecurite");

            //---- SEACT ----
            SEACT.setText("S\u00e9curit\u00e9 active");
            SEACT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SEACT.setName("SEACT");

            //---- OBJ_37 ----
            OBJ_37.setText("Num\u00e9ro de s\u00e9rie IBM");
            OBJ_37.setName("OBJ_37");

            //---- OBJ_39 ----
            OBJ_39.setText("Num\u00e9ro de contr\u00f4le RI");
            OBJ_39.setName("OBJ_39");

            //---- NCRI ----
            NCRI.setComponentPopupMenu(BTD);
            NCRI.setName("NCRI");

            //---- SENS ----
            SENS.setComponentPopupMenu(BTD);
            SENS.setName("SENS");

            GroupLayout pnlSecuriteLayout = new GroupLayout(pnlSecurite);
            pnlSecurite.setLayout(pnlSecuriteLayout);
            pnlSecuriteLayout.setHorizontalGroup(
              pnlSecuriteLayout.createParallelGroup()
                .addGroup(pnlSecuriteLayout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addGroup(pnlSecuriteLayout.createParallelGroup()
                    .addComponent(SEACT, GroupLayout.PREFERRED_SIZE, 177, GroupLayout.PREFERRED_SIZE)
                    .addGroup(pnlSecuriteLayout.createSequentialGroup()
                      .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 148, GroupLayout.PREFERRED_SIZE)
                      .addGap(32, 32, 32)
                      .addComponent(SENS, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlSecuriteLayout.createSequentialGroup()
                      .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 156, GroupLayout.PREFERRED_SIZE)
                      .addGap(24, 24, 24)
                      .addComponent(NCRI, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE))))
            );
            pnlSecuriteLayout.setVerticalGroup(
              pnlSecuriteLayout.createParallelGroup()
                .addGroup(pnlSecuriteLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(SEACT, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addGap(10, 10, 10)
                  .addGroup(pnlSecuriteLayout.createParallelGroup()
                    .addGroup(pnlSecuriteLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(SENS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(7, 7, 7)
                  .addGroup(pnlSecuriteLayout.createParallelGroup()
                    .addGroup(pnlSecuriteLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(NCRI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }

          GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
          xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
          xTitledPanel1ContentContainerLayout.setHorizontalGroup(
            xTitledPanel1ContentContainerLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                  .addComponent(pnlSecurite, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(pnlDroits, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
          );
          xTitledPanel1ContentContainerLayout.setVerticalGroup(
            xTitledPanel1ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(pnlSecurite, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(pnlDroits, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        pnlContenu.add(xTitledPanel1);
        xTitledPanel1.setBounds(13, 14, 744, xTitledPanel1.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < pnlContenu.getComponentCount(); i++) {
            Rectangle bounds = pnlContenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = pnlContenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          pnlContenu.setMinimumSize(preferredSize);
          pnlContenu.setPreferredSize(preferredSize);
        }
      }
      pnlSud.add(pnlContenu, BorderLayout.CENTER);
    }
    add(pnlSud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel pnlNord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private SNPanelFond pnlSud;
  private JPanel pnlMenus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelContenu pnlContenu;
  private JXTitledPanel xTitledPanel1;
  private SNPanel pnlDroits;
  private XRiTextField SEU002;
  private XRiTextField SEU001;
  private XRiTextField SEU003;
  private XRiTextField SEU004;
  private XRiTextField SEU006;
  private XRiTextField SEU005;
  private XRiTextField SEU007;
  private XRiTextField SEU008;
  private XRiTextField SEU010;
  private XRiTextField SEU009;
  private XRiTextField SEU013;
  private XRiTextField SEU012;
  private XRiTextField SEU011;
  private XRiTextField SEU017;
  private XRiTextField SEU015;
  private XRiTextField SEU016;
  private XRiTextField SEU014;
  private XRiTextField SEU020;
  private XRiTextField SEU019;
  private XRiTextField SEU018;
  private SNPanel pnlSecurite;
  private XRiCheckBox SEACT;
  private JLabel OBJ_37;
  private JLabel OBJ_39;
  private XRiTextField NCRI;
  private XRiTextField SENS;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
