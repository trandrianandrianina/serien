
package ri.serien.libecranrpg.vcgm.VCGM23FM;
// Nom Fichier: i_VCGM23FM_FMTB1_FMTF1_353.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM23FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VCGM23FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDefaultButton(bouton_valider);
    // setDialog(true);
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    LIBGES.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBGES@")).trim());
    LIBCOA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBCOA@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    gererLesErreurs("19");
    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  @TITRE@"));
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_46_OBJ_46 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_48_OBJ_48 = new JLabel();
    INDCOD = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_63_OBJ_63 = new JLabel();
    ABMTT = new XRiTextField();
    OBJ_65_OBJ_65 = new JLabel();
    ABDEBX = new XRiCalendrier();
    OBJ_67_OBJ_67 = new JLabel();
    ABFINX = new XRiCalendrier();
    OBJ_69_OBJ_69 = new JLabel();
    ABGESX = new XRiTextField();
    LIBGES = new RiZoneSortie();
    OBJ_72_OBJ_72 = new JLabel();
    ABLGE = new XRiTextField();
    OBJ_56_OBJ_56 = new JLabel();
    ABSAN = new XRiTextField();
    OBJ_57_OBJ_57 = new JLabel();
    ABACT = new XRiTextField();
    OBJ_58_OBJ_58 = new JLabel();
    ABNAT = new XRiTextField();
    OBJ_77_OBJ_77 = new JLabel();
    ABCOAX = new XRiTextField();
    LIBCOA = new RiZoneSortie();
    OBJ_80_OBJ_80 = new JLabel();
    ABLAB = new XRiTextField();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_89_OBJ_89 = new JLabel();
    ABLIB = new XRiTextField();
    OBJ_54_OBJ_54 = new JLabel();
    ABCL1 = new XRiTextField();
    LIBCPT = new XRiTextField();
    ABNCA = new XRiTextField();
    ABNCGX = new XRiTextField();
    OBJ_60_OBJ_60 = new JLabel();

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_19 ----
      OBJ_19.setText("Invite");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Fiche abonnement");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 30));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_46_OBJ_46 ----
          OBJ_46_OBJ_46.setText("Soci\u00e9t\u00e9");
          OBJ_46_OBJ_46.setName("OBJ_46_OBJ_46");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_48_OBJ_48 ----
          OBJ_48_OBJ_48.setText("Code");
          OBJ_48_OBJ_48.setName("OBJ_48_OBJ_48");

          //---- INDCOD ----
          INDCOD.setComponentPopupMenu(BTD);
          INDCOD.setName("INDCOD");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_46_OBJ_46, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(OBJ_48_OBJ_48, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19)
                .addComponent(INDCOD, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_46_OBJ_46, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_48_OBJ_48, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDCOD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("D\u00e9tail des \u00e9critures");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(700, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setTitle("Montant - p\u00e9riode d'abonnement - comptes");
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- OBJ_63_OBJ_63 ----
            OBJ_63_OBJ_63.setText("Montant");
            OBJ_63_OBJ_63.setName("OBJ_63_OBJ_63");
            xTitledPanel1ContentContainer.add(OBJ_63_OBJ_63);
            OBJ_63_OBJ_63.setBounds(35, 24, 79, 20);

            //---- ABMTT ----
            ABMTT.setComponentPopupMenu(BTD);
            ABMTT.setName("ABMTT");
            xTitledPanel1ContentContainer.add(ABMTT);
            ABMTT.setBounds(170, 20, 108, ABMTT.getPreferredSize().height);

            //---- OBJ_65_OBJ_65 ----
            OBJ_65_OBJ_65.setText("entre le");
            OBJ_65_OBJ_65.setName("OBJ_65_OBJ_65");
            xTitledPanel1ContentContainer.add(OBJ_65_OBJ_65);
            OBJ_65_OBJ_65.setBounds(320, 24, 48, 20);

            //---- ABDEBX ----
            ABDEBX.setComponentPopupMenu(BTD);
            ABDEBX.setTypeSaisie(6);
            ABDEBX.setName("ABDEBX");
            xTitledPanel1ContentContainer.add(ABDEBX);
            ABDEBX.setBounds(375, 20, 90, ABDEBX.getPreferredSize().height);

            //---- OBJ_67_OBJ_67 ----
            OBJ_67_OBJ_67.setText("et le");
            OBJ_67_OBJ_67.setName("OBJ_67_OBJ_67");
            xTitledPanel1ContentContainer.add(OBJ_67_OBJ_67);
            OBJ_67_OBJ_67.setBounds(475, 24, 30, 20);

            //---- ABFINX ----
            ABFINX.setComponentPopupMenu(BTD);
            ABFINX.setTypeSaisie(6);
            ABFINX.setName("ABFINX");
            xTitledPanel1ContentContainer.add(ABFINX);
            ABFINX.setBounds(515, 20, 90, ABFINX.getPreferredSize().height);

            //---- OBJ_69_OBJ_69 ----
            OBJ_69_OBJ_69.setText("Gestion");
            OBJ_69_OBJ_69.setName("OBJ_69_OBJ_69");
            xTitledPanel1ContentContainer.add(OBJ_69_OBJ_69);
            OBJ_69_OBJ_69.setBounds(35, 84, 50, 20);

            //---- ABGESX ----
            ABGESX.setComponentPopupMenu(BTD);
            ABGESX.setName("ABGESX");
            xTitledPanel1ContentContainer.add(ABGESX);
            ABGESX.setBounds(170, 80, 60, ABGESX.getPreferredSize().height);

            //---- LIBGES ----
            LIBGES.setText("@LIBGES@");
            LIBGES.setForeground(Color.black);
            LIBGES.setName("LIBGES");
            xTitledPanel1ContentContainer.add(LIBGES);
            LIBGES.setBounds(250, 85, 279, LIBGES.getPreferredSize().height);

            //---- OBJ_72_OBJ_72 ----
            OBJ_72_OBJ_72.setText("Libell\u00e9 de l'\u00e9criture");
            OBJ_72_OBJ_72.setName("OBJ_72_OBJ_72");
            xTitledPanel1ContentContainer.add(OBJ_72_OBJ_72);
            OBJ_72_OBJ_72.setBounds(35, 114, 115, 20);

            //---- ABLGE ----
            ABLGE.setName("ABLGE");
            xTitledPanel1ContentContainer.add(ABLGE);
            ABLGE.setBounds(250, 110, 279, ABLGE.getPreferredSize().height);

            //---- OBJ_56_OBJ_56 ----
            OBJ_56_OBJ_56.setText("Section");
            OBJ_56_OBJ_56.setName("OBJ_56_OBJ_56");
            xTitledPanel1ContentContainer.add(OBJ_56_OBJ_56);
            OBJ_56_OBJ_56.setBounds(35, 144, 48, 20);

            //---- ABSAN ----
            ABSAN.setComponentPopupMenu(BTD);
            ABSAN.setName("ABSAN");
            xTitledPanel1ContentContainer.add(ABSAN);
            ABSAN.setBounds(170, 140, 50, ABSAN.getPreferredSize().height);

            //---- OBJ_57_OBJ_57 ----
            OBJ_57_OBJ_57.setText("Affaire");
            OBJ_57_OBJ_57.setName("OBJ_57_OBJ_57");
            xTitledPanel1ContentContainer.add(OBJ_57_OBJ_57);
            OBJ_57_OBJ_57.setBounds(250, 144, 42, 20);

            //---- ABACT ----
            ABACT.setComponentPopupMenu(BTD);
            ABACT.setName("ABACT");
            xTitledPanel1ContentContainer.add(ABACT);
            ABACT.setBounds(318, 140, 60, ABACT.getPreferredSize().height);

            //---- OBJ_58_OBJ_58 ----
            OBJ_58_OBJ_58.setText("Nature");
            OBJ_58_OBJ_58.setName("OBJ_58_OBJ_58");
            xTitledPanel1ContentContainer.add(OBJ_58_OBJ_58);
            OBJ_58_OBJ_58.setBounds(420, 144, 43, 20);

            //---- ABNAT ----
            ABNAT.setComponentPopupMenu(BTD);
            ABNAT.setName("ABNAT");
            xTitledPanel1ContentContainer.add(ABNAT);
            ABNAT.setBounds(476, 140, 52, ABNAT.getPreferredSize().height);

            //---- OBJ_77_OBJ_77 ----
            OBJ_77_OBJ_77.setText("Abonnement");
            OBJ_77_OBJ_77.setName("OBJ_77_OBJ_77");
            xTitledPanel1ContentContainer.add(OBJ_77_OBJ_77);
            OBJ_77_OBJ_77.setBounds(35, 219, 79, 20);

            //---- ABCOAX ----
            ABCOAX.setComponentPopupMenu(BTD);
            ABCOAX.setName("ABCOAX");
            xTitledPanel1ContentContainer.add(ABCOAX);
            ABCOAX.setBounds(170, 215, 70, ABCOAX.getPreferredSize().height);

            //---- LIBCOA ----
            LIBCOA.setText("@LIBCOA@");
            LIBCOA.setForeground(Color.black);
            LIBCOA.setName("LIBCOA");
            xTitledPanel1ContentContainer.add(LIBCOA);
            LIBCOA.setBounds(250, 220, 279, LIBCOA.getPreferredSize().height);

            //---- OBJ_80_OBJ_80 ----
            OBJ_80_OBJ_80.setText("Libell\u00e9 de l'\u00e9criture");
            OBJ_80_OBJ_80.setName("OBJ_80_OBJ_80");
            xTitledPanel1ContentContainer.add(OBJ_80_OBJ_80);
            OBJ_80_OBJ_80.setBounds(35, 249, 135, 20);

            //---- ABLAB ----
            ABLAB.setName("ABLAB");
            xTitledPanel1ContentContainer.add(ABLAB);
            ABLAB.setBounds(250, 245, 279, ABLAB.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setTitle("Identification");
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //---- OBJ_89_OBJ_89 ----
            OBJ_89_OBJ_89.setText("Libell\u00e9");
            OBJ_89_OBJ_89.setName("OBJ_89_OBJ_89");
            xTitledPanel2ContentContainer.add(OBJ_89_OBJ_89);
            OBJ_89_OBJ_89.setBounds(35, 15, 43, 20);

            //---- ABLIB ----
            ABLIB.setComponentPopupMenu(BTD);
            ABLIB.setName("ABLIB");
            xTitledPanel2ContentContainer.add(ABLIB);
            ABLIB.setBounds(85, 10, 310, ABLIB.getPreferredSize().height);

            //---- OBJ_54_OBJ_54 ----
            OBJ_54_OBJ_54.setText("Cl\u00e9");
            OBJ_54_OBJ_54.setName("OBJ_54_OBJ_54");
            xTitledPanel2ContentContainer.add(OBJ_54_OBJ_54);
            OBJ_54_OBJ_54.setBounds(405, 15, 24, 20);

            //---- ABCL1 ----
            ABCL1.setComponentPopupMenu(BTD);
            ABCL1.setName("ABCL1");
            xTitledPanel2ContentContainer.add(ABCL1);
            ABCL1.setBounds(435, 10, 160, ABCL1.getPreferredSize().height);

            //---- LIBCPT ----
            LIBCPT.setName("LIBCPT");
            xTitledPanel2ContentContainer.add(LIBCPT);
            LIBCPT.setBounds(220, 51, 310, LIBCPT.getPreferredSize().height);

            //---- ABNCA ----
            ABNCA.setComponentPopupMenu(BTD);
            ABNCA.setName("ABNCA");
            xTitledPanel2ContentContainer.add(ABNCA);
            ABNCA.setBounds(150, 51, 60, ABNCA.getPreferredSize().height);

            //---- ABNCGX ----
            ABNCGX.setComponentPopupMenu(BTD);
            ABNCGX.setName("ABNCGX");
            xTitledPanel2ContentContainer.add(ABNCGX);
            ABNCGX.setBounds(85, 51, 60, ABNCGX.getPreferredSize().height);

            //---- OBJ_60_OBJ_60 ----
            OBJ_60_OBJ_60.setText("Tiers");
            OBJ_60_OBJ_60.setName("OBJ_60_OBJ_60");
            xTitledPanel2ContentContainer.add(OBJ_60_OBJ_60);
            OBJ_60_OBJ_60.setBounds(35, 55, 34, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                  .addComponent(xTitledPanel2, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 674, Short.MAX_VALUE)
                  .addComponent(xTitledPanel1, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 674, Short.MAX_VALUE))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 129, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 330, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_46_OBJ_46;
  private XRiTextField INDETB;
  private JLabel OBJ_48_OBJ_48;
  private XRiTextField INDCOD;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_63_OBJ_63;
  private XRiTextField ABMTT;
  private JLabel OBJ_65_OBJ_65;
  private XRiCalendrier ABDEBX;
  private JLabel OBJ_67_OBJ_67;
  private XRiCalendrier ABFINX;
  private JLabel OBJ_69_OBJ_69;
  private XRiTextField ABGESX;
  private RiZoneSortie LIBGES;
  private JLabel OBJ_72_OBJ_72;
  private XRiTextField ABLGE;
  private JLabel OBJ_56_OBJ_56;
  private XRiTextField ABSAN;
  private JLabel OBJ_57_OBJ_57;
  private XRiTextField ABACT;
  private JLabel OBJ_58_OBJ_58;
  private XRiTextField ABNAT;
  private JLabel OBJ_77_OBJ_77;
  private XRiTextField ABCOAX;
  private RiZoneSortie LIBCOA;
  private JLabel OBJ_80_OBJ_80;
  private XRiTextField ABLAB;
  private JXTitledPanel xTitledPanel2;
  private JLabel OBJ_89_OBJ_89;
  private XRiTextField ABLIB;
  private JLabel OBJ_54_OBJ_54;
  private XRiTextField ABCL1;
  private XRiTextField LIBCPT;
  private XRiTextField ABNCA;
  private XRiTextField ABNCGX;
  private JLabel OBJ_60_OBJ_60;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
