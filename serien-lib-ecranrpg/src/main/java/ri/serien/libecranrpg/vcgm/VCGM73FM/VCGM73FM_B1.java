
package ri.serien.libecranrpg.vcgm.VCGM73FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGM73FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM73FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLLITI@")).trim());
    OBJ_47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMT1@")).trim());
    OBJ_54.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1DEV@")).trim());
    NRLV01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NRLV01@")).trim());
    NRLV02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NRLV02@")).trim());
    NRLV03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NRLV03@")).trim());
    NRLV04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NRLV04@")).trim());
    LDR01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LDR01@ @DDR01@")).trim());
    LDR02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LDR02@ @DDR02@")).trim());
    LDR03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LDR03@ @DDR03@")).trim());
    LDR04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LDR04@ @DDR04@")).trim());
    OBJ_77.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBSAN@")).trim());
    OBJ_78.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAFF@")).trim());
    OBJ_79.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBNAT@")).trim());
    TTVA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TTVA@")).trim());
    LIBTYF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBTYF@")).trim());
    OBJ_84.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAX1@")).trim());
    OBJ_88.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAX2@")).trim());
    OBJ_94.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAX3@")).trim());
    OBJ_85.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAX4@")).trim());
    OBJ_89.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAX5@")).trim());
    OBJ_95.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAX6@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    /*riSousMenu_bt7.setVisible( lexique.isTrue("82"));
    riSousMenu_bt10.setEnabled( lexique.isTrue("82"));
    riSousMenu_bt6.setEnabled( lexique.isTrue("85"));
    riSousMenu_bt9.setEnabled( lexique.isTrue("74"));
    riSousMenu15.setEnabled(!interpreteurD.analyseExpression("@L1PCE@").trim().equalsIgnoreCase(""));
    riSousMenu_bt8.setEnabled( lexique.isTrue("74"));*/
    riSousMenu7.setEnabled(lexique.isTrue("82"));
    riSousMenu10.setEnabled(lexique.isTrue("82"));
    riSousMenu6.setEnabled(lexique.isTrue("85"));
    riSousMenu9.setEnabled(lexique.isTrue("74"));
    riSousMenu15.setEnabled(!interpreteurD.analyseExpression("@L1PCE@").trim().equalsIgnoreCase(""));
    riSousMenu8.setEnabled(lexique.isTrue("74"));
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    boolean indic = false, indic2 = false;
    
    xTitledPanel5.setVisible(lexique.isTrue("72"));
    OBJ_34.setVisible(lexique.isPresent("L1NCPX"));
    OBJ_36.setVisible(lexique.isPresent("L1RFC"));
    OBJ_31.setVisible(lexique.isPresent("WLIBB1"));
    OBJ_90.setVisible(lexique.isTrue("74"));
    NJF.setVisible(lexique.isTrue("74"));
    L1PCE.setVisible(lexique.isTrue("N70"));
    OBJ_40.setEnabled(L1PCE.isVisible());
    L1DVBX.setVisible(lexique.isTrue("80"));
    OBJ_38.setVisible(lexique.isTrue("80"));
    L1RLV.setVisible(lexique.isTrue("73"));
    OBJ_42.setVisible(L1RLV.isVisible());
    L1MTT.setVisible(lexique.isTrue("N65"));
    OBJ_47.setVisible(L1MTT.isVisible());
    L1MTD.setVisible(lexique.isTrue("75"));
    OBJ_52.setVisible(L1MTD.isVisible());
    OBJ_54.setVisible(L1MTD.isVisible());
    L1QTE.setVisible(lexique.isTrue("76"));
    OBJ_50.setVisible(L1QTE.isVisible());
    WTXD.setVisible(lexique.isTrue("75"));
    OBJ_55.setVisible(WTXD.isVisible());
    indic = lexique.isTrue("72");
    indic2 = lexique.isTrue("(72) AND (82)");
    OBJ_61.setVisible(indic);
    OBJ_73.setVisible(indic);
    OBJ_75.setVisible(indic);
    L1NAT.setVisible(indic);
    L1SAN.setVisible(indic);
    L1ACT.setVisible(indic);
    OBJ_79.setVisible(indic2);
    OBJ_78.setVisible(indic2);
    OBJ_77.setVisible(indic2);
    // Détail des échéances
    L1AA1.setVisible(indic);
    L1AA2.setVisible(indic);
    L1AA3.setVisible(indic);
    L1AA4.setVisible(indic);
    L1AA5.setVisible(indic);
    L1AA6.setVisible(indic);
    OBJ_95.setVisible(indic2);
    OBJ_94.setVisible(indic2);
    OBJ_89.setVisible(indic2);
    OBJ_88.setVisible(indic2);
    OBJ_85.setVisible(indic2);
    OBJ_84.setVisible(indic2);
    LIBTYF.setVisible(!lexique.HostFieldGetData("WAFFR").equalsIgnoreCase("R"));
    LIBTYF.setVisible(lexique.isTrue("85"));
    label20.setVisible(lexique.isTrue("85"));
    xTitledPanel2.setVisible(indic);
    xTitledPanel3.setVisible(indic);
    xTitledPanel4.setVisible(lexique.isTrue("(74) AND (N65)"));
    
    NRLV01.setVisible((lexique.isTrue("74")) && (!lexique.HostFieldGetData("NRLV01").trim().equalsIgnoreCase("")));
    NRLV02.setVisible((lexique.isTrue("74")) && (!lexique.HostFieldGetData("NRLV02").trim().equalsIgnoreCase("")));
    NRLV03.setVisible((lexique.isTrue("74")) && (!lexique.HostFieldGetData("NRLV03").trim().equalsIgnoreCase("")));
    NRLV04.setVisible((lexique.isTrue("74")) && (!lexique.HostFieldGetData("NRLV04").trim().equalsIgnoreCase("")));
    
    if (lexique.HostFieldGetData("L1SNS").contains("C")) {
      OBJ_49.setText("Crédit");
    }
    else {
      OBJ_49.setText("Débit");
    }
    
    // Taille de la popup
    int largeurPop = 875;
    int hauteurPop = 755;
    
    if (!xTitledPanel1.isVisible()) {
      hauteurPop = hauteurPop - 110;
    }
    if (!xTitledPanel2.isVisible()) {
      hauteurPop = hauteurPop - 120;
    }
    if (!xTitledPanel3.isVisible()) {
      hauteurPop = hauteurPop - 100;
    }
    if (!xTitledPanel4.isVisible()) {
      hauteurPop = hauteurPop - 120;
    }
    
    this.setPreferredSize(new Dimension(largeurPop, hauteurPop));
    this.repaint();
    // fin taille popup
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Compte - @WLIB@"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(20, 40);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(6, 79);
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(6, 1);
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19");
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void riSousMenu_bt13ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(5, 78);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void riSousMenu_bt_dupliActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F17");
  }

  private void riSousMenu_bt16ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_del = new RiSousMenu();
    riSousMenu_bt_del = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riSousMenu13 = new RiSousMenu();
    riSousMenu_bt13 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    riSousMenu16 = new RiSousMenu();
    riSousMenu_bt16 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel3 = new JPanel();
    WLIBB1 = new XRiTextField();
    OBJ_36 = new JLabel();
    OBJ_31 = new JLabel();
    OBJ_40 = new JLabel();
    L1RFC = new XRiTextField();
    OBJ_28 = new JLabel();
    OBJ_38 = new JLabel();
    OBJ_34 = new JLabel();
    L1DVBX = new XRiCalendrier();
    OBJ_42 = new JLabel();
    L1PCE = new XRiTextField();
    L1RLV = new XRiTextField();
    L1NCPX = new XRiTextField();
    L1NLI = new XRiTextField();
    L1CLB = new XRiTextField();
    label1 = new JLabel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_47 = new JLabel();
    L1MTT = new XRiTextField();
    L1MTD = new XRiTextField();
    OBJ_54 = new RiZoneSortie();
    OBJ_52 = new JLabel();
    OBJ_50 = new JLabel();
    OBJ_55 = new JLabel();
    L1QTE = new XRiTextField();
    WTXD = new XRiTextField();
    xTitledPanel4 = new JXTitledPanel();
    OBJ_90 = new JLabel();
    NJF = new XRiTextField();
    NRLV01 = new RiZoneSortie();
    NRLV02 = new RiZoneSortie();
    NRLV03 = new RiZoneSortie();
    NRLV04 = new RiZoneSortie();
    NIV01 = new XRiTextField();
    NIV02 = new XRiTextField();
    NIV03 = new XRiTextField();
    NIV04 = new XRiTextField();
    MTT01 = new XRiTextField();
    MTT02 = new XRiTextField();
    MTT03 = new XRiTextField();
    MTT04 = new XRiTextField();
    ECH01 = new XRiTextField();
    ECH02 = new XRiTextField();
    ECH03 = new XRiTextField();
    ECH04 = new XRiTextField();
    NBJ01 = new XRiTextField();
    NBJ02 = new XRiTextField();
    NBJ03 = new XRiTextField();
    NBJ04 = new XRiTextField();
    RGL01 = new XRiTextField();
    RGL02 = new XRiTextField();
    RGL03 = new XRiTextField();
    RGL04 = new XRiTextField();
    PRI01 = new XRiTextField();
    PRI02 = new XRiTextField();
    PRI03 = new XRiTextField();
    PRI04 = new XRiTextField();
    label14 = new JLabel();
    label15 = new JLabel();
    label16 = new JLabel();
    label17 = new JLabel();
    label18 = new JLabel();
    label19 = new JLabel();
    LDR01 = new JLabel();
    LDR02 = new JLabel();
    LDR03 = new JLabel();
    LDR04 = new JLabel();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_77 = new RiZoneSortie();
    OBJ_78 = new RiZoneSortie();
    OBJ_79 = new RiZoneSortie();
    OBJ_75 = new JLabel();
    OBJ_73 = new JLabel();
    L1ACT = new XRiTextField();
    L1SAN = new XRiTextField();
    L1NAT = new XRiTextField();
    OBJ_61 = new JLabel();
    OBJ_74 = new JLabel();
    xTitledPanel5 = new JXTitledPanel();
    OBJ_48 = new JLabel();
    L1IN6 = new XRiTextField();
    TTVA = new RiZoneSortie();
    OBJ_51 = new JLabel();
    L1AFAL = new XRiTextField();
    OBJ_53 = new JLabel();
    xTitledPanel3 = new JXTitledPanel();
    LIBTYF = new RiZoneSortie();
    L1AA1 = new XRiTextField();
    L1AA4 = new XRiTextField();
    L1AA2 = new XRiTextField();
    L1AA5 = new XRiTextField();
    L1AA3 = new XRiTextField();
    L1AA6 = new XRiTextField();
    OBJ_84 = new RiZoneSortie();
    OBJ_88 = new RiZoneSortie();
    OBJ_94 = new RiZoneSortie();
    OBJ_85 = new RiZoneSortie();
    OBJ_89 = new RiZoneSortie();
    OBJ_95 = new RiZoneSortie();
    label20 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_23 = new JMenuItem();
    OBJ_22 = new JMenuItem();
    OBJ_49 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(875, 755));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("En cours");
            riSousMenu_bt_consult.setToolTipText("En cours");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Historique");
            riSousMenu_bt_modif.setToolTipText("Historique");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Justification");
            riSousMenu_bt_crea.setToolTipText("Justification");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Lettrage d'\u00e9critures");
            riSousMenu_bt_suppr.setToolTipText("Lettrage d'\u00e9critures");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_del ========
          {
            riSousMenuF_del.setName("riSousMenuF_del");

            //---- riSousMenu_bt_del ----
            riSousMenu_bt_del.setText("D\u00e9lettrage d'\u00e9critures");
            riSousMenu_bt_del.setToolTipText("D\u00e9lettrage d'\u00e9critures");
            riSousMenu_bt_del.setName("riSousMenu_bt_del");
            riSousMenu_bt_del.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt_dupliActionPerformed(e);
              }
            });
            riSousMenuF_del.add(riSousMenu_bt_del);
          }
          menus_haut.add(riSousMenuF_del);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Type de frais");
            riSousMenu_bt6.setToolTipText("Type de frais");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("G\u00e9n\u00e9ration d'un litige");
            riSousMenu_bt7.setToolTipText("G\u00e9n\u00e9ration d'un litige");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);

          //======== riSousMenu8 ========
          {
            riSousMenu8.setName("riSousMenu8");

            //---- riSousMenu_bt8 ----
            riSousMenu_bt8.setText("Acc\u00e8s aux \u00e9ch\u00e9ances");
            riSousMenu_bt8.setToolTipText("Acc\u00e8s aux \u00e9ch\u00e9ances");
            riSousMenu_bt8.setName("riSousMenu_bt8");
            riSousMenu_bt8.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt8ActionPerformed(e);
              }
            });
            riSousMenu8.add(riSousMenu_bt8);
          }
          menus_haut.add(riSousMenu8);

          //======== riSousMenu9 ========
          {
            riSousMenu9.setName("riSousMenu9");

            //---- riSousMenu_bt9 ----
            riSousMenu_bt9.setText("Autres \u00e9ch\u00e9ances");
            riSousMenu_bt9.setToolTipText("Autres \u00e9ch\u00e9ances");
            riSousMenu_bt9.setName("riSousMenu_bt9");
            riSousMenu_bt9.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt9ActionPerformed(e);
              }
            });
            riSousMenu9.add(riSousMenu_bt9);
          }
          menus_haut.add(riSousMenu9);

          //======== riSousMenu10 ========
          {
            riSousMenu10.setName("riSousMenu10");

            //---- riSousMenu_bt10 ----
            riSousMenu_bt10.setText("Visualisation relances");
            riSousMenu_bt10.setToolTipText("Visualisation des relances");
            riSousMenu_bt10.setName("riSousMenu_bt10");
            riSousMenu_bt10.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt10ActionPerformed(e);
              }
            });
            riSousMenu10.add(riSousMenu_bt10);
          }
          menus_haut.add(riSousMenu10);

          //======== riSousMenu11 ========
          {
            riSousMenu11.setName("riSousMenu11");

            //---- riSousMenu_bt11 ----
            riSousMenu_bt11.setText("Affichage en devise");
            riSousMenu_bt11.setToolTipText("Affichage en devise");
            riSousMenu_bt11.setName("riSousMenu_bt11");
            riSousMenu_bt11.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt11ActionPerformed(e);
              }
            });
            riSousMenu11.add(riSousMenu_bt11);
          }
          menus_haut.add(riSousMenu11);

          //======== riSousMenu12 ========
          {
            riSousMenu12.setName("riSousMenu12");

            //---- riSousMenu_bt12 ----
            riSousMenu_bt12.setText("Plan comptable");
            riSousMenu_bt12.setToolTipText("Plan comptable");
            riSousMenu_bt12.setName("riSousMenu_bt12");
            riSousMenu_bt12.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt12ActionPerformed(e);
              }
            });
            riSousMenu12.add(riSousMenu_bt12);
          }
          menus_haut.add(riSousMenu12);

          //======== riSousMenu13 ========
          {
            riSousMenu13.setName("riSousMenu13");

            //---- riSousMenu_bt13 ----
            riSousMenu_bt13.setText("Fiche de r\u00e9vision");
            riSousMenu_bt13.setToolTipText("Fiche de r\u00e9vision");
            riSousMenu_bt13.setName("riSousMenu_bt13");
            riSousMenu_bt13.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt13ActionPerformed(e);
              }
            });
            riSousMenu13.add(riSousMenu_bt13);
          }
          menus_haut.add(riSousMenu13);

          //======== riMenu3 ========
          {
            riMenu3.setName("riMenu3");

            //---- riMenu_bt3 ----
            riMenu_bt3.setText("Outils");
            riMenu_bt3.setName("riMenu_bt3");
            riMenu3.add(riMenu_bt3);
          }
          menus_haut.add(riMenu3);

          //======== riSousMenu14 ========
          {
            riSousMenu14.setName("riSousMenu14");

            //---- riSousMenu_bt14 ----
            riSousMenu_bt14.setText("Bloc-notes");
            riSousMenu_bt14.setToolTipText("Bloc-notes");
            riSousMenu_bt14.setName("riSousMenu_bt14");
            riSousMenu_bt14.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt14ActionPerformed(e);
              }
            });
            riSousMenu14.add(riSousMenu_bt14);
          }
          menus_haut.add(riSousMenu14);

          //======== riSousMenu15 ========
          {
            riSousMenu15.setName("riSousMenu15");

            //---- riSousMenu_bt15 ----
            riSousMenu_bt15.setText("Documents li\u00e9s \u00e9criture");
            riSousMenu_bt15.setToolTipText("Documents li\u00e9s \u00e0 l'\u00e9criture");
            riSousMenu_bt15.setName("riSousMenu_bt15");
            riSousMenu_bt15.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt15ActionPerformed(e);
              }
            });
            riSousMenu15.add(riSousMenu_bt15);
          }
          menus_haut.add(riSousMenu15);

          //======== riSousMenu16 ========
          {
            riSousMenu16.setName("riSousMenu16");

            //---- riSousMenu_bt16 ----
            riSousMenu_bt16.setText("Lien URL");
            riSousMenu_bt16.setToolTipText("Lien URL");
            riSousMenu_bt16.setName("riSousMenu_bt16");
            riSousMenu_bt16.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt16ActionPerformed(e);
              }
            });
            riSousMenu16.add(riSousMenu_bt16);
          }
          menus_haut.add(riSousMenu16);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel3 ========
        {
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);

          //---- WLIBB1 ----
          WLIBB1.setName("WLIBB1");
          panel3.add(WLIBB1);
          WLIBB1.setBounds(160, 35, 364, WLIBB1.getPreferredSize().height);

          //---- OBJ_36 ----
          OBJ_36.setText("R\u00e9f\u00e9rence classement");
          OBJ_36.setName("OBJ_36");
          panel3.add(OBJ_36);
          OBJ_36.setBounds(10, 65, 138, 28);

          //---- OBJ_31 ----
          OBJ_31.setText("Libell\u00e9 \u00e9criture");
          OBJ_31.setName("OBJ_31");
          panel3.add(OBJ_31);
          OBJ_31.setBounds(10, 35, 109, 28);

          //---- OBJ_40 ----
          OBJ_40.setText("Num\u00e9ro de pi\u00e8ce");
          OBJ_40.setName("OBJ_40");
          panel3.add(OBJ_40);
          OBJ_40.setBounds(10, 95, 108, 28);

          //---- L1RFC ----
          L1RFC.setName("L1RFC");
          panel3.add(L1RFC);
          L1RFC.setBounds(160, 65, 110, L1RFC.getPreferredSize().height);

          //---- OBJ_28 ----
          OBJ_28.setText("Num\u00e9ro de ligne");
          OBJ_28.setName("OBJ_28");
          panel3.add(OBJ_28);
          OBJ_28.setBounds(10, 5, 102, 28);

          //---- OBJ_38 ----
          OBJ_38.setText("Date de valeur");
          OBJ_38.setName("OBJ_38");
          panel3.add(OBJ_38);
          OBJ_38.setBounds(470, 65, 91, 28);

          //---- OBJ_34 ----
          OBJ_34.setText("Contre partie");
          OBJ_34.setName("OBJ_34");
          panel3.add(OBJ_34);
          OBJ_34.setBounds(295, 65, 79, 28);

          //---- L1DVBX ----
          L1DVBX.setName("L1DVBX");
          panel3.add(L1DVBX);
          L1DVBX.setBounds(570, 65, 105, L1DVBX.getPreferredSize().height);

          //---- OBJ_42 ----
          OBJ_42.setText("Relev\u00e9");
          OBJ_42.setName("OBJ_42");
          panel3.add(OBJ_42);
          OBJ_42.setBounds(470, 95, 66, 28);

          //---- L1PCE ----
          L1PCE.setName("L1PCE");
          panel3.add(L1PCE);
          L1PCE.setBounds(160, 95, 60, L1PCE.getPreferredSize().height);

          //---- L1RLV ----
          L1RLV.setName("L1RLV");
          panel3.add(L1RLV);
          L1RLV.setBounds(570, 95, 80, L1RLV.getPreferredSize().height);

          //---- L1NCPX ----
          L1NCPX.setName("L1NCPX");
          panel3.add(L1NCPX);
          L1NCPX.setBounds(380, 65, 70, L1NCPX.getPreferredSize().height);

          //---- L1NLI ----
          L1NLI.setHorizontalAlignment(SwingConstants.RIGHT);
          L1NLI.setName("L1NLI");
          panel3.add(L1NLI);
          L1NLI.setBounds(160, 5, 40, L1NLI.getPreferredSize().height);

          //---- L1CLB ----
          L1CLB.setName("L1CLB");
          panel3.add(L1CLB);
          L1CLB.setBounds(530, 35, 20, L1CLB.getPreferredSize().height);

          //---- label1 ----
          label1.setText("@WLLITI@");
          label1.setHorizontalAlignment(SwingConstants.RIGHT);
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD, label1.getFont().getSize() + 3f));
          label1.setForeground(new Color(204, 0, 0));
          label1.setName("label1");
          panel3.add(label1);
          label1.setBounds(470, 5, 200, 25);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel3.getComponentCount(); i++) {
              Rectangle bounds = panel3.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel3.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel3.setMinimumSize(preferredSize);
            panel3.setPreferredSize(preferredSize);
          }
        }

        //======== xTitledPanel1 ========
        {
          xTitledPanel1.setBorder(new DropShadowBorder());
          xTitledPanel1.setTitle("Montant");
          xTitledPanel1.setName("xTitledPanel1");
          Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
          xTitledPanel1ContentContainer.setLayout(null);

          //---- OBJ_47 ----
          OBJ_47.setText("@LIBMT1@");
          OBJ_47.setName("OBJ_47");
          xTitledPanel1ContentContainer.add(OBJ_47);
          OBJ_47.setBounds(10, 10, 145, 19);

          //---- L1MTT ----
          L1MTT.setName("L1MTT");
          xTitledPanel1ContentContainer.add(L1MTT);
          L1MTT.setBounds(160, 5, 105, L1MTT.getPreferredSize().height);

          //---- L1MTD ----
          L1MTD.setName("L1MTD");
          xTitledPanel1ContentContainer.add(L1MTD);
          L1MTD.setBounds(160, 42, 105, L1MTD.getPreferredSize().height);

          //---- OBJ_54 ----
          OBJ_54.setText("@L1DEV@");
          OBJ_54.setName("OBJ_54");
          xTitledPanel1ContentContainer.add(OBJ_54);
          OBJ_54.setBounds(297, 44, 66, OBJ_54.getPreferredSize().height);

          //---- OBJ_52 ----
          OBJ_52.setText("Devise");
          OBJ_52.setName("OBJ_52");
          xTitledPanel1ContentContainer.add(OBJ_52);
          OBJ_52.setBounds(10, 47, 148, 19);

          //---- OBJ_50 ----
          OBJ_50.setText("Quantit\u00e9");
          OBJ_50.setName("OBJ_50");
          xTitledPanel1ContentContainer.add(OBJ_50);
          OBJ_50.setBounds(405, 10, 60, 19);

          //---- OBJ_55 ----
          OBJ_55.setText("Taux");
          OBJ_55.setName("OBJ_55");
          xTitledPanel1ContentContainer.add(OBJ_55);
          OBJ_55.setBounds(405, 47, 39, 19);

          //---- L1QTE ----
          L1QTE.setName("L1QTE");
          xTitledPanel1ContentContainer.add(L1QTE);
          L1QTE.setBounds(465, 5, 109, L1QTE.getPreferredSize().height);

          //---- WTXD ----
          WTXD.setName("WTXD");
          xTitledPanel1ContentContainer.add(WTXD);
          WTXD.setBounds(465, 42, 109, WTXD.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel1ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
          }
        }

        //======== xTitledPanel4 ========
        {
          xTitledPanel4.setTitle("D\u00e9tail des \u00e9ch\u00e9ances");
          xTitledPanel4.setBorder(new DropShadowBorder());
          xTitledPanel4.setName("xTitledPanel4");
          Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
          xTitledPanel4ContentContainer.setLayout(null);

          //---- OBJ_90 ----
          OBJ_90.setText("Nombre de jours depuis la facture");
          OBJ_90.setName("OBJ_90");
          xTitledPanel4ContentContainer.add(OBJ_90);
          OBJ_90.setBounds(30, 133, 195, 22);

          //---- NJF ----
          NJF.setName("NJF");
          xTitledPanel4ContentContainer.add(NJF);
          NJF.setBounds(308, 130, 40, NJF.getPreferredSize().height);

          //---- NRLV01 ----
          NRLV01.setText("@NRLV01@");
          NRLV01.setBorder(new BevelBorder(BevelBorder.LOWERED));
          NRLV01.setName("NRLV01");
          xTitledPanel4ContentContainer.add(NRLV01);
          NRLV01.setBounds(30, 23, 70, 23);

          //---- NRLV02 ----
          NRLV02.setText("@NRLV02@");
          NRLV02.setBorder(new BevelBorder(BevelBorder.LOWERED));
          NRLV02.setName("NRLV02");
          xTitledPanel4ContentContainer.add(NRLV02);
          NRLV02.setBounds(30, 48, 70, 23);

          //---- NRLV03 ----
          NRLV03.setText("@NRLV03@");
          NRLV03.setBorder(new BevelBorder(BevelBorder.LOWERED));
          NRLV03.setName("NRLV03");
          xTitledPanel4ContentContainer.add(NRLV03);
          NRLV03.setBounds(30, 73, 70, 23);

          //---- NRLV04 ----
          NRLV04.setText("@NRLV04@");
          NRLV04.setBorder(new BevelBorder(BevelBorder.LOWERED));
          NRLV04.setName("NRLV04");
          xTitledPanel4ContentContainer.add(NRLV04);
          NRLV04.setBounds(30, 98, 70, 23);

          //---- NIV01 ----
          NIV01.setHorizontalAlignment(SwingConstants.CENTER);
          NIV01.setName("NIV01");
          xTitledPanel4ContentContainer.add(NIV01);
          NIV01.setBounds(308, 20, 20, NIV01.getPreferredSize().height);

          //---- NIV02 ----
          NIV02.setName("NIV02");
          xTitledPanel4ContentContainer.add(NIV02);
          NIV02.setBounds(308, 45, 20, NIV02.getPreferredSize().height);

          //---- NIV03 ----
          NIV03.setName("NIV03");
          xTitledPanel4ContentContainer.add(NIV03);
          NIV03.setBounds(308, 70, 20, NIV03.getPreferredSize().height);

          //---- NIV04 ----
          NIV04.setName("NIV04");
          xTitledPanel4ContentContainer.add(NIV04);
          NIV04.setBounds(308, 95, 20, NIV04.getPreferredSize().height);

          //---- MTT01 ----
          MTT01.setName("MTT01");
          xTitledPanel4ContentContainer.add(MTT01);
          MTT01.setBounds(334, 20, 90, MTT01.getPreferredSize().height);

          //---- MTT02 ----
          MTT02.setName("MTT02");
          xTitledPanel4ContentContainer.add(MTT02);
          MTT02.setBounds(334, 45, 90, MTT02.getPreferredSize().height);

          //---- MTT03 ----
          MTT03.setName("MTT03");
          xTitledPanel4ContentContainer.add(MTT03);
          MTT03.setBounds(334, 70, 90, MTT03.getPreferredSize().height);

          //---- MTT04 ----
          MTT04.setName("MTT04");
          xTitledPanel4ContentContainer.add(MTT04);
          MTT04.setBounds(334, 95, 90, MTT04.getPreferredSize().height);

          //---- ECH01 ----
          ECH01.setHorizontalAlignment(SwingConstants.RIGHT);
          ECH01.setName("ECH01");
          xTitledPanel4ContentContainer.add(ECH01);
          ECH01.setBounds(430, 20, 60, ECH01.getPreferredSize().height);

          //---- ECH02 ----
          ECH02.setHorizontalAlignment(SwingConstants.RIGHT);
          ECH02.setName("ECH02");
          xTitledPanel4ContentContainer.add(ECH02);
          ECH02.setBounds(430, 45, 60, ECH02.getPreferredSize().height);

          //---- ECH03 ----
          ECH03.setHorizontalAlignment(SwingConstants.RIGHT);
          ECH03.setName("ECH03");
          xTitledPanel4ContentContainer.add(ECH03);
          ECH03.setBounds(430, 70, 60, ECH03.getPreferredSize().height);

          //---- ECH04 ----
          ECH04.setHorizontalAlignment(SwingConstants.RIGHT);
          ECH04.setName("ECH04");
          xTitledPanel4ContentContainer.add(ECH04);
          ECH04.setBounds(430, 95, 60, ECH04.getPreferredSize().height);

          //---- NBJ01 ----
          NBJ01.setName("NBJ01");
          xTitledPanel4ContentContainer.add(NBJ01);
          NBJ01.setBounds(496, 20, 40, NBJ01.getPreferredSize().height);

          //---- NBJ02 ----
          NBJ02.setName("NBJ02");
          xTitledPanel4ContentContainer.add(NBJ02);
          NBJ02.setBounds(496, 45, 40, NBJ02.getPreferredSize().height);

          //---- NBJ03 ----
          NBJ03.setName("NBJ03");
          xTitledPanel4ContentContainer.add(NBJ03);
          NBJ03.setBounds(496, 70, 40, NBJ03.getPreferredSize().height);

          //---- NBJ04 ----
          NBJ04.setName("NBJ04");
          xTitledPanel4ContentContainer.add(NBJ04);
          NBJ04.setBounds(496, 95, 40, NBJ04.getPreferredSize().height);

          //---- RGL01 ----
          RGL01.setName("RGL01");
          xTitledPanel4ContentContainer.add(RGL01);
          RGL01.setBounds(542, 20, 30, RGL01.getPreferredSize().height);

          //---- RGL02 ----
          RGL02.setName("RGL02");
          xTitledPanel4ContentContainer.add(RGL02);
          RGL02.setBounds(542, 45, 30, RGL02.getPreferredSize().height);

          //---- RGL03 ----
          RGL03.setName("RGL03");
          xTitledPanel4ContentContainer.add(RGL03);
          RGL03.setBounds(542, 70, 30, RGL03.getPreferredSize().height);

          //---- RGL04 ----
          RGL04.setName("RGL04");
          xTitledPanel4ContentContainer.add(RGL04);
          RGL04.setBounds(542, 95, 30, RGL04.getPreferredSize().height);

          //---- PRI01 ----
          PRI01.setName("PRI01");
          xTitledPanel4ContentContainer.add(PRI01);
          PRI01.setBounds(578, 20, 80, PRI01.getPreferredSize().height);

          //---- PRI02 ----
          PRI02.setName("PRI02");
          xTitledPanel4ContentContainer.add(PRI02);
          PRI02.setBounds(578, 45, 80, PRI02.getPreferredSize().height);

          //---- PRI03 ----
          PRI03.setName("PRI03");
          xTitledPanel4ContentContainer.add(PRI03);
          PRI03.setBounds(578, 70, 80, PRI03.getPreferredSize().height);

          //---- PRI04 ----
          PRI04.setName("PRI04");
          xTitledPanel4ContentContainer.add(PRI04);
          PRI04.setBounds(578, 95, 80, PRI04.getPreferredSize().height);

          //---- label14 ----
          label14.setText("N");
          label14.setHorizontalAlignment(SwingConstants.CENTER);
          label14.setName("label14");
          xTitledPanel4ContentContainer.add(label14);
          label14.setBounds(308, 0, 20, 21);

          //---- label15 ----
          label15.setText("Montant");
          label15.setHorizontalAlignment(SwingConstants.CENTER);
          label15.setName("label15");
          xTitledPanel4ContentContainer.add(label15);
          label15.setBounds(334, 0, 90, 21);

          //---- label16 ----
          label16.setText("Ech\u00e9ance");
          label16.setHorizontalAlignment(SwingConstants.CENTER);
          label16.setName("label16");
          xTitledPanel4ContentContainer.add(label16);
          label16.setBounds(430, 0, 60, 21);

          //---- label17 ----
          label17.setText("Nb J");
          label17.setHorizontalAlignment(SwingConstants.CENTER);
          label17.setName("label17");
          xTitledPanel4ContentContainer.add(label17);
          label17.setBounds(496, 0, 40, 21);

          //---- label18 ----
          label18.setText("Rg");
          label18.setHorizontalAlignment(SwingConstants.CENTER);
          label18.setName("label18");
          xTitledPanel4ContentContainer.add(label18);
          label18.setBounds(542, 0, 30, 21);

          //---- label19 ----
          label19.setText("Pr\u00e9imputation");
          label19.setHorizontalAlignment(SwingConstants.CENTER);
          label19.setName("label19");
          xTitledPanel4ContentContainer.add(label19);
          label19.setBounds(581, 0, 80, 21);

          //---- LDR01 ----
          LDR01.setText("@LDR01@ @DDR01@");
          LDR01.setHorizontalAlignment(SwingConstants.LEFT);
          LDR01.setName("LDR01");
          xTitledPanel4ContentContainer.add(LDR01);
          LDR01.setBounds(140, 23, 165, 23);

          //---- LDR02 ----
          LDR02.setText("@LDR02@ @DDR02@");
          LDR02.setHorizontalAlignment(SwingConstants.LEFT);
          LDR02.setName("LDR02");
          xTitledPanel4ContentContainer.add(LDR02);
          LDR02.setBounds(140, 48, 165, 23);

          //---- LDR03 ----
          LDR03.setText("@LDR03@ @DDR03@");
          LDR03.setHorizontalAlignment(SwingConstants.LEFT);
          LDR03.setName("LDR03");
          xTitledPanel4ContentContainer.add(LDR03);
          LDR03.setBounds(140, 73, 165, 23);

          //---- LDR04 ----
          LDR04.setText("@LDR04@ @DDR04@");
          LDR04.setHorizontalAlignment(SwingConstants.LEFT);
          LDR04.setName("LDR04");
          xTitledPanel4ContentContainer.add(LDR04);
          LDR04.setBounds(140, 98, 165, 23);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel4ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel4ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel4ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel4ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel4ContentContainer.setPreferredSize(preferredSize);
          }
        }

        //======== xTitledPanel2 ========
        {
          xTitledPanel2.setBorder(new DropShadowBorder());
          xTitledPanel2.setTitle("Affectation analytique");
          xTitledPanel2.setName("xTitledPanel2");
          Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
          xTitledPanel2ContentContainer.setLayout(null);

          //---- OBJ_77 ----
          OBJ_77.setText("@LIBSAN@");
          OBJ_77.setName("OBJ_77");
          xTitledPanel2ContentContainer.add(OBJ_77);
          OBJ_77.setBounds(230, 5, 225, OBJ_77.getPreferredSize().height);

          //---- OBJ_78 ----
          OBJ_78.setText("@LIBAFF@");
          OBJ_78.setName("OBJ_78");
          xTitledPanel2ContentContainer.add(OBJ_78);
          OBJ_78.setBounds(230, 55, 225, OBJ_78.getPreferredSize().height);

          //---- OBJ_79 ----
          OBJ_79.setText("@LIBNAT@");
          OBJ_79.setName("OBJ_79");
          xTitledPanel2ContentContainer.add(OBJ_79);
          OBJ_79.setBounds(230, 30, 225, OBJ_79.getPreferredSize().height);

          //---- OBJ_75 ----
          OBJ_75.setText("Nature");
          OBJ_75.setName("OBJ_75");
          xTitledPanel2ContentContainer.add(OBJ_75);
          OBJ_75.setBounds(55, 30, 40, 28);

          //---- OBJ_73 ----
          OBJ_73.setText("Affaire");
          OBJ_73.setName("OBJ_73");
          xTitledPanel2ContentContainer.add(OBJ_73);
          OBJ_73.setBounds(55, 55, 40, 28);

          //---- L1ACT ----
          L1ACT.setName("L1ACT");
          xTitledPanel2ContentContainer.add(L1ACT);
          L1ACT.setBounds(160, 55, 60, L1ACT.getPreferredSize().height);

          //---- L1SAN ----
          L1SAN.setName("L1SAN");
          xTitledPanel2ContentContainer.add(L1SAN);
          L1SAN.setBounds(160, 5, 50, L1SAN.getPreferredSize().height);

          //---- L1NAT ----
          L1NAT.setName("L1NAT");
          xTitledPanel2ContentContainer.add(L1NAT);
          L1NAT.setBounds(160, 30, 60, L1NAT.getPreferredSize().height);

          //---- OBJ_61 ----
          OBJ_61.setText("Section");
          OBJ_61.setName("OBJ_61");
          xTitledPanel2ContentContainer.add(OBJ_61);
          OBJ_61.setBounds(0, 0, 0, 0);

          //---- OBJ_74 ----
          OBJ_74.setText("Section");
          OBJ_74.setName("OBJ_74");
          xTitledPanel2ContentContainer.add(OBJ_74);
          OBJ_74.setBounds(55, 5, 55, 28);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel2ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
          }
        }

        //======== xTitledPanel5 ========
        {
          xTitledPanel5.setBorder(new DropShadowBorder());
          xTitledPanel5.setTitle("TVA");
          xTitledPanel5.setName("xTitledPanel5");
          Container xTitledPanel5ContentContainer = xTitledPanel5.getContentContainer();
          xTitledPanel5ContentContainer.setLayout(null);

          //---- OBJ_48 ----
          OBJ_48.setText("Code TVA");
          OBJ_48.setName("OBJ_48");
          xTitledPanel5ContentContainer.add(OBJ_48);
          OBJ_48.setBounds(10, 10, 70, 19);

          //---- L1IN6 ----
          L1IN6.setName("L1IN6");
          xTitledPanel5ContentContainer.add(L1IN6);
          L1IN6.setBounds(80, 5, 24, L1IN6.getPreferredSize().height);

          //---- TTVA ----
          TTVA.setText("@TTVA@");
          TTVA.setHorizontalAlignment(SwingConstants.RIGHT);
          TTVA.setName("TTVA");
          xTitledPanel5ContentContainer.add(TTVA);
          TTVA.setBounds(220, 7, 44, TTVA.getPreferredSize().height);

          //---- OBJ_51 ----
          OBJ_51.setText("/");
          OBJ_51.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_51.setName("OBJ_51");
          xTitledPanel5ContentContainer.add(OBJ_51);
          OBJ_51.setBounds(265, 10, 25, 19);

          //---- L1AFAL ----
          L1AFAL.setName("L1AFAL");
          xTitledPanel5ContentContainer.add(L1AFAL);
          L1AFAL.setBounds(295, 5, 34, L1AFAL.getPreferredSize().height);

          //---- OBJ_53 ----
          OBJ_53.setText("Taux");
          OBJ_53.setName("OBJ_53");
          xTitledPanel5ContentContainer.add(OBJ_53);
          OBJ_53.setBounds(165, 10, 55, 19);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel5ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel5ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel5ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel5ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel5ContentContainer.setPreferredSize(preferredSize);
          }
        }

        //======== xTitledPanel3 ========
        {
          xTitledPanel3.setBorder(new DropShadowBorder());
          xTitledPanel3.setTitle("Axes");
          xTitledPanel3.setName("xTitledPanel3");
          Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
          xTitledPanel3ContentContainer.setLayout(null);

          //---- LIBTYF ----
          LIBTYF.setText("@LIBTYF@");
          LIBTYF.setName("LIBTYF");
          xTitledPanel3ContentContainer.add(LIBTYF);
          LIBTYF.setBounds(420, 100, 232, LIBTYF.getPreferredSize().height);

          //---- L1AA1 ----
          L1AA1.setName("L1AA1");
          xTitledPanel3ContentContainer.add(L1AA1);
          L1AA1.setBounds(10, 5, 60, L1AA1.getPreferredSize().height);

          //---- L1AA4 ----
          L1AA4.setName("L1AA4");
          xTitledPanel3ContentContainer.add(L1AA4);
          L1AA4.setBounds(340, 5, 60, L1AA4.getPreferredSize().height);

          //---- L1AA2 ----
          L1AA2.setName("L1AA2");
          xTitledPanel3ContentContainer.add(L1AA2);
          L1AA2.setBounds(10, 35, 60, L1AA2.getPreferredSize().height);

          //---- L1AA5 ----
          L1AA5.setName("L1AA5");
          xTitledPanel3ContentContainer.add(L1AA5);
          L1AA5.setBounds(340, 35, 60, L1AA5.getPreferredSize().height);

          //---- L1AA3 ----
          L1AA3.setName("L1AA3");
          xTitledPanel3ContentContainer.add(L1AA3);
          L1AA3.setBounds(10, 65, 60, L1AA3.getPreferredSize().height);

          //---- L1AA6 ----
          L1AA6.setName("L1AA6");
          xTitledPanel3ContentContainer.add(L1AA6);
          L1AA6.setBounds(340, 65, 60, L1AA6.getPreferredSize().height);

          //---- OBJ_84 ----
          OBJ_84.setText("@LIBAX1@");
          OBJ_84.setFont(OBJ_84.getFont().deriveFont(OBJ_84.getFont().getSize() - 2f));
          OBJ_84.setName("OBJ_84");
          xTitledPanel3ContentContainer.add(OBJ_84);
          OBJ_84.setBounds(75, 5, 232, OBJ_84.getPreferredSize().height);

          //---- OBJ_88 ----
          OBJ_88.setText("@LIBAX2@");
          OBJ_88.setFont(OBJ_88.getFont().deriveFont(OBJ_88.getFont().getSize() - 2f));
          OBJ_88.setName("OBJ_88");
          xTitledPanel3ContentContainer.add(OBJ_88);
          OBJ_88.setBounds(75, 35, 232, OBJ_88.getPreferredSize().height);

          //---- OBJ_94 ----
          OBJ_94.setText("@LIBAX3@");
          OBJ_94.setFont(OBJ_94.getFont().deriveFont(OBJ_94.getFont().getSize() - 2f));
          OBJ_94.setName("OBJ_94");
          xTitledPanel3ContentContainer.add(OBJ_94);
          OBJ_94.setBounds(75, 65, 232, OBJ_94.getPreferredSize().height);

          //---- OBJ_85 ----
          OBJ_85.setText("@LIBAX4@");
          OBJ_85.setFont(OBJ_85.getFont().deriveFont(OBJ_85.getFont().getSize() - 2f));
          OBJ_85.setName("OBJ_85");
          xTitledPanel3ContentContainer.add(OBJ_85);
          OBJ_85.setBounds(420, 5, 232, OBJ_85.getPreferredSize().height);

          //---- OBJ_89 ----
          OBJ_89.setText("@LIBAX5@");
          OBJ_89.setFont(OBJ_89.getFont().deriveFont(OBJ_89.getFont().getSize() - 2f));
          OBJ_89.setName("OBJ_89");
          xTitledPanel3ContentContainer.add(OBJ_89);
          OBJ_89.setBounds(420, 35, 232, OBJ_89.getPreferredSize().height);

          //---- OBJ_95 ----
          OBJ_95.setText("@LIBAX6@");
          OBJ_95.setFont(OBJ_95.getFont().deriveFont(OBJ_95.getFont().getSize() - 2f));
          OBJ_95.setName("OBJ_95");
          xTitledPanel3ContentContainer.add(OBJ_95);
          OBJ_95.setBounds(420, 65, 232, OBJ_95.getPreferredSize().height);

          //---- label20 ----
          label20.setText("Type de frais");
          label20.setName("label20");
          xTitledPanel3ContentContainer.add(label20);
          label20.setBounds(340, 100, 80, 24);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel3ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(12, 12, 12)
              .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                .addComponent(panel3, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(xTitledPanel4, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(xTitledPanel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
              .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 675, GroupLayout.PREFERRED_SIZE)
                .addComponent(xTitledPanel5, GroupLayout.PREFERRED_SIZE, 675, GroupLayout.PREFERRED_SIZE)
                .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 675, GroupLayout.PREFERRED_SIZE))
              .addContainerGap(18, Short.MAX_VALUE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
              .addGap(18, 18, 18)
              .addComponent(xTitledPanel5, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
              .addGap(16, 16, 16)
              .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
              .addGap(6, 6, 6)
              .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_23 ----
      OBJ_23.setText("Choix possibles");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD.add(OBJ_23);

      //---- OBJ_22 ----
      OBJ_22.setText("Aide en ligne");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);
    }

    //---- OBJ_49 ----
    OBJ_49.setName("OBJ_49");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_del;
  private RiSousMenu_bt riSousMenu_bt_del;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiSousMenu riSousMenu13;
  private RiSousMenu_bt riSousMenu_bt13;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private RiSousMenu riSousMenu16;
  private RiSousMenu_bt riSousMenu_bt16;
  private JPanel p_contenu;
  private JPanel panel3;
  private XRiTextField WLIBB1;
  private JLabel OBJ_36;
  private JLabel OBJ_31;
  private JLabel OBJ_40;
  private XRiTextField L1RFC;
  private JLabel OBJ_28;
  private JLabel OBJ_38;
  private JLabel OBJ_34;
  private XRiCalendrier L1DVBX;
  private JLabel OBJ_42;
  private XRiTextField L1PCE;
  private XRiTextField L1RLV;
  private XRiTextField L1NCPX;
  private XRiTextField L1NLI;
  private XRiTextField L1CLB;
  private JLabel label1;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_47;
  private XRiTextField L1MTT;
  private XRiTextField L1MTD;
  private RiZoneSortie OBJ_54;
  private JLabel OBJ_52;
  private JLabel OBJ_50;
  private JLabel OBJ_55;
  private XRiTextField L1QTE;
  private XRiTextField WTXD;
  private JXTitledPanel xTitledPanel4;
  private JLabel OBJ_90;
  private XRiTextField NJF;
  private RiZoneSortie NRLV01;
  private RiZoneSortie NRLV02;
  private RiZoneSortie NRLV03;
  private RiZoneSortie NRLV04;
  private XRiTextField NIV01;
  private XRiTextField NIV02;
  private XRiTextField NIV03;
  private XRiTextField NIV04;
  private XRiTextField MTT01;
  private XRiTextField MTT02;
  private XRiTextField MTT03;
  private XRiTextField MTT04;
  private XRiTextField ECH01;
  private XRiTextField ECH02;
  private XRiTextField ECH03;
  private XRiTextField ECH04;
  private XRiTextField NBJ01;
  private XRiTextField NBJ02;
  private XRiTextField NBJ03;
  private XRiTextField NBJ04;
  private XRiTextField RGL01;
  private XRiTextField RGL02;
  private XRiTextField RGL03;
  private XRiTextField RGL04;
  private XRiTextField PRI01;
  private XRiTextField PRI02;
  private XRiTextField PRI03;
  private XRiTextField PRI04;
  private JLabel label14;
  private JLabel label15;
  private JLabel label16;
  private JLabel label17;
  private JLabel label18;
  private JLabel label19;
  private JLabel LDR01;
  private JLabel LDR02;
  private JLabel LDR03;
  private JLabel LDR04;
  private JXTitledPanel xTitledPanel2;
  private RiZoneSortie OBJ_77;
  private RiZoneSortie OBJ_78;
  private RiZoneSortie OBJ_79;
  private JLabel OBJ_75;
  private JLabel OBJ_73;
  private XRiTextField L1ACT;
  private XRiTextField L1SAN;
  private XRiTextField L1NAT;
  private JLabel OBJ_61;
  private JLabel OBJ_74;
  private JXTitledPanel xTitledPanel5;
  private JLabel OBJ_48;
  private XRiTextField L1IN6;
  private RiZoneSortie TTVA;
  private JLabel OBJ_51;
  private XRiTextField L1AFAL;
  private JLabel OBJ_53;
  private JXTitledPanel xTitledPanel3;
  private RiZoneSortie LIBTYF;
  private XRiTextField L1AA1;
  private XRiTextField L1AA4;
  private XRiTextField L1AA2;
  private XRiTextField L1AA5;
  private XRiTextField L1AA3;
  private XRiTextField L1AA6;
  private RiZoneSortie OBJ_84;
  private RiZoneSortie OBJ_88;
  private RiZoneSortie OBJ_94;
  private RiZoneSortie OBJ_85;
  private RiZoneSortie OBJ_89;
  private RiZoneSortie OBJ_95;
  private JLabel label20;
  private JPopupMenu BTD;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_22;
  private JLabel OBJ_49;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

}
