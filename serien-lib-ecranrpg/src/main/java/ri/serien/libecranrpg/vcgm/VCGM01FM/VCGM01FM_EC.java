
package ri.serien.libecranrpg.vcgm.VCGM01FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM01FM_EC extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM01FM_EC(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_43_OBJ_43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EJLJ01@")).trim());
    OBJ_46_OBJ_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EJLJ02@")).trim());
    OBJ_49_OBJ_49.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EJLJ03@")).trim());
    OBJ_52_OBJ_52.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EJLJ04@")).trim());
    OBJ_55_OBJ_55.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EJLJ05@")).trim());
    OBJ_58_OBJ_58.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EJLJ06@")).trim());
    OBJ_61_OBJ_61.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EJLJ07@")).trim());
    OBJ_64_OBJ_64.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EJLJ08@")).trim());
    OBJ_67_OBJ_67.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EJLJ09@")).trim());
    OBJ_70_OBJ_70.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EJLJ10@")).trim());
    OBJ_73_OBJ_73.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EJLJ11@")).trim());
    OBJ_76_OBJ_76.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EJLJ12@")).trim());
    OBJ_42_OBJ_42.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EJLC01@")).trim());
    OBJ_45_OBJ_45.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EJLC02@")).trim());
    OBJ_48_OBJ_48.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EJLC03@")).trim());
    OBJ_51_OBJ_51.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EJLC04@")).trim());
    OBJ_54_OBJ_54.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EJLC05@")).trim());
    OBJ_57_OBJ_57.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EJLC06@")).trim());
    OBJ_60_OBJ_60.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EJLC07@")).trim());
    OBJ_63_OBJ_63.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EJLC08@")).trim());
    OBJ_66_OBJ_66.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EJLC09@")).trim());
    OBJ_69_OBJ_69.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EJLC10@")).trim());
    OBJ_72_OBJ_72.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EJLC11@")).trim());
    OBJ_75_OBJ_75.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EJLC12@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    OBJ_75_OBJ_75.setVisible(lexique.isPresent("EJLC12"));
    OBJ_72_OBJ_72.setVisible(lexique.isPresent("EJLC11"));
    OBJ_69_OBJ_69.setVisible(lexique.isPresent("EJLC10"));
    OBJ_66_OBJ_66.setVisible(lexique.isPresent("EJLC09"));
    OBJ_63_OBJ_63.setVisible(lexique.isPresent("EJLC08"));
    OBJ_60_OBJ_60.setVisible(lexique.isPresent("EJLC07"));
    OBJ_57_OBJ_57.setVisible(lexique.isPresent("EJLC06"));
    OBJ_54_OBJ_54.setVisible(lexique.isPresent("EJLC05"));
    OBJ_51_OBJ_51.setVisible(lexique.isPresent("EJLC04"));
    OBJ_48_OBJ_48.setVisible(lexique.isPresent("EJLC03"));
    OBJ_45_OBJ_45.setVisible(lexique.isPresent("EJLC02"));
    OBJ_42_OBJ_42.setVisible(lexique.isPresent("EJLC01"));
    OBJ_76_OBJ_76.setVisible(lexique.isPresent("EJLJ12"));
    OBJ_73_OBJ_73.setVisible(lexique.isPresent("EJLJ11"));
    OBJ_70_OBJ_70.setVisible(lexique.isPresent("EJLJ10"));
    OBJ_67_OBJ_67.setVisible(lexique.isPresent("EJLJ09"));
    OBJ_64_OBJ_64.setVisible(lexique.isPresent("EJLJ08"));
    OBJ_61_OBJ_61.setVisible(lexique.isPresent("EJLJ07"));
    OBJ_58_OBJ_58.setVisible(lexique.isPresent("EJLJ06"));
    OBJ_55_OBJ_55.setVisible(lexique.isPresent("EJLJ05"));
    OBJ_52_OBJ_52.setVisible(lexique.isPresent("EJLJ04"));
    OBJ_49_OBJ_49.setVisible(lexique.isPresent("EJLJ03"));
    OBJ_46_OBJ_46.setVisible(lexique.isPresent("EJLJ02"));
    OBJ_43_OBJ_43.setVisible(lexique.isPresent("EJLJ01"));
    
    

    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44_OBJ_45 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_45_OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46_OBJ_47 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_43_OBJ_43 = new RiZoneSortie();
    OBJ_46_OBJ_46 = new RiZoneSortie();
    OBJ_49_OBJ_49 = new RiZoneSortie();
    OBJ_52_OBJ_52 = new RiZoneSortie();
    OBJ_55_OBJ_55 = new RiZoneSortie();
    OBJ_58_OBJ_58 = new RiZoneSortie();
    OBJ_61_OBJ_61 = new RiZoneSortie();
    OBJ_64_OBJ_64 = new RiZoneSortie();
    OBJ_67_OBJ_67 = new RiZoneSortie();
    OBJ_70_OBJ_70 = new RiZoneSortie();
    OBJ_73_OBJ_73 = new RiZoneSortie();
    OBJ_76_OBJ_76 = new RiZoneSortie();
    OBJ_42_OBJ_42 = new RiZoneSortie();
    OBJ_45_OBJ_45 = new RiZoneSortie();
    OBJ_48_OBJ_48 = new RiZoneSortie();
    OBJ_51_OBJ_51 = new RiZoneSortie();
    OBJ_54_OBJ_54 = new RiZoneSortie();
    OBJ_57_OBJ_57 = new RiZoneSortie();
    OBJ_60_OBJ_60 = new RiZoneSortie();
    OBJ_63_OBJ_63 = new RiZoneSortie();
    OBJ_66_OBJ_66 = new RiZoneSortie();
    OBJ_69_OBJ_69 = new RiZoneSortie();
    OBJ_72_OBJ_72 = new RiZoneSortie();
    OBJ_75_OBJ_75 = new RiZoneSortie();
    OBJ_78_OBJ_78 = new JLabel();
    OBJ_40_OBJ_40 = new JLabel();
    OBJ_77_OBJ_77 = new JLabel();
    EJZ12C = new XRiTextField();
    EJZ13C = new XRiTextField();
    EJZ21C = new XRiTextField();
    EJZ22C = new XRiTextField();
    EJZ23C = new XRiTextField();
    EJZ31C = new XRiTextField();
    EJZ32C = new XRiTextField();
    EJZ33C = new XRiTextField();
    EJZ41C = new XRiTextField();
    EJZ42C = new XRiTextField();
    EJZ43C = new XRiTextField();
    EJZ11C = new XRiTextField();
    OBJ_39_OBJ_39 = new JLabel();
    EJZ11J = new XRiTextField();
    EJZ12J = new XRiTextField();
    EJZ13J = new XRiTextField();
    EJZ21J = new XRiTextField();
    EJZ22J = new XRiTextField();
    EJZ23J = new XRiTextField();
    EJZ31J = new XRiTextField();
    EJZ32J = new XRiTextField();
    EJZ33J = new XRiTextField();
    EJZ41J = new XRiTextField();
    EJZ42J = new XRiTextField();
    EJZ43J = new XRiTextField();
    OBJ_44_OBJ_44 = new JLabel();
    OBJ_47_OBJ_47 = new JLabel();
    OBJ_50_OBJ_50 = new JLabel();
    OBJ_41_OBJ_41 = new JLabel();
    OBJ_53_OBJ_53 = new JLabel();
    OBJ_56_OBJ_56 = new JLabel();
    OBJ_59_OBJ_59 = new JLabel();
    OBJ_62_OBJ_62 = new JLabel();
    OBJ_65_OBJ_65 = new JLabel();
    OBJ_68_OBJ_68 = new JLabel();
    OBJ_71_OBJ_71 = new JLabel();
    OBJ_74_OBJ_74 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44_OBJ_45 ----
          OBJ_44_OBJ_45.setText("Soci\u00e9t\u00e9");
          OBJ_44_OBJ_45.setName("OBJ_44_OBJ_45");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setName("INDETB");

          //---- OBJ_45_OBJ_46 ----
          OBJ_45_OBJ_46.setText("Code");
          OBJ_45_OBJ_46.setName("OBJ_45_OBJ_46");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setName("INDTYP");

          //---- OBJ_46_OBJ_47 ----
          OBJ_46_OBJ_47.setText("Ordre");
          OBJ_46_OBJ_47.setName("OBJ_46_OBJ_47");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(800, 450));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Gestion des \u00e9ch\u00e9ances");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

            //---- OBJ_43_OBJ_43 ----
            OBJ_43_OBJ_43.setText("@EJLJ01@");
            OBJ_43_OBJ_43.setName("OBJ_43_OBJ_43");

            //---- OBJ_46_OBJ_46 ----
            OBJ_46_OBJ_46.setText("@EJLJ02@");
            OBJ_46_OBJ_46.setName("OBJ_46_OBJ_46");

            //---- OBJ_49_OBJ_49 ----
            OBJ_49_OBJ_49.setText("@EJLJ03@");
            OBJ_49_OBJ_49.setName("OBJ_49_OBJ_49");

            //---- OBJ_52_OBJ_52 ----
            OBJ_52_OBJ_52.setText("@EJLJ04@");
            OBJ_52_OBJ_52.setName("OBJ_52_OBJ_52");

            //---- OBJ_55_OBJ_55 ----
            OBJ_55_OBJ_55.setText("@EJLJ05@");
            OBJ_55_OBJ_55.setName("OBJ_55_OBJ_55");

            //---- OBJ_58_OBJ_58 ----
            OBJ_58_OBJ_58.setText("@EJLJ06@");
            OBJ_58_OBJ_58.setName("OBJ_58_OBJ_58");

            //---- OBJ_61_OBJ_61 ----
            OBJ_61_OBJ_61.setText("@EJLJ07@");
            OBJ_61_OBJ_61.setName("OBJ_61_OBJ_61");

            //---- OBJ_64_OBJ_64 ----
            OBJ_64_OBJ_64.setText("@EJLJ08@");
            OBJ_64_OBJ_64.setName("OBJ_64_OBJ_64");

            //---- OBJ_67_OBJ_67 ----
            OBJ_67_OBJ_67.setText("@EJLJ09@");
            OBJ_67_OBJ_67.setName("OBJ_67_OBJ_67");

            //---- OBJ_70_OBJ_70 ----
            OBJ_70_OBJ_70.setText("@EJLJ10@");
            OBJ_70_OBJ_70.setName("OBJ_70_OBJ_70");

            //---- OBJ_73_OBJ_73 ----
            OBJ_73_OBJ_73.setText("@EJLJ11@");
            OBJ_73_OBJ_73.setName("OBJ_73_OBJ_73");

            //---- OBJ_76_OBJ_76 ----
            OBJ_76_OBJ_76.setText("@EJLJ12@");
            OBJ_76_OBJ_76.setName("OBJ_76_OBJ_76");

            //---- OBJ_42_OBJ_42 ----
            OBJ_42_OBJ_42.setText("@EJLC01@");
            OBJ_42_OBJ_42.setName("OBJ_42_OBJ_42");

            //---- OBJ_45_OBJ_45 ----
            OBJ_45_OBJ_45.setText("@EJLC02@");
            OBJ_45_OBJ_45.setName("OBJ_45_OBJ_45");

            //---- OBJ_48_OBJ_48 ----
            OBJ_48_OBJ_48.setText("@EJLC03@");
            OBJ_48_OBJ_48.setName("OBJ_48_OBJ_48");

            //---- OBJ_51_OBJ_51 ----
            OBJ_51_OBJ_51.setText("@EJLC04@");
            OBJ_51_OBJ_51.setName("OBJ_51_OBJ_51");

            //---- OBJ_54_OBJ_54 ----
            OBJ_54_OBJ_54.setText("@EJLC05@");
            OBJ_54_OBJ_54.setName("OBJ_54_OBJ_54");

            //---- OBJ_57_OBJ_57 ----
            OBJ_57_OBJ_57.setText("@EJLC06@");
            OBJ_57_OBJ_57.setName("OBJ_57_OBJ_57");

            //---- OBJ_60_OBJ_60 ----
            OBJ_60_OBJ_60.setText("@EJLC07@");
            OBJ_60_OBJ_60.setName("OBJ_60_OBJ_60");

            //---- OBJ_63_OBJ_63 ----
            OBJ_63_OBJ_63.setText("@EJLC08@");
            OBJ_63_OBJ_63.setName("OBJ_63_OBJ_63");

            //---- OBJ_66_OBJ_66 ----
            OBJ_66_OBJ_66.setText("@EJLC09@");
            OBJ_66_OBJ_66.setName("OBJ_66_OBJ_66");

            //---- OBJ_69_OBJ_69 ----
            OBJ_69_OBJ_69.setText("@EJLC10@");
            OBJ_69_OBJ_69.setName("OBJ_69_OBJ_69");

            //---- OBJ_72_OBJ_72 ----
            OBJ_72_OBJ_72.setText("@EJLC11@");
            OBJ_72_OBJ_72.setName("OBJ_72_OBJ_72");

            //---- OBJ_75_OBJ_75 ----
            OBJ_75_OBJ_75.setText("@EJLC12@");
            OBJ_75_OBJ_75.setName("OBJ_75_OBJ_75");

            //---- OBJ_78_OBJ_78 ----
            OBJ_78_OBJ_78.setText("Libell\u00e9 compte g\u00e9n\u00e9ral");
            OBJ_78_OBJ_78.setFont(OBJ_78_OBJ_78.getFont().deriveFont(OBJ_78_OBJ_78.getFont().getStyle() | Font.BOLD));
            OBJ_78_OBJ_78.setName("OBJ_78_OBJ_78");

            //---- OBJ_40_OBJ_40 ----
            OBJ_40_OBJ_40.setText("Libell\u00e9 journal");
            OBJ_40_OBJ_40.setFont(OBJ_40_OBJ_40.getFont().deriveFont(OBJ_40_OBJ_40.getFont().getStyle() | Font.BOLD));
            OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");

            //---- OBJ_77_OBJ_77 ----
            OBJ_77_OBJ_77.setText("N\u00b0 compte");
            OBJ_77_OBJ_77.setFont(OBJ_77_OBJ_77.getFont().deriveFont(OBJ_77_OBJ_77.getFont().getStyle() | Font.BOLD));
            OBJ_77_OBJ_77.setName("OBJ_77_OBJ_77");

            //---- EJZ12C ----
            EJZ12C.setName("EJZ12C");

            //---- EJZ13C ----
            EJZ13C.setName("EJZ13C");

            //---- EJZ21C ----
            EJZ21C.setName("EJZ21C");

            //---- EJZ22C ----
            EJZ22C.setName("EJZ22C");

            //---- EJZ23C ----
            EJZ23C.setName("EJZ23C");

            //---- EJZ31C ----
            EJZ31C.setName("EJZ31C");

            //---- EJZ32C ----
            EJZ32C.setName("EJZ32C");

            //---- EJZ33C ----
            EJZ33C.setName("EJZ33C");

            //---- EJZ41C ----
            EJZ41C.setName("EJZ41C");

            //---- EJZ42C ----
            EJZ42C.setName("EJZ42C");

            //---- EJZ43C ----
            EJZ43C.setName("EJZ43C");

            //---- EJZ11C ----
            EJZ11C.setName("EJZ11C");

            //---- OBJ_39_OBJ_39 ----
            OBJ_39_OBJ_39.setText("JO");
            OBJ_39_OBJ_39.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_39_OBJ_39.setFont(OBJ_39_OBJ_39.getFont().deriveFont(OBJ_39_OBJ_39.getFont().getStyle() | Font.BOLD));
            OBJ_39_OBJ_39.setName("OBJ_39_OBJ_39");

            //---- EJZ11J ----
            EJZ11J.setName("EJZ11J");

            //---- EJZ12J ----
            EJZ12J.setName("EJZ12J");

            //---- EJZ13J ----
            EJZ13J.setName("EJZ13J");

            //---- EJZ21J ----
            EJZ21J.setName("EJZ21J");

            //---- EJZ22J ----
            EJZ22J.setName("EJZ22J");

            //---- EJZ23J ----
            EJZ23J.setName("EJZ23J");

            //---- EJZ31J ----
            EJZ31J.setName("EJZ31J");

            //---- EJZ32J ----
            EJZ32J.setName("EJZ32J");

            //---- EJZ33J ----
            EJZ33J.setName("EJZ33J");

            //---- EJZ41J ----
            EJZ41J.setName("EJZ41J");

            //---- EJZ42J ----
            EJZ42J.setName("EJZ42J");

            //---- EJZ43J ----
            EJZ43J.setName("EJZ43J");

            //---- OBJ_44_OBJ_44 ----
            OBJ_44_OBJ_44.setText("2");
            OBJ_44_OBJ_44.setName("OBJ_44_OBJ_44");

            //---- OBJ_47_OBJ_47 ----
            OBJ_47_OBJ_47.setText("3");
            OBJ_47_OBJ_47.setName("OBJ_47_OBJ_47");

            //---- OBJ_50_OBJ_50 ----
            OBJ_50_OBJ_50.setText("4");
            OBJ_50_OBJ_50.setName("OBJ_50_OBJ_50");

            //---- OBJ_41_OBJ_41 ----
            OBJ_41_OBJ_41.setText("1");
            OBJ_41_OBJ_41.setName("OBJ_41_OBJ_41");

            //---- OBJ_53_OBJ_53 ----
            OBJ_53_OBJ_53.setText("5");
            OBJ_53_OBJ_53.setName("OBJ_53_OBJ_53");

            //---- OBJ_56_OBJ_56 ----
            OBJ_56_OBJ_56.setText("6");
            OBJ_56_OBJ_56.setName("OBJ_56_OBJ_56");

            //---- OBJ_59_OBJ_59 ----
            OBJ_59_OBJ_59.setText("7");
            OBJ_59_OBJ_59.setName("OBJ_59_OBJ_59");

            //---- OBJ_62_OBJ_62 ----
            OBJ_62_OBJ_62.setText("8");
            OBJ_62_OBJ_62.setName("OBJ_62_OBJ_62");

            //---- OBJ_65_OBJ_65 ----
            OBJ_65_OBJ_65.setText("9");
            OBJ_65_OBJ_65.setName("OBJ_65_OBJ_65");

            //---- OBJ_68_OBJ_68 ----
            OBJ_68_OBJ_68.setText("10");
            OBJ_68_OBJ_68.setName("OBJ_68_OBJ_68");

            //---- OBJ_71_OBJ_71 ----
            OBJ_71_OBJ_71.setText("11");
            OBJ_71_OBJ_71.setName("OBJ_71_OBJ_71");

            //---- OBJ_74_OBJ_74 ----
            OBJ_74_OBJ_74.setText("12");
            OBJ_74_OBJ_74.setName("OBJ_74_OBJ_74");

            GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
            xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
            xTitledPanel1ContentContainerLayout.setHorizontalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(45, 45, 45)
                      .addComponent(OBJ_77_OBJ_77, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                      .addGap(25, 25, 25)
                      .addComponent(OBJ_78_OBJ_78, GroupLayout.PREFERRED_SIZE, 163, GroupLayout.PREFERRED_SIZE)
                      .addGap(107, 107, 107)
                      .addComponent(OBJ_39_OBJ_39, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(20, 20, 20)
                      .addComponent(OBJ_40_OBJ_40, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_47_OBJ_47, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_41_OBJ_41, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_44_OBJ_44, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_50_OBJ_50, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_53_OBJ_53, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_71_OBJ_71, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_59_OBJ_59, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_56_OBJ_56, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_62_OBJ_62, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_65_OBJ_65, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_74_OBJ_74, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_68_OBJ_68, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(20, 20, 20)
                          .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                            .addComponent(EJZ13C, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                            .addComponent(EJZ21C, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                            .addComponent(EJZ11C, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                            .addComponent(EJZ12C, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                            .addComponent(EJZ41C, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                            .addComponent(EJZ33C, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                            .addComponent(EJZ42C, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                            .addComponent(EJZ23C, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                            .addComponent(EJZ43C, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                            .addComponent(EJZ31C, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                            .addComponent(EJZ32C, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                            .addComponent(EJZ22C, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))))
                      .addGap(25, 25, 25)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_42_OBJ_42, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_45_OBJ_45, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_48_OBJ_48, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_51_OBJ_51, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_54_OBJ_54, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_57_OBJ_57, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_60_OBJ_60, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_63_OBJ_63, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_66_OBJ_66, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_69_OBJ_69, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_72_OBJ_72, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_75_OBJ_75, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE))
                      .addGap(24, 24, 24)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(EJZ21J, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EJZ13J, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EJZ11J, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EJZ12J, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EJZ22J, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EJZ31J, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EJZ43J, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EJZ32J, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EJZ42J, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EJZ33J, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EJZ41J, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EJZ23J, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                      .addGap(20, 20, 20)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_43_OBJ_43, GroupLayout.PREFERRED_SIZE, 252, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_46_OBJ_46, GroupLayout.PREFERRED_SIZE, 252, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_49_OBJ_49, GroupLayout.PREFERRED_SIZE, 252, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_52_OBJ_52, GroupLayout.PREFERRED_SIZE, 252, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_55_OBJ_55, GroupLayout.PREFERRED_SIZE, 252, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_58_OBJ_58, GroupLayout.PREFERRED_SIZE, 252, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_61_OBJ_61, GroupLayout.PREFERRED_SIZE, 252, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_64_OBJ_64, GroupLayout.PREFERRED_SIZE, 252, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_67_OBJ_67, GroupLayout.PREFERRED_SIZE, 252, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_70_OBJ_70, GroupLayout.PREFERRED_SIZE, 252, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_73_OBJ_73, GroupLayout.PREFERRED_SIZE, 252, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_76_OBJ_76, GroupLayout.PREFERRED_SIZE, 252, GroupLayout.PREFERRED_SIZE))))
                  .addContainerGap(57, Short.MAX_VALUE))
            );
            xTitledPanel1ContentContainerLayout.setVerticalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_77_OBJ_77, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_78_OBJ_78, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_39_OBJ_39, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_40_OBJ_40, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(58, 58, 58)
                          .addComponent(OBJ_47_OBJ_47, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(54, 54, 54)
                          .addComponent(EJZ13C, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(4, 4, 4)
                          .addComponent(OBJ_41_OBJ_41, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(81, 81, 81)
                          .addComponent(EJZ21C, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(31, 31, 31)
                          .addComponent(OBJ_44_OBJ_44, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(85, 85, 85)
                          .addComponent(OBJ_50_OBJ_50, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                        .addComponent(EJZ11C, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(27, 27, 27)
                          .addComponent(EJZ12C, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_53_OBJ_53, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(139, 139, 139)
                          .addComponent(OBJ_71_OBJ_71, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(31, 31, 31)
                          .addComponent(OBJ_59_OBJ_59, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(108, 108, 108)
                          .addComponent(EJZ41C, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(4, 4, 4)
                          .addComponent(OBJ_56_OBJ_56, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(58, 58, 58)
                          .addComponent(OBJ_62_OBJ_62, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(81, 81, 81)
                          .addComponent(EJZ33C, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(135, 135, 135)
                          .addComponent(EJZ42C, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(EJZ23C, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(85, 85, 85)
                          .addComponent(OBJ_65_OBJ_65, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(162, 162, 162)
                          .addComponent(EJZ43C, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(27, 27, 27)
                          .addComponent(EJZ31C, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(54, 54, 54)
                          .addComponent(EJZ32C, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(301, 301, 301)
                      .addComponent(OBJ_74_OBJ_74, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(247, 247, 247)
                      .addComponent(OBJ_68_OBJ_68, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(108, 108, 108)
                      .addComponent(EJZ22C, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_42_OBJ_42, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_45_OBJ_45, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_48_OBJ_48, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_51_OBJ_51, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_54_OBJ_54, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_57_OBJ_57, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_60_OBJ_60, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_63_OBJ_63, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_66_OBJ_66, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_69_OBJ_69, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_72_OBJ_72, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_75_OBJ_75, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(81, 81, 81)
                          .addComponent(EJZ21J, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(54, 54, 54)
                          .addComponent(EJZ13J, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(EJZ11J, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(27, 27, 27)
                          .addComponent(EJZ12J, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(108, 108, 108)
                          .addComponent(EJZ22J, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(26, 26, 26)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(EJZ31J, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(135, 135, 135)
                          .addComponent(EJZ43J, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(27, 27, 27)
                          .addComponent(EJZ32J, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(108, 108, 108)
                          .addComponent(EJZ42J, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(54, 54, 54)
                          .addComponent(EJZ33J, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(81, 81, 81)
                          .addComponent(EJZ41J, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(135, 135, 135)
                      .addComponent(EJZ23J, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_43_OBJ_43, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_46_OBJ_46, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_49_OBJ_49, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_52_OBJ_52, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_55_OBJ_55, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_58_OBJ_58, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_61_OBJ_61, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_64_OBJ_64, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_67_OBJ_67, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_70_OBJ_70, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_73_OBJ_73, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_76_OBJ_76, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addContainerGap(27, Short.MAX_VALUE))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44_OBJ_45;
  private XRiTextField INDETB;
  private JLabel OBJ_45_OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_46_OBJ_47;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private RiZoneSortie OBJ_43_OBJ_43;
  private RiZoneSortie OBJ_46_OBJ_46;
  private RiZoneSortie OBJ_49_OBJ_49;
  private RiZoneSortie OBJ_52_OBJ_52;
  private RiZoneSortie OBJ_55_OBJ_55;
  private RiZoneSortie OBJ_58_OBJ_58;
  private RiZoneSortie OBJ_61_OBJ_61;
  private RiZoneSortie OBJ_64_OBJ_64;
  private RiZoneSortie OBJ_67_OBJ_67;
  private RiZoneSortie OBJ_70_OBJ_70;
  private RiZoneSortie OBJ_73_OBJ_73;
  private RiZoneSortie OBJ_76_OBJ_76;
  private RiZoneSortie OBJ_42_OBJ_42;
  private RiZoneSortie OBJ_45_OBJ_45;
  private RiZoneSortie OBJ_48_OBJ_48;
  private RiZoneSortie OBJ_51_OBJ_51;
  private RiZoneSortie OBJ_54_OBJ_54;
  private RiZoneSortie OBJ_57_OBJ_57;
  private RiZoneSortie OBJ_60_OBJ_60;
  private RiZoneSortie OBJ_63_OBJ_63;
  private RiZoneSortie OBJ_66_OBJ_66;
  private RiZoneSortie OBJ_69_OBJ_69;
  private RiZoneSortie OBJ_72_OBJ_72;
  private RiZoneSortie OBJ_75_OBJ_75;
  private JLabel OBJ_78_OBJ_78;
  private JLabel OBJ_40_OBJ_40;
  private JLabel OBJ_77_OBJ_77;
  private XRiTextField EJZ12C;
  private XRiTextField EJZ13C;
  private XRiTextField EJZ21C;
  private XRiTextField EJZ22C;
  private XRiTextField EJZ23C;
  private XRiTextField EJZ31C;
  private XRiTextField EJZ32C;
  private XRiTextField EJZ33C;
  private XRiTextField EJZ41C;
  private XRiTextField EJZ42C;
  private XRiTextField EJZ43C;
  private XRiTextField EJZ11C;
  private JLabel OBJ_39_OBJ_39;
  private XRiTextField EJZ11J;
  private XRiTextField EJZ12J;
  private XRiTextField EJZ13J;
  private XRiTextField EJZ21J;
  private XRiTextField EJZ22J;
  private XRiTextField EJZ23J;
  private XRiTextField EJZ31J;
  private XRiTextField EJZ32J;
  private XRiTextField EJZ33J;
  private XRiTextField EJZ41J;
  private XRiTextField EJZ42J;
  private XRiTextField EJZ43J;
  private JLabel OBJ_44_OBJ_44;
  private JLabel OBJ_47_OBJ_47;
  private JLabel OBJ_50_OBJ_50;
  private JLabel OBJ_41_OBJ_41;
  private JLabel OBJ_53_OBJ_53;
  private JLabel OBJ_56_OBJ_56;
  private JLabel OBJ_59_OBJ_59;
  private JLabel OBJ_62_OBJ_62;
  private JLabel OBJ_65_OBJ_65;
  private JLabel OBJ_68_OBJ_68;
  private JLabel OBJ_71_OBJ_71;
  private JLabel OBJ_74_OBJ_74;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
