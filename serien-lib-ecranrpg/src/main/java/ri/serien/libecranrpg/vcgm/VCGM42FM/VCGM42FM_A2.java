
package ri.serien.libecranrpg.vcgm.VCGM42FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.outil.font.CustomFont;

/**
 * @author Stéphane Vénéri
 */
public class VCGM42FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "<html><font face='monospaced' color='#000000' size='3'>@HLD01@</font></html>", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 549, };
  private Color[][] _LIST_Text_Color = new Color[20][1];
  private Font[][] police = new Font[20][1];
  private Color couleurLiti = new Color(255, 100, 0);
  private Font normal = CustomFont.loadFont("fonts/VeraMono.ttf", 12);// new Font("Courier New", Font.PLAIN, 12);
  private Font gras = CustomFont.loadFont("fonts/VeraMoBd.ttf", 12); // new Font("Courier New", Font.BOLD, 12);
  
  public VCGM42FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, _LIST_Text_Color, null, police);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDBDO.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDBDO@")).trim());
    INDREL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDREL@")).trim());
    OBJ_56.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@JOLIB@")).trim());
    MTREMI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MTREMI@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    INDBDO.setEnabled(lexique.isPresent("INDBDO"));
    INDETB.setVisible(lexique.isPresent("INDETB"));
    INDREL.setEnabled(lexique.isPresent("INDREL"));
    
    OBJ_55.setVisible(lexique.isPresent("V01F"));
    OBJ_56.setVisible(lexique.isPresent("JOLIB"));
    OBJ_13.setVisible(lexique.isPresent("WTP01"));
    CHOISIR.setVisible(lexique.isPresent("WTP01"));
    OBJ_66.setVisible(lexique.isTrue("44"));
    
    // Couleurs de la liste si "!" en position 8 de la ligne (litige)
    for (int i = 0; i < 20; i++) {
      // si ligne en litige
      if ((lexique.HostFieldGetData("LD" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))).substring(7, 8).equalsIgnoreCase("!"))) {
        _LIST_Text_Color[i][0] = couleurLiti;
        police[i][0] = gras;
      }
      else {
        _LIST_Text_Color[i][0] = Color.BLACK;
        police[i][0] = normal;
      }
    }
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("+");
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("-");
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      int i = WTP01.getSelectedRow();
      if ((lexique.HostFieldGetData("LD" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))).contains("OUI"))) {
        WTP01.setValeurTop("-");
      }
      else {
        WTP01.setValeurTop("+");
      }
      lexique.HostScreenSendKey(this, "Enter", false);
    }
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("C");
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_32 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_55 = new JLabel();
    INDBDO = new RiZoneSortie();
    OBJ_57 = new JLabel();
    INDREL = new RiZoneSortie();
    OBJ_56 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_58 = new JLabel();
    OBJ_59 = new JLabel();
    INDDVL = new XRiCalendrier();
    INDDRG = new XRiCalendrier();
    OBJ_62 = new JLabel();
    MTREMI = new RiZoneSortie();
    OBJ_60 = new JLabel();
    OBJ_61 = new JLabel();
    OBJ_63 = new JLabel();
    WDECX = new XRiCalendrier();
    WDNOM = new XRiTextField();
    WNCG = new XRiTextField();
    WNCA = new XRiTextField();
    OBJ_64 = new JLabel();
    MTTDEB = new XRiTextField();
    MTTFIN = new XRiTextField();
    OBJ_65 = new JLabel();
    OBJ_66 = new JLabel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(980, 610));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Relev\u00e9 de banque");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(800, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_32 ----
          OBJ_32.setText("Etablissement");
          OBJ_32.setName("OBJ_32");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");

          //---- OBJ_55 ----
          OBJ_55.setText("Banque");
          OBJ_55.setName("OBJ_55");

          //---- INDBDO ----
          INDBDO.setComponentPopupMenu(BTD);
          INDBDO.setOpaque(false);
          INDBDO.setText("@INDBDO@");
          INDBDO.setName("INDBDO");

          //---- OBJ_57 ----
          OBJ_57.setText("Relev\u00e9");
          OBJ_57.setName("OBJ_57");

          //---- INDREL ----
          INDREL.setComponentPopupMenu(BTD);
          INDREL.setOpaque(false);
          INDREL.setText("@INDREL@");
          INDREL.setName("INDREL");

          //---- OBJ_56 ----
          OBJ_56.setText("@JOLIB@");
          OBJ_56.setOpaque(false);
          OBJ_56.setName("OBJ_56");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(50, 50, 50)
                    .addComponent(INDBDO, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)))
                .addGap(21, 21, 21)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(50, 50, 50)
                    .addComponent(INDREL, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(INDBDO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(INDREL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Dettes domicili\u00e9es ");
              riSousMenu_bt6.setToolTipText("Affichage dettes domicili\u00e9es et non r\u00e9gl\u00e9es");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(780, 520));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_58 ----
            OBJ_58.setText("Date de valeur");
            OBJ_58.setName("OBJ_58");
            panel1.add(OBJ_58);
            OBJ_58.setBounds(425, 20, 135, 28);

            //---- OBJ_59 ----
            OBJ_59.setText("Date de r\u00e8glement");
            OBJ_59.setName("OBJ_59");
            panel1.add(OBJ_59);
            OBJ_59.setBounds(425, 55, 135, 28);

            //---- INDDVL ----
            INDDVL.setComponentPopupMenu(BTD);
            INDDVL.setName("INDDVL");
            panel1.add(INDDVL);
            INDDVL.setBounds(565, 20, 105, INDDVL.getPreferredSize().height);

            //---- INDDRG ----
            INDDRG.setComponentPopupMenu(BTD);
            INDDRG.setName("INDDRG");
            panel1.add(INDDRG);
            INDDRG.setBounds(565, 55, 105, INDDRG.getPreferredSize().height);

            //---- OBJ_62 ----
            OBJ_62.setText("Montant du relev\u00e9");
            OBJ_62.setName("OBJ_62");
            panel1.add(OBJ_62);
            OBJ_62.setBounds(425, 90, 135, 28);

            //---- MTREMI ----
            MTREMI.setComponentPopupMenu(BTD);
            MTREMI.setText("@MTREMI@");
            MTREMI.setHorizontalAlignment(SwingConstants.RIGHT);
            MTREMI.setName("MTREMI");
            panel1.add(MTREMI);
            MTREMI.setBounds(565, 92, 95, MTREMI.getPreferredSize().height);

            //---- OBJ_60 ----
            OBJ_60.setText("Num\u00e9ro de compte");
            OBJ_60.setName("OBJ_60");
            panel1.add(OBJ_60);
            OBJ_60.setBounds(20, 20, 135, 28);

            //---- OBJ_61 ----
            OBJ_61.setText("Recherche alphab\u00e9tique");
            OBJ_61.setName("OBJ_61");
            panel1.add(OBJ_61);
            OBJ_61.setBounds(20, 55, 145, 28);

            //---- OBJ_63 ----
            OBJ_63.setText("Date d'\u00e9ch\u00e9ance");
            OBJ_63.setName("OBJ_63");
            panel1.add(OBJ_63);
            OBJ_63.setBounds(20, 125, 135, 28);

            //---- WDECX ----
            WDECX.setComponentPopupMenu(BTD);
            WDECX.setName("WDECX");
            panel1.add(WDECX);
            WDECX.setBounds(180, 125, 105, WDECX.getPreferredSize().height);

            //---- WDNOM ----
            WDNOM.setName("WDNOM");
            panel1.add(WDNOM);
            WDNOM.setBounds(180, 55, 160, WDNOM.getPreferredSize().height);

            //---- WNCG ----
            WNCG.setName("WNCG");
            panel1.add(WNCG);
            WNCG.setBounds(180, 20, 60, WNCG.getPreferredSize().height);

            //---- WNCA ----
            WNCA.setName("WNCA");
            panel1.add(WNCA);
            WNCA.setBounds(245, 20, 60, WNCA.getPreferredSize().height);

            //---- OBJ_64 ----
            OBJ_64.setText("Recherche par montant de");
            OBJ_64.setName("OBJ_64");
            panel1.add(OBJ_64);
            OBJ_64.setBounds(20, 90, 150, 28);

            //---- MTTDEB ----
            MTTDEB.setName("MTTDEB");
            panel1.add(MTTDEB);
            MTTDEB.setBounds(180, 90, 100, MTTDEB.getPreferredSize().height);

            //---- MTTFIN ----
            MTTFIN.setName("MTTFIN");
            panel1.add(MTTFIN);
            MTTFIN.setBounds(295, 90, 100, MTTFIN.getPreferredSize().height);

            //---- OBJ_65 ----
            OBJ_65.setText("\u00e0");
            OBJ_65.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_65.setName("OBJ_65");
            panel1.add(OBJ_65);
            OBJ_65.setBounds(280, 90, 15, 28);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //---- OBJ_66 ----
          OBJ_66.setText("ATTENTION : relev\u00e9 d\u00e9j\u00e0 comptabilis\u00e9");
          OBJ_66.setForeground(new Color(204, 0, 51));
          OBJ_66.setName("OBJ_66");

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");

          //======== SCROLLPANE_LIST ========
          {
            SCROLLPANE_LIST.setComponentPopupMenu(BTD);
            SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

            //---- WTP01 ----
            WTP01.setComponentPopupMenu(BTD);
            WTP01.setName("WTP01");
            WTP01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                WTP01MouseClicked(e);
              }
            });
            SCROLLPANE_LIST.setViewportView(WTP01);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 703, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createParallelGroup()
                    .addComponent(OBJ_66, GroupLayout.PREFERRED_SIZE, 568, GroupLayout.PREFERRED_SIZE)
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 666, GroupLayout.PREFERRED_SIZE)
                      .addGap(9, 9, 9)
                      .addGroup(p_contenuLayout.createParallelGroup()
                        .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                        .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)))))
                .addGap(49, 49, 49))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 167, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_66, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
                    .addGap(20, 20, 20)
                    .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE))))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Ajouter la dette \u00e0 la remise");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_13 ----
      OBJ_13.setText("Enlever la dette \u00e0 la remise");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("Vue des \u00e9critures comptables");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_32;
  private RiZoneSortie INDETB;
  private JLabel OBJ_55;
  private RiZoneSortie INDBDO;
  private JLabel OBJ_57;
  private RiZoneSortie INDREL;
  private RiZoneSortie OBJ_56;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_58;
  private JLabel OBJ_59;
  private XRiCalendrier INDDVL;
  private XRiCalendrier INDDRG;
  private JLabel OBJ_62;
  private RiZoneSortie MTREMI;
  private JLabel OBJ_60;
  private JLabel OBJ_61;
  private JLabel OBJ_63;
  private XRiCalendrier WDECX;
  private XRiTextField WDNOM;
  private XRiTextField WNCG;
  private XRiTextField WNCA;
  private JLabel OBJ_64;
  private XRiTextField MTTDEB;
  private XRiTextField MTTFIN;
  private JLabel OBJ_65;
  private JLabel OBJ_66;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
