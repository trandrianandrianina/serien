/*
 * Created by JFormDesigner on Wed Feb 24 16:24:25 CET 2010
 */

package ri.serien.libecranrpg.vcgm.VCGM31FM;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.WindowConstants;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.DefaultKeyedValues;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.general.DatasetUtilities;

import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composantrpg.lexical.RiGraphe;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * @author Stéphane Vénéri
 * 
 * 
 *         GRAPHE DE COMPARAISON DE L'EVOLUTION DE 2 ANNEES
 *         
 */
public class VCGM31FM_TT extends JFrame {
  private Lexical lexique = null;
  private iData interpreteurD = null;
  private JPanel master = null;
  
  String[] annees = new String[5];
  String[] donnees = new String[5];
  String[] variation = new String[4];
  private RiGraphe graphe = null;
  // private String[] moisLib={"Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre",
  // "Octobre", "Novembre", "Décembre"};
  
  /**
   * Constructeur
   */
  public VCGM31FM_TT(JPanel panel, Lexical lex, iData iD) {
    master = panel;
    lexique = lex;
    interpreteurD = iD;
    initComponents();
    initData();
    setSize(1000, 700);
    setVisible(true);
  }
  
  /**
   * Initialise les données pour les graphes
   *
   */
  private void initData() {
    // Préparation des données
    DefaultKeyedValues data = new DefaultKeyedValues();
    DefaultKeyedValues data2 = new DefaultKeyedValues();
    
    // Chargement des années
    String chaine = lexique.HostFieldGetData("JIT");
    if (chaine.length() >= 6) {
      annees[0] = chaine.substring(1, 6).trim();
      if (chaine.length() >= 12) {
        annees[1] = chaine.substring(7, 12).trim();
        if (chaine.length() >= 18) {
          annees[2] = chaine.substring(13, 18).trim();
          if (chaine.length() >= 24) {
            annees[3] = chaine.substring(19, 24).trim();
          }
          else {
            annees[3] = chaine.substring(19).trim();
            if (chaine.length() >= 30) {
              annees[4] = chaine.substring(25, 30).trim();
            }
            else {
              annees[4] = chaine.substring(25).trim();
            }
          }
        }
        else {
          annees[2] = chaine.substring(13).trim();
        }
      }
      else {
        annees[1] = chaine.substring(7).trim();
      }
    }
    else {
      annees[0] = chaine.trim();
    }
    
    // Chargement des données
    for (int i = 0; i < donnees.length; i++) {
      donnees[i] = lexique.HostFieldGetNumericData("JWA0" + (i + 1)).trim();
    }
    
    for (int i = 0; i < variation.length; i++) {
      variation[i] = lexique.HostFieldGetNumericData("RA0" + (i + 1)).trim();
    }
    
    for (int an = 0; an < 5; an++) {
      data.addValue(annees[an], Double.parseDouble(donnees[an]));
      data2.addValue(annees[an], Double.parseDouble(variation[an]));
    }
    
    CategoryDataset dataset = DatasetUtilities.createCategoryDataset(annees[0], data);
    CategoryDataset dataset2 = DatasetUtilities.createCategoryDataset(annees[1], data2);
    
    CategoryDataset[] d = { dataset, dataset2 };
    
    JFreeChart graphe = createChart(d);
    
    l_graphe.setIcon(new ImageIcon(graphe.createBufferedImage(l_graphe.getWidth(), l_graphe.getHeight())));
    
  }
  
  public static JFreeChart createChart(CategoryDataset[] datasets) {
    // create the chart...
    
    JFreeChart chart = ChartFactory.createBarChart("Cinq dernières années d'évolution mensuelle pour les comptes sélectionnés", // chart
                                                                                                                                // title
        "mois", // domain axis label
        " ", // range axis label
        datasets[0], // data
        PlotOrientation.VERTICAL, true, // include legend
        true, false);
    
    CategoryPlot plot = (CategoryPlot) chart.getPlot();
    CategoryAxis domainAxis = plot.getDomainAxis();
    
    domainAxis.setLowerMargin(0.02);
    domainAxis.setUpperMargin(0.02);
    domainAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_90);
    
    LineAndShapeRenderer renderer1 = new LineAndShapeRenderer();
    LineAndShapeRenderer renderer2 = new LineAndShapeRenderer();
    
    NumberAxis axis1 = new NumberAxis("");
    // axis2.setNumberFormatOverride(NumberFormat.getPercentInstance());
    plot.setRangeAxis(0, axis1);
    plot.setDataset(0, datasets[0]);
    plot.setRenderer(0, renderer1);
    plot.mapDatasetToRangeAxis(0, 0);
    plot.getRangeAxis(0).setVisible(false);
    axis1.setLabelPaint(Color.magenta);
    axis1.setTickLabelPaint(Color.magenta);
    
    NumberAxis axis2 = new NumberAxis("");
    // axis2.setNumberFormatOverride(NumberFormat.getPercentInstance());
    plot.setRangeAxis(1, axis2);
    plot.setDataset(1, datasets[1]);
    plot.setRenderer(1, renderer2);
    plot.mapDatasetToRangeAxis(1, 1);
    plot.getRangeAxis(1).setVisible(false);
    axis2.setLabelPaint(Color.red);
    axis2.setTickLabelPaint(Color.red);
    
    plot.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);
    
    // ChartUtilities.applyCurrentTheme(chart);
    
    return chart;
  }
  
  private void MI_CopierActionPerformed(ActionEvent e) {
    graphe.sendToClipBoard(l_graphe.getWidth(), l_graphe.getHeight());
  }
  
  private void MI_EnregistrerActionPerformed(ActionEvent e) {
    graphe.saveGraphe(null, l_graphe.getWidth(), l_graphe.getHeight());
  }
  
  private void thisComponentResized(ComponentEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    l_graphe = new JLabel();
    BTD = new JPopupMenu();
    MI_Copier = new JMenuItem();
    MI_Enregistrer = new JMenuItem();
    
    // ======== this ========
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setTitle("Analyse des comptes");
    setIconImage(null);
    setName("this");
    addComponentListener(new ComponentAdapter() {
      @Override
      public void componentResized(ComponentEvent e) {
        thisComponentResized(e);
      }
    });
    Container contentPane = getContentPane();
    contentPane.setLayout(null);
    
    // ---- l_graphe ----
    l_graphe.setComponentPopupMenu(BTD);
    l_graphe.setName("l_graphe");
    contentPane.add(l_graphe);
    l_graphe.setBounds(1, 0, 920, 620);
    
    {
      // compute preferred size
      Dimension preferredSize = new Dimension();
      for (int i = 0; i < contentPane.getComponentCount(); i++) {
        Rectangle bounds = contentPane.getComponent(i).getBounds();
        preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
        preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
      }
      Insets insets = contentPane.getInsets();
      preferredSize.width += insets.right;
      preferredSize.height += insets.bottom;
      contentPane.setMinimumSize(preferredSize);
      contentPane.setPreferredSize(preferredSize);
    }
    pack();
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- MI_Copier ----
      MI_Copier.setText("Copier");
      MI_Copier.setName("MI_Copier");
      MI_Copier.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          MI_CopierActionPerformed(e);
        }
      });
      BTD.add(MI_Copier);
      
      // ---- MI_Enregistrer ----
      MI_Enregistrer.setText("Enregistrer sous");
      MI_Enregistrer.setName("MI_Enregistrer");
      MI_Enregistrer.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          MI_EnregistrerActionPerformed(e);
        }
      });
      BTD.add(MI_Enregistrer);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel l_graphe;
  private JPopupMenu BTD;
  private JMenuItem MI_Copier;
  private JMenuItem MI_Enregistrer;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
