
package ri.serien.libecranrpg.vcgm.VCGM10FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VCGM10FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _LDR01_Title = { "", "", "H3", "LIBMT2", "Echéance", "Règlement", "Préimputation", };
  private String[][] _LDR01_Data = { { "LDR01", "DDR01", "NIV01", "MTT01", "ECH01", "RGL01", "PRI01", },
      { "LDR02", "DDR02", "NIV02", "MTT02", "ECH02", "RGL02", "PRI02", },
      { "LDR03", "DDR03", "NIV03", "MTT03", "ECH03", "RGL03", "PRI03", },
      { "LDR04", "DDR04", "NIV04", "MTT04", "ECH04", "RGL04", "PRI04", }, };
  private int[] _LDR01_Width = { 81, 67, 24, 109, 72, 85, 97, };
  // private String[] _LECHEANCE_Top=null;
  
  public VCGM10FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    LDR01.setAspectTable(null, _LDR01_Title, _LDR01_Data, _LDR01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBCPT@")).trim());
    OBJ_25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OBS@")).trim());
    OBJ_86.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYPE@")).trim());
    OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMT1@")).trim());
    OBJ_52.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1SNS@")).trim());
    OBJ_57.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1DEV@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LECHEANCE, LECHEANCE.get_LIST_Title_Data_Brut(), _LECHEANCE_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    L1CLB.setEnabled(lexique.isPresent("L1CLB"));
    L1LIB7.setEnabled(lexique.isPresent("L1LIB7"));
    L1LIB6.setEnabled(lexique.isPresent("L1LIB6"));
    L1SAN.setVisible(lexique.isPresent("L1SAN"));
    WAFFAC.setVisible(lexique.isPresent("WAFFAC"));
    L1NCA.setVisible(lexique.isPresent("L1NCA"));
    L1NCG.setVisible(lexique.isPresent("L1NCG"));
    L1RLV.setEnabled(lexique.isPresent("L1RLV"));
    L1PCE.setVisible(lexique.isPresent("L1PCE"));
    OBJ_55.setEnabled(lexique.isPresent("L1QTE"));
    L1AA6.setVisible(lexique.isPresent("L1AA6"));
    L1AA5.setVisible(lexique.isPresent("L1AA5"));
    L1AA4.setVisible(lexique.isPresent("L1AA4"));
    L1AA3.setVisible(lexique.isPresent("L1AA3"));
    L1AA2.setVisible(lexique.isPresent("L1AA2"));
    L1AA1.setVisible(lexique.isPresent("L1AA1"));
    L1NAT.setVisible(lexique.isPresent("L1NAT"));
    L1ACT.setVisible(lexique.isPresent("L1ACT"));
    OBJ_57.setEnabled(lexique.isPresent("L1MTD"));
    OBJ_46.setEnabled(lexique.isPresent("L1RLV"));
    L1DVBX.setEnabled(lexique.isPresent("L1DVBX"));
    OBJ_39.setEnabled(lexique.isPresent("L1NCPX"));
    L1NCPX.setVisible(lexique.isPresent("L1NCPX"));
    OBJ_37.setEnabled(lexique.isPresent("L1RFC"));
    OBJ_43.setEnabled(lexique.isPresent("L1DVBX"));
    L1LIB8.setEnabled(lexique.isPresent("L1LIB8"));
    L1MTD.setEnabled(lexique.isPresent("L1MTD"));
    L1MTT.setEnabled(lexique.isPresent("L1MTT"));
    OBJ_52.setEnabled(lexique.isPresent("LIBMT1"));
    L1LIB5.setEnabled(lexique.isPresent("L1LIB5"));
    L1QTE.setEnabled(lexique.isPresent("L1QTE"));
    OBJ_86.setVisible(lexique.isPresent("LIBTYF"));
    L1RFC.setEnabled(lexique.isPresent("L1RFC"));
    OBJ_42.setEnabled(lexique.isPresent("L1PCE"));
    DVCHG.setEnabled(lexique.isPresent("DVCHG"));
    LIBTYF.setVisible(lexique.isPresent("LIBTYF"));
    OBJ_29.setVisible(lexique.isPresent("LIBCPT"));
    LDR01.setVisible(lexique.HostFieldGetData("H3").equalsIgnoreCase("N"));
    
    if (lexique.isTrue("(N72) AND (N74)")) {
      setPreferredSize(new Dimension(1115, 370));
    }
    else {
      setPreferredSize(new Dimension(1115, 520));
    }
    
    xTitledPanel4.setVisible(lexique.isTrue("72"));
    xTitledPanel6.setVisible(lexique.isTrue("72"));
    xTitledPanel3.setVisible(lexique.isTrue("74"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void OBJ_86ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(20, 40);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_82ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_29 = new RiZoneSortie();
    OBJ_42 = new JLabel();
    L1RFC = new XRiTextField();
    L1LIB5 = new XRiTextField();
    L1LIB8 = new XRiTextField();
    OBJ_26 = new JLabel();
    OBJ_43 = new JLabel();
    OBJ_37 = new JLabel();
    L1NCPX = new XRiTextField();
    OBJ_39 = new JLabel();
    L1DVBX = new XRiTextField();
    OBJ_25 = new RiZoneSortie();
    OBJ_46 = new JLabel();
    L1PCE = new XRiTextField();
    L1RLV = new XRiTextField();
    L1NCG = new XRiTextField();
    L1NCA = new XRiTextField();
    WAFFAC = new XRiTextField();
    L1LIB6 = new XRiTextField();
    OBJ_30 = new JLabel();
    L1LIB7 = new XRiTextField();
    OBJ_31 = new JLabel();
    L1CLB = new XRiTextField();
    OBJ_86 = new JButton();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_50 = new JLabel();
    OBJ_55 = new JLabel();
    L1MTT = new XRiTextField();
    L1MTD = new XRiTextField();
    OBJ_52 = new RiZoneSortie();
    OBJ_57 = new RiZoneSortie();
    OBJ_53 = new JLabel();
    OBJ_58 = new JLabel();
    L1QTE = new XRiTextField();
    DVCHG = new XRiTextField();
    xTitledPanel6 = new JXTitledPanel();
    OBJ_24 = new JLabel();
    L1SAN = new XRiTextField();
    OBJ_71 = new JLabel();
    L1ACT = new XRiTextField();
    OBJ_73 = new JLabel();
    L1NAT = new XRiTextField();
    xTitledPanel4 = new JXTitledPanel();
    L1AA1 = new XRiTextField();
    L1AA2 = new XRiTextField();
    OBJ_82 = new JButton();
    LIBTYF = new XRiTextField();
    L1AA3 = new XRiTextField();
    L1AA4 = new XRiTextField();
    L1AA5 = new XRiTextField();
    L1AA6 = new XRiTextField();
    xTitledPanel3 = new JXTitledPanel();
    SCROLLPANE_LECHEANCE = new JScrollPane();
    LDR01 = new XRiTable();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1115, 520));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Acc\u00e8s aux \u00e9ch\u00e9ances");
            riSousMenu_bt6.setToolTipText("Acc\u00e8s aux \u00e9ch\u00e9ances");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Vue de la facture");
            riSousMenu_bt7.setToolTipText("Vue de la facture");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);

          //======== riMenu3 ========
          {
            riMenu3.setName("riMenu3");

            //---- riMenu_bt3 ----
            riMenu_bt3.setText("Outils");
            riMenu_bt3.setName("riMenu_bt3");
            riMenu3.add(riMenu_bt3);
          }
          menus_haut.add(riMenu3);

          //======== riSousMenu14 ========
          {
            riSousMenu14.setName("riSousMenu14");

            //---- riSousMenu_bt14 ----
            riSousMenu_bt14.setText("Documents li\u00e9s");
            riSousMenu_bt14.setToolTipText("Documents li\u00e9s");
            riSousMenu_bt14.setName("riSousMenu_bt14");
            riSousMenu_bt14.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt14ActionPerformed(e);
              }
            });
            riSousMenu14.add(riSousMenu_bt14);
          }
          menus_haut.add(riSousMenu14);

          //======== riSousMenu15 ========
          {
            riSousMenu15.setName("riSousMenu15");

            //---- riSousMenu_bt15 ----
            riSousMenu_bt15.setText("Bloc-notes");
            riSousMenu_bt15.setToolTipText("Bloc-notes");
            riSousMenu_bt15.setName("riSousMenu_bt15");
            riSousMenu_bt15.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt15ActionPerformed(e);
              }
            });
            riSousMenu15.add(riSousMenu_bt15);
          }
          menus_haut.add(riSousMenu15);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== xTitledPanel2 ========
        {
          xTitledPanel2.setTitle("Ecriture");
          xTitledPanel2.setBorder(new DropShadowBorder());
          xTitledPanel2.setName("xTitledPanel2");
          Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();

          //---- OBJ_29 ----
          OBJ_29.setText("@LIBCPT@");
          OBJ_29.setName("OBJ_29");

          //---- OBJ_42 ----
          OBJ_42.setText("Num\u00e9ro de pi\u00e8ce");
          OBJ_42.setName("OBJ_42");

          //---- L1RFC ----
          L1RFC.setComponentPopupMenu(BTD);
          L1RFC.setName("L1RFC");

          //---- L1LIB5 ----
          L1LIB5.setComponentPopupMenu(BTD);
          L1LIB5.setName("L1LIB5");

          //---- L1LIB8 ----
          L1LIB8.setComponentPopupMenu(BTD);
          L1LIB8.setName("L1LIB8");

          //---- OBJ_26 ----
          OBJ_26.setText("Compte / Tiers");
          OBJ_26.setName("OBJ_26");

          //---- OBJ_43 ----
          OBJ_43.setText("Date de valeur");
          OBJ_43.setName("OBJ_43");

          //---- OBJ_37 ----
          OBJ_37.setText("R\u00e9f\u00e9rence classement");
          OBJ_37.setName("OBJ_37");

          //---- L1NCPX ----
          L1NCPX.setComponentPopupMenu(BTD);
          L1NCPX.setName("L1NCPX");

          //---- OBJ_39 ----
          OBJ_39.setText("Contre partie");
          OBJ_39.setName("OBJ_39");

          //---- L1DVBX ----
          L1DVBX.setComponentPopupMenu(BTD);
          L1DVBX.setName("L1DVBX");

          //---- OBJ_25 ----
          OBJ_25.setText("@OBS@");
          OBJ_25.setName("OBJ_25");

          //---- OBJ_46 ----
          OBJ_46.setText("Relev\u00e9");
          OBJ_46.setName("OBJ_46");

          //---- L1PCE ----
          L1PCE.setComponentPopupMenu(BTD);
          L1PCE.setName("L1PCE");

          //---- L1RLV ----
          L1RLV.setComponentPopupMenu(BTD);
          L1RLV.setName("L1RLV");

          //---- L1NCG ----
          L1NCG.setName("L1NCG");

          //---- L1NCA ----
          L1NCA.setName("L1NCA");

          //---- WAFFAC ----
          WAFFAC.setComponentPopupMenu(BTD);
          WAFFAC.setName("WAFFAC");

          //---- L1LIB6 ----
          L1LIB6.setComponentPopupMenu(BTD);
          L1LIB6.setName("L1LIB6");

          //---- OBJ_30 ----
          OBJ_30.setText("Mon");
          OBJ_30.setName("OBJ_30");

          //---- L1LIB7 ----
          L1LIB7.setComponentPopupMenu(BTD);
          L1LIB7.setName("L1LIB7");

          //---- OBJ_31 ----
          OBJ_31.setText("Eco");
          OBJ_31.setName("OBJ_31");

          //---- L1CLB ----
          L1CLB.setToolTipText("Code libell\u00e9");
          L1CLB.setComponentPopupMenu(BTD);
          L1CLB.setName("L1CLB");

          //---- OBJ_86 ----
          OBJ_86.setText("@TYPE@");
          OBJ_86.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_86.setName("OBJ_86");
          OBJ_86.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_86ActionPerformed(e);
            }
          });

          GroupLayout xTitledPanel2ContentContainerLayout = new GroupLayout(xTitledPanel2ContentContainer);
          xTitledPanel2ContentContainer.setLayout(xTitledPanel2ContentContainerLayout);
          xTitledPanel2ContentContainerLayout.setHorizontalGroup(
            xTitledPanel2ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addComponent(OBJ_26, GroupLayout.PREFERRED_SIZE, 91, GroupLayout.PREFERRED_SIZE)
                .addGap(79, 79, 79)
                .addComponent(L1NCG, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(L1NCA, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 289, GroupLayout.PREFERRED_SIZE)
                .addGap(186, 186, 186)
                .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 72, GroupLayout.PREFERRED_SIZE))
              .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                .addGap(207, 207, 207)
                .addComponent(L1LIB5, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(40, 40, 40)
                    .addComponent(L1LIB7, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addComponent(L1LIB6, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                .addComponent(L1LIB8, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(L1CLB, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(300, 300, 300)
                .addComponent(OBJ_86, GroupLayout.PREFERRED_SIZE, 72, GroupLayout.PREFERRED_SIZE))
              .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                  .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 173, GroupLayout.PREFERRED_SIZE)
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(170, 170, 170)
                    .addComponent(L1RFC, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)))
                .addGap(235, 235, 235)
                .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE)
                .addGap(71, 71, 71)
                .addComponent(L1NCPX, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
              .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE)
                .addGap(62, 62, 62)
                .addComponent(L1PCE, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
                .addGap(279, 279, 279)
                .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 91, GroupLayout.PREFERRED_SIZE)
                .addGap(59, 59, 59)
                .addComponent(L1DVBX, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE))
              .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                .addGap(207, 207, 207)
                .addComponent(WAFFAC, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(325, 325, 325)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
                .addGap(84, 84, 84)
                .addComponent(L1RLV, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE))
          );
          xTitledPanel2ContentContainerLayout.setVerticalGroup(
            xTitledPanel2ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_26, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(L1NCG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(L1NCA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(1, 1, 1)
                .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(L1LIB5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                      .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                      .addComponent(L1LIB7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(L1LIB6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(L1LIB8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(L1CLB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_86, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)))
                .addGap(1, 1, 1)
                .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                  .addComponent(L1RFC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(L1NCPX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3)
                .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(L1PCE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(L1DVBX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3)
                .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                  .addComponent(WAFFAC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(L1RLV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_contenu.add(xTitledPanel2);
        xTitledPanel2.setBounds(10, 10, 924, 230);

        //======== xTitledPanel1 ========
        {
          xTitledPanel1.setTitle("Montant");
          xTitledPanel1.setBorder(new DropShadowBorder());
          xTitledPanel1.setName("xTitledPanel1");
          Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

          //---- OBJ_50 ----
          OBJ_50.setText("@LIBMT1@");
          OBJ_50.setName("OBJ_50");

          //---- OBJ_55 ----
          OBJ_55.setText("Devise");
          OBJ_55.setName("OBJ_55");

          //---- L1MTT ----
          L1MTT.setComponentPopupMenu(BTD);
          L1MTT.setName("L1MTT");

          //---- L1MTD ----
          L1MTD.setComponentPopupMenu(BTD);
          L1MTD.setName("L1MTD");

          //---- OBJ_52 ----
          OBJ_52.setText("@L1SNS@");
          OBJ_52.setName("OBJ_52");

          //---- OBJ_57 ----
          OBJ_57.setText("@L1DEV@");
          OBJ_57.setName("OBJ_57");

          //---- OBJ_53 ----
          OBJ_53.setText("Quantit\u00e9");
          OBJ_53.setName("OBJ_53");

          //---- OBJ_58 ----
          OBJ_58.setText("Taux");
          OBJ_58.setName("OBJ_58");

          //---- L1QTE ----
          L1QTE.setComponentPopupMenu(BTD);
          L1QTE.setName("L1QTE");

          //---- DVCHG ----
          DVCHG.setComponentPopupMenu(BTD);
          DVCHG.setName("DVCHG");

          GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
          xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
          xTitledPanel1ContentContainerLayout.setHorizontalGroup(
            xTitledPanel1ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
                    .addGap(73, 73, 73)
                    .addComponent(L1MTT, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
                    .addGap(85, 85, 85)
                    .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(L1QTE, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
                    .addGap(106, 106, 106)
                    .addComponent(L1MTD, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
                    .addGap(85, 85, 85)
                    .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(DVCHG, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE))))
          );
          xTitledPanel1ContentContainerLayout.setVerticalGroup(
            xTitledPanel1ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(L1MTT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(L1QTE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(L1MTD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(DVCHG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_contenu.add(xTitledPanel1);
        xTitledPanel1.setBounds(10, 250, 924, 108);

        //======== xTitledPanel6 ========
        {
          xTitledPanel6.setTitle("Affectation analytique");
          xTitledPanel6.setBorder(new DropShadowBorder());
          xTitledPanel6.setName("xTitledPanel6");
          Container xTitledPanel6ContentContainer = xTitledPanel6.getContentContainer();

          //---- OBJ_24 ----
          OBJ_24.setText("Section");
          OBJ_24.setName("OBJ_24");

          //---- L1SAN ----
          L1SAN.setComponentPopupMenu(BTD);
          L1SAN.setName("L1SAN");

          //---- OBJ_71 ----
          OBJ_71.setText("Affaire");
          OBJ_71.setName("OBJ_71");

          //---- L1ACT ----
          L1ACT.setComponentPopupMenu(BTD);
          L1ACT.setName("L1ACT");

          //---- OBJ_73 ----
          OBJ_73.setText("Nature");
          OBJ_73.setName("OBJ_73");

          //---- L1NAT ----
          L1NAT.setName("L1NAT");

          GroupLayout xTitledPanel6ContentContainerLayout = new GroupLayout(xTitledPanel6ContentContainer);
          xTitledPanel6ContentContainer.setLayout(xTitledPanel6ContentContainerLayout);
          xTitledPanel6ContentContainerLayout.setHorizontalGroup(
            xTitledPanel6ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel6ContentContainerLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(xTitledPanel6ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel6ContentContainerLayout.createSequentialGroup()
                    .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                    .addGap(73, 73, 73)
                    .addComponent(OBJ_71, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
                    .addGap(73, 73, 73)
                    .addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel6ContentContainerLayout.createSequentialGroup()
                    .addComponent(L1SAN, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addGap(99, 99, 99)
                    .addComponent(L1ACT, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
                    .addGap(83, 83, 83)
                    .addComponent(L1NAT, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE))))
          );
          xTitledPanel6ContentContainerLayout.setVerticalGroup(
            xTitledPanel6ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel6ContentContainerLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(xTitledPanel6ContentContainerLayout.createParallelGroup()
                  .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_71, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(xTitledPanel6ContentContainerLayout.createParallelGroup()
                  .addComponent(L1SAN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(L1ACT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(L1NAT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_contenu.add(xTitledPanel6);
        xTitledPanel6.setBounds(10, 365, 430, 140);

        //======== xTitledPanel4 ========
        {
          xTitledPanel4.setTitle("Axes");
          xTitledPanel4.setBorder(new DropShadowBorder());
          xTitledPanel4.setName("xTitledPanel4");
          Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
          xTitledPanel4ContentContainer.setLayout(null);

          //---- L1AA1 ----
          L1AA1.setComponentPopupMenu(BTD);
          L1AA1.setName("L1AA1");
          xTitledPanel4ContentContainer.add(L1AA1);
          L1AA1.setBounds(12, 21, 54, L1AA1.getPreferredSize().height);

          //---- L1AA2 ----
          L1AA2.setComponentPopupMenu(BTD);
          L1AA2.setName("L1AA2");
          xTitledPanel4ContentContainer.add(L1AA2);
          L1AA2.setBounds(82, 21, 54, L1AA2.getPreferredSize().height);

          //---- OBJ_82 ----
          OBJ_82.setText("Type de frais");
          OBJ_82.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_82.setName("OBJ_82");
          OBJ_82.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_82ActionPerformed(e);
            }
          });
          xTitledPanel4ContentContainer.add(OBJ_82);
          OBJ_82.setBounds(12, 61, 100, 25);

          //---- LIBTYF ----
          LIBTYF.setComponentPopupMenu(BTD);
          LIBTYF.setName("LIBTYF");
          xTitledPanel4ContentContainer.add(LIBTYF);
          LIBTYF.setBounds(156, 61, 250, LIBTYF.getPreferredSize().height);

          //---- L1AA3 ----
          L1AA3.setComponentPopupMenu(BTD);
          L1AA3.setName("L1AA3");
          xTitledPanel4ContentContainer.add(L1AA3);
          L1AA3.setBounds(152, 21, 54, L1AA3.getPreferredSize().height);

          //---- L1AA4 ----
          L1AA4.setComponentPopupMenu(BTD);
          L1AA4.setName("L1AA4");
          xTitledPanel4ContentContainer.add(L1AA4);
          L1AA4.setBounds(217, 21, 54, L1AA4.getPreferredSize().height);

          //---- L1AA5 ----
          L1AA5.setComponentPopupMenu(BTD);
          L1AA5.setName("L1AA5");
          xTitledPanel4ContentContainer.add(L1AA5);
          L1AA5.setBounds(287, 21, 54, L1AA5.getPreferredSize().height);

          //---- L1AA6 ----
          L1AA6.setComponentPopupMenu(BTD);
          L1AA6.setName("L1AA6");
          xTitledPanel4ContentContainer.add(L1AA6);
          L1AA6.setBounds(352, 21, 54, L1AA6.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel4ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel4ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel4ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel4ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel4ContentContainer.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(xTitledPanel4);
        xTitledPanel4.setBounds(455, 365, 475, 142);

        //======== xTitledPanel3 ========
        {
          xTitledPanel3.setBorder(new DropShadowBorder());
          xTitledPanel3.setName("xTitledPanel3");
          Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();

          //======== SCROLLPANE_LECHEANCE ========
          {
            SCROLLPANE_LECHEANCE.setToolTipText("Page Up/Dwn \u00e9ch\u00e9ances suppl\u00e9mentaires");
            SCROLLPANE_LECHEANCE.setComponentPopupMenu(BTD);
            SCROLLPANE_LECHEANCE.setName("SCROLLPANE_LECHEANCE");

            //---- LDR01 ----
            LDR01.setName("LDR01");
            SCROLLPANE_LECHEANCE.setViewportView(LDR01);
          }

          GroupLayout xTitledPanel3ContentContainerLayout = new GroupLayout(xTitledPanel3ContentContainer);
          xTitledPanel3ContentContainer.setLayout(xTitledPanel3ContentContainerLayout);
          xTitledPanel3ContentContainerLayout.setHorizontalGroup(
            xTitledPanel3ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(SCROLLPANE_LECHEANCE, GroupLayout.PREFERRED_SIZE, 890, GroupLayout.PREFERRED_SIZE))
          );
          xTitledPanel3ContentContainerLayout.setVerticalGroup(
            xTitledPanel3ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(SCROLLPANE_LECHEANCE, GroupLayout.PREFERRED_SIZE, 92, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_contenu.add(xTitledPanel3);
        xTitledPanel3.setBounds(10, 365, 922, 142);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel2;
  private RiZoneSortie OBJ_29;
  private JLabel OBJ_42;
  private XRiTextField L1RFC;
  private XRiTextField L1LIB5;
  private XRiTextField L1LIB8;
  private JLabel OBJ_26;
  private JLabel OBJ_43;
  private JLabel OBJ_37;
  private XRiTextField L1NCPX;
  private JLabel OBJ_39;
  private XRiTextField L1DVBX;
  private RiZoneSortie OBJ_25;
  private JLabel OBJ_46;
  private XRiTextField L1PCE;
  private XRiTextField L1RLV;
  private XRiTextField L1NCG;
  private XRiTextField L1NCA;
  private XRiTextField WAFFAC;
  private XRiTextField L1LIB6;
  private JLabel OBJ_30;
  private XRiTextField L1LIB7;
  private JLabel OBJ_31;
  private XRiTextField L1CLB;
  private JButton OBJ_86;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_50;
  private JLabel OBJ_55;
  private XRiTextField L1MTT;
  private XRiTextField L1MTD;
  private RiZoneSortie OBJ_52;
  private RiZoneSortie OBJ_57;
  private JLabel OBJ_53;
  private JLabel OBJ_58;
  private XRiTextField L1QTE;
  private XRiTextField DVCHG;
  private JXTitledPanel xTitledPanel6;
  private JLabel OBJ_24;
  private XRiTextField L1SAN;
  private JLabel OBJ_71;
  private XRiTextField L1ACT;
  private JLabel OBJ_73;
  private XRiTextField L1NAT;
  private JXTitledPanel xTitledPanel4;
  private XRiTextField L1AA1;
  private XRiTextField L1AA2;
  private JButton OBJ_82;
  private XRiTextField LIBTYF;
  private XRiTextField L1AA3;
  private XRiTextField L1AA4;
  private XRiTextField L1AA5;
  private XRiTextField L1AA6;
  private JXTitledPanel xTitledPanel3;
  private JScrollPane SCROLLPANE_LECHEANCE;
  private XRiTable LDR01;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
