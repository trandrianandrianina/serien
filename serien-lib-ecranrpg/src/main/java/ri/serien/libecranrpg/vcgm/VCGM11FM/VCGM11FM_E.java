
package ri.serien.libecranrpg.vcgm.VCGM11FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VCGM11FM_E extends SNPanelEcranRPG implements ioFrame {
  
   
  // private String[] _LIST_Top=null;
  private String[] _LD01_Title = { "N°Li Jr Compte Tiers   Montant    S C          Libellé           N°Pièce Sect", };
  private String[][] _LD01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _LD01_Width = { 700, };
  // private String[][] _LIST_Title_Data_Brut=null;
  
  public VCGM11FM_E(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    LD01.setAspectTable(null, _LD01_Title, _LD01_Data, _LD01_Width, false, null, null, null, null);
    
    // _LIST_Title_Data_Brut = initTable(LIST, "Courier New", Font.BOLD, 12, "Courier New", Font.PLAIN, 12);
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    l_Bib.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_85.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSOL@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // Ajoute à la liste des oData les variables non liées directement à un composant graphique
    // majTable(LIST, _LIST_Title_Data_Brut, _LIST_Top);
    
    
    
    panel3.setVisible(!lexique.isTrue("78"));
    OBJ_85.setVisible(lexique.isTrue("40"));
    OBJ_86.setVisible(lexique.isTrue("40"));
    
    E1TDB.setVisible(lexique.isTrue("(N87 AND (N78)"));
    E1TCR.setVisible(lexique.isTrue("(N87 AND (N78)"));
    
    V06F.setVisible(lexique.HostFieldGetData("V05F").trim().equals("Saisir N° Ligne ou D"));
    
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    // TODO Icones
    OBJ_92.setIcon(lexique.chargerImage("images/tabl1.gif", true));
    bt_UP.setIcon(lexique.chargerImage("images/pgup20.png", true));
    bt_DOWN.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    xH_Titre.setIcon(ManagerSessionClient.getInstance().chargerLogoEtablissement(E1SOC.getText()));
    
    // Titre
    // setTitle(???);
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void bt_FonctionsActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_92ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, "Y");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1", false);
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@", false);
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@", false);
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@", false);
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@", false);
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@", false);
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@", false);
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void bt_UPActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGUP");
  }
  
  private void bt_DOWNActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGDOWN");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    bt_Fonctions = new JButton();
    l_Bib = new JLabel();
    OBJ_59_OBJ_59 = new JLabel();
    E1SOC = new XRiTextField();
    OBJ_61_OBJ_61 = new JLabel();
    E1CJO = new XRiTextField();
    JOLIB = new XRiTextField();
    OBJ_64_OBJ_64 = new JLabel();
    E1CFO = new XRiTextField();
    OBJ_66_OBJ_66 = new JLabel();
    E1DTEX = new XRiTextField();
    P_Centre = new JPanel();
    panel2 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    LD01 = new XRiTable();
    panel3 = new JPanel();
    OBJ_86 = new JLabel();
    E1TDB = new XRiTextField();
    E1TCR = new XRiTextField();
    OBJ_85 = new JLabel();
    OBJ_82 = new JLabel();
    OBJ_87 = new JLabel();
    OBJ_83 = new JLabel();
    bt_UP = new JButton();
    bt_DOWN = new JButton();
    panel1 = new JPanel();
    OBJ_92 = new JButton();
    V06F = new XRiTextField();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_20 = new JMenuItem();
    OBJ_21 = new JMenuItem();
    BTD2 = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_24 = new JMenuItem();
    CellConstraints cc = new CellConstraints();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(950, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("FOLIO COMPTABILITE");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setDescription("FM@LOCGRP/+1/@");
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 22));
        P_Infos.setName("P_Infos");

        //---- bt_Fonctions ----
        bt_Fonctions.setText("Fonctions");
        bt_Fonctions.setName("bt_Fonctions");
        bt_Fonctions.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_FonctionsActionPerformed(e);
          }
        });

        //---- l_Bib ----
        l_Bib.setText("@V01F@");
        l_Bib.setFont(new Font("sansserif", Font.BOLD, 12));
        l_Bib.setName("l_Bib");

        //---- OBJ_59_OBJ_59 ----
        OBJ_59_OBJ_59.setText("Soci\u00e9t\u00e9");
        OBJ_59_OBJ_59.setName("OBJ_59_OBJ_59");

        //---- E1SOC ----
        E1SOC.setComponentPopupMenu(BTD);
        E1SOC.setName("E1SOC");

        //---- OBJ_61_OBJ_61 ----
        OBJ_61_OBJ_61.setText("Journal");
        OBJ_61_OBJ_61.setName("OBJ_61_OBJ_61");

        //---- E1CJO ----
        E1CJO.setComponentPopupMenu(BTD);
        E1CJO.setName("E1CJO");

        //---- JOLIB ----
        JOLIB.setComponentPopupMenu(BTD);
        JOLIB.setName("JOLIB");

        //---- OBJ_64_OBJ_64 ----
        OBJ_64_OBJ_64.setText("Folio");
        OBJ_64_OBJ_64.setName("OBJ_64_OBJ_64");

        //---- E1CFO ----
        E1CFO.setComponentPopupMenu(BTD);
        E1CFO.setName("E1CFO");

        //---- OBJ_66_OBJ_66 ----
        OBJ_66_OBJ_66.setText("du");
        OBJ_66_OBJ_66.setName("OBJ_66_OBJ_66");

        //---- E1DTEX ----
        E1DTEX.setComponentPopupMenu(BTD);
        E1DTEX.setName("E1DTEX");

        GroupLayout P_InfosLayout = new GroupLayout(P_Infos);
        P_Infos.setLayout(P_InfosLayout);
        P_InfosLayout.setHorizontalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(19, 19, 19)
              .addComponent(l_Bib, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
              .addGap(9, 9, 9)
              .addComponent(OBJ_59_OBJ_59, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
              .addGap(1, 1, 1)
              .addComponent(E1SOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
              .addGap(14, 14, 14)
              .addComponent(OBJ_61_OBJ_61, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
              .addGap(9, 9, 9)
              .addComponent(E1CJO, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
              .addComponent(JOLIB, GroupLayout.PREFERRED_SIZE, 271, GroupLayout.PREFERRED_SIZE)
              .addGap(9, 9, 9)
              .addComponent(OBJ_64_OBJ_64, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
              .addGap(2, 2, 2)
              .addComponent(E1CFO, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
              .addGap(7, 7, 7)
              .addComponent(OBJ_66_OBJ_66, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
              .addGap(2, 2, 2)
              .addComponent(E1DTEX, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 123, Short.MAX_VALUE)
              .addComponent(bt_Fonctions, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
        );
        P_InfosLayout.setVerticalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(7, 7, 7)
              .addComponent(l_Bib, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(8, 8, 8)
              .addComponent(OBJ_59_OBJ_59))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(3, 3, 3)
              .addComponent(E1SOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(8, 8, 8)
              .addComponent(OBJ_61_OBJ_61))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(3, 3, 3)
              .addComponent(E1CJO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(3, 3, 3)
              .addComponent(JOLIB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(8, 8, 8)
              .addComponent(OBJ_64_OBJ_64))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(3, 3, 3)
              .addComponent(E1CFO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(8, 8, 8)
              .addComponent(OBJ_66_OBJ_66))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(3, 3, 3)
              .addGroup(P_InfosLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(E1DTEX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(bt_Fonctions)))
        );
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");

      //======== panel2 ========
      {
        panel2.setBorder(new TitledBorder("Lignes du Folio"));
        panel2.setName("panel2");

        //======== SCROLLPANE_LIST ========
        {
          SCROLLPANE_LIST.setToolTipText("Double-cliquez pour obtenir le d\u00e9tail");
          SCROLLPANE_LIST.setComponentPopupMenu(BTD2);
          SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

          //---- LD01 ----
          LD01.setComponentPopupMenu(BTD2);
          LD01.setName("LD01");
          SCROLLPANE_LIST.setViewportView(LD01);
        }

        //======== panel3 ========
        {
          panel3.setName("panel3");

          //---- OBJ_86 ----
          OBJ_86.setText("<--- Diff\u00e9rence Totaux ");
          OBJ_86.setFont(new Font("Courier New", Font.PLAIN, 12));
          OBJ_86.setForeground(Color.red);
          OBJ_86.setName("OBJ_86");

          //---- E1TDB ----
          E1TDB.setComponentPopupMenu(BTD);
          E1TDB.setName("E1TDB");

          //---- E1TCR ----
          E1TCR.setComponentPopupMenu(BTD);
          E1TCR.setName("E1TCR");

          //---- OBJ_85 ----
          OBJ_85.setText("@WSOL@");
          OBJ_85.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_85.setForeground(Color.red);
          OBJ_85.setName("OBJ_85");

          //---- OBJ_82 ----
          OBJ_82.setText("Totaux");
          OBJ_82.setFont(OBJ_82.getFont().deriveFont(OBJ_82.getFont().getSize() + 3f));
          OBJ_82.setName("OBJ_82");

          //---- OBJ_87 ----
          OBJ_87.setText("Cr\u00e9dit");
          OBJ_87.setName("OBJ_87");

          //---- OBJ_83 ----
          OBJ_83.setText("D\u00e9bit");
          OBJ_83.setName("OBJ_83");

          GroupLayout panel3Layout = new GroupLayout(panel3);
          panel3.setLayout(panel3Layout);
          panel3Layout.setHorizontalGroup(
            panel3Layout.createParallelGroup()
              .addGroup(panel3Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(OBJ_82, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19)
                .addGroup(panel3Layout.createParallelGroup()
                  .addComponent(OBJ_83, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_87, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(panel3Layout.createParallelGroup()
                  .addGroup(panel3Layout.createSequentialGroup()
                    .addComponent(E1TDB, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(OBJ_85, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_86, GroupLayout.PREFERRED_SIZE, 189, GroupLayout.PREFERRED_SIZE))
                  .addComponent(E1TCR, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE))
                .addContainerGap(228, Short.MAX_VALUE))
          );
          panel3Layout.setVerticalGroup(
            panel3Layout.createParallelGroup()
              .addGroup(panel3Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(panel3Layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                  .addComponent(OBJ_82)
                  .addComponent(OBJ_83)
                  .addComponent(E1TDB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(panel3Layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                  .addComponent(OBJ_87)
                  .addComponent(E1TCR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addComponent(OBJ_86)
                    .addComponent(OBJ_85)))
                .addGap(7, 7, 7))
          );
          panel3Layout.linkSize(SwingConstants.VERTICAL, new Component[] {OBJ_85, OBJ_86});
        }

        //---- bt_UP ----
        bt_UP.setText("");
        bt_UP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_UP.setName("bt_UP");
        bt_UP.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_UPActionPerformed(e);
          }
        });

        //---- bt_DOWN ----
        bt_DOWN.setText("");
        bt_DOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_DOWN.setName("bt_DOWN");
        bt_DOWN.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_DOWNActionPerformed(e);
          }
        });

        GroupLayout panel2Layout = new GroupLayout(panel2);
        panel2.setLayout(panel2Layout);
        panel2Layout.setHorizontalGroup(
          panel2Layout.createParallelGroup()
            .addGroup(panel2Layout.createSequentialGroup()
              .addGap(9, 9, 9)
              .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 857, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGap(8, 8, 8)
              .addGroup(panel2Layout.createParallelGroup()
                .addComponent(bt_UP, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(bt_DOWN, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)))
        );
        panel2Layout.setVerticalGroup(
          panel2Layout.createParallelGroup()
            .addGroup(panel2Layout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addGroup(panel2Layout.createParallelGroup()
                .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                .addGroup(panel2Layout.createSequentialGroup()
                  .addComponent(bt_UP, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
                  .addGap(20, 20, 20)
                  .addComponent(bt_DOWN, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)))
              .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE))
        );
      }

      //======== panel1 ========
      {
        panel1.setName("panel1");
        panel1.setLayout(null);

        //---- OBJ_92 ----
        OBJ_92.setText("");
        OBJ_92.setToolTipText("Exportation folio dans tableur");
        OBJ_92.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_92.setName("OBJ_92");
        OBJ_92.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_92ActionPerformed(e);
          }
        });
        panel1.add(OBJ_92);
        OBJ_92.setBounds(5, 5, 40, 40);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel1.getComponentCount(); i++) {
            Rectangle bounds = panel1.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel1.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel1.setMinimumSize(preferredSize);
          panel1.setPreferredSize(preferredSize);
        }
      }

      //---- V06F ----
      V06F.setToolTipText("Saisir un num\u00e9ro de ligne");
      V06F.setName("V06F");

      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGroup(P_CentreLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(V06F, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE))
              .addGroup(GroupLayout.Alignment.LEADING, P_CentreLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
            .addComponent(panel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(5, 5, 5))
      );
      P_CentreLayout.setVerticalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 415, GroupLayout.PREFERRED_SIZE)))
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(V06F, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addContainerGap(91, Short.MAX_VALUE))
      );
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(200)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Recherche multi-crit\u00e8res");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      CMD.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setText("Annuler");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      CMD.add(OBJ_9);
      CMD.addSeparator();

      //---- OBJ_10 ----
      OBJ_10.setText("Saisie nouvelle \u00e9criture");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      CMD.add(OBJ_10);

      //---- OBJ_11 ----
      OBJ_11.setText("Modification \u00e9criture");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      CMD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Recherche \u00e9critures et retour sur ent\u00eate");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      CMD.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("On/Off Affichage quantit\u00e9");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      CMD.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("On/Off Affichage libell\u00e9 en folio analytique");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      CMD.add(OBJ_14);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_20 ----
      OBJ_20.setText("Aide en ligne");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);

      //---- OBJ_21 ----
      OBJ_21.setText("Invite");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);
    }

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD2.add(CHOISIR);

      //---- OBJ_24 ----
      OBJ_24.setText("Aide en ligne");
      OBJ_24.setName("OBJ_24");
      OBJ_24.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_24ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_24);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private JButton bt_Fonctions;
  private JLabel l_Bib;
  private JLabel OBJ_59_OBJ_59;
  private XRiTextField E1SOC;
  private JLabel OBJ_61_OBJ_61;
  private XRiTextField E1CJO;
  private XRiTextField JOLIB;
  private JLabel OBJ_64_OBJ_64;
  private XRiTextField E1CFO;
  private JLabel OBJ_66_OBJ_66;
  private XRiTextField E1DTEX;
  private JPanel P_Centre;
  private JPanel panel2;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable LD01;
  private JPanel panel3;
  private JLabel OBJ_86;
  private XRiTextField E1TDB;
  private XRiTextField E1TCR;
  private JLabel OBJ_85;
  private JLabel OBJ_82;
  private JLabel OBJ_87;
  private JLabel OBJ_83;
  private JButton bt_UP;
  private JButton bt_DOWN;
  private JPanel panel1;
  private JButton OBJ_92;
  private XRiTextField V06F;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JPopupMenu BTD;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_21;
  private JPopupMenu BTD2;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_24;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
