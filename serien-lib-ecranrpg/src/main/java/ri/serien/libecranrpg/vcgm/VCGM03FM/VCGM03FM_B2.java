
package ri.serien.libecranrpg.vcgm.VCGM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM03FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VCGM03FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDTYP@")).trim());
    INDIND.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDIND@")).trim());
    OBJ_87.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPAGE@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_142ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="Enter"
    // Scriptcall ("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_29 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_80 = new JLabel();
    INDTYP = new RiZoneSortie();
    OBJ_83 = new JLabel();
    INDIND = new RiZoneSortie();
    p_tete_droite = new JPanel();
    panel1 = new JPanel();
    OBJ_84 = new JLabel();
    OBJ_87 = new RiZoneSortie();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    P_Centre = new JPanel();
    OBJ_140 = new JXTitledSeparator();
    OBJ_50 = new JLabel();
    OBJ_60 = new JLabel();
    OBJ_68 = new JLabel();
    OBJ_76 = new JLabel();
    OBJ_86 = new JLabel();
    OBJ_94 = new JLabel();
    OBJ_102 = new JLabel();
    OBJ_110 = new JLabel();
    OBJ_119 = new JLabel();
    OBJ_127 = new JLabel();
    OBJ_135 = new JLabel();
    OBJ_53 = new JLabel();
    OBJ_63 = new JLabel();
    OBJ_71 = new JLabel();
    OBJ_79 = new JLabel();
    OBJ_89 = new JLabel();
    OBJ_105 = new JLabel();
    OBJ_130 = new JLabel();
    OBJ_156 = new JLabel();
    OBJ_97 = new JLabel();
    OBJ_113 = new JLabel();
    OBJ_122 = new JLabel();
    OBJ_142 = new SNBoutonLeger();
    OBJ_151 = new JLabel();
    OBJ_147 = new JLabel();
    OBJ_149 = new JLabel();
    OBJ_145 = new JLabel();
    PST23 = new XRiTextField();
    PST34 = new XRiTextField();
    PST44 = new XRiTextField();
    PST24 = new XRiTextField();
    PST35 = new XRiTextField();
    PST25 = new XRiTextField();
    PST36 = new XRiTextField();
    PST26 = new XRiTextField();
    PST37 = new XRiTextField();
    PST27 = new XRiTextField();
    PST38 = new XRiTextField();
    PST28 = new XRiTextField();
    PST39 = new XRiTextField();
    PST29 = new XRiTextField();
    PST40 = new XRiTextField();
    PST30 = new XRiTextField();
    PST41 = new XRiTextField();
    PST31 = new XRiTextField();
    PST42 = new XRiTextField();
    PST32 = new XRiTextField();
    PST43 = new XRiTextField();
    PST33 = new XRiTextField();
    V06F = new XRiTextField();
    V06F1 = new XRiTextField();
    OBJ_49 = new JLabel();
    OBJ_52 = new JLabel();
    OBJ_59 = new JLabel();
    OBJ_62 = new JLabel();
    OBJ_67 = new JLabel();
    OBJ_70 = new JLabel();
    OBJ_75 = new JLabel();
    OBJ_78 = new JLabel();
    OBJ_85 = new JLabel();
    OBJ_88 = new JLabel();
    OBJ_93 = new JLabel();
    OBJ_96 = new JLabel();
    OBJ_101 = new JLabel();
    OBJ_109 = new JLabel();
    OBJ_118 = new JLabel();
    OBJ_126 = new JLabel();
    OBJ_134 = new JLabel();
    OBJ_104 = new JLabel();
    OBJ_112 = new JLabel();
    OBJ_121 = new JLabel();
    OBJ_129 = new JLabel();
    OBJ_155 = new JLabel();
    OBJ_144 = new JLabel();
    OBJ_146 = new JLabel();
    OBJ_148 = new JLabel();
    OBJ_150 = new JLabel();
    OBJ_55 = new JLabel();
    OBJ_56 = new JLabel();
    OBJ_58 = new JLabel();
    OBJ_65 = new JLabel();
    OBJ_66 = new JLabel();
    OBJ_73 = new JLabel();
    OBJ_74 = new JLabel();
    OBJ_81 = new JLabel();
    OBJ_82 = new JLabel();
    OBJ_91 = new JLabel();
    OBJ_92 = new JLabel();
    OBJ_99 = new JLabel();
    OBJ_100 = new JLabel();
    OBJ_107 = new JLabel();
    OBJ_108 = new JLabel();
    OBJ_115 = new JLabel();
    OBJ_116 = new JLabel();
    OBJ_124 = new JLabel();
    OBJ_125 = new JLabel();
    OBJ_132 = new JLabel();
    OBJ_133 = new JLabel();
    OBJ_138 = new JLabel();
    separator1 = compFactory.createSeparator("Divers");
    BTD = new JPopupMenu();
    OBJ_14 = new JMenuItem();
    OBJ_13 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(860, 32));
          p_tete_gauche.setMinimumSize(new Dimension(860, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_29 ----
          OBJ_29.setText("Soci\u00e9t\u00e9");
          OBJ_29.setName("OBJ_29");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");

          //---- OBJ_80 ----
          OBJ_80.setText("Code");
          OBJ_80.setName("OBJ_80");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setOpaque(false);
          INDTYP.setText("@INDTYP@");
          INDTYP.setName("INDTYP");

          //---- OBJ_83 ----
          OBJ_83.setText("Ordre");
          OBJ_83.setName("OBJ_83");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setOpaque(false);
          INDIND.setText("@INDIND@");
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 59, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(50, 50, 50)
                    .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_80, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                .addGap(16, 16, 16)
                .addComponent(OBJ_83, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(509, 509, 509))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_80, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_83, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //======== panel1 ========
          {
            panel1.setPreferredSize(new Dimension(124, 24));
            panel1.setMinimumSize(new Dimension(124, 24));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_84 ----
            OBJ_84.setText("Page");
            OBJ_84.setName("OBJ_84");
            panel1.add(OBJ_84);
            OBJ_84.setBounds(45, 2, 45, 20);

            //---- OBJ_87 ----
            OBJ_87.setText("@WPAGE@");
            OBJ_87.setOpaque(false);
            OBJ_87.setComponentPopupMenu(null);
            OBJ_87.setName("OBJ_87");
            panel1.add(OBJ_87);
            OBJ_87.setBounds(90, 0, 34, OBJ_87.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_tete_droite.add(panel1);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 250));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 520));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== P_Centre ========
          {
            P_Centre.setPreferredSize(new Dimension(900, 468));
            P_Centre.setOpaque(false);
            P_Centre.setName("P_Centre");
            P_Centre.setLayout(null);

            //---- OBJ_140 ----
            OBJ_140.setTitle("Legende");
            OBJ_140.setName("OBJ_140");
            P_Centre.add(OBJ_140);
            OBJ_140.setBounds(25, 405, 840, OBJ_140.getPreferredSize().height);

            //---- OBJ_50 ----
            OBJ_50.setText("Gestion des abonnements");
            OBJ_50.setName("OBJ_50");
            P_Centre.add(OBJ_50);
            OBJ_50.setBounds(65, 54, 310, 20);

            //---- OBJ_60 ----
            OBJ_60.setText("Force la date d'\u00e9mission \u00e0 date jour du traitement");
            OBJ_60.setComponentPopupMenu(BTD);
            OBJ_60.setName("OBJ_60");
            P_Centre.add(OBJ_60);
            OBJ_60.setBounds(65, 85, 310, 20);

            //---- OBJ_68 ----
            OBJ_68.setText("Affichage du solde du compte en saisie d'\u00e9critures");
            OBJ_68.setComponentPopupMenu(BTD);
            OBJ_68.setName("OBJ_68");
            P_Centre.add(OBJ_68);
            OBJ_68.setBounds(65, 116, 310, 20);

            //---- OBJ_76 ----
            OBJ_76.setText("Affichage justification compte sur num\u00e9ro de pi\u00e8ce");
            OBJ_76.setComponentPopupMenu(BTD);
            OBJ_76.setName("OBJ_76");
            P_Centre.add(OBJ_76);
            OBJ_76.setBounds(65, 147, 310, 20);

            //---- OBJ_86 ----
            OBJ_86.setText("Affichage justification compte sur libell\u00e9");
            OBJ_86.setComponentPopupMenu(BTD);
            OBJ_86.setName("OBJ_86");
            P_Centre.add(OBJ_86);
            OBJ_86.setBounds(65, 178, 310, 20);

            //---- OBJ_94 ----
            OBJ_94.setText("Regroupement pr\u00e9l\u00e8vements sur envoi");
            OBJ_94.setComponentPopupMenu(BTD);
            OBJ_94.setName("OBJ_94");
            P_Centre.add(OBJ_94);
            OBJ_94.setBounds(65, 209, 310, 20);

            //---- OBJ_102 ----
            OBJ_102.setText("R\u00e9f.classement Oblig/Ctr/Libr");
            OBJ_102.setName("OBJ_102");
            P_Centre.add(OBJ_102);
            OBJ_102.setBounds(65, 240, 310, 20);

            //---- OBJ_110 ----
            OBJ_110.setText("Suppression controle/cl\u00e9 RIB");
            OBJ_110.setComponentPopupMenu(BTD);
            OBJ_110.setName("OBJ_110");
            P_Centre.add(OBJ_110);
            OBJ_110.setBounds(65, 271, 310, 20);

            //---- OBJ_119 ----
            OBJ_119.setText("Date Mobilisation obligatoire");
            OBJ_119.setComponentPopupMenu(BTD);
            OBJ_119.setName("OBJ_119");
            P_Centre.add(OBJ_119);
            OBJ_119.setBounds(65, 302, 310, 20);

            //---- OBJ_127 ----
            OBJ_127.setText("Avoir syst\u00e9matique sur relance");
            OBJ_127.setComponentPopupMenu(BTD);
            OBJ_127.setName("OBJ_127");
            P_Centre.add(OBJ_127);
            OBJ_127.setBounds(65, 333, 310, 20);

            //---- OBJ_135 ----
            OBJ_135.setText("Duplication auto. \"AC\" et \"SA\"");
            OBJ_135.setName("OBJ_135");
            P_Centre.add(OBJ_135);
            OBJ_135.setBounds(65, 364, 310, 20);

            //---- OBJ_53 ----
            OBJ_53.setText("Domiciliation bancaire sur ch\u00e8que non obligatoire");
            OBJ_53.setName("OBJ_53");
            P_Centre.add(OBJ_53);
            OBJ_53.setBounds(490, 54, 295, 20);

            //---- OBJ_63 ----
            OBJ_63.setText("Num\u00e9ro de carte bleue non contr\u00f4l\u00e9");
            OBJ_63.setName("OBJ_63");
            P_Centre.add(OBJ_63);
            OBJ_63.setBounds(490, 85, 295, 20);

            //---- OBJ_71 ----
            OBJ_71.setText("RIB obligatoire si r\u00e9glement sur traite");
            OBJ_71.setName("OBJ_71");
            P_Centre.add(OBJ_71);
            OBJ_71.setBounds(490, 116, 295, 20);

            //---- OBJ_79 ----
            OBJ_79.setText("Gestion des dettes sur relev\u00e9s de banque");
            OBJ_79.setName("OBJ_79");
            P_Centre.add(OBJ_79);
            OBJ_79.setBounds(490, 147, 295, 20);

            //---- OBJ_89 ----
            OBJ_89.setText("Contr\u00f4le r\u00e9f\u00e9rence \u00e9criture");
            OBJ_89.setName("OBJ_89");
            P_Centre.add(OBJ_89);
            OBJ_89.setBounds(490, 178, 295, 20);

            //---- OBJ_105 ----
            OBJ_105.setText("R\u00e9perc.Tiers/Obs en saisi Chq");
            OBJ_105.setName("OBJ_105");
            P_Centre.add(OBJ_105);
            OBJ_105.setBounds(490, 240, 295, 20);

            //---- OBJ_130 ----
            OBJ_130.setText("Pas de surimpression sur certaines Editions");
            OBJ_130.setName("OBJ_130");
            P_Centre.add(OBJ_130);
            OBJ_130.setBounds(490, 333, 295, 20);

            //---- OBJ_156 ----
            OBJ_156.setText("Code Alpha.2 unique sur Compte.Auxiliaire");
            OBJ_156.setName("OBJ_156");
            P_Centre.add(OBJ_156);
            OBJ_156.setBounds(490, 364, 295, 20);

            //---- OBJ_97 ----
            OBJ_97.setText("Famille obligatoire / Immos");
            OBJ_97.setName("OBJ_97");
            P_Centre.add(OBJ_97);
            OBJ_97.setBounds(490, 209, 295, 20);

            //---- OBJ_113 ----
            OBJ_113.setText("Date jour en cr\u00e9ation effet");
            OBJ_113.setName("OBJ_113");
            P_Centre.add(OBJ_113);
            OBJ_113.setBounds(490, 271, 295, 20);

            //---- OBJ_122 ----
            OBJ_122.setText("R\u00e8glement inter Soci\u00e9t\u00e9s possible");
            OBJ_122.setName("OBJ_122");
            P_Centre.add(OBJ_122);
            OBJ_122.setBounds(490, 302, 295, 20);

            //---- OBJ_142 ----
            OBJ_142.setText("Aller \u00e0 la page");
            OBJ_142.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_142.setName("OBJ_142");
            OBJ_142.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_142ActionPerformed(e);
              }
            });
            P_Centre.add(OBJ_142);
            OBJ_142.setBounds(new Rectangle(new Point(640, 440), OBJ_142.getPreferredSize()));

            //---- OBJ_151 ----
            OBJ_151.setText("Voir Documentation");
            OBJ_151.setName("OBJ_151");
            P_Centre.add(OBJ_151);
            OBJ_151.setBounds(355, 445, 120, 20);

            //---- OBJ_147 ----
            OBJ_147.setText("0 < Val < 9");
            OBJ_147.setName("OBJ_147");
            P_Centre.add(OBJ_147);
            OBJ_147.setBounds(150, 445, 63, 20);

            //---- OBJ_149 ----
            OBJ_149.setText("Voir Aide");
            OBJ_149.setName("OBJ_149");
            P_Centre.add(OBJ_149);
            OBJ_149.setBounds(255, 445, 58, 20);

            //---- OBJ_145 ----
            OBJ_145.setText("1= OUI");
            OBJ_145.setName("OBJ_145");
            P_Centre.add(OBJ_145);
            OBJ_145.setBounds(70, 445, 49, 20);

            //---- PST23 ----
            PST23.setComponentPopupMenu(BTD);
            PST23.setName("PST23");
            P_Centre.add(PST23);
            PST23.setBounds(375, 50, 20, PST23.getPreferredSize().height);

            //---- PST34 ----
            PST34.setComponentPopupMenu(BTD);
            PST34.setName("PST34");
            P_Centre.add(PST34);
            PST34.setBounds(790, 50, 20, PST34.getPreferredSize().height);

            //---- PST44 ----
            PST44.setComponentPopupMenu(BTD);
            PST44.setName("PST44");
            P_Centre.add(PST44);
            PST44.setBounds(790, 360, 20, PST44.getPreferredSize().height);

            //---- PST24 ----
            PST24.setComponentPopupMenu(BTD);
            PST24.setName("PST24");
            P_Centre.add(PST24);
            PST24.setBounds(375, 81, 21, PST24.getPreferredSize().height);

            //---- PST35 ----
            PST35.setComponentPopupMenu(BTD);
            PST35.setName("PST35");
            P_Centre.add(PST35);
            PST35.setBounds(790, 81, 20, PST35.getPreferredSize().height);

            //---- PST25 ----
            PST25.setComponentPopupMenu(BTD);
            PST25.setName("PST25");
            P_Centre.add(PST25);
            PST25.setBounds(375, 112, 21, PST25.getPreferredSize().height);

            //---- PST36 ----
            PST36.setComponentPopupMenu(BTD);
            PST36.setName("PST36");
            P_Centre.add(PST36);
            PST36.setBounds(790, 112, 20, PST36.getPreferredSize().height);

            //---- PST26 ----
            PST26.setComponentPopupMenu(BTD);
            PST26.setName("PST26");
            P_Centre.add(PST26);
            PST26.setBounds(375, 143, 21, PST26.getPreferredSize().height);

            //---- PST37 ----
            PST37.setComponentPopupMenu(BTD);
            PST37.setName("PST37");
            P_Centre.add(PST37);
            PST37.setBounds(790, 143, 20, PST37.getPreferredSize().height);

            //---- PST27 ----
            PST27.setComponentPopupMenu(BTD);
            PST27.setName("PST27");
            P_Centre.add(PST27);
            PST27.setBounds(375, 174, 21, PST27.getPreferredSize().height);

            //---- PST38 ----
            PST38.setComponentPopupMenu(BTD);
            PST38.setName("PST38");
            P_Centre.add(PST38);
            PST38.setBounds(790, 174, 20, PST38.getPreferredSize().height);

            //---- PST28 ----
            PST28.setComponentPopupMenu(BTD);
            PST28.setName("PST28");
            P_Centre.add(PST28);
            PST28.setBounds(375, 205, 21, PST28.getPreferredSize().height);

            //---- PST39 ----
            PST39.setComponentPopupMenu(BTD);
            PST39.setName("PST39");
            P_Centre.add(PST39);
            PST39.setBounds(790, 205, 20, PST39.getPreferredSize().height);

            //---- PST29 ----
            PST29.setComponentPopupMenu(BTD);
            PST29.setName("PST29");
            P_Centre.add(PST29);
            PST29.setBounds(375, 236, 20, PST29.getPreferredSize().height);

            //---- PST40 ----
            PST40.setComponentPopupMenu(BTD);
            PST40.setName("PST40");
            P_Centre.add(PST40);
            PST40.setBounds(790, 236, 20, PST40.getPreferredSize().height);

            //---- PST30 ----
            PST30.setComponentPopupMenu(BTD);
            PST30.setName("PST30");
            P_Centre.add(PST30);
            PST30.setBounds(375, 267, 21, PST30.getPreferredSize().height);

            //---- PST41 ----
            PST41.setComponentPopupMenu(BTD);
            PST41.setName("PST41");
            P_Centre.add(PST41);
            PST41.setBounds(790, 267, 20, PST41.getPreferredSize().height);

            //---- PST31 ----
            PST31.setComponentPopupMenu(BTD);
            PST31.setName("PST31");
            P_Centre.add(PST31);
            PST31.setBounds(375, 298, 21, PST31.getPreferredSize().height);

            //---- PST42 ----
            PST42.setComponentPopupMenu(BTD);
            PST42.setName("PST42");
            P_Centre.add(PST42);
            PST42.setBounds(790, 298, 20, PST42.getPreferredSize().height);

            //---- PST32 ----
            PST32.setComponentPopupMenu(BTD);
            PST32.setName("PST32");
            P_Centre.add(PST32);
            PST32.setBounds(375, 329, 21, PST32.getPreferredSize().height);

            //---- PST43 ----
            PST43.setComponentPopupMenu(BTD);
            PST43.setName("PST43");
            P_Centre.add(PST43);
            PST43.setBounds(790, 329, 20, PST43.getPreferredSize().height);

            //---- PST33 ----
            PST33.setComponentPopupMenu(BTD);
            PST33.setName("PST33");
            P_Centre.add(PST33);
            PST33.setBounds(375, 360, 20, PST33.getPreferredSize().height);

            //---- V06F ----
            V06F.setComponentPopupMenu(BTD);
            V06F.setName("V06F");
            P_Centre.add(V06F);
            V06F.setBounds(790, 441, 20, V06F.getPreferredSize().height);

            //---- V06F1 ----
            V06F1.setComponentPopupMenu(BTD);
            V06F1.setName("V06F1");
            P_Centre.add(V06F1);
            V06F1.setBounds(790, 441, 20, V06F1.getPreferredSize().height);

            //---- OBJ_49 ----
            OBJ_49.setText("23");
            OBJ_49.setName("OBJ_49");
            P_Centre.add(OBJ_49);
            OBJ_49.setBounds(45, 54, 18, 20);

            //---- OBJ_52 ----
            OBJ_52.setText("34");
            OBJ_52.setName("OBJ_52");
            P_Centre.add(OBJ_52);
            OBJ_52.setBounds(460, 54, 18, 20);

            //---- OBJ_59 ----
            OBJ_59.setText("24");
            OBJ_59.setName("OBJ_59");
            P_Centre.add(OBJ_59);
            OBJ_59.setBounds(45, 85, 18, 20);

            //---- OBJ_62 ----
            OBJ_62.setText("35");
            OBJ_62.setName("OBJ_62");
            P_Centre.add(OBJ_62);
            OBJ_62.setBounds(460, 85, 18, 20);

            //---- OBJ_67 ----
            OBJ_67.setText("25");
            OBJ_67.setName("OBJ_67");
            P_Centre.add(OBJ_67);
            OBJ_67.setBounds(45, 116, 18, 20);

            //---- OBJ_70 ----
            OBJ_70.setText("36");
            OBJ_70.setName("OBJ_70");
            P_Centre.add(OBJ_70);
            OBJ_70.setBounds(460, 116, 18, 20);

            //---- OBJ_75 ----
            OBJ_75.setText("26");
            OBJ_75.setName("OBJ_75");
            P_Centre.add(OBJ_75);
            OBJ_75.setBounds(45, 147, 18, 20);

            //---- OBJ_78 ----
            OBJ_78.setText("37");
            OBJ_78.setName("OBJ_78");
            P_Centre.add(OBJ_78);
            OBJ_78.setBounds(460, 147, 18, 20);

            //---- OBJ_85 ----
            OBJ_85.setText("27");
            OBJ_85.setName("OBJ_85");
            P_Centre.add(OBJ_85);
            OBJ_85.setBounds(45, 178, 18, 20);

            //---- OBJ_88 ----
            OBJ_88.setText("38");
            OBJ_88.setName("OBJ_88");
            P_Centre.add(OBJ_88);
            OBJ_88.setBounds(460, 178, 18, 20);

            //---- OBJ_93 ----
            OBJ_93.setText("28");
            OBJ_93.setName("OBJ_93");
            P_Centre.add(OBJ_93);
            OBJ_93.setBounds(45, 209, 18, 20);

            //---- OBJ_96 ----
            OBJ_96.setText("39");
            OBJ_96.setName("OBJ_96");
            P_Centre.add(OBJ_96);
            OBJ_96.setBounds(460, 209, 18, 20);

            //---- OBJ_101 ----
            OBJ_101.setText("29");
            OBJ_101.setName("OBJ_101");
            P_Centre.add(OBJ_101);
            OBJ_101.setBounds(45, 240, 18, 20);

            //---- OBJ_109 ----
            OBJ_109.setText("30");
            OBJ_109.setName("OBJ_109");
            P_Centre.add(OBJ_109);
            OBJ_109.setBounds(45, 271, 18, 20);

            //---- OBJ_118 ----
            OBJ_118.setText("31");
            OBJ_118.setName("OBJ_118");
            P_Centre.add(OBJ_118);
            OBJ_118.setBounds(45, 302, 18, 20);

            //---- OBJ_126 ----
            OBJ_126.setText("32");
            OBJ_126.setName("OBJ_126");
            P_Centre.add(OBJ_126);
            OBJ_126.setBounds(45, 333, 18, 20);

            //---- OBJ_134 ----
            OBJ_134.setText("33");
            OBJ_134.setName("OBJ_134");
            P_Centre.add(OBJ_134);
            OBJ_134.setBounds(45, 365, 18, 20);

            //---- OBJ_104 ----
            OBJ_104.setText("40");
            OBJ_104.setName("OBJ_104");
            P_Centre.add(OBJ_104);
            OBJ_104.setBounds(460, 240, 17, 20);

            //---- OBJ_112 ----
            OBJ_112.setText("41");
            OBJ_112.setName("OBJ_112");
            P_Centre.add(OBJ_112);
            OBJ_112.setBounds(460, 271, 17, 20);

            //---- OBJ_121 ----
            OBJ_121.setText("42");
            OBJ_121.setName("OBJ_121");
            P_Centre.add(OBJ_121);
            OBJ_121.setBounds(460, 302, 17, 20);

            //---- OBJ_129 ----
            OBJ_129.setText("43");
            OBJ_129.setName("OBJ_129");
            P_Centre.add(OBJ_129);
            OBJ_129.setBounds(460, 333, 17, 20);

            //---- OBJ_155 ----
            OBJ_155.setText("44");
            OBJ_155.setName("OBJ_155");
            P_Centre.add(OBJ_155);
            OBJ_155.setBounds(460, 364, 17, 20);

            //---- OBJ_144 ----
            OBJ_144.setText("a");
            OBJ_144.setName("OBJ_144");
            P_Centre.add(OBJ_144);
            OBJ_144.setBounds(45, 445, 18, 20);

            //---- OBJ_146 ----
            OBJ_146.setText("b");
            OBJ_146.setName("OBJ_146");
            P_Centre.add(OBJ_146);
            OBJ_146.setBounds(125, 445, 21, 20);

            //---- OBJ_148 ----
            OBJ_148.setText("c");
            OBJ_148.setName("OBJ_148");
            P_Centre.add(OBJ_148);
            OBJ_148.setBounds(230, 445, 17, 20);

            //---- OBJ_150 ----
            OBJ_150.setText("d");
            OBJ_150.setName("OBJ_150");
            P_Centre.add(OBJ_150);
            OBJ_150.setBounds(330, 445, 18, 20);

            //---- OBJ_55 ----
            OBJ_55.setText("a");
            OBJ_55.setComponentPopupMenu(BTD);
            OBJ_55.setName("OBJ_55");
            P_Centre.add(OBJ_55);
            OBJ_55.setBounds(405, 54, 12, 20);

            //---- OBJ_56 ----
            OBJ_56.setText("a");
            OBJ_56.setName("OBJ_56");
            P_Centre.add(OBJ_56);
            OBJ_56.setBounds(820, 54, 12, 20);

            //---- OBJ_58 ----
            OBJ_58.setText("c");
            OBJ_58.setName("OBJ_58");
            P_Centre.add(OBJ_58);
            OBJ_58.setBounds(820, 364, 12, 20);

            //---- OBJ_65 ----
            OBJ_65.setText("c");
            OBJ_65.setComponentPopupMenu(BTD);
            OBJ_65.setName("OBJ_65");
            P_Centre.add(OBJ_65);
            OBJ_65.setBounds(405, 85, 12, 20);

            //---- OBJ_66 ----
            OBJ_66.setText("a");
            OBJ_66.setName("OBJ_66");
            P_Centre.add(OBJ_66);
            OBJ_66.setBounds(820, 85, 12, 20);

            //---- OBJ_73 ----
            OBJ_73.setText("c");
            OBJ_73.setComponentPopupMenu(BTD);
            OBJ_73.setName("OBJ_73");
            P_Centre.add(OBJ_73);
            OBJ_73.setBounds(405, 116, 12, 20);

            //---- OBJ_74 ----
            OBJ_74.setText("a");
            OBJ_74.setName("OBJ_74");
            P_Centre.add(OBJ_74);
            OBJ_74.setBounds(820, 116, 12, 20);

            //---- OBJ_81 ----
            OBJ_81.setText("c");
            OBJ_81.setComponentPopupMenu(BTD);
            OBJ_81.setName("OBJ_81");
            P_Centre.add(OBJ_81);
            OBJ_81.setBounds(405, 147, 12, 20);

            //---- OBJ_82 ----
            OBJ_82.setText("a");
            OBJ_82.setName("OBJ_82");
            P_Centre.add(OBJ_82);
            OBJ_82.setBounds(820, 147, 12, 20);

            //---- OBJ_91 ----
            OBJ_91.setText("c");
            OBJ_91.setComponentPopupMenu(BTD);
            OBJ_91.setName("OBJ_91");
            P_Centre.add(OBJ_91);
            OBJ_91.setBounds(405, 178, 12, 20);

            //---- OBJ_92 ----
            OBJ_92.setText("c");
            OBJ_92.setName("OBJ_92");
            P_Centre.add(OBJ_92);
            OBJ_92.setBounds(820, 178, 12, 20);

            //---- OBJ_99 ----
            OBJ_99.setText("a");
            OBJ_99.setComponentPopupMenu(BTD);
            OBJ_99.setName("OBJ_99");
            P_Centre.add(OBJ_99);
            OBJ_99.setBounds(405, 209, 12, 20);

            //---- OBJ_100 ----
            OBJ_100.setText("a");
            OBJ_100.setName("OBJ_100");
            P_Centre.add(OBJ_100);
            OBJ_100.setBounds(820, 209, 12, 20);

            //---- OBJ_107 ----
            OBJ_107.setText("c");
            OBJ_107.setName("OBJ_107");
            P_Centre.add(OBJ_107);
            OBJ_107.setBounds(405, 240, 12, 20);

            //---- OBJ_108 ----
            OBJ_108.setText("c");
            OBJ_108.setName("OBJ_108");
            P_Centre.add(OBJ_108);
            OBJ_108.setBounds(820, 240, 12, 20);

            //---- OBJ_115 ----
            OBJ_115.setText("a");
            OBJ_115.setComponentPopupMenu(BTD);
            OBJ_115.setName("OBJ_115");
            P_Centre.add(OBJ_115);
            OBJ_115.setBounds(405, 271, 12, 20);

            //---- OBJ_116 ----
            OBJ_116.setText("a");
            OBJ_116.setName("OBJ_116");
            P_Centre.add(OBJ_116);
            OBJ_116.setBounds(820, 271, 12, 20);

            //---- OBJ_124 ----
            OBJ_124.setText("a");
            OBJ_124.setComponentPopupMenu(BTD);
            OBJ_124.setName("OBJ_124");
            P_Centre.add(OBJ_124);
            OBJ_124.setBounds(405, 302, 12, 20);

            //---- OBJ_125 ----
            OBJ_125.setText("a");
            OBJ_125.setName("OBJ_125");
            P_Centre.add(OBJ_125);
            OBJ_125.setBounds(820, 302, 12, 20);

            //---- OBJ_132 ----
            OBJ_132.setText("a");
            OBJ_132.setComponentPopupMenu(BTD);
            OBJ_132.setName("OBJ_132");
            P_Centre.add(OBJ_132);
            OBJ_132.setBounds(405, 333, 12, 20);

            //---- OBJ_133 ----
            OBJ_133.setText("a");
            OBJ_133.setName("OBJ_133");
            P_Centre.add(OBJ_133);
            OBJ_133.setBounds(820, 333, 12, 20);

            //---- OBJ_138 ----
            OBJ_138.setText("c");
            OBJ_138.setComponentPopupMenu(BTD);
            OBJ_138.setName("OBJ_138");
            P_Centre.add(OBJ_138);
            OBJ_138.setBounds(405, 364, 12, 20);

            //---- separator1 ----
            separator1.setName("separator1");
            P_Centre.add(separator1);
            separator1.setBounds(25, 20, 840, separator1.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < P_Centre.getComponentCount(); i++) {
                Rectangle bounds = P_Centre.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = P_Centre.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              P_Centre.setMinimumSize(preferredSize);
              P_Centre.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(P_Centre, GroupLayout.PREFERRED_SIZE, 885, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(P_Centre, GroupLayout.PREFERRED_SIZE, 485, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_14 ----
      OBJ_14.setText("Choix possibles");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);

      //---- OBJ_13 ----
      OBJ_13.setText("Aide en ligne");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_29;
  private RiZoneSortie INDETB;
  private JLabel OBJ_80;
  private RiZoneSortie INDTYP;
  private JLabel OBJ_83;
  private RiZoneSortie INDIND;
  private JPanel p_tete_droite;
  private JPanel panel1;
  private JLabel OBJ_84;
  private RiZoneSortie OBJ_87;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel P_Centre;
  private JXTitledSeparator OBJ_140;
  private JLabel OBJ_50;
  private JLabel OBJ_60;
  private JLabel OBJ_68;
  private JLabel OBJ_76;
  private JLabel OBJ_86;
  private JLabel OBJ_94;
  private JLabel OBJ_102;
  private JLabel OBJ_110;
  private JLabel OBJ_119;
  private JLabel OBJ_127;
  private JLabel OBJ_135;
  private JLabel OBJ_53;
  private JLabel OBJ_63;
  private JLabel OBJ_71;
  private JLabel OBJ_79;
  private JLabel OBJ_89;
  private JLabel OBJ_105;
  private JLabel OBJ_130;
  private JLabel OBJ_156;
  private JLabel OBJ_97;
  private JLabel OBJ_113;
  private JLabel OBJ_122;
  private SNBoutonLeger OBJ_142;
  private JLabel OBJ_151;
  private JLabel OBJ_147;
  private JLabel OBJ_149;
  private JLabel OBJ_145;
  private XRiTextField PST23;
  private XRiTextField PST34;
  private XRiTextField PST44;
  private XRiTextField PST24;
  private XRiTextField PST35;
  private XRiTextField PST25;
  private XRiTextField PST36;
  private XRiTextField PST26;
  private XRiTextField PST37;
  private XRiTextField PST27;
  private XRiTextField PST38;
  private XRiTextField PST28;
  private XRiTextField PST39;
  private XRiTextField PST29;
  private XRiTextField PST40;
  private XRiTextField PST30;
  private XRiTextField PST41;
  private XRiTextField PST31;
  private XRiTextField PST42;
  private XRiTextField PST32;
  private XRiTextField PST43;
  private XRiTextField PST33;
  private XRiTextField V06F;
  private XRiTextField V06F1;
  private JLabel OBJ_49;
  private JLabel OBJ_52;
  private JLabel OBJ_59;
  private JLabel OBJ_62;
  private JLabel OBJ_67;
  private JLabel OBJ_70;
  private JLabel OBJ_75;
  private JLabel OBJ_78;
  private JLabel OBJ_85;
  private JLabel OBJ_88;
  private JLabel OBJ_93;
  private JLabel OBJ_96;
  private JLabel OBJ_101;
  private JLabel OBJ_109;
  private JLabel OBJ_118;
  private JLabel OBJ_126;
  private JLabel OBJ_134;
  private JLabel OBJ_104;
  private JLabel OBJ_112;
  private JLabel OBJ_121;
  private JLabel OBJ_129;
  private JLabel OBJ_155;
  private JLabel OBJ_144;
  private JLabel OBJ_146;
  private JLabel OBJ_148;
  private JLabel OBJ_150;
  private JLabel OBJ_55;
  private JLabel OBJ_56;
  private JLabel OBJ_58;
  private JLabel OBJ_65;
  private JLabel OBJ_66;
  private JLabel OBJ_73;
  private JLabel OBJ_74;
  private JLabel OBJ_81;
  private JLabel OBJ_82;
  private JLabel OBJ_91;
  private JLabel OBJ_92;
  private JLabel OBJ_99;
  private JLabel OBJ_100;
  private JLabel OBJ_107;
  private JLabel OBJ_108;
  private JLabel OBJ_115;
  private JLabel OBJ_116;
  private JLabel OBJ_124;
  private JLabel OBJ_125;
  private JLabel OBJ_132;
  private JLabel OBJ_133;
  private JLabel OBJ_138;
  private JComponent separator1;
  private JPopupMenu BTD;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_13;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
