
package ri.serien.libecranrpg.vcgm.VCGM06FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGM06FM_EB extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM06FM_EB(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Déclaration d'échange de biens"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_27 = new JLabel();
    OBJ_32 = new JLabel();
    OBJ_37 = new JLabel();
    OBJ_42 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_52 = new JLabel();
    XECOEF = new XRiTextField();
    XECO14 = new XRiTextField();
    XECO13 = new XRiTextField();
    XECO11 = new XRiTextField();
    XECO10 = new XRiTextField();
    XECO3 = new XRiTextField();
    xTitledPanel2 = new JXTitledPanel();
    XECP01 = new XRiTextField();
    XECP02 = new XRiTextField();
    XECP03 = new XRiTextField();
    XECP04 = new XRiTextField();
    XECP05 = new XRiTextField();
    OBJ_24 = new JLabel();
    OBJ_29 = new JLabel();
    OBJ_34 = new JLabel();
    OBJ_39 = new JLabel();
    OBJ_44 = new JLabel();
    XECO12 = new XRiTextField();
    XEDE02 = new XRiTextField();
    XEDE03 = new XRiTextField();
    XEDE04 = new XRiTextField();
    XEDE05 = new XRiTextField();
    XECP06 = new XRiTextField();
    XECP07 = new XRiTextField();
    XECP08 = new XRiTextField();
    XECP09 = new XRiTextField();
    XECP10 = new XRiTextField();
    OBJ_49 = new JLabel();
    OBJ_54 = new JLabel();
    OBJ_57 = new JLabel();
    OBJ_60 = new JLabel();
    OBJ_63 = new JLabel();
    XEDE06 = new XRiTextField();
    XEDE07 = new XRiTextField();
    XEDE08 = new XRiTextField();
    XEDE09 = new XRiTextField();
    XEDE10 = new XRiTextField();
    xTitledSeparator1 = new JXTitledSeparator();
    xTitledSeparator2 = new JXTitledSeparator();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(790, 440));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== xTitledPanel1 ========
        {
          xTitledPanel1.setBorder(new DropShadowBorder());
          xTitledPanel1.setName("xTitledPanel1");
          Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

          //---- OBJ_27 ----
          OBJ_27.setText("Pays de destination / provenance");
          OBJ_27.setName("OBJ_27");

          //---- OBJ_32 ----
          OBJ_32.setText("Condition de livraison");
          OBJ_32.setName("OBJ_32");

          //---- OBJ_37 ----
          OBJ_37.setText("Mode de transport");
          OBJ_37.setName("OBJ_37");

          //---- OBJ_42 ----
          OBJ_42.setText("Pays d'origine");
          OBJ_42.setName("OBJ_42");

          //---- OBJ_47 ----
          OBJ_47.setText("N\u00b0 d'identification");
          OBJ_47.setName("OBJ_47");

          //---- OBJ_52 ----
          OBJ_52.setText("Coeficient quantit\u00e9 deb / Quantit\u00e9 cpt");
          OBJ_52.setName("OBJ_52");

          //---- XECOEF ----
          XECOEF.setComponentPopupMenu(BTD);
          XECOEF.setName("XECOEF");

          //---- XECO14 ----
          XECO14.setComponentPopupMenu(BTD);
          XECO14.setName("XECO14");

          //---- XECO13 ----
          XECO13.setComponentPopupMenu(BTD);
          XECO13.setName("XECO13");

          //---- XECO11 ----
          XECO11.setComponentPopupMenu(BTD);
          XECO11.setName("XECO11");

          //---- XECO10 ----
          XECO10.setComponentPopupMenu(BTD);
          XECO10.setName("XECO10");

          //---- XECO3 ----
          XECO3.setComponentPopupMenu(BTD);
          XECO3.setName("XECO3");

          GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
          xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
          xTitledPanel1ContentContainerLayout.setHorizontalGroup(
            xTitledPanel1ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                  .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 245, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, 245, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 245, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 245, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 245, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 245, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                  .addComponent(XECO13, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(XECOEF, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
                  .addComponent(XECO3, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(XECO10, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addComponent(XECO14, GroupLayout.PREFERRED_SIZE, 156, GroupLayout.PREFERRED_SIZE)
                  .addComponent(XECO11, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
          );
          xTitledPanel1ContentContainerLayout.setVerticalGroup(
            xTitledPanel1ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(OBJ_27)
                .addGap(11, 11, 11)
                .addComponent(OBJ_32)
                .addGap(11, 11, 11)
                .addComponent(OBJ_37)
                .addGap(11, 11, 11)
                .addComponent(OBJ_42)
                .addGap(11, 11, 11)
                .addComponent(OBJ_47)
                .addGap(11, 11, 11)
                .addComponent(OBJ_52))
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(81, 81, 81)
                    .addComponent(XECO13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(26, 26, 26)
                    .addComponent(XECOEF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(XECO3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(27, 27, 27)
                    .addComponent(XECO10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(108, 108, 108)
                    .addComponent(XECO14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(54, 54, 54)
                    .addComponent(XECO11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
          );
        }
        p_contenu.add(xTitledPanel1);
        xTitledPanel1.setBounds(10, 15, 593, 210);

        //======== xTitledPanel2 ========
        {
          xTitledPanel2.setBorder(new DropShadowBorder());
          xTitledPanel2.setName("xTitledPanel2");
          Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();

          //---- XECP01 ----
          XECP01.setName("XECP01");

          //---- XECP02 ----
          XECP02.setName("XECP02");

          //---- XECP03 ----
          XECP03.setName("XECP03");

          //---- XECP04 ----
          XECP04.setName("XECP04");

          //---- XECP05 ----
          XECP05.setName("XECP05");

          //---- OBJ_24 ----
          OBJ_24.setText("01");
          OBJ_24.setName("OBJ_24");

          //---- OBJ_29 ----
          OBJ_29.setText("02");
          OBJ_29.setName("OBJ_29");

          //---- OBJ_34 ----
          OBJ_34.setText("03");
          OBJ_34.setName("OBJ_34");

          //---- OBJ_39 ----
          OBJ_39.setText("04");
          OBJ_39.setName("OBJ_39");

          //---- OBJ_44 ----
          OBJ_44.setText("05");
          OBJ_44.setName("OBJ_44");

          //---- XECO12 ----
          XECO12.setName("XECO12");

          //---- XEDE02 ----
          XEDE02.setName("XEDE02");

          //---- XEDE03 ----
          XEDE03.setName("XEDE03");

          //---- XEDE04 ----
          XEDE04.setName("XEDE04");

          //---- XEDE05 ----
          XEDE05.setName("XEDE05");

          //---- XECP06 ----
          XECP06.setName("XECP06");

          //---- XECP07 ----
          XECP07.setName("XECP07");

          //---- XECP08 ----
          XECP08.setName("XECP08");

          //---- XECP09 ----
          XECP09.setName("XECP09");

          //---- XECP10 ----
          XECP10.setName("XECP10");

          //---- OBJ_49 ----
          OBJ_49.setText("06");
          OBJ_49.setName("OBJ_49");

          //---- OBJ_54 ----
          OBJ_54.setText("07");
          OBJ_54.setName("OBJ_54");

          //---- OBJ_57 ----
          OBJ_57.setText("08");
          OBJ_57.setName("OBJ_57");

          //---- OBJ_60 ----
          OBJ_60.setText("09");
          OBJ_60.setName("OBJ_60");

          //---- OBJ_63 ----
          OBJ_63.setText("10");
          OBJ_63.setName("OBJ_63");

          //---- XEDE06 ----
          XEDE06.setName("XEDE06");

          //---- XEDE07 ----
          XEDE07.setName("XEDE07");

          //---- XEDE08 ----
          XEDE08.setName("XEDE08");

          //---- XEDE09 ----
          XEDE09.setName("XEDE09");

          //---- XEDE10 ----
          XEDE10.setName("XEDE10");

          //---- xTitledSeparator1 ----
          xTitledSeparator1.setTitle("D\u00e9partement");
          xTitledSeparator1.setName("xTitledSeparator1");

          //---- xTitledSeparator2 ----
          xTitledSeparator2.setTitle("C.P.Trs");
          xTitledSeparator2.setName("xTitledSeparator2");

          GroupLayout xTitledPanel2ContentContainerLayout = new GroupLayout(xTitledPanel2ContentContainer);
          xTitledPanel2ContentContainer.setLayout(xTitledPanel2ContentContainerLayout);
          xTitledPanel2ContentContainerLayout.setHorizontalGroup(
            xTitledPanel2ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(xTitledSeparator2, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                      .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE))
                    .addGap(9, 9, 9)
                    .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                      .addComponent(XEDE02, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addComponent(XECO12, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addComponent(XEDE03, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addComponent(XEDE05, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addComponent(XEDE04, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                    .addGap(5, 5, 5)
                    .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                      .addComponent(XECP01, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                      .addComponent(XECP02, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                      .addComponent(XECP04, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                      .addComponent(XECP05, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                      .addComponent(XECP03, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE))
                    .addGap(62, 62, 62)
                    .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                      .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE))
                    .addGap(9, 9, 9)
                    .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                      .addComponent(XEDE07, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                      .addComponent(XEDE06, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                      .addComponent(XEDE09, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                      .addComponent(XEDE10, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                      .addComponent(XEDE08, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
                    .addGap(10, 10, 10)
                    .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                      .addComponent(XECP06, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                      .addComponent(XECP10, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                      .addComponent(XECP09, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                      .addComponent(XECP08, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                      .addComponent(XECP07, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)))))
          );
          xTitledPanel2ContentContainerLayout.setVerticalGroup(
            xTitledPanel2ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                  .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(xTitledSeparator2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addComponent(OBJ_24)
                    .addGap(11, 11, 11)
                    .addComponent(OBJ_29)
                    .addGap(11, 11, 11)
                    .addComponent(OBJ_34)
                    .addGap(11, 11, 11)
                    .addComponent(OBJ_39)
                    .addGap(11, 11, 11)
                    .addComponent(OBJ_44))
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                      .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(XEDE02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addComponent(XECO12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addComponent(XEDE03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                    .addGap(26, 26, 26)
                    .addComponent(XEDE05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(81, 81, 81)
                    .addComponent(XEDE04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                      .addComponent(XECP01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(XECP02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                    .addGap(26, 26, 26)
                    .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                      .addComponent(XECP04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(XECP05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(54, 54, 54)
                    .addComponent(XECP03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(7, 7, 7)
                    .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(7, 7, 7)
                    .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(7, 7, 7)
                    .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(7, 7, 7)
                    .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                      .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(XEDE07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addComponent(XEDE06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(26, 26, 26)
                    .addComponent(XEDE09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(108, 108, 108)
                    .addComponent(XEDE10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(54, 54, 54)
                    .addComponent(XEDE08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addComponent(XECP06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(26, 26, 26)
                    .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                      .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addComponent(XECP10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(XECP09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addComponent(XECP08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(27, 27, 27)
                    .addComponent(XECP07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
          );
        }
        p_contenu.add(xTitledPanel2);
        xTitledPanel2.setBounds(10, 235, 593, 195);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_27;
  private JLabel OBJ_32;
  private JLabel OBJ_37;
  private JLabel OBJ_42;
  private JLabel OBJ_47;
  private JLabel OBJ_52;
  private XRiTextField XECOEF;
  private XRiTextField XECO14;
  private XRiTextField XECO13;
  private XRiTextField XECO11;
  private XRiTextField XECO10;
  private XRiTextField XECO3;
  private JXTitledPanel xTitledPanel2;
  private XRiTextField XECP01;
  private XRiTextField XECP02;
  private XRiTextField XECP03;
  private XRiTextField XECP04;
  private XRiTextField XECP05;
  private JLabel OBJ_24;
  private JLabel OBJ_29;
  private JLabel OBJ_34;
  private JLabel OBJ_39;
  private JLabel OBJ_44;
  private XRiTextField XECO12;
  private XRiTextField XEDE02;
  private XRiTextField XEDE03;
  private XRiTextField XEDE04;
  private XRiTextField XEDE05;
  private XRiTextField XECP06;
  private XRiTextField XECP07;
  private XRiTextField XECP08;
  private XRiTextField XECP09;
  private XRiTextField XECP10;
  private JLabel OBJ_49;
  private JLabel OBJ_54;
  private JLabel OBJ_57;
  private JLabel OBJ_60;
  private JLabel OBJ_63;
  private XRiTextField XEDE06;
  private XRiTextField XEDE07;
  private XRiTextField XEDE08;
  private XRiTextField XEDE09;
  private XRiTextField XEDE10;
  private JXTitledSeparator xTitledSeparator1;
  private JXTitledSeparator xTitledSeparator2;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
