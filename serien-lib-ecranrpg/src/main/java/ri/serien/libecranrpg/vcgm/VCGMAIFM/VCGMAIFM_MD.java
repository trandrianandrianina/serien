
package ri.serien.libecranrpg.vcgm.VCGMAIFM;
// Nom Fichier: pop_VCGMAIFM_FMTMD_850.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class VCGMAIFM_MD extends SNPanelEcranRPG implements ioFrame {
  
  
  public VCGMAIFM_MD(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Choix fonctions"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "R");
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "M");
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "I");
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "A");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "D");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "C");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    OBJ_7 = new JButton();
    OBJ_8 = new JButton();
    OBJ_9 = new JButton();
    OBJ_10 = new JButton();
    OBJ_11 = new JButton();
    OBJ_12 = new JButton();

    //======== this ========
    setName("this");
    setLayout(null);

    //---- OBJ_7 ----
    OBJ_7.setText("Retour");
    OBJ_7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_7.setName("OBJ_7");
    OBJ_7.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_7ActionPerformed(e);
      }
    });
    add(OBJ_7);
    OBJ_7.setBounds(10, 10, 130, 24);

    //---- OBJ_8 ----
    OBJ_8.setText("Modification");
    OBJ_8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_8.setName("OBJ_8");
    OBJ_8.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_8ActionPerformed(e);
      }
    });
    add(OBJ_8);
    OBJ_8.setBounds(10, 65, 130, 24);

    //---- OBJ_9 ----
    OBJ_9.setText("Interrogation");
    OBJ_9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_9.setName("OBJ_9");
    OBJ_9.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_9ActionPerformed(e);
      }
    });
    add(OBJ_9);
    OBJ_9.setBounds(10, 90, 130, 24);

    //---- OBJ_10 ----
    OBJ_10.setText("Annulation");
    OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_10.setName("OBJ_10");
    OBJ_10.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_10ActionPerformed(e);
      }
    });
    add(OBJ_10);
    OBJ_10.setBounds(10, 120, 130, 24);

    //---- OBJ_11 ----
    OBJ_11.setText("Duplication");
    OBJ_11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_11.setName("OBJ_11");
    OBJ_11.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_11ActionPerformed(e);
      }
    });
    add(OBJ_11);
    OBJ_11.setBounds(10, 150, 130, 24);

    //---- OBJ_12 ----
    OBJ_12.setText("Cr\u00e9ation");
    OBJ_12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_12.setName("OBJ_12");
    OBJ_12.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_12ActionPerformed(e);
      }
    });
    add(OBJ_12);
    OBJ_12.setBounds(10, 35, 130, 24);

    setPreferredSize(new Dimension(150, 185));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JButton OBJ_7;
  private JButton OBJ_8;
  private JButton OBJ_9;
  private JButton OBJ_10;
  private JButton OBJ_11;
  private JButton OBJ_12;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
