
package ri.serien.libecranrpg.vcgm.VCGM20FM;
// Nom Fichier: pop_VCGM20FM_FMTMD_481.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;

import org.jdesktop.swingx.JXTitledSeparator;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class VCGM20FM_MD extends SNPanelEcranRPG implements ioFrame {
  
  
  public VCGM20FM_MD(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Gestion des dettes"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "R", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "C", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "M", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "I", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "A", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "D", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "L", false);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "Q", false);
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "V", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    OBJ_7 = new JButton();
    OBJ_8 = new JButton();
    OBJ_9 = new JButton();
    OBJ_10 = new JButton();
    OBJ_11 = new JButton();
    OBJ_12 = new JButton();
    OBJ_13 = new JButton();
    OBJ_14 = new JButton();
    OBJ_15 = new JButton();
    xTitledSeparator1 = new JXTitledSeparator();

    //======== this ========
    setName("this");
    setLayout(null);

    //---- OBJ_7 ----
    OBJ_7.setText("Retour");
    OBJ_7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_7.setName("OBJ_7");
    OBJ_7.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_7ActionPerformed(e);
      }
    });
    add(OBJ_7);
    OBJ_7.setBounds(34, 38, 130, 24);

    //---- OBJ_8 ----
    OBJ_8.setText("Cr\u00e9ation");
    OBJ_8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_8.setName("OBJ_8");
    OBJ_8.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_8ActionPerformed(e);
      }
    });
    add(OBJ_8);
    OBJ_8.setBounds(34, 66, 130, 24);

    //---- OBJ_9 ----
    OBJ_9.setText("Modification");
    OBJ_9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_9.setName("OBJ_9");
    OBJ_9.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_9ActionPerformed(e);
      }
    });
    add(OBJ_9);
    OBJ_9.setBounds(34, 96, 130, 24);

    //---- OBJ_10 ----
    OBJ_10.setText("Interrogation");
    OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_10.setName("OBJ_10");
    OBJ_10.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_10ActionPerformed(e);
      }
    });
    add(OBJ_10);
    OBJ_10.setBounds(34, 126, 130, 24);

    //---- OBJ_11 ----
    OBJ_11.setText("Annulation");
    OBJ_11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_11.setName("OBJ_11");
    OBJ_11.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_11ActionPerformed(e);
      }
    });
    add(OBJ_11);
    OBJ_11.setBounds(34, 156, 130, 24);

    //---- OBJ_12 ----
    OBJ_12.setText("Duplication");
    OBJ_12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_12.setName("OBJ_12");
    OBJ_12.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_12ActionPerformed(e);
      }
    });
    add(OBJ_12);
    OBJ_12.setBounds(34, 186, 130, 24);

    //---- OBJ_13 ----
    OBJ_13.setText("Lettres de change");
    OBJ_13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_13.setName("OBJ_13");
    OBJ_13.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_13ActionPerformed(e);
      }
    });
    add(OBJ_13);
    OBJ_13.setBounds(34, 216, 130, 24);

    //---- OBJ_14 ----
    OBJ_14.setText("Ch\u00e8que \u00e0 payer");
    OBJ_14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_14.setName("OBJ_14");
    OBJ_14.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_14ActionPerformed(e);
      }
    });
    add(OBJ_14);
    OBJ_14.setBounds(34, 246, 130, 24);

    //---- OBJ_15 ----
    OBJ_15.setText("Virements");
    OBJ_15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_15.setName("OBJ_15");
    OBJ_15.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_15ActionPerformed(e);
      }
    });
    add(OBJ_15);
    OBJ_15.setBounds(34, 276, 130, 24);

    //---- xTitledSeparator1 ----
    xTitledSeparator1.setTitle("Fonctions");
    xTitledSeparator1.setName("xTitledSeparator1");
    add(xTitledSeparator1);
    xTitledSeparator1.setBounds(10, 15, 180, xTitledSeparator1.getPreferredSize().height);

    setPreferredSize(new Dimension(196, 328));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JButton OBJ_7;
  private JButton OBJ_8;
  private JButton OBJ_9;
  private JButton OBJ_10;
  private JButton OBJ_11;
  private JButton OBJ_12;
  private JButton OBJ_13;
  private JButton OBJ_14;
  private JButton OBJ_15;
  private JXTitledSeparator xTitledSeparator1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
