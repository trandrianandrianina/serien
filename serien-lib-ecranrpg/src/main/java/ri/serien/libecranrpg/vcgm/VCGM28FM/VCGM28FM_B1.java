
package ri.serien.libecranrpg.vcgm.VCGM28FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM28FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  // TODO declarations classe spécifiques...
  
  public VCGM28FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    WDV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDV@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@"));
    
    

    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // TODO getDATA spécifiques
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_46 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_48 = new JLabel();
    INDNCG = new XRiTextField();
    INDNCA = new XRiTextField();
    OBJ_51 = new JLabel();
    INDPCE = new XRiTextField();
    OBJ_53 = new JLabel();
    INDRAN = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    RLLIBC = new XRiTextField();
    RLLOC = new XRiTextField();
    RLRUE = new XRiTextField();
    RLCPL = new XRiTextField();
    RLPAC = new XRiTextField();
    RLPAY = new XRiTextField();
    RLVIL = new XRiTextField();
    RLREPN = new XRiTextField();
    OBJ_60 = new JLabel();
    RLTEL = new XRiTextField();
    RLFAX = new XRiTextField();
    OBJ_59 = new JLabel();
    OBJ_80 = new JLabel();
    RLCDP = new XRiTextField();
    RLCOP = new XRiTextField();
    RLREP = new XRiTextField();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_88 = new JLabel();
    RLPCE = new XRiTextField();
    RLRAN = new XRiTextField();
    OBJ_91 = new JLabel();
    RLDECX = new XRiTextField();
    OBJ_93 = new JLabel();
    RLCJO = new XRiTextField();
    RLCFO = new XRiTextField();
    RLNLL = new XRiTextField();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_100 = new JLabel();
    RLNIV = new XRiTextField();
    OBJ_102 = new JLabel();
    RLDFAX = new XRiTextField();
    OBJ_104 = new JLabel();
    RLRGL = new XRiTextField();
    OBJ_106 = new JLabel();
    RLECHX = new XRiTextField();
    OBJ_66 = new JLabel();
    NJF = new XRiTextField();
    OBJ_68 = new JLabel();
    NBJ = new XRiTextField();
    OBJ_109 = new JLabel();
    RLMTTX = new XRiTextField();
    WSENS = new XRiTextField();
    WDV = new RiZoneSortie();
    RLLIB = new XRiTextField();
    OBJ_81 = new JLabel();
    OBJ_82 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_20 = new JMenuItem();
    OBJ_19 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion des relances");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_46 ----
          OBJ_46.setText("Soci\u00e9t\u00e9");
          OBJ_46.setName("OBJ_46");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_48 ----
          OBJ_48.setText("Compte");
          OBJ_48.setName("OBJ_48");

          //---- INDNCG ----
          INDNCG.setComponentPopupMenu(BTD);
          INDNCG.setName("INDNCG");

          //---- INDNCA ----
          INDNCA.setComponentPopupMenu(BTD);
          INDNCA.setName("INDNCA");

          //---- OBJ_51 ----
          OBJ_51.setText("Pi\u00e8ce");
          OBJ_51.setName("OBJ_51");

          //---- INDPCE ----
          INDPCE.setComponentPopupMenu(BTD);
          INDPCE.setName("INDPCE");

          //---- OBJ_53 ----
          OBJ_53.setText("R\u00e8glement");
          OBJ_53.setName("OBJ_53");

          //---- INDRAN ----
          INDRAN.setComponentPopupMenu(BTD);
          INDRAN.setName("INDRAN");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34)
                .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDNCG, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INDNCA, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDPCE, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22)
                .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDRAN, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDNCG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDNCA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDPCE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDRAN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_46)
                  .addComponent(OBJ_48)
                  .addComponent(OBJ_51)
                  .addComponent(OBJ_53)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Visualisation du compte");
              riSousMenu_bt6.setToolTipText("Visualisation du compte");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Analyse statistique");
              riSousMenu_bt7.setToolTipText("Analyse du compte (histogrammes)");
              riSousMenu_bt7.setSelectedIcon(null);
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Fiche r\u00e9vision");
              riSousMenu_bt8.setToolTipText("Fiche r\u00e9vision");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Facture dans GVM");
              riSousMenu_bt9.setToolTipText("Facture dans la gestion des ventes");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Bloc-notes");
              riSousMenu_bt14.setToolTipText("Bloc-notes");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(880, 560));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- RLLIBC ----
            RLLIBC.setName("RLLIBC");
            panel2.add(RLLIBC);
            RLLIBC.setBounds(20, 45, 310, RLLIBC.getPreferredSize().height);

            //---- RLLOC ----
            RLLOC.setName("RLLOC");
            panel2.add(RLLOC);
            RLLOC.setBounds(20, 135, 310, RLLOC.getPreferredSize().height);

            //---- RLRUE ----
            RLRUE.setName("RLRUE");
            panel2.add(RLRUE);
            RLRUE.setBounds(20, 105, 310, RLRUE.getPreferredSize().height);

            //---- RLCPL ----
            RLCPL.setName("RLCPL");
            panel2.add(RLCPL);
            RLCPL.setBounds(20, 75, 310, RLCPL.getPreferredSize().height);

            //---- RLPAC ----
            RLPAC.setName("RLPAC");
            panel2.add(RLPAC);
            RLPAC.setBounds(483, 195, 310, RLPAC.getPreferredSize().height);

            //---- RLPAY ----
            RLPAY.setName("RLPAY");
            panel2.add(RLPAY);
            RLPAY.setBounds(20, 195, 270, RLPAY.getPreferredSize().height);

            //---- RLVIL ----
            RLVIL.setName("RLVIL");
            panel2.add(RLVIL);
            RLVIL.setBounds(90, 165, 240, RLVIL.getPreferredSize().height);

            //---- RLREPN ----
            RLREPN.setName("RLREPN");
            panel2.add(RLREPN);
            RLREPN.setBounds(511, 45, 310, RLREPN.getPreferredSize().height);

            //---- OBJ_60 ----
            OBJ_60.setText("Repr\u00e9sentant");
            OBJ_60.setName("OBJ_60");
            panel2.add(OBJ_60);
            OBJ_60.setBounds(487, 20, 277, 25);

            //---- RLTEL ----
            RLTEL.setToolTipText("Num\u00e9ro de t\u00e9l\u00e9phone");
            RLTEL.setName("RLTEL");
            panel2.add(RLTEL);
            RLTEL.setBounds(553, 105, 172, RLTEL.getPreferredSize().height);

            //---- RLFAX ----
            RLFAX.setToolTipText("Num\u00e9ro de fax");
            RLFAX.setName("RLFAX");
            panel2.add(RLFAX);
            RLFAX.setBounds(553, 135, 172, RLFAX.getPreferredSize().height);

            //---- OBJ_59 ----
            OBJ_59.setText("Identification");
            OBJ_59.setName("OBJ_59");
            panel2.add(OBJ_59);
            OBJ_59.setBounds(20, 20, 252, 25);

            //---- OBJ_80 ----
            OBJ_80.setText("Contact");
            OBJ_80.setName("OBJ_80");
            panel2.add(OBJ_80);
            OBJ_80.setBounds(483, 170, 67, 28);

            //---- RLCDP ----
            RLCDP.setName("RLCDP");
            panel2.add(RLCDP);
            RLCDP.setBounds(20, 165, 52, RLCDP.getPreferredSize().height);

            //---- RLCOP ----
            RLCOP.setName("RLCOP");
            panel2.add(RLCOP);
            RLCOP.setBounds(290, 195, 40, RLCOP.getPreferredSize().height);

            //---- RLREP ----
            RLREP.setName("RLREP");
            panel2.add(RLREP);
            RLREP.setBounds(483, 45, 30, RLREP.getPreferredSize().height);

            //======== xTitledPanel1 ========
            {
              xTitledPanel1.setBorder(new DropShadowBorder());
              xTitledPanel1.setTitle("D\u00e9tail de l'\u00e9criture");
              xTitledPanel1.setName("xTitledPanel1");
              Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
              xTitledPanel1ContentContainer.setLayout(null);

              //---- OBJ_88 ----
              OBJ_88.setText("N\u00b0 pi\u00e8ce");
              OBJ_88.setName("OBJ_88");
              xTitledPanel1ContentContainer.add(OBJ_88);
              OBJ_88.setBounds(20, 10, 57, 28);

              //---- RLPCE ----
              RLPCE.setName("RLPCE");
              xTitledPanel1ContentContainer.add(RLPCE);
              RLPCE.setBounds(80, 10, 61, RLPCE.getPreferredSize().height);

              //---- RLRAN ----
              RLRAN.setName("RLRAN");
              xTitledPanel1ContentContainer.add(RLRAN);
              RLRAN.setBounds(150, 10, 22, RLRAN.getPreferredSize().height);

              //---- OBJ_91 ----
              OBJ_91.setText("Date d'\u00e9criture");
              OBJ_91.setName("OBJ_91");
              xTitledPanel1ContentContainer.add(OBJ_91);
              OBJ_91.setBounds(185, 10, 90, 28);

              //---- RLDECX ----
              RLDECX.setName("RLDECX");
              xTitledPanel1ContentContainer.add(RLDECX);
              RLDECX.setBounds(275, 10, 46, RLDECX.getPreferredSize().height);

              //---- OBJ_93 ----
              OBJ_93.setText("Journal / Folio / Ligne");
              OBJ_93.setName("OBJ_93");
              xTitledPanel1ContentContainer.add(OBJ_93);
              OBJ_93.setBounds(330, 10, 130, 28);

              //---- RLCJO ----
              RLCJO.setName("RLCJO");
              xTitledPanel1ContentContainer.add(RLCJO);
              RLCJO.setBounds(460, 10, 25, RLCJO.getPreferredSize().height);

              //---- RLCFO ----
              RLCFO.setName("RLCFO");
              xTitledPanel1ContentContainer.add(RLCFO);
              RLCFO.setBounds(490, 10, 48, RLCFO.getPreferredSize().height);

              //---- RLNLL ----
              RLNLL.setName("RLNLL");
              xTitledPanel1ContentContainer.add(RLNLL);
              RLNLL.setBounds(545, 10, 42, RLNLL.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = xTitledPanel1ContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
                xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
              }
            }
            panel2.add(xTitledPanel1);
            xTitledPanel1.setBounds(20, 250, 790, 80);

            //======== xTitledPanel2 ========
            {
              xTitledPanel2.setBorder(new DropShadowBorder());
              xTitledPanel2.setTitle("Relance");
              xTitledPanel2.setName("xTitledPanel2");
              Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
              xTitledPanel2ContentContainer.setLayout(null);

              //---- OBJ_100 ----
              OBJ_100.setText("Niveau");
              OBJ_100.setName("OBJ_100");
              xTitledPanel2ContentContainer.add(OBJ_100);
              OBJ_100.setBounds(15, 5, 60, 28);

              //---- RLNIV ----
              RLNIV.setComponentPopupMenu(BTD);
              RLNIV.setName("RLNIV");
              xTitledPanel2ContentContainer.add(RLNIV);
              RLNIV.setBounds(80, 5, 20, RLNIV.getPreferredSize().height);

              //---- OBJ_102 ----
              OBJ_102.setText("Date de facture");
              OBJ_102.setName("OBJ_102");
              xTitledPanel2ContentContainer.add(OBJ_102);
              OBJ_102.setBounds(180, 5, 94, 28);

              //---- RLDFAX ----
              RLDFAX.setName("RLDFAX");
              xTitledPanel2ContentContainer.add(RLDFAX);
              RLDFAX.setBounds(275, 5, 76, RLDFAX.getPreferredSize().height);

              //---- OBJ_104 ----
              OBJ_104.setText("R\u00e8glement");
              OBJ_104.setName("OBJ_104");
              xTitledPanel2ContentContainer.add(OBJ_104);
              OBJ_104.setBounds(380, 5, 69, 28);

              //---- RLRGL ----
              RLRGL.setComponentPopupMenu(BTD);
              RLRGL.setName("RLRGL");
              xTitledPanel2ContentContainer.add(RLRGL);
              RLRGL.setBounds(460, 5, 30, RLRGL.getPreferredSize().height);

              //---- OBJ_106 ----
              OBJ_106.setText("Ech\u00e9ance");
              OBJ_106.setName("OBJ_106");
              xTitledPanel2ContentContainer.add(OBJ_106);
              OBJ_106.setBounds(520, 5, 69, 28);

              //---- RLECHX ----
              RLECHX.setComponentPopupMenu(BTD);
              RLECHX.setName("RLECHX");
              xTitledPanel2ContentContainer.add(RLECHX);
              RLECHX.setBounds(614, 5, 76, RLECHX.getPreferredSize().height);

              //---- OBJ_66 ----
              OBJ_66.setText("Nombre jours");
              OBJ_66.setName("OBJ_66");
              xTitledPanel2ContentContainer.add(OBJ_66);
              OBJ_66.setBounds(180, 35, 90, 28);

              //---- NJF ----
              NJF.setName("NJF");
              xTitledPanel2ContentContainer.add(NJF);
              NJF.setBounds(275, 35, 44, NJF.getPreferredSize().height);

              //---- OBJ_68 ----
              OBJ_68.setText("Nombre jours");
              OBJ_68.setName("OBJ_68");
              xTitledPanel2ContentContainer.add(OBJ_68);
              OBJ_68.setBounds(520, 35, 85, 28);

              //---- NBJ ----
              NBJ.setName("NBJ");
              xTitledPanel2ContentContainer.add(NBJ);
              NBJ.setBounds(614, 35, 44, NBJ.getPreferredSize().height);

              //---- OBJ_109 ----
              OBJ_109.setText("Montant");
              OBJ_109.setName("OBJ_109");
              xTitledPanel2ContentContainer.add(OBJ_109);
              OBJ_109.setBounds(15, 65, 60, 28);

              //---- RLMTTX ----
              RLMTTX.setName("RLMTTX");
              xTitledPanel2ContentContainer.add(RLMTTX);
              RLMTTX.setBounds(80, 65, 94, RLMTTX.getPreferredSize().height);

              //---- WSENS ----
              WSENS.setName("WSENS");
              xTitledPanel2ContentContainer.add(WSENS);
              WSENS.setBounds(175, 65, 34, WSENS.getPreferredSize().height);

              //---- WDV ----
              WDV.setText("@WDV@");
              WDV.setName("WDV");
              xTitledPanel2ContentContainer.add(WDV);
              WDV.setBounds(275, 67, 45, WDV.getPreferredSize().height);

              //---- RLLIB ----
              RLLIB.setName("RLLIB");
              xTitledPanel2ContentContainer.add(RLLIB);
              RLLIB.setBounds(80, 100, 610, RLLIB.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = xTitledPanel2ContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
                xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
              }
            }
            panel2.add(xTitledPanel2);
            xTitledPanel2.setBounds(20, 345, 790, 175);

            //---- OBJ_81 ----
            OBJ_81.setText("Fax");
            OBJ_81.setName("OBJ_81");
            panel2.add(OBJ_81);
            OBJ_81.setBounds(483, 135, 67, 28);

            //---- OBJ_82 ----
            OBJ_82.setText("T\u00e9l\u00e9phone");
            OBJ_82.setName("OBJ_82");
            panel2.add(OBJ_82);
            OBJ_82.setBounds(483, 105, 67, 28);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 854, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 532, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_20 ----
      OBJ_20.setText("Choix possibles");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);

      //---- OBJ_19 ----
      OBJ_19.setText("Aide en ligne");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_46;
  private XRiTextField INDETB;
  private JLabel OBJ_48;
  private XRiTextField INDNCG;
  private XRiTextField INDNCA;
  private JLabel OBJ_51;
  private XRiTextField INDPCE;
  private JLabel OBJ_53;
  private XRiTextField INDRAN;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiTextField RLLIBC;
  private XRiTextField RLLOC;
  private XRiTextField RLRUE;
  private XRiTextField RLCPL;
  private XRiTextField RLPAC;
  private XRiTextField RLPAY;
  private XRiTextField RLVIL;
  private XRiTextField RLREPN;
  private JLabel OBJ_60;
  private XRiTextField RLTEL;
  private XRiTextField RLFAX;
  private JLabel OBJ_59;
  private JLabel OBJ_80;
  private XRiTextField RLCDP;
  private XRiTextField RLCOP;
  private XRiTextField RLREP;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_88;
  private XRiTextField RLPCE;
  private XRiTextField RLRAN;
  private JLabel OBJ_91;
  private XRiTextField RLDECX;
  private JLabel OBJ_93;
  private XRiTextField RLCJO;
  private XRiTextField RLCFO;
  private XRiTextField RLNLL;
  private JXTitledPanel xTitledPanel2;
  private JLabel OBJ_100;
  private XRiTextField RLNIV;
  private JLabel OBJ_102;
  private XRiTextField RLDFAX;
  private JLabel OBJ_104;
  private XRiTextField RLRGL;
  private JLabel OBJ_106;
  private XRiTextField RLECHX;
  private JLabel OBJ_66;
  private XRiTextField NJF;
  private JLabel OBJ_68;
  private XRiTextField NBJ;
  private JLabel OBJ_109;
  private XRiTextField RLMTTX;
  private XRiTextField WSENS;
  private RiZoneSortie WDV;
  private XRiTextField RLLIB;
  private JLabel OBJ_81;
  private JLabel OBJ_82;
  private JPopupMenu BTD;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
