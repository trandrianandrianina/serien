
package ri.serien.libecranrpg.vcgm.VCGM01FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM01FM_EH extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] EHSNS_Value = { "*", "C", "D", };
  private String[] EHSNS_Title = { "Tous sens autorisés", "Crédit", "Débit", };
  
  public VCGM01FM_EH(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    EHSNS.setValeurs(EHSNS_Value, EHSNS_Title);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    EHJ01_CHK.setSelected(lexique.HostFieldGetData("EHJ01").equals("**"));
    P_SEL0.setVisible(!EHJ01_CHK.isSelected());
    
    

    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    // riMenu_bt2.setIcon(lexique.getImage("images/options.png",true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    if (EHJ01_CHK.isSelected()) {
      lexique.HostFieldPutData("EHJ01", 0, "**");
      // lexique.HostFieldPutData("EHSNS", 0, EHSNS_Value[EHSNS.getSelectedIndex()]);
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void EHJ01_CHKActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
    if (!EHJ01_CHK.isSelected()) {
      EHJ01.setText("");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44_OBJ_45 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_45_OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46_OBJ_47 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_86_OBJ_87 = new JLabel();
    P_SEL0 = new JPanel();
    EHJ01 = new XRiTextField();
    EHJ02 = new XRiTextField();
    EHJ03 = new XRiTextField();
    EHJ04 = new XRiTextField();
    EHJ05 = new XRiTextField();
    EHJ01_CHK = new JCheckBox();
    panel2 = new JPanel();
    EHC01 = new XRiTextField();
    EHC02 = new XRiTextField();
    EHC03 = new XRiTextField();
    EHC04 = new XRiTextField();
    EHC05 = new XRiTextField();
    EHSNS = new XRiComboBox();
    panel1 = new JPanel();
    OBJ_79_OBJ_79 = new JLabel();
    OBJ_83_OBJ_83 = new JLabel();
    OBJ_81_OBJ_81 = new JLabel();
    OBJ_63_OBJ_63 = new JLabel();
    EHL01 = new XRiTextField();
    EHL02 = new XRiTextField();
    EHL03 = new XRiTextField();
    EHL04 = new XRiTextField();
    OBJ_85_OBJ_85 = new JLabel();
    EHL05 = new XRiTextField();
    OBJ_78_OBJ_78 = new JLabel();
    OBJ_87_OBJ_87 = new JLabel();
    EHL06 = new XRiTextField();
    EHL07 = new XRiTextField();
    OBJ_82_OBJ_82 = new JLabel();
    OBJ_80_OBJ_80 = new JLabel();
    OBJ_84_OBJ_84 = new JLabel();
    EHL08 = new XRiTextField();
    EHL09 = new XRiTextField();
    EHL10 = new XRiTextField();
    OBJ_52_OBJ_52 = new JLabel();
    OBJ_51_OBJ_51 = new JLabel();
    OBJ_88_OBJ_88 = new JLabel();
    OBJ_86_OBJ_86 = new JLabel();
    EHL11 = new XRiTextField();
    EHL12 = new XRiTextField();
    EHL13 = new XRiTextField();
    EHL14 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44_OBJ_45 ----
          OBJ_44_OBJ_45.setText("Soci\u00e9t\u00e9");
          OBJ_44_OBJ_45.setName("OBJ_44_OBJ_45");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setName("INDETB");

          //---- OBJ_45_OBJ_46 ----
          OBJ_45_OBJ_46.setText("Code");
          OBJ_45_OBJ_46.setName("OBJ_45_OBJ_46");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setName("INDTYP");

          //---- OBJ_46_OBJ_47 ----
          OBJ_46_OBJ_47.setText("Ordre");
          OBJ_46_OBJ_47.setName("OBJ_46_OBJ_47");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(800, 480));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Extraction des honoraires");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- OBJ_86_OBJ_87 ----
            OBJ_86_OBJ_87.setText("Sens \u00e9critures comptables");
            OBJ_86_OBJ_87.setName("OBJ_86_OBJ_87");
            xTitledPanel1ContentContainer.add(OBJ_86_OBJ_87);
            OBJ_86_OBJ_87.setBounds(20, 96, 165, 20);

            //======== P_SEL0 ========
            {
              P_SEL0.setOpaque(false);
              P_SEL0.setBorder(new TitledBorder("Codes journaux"));
              P_SEL0.setName("P_SEL0");
              P_SEL0.setLayout(null);

              //---- EHJ01 ----
              EHJ01.setName("EHJ01");
              P_SEL0.add(EHJ01);
              EHJ01.setBounds(35, 35, 30, EHJ01.getPreferredSize().height);

              //---- EHJ02 ----
              EHJ02.setName("EHJ02");
              P_SEL0.add(EHJ02);
              EHJ02.setBounds(75, 35, 30, EHJ02.getPreferredSize().height);

              //---- EHJ03 ----
              EHJ03.setName("EHJ03");
              P_SEL0.add(EHJ03);
              EHJ03.setBounds(120, 35, 30, EHJ03.getPreferredSize().height);

              //---- EHJ04 ----
              EHJ04.setName("EHJ04");
              P_SEL0.add(EHJ04);
              EHJ04.setBounds(165, 35, 30, EHJ04.getPreferredSize().height);

              //---- EHJ05 ----
              EHJ05.setName("EHJ05");
              P_SEL0.add(EHJ05);
              EHJ05.setBounds(210, 35, 30, EHJ05.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < P_SEL0.getComponentCount(); i++) {
                  Rectangle bounds = P_SEL0.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = P_SEL0.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                P_SEL0.setMinimumSize(preferredSize);
                P_SEL0.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(P_SEL0);
            P_SEL0.setBounds(185, 5, 283, 80);

            //---- EHJ01_CHK ----
            EHJ01_CHK.setText("Tous les journaux");
            EHJ01_CHK.setName("EHJ01_CHK");
            EHJ01_CHK.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                EHJ01_CHKActionPerformed(e);
              }
            });
            xTitledPanel1ContentContainer.add(EHJ01_CHK);
            EHJ01_CHK.setBounds(20, 36, 140, EHJ01_CHK.getPreferredSize().height);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("Collectifs \u00e0 s\u00e9lectionner"));
              panel2.setOpaque(false);
              panel2.setName("panel2");

              //---- EHC01 ----
              EHC01.setName("EHC01");

              //---- EHC02 ----
              EHC02.setName("EHC02");

              //---- EHC03 ----
              EHC03.setName("EHC03");

              //---- EHC04 ----
              EHC04.setName("EHC04");

              //---- EHC05 ----
              EHC05.setName("EHC05");

              GroupLayout panel2Layout = new GroupLayout(panel2);
              panel2.setLayout(panel2Layout);
              panel2Layout.setHorizontalGroup(
                panel2Layout.createParallelGroup()
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(21, 21, 21)
                    .addComponent(EHC01, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                    .addGap(35, 35, 35)
                    .addComponent(EHC02, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                    .addGap(35, 35, 35)
                    .addComponent(EHC03, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                    .addGap(35, 35, 35)
                    .addComponent(EHC04, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                    .addGap(35, 35, 35)
                    .addComponent(EHC05, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
              );
              panel2Layout.setVerticalGroup(
                panel2Layout.createParallelGroup()
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addGroup(panel2Layout.createParallelGroup()
                      .addComponent(EHC01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(EHC02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(EHC03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(EHC04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(EHC05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
              );
            }
            xTitledPanel1ContentContainer.add(panel2);
            panel2.setBounds(20, 135, 735, 102);

            //---- EHSNS ----
            EHSNS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EHSNS.setName("EHSNS");
            xTitledPanel1ContentContainer.add(EHSNS);
            EHSNS.setBounds(185, 93, 175, EHSNS.getPreferredSize().height);

            //======== panel1 ========
            {
              panel1.setBorder(new TitledBorder("D\u00e9tail des honoraires"));
              panel1.setOpaque(false);
              panel1.setName("panel1");

              //---- OBJ_79_OBJ_79 ----
              OBJ_79_OBJ_79.setText("Commissions");
              OBJ_79_OBJ_79.setName("OBJ_79_OBJ_79");

              //---- OBJ_83_OBJ_83 ----
              OBJ_83_OBJ_83.setText("Ristournes");
              OBJ_83_OBJ_83.setName("OBJ_83_OBJ_83");

              //---- OBJ_81_OBJ_81 ----
              OBJ_81_OBJ_81.setText("Courtages");
              OBJ_81_OBJ_81.setName("OBJ_81_OBJ_81");

              //---- OBJ_63_OBJ_63 ----
              OBJ_63_OBJ_63.setText("Vacations");
              OBJ_63_OBJ_63.setName("OBJ_63_OBJ_63");

              //---- EHL01 ----
              EHL01.setName("EHL01");

              //---- EHL02 ----
              EHL02.setName("EHL02");

              //---- EHL03 ----
              EHL03.setName("EHL03");

              //---- EHL04 ----
              EHL04.setName("EHL04");

              //---- OBJ_85_OBJ_85 ----
              OBJ_85_OBJ_85.setText("Jetons de pr\u00e9sence");
              OBJ_85_OBJ_85.setName("OBJ_85_OBJ_85");

              //---- EHL05 ----
              EHL05.setName("EHL05");

              //---- OBJ_78_OBJ_78 ----
              OBJ_78_OBJ_78.setText("Droits d'inventeur");
              OBJ_78_OBJ_78.setName("OBJ_78_OBJ_78");

              //---- OBJ_87_OBJ_87 ----
              OBJ_87_OBJ_87.setText("Droits d'auteur");
              OBJ_87_OBJ_87.setName("OBJ_87_OBJ_87");

              //---- EHL06 ----
              EHL06.setName("EHL06");

              //---- EHL07 ----
              EHL07.setName("EHL07");

              //---- OBJ_82_OBJ_82 ----
              OBJ_82_OBJ_82.setText("Indemnisation remboursement");
              OBJ_82_OBJ_82.setName("OBJ_82_OBJ_82");

              //---- OBJ_80_OBJ_80 ----
              OBJ_80_OBJ_80.setText("Autres r\u00e9mun\u00e9rations");
              OBJ_80_OBJ_80.setName("OBJ_80_OBJ_80");

              //---- OBJ_84_OBJ_84 ----
              OBJ_84_OBJ_84.setText("Avantages en nature");
              OBJ_84_OBJ_84.setName("OBJ_84_OBJ_84");

              //---- EHL08 ----
              EHL08.setName("EHL08");

              //---- EHL09 ----
              EHL09.setName("EHL09");

              //---- EHL10 ----
              EHL10.setName("EHL10");

              //---- OBJ_52_OBJ_52 ----
              OBJ_52_OBJ_52.setText("Retenue source D.auteur");
              OBJ_52_OBJ_52.setName("OBJ_52_OBJ_52");

              //---- OBJ_51_OBJ_51 ----
              OBJ_51_OBJ_51.setText("TVA droits d'auteur");
              OBJ_51_OBJ_51.setName("OBJ_51_OBJ_51");

              //---- OBJ_88_OBJ_88 ----
              OBJ_88_OBJ_88.setText("TVA Nette / Imp\u00f4ts");
              OBJ_88_OBJ_88.setName("OBJ_88_OBJ_88");

              //---- OBJ_86_OBJ_86 ----
              OBJ_86_OBJ_86.setText("Retenues Imp\u00f4ts");
              OBJ_86_OBJ_86.setName("OBJ_86_OBJ_86");

              //---- EHL11 ----
              EHL11.setName("EHL11");

              //---- EHL12 ----
              EHL12.setName("EHL12");

              //---- EHL13 ----
              EHL13.setName("EHL13");

              //---- EHL14 ----
              EHL14.setName("EHL14");

              GroupLayout panel1Layout = new GroupLayout(panel1);
              panel1.setLayout(panel1Layout);
              panel1Layout.setHorizontalGroup(
                panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addGroup(panel1Layout.createParallelGroup()
                      .addComponent(OBJ_63_OBJ_63, GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_79_OBJ_79, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_81_OBJ_81, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_83_OBJ_83, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_85_OBJ_85, GroupLayout.PREFERRED_SIZE, 126, GroupLayout.PREFERRED_SIZE))
                    .addGap(19, 19, 19)
                    .addGroup(panel1Layout.createParallelGroup()
                      .addComponent(EHL02, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(EHL01, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(EHL05, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(EHL04, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(EHL03, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGap(60, 60, 60)
                    .addGroup(panel1Layout.createParallelGroup()
                      .addComponent(OBJ_87_OBJ_87, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_78_OBJ_78, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_80_OBJ_80, GroupLayout.PREFERRED_SIZE, 129, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_82_OBJ_82, GroupLayout.PREFERRED_SIZE, 183, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_84_OBJ_84, GroupLayout.PREFERRED_SIZE, 126, GroupLayout.PREFERRED_SIZE))
                    .addGap(7, 7, 7)
                    .addGroup(panel1Layout.createParallelGroup()
                      .addComponent(EHL08, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(EHL10, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(EHL06, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(EHL07, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(EHL09, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGap(55, 55, 55)
                    .addGroup(panel1Layout.createParallelGroup()
                      .addComponent(OBJ_86_OBJ_86, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_88_OBJ_88, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_51_OBJ_51, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_52_OBJ_52, GroupLayout.PREFERRED_SIZE, 169, GroupLayout.PREFERRED_SIZE))
                    .addGap(16, 16, 16)
                    .addGroup(panel1Layout.createParallelGroup()
                      .addComponent(EHL11, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(EHL12, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(EHL14, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(EHL13, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
              );
              panel1Layout.setVerticalGroup(
                panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(OBJ_63_OBJ_63, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(3, 3, 3)
                    .addComponent(OBJ_79_OBJ_79, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(3, 3, 3)
                    .addComponent(OBJ_81_OBJ_81, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(3, 3, 3)
                    .addComponent(OBJ_83_OBJ_83, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(3, 3, 3)
                    .addComponent(OBJ_85_OBJ_85))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addGroup(panel1Layout.createParallelGroup()
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGroup(panel1Layout.createParallelGroup()
                          .addGroup(panel1Layout.createSequentialGroup()
                            .addGap(23, 23, 23)
                            .addComponent(EHL02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addComponent(EHL01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(panel1Layout.createParallelGroup()
                          .addGroup(panel1Layout.createSequentialGroup()
                            .addGap(21, 21, 21)
                            .addComponent(EHL05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addComponent(EHL04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(46, 46, 46)
                        .addComponent(EHL03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(12, 12, 12)
                    .addComponent(OBJ_87_OBJ_87)
                    .addGap(7, 7, 7)
                    .addComponent(OBJ_78_OBJ_78)
                    .addGap(7, 7, 7)
                    .addComponent(OBJ_80_OBJ_80)
                    .addGap(7, 7, 7)
                    .addComponent(OBJ_82_OBJ_82)
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_84_OBJ_84))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addGroup(panel1Layout.createParallelGroup()
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(46, 46, 46)
                        .addComponent(EHL08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(16, 16, 16)
                        .addComponent(EHL10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addComponent(EHL06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addComponent(EHL07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(69, 69, 69)
                        .addComponent(EHL09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(12, 12, 12)
                    .addComponent(OBJ_86_OBJ_86)
                    .addGap(7, 7, 7)
                    .addComponent(OBJ_88_OBJ_88)
                    .addGap(7, 7, 7)
                    .addComponent(OBJ_51_OBJ_51)
                    .addGap(7, 7, 7)
                    .addComponent(OBJ_52_OBJ_52))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addGroup(panel1Layout.createParallelGroup()
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGroup(panel1Layout.createParallelGroup()
                          .addComponent(EHL11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel1Layout.createSequentialGroup()
                            .addGap(23, 23, 23)
                            .addComponent(EHL12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addComponent(EHL14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(46, 46, 46)
                        .addComponent(EHL13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
              );
            }
            xTitledPanel1ContentContainer.add(panel1);
            panel1.setBounds(20, 244, 735, 170);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 774, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 452, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44_OBJ_45;
  private XRiTextField INDETB;
  private JLabel OBJ_45_OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_46_OBJ_47;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_86_OBJ_87;
  private JPanel P_SEL0;
  private XRiTextField EHJ01;
  private XRiTextField EHJ02;
  private XRiTextField EHJ03;
  private XRiTextField EHJ04;
  private XRiTextField EHJ05;
  private JCheckBox EHJ01_CHK;
  private JPanel panel2;
  private XRiTextField EHC01;
  private XRiTextField EHC02;
  private XRiTextField EHC03;
  private XRiTextField EHC04;
  private XRiTextField EHC05;
  private XRiComboBox EHSNS;
  private JPanel panel1;
  private JLabel OBJ_79_OBJ_79;
  private JLabel OBJ_83_OBJ_83;
  private JLabel OBJ_81_OBJ_81;
  private JLabel OBJ_63_OBJ_63;
  private XRiTextField EHL01;
  private XRiTextField EHL02;
  private XRiTextField EHL03;
  private XRiTextField EHL04;
  private JLabel OBJ_85_OBJ_85;
  private XRiTextField EHL05;
  private JLabel OBJ_78_OBJ_78;
  private JLabel OBJ_87_OBJ_87;
  private XRiTextField EHL06;
  private XRiTextField EHL07;
  private JLabel OBJ_82_OBJ_82;
  private JLabel OBJ_80_OBJ_80;
  private JLabel OBJ_84_OBJ_84;
  private XRiTextField EHL08;
  private XRiTextField EHL09;
  private XRiTextField EHL10;
  private JLabel OBJ_52_OBJ_52;
  private JLabel OBJ_51_OBJ_51;
  private JLabel OBJ_88_OBJ_88;
  private JLabel OBJ_86_OBJ_86;
  private XRiTextField EHL11;
  private XRiTextField EHL12;
  private XRiTextField EHL13;
  private XRiTextField EHL14;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
