
package ri.serien.libecranrpg.vcgm.VCGM55FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGM55FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM55FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_33_OBJ_33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLAXE@")).trim());
    WVA01.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA01@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER01@")).trim());
    WVA02.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA02@")).trim());
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER02@")).trim());
    WVA03.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA03@")).trim());
    label3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER03@")).trim());
    WVA04.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA04@")).trim());
    label4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER04@")).trim());
    WVA05.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA05@")).trim());
    label5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER05@")).trim());
    WVA06.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA06@")).trim());
    label6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER06@")).trim());
    WVA07.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA07@")).trim());
    label7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER07@")).trim());
    WVA08.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA08@")).trim());
    label8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER08@")).trim());
    WVA09.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA09@")).trim());
    label9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER09@")).trim());
    WVA10.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA10@")).trim());
    label18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER10@")).trim());
    WVA11.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA11@")).trim());
    label19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER11@")).trim());
    WVA12.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA12@")).trim());
    label20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER12@")).trim());
    WVA13.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA13@")).trim());
    label21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER13@")).trim());
    WVA14.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA14@")).trim());
    label22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER14@")).trim());
    WVA15.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA15@")).trim());
    label23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER15@")).trim());
    WVA16.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA16@")).trim());
    label24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER16@")).trim());
    WVA17.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA17@")).trim());
    label25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER17@")).trim());
    WVA18.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA18@")).trim());
    label26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER18@")).trim());
    WVA19.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA19@")).trim());
    label27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER19@")).trim());
    WVA20.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA20@")).trim());
    label28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER20@")).trim());
    WVA21.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA21@")).trim());
    label29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER21@")).trim());
    WVA22.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA22@")).trim());
    label30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER22@")).trim());
    WVA23.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA23@")).trim());
    label31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER23@")).trim());
    WVA24.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA24@")).trim());
    label32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER24@")).trim());
    WVA25.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA25@")).trim());
    label33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER25@")).trim());
    WVA26.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA26@")).trim());
    label34.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER26@")).trim());
    WVA27.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA27@")).trim());
    label35.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER27@")).trim());
    WVA28.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA28@")).trim());
    label36.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER28@")).trim());
    WVA29.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA29@")).trim());
    label37.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER29@")).trim());
    WVA30.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA30@")).trim());
    label38.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER30@")).trim());
    WVA31.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA31@")).trim());
    label39.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER31@")).trim());
    WVA32.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA32@")).trim());
    label40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER32@")).trim());
    WVA33.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA33@")).trim());
    label41.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER33@")).trim());
    WVA34.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA34@")).trim());
    label42.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER34@")).trim());
    WVA35.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA35@")).trim());
    label43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER35@")).trim());
    WVA36.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA36@")).trim());
    label44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER36@")).trim());
    WVA37.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA37@")).trim());
    label45.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER37@")).trim());
    WVA38.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA38@")).trim());
    label46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER38@")).trim());
    WVA39.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA39@")).trim());
    label47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER39@")).trim());
    WVA40.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA40@")).trim());
    label48.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER40@")).trim());
    WVA41.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA41@")).trim());
    label49.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER41@")).trim());
    WVA42.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LVA42@")).trim());
    label50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLER42@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    OBJ_29_OBJ_29.setVisible(lexique.isPresent("WECVTL"));
    OBJ_37_OBJ_37.setVisible(lexique.isPresent("WMTVTL"));
    OBJ_33_OBJ_33.setVisible(lexique.isPresent("WLAXE"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Affichage de l'imputation"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_33_OBJ_33 = new JLabel();
    OBJ_37_OBJ_37 = new JLabel();
    WMTVTL = new XRiTextField();
    WECVTL = new XRiTextField();
    OBJ_29_OBJ_29 = new JLabel();
    WVM01 = new XRiTextField();
    WVA01 = new XRiTextField();
    label1 = new JLabel();
    WVM02 = new XRiTextField();
    WVA02 = new XRiTextField();
    label2 = new JLabel();
    WVM03 = new XRiTextField();
    WVA03 = new XRiTextField();
    label3 = new JLabel();
    WVM04 = new XRiTextField();
    WVA04 = new XRiTextField();
    label4 = new JLabel();
    WVM05 = new XRiTextField();
    WVA05 = new XRiTextField();
    label5 = new JLabel();
    WVM06 = new XRiTextField();
    WVA06 = new XRiTextField();
    label6 = new JLabel();
    WVM07 = new XRiTextField();
    WVA07 = new XRiTextField();
    label7 = new JLabel();
    WVM08 = new XRiTextField();
    WVA08 = new XRiTextField();
    label8 = new JLabel();
    WVM09 = new XRiTextField();
    WVA09 = new XRiTextField();
    label9 = new JLabel();
    WVM10 = new XRiTextField();
    WVA10 = new XRiTextField();
    label18 = new JLabel();
    WVM11 = new XRiTextField();
    WVA11 = new XRiTextField();
    label19 = new JLabel();
    WVM12 = new XRiTextField();
    WVA12 = new XRiTextField();
    label20 = new JLabel();
    WVM13 = new XRiTextField();
    WVA13 = new XRiTextField();
    label21 = new JLabel();
    WVM14 = new XRiTextField();
    WVA14 = new XRiTextField();
    label22 = new JLabel();
    WVM15 = new XRiTextField();
    WVA15 = new XRiTextField();
    label23 = new JLabel();
    WVM16 = new XRiTextField();
    WVA16 = new XRiTextField();
    label24 = new JLabel();
    WVM17 = new XRiTextField();
    WVA17 = new XRiTextField();
    label25 = new JLabel();
    WVM18 = new XRiTextField();
    WVA18 = new XRiTextField();
    label26 = new JLabel();
    WVM19 = new XRiTextField();
    WVA19 = new XRiTextField();
    label27 = new JLabel();
    WVM20 = new XRiTextField();
    WVA20 = new XRiTextField();
    label28 = new JLabel();
    WVM21 = new XRiTextField();
    WVA21 = new XRiTextField();
    label29 = new JLabel();
    WVM22 = new XRiTextField();
    WVA22 = new XRiTextField();
    label30 = new JLabel();
    WVM23 = new XRiTextField();
    WVA23 = new XRiTextField();
    label31 = new JLabel();
    WVM24 = new XRiTextField();
    WVA24 = new XRiTextField();
    label32 = new JLabel();
    WVM25 = new XRiTextField();
    WVA25 = new XRiTextField();
    label33 = new JLabel();
    WVM26 = new XRiTextField();
    WVA26 = new XRiTextField();
    label34 = new JLabel();
    WVM27 = new XRiTextField();
    WVA27 = new XRiTextField();
    label35 = new JLabel();
    WVM28 = new XRiTextField();
    WVA28 = new XRiTextField();
    label36 = new JLabel();
    WVM29 = new XRiTextField();
    WVA29 = new XRiTextField();
    label37 = new JLabel();
    WVM30 = new XRiTextField();
    WVA30 = new XRiTextField();
    label38 = new JLabel();
    WVM31 = new XRiTextField();
    WVA31 = new XRiTextField();
    label39 = new JLabel();
    WVM32 = new XRiTextField();
    WVA32 = new XRiTextField();
    label40 = new JLabel();
    WVM33 = new XRiTextField();
    WVA33 = new XRiTextField();
    label41 = new JLabel();
    WVM34 = new XRiTextField();
    WVA34 = new XRiTextField();
    label42 = new JLabel();
    WVM35 = new XRiTextField();
    WVA35 = new XRiTextField();
    label43 = new JLabel();
    WVM36 = new XRiTextField();
    WVA36 = new XRiTextField();
    label44 = new JLabel();
    WVM37 = new XRiTextField();
    WVA37 = new XRiTextField();
    label45 = new JLabel();
    WVM38 = new XRiTextField();
    WVA38 = new XRiTextField();
    label46 = new JLabel();
    WVM39 = new XRiTextField();
    WVA39 = new XRiTextField();
    label47 = new JLabel();
    WVM40 = new XRiTextField();
    WVA40 = new XRiTextField();
    label48 = new JLabel();
    WVM41 = new XRiTextField();
    WVA41 = new XRiTextField();
    label49 = new JLabel();
    WVM42 = new XRiTextField();
    WVA42 = new XRiTextField();
    label50 = new JLabel();
    OBJ_37_OBJ_38 = new JLabel();
    OBJ_37_OBJ_39 = new JLabel();
    OBJ_37_OBJ_40 = new JLabel();
    OBJ_37_OBJ_41 = new JLabel();
    OBJ_37_OBJ_42 = new JLabel();
    OBJ_37_OBJ_43 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 580));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Imputation multi-axes"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_33_OBJ_33 ----
          OBJ_33_OBJ_33.setText("@WLAXE@");
          OBJ_33_OBJ_33.setName("OBJ_33_OBJ_33");
          panel1.add(OBJ_33_OBJ_33);
          OBJ_33_OBJ_33.setBounds(20, 520, 328, 28);

          //---- OBJ_37_OBJ_37 ----
          OBJ_37_OBJ_37.setText("Montant par ventilation");
          OBJ_37_OBJ_37.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_37_OBJ_37.setFont(OBJ_37_OBJ_37.getFont().deriveFont(OBJ_37_OBJ_37.getFont().getStyle() | Font.BOLD));
          OBJ_37_OBJ_37.setName("OBJ_37_OBJ_37");
          panel1.add(OBJ_37_OBJ_37);
          OBJ_37_OBJ_37.setBounds(390, 485, 145, 28);

          //---- WMTVTL ----
          WMTVTL.setName("WMTVTL");
          panel1.add(WMTVTL);
          WMTVTL.setBounds(545, 485, 105, WMTVTL.getPreferredSize().height);

          //---- WECVTL ----
          WECVTL.setName("WECVTL");
          panel1.add(WECVTL);
          WECVTL.setBounds(545, 520, 105, WECVTL.getPreferredSize().height);

          //---- OBJ_29_OBJ_29 ----
          OBJ_29_OBJ_29.setText("Ecart");
          OBJ_29_OBJ_29.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_29_OBJ_29.setFont(OBJ_29_OBJ_29.getFont().deriveFont(OBJ_29_OBJ_29.getFont().getStyle() | Font.BOLD));
          OBJ_29_OBJ_29.setName("OBJ_29_OBJ_29");
          panel1.add(OBJ_29_OBJ_29);
          OBJ_29_OBJ_29.setBounds(480, 520, 52, 28);

          //---- WVM01 ----
          WVM01.setName("WVM01");
          panel1.add(WVM01);
          WVM01.setBounds(20, 55, 105, WVM01.getPreferredSize().height);

          //---- WVA01 ----
          WVA01.setToolTipText("@LVA01@");
          WVA01.setName("WVA01");
          panel1.add(WVA01);
          WVA01.setBounds(130, 55, 60, WVA01.getPreferredSize().height);

          //---- label1 ----
          label1.setText("@WLER01@");
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(195, 55, 25, 28);

          //---- WVM02 ----
          WVM02.setName("WVM02");
          panel1.add(WVM02);
          WVM02.setBounds(250, 55, 105, WVM02.getPreferredSize().height);

          //---- WVA02 ----
          WVA02.setToolTipText("@LVA02@");
          WVA02.setName("WVA02");
          panel1.add(WVA02);
          WVA02.setBounds(360, 55, 60, WVA02.getPreferredSize().height);

          //---- label2 ----
          label2.setText("@WLER02@");
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(425, 55, 25, 28);

          //---- WVM03 ----
          WVM03.setName("WVM03");
          panel1.add(WVM03);
          WVM03.setBounds(480, 55, 105, WVM03.getPreferredSize().height);

          //---- WVA03 ----
          WVA03.setToolTipText("@LVA03@");
          WVA03.setName("WVA03");
          panel1.add(WVA03);
          WVA03.setBounds(590, 55, 60, WVA03.getPreferredSize().height);

          //---- label3 ----
          label3.setText("@WLER03@");
          label3.setName("label3");
          panel1.add(label3);
          label3.setBounds(655, 55, 25, 28);

          //---- WVM04 ----
          WVM04.setName("WVM04");
          panel1.add(WVM04);
          WVM04.setBounds(20, 85, 105, WVM04.getPreferredSize().height);

          //---- WVA04 ----
          WVA04.setToolTipText("@LVA04@");
          WVA04.setName("WVA04");
          panel1.add(WVA04);
          WVA04.setBounds(130, 85, 60, WVA04.getPreferredSize().height);

          //---- label4 ----
          label4.setText("@WLER04@");
          label4.setName("label4");
          panel1.add(label4);
          label4.setBounds(195, 85, 25, 28);

          //---- WVM05 ----
          WVM05.setName("WVM05");
          panel1.add(WVM05);
          WVM05.setBounds(250, 85, 105, WVM05.getPreferredSize().height);

          //---- WVA05 ----
          WVA05.setToolTipText("@LVA05@");
          WVA05.setName("WVA05");
          panel1.add(WVA05);
          WVA05.setBounds(360, 85, 60, WVA05.getPreferredSize().height);

          //---- label5 ----
          label5.setText("@WLER05@");
          label5.setName("label5");
          panel1.add(label5);
          label5.setBounds(425, 85, 25, 28);

          //---- WVM06 ----
          WVM06.setName("WVM06");
          panel1.add(WVM06);
          WVM06.setBounds(480, 85, 105, WVM06.getPreferredSize().height);

          //---- WVA06 ----
          WVA06.setToolTipText("@LVA06@");
          WVA06.setName("WVA06");
          panel1.add(WVA06);
          WVA06.setBounds(590, 85, 60, WVA06.getPreferredSize().height);

          //---- label6 ----
          label6.setText("@WLER06@");
          label6.setName("label6");
          panel1.add(label6);
          label6.setBounds(655, 85, 25, 28);

          //---- WVM07 ----
          WVM07.setName("WVM07");
          panel1.add(WVM07);
          WVM07.setBounds(20, 115, 105, WVM07.getPreferredSize().height);

          //---- WVA07 ----
          WVA07.setToolTipText("@LVA07@");
          WVA07.setName("WVA07");
          panel1.add(WVA07);
          WVA07.setBounds(130, 115, 60, WVA07.getPreferredSize().height);

          //---- label7 ----
          label7.setText("@WLER07@");
          label7.setName("label7");
          panel1.add(label7);
          label7.setBounds(195, 115, 25, 28);

          //---- WVM08 ----
          WVM08.setName("WVM08");
          panel1.add(WVM08);
          WVM08.setBounds(250, 115, 105, WVM08.getPreferredSize().height);

          //---- WVA08 ----
          WVA08.setToolTipText("@LVA08@");
          WVA08.setName("WVA08");
          panel1.add(WVA08);
          WVA08.setBounds(360, 115, 60, WVA08.getPreferredSize().height);

          //---- label8 ----
          label8.setText("@WLER08@");
          label8.setName("label8");
          panel1.add(label8);
          label8.setBounds(425, 115, 25, 28);

          //---- WVM09 ----
          WVM09.setName("WVM09");
          panel1.add(WVM09);
          WVM09.setBounds(480, 115, 105, WVM09.getPreferredSize().height);

          //---- WVA09 ----
          WVA09.setToolTipText("@LVA09@");
          WVA09.setName("WVA09");
          panel1.add(WVA09);
          WVA09.setBounds(590, 115, 60, WVA09.getPreferredSize().height);

          //---- label9 ----
          label9.setText("@WLER09@");
          label9.setName("label9");
          panel1.add(label9);
          label9.setBounds(655, 115, 25, 28);

          //---- WVM10 ----
          WVM10.setName("WVM10");
          panel1.add(WVM10);
          WVM10.setBounds(20, 145, 105, WVM10.getPreferredSize().height);

          //---- WVA10 ----
          WVA10.setToolTipText("@LVA10@");
          WVA10.setName("WVA10");
          panel1.add(WVA10);
          WVA10.setBounds(130, 145, 60, WVA10.getPreferredSize().height);

          //---- label18 ----
          label18.setText("@WLER10@");
          label18.setName("label18");
          panel1.add(label18);
          label18.setBounds(195, 145, 25, 28);

          //---- WVM11 ----
          WVM11.setName("WVM11");
          panel1.add(WVM11);
          WVM11.setBounds(250, 145, 105, WVM11.getPreferredSize().height);

          //---- WVA11 ----
          WVA11.setToolTipText("@LVA11@");
          WVA11.setName("WVA11");
          panel1.add(WVA11);
          WVA11.setBounds(360, 145, 60, WVA11.getPreferredSize().height);

          //---- label19 ----
          label19.setText("@WLER11@");
          label19.setName("label19");
          panel1.add(label19);
          label19.setBounds(425, 145, 25, 28);

          //---- WVM12 ----
          WVM12.setName("WVM12");
          panel1.add(WVM12);
          WVM12.setBounds(480, 145, 105, WVM12.getPreferredSize().height);

          //---- WVA12 ----
          WVA12.setToolTipText("@LVA12@");
          WVA12.setName("WVA12");
          panel1.add(WVA12);
          WVA12.setBounds(590, 145, 60, WVA12.getPreferredSize().height);

          //---- label20 ----
          label20.setText("@WLER12@");
          label20.setName("label20");
          panel1.add(label20);
          label20.setBounds(655, 145, 25, 28);

          //---- WVM13 ----
          WVM13.setName("WVM13");
          panel1.add(WVM13);
          WVM13.setBounds(20, 175, 105, WVM13.getPreferredSize().height);

          //---- WVA13 ----
          WVA13.setToolTipText("@LVA13@");
          WVA13.setName("WVA13");
          panel1.add(WVA13);
          WVA13.setBounds(130, 175, 60, WVA13.getPreferredSize().height);

          //---- label21 ----
          label21.setText("@WLER13@");
          label21.setName("label21");
          panel1.add(label21);
          label21.setBounds(195, 175, 25, 28);

          //---- WVM14 ----
          WVM14.setName("WVM14");
          panel1.add(WVM14);
          WVM14.setBounds(250, 175, 105, WVM14.getPreferredSize().height);

          //---- WVA14 ----
          WVA14.setToolTipText("@LVA14@");
          WVA14.setName("WVA14");
          panel1.add(WVA14);
          WVA14.setBounds(360, 175, 60, WVA14.getPreferredSize().height);

          //---- label22 ----
          label22.setText("@WLER14@");
          label22.setName("label22");
          panel1.add(label22);
          label22.setBounds(425, 175, 25, 28);

          //---- WVM15 ----
          WVM15.setName("WVM15");
          panel1.add(WVM15);
          WVM15.setBounds(480, 175, 105, WVM15.getPreferredSize().height);

          //---- WVA15 ----
          WVA15.setToolTipText("@LVA15@");
          WVA15.setName("WVA15");
          panel1.add(WVA15);
          WVA15.setBounds(590, 175, 60, WVA15.getPreferredSize().height);

          //---- label23 ----
          label23.setText("@WLER15@");
          label23.setName("label23");
          panel1.add(label23);
          label23.setBounds(655, 175, 25, 28);

          //---- WVM16 ----
          WVM16.setName("WVM16");
          panel1.add(WVM16);
          WVM16.setBounds(20, 205, 105, WVM16.getPreferredSize().height);

          //---- WVA16 ----
          WVA16.setToolTipText("@LVA16@");
          WVA16.setName("WVA16");
          panel1.add(WVA16);
          WVA16.setBounds(130, 205, 60, WVA16.getPreferredSize().height);

          //---- label24 ----
          label24.setText("@WLER16@");
          label24.setName("label24");
          panel1.add(label24);
          label24.setBounds(195, 205, 25, 28);

          //---- WVM17 ----
          WVM17.setName("WVM17");
          panel1.add(WVM17);
          WVM17.setBounds(250, 205, 105, WVM17.getPreferredSize().height);

          //---- WVA17 ----
          WVA17.setToolTipText("@LVA17@");
          WVA17.setName("WVA17");
          panel1.add(WVA17);
          WVA17.setBounds(360, 205, 60, WVA17.getPreferredSize().height);

          //---- label25 ----
          label25.setText("@WLER17@");
          label25.setName("label25");
          panel1.add(label25);
          label25.setBounds(425, 205, 25, 28);

          //---- WVM18 ----
          WVM18.setName("WVM18");
          panel1.add(WVM18);
          WVM18.setBounds(480, 205, 105, WVM18.getPreferredSize().height);

          //---- WVA18 ----
          WVA18.setToolTipText("@LVA18@");
          WVA18.setName("WVA18");
          panel1.add(WVA18);
          WVA18.setBounds(590, 205, 60, WVA18.getPreferredSize().height);

          //---- label26 ----
          label26.setText("@WLER18@");
          label26.setName("label26");
          panel1.add(label26);
          label26.setBounds(655, 205, 25, 28);

          //---- WVM19 ----
          WVM19.setName("WVM19");
          panel1.add(WVM19);
          WVM19.setBounds(20, 235, 105, WVM19.getPreferredSize().height);

          //---- WVA19 ----
          WVA19.setToolTipText("@LVA19@");
          WVA19.setName("WVA19");
          panel1.add(WVA19);
          WVA19.setBounds(130, 235, 60, WVA19.getPreferredSize().height);

          //---- label27 ----
          label27.setText("@WLER19@");
          label27.setName("label27");
          panel1.add(label27);
          label27.setBounds(195, 235, 25, 28);

          //---- WVM20 ----
          WVM20.setName("WVM20");
          panel1.add(WVM20);
          WVM20.setBounds(250, 235, 105, WVM20.getPreferredSize().height);

          //---- WVA20 ----
          WVA20.setToolTipText("@LVA20@");
          WVA20.setName("WVA20");
          panel1.add(WVA20);
          WVA20.setBounds(360, 235, 60, WVA20.getPreferredSize().height);

          //---- label28 ----
          label28.setText("@WLER20@");
          label28.setName("label28");
          panel1.add(label28);
          label28.setBounds(425, 235, 25, 28);

          //---- WVM21 ----
          WVM21.setName("WVM21");
          panel1.add(WVM21);
          WVM21.setBounds(480, 235, 105, WVM21.getPreferredSize().height);

          //---- WVA21 ----
          WVA21.setToolTipText("@LVA21@");
          WVA21.setName("WVA21");
          panel1.add(WVA21);
          WVA21.setBounds(590, 235, 60, WVA21.getPreferredSize().height);

          //---- label29 ----
          label29.setText("@WLER21@");
          label29.setName("label29");
          panel1.add(label29);
          label29.setBounds(655, 235, 25, 28);

          //---- WVM22 ----
          WVM22.setName("WVM22");
          panel1.add(WVM22);
          WVM22.setBounds(20, 265, 105, WVM22.getPreferredSize().height);

          //---- WVA22 ----
          WVA22.setToolTipText("@LVA22@");
          WVA22.setName("WVA22");
          panel1.add(WVA22);
          WVA22.setBounds(130, 265, 60, WVA22.getPreferredSize().height);

          //---- label30 ----
          label30.setText("@WLER22@");
          label30.setName("label30");
          panel1.add(label30);
          label30.setBounds(195, 265, 25, 28);

          //---- WVM23 ----
          WVM23.setName("WVM23");
          panel1.add(WVM23);
          WVM23.setBounds(250, 265, 105, WVM23.getPreferredSize().height);

          //---- WVA23 ----
          WVA23.setToolTipText("@LVA23@");
          WVA23.setName("WVA23");
          panel1.add(WVA23);
          WVA23.setBounds(360, 265, 60, WVA23.getPreferredSize().height);

          //---- label31 ----
          label31.setText("@WLER23@");
          label31.setName("label31");
          panel1.add(label31);
          label31.setBounds(425, 265, 25, 28);

          //---- WVM24 ----
          WVM24.setName("WVM24");
          panel1.add(WVM24);
          WVM24.setBounds(480, 265, 105, WVM24.getPreferredSize().height);

          //---- WVA24 ----
          WVA24.setToolTipText("@LVA24@");
          WVA24.setName("WVA24");
          panel1.add(WVA24);
          WVA24.setBounds(590, 265, 60, WVA24.getPreferredSize().height);

          //---- label32 ----
          label32.setText("@WLER24@");
          label32.setName("label32");
          panel1.add(label32);
          label32.setBounds(655, 265, 25, 28);

          //---- WVM25 ----
          WVM25.setName("WVM25");
          panel1.add(WVM25);
          WVM25.setBounds(20, 295, 105, WVM25.getPreferredSize().height);

          //---- WVA25 ----
          WVA25.setToolTipText("@LVA25@");
          WVA25.setName("WVA25");
          panel1.add(WVA25);
          WVA25.setBounds(130, 295, 60, WVA25.getPreferredSize().height);

          //---- label33 ----
          label33.setText("@WLER25@");
          label33.setName("label33");
          panel1.add(label33);
          label33.setBounds(195, 295, 25, 28);

          //---- WVM26 ----
          WVM26.setName("WVM26");
          panel1.add(WVM26);
          WVM26.setBounds(250, 295, 105, WVM26.getPreferredSize().height);

          //---- WVA26 ----
          WVA26.setToolTipText("@LVA26@");
          WVA26.setName("WVA26");
          panel1.add(WVA26);
          WVA26.setBounds(360, 295, 60, WVA26.getPreferredSize().height);

          //---- label34 ----
          label34.setText("@WLER26@");
          label34.setName("label34");
          panel1.add(label34);
          label34.setBounds(425, 295, 25, 28);

          //---- WVM27 ----
          WVM27.setName("WVM27");
          panel1.add(WVM27);
          WVM27.setBounds(480, 295, 105, WVM27.getPreferredSize().height);

          //---- WVA27 ----
          WVA27.setToolTipText("@LVA27@");
          WVA27.setName("WVA27");
          panel1.add(WVA27);
          WVA27.setBounds(590, 295, 60, WVA27.getPreferredSize().height);

          //---- label35 ----
          label35.setText("@WLER27@");
          label35.setName("label35");
          panel1.add(label35);
          label35.setBounds(655, 295, 25, 28);

          //---- WVM28 ----
          WVM28.setName("WVM28");
          panel1.add(WVM28);
          WVM28.setBounds(20, 325, 105, WVM28.getPreferredSize().height);

          //---- WVA28 ----
          WVA28.setToolTipText("@LVA28@");
          WVA28.setName("WVA28");
          panel1.add(WVA28);
          WVA28.setBounds(130, 325, 60, WVA28.getPreferredSize().height);

          //---- label36 ----
          label36.setText("@WLER28@");
          label36.setName("label36");
          panel1.add(label36);
          label36.setBounds(195, 325, 25, 28);

          //---- WVM29 ----
          WVM29.setName("WVM29");
          panel1.add(WVM29);
          WVM29.setBounds(250, 325, 105, WVM29.getPreferredSize().height);

          //---- WVA29 ----
          WVA29.setToolTipText("@LVA29@");
          WVA29.setName("WVA29");
          panel1.add(WVA29);
          WVA29.setBounds(360, 325, 60, WVA29.getPreferredSize().height);

          //---- label37 ----
          label37.setText("@WLER29@");
          label37.setName("label37");
          panel1.add(label37);
          label37.setBounds(425, 325, 25, 28);

          //---- WVM30 ----
          WVM30.setName("WVM30");
          panel1.add(WVM30);
          WVM30.setBounds(480, 325, 105, WVM30.getPreferredSize().height);

          //---- WVA30 ----
          WVA30.setToolTipText("@LVA30@");
          WVA30.setName("WVA30");
          panel1.add(WVA30);
          WVA30.setBounds(590, 325, 60, WVA30.getPreferredSize().height);

          //---- label38 ----
          label38.setText("@WLER30@");
          label38.setName("label38");
          panel1.add(label38);
          label38.setBounds(655, 325, 25, 28);

          //---- WVM31 ----
          WVM31.setName("WVM31");
          panel1.add(WVM31);
          WVM31.setBounds(20, 355, 105, WVM31.getPreferredSize().height);

          //---- WVA31 ----
          WVA31.setToolTipText("@LVA31@");
          WVA31.setName("WVA31");
          panel1.add(WVA31);
          WVA31.setBounds(130, 355, 60, WVA31.getPreferredSize().height);

          //---- label39 ----
          label39.setText("@WLER31@");
          label39.setName("label39");
          panel1.add(label39);
          label39.setBounds(195, 355, 25, 28);

          //---- WVM32 ----
          WVM32.setName("WVM32");
          panel1.add(WVM32);
          WVM32.setBounds(250, 355, 105, WVM32.getPreferredSize().height);

          //---- WVA32 ----
          WVA32.setToolTipText("@LVA32@");
          WVA32.setName("WVA32");
          panel1.add(WVA32);
          WVA32.setBounds(360, 355, 60, WVA32.getPreferredSize().height);

          //---- label40 ----
          label40.setText("@WLER32@");
          label40.setName("label40");
          panel1.add(label40);
          label40.setBounds(425, 355, 25, 28);

          //---- WVM33 ----
          WVM33.setName("WVM33");
          panel1.add(WVM33);
          WVM33.setBounds(480, 355, 105, WVM33.getPreferredSize().height);

          //---- WVA33 ----
          WVA33.setToolTipText("@LVA33@");
          WVA33.setName("WVA33");
          panel1.add(WVA33);
          WVA33.setBounds(590, 355, 60, WVA33.getPreferredSize().height);

          //---- label41 ----
          label41.setText("@WLER33@");
          label41.setName("label41");
          panel1.add(label41);
          label41.setBounds(655, 355, 25, 28);

          //---- WVM34 ----
          WVM34.setName("WVM34");
          panel1.add(WVM34);
          WVM34.setBounds(20, 385, 105, WVM34.getPreferredSize().height);

          //---- WVA34 ----
          WVA34.setToolTipText("@LVA34@");
          WVA34.setName("WVA34");
          panel1.add(WVA34);
          WVA34.setBounds(130, 385, 60, WVA34.getPreferredSize().height);

          //---- label42 ----
          label42.setText("@WLER34@");
          label42.setName("label42");
          panel1.add(label42);
          label42.setBounds(195, 385, 25, 28);

          //---- WVM35 ----
          WVM35.setName("WVM35");
          panel1.add(WVM35);
          WVM35.setBounds(250, 385, 105, WVM35.getPreferredSize().height);

          //---- WVA35 ----
          WVA35.setToolTipText("@LVA35@");
          WVA35.setName("WVA35");
          panel1.add(WVA35);
          WVA35.setBounds(360, 385, 60, WVA35.getPreferredSize().height);

          //---- label43 ----
          label43.setText("@WLER35@");
          label43.setName("label43");
          panel1.add(label43);
          label43.setBounds(425, 385, 25, 28);

          //---- WVM36 ----
          WVM36.setName("WVM36");
          panel1.add(WVM36);
          WVM36.setBounds(480, 385, 105, WVM36.getPreferredSize().height);

          //---- WVA36 ----
          WVA36.setToolTipText("@LVA36@");
          WVA36.setName("WVA36");
          panel1.add(WVA36);
          WVA36.setBounds(590, 385, 60, WVA36.getPreferredSize().height);

          //---- label44 ----
          label44.setText("@WLER36@");
          label44.setName("label44");
          panel1.add(label44);
          label44.setBounds(655, 385, 25, 28);

          //---- WVM37 ----
          WVM37.setName("WVM37");
          panel1.add(WVM37);
          WVM37.setBounds(20, 415, 105, WVM37.getPreferredSize().height);

          //---- WVA37 ----
          WVA37.setToolTipText("@LVA37@");
          WVA37.setName("WVA37");
          panel1.add(WVA37);
          WVA37.setBounds(130, 415, 60, WVA37.getPreferredSize().height);

          //---- label45 ----
          label45.setText("@WLER37@");
          label45.setName("label45");
          panel1.add(label45);
          label45.setBounds(195, 415, 25, 28);

          //---- WVM38 ----
          WVM38.setName("WVM38");
          panel1.add(WVM38);
          WVM38.setBounds(250, 415, 105, WVM38.getPreferredSize().height);

          //---- WVA38 ----
          WVA38.setToolTipText("@LVA38@");
          WVA38.setName("WVA38");
          panel1.add(WVA38);
          WVA38.setBounds(360, 415, 60, WVA38.getPreferredSize().height);

          //---- label46 ----
          label46.setText("@WLER38@");
          label46.setName("label46");
          panel1.add(label46);
          label46.setBounds(425, 415, 25, 28);

          //---- WVM39 ----
          WVM39.setName("WVM39");
          panel1.add(WVM39);
          WVM39.setBounds(480, 415, 105, WVM39.getPreferredSize().height);

          //---- WVA39 ----
          WVA39.setToolTipText("@LVA39@");
          WVA39.setName("WVA39");
          panel1.add(WVA39);
          WVA39.setBounds(590, 415, 60, WVA39.getPreferredSize().height);

          //---- label47 ----
          label47.setText("@WLER39@");
          label47.setName("label47");
          panel1.add(label47);
          label47.setBounds(655, 415, 25, 28);

          //---- WVM40 ----
          WVM40.setName("WVM40");
          panel1.add(WVM40);
          WVM40.setBounds(20, 445, 105, WVM40.getPreferredSize().height);

          //---- WVA40 ----
          WVA40.setToolTipText("@LVA40@");
          WVA40.setName("WVA40");
          panel1.add(WVA40);
          WVA40.setBounds(130, 445, 60, WVA40.getPreferredSize().height);

          //---- label48 ----
          label48.setText("@WLER40@");
          label48.setName("label48");
          panel1.add(label48);
          label48.setBounds(195, 445, 25, 28);

          //---- WVM41 ----
          WVM41.setName("WVM41");
          panel1.add(WVM41);
          WVM41.setBounds(250, 445, 105, WVM41.getPreferredSize().height);

          //---- WVA41 ----
          WVA41.setToolTipText("@LVA41@");
          WVA41.setName("WVA41");
          panel1.add(WVA41);
          WVA41.setBounds(360, 445, 60, WVA41.getPreferredSize().height);

          //---- label49 ----
          label49.setText("@WLER41@");
          label49.setName("label49");
          panel1.add(label49);
          label49.setBounds(425, 445, 25, 28);

          //---- WVM42 ----
          WVM42.setName("WVM42");
          panel1.add(WVM42);
          WVM42.setBounds(480, 445, 105, WVM42.getPreferredSize().height);

          //---- WVA42 ----
          WVA42.setToolTipText("@LVA42@");
          WVA42.setName("WVA42");
          panel1.add(WVA42);
          WVA42.setBounds(590, 445, 60, WVA42.getPreferredSize().height);

          //---- label50 ----
          label50.setText("@WLER42@");
          label50.setName("label50");
          panel1.add(label50);
          label50.setBounds(655, 445, 25, 28);

          //---- OBJ_37_OBJ_38 ----
          OBJ_37_OBJ_38.setText("Montant");
          OBJ_37_OBJ_38.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_37_OBJ_38.setFont(OBJ_37_OBJ_38.getFont().deriveFont(OBJ_37_OBJ_38.getFont().getStyle() | Font.BOLD));
          OBJ_37_OBJ_38.setName("OBJ_37_OBJ_38");
          panel1.add(OBJ_37_OBJ_38);
          OBJ_37_OBJ_38.setBounds(20, 30, 105, 28);

          //---- OBJ_37_OBJ_39 ----
          OBJ_37_OBJ_39.setText("Montant");
          OBJ_37_OBJ_39.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_37_OBJ_39.setFont(OBJ_37_OBJ_39.getFont().deriveFont(OBJ_37_OBJ_39.getFont().getStyle() | Font.BOLD));
          OBJ_37_OBJ_39.setName("OBJ_37_OBJ_39");
          panel1.add(OBJ_37_OBJ_39);
          OBJ_37_OBJ_39.setBounds(250, 30, 105, 28);

          //---- OBJ_37_OBJ_40 ----
          OBJ_37_OBJ_40.setText("Montant");
          OBJ_37_OBJ_40.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_37_OBJ_40.setFont(OBJ_37_OBJ_40.getFont().deriveFont(OBJ_37_OBJ_40.getFont().getStyle() | Font.BOLD));
          OBJ_37_OBJ_40.setName("OBJ_37_OBJ_40");
          panel1.add(OBJ_37_OBJ_40);
          OBJ_37_OBJ_40.setBounds(480, 30, 105, 28);

          //---- OBJ_37_OBJ_41 ----
          OBJ_37_OBJ_41.setText("Axe");
          OBJ_37_OBJ_41.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_37_OBJ_41.setFont(OBJ_37_OBJ_41.getFont().deriveFont(OBJ_37_OBJ_41.getFont().getStyle() | Font.BOLD));
          OBJ_37_OBJ_41.setName("OBJ_37_OBJ_41");
          panel1.add(OBJ_37_OBJ_41);
          OBJ_37_OBJ_41.setBounds(130, 30, 60, 28);

          //---- OBJ_37_OBJ_42 ----
          OBJ_37_OBJ_42.setText("Axe");
          OBJ_37_OBJ_42.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_37_OBJ_42.setFont(OBJ_37_OBJ_42.getFont().deriveFont(OBJ_37_OBJ_42.getFont().getStyle() | Font.BOLD));
          OBJ_37_OBJ_42.setName("OBJ_37_OBJ_42");
          panel1.add(OBJ_37_OBJ_42);
          OBJ_37_OBJ_42.setBounds(360, 30, 60, 28);

          //---- OBJ_37_OBJ_43 ----
          OBJ_37_OBJ_43.setText("Axe");
          OBJ_37_OBJ_43.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_37_OBJ_43.setFont(OBJ_37_OBJ_43.getFont().deriveFont(OBJ_37_OBJ_43.getFont().getStyle() | Font.BOLD));
          OBJ_37_OBJ_43.setName("OBJ_37_OBJ_43");
          panel1.add(OBJ_37_OBJ_43);
          OBJ_37_OBJ_43.setBounds(590, 30, 60, 28);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 15, 695, 565);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_33_OBJ_33;
  private JLabel OBJ_37_OBJ_37;
  private XRiTextField WMTVTL;
  private XRiTextField WECVTL;
  private JLabel OBJ_29_OBJ_29;
  private XRiTextField WVM01;
  private XRiTextField WVA01;
  private JLabel label1;
  private XRiTextField WVM02;
  private XRiTextField WVA02;
  private JLabel label2;
  private XRiTextField WVM03;
  private XRiTextField WVA03;
  private JLabel label3;
  private XRiTextField WVM04;
  private XRiTextField WVA04;
  private JLabel label4;
  private XRiTextField WVM05;
  private XRiTextField WVA05;
  private JLabel label5;
  private XRiTextField WVM06;
  private XRiTextField WVA06;
  private JLabel label6;
  private XRiTextField WVM07;
  private XRiTextField WVA07;
  private JLabel label7;
  private XRiTextField WVM08;
  private XRiTextField WVA08;
  private JLabel label8;
  private XRiTextField WVM09;
  private XRiTextField WVA09;
  private JLabel label9;
  private XRiTextField WVM10;
  private XRiTextField WVA10;
  private JLabel label18;
  private XRiTextField WVM11;
  private XRiTextField WVA11;
  private JLabel label19;
  private XRiTextField WVM12;
  private XRiTextField WVA12;
  private JLabel label20;
  private XRiTextField WVM13;
  private XRiTextField WVA13;
  private JLabel label21;
  private XRiTextField WVM14;
  private XRiTextField WVA14;
  private JLabel label22;
  private XRiTextField WVM15;
  private XRiTextField WVA15;
  private JLabel label23;
  private XRiTextField WVM16;
  private XRiTextField WVA16;
  private JLabel label24;
  private XRiTextField WVM17;
  private XRiTextField WVA17;
  private JLabel label25;
  private XRiTextField WVM18;
  private XRiTextField WVA18;
  private JLabel label26;
  private XRiTextField WVM19;
  private XRiTextField WVA19;
  private JLabel label27;
  private XRiTextField WVM20;
  private XRiTextField WVA20;
  private JLabel label28;
  private XRiTextField WVM21;
  private XRiTextField WVA21;
  private JLabel label29;
  private XRiTextField WVM22;
  private XRiTextField WVA22;
  private JLabel label30;
  private XRiTextField WVM23;
  private XRiTextField WVA23;
  private JLabel label31;
  private XRiTextField WVM24;
  private XRiTextField WVA24;
  private JLabel label32;
  private XRiTextField WVM25;
  private XRiTextField WVA25;
  private JLabel label33;
  private XRiTextField WVM26;
  private XRiTextField WVA26;
  private JLabel label34;
  private XRiTextField WVM27;
  private XRiTextField WVA27;
  private JLabel label35;
  private XRiTextField WVM28;
  private XRiTextField WVA28;
  private JLabel label36;
  private XRiTextField WVM29;
  private XRiTextField WVA29;
  private JLabel label37;
  private XRiTextField WVM30;
  private XRiTextField WVA30;
  private JLabel label38;
  private XRiTextField WVM31;
  private XRiTextField WVA31;
  private JLabel label39;
  private XRiTextField WVM32;
  private XRiTextField WVA32;
  private JLabel label40;
  private XRiTextField WVM33;
  private XRiTextField WVA33;
  private JLabel label41;
  private XRiTextField WVM34;
  private XRiTextField WVA34;
  private JLabel label42;
  private XRiTextField WVM35;
  private XRiTextField WVA35;
  private JLabel label43;
  private XRiTextField WVM36;
  private XRiTextField WVA36;
  private JLabel label44;
  private XRiTextField WVM37;
  private XRiTextField WVA37;
  private JLabel label45;
  private XRiTextField WVM38;
  private XRiTextField WVA38;
  private JLabel label46;
  private XRiTextField WVM39;
  private XRiTextField WVA39;
  private JLabel label47;
  private XRiTextField WVM40;
  private XRiTextField WVA40;
  private JLabel label48;
  private XRiTextField WVM41;
  private XRiTextField WVA41;
  private JLabel label49;
  private XRiTextField WVM42;
  private XRiTextField WVA42;
  private JLabel label50;
  private JLabel OBJ_37_OBJ_38;
  private JLabel OBJ_37_OBJ_39;
  private JLabel OBJ_37_OBJ_40;
  private JLabel OBJ_37_OBJ_41;
  private JLabel OBJ_37_OBJ_42;
  private JLabel OBJ_37_OBJ_43;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
