
package ri.serien.libecranrpg.vcgm.VCGMSEFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGMSEFM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] A_Value = { "", "0", "1", "2", "3" };
  private String[] A_Title = { "", "tous les droits", "interro et modification", "interro seulement", "aucun droit" };
  private String[] B_Value = { "", "0", "1", "2", "3", "9" };
  private String[] B_Title = { "", "tous les droits", "interro et modification", "interro seulement", "aucun droit", "superviseur" };
  private String[] Value_20 = { "", "0", "1", "9" };
  private String[] Title_20 = { "", "Non autorisé", "Autorisé", "Tous droits + sortir Folio" };
  private String[] Value_14 = { "", "0", "1", "9" };
  private String[] Title_14 = { "", "Non autorisé", "Autorisé", "superviseur" };
  
  public VCGMSEFM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    SET014.setValeurs(Value_14, Title_14);
    SET020.setValeurs(Value_20, Title_20);
    SET012.setValeurs(A_Value, A_Title);
    SET007.setValeurs(A_Value, A_Title);
    SET006.setValeurs(A_Value, A_Title);
    SET005.setValeurs(B_Value, B_Title);
    SET004.setValeursSelection("0", "1");
    SET003.setValeurs(A_Value, A_Title);
    SET002.setValeurs(A_Value, A_Title);
    SET001.setValeurs(A_Value, A_Title);
    SET024.setValeursSelection("1", " ");
    SET023.setValeursSelection("1", " ");
    SET022.setValeursSelection("1", " ");
    SET021.setValeursSelection("1", " ");
    SET019.setValeursSelection("1", " ");
    SET018.setValeurs(Value_14, Title_14);
    SET017.setValeursSelection("1", " ");
    SET016.setValeursSelection("1", " ");
    SET015.setValeursSelection("1", " ");
    SET013.setValeursSelection("1", " ");
    SET011.setValeursSelection("1", " ");
    SET010.setValeursSelection("1", " ");
    SET009.setValeursSelection("1", " ");
    SET008.setValeursSelection("1", " ");
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDUSR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDUSR@")).trim());
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    OBJ_158.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPAGE@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText().trim());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_68ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlNord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDUSR = new RiZoneSortie();
    OBJ_64 = new JLabel();
    INDETB = new RiZoneSortie();
    p_tete_droite = new JPanel();
    OBJ_158 = new JLabel();
    OBJ_157 = new JLabel();
    pnlSud = new SNPanelFond();
    pnlMenus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    pnlContenu = new SNPanelContenu();
    xTitledPanel1 = new JXTitledPanel();
    panel1 = new JPanel();
    OBJ_76 = new JLabel();
    OBJ_146 = new JLabel();
    OBJ_90 = new JLabel();
    OBJ_82 = new JLabel();
    OBJ_102 = new JLabel();
    OBJ_108 = new JLabel();
    OBJ_114 = new JLabel();
    SET008 = new XRiCheckBox();
    SET009 = new XRiCheckBox();
    SET010 = new XRiCheckBox();
    SET011 = new XRiCheckBox();
    OBJ_75 = new JLabel();
    OBJ_81 = new JLabel();
    OBJ_89 = new JLabel();
    OBJ_95 = new JLabel();
    OBJ_101 = new JLabel();
    OBJ_107 = new JLabel();
    OBJ_113 = new JLabel();
    OBJ_119 = new JLabel();
    OBJ_125 = new JLabel();
    OBJ_132 = new JLabel();
    OBJ_138 = new JLabel();
    OBJ_145 = new JLabel();
    SET001 = new XRiComboBox();
    SET002 = new XRiComboBox();
    SET003 = new XRiComboBox();
    SET005 = new XRiComboBox();
    SET006 = new XRiComboBox();
    SET007 = new XRiComboBox();
    SET012 = new XRiComboBox();
    SET004 = new XRiCheckBox();
    panel2 = new JPanel();
    OBJ_85 = new JLabel();
    SET013 = new XRiCheckBox();
    SET015 = new XRiCheckBox();
    SET016 = new XRiCheckBox();
    SET017 = new XRiCheckBox();
    SET019 = new XRiCheckBox();
    SET021 = new XRiCheckBox();
    SET022 = new XRiCheckBox();
    SET023 = new XRiCheckBox();
    SET024 = new XRiCheckBox();
    OBJ_78 = new JLabel();
    OBJ_84 = new JLabel();
    OBJ_92 = new JLabel();
    OBJ_98 = new JLabel();
    OBJ_104 = new JLabel();
    OBJ_110 = new JLabel();
    OBJ_116 = new JLabel();
    OBJ_122 = new JLabel();
    OBJ_128 = new JLabel();
    OBJ_135 = new JLabel();
    OBJ_141 = new JLabel();
    OBJ_148 = new JLabel();
    SET020 = new XRiComboBox();
    label1 = new JLabel();
    SET014 = new XRiComboBox();
    SET018 = new XRiComboBox();
    OBJ_86 = new JLabel();
    pnlBas = new SNPanel();
    OBJ_68 = new SNBoutonLeger();
    V06F = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlNord ========
    {
      pnlNord.setName("pnlNord");
      pnlNord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("S\u00e9curit\u00e9 de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      pnlNord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- OBJ_42 ----
          OBJ_42.setText("Utilisateur");
          OBJ_42.setName("OBJ_42");
          
          // ---- INDUSR ----
          INDUSR.setComponentPopupMenu(BTD);
          INDUSR.setOpaque(false);
          INDUSR.setText("@INDUSR@");
          INDUSR.setName("INDUSR");
          
          // ---- OBJ_64 ----
          OBJ_64.setText("Etablissement");
          OBJ_64.setName("OBJ_64");
          
          // ---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setText("@INDETB@353");
          INDETB.setOpaque(false);
          INDETB.setName("INDETB");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5)
                  .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE).addGap(2, 2, 2)
                  .addComponent(INDUSR, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE).addGap(60, 60, 60)
                  .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE).addGap(1, 1, 1)
                  .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDUSR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2)
                  .addGroup(p_tete_gaucheLayout.createParallelGroup()
                      .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
          
          // ---- OBJ_158 ----
          OBJ_158.setText("@WPAGE@");
          OBJ_158.setName("OBJ_158");
          p_tete_droite.add(OBJ_158);
          
          // ---- OBJ_157 ----
          OBJ_157.setText("Page    ");
          OBJ_157.setName("OBJ_157");
          p_tete_droite.add(OBJ_157);
        }
        barre_tete.add(p_tete_droite);
      }
      pnlNord.add(barre_tete);
    }
    add(pnlNord, BorderLayout.NORTH);
    
    // ======== pnlSud ========
    {
      pnlSud.setName("pnlSud");
      pnlSud.setLayout(new BorderLayout());
      
      // ======== pnlMenus ========
      {
        pnlMenus.setPreferredSize(new Dimension(170, 0));
        pnlMenus.setMinimumSize(new Dimension(170, 0));
        pnlMenus.setBackground(new Color(238, 239, 241));
        pnlMenus.setBorder(LineBorder.createGrayLineBorder());
        pnlMenus.setName("pnlMenus");
        pnlMenus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        pnlMenus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");
              
              // ---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);
            
            // ======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");
              
              // ---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);
            
            // ======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");
              
              // ---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        pnlMenus.add(scroll_droite, BorderLayout.NORTH);
      }
      pnlSud.add(pnlMenus, BorderLayout.LINE_END);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setPreferredSize(new Dimension(950, 500));
        pnlContenu.setBackground(new Color(239, 239, 222));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(null);
        
        // ======== xTitledPanel1 ========
        {
          xTitledPanel1.setBorder(new DropShadowBorder());
          xTitledPanel1.setTitle("Objet de la s\u00e9curit\u00e9 (C.G.M)");
          xTitledPanel1.setName("xTitledPanel1");
          Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
          xTitledPanel1ContentContainer.setLayout(null);
          
          // ======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);
            
            // ---- OBJ_76 ----
            OBJ_76.setText("Personnalisation du syst\u00e8me");
            OBJ_76.setName("OBJ_76");
            panel1.add(OBJ_76);
            OBJ_76.setBounds(198, 23, 245, 17);
            
            // ---- OBJ_146 ----
            OBJ_146.setText("Visualisation et pointage des comptes");
            OBJ_146.setName("OBJ_146");
            panel1.add(OBJ_146);
            OBJ_146.setBounds(198, 364, 245, 17);
            
            // ---- OBJ_90 ----
            OBJ_90.setText("Plan comptable auxiliaire");
            OBJ_90.setName("OBJ_90");
            panel1.add(OBJ_90);
            OBJ_90.setBounds(198, 85, 245, 17);
            
            // ---- OBJ_82 ----
            OBJ_82.setText("Plan comptable g\u00e9n\u00e9ral");
            OBJ_82.setName("OBJ_82");
            panel1.add(OBJ_82);
            OBJ_82.setBounds(198, 54, 245, 17);
            
            // ---- OBJ_102 ----
            OBJ_102.setText("Gestion des budgets");
            OBJ_102.setName("OBJ_102");
            panel1.add(OBJ_102);
            OBJ_102.setBounds(198, 147, 245, 17);
            
            // ---- OBJ_108 ----
            OBJ_108.setText("Analyse budg\u00e9taire");
            OBJ_108.setName("OBJ_108");
            panel1.add(OBJ_108);
            OBJ_108.setBounds(198, 178, 245, 17);
            
            // ---- OBJ_114 ----
            OBJ_114.setText("Ecritures types");
            OBJ_114.setName("OBJ_114");
            panel1.add(OBJ_114);
            OBJ_114.setBounds(198, 209, 245, 17);
            
            // ---- SET008 ----
            SET008.setComponentPopupMenu(BTD);
            SET008.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET008.setText("Saisie des \u00e9critures comptable");
            SET008.setName("SET008");
            panel1.add(SET008);
            SET008.setBounds(48, 239, 395, 17);
            
            // ---- SET009 ----
            SET009.setComponentPopupMenu(BTD);
            SET009.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET009.setText("Recherche d'\u00e9critures");
            SET009.setName("SET009");
            panel1.add(SET009);
            SET009.setBounds(48, 270, 395, 17);
            
            // ---- SET010 ----
            SET010.setComponentPopupMenu(BTD);
            SET010.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET010.setText("Saisie tr\u00e9sorerie \u00e0 payer");
            SET010.setName("SET010");
            panel1.add(SET010);
            SET010.setBounds(48, 301, 395, 17);
            
            // ---- SET011 ----
            SET011.setComponentPopupMenu(BTD);
            SET011.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET011.setText("Saisie tr\u00e9sorerie \u00e0 re\u00e7evoir");
            SET011.setName("SET011");
            panel1.add(SET011);
            SET011.setBounds(48, 332, 395, 17);
            
            // ---- OBJ_75 ----
            OBJ_75.setText("01");
            OBJ_75.setFont(OBJ_75.getFont().deriveFont(OBJ_75.getFont().getStyle() | Font.BOLD));
            OBJ_75.setName("OBJ_75");
            panel1.add(OBJ_75);
            OBJ_75.setBounds(23, 23, 21, 17);
            
            // ---- OBJ_81 ----
            OBJ_81.setText("02");
            OBJ_81.setFont(OBJ_81.getFont().deriveFont(OBJ_81.getFont().getStyle() | Font.BOLD));
            OBJ_81.setName("OBJ_81");
            panel1.add(OBJ_81);
            OBJ_81.setBounds(23, 54, 21, 17);
            
            // ---- OBJ_89 ----
            OBJ_89.setText("03");
            OBJ_89.setFont(OBJ_89.getFont().deriveFont(OBJ_89.getFont().getStyle() | Font.BOLD));
            OBJ_89.setName("OBJ_89");
            panel1.add(OBJ_89);
            OBJ_89.setBounds(23, 85, 21, 17);
            
            // ---- OBJ_95 ----
            OBJ_95.setText("04");
            OBJ_95.setFont(OBJ_95.getFont().deriveFont(OBJ_95.getFont().getStyle() | Font.BOLD));
            OBJ_95.setName("OBJ_95");
            panel1.add(OBJ_95);
            OBJ_95.setBounds(23, 116, 21, 17);
            
            // ---- OBJ_101 ----
            OBJ_101.setText("05");
            OBJ_101.setFont(OBJ_101.getFont().deriveFont(OBJ_101.getFont().getStyle() | Font.BOLD));
            OBJ_101.setName("OBJ_101");
            panel1.add(OBJ_101);
            OBJ_101.setBounds(23, 147, 21, 17);
            
            // ---- OBJ_107 ----
            OBJ_107.setText("06");
            OBJ_107.setFont(OBJ_107.getFont().deriveFont(OBJ_107.getFont().getStyle() | Font.BOLD));
            OBJ_107.setName("OBJ_107");
            panel1.add(OBJ_107);
            OBJ_107.setBounds(23, 178, 21, 17);
            
            // ---- OBJ_113 ----
            OBJ_113.setText("07");
            OBJ_113.setFont(OBJ_113.getFont().deriveFont(OBJ_113.getFont().getStyle() | Font.BOLD));
            OBJ_113.setName("OBJ_113");
            panel1.add(OBJ_113);
            OBJ_113.setBounds(23, 209, 21, 17);
            
            // ---- OBJ_119 ----
            OBJ_119.setText("08");
            OBJ_119.setFont(OBJ_119.getFont().deriveFont(OBJ_119.getFont().getStyle() | Font.BOLD));
            OBJ_119.setName("OBJ_119");
            panel1.add(OBJ_119);
            OBJ_119.setBounds(23, 239, 21, 17);
            
            // ---- OBJ_125 ----
            OBJ_125.setText("09");
            OBJ_125.setFont(OBJ_125.getFont().deriveFont(OBJ_125.getFont().getStyle() | Font.BOLD));
            OBJ_125.setName("OBJ_125");
            panel1.add(OBJ_125);
            OBJ_125.setBounds(23, 270, 21, 17);
            
            // ---- OBJ_132 ----
            OBJ_132.setText("10");
            OBJ_132.setFont(OBJ_132.getFont().deriveFont(OBJ_132.getFont().getStyle() | Font.BOLD));
            OBJ_132.setName("OBJ_132");
            panel1.add(OBJ_132);
            OBJ_132.setBounds(23, 301, 21, 17);
            
            // ---- OBJ_138 ----
            OBJ_138.setText("11");
            OBJ_138.setFont(OBJ_138.getFont().deriveFont(OBJ_138.getFont().getStyle() | Font.BOLD));
            OBJ_138.setName("OBJ_138");
            panel1.add(OBJ_138);
            OBJ_138.setBounds(23, 332, 21, 17);
            
            // ---- OBJ_145 ----
            OBJ_145.setText("12");
            OBJ_145.setFont(OBJ_145.getFont().deriveFont(OBJ_145.getFont().getStyle() | Font.BOLD));
            OBJ_145.setName("OBJ_145");
            panel1.add(OBJ_145);
            OBJ_145.setBounds(23, 364, 21, 17);
            
            // ---- SET001 ----
            SET001.setComponentPopupMenu(BTD);
            SET001.setName("SET001");
            panel1.add(SET001);
            SET001.setBounds(48, 18, 140, SET001.getPreferredSize().height);
            
            // ---- SET002 ----
            SET002.setComponentPopupMenu(BTD);
            SET002.setName("SET002");
            panel1.add(SET002);
            SET002.setBounds(48, 49, 140, SET002.getPreferredSize().height);
            
            // ---- SET003 ----
            SET003.setComponentPopupMenu(BTD);
            SET003.setName("SET003");
            panel1.add(SET003);
            SET003.setBounds(48, 80, 140, SET003.getPreferredSize().height);
            
            // ---- SET005 ----
            SET005.setComponentPopupMenu(BTD);
            SET005.setName("SET005");
            panel1.add(SET005);
            SET005.setBounds(48, 142, 140, SET005.getPreferredSize().height);
            
            // ---- SET006 ----
            SET006.setComponentPopupMenu(BTD);
            SET006.setName("SET006");
            panel1.add(SET006);
            SET006.setBounds(48, 173, 140, SET006.getPreferredSize().height);
            
            // ---- SET007 ----
            SET007.setComponentPopupMenu(BTD);
            SET007.setName("SET007");
            panel1.add(SET007);
            SET007.setBounds(48, 204, 140, SET007.getPreferredSize().height);
            
            // ---- SET012 ----
            SET012.setComponentPopupMenu(BTD);
            SET012.setName("SET012");
            panel1.add(SET012);
            SET012.setBounds(48, 359, 140, SET012.getPreferredSize().height);
            
            // ---- SET004 ----
            SET004.setComponentPopupMenu(BTD);
            SET004.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET004.setText("Plan comptable reporting");
            SET004.setName("SET004");
            panel1.add(SET004);
            SET004.setBounds(48, 115, 372, SET004.getPreferredSize().height);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          xTitledPanel1ContentContainer.add(panel1);
          panel1.setBounds(10, 5, 445, 395);
          
          // ======== panel2 ========
          {
            panel2.setBorder(new TitledBorder(""));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);
            
            // ---- OBJ_85 ----
            OBJ_85.setText("Gestion des dettesc");
            OBJ_85.setName("OBJ_85");
            panel2.add(OBJ_85);
            OBJ_85.setBounds(240, 49, 185, 28);
            
            // ---- SET013 ----
            SET013.setComponentPopupMenu(BTD);
            SET013.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET013.setText("Saisie des \u00e9critures pr\u00e9visionnelles");
            SET013.setName("SET013");
            panel2.add(SET013);
            SET013.setBounds(48, 23, 372, 18);
            
            // ---- SET015 ----
            SET015.setComponentPopupMenu(BTD);
            SET015.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET015.setText("Relances clients");
            SET015.setName("SET015");
            panel2.add(SET015);
            SET015.setBounds(48, 83, 372, 18);
            
            // ---- SET016 ----
            SET016.setComponentPopupMenu(BTD);
            SET016.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET016.setText("Lettrages automatiques");
            SET016.setName("SET016");
            panel2.add(SET016);
            SET016.setBounds(48, 114, 372, 18);
            
            // ---- SET017 ----
            SET017.setComponentPopupMenu(BTD);
            SET017.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET017.setText("Injections FCO");
            SET017.setName("SET017");
            panel2.add(SET017);
            SET017.setBounds(48, 145, 372, 18);
            
            // ---- SET019 ----
            SET019.setComponentPopupMenu(BTD);
            SET019.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET019.setText("Extraction d'\u00e9critures");
            SET019.setName("SET019");
            panel2.add(SET019);
            SET019.setBounds(48, 207, 362, 18);
            
            // ---- SET021 ----
            SET021.setComponentPopupMenu(BTD);
            SET021.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET021.setText("TVA sur encaissements");
            SET021.setName("SET021");
            panel2.add(SET021);
            SET021.setBounds(48, 269, 362, 18);
            
            // ---- SET022 ----
            SET022.setComponentPopupMenu(BTD);
            SET022.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET022.setText("Purge bordereaux de rapprochement");
            SET022.setName("SET022");
            panel2.add(SET022);
            SET022.setBounds(48, 300, 362, 18);
            
            // ---- SET023 ----
            SET023.setComponentPopupMenu(BTD);
            SET023.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET023.setText("Comptabilisation des effets");
            SET023.setName("SET023");
            panel2.add(SET023);
            SET023.setBounds(48, 331, 362, 18);
            
            // ---- SET024 ----
            SET024.setComponentPopupMenu(BTD);
            SET024.setToolTipText("coch\u00e9 = autoris\u00e9 / d\u00e9coch\u00e9 = interdit");
            SET024.setText("Reg\u00e9n\u00e9ration des compteurs");
            SET024.setName("SET024");
            panel2.add(SET024);
            SET024.setBounds(48, 362, 362, 18);
            
            // ---- OBJ_78 ----
            OBJ_78.setText("13");
            OBJ_78.setFont(OBJ_78.getFont().deriveFont(OBJ_78.getFont().getStyle() | Font.BOLD));
            OBJ_78.setName("OBJ_78");
            panel2.add(OBJ_78);
            OBJ_78.setBounds(23, 23, 21, 18);
            
            // ---- OBJ_84 ----
            OBJ_84.setText("14");
            OBJ_84.setFont(OBJ_84.getFont().deriveFont(OBJ_84.getFont().getStyle() | Font.BOLD));
            OBJ_84.setName("OBJ_84");
            panel2.add(OBJ_84);
            OBJ_84.setBounds(23, 54, 21, 18);
            
            // ---- OBJ_92 ----
            OBJ_92.setText("15");
            OBJ_92.setFont(OBJ_92.getFont().deriveFont(OBJ_92.getFont().getStyle() | Font.BOLD));
            OBJ_92.setName("OBJ_92");
            panel2.add(OBJ_92);
            OBJ_92.setBounds(23, 83, 21, 18);
            
            // ---- OBJ_98 ----
            OBJ_98.setText("16");
            OBJ_98.setFont(OBJ_98.getFont().deriveFont(OBJ_98.getFont().getStyle() | Font.BOLD));
            OBJ_98.setName("OBJ_98");
            panel2.add(OBJ_98);
            OBJ_98.setBounds(23, 114, 21, 18);
            
            // ---- OBJ_104 ----
            OBJ_104.setText("17");
            OBJ_104.setFont(OBJ_104.getFont().deriveFont(OBJ_104.getFont().getStyle() | Font.BOLD));
            OBJ_104.setName("OBJ_104");
            panel2.add(OBJ_104);
            OBJ_104.setBounds(23, 145, 21, 18);
            
            // ---- OBJ_110 ----
            OBJ_110.setText("18");
            OBJ_110.setFont(OBJ_110.getFont().deriveFont(OBJ_110.getFont().getStyle() | Font.BOLD));
            OBJ_110.setName("OBJ_110");
            panel2.add(OBJ_110);
            OBJ_110.setBounds(23, 176, 21, 18);
            
            // ---- OBJ_116 ----
            OBJ_116.setText("19");
            OBJ_116.setFont(OBJ_116.getFont().deriveFont(OBJ_116.getFont().getStyle() | Font.BOLD));
            OBJ_116.setName("OBJ_116");
            panel2.add(OBJ_116);
            OBJ_116.setBounds(23, 207, 21, 18);
            
            // ---- OBJ_122 ----
            OBJ_122.setText("20");
            OBJ_122.setFont(OBJ_122.getFont().deriveFont(OBJ_122.getFont().getStyle() | Font.BOLD));
            OBJ_122.setName("OBJ_122");
            panel2.add(OBJ_122);
            OBJ_122.setBounds(23, 238, 21, 18);
            
            // ---- OBJ_128 ----
            OBJ_128.setText("21");
            OBJ_128.setFont(OBJ_128.getFont().deriveFont(OBJ_128.getFont().getStyle() | Font.BOLD));
            OBJ_128.setName("OBJ_128");
            panel2.add(OBJ_128);
            OBJ_128.setBounds(23, 269, 21, 18);
            
            // ---- OBJ_135 ----
            OBJ_135.setText("22");
            OBJ_135.setFont(OBJ_135.getFont().deriveFont(OBJ_135.getFont().getStyle() | Font.BOLD));
            OBJ_135.setName("OBJ_135");
            panel2.add(OBJ_135);
            OBJ_135.setBounds(23, 300, 21, 18);
            
            // ---- OBJ_141 ----
            OBJ_141.setText("23");
            OBJ_141.setFont(OBJ_141.getFont().deriveFont(OBJ_141.getFont().getStyle() | Font.BOLD));
            OBJ_141.setName("OBJ_141");
            panel2.add(OBJ_141);
            OBJ_141.setBounds(23, 331, 21, 18);
            
            // ---- OBJ_148 ----
            OBJ_148.setText("24");
            OBJ_148.setFont(OBJ_148.getFont().deriveFont(OBJ_148.getFont().getStyle() | Font.BOLD));
            OBJ_148.setName("OBJ_148");
            panel2.add(OBJ_148);
            OBJ_148.setBounds(23, 362, 21, 18);
            
            // ---- SET020 ----
            SET020.setName("SET020");
            panel2.add(SET020);
            SET020.setBounds(50, 234, 180, SET020.getPreferredSize().height);
            
            // ---- label1 ----
            label1.setText("Entr\u00e9e de folio en comptabilit\u00e9");
            label1.setName("label1");
            panel2.add(label1);
            label1.setBounds(240, 235, 185, 25);
            
            // ---- SET014 ----
            SET014.setName("SET014");
            panel2.add(SET014);
            SET014.setBounds(48, 50, 180, SET014.getPreferredSize().height);
            
            // ---- SET018 ----
            SET018.setName("SET018");
            panel2.add(SET018);
            SET018.setBounds(48, 172, 180, SET018.getPreferredSize().height);
            
            // ---- OBJ_86 ----
            OBJ_86.setText("Arr\u00eat\u00e9, cl\u00f4ture et basculement");
            OBJ_86.setName("OBJ_86");
            panel2.add(OBJ_86);
            OBJ_86.setBounds(240, 171, 185, 28);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          xTitledPanel1ContentContainer.add(panel2);
          panel2.setBounds(465, 5, 445, 395);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel1ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
          }
        }
        pnlContenu.add(xTitledPanel1);
        xTitledPanel1.setBounds(15, 10, 924, 437);
        
        // ======== pnlBas ========
        {
          pnlBas.setOpaque(false);
          pnlBas.setName("pnlBas");
          pnlBas.setLayout(null);
          
          // ---- OBJ_68 ----
          OBJ_68.setText("Aller \u00e0 la page");
          OBJ_68.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_68.setName("OBJ_68");
          OBJ_68.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_68ActionPerformed(e);
            }
          });
          pnlBas.add(OBJ_68);
          OBJ_68.setBounds(new Rectangle(new Point(75, 0), OBJ_68.getPreferredSize()));
          
          // ---- V06F ----
          V06F.setComponentPopupMenu(BTD);
          V06F.setName("V06F");
          pnlBas.add(V06F);
          V06F.setBounds(220, 0, 25, V06F.getPreferredSize().height);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < pnlBas.getComponentCount(); i++) {
              Rectangle bounds = pnlBas.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = pnlBas.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            pnlBas.setMinimumSize(preferredSize);
            pnlBas.setPreferredSize(preferredSize);
          }
        }
        pnlContenu.add(pnlBas);
        pnlBas.setBounds(679, 458, 270, 34);
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < pnlContenu.getComponentCount(); i++) {
            Rectangle bounds = pnlContenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = pnlContenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          pnlContenu.setMinimumSize(preferredSize);
          pnlContenu.setPreferredSize(preferredSize);
        }
      }
      pnlSud.add(pnlContenu, BorderLayout.CENTER);
    }
    add(pnlSud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlNord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private RiZoneSortie INDUSR;
  private JLabel OBJ_64;
  private RiZoneSortie INDETB;
  private JPanel p_tete_droite;
  private JLabel OBJ_158;
  private JLabel OBJ_157;
  private SNPanelFond pnlSud;
  private JPanel pnlMenus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelContenu pnlContenu;
  private JXTitledPanel xTitledPanel1;
  private JPanel panel1;
  private JLabel OBJ_76;
  private JLabel OBJ_146;
  private JLabel OBJ_90;
  private JLabel OBJ_82;
  private JLabel OBJ_102;
  private JLabel OBJ_108;
  private JLabel OBJ_114;
  private XRiCheckBox SET008;
  private XRiCheckBox SET009;
  private XRiCheckBox SET010;
  private XRiCheckBox SET011;
  private JLabel OBJ_75;
  private JLabel OBJ_81;
  private JLabel OBJ_89;
  private JLabel OBJ_95;
  private JLabel OBJ_101;
  private JLabel OBJ_107;
  private JLabel OBJ_113;
  private JLabel OBJ_119;
  private JLabel OBJ_125;
  private JLabel OBJ_132;
  private JLabel OBJ_138;
  private JLabel OBJ_145;
  private XRiComboBox SET001;
  private XRiComboBox SET002;
  private XRiComboBox SET003;
  private XRiComboBox SET005;
  private XRiComboBox SET006;
  private XRiComboBox SET007;
  private XRiComboBox SET012;
  private XRiCheckBox SET004;
  private JPanel panel2;
  private JLabel OBJ_85;
  private XRiCheckBox SET013;
  private XRiCheckBox SET015;
  private XRiCheckBox SET016;
  private XRiCheckBox SET017;
  private XRiCheckBox SET019;
  private XRiCheckBox SET021;
  private XRiCheckBox SET022;
  private XRiCheckBox SET023;
  private XRiCheckBox SET024;
  private JLabel OBJ_78;
  private JLabel OBJ_84;
  private JLabel OBJ_92;
  private JLabel OBJ_98;
  private JLabel OBJ_104;
  private JLabel OBJ_110;
  private JLabel OBJ_116;
  private JLabel OBJ_122;
  private JLabel OBJ_128;
  private JLabel OBJ_135;
  private JLabel OBJ_141;
  private JLabel OBJ_148;
  private XRiComboBox SET020;
  private JLabel label1;
  private XRiComboBox SET014;
  private XRiComboBox SET018;
  private JLabel OBJ_86;
  private SNPanel pnlBas;
  private SNBoutonLeger OBJ_68;
  private XRiTextField V06F;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
