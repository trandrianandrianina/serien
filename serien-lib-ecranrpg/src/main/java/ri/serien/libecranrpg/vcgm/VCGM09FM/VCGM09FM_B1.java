
package ri.serien.libecranrpg.vcgm.VCGM09FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.ODialog;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.autonome.recherchejournaux.GfxRechercheJournaux;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiSpinner;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM09FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] valeurSpi = { " ", "D", "C" };
  public ODialog dialog_JO = null;
  
  public VCGM09FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    
    SNS1.setValeurs(valeurSpi);
    SNS2.setValeurs(valeurSpi);
    SNS3.setValeurs(valeurSpi);
    SNS4.setValeurs(valeurSpi);
    SNS5.setValeurs(valeurSpi);
    SNS6.setValeurs(valeurSpi);
    SNS7.setValeurs(valeurSpi);
    SNS8.setValeurs(valeurSpi);
    SNS9.setValeurs(valeurSpi);
    SNS10.setValeurs(valeurSpi);
    SNS11.setValeurs(valeurSpi);
    SNS12.setValeurs(valeurSpi);
    SNS13.setValeurs(valeurSpi);
    SNS14.setValeurs(valeurSpi);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    FL1.setVisible((lexique.isTrue("N31")));
    FL2.setVisible((lexique.isTrue("N32")));
    FL3.setVisible((lexique.isTrue("N33")));
    FL4.setVisible((lexique.isTrue("N34")));
    FL5.setVisible((lexique.isTrue("N35")));
    FL6.setVisible((lexique.isTrue("N36")));
    FL7.setVisible((lexique.isTrue("N37")));
    FL8.setVisible((lexique.isTrue("N38")));
    FL9.setVisible((lexique.isTrue("N39")));
    FL10.setVisible((lexique.isTrue("N40")));
    FL11.setVisible((lexique.isTrue("N41")));
    FL12.setVisible((lexique.isTrue("N42")));
    FL13.setVisible((lexique.isTrue("N43")));
    FL14.setVisible((lexique.isTrue("N44")));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "DUPLICATE");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void BTN_JOActionPerformed(ActionEvent e) {
    lexique.addVariableGlobale("CODE_SOC", INDETB.getText());
    lexique.addVariableGlobale("ZONE_JO", "ETJO");
    if (dialog_JO == null) {
      dialog_JO = new ODialog((Window) getTopLevelAncestor(), new GfxRechercheJournaux(this));
    }
    dialog_JO.affichePopupPerso();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    INDETB = new XRiTextField();
    OBJ_34 = new JLabel();
    OBJ_69 = new JLabel();
    INDCOD = new XRiTextField();
    ETLIB = new XRiTextField();
    ETJO = new XRiTextField();
    BTN_JO = new JButton();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel2 = new JXTitledPanel();
    ETO1 = new XRiTextField();
    ETO2 = new XRiTextField();
    ETO3 = new XRiTextField();
    ETO4 = new XRiTextField();
    ETO5 = new XRiTextField();
    ETO6 = new XRiTextField();
    ETO7 = new XRiTextField();
    ETO8 = new XRiTextField();
    ETO9 = new XRiTextField();
    ETO10 = new XRiTextField();
    ETO11 = new XRiTextField();
    ETO12 = new XRiTextField();
    ETO13 = new XRiTextField();
    ETO14 = new XRiTextField();
    ETO15 = new XRiTextField();
    ETO16 = new XRiTextField();
    ETO17 = new XRiTextField();
    ETO18 = new XRiTextField();
    OBJ_218 = new JLabel();
    OBJ_219 = new JLabel();
    OBJ_204 = new JLabel();
    OBJ_208 = new JLabel();
    OBJ_234 = new JLabel();
    OBJ_235 = new JLabel();
    OBJ_237 = new JLabel();
    OBJ_236 = new JLabel();
    OBJ_223 = new JLabel();
    OBJ_222 = new JLabel();
    OBJ_205 = new JLabel();
    OBJ_209 = new JLabel();
    OBJ_233 = new JLabel();
    OBJ_232 = new JLabel();
    OBJ_238 = new JLabel();
    OBJ_239 = new JLabel();
    OBJ_206 = new JLabel();
    OBJ_210 = new JLabel();
    OBJ_225 = new JLabel();
    OBJ_224 = new JLabel();
    OBJ_230 = new JLabel();
    OBJ_231 = new JLabel();
    OBJ_240 = new JLabel();
    OBJ_241 = new JLabel();
    OBJ_226 = new JLabel();
    OBJ_227 = new JLabel();
    OBJ_211 = new JLabel();
    OBJ_207 = new JLabel();
    OBJ_229 = new JLabel();
    OBJ_228 = new JLabel();
    OBJ_220 = new JLabel();
    OBJ_221 = new JLabel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_193 = new JLabel();
    OBJ_200 = new JLabel();
    NCG1X = new XRiTextField();
    TIE1 = new XRiTextField();
    SNS1 = new XRiSpinner();
    CLB1 = new XRiTextField();
    LIB1 = new XRiTextField();
    AFF01 = new XRiTextField();
    SAN1 = new XRiTextField();
    MTT1 = new XRiTextField();
    ZON1 = new XRiTextField();
    FL1 = new JLabel();
    NCG2X = new XRiTextField();
    TIE2 = new XRiTextField();
    SNS2 = new XRiSpinner();
    CLB2 = new XRiTextField();
    LIB2 = new XRiTextField();
    AFF02 = new XRiTextField();
    SAN2 = new XRiTextField();
    MTT2 = new XRiTextField();
    ZON2 = new XRiTextField();
    FL2 = new JLabel();
    NCG3X = new XRiTextField();
    TIE3 = new XRiTextField();
    SNS3 = new XRiSpinner();
    CLB3 = new XRiTextField();
    LIB3 = new XRiTextField();
    AFF03 = new XRiTextField();
    SAN3 = new XRiTextField();
    MTT3 = new XRiTextField();
    ZON3 = new XRiTextField();
    FL3 = new JLabel();
    NCG4X = new XRiTextField();
    TIE4 = new XRiTextField();
    SNS4 = new XRiSpinner();
    CLB4 = new XRiTextField();
    LIB4 = new XRiTextField();
    AFF04 = new XRiTextField();
    SAN4 = new XRiTextField();
    MTT4 = new XRiTextField();
    ZON4 = new XRiTextField();
    FL4 = new JLabel();
    NCG5X = new XRiTextField();
    TIE5 = new XRiTextField();
    SNS5 = new XRiSpinner();
    CLB5 = new XRiTextField();
    LIB5 = new XRiTextField();
    AFF05 = new XRiTextField();
    SAN5 = new XRiTextField();
    MTT5 = new XRiTextField();
    ZON5 = new XRiTextField();
    FL5 = new JLabel();
    NCG6X = new XRiTextField();
    TIE6 = new XRiTextField();
    SNS6 = new XRiSpinner();
    CLB6 = new XRiTextField();
    LIB6 = new XRiTextField();
    AFF06 = new XRiTextField();
    SAN6 = new XRiTextField();
    MTT6 = new XRiTextField();
    ZON6 = new XRiTextField();
    FL6 = new JLabel();
    NCG7X = new XRiTextField();
    TIE7 = new XRiTextField();
    SNS7 = new XRiSpinner();
    CLB7 = new XRiTextField();
    LIB7 = new XRiTextField();
    AFF07 = new XRiTextField();
    SAN7 = new XRiTextField();
    MTT7 = new XRiTextField();
    ZON7 = new XRiTextField();
    FL7 = new JLabel();
    NCG8X = new XRiTextField();
    TIE8 = new XRiTextField();
    SNS8 = new XRiSpinner();
    CLB8 = new XRiTextField();
    LIB8 = new XRiTextField();
    AFF08 = new XRiTextField();
    SAN8 = new XRiTextField();
    MTT8 = new XRiTextField();
    ZON8 = new XRiTextField();
    FL8 = new JLabel();
    NCG9X = new XRiTextField();
    TIE9 = new XRiTextField();
    SNS9 = new XRiSpinner();
    CLB9 = new XRiTextField();
    LIB9 = new XRiTextField();
    AFF09 = new XRiTextField();
    SAN9 = new XRiTextField();
    MTT9 = new XRiTextField();
    ZON9 = new XRiTextField();
    FL9 = new JLabel();
    NCG10X = new XRiTextField();
    TIE10 = new XRiTextField();
    SNS10 = new XRiSpinner();
    CLB10 = new XRiTextField();
    LIB10 = new XRiTextField();
    AFF10 = new XRiTextField();
    SAN10 = new XRiTextField();
    MTT10 = new XRiTextField();
    ZON10 = new XRiTextField();
    FL10 = new JLabel();
    NCG11X = new XRiTextField();
    TIE11 = new XRiTextField();
    SNS11 = new XRiSpinner();
    CLB11 = new XRiTextField();
    LIB11 = new XRiTextField();
    AFF11 = new XRiTextField();
    SAN11 = new XRiTextField();
    MTT11 = new XRiTextField();
    ZON11 = new XRiTextField();
    FL11 = new JLabel();
    NCG12X = new XRiTextField();
    TIE12 = new XRiTextField();
    SNS12 = new XRiSpinner();
    CLB12 = new XRiTextField();
    LIB12 = new XRiTextField();
    AFF12 = new XRiTextField();
    SAN12 = new XRiTextField();
    MTT12 = new XRiTextField();
    ZON12 = new XRiTextField();
    FL12 = new JLabel();
    NCG13X = new XRiTextField();
    TIE13 = new XRiTextField();
    SNS13 = new XRiSpinner();
    CLB13 = new XRiTextField();
    LIB13 = new XRiTextField();
    AFF13 = new XRiTextField();
    SAN13 = new XRiTextField();
    MTT13 = new XRiTextField();
    ZON13 = new XRiTextField();
    FL13 = new JLabel();
    NCG14X = new XRiTextField();
    TIE14 = new XRiTextField();
    SNS14 = new XRiSpinner();
    CLB14 = new XRiTextField();
    LIB14 = new XRiTextField();
    AFF14 = new XRiTextField();
    SAN14 = new XRiTextField();
    MTT14 = new XRiTextField();
    ZON14 = new XRiTextField();
    FL14 = new JLabel();
    OBJ_197 = new JLabel();
    OBJ_201 = new JLabel();
    OBJ_194 = new JLabel();
    OBJ_199 = new JLabel();
    OBJ_198 = new JLabel();
    OBJ_195 = new JLabel();
    OBJ_196 = new JLabel();
    TIE15 = new XRiTextField();
    TIE16 = new XRiTextField();
    TIE17 = new XRiTextField();
    TIE18 = new XRiTextField();
    TIE19 = new XRiTextField();
    TIE20 = new XRiTextField();
    TIE21 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_16 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Ecritures types");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_34 ----
          OBJ_34.setText("Soci\u00e9t\u00e9");
          OBJ_34.setName("OBJ_34");

          //---- OBJ_69 ----
          OBJ_69.setText("Code de pi\u00e8ce type");
          OBJ_69.setName("OBJ_69");

          //---- INDCOD ----
          INDCOD.setName("INDCOD");

          //---- ETLIB ----
          ETLIB.setComponentPopupMenu(BTD);
          ETLIB.setOpaque(false);
          ETLIB.setName("ETLIB");

          //---- ETJO ----
          ETJO.setComponentPopupMenu(BTD);
          ETJO.setName("ETJO");

          //---- BTN_JO ----
          BTN_JO.setText("Journal");
          BTN_JO.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BTN_JO.setToolTipText("Recherche d'un journal");
          BTN_JO.setName("BTN_JO");
          BTN_JO.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              BTN_JOActionPerformed(e);
            }
          });

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(OBJ_69, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(INDCOD, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(ETLIB, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(BTN_JO, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(ETJO, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_69, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDCOD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(ETLIB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(BTN_JO)
              .addComponent(ETJO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();

            //---- ETO1 ----
            ETO1.setName("ETO1");

            //---- ETO2 ----
            ETO2.setName("ETO2");

            //---- ETO3 ----
            ETO3.setName("ETO3");

            //---- ETO4 ----
            ETO4.setName("ETO4");

            //---- ETO5 ----
            ETO5.setName("ETO5");

            //---- ETO6 ----
            ETO6.setName("ETO6");

            //---- ETO7 ----
            ETO7.setName("ETO7");

            //---- ETO8 ----
            ETO8.setName("ETO8");

            //---- ETO9 ----
            ETO9.setName("ETO9");

            //---- ETO10 ----
            ETO10.setName("ETO10");

            //---- ETO11 ----
            ETO11.setName("ETO11");

            //---- ETO12 ----
            ETO12.setName("ETO12");

            //---- ETO13 ----
            ETO13.setName("ETO13");

            //---- ETO14 ----
            ETO14.setName("ETO14");

            //---- ETO15 ----
            ETO15.setName("ETO15");

            //---- ETO16 ----
            ETO16.setName("ETO16");

            //---- ETO17 ----
            ETO17.setName("ETO17");

            //---- ETO18 ----
            ETO18.setName("ETO18");

            //---- OBJ_218 ----
            OBJ_218.setText("=");
            OBJ_218.setName("OBJ_218");

            //---- OBJ_219 ----
            OBJ_219.setText("=");
            OBJ_219.setName("OBJ_219");

            //---- OBJ_204 ----
            OBJ_204.setText("X");
            OBJ_204.setName("OBJ_204");

            //---- OBJ_208 ----
            OBJ_208.setText("X");
            OBJ_208.setName("OBJ_208");

            //---- OBJ_234 ----
            OBJ_234.setText(")");
            OBJ_234.setName("OBJ_234");

            //---- OBJ_235 ----
            OBJ_235.setText(")");
            OBJ_235.setName("OBJ_235");

            //---- OBJ_237 ----
            OBJ_237.setText("+");
            OBJ_237.setName("OBJ_237");

            //---- OBJ_236 ----
            OBJ_236.setText("+");
            OBJ_236.setName("OBJ_236");

            //---- OBJ_223 ----
            OBJ_223.setText("(");
            OBJ_223.setName("OBJ_223");

            //---- OBJ_222 ----
            OBJ_222.setText("(");
            OBJ_222.setName("OBJ_222");

            //---- OBJ_205 ----
            OBJ_205.setText("X");
            OBJ_205.setName("OBJ_205");

            //---- OBJ_209 ----
            OBJ_209.setText("X");
            OBJ_209.setName("OBJ_209");

            //---- OBJ_233 ----
            OBJ_233.setText(")");
            OBJ_233.setName("OBJ_233");

            //---- OBJ_232 ----
            OBJ_232.setText(")");
            OBJ_232.setName("OBJ_232");

            //---- OBJ_238 ----
            OBJ_238.setText("+");
            OBJ_238.setName("OBJ_238");

            //---- OBJ_239 ----
            OBJ_239.setText("+");
            OBJ_239.setName("OBJ_239");

            //---- OBJ_206 ----
            OBJ_206.setText("X");
            OBJ_206.setName("OBJ_206");

            //---- OBJ_210 ----
            OBJ_210.setText("X");
            OBJ_210.setName("OBJ_210");

            //---- OBJ_225 ----
            OBJ_225.setText("(");
            OBJ_225.setName("OBJ_225");

            //---- OBJ_224 ----
            OBJ_224.setText("(");
            OBJ_224.setName("OBJ_224");

            //---- OBJ_230 ----
            OBJ_230.setText(")");
            OBJ_230.setName("OBJ_230");

            //---- OBJ_231 ----
            OBJ_231.setText(")");
            OBJ_231.setName("OBJ_231");

            //---- OBJ_240 ----
            OBJ_240.setText("+");
            OBJ_240.setName("OBJ_240");

            //---- OBJ_241 ----
            OBJ_241.setText("+");
            OBJ_241.setName("OBJ_241");

            //---- OBJ_226 ----
            OBJ_226.setText("(");
            OBJ_226.setName("OBJ_226");

            //---- OBJ_227 ----
            OBJ_227.setText("(");
            OBJ_227.setName("OBJ_227");

            //---- OBJ_211 ----
            OBJ_211.setText("X");
            OBJ_211.setName("OBJ_211");

            //---- OBJ_207 ----
            OBJ_207.setText("X");
            OBJ_207.setName("OBJ_207");

            //---- OBJ_229 ----
            OBJ_229.setText(")");
            OBJ_229.setName("OBJ_229");

            //---- OBJ_228 ----
            OBJ_228.setText(")");
            OBJ_228.setName("OBJ_228");

            //---- OBJ_220 ----
            OBJ_220.setText("(");
            OBJ_220.setName("OBJ_220");

            //---- OBJ_221 ----
            OBJ_221.setText("(");
            OBJ_221.setName("OBJ_221");

            GroupLayout xTitledPanel2ContentContainerLayout = new GroupLayout(xTitledPanel2ContentContainer);
            xTitledPanel2ContentContainer.setLayout(xTitledPanel2ContentContainerLayout);
            xTitledPanel2ContentContainerLayout.setHorizontalGroup(
              xTitledPanel2ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(8, 8, 8)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(ETO1, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ETO10, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_218, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_219, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE))
                  .addGap(8, 8, 8)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_220, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_221, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(10, 10, 10)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(ETO2, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ETO11, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))))
                  .addGap(10, 10, 10)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_204, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_208, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE))
                  .addGap(3, 3, 3)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(ETO3, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ETO12, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_234, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_235, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_236, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_237, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE))
                  .addGap(8, 8, 8)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_223, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_222, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(10, 10, 10)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(ETO4, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ETO13, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))))
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_205, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_209, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE))
                  .addGap(3, 3, 3)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(ETO5, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ETO14, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_233, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_232, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_239, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_238, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(10, 10, 10)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_224, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_225, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE))))
                  .addGap(3, 3, 3)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(ETO6, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ETO15, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_206, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_210, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(10, 10, 10)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(ETO7, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ETO16, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))))
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_231, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_230, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_240, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_241, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(10, 10, 10)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_227, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_226, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE))))
                  .addGap(3, 3, 3)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(ETO8, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ETO17, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_207, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_211, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(10, 10, 10)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(ETO9, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ETO18, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))))
                  .addGap(10, 10, 10)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_228, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_229, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)))
            );
            xTitledPanel2ContentContainerLayout.setVerticalGroup(
              xTitledPanel2ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(ETO1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(ETO10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_218)
                  .addGap(9, 9, 9)
                  .addComponent(OBJ_219))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_220))
                    .addComponent(ETO2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(4, 4, 4)
                  .addComponent(OBJ_221))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(26, 26, 26)
                  .addComponent(ETO11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(6, 6, 6)
                  .addComponent(OBJ_204)
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_208))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(ETO3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(ETO12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_234)
                  .addGap(9, 9, 9)
                  .addComponent(OBJ_235))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_236)
                  .addGap(9, 9, 9)
                  .addComponent(OBJ_237))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(ETO4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(6, 6, 6)
                          .addComponent(OBJ_223)))
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_222))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(ETO13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_205)
                  .addGap(9, 9, 9)
                  .addComponent(OBJ_209))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(ETO5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(ETO14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_233)
                  .addGap(9, 9, 9)
                  .addComponent(OBJ_232))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_224)
                    .addComponent(OBJ_239))
                  .addGap(9, 9, 9)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_225)
                    .addComponent(OBJ_238)))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(ETO6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(ETO15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_206))
                    .addComponent(ETO7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(ETO16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(31, 31, 31)
                      .addComponent(OBJ_210))))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_231)
                  .addGap(9, 9, 9)
                  .addComponent(OBJ_230))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_227)
                    .addComponent(OBJ_240))
                  .addGap(9, 9, 9)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_226)
                    .addComponent(OBJ_241)))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(ETO8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(ETO17, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(6, 6, 6)
                          .addComponent(OBJ_207))
                        .addComponent(ETO9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_211))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(ETO18, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_228)
                  .addGap(9, 9, 9)
                  .addComponent(OBJ_229))
            );
          }
          p_contenu.add(xTitledPanel2);
          xTitledPanel2.setBounds(15, 475, 880, xTitledPanel2.getPreferredSize().height);

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- OBJ_193 ----
            OBJ_193.setText("Compte");
            OBJ_193.setFont(OBJ_193.getFont().deriveFont(OBJ_193.getFont().getStyle() | Font.BOLD));
            OBJ_193.setName("OBJ_193");
            xTitledPanel1ContentContainer.add(OBJ_193);
            OBJ_193.setBounds(10, 25, 67, 25);

            //---- OBJ_200 ----
            OBJ_200.setText("Montant");
            OBJ_200.setFont(OBJ_200.getFont().deriveFont(OBJ_200.getFont().getStyle() | Font.BOLD));
            OBJ_200.setName("OBJ_200");
            xTitledPanel1ContentContainer.add(OBJ_200);
            OBJ_200.setBounds(640, 25, 60, 25);

            //---- NCG1X ----
            NCG1X.setComponentPopupMenu(BTD);
            NCG1X.setName("NCG1X");
            xTitledPanel1ContentContainer.add(NCG1X);
            NCG1X.setBounds(10, 50, 60, NCG1X.getPreferredSize().height);

            //---- TIE1 ----
            TIE1.setComponentPopupMenu(BTD);
            TIE1.setName("TIE1");
            xTitledPanel1ContentContainer.add(TIE1);
            TIE1.setBounds(90, 50, 60, TIE1.getPreferredSize().height);

            //---- SNS1 ----
            SNS1.setComponentPopupMenu(BTD);
            SNS1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNS1.setName("SNS1");
            xTitledPanel1ContentContainer.add(SNS1);
            SNS1.setBounds(160, 50, 45, SNS1.getPreferredSize().height);

            //---- CLB1 ----
            CLB1.setComponentPopupMenu(BTD);
            CLB1.setName("CLB1");
            xTitledPanel1ContentContainer.add(CLB1);
            CLB1.setBounds(210, 50, 20, CLB1.getPreferredSize().height);

            //---- LIB1 ----
            LIB1.setComponentPopupMenu(BTD);
            LIB1.setName("LIB1");
            xTitledPanel1ContentContainer.add(LIB1);
            LIB1.setBounds(235, 50, 270, LIB1.getPreferredSize().height);

            //---- AFF01 ----
            AFF01.setComponentPopupMenu(BTD);
            AFF01.setName("AFF01");
            xTitledPanel1ContentContainer.add(AFF01);
            AFF01.setBounds(510, 50, 60, AFF01.getPreferredSize().height);

            //---- SAN1 ----
            SAN1.setComponentPopupMenu(BTD);
            SAN1.setName("SAN1");
            xTitledPanel1ContentContainer.add(SAN1);
            SAN1.setBounds(580, 50, 50, SAN1.getPreferredSize().height);

            //---- MTT1 ----
            MTT1.setComponentPopupMenu(BTD);
            MTT1.setName("MTT1");
            xTitledPanel1ContentContainer.add(MTT1);
            MTT1.setBounds(635, 50, 100, MTT1.getPreferredSize().height);

            //---- ZON1 ----
            ZON1.setComponentPopupMenu(BTD);
            ZON1.setName("ZON1");
            xTitledPanel1ContentContainer.add(ZON1);
            ZON1.setBounds(745, 50, 50, ZON1.getPreferredSize().height);

            //---- FL1 ----
            FL1.setText("<--");
            FL1.setFont(new Font("Courier New", Font.BOLD, 17));
            FL1.setForeground(Color.red);
            FL1.setName("FL1");
            xTitledPanel1ContentContainer.add(FL1);
            FL1.setBounds(805, 50, 60, 26);

            //---- NCG2X ----
            NCG2X.setComponentPopupMenu(BTD);
            NCG2X.setName("NCG2X");
            xTitledPanel1ContentContainer.add(NCG2X);
            NCG2X.setBounds(10, 75, 60, NCG2X.getPreferredSize().height);

            //---- TIE2 ----
            TIE2.setComponentPopupMenu(BTD);
            TIE2.setName("TIE2");
            xTitledPanel1ContentContainer.add(TIE2);
            TIE2.setBounds(90, 75, 60, TIE2.getPreferredSize().height);

            //---- SNS2 ----
            SNS2.setComponentPopupMenu(BTD);
            SNS2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNS2.setName("SNS2");
            xTitledPanel1ContentContainer.add(SNS2);
            SNS2.setBounds(160, 75, 45, SNS2.getPreferredSize().height);

            //---- CLB2 ----
            CLB2.setComponentPopupMenu(BTD);
            CLB2.setName("CLB2");
            xTitledPanel1ContentContainer.add(CLB2);
            CLB2.setBounds(210, 75, 19, CLB2.getPreferredSize().height);

            //---- LIB2 ----
            LIB2.setComponentPopupMenu(BTD);
            LIB2.setName("LIB2");
            xTitledPanel1ContentContainer.add(LIB2);
            LIB2.setBounds(235, 75, 270, LIB2.getPreferredSize().height);

            //---- AFF02 ----
            AFF02.setComponentPopupMenu(BTD);
            AFF02.setName("AFF02");
            xTitledPanel1ContentContainer.add(AFF02);
            AFF02.setBounds(510, 75, 60, AFF02.getPreferredSize().height);

            //---- SAN2 ----
            SAN2.setComponentPopupMenu(BTD);
            SAN2.setName("SAN2");
            xTitledPanel1ContentContainer.add(SAN2);
            SAN2.setBounds(580, 75, 50, SAN2.getPreferredSize().height);

            //---- MTT2 ----
            MTT2.setComponentPopupMenu(BTD);
            MTT2.setName("MTT2");
            xTitledPanel1ContentContainer.add(MTT2);
            MTT2.setBounds(635, 75, 100, MTT2.getPreferredSize().height);

            //---- ZON2 ----
            ZON2.setComponentPopupMenu(BTD);
            ZON2.setName("ZON2");
            xTitledPanel1ContentContainer.add(ZON2);
            ZON2.setBounds(745, 75, 50, ZON2.getPreferredSize().height);

            //---- FL2 ----
            FL2.setText("<--");
            FL2.setFont(new Font("Courier New", Font.BOLD, 17));
            FL2.setForeground(Color.red);
            FL2.setName("FL2");
            xTitledPanel1ContentContainer.add(FL2);
            FL2.setBounds(805, 75, 60, 26);

            //---- NCG3X ----
            NCG3X.setComponentPopupMenu(BTD);
            NCG3X.setName("NCG3X");
            xTitledPanel1ContentContainer.add(NCG3X);
            NCG3X.setBounds(10, 100, 60, NCG3X.getPreferredSize().height);

            //---- TIE3 ----
            TIE3.setComponentPopupMenu(BTD);
            TIE3.setName("TIE3");
            xTitledPanel1ContentContainer.add(TIE3);
            TIE3.setBounds(90, 100, 60, TIE3.getPreferredSize().height);

            //---- SNS3 ----
            SNS3.setComponentPopupMenu(BTD);
            SNS3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNS3.setName("SNS3");
            xTitledPanel1ContentContainer.add(SNS3);
            SNS3.setBounds(160, 100, 45, SNS3.getPreferredSize().height);

            //---- CLB3 ----
            CLB3.setComponentPopupMenu(BTD);
            CLB3.setName("CLB3");
            xTitledPanel1ContentContainer.add(CLB3);
            CLB3.setBounds(210, 100, 19, CLB3.getPreferredSize().height);

            //---- LIB3 ----
            LIB3.setComponentPopupMenu(BTD);
            LIB3.setName("LIB3");
            xTitledPanel1ContentContainer.add(LIB3);
            LIB3.setBounds(235, 100, 270, LIB3.getPreferredSize().height);

            //---- AFF03 ----
            AFF03.setComponentPopupMenu(BTD);
            AFF03.setName("AFF03");
            xTitledPanel1ContentContainer.add(AFF03);
            AFF03.setBounds(510, 100, 60, AFF03.getPreferredSize().height);

            //---- SAN3 ----
            SAN3.setComponentPopupMenu(BTD);
            SAN3.setName("SAN3");
            xTitledPanel1ContentContainer.add(SAN3);
            SAN3.setBounds(580, 100, 50, SAN3.getPreferredSize().height);

            //---- MTT3 ----
            MTT3.setComponentPopupMenu(BTD);
            MTT3.setName("MTT3");
            xTitledPanel1ContentContainer.add(MTT3);
            MTT3.setBounds(635, 100, 100, MTT3.getPreferredSize().height);

            //---- ZON3 ----
            ZON3.setComponentPopupMenu(BTD);
            ZON3.setName("ZON3");
            xTitledPanel1ContentContainer.add(ZON3);
            ZON3.setBounds(745, 100, 50, ZON3.getPreferredSize().height);

            //---- FL3 ----
            FL3.setText("<--");
            FL3.setFont(new Font("Courier New", Font.BOLD, 17));
            FL3.setForeground(Color.red);
            FL3.setName("FL3");
            xTitledPanel1ContentContainer.add(FL3);
            FL3.setBounds(805, 100, 60, 26);

            //---- NCG4X ----
            NCG4X.setComponentPopupMenu(BTD);
            NCG4X.setName("NCG4X");
            xTitledPanel1ContentContainer.add(NCG4X);
            NCG4X.setBounds(10, 125, 60, NCG4X.getPreferredSize().height);

            //---- TIE4 ----
            TIE4.setComponentPopupMenu(BTD);
            TIE4.setName("TIE4");
            xTitledPanel1ContentContainer.add(TIE4);
            TIE4.setBounds(90, 125, 60, TIE4.getPreferredSize().height);

            //---- SNS4 ----
            SNS4.setComponentPopupMenu(BTD);
            SNS4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNS4.setName("SNS4");
            xTitledPanel1ContentContainer.add(SNS4);
            SNS4.setBounds(160, 125, 45, SNS4.getPreferredSize().height);

            //---- CLB4 ----
            CLB4.setComponentPopupMenu(BTD);
            CLB4.setName("CLB4");
            xTitledPanel1ContentContainer.add(CLB4);
            CLB4.setBounds(210, 125, 19, CLB4.getPreferredSize().height);

            //---- LIB4 ----
            LIB4.setComponentPopupMenu(BTD);
            LIB4.setName("LIB4");
            xTitledPanel1ContentContainer.add(LIB4);
            LIB4.setBounds(235, 125, 270, LIB4.getPreferredSize().height);

            //---- AFF04 ----
            AFF04.setComponentPopupMenu(BTD);
            AFF04.setName("AFF04");
            xTitledPanel1ContentContainer.add(AFF04);
            AFF04.setBounds(510, 125, 60, AFF04.getPreferredSize().height);

            //---- SAN4 ----
            SAN4.setComponentPopupMenu(BTD);
            SAN4.setName("SAN4");
            xTitledPanel1ContentContainer.add(SAN4);
            SAN4.setBounds(580, 125, 50, SAN4.getPreferredSize().height);

            //---- MTT4 ----
            MTT4.setComponentPopupMenu(BTD);
            MTT4.setName("MTT4");
            xTitledPanel1ContentContainer.add(MTT4);
            MTT4.setBounds(635, 125, 100, MTT4.getPreferredSize().height);

            //---- ZON4 ----
            ZON4.setComponentPopupMenu(BTD);
            ZON4.setName("ZON4");
            xTitledPanel1ContentContainer.add(ZON4);
            ZON4.setBounds(745, 125, 50, ZON4.getPreferredSize().height);

            //---- FL4 ----
            FL4.setText("<--");
            FL4.setFont(new Font("Courier New", Font.BOLD, 17));
            FL4.setForeground(Color.red);
            FL4.setName("FL4");
            xTitledPanel1ContentContainer.add(FL4);
            FL4.setBounds(805, 125, 60, 26);

            //---- NCG5X ----
            NCG5X.setComponentPopupMenu(BTD);
            NCG5X.setName("NCG5X");
            xTitledPanel1ContentContainer.add(NCG5X);
            NCG5X.setBounds(10, 150, 60, NCG5X.getPreferredSize().height);

            //---- TIE5 ----
            TIE5.setComponentPopupMenu(BTD);
            TIE5.setName("TIE5");
            xTitledPanel1ContentContainer.add(TIE5);
            TIE5.setBounds(90, 150, 60, TIE5.getPreferredSize().height);

            //---- SNS5 ----
            SNS5.setComponentPopupMenu(BTD);
            SNS5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNS5.setName("SNS5");
            xTitledPanel1ContentContainer.add(SNS5);
            SNS5.setBounds(160, 150, 45, SNS5.getPreferredSize().height);

            //---- CLB5 ----
            CLB5.setComponentPopupMenu(BTD);
            CLB5.setName("CLB5");
            xTitledPanel1ContentContainer.add(CLB5);
            CLB5.setBounds(210, 150, 19, CLB5.getPreferredSize().height);

            //---- LIB5 ----
            LIB5.setComponentPopupMenu(BTD);
            LIB5.setName("LIB5");
            xTitledPanel1ContentContainer.add(LIB5);
            LIB5.setBounds(235, 150, 270, LIB5.getPreferredSize().height);

            //---- AFF05 ----
            AFF05.setComponentPopupMenu(BTD);
            AFF05.setName("AFF05");
            xTitledPanel1ContentContainer.add(AFF05);
            AFF05.setBounds(510, 150, 60, AFF05.getPreferredSize().height);

            //---- SAN5 ----
            SAN5.setComponentPopupMenu(BTD);
            SAN5.setName("SAN5");
            xTitledPanel1ContentContainer.add(SAN5);
            SAN5.setBounds(580, 150, 50, SAN5.getPreferredSize().height);

            //---- MTT5 ----
            MTT5.setComponentPopupMenu(BTD);
            MTT5.setName("MTT5");
            xTitledPanel1ContentContainer.add(MTT5);
            MTT5.setBounds(635, 150, 100, MTT5.getPreferredSize().height);

            //---- ZON5 ----
            ZON5.setComponentPopupMenu(BTD);
            ZON5.setName("ZON5");
            xTitledPanel1ContentContainer.add(ZON5);
            ZON5.setBounds(745, 150, 50, ZON5.getPreferredSize().height);

            //---- FL5 ----
            FL5.setText("<--");
            FL5.setFont(new Font("Courier New", Font.BOLD, 17));
            FL5.setForeground(Color.red);
            FL5.setName("FL5");
            xTitledPanel1ContentContainer.add(FL5);
            FL5.setBounds(805, 150, 60, 26);

            //---- NCG6X ----
            NCG6X.setComponentPopupMenu(BTD);
            NCG6X.setName("NCG6X");
            xTitledPanel1ContentContainer.add(NCG6X);
            NCG6X.setBounds(10, 175, 60, NCG6X.getPreferredSize().height);

            //---- TIE6 ----
            TIE6.setComponentPopupMenu(BTD);
            TIE6.setName("TIE6");
            xTitledPanel1ContentContainer.add(TIE6);
            TIE6.setBounds(90, 175, 60, TIE6.getPreferredSize().height);

            //---- SNS6 ----
            SNS6.setComponentPopupMenu(BTD);
            SNS6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNS6.setName("SNS6");
            xTitledPanel1ContentContainer.add(SNS6);
            SNS6.setBounds(160, 175, 45, SNS6.getPreferredSize().height);

            //---- CLB6 ----
            CLB6.setComponentPopupMenu(BTD);
            CLB6.setName("CLB6");
            xTitledPanel1ContentContainer.add(CLB6);
            CLB6.setBounds(210, 175, 19, CLB6.getPreferredSize().height);

            //---- LIB6 ----
            LIB6.setComponentPopupMenu(BTD);
            LIB6.setName("LIB6");
            xTitledPanel1ContentContainer.add(LIB6);
            LIB6.setBounds(235, 175, 270, LIB6.getPreferredSize().height);

            //---- AFF06 ----
            AFF06.setComponentPopupMenu(BTD);
            AFF06.setName("AFF06");
            xTitledPanel1ContentContainer.add(AFF06);
            AFF06.setBounds(510, 175, 60, AFF06.getPreferredSize().height);

            //---- SAN6 ----
            SAN6.setComponentPopupMenu(BTD);
            SAN6.setName("SAN6");
            xTitledPanel1ContentContainer.add(SAN6);
            SAN6.setBounds(580, 175, 50, SAN6.getPreferredSize().height);

            //---- MTT6 ----
            MTT6.setComponentPopupMenu(BTD);
            MTT6.setName("MTT6");
            xTitledPanel1ContentContainer.add(MTT6);
            MTT6.setBounds(635, 175, 100, MTT6.getPreferredSize().height);

            //---- ZON6 ----
            ZON6.setComponentPopupMenu(BTD);
            ZON6.setName("ZON6");
            xTitledPanel1ContentContainer.add(ZON6);
            ZON6.setBounds(745, 175, 50, ZON6.getPreferredSize().height);

            //---- FL6 ----
            FL6.setText("<--");
            FL6.setFont(new Font("Courier New", Font.BOLD, 17));
            FL6.setForeground(Color.red);
            FL6.setName("FL6");
            xTitledPanel1ContentContainer.add(FL6);
            FL6.setBounds(805, 175, 60, 26);

            //---- NCG7X ----
            NCG7X.setComponentPopupMenu(BTD);
            NCG7X.setName("NCG7X");
            xTitledPanel1ContentContainer.add(NCG7X);
            NCG7X.setBounds(10, 200, 60, NCG7X.getPreferredSize().height);

            //---- TIE7 ----
            TIE7.setComponentPopupMenu(BTD);
            TIE7.setName("TIE7");
            xTitledPanel1ContentContainer.add(TIE7);
            TIE7.setBounds(90, 200, 60, TIE7.getPreferredSize().height);

            //---- SNS7 ----
            SNS7.setComponentPopupMenu(BTD);
            SNS7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNS7.setName("SNS7");
            xTitledPanel1ContentContainer.add(SNS7);
            SNS7.setBounds(160, 200, 45, SNS7.getPreferredSize().height);

            //---- CLB7 ----
            CLB7.setComponentPopupMenu(BTD);
            CLB7.setName("CLB7");
            xTitledPanel1ContentContainer.add(CLB7);
            CLB7.setBounds(210, 200, 19, CLB7.getPreferredSize().height);

            //---- LIB7 ----
            LIB7.setComponentPopupMenu(BTD);
            LIB7.setName("LIB7");
            xTitledPanel1ContentContainer.add(LIB7);
            LIB7.setBounds(235, 200, 270, LIB7.getPreferredSize().height);

            //---- AFF07 ----
            AFF07.setComponentPopupMenu(BTD);
            AFF07.setName("AFF07");
            xTitledPanel1ContentContainer.add(AFF07);
            AFF07.setBounds(510, 200, 60, AFF07.getPreferredSize().height);

            //---- SAN7 ----
            SAN7.setComponentPopupMenu(BTD);
            SAN7.setName("SAN7");
            xTitledPanel1ContentContainer.add(SAN7);
            SAN7.setBounds(580, 200, 50, SAN7.getPreferredSize().height);

            //---- MTT7 ----
            MTT7.setComponentPopupMenu(BTD);
            MTT7.setName("MTT7");
            xTitledPanel1ContentContainer.add(MTT7);
            MTT7.setBounds(635, 200, 100, MTT7.getPreferredSize().height);

            //---- ZON7 ----
            ZON7.setComponentPopupMenu(BTD);
            ZON7.setName("ZON7");
            xTitledPanel1ContentContainer.add(ZON7);
            ZON7.setBounds(745, 200, 50, ZON7.getPreferredSize().height);

            //---- FL7 ----
            FL7.setText("<--");
            FL7.setFont(new Font("Courier New", Font.BOLD, 17));
            FL7.setForeground(Color.red);
            FL7.setName("FL7");
            xTitledPanel1ContentContainer.add(FL7);
            FL7.setBounds(805, 200, 60, 26);

            //---- NCG8X ----
            NCG8X.setComponentPopupMenu(BTD);
            NCG8X.setName("NCG8X");
            xTitledPanel1ContentContainer.add(NCG8X);
            NCG8X.setBounds(10, 225, 60, NCG8X.getPreferredSize().height);

            //---- TIE8 ----
            TIE8.setComponentPopupMenu(BTD);
            TIE8.setName("TIE8");
            xTitledPanel1ContentContainer.add(TIE8);
            TIE8.setBounds(90, 225, 60, TIE8.getPreferredSize().height);

            //---- SNS8 ----
            SNS8.setComponentPopupMenu(BTD);
            SNS8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNS8.setName("SNS8");
            xTitledPanel1ContentContainer.add(SNS8);
            SNS8.setBounds(160, 225, 45, SNS8.getPreferredSize().height);

            //---- CLB8 ----
            CLB8.setComponentPopupMenu(BTD);
            CLB8.setName("CLB8");
            xTitledPanel1ContentContainer.add(CLB8);
            CLB8.setBounds(210, 225, 19, CLB8.getPreferredSize().height);

            //---- LIB8 ----
            LIB8.setComponentPopupMenu(BTD);
            LIB8.setName("LIB8");
            xTitledPanel1ContentContainer.add(LIB8);
            LIB8.setBounds(235, 225, 270, LIB8.getPreferredSize().height);

            //---- AFF08 ----
            AFF08.setComponentPopupMenu(BTD);
            AFF08.setName("AFF08");
            xTitledPanel1ContentContainer.add(AFF08);
            AFF08.setBounds(510, 225, 60, AFF08.getPreferredSize().height);

            //---- SAN8 ----
            SAN8.setComponentPopupMenu(BTD);
            SAN8.setName("SAN8");
            xTitledPanel1ContentContainer.add(SAN8);
            SAN8.setBounds(580, 225, 50, SAN8.getPreferredSize().height);

            //---- MTT8 ----
            MTT8.setComponentPopupMenu(BTD);
            MTT8.setName("MTT8");
            xTitledPanel1ContentContainer.add(MTT8);
            MTT8.setBounds(635, 225, 100, MTT8.getPreferredSize().height);

            //---- ZON8 ----
            ZON8.setComponentPopupMenu(BTD);
            ZON8.setName("ZON8");
            xTitledPanel1ContentContainer.add(ZON8);
            ZON8.setBounds(745, 225, 50, ZON8.getPreferredSize().height);

            //---- FL8 ----
            FL8.setText("<--");
            FL8.setFont(new Font("Courier New", Font.BOLD, 17));
            FL8.setForeground(Color.red);
            FL8.setName("FL8");
            xTitledPanel1ContentContainer.add(FL8);
            FL8.setBounds(805, 225, 60, 26);

            //---- NCG9X ----
            NCG9X.setComponentPopupMenu(BTD);
            NCG9X.setName("NCG9X");
            xTitledPanel1ContentContainer.add(NCG9X);
            NCG9X.setBounds(10, 250, 60, NCG9X.getPreferredSize().height);

            //---- TIE9 ----
            TIE9.setComponentPopupMenu(BTD);
            TIE9.setName("TIE9");
            xTitledPanel1ContentContainer.add(TIE9);
            TIE9.setBounds(90, 250, 60, TIE9.getPreferredSize().height);

            //---- SNS9 ----
            SNS9.setComponentPopupMenu(BTD);
            SNS9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNS9.setName("SNS9");
            xTitledPanel1ContentContainer.add(SNS9);
            SNS9.setBounds(160, 250, 45, SNS9.getPreferredSize().height);

            //---- CLB9 ----
            CLB9.setComponentPopupMenu(BTD);
            CLB9.setName("CLB9");
            xTitledPanel1ContentContainer.add(CLB9);
            CLB9.setBounds(210, 250, 19, CLB9.getPreferredSize().height);

            //---- LIB9 ----
            LIB9.setComponentPopupMenu(BTD);
            LIB9.setName("LIB9");
            xTitledPanel1ContentContainer.add(LIB9);
            LIB9.setBounds(235, 250, 270, LIB9.getPreferredSize().height);

            //---- AFF09 ----
            AFF09.setComponentPopupMenu(BTD);
            AFF09.setName("AFF09");
            xTitledPanel1ContentContainer.add(AFF09);
            AFF09.setBounds(510, 250, 60, AFF09.getPreferredSize().height);

            //---- SAN9 ----
            SAN9.setComponentPopupMenu(BTD);
            SAN9.setName("SAN9");
            xTitledPanel1ContentContainer.add(SAN9);
            SAN9.setBounds(580, 250, 50, SAN9.getPreferredSize().height);

            //---- MTT9 ----
            MTT9.setComponentPopupMenu(BTD);
            MTT9.setName("MTT9");
            xTitledPanel1ContentContainer.add(MTT9);
            MTT9.setBounds(635, 250, 100, MTT9.getPreferredSize().height);

            //---- ZON9 ----
            ZON9.setComponentPopupMenu(BTD);
            ZON9.setName("ZON9");
            xTitledPanel1ContentContainer.add(ZON9);
            ZON9.setBounds(745, 250, 50, ZON9.getPreferredSize().height);

            //---- FL9 ----
            FL9.setText("<--");
            FL9.setFont(new Font("Courier New", Font.BOLD, 17));
            FL9.setForeground(Color.red);
            FL9.setName("FL9");
            xTitledPanel1ContentContainer.add(FL9);
            FL9.setBounds(805, 250, 60, 26);

            //---- NCG10X ----
            NCG10X.setComponentPopupMenu(BTD);
            NCG10X.setName("NCG10X");
            xTitledPanel1ContentContainer.add(NCG10X);
            NCG10X.setBounds(10, 275, 60, NCG10X.getPreferredSize().height);

            //---- TIE10 ----
            TIE10.setComponentPopupMenu(BTD);
            TIE10.setName("TIE10");
            xTitledPanel1ContentContainer.add(TIE10);
            TIE10.setBounds(90, 275, 60, TIE10.getPreferredSize().height);

            //---- SNS10 ----
            SNS10.setComponentPopupMenu(BTD);
            SNS10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNS10.setName("SNS10");
            xTitledPanel1ContentContainer.add(SNS10);
            SNS10.setBounds(160, 275, 45, SNS10.getPreferredSize().height);

            //---- CLB10 ----
            CLB10.setComponentPopupMenu(BTD);
            CLB10.setName("CLB10");
            xTitledPanel1ContentContainer.add(CLB10);
            CLB10.setBounds(210, 275, 19, CLB10.getPreferredSize().height);

            //---- LIB10 ----
            LIB10.setComponentPopupMenu(BTD);
            LIB10.setName("LIB10");
            xTitledPanel1ContentContainer.add(LIB10);
            LIB10.setBounds(235, 275, 270, LIB10.getPreferredSize().height);

            //---- AFF10 ----
            AFF10.setComponentPopupMenu(BTD);
            AFF10.setName("AFF10");
            xTitledPanel1ContentContainer.add(AFF10);
            AFF10.setBounds(510, 275, 60, AFF10.getPreferredSize().height);

            //---- SAN10 ----
            SAN10.setComponentPopupMenu(BTD);
            SAN10.setName("SAN10");
            xTitledPanel1ContentContainer.add(SAN10);
            SAN10.setBounds(580, 275, 50, SAN10.getPreferredSize().height);

            //---- MTT10 ----
            MTT10.setComponentPopupMenu(BTD);
            MTT10.setName("MTT10");
            xTitledPanel1ContentContainer.add(MTT10);
            MTT10.setBounds(635, 275, 100, MTT10.getPreferredSize().height);

            //---- ZON10 ----
            ZON10.setComponentPopupMenu(BTD);
            ZON10.setName("ZON10");
            xTitledPanel1ContentContainer.add(ZON10);
            ZON10.setBounds(745, 275, 50, ZON10.getPreferredSize().height);

            //---- FL10 ----
            FL10.setText("<--");
            FL10.setFont(new Font("Courier New", Font.BOLD, 17));
            FL10.setForeground(Color.red);
            FL10.setName("FL10");
            xTitledPanel1ContentContainer.add(FL10);
            FL10.setBounds(805, 275, 60, 26);

            //---- NCG11X ----
            NCG11X.setComponentPopupMenu(BTD);
            NCG11X.setName("NCG11X");
            xTitledPanel1ContentContainer.add(NCG11X);
            NCG11X.setBounds(10, 300, 60, NCG11X.getPreferredSize().height);

            //---- TIE11 ----
            TIE11.setComponentPopupMenu(BTD);
            TIE11.setName("TIE11");
            xTitledPanel1ContentContainer.add(TIE11);
            TIE11.setBounds(90, 300, 60, TIE11.getPreferredSize().height);

            //---- SNS11 ----
            SNS11.setComponentPopupMenu(BTD);
            SNS11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNS11.setName("SNS11");
            xTitledPanel1ContentContainer.add(SNS11);
            SNS11.setBounds(160, 300, 45, SNS11.getPreferredSize().height);

            //---- CLB11 ----
            CLB11.setComponentPopupMenu(BTD);
            CLB11.setName("CLB11");
            xTitledPanel1ContentContainer.add(CLB11);
            CLB11.setBounds(210, 300, 19, CLB11.getPreferredSize().height);

            //---- LIB11 ----
            LIB11.setComponentPopupMenu(BTD);
            LIB11.setName("LIB11");
            xTitledPanel1ContentContainer.add(LIB11);
            LIB11.setBounds(235, 300, 270, LIB11.getPreferredSize().height);

            //---- AFF11 ----
            AFF11.setComponentPopupMenu(BTD);
            AFF11.setName("AFF11");
            xTitledPanel1ContentContainer.add(AFF11);
            AFF11.setBounds(510, 300, 60, AFF11.getPreferredSize().height);

            //---- SAN11 ----
            SAN11.setComponentPopupMenu(BTD);
            SAN11.setName("SAN11");
            xTitledPanel1ContentContainer.add(SAN11);
            SAN11.setBounds(580, 300, 50, SAN11.getPreferredSize().height);

            //---- MTT11 ----
            MTT11.setComponentPopupMenu(BTD);
            MTT11.setName("MTT11");
            xTitledPanel1ContentContainer.add(MTT11);
            MTT11.setBounds(635, 300, 100, MTT11.getPreferredSize().height);

            //---- ZON11 ----
            ZON11.setComponentPopupMenu(BTD);
            ZON11.setName("ZON11");
            xTitledPanel1ContentContainer.add(ZON11);
            ZON11.setBounds(745, 300, 50, ZON11.getPreferredSize().height);

            //---- FL11 ----
            FL11.setText("<--");
            FL11.setFont(new Font("Courier New", Font.BOLD, 17));
            FL11.setForeground(Color.red);
            FL11.setName("FL11");
            xTitledPanel1ContentContainer.add(FL11);
            FL11.setBounds(805, 300, 60, 26);

            //---- NCG12X ----
            NCG12X.setComponentPopupMenu(BTD);
            NCG12X.setName("NCG12X");
            xTitledPanel1ContentContainer.add(NCG12X);
            NCG12X.setBounds(10, 325, 60, NCG12X.getPreferredSize().height);

            //---- TIE12 ----
            TIE12.setComponentPopupMenu(BTD);
            TIE12.setName("TIE12");
            xTitledPanel1ContentContainer.add(TIE12);
            TIE12.setBounds(90, 325, 60, TIE12.getPreferredSize().height);

            //---- SNS12 ----
            SNS12.setComponentPopupMenu(BTD);
            SNS12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNS12.setName("SNS12");
            xTitledPanel1ContentContainer.add(SNS12);
            SNS12.setBounds(160, 325, 45, SNS12.getPreferredSize().height);

            //---- CLB12 ----
            CLB12.setComponentPopupMenu(BTD);
            CLB12.setName("CLB12");
            xTitledPanel1ContentContainer.add(CLB12);
            CLB12.setBounds(210, 325, 19, CLB12.getPreferredSize().height);

            //---- LIB12 ----
            LIB12.setComponentPopupMenu(BTD);
            LIB12.setName("LIB12");
            xTitledPanel1ContentContainer.add(LIB12);
            LIB12.setBounds(235, 325, 270, LIB12.getPreferredSize().height);

            //---- AFF12 ----
            AFF12.setComponentPopupMenu(BTD);
            AFF12.setName("AFF12");
            xTitledPanel1ContentContainer.add(AFF12);
            AFF12.setBounds(510, 325, 60, AFF12.getPreferredSize().height);

            //---- SAN12 ----
            SAN12.setComponentPopupMenu(BTD);
            SAN12.setName("SAN12");
            xTitledPanel1ContentContainer.add(SAN12);
            SAN12.setBounds(580, 325, 50, SAN12.getPreferredSize().height);

            //---- MTT12 ----
            MTT12.setComponentPopupMenu(BTD);
            MTT12.setName("MTT12");
            xTitledPanel1ContentContainer.add(MTT12);
            MTT12.setBounds(635, 325, 100, MTT12.getPreferredSize().height);

            //---- ZON12 ----
            ZON12.setComponentPopupMenu(BTD);
            ZON12.setName("ZON12");
            xTitledPanel1ContentContainer.add(ZON12);
            ZON12.setBounds(745, 325, 50, ZON12.getPreferredSize().height);

            //---- FL12 ----
            FL12.setText("<--");
            FL12.setFont(new Font("Courier New", Font.BOLD, 17));
            FL12.setForeground(Color.red);
            FL12.setName("FL12");
            xTitledPanel1ContentContainer.add(FL12);
            FL12.setBounds(805, 325, 60, 26);

            //---- NCG13X ----
            NCG13X.setComponentPopupMenu(BTD);
            NCG13X.setName("NCG13X");
            xTitledPanel1ContentContainer.add(NCG13X);
            NCG13X.setBounds(10, 350, 60, NCG13X.getPreferredSize().height);

            //---- TIE13 ----
            TIE13.setComponentPopupMenu(BTD);
            TIE13.setName("TIE13");
            xTitledPanel1ContentContainer.add(TIE13);
            TIE13.setBounds(90, 350, 60, TIE13.getPreferredSize().height);

            //---- SNS13 ----
            SNS13.setComponentPopupMenu(BTD);
            SNS13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNS13.setName("SNS13");
            xTitledPanel1ContentContainer.add(SNS13);
            SNS13.setBounds(160, 350, 45, SNS13.getPreferredSize().height);

            //---- CLB13 ----
            CLB13.setComponentPopupMenu(BTD);
            CLB13.setName("CLB13");
            xTitledPanel1ContentContainer.add(CLB13);
            CLB13.setBounds(210, 350, 19, CLB13.getPreferredSize().height);

            //---- LIB13 ----
            LIB13.setComponentPopupMenu(BTD);
            LIB13.setName("LIB13");
            xTitledPanel1ContentContainer.add(LIB13);
            LIB13.setBounds(235, 350, 270, LIB13.getPreferredSize().height);

            //---- AFF13 ----
            AFF13.setComponentPopupMenu(BTD);
            AFF13.setName("AFF13");
            xTitledPanel1ContentContainer.add(AFF13);
            AFF13.setBounds(510, 350, 60, AFF13.getPreferredSize().height);

            //---- SAN13 ----
            SAN13.setComponentPopupMenu(BTD);
            SAN13.setName("SAN13");
            xTitledPanel1ContentContainer.add(SAN13);
            SAN13.setBounds(580, 350, 50, SAN13.getPreferredSize().height);

            //---- MTT13 ----
            MTT13.setComponentPopupMenu(BTD);
            MTT13.setName("MTT13");
            xTitledPanel1ContentContainer.add(MTT13);
            MTT13.setBounds(635, 350, 100, MTT13.getPreferredSize().height);

            //---- ZON13 ----
            ZON13.setComponentPopupMenu(BTD);
            ZON13.setName("ZON13");
            xTitledPanel1ContentContainer.add(ZON13);
            ZON13.setBounds(745, 350, 50, ZON13.getPreferredSize().height);

            //---- FL13 ----
            FL13.setText("<--");
            FL13.setFont(new Font("Courier New", Font.BOLD, 17));
            FL13.setForeground(Color.red);
            FL13.setName("FL13");
            xTitledPanel1ContentContainer.add(FL13);
            FL13.setBounds(805, 350, 60, 26);

            //---- NCG14X ----
            NCG14X.setComponentPopupMenu(BTD);
            NCG14X.setName("NCG14X");
            xTitledPanel1ContentContainer.add(NCG14X);
            NCG14X.setBounds(10, 375, 60, NCG14X.getPreferredSize().height);

            //---- TIE14 ----
            TIE14.setComponentPopupMenu(BTD);
            TIE14.setName("TIE14");
            xTitledPanel1ContentContainer.add(TIE14);
            TIE14.setBounds(90, 375, 60, TIE14.getPreferredSize().height);

            //---- SNS14 ----
            SNS14.setComponentPopupMenu(BTD);
            SNS14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNS14.setName("SNS14");
            xTitledPanel1ContentContainer.add(SNS14);
            SNS14.setBounds(160, 375, 45, SNS14.getPreferredSize().height);

            //---- CLB14 ----
            CLB14.setComponentPopupMenu(BTD);
            CLB14.setName("CLB14");
            xTitledPanel1ContentContainer.add(CLB14);
            CLB14.setBounds(210, 375, 19, CLB14.getPreferredSize().height);

            //---- LIB14 ----
            LIB14.setComponentPopupMenu(BTD);
            LIB14.setName("LIB14");
            xTitledPanel1ContentContainer.add(LIB14);
            LIB14.setBounds(235, 375, 270, LIB14.getPreferredSize().height);

            //---- AFF14 ----
            AFF14.setComponentPopupMenu(BTD);
            AFF14.setName("AFF14");
            xTitledPanel1ContentContainer.add(AFF14);
            AFF14.setBounds(510, 375, 60, AFF14.getPreferredSize().height);

            //---- SAN14 ----
            SAN14.setComponentPopupMenu(BTD);
            SAN14.setName("SAN14");
            xTitledPanel1ContentContainer.add(SAN14);
            SAN14.setBounds(580, 375, 50, SAN14.getPreferredSize().height);

            //---- MTT14 ----
            MTT14.setComponentPopupMenu(BTD);
            MTT14.setName("MTT14");
            xTitledPanel1ContentContainer.add(MTT14);
            MTT14.setBounds(635, 375, 100, MTT14.getPreferredSize().height);

            //---- ZON14 ----
            ZON14.setComponentPopupMenu(BTD);
            ZON14.setName("ZON14");
            xTitledPanel1ContentContainer.add(ZON14);
            ZON14.setBounds(745, 375, 50, ZON14.getPreferredSize().height);

            //---- FL14 ----
            FL14.setText("<--");
            FL14.setFont(new Font("Courier New", Font.BOLD, 17));
            FL14.setForeground(Color.red);
            FL14.setName("FL14");
            xTitledPanel1ContentContainer.add(FL14);
            FL14.setBounds(805, 375, 60, 26);

            //---- OBJ_197 ----
            OBJ_197.setText("Libell\u00e9");
            OBJ_197.setFont(OBJ_197.getFont().deriveFont(OBJ_197.getFont().getStyle() | Font.BOLD));
            OBJ_197.setName("OBJ_197");
            xTitledPanel1ContentContainer.add(OBJ_197);
            OBJ_197.setBounds(240, 25, 43, 25);

            //---- OBJ_201 ----
            OBJ_201.setText("OZnn");
            OBJ_201.setFont(OBJ_201.getFont().deriveFont(OBJ_201.getFont().getStyle() | Font.BOLD));
            OBJ_201.setName("OBJ_201");
            xTitledPanel1ContentContainer.add(OBJ_201);
            OBJ_201.setBounds(750, 25, 36, 25);

            //---- OBJ_194 ----
            OBJ_194.setText("Tiers");
            OBJ_194.setFont(OBJ_194.getFont().deriveFont(OBJ_194.getFont().getStyle() | Font.BOLD));
            OBJ_194.setName("OBJ_194");
            xTitledPanel1ContentContainer.add(OBJ_194);
            OBJ_194.setBounds(95, 25, 34, 25);

            //---- OBJ_199 ----
            OBJ_199.setText("Sect");
            OBJ_199.setFont(OBJ_199.getFont().deriveFont(OBJ_199.getFont().getStyle() | Font.BOLD));
            OBJ_199.setName("OBJ_199");
            xTitledPanel1ContentContainer.add(OBJ_199);
            OBJ_199.setBounds(585, 25, 30, 25);

            //---- OBJ_198 ----
            OBJ_198.setText("Aff");
            OBJ_198.setFont(OBJ_198.getFont().deriveFont(OBJ_198.getFont().getStyle() | Font.BOLD));
            OBJ_198.setName("OBJ_198");
            xTitledPanel1ContentContainer.add(OBJ_198);
            OBJ_198.setBounds(515, 25, 18, 25);

            //---- OBJ_195 ----
            OBJ_195.setText("S");
            OBJ_195.setFont(OBJ_195.getFont().deriveFont(OBJ_195.getFont().getStyle() | Font.BOLD));
            OBJ_195.setName("OBJ_195");
            xTitledPanel1ContentContainer.add(OBJ_195);
            OBJ_195.setBounds(165, 25, 12, 25);

            //---- OBJ_196 ----
            OBJ_196.setText("C");
            OBJ_196.setFont(OBJ_196.getFont().deriveFont(OBJ_196.getFont().getStyle() | Font.BOLD));
            OBJ_196.setName("OBJ_196");
            xTitledPanel1ContentContainer.add(OBJ_196);
            OBJ_196.setBounds(215, 25, 12, 25);

            //---- TIE15 ----
            TIE15.setComponentPopupMenu(BTD);
            TIE15.setName("TIE15");
            xTitledPanel1ContentContainer.add(TIE15);
            TIE15.setBounds(90, 370, 60, TIE15.getPreferredSize().height);

            //---- TIE16 ----
            TIE16.setComponentPopupMenu(BTD);
            TIE16.setName("TIE16");
            xTitledPanel1ContentContainer.add(TIE16);
            TIE16.setBounds(90, 345, 60, TIE16.getPreferredSize().height);

            //---- TIE17 ----
            TIE17.setComponentPopupMenu(BTD);
            TIE17.setName("TIE17");
            xTitledPanel1ContentContainer.add(TIE17);
            TIE17.setBounds(90, 320, 60, TIE17.getPreferredSize().height);

            //---- TIE18 ----
            TIE18.setComponentPopupMenu(BTD);
            TIE18.setName("TIE18");
            xTitledPanel1ContentContainer.add(TIE18);
            TIE18.setBounds(90, 295, 60, TIE18.getPreferredSize().height);

            //---- TIE19 ----
            TIE19.setComponentPopupMenu(BTD);
            TIE19.setName("TIE19");
            xTitledPanel1ContentContainer.add(TIE19);
            TIE19.setBounds(90, 270, 60, TIE19.getPreferredSize().height);

            //---- TIE20 ----
            TIE20.setComponentPopupMenu(BTD);
            TIE20.setName("TIE20");
            xTitledPanel1ContentContainer.add(TIE20);
            TIE20.setBounds(90, 245, 60, TIE20.getPreferredSize().height);

            //---- TIE21 ----
            TIE21.setComponentPopupMenu(BTD);
            TIE21.setName("TIE21");
            xTitledPanel1ContentContainer.add(TIE21);
            TIE21.setBounds(90, 220, 60, TIE21.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(xTitledPanel1);
          xTitledPanel1.setBounds(13, 14, 880, 451);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_16 ----
      OBJ_16.setText("Duplication");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private XRiTextField INDETB;
  private JLabel OBJ_34;
  private JLabel OBJ_69;
  private XRiTextField INDCOD;
  private XRiTextField ETLIB;
  private XRiTextField ETJO;
  private JButton BTN_JO;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel2;
  private XRiTextField ETO1;
  private XRiTextField ETO2;
  private XRiTextField ETO3;
  private XRiTextField ETO4;
  private XRiTextField ETO5;
  private XRiTextField ETO6;
  private XRiTextField ETO7;
  private XRiTextField ETO8;
  private XRiTextField ETO9;
  private XRiTextField ETO10;
  private XRiTextField ETO11;
  private XRiTextField ETO12;
  private XRiTextField ETO13;
  private XRiTextField ETO14;
  private XRiTextField ETO15;
  private XRiTextField ETO16;
  private XRiTextField ETO17;
  private XRiTextField ETO18;
  private JLabel OBJ_218;
  private JLabel OBJ_219;
  private JLabel OBJ_204;
  private JLabel OBJ_208;
  private JLabel OBJ_234;
  private JLabel OBJ_235;
  private JLabel OBJ_237;
  private JLabel OBJ_236;
  private JLabel OBJ_223;
  private JLabel OBJ_222;
  private JLabel OBJ_205;
  private JLabel OBJ_209;
  private JLabel OBJ_233;
  private JLabel OBJ_232;
  private JLabel OBJ_238;
  private JLabel OBJ_239;
  private JLabel OBJ_206;
  private JLabel OBJ_210;
  private JLabel OBJ_225;
  private JLabel OBJ_224;
  private JLabel OBJ_230;
  private JLabel OBJ_231;
  private JLabel OBJ_240;
  private JLabel OBJ_241;
  private JLabel OBJ_226;
  private JLabel OBJ_227;
  private JLabel OBJ_211;
  private JLabel OBJ_207;
  private JLabel OBJ_229;
  private JLabel OBJ_228;
  private JLabel OBJ_220;
  private JLabel OBJ_221;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_193;
  private JLabel OBJ_200;
  private XRiTextField NCG1X;
  private XRiTextField TIE1;
  private XRiSpinner SNS1;
  private XRiTextField CLB1;
  private XRiTextField LIB1;
  private XRiTextField AFF01;
  private XRiTextField SAN1;
  private XRiTextField MTT1;
  private XRiTextField ZON1;
  private JLabel FL1;
  private XRiTextField NCG2X;
  private XRiTextField TIE2;
  private XRiSpinner SNS2;
  private XRiTextField CLB2;
  private XRiTextField LIB2;
  private XRiTextField AFF02;
  private XRiTextField SAN2;
  private XRiTextField MTT2;
  private XRiTextField ZON2;
  private JLabel FL2;
  private XRiTextField NCG3X;
  private XRiTextField TIE3;
  private XRiSpinner SNS3;
  private XRiTextField CLB3;
  private XRiTextField LIB3;
  private XRiTextField AFF03;
  private XRiTextField SAN3;
  private XRiTextField MTT3;
  private XRiTextField ZON3;
  private JLabel FL3;
  private XRiTextField NCG4X;
  private XRiTextField TIE4;
  private XRiSpinner SNS4;
  private XRiTextField CLB4;
  private XRiTextField LIB4;
  private XRiTextField AFF04;
  private XRiTextField SAN4;
  private XRiTextField MTT4;
  private XRiTextField ZON4;
  private JLabel FL4;
  private XRiTextField NCG5X;
  private XRiTextField TIE5;
  private XRiSpinner SNS5;
  private XRiTextField CLB5;
  private XRiTextField LIB5;
  private XRiTextField AFF05;
  private XRiTextField SAN5;
  private XRiTextField MTT5;
  private XRiTextField ZON5;
  private JLabel FL5;
  private XRiTextField NCG6X;
  private XRiTextField TIE6;
  private XRiSpinner SNS6;
  private XRiTextField CLB6;
  private XRiTextField LIB6;
  private XRiTextField AFF06;
  private XRiTextField SAN6;
  private XRiTextField MTT6;
  private XRiTextField ZON6;
  private JLabel FL6;
  private XRiTextField NCG7X;
  private XRiTextField TIE7;
  private XRiSpinner SNS7;
  private XRiTextField CLB7;
  private XRiTextField LIB7;
  private XRiTextField AFF07;
  private XRiTextField SAN7;
  private XRiTextField MTT7;
  private XRiTextField ZON7;
  private JLabel FL7;
  private XRiTextField NCG8X;
  private XRiTextField TIE8;
  private XRiSpinner SNS8;
  private XRiTextField CLB8;
  private XRiTextField LIB8;
  private XRiTextField AFF08;
  private XRiTextField SAN8;
  private XRiTextField MTT8;
  private XRiTextField ZON8;
  private JLabel FL8;
  private XRiTextField NCG9X;
  private XRiTextField TIE9;
  private XRiSpinner SNS9;
  private XRiTextField CLB9;
  private XRiTextField LIB9;
  private XRiTextField AFF09;
  private XRiTextField SAN9;
  private XRiTextField MTT9;
  private XRiTextField ZON9;
  private JLabel FL9;
  private XRiTextField NCG10X;
  private XRiTextField TIE10;
  private XRiSpinner SNS10;
  private XRiTextField CLB10;
  private XRiTextField LIB10;
  private XRiTextField AFF10;
  private XRiTextField SAN10;
  private XRiTextField MTT10;
  private XRiTextField ZON10;
  private JLabel FL10;
  private XRiTextField NCG11X;
  private XRiTextField TIE11;
  private XRiSpinner SNS11;
  private XRiTextField CLB11;
  private XRiTextField LIB11;
  private XRiTextField AFF11;
  private XRiTextField SAN11;
  private XRiTextField MTT11;
  private XRiTextField ZON11;
  private JLabel FL11;
  private XRiTextField NCG12X;
  private XRiTextField TIE12;
  private XRiSpinner SNS12;
  private XRiTextField CLB12;
  private XRiTextField LIB12;
  private XRiTextField AFF12;
  private XRiTextField SAN12;
  private XRiTextField MTT12;
  private XRiTextField ZON12;
  private JLabel FL12;
  private XRiTextField NCG13X;
  private XRiTextField TIE13;
  private XRiSpinner SNS13;
  private XRiTextField CLB13;
  private XRiTextField LIB13;
  private XRiTextField AFF13;
  private XRiTextField SAN13;
  private XRiTextField MTT13;
  private XRiTextField ZON13;
  private JLabel FL13;
  private XRiTextField NCG14X;
  private XRiTextField TIE14;
  private XRiSpinner SNS14;
  private XRiTextField CLB14;
  private XRiTextField LIB14;
  private XRiTextField AFF14;
  private XRiTextField SAN14;
  private XRiTextField MTT14;
  private XRiTextField ZON14;
  private JLabel FL14;
  private JLabel OBJ_197;
  private JLabel OBJ_201;
  private JLabel OBJ_194;
  private JLabel OBJ_199;
  private JLabel OBJ_198;
  private JLabel OBJ_195;
  private JLabel OBJ_196;
  private XRiTextField TIE15;
  private XRiTextField TIE16;
  private XRiTextField TIE17;
  private XRiTextField TIE18;
  private XRiTextField TIE19;
  private XRiTextField TIE20;
  private XRiTextField TIE21;
  private JPopupMenu BTD;
  private JMenuItem OBJ_16;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
