
package ri.serien.libecranrpg.vcgm.VCGM05FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.outil.font.CustomFont;

/**
 * @author Stéphane Vénéri
 */
public class VCGM05FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _LB01_Title = { "HLB01", };
  private String[][] _LB01_Data = { { "LB01", }, { "LB02", }, { "LB03", }, { "LB04", }, { "LB05", }, { "LB06", }, { "LB07", }, { "LB08", },
      { "LB09", }, { "LB10", }, { "LB11", }, { "LB12", }, { "LB13", }, { "LB14", }, { "LB15", }, { "LB16", }, { "LB17", }, { "LB18", }, };
  private int[] _LB01_Width = { 658, };
  // private String[] _LIST_Top=null;
  // private String[][] _LIST_Title_Data_Brut=null;
  private int[] _LIST_Justification =
      { SwingConstants.LEFT, SwingConstants.RIGHT, SwingConstants.RIGHT, SwingConstants.RIGHT, SwingConstants.RIGHT };
  private Color[][] _LIST_Text_Color = new Color[18][1];
  private Color[][] _LIST_Fond_Color = null;
  private Color bleu = new Color(139, 141, 234);
  private Font[][] police = new Font[18][1];
  private Font normal = CustomFont.loadFont("fonts/VeraMono.ttf", 12);// new Font("Courier New", Font.PLAIN, 12);
  private Font gras = CustomFont.loadFont("fonts/VeraMoBd.ttf", 12); // new Font("Courier New", Font.BOLD, 12);
  // private boolean constructionIsFinie = false;
  
  public VCGM05FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    // LB01.getTableHeader().setFont(new Font("Courier New", Font.BOLD, 12));
    // ((DefaultTableCellRenderer)LB01.getTableHeader().getDefaultRenderer()).setHorizontalAlignment( JLabel.LEFT );
    // _LIST_Title_Data_Brut = initTable(LIST);
    
    // Ajout
    initDiverses();
    LB01.setAspectTable(null, _LB01_Title, _LB01_Data, _LB01_Width, false, _LIST_Justification, _LIST_Text_Color, _LIST_Fond_Color, police);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    xTitledPanel1.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBSIM@")).trim());
    OBJ_73.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CPSENS@")).trim());
    OBJ_74.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDEV@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // Couleurs de la liste (suivant indicateurs de 20 à 37)
    for (int i = 0; i < 18; i++) {
      
      if (lexique.isTrue(String.valueOf(i + 20))) {
        _LIST_Text_Color[i][0] = bleu;
        police[i][0] = gras;
      }
      else {
        _LIST_Text_Color[i][0] = Color.BLACK;
        police[i][0] = normal;
      }
      
    }
    
    // Gestion du titre de la table
    /*_LIST_Title_Data_Brut[0][0] = "Période            |    Débit     |     Crédit   | Solde Période| Solde Cumulé|";*/
    // Ajoute à la liste des oData les variables non liées directement à un composant graphique
    // majTable( LIST, _LIST_Title_Data_Brut, _LIST_Top, _LIST_Justification, _LIST_Text_Color, _LIST_Fond_Color,
    // police);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    OBJ_73.setVisible(lexique.isPresent("CPSENS"));
    riSousMenu9.setEnabled(!lexique.isTrue("41"));
    riSousMenu14.setEnabled(interpreteurD.analyseExpression("@V01F@").equalsIgnoreCase("INTERRO."));
    riSousMenu6.setEnabled(interpreteurD.analyseExpression("@V01F@").equalsIgnoreCase("INTERRO."));
    CPSOPD.setVisible(lexique.isPresent("CPSOPD"));
    
    String ncg = "";
    
    ncg = lexique.HostFieldGetData("INDNCX");
    if ((ncg != null) && (ncg.length() > 0)) {
      riSousMenu11.setEnabled(ncg.charAt(0) == '5');
      OBJ_23.setVisible(ncg.charAt(0) == '6');
    }
    
    // Titre
    setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("Fiche compte général")));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/stats.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", true);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F17", true);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19", true);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", true);
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", true);
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23", true);
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21", true);
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, "CPT");
    lexique.HostScreenSendKey(this, "ENTER", true);
    lexique.HostFieldPutData("V06F", 0, "GRA");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut((LB01.getSelectedRow() + 4), 2);
    lexique.HostScreenSendKey(this, "F7", true);
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    G1LIB = new XRiTextField();
    OBJ_71 = new JLabel();
    CPSOPD = new XRiTextField();
    OBJ_73 = new JLabel();
    OBJ_74 = new JLabel();
    SCROLLPANE_LIST = new JScrollPane();
    LB01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    barre_tete = new JMenuBar();
    P_Infos = new JPanel();
    OBJ_41 = new JLabel();
    INDNCX = new XRiTextField();
    OBJ_40 = new JLabel();
    INDETB = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_23 = new JMenuItem();
    OBJ_24 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1100, 480));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Visualisation comptes");
            riSousMenu_bt6.setToolTipText("Visualisation des comptes");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Analyse budg\u00e9taire");
            riSousMenu_bt7.setToolTipText("Visualisation  analyse budg\u00e9taire du compte");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);

          //======== riSousMenu8 ========
          {
            riSousMenu8.setName("riSousMenu8");

            //---- riSousMenu_bt8 ----
            riSousMenu_bt8.setText("Affichage en devise");
            riSousMenu_bt8.setToolTipText("Affichage en devise sur option CPT ou SIM");
            riSousMenu_bt8.setName("riSousMenu_bt8");
            riSousMenu_bt8.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt8ActionPerformed(e);
              }
            });
            riSousMenu8.add(riSousMenu_bt8);
          }
          menus_haut.add(riSousMenu8);

          //======== riSousMenu9 ========
          {
            riSousMenu9.setName("riSousMenu9");

            //---- riSousMenu_bt9 ----
            riSousMenu_bt9.setText("Fiche abonnement");
            riSousMenu_bt9.setToolTipText("Fiche abonnement");
            riSousMenu_bt9.setName("riSousMenu_bt9");
            riSousMenu_bt9.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt9ActionPerformed(e);
              }
            });
            riSousMenu9.add(riSousMenu_bt9);
          }
          menus_haut.add(riSousMenu9);

          //======== riSousMenu10 ========
          {
            riSousMenu10.setName("riSousMenu10");

            //---- riSousMenu_bt10 ----
            riSousMenu_bt10.setText("Fiche de r\u00e9vision");
            riSousMenu_bt10.setToolTipText("Fiche de r\u00e9vision");
            riSousMenu_bt10.setName("riSousMenu_bt10");
            riSousMenu_bt10.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt10ActionPerformed(e);
              }
            });
            riSousMenu10.add(riSousMenu_bt10);
          }
          menus_haut.add(riSousMenu10);

          //======== riSousMenu11 ========
          {
            riSousMenu11.setName("riSousMenu11");

            //---- riSousMenu_bt11 ----
            riSousMenu_bt11.setText("Soldes en date de valeur");
            riSousMenu_bt11.setToolTipText("Soldes en date de valeur (Pour les comptes de classe 5)");
            riSousMenu_bt11.setName("riSousMenu_bt11");
            riSousMenu_bt11.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt11ActionPerformed(e);
              }
            });
            riSousMenu11.add(riSousMenu_bt11);
          }
          menus_haut.add(riSousMenu11);

          //======== riMenu3 ========
          {
            riMenu3.setName("riMenu3");

            //---- riMenu_bt3 ----
            riMenu_bt3.setText("Statistiques");
            riMenu_bt3.setName("riMenu_bt3");
            riMenu3.add(riMenu_bt3);
          }
          menus_haut.add(riMenu3);

          //======== riSousMenu14 ========
          {
            riSousMenu14.setName("riSousMenu14");

            //---- riSousMenu_bt14 ----
            riSousMenu_bt14.setText("Analyse des comptes");
            riSousMenu_bt14.setToolTipText("Analyse des comptes (Histogramme)");
            riSousMenu_bt14.setName("riSousMenu_bt14");
            riSousMenu_bt14.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt14ActionPerformed(e);
              }
            });
            riSousMenu14.add(riSousMenu_bt14);
          }
          menus_haut.add(riSousMenu14);

          //======== riSousMenu15 ========
          {
            riSousMenu15.setName("riSousMenu15");

            //---- riSousMenu_bt15 ----
            riSousMenu_bt15.setText("Evolution du solde");
            riSousMenu_bt15.setSelectedIcon(null);
            riSousMenu_bt15.setToolTipText("Evolution du solde sur ce compte (graphes statistiques)");
            riSousMenu_bt15.setName("riSousMenu_bt15");
            riSousMenu_bt15.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt15ActionPerformed(e);
              }
            });
            riSousMenu15.add(riSousMenu_bt15);
          }
          menus_haut.add(riSousMenu15);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== xTitledPanel1 ========
        {
          xTitledPanel1.setTitle("@LIBSIM@");
          xTitledPanel1.setBorder(new DropShadowBorder());
          xTitledPanel1.setName("xTitledPanel1");
          Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

          //---- G1LIB ----
          G1LIB.setFont(G1LIB.getFont().deriveFont(G1LIB.getFont().getStyle() | Font.BOLD, G1LIB.getFont().getSize() + 1f));
          G1LIB.setName("G1LIB");

          //---- OBJ_71 ----
          OBJ_71.setText("Solde");
          OBJ_71.setName("OBJ_71");

          //---- CPSOPD ----
          CPSOPD.setComponentPopupMenu(BTD);
          CPSOPD.setHorizontalAlignment(SwingConstants.RIGHT);
          CPSOPD.setName("CPSOPD");

          //---- OBJ_73 ----
          OBJ_73.setText("@CPSENS@");
          OBJ_73.setName("OBJ_73");

          //---- OBJ_74 ----
          OBJ_74.setText("@WDEV@");
          OBJ_74.setName("OBJ_74");

          GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
          xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
          xTitledPanel1ContentContainerLayout.setHorizontalGroup(
            xTitledPanel1ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(G1LIB, GroupLayout.PREFERRED_SIZE, 335, GroupLayout.PREFERRED_SIZE)
                .addGap(85, 85, 85)
                .addComponent(OBJ_71, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(CPSOPD, GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(OBJ_74, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE))
          );
          xTitledPanel1ContentContainerLayout.setVerticalGroup(
            xTitledPanel1ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(G1LIB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(OBJ_71, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(CPSOPD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(OBJ_74, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_contenu.add(xTitledPanel1);
        xTitledPanel1.setBounds(10, 10, 905, 85);

        //======== SCROLLPANE_LIST ========
        {
          SCROLLPANE_LIST.setComponentPopupMenu(BTD);
          SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

          //---- LB01 ----
          LB01.setComponentPopupMenu(BTD);
          LB01.setName("LB01");
          SCROLLPANE_LIST.setViewportView(LB01);
        }
        p_contenu.add(SCROLLPANE_LIST);
        SCROLLPANE_LIST.setBounds(15, 105, 865, 317);

        //---- BT_PGUP ----
        BT_PGUP.setText("");
        BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGUP.setName("BT_PGUP");
        p_contenu.add(BT_PGUP);
        BT_PGUP.setBounds(890, 105, 25, 150);

        //---- BT_PGDOWN ----
        BT_PGDOWN.setText("");
        BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGDOWN.setName("BT_PGDOWN");
        p_contenu.add(BT_PGDOWN);
        BT_PGDOWN.setBounds(890, 272, 25, 150);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== barre_tete ========
    {
      barre_tete.setPreferredSize(new Dimension(111, 35));
      barre_tete.setName("barre_tete");

      //======== P_Infos ========
      {
        P_Infos.setBorder(null);
        P_Infos.setMinimumSize(new Dimension(100, 40));
        P_Infos.setPreferredSize(new Dimension(100, 40));
        P_Infos.setOpaque(false);
        P_Infos.setName("P_Infos");

        //---- OBJ_41 ----
        OBJ_41.setText("Num\u00e9ro de compte");
        OBJ_41.setName("OBJ_41");

        //---- INDNCX ----
        INDNCX.setComponentPopupMenu(BTD);
        INDNCX.setName("INDNCX");

        //---- OBJ_40 ----
        OBJ_40.setText("Soci\u00e9t\u00e9");
        OBJ_40.setName("OBJ_40");

        //---- INDETB ----
        INDETB.setComponentPopupMenu(BTD);
        INDETB.setName("INDETB");

        GroupLayout P_InfosLayout = new GroupLayout(P_Infos);
        P_Infos.setLayout(P_InfosLayout);
        P_InfosLayout.setHorizontalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(3, 3, 3)
              .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
              .addGap(0, 0, 0)
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
              .addGap(35, 35, 35)
              .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
              .addGap(0, 0, 0)
              .addComponent(INDNCX, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
        );
        P_InfosLayout.setVerticalGroup(
          P_InfosLayout.createParallelGroup()
            .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addComponent(INDNCX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(6, 6, 6)
              .addGroup(P_InfosLayout.createParallelGroup()
                .addComponent(OBJ_40)
                .addComponent(OBJ_41)))
        );
      }
      barre_tete.add(P_Infos);
    }
    add(barre_tete, BorderLayout.NORTH);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_23 ----
      OBJ_23.setText("Ventilation par types de frais");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD.add(OBJ_23);

      //---- OBJ_24 ----
      OBJ_24.setText("Aide en ligne");
      OBJ_24.setName("OBJ_24");
      OBJ_24.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_24ActionPerformed(e);
        }
      });
      BTD.add(OBJ_24);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private XRiTextField G1LIB;
  private JLabel OBJ_71;
  private XRiTextField CPSOPD;
  private JLabel OBJ_73;
  private JLabel OBJ_74;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable LB01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JMenuBar barre_tete;
  private JPanel P_Infos;
  private JLabel OBJ_41;
  private XRiTextField INDNCX;
  private JLabel OBJ_40;
  private XRiTextField INDETB;
  private JPopupMenu BTD;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_24;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
