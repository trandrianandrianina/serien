
package ri.serien.libecranrpg.vcgm.VCGM12FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VCGM12FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "HLD01", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 541, };
  // private String[][] _LIST_Title_Data_Brut=null;
  
  public VCGM12FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    // WTP01.getTableHeader().setFont(new Font("Courier New", Font.BOLD, 12));
    // ((DefaultTableCellRenderer)WTP01.getTableHeader().getDefaultRenderer()).setHorizontalAlignment( JLabel.LEFT );
    // _LIST_Title_Data_Brut = initTable(LIST);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, _LIST_Title_Data_Brut, _WTP01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    INDETB.setEnabled(lexique.isPresent("INDETB"));
    WSAN.setEnabled(lexique.isPresent("WSAN"));
    NCGX.setEnabled(lexique.isPresent("NCGX"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "1", "Enter");
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _WTP01_Top, "1", "Enter", e);
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    barre_tete = new JMenuBar();
    lb_a_virer = new JLabel();
    panel1 = new JPanel();
    OBJ_53 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_54 = new JLabel();
    NCGX = new XRiTextField();
    OBJ_61 = new JLabel();
    WSAN = new XRiTextField();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1040, 460));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setPreferredSize(new Dimension(1035, 460));
        p_contenu.setName("p_contenu");

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("R\u00e9sultat de la recherche"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //======== SCROLLPANE_LIST ========
          {
            SCROLLPANE_LIST.setComponentPopupMenu(BTD);
            SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

            //---- WTP01 ----
            WTP01.setComponentPopupMenu(BTD);
            WTP01.setName("WTP01");
            WTP01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                WTP01MouseClicked(e);
              }
            });
            SCROLLPANE_LIST.setViewportView(WTP01);
          }
          panel2.add(SCROLLPANE_LIST);
          SCROLLPANE_LIST.setBounds(38, 54, 715, 270);

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          panel2.add(BT_PGUP);
          BT_PGUP.setBounds(760, 55, 25, 125);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          panel2.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(760, 200, 25, 125);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(25, 25, 25)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 820, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(30, 30, 30)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 365, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== barre_tete ========
    {
      barre_tete.setPreferredSize(new Dimension(111, 30));
      barre_tete.setName("barre_tete");

      //---- lb_a_virer ----
      lb_a_virer.setForeground(new Color(255, 51, 51));
      lb_a_virer.setName("lb_a_virer");
      barre_tete.add(lb_a_virer);

      //======== panel1 ========
      {
        panel1.setOpaque(false);
        panel1.setPreferredSize(new Dimension(600, 27));
        panel1.setMaximumSize(new Dimension(600, 27));
        panel1.setMinimumSize(new Dimension(600, 27));
        panel1.setName("panel1");

        //---- OBJ_53 ----
        OBJ_53.setText("Soci\u00e9t\u00e9");
        OBJ_53.setName("OBJ_53");

        //---- INDETB ----
        INDETB.setComponentPopupMenu(BTD);
        INDETB.setName("INDETB");

        //---- OBJ_54 ----
        OBJ_54.setText("Num\u00e9ro de compte");
        OBJ_54.setName("OBJ_54");

        //---- NCGX ----
        NCGX.setComponentPopupMenu(BTD);
        NCGX.setName("NCGX");

        //---- OBJ_61 ----
        OBJ_61.setText("Section");
        OBJ_61.setName("OBJ_61");

        //---- WSAN ----
        WSAN.setComponentPopupMenu(BTD);
        WSAN.setName("WSAN");

        GroupLayout panel1Layout = new GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
          panel1Layout.createParallelGroup()
            .addGroup(panel1Layout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addGroup(panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(55, 55, 55)
                  .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 59, GroupLayout.PREFERRED_SIZE))
              .addGap(30, 30, 30)
              .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
              .addComponent(NCGX, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
              .addGap(30, 30, 30)
              .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
              .addComponent(WSAN, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
        );
        panel1Layout.setVerticalGroup(
          panel1Layout.createParallelGroup()
            .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
            .addGroup(panel1Layout.createSequentialGroup()
              .addGap(3, 3, 3)
              .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addGroup(panel1Layout.createSequentialGroup()
              .addGap(3, 3, 3)
              .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addComponent(NCGX, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
            .addGroup(panel1Layout.createSequentialGroup()
              .addGap(3, 3, 3)
              .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addComponent(WSAN, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
        );
      }
      barre_tete.add(panel1);
    }
    add(barre_tete, BorderLayout.NORTH);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel2;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JMenuBar barre_tete;
  private JLabel lb_a_virer;
  private JPanel panel1;
  private JLabel OBJ_53;
  private XRiTextField INDETB;
  private JLabel OBJ_54;
  private XRiTextField NCGX;
  private JLabel OBJ_61;
  private XRiTextField WSAN;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
