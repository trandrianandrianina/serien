
package ri.serien.libecranrpg.vcgm.VCGM11FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM11FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] WNCPT_Value = { "", "0", "1", "2", "3", };
  private String[] E1SSR_Value = { "", "C", "D", "S" };
  
  public VCGM11FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WNCPT.setValeurs(WNCPT_Value, null);
    E1SSR.setValeurs(E1SSR_Value, null);
    E1TRV.setValeursSelection("1", " ");
    E1TIR.setValeursSelection("X", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WISOC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WISOC@")).trim());
    WICJO.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WICJO@")).trim());
    WICFO.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WICFO@")).trim());
    WIDTEX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WIDTEX@")).trim());
    WDTFRA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDTFRA@")).trim());
    xTS_LIBMTT.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMTT@")).trim());
    l_WTMT1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT01@")).trim());
    l_WTMT3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT03@")).trim());
    l_WTMT5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT05@")).trim());
    l_WTMT7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT07@")).trim());
    l_WTMT9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT09@")).trim());
    l_WTMT11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT11@")).trim());
    l_WTMT13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT13@")).trim());
    l_WTMT15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT15@")).trim());
    l_WTMT17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT17@")).trim());
    l_WTMT19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT19@")).trim());
    l_WTMT21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT21@")).trim());
    l_WTMT23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT23@")).trim());
    l_WTMT25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT25@")).trim());
    l_WTMT27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT27@")).trim());
    l_WTMT29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT29@")).trim());
    l_WTMT31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT31@")).trim());
    xTS_LIBMTT2.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMT2@")).trim());
    l_WTMT2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT02@")).trim());
    l_WTMT4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT04@")).trim());
    l_WTMT6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT06@")).trim());
    l_WTMT8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT08@")).trim());
    l_WTMT10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT10@")).trim());
    l_WTMT12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT12@")).trim());
    l_WTMT14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT14@")).trim());
    l_WTMT16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT16@")).trim());
    l_WTMT18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT18@")).trim());
    l_WTMT20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT20@")).trim());
    l_WTMT22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT22@")).trim());
    l_WTMT24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT24@")).trim());
    l_WTMT26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT26@")).trim());
    l_WTMT28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT28@")).trim());
    l_WTMT30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT30@")).trim());
    l_WTMT32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTMT32@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    

    
    


    
    
    // E1CLR.setEnabled( lexique.isPresent("E1CLR"));
    WICJO.setVisible(lexique.isPresent("WICJO"));
    WICFO.setVisible(lexique.isPresent("WICFO"));
    WISOC.setVisible(lexique.isPresent("WISOC"));
    // E1NCRX.setEnabled( lexique.isPresent("E1NCRX"));
    // WNCP.setEnabled( lexique.isPresent("WNCP"));
    WIDTEX.setVisible(lexique.isPresent("WIDTEX"));
    // E1TRV.setVisible(lexique.isTrue("N50"));
    // E1TRV.setEnabled( lexique.isPresent("E1TRV"));
    // E1TRV.setSelected(lexique.HostFieldGetData("E1TRV").equalsIgnoreCase("1"));
    // E1LBR1.setVisible(lexique.isTrue("94"));
    // WNCPT.setEnabled( lexique.isPresent("WNCPT"));
    WDTFRA.setVisible(lexique.isPresent("WDTFRA"));
    // OBJ_66.setVisible(interpreteurD.analyseExpression("@TST01@").equalsIgnoreCase("JWMTR"));
    // OBJ_65.setVisible(interpreteurD.analyseExpression("@TST01@").equalsIgnoreCase("JWMTR"));
    // E1LBR2.setVisible(lexique.isTrue("94"));
    // E1SSR.setEnabled( lexique.isPresent("E1SSR"));
    // E1TIR.setVisible( lexique.isPresent("E1TIR"));
    // E1TIR.setSelected(lexique.HostFieldGetData("E1TIR").equalsIgnoreCase("X"));
    // E1LBR.setVisible(lexique.isTrue("N94"));
    OBJ_52.setVisible(lexique.isTrue("94"));
    if (lexique.isTrue("94")) {
      label1.setText("Référence facture");
    }
    else {
      label1.setText("Libellé");
    }
    
    p_Totalisateurs.setVisible(!lexique.isTrue("69"));
    if (p_Totalisateurs.isVisible()) {
      p_contenu.setPreferredSize(new Dimension(942, 580));
    }
    else {
      p_contenu.setPreferredSize(new Dimension(942, 170));
    }
    xTS_LIBMTT.setVisible(lexique.isTrue("(N85)"));
    xTS_LIBMTT2.setVisible(lexique.isTrue("(N85)"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @WLIBA1@ @LIBPG@"));
    
    

    
    p_bpresentation.setCodeEtablissement(WISOC.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WISOC.getText()));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (E1TRV.isSelected())
    // lexique.HostFieldPutData("E1TRV", 0, "1");
    // else
    // lexique.HostFieldPutData("E1TRV", 0, " ");
    // lexique.HostFieldPutData("WNCPT", 0, WNCPT_Value[WNCPT.getSelectedIndex()]);
    // lexique.HostFieldPutData("E1SSR", 0, E1SSR_Value[E1SSR.getSelectedIndex()]);
    // if (E1TIR.isSelected())
    // lexique.HostFieldPutData("E1TIR", 0, "X");
    // else
    // lexique.HostFieldPutData("E1TIR", 0, " ");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_49 = new JLabel();
    WISOC = new RiZoneSortie();
    OBJ_56 = new JLabel();
    WICJO = new RiZoneSortie();
    OBJ_57 = new JLabel();
    WICFO = new RiZoneSortie();
    OBJ_58 = new JLabel();
    WIDTEX = new RiZoneSortie();
    WDTFRA = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    E1LBR = new XRiTextField();
    E1TIR = new XRiCheckBox();
    E1SSR = new XRiComboBox();
    E1LBR2 = new XRiTextField();
    WNCPT = new XRiComboBox();
    E1LBR1 = new XRiTextField();
    E1TRV = new XRiCheckBox();
    OBJ_52 = new JLabel();
    WNCP = new XRiTextField();
    E1NCRX = new XRiTextField();
    OBJ_48_OBJ_48 = new JLabel();
    OBJ_53_OBJ_53 = new JLabel();
    OBJ_49_OBJ_49 = new JLabel();
    E1CLR = new XRiTextField();
    OBJ_50_OBJ_50 = new JLabel();
    OBJ_54_OBJ_54 = new JLabel();
    label1 = new JLabel();
    p_Totalisateurs = new JPanel();
    p_Totalisateur1 = new JPanel();
    xTS_LIBMTT = new JXTitledSeparator();
    xTS_Compte = new JXTitledSeparator();
    xTS_Sens = new JXTitledSeparator();
    xTS_Section = new JXTitledSeparator();
    xTS_Libelle = new JXTitledSeparator();
    l_WTMT1 = new RiZoneSortie();
    WTNC01 = new XRiTextField();
    WTSS01 = new XRiTextField();
    WTSA01 = new XRiTextField();
    WTLI01 = new XRiTextField();
    l_WTMT3 = new RiZoneSortie();
    WTNC03 = new XRiTextField();
    WTSS03 = new XRiTextField();
    WTSA03 = new XRiTextField();
    WTLI03 = new XRiTextField();
    l_WTMT5 = new RiZoneSortie();
    WTNC05 = new XRiTextField();
    WTSS05 = new XRiTextField();
    WTSA05 = new XRiTextField();
    WTLI05 = new XRiTextField();
    l_WTMT7 = new RiZoneSortie();
    WTNC07 = new XRiTextField();
    WTSS07 = new XRiTextField();
    WTSA07 = new XRiTextField();
    WTLI07 = new XRiTextField();
    l_WTMT9 = new RiZoneSortie();
    WTNC09 = new XRiTextField();
    WTSS09 = new XRiTextField();
    WTSA09 = new XRiTextField();
    WTLI09 = new XRiTextField();
    l_WTMT11 = new RiZoneSortie();
    WTNC11 = new XRiTextField();
    WTSS11 = new XRiTextField();
    WTSA11 = new XRiTextField();
    WTLI11 = new XRiTextField();
    l_WTMT13 = new RiZoneSortie();
    WTNC13 = new XRiTextField();
    WTSS13 = new XRiTextField();
    WTSA13 = new XRiTextField();
    WTLI13 = new XRiTextField();
    l_WTMT15 = new RiZoneSortie();
    WTNC15 = new XRiTextField();
    WTSS15 = new XRiTextField();
    WTSA15 = new XRiTextField();
    WTLI15 = new XRiTextField();
    l_WTMT17 = new RiZoneSortie();
    WTNC17 = new XRiTextField();
    WTSS17 = new XRiTextField();
    WTSA17 = new XRiTextField();
    WTLI17 = new XRiTextField();
    l_WTMT19 = new RiZoneSortie();
    WTNC19 = new XRiTextField();
    WTSS19 = new XRiTextField();
    WTSA19 = new XRiTextField();
    WTLI19 = new XRiTextField();
    l_WTMT21 = new RiZoneSortie();
    WTNC21 = new XRiTextField();
    WTSS21 = new XRiTextField();
    WTSA21 = new XRiTextField();
    WTLI21 = new XRiTextField();
    l_WTMT23 = new RiZoneSortie();
    WTNC23 = new XRiTextField();
    WTSS23 = new XRiTextField();
    WTSA23 = new XRiTextField();
    WTLI23 = new XRiTextField();
    l_WTMT25 = new RiZoneSortie();
    WTNC25 = new XRiTextField();
    WTSS25 = new XRiTextField();
    WTSA25 = new XRiTextField();
    WTLI25 = new XRiTextField();
    l_WTMT27 = new RiZoneSortie();
    WTNC27 = new XRiTextField();
    WTSS27 = new XRiTextField();
    WTSA27 = new XRiTextField();
    WTLI27 = new XRiTextField();
    l_WTMT29 = new RiZoneSortie();
    WTNC29 = new XRiTextField();
    WTSS29 = new XRiTextField();
    WTSA29 = new XRiTextField();
    WTLI29 = new XRiTextField();
    l_WTMT31 = new RiZoneSortie();
    WTNC31 = new XRiTextField();
    WTSS31 = new XRiTextField();
    WTSA31 = new XRiTextField();
    WTLI31 = new XRiTextField();
    p_Totalisateur2 = new JPanel();
    xTS_LIBMTT2 = new JXTitledSeparator();
    xTS_Compte2 = new JXTitledSeparator();
    xTS_Sens2 = new JXTitledSeparator();
    xTS_Section2 = new JXTitledSeparator();
    xTS_Libelle2 = new JXTitledSeparator();
    l_WTMT2 = new RiZoneSortie();
    WTNC02 = new XRiTextField();
    WTSS02 = new XRiTextField();
    WTSA02 = new XRiTextField();
    WTLI02 = new XRiTextField();
    l_WTMT4 = new RiZoneSortie();
    WTNC04 = new XRiTextField();
    WTSS04 = new XRiTextField();
    WTSA04 = new XRiTextField();
    WTLI04 = new XRiTextField();
    l_WTMT6 = new RiZoneSortie();
    WTNC06 = new XRiTextField();
    WTSS06 = new XRiTextField();
    WTSA06 = new XRiTextField();
    WTLI06 = new XRiTextField();
    l_WTMT8 = new RiZoneSortie();
    WTNC08 = new XRiTextField();
    WTSS08 = new XRiTextField();
    WTSA08 = new XRiTextField();
    WTLI08 = new XRiTextField();
    l_WTMT10 = new RiZoneSortie();
    WTNC10 = new XRiTextField();
    WTSS10 = new XRiTextField();
    WTSA10 = new XRiTextField();
    WTLI10 = new XRiTextField();
    l_WTMT12 = new RiZoneSortie();
    WTNC12 = new XRiTextField();
    WTSS12 = new XRiTextField();
    WTSA12 = new XRiTextField();
    WTLI12 = new XRiTextField();
    l_WTMT14 = new RiZoneSortie();
    WTNC14 = new XRiTextField();
    WTSS14 = new XRiTextField();
    WTSA14 = new XRiTextField();
    WTLI14 = new XRiTextField();
    l_WTMT16 = new RiZoneSortie();
    WTNC16 = new XRiTextField();
    WTSS16 = new XRiTextField();
    WTSA16 = new XRiTextField();
    WTLI16 = new XRiTextField();
    l_WTMT18 = new RiZoneSortie();
    WTNC18 = new XRiTextField();
    WTSS18 = new XRiTextField();
    WTSA18 = new XRiTextField();
    WTLI18 = new XRiTextField();
    l_WTMT20 = new RiZoneSortie();
    WTNC20 = new XRiTextField();
    WTSS20 = new XRiTextField();
    WTSA20 = new XRiTextField();
    WTLI20 = new XRiTextField();
    l_WTMT22 = new RiZoneSortie();
    WTNC22 = new XRiTextField();
    WTSS22 = new XRiTextField();
    WTSA22 = new XRiTextField();
    WTLI22 = new XRiTextField();
    l_WTMT24 = new RiZoneSortie();
    WTNC24 = new XRiTextField();
    WTSS24 = new XRiTextField();
    WTSA24 = new XRiTextField();
    WTLI24 = new XRiTextField();
    l_WTMT26 = new RiZoneSortie();
    WTNC26 = new XRiTextField();
    WTSS26 = new XRiTextField();
    WTSA26 = new XRiTextField();
    WTLI26 = new XRiTextField();
    l_WTMT28 = new RiZoneSortie();
    WTNC28 = new XRiTextField();
    WTSS28 = new XRiTextField();
    WTSA28 = new XRiTextField();
    WTLI28 = new XRiTextField();
    l_WTMT30 = new RiZoneSortie();
    WTNC30 = new XRiTextField();
    WTSS30 = new XRiTextField();
    WTSA30 = new XRiTextField();
    WTLI30 = new XRiTextField();
    l_WTMT32 = new RiZoneSortie();
    WTNC32 = new XRiTextField();
    WTSS32 = new XRiTextField();
    WTSA32 = new XRiTextField();
    WTLI32 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1142, 680));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Folio de comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 40));
          p_tete_gauche.setMinimumSize(new Dimension(900, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_49 ----
          OBJ_49.setText("Soci\u00e9t\u00e9");
          OBJ_49.setName("OBJ_49");

          //---- WISOC ----
          WISOC.setComponentPopupMenu(null);
          WISOC.setOpaque(false);
          WISOC.setText("@WISOC@");
          WISOC.setName("WISOC");

          //---- OBJ_56 ----
          OBJ_56.setText("Journal");
          OBJ_56.setName("OBJ_56");

          //---- WICJO ----
          WICJO.setComponentPopupMenu(null);
          WICJO.setOpaque(false);
          WICJO.setText("@WICJO@");
          WICJO.setName("WICJO");

          //---- OBJ_57 ----
          OBJ_57.setText("Folio");
          OBJ_57.setName("OBJ_57");

          //---- WICFO ----
          WICFO.setComponentPopupMenu(null);
          WICFO.setOpaque(false);
          WICFO.setText("@WICFO@");
          WICFO.setName("WICFO");

          //---- OBJ_58 ----
          OBJ_58.setText("Date");
          OBJ_58.setName("OBJ_58");

          //---- WIDTEX ----
          WIDTEX.setOpaque(false);
          WIDTEX.setText("@WIDTEX@");
          WIDTEX.setComponentPopupMenu(null);
          WIDTEX.setHorizontalAlignment(SwingConstants.CENTER);
          WIDTEX.setName("WIDTEX");

          //---- WDTFRA ----
          WDTFRA.setText("@WDTFRA@");
          WDTFRA.setOpaque(false);
          WDTFRA.setComponentPopupMenu(null);
          WDTFRA.setName("WDTFRA");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(WISOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(WICJO, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21)
                .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(WICFO, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(WIDTEX, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(WDTFRA, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(WISOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WICJO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WICFO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WIDTEX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WDTFRA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(950, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(942, 580));
          p_contenu.setName("p_contenu");

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Zones \u00e0 r\u00e9percuter"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- E1LBR ----
            E1LBR.setComponentPopupMenu(null);
            E1LBR.setName("E1LBR");
            panel2.add(E1LBR);
            E1LBR.setBounds(193, 65, 220, E1LBR.getPreferredSize().height);

            //---- E1TIR ----
            E1TIR.setText("R\u00e9percussion nom du tiers");
            E1TIR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            E1TIR.setName("E1TIR");
            panel2.add(E1TIR);
            E1TIR.setBounds(38, 105, 184, 20);

            //---- E1SSR ----
            E1SSR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            E1SSR.setName("E1SSR");
            panel2.add(E1SSR);
            E1SSR.setBounds(108, 66, 45, E1SSR.getPreferredSize().height);

            //---- E1LBR2 ----
            E1LBR2.setComponentPopupMenu(BTD);
            E1LBR2.setName("E1LBR2");
            panel2.add(E1LBR2);
            E1LBR2.setBounds(433, 65, 140, E1LBR2.getPreferredSize().height);

            //---- WNCPT ----
            WNCPT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WNCPT.setToolTipText("<html>\"0\" = Seul le Num\u00e9ro de contrepartie est r\u00e9percut\u00e9 sur la ligne,<br/> \"1\" = La ligne suivante de contrepartie est propos\u00e9e automatiquement avec le montant,<br/> le code libell\u00e9, et le Num\u00e9ro de Pi\u00e8ce,<br/> \"2\" = La ligne suivante de contrepartie est g\u00e9n\u00e9r\u00e9e automatiquement avec le montant, le code libell\u00e9, et le num\u00e9ro de pi\u00e8ce.<br/> \"3\" = Le Solde du Folio est automatique avec le Compte de Contrepartie.</html>");
            WNCPT.setName("WNCPT");
            panel2.add(WNCPT);
            WNCPT.setBounds(793, 66, 46, WNCPT.getPreferredSize().height);

            //---- E1LBR1 ----
            E1LBR1.setComponentPopupMenu(null);
            E1LBR1.setName("E1LBR1");
            panel2.add(E1LBR1);
            E1LBR1.setBounds(193, 65, 110, E1LBR1.getPreferredSize().height);

            //---- E1TRV ----
            E1TRV.setText("Ventilation");
            E1TRV.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            E1TRV.setName("E1TRV");
            panel2.add(E1TRV);
            E1TRV.setBounds(593, 69, 82, 20);

            //---- OBJ_52 ----
            OBJ_52.setText("Libell\u00e9");
            OBJ_52.setName("OBJ_52");
            panel2.add(OBJ_52);
            OBJ_52.setBounds(435, 40, 61, 20);

            //---- WNCP ----
            WNCP.setComponentPopupMenu(BTD);
            WNCP.setName("WNCP");
            panel2.add(WNCP);
            WNCP.setBounds(698, 65, 71, WNCP.getPreferredSize().height);

            //---- E1NCRX ----
            E1NCRX.setComponentPopupMenu(BTD);
            E1NCRX.setName("E1NCRX");
            panel2.add(E1NCRX);
            E1NCRX.setBounds(38, 65, 60, E1NCRX.getPreferredSize().height);

            //---- OBJ_48_OBJ_48 ----
            OBJ_48_OBJ_48.setText("Compte");
            OBJ_48_OBJ_48.setName("OBJ_48_OBJ_48");
            panel2.add(OBJ_48_OBJ_48);
            OBJ_48_OBJ_48.setBounds(40, 40, 51, 20);

            //---- OBJ_53_OBJ_53 ----
            OBJ_53_OBJ_53.setText("N\u00b0contrepartie");
            OBJ_53_OBJ_53.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_53_OBJ_53.setName("OBJ_53_OBJ_53");
            panel2.add(OBJ_53_OBJ_53);
            OBJ_53_OBJ_53.setBounds(688, 40, 93, 20);

            //---- OBJ_49_OBJ_49 ----
            OBJ_49_OBJ_49.setText("Sens");
            OBJ_49_OBJ_49.setName("OBJ_49_OBJ_49");
            panel2.add(OBJ_49_OBJ_49);
            OBJ_49_OBJ_49.setBounds(110, 40, 40, 20);

            //---- E1CLR ----
            E1CLR.setComponentPopupMenu(BTD);
            E1CLR.setName("E1CLR");
            panel2.add(E1CLR);
            E1CLR.setBounds(158, 65, 20, E1CLR.getPreferredSize().height);

            //---- OBJ_50_OBJ_50 ----
            OBJ_50_OBJ_50.setText("C");
            OBJ_50_OBJ_50.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_50_OBJ_50.setName("OBJ_50_OBJ_50");
            panel2.add(OBJ_50_OBJ_50);
            OBJ_50_OBJ_50.setBounds(160, 40, 20, 20);

            //---- OBJ_54_OBJ_54 ----
            OBJ_54_OBJ_54.setText("Type");
            OBJ_54_OBJ_54.setName("OBJ_54_OBJ_54");
            panel2.add(OBJ_54_OBJ_54);
            OBJ_54_OBJ_54.setBounds(793, 40, 46, 20);

            //---- label1 ----
            label1.setText("R\u00e9f\u00e9rence facture");
            label1.setName("label1");
            panel2.add(label1);
            label1.setBounds(195, 40, 125, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          //======== p_Totalisateurs ========
          {
            p_Totalisateurs.setBorder(new TitledBorder("Totalisateurs"));
            p_Totalisateurs.setOpaque(false);
            p_Totalisateurs.setName("p_Totalisateurs");
            p_Totalisateurs.setLayout(null);

            //======== p_Totalisateur1 ========
            {
              p_Totalisateur1.setOpaque(false);
              p_Totalisateur1.setName("p_Totalisateur1");

              //---- xTS_LIBMTT ----
              xTS_LIBMTT.setTitle("@LIBMTT@");
              xTS_LIBMTT.setName("xTS_LIBMTT");

              //---- xTS_Compte ----
              xTS_Compte.setTitle("Compte");
              xTS_Compte.setName("xTS_Compte");

              //---- xTS_Sens ----
              xTS_Sens.setTitle("Sens");
              xTS_Sens.setName("xTS_Sens");

              //---- xTS_Section ----
              xTS_Section.setTitle("Section");
              xTS_Section.setName("xTS_Section");

              //---- xTS_Libelle ----
              xTS_Libelle.setTitle("Libell\u00e9");
              xTS_Libelle.setName("xTS_Libelle");

              //---- l_WTMT1 ----
              l_WTMT1.setText("@WTMT01@");
              l_WTMT1.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT1.setName("l_WTMT1");

              //---- WTNC01 ----
              WTNC01.setName("WTNC01");

              //---- WTSS01 ----
              WTSS01.setName("WTSS01");

              //---- WTSA01 ----
              WTSA01.setComponentPopupMenu(BTD);
              WTSA01.setName("WTSA01");

              //---- WTLI01 ----
              WTLI01.setName("WTLI01");

              //---- l_WTMT3 ----
              l_WTMT3.setText("@WTMT03@");
              l_WTMT3.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT3.setName("l_WTMT3");

              //---- WTNC03 ----
              WTNC03.setName("WTNC03");

              //---- WTSS03 ----
              WTSS03.setName("WTSS03");

              //---- WTSA03 ----
              WTSA03.setComponentPopupMenu(BTD);
              WTSA03.setName("WTSA03");

              //---- WTLI03 ----
              WTLI03.setName("WTLI03");

              //---- l_WTMT5 ----
              l_WTMT5.setText("@WTMT05@");
              l_WTMT5.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT5.setName("l_WTMT5");

              //---- WTNC05 ----
              WTNC05.setName("WTNC05");

              //---- WTSS05 ----
              WTSS05.setName("WTSS05");

              //---- WTSA05 ----
              WTSA05.setComponentPopupMenu(BTD);
              WTSA05.setName("WTSA05");

              //---- WTLI05 ----
              WTLI05.setName("WTLI05");

              //---- l_WTMT7 ----
              l_WTMT7.setText("@WTMT07@");
              l_WTMT7.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT7.setName("l_WTMT7");

              //---- WTNC07 ----
              WTNC07.setName("WTNC07");

              //---- WTSS07 ----
              WTSS07.setName("WTSS07");

              //---- WTSA07 ----
              WTSA07.setComponentPopupMenu(BTD);
              WTSA07.setName("WTSA07");

              //---- WTLI07 ----
              WTLI07.setName("WTLI07");

              //---- l_WTMT9 ----
              l_WTMT9.setText("@WTMT09@");
              l_WTMT9.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT9.setName("l_WTMT9");

              //---- WTNC09 ----
              WTNC09.setName("WTNC09");

              //---- WTSS09 ----
              WTSS09.setName("WTSS09");

              //---- WTSA09 ----
              WTSA09.setComponentPopupMenu(BTD);
              WTSA09.setName("WTSA09");

              //---- WTLI09 ----
              WTLI09.setName("WTLI09");

              //---- l_WTMT11 ----
              l_WTMT11.setText("@WTMT11@");
              l_WTMT11.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT11.setName("l_WTMT11");

              //---- WTNC11 ----
              WTNC11.setName("WTNC11");

              //---- WTSS11 ----
              WTSS11.setName("WTSS11");

              //---- WTSA11 ----
              WTSA11.setComponentPopupMenu(BTD);
              WTSA11.setName("WTSA11");

              //---- WTLI11 ----
              WTLI11.setName("WTLI11");

              //---- l_WTMT13 ----
              l_WTMT13.setText("@WTMT13@");
              l_WTMT13.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT13.setName("l_WTMT13");

              //---- WTNC13 ----
              WTNC13.setName("WTNC13");

              //---- WTSS13 ----
              WTSS13.setName("WTSS13");

              //---- WTSA13 ----
              WTSA13.setComponentPopupMenu(BTD);
              WTSA13.setName("WTSA13");

              //---- WTLI13 ----
              WTLI13.setName("WTLI13");

              //---- l_WTMT15 ----
              l_WTMT15.setText("@WTMT15@");
              l_WTMT15.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT15.setName("l_WTMT15");

              //---- WTNC15 ----
              WTNC15.setName("WTNC15");

              //---- WTSS15 ----
              WTSS15.setName("WTSS15");

              //---- WTSA15 ----
              WTSA15.setComponentPopupMenu(BTD);
              WTSA15.setName("WTSA15");

              //---- WTLI15 ----
              WTLI15.setName("WTLI15");

              //---- l_WTMT17 ----
              l_WTMT17.setText("@WTMT17@");
              l_WTMT17.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT17.setName("l_WTMT17");

              //---- WTNC17 ----
              WTNC17.setName("WTNC17");

              //---- WTSS17 ----
              WTSS17.setName("WTSS17");

              //---- WTSA17 ----
              WTSA17.setComponentPopupMenu(BTD);
              WTSA17.setName("WTSA17");

              //---- WTLI17 ----
              WTLI17.setName("WTLI17");

              //---- l_WTMT19 ----
              l_WTMT19.setText("@WTMT19@");
              l_WTMT19.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT19.setName("l_WTMT19");

              //---- WTNC19 ----
              WTNC19.setName("WTNC19");

              //---- WTSS19 ----
              WTSS19.setName("WTSS19");

              //---- WTSA19 ----
              WTSA19.setComponentPopupMenu(BTD);
              WTSA19.setName("WTSA19");

              //---- WTLI19 ----
              WTLI19.setName("WTLI19");

              //---- l_WTMT21 ----
              l_WTMT21.setText("@WTMT21@");
              l_WTMT21.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT21.setName("l_WTMT21");

              //---- WTNC21 ----
              WTNC21.setName("WTNC21");

              //---- WTSS21 ----
              WTSS21.setName("WTSS21");

              //---- WTSA21 ----
              WTSA21.setComponentPopupMenu(BTD);
              WTSA21.setName("WTSA21");

              //---- WTLI21 ----
              WTLI21.setName("WTLI21");

              //---- l_WTMT23 ----
              l_WTMT23.setText("@WTMT23@");
              l_WTMT23.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT23.setName("l_WTMT23");

              //---- WTNC23 ----
              WTNC23.setName("WTNC23");

              //---- WTSS23 ----
              WTSS23.setName("WTSS23");

              //---- WTSA23 ----
              WTSA23.setComponentPopupMenu(BTD);
              WTSA23.setName("WTSA23");

              //---- WTLI23 ----
              WTLI23.setName("WTLI23");

              //---- l_WTMT25 ----
              l_WTMT25.setText("@WTMT25@");
              l_WTMT25.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT25.setName("l_WTMT25");

              //---- WTNC25 ----
              WTNC25.setName("WTNC25");

              //---- WTSS25 ----
              WTSS25.setName("WTSS25");

              //---- WTSA25 ----
              WTSA25.setComponentPopupMenu(BTD);
              WTSA25.setName("WTSA25");

              //---- WTLI25 ----
              WTLI25.setName("WTLI25");

              //---- l_WTMT27 ----
              l_WTMT27.setText("@WTMT27@");
              l_WTMT27.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT27.setName("l_WTMT27");

              //---- WTNC27 ----
              WTNC27.setName("WTNC27");

              //---- WTSS27 ----
              WTSS27.setName("WTSS27");

              //---- WTSA27 ----
              WTSA27.setComponentPopupMenu(BTD);
              WTSA27.setName("WTSA27");

              //---- WTLI27 ----
              WTLI27.setName("WTLI27");

              //---- l_WTMT29 ----
              l_WTMT29.setText("@WTMT29@");
              l_WTMT29.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT29.setName("l_WTMT29");

              //---- WTNC29 ----
              WTNC29.setName("WTNC29");

              //---- WTSS29 ----
              WTSS29.setName("WTSS29");

              //---- WTSA29 ----
              WTSA29.setComponentPopupMenu(BTD);
              WTSA29.setName("WTSA29");

              //---- WTLI29 ----
              WTLI29.setName("WTLI29");

              //---- l_WTMT31 ----
              l_WTMT31.setText("@WTMT31@");
              l_WTMT31.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT31.setName("l_WTMT31");

              //---- WTNC31 ----
              WTNC31.setName("WTNC31");

              //---- WTSS31 ----
              WTSS31.setName("WTSS31");

              //---- WTSA31 ----
              WTSA31.setComponentPopupMenu(BTD);
              WTSA31.setName("WTSA31");

              //---- WTLI31 ----
              WTLI31.setName("WTLI31");

              GroupLayout p_Totalisateur1Layout = new GroupLayout(p_Totalisateur1);
              p_Totalisateur1.setLayout(p_Totalisateur1Layout);
              p_Totalisateur1Layout.setHorizontalGroup(
                p_Totalisateur1Layout.createParallelGroup()
                  .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addGroup(p_Totalisateur1Layout.createParallelGroup()
                      .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                        .addComponent(xTS_LIBMTT, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                        .addGap(13, 13, 13)
                        .addComponent(xTS_Compte, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addGap(13, 13, 13)
                        .addComponent(xTS_Sens, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
                        .addGap(13, 13, 13)
                        .addComponent(xTS_Section, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addGap(13, 13, 13)
                        .addComponent(xTS_Libelle, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                        .addGroup(p_Totalisateur1Layout.createParallelGroup()
                          .addComponent(l_WTMT1, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT3, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT5, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT7, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT9, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT11, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT13, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT15, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT17, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT19, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT21, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT23, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT25, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT27, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT29, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT31, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE))
                        .addGap(15, 15, 15)
                        .addGroup(p_Totalisateur1Layout.createParallelGroup()
                          .addComponent(WTNC01, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC17, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC05, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC03, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC11, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC21, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC15, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC07, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC13, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC19, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC09, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC25, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC27, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC31, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC29, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC23, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                        .addGap(25, 25, 25)
                        .addGroup(p_Totalisateur1Layout.createParallelGroup()
                          .addComponent(WTSS09, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS03, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS13, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS01, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS17, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS07, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS21, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS19, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS23, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS15, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS11, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS05, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS29, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS31, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS27, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS25, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                        .addGap(29, 29, 29)
                        .addGroup(p_Totalisateur1Layout.createParallelGroup()
                          .addComponent(WTSA07, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA03, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA15, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA09, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA13, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA05, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA11, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA01, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA23, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA31, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA29, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA27, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA25, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA21, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA19, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA17, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                        .addGap(21, 21, 21)
                        .addGroup(p_Totalisateur1Layout.createParallelGroup()
                          .addComponent(WTLI23, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI25, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI09, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI03, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI01, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI05, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI21, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI11, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI15, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI19, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI13, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI07, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI17, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI27, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI31, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI29, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)))))
              );
              p_Totalisateur1Layout.setVerticalGroup(
                p_Totalisateur1Layout.createParallelGroup()
                  .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addGroup(p_Totalisateur1Layout.createParallelGroup()
                      .addComponent(xTS_LIBMTT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(xTS_Compte, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(xTS_Sens, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(xTS_Section, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(xTS_Libelle, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(6, 6, 6)
                    .addGroup(p_Totalisateur1Layout.createParallelGroup()
                      .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(l_WTMT1, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT3, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT5, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT7, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT9, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT11, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT13, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT15, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT17, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT19, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT21, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT23, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT25, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT27, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT29, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT31, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                        .addGroup(p_Totalisateur1Layout.createParallelGroup()
                          .addComponent(WTNC01, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(160, 160, 160)
                            .addComponent(WTNC17, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(40, 40, 40)
                            .addComponent(WTNC05, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(20, 20, 20)
                            .addComponent(WTNC03, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(100, 100, 100)
                            .addComponent(WTNC11, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(200, 200, 200)
                            .addComponent(WTNC21, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(140, 140, 140)
                            .addComponent(WTNC15, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(60, 60, 60)
                            .addComponent(WTNC07, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(120, 120, 120)
                            .addComponent(WTNC13, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(180, 180, 180)
                            .addComponent(WTNC19, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(80, 80, 80)
                            .addComponent(WTNC09, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)))
                        .addGap(16, 16, 16)
                        .addGroup(p_Totalisateur1Layout.createParallelGroup()
                          .addComponent(WTNC25, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(20, 20, 20)
                            .addComponent(WTNC27, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(60, 60, 60)
                            .addComponent(WTNC31, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(40, 40, 40)
                            .addComponent(WTNC29, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))))
                      .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                        .addGap(220, 220, 220)
                        .addComponent(WTNC23, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                        .addGroup(p_Totalisateur1Layout.createParallelGroup()
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(80, 80, 80)
                            .addComponent(WTSS09, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(20, 20, 20)
                            .addComponent(WTSS03, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(120, 120, 120)
                            .addComponent(WTSS13, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addComponent(WTSS01, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(160, 160, 160)
                            .addComponent(WTSS17, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(60, 60, 60)
                            .addComponent(WTSS07, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(200, 200, 200)
                            .addComponent(WTSS21, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(180, 180, 180)
                            .addComponent(WTSS19, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(220, 220, 220)
                            .addComponent(WTSS23, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(140, 140, 140)
                            .addComponent(WTSS15, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(100, 100, 100)
                            .addComponent(WTSS11, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(40, 40, 40)
                            .addComponent(WTSS05, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)))
                        .addGap(16, 16, 16)
                        .addGroup(p_Totalisateur1Layout.createParallelGroup()
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(20, 20, 20)
                            .addComponent(WTSS29, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(40, 40, 40)
                            .addComponent(WTSS31, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addComponent(WTSS27, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)))
                      .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                        .addGap(240, 240, 240)
                        .addComponent(WTSS25, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                        .addGroup(p_Totalisateur1Layout.createParallelGroup()
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(60, 60, 60)
                            .addComponent(WTSA07, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(20, 20, 20)
                            .addComponent(WTSA03, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(140, 140, 140)
                            .addComponent(WTSA15, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(80, 80, 80)
                            .addComponent(WTSA09, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(120, 120, 120)
                            .addComponent(WTSA13, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(40, 40, 40)
                            .addComponent(WTSA05, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(100, 100, 100)
                            .addComponent(WTSA11, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addComponent(WTSA01, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                        .addGap(16, 16, 16)
                        .addGroup(p_Totalisateur1Layout.createParallelGroup()
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(40, 40, 40)
                            .addComponent(WTSA23, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(120, 120, 120)
                            .addComponent(WTSA31, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(100, 100, 100)
                            .addComponent(WTSA29, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(80, 80, 80)
                            .addComponent(WTSA27, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(60, 60, 60)
                            .addComponent(WTSA25, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(20, 20, 20)
                            .addComponent(WTSA21, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addComponent(WTSA19, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)))
                      .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                        .addGap(160, 160, 160)
                        .addComponent(WTSA17, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                        .addGroup(p_Totalisateur1Layout.createParallelGroup()
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(220, 220, 220)
                            .addComponent(WTLI23, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(240, 240, 240)
                            .addComponent(WTLI25, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(80, 80, 80)
                            .addComponent(WTLI09, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(20, 20, 20)
                            .addComponent(WTLI03, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addComponent(WTLI01, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(40, 40, 40)
                            .addComponent(WTLI05, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(200, 200, 200)
                            .addComponent(WTLI21, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(100, 100, 100)
                            .addComponent(WTLI11, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(140, 140, 140)
                            .addComponent(WTLI15, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(180, 180, 180)
                            .addComponent(WTLI19, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(120, 120, 120)
                            .addComponent(WTLI13, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(60, 60, 60)
                            .addComponent(WTLI07, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(160, 160, 160)
                            .addComponent(WTLI17, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                            .addGap(260, 260, 260)
                            .addComponent(WTLI27, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)))
                        .addGap(16, 16, 16)
                        .addComponent(WTLI31, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_Totalisateur1Layout.createSequentialGroup()
                        .addGap(280, 280, 280)
                        .addComponent(WTLI29, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))))
              );
            }
            p_Totalisateurs.add(p_Totalisateur1);
            p_Totalisateur1.setBounds(25, 30, p_Totalisateur1.getPreferredSize().width, 350);

            //======== p_Totalisateur2 ========
            {
              p_Totalisateur2.setOpaque(false);
              p_Totalisateur2.setName("p_Totalisateur2");

              //---- xTS_LIBMTT2 ----
              xTS_LIBMTT2.setTitle("@LIBMT2@");
              xTS_LIBMTT2.setName("xTS_LIBMTT2");

              //---- xTS_Compte2 ----
              xTS_Compte2.setTitle("Compte");
              xTS_Compte2.setName("xTS_Compte2");

              //---- xTS_Sens2 ----
              xTS_Sens2.setTitle("Sens");
              xTS_Sens2.setName("xTS_Sens2");

              //---- xTS_Section2 ----
              xTS_Section2.setTitle("Section");
              xTS_Section2.setName("xTS_Section2");

              //---- xTS_Libelle2 ----
              xTS_Libelle2.setTitle("Libell\u00e9");
              xTS_Libelle2.setName("xTS_Libelle2");

              //---- l_WTMT2 ----
              l_WTMT2.setText("@WTMT02@");
              l_WTMT2.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT2.setName("l_WTMT2");

              //---- WTNC02 ----
              WTNC02.setName("WTNC02");

              //---- WTSS02 ----
              WTSS02.setName("WTSS02");

              //---- WTSA02 ----
              WTSA02.setComponentPopupMenu(BTD);
              WTSA02.setName("WTSA02");

              //---- WTLI02 ----
              WTLI02.setName("WTLI02");

              //---- l_WTMT4 ----
              l_WTMT4.setText("@WTMT04@");
              l_WTMT4.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT4.setName("l_WTMT4");

              //---- WTNC04 ----
              WTNC04.setName("WTNC04");

              //---- WTSS04 ----
              WTSS04.setName("WTSS04");

              //---- WTSA04 ----
              WTSA04.setComponentPopupMenu(BTD);
              WTSA04.setName("WTSA04");

              //---- WTLI04 ----
              WTLI04.setName("WTLI04");

              //---- l_WTMT6 ----
              l_WTMT6.setText("@WTMT06@");
              l_WTMT6.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT6.setName("l_WTMT6");

              //---- WTNC06 ----
              WTNC06.setName("WTNC06");

              //---- WTSS06 ----
              WTSS06.setName("WTSS06");

              //---- WTSA06 ----
              WTSA06.setComponentPopupMenu(BTD);
              WTSA06.setName("WTSA06");

              //---- WTLI06 ----
              WTLI06.setName("WTLI06");

              //---- l_WTMT8 ----
              l_WTMT8.setText("@WTMT08@");
              l_WTMT8.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT8.setName("l_WTMT8");

              //---- WTNC08 ----
              WTNC08.setName("WTNC08");

              //---- WTSS08 ----
              WTSS08.setName("WTSS08");

              //---- WTSA08 ----
              WTSA08.setComponentPopupMenu(BTD);
              WTSA08.setName("WTSA08");

              //---- WTLI08 ----
              WTLI08.setName("WTLI08");

              //---- l_WTMT10 ----
              l_WTMT10.setText("@WTMT10@");
              l_WTMT10.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT10.setName("l_WTMT10");

              //---- WTNC10 ----
              WTNC10.setName("WTNC10");

              //---- WTSS10 ----
              WTSS10.setName("WTSS10");

              //---- WTSA10 ----
              WTSA10.setComponentPopupMenu(BTD);
              WTSA10.setName("WTSA10");

              //---- WTLI10 ----
              WTLI10.setName("WTLI10");

              //---- l_WTMT12 ----
              l_WTMT12.setText("@WTMT12@");
              l_WTMT12.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT12.setName("l_WTMT12");

              //---- WTNC12 ----
              WTNC12.setName("WTNC12");

              //---- WTSS12 ----
              WTSS12.setName("WTSS12");

              //---- WTSA12 ----
              WTSA12.setComponentPopupMenu(BTD);
              WTSA12.setName("WTSA12");

              //---- WTLI12 ----
              WTLI12.setName("WTLI12");

              //---- l_WTMT14 ----
              l_WTMT14.setText("@WTMT14@");
              l_WTMT14.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT14.setName("l_WTMT14");

              //---- WTNC14 ----
              WTNC14.setName("WTNC14");

              //---- WTSS14 ----
              WTSS14.setName("WTSS14");

              //---- WTSA14 ----
              WTSA14.setComponentPopupMenu(BTD);
              WTSA14.setName("WTSA14");

              //---- WTLI14 ----
              WTLI14.setName("WTLI14");

              //---- l_WTMT16 ----
              l_WTMT16.setText("@WTMT16@");
              l_WTMT16.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT16.setName("l_WTMT16");

              //---- WTNC16 ----
              WTNC16.setName("WTNC16");

              //---- WTSS16 ----
              WTSS16.setName("WTSS16");

              //---- WTSA16 ----
              WTSA16.setComponentPopupMenu(BTD);
              WTSA16.setName("WTSA16");

              //---- WTLI16 ----
              WTLI16.setName("WTLI16");

              //---- l_WTMT18 ----
              l_WTMT18.setText("@WTMT18@");
              l_WTMT18.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT18.setName("l_WTMT18");

              //---- WTNC18 ----
              WTNC18.setName("WTNC18");

              //---- WTSS18 ----
              WTSS18.setName("WTSS18");

              //---- WTSA18 ----
              WTSA18.setComponentPopupMenu(BTD);
              WTSA18.setName("WTSA18");

              //---- WTLI18 ----
              WTLI18.setName("WTLI18");

              //---- l_WTMT20 ----
              l_WTMT20.setText("@WTMT20@");
              l_WTMT20.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT20.setName("l_WTMT20");

              //---- WTNC20 ----
              WTNC20.setName("WTNC20");

              //---- WTSS20 ----
              WTSS20.setName("WTSS20");

              //---- WTSA20 ----
              WTSA20.setComponentPopupMenu(BTD);
              WTSA20.setName("WTSA20");

              //---- WTLI20 ----
              WTLI20.setName("WTLI20");

              //---- l_WTMT22 ----
              l_WTMT22.setText("@WTMT22@");
              l_WTMT22.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT22.setName("l_WTMT22");

              //---- WTNC22 ----
              WTNC22.setName("WTNC22");

              //---- WTSS22 ----
              WTSS22.setName("WTSS22");

              //---- WTSA22 ----
              WTSA22.setComponentPopupMenu(BTD);
              WTSA22.setName("WTSA22");

              //---- WTLI22 ----
              WTLI22.setName("WTLI22");

              //---- l_WTMT24 ----
              l_WTMT24.setText("@WTMT24@");
              l_WTMT24.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT24.setName("l_WTMT24");

              //---- WTNC24 ----
              WTNC24.setName("WTNC24");

              //---- WTSS24 ----
              WTSS24.setName("WTSS24");

              //---- WTSA24 ----
              WTSA24.setComponentPopupMenu(BTD);
              WTSA24.setName("WTSA24");

              //---- WTLI24 ----
              WTLI24.setName("WTLI24");

              //---- l_WTMT26 ----
              l_WTMT26.setText("@WTMT26@");
              l_WTMT26.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT26.setName("l_WTMT26");

              //---- WTNC26 ----
              WTNC26.setName("WTNC26");

              //---- WTSS26 ----
              WTSS26.setName("WTSS26");

              //---- WTSA26 ----
              WTSA26.setComponentPopupMenu(BTD);
              WTSA26.setName("WTSA26");

              //---- WTLI26 ----
              WTLI26.setName("WTLI26");

              //---- l_WTMT28 ----
              l_WTMT28.setText("@WTMT28@");
              l_WTMT28.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT28.setName("l_WTMT28");

              //---- WTNC28 ----
              WTNC28.setName("WTNC28");

              //---- WTSS28 ----
              WTSS28.setName("WTSS28");

              //---- WTSA28 ----
              WTSA28.setComponentPopupMenu(BTD);
              WTSA28.setName("WTSA28");

              //---- WTLI28 ----
              WTLI28.setName("WTLI28");

              //---- l_WTMT30 ----
              l_WTMT30.setText("@WTMT30@");
              l_WTMT30.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT30.setName("l_WTMT30");

              //---- WTNC30 ----
              WTNC30.setName("WTNC30");

              //---- WTSS30 ----
              WTSS30.setName("WTSS30");

              //---- WTSA30 ----
              WTSA30.setComponentPopupMenu(BTD);
              WTSA30.setName("WTSA30");

              //---- WTLI30 ----
              WTLI30.setName("WTLI30");

              //---- l_WTMT32 ----
              l_WTMT32.setText("@WTMT32@");
              l_WTMT32.setHorizontalAlignment(SwingConstants.RIGHT);
              l_WTMT32.setName("l_WTMT32");

              //---- WTNC32 ----
              WTNC32.setName("WTNC32");

              //---- WTSS32 ----
              WTSS32.setName("WTSS32");

              //---- WTSA32 ----
              WTSA32.setComponentPopupMenu(BTD);
              WTSA32.setName("WTSA32");

              //---- WTLI32 ----
              WTLI32.setName("WTLI32");

              GroupLayout p_Totalisateur2Layout = new GroupLayout(p_Totalisateur2);
              p_Totalisateur2.setLayout(p_Totalisateur2Layout);
              p_Totalisateur2Layout.setHorizontalGroup(
                p_Totalisateur2Layout.createParallelGroup()
                  .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addGroup(p_Totalisateur2Layout.createParallelGroup()
                      .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                        .addComponent(xTS_LIBMTT2, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                        .addGap(13, 13, 13)
                        .addComponent(xTS_Compte2, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addGap(13, 13, 13)
                        .addComponent(xTS_Sens2, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
                        .addGap(13, 13, 13)
                        .addComponent(xTS_Section2, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addGap(13, 13, 13)
                        .addComponent(xTS_Libelle2, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                        .addGroup(p_Totalisateur2Layout.createParallelGroup()
                          .addComponent(l_WTMT2, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT4, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT6, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT8, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT10, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT12, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT14, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT16, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT18, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT20, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT22, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT24, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT26, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT28, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT30, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                          .addComponent(l_WTMT32, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE))
                        .addGap(15, 15, 15)
                        .addGroup(p_Totalisateur2Layout.createParallelGroup()
                          .addComponent(WTNC10, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC04, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC14, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC20, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC06, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC08, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC16, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC02, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC22, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC24, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC12, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC18, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC26, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC28, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC32, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC30, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                        .addGap(25, 25, 25)
                        .addGroup(p_Totalisateur2Layout.createParallelGroup()
                          .addComponent(WTSS08, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS28, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS16, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS02, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS12, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS26, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS24, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS20, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS04, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS18, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS06, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS14, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS22, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS10, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS32, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS30, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                        .addGap(29, 29, 29)
                        .addGroup(p_Totalisateur2Layout.createParallelGroup()
                          .addComponent(WTSA14, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA08, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA12, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA04, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA10, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA06, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA02, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA28, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA22, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA20, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA24, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA30, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA18, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA32, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA26, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA16, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                        .addGap(21, 21, 21)
                        .addGroup(p_Totalisateur2Layout.createParallelGroup()
                          .addComponent(WTLI10, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI04, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI16, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI12, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI08, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI14, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI18, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI06, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI32, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI28, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI26, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI30, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI22, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI24, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI02, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI20, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)))))
              );
              p_Totalisateur2Layout.setVerticalGroup(
                p_Totalisateur2Layout.createParallelGroup()
                  .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addGroup(p_Totalisateur2Layout.createParallelGroup()
                      .addComponent(xTS_LIBMTT2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(xTS_Compte2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(xTS_Sens2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(xTS_Section2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(xTS_Libelle2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(6, 6, 6)
                    .addGroup(p_Totalisateur2Layout.createParallelGroup()
                      .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(l_WTMT2, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT4, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT6, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT8, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT10, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT12, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT14, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT16, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT18, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT20, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT22, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT24, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT26, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT28, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT30, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(l_WTMT32, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                        .addGroup(p_Totalisateur2Layout.createParallelGroup()
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(80, 80, 80)
                            .addComponent(WTNC10, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(20, 20, 20)
                            .addComponent(WTNC04, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(120, 120, 120)
                            .addComponent(WTNC14, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(180, 180, 180)
                            .addComponent(WTNC20, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(40, 40, 40)
                            .addComponent(WTNC06, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(60, 60, 60)
                            .addComponent(WTNC08, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(140, 140, 140)
                            .addComponent(WTNC16, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addComponent(WTNC02, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(200, 200, 200)
                            .addComponent(WTNC22, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(220, 220, 220)
                            .addComponent(WTNC24, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(100, 100, 100)
                            .addComponent(WTNC12, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(160, 160, 160)
                            .addComponent(WTNC18, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(240, 240, 240)
                            .addComponent(WTNC26, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(260, 260, 260)
                            .addComponent(WTNC28, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)))
                        .addGap(16, 16, 16)
                        .addComponent(WTNC32, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                        .addGap(280, 280, 280)
                        .addComponent(WTNC30, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                        .addGroup(p_Totalisateur2Layout.createParallelGroup()
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(60, 60, 60)
                            .addComponent(WTSS08, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(260, 260, 260)
                            .addComponent(WTSS28, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(140, 140, 140)
                            .addComponent(WTSS16, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addComponent(WTSS02, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(100, 100, 100)
                            .addComponent(WTSS12, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(240, 240, 240)
                            .addComponent(WTSS26, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(220, 220, 220)
                            .addComponent(WTSS24, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(180, 180, 180)
                            .addComponent(WTSS20, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(20, 20, 20)
                            .addComponent(WTSS04, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(160, 160, 160)
                            .addComponent(WTSS18, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(40, 40, 40)
                            .addComponent(WTSS06, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(120, 120, 120)
                            .addComponent(WTSS14, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(200, 200, 200)
                            .addComponent(WTSS22, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(80, 80, 80)
                            .addComponent(WTSS10, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)))
                        .addGap(16, 16, 16)
                        .addComponent(WTSS32, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                        .addGap(280, 280, 280)
                        .addComponent(WTSS30, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                        .addGroup(p_Totalisateur2Layout.createParallelGroup()
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(120, 120, 120)
                            .addComponent(WTSA14, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(60, 60, 60)
                            .addComponent(WTSA08, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(100, 100, 100)
                            .addComponent(WTSA12, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(20, 20, 20)
                            .addComponent(WTSA04, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(80, 80, 80)
                            .addComponent(WTSA10, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(40, 40, 40)
                            .addComponent(WTSA06, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addComponent(WTSA02, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                        .addGap(16, 16, 16)
                        .addGroup(p_Totalisateur2Layout.createParallelGroup()
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(100, 100, 100)
                            .addComponent(WTSA28, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(40, 40, 40)
                            .addComponent(WTSA22, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(20, 20, 20)
                            .addComponent(WTSA20, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(60, 60, 60)
                            .addComponent(WTSA24, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(120, 120, 120)
                            .addComponent(WTSA30, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addComponent(WTSA18, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(140, 140, 140)
                            .addComponent(WTSA32, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(80, 80, 80)
                            .addComponent(WTSA26, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))))
                      .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                        .addGap(140, 140, 140)
                        .addComponent(WTSA16, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(p_Totalisateur2Layout.createParallelGroup()
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(60, 60, 60)
                            .addComponent(WTLI10, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addComponent(WTLI04, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(120, 120, 120)
                            .addComponent(WTLI16, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(80, 80, 80)
                            .addComponent(WTLI12, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(40, 40, 40)
                            .addComponent(WTLI08, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(100, 100, 100)
                            .addComponent(WTLI14, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(140, 140, 140)
                            .addComponent(WTLI18, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(20, 20, 20)
                            .addComponent(WTLI06, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)))
                        .addGap(16, 16, 16)
                        .addGroup(p_Totalisateur2Layout.createParallelGroup()
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(100, 100, 100)
                            .addComponent(WTLI32, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(60, 60, 60)
                            .addComponent(WTLI28, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(40, 40, 40)
                            .addComponent(WTLI26, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(80, 80, 80)
                            .addComponent(WTLI30, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                          .addComponent(WTLI22, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                          .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                            .addGap(20, 20, 20)
                            .addComponent(WTLI24, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))))
                      .addComponent(WTLI02, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                      .addGroup(p_Totalisateur2Layout.createSequentialGroup()
                        .addGap(180, 180, 180)
                        .addComponent(WTLI20, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))))
              );
            }
            p_Totalisateurs.add(p_Totalisateur2);
            p_Totalisateur2.setBounds(470, 30, p_Totalisateur2.getPreferredSize().width, 350);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < p_Totalisateurs.getComponentCount(); i++) {
                Rectangle bounds = p_Totalisateurs.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = p_Totalisateurs.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              p_Totalisateurs.setMinimumSize(preferredSize);
              p_Totalisateurs.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                  .addComponent(panel2, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(p_Totalisateurs, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 143, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(p_Totalisateurs, GroupLayout.PREFERRED_SIZE, 402, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(13, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Choix possibles");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_49;
  private RiZoneSortie WISOC;
  private JLabel OBJ_56;
  private RiZoneSortie WICJO;
  private JLabel OBJ_57;
  private RiZoneSortie WICFO;
  private JLabel OBJ_58;
  private RiZoneSortie WIDTEX;
  private RiZoneSortie WDTFRA;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiTextField E1LBR;
  private XRiCheckBox E1TIR;
  private XRiComboBox E1SSR;
  private XRiTextField E1LBR2;
  private XRiComboBox WNCPT;
  private XRiTextField E1LBR1;
  private XRiCheckBox E1TRV;
  private JLabel OBJ_52;
  private XRiTextField WNCP;
  private XRiTextField E1NCRX;
  private JLabel OBJ_48_OBJ_48;
  private JLabel OBJ_53_OBJ_53;
  private JLabel OBJ_49_OBJ_49;
  private XRiTextField E1CLR;
  private JLabel OBJ_50_OBJ_50;
  private JLabel OBJ_54_OBJ_54;
  private JLabel label1;
  private JPanel p_Totalisateurs;
  private JPanel p_Totalisateur1;
  private JXTitledSeparator xTS_LIBMTT;
  private JXTitledSeparator xTS_Compte;
  private JXTitledSeparator xTS_Sens;
  private JXTitledSeparator xTS_Section;
  private JXTitledSeparator xTS_Libelle;
  private RiZoneSortie l_WTMT1;
  private XRiTextField WTNC01;
  private XRiTextField WTSS01;
  private XRiTextField WTSA01;
  private XRiTextField WTLI01;
  private RiZoneSortie l_WTMT3;
  private XRiTextField WTNC03;
  private XRiTextField WTSS03;
  private XRiTextField WTSA03;
  private XRiTextField WTLI03;
  private RiZoneSortie l_WTMT5;
  private XRiTextField WTNC05;
  private XRiTextField WTSS05;
  private XRiTextField WTSA05;
  private XRiTextField WTLI05;
  private RiZoneSortie l_WTMT7;
  private XRiTextField WTNC07;
  private XRiTextField WTSS07;
  private XRiTextField WTSA07;
  private XRiTextField WTLI07;
  private RiZoneSortie l_WTMT9;
  private XRiTextField WTNC09;
  private XRiTextField WTSS09;
  private XRiTextField WTSA09;
  private XRiTextField WTLI09;
  private RiZoneSortie l_WTMT11;
  private XRiTextField WTNC11;
  private XRiTextField WTSS11;
  private XRiTextField WTSA11;
  private XRiTextField WTLI11;
  private RiZoneSortie l_WTMT13;
  private XRiTextField WTNC13;
  private XRiTextField WTSS13;
  private XRiTextField WTSA13;
  private XRiTextField WTLI13;
  private RiZoneSortie l_WTMT15;
  private XRiTextField WTNC15;
  private XRiTextField WTSS15;
  private XRiTextField WTSA15;
  private XRiTextField WTLI15;
  private RiZoneSortie l_WTMT17;
  private XRiTextField WTNC17;
  private XRiTextField WTSS17;
  private XRiTextField WTSA17;
  private XRiTextField WTLI17;
  private RiZoneSortie l_WTMT19;
  private XRiTextField WTNC19;
  private XRiTextField WTSS19;
  private XRiTextField WTSA19;
  private XRiTextField WTLI19;
  private RiZoneSortie l_WTMT21;
  private XRiTextField WTNC21;
  private XRiTextField WTSS21;
  private XRiTextField WTSA21;
  private XRiTextField WTLI21;
  private RiZoneSortie l_WTMT23;
  private XRiTextField WTNC23;
  private XRiTextField WTSS23;
  private XRiTextField WTSA23;
  private XRiTextField WTLI23;
  private RiZoneSortie l_WTMT25;
  private XRiTextField WTNC25;
  private XRiTextField WTSS25;
  private XRiTextField WTSA25;
  private XRiTextField WTLI25;
  private RiZoneSortie l_WTMT27;
  private XRiTextField WTNC27;
  private XRiTextField WTSS27;
  private XRiTextField WTSA27;
  private XRiTextField WTLI27;
  private RiZoneSortie l_WTMT29;
  private XRiTextField WTNC29;
  private XRiTextField WTSS29;
  private XRiTextField WTSA29;
  private XRiTextField WTLI29;
  private RiZoneSortie l_WTMT31;
  private XRiTextField WTNC31;
  private XRiTextField WTSS31;
  private XRiTextField WTSA31;
  private XRiTextField WTLI31;
  private JPanel p_Totalisateur2;
  private JXTitledSeparator xTS_LIBMTT2;
  private JXTitledSeparator xTS_Compte2;
  private JXTitledSeparator xTS_Sens2;
  private JXTitledSeparator xTS_Section2;
  private JXTitledSeparator xTS_Libelle2;
  private RiZoneSortie l_WTMT2;
  private XRiTextField WTNC02;
  private XRiTextField WTSS02;
  private XRiTextField WTSA02;
  private XRiTextField WTLI02;
  private RiZoneSortie l_WTMT4;
  private XRiTextField WTNC04;
  private XRiTextField WTSS04;
  private XRiTextField WTSA04;
  private XRiTextField WTLI04;
  private RiZoneSortie l_WTMT6;
  private XRiTextField WTNC06;
  private XRiTextField WTSS06;
  private XRiTextField WTSA06;
  private XRiTextField WTLI06;
  private RiZoneSortie l_WTMT8;
  private XRiTextField WTNC08;
  private XRiTextField WTSS08;
  private XRiTextField WTSA08;
  private XRiTextField WTLI08;
  private RiZoneSortie l_WTMT10;
  private XRiTextField WTNC10;
  private XRiTextField WTSS10;
  private XRiTextField WTSA10;
  private XRiTextField WTLI10;
  private RiZoneSortie l_WTMT12;
  private XRiTextField WTNC12;
  private XRiTextField WTSS12;
  private XRiTextField WTSA12;
  private XRiTextField WTLI12;
  private RiZoneSortie l_WTMT14;
  private XRiTextField WTNC14;
  private XRiTextField WTSS14;
  private XRiTextField WTSA14;
  private XRiTextField WTLI14;
  private RiZoneSortie l_WTMT16;
  private XRiTextField WTNC16;
  private XRiTextField WTSS16;
  private XRiTextField WTSA16;
  private XRiTextField WTLI16;
  private RiZoneSortie l_WTMT18;
  private XRiTextField WTNC18;
  private XRiTextField WTSS18;
  private XRiTextField WTSA18;
  private XRiTextField WTLI18;
  private RiZoneSortie l_WTMT20;
  private XRiTextField WTNC20;
  private XRiTextField WTSS20;
  private XRiTextField WTSA20;
  private XRiTextField WTLI20;
  private RiZoneSortie l_WTMT22;
  private XRiTextField WTNC22;
  private XRiTextField WTSS22;
  private XRiTextField WTSA22;
  private XRiTextField WTLI22;
  private RiZoneSortie l_WTMT24;
  private XRiTextField WTNC24;
  private XRiTextField WTSS24;
  private XRiTextField WTSA24;
  private XRiTextField WTLI24;
  private RiZoneSortie l_WTMT26;
  private XRiTextField WTNC26;
  private XRiTextField WTSS26;
  private XRiTextField WTSA26;
  private XRiTextField WTLI26;
  private RiZoneSortie l_WTMT28;
  private XRiTextField WTNC28;
  private XRiTextField WTSS28;
  private XRiTextField WTSA28;
  private XRiTextField WTLI28;
  private RiZoneSortie l_WTMT30;
  private XRiTextField WTNC30;
  private XRiTextField WTSS30;
  private XRiTextField WTSA30;
  private XRiTextField WTLI30;
  private RiZoneSortie l_WTMT32;
  private XRiTextField WTNC32;
  private XRiTextField WTSS32;
  private XRiTextField WTSA32;
  private XRiTextField WTLI32;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
