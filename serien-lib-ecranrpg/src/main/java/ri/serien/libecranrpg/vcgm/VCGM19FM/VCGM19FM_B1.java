
package ri.serien.libecranrpg.vcgm.VCGM19FM;
// Nom Fichier: pop_VCGM19FM_FMTB1_471.java

import java.awt.Container;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.LayoutStyle;
import javax.swing.SpinnerListModel;

import org.jdesktop.swingx.JXTitledPanel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiSpinner;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGM19FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM19FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(OBJ_44);
    
    // Menu Command
    // setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    NBJ14.setEnabled(lexique.isPresent("NBJ14"));
    NBJ13.setEnabled(lexique.isPresent("NBJ13"));
    NBJ12.setEnabled(lexique.isPresent("NBJ12"));
    NBJ11.setEnabled(lexique.isPresent("NBJ11"));
    NBJ10.setEnabled(lexique.isPresent("NBJ10"));
    NBJ9.setEnabled(lexique.isPresent("NBJ9"));
    NBJ5.setEnabled(lexique.isPresent("NBJ5"));
    NBJ4.setEnabled(lexique.isPresent("NBJ4"));
    NBJ3.setEnabled(lexique.isPresent("NBJ3"));
    NBJ2.setEnabled(lexique.isPresent("NBJ2"));
    NBJ1.setEnabled(lexique.isPresent("NBJ1"));
    
    // TODO Icones
    OBJ_45.setIcon(lexique.chargerImage("images/retour.png", true));
    OBJ_44.setIcon(lexique.chargerImage("images/OK.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_44ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // Touche="ENTER"
    // Scriptcall("G_Touche")
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_45ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // Touche="F12"
    // Scriptcall("G_Touche")
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", true);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vcgm19"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    xTitledPanel1 = new JXTitledPanel();
    OBJ_49 = new JLabel();
    OBJ_51 = new JLabel();
    OBJ_21 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_53 = new JLabel();
    OBJ_58 = new JLabel();
    SGN1 = new XRiSpinner();
    SGN2 = new XRiSpinner();
    SGN3 = new XRiSpinner();
    SGN4 = new XRiSpinner();
    SGN5 = new XRiSpinner();
    NBJ1 = new XRiTextField();
    NBJ2 = new XRiTextField();
    NBJ3 = new XRiTextField();
    NBJ4 = new XRiTextField();
    NBJ5 = new XRiTextField();
    OBJ_61 = new JLabel();
    OBJ_62 = new JLabel();
    OBJ_63 = new JLabel();
    OBJ_64 = new JLabel();
    OBJ_65 = new JLabel();
    OBJ_66 = new JLabel();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_50 = new JLabel();
    OBJ_46 = new JLabel();
    OBJ_52 = new JLabel();
    OBJ_48 = new JLabel();
    OBJ_55 = new JLabel();
    OBJ_59 = new JLabel();
    OBJ_54 = new JLabel();
    SGN9 = new XRiSpinner();
    SGN10 = new XRiSpinner();
    SGN11 = new XRiSpinner();
    SGN12 = new XRiSpinner();
    SGN13 = new XRiSpinner();
    SGN14 = new XRiSpinner();
    NBJ9 = new XRiTextField();
    NBJ10 = new XRiTextField();
    NBJ11 = new XRiTextField();
    NBJ12 = new XRiTextField();
    NBJ13 = new XRiTextField();
    NBJ14 = new XRiTextField();
    OBJ_60 = new JLabel();
    OBJ_67 = new JLabel();
    OBJ_68 = new JLabel();
    OBJ_69 = new JLabel();
    OBJ_70 = new JLabel();
    OBJ_71 = new JLabel();
    OBJ_72 = new JLabel();
    panel1 = new JPanel();
    OBJ_44 = new JButton();
    OBJ_45 = new JButton();
    OBJ_56 = new JLabel();
    OBJ_57 = new JLabel();
    OBJ_4 = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setName("this");

    //======== xTitledPanel1 ========
    {
      xTitledPanel1.setTitle("Tr\u00e9sorerie \u00e0 recevoir");
      xTitledPanel1.setName("xTitledPanel1");
      Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
      xTitledPanel1ContentContainer.setLayout(null);

      //---- OBJ_49 ----
      OBJ_49.setText("Effet remis \u00e0 l'encaissement");
      OBJ_49.setName("OBJ_49");
      xTitledPanel1ContentContainer.add(OBJ_49);
      OBJ_49.setBounds(20, 99, 162, 20);

      //---- OBJ_51 ----
      OBJ_51.setText("Effet remis \u00e0 l'escompte");
      OBJ_51.setName("OBJ_51");
      xTitledPanel1ContentContainer.add(OBJ_51);
      OBJ_51.setBounds(20, 129, 145, 20);

      //---- OBJ_21 ----
      OBJ_21.setText("Ch\u00e8que hors place");
      OBJ_21.setName("OBJ_21");
      xTitledPanel1ContentContainer.add(OBJ_21);
      OBJ_21.setBounds(20, 39, 117, 20);

      //---- OBJ_47 ----
      OBJ_47.setText("Ch\u00e8que sur  place");
      OBJ_47.setName("OBJ_47");
      xTitledPanel1ContentContainer.add(OBJ_47);
      OBJ_47.setBounds(20, 69, 111, 20);

      //---- OBJ_53 ----
      OBJ_53.setText("Virement");
      OBJ_53.setName("OBJ_53");
      xTitledPanel1ContentContainer.add(OBJ_53);
      OBJ_53.setBounds(20, 159, 57, 20);

      //---- OBJ_58 ----
      OBJ_58.setText("Sens");
      OBJ_58.setName("OBJ_58");
      xTitledPanel1ContentContainer.add(OBJ_58);
      OBJ_58.setBounds(227, 10, 34, 20);

      //---- SGN1 ----
      SGN1.setComponentPopupMenu(BTD);
      SGN1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      SGN1.setModel(new SpinnerListModel(new String[] {"+", "-"}));
      SGN1.setName("SGN1");
      xTitledPanel1ContentContainer.add(SGN1);
      SGN1.setBounds(225, 38, 40, 22);

      //---- SGN2 ----
      SGN2.setComponentPopupMenu(BTD);
      SGN2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      SGN2.setModel(new SpinnerListModel(new String[] {"+", "-"}));
      SGN2.setName("SGN2");
      xTitledPanel1ContentContainer.add(SGN2);
      SGN2.setBounds(225, 68, 40, 22);

      //---- SGN3 ----
      SGN3.setComponentPopupMenu(BTD);
      SGN3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      SGN3.setModel(new SpinnerListModel(new String[] {"+", "-"}));
      SGN3.setName("SGN3");
      xTitledPanel1ContentContainer.add(SGN3);
      SGN3.setBounds(225, 98, 40, 22);

      //---- SGN4 ----
      SGN4.setComponentPopupMenu(BTD);
      SGN4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      SGN4.setModel(new SpinnerListModel(new String[] {"+", "-"}));
      SGN4.setName("SGN4");
      xTitledPanel1ContentContainer.add(SGN4);
      SGN4.setBounds(225, 128, 40, 22);

      //---- SGN5 ----
      SGN5.setComponentPopupMenu(BTD);
      SGN5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      SGN5.setModel(new SpinnerListModel(new String[] {"+", "-"}));
      SGN5.setName("SGN5");
      xTitledPanel1ContentContainer.add(SGN5);
      SGN5.setBounds(225, 158, 40, 22);

      //---- NBJ1 ----
      NBJ1.setComponentPopupMenu(BTD);
      NBJ1.setName("NBJ1");
      xTitledPanel1ContentContainer.add(NBJ1);
      NBJ1.setBounds(275, 35, 28, NBJ1.getPreferredSize().height);

      //---- NBJ2 ----
      NBJ2.setComponentPopupMenu(BTD);
      NBJ2.setName("NBJ2");
      xTitledPanel1ContentContainer.add(NBJ2);
      NBJ2.setBounds(275, 65, 28, NBJ2.getPreferredSize().height);

      //---- NBJ3 ----
      NBJ3.setComponentPopupMenu(BTD);
      NBJ3.setName("NBJ3");
      xTitledPanel1ContentContainer.add(NBJ3);
      NBJ3.setBounds(275, 95, 28, NBJ3.getPreferredSize().height);

      //---- NBJ4 ----
      NBJ4.setComponentPopupMenu(BTD);
      NBJ4.setName("NBJ4");
      xTitledPanel1ContentContainer.add(NBJ4);
      NBJ4.setBounds(275, 125, 28, NBJ4.getPreferredSize().height);

      //---- NBJ5 ----
      NBJ5.setComponentPopupMenu(BTD);
      NBJ5.setName("NBJ5");
      xTitledPanel1ContentContainer.add(NBJ5);
      NBJ5.setBounds(275, 155, 28, NBJ5.getPreferredSize().height);

      //---- OBJ_61 ----
      OBJ_61.setText("Nb");
      OBJ_61.setName("OBJ_61");
      xTitledPanel1ContentContainer.add(OBJ_61);
      OBJ_61.setBounds(277, 10, 21, 20);

      //---- OBJ_62 ----
      OBJ_62.setText("(1)");
      OBJ_62.setName("OBJ_62");
      xTitledPanel1ContentContainer.add(OBJ_62);
      OBJ_62.setBounds(200, 39, 18, 20);

      //---- OBJ_63 ----
      OBJ_63.setText("(1)");
      OBJ_63.setName("OBJ_63");
      xTitledPanel1ContentContainer.add(OBJ_63);
      OBJ_63.setBounds(200, 69, 18, 20);

      //---- OBJ_64 ----
      OBJ_64.setText("(2)");
      OBJ_64.setName("OBJ_64");
      xTitledPanel1ContentContainer.add(OBJ_64);
      OBJ_64.setBounds(200, 99, 18, 20);

      //---- OBJ_65 ----
      OBJ_65.setText("(1)");
      OBJ_65.setName("OBJ_65");
      xTitledPanel1ContentContainer.add(OBJ_65);
      OBJ_65.setBounds(200, 129, 18, 20);

      //---- OBJ_66 ----
      OBJ_66.setText("(1)");
      OBJ_66.setName("OBJ_66");
      xTitledPanel1ContentContainer.add(OBJ_66);
      OBJ_66.setBounds(200, 159, 18, 20);
    }

    //======== xTitledPanel2 ========
    {
      xTitledPanel2.setTitle("Tr\u00e9sorerie \u00e0 payer");
      xTitledPanel2.setName("xTitledPanel2");
      Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
      xTitledPanel2ContentContainer.setLayout(null);

      //---- OBJ_50 ----
      OBJ_50.setText("Ch\u00e8que organismes sociaux");
      OBJ_50.setName("OBJ_50");
      xTitledPanel2ContentContainer.add(OBJ_50);
      OBJ_50.setBounds(20, 99, 174, 20);

      //---- OBJ_46 ----
      OBJ_46.setText("Ch\u00e8que fournisseur");
      OBJ_46.setName("OBJ_46");
      xTitledPanel2ContentContainer.add(OBJ_46);
      OBJ_46.setBounds(20, 39, 118, 20);

      //---- OBJ_52 ----
      OBJ_52.setText("Ch\u00e8que divers");
      OBJ_52.setName("OBJ_52");
      xTitledPanel2ContentContainer.add(OBJ_52);
      OBJ_52.setBounds(20, 129, 90, 20);

      //---- OBJ_48 ----
      OBJ_48.setText("Ch\u00e8que Etat");
      OBJ_48.setName("OBJ_48");
      xTitledPanel2ContentContainer.add(OBJ_48);
      OBJ_48.setBounds(20, 69, 76, 20);

      //---- OBJ_55 ----
      OBJ_55.setText("Virement");
      OBJ_55.setName("OBJ_55");
      xTitledPanel2ContentContainer.add(OBJ_55);
      OBJ_55.setBounds(20, 189, 57, 20);

      //---- OBJ_59 ----
      OBJ_59.setText("Sens");
      OBJ_59.setName("OBJ_59");
      xTitledPanel2ContentContainer.add(OBJ_59);
      OBJ_59.setBounds(227, 10, 34, 20);

      //---- OBJ_54 ----
      OBJ_54.setText("Effet");
      OBJ_54.setName("OBJ_54");
      xTitledPanel2ContentContainer.add(OBJ_54);
      OBJ_54.setBounds(20, 159, 30, 20);

      //---- SGN9 ----
      SGN9.setComponentPopupMenu(BTD);
      SGN9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      SGN9.setModel(new SpinnerListModel(new String[] {"+", "-"}));
      SGN9.setName("SGN9");
      xTitledPanel2ContentContainer.add(SGN9);
      SGN9.setBounds(225, 38, 40, 22);

      //---- SGN10 ----
      SGN10.setComponentPopupMenu(BTD);
      SGN10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      SGN10.setModel(new SpinnerListModel(new String[] {"+", "-"}));
      SGN10.setName("SGN10");
      xTitledPanel2ContentContainer.add(SGN10);
      SGN10.setBounds(225, 68, 40, 22);

      //---- SGN11 ----
      SGN11.setComponentPopupMenu(BTD);
      SGN11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      SGN11.setModel(new SpinnerListModel(new String[] {"+", "-"}));
      SGN11.setName("SGN11");
      xTitledPanel2ContentContainer.add(SGN11);
      SGN11.setBounds(225, 98, 40, 22);

      //---- SGN12 ----
      SGN12.setComponentPopupMenu(BTD);
      SGN12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      SGN12.setModel(new SpinnerListModel(new String[] {"+", "-"}));
      SGN12.setName("SGN12");
      xTitledPanel2ContentContainer.add(SGN12);
      SGN12.setBounds(225, 128, 40, 22);

      //---- SGN13 ----
      SGN13.setComponentPopupMenu(BTD);
      SGN13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      SGN13.setModel(new SpinnerListModel(new String[] {"+", "-"}));
      SGN13.setName("SGN13");
      xTitledPanel2ContentContainer.add(SGN13);
      SGN13.setBounds(225, 158, 40, 22);

      //---- SGN14 ----
      SGN14.setComponentPopupMenu(BTD);
      SGN14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      SGN14.setModel(new SpinnerListModel(new String[] {"+", "-"}));
      SGN14.setName("SGN14");
      xTitledPanel2ContentContainer.add(SGN14);
      SGN14.setBounds(225, 188, 40, 22);

      //---- NBJ9 ----
      NBJ9.setComponentPopupMenu(BTD);
      NBJ9.setName("NBJ9");
      xTitledPanel2ContentContainer.add(NBJ9);
      NBJ9.setBounds(275, 35, 28, NBJ9.getPreferredSize().height);

      //---- NBJ10 ----
      NBJ10.setComponentPopupMenu(BTD);
      NBJ10.setName("NBJ10");
      xTitledPanel2ContentContainer.add(NBJ10);
      NBJ10.setBounds(275, 65, 28, NBJ10.getPreferredSize().height);

      //---- NBJ11 ----
      NBJ11.setComponentPopupMenu(BTD);
      NBJ11.setName("NBJ11");
      xTitledPanel2ContentContainer.add(NBJ11);
      NBJ11.setBounds(275, 95, 28, NBJ11.getPreferredSize().height);

      //---- NBJ12 ----
      NBJ12.setComponentPopupMenu(BTD);
      NBJ12.setName("NBJ12");
      xTitledPanel2ContentContainer.add(NBJ12);
      NBJ12.setBounds(275, 125, 28, NBJ12.getPreferredSize().height);

      //---- NBJ13 ----
      NBJ13.setComponentPopupMenu(BTD);
      NBJ13.setName("NBJ13");
      xTitledPanel2ContentContainer.add(NBJ13);
      NBJ13.setBounds(275, 155, 28, NBJ13.getPreferredSize().height);

      //---- NBJ14 ----
      NBJ14.setComponentPopupMenu(BTD);
      NBJ14.setName("NBJ14");
      xTitledPanel2ContentContainer.add(NBJ14);
      NBJ14.setBounds(275, 185, 28, NBJ14.getPreferredSize().height);

      //---- OBJ_60 ----
      OBJ_60.setText("Nb");
      OBJ_60.setName("OBJ_60");
      xTitledPanel2ContentContainer.add(OBJ_60);
      OBJ_60.setBounds(277, 10, 21, 20);

      //---- OBJ_67 ----
      OBJ_67.setText("(1)");
      OBJ_67.setName("OBJ_67");
      xTitledPanel2ContentContainer.add(OBJ_67);
      OBJ_67.setBounds(200, 39, 18, 20);

      //---- OBJ_68 ----
      OBJ_68.setText("(1)");
      OBJ_68.setName("OBJ_68");
      xTitledPanel2ContentContainer.add(OBJ_68);
      OBJ_68.setBounds(200, 69, 18, 20);

      //---- OBJ_69 ----
      OBJ_69.setText("(1)");
      OBJ_69.setName("OBJ_69");
      xTitledPanel2ContentContainer.add(OBJ_69);
      OBJ_69.setBounds(200, 99, 18, 20);

      //---- OBJ_70 ----
      OBJ_70.setText("(1)");
      OBJ_70.setName("OBJ_70");
      xTitledPanel2ContentContainer.add(OBJ_70);
      OBJ_70.setBounds(200, 129, 18, 20);

      //---- OBJ_71 ----
      OBJ_71.setText("(2)");
      OBJ_71.setName("OBJ_71");
      xTitledPanel2ContentContainer.add(OBJ_71);
      OBJ_71.setBounds(200, 159, 18, 20);

      //---- OBJ_72 ----
      OBJ_72.setText("(2)");
      OBJ_72.setName("OBJ_72");
      xTitledPanel2ContentContainer.add(OBJ_72);
      OBJ_72.setBounds(200, 189, 18, 20);
    }

    //======== panel1 ========
    {
      panel1.setName("panel1");
      panel1.setLayout(null);

      //---- OBJ_44 ----
      OBJ_44.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_44.setToolTipText("Ok");
      OBJ_44.setName("OBJ_44");
      OBJ_44.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_44ActionPerformed(e);
        }
      });
      panel1.add(OBJ_44);
      OBJ_44.setBounds(10, 5, 56, 40);

      //---- OBJ_45 ----
      OBJ_45.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_45.setToolTipText("Retour");
      OBJ_45.setName("OBJ_45");
      OBJ_45.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_45ActionPerformed(e);
        }
      });
      panel1.add(OBJ_45);
      OBJ_45.setBounds(70, 5, 56, 40);
    }

    //---- OBJ_56 ----
    OBJ_56.setText("(1) Ecart sur date d'op\u00e9ration");
    OBJ_56.setName("OBJ_56");

    //---- OBJ_57 ----
    OBJ_57.setText("(2) Ecart sur date d'\u00e9ch\u00e9ance");
    OBJ_57.setName("OBJ_57");

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(15, 15, 15)
          .addGroup(layout.createParallelGroup()
            .addGroup(layout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addGroup(layout.createParallelGroup()
                .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 193, GroupLayout.PREFERRED_SIZE)
                .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 196, GroupLayout.PREFERRED_SIZE))
              .addGap(339, 339, 339)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
              .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE)
              .addGap(10, 10, 10)
              .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(15, 15, 15)
          .addGroup(layout.createParallelGroup()
            .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 255, GroupLayout.PREFERRED_SIZE)
            .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 255, GroupLayout.PREFERRED_SIZE))
          .addGroup(layout.createParallelGroup()
            .addGroup(layout.createSequentialGroup()
              .addGap(5, 5, 5)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
          .addContainerGap(15, Short.MAX_VALUE))
    );

    //======== OBJ_4 ========
    {
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("R\u00e9afficher");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_6);

      //---- OBJ_7 ----
      OBJ_7.setText("Annuler");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);
      OBJ_4.addSeparator();

      //---- OBJ_8 ----
      OBJ_8.setText("Exploitation");
      OBJ_8.setName("OBJ_8");
      OBJ_4.add(OBJ_8);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_49;
  private JLabel OBJ_51;
  private JLabel OBJ_21;
  private JLabel OBJ_47;
  private JLabel OBJ_53;
  private JLabel OBJ_58;
  private XRiSpinner SGN1;
  private XRiSpinner SGN2;
  private XRiSpinner SGN3;
  private XRiSpinner SGN4;
  private XRiSpinner SGN5;
  private XRiTextField NBJ1;
  private XRiTextField NBJ2;
  private XRiTextField NBJ3;
  private XRiTextField NBJ4;
  private XRiTextField NBJ5;
  private JLabel OBJ_61;
  private JLabel OBJ_62;
  private JLabel OBJ_63;
  private JLabel OBJ_64;
  private JLabel OBJ_65;
  private JLabel OBJ_66;
  private JXTitledPanel xTitledPanel2;
  private JLabel OBJ_50;
  private JLabel OBJ_46;
  private JLabel OBJ_52;
  private JLabel OBJ_48;
  private JLabel OBJ_55;
  private JLabel OBJ_59;
  private JLabel OBJ_54;
  private XRiSpinner SGN9;
  private XRiSpinner SGN10;
  private XRiSpinner SGN11;
  private XRiSpinner SGN12;
  private XRiSpinner SGN13;
  private XRiSpinner SGN14;
  private XRiTextField NBJ9;
  private XRiTextField NBJ10;
  private XRiTextField NBJ11;
  private XRiTextField NBJ12;
  private XRiTextField NBJ13;
  private XRiTextField NBJ14;
  private JLabel OBJ_60;
  private JLabel OBJ_67;
  private JLabel OBJ_68;
  private JLabel OBJ_69;
  private JLabel OBJ_70;
  private JLabel OBJ_71;
  private JLabel OBJ_72;
  private JPanel panel1;
  private JButton OBJ_44;
  private JButton OBJ_45;
  private JLabel OBJ_56;
  private JLabel OBJ_57;
  private JPopupMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
