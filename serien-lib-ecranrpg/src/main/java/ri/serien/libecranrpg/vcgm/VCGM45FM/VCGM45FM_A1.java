
package ri.serien.libecranrpg.vcgm.VCGM45FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM45FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM45FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    SCAN6.setValeurs(" ", SCAN6_GRP);
    SCAN6_TOUT.setValeurs("X");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    // SCAN6_TOUT.setSelected(lexique.HostFieldGetData("SCAN6").equalsIgnoreCase("X"));
    // SCAN6.setSelected(lexique.HostFieldGetData("SCAN6").equalsIgnoreCase(" "));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (SCAN6_TOUT.isSelected())
    // lexique.HostFieldPutData("SCAN6", 0, "X");
    // if (SCAN6.isSelected())
    // lexique.HostFieldPutData("SCAN6", 0, " ");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTDA.getInvoker().getName());
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_34 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_35 = new JLabel();
    MOIX = new XRiTextField();
    OBJ_36 = new JLabel();
    INDCJO = new XRiTextField();
    OBJ_37 = new JLabel();
    INDCFO = new XRiTextField();
    OBJ_38 = new JLabel();
    WBQE = new XRiTextField();
    OBJ_78 = new JLabel();
    DATEX = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_66 = new JLabel();
    OBJ_64 = new JLabel();
    OBJ_56 = new JLabel();
    OBJ_54 = new JLabel();
    ARG11 = new XRiTextField();
    ARG12 = new XRiTextField();
    ARG2 = new XRiTextField();
    ARG3 = new XRiTextField();
    ARG6 = new XRiTextField();
    SCAN6_TOUT = new XRiRadioButton();
    SCAN6 = new XRiRadioButton();
    OBJ_62 = new JLabel();
    ARG2M = new XRiTextField();
    ARG13 = new XRiTextField();
    OBJ_55 = new JLabel();
    OBJ_60 = new JLabel();
    label1 = new JLabel();
    BTDA = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_15 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    SCAN6_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Mobilisation DAILLY");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_34 ----
          OBJ_34.setText("Soci\u00e9t\u00e9");
          OBJ_34.setName("OBJ_34");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTDA);
          INDETB.setName("INDETB");

          //---- OBJ_35 ----
          OBJ_35.setText("MM.AA");
          OBJ_35.setName("OBJ_35");

          //---- MOIX ----
          MOIX.setComponentPopupMenu(BTDA);
          MOIX.setName("MOIX");

          //---- OBJ_36 ----
          OBJ_36.setText("Jo");
          OBJ_36.setName("OBJ_36");

          //---- INDCJO ----
          INDCJO.setComponentPopupMenu(BTDA);
          INDCJO.setName("INDCJO");

          //---- OBJ_37 ----
          OBJ_37.setText("Folio");
          OBJ_37.setName("OBJ_37");

          //---- INDCFO ----
          INDCFO.setComponentPopupMenu(BTDA);
          INDCFO.setName("INDCFO");

          //---- OBJ_38 ----
          OBJ_38.setText("Banque");
          OBJ_38.setName("OBJ_38");

          //---- WBQE ----
          WBQE.setComponentPopupMenu(BTDA);
          WBQE.setName("WBQE");

          //---- OBJ_78 ----
          OBJ_78.setText("Date");
          OBJ_78.setName("OBJ_78");

          //---- DATEX ----
          DATEX.setComponentPopupMenu(BTDA);
          DATEX.setName("DATEX");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(MOIX, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(INDCJO, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(INDCFO, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(WBQE, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_78, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(DATEX, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(MOIX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDCJO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDCFO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WBQE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(DATEX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_34)
                  .addComponent(OBJ_35)
                  .addComponent(OBJ_36)
                  .addComponent(OBJ_37)
                  .addComponent(OBJ_38)
                  .addComponent(OBJ_78)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(560, 300));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Recherche multi-crit\u00e8res"));
            panel1.setOpaque(false);
            panel1.setName("panel1");

            //---- OBJ_66 ----
            OBJ_66.setText("Libell\u00e9 de l'\u00e9criture");
            OBJ_66.setName("OBJ_66");

            //---- OBJ_64 ----
            OBJ_64.setText("Num\u00e9ro de pi\u00e8ce");
            OBJ_64.setName("OBJ_64");

            //---- OBJ_56 ----
            OBJ_56.setText("Num\u00e9ro de compte");
            OBJ_56.setName("OBJ_56");

            //---- OBJ_54 ----
            OBJ_54.setText("Collectif   ");
            OBJ_54.setName("OBJ_54");

            //---- ARG11 ----
            ARG11.setComponentPopupMenu(BTD);
            ARG11.setName("ARG11");

            //---- ARG12 ----
            ARG12.setComponentPopupMenu(BTD);
            ARG12.setName("ARG12");

            //---- ARG2 ----
            ARG2.setComponentPopupMenu(BTDA);
            ARG2.setName("ARG2");

            //---- ARG3 ----
            ARG3.setComponentPopupMenu(BTD);
            ARG3.setName("ARG3");

            //---- ARG6 ----
            ARG6.setComponentPopupMenu(BTDA);
            ARG6.setName("ARG6");

            //---- SCAN6_TOUT ----
            SCAN6_TOUT.setText("Sur tout le libell\u00e9");
            SCAN6_TOUT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SCAN6_TOUT.setName("SCAN6_TOUT");

            //---- SCAN6 ----
            SCAN6.setText("Sur les premiers caract\u00e8res");
            SCAN6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SCAN6.setName("SCAN6");

            //---- OBJ_62 ----
            OBJ_62.setText("et");
            OBJ_62.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_62.setName("OBJ_62");

            //---- ARG2M ----
            ARG2M.setComponentPopupMenu(BTDA);
            ARG2M.setName("ARG2M");

            //---- ARG13 ----
            ARG13.setComponentPopupMenu(BTD);
            ARG13.setName("ARG13");

            //---- OBJ_55 ----
            OBJ_55.setText("ou r\u00e9f\u00e9rence de classement");
            OBJ_55.setName("OBJ_55");

            //---- OBJ_60 ----
            OBJ_60.setText("Montant compris entre");
            OBJ_60.setName("OBJ_60");

            //---- label1 ----
            label1.setText("Auxiliaire");
            label1.setName("label1");

            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(160, 160, 160)
                  .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 59, GroupLayout.PREFERRED_SIZE)
                  .addGap(6, 6, 6)
                  .addComponent(label1)
                  .addGap(46, 46, 46)
                  .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 139, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(ARG11, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addComponent(ARG12, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addGap(35, 35, 35)
                  .addComponent(ARG13, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addComponent(ARG2, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE)
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                  .addGap(6, 6, 6)
                  .addComponent(ARG2M, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 127, GroupLayout.PREFERRED_SIZE)
                  .addGap(13, 13, 13)
                  .addComponent(ARG3, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(OBJ_66, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
                  .addGap(25, 25, 25)
                  .addComponent(ARG6, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)
                  .addGap(28, 28, 28)
                  .addComponent(SCAN6, GroupLayout.PREFERRED_SIZE, 189, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(319, 319, 319)
                  .addComponent(SCAN6_TOUT, GroupLayout.PREFERRED_SIZE, 189, GroupLayout.PREFERRED_SIZE))
            );
            panel1Layout.setVerticalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(23, 23, 23)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(OBJ_54)
                    .addComponent(label1)
                    .addComponent(OBJ_55))
                  .addGap(4, 4, 4)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_56))
                    .addComponent(ARG11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ARG12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ARG13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(7, 7, 7)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(ARG2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ARG2M, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addGroup(panel1Layout.createParallelGroup()
                        .addComponent(OBJ_60)
                        .addComponent(OBJ_62))))
                  .addGap(7, 7, 7)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_64))
                    .addComponent(ARG3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(7, 7, 7)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_66))
                    .addComponent(ARG6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(SCAN6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(7, 7, 7)
                  .addComponent(SCAN6_TOUT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addContainerGap(29, Short.MAX_VALUE))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTDA ========
    {
      BTDA.setName("BTDA");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTDA.add(OBJ_5);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_15 ----
      OBJ_15.setText("Choix possibles");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);

      //---- OBJ_14 ----
      OBJ_14.setText("Aide en ligne");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);
    }

    //---- SCAN6_GRP ----
    SCAN6_GRP.add(SCAN6_TOUT);
    SCAN6_GRP.add(SCAN6);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_34;
  private XRiTextField INDETB;
  private JLabel OBJ_35;
  private XRiTextField MOIX;
  private JLabel OBJ_36;
  private XRiTextField INDCJO;
  private JLabel OBJ_37;
  private XRiTextField INDCFO;
  private JLabel OBJ_38;
  private XRiTextField WBQE;
  private JLabel OBJ_78;
  private XRiTextField DATEX;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_66;
  private JLabel OBJ_64;
  private JLabel OBJ_56;
  private JLabel OBJ_54;
  private XRiTextField ARG11;
  private XRiTextField ARG12;
  private XRiTextField ARG2;
  private XRiTextField ARG3;
  private XRiTextField ARG6;
  private XRiRadioButton SCAN6_TOUT;
  private XRiRadioButton SCAN6;
  private JLabel OBJ_62;
  private XRiTextField ARG2M;
  private XRiTextField ARG13;
  private JLabel OBJ_55;
  private JLabel OBJ_60;
  private JLabel label1;
  private JPopupMenu BTDA;
  private JMenuItem OBJ_5;
  private JPopupMenu BTD;
  private JMenuItem OBJ_15;
  private JMenuItem OBJ_14;
  private ButtonGroup SCAN6_GRP;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
