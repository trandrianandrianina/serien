
package ri.serien.libecranrpg.vcgm.VCGM06FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGM06FM_RG extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM06FM_RG(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBREE@")).trim());
    LIBSIJ.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBSIJ@")).trim());
    RGMDR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RGMDR@")).trim());
    RGDEL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RGDEL@")).trim());
    RGNBR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RGNBR@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Fiche réglements et Relances"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F17");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(17, 58);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(20, 76);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_20 = new JLabel();
    RGREE = new XRiTextField();
    OBJ_22 = new RiZoneSortie();
    xTitledPanel2 = new JXTitledPanel();
    LIBSIJ = new RiZoneSortie();
    RGRES = new XRiTextField();
    RGTEL = new XRiTextField();
    OBJ_28 = new JLabel();
    OBJ_32 = new JLabel();
    OBJ_30 = new JLabel();
    RGSIJ = new XRiTextField();
    xTitledPanel3 = new JXTitledPanel();
    RGMSY = new XRiTextField();
    RGNSY = new XRiTextField();
    NGRSY = new XRiTextField();
    RGLSY = new XRiTextField();
    RGVSY = new XRiTextField();
    RGTSY = new XRiTextField();
    OBJ_43 = new JLabel();
    OBJ_50 = new JLabel();
    RGCSY = new XRiTextField();
    OBJ_38 = new JLabel();
    OBJ_48 = new JLabel();
    OBJ_45 = new JLabel();
    OBJ_41 = new JLabel();
    xTitledPanel4 = new JXTitledPanel();
    OBJ_57 = new JLabel();
    RGMDR = new XRiTextField();
    OBJ_55 = new JLabel();
    RGDDRX = new XRiCalendrier();
    OBJ_59 = new JLabel();
    OBJ_61 = new JLabel();
    RGDEL = new XRiTextField();
    RGNBR = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(820, 675));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("M\u00e9mo");
            riSousMenu_bt6.setToolTipText("M\u00e9mo");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Encours client");
            riSousMenu_bt7.setToolTipText("Encours client");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);

          //======== riSousMenu8 ========
          {
            riSousMenu8.setName("riSousMenu8");

            //---- riSousMenu_bt8 ----
            riSousMenu_bt8.setText("Historique relance");
            riSousMenu_bt8.setToolTipText("Historique relance");
            riSousMenu_bt8.setName("riSousMenu_bt8");
            riSousMenu_bt8.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt8ActionPerformed(e);
              }
            });
            riSousMenu8.add(riSousMenu_bt8);
          }
          menus_haut.add(riSousMenu8);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== xTitledPanel1 ========
        {
          xTitledPanel1.setTitle("Responsable du compte dans l'entreprise");
          xTitledPanel1.setBorder(new DropShadowBorder());
          xTitledPanel1.setName("xTitledPanel1");
          Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

          //---- OBJ_20 ----
          OBJ_20.setText("Nom du responsable");
          OBJ_20.setName("OBJ_20");

          //---- RGREE ----
          RGREE.setComponentPopupMenu(BTD);
          RGREE.setName("RGREE");

          //---- OBJ_22 ----
          OBJ_22.setText("@LIBREE@");
          OBJ_22.setOpaque(false);
          OBJ_22.setName("OBJ_22");

          GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
          xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
          xTitledPanel1ContentContainerLayout.setHorizontalGroup(
            xTitledPanel1ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
                .addComponent(RGREE, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE))
          );
          xTitledPanel1ContentContainerLayout.setVerticalGroup(
            xTitledPanel1ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(OBJ_20))
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(RGREE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_contenu.add(xTitledPanel1);
        xTitledPanel1.setBounds(10, 10, 633, 87);

        //======== xTitledPanel2 ========
        {
          xTitledPanel2.setTitle("Responsable du compte chez le client");
          xTitledPanel2.setBorder(new DropShadowBorder());
          xTitledPanel2.setName("xTitledPanel2");
          Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();

          //---- LIBSIJ ----
          LIBSIJ.setText("@LIBSIJ@");
          LIBSIJ.setComponentPopupMenu(BTD);
          LIBSIJ.setBackground(new Color(214, 217, 223));
          LIBSIJ.setOpaque(false);
          LIBSIJ.setName("LIBSIJ");

          //---- RGRES ----
          RGRES.setComponentPopupMenu(BTD);
          RGRES.setName("RGRES");

          //---- RGTEL ----
          RGTEL.setComponentPopupMenu(BTD);
          RGTEL.setName("RGTEL");

          //---- OBJ_28 ----
          OBJ_28.setText("Nom du responsable");
          OBJ_28.setName("OBJ_28");

          //---- OBJ_32 ----
          OBJ_32.setText("Situation juridique");
          OBJ_32.setName("OBJ_32");

          //---- OBJ_30 ----
          OBJ_30.setIcon(null);
          OBJ_30.setText("T\u00e9l\u00e9phone");
          OBJ_30.setName("OBJ_30");

          //---- RGSIJ ----
          RGSIJ.setComponentPopupMenu(BTD);
          RGSIJ.setName("RGSIJ");

          GroupLayout xTitledPanel2ContentContainerLayout = new GroupLayout(xTitledPanel2ContentContainer);
          xTitledPanel2ContentContainer.setLayout(xTitledPanel2ContentContainerLayout);
          xTitledPanel2ContentContainerLayout.setHorizontalGroup(
            xTitledPanel2ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                    .addComponent(RGRES, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                    .addComponent(RGTEL, GroupLayout.PREFERRED_SIZE, 172, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                    .addComponent(RGSIJ, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(LIBSIJ, GroupLayout.PREFERRED_SIZE, 292, GroupLayout.PREFERRED_SIZE))))
          );
          xTitledPanel2ContentContainerLayout.setVerticalGroup(
            xTitledPanel2ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_28))
                  .addComponent(RGRES, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_30))
                  .addComponent(RGTEL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_32))
                  .addComponent(RGSIJ, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(LIBSIJ, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
          );
        }
        p_contenu.add(xTitledPanel2);
        xTitledPanel2.setBounds(10, 103, 633, 170);

        //======== xTitledPanel3 ========
        {
          xTitledPanel3.setTitle("Coordonn\u00e9es syndic.");
          xTitledPanel3.setBorder(new DropShadowBorder());
          xTitledPanel3.setName("xTitledPanel3");
          Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();

          //---- RGMSY ----
          RGMSY.setComponentPopupMenu(BTD);
          RGMSY.setName("RGMSY");

          //---- RGNSY ----
          RGNSY.setComponentPopupMenu(BTD);
          RGNSY.setName("RGNSY");

          //---- NGRSY ----
          NGRSY.setComponentPopupMenu(BTD);
          NGRSY.setName("NGRSY");

          //---- RGLSY ----
          RGLSY.setComponentPopupMenu(BTD);
          RGLSY.setName("RGLSY");

          //---- RGVSY ----
          RGVSY.setComponentPopupMenu(BTD);
          RGVSY.setName("RGVSY");

          //---- RGTSY ----
          RGTSY.setComponentPopupMenu(BTD);
          RGTSY.setName("RGTSY");

          //---- OBJ_43 ----
          OBJ_43.setText("Localit\u00e9");
          OBJ_43.setName("OBJ_43");

          //---- OBJ_50 ----
          OBJ_50.setText("Mail");
          OBJ_50.setName("OBJ_50");

          //---- RGCSY ----
          RGCSY.setComponentPopupMenu(BTD);
          RGCSY.setName("RGCSY");

          //---- OBJ_38 ----
          OBJ_38.setText("Nom");
          OBJ_38.setName("OBJ_38");

          //---- OBJ_48 ----
          OBJ_48.setIcon(null);
          OBJ_48.setText("T\u00e9l\u00e9phone");
          OBJ_48.setName("OBJ_48");

          //---- OBJ_45 ----
          OBJ_45.setText("Ville");
          OBJ_45.setName("OBJ_45");

          //---- OBJ_41 ----
          OBJ_41.setText("Rue");
          OBJ_41.setName("OBJ_41");

          GroupLayout xTitledPanel3ContentContainerLayout = new GroupLayout(xTitledPanel3ContentContainer);
          xTitledPanel3ContentContainer.setLayout(xTitledPanel3ContentContainerLayout);
          xTitledPanel3ContentContainerLayout.setHorizontalGroup(
            xTitledPanel3ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                    .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
                    .addComponent(RGNSY, GroupLayout.PREFERRED_SIZE, 320, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                    .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NGRSY, GroupLayout.PREFERRED_SIZE, 320, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                    .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
                    .addComponent(RGLSY, GroupLayout.PREFERRED_SIZE, 320, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                    .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
                    .addComponent(RGCSY, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                    .addGap(8, 8, 8)
                    .addComponent(RGVSY, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                    .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
                    .addComponent(RGTSY, GroupLayout.PREFERRED_SIZE, 172, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                    .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
                    .addComponent(RGMSY, GroupLayout.PREFERRED_SIZE, 375, GroupLayout.PREFERRED_SIZE))))
          );
          xTitledPanel3ContentContainerLayout.setVerticalGroup(
            xTitledPanel3ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_38))
                  .addComponent(RGNSY, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3)
                .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_41))
                  .addComponent(NGRSY, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3)
                .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_43))
                  .addComponent(RGLSY, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3)
                .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_45))
                  .addComponent(RGCSY, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(RGVSY, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3)
                .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_48))
                  .addComponent(RGTSY, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3)
                .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_50))
                  .addComponent(RGMSY, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_contenu.add(xTitledPanel3);
        xTitledPanel3.setBounds(10, 279, 633, 230);

        //======== xTitledPanel4 ========
        {
          xTitledPanel4.setBorder(new DropShadowBorder());
          xTitledPanel4.setTitle("Derni\u00e8re relance");
          xTitledPanel4.setName("xTitledPanel4");
          Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();

          //---- OBJ_57 ----
          OBJ_57.setText("Montant");
          OBJ_57.setName("OBJ_57");

          //---- RGMDR ----
          RGMDR.setText("@RGMDR@");
          RGMDR.setFont(RGMDR.getFont().deriveFont(RGMDR.getFont().getStyle() & ~Font.ITALIC));
          RGMDR.setHorizontalAlignment(SwingConstants.RIGHT);
          RGMDR.setName("RGMDR");

          //---- OBJ_55 ----
          OBJ_55.setText("le");
          OBJ_55.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_55.setName("OBJ_55");

          //---- RGDDRX ----
          RGDDRX.setFont(RGDDRX.getFont().deriveFont(RGDDRX.getFont().getStyle() & ~Font.ITALIC));
          RGDDRX.setName("RGDDRX");

          //---- OBJ_59 ----
          OBJ_59.setText("D\u00e9lais moyen de r\u00e8glement");
          OBJ_59.setName("OBJ_59");

          //---- OBJ_61 ----
          OBJ_61.setText("Nombres de relances");
          OBJ_61.setName("OBJ_61");

          //---- RGDEL ----
          RGDEL.setText("@RGDEL@");
          RGDEL.setFont(RGDEL.getFont().deriveFont(RGDEL.getFont().getStyle() & ~Font.ITALIC));
          RGDEL.setHorizontalAlignment(SwingConstants.RIGHT);
          RGDEL.setName("RGDEL");

          //---- RGNBR ----
          RGNBR.setText("@RGNBR@");
          RGNBR.setFont(RGNBR.getFont().deriveFont(RGNBR.getFont().getStyle() & ~Font.ITALIC));
          RGNBR.setHorizontalAlignment(SwingConstants.RIGHT);
          RGNBR.setName("RGNBR");

          GroupLayout xTitledPanel4ContentContainerLayout = new GroupLayout(xTitledPanel4ContentContainer);
          xTitledPanel4ContentContainer.setLayout(xTitledPanel4ContentContainerLayout);
          xTitledPanel4ContentContainerLayout.setHorizontalGroup(
            xTitledPanel4ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(xTitledPanel4ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                    .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
                    .addComponent(RGMDR, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(RGDDRX, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                    .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
                    .addComponent(RGDEL, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                    .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
                    .addComponent(RGNBR, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))))
          );
          xTitledPanel4ContentContainerLayout.setVerticalGroup(
            xTitledPanel4ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(xTitledPanel4ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_57))
                  .addComponent(RGMDR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_55))
                  .addComponent(RGDDRX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(xTitledPanel4ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_59))
                  .addComponent(RGDEL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(xTitledPanel4ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_61))
                  .addComponent(RGNBR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_contenu.add(xTitledPanel4);
        xTitledPanel4.setBounds(10, 515, 633, 150);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_20;
  private XRiTextField RGREE;
  private RiZoneSortie OBJ_22;
  private JXTitledPanel xTitledPanel2;
  private RiZoneSortie LIBSIJ;
  private XRiTextField RGRES;
  private XRiTextField RGTEL;
  private JLabel OBJ_28;
  private JLabel OBJ_32;
  private JLabel OBJ_30;
  private XRiTextField RGSIJ;
  private JXTitledPanel xTitledPanel3;
  private XRiTextField RGMSY;
  private XRiTextField RGNSY;
  private XRiTextField NGRSY;
  private XRiTextField RGLSY;
  private XRiTextField RGVSY;
  private XRiTextField RGTSY;
  private JLabel OBJ_43;
  private JLabel OBJ_50;
  private XRiTextField RGCSY;
  private JLabel OBJ_38;
  private JLabel OBJ_48;
  private JLabel OBJ_45;
  private JLabel OBJ_41;
  private JXTitledPanel xTitledPanel4;
  private JLabel OBJ_57;
  private XRiTextField RGMDR;
  private JLabel OBJ_55;
  private XRiCalendrier RGDDRX;
  private JLabel OBJ_59;
  private JLabel OBJ_61;
  private XRiTextField RGDEL;
  private XRiTextField RGNBR;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
