
package ri.serien.libecranrpg.vcgm.VCGMEGFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGMEGFM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGMEGFM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    LIB01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB01@")).trim());
    LON01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON01@")).trim());
    EDT01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT01@")).trim());
    LIB02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB02@")).trim());
    LON02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON02@")).trim());
    EDT02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT02@")).trim());
    LIB03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB03@")).trim());
    LON03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON03@")).trim());
    EDT03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT03@")).trim());
    LIB04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB04@")).trim());
    LON04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON04@")).trim());
    EDT04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT04@")).trim());
    LIB05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB05@")).trim());
    LON05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON05@")).trim());
    EDT05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT05@")).trim());
    LIB06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB06@")).trim());
    LON06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON06@")).trim());
    EDT06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT06@")).trim());
    LIB07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB07@")).trim());
    LON07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON07@")).trim());
    EDT07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT07@")).trim());
    LIB08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB08@")).trim());
    LON08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON08@")).trim());
    EDT08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT08@")).trim());
    LIB09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB09@")).trim());
    LON09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON09@")).trim());
    EDT09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT09@")).trim());
    LIB10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB10@")).trim());
    LON10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON10@")).trim());
    EDT10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT10@")).trim());
    LIB11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB11@")).trim());
    LON11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON11@")).trim());
    EDT11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT11@")).trim());
    LIB12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB12@")).trim());
    LON12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON12@")).trim());
    EDT12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT12@")).trim());
    LIB13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB13@")).trim());
    LON13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON13@")).trim());
    EDT13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT13@")).trim());
    LIB14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB14@")).trim());
    LON14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON14@")).trim());
    EDT14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT14@")).trim());
    LIB15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB15@")).trim());
    LON15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON15@")).trim());
    EDT15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT15@")).trim());
    LIB16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB16@")).trim());
    LON16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON16@")).trim());
    EDT16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT16@")).trim());
    LIB17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB17@")).trim());
    LON17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON17@")).trim());
    EDT17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT17@")).trim());
    LIB18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB18@")).trim());
    LON18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON18@")).trim());
    EDT18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT18@")).trim());
    LIB19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB19@")).trim());
    LON19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON19@")).trim());
    EDT19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT19@")).trim());
    LIB20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB20@")).trim());
    LON20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON20@")).trim());
    EDT20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT20@")).trim());
    LIB21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB21@")).trim());
    LON21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON21@")).trim());
    EDT21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT21@")).trim());
    LIB22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB22@")).trim());
    LON22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON22@")).trim());
    EDT22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT22@")).trim());
    LIB23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB23@")).trim());
    LON23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON23@")).trim());
    EDT23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT23@")).trim());
    LIB24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB24@")).trim());
    LON24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON24@")).trim());
    EDT24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT24@")).trim());
    LIB25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB25@")).trim());
    LON25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON25@")).trim());
    EDT25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT01@")).trim());
    LIB26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB26@")).trim());
    LON26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON26@")).trim());
    EDT26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT01@")).trim());
    LIB27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB27@")).trim());
    LON27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON27@")).trim());
    EDT27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT01@")).trim());
    LIB28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB28@")).trim());
    LON28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON28@")).trim());
    EDT28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT01@")).trim());
    LIB29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB29@")).trim());
    LON29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON29@")).trim());
    EDT29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT01@")).trim());
    LIB30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB30@")).trim());
    LON30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LON30@")).trim());
    EDT30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EDT01@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    EDNLI.setEnabled(lexique.isPresent("EDNLI"));
    EDCAR.setEnabled(lexique.isPresent("EDCAR"));
    EDLIBR.setEnabled(lexique.isPresent("EDLIBR"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDSOC.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(INDSOC.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_26 = new JLabel();
    INDSOC = new XRiTextField();
    OBJ_28 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_52 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_66 = new JLabel();
    OBJ_35 = new JLabel();
    OBJ_36 = new JLabel();
    OBJ_65 = new JLabel();
    EDCAR = new XRiTextField();
    EDNLI = new XRiTextField();
    panel2 = new JPanel();
    EDLI01 = new XRiTextField();
    EDCO01 = new XRiTextField();
    LIB01 = new RiZoneSortie();
    LON01 = new RiZoneSortie();
    EDT01 = new RiZoneSortie();
    LIB02 = new RiZoneSortie();
    LON02 = new RiZoneSortie();
    EDLI02 = new XRiTextField();
    EDCO02 = new XRiTextField();
    EDT02 = new RiZoneSortie();
    LIB03 = new RiZoneSortie();
    LON03 = new RiZoneSortie();
    EDLI03 = new XRiTextField();
    EDCO03 = new XRiTextField();
    EDT03 = new RiZoneSortie();
    LIB04 = new RiZoneSortie();
    LON04 = new RiZoneSortie();
    EDLI04 = new XRiTextField();
    EDCO04 = new XRiTextField();
    EDT04 = new RiZoneSortie();
    LIB05 = new RiZoneSortie();
    LON05 = new RiZoneSortie();
    EDLI05 = new XRiTextField();
    EDCO05 = new XRiTextField();
    EDT05 = new RiZoneSortie();
    LIB06 = new RiZoneSortie();
    LON06 = new RiZoneSortie();
    EDLI06 = new XRiTextField();
    EDCO06 = new XRiTextField();
    EDT06 = new RiZoneSortie();
    LIB07 = new RiZoneSortie();
    LON07 = new RiZoneSortie();
    EDLI07 = new XRiTextField();
    EDCO07 = new XRiTextField();
    EDT07 = new RiZoneSortie();
    LIB08 = new RiZoneSortie();
    LON08 = new RiZoneSortie();
    EDLI08 = new XRiTextField();
    EDCO08 = new XRiTextField();
    EDT08 = new RiZoneSortie();
    LIB09 = new RiZoneSortie();
    LON09 = new RiZoneSortie();
    EDLI09 = new XRiTextField();
    EDCO09 = new XRiTextField();
    EDT09 = new RiZoneSortie();
    LIB10 = new RiZoneSortie();
    LON10 = new RiZoneSortie();
    EDLI10 = new XRiTextField();
    EDCO10 = new XRiTextField();
    EDT10 = new RiZoneSortie();
    LIB11 = new RiZoneSortie();
    LON11 = new RiZoneSortie();
    EDLI11 = new XRiTextField();
    EDCO11 = new XRiTextField();
    EDT11 = new RiZoneSortie();
    LIB12 = new RiZoneSortie();
    LON12 = new RiZoneSortie();
    EDLI12 = new XRiTextField();
    EDCO12 = new XRiTextField();
    EDT12 = new RiZoneSortie();
    LIB13 = new RiZoneSortie();
    LON13 = new RiZoneSortie();
    EDLI13 = new XRiTextField();
    EDCO13 = new XRiTextField();
    EDT13 = new RiZoneSortie();
    LIB14 = new RiZoneSortie();
    LON14 = new RiZoneSortie();
    EDLI14 = new XRiTextField();
    EDCO14 = new XRiTextField();
    EDT14 = new RiZoneSortie();
    LIB15 = new RiZoneSortie();
    LON15 = new RiZoneSortie();
    EDLI15 = new XRiTextField();
    EDCO15 = new XRiTextField();
    EDT15 = new RiZoneSortie();
    EDLI16 = new XRiTextField();
    EDCO16 = new XRiTextField();
    LIB16 = new RiZoneSortie();
    LON16 = new RiZoneSortie();
    EDT16 = new RiZoneSortie();
    LIB17 = new RiZoneSortie();
    LON17 = new RiZoneSortie();
    EDLI17 = new XRiTextField();
    EDCO17 = new XRiTextField();
    EDT17 = new RiZoneSortie();
    LIB18 = new RiZoneSortie();
    LON18 = new RiZoneSortie();
    EDLI18 = new XRiTextField();
    EDCO18 = new XRiTextField();
    EDT18 = new RiZoneSortie();
    LIB19 = new RiZoneSortie();
    LON19 = new RiZoneSortie();
    EDLI19 = new XRiTextField();
    EDCO19 = new XRiTextField();
    EDT19 = new RiZoneSortie();
    LIB20 = new RiZoneSortie();
    LON20 = new RiZoneSortie();
    EDLI20 = new XRiTextField();
    EDCO20 = new XRiTextField();
    EDT20 = new RiZoneSortie();
    LIB21 = new RiZoneSortie();
    LON21 = new RiZoneSortie();
    EDLI21 = new XRiTextField();
    EDCO21 = new XRiTextField();
    EDT21 = new RiZoneSortie();
    LIB22 = new RiZoneSortie();
    LON22 = new RiZoneSortie();
    EDLI22 = new XRiTextField();
    EDCO22 = new XRiTextField();
    EDT22 = new RiZoneSortie();
    LIB23 = new RiZoneSortie();
    LON23 = new RiZoneSortie();
    EDLI23 = new XRiTextField();
    EDCO23 = new XRiTextField();
    EDT23 = new RiZoneSortie();
    LIB24 = new RiZoneSortie();
    LON24 = new RiZoneSortie();
    EDLI24 = new XRiTextField();
    EDCO24 = new XRiTextField();
    EDT24 = new RiZoneSortie();
    LIB25 = new RiZoneSortie();
    LON25 = new RiZoneSortie();
    EDLI25 = new XRiTextField();
    EDCO25 = new XRiTextField();
    EDT25 = new RiZoneSortie();
    LIB26 = new RiZoneSortie();
    LON26 = new RiZoneSortie();
    EDLI26 = new XRiTextField();
    EDCO26 = new XRiTextField();
    EDT26 = new RiZoneSortie();
    LIB27 = new RiZoneSortie();
    LON27 = new RiZoneSortie();
    EDLI27 = new XRiTextField();
    EDCO27 = new XRiTextField();
    EDT27 = new RiZoneSortie();
    LIB28 = new RiZoneSortie();
    LON28 = new RiZoneSortie();
    EDLI28 = new XRiTextField();
    EDCO28 = new XRiTextField();
    EDT28 = new RiZoneSortie();
    LIB29 = new RiZoneSortie();
    LON29 = new RiZoneSortie();
    EDLI29 = new XRiTextField();
    EDCO29 = new XRiTextField();
    EDT29 = new RiZoneSortie();
    LIB30 = new RiZoneSortie();
    LON30 = new RiZoneSortie();
    EDLI30 = new XRiTextField();
    EDCO30 = new XRiTextField();
    EDT30 = new RiZoneSortie();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    OBJ_62 = new JLabel();
    EDLIBR = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de l'\u00e9dition de ch\u00e8ques");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_26 ----
          OBJ_26.setText("Soci\u00e9t\u00e9");
          OBJ_26.setName("OBJ_26");

          //---- INDSOC ----
          INDSOC.setComponentPopupMenu(BTD);
          INDSOC.setName("INDSOC");

          //---- OBJ_28 ----
          OBJ_28.setText("Type");
          OBJ_28.setName("OBJ_28");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setName("INDTYP");

          //---- OBJ_52 ----
          OBJ_52.setText("Code");
          OBJ_52.setName("OBJ_52");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_26, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(53, 53, 53)
                .addComponent(INDSOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDSOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_26, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Ch\u00e8que EURO CPI 15");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Ch\u00e8que EURO CPI 10");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Caract\u00e9ristiques de l'imprim\u00e9"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_66 ----
            OBJ_66.setText("(Nb Caract\u00e8res)");
            OBJ_66.setName("OBJ_66");
            panel1.add(OBJ_66);
            OBJ_66.setBounds(505, 55, 95, 20);

            //---- OBJ_35 ----
            OBJ_35.setText("Hauteur Imprim\u00e9");
            OBJ_35.setName("OBJ_35");
            panel1.add(OBJ_35);
            OBJ_35.setBounds(395, 30, 102, 18);

            //---- OBJ_36 ----
            OBJ_36.setText("Largeur Imprim\u00e9");
            OBJ_36.setName("OBJ_36");
            panel1.add(OBJ_36);
            OBJ_36.setBounds(535, 30, 100, 18);

            //---- OBJ_65 ----
            OBJ_65.setText("(Nb  lignes)");
            OBJ_65.setName("OBJ_65");
            panel1.add(OBJ_65);
            OBJ_65.setBounds(395, 55, 72, 20);

            //---- EDCAR ----
            EDCAR.setComponentPopupMenu(BTD);
            EDCAR.setName("EDCAR");
            panel1.add(EDCAR);
            EDCAR.setBounds(600, 50, 34, EDCAR.getPreferredSize().height);

            //---- EDNLI ----
            EDNLI.setComponentPopupMenu(BTD);
            EDNLI.setName("EDNLI");
            panel1.add(EDNLI);
            EDNLI.setBounds(465, 50, 26, EDNLI.getPreferredSize().height);

            //======== panel2 ========
            {
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- EDLI01 ----
              EDLI01.setComponentPopupMenu(BTD);
              EDLI01.setName("EDLI01");
              panel2.add(EDLI01);
              EDLI01.setBounds(285, 25, 26, 28);

              //---- EDCO01 ----
              EDCO01.setComponentPopupMenu(BTD);
              EDCO01.setName("EDCO01");
              panel2.add(EDCO01);
              EDCO01.setBounds(310, 25, 34, 28);

              //---- LIB01 ----
              LIB01.setText("@LIB01@");
              LIB01.setName("LIB01");
              panel2.add(LIB01);
              LIB01.setBounds(20, 27, 210, LIB01.getPreferredSize().height);

              //---- LON01 ----
              LON01.setText("@LON01@");
              LON01.setName("LON01");
              panel2.add(LON01);
              LON01.setBounds(230, 27, 55, LON01.getPreferredSize().height);

              //---- EDT01 ----
              EDT01.setText("@EDT01@");
              EDT01.setName("EDT01");
              panel2.add(EDT01);
              EDT01.setBounds(345, 27, 20, EDT01.getPreferredSize().height);

              //---- LIB02 ----
              LIB02.setText("@LIB02@");
              LIB02.setName("LIB02");
              panel2.add(LIB02);
              LIB02.setBounds(20, 52, 210, LIB02.getPreferredSize().height);

              //---- LON02 ----
              LON02.setText("@LON02@");
              LON02.setName("LON02");
              panel2.add(LON02);
              LON02.setBounds(230, 52, 55, LON02.getPreferredSize().height);

              //---- EDLI02 ----
              EDLI02.setComponentPopupMenu(BTD);
              EDLI02.setName("EDLI02");
              panel2.add(EDLI02);
              EDLI02.setBounds(285, 50, 26, EDLI02.getPreferredSize().height);

              //---- EDCO02 ----
              EDCO02.setComponentPopupMenu(BTD);
              EDCO02.setName("EDCO02");
              panel2.add(EDCO02);
              EDCO02.setBounds(310, 50, 34, EDCO02.getPreferredSize().height);

              //---- EDT02 ----
              EDT02.setText("@EDT02@");
              EDT02.setName("EDT02");
              panel2.add(EDT02);
              EDT02.setBounds(345, 52, 20, EDT02.getPreferredSize().height);

              //---- LIB03 ----
              LIB03.setText("@LIB03@");
              LIB03.setName("LIB03");
              panel2.add(LIB03);
              LIB03.setBounds(20, 77, 210, LIB03.getPreferredSize().height);

              //---- LON03 ----
              LON03.setText("@LON03@");
              LON03.setName("LON03");
              panel2.add(LON03);
              LON03.setBounds(230, 77, 55, LON03.getPreferredSize().height);

              //---- EDLI03 ----
              EDLI03.setComponentPopupMenu(BTD);
              EDLI03.setName("EDLI03");
              panel2.add(EDLI03);
              EDLI03.setBounds(285, 75, 26, EDLI03.getPreferredSize().height);

              //---- EDCO03 ----
              EDCO03.setComponentPopupMenu(BTD);
              EDCO03.setName("EDCO03");
              panel2.add(EDCO03);
              EDCO03.setBounds(310, 75, 34, EDCO03.getPreferredSize().height);

              //---- EDT03 ----
              EDT03.setText("@EDT03@");
              EDT03.setName("EDT03");
              panel2.add(EDT03);
              EDT03.setBounds(345, 77, 20, EDT03.getPreferredSize().height);

              //---- LIB04 ----
              LIB04.setText("@LIB04@");
              LIB04.setName("LIB04");
              panel2.add(LIB04);
              LIB04.setBounds(20, 102, 210, LIB04.getPreferredSize().height);

              //---- LON04 ----
              LON04.setText("@LON04@");
              LON04.setName("LON04");
              panel2.add(LON04);
              LON04.setBounds(230, 102, 55, LON04.getPreferredSize().height);

              //---- EDLI04 ----
              EDLI04.setComponentPopupMenu(BTD);
              EDLI04.setName("EDLI04");
              panel2.add(EDLI04);
              EDLI04.setBounds(285, 100, 26, EDLI04.getPreferredSize().height);

              //---- EDCO04 ----
              EDCO04.setComponentPopupMenu(BTD);
              EDCO04.setName("EDCO04");
              panel2.add(EDCO04);
              EDCO04.setBounds(310, 100, 34, EDCO04.getPreferredSize().height);

              //---- EDT04 ----
              EDT04.setText("@EDT04@");
              EDT04.setName("EDT04");
              panel2.add(EDT04);
              EDT04.setBounds(345, 102, 20, EDT04.getPreferredSize().height);

              //---- LIB05 ----
              LIB05.setText("@LIB05@");
              LIB05.setName("LIB05");
              panel2.add(LIB05);
              LIB05.setBounds(20, 127, 210, LIB05.getPreferredSize().height);

              //---- LON05 ----
              LON05.setText("@LON05@");
              LON05.setName("LON05");
              panel2.add(LON05);
              LON05.setBounds(230, 127, 55, LON05.getPreferredSize().height);

              //---- EDLI05 ----
              EDLI05.setComponentPopupMenu(BTD);
              EDLI05.setName("EDLI05");
              panel2.add(EDLI05);
              EDLI05.setBounds(285, 125, 26, EDLI05.getPreferredSize().height);

              //---- EDCO05 ----
              EDCO05.setComponentPopupMenu(BTD);
              EDCO05.setName("EDCO05");
              panel2.add(EDCO05);
              EDCO05.setBounds(310, 125, 34, EDCO05.getPreferredSize().height);

              //---- EDT05 ----
              EDT05.setText("@EDT05@");
              EDT05.setName("EDT05");
              panel2.add(EDT05);
              EDT05.setBounds(345, 127, 20, EDT05.getPreferredSize().height);

              //---- LIB06 ----
              LIB06.setText("@LIB06@");
              LIB06.setName("LIB06");
              panel2.add(LIB06);
              LIB06.setBounds(20, 152, 210, LIB06.getPreferredSize().height);

              //---- LON06 ----
              LON06.setText("@LON06@");
              LON06.setName("LON06");
              panel2.add(LON06);
              LON06.setBounds(230, 152, 55, LON06.getPreferredSize().height);

              //---- EDLI06 ----
              EDLI06.setComponentPopupMenu(BTD);
              EDLI06.setName("EDLI06");
              panel2.add(EDLI06);
              EDLI06.setBounds(285, 150, 26, EDLI06.getPreferredSize().height);

              //---- EDCO06 ----
              EDCO06.setComponentPopupMenu(BTD);
              EDCO06.setName("EDCO06");
              panel2.add(EDCO06);
              EDCO06.setBounds(310, 150, 34, EDCO06.getPreferredSize().height);

              //---- EDT06 ----
              EDT06.setText("@EDT06@");
              EDT06.setName("EDT06");
              panel2.add(EDT06);
              EDT06.setBounds(345, 152, 20, EDT06.getPreferredSize().height);

              //---- LIB07 ----
              LIB07.setText("@LIB07@");
              LIB07.setName("LIB07");
              panel2.add(LIB07);
              LIB07.setBounds(20, 177, 210, LIB07.getPreferredSize().height);

              //---- LON07 ----
              LON07.setText("@LON07@");
              LON07.setName("LON07");
              panel2.add(LON07);
              LON07.setBounds(230, 177, 55, LON07.getPreferredSize().height);

              //---- EDLI07 ----
              EDLI07.setComponentPopupMenu(BTD);
              EDLI07.setName("EDLI07");
              panel2.add(EDLI07);
              EDLI07.setBounds(285, 175, 26, EDLI07.getPreferredSize().height);

              //---- EDCO07 ----
              EDCO07.setComponentPopupMenu(BTD);
              EDCO07.setName("EDCO07");
              panel2.add(EDCO07);
              EDCO07.setBounds(310, 175, 34, EDCO07.getPreferredSize().height);

              //---- EDT07 ----
              EDT07.setText("@EDT07@");
              EDT07.setName("EDT07");
              panel2.add(EDT07);
              EDT07.setBounds(345, 177, 20, EDT07.getPreferredSize().height);

              //---- LIB08 ----
              LIB08.setText("@LIB08@");
              LIB08.setName("LIB08");
              panel2.add(LIB08);
              LIB08.setBounds(20, 202, 210, LIB08.getPreferredSize().height);

              //---- LON08 ----
              LON08.setText("@LON08@");
              LON08.setName("LON08");
              panel2.add(LON08);
              LON08.setBounds(230, 202, 55, LON08.getPreferredSize().height);

              //---- EDLI08 ----
              EDLI08.setComponentPopupMenu(BTD);
              EDLI08.setName("EDLI08");
              panel2.add(EDLI08);
              EDLI08.setBounds(285, 200, 26, EDLI08.getPreferredSize().height);

              //---- EDCO08 ----
              EDCO08.setComponentPopupMenu(BTD);
              EDCO08.setName("EDCO08");
              panel2.add(EDCO08);
              EDCO08.setBounds(310, 200, 34, EDCO08.getPreferredSize().height);

              //---- EDT08 ----
              EDT08.setText("@EDT08@");
              EDT08.setName("EDT08");
              panel2.add(EDT08);
              EDT08.setBounds(345, 202, 20, EDT08.getPreferredSize().height);

              //---- LIB09 ----
              LIB09.setText("@LIB09@");
              LIB09.setName("LIB09");
              panel2.add(LIB09);
              LIB09.setBounds(20, 227, 210, LIB09.getPreferredSize().height);

              //---- LON09 ----
              LON09.setText("@LON09@");
              LON09.setName("LON09");
              panel2.add(LON09);
              LON09.setBounds(230, 227, 55, LON09.getPreferredSize().height);

              //---- EDLI09 ----
              EDLI09.setComponentPopupMenu(BTD);
              EDLI09.setName("EDLI09");
              panel2.add(EDLI09);
              EDLI09.setBounds(285, 225, 26, EDLI09.getPreferredSize().height);

              //---- EDCO09 ----
              EDCO09.setComponentPopupMenu(BTD);
              EDCO09.setName("EDCO09");
              panel2.add(EDCO09);
              EDCO09.setBounds(310, 225, 34, EDCO09.getPreferredSize().height);

              //---- EDT09 ----
              EDT09.setText("@EDT09@");
              EDT09.setName("EDT09");
              panel2.add(EDT09);
              EDT09.setBounds(345, 227, 20, EDT09.getPreferredSize().height);

              //---- LIB10 ----
              LIB10.setText("@LIB10@");
              LIB10.setName("LIB10");
              panel2.add(LIB10);
              LIB10.setBounds(20, 252, 210, LIB10.getPreferredSize().height);

              //---- LON10 ----
              LON10.setText("@LON10@");
              LON10.setName("LON10");
              panel2.add(LON10);
              LON10.setBounds(230, 252, 55, LON10.getPreferredSize().height);

              //---- EDLI10 ----
              EDLI10.setComponentPopupMenu(BTD);
              EDLI10.setName("EDLI10");
              panel2.add(EDLI10);
              EDLI10.setBounds(285, 250, 26, EDLI10.getPreferredSize().height);

              //---- EDCO10 ----
              EDCO10.setComponentPopupMenu(BTD);
              EDCO10.setName("EDCO10");
              panel2.add(EDCO10);
              EDCO10.setBounds(310, 250, 34, EDCO10.getPreferredSize().height);

              //---- EDT10 ----
              EDT10.setText("@EDT10@");
              EDT10.setName("EDT10");
              panel2.add(EDT10);
              EDT10.setBounds(345, 252, 20, EDT10.getPreferredSize().height);

              //---- LIB11 ----
              LIB11.setText("@LIB11@");
              LIB11.setName("LIB11");
              panel2.add(LIB11);
              LIB11.setBounds(20, 277, 210, LIB11.getPreferredSize().height);

              //---- LON11 ----
              LON11.setText("@LON11@");
              LON11.setName("LON11");
              panel2.add(LON11);
              LON11.setBounds(230, 277, 55, LON11.getPreferredSize().height);

              //---- EDLI11 ----
              EDLI11.setComponentPopupMenu(BTD);
              EDLI11.setName("EDLI11");
              panel2.add(EDLI11);
              EDLI11.setBounds(285, 275, 26, EDLI11.getPreferredSize().height);

              //---- EDCO11 ----
              EDCO11.setComponentPopupMenu(BTD);
              EDCO11.setName("EDCO11");
              panel2.add(EDCO11);
              EDCO11.setBounds(310, 275, 34, EDCO11.getPreferredSize().height);

              //---- EDT11 ----
              EDT11.setText("@EDT11@");
              EDT11.setName("EDT11");
              panel2.add(EDT11);
              EDT11.setBounds(345, 277, 20, EDT11.getPreferredSize().height);

              //---- LIB12 ----
              LIB12.setText("@LIB12@");
              LIB12.setName("LIB12");
              panel2.add(LIB12);
              LIB12.setBounds(20, 302, 210, LIB12.getPreferredSize().height);

              //---- LON12 ----
              LON12.setText("@LON12@");
              LON12.setName("LON12");
              panel2.add(LON12);
              LON12.setBounds(230, 302, 55, LON12.getPreferredSize().height);

              //---- EDLI12 ----
              EDLI12.setComponentPopupMenu(BTD);
              EDLI12.setName("EDLI12");
              panel2.add(EDLI12);
              EDLI12.setBounds(285, 300, 26, EDLI12.getPreferredSize().height);

              //---- EDCO12 ----
              EDCO12.setComponentPopupMenu(BTD);
              EDCO12.setName("EDCO12");
              panel2.add(EDCO12);
              EDCO12.setBounds(310, 300, 34, EDCO12.getPreferredSize().height);

              //---- EDT12 ----
              EDT12.setText("@EDT12@");
              EDT12.setName("EDT12");
              panel2.add(EDT12);
              EDT12.setBounds(345, 302, 20, EDT12.getPreferredSize().height);

              //---- LIB13 ----
              LIB13.setText("@LIB13@");
              LIB13.setName("LIB13");
              panel2.add(LIB13);
              LIB13.setBounds(20, 327, 210, LIB13.getPreferredSize().height);

              //---- LON13 ----
              LON13.setText("@LON13@");
              LON13.setName("LON13");
              panel2.add(LON13);
              LON13.setBounds(230, 327, 55, LON13.getPreferredSize().height);

              //---- EDLI13 ----
              EDLI13.setComponentPopupMenu(BTD);
              EDLI13.setName("EDLI13");
              panel2.add(EDLI13);
              EDLI13.setBounds(285, 325, 26, EDLI13.getPreferredSize().height);

              //---- EDCO13 ----
              EDCO13.setComponentPopupMenu(BTD);
              EDCO13.setName("EDCO13");
              panel2.add(EDCO13);
              EDCO13.setBounds(310, 325, 34, EDCO13.getPreferredSize().height);

              //---- EDT13 ----
              EDT13.setText("@EDT13@");
              EDT13.setName("EDT13");
              panel2.add(EDT13);
              EDT13.setBounds(345, 327, 20, EDT13.getPreferredSize().height);

              //---- LIB14 ----
              LIB14.setText("@LIB14@");
              LIB14.setName("LIB14");
              panel2.add(LIB14);
              LIB14.setBounds(20, 352, 210, LIB14.getPreferredSize().height);

              //---- LON14 ----
              LON14.setText("@LON14@");
              LON14.setName("LON14");
              panel2.add(LON14);
              LON14.setBounds(230, 352, 55, LON14.getPreferredSize().height);

              //---- EDLI14 ----
              EDLI14.setComponentPopupMenu(BTD);
              EDLI14.setName("EDLI14");
              panel2.add(EDLI14);
              EDLI14.setBounds(285, 350, 26, EDLI14.getPreferredSize().height);

              //---- EDCO14 ----
              EDCO14.setComponentPopupMenu(BTD);
              EDCO14.setName("EDCO14");
              panel2.add(EDCO14);
              EDCO14.setBounds(310, 350, 34, EDCO14.getPreferredSize().height);

              //---- EDT14 ----
              EDT14.setText("@EDT14@");
              EDT14.setName("EDT14");
              panel2.add(EDT14);
              EDT14.setBounds(345, 352, 20, EDT14.getPreferredSize().height);

              //---- LIB15 ----
              LIB15.setText("@LIB15@");
              LIB15.setName("LIB15");
              panel2.add(LIB15);
              LIB15.setBounds(20, 377, 210, LIB15.getPreferredSize().height);

              //---- LON15 ----
              LON15.setText("@LON15@");
              LON15.setName("LON15");
              panel2.add(LON15);
              LON15.setBounds(230, 377, 55, LON15.getPreferredSize().height);

              //---- EDLI15 ----
              EDLI15.setComponentPopupMenu(BTD);
              EDLI15.setName("EDLI15");
              panel2.add(EDLI15);
              EDLI15.setBounds(285, 375, 26, EDLI15.getPreferredSize().height);

              //---- EDCO15 ----
              EDCO15.setComponentPopupMenu(BTD);
              EDCO15.setName("EDCO15");
              panel2.add(EDCO15);
              EDCO15.setBounds(310, 375, 34, EDCO15.getPreferredSize().height);

              //---- EDT15 ----
              EDT15.setText("@EDT15@");
              EDT15.setName("EDT15");
              panel2.add(EDT15);
              EDT15.setBounds(345, 377, 20, EDT15.getPreferredSize().height);

              //---- EDLI16 ----
              EDLI16.setComponentPopupMenu(BTD);
              EDLI16.setName("EDLI16");
              panel2.add(EDLI16);
              EDLI16.setBounds(685, 25, 26, EDLI16.getPreferredSize().height);

              //---- EDCO16 ----
              EDCO16.setComponentPopupMenu(BTD);
              EDCO16.setName("EDCO16");
              panel2.add(EDCO16);
              EDCO16.setBounds(710, 25, 34, EDCO16.getPreferredSize().height);

              //---- LIB16 ----
              LIB16.setText("@LIB16@");
              LIB16.setName("LIB16");
              panel2.add(LIB16);
              LIB16.setBounds(420, 27, 210, LIB16.getPreferredSize().height);

              //---- LON16 ----
              LON16.setText("@LON16@");
              LON16.setName("LON16");
              panel2.add(LON16);
              LON16.setBounds(630, 27, 55, LON16.getPreferredSize().height);

              //---- EDT16 ----
              EDT16.setText("@EDT16@");
              EDT16.setName("EDT16");
              panel2.add(EDT16);
              EDT16.setBounds(745, 27, 20, EDT16.getPreferredSize().height);

              //---- LIB17 ----
              LIB17.setText("@LIB17@");
              LIB17.setName("LIB17");
              panel2.add(LIB17);
              LIB17.setBounds(420, 52, 210, LIB17.getPreferredSize().height);

              //---- LON17 ----
              LON17.setText("@LON17@");
              LON17.setName("LON17");
              panel2.add(LON17);
              LON17.setBounds(630, 52, 55, LON17.getPreferredSize().height);

              //---- EDLI17 ----
              EDLI17.setComponentPopupMenu(BTD);
              EDLI17.setName("EDLI17");
              panel2.add(EDLI17);
              EDLI17.setBounds(685, 50, 26, EDLI17.getPreferredSize().height);

              //---- EDCO17 ----
              EDCO17.setComponentPopupMenu(BTD);
              EDCO17.setName("EDCO17");
              panel2.add(EDCO17);
              EDCO17.setBounds(710, 50, 34, EDCO17.getPreferredSize().height);

              //---- EDT17 ----
              EDT17.setText("@EDT17@");
              EDT17.setName("EDT17");
              panel2.add(EDT17);
              EDT17.setBounds(745, 52, 20, EDT17.getPreferredSize().height);

              //---- LIB18 ----
              LIB18.setText("@LIB18@");
              LIB18.setName("LIB18");
              panel2.add(LIB18);
              LIB18.setBounds(420, 77, 210, LIB18.getPreferredSize().height);

              //---- LON18 ----
              LON18.setText("@LON18@");
              LON18.setName("LON18");
              panel2.add(LON18);
              LON18.setBounds(630, 77, 55, LON18.getPreferredSize().height);

              //---- EDLI18 ----
              EDLI18.setComponentPopupMenu(BTD);
              EDLI18.setName("EDLI18");
              panel2.add(EDLI18);
              EDLI18.setBounds(685, 75, 26, EDLI18.getPreferredSize().height);

              //---- EDCO18 ----
              EDCO18.setComponentPopupMenu(BTD);
              EDCO18.setName("EDCO18");
              panel2.add(EDCO18);
              EDCO18.setBounds(710, 75, 34, EDCO18.getPreferredSize().height);

              //---- EDT18 ----
              EDT18.setText("@EDT18@");
              EDT18.setName("EDT18");
              panel2.add(EDT18);
              EDT18.setBounds(745, 77, 20, EDT18.getPreferredSize().height);

              //---- LIB19 ----
              LIB19.setText("@LIB19@");
              LIB19.setName("LIB19");
              panel2.add(LIB19);
              LIB19.setBounds(420, 102, 210, LIB19.getPreferredSize().height);

              //---- LON19 ----
              LON19.setText("@LON19@");
              LON19.setName("LON19");
              panel2.add(LON19);
              LON19.setBounds(630, 102, 55, LON19.getPreferredSize().height);

              //---- EDLI19 ----
              EDLI19.setComponentPopupMenu(BTD);
              EDLI19.setName("EDLI19");
              panel2.add(EDLI19);
              EDLI19.setBounds(685, 100, 26, EDLI19.getPreferredSize().height);

              //---- EDCO19 ----
              EDCO19.setComponentPopupMenu(BTD);
              EDCO19.setName("EDCO19");
              panel2.add(EDCO19);
              EDCO19.setBounds(710, 100, 34, EDCO19.getPreferredSize().height);

              //---- EDT19 ----
              EDT19.setText("@EDT19@");
              EDT19.setName("EDT19");
              panel2.add(EDT19);
              EDT19.setBounds(745, 102, 20, EDT19.getPreferredSize().height);

              //---- LIB20 ----
              LIB20.setText("@LIB20@");
              LIB20.setName("LIB20");
              panel2.add(LIB20);
              LIB20.setBounds(420, 127, 210, LIB20.getPreferredSize().height);

              //---- LON20 ----
              LON20.setText("@LON20@");
              LON20.setName("LON20");
              panel2.add(LON20);
              LON20.setBounds(630, 127, 55, LON20.getPreferredSize().height);

              //---- EDLI20 ----
              EDLI20.setComponentPopupMenu(BTD);
              EDLI20.setName("EDLI20");
              panel2.add(EDLI20);
              EDLI20.setBounds(685, 125, 26, EDLI20.getPreferredSize().height);

              //---- EDCO20 ----
              EDCO20.setComponentPopupMenu(BTD);
              EDCO20.setName("EDCO20");
              panel2.add(EDCO20);
              EDCO20.setBounds(710, 125, 34, EDCO20.getPreferredSize().height);

              //---- EDT20 ----
              EDT20.setText("@EDT20@");
              EDT20.setName("EDT20");
              panel2.add(EDT20);
              EDT20.setBounds(745, 127, 20, EDT20.getPreferredSize().height);

              //---- LIB21 ----
              LIB21.setText("@LIB21@");
              LIB21.setName("LIB21");
              panel2.add(LIB21);
              LIB21.setBounds(420, 152, 210, LIB21.getPreferredSize().height);

              //---- LON21 ----
              LON21.setText("@LON21@");
              LON21.setName("LON21");
              panel2.add(LON21);
              LON21.setBounds(630, 152, 55, LON21.getPreferredSize().height);

              //---- EDLI21 ----
              EDLI21.setComponentPopupMenu(BTD);
              EDLI21.setName("EDLI21");
              panel2.add(EDLI21);
              EDLI21.setBounds(685, 150, 26, EDLI21.getPreferredSize().height);

              //---- EDCO21 ----
              EDCO21.setComponentPopupMenu(BTD);
              EDCO21.setName("EDCO21");
              panel2.add(EDCO21);
              EDCO21.setBounds(710, 150, 34, EDCO21.getPreferredSize().height);

              //---- EDT21 ----
              EDT21.setText("@EDT21@");
              EDT21.setName("EDT21");
              panel2.add(EDT21);
              EDT21.setBounds(745, 152, 20, EDT21.getPreferredSize().height);

              //---- LIB22 ----
              LIB22.setText("@LIB22@");
              LIB22.setName("LIB22");
              panel2.add(LIB22);
              LIB22.setBounds(420, 177, 210, LIB22.getPreferredSize().height);

              //---- LON22 ----
              LON22.setText("@LON22@");
              LON22.setName("LON22");
              panel2.add(LON22);
              LON22.setBounds(630, 177, 55, LON22.getPreferredSize().height);

              //---- EDLI22 ----
              EDLI22.setComponentPopupMenu(BTD);
              EDLI22.setName("EDLI22");
              panel2.add(EDLI22);
              EDLI22.setBounds(685, 175, 26, EDLI22.getPreferredSize().height);

              //---- EDCO22 ----
              EDCO22.setComponentPopupMenu(BTD);
              EDCO22.setName("EDCO22");
              panel2.add(EDCO22);
              EDCO22.setBounds(710, 175, 34, EDCO22.getPreferredSize().height);

              //---- EDT22 ----
              EDT22.setText("@EDT22@");
              EDT22.setName("EDT22");
              panel2.add(EDT22);
              EDT22.setBounds(745, 177, 20, EDT22.getPreferredSize().height);

              //---- LIB23 ----
              LIB23.setText("@LIB23@");
              LIB23.setName("LIB23");
              panel2.add(LIB23);
              LIB23.setBounds(420, 202, 210, LIB23.getPreferredSize().height);

              //---- LON23 ----
              LON23.setText("@LON23@");
              LON23.setName("LON23");
              panel2.add(LON23);
              LON23.setBounds(630, 202, 55, LON23.getPreferredSize().height);

              //---- EDLI23 ----
              EDLI23.setComponentPopupMenu(BTD);
              EDLI23.setName("EDLI23");
              panel2.add(EDLI23);
              EDLI23.setBounds(685, 200, 26, EDLI23.getPreferredSize().height);

              //---- EDCO23 ----
              EDCO23.setComponentPopupMenu(BTD);
              EDCO23.setName("EDCO23");
              panel2.add(EDCO23);
              EDCO23.setBounds(710, 200, 34, EDCO23.getPreferredSize().height);

              //---- EDT23 ----
              EDT23.setText("@EDT23@");
              EDT23.setName("EDT23");
              panel2.add(EDT23);
              EDT23.setBounds(745, 202, 20, EDT23.getPreferredSize().height);

              //---- LIB24 ----
              LIB24.setText("@LIB24@");
              LIB24.setName("LIB24");
              panel2.add(LIB24);
              LIB24.setBounds(420, 227, 210, LIB24.getPreferredSize().height);

              //---- LON24 ----
              LON24.setText("@LON24@");
              LON24.setName("LON24");
              panel2.add(LON24);
              LON24.setBounds(630, 227, 55, LON24.getPreferredSize().height);

              //---- EDLI24 ----
              EDLI24.setComponentPopupMenu(BTD);
              EDLI24.setName("EDLI24");
              panel2.add(EDLI24);
              EDLI24.setBounds(685, 225, 26, EDLI24.getPreferredSize().height);

              //---- EDCO24 ----
              EDCO24.setComponentPopupMenu(BTD);
              EDCO24.setName("EDCO24");
              panel2.add(EDCO24);
              EDCO24.setBounds(710, 225, 34, EDCO24.getPreferredSize().height);

              //---- EDT24 ----
              EDT24.setText("@EDT24@");
              EDT24.setName("EDT24");
              panel2.add(EDT24);
              EDT24.setBounds(745, 227, 20, EDT24.getPreferredSize().height);

              //---- LIB25 ----
              LIB25.setText("@LIB25@");
              LIB25.setName("LIB25");
              panel2.add(LIB25);
              LIB25.setBounds(420, 252, 210, LIB25.getPreferredSize().height);

              //---- LON25 ----
              LON25.setText("@LON25@");
              LON25.setName("LON25");
              panel2.add(LON25);
              LON25.setBounds(630, 252, 55, LON25.getPreferredSize().height);

              //---- EDLI25 ----
              EDLI25.setComponentPopupMenu(BTD);
              EDLI25.setName("EDLI25");
              panel2.add(EDLI25);
              EDLI25.setBounds(685, 250, 26, EDLI25.getPreferredSize().height);

              //---- EDCO25 ----
              EDCO25.setComponentPopupMenu(BTD);
              EDCO25.setName("EDCO25");
              panel2.add(EDCO25);
              EDCO25.setBounds(710, 250, 34, EDCO25.getPreferredSize().height);

              //---- EDT25 ----
              EDT25.setText("@EDT01@");
              EDT25.setName("EDT25");
              panel2.add(EDT25);
              EDT25.setBounds(745, 252, 20, EDT25.getPreferredSize().height);

              //---- LIB26 ----
              LIB26.setText("@LIB26@");
              LIB26.setName("LIB26");
              panel2.add(LIB26);
              LIB26.setBounds(420, 277, 210, LIB26.getPreferredSize().height);

              //---- LON26 ----
              LON26.setText("@LON26@");
              LON26.setName("LON26");
              panel2.add(LON26);
              LON26.setBounds(630, 277, 55, LON26.getPreferredSize().height);

              //---- EDLI26 ----
              EDLI26.setComponentPopupMenu(BTD);
              EDLI26.setName("EDLI26");
              panel2.add(EDLI26);
              EDLI26.setBounds(685, 275, 26, EDLI26.getPreferredSize().height);

              //---- EDCO26 ----
              EDCO26.setComponentPopupMenu(BTD);
              EDCO26.setName("EDCO26");
              panel2.add(EDCO26);
              EDCO26.setBounds(710, 275, 34, EDCO26.getPreferredSize().height);

              //---- EDT26 ----
              EDT26.setText("@EDT01@");
              EDT26.setName("EDT26");
              panel2.add(EDT26);
              EDT26.setBounds(745, 277, 20, EDT26.getPreferredSize().height);

              //---- LIB27 ----
              LIB27.setText("@LIB27@");
              LIB27.setName("LIB27");
              panel2.add(LIB27);
              LIB27.setBounds(420, 302, 210, LIB27.getPreferredSize().height);

              //---- LON27 ----
              LON27.setText("@LON27@");
              LON27.setName("LON27");
              panel2.add(LON27);
              LON27.setBounds(630, 302, 55, LON27.getPreferredSize().height);

              //---- EDLI27 ----
              EDLI27.setComponentPopupMenu(BTD);
              EDLI27.setName("EDLI27");
              panel2.add(EDLI27);
              EDLI27.setBounds(685, 300, 26, EDLI27.getPreferredSize().height);

              //---- EDCO27 ----
              EDCO27.setComponentPopupMenu(BTD);
              EDCO27.setName("EDCO27");
              panel2.add(EDCO27);
              EDCO27.setBounds(710, 300, 34, EDCO27.getPreferredSize().height);

              //---- EDT27 ----
              EDT27.setText("@EDT01@");
              EDT27.setName("EDT27");
              panel2.add(EDT27);
              EDT27.setBounds(745, 302, 20, EDT27.getPreferredSize().height);

              //---- LIB28 ----
              LIB28.setText("@LIB28@");
              LIB28.setName("LIB28");
              panel2.add(LIB28);
              LIB28.setBounds(420, 327, 210, LIB28.getPreferredSize().height);

              //---- LON28 ----
              LON28.setText("@LON28@");
              LON28.setName("LON28");
              panel2.add(LON28);
              LON28.setBounds(630, 327, 55, LON28.getPreferredSize().height);

              //---- EDLI28 ----
              EDLI28.setComponentPopupMenu(BTD);
              EDLI28.setName("EDLI28");
              panel2.add(EDLI28);
              EDLI28.setBounds(685, 325, 26, EDLI28.getPreferredSize().height);

              //---- EDCO28 ----
              EDCO28.setComponentPopupMenu(BTD);
              EDCO28.setName("EDCO28");
              panel2.add(EDCO28);
              EDCO28.setBounds(710, 325, 34, EDCO28.getPreferredSize().height);

              //---- EDT28 ----
              EDT28.setText("@EDT01@");
              EDT28.setName("EDT28");
              panel2.add(EDT28);
              EDT28.setBounds(745, 327, 20, EDT28.getPreferredSize().height);

              //---- LIB29 ----
              LIB29.setText("@LIB29@");
              LIB29.setName("LIB29");
              panel2.add(LIB29);
              LIB29.setBounds(420, 352, 210, LIB29.getPreferredSize().height);

              //---- LON29 ----
              LON29.setText("@LON29@");
              LON29.setName("LON29");
              panel2.add(LON29);
              LON29.setBounds(630, 352, 55, LON29.getPreferredSize().height);

              //---- EDLI29 ----
              EDLI29.setComponentPopupMenu(BTD);
              EDLI29.setName("EDLI29");
              panel2.add(EDLI29);
              EDLI29.setBounds(685, 350, 26, EDLI29.getPreferredSize().height);

              //---- EDCO29 ----
              EDCO29.setComponentPopupMenu(BTD);
              EDCO29.setName("EDCO29");
              panel2.add(EDCO29);
              EDCO29.setBounds(710, 350, 34, EDCO29.getPreferredSize().height);

              //---- EDT29 ----
              EDT29.setText("@EDT01@");
              EDT29.setName("EDT29");
              panel2.add(EDT29);
              EDT29.setBounds(745, 352, 20, EDT29.getPreferredSize().height);

              //---- LIB30 ----
              LIB30.setText("@LIB30@");
              LIB30.setName("LIB30");
              panel2.add(LIB30);
              LIB30.setBounds(420, 377, 210, LIB30.getPreferredSize().height);

              //---- LON30 ----
              LON30.setText("@LON30@");
              LON30.setName("LON30");
              panel2.add(LON30);
              LON30.setBounds(630, 377, 55, LON30.getPreferredSize().height);

              //---- EDLI30 ----
              EDLI30.setComponentPopupMenu(BTD);
              EDLI30.setName("EDLI30");
              panel2.add(EDLI30);
              EDLI30.setBounds(685, 375, 26, EDLI30.getPreferredSize().height);

              //---- EDCO30 ----
              EDCO30.setComponentPopupMenu(BTD);
              EDCO30.setName("EDCO30");
              panel2.add(EDCO30);
              EDCO30.setBounds(710, 375, 34, EDCO30.getPreferredSize().height);

              //---- EDT30 ----
              EDT30.setText("@EDT01@");
              EDT30.setName("EDT30");
              panel2.add(EDT30);
              EDT30.setBounds(745, 377, 20, EDT30.getPreferredSize().height);

              //---- label1 ----
              label1.setText("Zones");
              label1.setName("label1");
              panel2.add(label1);
              label1.setBounds(20, 5, 210, label1.getPreferredSize().height);

              //---- label2 ----
              label2.setText("Zones");
              label2.setName("label2");
              panel2.add(label2);
              label2.setBounds(420, 5, 210, 16);

              //---- label3 ----
              label3.setText("Longueur");
              label3.setName("label3");
              panel2.add(label3);
              label3.setBounds(230, 5, 55, label3.getPreferredSize().height);

              //---- label4 ----
              label4.setText("Longueur");
              label4.setName("label4");
              panel2.add(label4);
              label4.setBounds(630, 5, 55, label4.getPreferredSize().height);

              //---- label5 ----
              label5.setText("Li");
              label5.setName("label5");
              panel2.add(label5);
              label5.setBounds(291, 5, 15, label5.getPreferredSize().height);

              //---- label6 ----
              label6.setText("Col");
              label6.setName("label6");
              panel2.add(label6);
              label6.setBounds(315, 5, 25, label6.getPreferredSize().height);

              //---- label7 ----
              label7.setText("Li");
              label7.setName("label7");
              panel2.add(label7);
              label7.setBounds(690, 5, 15, label7.getPreferredSize().height);

              //---- label8 ----
              label8.setText("Col");
              label8.setName("label8");
              panel2.add(label8);
              label8.setBounds(715, 5, 25, label8.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel2);
            panel2.setBounds(45, 105, 785, 435);

            //---- OBJ_62 ----
            OBJ_62.setText("R\u00e9f\u00e9rence lettre");
            OBJ_62.setName("OBJ_62");
            panel1.add(OBJ_62);
            OBJ_62.setBounds(65, 30, 117, 18);

            //---- EDLIBR ----
            EDLIBR.setComponentPopupMenu(BTD);
            EDLIBR.setName("EDLIBR");
            panel1.add(EDLIBR);
            EDLIBR.setBounds(65, 51, 110, EDLIBR.getPreferredSize().height);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 874, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 552, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_26;
  private XRiTextField INDSOC;
  private JLabel OBJ_28;
  private XRiTextField INDTYP;
  private JLabel OBJ_52;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_66;
  private JLabel OBJ_35;
  private JLabel OBJ_36;
  private JLabel OBJ_65;
  private XRiTextField EDCAR;
  private XRiTextField EDNLI;
  private JPanel panel2;
  private XRiTextField EDLI01;
  private XRiTextField EDCO01;
  private RiZoneSortie LIB01;
  private RiZoneSortie LON01;
  private RiZoneSortie EDT01;
  private RiZoneSortie LIB02;
  private RiZoneSortie LON02;
  private XRiTextField EDLI02;
  private XRiTextField EDCO02;
  private RiZoneSortie EDT02;
  private RiZoneSortie LIB03;
  private RiZoneSortie LON03;
  private XRiTextField EDLI03;
  private XRiTextField EDCO03;
  private RiZoneSortie EDT03;
  private RiZoneSortie LIB04;
  private RiZoneSortie LON04;
  private XRiTextField EDLI04;
  private XRiTextField EDCO04;
  private RiZoneSortie EDT04;
  private RiZoneSortie LIB05;
  private RiZoneSortie LON05;
  private XRiTextField EDLI05;
  private XRiTextField EDCO05;
  private RiZoneSortie EDT05;
  private RiZoneSortie LIB06;
  private RiZoneSortie LON06;
  private XRiTextField EDLI06;
  private XRiTextField EDCO06;
  private RiZoneSortie EDT06;
  private RiZoneSortie LIB07;
  private RiZoneSortie LON07;
  private XRiTextField EDLI07;
  private XRiTextField EDCO07;
  private RiZoneSortie EDT07;
  private RiZoneSortie LIB08;
  private RiZoneSortie LON08;
  private XRiTextField EDLI08;
  private XRiTextField EDCO08;
  private RiZoneSortie EDT08;
  private RiZoneSortie LIB09;
  private RiZoneSortie LON09;
  private XRiTextField EDLI09;
  private XRiTextField EDCO09;
  private RiZoneSortie EDT09;
  private RiZoneSortie LIB10;
  private RiZoneSortie LON10;
  private XRiTextField EDLI10;
  private XRiTextField EDCO10;
  private RiZoneSortie EDT10;
  private RiZoneSortie LIB11;
  private RiZoneSortie LON11;
  private XRiTextField EDLI11;
  private XRiTextField EDCO11;
  private RiZoneSortie EDT11;
  private RiZoneSortie LIB12;
  private RiZoneSortie LON12;
  private XRiTextField EDLI12;
  private XRiTextField EDCO12;
  private RiZoneSortie EDT12;
  private RiZoneSortie LIB13;
  private RiZoneSortie LON13;
  private XRiTextField EDLI13;
  private XRiTextField EDCO13;
  private RiZoneSortie EDT13;
  private RiZoneSortie LIB14;
  private RiZoneSortie LON14;
  private XRiTextField EDLI14;
  private XRiTextField EDCO14;
  private RiZoneSortie EDT14;
  private RiZoneSortie LIB15;
  private RiZoneSortie LON15;
  private XRiTextField EDLI15;
  private XRiTextField EDCO15;
  private RiZoneSortie EDT15;
  private XRiTextField EDLI16;
  private XRiTextField EDCO16;
  private RiZoneSortie LIB16;
  private RiZoneSortie LON16;
  private RiZoneSortie EDT16;
  private RiZoneSortie LIB17;
  private RiZoneSortie LON17;
  private XRiTextField EDLI17;
  private XRiTextField EDCO17;
  private RiZoneSortie EDT17;
  private RiZoneSortie LIB18;
  private RiZoneSortie LON18;
  private XRiTextField EDLI18;
  private XRiTextField EDCO18;
  private RiZoneSortie EDT18;
  private RiZoneSortie LIB19;
  private RiZoneSortie LON19;
  private XRiTextField EDLI19;
  private XRiTextField EDCO19;
  private RiZoneSortie EDT19;
  private RiZoneSortie LIB20;
  private RiZoneSortie LON20;
  private XRiTextField EDLI20;
  private XRiTextField EDCO20;
  private RiZoneSortie EDT20;
  private RiZoneSortie LIB21;
  private RiZoneSortie LON21;
  private XRiTextField EDLI21;
  private XRiTextField EDCO21;
  private RiZoneSortie EDT21;
  private RiZoneSortie LIB22;
  private RiZoneSortie LON22;
  private XRiTextField EDLI22;
  private XRiTextField EDCO22;
  private RiZoneSortie EDT22;
  private RiZoneSortie LIB23;
  private RiZoneSortie LON23;
  private XRiTextField EDLI23;
  private XRiTextField EDCO23;
  private RiZoneSortie EDT23;
  private RiZoneSortie LIB24;
  private RiZoneSortie LON24;
  private XRiTextField EDLI24;
  private XRiTextField EDCO24;
  private RiZoneSortie EDT24;
  private RiZoneSortie LIB25;
  private RiZoneSortie LON25;
  private XRiTextField EDLI25;
  private XRiTextField EDCO25;
  private RiZoneSortie EDT25;
  private RiZoneSortie LIB26;
  private RiZoneSortie LON26;
  private XRiTextField EDLI26;
  private XRiTextField EDCO26;
  private RiZoneSortie EDT26;
  private RiZoneSortie LIB27;
  private RiZoneSortie LON27;
  private XRiTextField EDLI27;
  private XRiTextField EDCO27;
  private RiZoneSortie EDT27;
  private RiZoneSortie LIB28;
  private RiZoneSortie LON28;
  private XRiTextField EDLI28;
  private XRiTextField EDCO28;
  private RiZoneSortie EDT28;
  private RiZoneSortie LIB29;
  private RiZoneSortie LON29;
  private XRiTextField EDLI29;
  private XRiTextField EDCO29;
  private RiZoneSortie EDT29;
  private RiZoneSortie LIB30;
  private RiZoneSortie LON30;
  private XRiTextField EDLI30;
  private XRiTextField EDCO30;
  private RiZoneSortie EDT30;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel OBJ_62;
  private XRiTextField EDLIBR;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
