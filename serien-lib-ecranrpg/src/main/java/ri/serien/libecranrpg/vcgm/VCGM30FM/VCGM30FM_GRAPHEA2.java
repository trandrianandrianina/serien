/*
 * Created by JFormDesigner on Wed Feb 24 16:24:25 CET 2010
 */

package ri.serien.libecranrpg.vcgm.VCGM30FM;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.WindowConstants;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.DefaultKeyedValues;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.general.DatasetUtilities;

import ri.serien.libswing.composantrpg.lexical.RiGraphe;
import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * @author Stéphane Vénéri
 * 
 * 
 *         GRAPHE DE COMPARAISON DE L'EVOLUTION DE 2 ANNEES
 * 
 */
public class VCGM30FM_GRAPHEA2 extends JFrame {
  
  
  String[] annees = new String[5];
  String[][] donnees = new String[5][12];
  private RiGraphe graphe = null;
  private String[] moisLib =
      { "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre" };
  
  /**
   * Constructeur
   * @param annee
   * @param donnee
   */
  public VCGM30FM_GRAPHEA2(String[] annees, String[][] donnees) {
    super();
    initComponents();
    
    this.annees = annees;
    this.donnees = donnees;
    
    initData();
    setSize(1000, 700);
    setBackground(SNCharteGraphique.COULEUR_FOND);
    setVisible(true);
  }
  
  /**
   * Initialise les données pour les graphes
   *
   */
  private void initData() {
    // Préparation des données
    
    DefaultKeyedValues data = new DefaultKeyedValues();
    DefaultKeyedValues data2 = new DefaultKeyedValues();
    DefaultKeyedValues data3 = new DefaultKeyedValues();
    DefaultKeyedValues data4 = new DefaultKeyedValues();
    DefaultKeyedValues data5 = new DefaultKeyedValues();
    
    for (int mois = 0; mois < 12; mois++) {
      data.addValue(moisLib[mois], Double.parseDouble(donnees[0][mois]));
      data2.addValue(moisLib[mois], Double.parseDouble(donnees[1][mois]));
      data3.addValue(moisLib[mois], Double.parseDouble(donnees[2][mois]));
      data4.addValue(moisLib[mois], Double.parseDouble(donnees[3][mois]));
      data5.addValue(moisLib[mois], Double.parseDouble(donnees[4][mois]));
    }
    
    CategoryDataset dataset = DatasetUtilities.createCategoryDataset(annees[0], data);
    CategoryDataset dataset2 = DatasetUtilities.createCategoryDataset(annees[1], data2);
    CategoryDataset dataset3 = DatasetUtilities.createCategoryDataset(annees[2], data3);
    CategoryDataset dataset4 = DatasetUtilities.createCategoryDataset(annees[3], data4);
    CategoryDataset dataset5 = DatasetUtilities.createCategoryDataset(annees[4], data5);
    
    CategoryDataset[] d = { dataset, dataset2, dataset3, dataset4, dataset5 };
    
    JFreeChart graphe = createChart(d);
    
    l_graphe.setIcon(new ImageIcon(graphe.createBufferedImage(l_graphe.getWidth(), l_graphe.getHeight())));
    
  }
  
  public static JFreeChart createChart(CategoryDataset[] datasets) {
    // create the chart...
    
    JFreeChart chart = ChartFactory.createBarChart("Cinq dernières années d'évolution mensuelle pour les comptes sélectionnés", // chart
                                                                                                                                // title
        "mois", // domain axis label
        " ", // range axis label
        datasets[0], // data
        PlotOrientation.VERTICAL, true, // include legend
        true, false);
    
    CategoryPlot plot = (CategoryPlot) chart.getPlot();
    CategoryAxis domainAxis = plot.getDomainAxis();
    
    domainAxis.setLowerMargin(0.02);
    domainAxis.setUpperMargin(0.02);
    domainAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_90);
    
    LineAndShapeRenderer renderer1 = new LineAndShapeRenderer();
    LineAndShapeRenderer renderer2 = new LineAndShapeRenderer();
    LineAndShapeRenderer renderer3 = new LineAndShapeRenderer();
    LineAndShapeRenderer renderer4 = new LineAndShapeRenderer();
    LineAndShapeRenderer renderer5 = new LineAndShapeRenderer();
    
    NumberAxis axis1 = new NumberAxis("");
    // axis2.setNumberFormatOverride(NumberFormat.getPercentInstance());
    plot.setRangeAxis(0, axis1);
    plot.setDataset(0, datasets[0]);
    plot.setRenderer(0, renderer1);
    plot.mapDatasetToRangeAxis(0, 0);
    plot.getRangeAxis(0).setVisible(false);
    axis1.setLabelPaint(Color.magenta);
    axis1.setTickLabelPaint(Color.magenta);
    
    NumberAxis axis2 = new NumberAxis("");
    // axis2.setNumberFormatOverride(NumberFormat.getPercentInstance());
    plot.setRangeAxis(1, axis2);
    plot.setDataset(1, datasets[1]);
    plot.setRenderer(1, renderer2);
    plot.mapDatasetToRangeAxis(1, 1);
    plot.getRangeAxis(1).setVisible(false);
    axis2.setLabelPaint(Color.red);
    axis2.setTickLabelPaint(Color.red);
    
    NumberAxis axis3 = new NumberAxis("");
    plot.setRangeAxis(2, axis3);
    plot.setDataset(2, datasets[2]);
    plot.setRenderer(2, renderer3);
    plot.mapDatasetToRangeAxis(2, 2);
    plot.getRangeAxis(2).setVisible(false);
    axis3.setLabelPaint(Color.cyan);
    axis3.setTickLabelPaint(Color.cyan);
    
    NumberAxis axis4 = new NumberAxis("");
    plot.setRangeAxis(3, axis4);
    plot.setDataset(3, datasets[3]);
    plot.setRenderer(3, renderer4);
    plot.mapDatasetToRangeAxis(3, 3);
    plot.getRangeAxis(3).setVisible(false);
    axis4.setLabelPaint(Color.orange);
    axis4.setTickLabelPaint(Color.orange);
    
    NumberAxis axis5 = new NumberAxis("");
    plot.setRangeAxis(4, axis5);
    plot.setDataset(4, datasets[4]);
    plot.setRenderer(4, renderer5);
    plot.mapDatasetToRangeAxis(4, 4);
    plot.getRangeAxis(4).setVisible(false);
    axis5.setLabelPaint(Color.yellow);
    axis5.setTickLabelPaint(Color.yellow);
    
    plot.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);
    
    // ChartUtilities.applyCurrentTheme(chart);
    
    return chart;
  }
  
  private void MI_CopierActionPerformed(ActionEvent e) {
    graphe.sendToClipBoard(l_graphe.getWidth(), l_graphe.getHeight());
  }
  
  private void MI_EnregistrerActionPerformed(ActionEvent e) {
    graphe.saveGraphe(null, l_graphe.getWidth(), l_graphe.getHeight());
  }
  
  private void thisComponentResized(ComponentEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    l_graphe = new JLabel();
    BTD = new JPopupMenu();
    MI_Copier = new JMenuItem();
    MI_Enregistrer = new JMenuItem();

    //======== this ========
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setTitle("Analyse des comptes");
    setIconImage(null);
    setBackground(new Color(238, 238, 210));
    setName("this");
    addComponentListener(new ComponentAdapter() {
      @Override
      public void componentResized(ComponentEvent e) {
        thisComponentResized(e);
      }
    });
    Container contentPane = getContentPane();

    //---- l_graphe ----
    l_graphe.setComponentPopupMenu(BTD);
    l_graphe.setName("l_graphe");

    GroupLayout contentPaneLayout = new GroupLayout(contentPane);
    contentPane.setLayout(contentPaneLayout);
    contentPaneLayout.setHorizontalGroup(
      contentPaneLayout.createParallelGroup()
        .addComponent(l_graphe, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 932, Short.MAX_VALUE)
    );
    contentPaneLayout.setVerticalGroup(
      contentPaneLayout.createParallelGroup()
        .addComponent(l_graphe, GroupLayout.DEFAULT_SIZE, 628, Short.MAX_VALUE)
    );
    pack();

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- MI_Copier ----
      MI_Copier.setText("Copier");
      MI_Copier.setName("MI_Copier");
      MI_Copier.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          MI_CopierActionPerformed(e);
        }
      });
      BTD.add(MI_Copier);

      //---- MI_Enregistrer ----
      MI_Enregistrer.setText("Enregistrer sous");
      MI_Enregistrer.setName("MI_Enregistrer");
      MI_Enregistrer.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          MI_EnregistrerActionPerformed(e);
        }
      });
      BTD.add(MI_Enregistrer);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel l_graphe;
  private JPopupMenu BTD;
  private JMenuItem MI_Copier;
  private JMenuItem MI_Enregistrer;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
