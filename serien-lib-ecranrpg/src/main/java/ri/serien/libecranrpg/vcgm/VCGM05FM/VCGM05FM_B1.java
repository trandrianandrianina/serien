
package ri.serien.libecranrpg.vcgm.VCGM05FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM05FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  // TODO declarations classe spécifiques...
  private String[] G1PTG_Value = { "", "C", "D", };
  private String[] G1PTG_Title = { "non défini", "Crédit", "Débit", };
  private String[] G1SNS_Value = { "", "C", "D", };
  private String[] G1SNS_Title = { "non défini", "Crédit", "Débit", };
  private String[] G1ED2_Value = { "", "1", };
  private String[] G1ED2_Title = { "Détail", "Pas de détail", };
  private String[] G1TAT_Value = { "0", "1", "2", "9", };
  private String[] G1TAT_Title = { "Pas de message", "Message en gras", "Message en rouge", "Compte désactivé", };
  private String[] G1QTE_Value = { "", "X", "O", };
  private String[] G1QTE_Title = { "Non géré en quantité", "Géré en quantité en plus des valeurs", "Saisie obligatoire des quantités", };
  private String[] G1ED1_Value = { "", "1", "2", };
  private String[] G1ED1_Title = { "(Aucun)", "Avec récapitulatif mensuel", "Avec récapitulatif mensuel et l'édition des soldes cumulés", };
  private String[] G1RPT_Value = { "", "1", "2", "3", "9", };
  private String[] G1RPT_Title = { "(Aucun)", "Lettrage sur numéro de pièce", "Lettrage sur mois d'imputation",
      "Lettrage par similitudes de sommes", "Lettrage par solde à zéro (comptes soldés)", };
  private Icon bloc_couleur = null;
  
  public VCGM05FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // TODO constructeur spécifiques...
    
    // Ajout
    initDiverses();
    G1RPT.setValeurs(G1RPT_Value, G1RPT_Title);
    G1PTG.setValeurs(G1PTG_Value, G1PTG_Title);
    G1ED2.setValeurs(G1ED2_Value, G1ED2_Title);
    G1ED1.setValeurs(G1ED1_Value, G1ED1_Title);
    G1SNS.setValeurs(G1SNS_Value, G1SNS_Title);
    G1TAT.setValeurs(G1TAT_Value, G1TAT_Title);
    G1QTE.setValeurs(G1QTE_Value, G1QTE_Title);
    G1TAO.setValeursSelection("X", " ");
    G1COL.setValeursSelection("X", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    LIBNA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBNA@")).trim());
    LIBNA.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBNA@")).trim());
    LIBSA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBSA@")).trim());
    LIBSA.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBSA@")).trim());
    LIBNCV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBCV@")).trim());
    LIBNCV.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBCV@")).trim());
    OBJ_86.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA1@")).trim());
    OBJ_87.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA2@")).trim());
    OBJ_88.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA3@")).trim());
    OBJ_89.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA4@")).trim());
    OBJ_90.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA5@")).trim());
    OBJ_91.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA6@")).trim());
    UCLEX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UCLEX@")).trim());
    BQEAGE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@BQEAGE@")).trim());
    UCLEY.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UCLEY@")).trim());
    OBJ_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDEV@")).trim());
    OBJ_48.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CPSENS@")).trim());
    OBJ_57.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TTVA@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    bloc_couleur = lexique.chargerImage("images/bloc_notes.png", true);
    button1.setIcon(bloc_couleur);
    
    xTitledPanel4.setVisible(lexique.isTrue("(N41) AND (43)"));
    xTitledPanel5.setVisible(lexique.isTrue("44"));
    OBJ_64.setVisible(lexique.isTrue("N41"));
    
    SECTION_RB.setSelected(!lexique.HostFieldGetData("G1SAN").trim().equalsIgnoreCase(""));
    CLE_RB.setSelected(!lexique.HostFieldGetData("G1CVA").trim().equalsIgnoreCase(""));
    // G1SAN.setEnabled(!OBJ_79.isSelected());
    // G1CVA.setEnabled(!OBJ_76.isSelected());
    // OBJ_76.setEnabled(interpreteurD.analyseExpression("@G1CVA@").equalsIgnoreCase(""));
    // OBJ_79.setEnabled(interpreteurD.analyseExpression("@G1SAN@").equalsIgnoreCase(""));
    // OBJ_79.setVisible( lexique.isTrue("42"));
    
    // G1SAN1.setEnabled(G1SAN.isEditable() && interpreteurD.analyseExpression("@G1CVA@").equalsIgnoreCase(""));
    G1SAN_CHK.setSelected(lexique.HostFieldGetData("G1SAN").equalsIgnoreCase("$SAI"));
    
    OBJ_46.setVisible(lexique.isPresent("CPSOPD"));
    OBJ_48.setVisible(lexique.isPresent("CPSOPD"));
    OBJ_50.setVisible(G1SNS.isVisible());
    OBJ_56.setVisible(G1FIR.isVisible());
    OBJ_55.setVisible(WNAT.isVisible());
    OBJ_49.setVisible(G1DEV.isVisible());
    OBJ_91.setVisible(lexique.isPresent("LIBAA6"));
    OBJ_90.setVisible(lexique.isPresent("LIBAA5"));
    OBJ_89.setVisible(lexique.isPresent("LIBAA4"));
    OBJ_88.setVisible(lexique.isPresent("LIBAA3"));
    OBJ_87.setVisible(lexique.isPresent("LIBAA2"));
    OBJ_86.setVisible(lexique.isPresent("LIBAA1"));
    OBJ_45.setVisible(lexique.isPresent("CPSOPD"));
    OBJ_51.setEnabled(lexique.isPresent("G1CL1"));
    // G1COL.setSelected(lexique.HostFieldGetData("G1COL").equalsIgnoreCase("X"));
    xTitledPanel2.setVisible(!lexique.isTrue("42"));
    xTitledPanel3.setVisible(!lexique.isTrue("41"));
    // G1TAO.setSelected(lexique.HostFieldGetData("G1TAO").equalsIgnoreCase("X"));
    G1CCR.setVisible(lexique.isTrue("N95"));
    G1CVR.setVisible(lexique.isTrue("(N95) AND (50)"));
    label2.setVisible(G1CVR.isVisible());
    OBJ_53.setVisible(G1CCR.isVisible());
    
    panel6.setVisible(lexique.isPresent("WBQE"));
    
    if (lexique.isTrue("72")) {
      desactive.setVisible(true);
      P_Centre.setComponentZOrder(p_desac, 0);
    }
    else {
      desactive.setVisible(false);
    }
    
    String ncg = lexique.HostFieldGetData("INDNCG");
    if ((ncg != null) && (ncg.length() > 0)) {
      riSousMenu7.setEnabled(ncg.charAt(0) == '5');
      riSousMenu10.setEnabled((ncg.charAt(0) == '6') || (ncg.charAt(0) == '7'));
      // OBJ_121.setVisible(ncg.charAt(0)=='6');
      riSousMenu9.setEnabled(ncg.charAt(0) == '6');
      riSousMenu8.setEnabled((lexique.isTrue("(51) OR (52)")) && (ncg.charAt(0) == '4' || ncg.charAt(0) == '5'));
    }
    
    UCLEX.setVisible(!lexique.HostFieldGetData("UCLEX").trim().equalsIgnoreCase(""));
    UCLEY.setVisible(!lexique.HostFieldGetData("UCLEY").trim().equalsIgnoreCase(""));
    
    // découpe de l'IBAN pour présentation standard
    String zoneIban = lexique.HostFieldGetData("WRIE");
    if (zoneIban.length() >= 0) {
      if (zoneIban.length() <= 4) {
        WIBAN1.setText(zoneIban.substring(0, zoneIban.length()));
      }
      else {
        WIBAN1.setText(zoneIban.substring(0, 4));
      }
    }
    if (zoneIban.length() >= 4) {
      if (zoneIban.length() <= 8) {
        WIBAN2.setText(zoneIban.substring(4, zoneIban.length()));
      }
      else {
        WIBAN2.setText(zoneIban.substring(4, 8));
      }
    }
    if (zoneIban.length() >= 8) {
      if (zoneIban.length() <= 12) {
        WIBAN3.setText(zoneIban.substring(8, zoneIban.length()));
      }
      else {
        WIBAN3.setText(zoneIban.substring(8, 12));
      }
    }
    if (zoneIban.length() >= 12) {
      if (zoneIban.length() <= 16) {
        WIBAN4.setText(zoneIban.substring(12, zoneIban.length()));
      }
      else {
        WIBAN4.setText(zoneIban.substring(12, 16));
      }
    }
    if (zoneIban.length() >= 16) {
      if (zoneIban.length() <= 20) {
        WIBAN5.setText(zoneIban.substring(16, zoneIban.length()));
      }
      else {
        WIBAN5.setText(zoneIban.substring(16, 20));
      }
    }
    if (zoneIban.length() >= 20) {
      if (zoneIban.length() <= 24) {
        WIBAN6.setText(zoneIban.substring(20, zoneIban.length()));
      }
      else {
        WIBAN6.setText(zoneIban.substring(20, 24));
      }
    }
    if (zoneIban.length() >= 24) {
      if (zoneIban.length() <= 28) {
        WIBAN7.setText(zoneIban.substring(24, zoneIban.length()));
      }
      else {
        WIBAN7.setText(zoneIban.substring(24, 28));
      }
    }
    if (zoneIban.length() >= 28) {
      WIBAN8.setText(zoneIban.substring(28, zoneIban.length()));
    }
    
    // FICHE REVISION
    button1.setVisible(lexique.isTrue("79"));
    
    // Menu contextuel
    OBJ_27.setVisible(!lexique.isTrue("53"));
    

    
    
    desactive.setIcon(lexique.chargerImage("images/desact.png", true));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
    riMenu_bt4.setIcon(lexique.chargerImage("images/stats.png", true));
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    
    // Titre
    setTitle(p_bpresentation.getText());
  }
  
  @Override
  public void getData() {
    if (!SECTION_RB.isSelected()) {
      G1SAN.setText("");
    }
    if (!CLE_RB.isSelected()) {
      G1CVA.setText("");
    }
    super.getData();
    
    if (G1SAN_CHK.isSelected()) {
      lexique.HostFieldPutData("G1SAN", 0, "$SAI");
    }
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F17");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void riSousMenu_bt16ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt18ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, "CPT");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt19ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, "SIM");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt20ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void riSousMenu_bt21ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, "GRA");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void SECTION_RBActionPerformed(ActionEvent e) {
    if (SECTION_RB.isSelected()) {
      G1CVA.setText("");
    }
  }
  
  private void OBJ_27ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void G1SAN1ActionPerformed(ActionEvent e) {
    if (G1SAN_CHK.isSelected()) {
      G1SAN.setText("$SAI");
    }
    else {
      G1SAN.setText("");
    }
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void CLE_RBActionPerformed(ActionEvent e) {
    if (CLE_RB.isSelected()) {
      G1SAN.setText("");
    }
  }
  
  private void button1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_40 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_41 = new JLabel();
    INDNCG = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu18 = new RiSousMenu();
    riSousMenu_bt18 = new RiSousMenu_bt();
    riSousMenu19 = new RiSousMenu();
    riSousMenu_bt19 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    riSousMenu16 = new RiSousMenu();
    riSousMenu_bt16 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riMenu4 = new RiMenu();
    riMenu_bt4 = new RiMenu_bt();
    riSousMenu20 = new RiSousMenu();
    riSousMenu_bt20 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu21 = new RiSousMenu();
    riSousMenu_bt21 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    P_Centre = new JLayeredPane();
    panel3 = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_60 = new JLabel();
    G1ED1 = new XRiComboBox();
    OBJ_63 = new JLabel();
    G1ED2 = new XRiComboBox();
    EEZA1 = new XRiTextField();
    EEZA2 = new XRiTextField();
    EEZA3 = new XRiTextField();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_69 = new JLabel();
    G1PTG = new XRiComboBox();
    OBJ_71 = new JLabel();
    G1RPT = new XRiComboBox();
    xTitledPanel3 = new JXTitledPanel();
    SECTION_RB = new JRadioButton();
    G1SAN = new XRiTextField();
    CLE_RB = new JRadioButton();
    G1CVA = new XRiTextField();
    G1NAT = new XRiTextField();
    G1SAN_CHK = new JCheckBox();
    G1TAO = new XRiCheckBox();
    OBJ_81 = new JLabel();
    LIBNA = new RiZoneSortie();
    LIBSA = new RiZoneSortie();
    LIBNCV = new RiZoneSortie();
    xTitledPanel4 = new JXTitledPanel();
    OBJ_86 = new JLabel();
    OBJ_87 = new JLabel();
    OBJ_88 = new JLabel();
    OBJ_89 = new JLabel();
    OBJ_90 = new JLabel();
    OBJ_91 = new JLabel();
    G1AA1 = new XRiTextField();
    G1AA2 = new XRiTextField();
    G1AA3 = new XRiTextField();
    G1AA4 = new XRiTextField();
    G1AA5 = new XRiTextField();
    G1AA6 = new XRiTextField();
    xTitledPanel5 = new JXTitledPanel();
    OBJ_43 = new JPanel();
    panel4 = new JPanel();
    BQELIB = new XRiTextField();
    OBJ_22_OBJ_24 = new JLabel();
    panel6 = new JPanel();
    OBJ_22_OBJ_22 = new JLabel();
    WBQE = new XRiTextField();
    WGUI = new XRiTextField();
    WCPT = new XRiTextField();
    WRIB = new XRiTextField();
    UCLEX = new RiZoneSortie();
    OBJ_22_OBJ_25 = new JLabel();
    OBJ_22_OBJ_26 = new JLabel();
    OBJ_22_OBJ_27 = new JLabel();
    OBJ_22_OBJ_28 = new JLabel();
    panel5 = new JPanel();
    BQEAGE = new RiZoneSortie();
    OBJ_40_OBJ_40 = new JLabel();
    WIBAN1 = new RiZoneSortie();
    WIBAN2 = new RiZoneSortie();
    WIBAN3 = new RiZoneSortie();
    WIBAN4 = new RiZoneSortie();
    WIBAN5 = new RiZoneSortie();
    WIBAN6 = new RiZoneSortie();
    WIBAN7 = new RiZoneSortie();
    WIBAN8 = new RiZoneSortie();
    UCLEY = new RiZoneSortie();
    OBJ_22_OBJ_23 = new JLabel();
    p_desac = new JPanel();
    desactive = new JLabel();
    panel1 = new JXTitledPanel();
    OBJ_45 = new JLabel();
    OBJ_46 = new RiZoneSortie();
    CPSOPD = new XRiTextField();
    OBJ_48 = new RiZoneSortie();
    OBJ_51 = new JLabel();
    G1CL1 = new XRiTextField();
    OBJ_53 = new JLabel();
    G1CCR = new XRiTextField();
    G1CVR = new XRiTextField();
    OBJ_56 = new JLabel();
    G1FIR = new XRiTextField();
    OBJ_55 = new JLabel();
    WNAT = new XRiTextField();
    label2 = new JLabel();
    panel2 = new JPanel();
    G1QTE = new XRiComboBox();
    G1TAT = new XRiComboBox();
    G1LIB = new XRiTextField();
    G1SNS = new XRiComboBox();
    G1ATT = new XRiTextField();
    G1COL = new XRiCheckBox();
    OBJ_54 = new JLabel();
    OBJ_52 = new JLabel();
    OBJ_49 = new JLabel();
    OBJ_50 = new JLabel();
    G1DEV = new XRiTextField();
    button1 = new JButton();
    OBJ_64 = new JLabel();
    EETVA = new XRiTextField();
    OBJ_57 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_27 = new JMenuItem();
    OBJ_26 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Compte g\u00e9n\u00e9ral");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_40 ----
          OBJ_40.setText("Soci\u00e9t\u00e9");
          OBJ_40.setName("OBJ_40");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_41 ----
          OBJ_41.setText("Num\u00e9ro de compte");
          OBJ_41.setName("OBJ_41");

          //---- INDNCG ----
          INDNCG.setComponentPopupMenu(BTD);
          INDNCG.setName("INDNCG");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_40)
                .addGap(59, 59, 59)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36)
                .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(INDNCG, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(345, 345, 345))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_40))
              .addGroup(p_tete_gaucheLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(INDNCG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(OBJ_41))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Visualisation comptes");
              riSousMenu_bt6.setToolTipText("Visualisation des comptes");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Soldes  date de valeur");
              riSousMenu_bt7.setToolTipText("Soldes en date de valeur (Pour les comptes de classe 5)");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Domiciliation bancaire");
              riSousMenu_bt8.setToolTipText("Domiciliation bancaire");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu18 ========
            {
              riSousMenu18.setName("riSousMenu18");

              //---- riSousMenu_bt18 ----
              riSousMenu_bt18.setText("Affichage des compteurs");
              riSousMenu_bt18.setToolTipText("Affichage des compteurs");
              riSousMenu_bt18.setName("riSousMenu_bt18");
              riSousMenu_bt18.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt18ActionPerformed(e);
                }
              });
              riSousMenu18.add(riSousMenu_bt18);
            }
            menus_haut.add(riSousMenu18);

            //======== riSousMenu19 ========
            {
              riSousMenu19.setName("riSousMenu19");

              //---- riSousMenu_bt19 ----
              riSousMenu_bt19.setText("Compteurs+pr\u00e9visionnel");
              riSousMenu_bt19.setToolTipText("Affichage des compteurs plus pr\u00e9visionnel");
              riSousMenu_bt19.setName("riSousMenu_bt19");
              riSousMenu_bt19.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt19ActionPerformed(e);
                }
              });
              riSousMenu19.add(riSousMenu_bt19);
            }
            menus_haut.add(riSousMenu19);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Association type de frais");
              riSousMenu_bt9.setToolTipText("Association type de frais");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");

              //---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Fiche abonnement");
              riSousMenu_bt15.setToolTipText("Fiche abonnement");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed(e);
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);

            //======== riSousMenu16 ========
            {
              riSousMenu16.setName("riSousMenu16");

              //---- riSousMenu_bt16 ----
              riSousMenu_bt16.setText("Fiche de r\u00e9vision");
              riSousMenu_bt16.setToolTipText("Fiche de r\u00e9vision");
              riSousMenu_bt16.setName("riSousMenu_bt16");
              riSousMenu_bt16.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt16ActionPerformed(e);
                }
              });
              riSousMenu16.add(riSousMenu_bt16);
            }
            menus_haut.add(riSousMenu16);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Documents li\u00e9s");
              riSousMenu_bt14.setToolTipText("Documents li\u00e9s");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);

            //======== riMenu4 ========
            {
              riMenu4.setName("riMenu4");

              //---- riMenu_bt4 ----
              riMenu_bt4.setText("Statistiques");
              riMenu_bt4.setName("riMenu_bt4");
              riMenu4.add(riMenu_bt4);
            }
            menus_haut.add(riMenu4);

            //======== riSousMenu20 ========
            {
              riSousMenu20.setName("riSousMenu20");

              //---- riSousMenu_bt20 ----
              riSousMenu_bt20.setText("Analyse des comptes");
              riSousMenu_bt20.setToolTipText("Analyse des comptes (graphes statistiques)");
              riSousMenu_bt20.setName("riSousMenu_bt20");
              riSousMenu_bt20.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt20ActionPerformed(e);
                }
              });
              riSousMenu20.add(riSousMenu_bt20);
            }
            menus_haut.add(riSousMenu20);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Analyse budg\u00e9taire");
              riSousMenu_bt10.setToolTipText("Visualisation analyse budg\u00e9taire");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);

            //======== riSousMenu21 ========
            {
              riSousMenu21.setName("riSousMenu21");

              //---- riSousMenu_bt21 ----
              riSousMenu_bt21.setText("Evolution du solde");
              riSousMenu_bt21.setToolTipText("Evolution du solde sur ce compte (graphes statistiques)");
              riSousMenu_bt21.setName("riSousMenu_bt21");
              riSousMenu_bt21.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt21ActionPerformed(e);
                }
              });
              riSousMenu21.add(riSousMenu_bt21);
            }
            menus_haut.add(riSousMenu21);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(970, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(970, 600));
          p_contenu.setName("p_contenu");

          //======== P_Centre ========
          {
            P_Centre.setPreferredSize(new Dimension(950, 600));
            P_Centre.setName("P_Centre");

            //======== panel3 ========
            {
              panel3.setOpaque(false);
              panel3.setName("panel3");

              //======== xTitledPanel1 ========
              {
                xTitledPanel1.setTitle("Options d'\u00e9dition");
                xTitledPanel1.setBorder(new DropShadowBorder());
                xTitledPanel1.setName("xTitledPanel1");
                Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
                xTitledPanel1ContentContainer.setLayout(null);

                //---- OBJ_60 ----
                OBJ_60.setText("Historique d\u00e9taill\u00e9");
                OBJ_60.setName("OBJ_60");
                xTitledPanel1ContentContainer.add(OBJ_60);
                OBJ_60.setBounds(10, 5, 110, 30);

                //---- G1ED1 ----
                G1ED1.setComponentPopupMenu(BTD);
                G1ED1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                G1ED1.setName("G1ED1");
                xTitledPanel1ContentContainer.add(G1ED1);
                G1ED1.setBounds(130, 5, 305, G1ED1.getPreferredSize().height);

                //---- OBJ_63 ----
                OBJ_63.setText("Historique centralis\u00e9");
                OBJ_63.setName("OBJ_63");
                xTitledPanel1ContentContainer.add(OBJ_63);
                OBJ_63.setBounds(10, 30, 130, 30);

                //---- G1ED2 ----
                G1ED2.setComponentPopupMenu(BTD);
                G1ED2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                G1ED2.setName("G1ED2");
                xTitledPanel1ContentContainer.add(G1ED2);
                G1ED2.setBounds(130, 32, 107, G1ED2.getPreferredSize().height);

                //---- EEZA1 ----
                EEZA1.setComponentPopupMenu(BTD);
                EEZA1.setName("EEZA1");
                xTitledPanel1ContentContainer.add(EEZA1);
                EEZA1.setBounds(10, 65, 160, EEZA1.getPreferredSize().height);

                //---- EEZA2 ----
                EEZA2.setComponentPopupMenu(BTD);
                EEZA2.setName("EEZA2");
                xTitledPanel1ContentContainer.add(EEZA2);
                EEZA2.setBounds(175, 65, 160, EEZA2.getPreferredSize().height);

                //---- EEZA3 ----
                EEZA3.setComponentPopupMenu(BTD);
                EEZA3.setName("EEZA3");
                xTitledPanel1ContentContainer.add(EEZA3);
                EEZA3.setBounds(10, 95, 160, EEZA3.getPreferredSize().height);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                    Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = xTitledPanel1ContentContainer.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
                  xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
                }
              }

              //======== xTitledPanel2 ========
              {
                xTitledPanel2.setTitle("Lettrage");
                xTitledPanel2.setBorder(new DropShadowBorder());
                xTitledPanel2.setName("xTitledPanel2");
                Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
                xTitledPanel2ContentContainer.setLayout(null);

                //---- OBJ_69 ----
                OBJ_69.setText("Sens normal");
                OBJ_69.setName("OBJ_69");
                xTitledPanel2ContentContainer.add(OBJ_69);
                OBJ_69.setBounds(15, 10, 100, 26);

                //---- G1PTG ----
                G1PTG.setComponentPopupMenu(BTD);
                G1PTG.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                G1PTG.setName("G1PTG");
                xTitledPanel2ContentContainer.add(G1PTG);
                G1PTG.setBounds(100, 10, 109, G1PTG.getPreferredSize().height);

                //---- OBJ_71 ----
                OBJ_71.setText("R\u00e9f\u00e9rence");
                OBJ_71.setName("OBJ_71");
                xTitledPanel2ContentContainer.add(OBJ_71);
                OBJ_71.setBounds(15, 38, 90, 30);

                //---- G1RPT ----
                G1RPT.setComponentPopupMenu(BTD);
                G1RPT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                G1RPT.setName("G1RPT");
                xTitledPanel2ContentContainer.add(G1RPT);
                G1RPT.setBounds(100, 40, 340, G1RPT.getPreferredSize().height);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                    Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = xTitledPanel2ContentContainer.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
                  xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
                }
              }

              //======== xTitledPanel3 ========
              {
                xTitledPanel3.setTitle("Affectation automatique");
                xTitledPanel3.setBorder(new DropShadowBorder());
                xTitledPanel3.setName("xTitledPanel3");
                Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
                xTitledPanel3ContentContainer.setLayout(null);

                //---- SECTION_RB ----
                SECTION_RB.setText("Section");
                SECTION_RB.setName("SECTION_RB");
                SECTION_RB.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    SECTION_RBActionPerformed(e);
                  }
                });
                xTitledPanel3ContentContainer.add(SECTION_RB);
                SECTION_RB.setBounds(15, 10, 70, SECTION_RB.getPreferredSize().height);

                //---- G1SAN ----
                G1SAN.setComponentPopupMenu(BTD);
                G1SAN.setName("G1SAN");
                xTitledPanel3ContentContainer.add(G1SAN);
                G1SAN.setBounds(85, 5, 50, G1SAN.getPreferredSize().height);

                //---- CLE_RB ----
                CLE_RB.setText("Cl\u00e9 de ventilation par sections");
                CLE_RB.setName("CLE_RB");
                CLE_RB.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    CLE_RBActionPerformed(e);
                  }
                });
                xTitledPanel3ContentContainer.add(CLE_RB);
                CLE_RB.setBounds(15, 45, 200, CLE_RB.getPreferredSize().height);

                //---- G1CVA ----
                G1CVA.setComponentPopupMenu(BTD);
                G1CVA.setName("G1CVA");
                xTitledPanel3ContentContainer.add(G1CVA);
                G1CVA.setBounds(95, 63, 40, G1CVA.getPreferredSize().height);

                //---- G1NAT ----
                G1NAT.setComponentPopupMenu(BTD);
                G1NAT.setName("G1NAT");
                xTitledPanel3ContentContainer.add(G1NAT);
                G1NAT.setBounds(65, 98, 70, G1NAT.getPreferredSize().height);

                //---- G1SAN_CHK ----
                G1SAN_CHK.setText("Saisie obligatoire d'une section");
                G1SAN_CHK.setComponentPopupMenu(BTD);
                G1SAN_CHK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                G1SAN_CHK.setName("G1SAN_CHK");
                G1SAN_CHK.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    G1SAN1ActionPerformed(e);
                  }
                });
                xTitledPanel3ContentContainer.add(G1SAN_CHK);
                G1SAN_CHK.setBounds(260, 135, 215, G1SAN_CHK.getPreferredSize().height);

                //---- G1TAO ----
                G1TAO.setText("Saisie d'un code affaire obligatoire");
                G1TAO.setComponentPopupMenu(BTD);
                G1TAO.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                G1TAO.setName("G1TAO");
                xTitledPanel3ContentContainer.add(G1TAO);
                G1TAO.setBounds(15, 135, 230, G1TAO.getPreferredSize().height);

                //---- OBJ_81 ----
                OBJ_81.setText("Nature");
                OBJ_81.setName("OBJ_81");
                xTitledPanel3ContentContainer.add(OBJ_81);
                OBJ_81.setBounds(15, 98, 50, 28);

                //---- LIBNA ----
                LIBNA.setText("@LIBNA@");
                LIBNA.setToolTipText("@LIBNA@");
                LIBNA.setName("LIBNA");
                xTitledPanel3ContentContainer.add(LIBNA);
                LIBNA.setBounds(135, 100, 314, 24);

                //---- LIBSA ----
                LIBSA.setText("@LIBSA@");
                LIBSA.setToolTipText("@LIBSA@");
                LIBSA.setName("LIBSA");
                xTitledPanel3ContentContainer.add(LIBSA);
                LIBSA.setBounds(135, 7, 314, LIBSA.getPreferredSize().height);

                //---- LIBNCV ----
                LIBNCV.setText("@LIBCV@");
                LIBNCV.setToolTipText("@LIBCV@");
                LIBNCV.setName("LIBNCV");
                xTitledPanel3ContentContainer.add(LIBNCV);
                LIBNCV.setBounds(135, 65, 314, LIBNCV.getPreferredSize().height);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
                    Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = xTitledPanel3ContentContainer.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
                  xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
                }
              }

              //======== xTitledPanel4 ========
              {
                xTitledPanel4.setTitle("Axes pour analyse");
                xTitledPanel4.setBorder(new DropShadowBorder());
                xTitledPanel4.setName("xTitledPanel4");
                Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
                xTitledPanel4ContentContainer.setLayout(null);

                //---- OBJ_86 ----
                OBJ_86.setText("@LIBAA1@");
                OBJ_86.setForeground(Color.darkGray);
                OBJ_86.setHorizontalTextPosition(SwingConstants.CENTER);
                OBJ_86.setHorizontalAlignment(SwingConstants.CENTER);
                OBJ_86.setFont(OBJ_86.getFont().deriveFont(OBJ_86.getFont().getStyle() | Font.BOLD));
                OBJ_86.setName("OBJ_86");
                xTitledPanel4ContentContainer.add(OBJ_86);
                OBJ_86.setBounds(15, 20, 70, 25);

                //---- OBJ_87 ----
                OBJ_87.setText("@LIBAA2@");
                OBJ_87.setForeground(Color.darkGray);
                OBJ_87.setHorizontalTextPosition(SwingConstants.CENTER);
                OBJ_87.setHorizontalAlignment(SwingConstants.CENTER);
                OBJ_87.setFont(OBJ_87.getFont().deriveFont(OBJ_87.getFont().getStyle() | Font.BOLD));
                OBJ_87.setName("OBJ_87");
                xTitledPanel4ContentContainer.add(OBJ_87);
                OBJ_87.setBounds(85, 20, 70, 25);

                //---- OBJ_88 ----
                OBJ_88.setText("@LIBAA3@");
                OBJ_88.setForeground(Color.darkGray);
                OBJ_88.setHorizontalTextPosition(SwingConstants.CENTER);
                OBJ_88.setHorizontalAlignment(SwingConstants.CENTER);
                OBJ_88.setFont(OBJ_88.getFont().deriveFont(OBJ_88.getFont().getStyle() | Font.BOLD));
                OBJ_88.setName("OBJ_88");
                xTitledPanel4ContentContainer.add(OBJ_88);
                OBJ_88.setBounds(159, 20, 70, 25);

                //---- OBJ_89 ----
                OBJ_89.setText("@LIBAA4@");
                OBJ_89.setForeground(Color.darkGray);
                OBJ_89.setHorizontalTextPosition(SwingConstants.CENTER);
                OBJ_89.setHorizontalAlignment(SwingConstants.CENTER);
                OBJ_89.setFont(OBJ_89.getFont().deriveFont(OBJ_89.getFont().getStyle() | Font.BOLD));
                OBJ_89.setName("OBJ_89");
                xTitledPanel4ContentContainer.add(OBJ_89);
                OBJ_89.setBounds(231, 20, 70, 25);

                //---- OBJ_90 ----
                OBJ_90.setText("@LIBAA5@");
                OBJ_90.setForeground(Color.darkGray);
                OBJ_90.setHorizontalTextPosition(SwingConstants.CENTER);
                OBJ_90.setHorizontalAlignment(SwingConstants.CENTER);
                OBJ_90.setFont(OBJ_90.getFont().deriveFont(OBJ_90.getFont().getStyle() | Font.BOLD));
                OBJ_90.setName("OBJ_90");
                xTitledPanel4ContentContainer.add(OBJ_90);
                OBJ_90.setBounds(303, 20, 70, 25);

                //---- OBJ_91 ----
                OBJ_91.setText("@LIBAA6@");
                OBJ_91.setForeground(Color.darkGray);
                OBJ_91.setHorizontalTextPosition(SwingConstants.CENTER);
                OBJ_91.setHorizontalAlignment(SwingConstants.CENTER);
                OBJ_91.setFont(OBJ_91.getFont().deriveFont(OBJ_91.getFont().getStyle() | Font.BOLD));
                OBJ_91.setName("OBJ_91");
                xTitledPanel4ContentContainer.add(OBJ_91);
                OBJ_91.setBounds(375, 20, 70, 25);

                //---- G1AA1 ----
                G1AA1.setComponentPopupMenu(BTD);
                G1AA1.setName("G1AA1");
                xTitledPanel4ContentContainer.add(G1AA1);
                G1AA1.setBounds(15, 45, 70, G1AA1.getPreferredSize().height);

                //---- G1AA2 ----
                G1AA2.setComponentPopupMenu(BTD);
                G1AA2.setName("G1AA2");
                xTitledPanel4ContentContainer.add(G1AA2);
                G1AA2.setBounds(87, 45, 70, G1AA2.getPreferredSize().height);

                //---- G1AA3 ----
                G1AA3.setComponentPopupMenu(BTD);
                G1AA3.setName("G1AA3");
                xTitledPanel4ContentContainer.add(G1AA3);
                G1AA3.setBounds(159, 45, 70, G1AA3.getPreferredSize().height);

                //---- G1AA4 ----
                G1AA4.setComponentPopupMenu(BTD);
                G1AA4.setName("G1AA4");
                xTitledPanel4ContentContainer.add(G1AA4);
                G1AA4.setBounds(231, 45, 70, G1AA4.getPreferredSize().height);

                //---- G1AA5 ----
                G1AA5.setComponentPopupMenu(BTD);
                G1AA5.setName("G1AA5");
                xTitledPanel4ContentContainer.add(G1AA5);
                G1AA5.setBounds(303, 45, 70, G1AA5.getPreferredSize().height);

                //---- G1AA6 ----
                G1AA6.setComponentPopupMenu(BTD);
                G1AA6.setName("G1AA6");
                xTitledPanel4ContentContainer.add(G1AA6);
                G1AA6.setBounds(375, 45, 70, G1AA6.getPreferredSize().height);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < xTitledPanel4ContentContainer.getComponentCount(); i++) {
                    Rectangle bounds = xTitledPanel4ContentContainer.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = xTitledPanel4ContentContainer.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  xTitledPanel4ContentContainer.setMinimumSize(preferredSize);
                  xTitledPanel4ContentContainer.setPreferredSize(preferredSize);
                }
              }

              //======== xTitledPanel5 ========
              {
                xTitledPanel5.setTitle("Domiciliation");
                xTitledPanel5.setName("xTitledPanel5");
                Container xTitledPanel5ContentContainer = xTitledPanel5.getContentContainer();
                xTitledPanel5ContentContainer.setLayout(null);

                //======== OBJ_43 ========
                {
                  OBJ_43.setOpaque(false);
                  OBJ_43.setName("OBJ_43");
                  OBJ_43.setLayout(null);

                  //======== panel4 ========
                  {
                    panel4.setOpaque(false);
                    panel4.setName("panel4");
                    panel4.setLayout(null);

                    //---- BQELIB ----
                    BQELIB.setComponentPopupMenu(BTD);
                    BQELIB.setName("BQELIB");
                    panel4.add(BQELIB);
                    BQELIB.setBounds(114, 25, 220, BQELIB.getPreferredSize().height);

                    //---- OBJ_22_OBJ_24 ----
                    OBJ_22_OBJ_24.setText("Banque");
                    OBJ_22_OBJ_24.setName("OBJ_22_OBJ_24");
                    panel4.add(OBJ_22_OBJ_24);
                    OBJ_22_OBJ_24.setBounds(5, 29, 110, 20);

                    //======== panel6 ========
                    {
                      panel6.setOpaque(false);
                      panel6.setName("panel6");
                      panel6.setLayout(null);

                      //---- OBJ_22_OBJ_22 ----
                      OBJ_22_OBJ_22.setText("RIB");
                      OBJ_22_OBJ_22.setName("OBJ_22_OBJ_22");
                      panel6.add(OBJ_22_OBJ_22);
                      OBJ_22_OBJ_22.setBounds(5, 25, 55, 20);

                      //---- WBQE ----
                      WBQE.setComponentPopupMenu(BTD);
                      WBQE.setToolTipText("num\u00e9ro de banque");
                      WBQE.setName("WBQE");
                      panel6.add(WBQE);
                      WBQE.setBounds(60, 20, 50, WBQE.getPreferredSize().height);

                      //---- WGUI ----
                      WGUI.setComponentPopupMenu(BTD);
                      WGUI.setToolTipText("num\u00e9ro de guichet");
                      WGUI.setName("WGUI");
                      panel6.add(WGUI);
                      WGUI.setBounds(110, 20, 50, WGUI.getPreferredSize().height);

                      //---- WCPT ----
                      WCPT.setComponentPopupMenu(BTD);
                      WCPT.setToolTipText("num\u00e9ro de compte");
                      WCPT.setName("WCPT");
                      panel6.add(WCPT);
                      WCPT.setBounds(165, 20, 121, WCPT.getPreferredSize().height);

                      //---- WRIB ----
                      WRIB.setComponentPopupMenu(BTD);
                      WRIB.setToolTipText("Cl\u00e9");
                      WRIB.setName("WRIB");
                      panel6.add(WRIB);
                      WRIB.setBounds(320, 20, 28, WRIB.getPreferredSize().height);

                      //---- UCLEX ----
                      UCLEX.setText("@UCLEX@");
                      UCLEX.setForeground(Color.red);
                      UCLEX.setToolTipText("Cl\u00e9 r\u00e9elle");
                      UCLEX.setName("UCLEX");
                      panel6.add(UCLEX);
                      UCLEX.setBounds(455, 20, 25, UCLEX.getPreferredSize().height);

                      //---- OBJ_22_OBJ_25 ----
                      OBJ_22_OBJ_25.setText("Banque");
                      OBJ_22_OBJ_25.setHorizontalAlignment(SwingConstants.CENTER);
                      OBJ_22_OBJ_25.setFont(OBJ_22_OBJ_25.getFont().deriveFont(OBJ_22_OBJ_25.getFont().getStyle() | Font.BOLD));
                      OBJ_22_OBJ_25.setName("OBJ_22_OBJ_25");
                      panel6.add(OBJ_22_OBJ_25);
                      OBJ_22_OBJ_25.setBounds(60, 0, 50, 20);

                      //---- OBJ_22_OBJ_26 ----
                      OBJ_22_OBJ_26.setText("Guichet");
                      OBJ_22_OBJ_26.setHorizontalAlignment(SwingConstants.CENTER);
                      OBJ_22_OBJ_26.setFont(OBJ_22_OBJ_26.getFont().deriveFont(OBJ_22_OBJ_26.getFont().getStyle() | Font.BOLD));
                      OBJ_22_OBJ_26.setName("OBJ_22_OBJ_26");
                      panel6.add(OBJ_22_OBJ_26);
                      OBJ_22_OBJ_26.setBounds(110, 0, 50, 20);

                      //---- OBJ_22_OBJ_27 ----
                      OBJ_22_OBJ_27.setText("Num\u00e9ro de compte");
                      OBJ_22_OBJ_27.setHorizontalAlignment(SwingConstants.CENTER);
                      OBJ_22_OBJ_27.setFont(OBJ_22_OBJ_27.getFont().deriveFont(OBJ_22_OBJ_27.getFont().getStyle() | Font.BOLD));
                      OBJ_22_OBJ_27.setName("OBJ_22_OBJ_27");
                      panel6.add(OBJ_22_OBJ_27);
                      OBJ_22_OBJ_27.setBounds(165, 0, 121, 20);

                      //---- OBJ_22_OBJ_28 ----
                      OBJ_22_OBJ_28.setText("Cl\u00e9");
                      OBJ_22_OBJ_28.setHorizontalAlignment(SwingConstants.CENTER);
                      OBJ_22_OBJ_28.setFont(OBJ_22_OBJ_28.getFont().deriveFont(OBJ_22_OBJ_28.getFont().getStyle() | Font.BOLD));
                      OBJ_22_OBJ_28.setName("OBJ_22_OBJ_28");
                      panel6.add(OBJ_22_OBJ_28);
                      OBJ_22_OBJ_28.setBounds(320, 0, 28, 20);

                      {
                        // compute preferred size
                        Dimension preferredSize = new Dimension();
                        for(int i = 0; i < panel6.getComponentCount(); i++) {
                          Rectangle bounds = panel6.getComponent(i).getBounds();
                          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                        }
                        Insets insets = panel6.getInsets();
                        preferredSize.width += insets.right;
                        preferredSize.height += insets.bottom;
                        panel6.setMinimumSize(preferredSize);
                        panel6.setPreferredSize(preferredSize);
                      }
                    }
                    panel4.add(panel6);
                    panel6.setBounds(350, 5, 490, 50);

                    {
                      // compute preferred size
                      Dimension preferredSize = new Dimension();
                      for(int i = 0; i < panel4.getComponentCount(); i++) {
                        Rectangle bounds = panel4.getComponent(i).getBounds();
                        preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                        preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                      }
                      Insets insets = panel4.getInsets();
                      preferredSize.width += insets.right;
                      preferredSize.height += insets.bottom;
                      panel4.setMinimumSize(preferredSize);
                      panel4.setPreferredSize(preferredSize);
                    }
                  }
                  OBJ_43.add(panel4);
                  panel4.setBounds(10, 5, 875, 60);

                  //======== panel5 ========
                  {
                    panel5.setOpaque(false);
                    panel5.setName("panel5");
                    panel5.setLayout(null);

                    //---- BQEAGE ----
                    BQEAGE.setComponentPopupMenu(BTD);
                    BQEAGE.setText("@BQEAGE@");
                    BQEAGE.setName("BQEAGE");
                    panel5.add(BQEAGE);
                    BQEAGE.setBounds(114, 15, 220, BQEAGE.getPreferredSize().height);

                    //---- OBJ_40_OBJ_40 ----
                    OBJ_40_OBJ_40.setText("IBAN");
                    OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");
                    panel5.add(OBJ_40_OBJ_40);
                    OBJ_40_OBJ_40.setBounds(355, 19, 56, 20);

                    //---- WIBAN1 ----
                    WIBAN1.setComponentPopupMenu(BTD);
                    WIBAN1.setName("WIBAN1");
                    panel5.add(WIBAN1);
                    WIBAN1.setBounds(410, 15, 50, WIBAN1.getPreferredSize().height);

                    //---- WIBAN2 ----
                    WIBAN2.setComponentPopupMenu(BTD);
                    WIBAN2.setName("WIBAN2");
                    panel5.add(WIBAN2);
                    WIBAN2.setBounds(462, 15, 50, WIBAN2.getPreferredSize().height);

                    //---- WIBAN3 ----
                    WIBAN3.setComponentPopupMenu(BTD);
                    WIBAN3.setName("WIBAN3");
                    panel5.add(WIBAN3);
                    WIBAN3.setBounds(514, 15, 50, WIBAN3.getPreferredSize().height);

                    //---- WIBAN4 ----
                    WIBAN4.setComponentPopupMenu(BTD);
                    WIBAN4.setName("WIBAN4");
                    panel5.add(WIBAN4);
                    WIBAN4.setBounds(566, 15, 50, WIBAN4.getPreferredSize().height);

                    //---- WIBAN5 ----
                    WIBAN5.setComponentPopupMenu(BTD);
                    WIBAN5.setName("WIBAN5");
                    panel5.add(WIBAN5);
                    WIBAN5.setBounds(618, 15, 50, WIBAN5.getPreferredSize().height);

                    //---- WIBAN6 ----
                    WIBAN6.setComponentPopupMenu(BTD);
                    WIBAN6.setName("WIBAN6");
                    panel5.add(WIBAN6);
                    WIBAN6.setBounds(670, 15, 50, WIBAN6.getPreferredSize().height);

                    //---- WIBAN7 ----
                    WIBAN7.setComponentPopupMenu(BTD);
                    WIBAN7.setName("WIBAN7");
                    panel5.add(WIBAN7);
                    WIBAN7.setBounds(722, 15, 50, WIBAN7.getPreferredSize().height);

                    //---- WIBAN8 ----
                    WIBAN8.setComponentPopupMenu(BTD);
                    WIBAN8.setName("WIBAN8");
                    panel5.add(WIBAN8);
                    WIBAN8.setBounds(774, 15, 28, WIBAN8.getPreferredSize().height);

                    //---- UCLEY ----
                    UCLEY.setText("@UCLEY@");
                    UCLEY.setForeground(Color.red);
                    UCLEY.setName("UCLEY");
                    panel5.add(UCLEY);
                    UCLEY.setBounds(805, 17, 25, UCLEY.getPreferredSize().height);

                    //---- OBJ_22_OBJ_23 ----
                    OBJ_22_OBJ_23.setText("Guichet ou SWIFT");
                    OBJ_22_OBJ_23.setName("OBJ_22_OBJ_23");
                    panel5.add(OBJ_22_OBJ_23);
                    OBJ_22_OBJ_23.setBounds(5, 19, 110, 20);

                    {
                      // compute preferred size
                      Dimension preferredSize = new Dimension();
                      for(int i = 0; i < panel5.getComponentCount(); i++) {
                        Rectangle bounds = panel5.getComponent(i).getBounds();
                        preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                        preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                      }
                      Insets insets = panel5.getInsets();
                      preferredSize.width += insets.right;
                      preferredSize.height += insets.bottom;
                      panel5.setMinimumSize(preferredSize);
                      panel5.setPreferredSize(preferredSize);
                    }
                  }
                  OBJ_43.add(panel5);
                  panel5.setBounds(10, 70, 875, 55);

                  {
                    // compute preferred size
                    Dimension preferredSize = new Dimension();
                    for(int i = 0; i < OBJ_43.getComponentCount(); i++) {
                      Rectangle bounds = OBJ_43.getComponent(i).getBounds();
                      preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                      preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                    }
                    Insets insets = OBJ_43.getInsets();
                    preferredSize.width += insets.right;
                    preferredSize.height += insets.bottom;
                    OBJ_43.setMinimumSize(preferredSize);
                    OBJ_43.setPreferredSize(preferredSize);
                  }
                }
                xTitledPanel5ContentContainer.add(OBJ_43);
                OBJ_43.setBounds(5, 2, 895, 155);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < xTitledPanel5ContentContainer.getComponentCount(); i++) {
                    Rectangle bounds = xTitledPanel5ContentContainer.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = xTitledPanel5ContentContainer.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  xTitledPanel5ContentContainer.setMinimumSize(preferredSize);
                  xTitledPanel5ContentContainer.setPreferredSize(preferredSize);
                }
              }

              GroupLayout panel3Layout = new GroupLayout(panel3);
              panel3.setLayout(panel3Layout);
              panel3Layout.setHorizontalGroup(
                panel3Layout.createParallelGroup()
                  .addGroup(panel3Layout.createSequentialGroup()
                    .addGroup(panel3Layout.createParallelGroup()
                      .addGroup(panel3Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(panel3Layout.createParallelGroup()
                          .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 445, GroupLayout.PREFERRED_SIZE)
                          .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panel3Layout.createParallelGroup()
                          .addComponent(xTitledPanel3, GroupLayout.DEFAULT_SIZE, 0, Short.MAX_VALUE)
                          .addComponent(xTitledPanel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                      .addGroup(panel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(xTitledPanel5, GroupLayout.PREFERRED_SIZE, 923, GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                    .addContainerGap())
              );
              panel3Layout.setVerticalGroup(
                panel3Layout.createParallelGroup()
                  .addGroup(panel3Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addGroup(panel3Layout.createParallelGroup()
                      .addGroup(panel3Layout.createSequentialGroup()
                        .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel3Layout.createSequentialGroup()
                        .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 191, GroupLayout.PREFERRED_SIZE)))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(xTitledPanel5, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
                    .addContainerGap())
              );
            }
            P_Centre.add(panel3, JLayeredPane.DEFAULT_LAYER);
            panel3.setBounds(10, 185, 940, 395);

            //======== p_desac ========
            {
              p_desac.setOpaque(false);
              p_desac.setName("p_desac");
              p_desac.setLayout(null);

              //---- desactive ----
              desactive.setHorizontalAlignment(SwingConstants.CENTER);
              desactive.setName("desactive");
              p_desac.add(desactive);
              desactive.setBounds(10, 10, 905, 550);

              //======== panel1 ========
              {
                panel1.setTitle("Compte");
                panel1.setBorder(new DropShadowBorder());
                panel1.setName("panel1");
                Container panel1ContentContainer = panel1.getContentContainer();

                //---- OBJ_45 ----
                OBJ_45.setText("Solde en");
                OBJ_45.setName("OBJ_45");

                //---- OBJ_46 ----
                OBJ_46.setText("@WDEV@");
                OBJ_46.setForeground(Color.darkGray);
                OBJ_46.setFont(OBJ_46.getFont().deriveFont(OBJ_46.getFont().getStyle() | Font.BOLD));
                OBJ_46.setName("OBJ_46");

                //---- CPSOPD ----
                CPSOPD.setComponentPopupMenu(BTD);
                CPSOPD.setHorizontalAlignment(SwingConstants.RIGHT);
                CPSOPD.setName("CPSOPD");

                //---- OBJ_48 ----
                OBJ_48.setText("@CPSENS@");
                OBJ_48.setName("OBJ_48");

                //---- OBJ_51 ----
                OBJ_51.setText("Cl\u00e9 de classement");
                OBJ_51.setName("OBJ_51");

                //---- G1CL1 ----
                G1CL1.setComponentPopupMenu(BTD);
                G1CL1.setName("G1CL1");

                //---- OBJ_53 ----
                OBJ_53.setText("Compte reporting");
                OBJ_53.setName("OBJ_53");

                //---- G1CCR ----
                G1CCR.setComponentPopupMenu(BTD);
                G1CCR.setName("G1CCR");

                //---- G1CVR ----
                G1CVR.setComponentPopupMenu(BTD);
                G1CVR.setName("G1CVR");

                //---- OBJ_56 ----
                OBJ_56.setText("Firme");
                OBJ_56.setName("OBJ_56");

                //---- G1FIR ----
                G1FIR.setComponentPopupMenu(BTD);
                G1FIR.setName("G1FIR");

                //---- OBJ_55 ----
                OBJ_55.setText("Nature");
                OBJ_55.setName("OBJ_55");

                //---- WNAT ----
                WNAT.setComponentPopupMenu(BTD);
                WNAT.setName("WNAT");

                //---- label2 ----
                label2.setText("Cl\u00e9");
                label2.setName("label2");

                //======== panel2 ========
                {
                  panel2.setOpaque(false);
                  panel2.setName("panel2");

                  //---- G1QTE ----
                  G1QTE.setComponentPopupMenu(BTD);
                  G1QTE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                  G1QTE.setName("G1QTE");

                  //---- G1TAT ----
                  G1TAT.setComponentPopupMenu(BTD);
                  G1TAT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                  G1TAT.setName("G1TAT");

                  //---- G1LIB ----
                  G1LIB.setComponentPopupMenu(BTD);
                  G1LIB.setFont(G1LIB.getFont().deriveFont(G1LIB.getFont().getStyle() | Font.BOLD, G1LIB.getFont().getSize() + 2f));
                  G1LIB.setName("G1LIB");

                  //---- G1SNS ----
                  G1SNS.setComponentPopupMenu(BTD);
                  G1SNS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                  G1SNS.setName("G1SNS");

                  //---- G1ATT ----
                  G1ATT.setComponentPopupMenu(BTD);
                  G1ATT.setName("G1ATT");

                  //---- G1COL ----
                  G1COL.setText("Compte collectif");
                  G1COL.setComponentPopupMenu(BTD);
                  G1COL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                  G1COL.setName("G1COL");

                  //---- OBJ_54 ----
                  OBJ_54.setText("Message");
                  OBJ_54.setName("OBJ_54");

                  //---- OBJ_52 ----
                  OBJ_52.setText("Quantit\u00e9");
                  OBJ_52.setName("OBJ_52");

                  //---- OBJ_49 ----
                  OBJ_49.setText("Devise");
                  OBJ_49.setName("OBJ_49");

                  //---- OBJ_50 ----
                  OBJ_50.setText("Sens");
                  OBJ_50.setName("OBJ_50");

                  //---- G1DEV ----
                  G1DEV.setComponentPopupMenu(BTD);
                  G1DEV.setName("G1DEV");

                  GroupLayout panel2Layout = new GroupLayout(panel2);
                  panel2.setLayout(panel2Layout);
                  panel2Layout.setHorizontalGroup(
                    panel2Layout.createParallelGroup()
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addGroup(panel2Layout.createParallelGroup()
                          .addComponent(G1LIB, GroupLayout.PREFERRED_SIZE, 415, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                            .addGap(15, 15, 15)
                            .addComponent(G1DEV, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                            .addGap(20, 20, 20)
                            .addComponent(G1COL, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
                            .addGap(5, 5, 5)
                            .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                            .addGap(10, 10, 10)
                            .addComponent(G1SNS, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                            .addGap(15, 15, 15)
                            .addComponent(G1QTE, GroupLayout.PREFERRED_SIZE, 340, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                            .addGap(15, 15, 15)
                            .addComponent(G1ATT, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
                            .addGap(5, 5, 5)
                            .addComponent(G1TAT, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE))))
                  );
                  panel2Layout.setVerticalGroup(
                    panel2Layout.createParallelGroup()
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(G1LIB, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)
                        .addGroup(panel2Layout.createParallelGroup()
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(6, 6, 6)
                            .addComponent(OBJ_49))
                          .addComponent(G1DEV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(5, 5, 5)
                            .addComponent(G1COL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(6, 6, 6)
                            .addComponent(OBJ_50))
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(1, 1, 1)
                            .addComponent(G1SNS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                        .addGap(3, 3, 3)
                        .addGroup(panel2Layout.createParallelGroup()
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(5, 5, 5)
                            .addComponent(OBJ_52))
                          .addComponent(G1QTE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGap(3, 3, 3)
                        .addGroup(panel2Layout.createParallelGroup()
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(6, 6, 6)
                            .addComponent(OBJ_54))
                          .addComponent(G1ATT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(1, 1, 1)
                            .addComponent(G1TAT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  );
                }

                //---- button1 ----
                button1.setContentAreaFilled(false);
                button1.setToolTipText("La fiche de r\u00e9vision contient des informations");
                button1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                button1.setName("button1");
                button1.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    button1ActionPerformed(e);
                  }
                });

                //---- OBJ_64 ----
                OBJ_64.setText("TVA");
                OBJ_64.setName("OBJ_64");

                //---- EETVA ----
                EETVA.setComponentPopupMenu(BTD);
                EETVA.setName("EETVA");

                //---- OBJ_57 ----
                OBJ_57.setText("@TTVA@");
                OBJ_57.setHorizontalAlignment(SwingConstants.RIGHT);
                OBJ_57.setName("OBJ_57");

                GroupLayout panel1ContentContainerLayout = new GroupLayout(panel1ContentContainer);
                panel1ContentContainer.setLayout(panel1ContentContainerLayout);
                panel1ContentContainerLayout.setHorizontalGroup(
                  panel1ContentContainerLayout.createParallelGroup()
                    .addGroup(panel1ContentContainerLayout.createSequentialGroup()
                      .addContainerGap()
                      .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(33, 33, 33)
                      .addGroup(panel1ContentContainerLayout.createParallelGroup()
                        .addGroup(panel1ContentContainerLayout.createSequentialGroup()
                          .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                          .addGap(35, 35, 35)
                          .addComponent(G1FIR, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addGap(50, 50, 50)
                          .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addGap(5, 5, 5)
                          .addComponent(WNAT, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 46, Short.MAX_VALUE)
                          .addComponent(button1, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
                          .addContainerGap())
                        .addGroup(panel1ContentContainerLayout.createSequentialGroup()
                          .addGroup(panel1ContentContainerLayout.createParallelGroup()
                            .addGroup(panel1ContentContainerLayout.createSequentialGroup()
                              .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                              .addGap(35, 35, 35)
                              .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                              .addGap(5, 5, 5)
                              .addComponent(CPSOPD, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
                              .addGap(5, 5, 5)
                              .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
                            .addGroup(panel1ContentContainerLayout.createSequentialGroup()
                              .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                              .addGap(35, 35, 35)
                              .addComponent(G1CL1, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
                              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                              .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                              .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                              .addComponent(EETVA, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                              .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
                            .addGroup(panel1ContentContainerLayout.createSequentialGroup()
                              .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                              .addGap(35, 35, 35)
                              .addComponent(G1CCR, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
                              .addGap(26, 26, 26)
                              .addComponent(label2, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                              .addGap(5, 5, 5)
                              .addComponent(G1CVR, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)))
                          .addGap(0, 28, Short.MAX_VALUE))))
                );
                panel1ContentContainerLayout.setVerticalGroup(
                  panel1ContentContainerLayout.createParallelGroup()
                    .addGroup(panel1ContentContainerLayout.createSequentialGroup()
                      .addGroup(panel1ContentContainerLayout.createParallelGroup()
                        .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(panel1ContentContainerLayout.createSequentialGroup()
                          .addGap(15, 15, 15)
                          .addGroup(panel1ContentContainerLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                            .addComponent(button1, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
                            .addGroup(panel1ContentContainerLayout.createSequentialGroup()
                              .addGroup(panel1ContentContainerLayout.createParallelGroup()
                                .addGroup(panel1ContentContainerLayout.createSequentialGroup()
                                  .addGap(7, 7, 7)
                                  .addComponent(OBJ_45))
                                .addGroup(panel1ContentContainerLayout.createSequentialGroup()
                                  .addGap(3, 3, 3)
                                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addGroup(panel1ContentContainerLayout.createSequentialGroup()
                                  .addGap(1, 1, 1)
                                  .addComponent(CPSOPD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addGroup(panel1ContentContainerLayout.createSequentialGroup()
                                  .addGap(3, 3, 3)
                                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                              .addGap(2, 2, 2)
                              .addGroup(panel1ContentContainerLayout.createParallelGroup()
                                .addGroup(panel1ContentContainerLayout.createSequentialGroup()
                                  .addGap(6, 6, 6)
                                  .addComponent(OBJ_51))
                                .addGroup(panel1ContentContainerLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                  .addComponent(G1CL1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                  .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                                  .addComponent(EETVA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                  .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                              .addGap(1, 1, 1)
                              .addGroup(panel1ContentContainerLayout.createParallelGroup()
                                .addComponent(G1CCR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addComponent(G1CVR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addGroup(panel1ContentContainerLayout.createSequentialGroup()
                                  .addGap(6, 6, 6)
                                  .addGroup(panel1ContentContainerLayout.createParallelGroup()
                                    .addComponent(OBJ_53)
                                    .addComponent(label2))))
                              .addGap(1, 1, 1)
                              .addGroup(panel1ContentContainerLayout.createParallelGroup()
                                .addComponent(G1FIR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addComponent(WNAT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addGroup(panel1ContentContainerLayout.createSequentialGroup()
                                  .addGap(6, 6, 6)
                                  .addGroup(panel1ContentContainerLayout.createParallelGroup()
                                    .addComponent(OBJ_56)
                                    .addComponent(OBJ_55))))))))
                      .addContainerGap(7, Short.MAX_VALUE))
                );
              }
              p_desac.add(panel1);
              panel1.setBounds(0, -5, 925, 170);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < p_desac.getComponentCount(); i++) {
                  Rectangle bounds = p_desac.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = p_desac.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                p_desac.setMinimumSize(preferredSize);
                p_desac.setPreferredSize(preferredSize);
              }
            }
            P_Centre.add(p_desac, JLayeredPane.DEFAULT_LAYER);
            p_desac.setBounds(new Rectangle(new Point(20, 15), p_desac.getPreferredSize()));
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(P_Centre, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(P_Centre, GroupLayout.DEFAULT_SIZE, 581, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_27 ----
      OBJ_27.setText("Choix possibles");
      OBJ_27.setName("OBJ_27");
      OBJ_27.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_27ActionPerformed(e);
        }
      });
      BTD.add(OBJ_27);

      //---- OBJ_26 ----
      OBJ_26.setText("Aide en ligne");
      OBJ_26.setName("OBJ_26");
      OBJ_26.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_26ActionPerformed(e);
        }
      });
      BTD.add(OBJ_26);
    }

    //---- SECTION_GRP ----
    ButtonGroup SECTION_GRP = new ButtonGroup();
    SECTION_GRP.add(SECTION_RB);
    SECTION_GRP.add(CLE_RB);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_40;
  private XRiTextField INDETB;
  private JLabel OBJ_41;
  private XRiTextField INDNCG;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu18;
  private RiSousMenu_bt riSousMenu_bt18;
  private RiSousMenu riSousMenu19;
  private RiSousMenu_bt riSousMenu_bt19;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private RiSousMenu riSousMenu16;
  private RiSousMenu_bt riSousMenu_bt16;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiMenu riMenu4;
  private RiMenu_bt riMenu_bt4;
  private RiSousMenu riSousMenu20;
  private RiSousMenu_bt riSousMenu_bt20;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu21;
  private RiSousMenu_bt riSousMenu_bt21;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JLayeredPane P_Centre;
  private JPanel panel3;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_60;
  private XRiComboBox G1ED1;
  private JLabel OBJ_63;
  private XRiComboBox G1ED2;
  private XRiTextField EEZA1;
  private XRiTextField EEZA2;
  private XRiTextField EEZA3;
  private JXTitledPanel xTitledPanel2;
  private JLabel OBJ_69;
  private XRiComboBox G1PTG;
  private JLabel OBJ_71;
  private XRiComboBox G1RPT;
  private JXTitledPanel xTitledPanel3;
  private JRadioButton SECTION_RB;
  private XRiTextField G1SAN;
  private JRadioButton CLE_RB;
  private XRiTextField G1CVA;
  private XRiTextField G1NAT;
  private JCheckBox G1SAN_CHK;
  private XRiCheckBox G1TAO;
  private JLabel OBJ_81;
  private RiZoneSortie LIBNA;
  private RiZoneSortie LIBSA;
  private RiZoneSortie LIBNCV;
  private JXTitledPanel xTitledPanel4;
  private JLabel OBJ_86;
  private JLabel OBJ_87;
  private JLabel OBJ_88;
  private JLabel OBJ_89;
  private JLabel OBJ_90;
  private JLabel OBJ_91;
  private XRiTextField G1AA1;
  private XRiTextField G1AA2;
  private XRiTextField G1AA3;
  private XRiTextField G1AA4;
  private XRiTextField G1AA5;
  private XRiTextField G1AA6;
  private JXTitledPanel xTitledPanel5;
  private JPanel OBJ_43;
  private JPanel panel4;
  private XRiTextField BQELIB;
  private JLabel OBJ_22_OBJ_24;
  private JPanel panel6;
  private JLabel OBJ_22_OBJ_22;
  private XRiTextField WBQE;
  private XRiTextField WGUI;
  private XRiTextField WCPT;
  private XRiTextField WRIB;
  private RiZoneSortie UCLEX;
  private JLabel OBJ_22_OBJ_25;
  private JLabel OBJ_22_OBJ_26;
  private JLabel OBJ_22_OBJ_27;
  private JLabel OBJ_22_OBJ_28;
  private JPanel panel5;
  private RiZoneSortie BQEAGE;
  private JLabel OBJ_40_OBJ_40;
  private RiZoneSortie WIBAN1;
  private RiZoneSortie WIBAN2;
  private RiZoneSortie WIBAN3;
  private RiZoneSortie WIBAN4;
  private RiZoneSortie WIBAN5;
  private RiZoneSortie WIBAN6;
  private RiZoneSortie WIBAN7;
  private RiZoneSortie WIBAN8;
  private RiZoneSortie UCLEY;
  private JLabel OBJ_22_OBJ_23;
  private JPanel p_desac;
  private JLabel desactive;
  private JXTitledPanel panel1;
  private JLabel OBJ_45;
  private RiZoneSortie OBJ_46;
  private XRiTextField CPSOPD;
  private RiZoneSortie OBJ_48;
  private JLabel OBJ_51;
  private XRiTextField G1CL1;
  private JLabel OBJ_53;
  private XRiTextField G1CCR;
  private XRiTextField G1CVR;
  private JLabel OBJ_56;
  private XRiTextField G1FIR;
  private JLabel OBJ_55;
  private XRiTextField WNAT;
  private JLabel label2;
  private JPanel panel2;
  private XRiComboBox G1QTE;
  private XRiComboBox G1TAT;
  private XRiTextField G1LIB;
  private XRiComboBox G1SNS;
  private XRiTextField G1ATT;
  private XRiCheckBox G1COL;
  private JLabel OBJ_54;
  private JLabel OBJ_52;
  private JLabel OBJ_49;
  private JLabel OBJ_50;
  private XRiTextField G1DEV;
  private JButton button1;
  private JLabel OBJ_64;
  private XRiTextField EETVA;
  private RiZoneSortie OBJ_57;
  private JPopupMenu BTD;
  private JMenuItem OBJ_27;
  private JMenuItem OBJ_26;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
