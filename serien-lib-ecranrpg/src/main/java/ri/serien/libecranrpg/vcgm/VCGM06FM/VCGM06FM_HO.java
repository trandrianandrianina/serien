
package ri.serien.libecranrpg.vcgm.VCGM06FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGM06FM_HO extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM06FM_HO(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Fiche honoraires"));
    
    

  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    A1LIB = new XRiTextField();
    HOPRO = new XRiTextField();
    OBJ_17 = new JLabel();
    HOSIR = new XRiTextField();
    OBJ_20 = new JLabel();
    OBJ_18 = new JLabel();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_48 = new JLabel();
    OBJ_51 = new JLabel();
    HOCOU = new XRiTextField();
    HOJET = new XRiTextField();
    HODIN = new XRiTextField();
    OBJ_45 = new JLabel();
    OBJ_42 = new JLabel();
    OBJ_54 = new JLabel();
    OBJ_56 = new JLabel();
    OBJ_43 = new JLabel();
    HOREM = new XRiTextField();
    HORIM = new XRiTextField();
    HOCOM = new XRiTextField();
    HORIS = new XRiTextField();
    OBJ_46 = new JLabel();
    OBJ_59 = new JLabel();
    OBJ_44 = new JLabel();
    OBJ_55 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_52 = new JLabel();
    OBJ_57 = new JLabel();
    OBJ_53 = new JLabel();
    OBJ_49 = new JLabel();
    OBJ_50 = new JLabel();
    HOPAR = new XRiTextField();
    HODAU = new XRiTextField();
    HORMU = new XRiTextField();
    HOAVN = new XRiTextField();
    HOTVA = new XRiTextField();
    HOTVD = new XRiTextField();
    HORTS = new XRiTextField();
    OBJ_58 = new JLabel();
    HONAT = new XRiTextField();
    HORST = new XRiTextField();
    HOVAC = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(805, 470));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== xTitledPanel1 ========
        {
          xTitledPanel1.setTitle("Fiche honoraires");
          xTitledPanel1.setBorder(new DropShadowBorder());
          xTitledPanel1.setName("xTitledPanel1");
          Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

          //---- A1LIB ----
          A1LIB.setName("A1LIB");

          //---- HOPRO ----
          HOPRO.setComponentPopupMenu(BTD);
          HOPRO.setName("HOPRO");

          //---- OBJ_17 ----
          OBJ_17.setText("Nom du fournisseur");
          OBJ_17.setName("OBJ_17");

          //---- HOSIR ----
          HOSIR.setComponentPopupMenu(BTD);
          HOSIR.setName("HOSIR");

          //---- OBJ_20 ----
          OBJ_20.setText("Num\u00e9ro SIRET");
          OBJ_20.setName("OBJ_20");

          //---- OBJ_18 ----
          OBJ_18.setText("Profession");
          OBJ_18.setName("OBJ_18");

          GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
          xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
          xTitledPanel1ContentContainerLayout.setHorizontalGroup(
            xTitledPanel1ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addComponent(OBJ_17, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, 0)
                    .addComponent(A1LIB, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addComponent(OBJ_18, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, 0)
                    .addComponent(HOPRO, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, 0)
                    .addComponent(HOSIR, GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE))))
          );
          xTitledPanel1ContentContainerLayout.setVerticalGroup(
            xTitledPanel1ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_17, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(A1LIB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_18, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(HOPRO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(HOSIR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_contenu.add(xTitledPanel1);
        xTitledPanel1.setBounds(10, 10, 618, 128);

        //======== xTitledPanel2 ========
        {
          xTitledPanel2.setBorder(new DropShadowBorder());
          xTitledPanel2.setTitle("D\u00e9tail des honoraires");
          xTitledPanel2.setName("xTitledPanel2");
          Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
          xTitledPanel2ContentContainer.setLayout(null);

          //---- OBJ_48 ----
          OBJ_48.setText("Jetons de pr\u00e9sences");
          OBJ_48.setName("OBJ_48");
          xTitledPanel2ContentContainer.add(OBJ_48);
          OBJ_48.setBounds(15, 61, 145, 28);

          //---- OBJ_51 ----
          OBJ_51.setText("Droits d'inventeur");
          OBJ_51.setName("OBJ_51");
          xTitledPanel2ContentContainer.add(OBJ_51);
          OBJ_51.setBounds(15, 89, 145, 28);

          //---- HOCOU ----
          HOCOU.setComponentPopupMenu(BTD);
          HOCOU.setName("HOCOU");
          xTitledPanel2ContentContainer.add(HOCOU);
          HOCOU.setBounds(160, 33, 96, HOCOU.getPreferredSize().height);

          //---- HOJET ----
          HOJET.setComponentPopupMenu(BTD);
          HOJET.setName("HOJET");
          xTitledPanel2ContentContainer.add(HOJET);
          HOJET.setBounds(160, 61, 96, HOJET.getPreferredSize().height);

          //---- HODIN ----
          HODIN.setComponentPopupMenu(BTD);
          HODIN.setName("HODIN");
          xTitledPanel2ContentContainer.add(HODIN);
          HODIN.setBounds(160, 89, 96, HODIN.getPreferredSize().height);

          //---- OBJ_45 ----
          OBJ_45.setText("Courtages");
          OBJ_45.setName("OBJ_45");
          xTitledPanel2ContentContainer.add(OBJ_45);
          OBJ_45.setBounds(15, 33, 145, 28);

          //---- OBJ_42 ----
          OBJ_42.setText("Vacations");
          OBJ_42.setName("OBJ_42");
          xTitledPanel2ContentContainer.add(OBJ_42);
          OBJ_42.setBounds(15, 5, 145, 28);

          //---- OBJ_54 ----
          OBJ_54.setText("Indem. remboursement");
          OBJ_54.setName("OBJ_54");
          xTitledPanel2ContentContainer.add(OBJ_54);
          OBJ_54.setBounds(15, 117, 145, 28);

          //---- OBJ_56 ----
          OBJ_56.setText("Retenues imp\u00f4ts");
          OBJ_56.setName("OBJ_56");
          xTitledPanel2ContentContainer.add(OBJ_56);
          OBJ_56.setBounds(15, 145, 145, 28);

          //---- OBJ_43 ----
          OBJ_43.setText("Commissions");
          OBJ_43.setName("OBJ_43");
          xTitledPanel2ContentContainer.add(OBJ_43);
          OBJ_43.setBounds(15, 173, 145, 28);

          //---- HOREM ----
          HOREM.setComponentPopupMenu(BTD);
          HOREM.setName("HOREM");
          xTitledPanel2ContentContainer.add(HOREM);
          HOREM.setBounds(160, 117, 96, HOREM.getPreferredSize().height);

          //---- HORIM ----
          HORIM.setComponentPopupMenu(BTD);
          HORIM.setName("HORIM");
          xTitledPanel2ContentContainer.add(HORIM);
          HORIM.setBounds(160, 145, 96, HORIM.getPreferredSize().height);

          //---- HOCOM ----
          HOCOM.setComponentPopupMenu(BTD);
          HOCOM.setName("HOCOM");
          xTitledPanel2ContentContainer.add(HOCOM);
          HOCOM.setBounds(160, 173, 96, HOCOM.getPreferredSize().height);

          //---- HORIS ----
          HORIS.setComponentPopupMenu(BTD);
          HORIS.setName("HORIS");
          xTitledPanel2ContentContainer.add(HORIS);
          HORIS.setBounds(160, 201, 96, HORIS.getPreferredSize().height);

          //---- OBJ_46 ----
          OBJ_46.setText("Ristournes");
          OBJ_46.setName("OBJ_46");
          xTitledPanel2ContentContainer.add(OBJ_46);
          OBJ_46.setBounds(15, 201, 145, 28);

          //---- OBJ_59 ----
          OBJ_59.setText("(Renseignements particuliers)");
          OBJ_59.setName("OBJ_59");
          xTitledPanel2ContentContainer.add(OBJ_59);
          OBJ_59.setBounds(250, 254, 192, 28);

          //---- OBJ_44 ----
          OBJ_44.setText("TVA droits d'auteur");
          OBJ_44.setName("OBJ_44");
          xTitledPanel2ContentContainer.add(OBJ_44);
          OBJ_44.setBounds(315, 117, 135, 28);

          //---- OBJ_55 ----
          OBJ_55.setText("Avantages en nature");
          OBJ_55.setName("OBJ_55");
          xTitledPanel2ContentContainer.add(OBJ_55);
          OBJ_55.setBounds(315, 61, 135, 28);

          //---- OBJ_47 ----
          OBJ_47.setText("Retenue source D.A");
          OBJ_47.setName("OBJ_47");
          xTitledPanel2ContentContainer.add(OBJ_47);
          OBJ_47.setBounds(315, 145, 135, 28);

          //---- OBJ_52 ----
          OBJ_52.setText("Autres r\u00e9mun\u00e9ration");
          OBJ_52.setName("OBJ_52");
          xTitledPanel2ContentContainer.add(OBJ_52);
          OBJ_52.setBounds(315, 33, 135, 28);

          //---- OBJ_57 ----
          OBJ_57.setText("TVA nette / Imp\u00f4ts");
          OBJ_57.setName("OBJ_57");
          xTitledPanel2ContentContainer.add(OBJ_57);
          OBJ_57.setBounds(315, 89, 135, 28);

          //---- OBJ_53 ----
          OBJ_53.setText("Type taux retenue");
          OBJ_53.setName("OBJ_53");
          xTitledPanel2ContentContainer.add(OBJ_53);
          OBJ_53.setBounds(315, 201, 135, 28);

          //---- OBJ_49 ----
          OBJ_49.setText("Droits d'auteur");
          OBJ_49.setName("OBJ_49");
          xTitledPanel2ContentContainer.add(OBJ_49);
          OBJ_49.setBounds(315, 5, 135, 28);

          //---- OBJ_50 ----
          OBJ_50.setText("Nature droits d'auteur");
          OBJ_50.setName("OBJ_50");
          xTitledPanel2ContentContainer.add(OBJ_50);
          OBJ_50.setBounds(315, 173, 135, 28);

          //---- HOPAR ----
          HOPAR.setComponentPopupMenu(BTD);
          HOPAR.setFont(new Font("Courier New", Font.PLAIN, 12));
          HOPAR.setName("HOPAR");
          xTitledPanel2ContentContainer.add(HOPAR);
          HOPAR.setBounds(450, 255, 85, HOPAR.getPreferredSize().height);

          //---- HODAU ----
          HODAU.setComponentPopupMenu(BTD);
          HODAU.setName("HODAU");
          xTitledPanel2ContentContainer.add(HODAU);
          HODAU.setBounds(450, 5, 96, HODAU.getPreferredSize().height);

          //---- HORMU ----
          HORMU.setComponentPopupMenu(BTD);
          HORMU.setName("HORMU");
          xTitledPanel2ContentContainer.add(HORMU);
          HORMU.setBounds(450, 33, 96, HORMU.getPreferredSize().height);

          //---- HOAVN ----
          HOAVN.setComponentPopupMenu(BTD);
          HOAVN.setName("HOAVN");
          xTitledPanel2ContentContainer.add(HOAVN);
          HOAVN.setBounds(450, 61, 96, HOAVN.getPreferredSize().height);

          //---- HOTVA ----
          HOTVA.setComponentPopupMenu(BTD);
          HOTVA.setName("HOTVA");
          xTitledPanel2ContentContainer.add(HOTVA);
          HOTVA.setBounds(450, 89, 96, HOTVA.getPreferredSize().height);

          //---- HOTVD ----
          HOTVD.setComponentPopupMenu(BTD);
          HOTVD.setName("HOTVD");
          xTitledPanel2ContentContainer.add(HOTVD);
          HOTVD.setBounds(450, 117, 96, HOTVD.getPreferredSize().height);

          //---- HORTS ----
          HORTS.setComponentPopupMenu(BTD);
          HORTS.setName("HORTS");
          xTitledPanel2ContentContainer.add(HORTS);
          HORTS.setBounds(450, 145, 96, HORTS.getPreferredSize().height);

          //---- OBJ_58 ----
          OBJ_58.setText("NLVA FRPRD");
          OBJ_58.setFont(new Font("Courier New", Font.BOLD, 12));
          OBJ_58.setName("OBJ_58");
          xTitledPanel2ContentContainer.add(OBJ_58);
          OBJ_58.setBounds(455, 230, 88, 28);

          //---- HONAT ----
          HONAT.setComponentPopupMenu(BTD);
          HONAT.setName("HONAT");
          xTitledPanel2ContentContainer.add(HONAT);
          HONAT.setBounds(450, 173, 30, HONAT.getPreferredSize().height);

          //---- HORST ----
          HORST.setComponentPopupMenu(BTD);
          HORST.setName("HORST");
          xTitledPanel2ContentContainer.add(HORST);
          HORST.setBounds(450, 201, 25, HORST.getPreferredSize().height);

          //---- HOVAC ----
          HOVAC.setName("HOVAC");
          xTitledPanel2ContentContainer.add(HOVAC);
          HOVAC.setBounds(160, 5, 96, HOVAC.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel2ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(xTitledPanel2);
        xTitledPanel2.setBounds(10, 145, 618, 315);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Choix possibles");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private XRiTextField A1LIB;
  private XRiTextField HOPRO;
  private JLabel OBJ_17;
  private XRiTextField HOSIR;
  private JLabel OBJ_20;
  private JLabel OBJ_18;
  private JXTitledPanel xTitledPanel2;
  private JLabel OBJ_48;
  private JLabel OBJ_51;
  private XRiTextField HOCOU;
  private XRiTextField HOJET;
  private XRiTextField HODIN;
  private JLabel OBJ_45;
  private JLabel OBJ_42;
  private JLabel OBJ_54;
  private JLabel OBJ_56;
  private JLabel OBJ_43;
  private XRiTextField HOREM;
  private XRiTextField HORIM;
  private XRiTextField HOCOM;
  private XRiTextField HORIS;
  private JLabel OBJ_46;
  private JLabel OBJ_59;
  private JLabel OBJ_44;
  private JLabel OBJ_55;
  private JLabel OBJ_47;
  private JLabel OBJ_52;
  private JLabel OBJ_57;
  private JLabel OBJ_53;
  private JLabel OBJ_49;
  private JLabel OBJ_50;
  private XRiTextField HOPAR;
  private XRiTextField HODAU;
  private XRiTextField HORMU;
  private XRiTextField HOAVN;
  private XRiTextField HOTVA;
  private XRiTextField HOTVD;
  private XRiTextField HORTS;
  private JLabel OBJ_58;
  private XRiTextField HONAT;
  private XRiTextField HORST;
  private XRiTextField HOVAC;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
