
package ri.serien.libecranrpg.vcgm.VCGM11FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM11FM_A4 extends SNPanelEcranRPG implements ioFrame {
  
  ImageIcon imageErr = null;
  
  public VCGM11FM_A4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    
    imageErr = lexique.chargerImage("images/err.png", true);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIBA1@")).trim());
    WISOC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WISOC@")).trim());
    WICJO.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WICJO@")).trim());
    WICFO.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WICFO@")).trim());
    WIDTEX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WIDTEX@")).trim());
    WCET.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCET@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMTR@")).trim());
    E1TDB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1TDB@")).trim());
    WSOL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSOL@")).trim());
    E1TCR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1TCR@")).trim());
    E1TDBR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1TDBR@")).trim());
    E1TCRR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1TCRR@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    

    
    


    
    
    
    OBJ_32.setVisible(interpreteurD.analyseExpression("@TST01@").equalsIgnoreCase("JWMTR"));
    OBJ_33.setVisible(interpreteurD.analyseExpression("@TST01@").equalsIgnoreCase("JWMTR"));
    OBJ_31.setVisible(interpreteurD.analyseExpression("@TST01@").equalsIgnoreCase("JWMTR"));
    WSOL.setVisible(lexique.isTrue("(N78) AND (40)"));
    OBJ_71_OBJ_71.setVisible(WSOL.isVisible());
    E1TDB.setVisible(lexique.isTrue("(N87) AND (N78)"));
    E1TDBR.setVisible(!E1TDB.isVisible());
    E1TCR.setVisible(lexique.isTrue("(N87) AND (N78)"));
    E1TCRR.setVisible(!E1TCR.isVisible());
    
    // TODO Icones
    OBJ_32.setIcon(lexique.chargerImage("images/aeuro.gif", true));
    OBJ_33.setIcon(lexique.chargerImage("images/neuro.gif", true));
    
    err1.setVisible(lexique.isTrue("25"));
    err2.setVisible(lexique.isTrue("26"));
    err3.setVisible(lexique.isTrue("27"));
    err4.setVisible(lexique.isTrue("28"));
    err5.setVisible(lexique.isTrue("29"));
    err6.setVisible(lexique.isTrue("30"));
    err7.setVisible(lexique.isTrue("31"));
    err8.setVisible(lexique.isTrue("32"));
    err9.setVisible(lexique.isTrue("33"));
    err10.setVisible(lexique.isTrue("34"));
    err11.setVisible(lexique.isTrue("35"));
    err12.setVisible(lexique.isTrue("36"));
    err13.setVisible(lexique.isTrue("37"));
    err14.setVisible(lexique.isTrue("38"));
    
    err1.setIcon(imageErr);
    err2.setIcon(imageErr);
    err3.setIcon(imageErr);
    err4.setIcon(imageErr);
    err5.setIcon(imageErr);
    err6.setIcon(imageErr);
    err7.setIcon(imageErr);
    err8.setIcon(imageErr);
    err9.setIcon(imageErr);
    err10.setIcon(imageErr);
    err11.setIcon(imageErr);
    err12.setIcon(imageErr);
    err13.setIcon(imageErr);
    err14.setIcon(imageErr);
    
    panel3.setVisible(lexique.isTrue("N78"));
    
    

    p_bpresentation.setCodeEtablissement(WISOC.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WISOC.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "DUPLICATE", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_75ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_32ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19", false);
  }
  
  private void OBJ_33ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_49_OBJ_49 = new JLabel();
    WISOC = new RiZoneSortie();
    OBJ_51_OBJ_51 = new JLabel();
    WICJO = new RiZoneSortie();
    OBJ_53_OBJ_53 = new JLabel();
    WICFO = new RiZoneSortie();
    OBJ_77_OBJ_77 = new JLabel();
    WIDTEX = new RiZoneSortie();
    OBJ_78_OBJ_78 = new JLabel();
    WCET = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    ETMT01 = new XRiTextField();
    label1 = new JLabel();
    ETPC01 = new XRiTextField();
    label2 = new JLabel();
    ETLI01 = new XRiTextField();
    label3 = new JLabel();
    ETSA01 = new XRiTextField();
    ETCL01 = new XRiTextField();
    ETSS01 = new XRiTextField();
    ETNG01 = new XRiTextField();
    ETNA01 = new XRiTextField();
    ETNC01 = new XRiTextField();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    ETMT02 = new XRiTextField();
    ETMT03 = new XRiTextField();
    ETMT04 = new XRiTextField();
    ETMT05 = new XRiTextField();
    ETMT06 = new XRiTextField();
    ETMT07 = new XRiTextField();
    ETMT08 = new XRiTextField();
    ETMT09 = new XRiTextField();
    ETMT10 = new XRiTextField();
    ETMT11 = new XRiTextField();
    ETMT12 = new XRiTextField();
    ETMT13 = new XRiTextField();
    ETMT14 = new XRiTextField();
    ETMT15 = new XRiTextField();
    ETPC02 = new XRiTextField();
    ETPC03 = new XRiTextField();
    ETPC04 = new XRiTextField();
    ETPC05 = new XRiTextField();
    ETPC06 = new XRiTextField();
    ETPC07 = new XRiTextField();
    ETPC08 = new XRiTextField();
    ETPC09 = new XRiTextField();
    ETPC10 = new XRiTextField();
    ETPC11 = new XRiTextField();
    ETPC12 = new XRiTextField();
    ETPC13 = new XRiTextField();
    ETPC14 = new XRiTextField();
    ETPC15 = new XRiTextField();
    ETLI02 = new XRiTextField();
    ETLI03 = new XRiTextField();
    ETLI04 = new XRiTextField();
    ETLI05 = new XRiTextField();
    ETLI06 = new XRiTextField();
    ETLI07 = new XRiTextField();
    ETLI08 = new XRiTextField();
    ETLI09 = new XRiTextField();
    ETLI10 = new XRiTextField();
    ETLI11 = new XRiTextField();
    ETLI12 = new XRiTextField();
    ETLI13 = new XRiTextField();
    ETLI14 = new XRiTextField();
    ETLI15 = new XRiTextField();
    ETSA02 = new XRiTextField();
    ETSA03 = new XRiTextField();
    ETSA04 = new XRiTextField();
    ETSA05 = new XRiTextField();
    ETSA06 = new XRiTextField();
    ETSA07 = new XRiTextField();
    ETSA08 = new XRiTextField();
    ETSA09 = new XRiTextField();
    ETSA10 = new XRiTextField();
    ETSA11 = new XRiTextField();
    ETSA12 = new XRiTextField();
    ETSA13 = new XRiTextField();
    ETSA14 = new XRiTextField();
    ETSA15 = new XRiTextField();
    ETCL02 = new XRiTextField();
    ETCL03 = new XRiTextField();
    ETCL04 = new XRiTextField();
    ETCL05 = new XRiTextField();
    ETCL06 = new XRiTextField();
    ETCL07 = new XRiTextField();
    ETCL08 = new XRiTextField();
    ETCL09 = new XRiTextField();
    ETCL10 = new XRiTextField();
    ETCL11 = new XRiTextField();
    ETCL12 = new XRiTextField();
    ETCL13 = new XRiTextField();
    ETCL14 = new XRiTextField();
    ETCL15 = new XRiTextField();
    ETSS02 = new XRiTextField();
    ETSS03 = new XRiTextField();
    ETSS04 = new XRiTextField();
    ETSS05 = new XRiTextField();
    ETSS06 = new XRiTextField();
    ETSS07 = new XRiTextField();
    ETSS08 = new XRiTextField();
    ETSS09 = new XRiTextField();
    ETSS10 = new XRiTextField();
    ETSS11 = new XRiTextField();
    ETSS12 = new XRiTextField();
    ETSS13 = new XRiTextField();
    ETSS14 = new XRiTextField();
    ETSS15 = new XRiTextField();
    ETNG02 = new XRiTextField();
    ETNG03 = new XRiTextField();
    ETNG04 = new XRiTextField();
    ETNG05 = new XRiTextField();
    ETNG06 = new XRiTextField();
    ETNG07 = new XRiTextField();
    ETNG08 = new XRiTextField();
    ETNG09 = new XRiTextField();
    ETNG10 = new XRiTextField();
    ETNG11 = new XRiTextField();
    ETNG12 = new XRiTextField();
    ETNG13 = new XRiTextField();
    ETNG14 = new XRiTextField();
    ETNG15 = new XRiTextField();
    ETNA2 = new XRiTextField();
    ETNA3 = new XRiTextField();
    ETNA4 = new XRiTextField();
    ETNA5 = new XRiTextField();
    ETNA6 = new XRiTextField();
    ETNA7 = new XRiTextField();
    ETNA8 = new XRiTextField();
    ETNA9 = new XRiTextField();
    ETNA10 = new XRiTextField();
    ETNA11 = new XRiTextField();
    ETNA12 = new XRiTextField();
    ETNA13 = new XRiTextField();
    ETNA14 = new XRiTextField();
    ETNA15 = new XRiTextField();
    ETNC02 = new XRiTextField();
    ETNC03 = new XRiTextField();
    ETNC04 = new XRiTextField();
    ETNC05 = new XRiTextField();
    ETNC06 = new XRiTextField();
    ETNC07 = new XRiTextField();
    ETNC08 = new XRiTextField();
    ETNC09 = new XRiTextField();
    ETNC10 = new XRiTextField();
    ETNC11 = new XRiTextField();
    ETNC12 = new XRiTextField();
    ETNC13 = new XRiTextField();
    ETNC14 = new XRiTextField();
    ETNC15 = new XRiTextField();
    err1 = new JLabel();
    err2 = new JLabel();
    err3 = new JLabel();
    err4 = new JLabel();
    err5 = new JLabel();
    err6 = new JLabel();
    err7 = new JLabel();
    err8 = new JLabel();
    err9 = new JLabel();
    err10 = new JLabel();
    err11 = new JLabel();
    err12 = new JLabel();
    err13 = new JLabel();
    err14 = new JLabel();
    err15 = new JLabel();
    panel3 = new JPanel();
    OBJ_71_OBJ_71 = new JLabel();
    E1TDB = new RiZoneSortie();
    WSOL = new RiZoneSortie();
    OBJ_68_OBJ_68 = new JLabel();
    E1TCR = new RiZoneSortie();
    OBJ_72_OBJ_72 = new JLabel();
    E1TDBR = new RiZoneSortie();
    E1TCRR = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    panel1 = new JPanel();
    OBJ_32 = new JButton();
    OBJ_31 = new JPanel();
    OBJ_33 = new JButton();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@WLIBA1@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_49_OBJ_49 ----
          OBJ_49_OBJ_49.setText("Soci\u00e9t\u00e9");
          OBJ_49_OBJ_49.setName("OBJ_49_OBJ_49");

          //---- WISOC ----
          WISOC.setComponentPopupMenu(null);
          WISOC.setOpaque(false);
          WISOC.setText("@WISOC@");
          WISOC.setName("WISOC");

          //---- OBJ_51_OBJ_51 ----
          OBJ_51_OBJ_51.setText("Journal");
          OBJ_51_OBJ_51.setName("OBJ_51_OBJ_51");

          //---- WICJO ----
          WICJO.setComponentPopupMenu(null);
          WICJO.setOpaque(false);
          WICJO.setText("@WICJO@");
          WICJO.setName("WICJO");

          //---- OBJ_53_OBJ_53 ----
          OBJ_53_OBJ_53.setText("Folio");
          OBJ_53_OBJ_53.setName("OBJ_53_OBJ_53");

          //---- WICFO ----
          WICFO.setComponentPopupMenu(null);
          WICFO.setOpaque(false);
          WICFO.setText("@WICFO@");
          WICFO.setName("WICFO");

          //---- OBJ_77_OBJ_77 ----
          OBJ_77_OBJ_77.setText("Date");
          OBJ_77_OBJ_77.setName("OBJ_77_OBJ_77");

          //---- WIDTEX ----
          WIDTEX.setComponentPopupMenu(null);
          WIDTEX.setOpaque(false);
          WIDTEX.setText("@WIDTEX@");
          WIDTEX.setHorizontalAlignment(SwingConstants.CENTER);
          WIDTEX.setName("WIDTEX");

          //---- OBJ_78_OBJ_78 ----
          OBJ_78_OBJ_78.setText("Type");
          OBJ_78_OBJ_78.setName("OBJ_78_OBJ_78");

          //---- WCET ----
          WCET.setComponentPopupMenu(null);
          WCET.setOpaque(false);
          WCET.setText("@WCET@");
          WCET.setName("WCET");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(50, 50, 50)
                    .addComponent(WISOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_49_OBJ_49, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addComponent(OBJ_51_OBJ_51, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(WICJO, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(35, 35, 35)
                    .addComponent(WICFO, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_53_OBJ_53, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(35, 35, 35)
                    .addComponent(WIDTEX, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_77_OBJ_77, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addComponent(OBJ_78_OBJ_78, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(WCET, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(WISOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addComponent(OBJ_49_OBJ_49, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addComponent(OBJ_51_OBJ_51, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(WICJO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WICFO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addComponent(OBJ_53_OBJ_53, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(WIDTEX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addComponent(OBJ_77_OBJ_77, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_78_OBJ_78, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(WCET, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Comptes de contrepartie");
              riSousMenu_bt6.setToolTipText("On/off affichage comptes de contrepartie");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Annuler");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(900, 600));
          p_contenu.setName("p_contenu");

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Saisie \u00e9criture type"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- ETMT01 ----
            ETMT01.setName("ETMT01");
            panel2.add(ETMT01);
            ETMT01.setBounds(25, 55, 130, ETMT01.getPreferredSize().height);

            //---- label1 ----
            label1.setText("@LIBMTR@");
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
            label1.setName("label1");
            panel2.add(label1);
            label1.setBounds(30, 30, 125, 25);

            //---- ETPC01 ----
            ETPC01.setName("ETPC01");
            panel2.add(ETPC01);
            ETPC01.setBounds(155, 55, 68, ETPC01.getPreferredSize().height);

            //---- label2 ----
            label2.setText("N\u00b0 pi\u00e8ce");
            label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
            label2.setName("label2");
            panel2.add(label2);
            label2.setBounds(160, 30, 60, 25);

            //---- ETLI01 ----
            ETLI01.setName("ETLI01");
            panel2.add(ETLI01);
            ETLI01.setBounds(223, 55, 260, ETLI01.getPreferredSize().height);

            //---- label3 ----
            label3.setText("Libell\u00e9");
            label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
            label3.setName("label3");
            panel2.add(label3);
            label3.setBounds(230, 30, 60, 25);

            //---- ETSA01 ----
            ETSA01.setComponentPopupMenu(BTD);
            ETSA01.setName("ETSA01");
            panel2.add(ETSA01);
            ETSA01.setBounds(484, 55, 50, ETSA01.getPreferredSize().height);

            //---- ETCL01 ----
            ETCL01.setName("ETCL01");
            panel2.add(ETCL01);
            ETCL01.setBounds(535, 55, 24, ETCL01.getPreferredSize().height);

            //---- ETSS01 ----
            ETSS01.setComponentPopupMenu(null);
            ETSS01.setName("ETSS01");
            panel2.add(ETSS01);
            ETSS01.setBounds(560, 55, 24, 28);

            //---- ETNG01 ----
            ETNG01.setComponentPopupMenu(BTD);
            ETNG01.setName("ETNG01");
            panel2.add(ETNG01);
            ETNG01.setBounds(585, 55, 70, ETNG01.getPreferredSize().height);

            //---- ETNA01 ----
            ETNA01.setName("ETNA01");
            panel2.add(ETNA01);
            ETNA01.setBounds(655, 55, 60, ETNA01.getPreferredSize().height);

            //---- ETNC01 ----
            ETNC01.setName("ETNC01");
            panel2.add(ETNC01);
            ETNC01.setBounds(715, 55, 60, 28);

            //---- label4 ----
            label4.setText("Section");
            label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
            label4.setName("label4");
            panel2.add(label4);
            label4.setBounds(485, 30, 53, 25);

            //---- label5 ----
            label5.setText(" C");
            label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
            label5.setName("label5");
            panel2.add(label5);
            label5.setBounds(540, 30, 20, 25);

            //---- label6 ----
            label6.setText(" S");
            label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
            label6.setName("label6");
            panel2.add(label6);
            label6.setBounds(565, 30, 20, 25);

            //---- label7 ----
            label7.setText("Compte");
            label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD));
            label7.setName("label7");
            panel2.add(label7);
            label7.setBounds(590, 30, 65, 25);

            //---- label8 ----
            label8.setText("Tiers");
            label8.setFont(label8.getFont().deriveFont(label8.getFont().getStyle() | Font.BOLD));
            label8.setName("label8");
            panel2.add(label8);
            label8.setBounds(660, 30, 50, 25);

            //---- label9 ----
            label9.setText("Contr.");
            label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD));
            label9.setName("label9");
            panel2.add(label9);
            label9.setBounds(720, 30, 50, 25);

            //---- ETMT02 ----
            ETMT02.setName("ETMT02");
            panel2.add(ETMT02);
            ETMT02.setBounds(25, 80, 130, 28);

            //---- ETMT03 ----
            ETMT03.setName("ETMT03");
            panel2.add(ETMT03);
            ETMT03.setBounds(25, 105, 130, 28);

            //---- ETMT04 ----
            ETMT04.setName("ETMT04");
            panel2.add(ETMT04);
            ETMT04.setBounds(25, 130, 130, 28);

            //---- ETMT05 ----
            ETMT05.setName("ETMT05");
            panel2.add(ETMT05);
            ETMT05.setBounds(25, 155, 130, 28);

            //---- ETMT06 ----
            ETMT06.setName("ETMT06");
            panel2.add(ETMT06);
            ETMT06.setBounds(25, 180, 130, 28);

            //---- ETMT07 ----
            ETMT07.setName("ETMT07");
            panel2.add(ETMT07);
            ETMT07.setBounds(25, 205, 130, 28);

            //---- ETMT08 ----
            ETMT08.setName("ETMT08");
            panel2.add(ETMT08);
            ETMT08.setBounds(25, 230, 130, 28);

            //---- ETMT09 ----
            ETMT09.setName("ETMT09");
            panel2.add(ETMT09);
            ETMT09.setBounds(25, 255, 130, 28);

            //---- ETMT10 ----
            ETMT10.setName("ETMT10");
            panel2.add(ETMT10);
            ETMT10.setBounds(25, 280, 130, 28);

            //---- ETMT11 ----
            ETMT11.setName("ETMT11");
            panel2.add(ETMT11);
            ETMT11.setBounds(25, 305, 130, 28);

            //---- ETMT12 ----
            ETMT12.setName("ETMT12");
            panel2.add(ETMT12);
            ETMT12.setBounds(25, 330, 130, 28);

            //---- ETMT13 ----
            ETMT13.setName("ETMT13");
            panel2.add(ETMT13);
            ETMT13.setBounds(25, 355, 130, 28);

            //---- ETMT14 ----
            ETMT14.setName("ETMT14");
            panel2.add(ETMT14);
            ETMT14.setBounds(25, 380, 130, 28);

            //---- ETMT15 ----
            ETMT15.setName("ETMT15");
            panel2.add(ETMT15);
            ETMT15.setBounds(25, 405, 130, 28);

            //---- ETPC02 ----
            ETPC02.setName("ETPC02");
            panel2.add(ETPC02);
            ETPC02.setBounds(155, 80, 68, 28);

            //---- ETPC03 ----
            ETPC03.setName("ETPC03");
            panel2.add(ETPC03);
            ETPC03.setBounds(155, 105, 68, 28);

            //---- ETPC04 ----
            ETPC04.setName("ETPC04");
            panel2.add(ETPC04);
            ETPC04.setBounds(155, 130, 68, 28);

            //---- ETPC05 ----
            ETPC05.setName("ETPC05");
            panel2.add(ETPC05);
            ETPC05.setBounds(155, 155, 68, 28);

            //---- ETPC06 ----
            ETPC06.setName("ETPC06");
            panel2.add(ETPC06);
            ETPC06.setBounds(155, 180, 68, 28);

            //---- ETPC07 ----
            ETPC07.setName("ETPC07");
            panel2.add(ETPC07);
            ETPC07.setBounds(155, 205, 68, 28);

            //---- ETPC08 ----
            ETPC08.setName("ETPC08");
            panel2.add(ETPC08);
            ETPC08.setBounds(155, 230, 68, 28);

            //---- ETPC09 ----
            ETPC09.setName("ETPC09");
            panel2.add(ETPC09);
            ETPC09.setBounds(155, 255, 68, 28);

            //---- ETPC10 ----
            ETPC10.setName("ETPC10");
            panel2.add(ETPC10);
            ETPC10.setBounds(155, 280, 68, 28);

            //---- ETPC11 ----
            ETPC11.setName("ETPC11");
            panel2.add(ETPC11);
            ETPC11.setBounds(155, 305, 68, 28);

            //---- ETPC12 ----
            ETPC12.setName("ETPC12");
            panel2.add(ETPC12);
            ETPC12.setBounds(155, 330, 68, 28);

            //---- ETPC13 ----
            ETPC13.setName("ETPC13");
            panel2.add(ETPC13);
            ETPC13.setBounds(155, 355, 68, 28);

            //---- ETPC14 ----
            ETPC14.setName("ETPC14");
            panel2.add(ETPC14);
            ETPC14.setBounds(155, 380, 68, 28);

            //---- ETPC15 ----
            ETPC15.setName("ETPC15");
            panel2.add(ETPC15);
            ETPC15.setBounds(155, 405, 68, 28);

            //---- ETLI02 ----
            ETLI02.setName("ETLI02");
            panel2.add(ETLI02);
            ETLI02.setBounds(223, 80, 260, 28);

            //---- ETLI03 ----
            ETLI03.setName("ETLI03");
            panel2.add(ETLI03);
            ETLI03.setBounds(223, 105, 260, 28);

            //---- ETLI04 ----
            ETLI04.setName("ETLI04");
            panel2.add(ETLI04);
            ETLI04.setBounds(223, 130, 260, 28);

            //---- ETLI05 ----
            ETLI05.setName("ETLI05");
            panel2.add(ETLI05);
            ETLI05.setBounds(223, 155, 260, 28);

            //---- ETLI06 ----
            ETLI06.setName("ETLI06");
            panel2.add(ETLI06);
            ETLI06.setBounds(223, 180, 260, 28);

            //---- ETLI07 ----
            ETLI07.setName("ETLI07");
            panel2.add(ETLI07);
            ETLI07.setBounds(223, 205, 260, 28);

            //---- ETLI08 ----
            ETLI08.setName("ETLI08");
            panel2.add(ETLI08);
            ETLI08.setBounds(223, 230, 260, 28);

            //---- ETLI09 ----
            ETLI09.setName("ETLI09");
            panel2.add(ETLI09);
            ETLI09.setBounds(223, 255, 260, 28);

            //---- ETLI10 ----
            ETLI10.setName("ETLI10");
            panel2.add(ETLI10);
            ETLI10.setBounds(223, 280, 260, 28);

            //---- ETLI11 ----
            ETLI11.setName("ETLI11");
            panel2.add(ETLI11);
            ETLI11.setBounds(223, 305, 260, 28);

            //---- ETLI12 ----
            ETLI12.setName("ETLI12");
            panel2.add(ETLI12);
            ETLI12.setBounds(223, 330, 260, 28);

            //---- ETLI13 ----
            ETLI13.setName("ETLI13");
            panel2.add(ETLI13);
            ETLI13.setBounds(223, 355, 260, 28);

            //---- ETLI14 ----
            ETLI14.setName("ETLI14");
            panel2.add(ETLI14);
            ETLI14.setBounds(223, 380, 260, 28);

            //---- ETLI15 ----
            ETLI15.setName("ETLI15");
            panel2.add(ETLI15);
            ETLI15.setBounds(223, 405, 260, 28);

            //---- ETSA02 ----
            ETSA02.setComponentPopupMenu(BTD);
            ETSA02.setName("ETSA02");
            panel2.add(ETSA02);
            ETSA02.setBounds(484, 80, 50, 28);

            //---- ETSA03 ----
            ETSA03.setComponentPopupMenu(BTD);
            ETSA03.setName("ETSA03");
            panel2.add(ETSA03);
            ETSA03.setBounds(484, 105, 50, 28);

            //---- ETSA04 ----
            ETSA04.setComponentPopupMenu(BTD);
            ETSA04.setName("ETSA04");
            panel2.add(ETSA04);
            ETSA04.setBounds(484, 130, 50, 28);

            //---- ETSA05 ----
            ETSA05.setComponentPopupMenu(BTD);
            ETSA05.setName("ETSA05");
            panel2.add(ETSA05);
            ETSA05.setBounds(484, 155, 50, 28);

            //---- ETSA06 ----
            ETSA06.setComponentPopupMenu(BTD);
            ETSA06.setName("ETSA06");
            panel2.add(ETSA06);
            ETSA06.setBounds(484, 180, 50, 28);

            //---- ETSA07 ----
            ETSA07.setComponentPopupMenu(BTD);
            ETSA07.setName("ETSA07");
            panel2.add(ETSA07);
            ETSA07.setBounds(484, 205, 50, 28);

            //---- ETSA08 ----
            ETSA08.setComponentPopupMenu(BTD);
            ETSA08.setName("ETSA08");
            panel2.add(ETSA08);
            ETSA08.setBounds(484, 230, 50, 28);

            //---- ETSA09 ----
            ETSA09.setComponentPopupMenu(BTD);
            ETSA09.setName("ETSA09");
            panel2.add(ETSA09);
            ETSA09.setBounds(484, 255, 50, 28);

            //---- ETSA10 ----
            ETSA10.setComponentPopupMenu(BTD);
            ETSA10.setName("ETSA10");
            panel2.add(ETSA10);
            ETSA10.setBounds(484, 280, 50, 28);

            //---- ETSA11 ----
            ETSA11.setComponentPopupMenu(BTD);
            ETSA11.setName("ETSA11");
            panel2.add(ETSA11);
            ETSA11.setBounds(484, 305, 50, 28);

            //---- ETSA12 ----
            ETSA12.setComponentPopupMenu(BTD);
            ETSA12.setName("ETSA12");
            panel2.add(ETSA12);
            ETSA12.setBounds(484, 330, 50, 28);

            //---- ETSA13 ----
            ETSA13.setComponentPopupMenu(BTD);
            ETSA13.setName("ETSA13");
            panel2.add(ETSA13);
            ETSA13.setBounds(484, 355, 50, 28);

            //---- ETSA14 ----
            ETSA14.setComponentPopupMenu(BTD);
            ETSA14.setName("ETSA14");
            panel2.add(ETSA14);
            ETSA14.setBounds(484, 380, 50, 28);

            //---- ETSA15 ----
            ETSA15.setComponentPopupMenu(BTD);
            ETSA15.setName("ETSA15");
            panel2.add(ETSA15);
            ETSA15.setBounds(484, 405, 50, 28);

            //---- ETCL02 ----
            ETCL02.setName("ETCL02");
            panel2.add(ETCL02);
            ETCL02.setBounds(535, 80, 24, 28);

            //---- ETCL03 ----
            ETCL03.setName("ETCL03");
            panel2.add(ETCL03);
            ETCL03.setBounds(535, 105, 24, 28);

            //---- ETCL04 ----
            ETCL04.setName("ETCL04");
            panel2.add(ETCL04);
            ETCL04.setBounds(535, 130, 24, 28);

            //---- ETCL05 ----
            ETCL05.setName("ETCL05");
            panel2.add(ETCL05);
            ETCL05.setBounds(535, 155, 24, 28);

            //---- ETCL06 ----
            ETCL06.setName("ETCL06");
            panel2.add(ETCL06);
            ETCL06.setBounds(535, 180, 24, 28);

            //---- ETCL07 ----
            ETCL07.setName("ETCL07");
            panel2.add(ETCL07);
            ETCL07.setBounds(535, 205, 24, 28);

            //---- ETCL08 ----
            ETCL08.setName("ETCL08");
            panel2.add(ETCL08);
            ETCL08.setBounds(535, 230, 24, 28);

            //---- ETCL09 ----
            ETCL09.setName("ETCL09");
            panel2.add(ETCL09);
            ETCL09.setBounds(535, 255, 24, 28);

            //---- ETCL10 ----
            ETCL10.setName("ETCL10");
            panel2.add(ETCL10);
            ETCL10.setBounds(535, 280, 24, 28);

            //---- ETCL11 ----
            ETCL11.setName("ETCL11");
            panel2.add(ETCL11);
            ETCL11.setBounds(535, 305, 24, 28);

            //---- ETCL12 ----
            ETCL12.setName("ETCL12");
            panel2.add(ETCL12);
            ETCL12.setBounds(535, 330, 24, 28);

            //---- ETCL13 ----
            ETCL13.setName("ETCL13");
            panel2.add(ETCL13);
            ETCL13.setBounds(535, 355, 24, 28);

            //---- ETCL14 ----
            ETCL14.setName("ETCL14");
            panel2.add(ETCL14);
            ETCL14.setBounds(535, 380, 24, 28);

            //---- ETCL15 ----
            ETCL15.setName("ETCL15");
            panel2.add(ETCL15);
            ETCL15.setBounds(535, 405, 24, 28);

            //---- ETSS02 ----
            ETSS02.setComponentPopupMenu(null);
            ETSS02.setName("ETSS02");
            panel2.add(ETSS02);
            ETSS02.setBounds(560, 80, 24, 28);

            //---- ETSS03 ----
            ETSS03.setComponentPopupMenu(null);
            ETSS03.setName("ETSS03");
            panel2.add(ETSS03);
            ETSS03.setBounds(560, 105, 24, 28);

            //---- ETSS04 ----
            ETSS04.setComponentPopupMenu(null);
            ETSS04.setName("ETSS04");
            panel2.add(ETSS04);
            ETSS04.setBounds(560, 130, 24, 28);

            //---- ETSS05 ----
            ETSS05.setComponentPopupMenu(null);
            ETSS05.setName("ETSS05");
            panel2.add(ETSS05);
            ETSS05.setBounds(560, 155, 24, 28);

            //---- ETSS06 ----
            ETSS06.setComponentPopupMenu(null);
            ETSS06.setName("ETSS06");
            panel2.add(ETSS06);
            ETSS06.setBounds(560, 180, 24, 28);

            //---- ETSS07 ----
            ETSS07.setComponentPopupMenu(null);
            ETSS07.setName("ETSS07");
            panel2.add(ETSS07);
            ETSS07.setBounds(560, 205, 24, 28);

            //---- ETSS08 ----
            ETSS08.setComponentPopupMenu(null);
            ETSS08.setName("ETSS08");
            panel2.add(ETSS08);
            ETSS08.setBounds(560, 230, 24, 28);

            //---- ETSS09 ----
            ETSS09.setComponentPopupMenu(null);
            ETSS09.setName("ETSS09");
            panel2.add(ETSS09);
            ETSS09.setBounds(560, 255, 24, 28);

            //---- ETSS10 ----
            ETSS10.setComponentPopupMenu(null);
            ETSS10.setName("ETSS10");
            panel2.add(ETSS10);
            ETSS10.setBounds(560, 280, 24, 28);

            //---- ETSS11 ----
            ETSS11.setComponentPopupMenu(null);
            ETSS11.setName("ETSS11");
            panel2.add(ETSS11);
            ETSS11.setBounds(560, 305, 24, 28);

            //---- ETSS12 ----
            ETSS12.setComponentPopupMenu(null);
            ETSS12.setName("ETSS12");
            panel2.add(ETSS12);
            ETSS12.setBounds(560, 330, 24, 28);

            //---- ETSS13 ----
            ETSS13.setComponentPopupMenu(null);
            ETSS13.setName("ETSS13");
            panel2.add(ETSS13);
            ETSS13.setBounds(560, 355, 24, 28);

            //---- ETSS14 ----
            ETSS14.setComponentPopupMenu(null);
            ETSS14.setName("ETSS14");
            panel2.add(ETSS14);
            ETSS14.setBounds(560, 380, 24, 28);

            //---- ETSS15 ----
            ETSS15.setComponentPopupMenu(null);
            ETSS15.setName("ETSS15");
            panel2.add(ETSS15);
            ETSS15.setBounds(560, 405, 24, 28);

            //---- ETNG02 ----
            ETNG02.setComponentPopupMenu(BTD);
            ETNG02.setName("ETNG02");
            panel2.add(ETNG02);
            ETNG02.setBounds(585, 80, 70, 28);

            //---- ETNG03 ----
            ETNG03.setComponentPopupMenu(BTD);
            ETNG03.setName("ETNG03");
            panel2.add(ETNG03);
            ETNG03.setBounds(585, 105, 70, 28);

            //---- ETNG04 ----
            ETNG04.setComponentPopupMenu(BTD);
            ETNG04.setName("ETNG04");
            panel2.add(ETNG04);
            ETNG04.setBounds(585, 130, 70, 28);

            //---- ETNG05 ----
            ETNG05.setComponentPopupMenu(BTD);
            ETNG05.setName("ETNG05");
            panel2.add(ETNG05);
            ETNG05.setBounds(585, 155, 70, 28);

            //---- ETNG06 ----
            ETNG06.setComponentPopupMenu(BTD);
            ETNG06.setName("ETNG06");
            panel2.add(ETNG06);
            ETNG06.setBounds(585, 180, 70, 28);

            //---- ETNG07 ----
            ETNG07.setComponentPopupMenu(BTD);
            ETNG07.setName("ETNG07");
            panel2.add(ETNG07);
            ETNG07.setBounds(585, 205, 70, 28);

            //---- ETNG08 ----
            ETNG08.setComponentPopupMenu(BTD);
            ETNG08.setName("ETNG08");
            panel2.add(ETNG08);
            ETNG08.setBounds(585, 230, 70, 28);

            //---- ETNG09 ----
            ETNG09.setComponentPopupMenu(BTD);
            ETNG09.setName("ETNG09");
            panel2.add(ETNG09);
            ETNG09.setBounds(585, 255, 70, 28);

            //---- ETNG10 ----
            ETNG10.setComponentPopupMenu(BTD);
            ETNG10.setName("ETNG10");
            panel2.add(ETNG10);
            ETNG10.setBounds(585, 280, 70, 28);

            //---- ETNG11 ----
            ETNG11.setComponentPopupMenu(BTD);
            ETNG11.setName("ETNG11");
            panel2.add(ETNG11);
            ETNG11.setBounds(585, 305, 70, 28);

            //---- ETNG12 ----
            ETNG12.setComponentPopupMenu(BTD);
            ETNG12.setName("ETNG12");
            panel2.add(ETNG12);
            ETNG12.setBounds(585, 330, 70, 28);

            //---- ETNG13 ----
            ETNG13.setComponentPopupMenu(BTD);
            ETNG13.setName("ETNG13");
            panel2.add(ETNG13);
            ETNG13.setBounds(585, 355, 70, 28);

            //---- ETNG14 ----
            ETNG14.setComponentPopupMenu(BTD);
            ETNG14.setName("ETNG14");
            panel2.add(ETNG14);
            ETNG14.setBounds(585, 380, 70, 28);

            //---- ETNG15 ----
            ETNG15.setComponentPopupMenu(BTD);
            ETNG15.setName("ETNG15");
            panel2.add(ETNG15);
            ETNG15.setBounds(585, 405, 70, 28);

            //---- ETNA2 ----
            ETNA2.setName("ETNA2");
            panel2.add(ETNA2);
            ETNA2.setBounds(655, 80, 60, 28);

            //---- ETNA3 ----
            ETNA3.setName("ETNA3");
            panel2.add(ETNA3);
            ETNA3.setBounds(655, 105, 60, 28);

            //---- ETNA4 ----
            ETNA4.setName("ETNA4");
            panel2.add(ETNA4);
            ETNA4.setBounds(655, 130, 60, 28);

            //---- ETNA5 ----
            ETNA5.setName("ETNA5");
            panel2.add(ETNA5);
            ETNA5.setBounds(655, 155, 60, 28);

            //---- ETNA6 ----
            ETNA6.setName("ETNA6");
            panel2.add(ETNA6);
            ETNA6.setBounds(655, 180, 60, 28);

            //---- ETNA7 ----
            ETNA7.setName("ETNA7");
            panel2.add(ETNA7);
            ETNA7.setBounds(655, 205, 60, 28);

            //---- ETNA8 ----
            ETNA8.setName("ETNA8");
            panel2.add(ETNA8);
            ETNA8.setBounds(655, 230, 60, 28);

            //---- ETNA9 ----
            ETNA9.setName("ETNA9");
            panel2.add(ETNA9);
            ETNA9.setBounds(655, 255, 60, 28);

            //---- ETNA10 ----
            ETNA10.setName("ETNA10");
            panel2.add(ETNA10);
            ETNA10.setBounds(655, 280, 60, 28);

            //---- ETNA11 ----
            ETNA11.setName("ETNA11");
            panel2.add(ETNA11);
            ETNA11.setBounds(655, 305, 60, 28);

            //---- ETNA12 ----
            ETNA12.setName("ETNA12");
            panel2.add(ETNA12);
            ETNA12.setBounds(655, 330, 60, 28);

            //---- ETNA13 ----
            ETNA13.setName("ETNA13");
            panel2.add(ETNA13);
            ETNA13.setBounds(655, 355, 60, 28);

            //---- ETNA14 ----
            ETNA14.setName("ETNA14");
            panel2.add(ETNA14);
            ETNA14.setBounds(655, 380, 60, 28);

            //---- ETNA15 ----
            ETNA15.setName("ETNA15");
            panel2.add(ETNA15);
            ETNA15.setBounds(655, 405, 60, 28);

            //---- ETNC02 ----
            ETNC02.setName("ETNC02");
            panel2.add(ETNC02);
            ETNC02.setBounds(715, 80, 60, 28);

            //---- ETNC03 ----
            ETNC03.setName("ETNC03");
            panel2.add(ETNC03);
            ETNC03.setBounds(715, 105, 60, 28);

            //---- ETNC04 ----
            ETNC04.setName("ETNC04");
            panel2.add(ETNC04);
            ETNC04.setBounds(715, 130, 60, 28);

            //---- ETNC05 ----
            ETNC05.setName("ETNC05");
            panel2.add(ETNC05);
            ETNC05.setBounds(715, 155, 60, 28);

            //---- ETNC06 ----
            ETNC06.setName("ETNC06");
            panel2.add(ETNC06);
            ETNC06.setBounds(715, 180, 60, 28);

            //---- ETNC07 ----
            ETNC07.setName("ETNC07");
            panel2.add(ETNC07);
            ETNC07.setBounds(715, 205, 60, 28);

            //---- ETNC08 ----
            ETNC08.setName("ETNC08");
            panel2.add(ETNC08);
            ETNC08.setBounds(715, 230, 60, 28);

            //---- ETNC09 ----
            ETNC09.setName("ETNC09");
            panel2.add(ETNC09);
            ETNC09.setBounds(715, 255, 60, 28);

            //---- ETNC10 ----
            ETNC10.setName("ETNC10");
            panel2.add(ETNC10);
            ETNC10.setBounds(715, 280, 60, 28);

            //---- ETNC11 ----
            ETNC11.setName("ETNC11");
            panel2.add(ETNC11);
            ETNC11.setBounds(715, 305, 60, 28);

            //---- ETNC12 ----
            ETNC12.setName("ETNC12");
            panel2.add(ETNC12);
            ETNC12.setBounds(715, 330, 60, 28);

            //---- ETNC13 ----
            ETNC13.setName("ETNC13");
            panel2.add(ETNC13);
            ETNC13.setBounds(715, 355, 60, 28);

            //---- ETNC14 ----
            ETNC14.setName("ETNC14");
            panel2.add(ETNC14);
            ETNC14.setBounds(715, 380, 60, 28);

            //---- ETNC15 ----
            ETNC15.setName("ETNC15");
            panel2.add(ETNC15);
            ETNC15.setBounds(715, 405, 60, 28);

            //---- err1 ----
            err1.setName("err1");
            panel2.add(err1);
            err1.setBounds(780, 56, 27, 27);

            //---- err2 ----
            err2.setName("err2");
            panel2.add(err2);
            err2.setBounds(780, 81, 27, 27);

            //---- err3 ----
            err3.setName("err3");
            panel2.add(err3);
            err3.setBounds(780, 106, 27, 27);

            //---- err4 ----
            err4.setName("err4");
            panel2.add(err4);
            err4.setBounds(780, 131, 27, 27);

            //---- err5 ----
            err5.setName("err5");
            panel2.add(err5);
            err5.setBounds(780, 156, 27, 27);

            //---- err6 ----
            err6.setName("err6");
            panel2.add(err6);
            err6.setBounds(780, 181, 27, 27);

            //---- err7 ----
            err7.setName("err7");
            panel2.add(err7);
            err7.setBounds(780, 206, 27, 27);

            //---- err8 ----
            err8.setName("err8");
            panel2.add(err8);
            err8.setBounds(780, 231, 27, 27);

            //---- err9 ----
            err9.setName("err9");
            panel2.add(err9);
            err9.setBounds(780, 256, 27, 27);

            //---- err10 ----
            err10.setName("err10");
            panel2.add(err10);
            err10.setBounds(780, 281, 27, 27);

            //---- err11 ----
            err11.setName("err11");
            panel2.add(err11);
            err11.setBounds(780, 306, 27, 27);

            //---- err12 ----
            err12.setName("err12");
            panel2.add(err12);
            err12.setBounds(780, 331, 27, 27);

            //---- err13 ----
            err13.setName("err13");
            panel2.add(err13);
            err13.setBounds(780, 356, 27, 27);

            //---- err14 ----
            err14.setName("err14");
            panel2.add(err14);
            err14.setBounds(780, 381, 27, 27);

            //---- err15 ----
            err15.setName("err15");
            panel2.add(err15);
            err15.setBounds(780, 406, 27, 27);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder("Totaux"));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- OBJ_71_OBJ_71 ----
            OBJ_71_OBJ_71.setText("<-- Diff\u00e9rence totaux");
            OBJ_71_OBJ_71.setForeground(Color.red);
            OBJ_71_OBJ_71.setFont(OBJ_71_OBJ_71.getFont().deriveFont(OBJ_71_OBJ_71.getFont().getStyle() | Font.BOLD));
            OBJ_71_OBJ_71.setName("OBJ_71_OBJ_71");
            panel3.add(OBJ_71_OBJ_71);
            OBJ_71_OBJ_71.setBounds(365, 32, 148, 20);

            //---- E1TDB ----
            E1TDB.setComponentPopupMenu(BTD);
            E1TDB.setText("@E1TDB@");
            E1TDB.setHorizontalAlignment(SwingConstants.RIGHT);
            E1TDB.setName("E1TDB");
            panel3.add(E1TDB);
            E1TDB.setBounds(80, 30, 140, E1TDB.getPreferredSize().height);

            //---- WSOL ----
            WSOL.setText("@WSOL@");
            WSOL.setHorizontalAlignment(SwingConstants.RIGHT);
            WSOL.setName("WSOL");
            panel3.add(WSOL);
            WSOL.setBounds(235, 30, 120, WSOL.getPreferredSize().height);

            //---- OBJ_68_OBJ_68 ----
            OBJ_68_OBJ_68.setText("D\u00e9bit");
            OBJ_68_OBJ_68.setName("OBJ_68_OBJ_68");
            panel3.add(OBJ_68_OBJ_68);
            OBJ_68_OBJ_68.setBounds(30, 32, 45, 20);

            //---- E1TCR ----
            E1TCR.setComponentPopupMenu(BTD);
            E1TCR.setText("@E1TCR@");
            E1TCR.setHorizontalAlignment(SwingConstants.RIGHT);
            E1TCR.setName("E1TCR");
            panel3.add(E1TCR);
            E1TCR.setBounds(80, 65, 140, E1TCR.getPreferredSize().height);

            //---- OBJ_72_OBJ_72 ----
            OBJ_72_OBJ_72.setText("Cr\u00e9dit");
            OBJ_72_OBJ_72.setName("OBJ_72_OBJ_72");
            panel3.add(OBJ_72_OBJ_72);
            OBJ_72_OBJ_72.setBounds(30, 67, 45, 20);

            //---- E1TDBR ----
            E1TDBR.setComponentPopupMenu(BTD);
            E1TDBR.setText("@E1TDBR@");
            E1TDBR.setHorizontalAlignment(SwingConstants.RIGHT);
            E1TDBR.setName("E1TDBR");
            panel3.add(E1TDBR);
            E1TDBR.setBounds(80, 30, 140, E1TDBR.getPreferredSize().height);

            //---- E1TCRR ----
            E1TCRR.setComponentPopupMenu(BTD);
            E1TCRR.setText("@E1TCRR@");
            E1TCRR.setHorizontalAlignment(SwingConstants.RIGHT);
            E1TCRR.setName("E1TCRR");
            panel3.add(E1TCRR);
            E1TCRR.setBounds(80, 65, 140, E1TCRR.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 845, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 845, GroupLayout.PREFERRED_SIZE)))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 455, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("Duplication");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }

    //======== panel1 ========
    {
      panel1.setName("panel1");
      panel1.setLayout(null);

      //---- OBJ_32 ----
      OBJ_32.setText("");
      OBJ_32.setToolTipText("Vous travaillez en Euro");
      OBJ_32.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_32.setName("OBJ_32");
      OBJ_32.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_32ActionPerformed(e);
        }
      });
      panel1.add(OBJ_32);
      OBJ_32.setBounds(25, 10, 43, 44);

      //======== OBJ_31 ========
      {
        OBJ_31.setName("OBJ_31");
        OBJ_31.setLayout(null);
      }
      panel1.add(OBJ_31);
      OBJ_31.setBounds(20, 10, 48, 48);

      //---- OBJ_33 ----
      OBJ_33.setText("");
      OBJ_33.setToolTipText("Vous travaillez en devise locale");
      OBJ_33.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_33.setName("OBJ_33");
      OBJ_33.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_33ActionPerformed(e);
        }
      });
      panel1.add(OBJ_33);
      OBJ_33.setBounds(25, 60, 43, 44);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < panel1.getComponentCount(); i++) {
          Rectangle bounds = panel1.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel1.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel1.setMinimumSize(preferredSize);
        panel1.setPreferredSize(preferredSize);
      }
    }

    //---- OBJ_8 ----
    OBJ_8.setText("On/off affichage comptes de contrepartie");
    OBJ_8.setName("OBJ_8");
    OBJ_8.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_8ActionPerformed(e);
      }
    });

    //---- OBJ_9 ----
    OBJ_9.setText("Annuler");
    OBJ_9.setName("OBJ_9");
    OBJ_9.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_9ActionPerformed(e);
      }
    });
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_49_OBJ_49;
  private RiZoneSortie WISOC;
  private JLabel OBJ_51_OBJ_51;
  private RiZoneSortie WICJO;
  private JLabel OBJ_53_OBJ_53;
  private RiZoneSortie WICFO;
  private JLabel OBJ_77_OBJ_77;
  private RiZoneSortie WIDTEX;
  private JLabel OBJ_78_OBJ_78;
  private RiZoneSortie WCET;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiTextField ETMT01;
  private JLabel label1;
  private XRiTextField ETPC01;
  private JLabel label2;
  private XRiTextField ETLI01;
  private JLabel label3;
  private XRiTextField ETSA01;
  private XRiTextField ETCL01;
  private XRiTextField ETSS01;
  private XRiTextField ETNG01;
  private XRiTextField ETNA01;
  private XRiTextField ETNC01;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private XRiTextField ETMT02;
  private XRiTextField ETMT03;
  private XRiTextField ETMT04;
  private XRiTextField ETMT05;
  private XRiTextField ETMT06;
  private XRiTextField ETMT07;
  private XRiTextField ETMT08;
  private XRiTextField ETMT09;
  private XRiTextField ETMT10;
  private XRiTextField ETMT11;
  private XRiTextField ETMT12;
  private XRiTextField ETMT13;
  private XRiTextField ETMT14;
  private XRiTextField ETMT15;
  private XRiTextField ETPC02;
  private XRiTextField ETPC03;
  private XRiTextField ETPC04;
  private XRiTextField ETPC05;
  private XRiTextField ETPC06;
  private XRiTextField ETPC07;
  private XRiTextField ETPC08;
  private XRiTextField ETPC09;
  private XRiTextField ETPC10;
  private XRiTextField ETPC11;
  private XRiTextField ETPC12;
  private XRiTextField ETPC13;
  private XRiTextField ETPC14;
  private XRiTextField ETPC15;
  private XRiTextField ETLI02;
  private XRiTextField ETLI03;
  private XRiTextField ETLI04;
  private XRiTextField ETLI05;
  private XRiTextField ETLI06;
  private XRiTextField ETLI07;
  private XRiTextField ETLI08;
  private XRiTextField ETLI09;
  private XRiTextField ETLI10;
  private XRiTextField ETLI11;
  private XRiTextField ETLI12;
  private XRiTextField ETLI13;
  private XRiTextField ETLI14;
  private XRiTextField ETLI15;
  private XRiTextField ETSA02;
  private XRiTextField ETSA03;
  private XRiTextField ETSA04;
  private XRiTextField ETSA05;
  private XRiTextField ETSA06;
  private XRiTextField ETSA07;
  private XRiTextField ETSA08;
  private XRiTextField ETSA09;
  private XRiTextField ETSA10;
  private XRiTextField ETSA11;
  private XRiTextField ETSA12;
  private XRiTextField ETSA13;
  private XRiTextField ETSA14;
  private XRiTextField ETSA15;
  private XRiTextField ETCL02;
  private XRiTextField ETCL03;
  private XRiTextField ETCL04;
  private XRiTextField ETCL05;
  private XRiTextField ETCL06;
  private XRiTextField ETCL07;
  private XRiTextField ETCL08;
  private XRiTextField ETCL09;
  private XRiTextField ETCL10;
  private XRiTextField ETCL11;
  private XRiTextField ETCL12;
  private XRiTextField ETCL13;
  private XRiTextField ETCL14;
  private XRiTextField ETCL15;
  private XRiTextField ETSS02;
  private XRiTextField ETSS03;
  private XRiTextField ETSS04;
  private XRiTextField ETSS05;
  private XRiTextField ETSS06;
  private XRiTextField ETSS07;
  private XRiTextField ETSS08;
  private XRiTextField ETSS09;
  private XRiTextField ETSS10;
  private XRiTextField ETSS11;
  private XRiTextField ETSS12;
  private XRiTextField ETSS13;
  private XRiTextField ETSS14;
  private XRiTextField ETSS15;
  private XRiTextField ETNG02;
  private XRiTextField ETNG03;
  private XRiTextField ETNG04;
  private XRiTextField ETNG05;
  private XRiTextField ETNG06;
  private XRiTextField ETNG07;
  private XRiTextField ETNG08;
  private XRiTextField ETNG09;
  private XRiTextField ETNG10;
  private XRiTextField ETNG11;
  private XRiTextField ETNG12;
  private XRiTextField ETNG13;
  private XRiTextField ETNG14;
  private XRiTextField ETNG15;
  private XRiTextField ETNA2;
  private XRiTextField ETNA3;
  private XRiTextField ETNA4;
  private XRiTextField ETNA5;
  private XRiTextField ETNA6;
  private XRiTextField ETNA7;
  private XRiTextField ETNA8;
  private XRiTextField ETNA9;
  private XRiTextField ETNA10;
  private XRiTextField ETNA11;
  private XRiTextField ETNA12;
  private XRiTextField ETNA13;
  private XRiTextField ETNA14;
  private XRiTextField ETNA15;
  private XRiTextField ETNC02;
  private XRiTextField ETNC03;
  private XRiTextField ETNC04;
  private XRiTextField ETNC05;
  private XRiTextField ETNC06;
  private XRiTextField ETNC07;
  private XRiTextField ETNC08;
  private XRiTextField ETNC09;
  private XRiTextField ETNC10;
  private XRiTextField ETNC11;
  private XRiTextField ETNC12;
  private XRiTextField ETNC13;
  private XRiTextField ETNC14;
  private XRiTextField ETNC15;
  private JLabel err1;
  private JLabel err2;
  private JLabel err3;
  private JLabel err4;
  private JLabel err5;
  private JLabel err6;
  private JLabel err7;
  private JLabel err8;
  private JLabel err9;
  private JLabel err10;
  private JLabel err11;
  private JLabel err12;
  private JLabel err13;
  private JLabel err14;
  private JLabel err15;
  private JPanel panel3;
  private JLabel OBJ_71_OBJ_71;
  private RiZoneSortie E1TDB;
  private RiZoneSortie WSOL;
  private JLabel OBJ_68_OBJ_68;
  private RiZoneSortie E1TCR;
  private JLabel OBJ_72_OBJ_72;
  private RiZoneSortie E1TDBR;
  private RiZoneSortie E1TCRR;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_12;
  private JPanel panel1;
  private JButton OBJ_32;
  private JPanel OBJ_31;
  private JButton OBJ_33;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
