
package ri.serien.libecranrpg.vcgm.VCGM33FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerListModel;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiSpinner;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM33FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _TOP1_Top = { "TOP1", "TOP2", "TOP3", "TOP4", "TOP5", "TOP6", "TOP7", "TOP8", "TOP9", };
  private String[] _TOP1_Title = { "HLD01", };
  private String[][] _TOP1_Data =
      { { "LE01", }, { "LE02", }, { "LE03", }, { "LE04", }, { "LE05", }, { "LE06", }, { "LE07", }, { "LE08", }, { "LE09", }, };
  private int[] _TOP1_Width = { 87, };
  
  public VCGM33FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TOP1.setAspectTable(_TOP1_Top, _TOP1_Title, _TOP1_Data, _TOP1_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_62.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENOM@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_78.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PER01@")).trim());
    OBJ_79.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PER02@")).trim());
    OBJ_80.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PER03@")).trim());
    OBJ_81.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PER04@")).trim());
    OBJ_82.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PER05@")).trim());
    OBJ_83.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PER06@")).trim());
    OBJ_84.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PER07@")).trim());
    OBJ_85.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PER08@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST2, LIST2.get_LIST_Title_Data_Brut(), _TOP1_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    P33SNS.setVisible(lexique.isPresent("P33SNS"));
    OBJ_114.setVisible(lexique.isPresent("WDIMP"));
    P33ETB.setVisible(lexique.isPresent("P33ETB"));
    WNIMP.setVisible(lexique.isPresent("WNIMP"));
    OBJ_101.setVisible(lexique.isPresent("P33SDE"));
    P33NCA.setVisible(lexique.isPresent("P33NCA"));
    P33NCG.setVisible(lexique.isPresent("P33NCG"));
    OBJ_113.setVisible(lexique.isPresent("WLIMP"));
    OBJ_111.setVisible(lexique.isPresent("WTIMP"));
    // NBJTRA.setVisible( lexique.isPresent("NBJTRA"));
    VAL08.setVisible(lexique.isPresent("VAL08"));
    VAL087.setVisible(lexique.isPresent("VAL087"));
    VAL06.setVisible(lexique.isPresent("VAL06"));
    VAL05.setVisible(lexique.isPresent("VAL05"));
    VAL04.setVisible(lexique.isPresent("VAL04"));
    VAL03.setVisible(lexique.isPresent("VAL03"));
    VAL02.setVisible(lexique.isPresent("VAL02"));
    VAL01.setVisible(lexique.isPresent("VAL01"));
    OBJ_85.setVisible(lexique.isPresent("PER08"));
    OBJ_84.setVisible(lexique.isPresent("PER07"));
    OBJ_83.setVisible(lexique.isPresent("PER06"));
    OBJ_82.setVisible(lexique.isPresent("PER05"));
    OBJ_81.setVisible(lexique.isPresent("PER04"));
    OBJ_80.setVisible(lexique.isPresent("PER03"));
    OBJ_79.setVisible(lexique.isPresent("PER02"));
    OBJ_78.setVisible(lexique.isPresent("PER01"));
    OBJ_112.setVisible(lexique.isPresent("WNIMP"));
    WTOT.setVisible(lexique.isPresent("WTOT"));
    WESC.setVisible(lexique.isPresent("WESC"));
    WENC.setVisible(lexique.isPresent("WENC"));
    WPOR.setVisible(lexique.isPresent("WPOR"));
    WACC.setVisible(lexique.isPresent("WACC"));
    P33SDE.setVisible(lexique.isPresent("P33SDE"));
    WDIMP.setVisible(lexique.isPresent("WDIMP"));
    WLIMP.setVisible(lexique.isPresent("WLIMP"));
    WTIMP.setVisible(lexique.isPresent("WTIMP"));
    OBJ_62.setVisible(lexique.isPresent("WENOM"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("En cours administratif"));
    
    

    
    p_bpresentation.setCodeEtablissement(P33ETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(P33ETB.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST2, _TOP1_Top, "1", "Enter");
    TOP1.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST2, _TOP1_Top, "5", "Enter");
    TOP1.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    
  }
  
  private void TOP1MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST2, _TOP1_Top, "1", "Enter", e);
    if (TOP1.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_50 = new JLabel();
    P33ETB = new XRiTextField();
    OBJ_54 = new JLabel();
    P33NCG = new XRiTextField();
    P33NCA = new XRiTextField();
    OBJ_62 = new RiZoneSortie();
    OBJ_101 = new JLabel();
    P33SDE = new XRiTextField();
    P33SNS = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel3 = new JXTitledPanel();
    WTIMP = new XRiTextField();
    WLIMP = new XRiTextField();
    OBJ_112 = new JLabel();
    OBJ_111 = new JLabel();
    OBJ_113 = new JLabel();
    WNIMP = new XRiTextField();
    OBJ_114 = new JLabel();
    WDIMP = new XRiTextField();
    panel5 = new JPanel();
    SCROLLPANE_LIST2 = new JScrollPane();
    TOP1 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_77 = new JLabel();
    OBJ_78 = new JLabel();
    OBJ_79 = new JLabel();
    OBJ_80 = new JLabel();
    OBJ_81 = new JLabel();
    OBJ_82 = new JLabel();
    OBJ_83 = new JLabel();
    OBJ_84 = new JLabel();
    OBJ_85 = new JLabel();
    VAL01 = new XRiTextField();
    VAL02 = new XRiTextField();
    VAL03 = new XRiTextField();
    VAL04 = new XRiTextField();
    VAL05 = new XRiTextField();
    VAL06 = new XRiTextField();
    VAL087 = new XRiTextField();
    VAL08 = new XRiTextField();
    NBJTRA = new XRiSpinner();
    OBJ_109 = new JLabel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_70 = new JLabel();
    OBJ_69 = new JLabel();
    OBJ_71 = new JLabel();
    OBJ_67 = new JLabel();
    WACC = new XRiTextField();
    WPOR = new XRiTextField();
    WENC = new XRiTextField();
    WESC = new XRiTextField();
    WTOT = new XRiTextField();
    OBJ_68 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    CHOISIR = new JMenuItem();
    OBJ_16 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("En cours administratif");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_50 ----
          OBJ_50.setText("Soci\u00e9t\u00e9");
          OBJ_50.setName("OBJ_50");

          //---- P33ETB ----
          P33ETB.setComponentPopupMenu(BTD);
          P33ETB.setName("P33ETB");

          //---- OBJ_54 ----
          OBJ_54.setText("Num\u00e9ro de compte");
          OBJ_54.setName("OBJ_54");

          //---- P33NCG ----
          P33NCG.setComponentPopupMenu(BTD);
          P33NCG.setName("P33NCG");

          //---- P33NCA ----
          P33NCA.setComponentPopupMenu(BTD);
          P33NCA.setName("P33NCA");

          //---- OBJ_62 ----
          OBJ_62.setText("@WENOM@");
          OBJ_62.setOpaque(false);
          OBJ_62.setName("OBJ_62");

          //---- OBJ_101 ----
          OBJ_101.setText("Solde");
          OBJ_101.setName("OBJ_101");

          //---- P33SDE ----
          P33SDE.setComponentPopupMenu(BTD);
          P33SDE.setName("P33SDE");

          //---- P33SNS ----
          P33SNS.setComponentPopupMenu(BTD);
          P33SNS.setName("P33SNS");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(P33ETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 113, GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(P33NCG, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(P33NCA, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addComponent(OBJ_101, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(P33SDE, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(P33SNS, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(P33ETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(P33NCG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(P33NCA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_101, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(P33SDE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(P33SNS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Affichage");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Par date d'\u00e9ch\u00e9ances");
              riSousMenu_bt6.setToolTipText("Affichage par date d'\u00e9ch\u00e9ances");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Du d\u00e9tail des impay\u00e9s");
              riSousMenu_bt7.setToolTipText("Affichage du d\u00e9tail des impay\u00e9s");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Du d\u00e9tail des ch\u00e8ques");
              riSousMenu_bt8.setToolTipText("Affichage du d\u00e9tail des ch\u00e8ques");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(760, 560));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel3 ========
          {
            xTitledPanel3.setTitle("Impayes");
            xTitledPanel3.setBorder(new DropShadowBorder());
            xTitledPanel3.setName("xTitledPanel3");
            Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
            xTitledPanel3ContentContainer.setLayout(null);

            //---- WTIMP ----
            WTIMP.setForeground(Color.red);
            WTIMP.setName("WTIMP");
            xTitledPanel3ContentContainer.add(WTIMP);
            WTIMP.setBounds(55, 15, 76, WTIMP.getPreferredSize().height);

            //---- WLIMP ----
            WLIMP.setName("WLIMP");
            xTitledPanel3ContentContainer.add(WLIMP);
            WLIMP.setBounds(315, 15, 76, WLIMP.getPreferredSize().height);

            //---- OBJ_112 ----
            OBJ_112.setText("Nombre");
            OBJ_112.setName("OBJ_112");
            xTitledPanel3ContentContainer.add(OBJ_112);
            OBJ_112.setBounds(160, 17, 55, 24);

            //---- OBJ_111 ----
            OBJ_111.setText("Total");
            OBJ_111.setName("OBJ_111");
            xTitledPanel3ContentContainer.add(OBJ_111);
            OBJ_111.setBounds(15, 17, 39, 24);

            //---- OBJ_113 ----
            OBJ_113.setText("Dont");
            OBJ_113.setName("OBJ_113");
            xTitledPanel3ContentContainer.add(OBJ_113);
            OBJ_113.setBounds(275, 17, 35, 24);

            //---- WNIMP ----
            WNIMP.setName("WNIMP");
            xTitledPanel3ContentContainer.add(WNIMP);
            WNIMP.setBounds(215, 15, 28, WNIMP.getPreferredSize().height);

            //---- OBJ_114 ----
            OBJ_114.setText("Du");
            OBJ_114.setName("OBJ_114");
            xTitledPanel3ContentContainer.add(OBJ_114);
            OBJ_114.setBounds(420, 17, 25, 24);

            //---- WDIMP ----
            WDIMP.setName("WDIMP");
            xTitledPanel3ContentContainer.add(WDIMP);
            WDIMP.setBounds(450, 15, 72, WDIMP.getPreferredSize().height);
          }

          //======== panel5 ========
          {
            panel5.setOpaque(false);
            panel5.setName("panel5");
            panel5.setLayout(null);

            //======== SCROLLPANE_LIST2 ========
            {
              SCROLLPANE_LIST2.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

              //---- TOP1 ----
              TOP1.setComponentPopupMenu(BTD);
              TOP1.setName("TOP1");
              TOP1.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  TOP1MouseClicked(e);
                }
              });
              SCROLLPANE_LIST2.setViewportView(TOP1);
            }
            panel5.add(SCROLLPANE_LIST2);
            SCROLLPANE_LIST2.setBounds(10, 5, 685, 173);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel5.add(BT_PGUP);
            BT_PGUP.setBounds(700, 5, 25, 80);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel5.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(700, 95, 25, 80);
          }

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setTitle("P\u00e9riodes");
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //---- OBJ_77 ----
            OBJ_77.setText("Par tranche de");
            OBJ_77.setName("OBJ_77");
            xTitledPanel2ContentContainer.add(OBJ_77);
            OBJ_77.setBounds(15, 20, 90, 20);

            //---- OBJ_78 ----
            OBJ_78.setText("@PER01@");
            OBJ_78.setName("OBJ_78");
            xTitledPanel2ContentContainer.add(OBJ_78);
            OBJ_78.setBounds(130, 20, 63, 20);

            //---- OBJ_79 ----
            OBJ_79.setText("@PER02@");
            OBJ_79.setName("OBJ_79");
            xTitledPanel2ContentContainer.add(OBJ_79);
            OBJ_79.setBounds(202, 20, 63, 20);

            //---- OBJ_80 ----
            OBJ_80.setText("@PER03@");
            OBJ_80.setName("OBJ_80");
            xTitledPanel2ContentContainer.add(OBJ_80);
            OBJ_80.setBounds(274, 20, 63, 20);

            //---- OBJ_81 ----
            OBJ_81.setText("@PER04@");
            OBJ_81.setName("OBJ_81");
            xTitledPanel2ContentContainer.add(OBJ_81);
            OBJ_81.setBounds(346, 20, 63, 20);

            //---- OBJ_82 ----
            OBJ_82.setText("@PER05@");
            OBJ_82.setName("OBJ_82");
            xTitledPanel2ContentContainer.add(OBJ_82);
            OBJ_82.setBounds(418, 20, 63, 20);

            //---- OBJ_83 ----
            OBJ_83.setText("@PER06@");
            OBJ_83.setName("OBJ_83");
            xTitledPanel2ContentContainer.add(OBJ_83);
            OBJ_83.setBounds(490, 20, 63, 20);

            //---- OBJ_84 ----
            OBJ_84.setText("@PER07@");
            OBJ_84.setName("OBJ_84");
            xTitledPanel2ContentContainer.add(OBJ_84);
            OBJ_84.setBounds(562, 20, 63, 20);

            //---- OBJ_85 ----
            OBJ_85.setText("@PER08@");
            OBJ_85.setName("OBJ_85");
            xTitledPanel2ContentContainer.add(OBJ_85);
            OBJ_85.setBounds(634, 20, 63, 20);

            //---- VAL01 ----
            VAL01.setName("VAL01");
            xTitledPanel2ContentContainer.add(VAL01);
            VAL01.setBounds(125, 45, 72, VAL01.getPreferredSize().height);

            //---- VAL02 ----
            VAL02.setName("VAL02");
            xTitledPanel2ContentContainer.add(VAL02);
            VAL02.setBounds(197, 45, 72, VAL02.getPreferredSize().height);

            //---- VAL03 ----
            VAL03.setName("VAL03");
            xTitledPanel2ContentContainer.add(VAL03);
            VAL03.setBounds(269, 45, 72, VAL03.getPreferredSize().height);

            //---- VAL04 ----
            VAL04.setName("VAL04");
            xTitledPanel2ContentContainer.add(VAL04);
            VAL04.setBounds(341, 45, 72, VAL04.getPreferredSize().height);

            //---- VAL05 ----
            VAL05.setName("VAL05");
            xTitledPanel2ContentContainer.add(VAL05);
            VAL05.setBounds(413, 45, 72, VAL05.getPreferredSize().height);

            //---- VAL06 ----
            VAL06.setName("VAL06");
            xTitledPanel2ContentContainer.add(VAL06);
            VAL06.setBounds(485, 45, 72, VAL06.getPreferredSize().height);

            //---- VAL087 ----
            VAL087.setName("VAL087");
            xTitledPanel2ContentContainer.add(VAL087);
            VAL087.setBounds(557, 45, 72, VAL087.getPreferredSize().height);

            //---- VAL08 ----
            VAL08.setName("VAL08");
            xTitledPanel2ContentContainer.add(VAL08);
            VAL08.setBounds(629, 45, 72, VAL08.getPreferredSize().height);

            //---- NBJTRA ----
            NBJTRA.setComponentPopupMenu(BTD);
            NBJTRA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            NBJTRA.setModel(new SpinnerListModel(new String[] {" ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "90", "91", "92", "93", "94", "95", "96", "97", "98", "99"}) {
              { setValue("1"); }
            });
            NBJTRA.setName("NBJTRA");
            xTitledPanel2ContentContainer.add(NBJTRA);
            NBJTRA.setBounds(15, 47, 48, 24);

            //---- OBJ_109 ----
            OBJ_109.setText("jours");
            OBJ_109.setName("OBJ_109");
            xTitledPanel2ContentContainer.add(OBJ_109);
            OBJ_109.setBounds(70, 49, 30, 20);
          }

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("R\u00e8glements non \u00e9chus");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- OBJ_70 ----
            OBJ_70.setText("Remise escompte");
            OBJ_70.setName("OBJ_70");
            xTitledPanel1ContentContainer.add(OBJ_70);
            OBJ_70.setBounds(15, 49, 110, 20);

            //---- OBJ_69 ----
            OBJ_69.setText("Remise encours");
            OBJ_69.setName("OBJ_69");
            xTitledPanel1ContentContainer.add(OBJ_69);
            OBJ_69.setBounds(240, 49, 102, 20);

            //---- OBJ_71 ----
            OBJ_71.setText("Total r\u00e9glement");
            OBJ_71.setName("OBJ_71");
            xTitledPanel1ContentContainer.add(OBJ_71);
            OBJ_71.setBounds(240, 20, 95, 20);

            //---- OBJ_67 ----
            OBJ_67.setText("A l'acceptation");
            OBJ_67.setName("OBJ_67");
            xTitledPanel1ContentContainer.add(OBJ_67);
            OBJ_67.setBounds(15, 19, 91, 20);

            //---- WACC ----
            WACC.setName("WACC");
            xTitledPanel1ContentContainer.add(WACC);
            WACC.setBounds(125, 15, 92, WACC.getPreferredSize().height);

            //---- WPOR ----
            WPOR.setName("WPOR");
            xTitledPanel1ContentContainer.add(WPOR);
            WPOR.setBounds(550, 15, 92, WPOR.getPreferredSize().height);

            //---- WENC ----
            WENC.setName("WENC");
            xTitledPanel1ContentContainer.add(WENC);
            WENC.setBounds(340, 45, 92, WENC.getPreferredSize().height);

            //---- WESC ----
            WESC.setName("WESC");
            xTitledPanel1ContentContainer.add(WESC);
            WESC.setBounds(125, 45, 92, WESC.getPreferredSize().height);

            //---- WTOT ----
            WTOT.setName("WTOT");
            xTitledPanel1ContentContainer.add(WTOT);
            WTOT.setBounds(340, 15, 92, WTOT.getPreferredSize().height);

            //---- OBJ_68 ----
            OBJ_68.setText("En portefeuille");
            OBJ_68.setName("OBJ_68");
            xTitledPanel1ContentContainer.add(OBJ_68);
            OBJ_68.setBounds(465, 19, 85, 20);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 730, GroupLayout.PREFERRED_SIZE)
                  .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 730, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 730, GroupLayout.PREFERRED_SIZE))
                  .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 730, GroupLayout.PREFERRED_SIZE))
                .addContainerGap(11, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(15, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Afficher \u00e0 partir de");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- CHOISIR ----
      CHOISIR.setText("Interroger");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);
      BTD.addSeparator();

      //---- OBJ_16 ----
      OBJ_16.setText("Aide en ligne");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_50;
  private XRiTextField P33ETB;
  private JLabel OBJ_54;
  private XRiTextField P33NCG;
  private XRiTextField P33NCA;
  private RiZoneSortie OBJ_62;
  private JLabel OBJ_101;
  private XRiTextField P33SDE;
  private XRiTextField P33SNS;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel3;
  private XRiTextField WTIMP;
  private XRiTextField WLIMP;
  private JLabel OBJ_112;
  private JLabel OBJ_111;
  private JLabel OBJ_113;
  private XRiTextField WNIMP;
  private JLabel OBJ_114;
  private XRiTextField WDIMP;
  private JPanel panel5;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable TOP1;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JXTitledPanel xTitledPanel2;
  private JLabel OBJ_77;
  private JLabel OBJ_78;
  private JLabel OBJ_79;
  private JLabel OBJ_80;
  private JLabel OBJ_81;
  private JLabel OBJ_82;
  private JLabel OBJ_83;
  private JLabel OBJ_84;
  private JLabel OBJ_85;
  private XRiTextField VAL01;
  private XRiTextField VAL02;
  private XRiTextField VAL03;
  private XRiTextField VAL04;
  private XRiTextField VAL05;
  private XRiTextField VAL06;
  private XRiTextField VAL087;
  private XRiTextField VAL08;
  private XRiSpinner NBJTRA;
  private JLabel OBJ_109;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_70;
  private JLabel OBJ_69;
  private JLabel OBJ_71;
  private JLabel OBJ_67;
  private XRiTextField WACC;
  private XRiTextField WPOR;
  private XRiTextField WENC;
  private XRiTextField WESC;
  private XRiTextField WTOT;
  private JLabel OBJ_68;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_16;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
