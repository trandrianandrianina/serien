
package ri.serien.libecranrpg.vcgm.VCGM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM03FM_B3 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VCGM03FM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDTYP@")).trim());
    INDIND.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDIND@")).trim());
    OBJ_90.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPAGE@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vcgm03"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_142ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_29 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_82 = new JLabel();
    INDTYP = new RiZoneSortie();
    OBJ_86 = new JLabel();
    INDIND = new RiZoneSortie();
    p_tete_droite = new JPanel();
    panel1 = new JPanel();
    OBJ_91 = new JLabel();
    OBJ_90 = new RiZoneSortie();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    P_Centre = new JPanel();
    OBJ_76 = new JLabel();
    OBJ_46 = new JLabel();
    OBJ_81 = new JLabel();
    OBJ_85 = new JLabel();
    OBJ_89 = new JLabel();
    OBJ_52 = new JLabel();
    OBJ_56 = new JLabel();
    OBJ_60 = new JLabel();
    OBJ_64 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_68 = new JLabel();
    OBJ_72 = new JLabel();
    PST45 = new XRiTextField();
    PST46 = new XRiTextField();
    PST47 = new XRiTextField();
    PST48 = new XRiTextField();
    PST49 = new XRiTextField();
    PST50 = new XRiTextField();
    PST51 = new XRiTextField();
    PST52 = new XRiTextField();
    PST53 = new XRiTextField();
    PST54 = new XRiTextField();
    PST55 = new XRiTextField();
    PST56 = new XRiTextField();
    OBJ_45 = new JLabel();
    OBJ_51 = new JLabel();
    OBJ_55 = new JLabel();
    OBJ_59 = new JLabel();
    OBJ_63 = new JLabel();
    OBJ_67 = new JLabel();
    OBJ_71 = new JLabel();
    OBJ_75 = new JLabel();
    OBJ_80 = new JLabel();
    OBJ_84 = new JLabel();
    OBJ_88 = new JLabel();
    OBJ_113 = new JLabel();
    OBJ_49 = new JLabel();
    OBJ_50 = new JLabel();
    OBJ_54 = new JLabel();
    OBJ_58 = new JLabel();
    OBJ_62 = new JLabel();
    OBJ_66 = new JLabel();
    OBJ_70 = new JLabel();
    OBJ_74 = new JLabel();
    OBJ_78 = new JLabel();
    OBJ_83 = new JLabel();
    OBJ_87 = new JLabel();
    OBJ_92 = new JLabel();
    separator1 = compFactory.createSeparator("Divers");
    OBJ_114 = new JLabel();
    OBJ_48 = new JLabel();
    PST57 = new XRiTextField();
    OBJ_53 = new JLabel();
    OBJ_115 = new JLabel();
    OBJ_57 = new JLabel();
    PST58 = new XRiTextField();
    OBJ_61 = new JLabel();
    OBJ_116 = new JLabel();
    OBJ_65 = new JLabel();
    PST59 = new XRiTextField();
    OBJ_69 = new JLabel();
    OBJ_73 = new JLabel();
    OBJ_117 = new JLabel();
    PST60 = new XRiTextField();
    OBJ_77 = new JLabel();
    OBJ_140 = new JXTitledSeparator();
    OBJ_142 = new SNBoutonLeger();
    OBJ_151 = new JLabel();
    OBJ_147 = new JLabel();
    OBJ_149 = new JLabel();
    OBJ_145 = new JLabel();
    V06F = new XRiTextField();
    V06F1 = new XRiTextField();
    OBJ_144 = new JLabel();
    OBJ_146 = new JLabel();
    OBJ_148 = new JLabel();
    OBJ_150 = new JLabel();
    OBJ_118 = new JLabel();
    OBJ_79 = new JLabel();
    PST61 = new XRiTextField();
    OBJ_93 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(800, 32));
          p_tete_gauche.setMinimumSize(new Dimension(800, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_29 ----
          OBJ_29.setText("Soci\u00e9t\u00e9");
          OBJ_29.setName("OBJ_29");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");

          //---- OBJ_82 ----
          OBJ_82.setText("Code");
          OBJ_82.setName("OBJ_82");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setOpaque(false);
          INDTYP.setText("@INDTYP@");
          INDTYP.setName("INDTYP");

          //---- OBJ_86 ----
          OBJ_86.setText("Ordre");
          OBJ_86.setName("OBJ_86");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setOpaque(false);
          INDIND.setText("@INDIND@");
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_82, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21)
                .addComponent(OBJ_86, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(394, 394, 394))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_82, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_86, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_91 ----
            OBJ_91.setText("Page");
            OBJ_91.setName("OBJ_91");
            panel1.add(OBJ_91);
            OBJ_91.setBounds(45, 2, 45, 20);

            //---- OBJ_90 ----
            OBJ_90.setText("@WPAGE@");
            OBJ_90.setOpaque(false);
            OBJ_90.setComponentPopupMenu(null);
            OBJ_90.setName("OBJ_90");
            panel1.add(OBJ_90);
            OBJ_90.setBounds(90, 0, 34, OBJ_90.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_tete_droite.add(panel1);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 280));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 520));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(900, 520));
          p_contenu.setName("p_contenu");

          //======== P_Centre ========
          {
            P_Centre.setPreferredSize(new Dimension(630, 460));
            P_Centre.setMinimumSize(new Dimension(630, 460));
            P_Centre.setOpaque(false);
            P_Centre.setName("P_Centre");
            P_Centre.setLayout(null);

            //---- OBJ_76 ----
            OBJ_76.setText("Une \u00e9criture par \u00e9ch\u00e9ance sur un num\u00e9ro de pi\u00e8ce");
            OBJ_76.setName("OBJ_76");
            P_Centre.add(OBJ_76);
            OBJ_76.setBounds(65, 272, 310, 20);

            //---- OBJ_46 ----
            OBJ_46.setText("Substitution n\u00b0auxiliaire par cl\u00e9 alphanum\u00e9rique 2");
            OBJ_46.setComponentPopupMenu(BTD);
            OBJ_46.setName("OBJ_46");
            P_Centre.add(OBJ_46);
            OBJ_46.setBounds(65, 55, 310, 20);

            //---- OBJ_81 ----
            OBJ_81.setText("Edt R\u00e9f/Frs/virements/ 12 car");
            OBJ_81.setName("OBJ_81");
            P_Centre.add(OBJ_81);
            OBJ_81.setBounds(65, 303, 310, 20);

            //---- OBJ_85 ----
            OBJ_85.setText("Edition sp\u00e9ciale pour PDF");
            OBJ_85.setName("OBJ_85");
            P_Centre.add(OBJ_85);
            OBJ_85.setBounds(65, 334, 310, 20);

            //---- OBJ_89 ----
            OBJ_89.setText("Non utilisation OBS/Pointage");
            OBJ_89.setName("OBJ_89");
            P_Centre.add(OBJ_89);
            OBJ_89.setBounds(490, 55, 300, 20);

            //---- OBJ_52 ----
            OBJ_52.setText("Compteurs sur axes analytiques");
            OBJ_52.setComponentPopupMenu(BTD);
            OBJ_52.setName("OBJ_52");
            P_Centre.add(OBJ_52);
            OBJ_52.setBounds(65, 86, 310, 20);

            //---- OBJ_56 ----
            OBJ_56.setText("Relance Client m\u00eame si solde CR");
            OBJ_56.setName("OBJ_56");
            P_Centre.add(OBJ_56);
            OBJ_56.setBounds(65, 117, 310, 20);

            //---- OBJ_60 ----
            OBJ_60.setText("Utilisation image personnalis\u00e9e");
            OBJ_60.setName("OBJ_60");
            P_Centre.add(OBJ_60);
            OBJ_60.setBounds(65, 148, 310, 20);

            //---- OBJ_64 ----
            OBJ_64.setText("Analyse mvts tr\u00e9sorerie/Nature");
            OBJ_64.setName("OBJ_64");
            P_Centre.add(OBJ_64);
            OBJ_64.setBounds(65, 179, 310, 20);

            //---- OBJ_47 ----
            OBJ_47.setText("Gestion sp\u00e9c.du pr\u00e9visionnel");
            OBJ_47.setName("OBJ_47");
            P_Centre.add(OBJ_47);
            OBJ_47.setBounds(490, 86, 300, 20);

            //---- OBJ_68 ----
            OBJ_68.setText("Code accept. / cr\u00e9ation Effets");
            OBJ_68.setName("OBJ_68");
            P_Centre.add(OBJ_68);
            OBJ_68.setBounds(65, 210, 310, 20);

            //---- OBJ_72 ----
            OBJ_72.setText("Plafond d'encours / PCA");
            OBJ_72.setName("OBJ_72");
            P_Centre.add(OBJ_72);
            OBJ_72.setBounds(65, 241, 310, 20);

            //---- PST45 ----
            PST45.setComponentPopupMenu(BTD);
            PST45.setName("PST45");
            P_Centre.add(PST45);
            PST45.setBounds(375, 51, 21, PST45.getPreferredSize().height);

            //---- PST46 ----
            PST46.setComponentPopupMenu(BTD);
            PST46.setName("PST46");
            P_Centre.add(PST46);
            PST46.setBounds(375, 82, 20, PST46.getPreferredSize().height);

            //---- PST47 ----
            PST47.setComponentPopupMenu(BTD);
            PST47.setName("PST47");
            P_Centre.add(PST47);
            PST47.setBounds(375, 113, 20, PST47.getPreferredSize().height);

            //---- PST48 ----
            PST48.setComponentPopupMenu(BTD);
            PST48.setName("PST48");
            P_Centre.add(PST48);
            PST48.setBounds(375, 144, 20, PST48.getPreferredSize().height);

            //---- PST49 ----
            PST49.setComponentPopupMenu(BTD);
            PST49.setName("PST49");
            P_Centre.add(PST49);
            PST49.setBounds(375, 175, 20, PST49.getPreferredSize().height);

            //---- PST50 ----
            PST50.setComponentPopupMenu(BTD);
            PST50.setName("PST50");
            P_Centre.add(PST50);
            PST50.setBounds(375, 206, 20, PST50.getPreferredSize().height);

            //---- PST51 ----
            PST51.setComponentPopupMenu(BTD);
            PST51.setName("PST51");
            P_Centre.add(PST51);
            PST51.setBounds(375, 237, 20, PST51.getPreferredSize().height);

            //---- PST52 ----
            PST52.setComponentPopupMenu(BTD);
            PST52.setName("PST52");
            P_Centre.add(PST52);
            PST52.setBounds(375, 268, 20, PST52.getPreferredSize().height);

            //---- PST53 ----
            PST53.setComponentPopupMenu(BTD);
            PST53.setName("PST53");
            P_Centre.add(PST53);
            PST53.setBounds(375, 299, 20, PST53.getPreferredSize().height);

            //---- PST54 ----
            PST54.setComponentPopupMenu(BTD);
            PST54.setName("PST54");
            P_Centre.add(PST54);
            PST54.setBounds(375, 330, 20, PST54.getPreferredSize().height);

            //---- PST55 ----
            PST55.setComponentPopupMenu(BTD);
            PST55.setName("PST55");
            P_Centre.add(PST55);
            PST55.setBounds(790, 50, 20, PST55.getPreferredSize().height);

            //---- PST56 ----
            PST56.setComponentPopupMenu(BTD);
            PST56.setName("PST56");
            P_Centre.add(PST56);
            PST56.setBounds(790, 81, 20, PST56.getPreferredSize().height);

            //---- OBJ_45 ----
            OBJ_45.setText("45");
            OBJ_45.setName("OBJ_45");
            P_Centre.add(OBJ_45);
            OBJ_45.setBounds(45, 55, 18, 20);

            //---- OBJ_51 ----
            OBJ_51.setText("46");
            OBJ_51.setName("OBJ_51");
            P_Centre.add(OBJ_51);
            OBJ_51.setBounds(45, 86, 18, 20);

            //---- OBJ_55 ----
            OBJ_55.setText("47");
            OBJ_55.setName("OBJ_55");
            P_Centre.add(OBJ_55);
            OBJ_55.setBounds(45, 117, 18, 20);

            //---- OBJ_59 ----
            OBJ_59.setText("48");
            OBJ_59.setName("OBJ_59");
            P_Centre.add(OBJ_59);
            OBJ_59.setBounds(45, 148, 18, 20);

            //---- OBJ_63 ----
            OBJ_63.setText("49");
            OBJ_63.setName("OBJ_63");
            P_Centre.add(OBJ_63);
            OBJ_63.setBounds(45, 179, 18, 20);

            //---- OBJ_67 ----
            OBJ_67.setText("50");
            OBJ_67.setName("OBJ_67");
            P_Centre.add(OBJ_67);
            OBJ_67.setBounds(45, 210, 18, 20);

            //---- OBJ_71 ----
            OBJ_71.setText("51");
            OBJ_71.setName("OBJ_71");
            P_Centre.add(OBJ_71);
            OBJ_71.setBounds(45, 241, 18, 20);

            //---- OBJ_75 ----
            OBJ_75.setText("52");
            OBJ_75.setName("OBJ_75");
            P_Centre.add(OBJ_75);
            OBJ_75.setBounds(45, 272, 18, 20);

            //---- OBJ_80 ----
            OBJ_80.setText("53");
            OBJ_80.setName("OBJ_80");
            P_Centre.add(OBJ_80);
            OBJ_80.setBounds(45, 303, 18, 20);

            //---- OBJ_84 ----
            OBJ_84.setText("54");
            OBJ_84.setName("OBJ_84");
            P_Centre.add(OBJ_84);
            OBJ_84.setBounds(45, 334, 18, 20);

            //---- OBJ_88 ----
            OBJ_88.setText("55");
            OBJ_88.setName("OBJ_88");
            P_Centre.add(OBJ_88);
            OBJ_88.setBounds(460, 55, 18, 20);

            //---- OBJ_113 ----
            OBJ_113.setText("56");
            OBJ_113.setName("OBJ_113");
            P_Centre.add(OBJ_113);
            OBJ_113.setBounds(460, 86, 18, 20);

            //---- OBJ_49 ----
            OBJ_49.setText("a");
            OBJ_49.setName("OBJ_49");
            P_Centre.add(OBJ_49);
            OBJ_49.setBounds(405, 55, 11, 20);

            //---- OBJ_50 ----
            OBJ_50.setText("a");
            OBJ_50.setName("OBJ_50");
            P_Centre.add(OBJ_50);
            OBJ_50.setBounds(820, 86, 11, 20);

            //---- OBJ_54 ----
            OBJ_54.setText("a");
            OBJ_54.setName("OBJ_54");
            P_Centre.add(OBJ_54);
            OBJ_54.setBounds(405, 86, 11, 20);

            //---- OBJ_58 ----
            OBJ_58.setText("a");
            OBJ_58.setName("OBJ_58");
            P_Centre.add(OBJ_58);
            OBJ_58.setBounds(405, 117, 11, 20);

            //---- OBJ_62 ----
            OBJ_62.setText("a");
            OBJ_62.setName("OBJ_62");
            P_Centre.add(OBJ_62);
            OBJ_62.setBounds(405, 148, 11, 20);

            //---- OBJ_66 ----
            OBJ_66.setText("a");
            OBJ_66.setName("OBJ_66");
            P_Centre.add(OBJ_66);
            OBJ_66.setBounds(405, 179, 11, 20);

            //---- OBJ_70 ----
            OBJ_70.setText("c");
            OBJ_70.setName("OBJ_70");
            P_Centre.add(OBJ_70);
            OBJ_70.setBounds(405, 210, 11, 20);

            //---- OBJ_74 ----
            OBJ_74.setText("a");
            OBJ_74.setName("OBJ_74");
            P_Centre.add(OBJ_74);
            OBJ_74.setBounds(405, 241, 11, 20);

            //---- OBJ_78 ----
            OBJ_78.setText("a");
            OBJ_78.setName("OBJ_78");
            P_Centre.add(OBJ_78);
            OBJ_78.setBounds(405, 272, 11, 20);

            //---- OBJ_83 ----
            OBJ_83.setText("a");
            OBJ_83.setName("OBJ_83");
            P_Centre.add(OBJ_83);
            OBJ_83.setBounds(405, 303, 11, 20);

            //---- OBJ_87 ----
            OBJ_87.setText("a");
            OBJ_87.setName("OBJ_87");
            P_Centre.add(OBJ_87);
            OBJ_87.setBounds(405, 334, 11, 20);

            //---- OBJ_92 ----
            OBJ_92.setText("a");
            OBJ_92.setName("OBJ_92");
            P_Centre.add(OBJ_92);
            OBJ_92.setBounds(820, 55, 11, 20);

            //---- separator1 ----
            separator1.setName("separator1");
            P_Centre.add(separator1);
            separator1.setBounds(25, 20, 840, separator1.getPreferredSize().height);

            //---- OBJ_114 ----
            OBJ_114.setText("57");
            OBJ_114.setName("OBJ_114");
            P_Centre.add(OBJ_114);
            OBJ_114.setBounds(460, 117, 18, 20);

            //---- OBJ_48 ----
            OBJ_48.setText("Protection r\u00e9f\u00e9rence cla/Cptes");
            OBJ_48.setName("OBJ_48");
            P_Centre.add(OBJ_48);
            OBJ_48.setBounds(490, 117, 300, 20);

            //---- PST57 ----
            PST57.setComponentPopupMenu(BTD);
            PST57.setName("PST57");
            P_Centre.add(PST57);
            PST57.setBounds(790, 112, 20, 28);

            //---- OBJ_53 ----
            OBJ_53.setText("a");
            OBJ_53.setName("OBJ_53");
            P_Centre.add(OBJ_53);
            OBJ_53.setBounds(820, 117, 11, 20);

            //---- OBJ_115 ----
            OBJ_115.setText("58");
            OBJ_115.setName("OBJ_115");
            P_Centre.add(OBJ_115);
            OBJ_115.setBounds(460, 148, 18, 20);

            //---- OBJ_57 ----
            OBJ_57.setText("Interface GIAM");
            OBJ_57.setName("OBJ_57");
            P_Centre.add(OBJ_57);
            OBJ_57.setBounds(490, 148, 300, 20);

            //---- PST58 ----
            PST58.setComponentPopupMenu(BTD);
            PST58.setName("PST58");
            P_Centre.add(PST58);
            PST58.setBounds(790, 143, 20, PST58.getPreferredSize().height);

            //---- OBJ_61 ----
            OBJ_61.setText("a");
            OBJ_61.setName("OBJ_61");
            P_Centre.add(OBJ_61);
            OBJ_61.setBounds(820, 148, 11, 20);

            //---- OBJ_116 ----
            OBJ_116.setText("59");
            OBJ_116.setName("OBJ_116");
            P_Centre.add(OBJ_116);
            OBJ_116.setBounds(460, 179, 18, 20);

            //---- OBJ_65 ----
            OBJ_65.setText("Nombre de mois / arr\u00eat planning");
            OBJ_65.setName("OBJ_65");
            P_Centre.add(OBJ_65);
            OBJ_65.setBounds(490, 179, 300, 20);

            //---- PST59 ----
            PST59.setComponentPopupMenu(BTD);
            PST59.setName("PST59");
            P_Centre.add(PST59);
            PST59.setBounds(790, 174, 20, PST59.getPreferredSize().height);

            //---- OBJ_69 ----
            OBJ_69.setText("d");
            OBJ_69.setName("OBJ_69");
            P_Centre.add(OBJ_69);
            OBJ_69.setBounds(820, 179, 11, 20);

            //---- OBJ_73 ----
            OBJ_73.setText("Pr\u00e9visionnel permanent");
            OBJ_73.setName("OBJ_73");
            P_Centre.add(OBJ_73);
            OBJ_73.setBounds(490, 210, 300, 20);

            //---- OBJ_117 ----
            OBJ_117.setText("60");
            OBJ_117.setName("OBJ_117");
            P_Centre.add(OBJ_117);
            OBJ_117.setBounds(460, 210, 18, 20);

            //---- PST60 ----
            PST60.setComponentPopupMenu(BTD);
            PST60.setName("PST60");
            P_Centre.add(PST60);
            PST60.setBounds(790, 205, 20, PST60.getPreferredSize().height);

            //---- OBJ_77 ----
            OBJ_77.setText("a");
            OBJ_77.setName("OBJ_77");
            P_Centre.add(OBJ_77);
            OBJ_77.setBounds(820, 210, 11, 20);

            //---- OBJ_140 ----
            OBJ_140.setTitle("Legende");
            OBJ_140.setName("OBJ_140");
            P_Centre.add(OBJ_140);
            OBJ_140.setBounds(25, 405, 840, OBJ_140.getPreferredSize().height);

            //---- OBJ_142 ----
            OBJ_142.setText("Aller \u00e0 la page");
            OBJ_142.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_142.setName("OBJ_142");
            OBJ_142.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_142ActionPerformed(e);
              }
            });
            P_Centre.add(OBJ_142);
            OBJ_142.setBounds(new Rectangle(new Point(640, 445), OBJ_142.getPreferredSize()));

            //---- OBJ_151 ----
            OBJ_151.setText("Voir Documentation");
            OBJ_151.setName("OBJ_151");
            P_Centre.add(OBJ_151);
            OBJ_151.setBounds(355, 445, 120, 20);

            //---- OBJ_147 ----
            OBJ_147.setText("0 < Val < 9");
            OBJ_147.setName("OBJ_147");
            P_Centre.add(OBJ_147);
            OBJ_147.setBounds(150, 445, 63, 20);

            //---- OBJ_149 ----
            OBJ_149.setText("Voir Aide");
            OBJ_149.setName("OBJ_149");
            P_Centre.add(OBJ_149);
            OBJ_149.setBounds(255, 445, 58, 20);

            //---- OBJ_145 ----
            OBJ_145.setText("1= OUI");
            OBJ_145.setName("OBJ_145");
            P_Centre.add(OBJ_145);
            OBJ_145.setBounds(70, 445, 49, 20);

            //---- V06F ----
            V06F.setComponentPopupMenu(BTD);
            V06F.setName("V06F");
            P_Centre.add(V06F);
            V06F.setBounds(790, 445, 20, V06F.getPreferredSize().height);

            //---- V06F1 ----
            V06F1.setComponentPopupMenu(BTD);
            V06F1.setName("V06F1");
            P_Centre.add(V06F1);
            V06F1.setBounds(790, 445, 20, V06F1.getPreferredSize().height);

            //---- OBJ_144 ----
            OBJ_144.setText("a");
            OBJ_144.setName("OBJ_144");
            P_Centre.add(OBJ_144);
            OBJ_144.setBounds(45, 445, 18, 20);

            //---- OBJ_146 ----
            OBJ_146.setText("b");
            OBJ_146.setName("OBJ_146");
            P_Centre.add(OBJ_146);
            OBJ_146.setBounds(125, 445, 21, 20);

            //---- OBJ_148 ----
            OBJ_148.setText("c");
            OBJ_148.setName("OBJ_148");
            P_Centre.add(OBJ_148);
            OBJ_148.setBounds(230, 445, 17, 20);

            //---- OBJ_150 ----
            OBJ_150.setText("d");
            OBJ_150.setName("OBJ_150");
            P_Centre.add(OBJ_150);
            OBJ_150.setBounds(330, 445, 18, 20);

            //---- OBJ_118 ----
            OBJ_118.setText("61");
            OBJ_118.setName("OBJ_118");
            P_Centre.add(OBJ_118);
            OBJ_118.setBounds(460, 241, 18, 20);

            //---- OBJ_79 ----
            OBJ_79.setText("Rang de la TVA depuis la gestion commerciale");
            OBJ_79.setName("OBJ_79");
            P_Centre.add(OBJ_79);
            OBJ_79.setBounds(490, 241, 300, 20);

            //---- PST61 ----
            PST61.setComponentPopupMenu(BTD);
            PST61.setName("PST61");
            P_Centre.add(PST61);
            PST61.setBounds(790, 237, 20, PST61.getPreferredSize().height);

            //---- OBJ_93 ----
            OBJ_93.setText("a");
            OBJ_93.setName("OBJ_93");
            P_Centre.add(OBJ_93);
            OBJ_93.setBounds(820, 241, 11, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < P_Centre.getComponentCount(); i++) {
                Rectangle bounds = P_Centre.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = P_Centre.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              P_Centre.setMinimumSize(preferredSize);
              P_Centre.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(P_Centre, GroupLayout.DEFAULT_SIZE, 874, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(P_Centre, GroupLayout.DEFAULT_SIZE, 492, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_29;
  private RiZoneSortie INDETB;
  private JLabel OBJ_82;
  private RiZoneSortie INDTYP;
  private JLabel OBJ_86;
  private RiZoneSortie INDIND;
  private JPanel p_tete_droite;
  private JPanel panel1;
  private JLabel OBJ_91;
  private RiZoneSortie OBJ_90;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel P_Centre;
  private JLabel OBJ_76;
  private JLabel OBJ_46;
  private JLabel OBJ_81;
  private JLabel OBJ_85;
  private JLabel OBJ_89;
  private JLabel OBJ_52;
  private JLabel OBJ_56;
  private JLabel OBJ_60;
  private JLabel OBJ_64;
  private JLabel OBJ_47;
  private JLabel OBJ_68;
  private JLabel OBJ_72;
  private XRiTextField PST45;
  private XRiTextField PST46;
  private XRiTextField PST47;
  private XRiTextField PST48;
  private XRiTextField PST49;
  private XRiTextField PST50;
  private XRiTextField PST51;
  private XRiTextField PST52;
  private XRiTextField PST53;
  private XRiTextField PST54;
  private XRiTextField PST55;
  private XRiTextField PST56;
  private JLabel OBJ_45;
  private JLabel OBJ_51;
  private JLabel OBJ_55;
  private JLabel OBJ_59;
  private JLabel OBJ_63;
  private JLabel OBJ_67;
  private JLabel OBJ_71;
  private JLabel OBJ_75;
  private JLabel OBJ_80;
  private JLabel OBJ_84;
  private JLabel OBJ_88;
  private JLabel OBJ_113;
  private JLabel OBJ_49;
  private JLabel OBJ_50;
  private JLabel OBJ_54;
  private JLabel OBJ_58;
  private JLabel OBJ_62;
  private JLabel OBJ_66;
  private JLabel OBJ_70;
  private JLabel OBJ_74;
  private JLabel OBJ_78;
  private JLabel OBJ_83;
  private JLabel OBJ_87;
  private JLabel OBJ_92;
  private JComponent separator1;
  private JLabel OBJ_114;
  private JLabel OBJ_48;
  private XRiTextField PST57;
  private JLabel OBJ_53;
  private JLabel OBJ_115;
  private JLabel OBJ_57;
  private XRiTextField PST58;
  private JLabel OBJ_61;
  private JLabel OBJ_116;
  private JLabel OBJ_65;
  private XRiTextField PST59;
  private JLabel OBJ_69;
  private JLabel OBJ_73;
  private JLabel OBJ_117;
  private XRiTextField PST60;
  private JLabel OBJ_77;
  private JXTitledSeparator OBJ_140;
  private SNBoutonLeger OBJ_142;
  private JLabel OBJ_151;
  private JLabel OBJ_147;
  private JLabel OBJ_149;
  private JLabel OBJ_145;
  private XRiTextField V06F;
  private XRiTextField V06F1;
  private JLabel OBJ_144;
  private JLabel OBJ_146;
  private JLabel OBJ_148;
  private JLabel OBJ_150;
  private JLabel OBJ_118;
  private JLabel OBJ_79;
  private XRiTextField PST61;
  private JLabel OBJ_93;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
