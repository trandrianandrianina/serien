
package ri.serien.libecranrpg.vcgm.VCGM11FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGM11FM_L3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM11FM_L3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    // setRefreshPanelUnderMe(false);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    xTitledSeparator1.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMTT@")).trim());
    xTitledSeparator6.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMT2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Ventilation sur totalisateurs"));
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    WLMT01 = new XRiTextField();
    xTitledSeparator1 = new JXTitledSeparator();
    xTitledSeparator2 = new JXTitledSeparator();
    xTitledSeparator3 = new JXTitledSeparator();
    xTitledSeparator4 = new JXTitledSeparator();
    xTitledSeparator5 = new JXTitledSeparator();
    WTNC01 = new XRiTextField();
    WTSS01 = new XRiTextField();
    WTSA01 = new XRiTextField();
    WTLI01 = new XRiTextField();
    WLMT03 = new XRiTextField();
    WTNC03 = new XRiTextField();
    WTSS03 = new XRiTextField();
    WTSA03 = new XRiTextField();
    WTLI03 = new XRiTextField();
    WLMT05 = new XRiTextField();
    WTNC05 = new XRiTextField();
    WTSS05 = new XRiTextField();
    WTSA05 = new XRiTextField();
    WTLI05 = new XRiTextField();
    WLMT07 = new XRiTextField();
    WTNC07 = new XRiTextField();
    WTSS07 = new XRiTextField();
    WTSA07 = new XRiTextField();
    WTLI07 = new XRiTextField();
    WLMT09 = new XRiTextField();
    WTNC09 = new XRiTextField();
    WTSS09 = new XRiTextField();
    WTSA09 = new XRiTextField();
    WTLI09 = new XRiTextField();
    WLMT11 = new XRiTextField();
    WTNC11 = new XRiTextField();
    WTSS11 = new XRiTextField();
    WTSA11 = new XRiTextField();
    WTLI11 = new XRiTextField();
    WLMT13 = new XRiTextField();
    WTNC13 = new XRiTextField();
    WTSS13 = new XRiTextField();
    WTSA13 = new XRiTextField();
    WTLI13 = new XRiTextField();
    WLMT15 = new XRiTextField();
    WTNC15 = new XRiTextField();
    WTSS15 = new XRiTextField();
    WTSA15 = new XRiTextField();
    WTLI15 = new XRiTextField();
    WLMT17 = new XRiTextField();
    WTNC17 = new XRiTextField();
    WTSS17 = new XRiTextField();
    WTSA17 = new XRiTextField();
    WTLI17 = new XRiTextField();
    WLMT19 = new XRiTextField();
    WTNC19 = new XRiTextField();
    WTSS19 = new XRiTextField();
    WTSA19 = new XRiTextField();
    WTLI119 = new XRiTextField();
    WLMT21 = new XRiTextField();
    WTNC21 = new XRiTextField();
    WTSS21 = new XRiTextField();
    WTSA21 = new XRiTextField();
    WTLI121 = new XRiTextField();
    WLMT23 = new XRiTextField();
    WTNC23 = new XRiTextField();
    WTSS23 = new XRiTextField();
    WTSA23 = new XRiTextField();
    WTLI123 = new XRiTextField();
    WLMT25 = new XRiTextField();
    WTNC25 = new XRiTextField();
    WTSS25 = new XRiTextField();
    WTSA25 = new XRiTextField();
    WTLI125 = new XRiTextField();
    WLMT27 = new XRiTextField();
    WTNC27 = new XRiTextField();
    WTSS27 = new XRiTextField();
    WTSA27 = new XRiTextField();
    WTLI127 = new XRiTextField();
    WLMT29 = new XRiTextField();
    WTNC29 = new XRiTextField();
    WTSS29 = new XRiTextField();
    WTSA29 = new XRiTextField();
    WTLI129 = new XRiTextField();
    WLMT31 = new XRiTextField();
    WTNC31 = new XRiTextField();
    WTSS31 = new XRiTextField();
    WTSA31 = new XRiTextField();
    WTLI131 = new XRiTextField();
    WLMT02 = new XRiTextField();
    xTitledSeparator6 = new JXTitledSeparator();
    xTitledSeparator7 = new JXTitledSeparator();
    xTitledSeparator8 = new JXTitledSeparator();
    xTitledSeparator9 = new JXTitledSeparator();
    xTitledSeparator10 = new JXTitledSeparator();
    WTNC02 = new XRiTextField();
    WTSS02 = new XRiTextField();
    WTSA02 = new XRiTextField();
    WTLI02 = new XRiTextField();
    WLMT04 = new XRiTextField();
    WTNC04 = new XRiTextField();
    WTSS04 = new XRiTextField();
    WTSA04 = new XRiTextField();
    WTLI04 = new XRiTextField();
    WLMT06 = new XRiTextField();
    WTNC06 = new XRiTextField();
    WTSS06 = new XRiTextField();
    WTSA06 = new XRiTextField();
    WTLI06 = new XRiTextField();
    WLMT08 = new XRiTextField();
    WTNC08 = new XRiTextField();
    WTSS08 = new XRiTextField();
    WTSA08 = new XRiTextField();
    WTLI08 = new XRiTextField();
    WLMT10 = new XRiTextField();
    WTNC10 = new XRiTextField();
    WTSS10 = new XRiTextField();
    WTSA10 = new XRiTextField();
    WTLI10 = new XRiTextField();
    WLMT12 = new XRiTextField();
    WTNC12 = new XRiTextField();
    WTSS12 = new XRiTextField();
    WTSA12 = new XRiTextField();
    WTLI12 = new XRiTextField();
    WLMT14 = new XRiTextField();
    WTNC14 = new XRiTextField();
    WTSS14 = new XRiTextField();
    WTSA14 = new XRiTextField();
    WTLI14 = new XRiTextField();
    WLMT16 = new XRiTextField();
    WTNC16 = new XRiTextField();
    WTSS16 = new XRiTextField();
    WTSA16 = new XRiTextField();
    WTLI16 = new XRiTextField();
    WLMT18 = new XRiTextField();
    WTNC18 = new XRiTextField();
    WTSS18 = new XRiTextField();
    WTSA18 = new XRiTextField();
    WTLI18 = new XRiTextField();
    WLMT20 = new XRiTextField();
    WTNC20 = new XRiTextField();
    WTSS20 = new XRiTextField();
    WTSA20 = new XRiTextField();
    WTLI20 = new XRiTextField();
    WLMT22 = new XRiTextField();
    WTNC22 = new XRiTextField();
    WTSS22 = new XRiTextField();
    WTSA22 = new XRiTextField();
    WTLI22 = new XRiTextField();
    WLMT24 = new XRiTextField();
    WTNC24 = new XRiTextField();
    WTSS24 = new XRiTextField();
    WTSA24 = new XRiTextField();
    WTLI24 = new XRiTextField();
    WLMT26 = new XRiTextField();
    WTNC26 = new XRiTextField();
    WTSS26 = new XRiTextField();
    WTSA26 = new XRiTextField();
    WTLI26 = new XRiTextField();
    WLMT28 = new XRiTextField();
    WTNC28 = new XRiTextField();
    WTSS28 = new XRiTextField();
    WTSA28 = new XRiTextField();
    WTLI28 = new XRiTextField();
    WLMT30 = new XRiTextField();
    WTNC30 = new XRiTextField();
    WTSS30 = new XRiTextField();
    WTSA30 = new XRiTextField();
    WTLI30 = new XRiTextField();
    WLMT32 = new XRiTextField();
    WTNC32 = new XRiTextField();
    WTSS32 = new XRiTextField();
    WTSA32 = new XRiTextField();
    WTLI32 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(930, 520));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Ventilations sur totalisateurs"));
          panel2.setOpaque(false);
          panel2.setName("panel2");

          //---- WLMT01 ----
          WLMT01.setName("WLMT01");

          //---- xTitledSeparator1 ----
          xTitledSeparator1.setTitle("@LIBMTT@");
          xTitledSeparator1.setName("xTitledSeparator1");

          //---- xTitledSeparator2 ----
          xTitledSeparator2.setTitle("Compte");
          xTitledSeparator2.setName("xTitledSeparator2");

          //---- xTitledSeparator3 ----
          xTitledSeparator3.setTitle("S");
          xTitledSeparator3.setName("xTitledSeparator3");

          //---- xTitledSeparator4 ----
          xTitledSeparator4.setTitle("Sect");
          xTitledSeparator4.setName("xTitledSeparator4");

          //---- xTitledSeparator5 ----
          xTitledSeparator5.setTitle("Libell\u00e9");
          xTitledSeparator5.setName("xTitledSeparator5");

          //---- WTNC01 ----
          WTNC01.setName("WTNC01");

          //---- WTSS01 ----
          WTSS01.setName("WTSS01");

          //---- WTSA01 ----
          WTSA01.setName("WTSA01");

          //---- WTLI01 ----
          WTLI01.setName("WTLI01");

          //---- WLMT03 ----
          WLMT03.setName("WLMT03");

          //---- WTNC03 ----
          WTNC03.setName("WTNC03");

          //---- WTSS03 ----
          WTSS03.setName("WTSS03");

          //---- WTSA03 ----
          WTSA03.setName("WTSA03");

          //---- WTLI03 ----
          WTLI03.setName("WTLI03");

          //---- WLMT05 ----
          WLMT05.setName("WLMT05");

          //---- WTNC05 ----
          WTNC05.setName("WTNC05");

          //---- WTSS05 ----
          WTSS05.setName("WTSS05");

          //---- WTSA05 ----
          WTSA05.setName("WTSA05");

          //---- WTLI05 ----
          WTLI05.setName("WTLI05");

          //---- WLMT07 ----
          WLMT07.setName("WLMT07");

          //---- WTNC07 ----
          WTNC07.setName("WTNC07");

          //---- WTSS07 ----
          WTSS07.setName("WTSS07");

          //---- WTSA07 ----
          WTSA07.setName("WTSA07");

          //---- WTLI07 ----
          WTLI07.setName("WTLI07");

          //---- WLMT09 ----
          WLMT09.setName("WLMT09");

          //---- WTNC09 ----
          WTNC09.setName("WTNC09");

          //---- WTSS09 ----
          WTSS09.setName("WTSS09");

          //---- WTSA09 ----
          WTSA09.setName("WTSA09");

          //---- WTLI09 ----
          WTLI09.setName("WTLI09");

          //---- WLMT11 ----
          WLMT11.setName("WLMT11");

          //---- WTNC11 ----
          WTNC11.setName("WTNC11");

          //---- WTSS11 ----
          WTSS11.setName("WTSS11");

          //---- WTSA11 ----
          WTSA11.setName("WTSA11");

          //---- WTLI11 ----
          WTLI11.setName("WTLI11");

          //---- WLMT13 ----
          WLMT13.setName("WLMT13");

          //---- WTNC13 ----
          WTNC13.setName("WTNC13");

          //---- WTSS13 ----
          WTSS13.setName("WTSS13");

          //---- WTSA13 ----
          WTSA13.setName("WTSA13");

          //---- WTLI13 ----
          WTLI13.setName("WTLI13");

          //---- WLMT15 ----
          WLMT15.setName("WLMT15");

          //---- WTNC15 ----
          WTNC15.setName("WTNC15");

          //---- WTSS15 ----
          WTSS15.setName("WTSS15");

          //---- WTSA15 ----
          WTSA15.setName("WTSA15");

          //---- WTLI15 ----
          WTLI15.setName("WTLI15");

          //---- WLMT17 ----
          WLMT17.setName("WLMT17");

          //---- WTNC17 ----
          WTNC17.setName("WTNC17");

          //---- WTSS17 ----
          WTSS17.setName("WTSS17");

          //---- WTSA17 ----
          WTSA17.setName("WTSA17");

          //---- WTLI17 ----
          WTLI17.setName("WTLI17");

          //---- WLMT19 ----
          WLMT19.setName("WLMT19");

          //---- WTNC19 ----
          WTNC19.setName("WTNC19");

          //---- WTSS19 ----
          WTSS19.setName("WTSS19");

          //---- WTSA19 ----
          WTSA19.setName("WTSA19");

          //---- WTLI119 ----
          WTLI119.setName("WTLI119");

          //---- WLMT21 ----
          WLMT21.setName("WLMT21");

          //---- WTNC21 ----
          WTNC21.setName("WTNC21");

          //---- WTSS21 ----
          WTSS21.setName("WTSS21");

          //---- WTSA21 ----
          WTSA21.setName("WTSA21");

          //---- WTLI121 ----
          WTLI121.setName("WTLI121");

          //---- WLMT23 ----
          WLMT23.setName("WLMT23");

          //---- WTNC23 ----
          WTNC23.setName("WTNC23");

          //---- WTSS23 ----
          WTSS23.setName("WTSS23");

          //---- WTSA23 ----
          WTSA23.setName("WTSA23");

          //---- WTLI123 ----
          WTLI123.setName("WTLI123");

          //---- WLMT25 ----
          WLMT25.setName("WLMT25");

          //---- WTNC25 ----
          WTNC25.setName("WTNC25");

          //---- WTSS25 ----
          WTSS25.setName("WTSS25");

          //---- WTSA25 ----
          WTSA25.setName("WTSA25");

          //---- WTLI125 ----
          WTLI125.setName("WTLI125");

          //---- WLMT27 ----
          WLMT27.setName("WLMT27");

          //---- WTNC27 ----
          WTNC27.setName("WTNC27");

          //---- WTSS27 ----
          WTSS27.setName("WTSS27");

          //---- WTSA27 ----
          WTSA27.setName("WTSA27");

          //---- WTLI127 ----
          WTLI127.setName("WTLI127");

          //---- WLMT29 ----
          WLMT29.setName("WLMT29");

          //---- WTNC29 ----
          WTNC29.setName("WTNC29");

          //---- WTSS29 ----
          WTSS29.setName("WTSS29");

          //---- WTSA29 ----
          WTSA29.setName("WTSA29");

          //---- WTLI129 ----
          WTLI129.setName("WTLI129");

          //---- WLMT31 ----
          WLMT31.setName("WLMT31");

          //---- WTNC31 ----
          WTNC31.setName("WTNC31");

          //---- WTSS31 ----
          WTSS31.setName("WTSS31");

          //---- WTSA31 ----
          WTSA31.setName("WTSA31");

          //---- WTLI131 ----
          WTLI131.setName("WTLI131");

          //---- WLMT02 ----
          WLMT02.setName("WLMT02");

          //---- xTitledSeparator6 ----
          xTitledSeparator6.setTitle("@LIBMT2@");
          xTitledSeparator6.setName("xTitledSeparator6");

          //---- xTitledSeparator7 ----
          xTitledSeparator7.setTitle("Compte");
          xTitledSeparator7.setName("xTitledSeparator7");

          //---- xTitledSeparator8 ----
          xTitledSeparator8.setTitle("S");
          xTitledSeparator8.setName("xTitledSeparator8");

          //---- xTitledSeparator9 ----
          xTitledSeparator9.setTitle("Sect");
          xTitledSeparator9.setName("xTitledSeparator9");

          //---- xTitledSeparator10 ----
          xTitledSeparator10.setTitle("Libell\u00e9");
          xTitledSeparator10.setName("xTitledSeparator10");

          //---- WTNC02 ----
          WTNC02.setName("WTNC02");

          //---- WTSS02 ----
          WTSS02.setName("WTSS02");

          //---- WTSA02 ----
          WTSA02.setName("WTSA02");

          //---- WTLI02 ----
          WTLI02.setName("WTLI02");

          //---- WLMT04 ----
          WLMT04.setName("WLMT04");

          //---- WTNC04 ----
          WTNC04.setName("WTNC04");

          //---- WTSS04 ----
          WTSS04.setName("WTSS04");

          //---- WTSA04 ----
          WTSA04.setName("WTSA04");

          //---- WTLI04 ----
          WTLI04.setName("WTLI04");

          //---- WLMT06 ----
          WLMT06.setName("WLMT06");

          //---- WTNC06 ----
          WTNC06.setName("WTNC06");

          //---- WTSS06 ----
          WTSS06.setName("WTSS06");

          //---- WTSA06 ----
          WTSA06.setName("WTSA06");

          //---- WTLI06 ----
          WTLI06.setName("WTLI06");

          //---- WLMT08 ----
          WLMT08.setName("WLMT08");

          //---- WTNC08 ----
          WTNC08.setName("WTNC08");

          //---- WTSS08 ----
          WTSS08.setName("WTSS08");

          //---- WTSA08 ----
          WTSA08.setName("WTSA08");

          //---- WTLI08 ----
          WTLI08.setName("WTLI08");

          //---- WLMT10 ----
          WLMT10.setName("WLMT10");

          //---- WTNC10 ----
          WTNC10.setName("WTNC10");

          //---- WTSS10 ----
          WTSS10.setName("WTSS10");

          //---- WTSA10 ----
          WTSA10.setName("WTSA10");

          //---- WTLI10 ----
          WTLI10.setName("WTLI10");

          //---- WLMT12 ----
          WLMT12.setName("WLMT12");

          //---- WTNC12 ----
          WTNC12.setName("WTNC12");

          //---- WTSS12 ----
          WTSS12.setName("WTSS12");

          //---- WTSA12 ----
          WTSA12.setName("WTSA12");

          //---- WTLI12 ----
          WTLI12.setName("WTLI12");

          //---- WLMT14 ----
          WLMT14.setName("WLMT14");

          //---- WTNC14 ----
          WTNC14.setName("WTNC14");

          //---- WTSS14 ----
          WTSS14.setName("WTSS14");

          //---- WTSA14 ----
          WTSA14.setName("WTSA14");

          //---- WTLI14 ----
          WTLI14.setName("WTLI14");

          //---- WLMT16 ----
          WLMT16.setName("WLMT16");

          //---- WTNC16 ----
          WTNC16.setName("WTNC16");

          //---- WTSS16 ----
          WTSS16.setName("WTSS16");

          //---- WTSA16 ----
          WTSA16.setName("WTSA16");

          //---- WTLI16 ----
          WTLI16.setName("WTLI16");

          //---- WLMT18 ----
          WLMT18.setName("WLMT18");

          //---- WTNC18 ----
          WTNC18.setName("WTNC18");

          //---- WTSS18 ----
          WTSS18.setName("WTSS18");

          //---- WTSA18 ----
          WTSA18.setName("WTSA18");

          //---- WTLI18 ----
          WTLI18.setName("WTLI18");

          //---- WLMT20 ----
          WLMT20.setName("WLMT20");

          //---- WTNC20 ----
          WTNC20.setName("WTNC20");

          //---- WTSS20 ----
          WTSS20.setName("WTSS20");

          //---- WTSA20 ----
          WTSA20.setName("WTSA20");

          //---- WTLI20 ----
          WTLI20.setName("WTLI20");

          //---- WLMT22 ----
          WLMT22.setName("WLMT22");

          //---- WTNC22 ----
          WTNC22.setName("WTNC22");

          //---- WTSS22 ----
          WTSS22.setName("WTSS22");

          //---- WTSA22 ----
          WTSA22.setName("WTSA22");

          //---- WTLI22 ----
          WTLI22.setName("WTLI22");

          //---- WLMT24 ----
          WLMT24.setName("WLMT24");

          //---- WTNC24 ----
          WTNC24.setName("WTNC24");

          //---- WTSS24 ----
          WTSS24.setName("WTSS24");

          //---- WTSA24 ----
          WTSA24.setName("WTSA24");

          //---- WTLI24 ----
          WTLI24.setName("WTLI24");

          //---- WLMT26 ----
          WLMT26.setName("WLMT26");

          //---- WTNC26 ----
          WTNC26.setName("WTNC26");

          //---- WTSS26 ----
          WTSS26.setName("WTSS26");

          //---- WTSA26 ----
          WTSA26.setName("WTSA26");

          //---- WTLI26 ----
          WTLI26.setName("WTLI26");

          //---- WLMT28 ----
          WLMT28.setName("WLMT28");

          //---- WTNC28 ----
          WTNC28.setName("WTNC28");

          //---- WTSS28 ----
          WTSS28.setName("WTSS28");

          //---- WTSA28 ----
          WTSA28.setName("WTSA28");

          //---- WTLI28 ----
          WTLI28.setName("WTLI28");

          //---- WLMT30 ----
          WLMT30.setName("WLMT30");

          //---- WTNC30 ----
          WTNC30.setName("WTNC30");

          //---- WTSS30 ----
          WTSS30.setName("WTSS30");

          //---- WTSA30 ----
          WTSA30.setName("WTSA30");

          //---- WTLI30 ----
          WTLI30.setName("WTLI30");

          //---- WLMT32 ----
          WLMT32.setName("WLMT32");

          //---- WTNC32 ----
          WTNC32.setName("WTNC32");

          //---- WTSS32 ----
          WTSS32.setName("WTSS32");

          //---- WTSA32 ----
          WTSA32.setName("WTSA32");

          //---- WTLI32 ----
          WTLI32.setName("WTLI32");

          GroupLayout panel2Layout = new GroupLayout(panel2);
          panel2.setLayout(panel2Layout);
          panel2Layout.setHorizontalGroup(
            panel2Layout.createParallelGroup()
              .addGroup(panel2Layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addGroup(panel2Layout.createParallelGroup()
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGroup(panel2Layout.createParallelGroup()
                      .addComponent(WLMT09, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WLMT07, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WLMT15, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createParallelGroup()
                      .addComponent(WTNC01, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTNC07, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTNC09, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTNC05, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTNC17, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTNC19, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createParallelGroup()
                      .addComponent(WTSS03, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTSS09, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTSS15, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTSS19, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createParallelGroup()
                      .addComponent(WTSA07, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTSA03, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTSA05, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTSA11, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTSA13, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTSA19, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTSA17, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createParallelGroup()
                      .addComponent(WTLI01, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTLI07, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTLI05, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTLI11, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTLI119, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addComponent(WLMT27, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(60, 60, 60)
                    .addGroup(panel2Layout.createParallelGroup()
                      .addComponent(WTSS27, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTSS31, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTSS29, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createParallelGroup()
                      .addComponent(WTSA23, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTSA27, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTSA25, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createParallelGroup()
                      .addComponent(WTLI127, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTLI125, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTLI123, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTLI131, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(165, 165, 165)
                    .addComponent(xTitledSeparator3, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(160, 160, 160)
                    .addComponent(WTSS07, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(180, 180, 180)
                    .addComponent(WTSA01, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(160, 160, 160)
                    .addComponent(WTSS25, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(160, 160, 160)
                    .addComponent(WTSS01, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(WLMT05, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(100, 100, 100)
                    .addComponent(WTNC11, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(185, 185, 185)
                    .addComponent(xTitledSeparator4, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(100, 100, 100)
                    .addComponent(WTNC27, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(180, 180, 180)
                    .addComponent(WTSA21, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                  .addComponent(WLMT31, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(160, 160, 160)
                    .addComponent(WTSS11, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(160, 160, 160)
                    .addComponent(WTSS17, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(230, 230, 230)
                    .addComponent(WTLI17, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(230, 230, 230)
                    .addComponent(WTLI13, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(100, 100, 100)
                    .addComponent(WTNC31, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(100, 100, 100)
                    .addComponent(WTNC29, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(180, 180, 180)
                    .addComponent(WTSA31, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(105, 105, 105)
                    .addComponent(xTitledSeparator2, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(100, 100, 100)
                    .addComponent(WTNC23, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(160, 160, 160)
                    .addComponent(WTSS05, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addComponent(WLMT17, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WLMT01, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(100, 100, 100)
                    .addComponent(WTNC15, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(160, 160, 160)
                    .addComponent(WTSS23, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(WLMT23, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WLMT11, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WLMT19, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(160, 160, 160)
                    .addComponent(WTSS13, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(WLMT13, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(180, 180, 180)
                    .addComponent(WTSA29, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(235, 235, 235)
                    .addComponent(xTitledSeparator5, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(100, 100, 100)
                    .addComponent(WTNC13, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(100, 100, 100)
                    .addComponent(WTNC21, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addComponent(WLMT29, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WLMT03, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(100, 100, 100)
                    .addComponent(WTNC25, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addComponent(WLMT25, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(180, 180, 180)
                    .addComponent(WTSA15, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                  .addComponent(WLMT21, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(230, 230, 230)
                    .addComponent(WTLI09, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(230, 230, 230)
                    .addComponent(WTLI15, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(230, 230, 230)
                    .addComponent(WTLI129, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(230, 230, 230)
                    .addComponent(WTLI121, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(230, 230, 230)
                    .addComponent(WTLI03, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(100, 100, 100)
                    .addComponent(WTNC03, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(180, 180, 180)
                    .addComponent(WTSA09, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(160, 160, 160)
                    .addComponent(WTSS21, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
                .addGap(15, 15, 15)
                .addGroup(panel2Layout.createParallelGroup()
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGroup(panel2Layout.createParallelGroup()
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGroup(panel2Layout.createParallelGroup()
                          .addComponent(WLMT02, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WLMT06, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                        .addComponent(WTNC04, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(105, 105, 105)
                        .addComponent(xTitledSeparator7, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(160, 160, 160)
                        .addComponent(WTSS04, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(xTitledSeparator6, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGroup(panel2Layout.createParallelGroup()
                          .addComponent(WLMT10, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WLMT14, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel2Layout.createParallelGroup()
                          .addComponent(WTNC10, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC12, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTNC16, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel2Layout.createParallelGroup()
                          .addComponent(WTSS12, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSS16, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
                      .addComponent(WLMT08, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createParallelGroup()
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addComponent(WTSA06, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addGroup(panel2Layout.createParallelGroup()
                          .addComponent(WTLI02, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(5, 5, 5)
                            .addComponent(xTitledSeparator10, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))))
                      .addComponent(WTSA04, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(xTitledSeparator9, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGroup(panel2Layout.createParallelGroup()
                          .addComponent(WTSA16, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA12, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTSA14, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel2Layout.createParallelGroup()
                          .addComponent(WTLI10, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WTLI14, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)))))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGroup(panel2Layout.createParallelGroup()
                      .addComponent(WLMT20, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WLMT22, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WLMT26, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WLMT28, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WLMT30, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createParallelGroup()
                      .addComponent(WTNC22, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTNC26, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTNC32, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTNC28, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTNC30, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createParallelGroup()
                      .addComponent(WTSS24, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTSS22, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTSS28, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTSS30, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createParallelGroup()
                      .addComponent(WTSA22, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTSA32, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTSA30, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTSA28, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createParallelGroup()
                      .addComponent(WTLI22, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTLI26, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTLI32, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTLI30, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)))
                  .addComponent(WLMT24, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(160, 160, 160)
                    .addComponent(WTSS10, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(230, 230, 230)
                    .addComponent(WTLI24, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                  .addComponent(WLMT16, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(230, 230, 230)
                    .addComponent(WTLI18, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(160, 160, 160)
                    .addComponent(WTSS32, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(100, 100, 100)
                    .addComponent(WTNC02, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(100, 100, 100)
                    .addComponent(WTNC14, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(230, 230, 230)
                    .addComponent(WTLI28, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(160, 160, 160)
                    .addComponent(WTSS18, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(160, 160, 160)
                    .addComponent(WTSS08, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(180, 180, 180)
                    .addComponent(WTSA20, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(230, 230, 230)
                    .addComponent(WTLI04, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(160, 160, 160)
                    .addComponent(WTSS02, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(180, 180, 180)
                    .addComponent(WTSA10, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(165, 165, 165)
                    .addComponent(xTitledSeparator8, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(160, 160, 160)
                    .addComponent(WTSS20, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(WLMT12, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(100, 100, 100)
                    .addComponent(WTNC08, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addComponent(WLMT32, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WLMT18, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(180, 180, 180)
                    .addComponent(WTSA26, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(230, 230, 230)
                    .addComponent(WTLI12, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(100, 100, 100)
                    .addComponent(WTNC24, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(230, 230, 230)
                    .addComponent(WTLI06, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(100, 100, 100)
                    .addComponent(WTNC20, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(180, 180, 180)
                    .addComponent(WTSA08, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(100, 100, 100)
                    .addComponent(WTNC06, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addComponent(WLMT04, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(160, 160, 160)
                    .addComponent(WTSS26, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(160, 160, 160)
                    .addComponent(WTSS06, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(230, 230, 230)
                    .addComponent(WTLI20, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(160, 160, 160)
                    .addComponent(WTSS14, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(180, 180, 180)
                    .addComponent(WTSA02, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(100, 100, 100)
                    .addComponent(WTNC18, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(230, 230, 230)
                    .addComponent(WTLI16, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(180, 180, 180)
                    .addComponent(WTSA24, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(230, 230, 230)
                    .addComponent(WTLI08, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(180, 180, 180)
                    .addComponent(WTSA18, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))))
          );
          panel2Layout.setVerticalGroup(
            panel2Layout.createParallelGroup()
              .addGroup(panel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(panel2Layout.createParallelGroup()
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(13, 13, 13)
                    .addGroup(panel2Layout.createParallelGroup()
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(81, 81, 81)
                        .addGroup(panel2Layout.createParallelGroup()
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(27, 27, 27)
                            .addComponent(WLMT09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addComponent(WLMT07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGap(53, 53, 53)
                        .addComponent(WLMT15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addComponent(WTNC01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addGroup(panel2Layout.createParallelGroup()
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(27, 27, 27)
                            .addComponent(WTNC07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(54, 54, 54)
                            .addComponent(WTNC09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addComponent(WTNC05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGap(80, 80, 80)
                        .addGroup(panel2Layout.createParallelGroup()
                          .addComponent(WTNC17, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(27, 27, 27)
                            .addComponent(WTNC19, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(WTSS03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(53, 53, 53)
                        .addComponent(WTSS09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(53, 53, 53)
                        .addComponent(WTSS15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addComponent(WTSS19, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(panel2Layout.createParallelGroup()
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(54, 54, 54)
                            .addComponent(WTSA07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addComponent(WTSA03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(27, 27, 27)
                            .addComponent(WTSA05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                        .addGap(26, 26, 26)
                        .addGroup(panel2Layout.createParallelGroup()
                          .addComponent(WTSA11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(27, 27, 27)
                            .addComponent(WTSA13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                        .addGap(26, 26, 26)
                        .addGroup(panel2Layout.createParallelGroup()
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(27, 27, 27)
                            .addComponent(WTSA19, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addComponent(WTSA17, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addComponent(WTLI01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addGroup(panel2Layout.createParallelGroup()
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(27, 27, 27)
                            .addComponent(WTLI07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addComponent(WTLI05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGap(26, 26, 26)
                        .addComponent(WTLI11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(80, 80, 80)
                        .addComponent(WTLI119, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                    .addGap(26, 26, 26)
                    .addGroup(panel2Layout.createParallelGroup()
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addComponent(WLMT27, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addGroup(panel2Layout.createParallelGroup()
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addComponent(WTSS27, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addGap(26, 26, 26)
                            .addComponent(WTSS31, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(27, 27, 27)
                            .addComponent(WTSS29, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addComponent(WTSA23, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addComponent(WTSA27, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(WTSA25, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGroup(panel2Layout.createParallelGroup()
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(54, 54, 54)
                            .addComponent(WTLI127, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(27, 27, 27)
                            .addComponent(WTLI125, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addComponent(WTLI123, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGap(26, 26, 26)
                        .addComponent(WTLI131, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addComponent(xTitledSeparator3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(94, 94, 94)
                    .addComponent(WTSS07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(13, 13, 13)
                    .addComponent(WTSA01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(337, 337, 337)
                    .addComponent(WTSS25, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(13, 13, 13)
                    .addComponent(WTSS01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(67, 67, 67)
                    .addComponent(WLMT05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(148, 148, 148)
                    .addComponent(WTNC11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(xTitledSeparator4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(364, 364, 364)
                    .addComponent(WTNC27, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(283, 283, 283)
                    .addComponent(WTSA21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(418, 418, 418)
                    .addComponent(WLMT31, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(148, 148, 148)
                    .addComponent(WTSS11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(229, 229, 229)
                    .addComponent(WTSS17, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(229, 229, 229)
                    .addComponent(WTLI17, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(175, 175, 175)
                    .addComponent(WTLI13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(418, 418, 418)
                    .addComponent(WTNC31, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(391, 391, 391)
                    .addComponent(WTNC29, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(418, 418, 418)
                    .addComponent(WTSA31, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(xTitledSeparator2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(310, 310, 310)
                    .addComponent(WTNC23, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(67, 67, 67)
                    .addComponent(WTSS05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(229, 229, 229)
                    .addComponent(WLMT17, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(13, 13, 13)
                    .addComponent(WLMT01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(202, 202, 202)
                    .addComponent(WTNC15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(310, 310, 310)
                    .addComponent(WTSS23, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(310, 310, 310)
                    .addComponent(WLMT23, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(148, 148, 148)
                    .addComponent(WLMT11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(256, 256, 256)
                    .addComponent(WLMT19, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(175, 175, 175)
                    .addComponent(WTSS13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(175, 175, 175)
                    .addComponent(WLMT13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(391, 391, 391)
                    .addComponent(WTSA29, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(xTitledSeparator5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(175, 175, 175)
                    .addComponent(WTNC13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(283, 283, 283)
                    .addComponent(WTNC21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(391, 391, 391)
                    .addComponent(WLMT29, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(40, 40, 40)
                    .addComponent(WLMT03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(337, 337, 337)
                    .addComponent(WTNC25, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(337, 337, 337)
                    .addComponent(WLMT25, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(202, 202, 202)
                    .addComponent(WTSA15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(283, 283, 283)
                    .addComponent(WLMT21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(121, 121, 121)
                    .addComponent(WTLI09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(202, 202, 202)
                    .addComponent(WTLI15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(391, 391, 391)
                    .addComponent(WTLI129, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(283, 283, 283)
                    .addComponent(WTLI121, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(40, 40, 40)
                    .addComponent(WTLI03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(40, 40, 40)
                    .addComponent(WTNC03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(121, 121, 121)
                    .addComponent(WTSA09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(283, 283, 283)
                    .addComponent(WTSS21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGroup(panel2Layout.createParallelGroup()
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGroup(panel2Layout.createParallelGroup()
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(13, 13, 13)
                            .addComponent(WLMT02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addGap(26, 26, 26)
                            .addComponent(WLMT06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(40, 40, 40)
                            .addComponent(WTNC04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addComponent(xTitledSeparator7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(40, 40, 40)
                            .addComponent(WTSS04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addComponent(xTitledSeparator6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGap(26, 26, 26)
                        .addGroup(panel2Layout.createParallelGroup()
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addComponent(WLMT10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addGap(26, 26, 26)
                            .addComponent(WLMT14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGroup(panel2Layout.createParallelGroup()
                              .addComponent(WTNC10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                              .addGroup(panel2Layout.createSequentialGroup()
                                .addGap(27, 27, 27)
                                .addComponent(WTNC12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                            .addGap(26, 26, 26)
                            .addComponent(WTNC16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(27, 27, 27)
                            .addComponent(WTSS12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addGap(26, 26, 26)
                            .addComponent(WTSS16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(94, 94, 94)
                        .addComponent(WLMT08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGroup(panel2Layout.createParallelGroup()
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(67, 67, 67)
                            .addComponent(WTSA06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(13, 13, 13)
                            .addComponent(WTLI02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addComponent(xTitledSeparator10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(40, 40, 40)
                            .addComponent(WTSA04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addComponent(xTitledSeparator9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGap(26, 26, 26)
                        .addGroup(panel2Layout.createParallelGroup()
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(27, 27, 27)
                            .addGroup(panel2Layout.createParallelGroup()
                              .addGroup(panel2Layout.createSequentialGroup()
                                .addGap(54, 54, 54)
                                .addComponent(WTSA16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addComponent(WTSA12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                              .addGroup(panel2Layout.createSequentialGroup()
                                .addGap(27, 27, 27)
                                .addComponent(WTSA14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addComponent(WTLI10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addGap(26, 26, 26)
                            .addComponent(WTLI14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))))
                    .addGap(26, 26, 26)
                    .addGroup(panel2Layout.createParallelGroup()
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGroup(panel2Layout.createParallelGroup()
                          .addComponent(WLMT20, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(27, 27, 27)
                            .addComponent(WLMT22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                        .addGap(26, 26, 26)
                        .addGroup(panel2Layout.createParallelGroup()
                          .addComponent(WLMT26, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(27, 27, 27)
                            .addComponent(WLMT28, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(54, 54, 54)
                            .addComponent(WLMT30, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(WTNC22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addGroup(panel2Layout.createParallelGroup()
                          .addComponent(WTNC26, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(81, 81, 81)
                            .addComponent(WTNC32, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(27, 27, 27)
                            .addComponent(WTNC28, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(54, 54, 54)
                            .addComponent(WTNC30, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(panel2Layout.createParallelGroup()
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(27, 27, 27)
                            .addComponent(WTSS24, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addComponent(WTSS22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGap(26, 26, 26)
                        .addGroup(panel2Layout.createParallelGroup()
                          .addComponent(WTSS28, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(27, 27, 27)
                            .addComponent(WTSS30, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(WTSA22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(53, 53, 53)
                        .addGroup(panel2Layout.createParallelGroup()
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(54, 54, 54)
                            .addComponent(WTSA32, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(27, 27, 27)
                            .addComponent(WTSA30, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addComponent(WTSA28, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(WTLI22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addComponent(WTLI26, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addGroup(panel2Layout.createParallelGroup()
                          .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(27, 27, 27)
                            .addComponent(WTLI32, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addComponent(WTLI30, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(310, 310, 310)
                    .addComponent(WLMT24, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(121, 121, 121)
                    .addComponent(WTSS10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(310, 310, 310)
                    .addComponent(WTLI24, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(202, 202, 202)
                    .addComponent(WLMT16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(229, 229, 229)
                    .addComponent(WTLI18, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(418, 418, 418)
                    .addComponent(WTSS32, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(13, 13, 13)
                    .addComponent(WTNC02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(175, 175, 175)
                    .addComponent(WTNC14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(364, 364, 364)
                    .addComponent(WTLI28, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(229, 229, 229)
                    .addComponent(WTSS18, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(94, 94, 94)
                    .addComponent(WTSS08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(256, 256, 256)
                    .addComponent(WTSA20, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(40, 40, 40)
                    .addComponent(WTLI04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(13, 13, 13)
                    .addComponent(WTSS02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(121, 121, 121)
                    .addComponent(WTSA10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(xTitledSeparator8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(256, 256, 256)
                    .addComponent(WTSS20, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(148, 148, 148)
                    .addComponent(WLMT12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(94, 94, 94)
                    .addComponent(WTNC08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(418, 418, 418)
                    .addComponent(WLMT32, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(229, 229, 229)
                    .addComponent(WLMT18, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(337, 337, 337)
                    .addComponent(WTSA26, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(148, 148, 148)
                    .addComponent(WTLI12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(310, 310, 310)
                    .addComponent(WTNC24, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(67, 67, 67)
                    .addComponent(WTLI06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(256, 256, 256)
                    .addComponent(WTNC20, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(94, 94, 94)
                    .addComponent(WTSA08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(67, 67, 67)
                    .addComponent(WTNC06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(40, 40, 40)
                    .addComponent(WLMT04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(337, 337, 337)
                    .addComponent(WTSS26, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(67, 67, 67)
                    .addComponent(WTSS06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(256, 256, 256)
                    .addComponent(WTLI20, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(175, 175, 175)
                    .addComponent(WTSS14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(13, 13, 13)
                    .addComponent(WTSA02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(229, 229, 229)
                    .addComponent(WTNC18, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(202, 202, 202)
                    .addComponent(WTLI16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(310, 310, 310)
                    .addComponent(WTSA24, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(94, 94, 94)
                    .addComponent(WTLI08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(229, 229, 229)
                    .addComponent(WTSA18, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
          );
        }
        p_contenu.add(panel2);
        panel2.setBounds(10, 15, 735, 495);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Choix possibles");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiTextField WLMT01;
  private JXTitledSeparator xTitledSeparator1;
  private JXTitledSeparator xTitledSeparator2;
  private JXTitledSeparator xTitledSeparator3;
  private JXTitledSeparator xTitledSeparator4;
  private JXTitledSeparator xTitledSeparator5;
  private XRiTextField WTNC01;
  private XRiTextField WTSS01;
  private XRiTextField WTSA01;
  private XRiTextField WTLI01;
  private XRiTextField WLMT03;
  private XRiTextField WTNC03;
  private XRiTextField WTSS03;
  private XRiTextField WTSA03;
  private XRiTextField WTLI03;
  private XRiTextField WLMT05;
  private XRiTextField WTNC05;
  private XRiTextField WTSS05;
  private XRiTextField WTSA05;
  private XRiTextField WTLI05;
  private XRiTextField WLMT07;
  private XRiTextField WTNC07;
  private XRiTextField WTSS07;
  private XRiTextField WTSA07;
  private XRiTextField WTLI07;
  private XRiTextField WLMT09;
  private XRiTextField WTNC09;
  private XRiTextField WTSS09;
  private XRiTextField WTSA09;
  private XRiTextField WTLI09;
  private XRiTextField WLMT11;
  private XRiTextField WTNC11;
  private XRiTextField WTSS11;
  private XRiTextField WTSA11;
  private XRiTextField WTLI11;
  private XRiTextField WLMT13;
  private XRiTextField WTNC13;
  private XRiTextField WTSS13;
  private XRiTextField WTSA13;
  private XRiTextField WTLI13;
  private XRiTextField WLMT15;
  private XRiTextField WTNC15;
  private XRiTextField WTSS15;
  private XRiTextField WTSA15;
  private XRiTextField WTLI15;
  private XRiTextField WLMT17;
  private XRiTextField WTNC17;
  private XRiTextField WTSS17;
  private XRiTextField WTSA17;
  private XRiTextField WTLI17;
  private XRiTextField WLMT19;
  private XRiTextField WTNC19;
  private XRiTextField WTSS19;
  private XRiTextField WTSA19;
  private XRiTextField WTLI119;
  private XRiTextField WLMT21;
  private XRiTextField WTNC21;
  private XRiTextField WTSS21;
  private XRiTextField WTSA21;
  private XRiTextField WTLI121;
  private XRiTextField WLMT23;
  private XRiTextField WTNC23;
  private XRiTextField WTSS23;
  private XRiTextField WTSA23;
  private XRiTextField WTLI123;
  private XRiTextField WLMT25;
  private XRiTextField WTNC25;
  private XRiTextField WTSS25;
  private XRiTextField WTSA25;
  private XRiTextField WTLI125;
  private XRiTextField WLMT27;
  private XRiTextField WTNC27;
  private XRiTextField WTSS27;
  private XRiTextField WTSA27;
  private XRiTextField WTLI127;
  private XRiTextField WLMT29;
  private XRiTextField WTNC29;
  private XRiTextField WTSS29;
  private XRiTextField WTSA29;
  private XRiTextField WTLI129;
  private XRiTextField WLMT31;
  private XRiTextField WTNC31;
  private XRiTextField WTSS31;
  private XRiTextField WTSA31;
  private XRiTextField WTLI131;
  private XRiTextField WLMT02;
  private JXTitledSeparator xTitledSeparator6;
  private JXTitledSeparator xTitledSeparator7;
  private JXTitledSeparator xTitledSeparator8;
  private JXTitledSeparator xTitledSeparator9;
  private JXTitledSeparator xTitledSeparator10;
  private XRiTextField WTNC02;
  private XRiTextField WTSS02;
  private XRiTextField WTSA02;
  private XRiTextField WTLI02;
  private XRiTextField WLMT04;
  private XRiTextField WTNC04;
  private XRiTextField WTSS04;
  private XRiTextField WTSA04;
  private XRiTextField WTLI04;
  private XRiTextField WLMT06;
  private XRiTextField WTNC06;
  private XRiTextField WTSS06;
  private XRiTextField WTSA06;
  private XRiTextField WTLI06;
  private XRiTextField WLMT08;
  private XRiTextField WTNC08;
  private XRiTextField WTSS08;
  private XRiTextField WTSA08;
  private XRiTextField WTLI08;
  private XRiTextField WLMT10;
  private XRiTextField WTNC10;
  private XRiTextField WTSS10;
  private XRiTextField WTSA10;
  private XRiTextField WTLI10;
  private XRiTextField WLMT12;
  private XRiTextField WTNC12;
  private XRiTextField WTSS12;
  private XRiTextField WTSA12;
  private XRiTextField WTLI12;
  private XRiTextField WLMT14;
  private XRiTextField WTNC14;
  private XRiTextField WTSS14;
  private XRiTextField WTSA14;
  private XRiTextField WTLI14;
  private XRiTextField WLMT16;
  private XRiTextField WTNC16;
  private XRiTextField WTSS16;
  private XRiTextField WTSA16;
  private XRiTextField WTLI16;
  private XRiTextField WLMT18;
  private XRiTextField WTNC18;
  private XRiTextField WTSS18;
  private XRiTextField WTSA18;
  private XRiTextField WTLI18;
  private XRiTextField WLMT20;
  private XRiTextField WTNC20;
  private XRiTextField WTSS20;
  private XRiTextField WTSA20;
  private XRiTextField WTLI20;
  private XRiTextField WLMT22;
  private XRiTextField WTNC22;
  private XRiTextField WTSS22;
  private XRiTextField WTSA22;
  private XRiTextField WTLI22;
  private XRiTextField WLMT24;
  private XRiTextField WTNC24;
  private XRiTextField WTSS24;
  private XRiTextField WTSA24;
  private XRiTextField WTLI24;
  private XRiTextField WLMT26;
  private XRiTextField WTNC26;
  private XRiTextField WTSS26;
  private XRiTextField WTSA26;
  private XRiTextField WTLI26;
  private XRiTextField WLMT28;
  private XRiTextField WTNC28;
  private XRiTextField WTSS28;
  private XRiTextField WTSA28;
  private XRiTextField WTLI28;
  private XRiTextField WLMT30;
  private XRiTextField WTNC30;
  private XRiTextField WTSS30;
  private XRiTextField WTSA30;
  private XRiTextField WTLI30;
  private XRiTextField WLMT32;
  private XRiTextField WTNC32;
  private XRiTextField WTSS32;
  private XRiTextField WTSA32;
  private XRiTextField WTLI32;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
