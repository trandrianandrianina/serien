
package ri.serien.libecranrpg.vcgm.VCGM11FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGM11FM_L2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] L1SNS_Value = { "", "D", "C", };
  private String[] L1SNS_Title = { "", "Débit", "Crédit", };
  
  private boolean dup = false;
  
  public VCGM11FM_L2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    // setRefreshPanelUnderMe(false);
    
    // Ajout
    initDiverses();
    L1SNS.setValeurs(L1SNS_Value, L1SNS_Title);
    L1VTL.setValeursSelection("1", " ");
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    

    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    W1SNS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W1SNS@")).trim());
    W2SNS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W2SNS@")).trim());
    OBJ_60.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W1ATT@")).trim());
    OBJ_25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMTT@")).trim());
    TTVA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TTVA@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    


    
    
    dup = lexique.HostFieldGetData("L1DUPX").trim().equals("*");
    
    // 1° Bloc
    OBJ_74.setVisible(lexique.isTrue("92"));
    W1SNS.setVisible(lexique.isPresent("W1SNS"));
    W2SNS.setVisible(lexique.isPresent("W2SNS"));
    
    // 2° Bloc
    if (lexique.isTrue("85")) {
      OBJ_25.setText(interpreteurD.analyseExpression("@LIBMR1@"));
      OBJ_25.setForeground(Color.RED);
    }
    else {
      OBJ_25.setText(interpreteurD.analyseExpression("@LIBMTT@"));
      OBJ_25.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    if (lexique.isTrue("99")) {
      L1MTT.setForeground(Color.PINK);
    }
    else {
      L1MTT.setForeground((Color) lexique.getSystemDefaultColor("TextField.foreground"));
    }
    
    p_DevTxMontant.setVisible(L1DEV.isVisible());
    OBJ_31.setVisible(L1QTE.isVisible());
    OBJ_46.setVisible(L1DVBX.isVisible());
    OBJ_60.setVisible(lexique.isTrue("77"));
    
    if (lexique.isTrue("79")) {
      OBJ_60.setForeground(Color.RED);
    }
    else {
      OBJ_60.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Ligne de folio"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    if (dup) {
      lexique.HostFieldPutData("L1DUPX", 0, "*");
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(18, 33);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(17, 9);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", true);
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11", false);
  }
  
  private void OBJ_74ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(17, 30);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "DUPLICATE", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    if (dup) {
      OBJ_19.setForeground((Color) lexique.getSystemDefaultColor("Label.Foreground"));
    }
    else {
      OBJ_19.setForeground(Color.GREEN);
    }
    dup = !dup;
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    WCOD = new XRiTextField();
    L1NLI = new XRiTextField();
    L1NCGX = new XRiTextField();
    L1NCA = new XRiTextField();
    OBJ_23 = new JLabel();
    L1DTJ = new XRiTextField();
    OBJ_74 = new SNBoutonDetail();
    LIBCPT = new XRiTextField();
    A1CPL = new XRiTextField();
    A1RUE = new XRiTextField();
    A1CDP = new XRiTextField();
    A1VIL = new XRiTextField();
    OBJ_55 = new JLabel();
    W1SOLD = new XRiTextField();
    USOLDE = new XRiTextField();
    W1SNS = new RiZoneSortie();
    W2SNS = new RiZoneSortie();
    OBJ_60 = new RiZoneSortie();
    panel2 = new JPanel();
    OBJ_25 = new JLabel();
    L1MTT = new XRiTextField();
    OBJ_26 = new JLabel();
    L1SNS = new XRiComboBox();
    OBJ_31 = new JLabel();
    L1QTE = new XRiTextField();
    OBJ_42 = new JLabel();
    L1CLB = new XRiTextField();
    OBJ_43 = new JLabel();
    L1LIB = new XRiTextField();
    OBJ_44 = new JLabel();
    L1RFC = new XRiTextField();
    OBJ_45 = new JLabel();
    L1PCE = new XRiTextField();
    OBJ_46 = new JLabel();
    L1DVBX = new XRiCalendrier();
    OBJ_54 = new JLabel();
    L1NCPX = new XRiTextField();
    l_TypeContrepartie = new JLabel();
    WLNCPT = new XRiTextField();
    L1VTL = new XRiCheckBox();
    p_DevTxMontant = new JPanel();
    OBJ_28 = new JLabel();
    L1DEV = new XRiTextField();
    OBJ_29 = new JLabel();
    WCHGX = new XRiTextField();
    OBJ_30 = new JLabel();
    L1MTD = new XRiTextField();
    W1LIBS = new XRiTextField();
    OBJ_19 = new JButton();
    panel3 = new JPanel();
    L1IN6 = new XRiTextField();
    OBJ_53 = new JLabel();
    TTVA = new RiZoneSortie();
    OBJ_51 = new JLabel();
    L1AFAL = new XRiTextField();
    Lbl_referencefacture = new JLabel();
    L1RPE = new XRiTextField();
    Lbl_datedefacture = new JLabel();
    L1DTRX = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_14 = new JMenuItem();
    OBJ_15 = new JMenuItem();
    OBJ_13 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1225, 305));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Vue du compte");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Plan comptable");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("For\u00e7age en devises");
              riSousMenu_bt8.setToolTipText("For\u00e7age en saisie sur comptes en devises");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Demande de lettrage");
              riSousMenu_bt9.setToolTipText("Demande de lettrage en cours de saisie");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Bloc-notes");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);

            //======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");

              //---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Documents li\u00e9s");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed(e);
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- WCOD ----
          WCOD.setComponentPopupMenu(BTD);
          WCOD.setName("WCOD");
          panel1.add(WCOD);
          WCOD.setBounds(36, 41, 24, WCOD.getPreferredSize().height);

          //---- L1NLI ----
          L1NLI.setComponentPopupMenu(BTD);
          L1NLI.setName("L1NLI");
          panel1.add(L1NLI);
          L1NLI.setBounds(60, 41, 41, L1NLI.getPreferredSize().height);

          //---- L1NCGX ----
          L1NCGX.setComponentPopupMenu(BTD);
          L1NCGX.setName("L1NCGX");
          panel1.add(L1NCGX);
          L1NCGX.setBounds(110, 41, 60, L1NCGX.getPreferredSize().height);

          //---- L1NCA ----
          L1NCA.setComponentPopupMenu(BTD);
          L1NCA.setHorizontalAlignment(SwingConstants.RIGHT);
          L1NCA.setName("L1NCA");
          panel1.add(L1NCA);
          L1NCA.setBounds(170, 41, 60, L1NCA.getPreferredSize().height);

          //---- OBJ_23 ----
          OBJ_23.setText("Le");
          OBJ_23.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_23.setName("OBJ_23");
          panel1.add(OBJ_23);
          OBJ_23.setBounds(260, 41, 25, 28);

          //---- L1DTJ ----
          L1DTJ.setComponentPopupMenu(BTD);
          L1DTJ.setHorizontalAlignment(SwingConstants.RIGHT);
          L1DTJ.setName("L1DTJ");
          panel1.add(L1DTJ);
          L1DTJ.setBounds(290, 41, 26, L1DTJ.getPreferredSize().height);

          //---- OBJ_74 ----
          OBJ_74.setText("");
          OBJ_74.setToolTipText("Recherche p\u00e9riode anglaise (445)");
          OBJ_74.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_74.setName("OBJ_74");
          OBJ_74.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_74ActionPerformed(e);
            }
          });
          panel1.add(OBJ_74);
          OBJ_74.setBounds(323, 41, 27, 28);

          //---- LIBCPT ----
          LIBCPT.setComponentPopupMenu(BTD);
          LIBCPT.setName("LIBCPT");
          panel1.add(LIBCPT);
          LIBCPT.setBounds(36, 74, 310, LIBCPT.getPreferredSize().height);

          //---- A1CPL ----
          A1CPL.setComponentPopupMenu(BTD);
          A1CPL.setName("A1CPL");
          panel1.add(A1CPL);
          A1CPL.setBounds(36, 104, 310, A1CPL.getPreferredSize().height);

          //---- A1RUE ----
          A1RUE.setComponentPopupMenu(BTD);
          A1RUE.setName("A1RUE");
          panel1.add(A1RUE);
          A1RUE.setBounds(36, 134, 310, A1RUE.getPreferredSize().height);

          //---- A1CDP ----
          A1CDP.setComponentPopupMenu(BTD);
          A1CDP.setName("A1CDP");
          panel1.add(A1CDP);
          A1CDP.setBounds(36, 163, 50, A1CDP.getPreferredSize().height);

          //---- A1VIL ----
          A1VIL.setComponentPopupMenu(BTD);
          A1VIL.setName("A1VIL");
          panel1.add(A1VIL);
          A1VIL.setBounds(91, 163, 255, A1VIL.getPreferredSize().height);

          //---- OBJ_55 ----
          OBJ_55.setText("Solde compte");
          OBJ_55.setFont(OBJ_55.getFont().deriveFont(OBJ_55.getFont().getStyle() | Font.BOLD));
          OBJ_55.setName("OBJ_55");
          panel1.add(OBJ_55);
          OBJ_55.setBounds(36, 227, 90, 28);

          //---- W1SOLD ----
          W1SOLD.setComponentPopupMenu(BTD);
          W1SOLD.setFont(W1SOLD.getFont().deriveFont(W1SOLD.getFont().getStyle() | Font.BOLD));
          W1SOLD.setName("W1SOLD");
          panel1.add(W1SOLD);
          W1SOLD.setBounds(136, 227, 80, W1SOLD.getPreferredSize().height);

          //---- USOLDE ----
          USOLDE.setComponentPopupMenu(BTD);
          USOLDE.setFont(USOLDE.getFont().deriveFont(USOLDE.getFont().getStyle() | Font.BOLD));
          USOLDE.setName("USOLDE");
          panel1.add(USOLDE);
          USOLDE.setBounds(136, 227, 110, USOLDE.getPreferredSize().height);

          //---- W1SNS ----
          W1SNS.setText("@W1SNS@");
          W1SNS.setName("W1SNS");
          panel1.add(W1SNS);
          W1SNS.setBounds(226, 229, 31, W1SNS.getPreferredSize().height);

          //---- W2SNS ----
          W2SNS.setText("@W2SNS@");
          W2SNS.setFont(W2SNS.getFont().deriveFont(W2SNS.getFont().getStyle() | Font.BOLD));
          W2SNS.setName("W2SNS");
          panel1.add(W2SNS);
          W2SNS.setBounds(256, 229, 40, W2SNS.getPreferredSize().height);

          //---- OBJ_60 ----
          OBJ_60.setText("@W1ATT@");
          OBJ_60.setForeground(Color.black);
          OBJ_60.setName("OBJ_60");
          panel1.add(OBJ_60);
          OBJ_60.setBounds(36, 194, 306, OBJ_60.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 365, 280);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder(""));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- OBJ_25 ----
          OBJ_25.setText("@LIBMTT@");
          OBJ_25.setName("OBJ_25");
          panel2.add(OBJ_25);
          OBJ_25.setBounds(45, 15, 104, 28);

          //---- L1MTT ----
          L1MTT.setComponentPopupMenu(BTD);
          L1MTT.setHorizontalAlignment(SwingConstants.RIGHT);
          L1MTT.setName("L1MTT");
          panel2.add(L1MTT);
          L1MTT.setBounds(45, 40, 104, L1MTT.getPreferredSize().height);

          //---- OBJ_26 ----
          OBJ_26.setText("Sens");
          OBJ_26.setName("OBJ_26");
          panel2.add(OBJ_26);
          OBJ_26.setBounds(155, 15, 34, 28);

          //---- L1SNS ----
          L1SNS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1SNS.setName("L1SNS");
          panel2.add(L1SNS);
          L1SNS.setBounds(155, 41, 75, L1SNS.getPreferredSize().height);

          //---- OBJ_31 ----
          OBJ_31.setText("Quantit\u00e9");
          OBJ_31.setName("OBJ_31");
          panel2.add(OBJ_31);
          OBJ_31.setBounds(540, 15, 52, 28);

          //---- L1QTE ----
          L1QTE.setComponentPopupMenu(BTD);
          L1QTE.setHorizontalAlignment(SwingConstants.RIGHT);
          L1QTE.setName("L1QTE");
          panel2.add(L1QTE);
          L1QTE.setBounds(535, 40, 104, L1QTE.getPreferredSize().height);

          //---- OBJ_42 ----
          OBJ_42.setText("C");
          OBJ_42.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_42.setName("OBJ_42");
          panel2.add(OBJ_42);
          OBJ_42.setBounds(45, 70, 20, 28);

          //---- L1CLB ----
          L1CLB.setComponentPopupMenu(BTD);
          L1CLB.setName("L1CLB");
          panel2.add(L1CLB);
          L1CLB.setBounds(45, 95, 24, L1CLB.getPreferredSize().height);

          //---- OBJ_43 ----
          OBJ_43.setText("Libell\u00e9");
          OBJ_43.setName("OBJ_43");
          panel2.add(OBJ_43);
          OBJ_43.setBounds(80, 70, 43, 28);

          //---- L1LIB ----
          L1LIB.setComponentPopupMenu(BTD);
          L1LIB.setName("L1LIB");
          panel2.add(L1LIB);
          L1LIB.setBounds(75, 95, 270, L1LIB.getPreferredSize().height);

          //---- OBJ_44 ----
          OBJ_44.setText("R\u00e9f classement");
          OBJ_44.setName("OBJ_44");
          panel2.add(OBJ_44);
          OBJ_44.setBounds(535, 70, 104, 28);

          //---- L1RFC ----
          L1RFC.setComponentPopupMenu(BTD);
          L1RFC.setName("L1RFC");
          panel2.add(L1RFC);
          L1RFC.setBounds(535, 95, 88, L1RFC.getPreferredSize().height);

          //---- OBJ_45 ----
          OBJ_45.setText("N\u00b0pi\u00e8ce");
          OBJ_45.setName("OBJ_45");
          panel2.add(OBJ_45);
          OBJ_45.setBounds(460, 70, 52, 28);

          //---- L1PCE ----
          L1PCE.setComponentPopupMenu(BTD);
          L1PCE.setHorizontalAlignment(SwingConstants.RIGHT);
          L1PCE.setName("L1PCE");
          panel2.add(L1PCE);
          L1PCE.setBounds(455, 95, 68, L1PCE.getPreferredSize().height);

          //---- OBJ_46 ----
          OBJ_46.setText("Date de valeur");
          OBJ_46.setName("OBJ_46");
          panel2.add(OBJ_46);
          OBJ_46.setBounds(345, 130, 100, 28);

          //---- L1DVBX ----
          L1DVBX.setComponentPopupMenu(BTD);
          L1DVBX.setName("L1DVBX");
          panel2.add(L1DVBX);
          L1DVBX.setBounds(345, 160, 105, L1DVBX.getPreferredSize().height);

          //---- OBJ_54 ----
          OBJ_54.setText("Contrepartie");
          OBJ_54.setName("OBJ_54");
          panel2.add(OBJ_54);
          OBJ_54.setBounds(45, 130, 80, 28);

          //---- L1NCPX ----
          L1NCPX.setComponentPopupMenu(BTD);
          L1NCPX.setName("L1NCPX");
          panel2.add(L1NCPX);
          L1NCPX.setBounds(45, 160, 70, L1NCPX.getPreferredSize().height);

          //---- l_TypeContrepartie ----
          l_TypeContrepartie.setText("Type");
          l_TypeContrepartie.setName("l_TypeContrepartie");
          panel2.add(l_TypeContrepartie);
          l_TypeContrepartie.setBounds(130, 130, l_TypeContrepartie.getPreferredSize().width, 28);

          //---- WLNCPT ----
          WLNCPT.setComponentPopupMenu(BTD);
          WLNCPT.setToolTipText("Num\u00e9ro de compte de contrepartie \u00e0 r\u00e9percuter sur la ligne.  Types de r\u00e9percutions possibles.  \"1\" = La ligne suivante de contrepartie est propos\u00e9e automatiquement avec montant, code libell\u00e9, N\u00b0de pi\u00e8ce...  \"2\" = Ligne suivante de contrepartie g\u00e9n\u00e9r\u00e9e automatiquement avec montant, code libell\u00e9, n\u00b0de pi\u00e8ce...");
          WLNCPT.setName("WLNCPT");
          panel2.add(WLNCPT);
          WLNCPT.setBounds(130, 160, 20, WLNCPT.getPreferredSize().height);

          //---- L1VTL ----
          L1VTL.setText("Ventilation \u00e9criture");
          L1VTL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1VTL.setName("L1VTL");
          panel2.add(L1VTL);
          L1VTL.setBounds(200, 160, 135, 28);

          //======== p_DevTxMontant ========
          {
            p_DevTxMontant.setOpaque(false);
            p_DevTxMontant.setName("p_DevTxMontant");
            p_DevTxMontant.setLayout(null);

            //---- OBJ_28 ----
            OBJ_28.setText("Devise");
            OBJ_28.setName("OBJ_28");
            p_DevTxMontant.add(OBJ_28);
            OBJ_28.setBounds(5, 5, 40, 28);

            //---- L1DEV ----
            L1DEV.setComponentPopupMenu(BTD);
            L1DEV.setName("L1DEV");
            p_DevTxMontant.add(L1DEV);
            L1DEV.setBounds(5, 30, 40, L1DEV.getPreferredSize().height);

            //---- OBJ_29 ----
            OBJ_29.setText("Taux");
            OBJ_29.setName("OBJ_29");
            p_DevTxMontant.add(OBJ_29);
            OBJ_29.setBounds(50, 5, 76, 28);

            //---- WCHGX ----
            WCHGX.setComponentPopupMenu(BTD);
            WCHGX.setHorizontalAlignment(SwingConstants.RIGHT);
            WCHGX.setName("WCHGX");
            p_DevTxMontant.add(WCHGX);
            WCHGX.setBounds(50, 30, 76, WCHGX.getPreferredSize().height);

            //---- OBJ_30 ----
            OBJ_30.setText("Montant en devise");
            OBJ_30.setName("OBJ_30");
            p_DevTxMontant.add(OBJ_30);
            OBJ_30.setBounds(130, 5, 104, 28);

            //---- L1MTD ----
            L1MTD.setComponentPopupMenu(BTD);
            L1MTD.setHorizontalAlignment(SwingConstants.RIGHT);
            L1MTD.setName("L1MTD");
            p_DevTxMontant.add(L1MTD);
            L1MTD.setBounds(130, 30, 104, L1MTD.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < p_DevTxMontant.getComponentCount(); i++) {
                Rectangle bounds = p_DevTxMontant.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = p_DevTxMontant.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              p_DevTxMontant.setMinimumSize(preferredSize);
              p_DevTxMontant.setPreferredSize(preferredSize);
            }
          }
          panel2.add(p_DevTxMontant);
          p_DevTxMontant.setBounds(290, 10, 235, p_DevTxMontant.getPreferredSize().height);

          //---- W1LIBS ----
          W1LIBS.setComponentPopupMenu(BTD);
          W1LIBS.setName("W1LIBS");
          panel2.add(W1LIBS);
          W1LIBS.setBounds(345, 95, 104, W1LIBS.getPreferredSize().height);

          //---- OBJ_19 ----
          OBJ_19.setText("Dup");
          OBJ_19.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_19.setName("OBJ_19");
          OBJ_19.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_19ActionPerformed(e);
            }
          });
          panel2.add(OBJ_19);
          OBJ_19.setBounds(235, 37, 55, 35);

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder("TVA"));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- L1IN6 ----
            L1IN6.setName("L1IN6");
            panel3.add(L1IN6);
            L1IN6.setBounds(20, 30, 24, L1IN6.getPreferredSize().height);

            //---- OBJ_53 ----
            OBJ_53.setText("Taux");
            OBJ_53.setName("OBJ_53");
            panel3.add(OBJ_53);
            OBJ_53.setBounds(55, 35, 45, 19);

            //---- TTVA ----
            TTVA.setText("@TTVA@");
            TTVA.setHorizontalAlignment(SwingConstants.RIGHT);
            TTVA.setName("TTVA");
            panel3.add(TTVA);
            TTVA.setBounds(100, 32, 44, TTVA.getPreferredSize().height);

            //---- OBJ_51 ----
            OBJ_51.setText("/");
            OBJ_51.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_51.setName("OBJ_51");
            panel3.add(OBJ_51);
            OBJ_51.setBounds(145, 35, 20, 19);

            //---- L1AFAL ----
            L1AFAL.setName("L1AFAL");
            panel3.add(L1AFAL);
            L1AFAL.setBounds(165, 30, 34, L1AFAL.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }
          panel2.add(panel3);
          panel3.setBounds(45, 195, 225, 70);

          //---- Lbl_referencefacture ----
          Lbl_referencefacture.setText("R\u00e9f\u00e9rence de la facture");
          Lbl_referencefacture.setName("Lbl_referencefacture");
          panel2.add(Lbl_referencefacture);
          Lbl_referencefacture.setBounds(325, 205, 135, 21);

          //---- L1RPE ----
          L1RPE.setComponentPopupMenu(BTD);
          L1RPE.setMinimumSize(new Dimension(164, 28));
          L1RPE.setMaximumSize(new Dimension(164, 28));
          L1RPE.setPreferredSize(new Dimension(164, 28));
          L1RPE.setName("L1RPE");
          panel2.add(L1RPE);
          L1RPE.setBounds(458, 201, 165, L1RPE.getPreferredSize().height);

          //---- Lbl_datedefacture ----
          Lbl_datedefacture.setText("Date r\u00e9elle de facture");
          Lbl_datedefacture.setName("Lbl_datedefacture");
          panel2.add(Lbl_datedefacture);
          Lbl_datedefacture.setBounds(325, 235, 125, 21);

          //---- L1DTRX ----
          L1DTRX.setComponentPopupMenu(BTD);
          L1DTRX.setName("L1DTRX");
          panel2.add(L1DTRX);
          L1DTRX.setBounds(458, 231, 70, L1DTRX.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel2);
        panel2.setBounds(385, 10, 660, 280);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_14 ----
      OBJ_14.setText("Choix possibles");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);

      //---- OBJ_15 ----
      OBJ_15.setText("Duplication");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);

      //---- OBJ_13 ----
      OBJ_13.setText("Aide en ligne");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField WCOD;
  private XRiTextField L1NLI;
  private XRiTextField L1NCGX;
  private XRiTextField L1NCA;
  private JLabel OBJ_23;
  private XRiTextField L1DTJ;
  private SNBoutonDetail OBJ_74;
  private XRiTextField LIBCPT;
  private XRiTextField A1CPL;
  private XRiTextField A1RUE;
  private XRiTextField A1CDP;
  private XRiTextField A1VIL;
  private JLabel OBJ_55;
  private XRiTextField W1SOLD;
  private XRiTextField USOLDE;
  private RiZoneSortie W1SNS;
  private RiZoneSortie W2SNS;
  private RiZoneSortie OBJ_60;
  private JPanel panel2;
  private JLabel OBJ_25;
  private XRiTextField L1MTT;
  private JLabel OBJ_26;
  private XRiComboBox L1SNS;
  private JLabel OBJ_31;
  private XRiTextField L1QTE;
  private JLabel OBJ_42;
  private XRiTextField L1CLB;
  private JLabel OBJ_43;
  private XRiTextField L1LIB;
  private JLabel OBJ_44;
  private XRiTextField L1RFC;
  private JLabel OBJ_45;
  private XRiTextField L1PCE;
  private JLabel OBJ_46;
  private XRiCalendrier L1DVBX;
  private JLabel OBJ_54;
  private XRiTextField L1NCPX;
  private JLabel l_TypeContrepartie;
  private XRiTextField WLNCPT;
  private XRiCheckBox L1VTL;
  private JPanel p_DevTxMontant;
  private JLabel OBJ_28;
  private XRiTextField L1DEV;
  private JLabel OBJ_29;
  private XRiTextField WCHGX;
  private JLabel OBJ_30;
  private XRiTextField L1MTD;
  private XRiTextField W1LIBS;
  private JButton OBJ_19;
  private JPanel panel3;
  private XRiTextField L1IN6;
  private JLabel OBJ_53;
  private RiZoneSortie TTVA;
  private JLabel OBJ_51;
  private XRiTextField L1AFAL;
  private JLabel Lbl_referencefacture;
  private XRiTextField L1RPE;
  private JLabel Lbl_datedefacture;
  private XRiTextField L1DTRX;
  private JPopupMenu BTD;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_15;
  private JMenuItem OBJ_13;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
