
package ri.serien.libecranrpg.vcgm.VCGM01FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM01FM_DC extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] DCAC10_Value = { "", "1", "2", "3", "4", "5", "6", "10", };
  private String[] DCAC09_Value = { "", "1", "2", "3", "4", "5", "6", "10", };
  private String[] DCAC08_Value = { "", "1", "2", "3", "4", "5", "6", "10", };
  private String[] DCAC07_Value = { "", "1", "2", "3", "4", "5", "6", "10", };
  private String[] DCAC06_Value = { "", "1", "2", "3", "4", "5", "6", "10", };
  private String[] DCAC05_Value = { "", "1", "2", "3", "4", "5", "6", "10", };
  private String[] DCAC04_Value = { "", "1", "2", "3", "4", "5", "6", "10", };
  private String[] DCAC03_Value = { "", "1", "2", "3", "4", "5", "6", "10", };
  private String[] DCAC02_Value = { "", "1", "2", "3", "4", "5", "6", "10", };
  private String[] DCAC01_Value = { "", "1", "2", "3", "4", "5", "6", "10", };
  private String[] DCAC_Title = { "0", "Critère sélection n°1 PCA", "Critère sélection n°2 PCA", "Critère sélection n°3 PCA",
      "Critère sélection n°4 PCA", "Critère sélection n°5 PCA", "Catégorie client (CC)", "Situation juridique", };
  
  public VCGM01FM_DC(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    DCAC10.setValeurs(DCAC10_Value, DCAC_Title);
    DCAC09.setValeurs(DCAC09_Value, DCAC_Title);
    DCAC08.setValeurs(DCAC08_Value, DCAC_Title);
    DCAC07.setValeurs(DCAC07_Value, DCAC_Title);
    DCAC06.setValeurs(DCAC06_Value, DCAC_Title);
    DCAC05.setValeurs(DCAC05_Value, DCAC_Title);
    DCAC04.setValeurs(DCAC04_Value, DCAC_Title);
    DCAC03.setValeurs(DCAC03_Value, DCAC_Title);
    DCAC02.setValeurs(DCAC02_Value, DCAC_Title);
    DCAC01.setValeurs(DCAC01_Value, DCAC_Title);
    DCAE10.setValeursSelection("1", " ");
    DCAE09.setValeursSelection("1", " ");
    DCAE08.setValeursSelection("1", " ");
    DCAE07.setValeursSelection("1", " ");
    DCAE06.setValeursSelection("1", " ");
    DCAE05.setValeursSelection("1", " ");
    DCAE04.setValeursSelection("1", " ");
    DCAE03.setValeursSelection("1", " ");
    DCAE02.setValeursSelection("1", " ");
    DCAE01.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    

    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (DCAE10.isSelected())
    // lexique.HostFieldPutData("DCAE10", 0, "1");
    // else
    // lexique.HostFieldPutData("DCAE10", 0, " ");
    // if (DCAE09.isSelected())
    // lexique.HostFieldPutData("DCAE09", 0, "1");
    // else
    // lexique.HostFieldPutData("DCAE09", 0, " ");
    // if (DCAE08.isSelected())
    // lexique.HostFieldPutData("DCAE08", 0, "1");
    // else
    // lexique.HostFieldPutData("DCAE08", 0, " ");
    // if (DCAE07.isSelected())
    // lexique.HostFieldPutData("DCAE07", 0, "1");
    // else
    // lexique.HostFieldPutData("DCAE07", 0, " ");
    // if (DCAE06.isSelected())
    // lexique.HostFieldPutData("DCAE06", 0, "1");
    // else
    // lexique.HostFieldPutData("DCAE06", 0, " ");
    // if (DCAE05.isSelected())
    // lexique.HostFieldPutData("DCAE05", 0, "1");
    // else
    // lexique.HostFieldPutData("DCAE05", 0, " ");
    // if (DCAE04.isSelected())
    // lexique.HostFieldPutData("DCAE04", 0, "1");
    // else
    // lexique.HostFieldPutData("DCAE04", 0, " ");
    // if (DCAE03.isSelected())
    // lexique.HostFieldPutData("DCAE03", 0, "1");
    // else
    // lexique.HostFieldPutData("DCAE03", 0, " ");
    // if (DCAE02.isSelected())
    // lexique.HostFieldPutData("DCAE02", 0, "1");
    // else
    // lexique.HostFieldPutData("DCAE02", 0, " ");
    // if (DCAE01.isSelected())
    // lexique.HostFieldPutData("DCAE01", 0, "1");
    // else
    // lexique.HostFieldPutData("DCAE01", 0, " ");
    // lexique.HostFieldPutData("DCAC10", 0, DCAC10_Value[DCAC10.getSelectedIndex()]);
    // lexique.HostFieldPutData("DCAC09", 0, DCAC09_Value[DCAC09.getSelectedIndex()]);
    // lexique.HostFieldPutData("DCAC08", 0, DCAC08_Value[DCAC08.getSelectedIndex()]);
    // lexique.HostFieldPutData("DCAC07", 0, DCAC07_Value[DCAC07.getSelectedIndex()]);
    // lexique.HostFieldPutData("DCAC06", 0, DCAC06_Value[DCAC06.getSelectedIndex()]);
    // lexique.HostFieldPutData("DCAC05", 0, DCAC05_Value[DCAC05.getSelectedIndex()]);
    // lexique.HostFieldPutData("DCAC04", 0, DCAC04_Value[DCAC04.getSelectedIndex()]);
    // lexique.HostFieldPutData("DCAC03", 0, DCAC03_Value[DCAC03.getSelectedIndex()]);
    // lexique.HostFieldPutData("DCAC02", 0, DCAC02_Value[DCAC02.getSelectedIndex()]);
    // lexique.HostFieldPutData("DCAC01", 0, DCAC01_Value[DCAC01.getSelectedIndex()]);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44_OBJ_45 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_45_OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46_OBJ_47 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    DCAC01 = new XRiComboBox();
    DCAC02 = new XRiComboBox();
    DCAC03 = new XRiComboBox();
    DCAC04 = new XRiComboBox();
    DCAC05 = new XRiComboBox();
    DCAC06 = new XRiComboBox();
    DCAC07 = new XRiComboBox();
    DCAC08 = new XRiComboBox();
    DCAC09 = new XRiComboBox();
    DCAC10 = new XRiComboBox();
    OBJ_127_OBJ_127 = new JLabel();
    OBJ_130_OBJ_130 = new JLabel();
    OBJ_134_OBJ_134 = new JLabel();
    OBJ_137_OBJ_137 = new JLabel();
    OBJ_138_OBJ_138 = new JLabel();
    OBJ_128_OBJ_128 = new JLabel();
    OBJ_131_OBJ_131 = new JLabel();
    OBJ_133_OBJ_133 = new JLabel();
    OBJ_124_OBJ_124 = new JLabel();
    OBJ_135_OBJ_135 = new JLabel();
    OBJ_139_OBJ_139 = new JLabel();
    OBJ_122_OBJ_122 = new JLabel();
    DCCM01 = new XRiTextField();
    DCCM02 = new XRiTextField();
    DCCM03 = new XRiTextField();
    DCCM04 = new XRiTextField();
    DCCM05 = new XRiTextField();
    DCCM06 = new XRiTextField();
    OBJ_126_OBJ_126 = new JLabel();
    OBJ_129_OBJ_129 = new JLabel();
    OBJ_123_OBJ_123 = new JLabel();
    OBJ_125_OBJ_125 = new JLabel();
    OBJ_136_OBJ_136 = new JLabel();
    OBJ_140_OBJ_140 = new JLabel();
    DCAV01 = new XRiTextField();
    DCAV02 = new XRiTextField();
    DCAV03 = new XRiTextField();
    DCAV04 = new XRiTextField();
    DCAV05 = new XRiTextField();
    DCAV06 = new XRiTextField();
    DCAV07 = new XRiTextField();
    DCAV08 = new XRiTextField();
    DCAV09 = new XRiTextField();
    DCAV10 = new XRiTextField();
    DCAP01 = new XRiTextField();
    DCAP02 = new XRiTextField();
    DCAP03 = new XRiTextField();
    DCAP04 = new XRiTextField();
    DCAP05 = new XRiTextField();
    DCAP06 = new XRiTextField();
    DCAP07 = new XRiTextField();
    DCAP08 = new XRiTextField();
    DCAP09 = new XRiTextField();
    DCAP10 = new XRiTextField();
    DCCP01 = new XRiTextField();
    DCCP02 = new XRiTextField();
    DCCP03 = new XRiTextField();
    DCCP04 = new XRiTextField();
    DCCP05 = new XRiTextField();
    DCCP06 = new XRiTextField();
    DCBJ01 = new XRiTextField();
    DCBP01 = new XRiTextField();
    DCBJ02 = new XRiTextField();
    DCBP02 = new XRiTextField();
    DCBJ03 = new XRiTextField();
    DCBP03 = new XRiTextField();
    DCBJ04 = new XRiTextField();
    DCBP04 = new XRiTextField();
    DCBJ05 = new XRiTextField();
    DCBP05 = new XRiTextField();
    DCBJ06 = new XRiTextField();
    DCBP06 = new XRiTextField();
    DCCC01 = new XRiTextField();
    DCCC02 = new XRiTextField();
    DCCC03 = new XRiTextField();
    DCCC04 = new XRiTextField();
    DCCC05 = new XRiTextField();
    DCCC06 = new XRiTextField();
    DCAE01 = new XRiCheckBox();
    DCAE02 = new XRiCheckBox();
    DCAE03 = new XRiCheckBox();
    DCAE04 = new XRiCheckBox();
    DCAE05 = new XRiCheckBox();
    DCAE06 = new XRiCheckBox();
    DCAE07 = new XRiCheckBox();
    DCAE08 = new XRiCheckBox();
    DCAE09 = new XRiCheckBox();
    DCAE10 = new XRiCheckBox();
    OBJ_143_OBJ_143 = new JLabel();
    OBJ_144_OBJ_144 = new JLabel();
    OBJ_145_OBJ_145 = new JLabel();
    OBJ_146_OBJ_146 = new JLabel();
    OBJ_147_OBJ_147 = new JLabel();
    OBJ_148_OBJ_148 = new JLabel();
    separator1 = compFactory.createSeparator("Plafond de couverture par assurance");
    separator2 = compFactory.createSeparator("M\u00e9thode statistique");
    separator3 = compFactory.createSeparator("M\u00e9thode ligne \u00e0 ligne");
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44_OBJ_45 ----
          OBJ_44_OBJ_45.setText("Soci\u00e9t\u00e9");
          OBJ_44_OBJ_45.setName("OBJ_44_OBJ_45");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setName("INDETB");

          //---- OBJ_45_OBJ_46 ----
          OBJ_45_OBJ_46.setText("Code");
          OBJ_45_OBJ_46.setName("OBJ_45_OBJ_46");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setName("INDTYP");

          //---- OBJ_46_OBJ_47 ----
          OBJ_46_OBJ_47.setText("Ordre");
          OBJ_46_OBJ_47.setName("OBJ_46_OBJ_47");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(800, 540));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("D\u00e9pr\u00e9ciation de cr\u00e9ances");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

            //---- DCAC01 ----
            DCAC01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DCAC01.setName("DCAC01");

            //---- DCAC02 ----
            DCAC02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DCAC02.setName("DCAC02");

            //---- DCAC03 ----
            DCAC03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DCAC03.setName("DCAC03");

            //---- DCAC04 ----
            DCAC04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DCAC04.setName("DCAC04");

            //---- DCAC05 ----
            DCAC05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DCAC05.setName("DCAC05");

            //---- DCAC06 ----
            DCAC06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DCAC06.setName("DCAC06");

            //---- DCAC07 ----
            DCAC07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DCAC07.setName("DCAC07");

            //---- DCAC08 ----
            DCAC08.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DCAC08.setName("DCAC08");

            //---- DCAC09 ----
            DCAC09.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DCAC09.setName("DCAC09");

            //---- DCAC10 ----
            DCAC10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DCAC10.setName("DCAC10");

            //---- OBJ_127_OBJ_127 ----
            OBJ_127_OBJ_127.setText("Montant plafond");
            OBJ_127_OBJ_127.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_127_OBJ_127.setName("OBJ_127_OBJ_127");

            //---- OBJ_130_OBJ_130 ----
            OBJ_130_OBJ_130.setText("Montant plafond");
            OBJ_130_OBJ_130.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_130_OBJ_130.setName("OBJ_130_OBJ_130");

            //---- OBJ_134_OBJ_134 ----
            OBJ_134_OBJ_134.setText("Valeur");
            OBJ_134_OBJ_134.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_134_OBJ_134.setName("OBJ_134_OBJ_134");

            //---- OBJ_137_OBJ_137 ----
            OBJ_137_OBJ_137.setText("Code zone");
            OBJ_137_OBJ_137.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_137_OBJ_137.setName("OBJ_137_OBJ_137");

            //---- OBJ_138_OBJ_138 ----
            OBJ_138_OBJ_138.setText("Valeur");
            OBJ_138_OBJ_138.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_138_OBJ_138.setName("OBJ_138_OBJ_138");

            //---- OBJ_128_OBJ_128 ----
            OBJ_128_OBJ_128.setText("% assur\u00e9");
            OBJ_128_OBJ_128.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_128_OBJ_128.setName("OBJ_128_OBJ_128");

            //---- OBJ_131_OBJ_131 ----
            OBJ_131_OBJ_131.setText("% assur\u00e9");
            OBJ_131_OBJ_131.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_131_OBJ_131.setName("OBJ_131_OBJ_131");

            //---- OBJ_133_OBJ_133 ----
            OBJ_133_OBJ_133.setText("Code zone");
            OBJ_133_OBJ_133.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_133_OBJ_133.setName("OBJ_133_OBJ_133");

            //---- OBJ_124_OBJ_124 ----
            OBJ_124_OBJ_124.setText("jours retards");
            OBJ_124_OBJ_124.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_124_OBJ_124.setName("OBJ_124_OBJ_124");

            //---- OBJ_135_OBJ_135 ----
            OBJ_135_OBJ_135.setText("Exclu dep.");
            OBJ_135_OBJ_135.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_135_OBJ_135.setName("OBJ_135_OBJ_135");

            //---- OBJ_139_OBJ_139 ----
            OBJ_139_OBJ_139.setText("Exclu dep.");
            OBJ_139_OBJ_139.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_139_OBJ_139.setName("OBJ_139_OBJ_139");

            //---- OBJ_122_OBJ_122 ----
            OBJ_122_OBJ_122.setText("jours retards");
            OBJ_122_OBJ_122.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_122_OBJ_122.setName("OBJ_122_OBJ_122");

            //---- DCCM01 ----
            DCCM01.setName("DCCM01");

            //---- DCCM02 ----
            DCCM02.setName("DCCM02");

            //---- DCCM03 ----
            DCCM03.setName("DCCM03");

            //---- DCCM04 ----
            DCCM04.setName("DCCM04");

            //---- DCCM05 ----
            DCCM05.setName("DCCM05");

            //---- DCCM06 ----
            DCCM06.setName("DCCM06");

            //---- OBJ_126_OBJ_126 ----
            OBJ_126_OBJ_126.setText("Code");
            OBJ_126_OBJ_126.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_126_OBJ_126.setName("OBJ_126_OBJ_126");

            //---- OBJ_129_OBJ_129 ----
            OBJ_129_OBJ_129.setText("Code");
            OBJ_129_OBJ_129.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_129_OBJ_129.setName("OBJ_129_OBJ_129");

            //---- OBJ_123_OBJ_123 ----
            OBJ_123_OBJ_123.setText("% d\u00e9p");
            OBJ_123_OBJ_123.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_123_OBJ_123.setName("OBJ_123_OBJ_123");

            //---- OBJ_125_OBJ_125 ----
            OBJ_125_OBJ_125.setText("% d\u00e9p");
            OBJ_125_OBJ_125.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_125_OBJ_125.setName("OBJ_125_OBJ_125");

            //---- OBJ_136_OBJ_136 ----
            OBJ_136_OBJ_136.setText("% d\u00e9p.");
            OBJ_136_OBJ_136.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_136_OBJ_136.setName("OBJ_136_OBJ_136");

            //---- OBJ_140_OBJ_140 ----
            OBJ_140_OBJ_140.setText("% d\u00e9p.");
            OBJ_140_OBJ_140.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_140_OBJ_140.setName("OBJ_140_OBJ_140");

            //---- DCAV01 ----
            DCAV01.setName("DCAV01");

            //---- DCAV02 ----
            DCAV02.setName("DCAV02");

            //---- DCAV03 ----
            DCAV03.setName("DCAV03");

            //---- DCAV04 ----
            DCAV04.setName("DCAV04");

            //---- DCAV05 ----
            DCAV05.setName("DCAV05");

            //---- DCAV06 ----
            DCAV06.setName("DCAV06");

            //---- DCAV07 ----
            DCAV07.setName("DCAV07");

            //---- DCAV08 ----
            DCAV08.setName("DCAV08");

            //---- DCAV09 ----
            DCAV09.setName("DCAV09");

            //---- DCAV10 ----
            DCAV10.setName("DCAV10");

            //---- DCAP01 ----
            DCAP01.setName("DCAP01");

            //---- DCAP02 ----
            DCAP02.setName("DCAP02");

            //---- DCAP03 ----
            DCAP03.setName("DCAP03");

            //---- DCAP04 ----
            DCAP04.setName("DCAP04");

            //---- DCAP05 ----
            DCAP05.setName("DCAP05");

            //---- DCAP06 ----
            DCAP06.setName("DCAP06");

            //---- DCAP07 ----
            DCAP07.setName("DCAP07");

            //---- DCAP08 ----
            DCAP08.setName("DCAP08");

            //---- DCAP09 ----
            DCAP09.setName("DCAP09");

            //---- DCAP10 ----
            DCAP10.setName("DCAP10");

            //---- DCCP01 ----
            DCCP01.setName("DCCP01");

            //---- DCCP02 ----
            DCCP02.setName("DCCP02");

            //---- DCCP03 ----
            DCCP03.setName("DCCP03");

            //---- DCCP04 ----
            DCCP04.setName("DCCP04");

            //---- DCCP05 ----
            DCCP05.setName("DCCP05");

            //---- DCCP06 ----
            DCCP06.setName("DCCP06");

            //---- DCBJ01 ----
            DCBJ01.setName("DCBJ01");

            //---- DCBP01 ----
            DCBP01.setName("DCBP01");

            //---- DCBJ02 ----
            DCBJ02.setName("DCBJ02");

            //---- DCBP02 ----
            DCBP02.setName("DCBP02");

            //---- DCBJ03 ----
            DCBJ03.setName("DCBJ03");

            //---- DCBP03 ----
            DCBP03.setName("DCBP03");

            //---- DCBJ04 ----
            DCBJ04.setName("DCBJ04");

            //---- DCBP04 ----
            DCBP04.setName("DCBP04");

            //---- DCBJ05 ----
            DCBJ05.setName("DCBJ05");

            //---- DCBP05 ----
            DCBP05.setName("DCBP05");

            //---- DCBJ06 ----
            DCBJ06.setName("DCBJ06");

            //---- DCBP06 ----
            DCBP06.setName("DCBP06");

            //---- DCCC01 ----
            DCCC01.setName("DCCC01");

            //---- DCCC02 ----
            DCCC02.setName("DCCC02");

            //---- DCCC03 ----
            DCCC03.setName("DCCC03");

            //---- DCCC04 ----
            DCCC04.setName("DCCC04");

            //---- DCCC05 ----
            DCCC05.setName("DCCC05");

            //---- DCCC06 ----
            DCCC06.setName("DCCC06");

            //---- DCAE01 ----
            DCAE01.setText("");
            DCAE01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DCAE01.setName("DCAE01");

            //---- DCAE02 ----
            DCAE02.setText("");
            DCAE02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DCAE02.setName("DCAE02");

            //---- DCAE03 ----
            DCAE03.setText("");
            DCAE03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DCAE03.setName("DCAE03");

            //---- DCAE04 ----
            DCAE04.setText("");
            DCAE04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DCAE04.setName("DCAE04");

            //---- DCAE05 ----
            DCAE05.setText("");
            DCAE05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DCAE05.setName("DCAE05");

            //---- DCAE06 ----
            DCAE06.setText("");
            DCAE06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DCAE06.setName("DCAE06");

            //---- DCAE07 ----
            DCAE07.setText("");
            DCAE07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DCAE07.setName("DCAE07");

            //---- DCAE08 ----
            DCAE08.setText("");
            DCAE08.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DCAE08.setName("DCAE08");

            //---- DCAE09 ----
            DCAE09.setText("");
            DCAE09.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DCAE09.setName("DCAE09");

            //---- DCAE10 ----
            DCAE10.setText("");
            DCAE10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DCAE10.setName("DCAE10");

            //---- OBJ_143_OBJ_143 ----
            OBJ_143_OBJ_143.setText("<");
            OBJ_143_OBJ_143.setName("OBJ_143_OBJ_143");

            //---- OBJ_144_OBJ_144 ----
            OBJ_144_OBJ_144.setText("<");
            OBJ_144_OBJ_144.setName("OBJ_144_OBJ_144");

            //---- OBJ_145_OBJ_145 ----
            OBJ_145_OBJ_145.setText("<");
            OBJ_145_OBJ_145.setName("OBJ_145_OBJ_145");

            //---- OBJ_146_OBJ_146 ----
            OBJ_146_OBJ_146.setText("<");
            OBJ_146_OBJ_146.setName("OBJ_146_OBJ_146");

            //---- OBJ_147_OBJ_147 ----
            OBJ_147_OBJ_147.setText("<");
            OBJ_147_OBJ_147.setName("OBJ_147_OBJ_147");

            //---- OBJ_148_OBJ_148 ----
            OBJ_148_OBJ_148.setText("<");
            OBJ_148_OBJ_148.setName("OBJ_148_OBJ_148");

            //---- separator1 ----
            separator1.setName("separator1");

            //---- separator2 ----
            separator2.setName("separator2");

            //---- separator3 ----
            separator3.setName("separator3");

            GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
            xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
            xTitledPanel1ContentContainerLayout.setHorizontalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(separator3, GroupLayout.PREFERRED_SIZE, 740, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(OBJ_133_OBJ_133, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)
                  .addGap(20, 20, 20)
                  .addComponent(OBJ_134_OBJ_134, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addComponent(OBJ_135_OBJ_135, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_136_OBJ_136, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addGap(25, 25, 25)
                  .addComponent(OBJ_137_OBJ_137, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)
                  .addGap(35, 35, 35)
                  .addComponent(OBJ_138_OBJ_138, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, 0)
                  .addComponent(OBJ_139_OBJ_139, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addComponent(OBJ_140_OBJ_140, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(DCAC03, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAC02, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAC01, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAC05, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAC04, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(DCAV02, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAV03, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAV05, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAV01, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAV04, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addGap(30, 30, 30)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(DCAE01, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAE02, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAE03, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAE04, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAE05, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE))
                  .addGap(28, 28, 28)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(DCAP01, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAP05, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAP04, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAP03, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAP02, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addGap(25, 25, 25)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(DCAC07, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAC06, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAC10, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAC09, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAC08, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(DCAV06, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAV07, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAV08, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAV10, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAV09, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addGap(30, 30, 30)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(DCAE06, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAE07, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAE08, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAE09, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAE10, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE))
                  .addGap(33, 33, 33)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(DCAP06, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAP08, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAP10, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAP09, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCAP07, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(separator2, GroupLayout.PREFERRED_SIZE, 740, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(102, 102, 102)
                  .addComponent(OBJ_122_OBJ_122, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
                  .addGap(8, 8, 8)
                  .addComponent(OBJ_123_OBJ_123, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addGap(209, 209, 209)
                  .addComponent(OBJ_124_OBJ_124, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(OBJ_125_OBJ_125, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(110, 110, 110)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_143_OBJ_143, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_144_OBJ_144, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_145_OBJ_145, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(DCBJ01, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCBJ02, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCBJ03, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addGap(37, 37, 37)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(DCBP01, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCBP03, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCBP02, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addGap(222, 222, 222)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_146_OBJ_146, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_147_OBJ_147, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_148_OBJ_148, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(DCBJ06, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCBJ04, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCBJ05, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addGap(30, 30, 30)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(DCBP06, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCBP04, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCBP05, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(separator1, GroupLayout.PREFERRED_SIZE, 740, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addComponent(OBJ_126_OBJ_126, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
                  .addGap(38, 38, 38)
                  .addComponent(OBJ_127_OBJ_127, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
                  .addGap(19, 19, 19)
                  .addComponent(OBJ_128_OBJ_128, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
                  .addGap(26, 26, 26)
                  .addComponent(OBJ_129_OBJ_129, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
                  .addGap(38, 38, 38)
                  .addComponent(OBJ_130_OBJ_130, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
                  .addGap(24, 24, 24)
                  .addComponent(OBJ_131_OBJ_131, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(30, 30, 30)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(DCCC01, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCCC03, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCCC02, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE))
                  .addGap(70, 70, 70)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(DCCM01, GroupLayout.PREFERRED_SIZE, 61, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCCM02, GroupLayout.PREFERRED_SIZE, 61, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCCM03, GroupLayout.PREFERRED_SIZE, 61, GroupLayout.PREFERRED_SIZE))
                  .addGap(69, 69, 69)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(DCCP01, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCCP03, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCCP02, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addGap(55, 55, 55)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(DCCC04, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCCC05, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCCC06, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE))
                  .addGap(70, 70, 70)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(DCCM04, GroupLayout.PREFERRED_SIZE, 61, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCCM06, GroupLayout.PREFERRED_SIZE, 61, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCCM05, GroupLayout.PREFERRED_SIZE, 61, GroupLayout.PREFERRED_SIZE))
                  .addGap(74, 74, 74)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(DCCP06, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCCP04, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DCCP05, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
            );
            xTitledPanel1ContentContainerLayout.setVerticalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addComponent(separator3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(6, 6, 6)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_133_OBJ_133)
                    .addComponent(OBJ_134_OBJ_134, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_135_OBJ_135, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_136_OBJ_136, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_137_OBJ_137, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_138_OBJ_138, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_139_OBJ_139, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_140_OBJ_140, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(DCAC03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(DCAC02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(DCAC01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGap(24, 24, 24)
                      .addComponent(DCAC05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(75, 75, 75)
                      .addComponent(DCAC04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(DCAV02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(DCAV03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(22, 22, 22)
                      .addComponent(DCAV05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(DCAV01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(75, 75, 75)
                      .addComponent(DCAV04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(DCAE01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(DCAE02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(DCAE03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(DCAE04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(DCAE05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(DCAP01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(22, 22, 22)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(DCAP05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(DCAP04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(DCAP03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(DCAP02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(DCAC07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(DCAC06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGap(24, 24, 24)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(DCAC10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(DCAC09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(DCAC08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(DCAV06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(DCAV07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(DCAV08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(22, 22, 22)
                      .addComponent(DCAV10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(75, 75, 75)
                      .addComponent(DCAV09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(DCAE06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(DCAE07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(DCAE08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(DCAE09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(DCAE10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(DCAP06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(22, 22, 22)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(DCAP08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(DCAP10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(DCAP09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(DCAP07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(12, 12, 12)
                  .addComponent(separator2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(11, 11, 11)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_122_OBJ_122)
                    .addComponent(OBJ_123_OBJ_123)
                    .addComponent(OBJ_124_OBJ_124, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_125_OBJ_125))
                  .addGap(3, 3, 3)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_143_OBJ_143)
                      .addGap(9, 9, 9)
                      .addComponent(OBJ_144_OBJ_144)
                      .addGap(9, 9, 9)
                      .addComponent(OBJ_145_OBJ_145))
                    .addComponent(DCBJ01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(DCBJ02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(DCBJ03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(DCBP01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(22, 22, 22)
                      .addComponent(DCBP03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(DCBP02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_146_OBJ_146)
                      .addGap(9, 9, 9)
                      .addComponent(OBJ_147_OBJ_147)
                      .addGap(9, 9, 9)
                      .addComponent(OBJ_148_OBJ_148))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(DCBJ06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(DCBJ04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(DCBJ05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(DCBP06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(DCBP04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(DCBP05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(17, 17, 17)
                  .addComponent(separator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(11, 11, 11)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_126_OBJ_126)
                    .addComponent(OBJ_127_OBJ_127)
                    .addComponent(OBJ_128_OBJ_128)
                    .addComponent(OBJ_129_OBJ_129)
                    .addComponent(OBJ_130_OBJ_130)
                    .addComponent(OBJ_131_OBJ_131))
                  .addGap(4, 4, 4)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(DCCC01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(DCCC03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(DCCC02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(DCCM01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(DCCM02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(DCCM03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(DCCP01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(22, 22, 22)
                      .addComponent(DCCP03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(DCCP02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(DCCC04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(DCCC05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(DCCC06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(DCCM04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(22, 22, 22)
                      .addComponent(DCCM06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(DCCM05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(DCCP06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(DCCP04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(DCCP05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44_OBJ_45;
  private XRiTextField INDETB;
  private JLabel OBJ_45_OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_46_OBJ_47;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private XRiComboBox DCAC01;
  private XRiComboBox DCAC02;
  private XRiComboBox DCAC03;
  private XRiComboBox DCAC04;
  private XRiComboBox DCAC05;
  private XRiComboBox DCAC06;
  private XRiComboBox DCAC07;
  private XRiComboBox DCAC08;
  private XRiComboBox DCAC09;
  private XRiComboBox DCAC10;
  private JLabel OBJ_127_OBJ_127;
  private JLabel OBJ_130_OBJ_130;
  private JLabel OBJ_134_OBJ_134;
  private JLabel OBJ_137_OBJ_137;
  private JLabel OBJ_138_OBJ_138;
  private JLabel OBJ_128_OBJ_128;
  private JLabel OBJ_131_OBJ_131;
  private JLabel OBJ_133_OBJ_133;
  private JLabel OBJ_124_OBJ_124;
  private JLabel OBJ_135_OBJ_135;
  private JLabel OBJ_139_OBJ_139;
  private JLabel OBJ_122_OBJ_122;
  private XRiTextField DCCM01;
  private XRiTextField DCCM02;
  private XRiTextField DCCM03;
  private XRiTextField DCCM04;
  private XRiTextField DCCM05;
  private XRiTextField DCCM06;
  private JLabel OBJ_126_OBJ_126;
  private JLabel OBJ_129_OBJ_129;
  private JLabel OBJ_123_OBJ_123;
  private JLabel OBJ_125_OBJ_125;
  private JLabel OBJ_136_OBJ_136;
  private JLabel OBJ_140_OBJ_140;
  private XRiTextField DCAV01;
  private XRiTextField DCAV02;
  private XRiTextField DCAV03;
  private XRiTextField DCAV04;
  private XRiTextField DCAV05;
  private XRiTextField DCAV06;
  private XRiTextField DCAV07;
  private XRiTextField DCAV08;
  private XRiTextField DCAV09;
  private XRiTextField DCAV10;
  private XRiTextField DCAP01;
  private XRiTextField DCAP02;
  private XRiTextField DCAP03;
  private XRiTextField DCAP04;
  private XRiTextField DCAP05;
  private XRiTextField DCAP06;
  private XRiTextField DCAP07;
  private XRiTextField DCAP08;
  private XRiTextField DCAP09;
  private XRiTextField DCAP10;
  private XRiTextField DCCP01;
  private XRiTextField DCCP02;
  private XRiTextField DCCP03;
  private XRiTextField DCCP04;
  private XRiTextField DCCP05;
  private XRiTextField DCCP06;
  private XRiTextField DCBJ01;
  private XRiTextField DCBP01;
  private XRiTextField DCBJ02;
  private XRiTextField DCBP02;
  private XRiTextField DCBJ03;
  private XRiTextField DCBP03;
  private XRiTextField DCBJ04;
  private XRiTextField DCBP04;
  private XRiTextField DCBJ05;
  private XRiTextField DCBP05;
  private XRiTextField DCBJ06;
  private XRiTextField DCBP06;
  private XRiTextField DCCC01;
  private XRiTextField DCCC02;
  private XRiTextField DCCC03;
  private XRiTextField DCCC04;
  private XRiTextField DCCC05;
  private XRiTextField DCCC06;
  private XRiCheckBox DCAE01;
  private XRiCheckBox DCAE02;
  private XRiCheckBox DCAE03;
  private XRiCheckBox DCAE04;
  private XRiCheckBox DCAE05;
  private XRiCheckBox DCAE06;
  private XRiCheckBox DCAE07;
  private XRiCheckBox DCAE08;
  private XRiCheckBox DCAE09;
  private XRiCheckBox DCAE10;
  private JLabel OBJ_143_OBJ_143;
  private JLabel OBJ_144_OBJ_144;
  private JLabel OBJ_145_OBJ_145;
  private JLabel OBJ_146_OBJ_146;
  private JLabel OBJ_147_OBJ_147;
  private JLabel OBJ_148_OBJ_148;
  private JComponent separator1;
  private JComponent separator2;
  private JComponent separator3;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
