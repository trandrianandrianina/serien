
package ri.serien.libecranrpg.vcgm.VCGM37FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGM37FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM37FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    LDF01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LDF01@ @DDF01@")).trim());
    LDF02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LDF02@ @DDF02@")).trim());
    LDF03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LDF03@ @DDF03@")).trim());
    LDF04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LDF04@ @DDF04@")).trim());
    LDF05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LDF05@ @DDF05@")).trim());
    LDF06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LDF06@ @DDF06@")).trim());
    LDF07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LDF07@ @DDF07@")).trim());
    LDF08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LDF08@ @DDF08@")).trim());
    LDF09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LDF09@ @DDF09@")).trim());
    LDF10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LDF10@ @DDF10@")).trim());
    LDF11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LDF11@ @DDF11@")).trim());
    LDF12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LDF12@ @DDF12@")).trim());
    IND01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@IND01@")).trim());
    IND02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@IND02@")).trim());
    IND03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@IND03@")).trim());
    IND04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@IND04@")).trim());
    IND05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@IND05@")).trim());
    IND06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@IND06@")).trim());
    IND07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@IND07@")).trim());
    IND08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@IND08@")).trim());
    IND09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@IND09@")).trim());
    IND10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@IND10@")).trim());
    IND11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@IND11@")).trim());
    IND12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@IND12@")).trim());
    label42.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMT1@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    setTitle("Echéances");
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    LDF01 = new JLabel();
    LDF02 = new JLabel();
    LDF03 = new JLabel();
    LDF04 = new JLabel();
    LDF05 = new JLabel();
    LDF06 = new JLabel();
    LDF07 = new JLabel();
    LDF08 = new JLabel();
    LDF09 = new JLabel();
    LDF10 = new JLabel();
    LDF11 = new JLabel();
    LDF12 = new JLabel();
    IND01 = new RiZoneSortie();
    NIF01 = new XRiTextField();
    MTF01 = new XRiTextField();
    ECF01 = new XRiTextField();
    NBJ01 = new XRiTextField();
    RGF01 = new XRiTextField();
    PRF01 = new XRiTextField();
    IND02 = new RiZoneSortie();
    IND03 = new RiZoneSortie();
    IND04 = new RiZoneSortie();
    IND05 = new RiZoneSortie();
    IND06 = new RiZoneSortie();
    IND07 = new RiZoneSortie();
    IND08 = new RiZoneSortie();
    IND09 = new RiZoneSortie();
    IND10 = new RiZoneSortie();
    IND11 = new RiZoneSortie();
    IND12 = new RiZoneSortie();
    NIF02 = new XRiTextField();
    NIF03 = new XRiTextField();
    NIF04 = new XRiTextField();
    NIF05 = new XRiTextField();
    NIF06 = new XRiTextField();
    NIF07 = new XRiTextField();
    NIF08 = new XRiTextField();
    NIF09 = new XRiTextField();
    NIF10 = new XRiTextField();
    NIF11 = new XRiTextField();
    NIF12 = new XRiTextField();
    MTF02 = new XRiTextField();
    MTF03 = new XRiTextField();
    MTF04 = new XRiTextField();
    MTF05 = new XRiTextField();
    MTF06 = new XRiTextField();
    MTF07 = new XRiTextField();
    MTF08 = new XRiTextField();
    MTF09 = new XRiTextField();
    MTF10 = new XRiTextField();
    MTF11 = new XRiTextField();
    MTF12 = new XRiTextField();
    ECF02 = new XRiTextField();
    ECF03 = new XRiTextField();
    ECF04 = new XRiTextField();
    ECF05 = new XRiTextField();
    ECF06 = new XRiTextField();
    ECF07 = new XRiTextField();
    ECF08 = new XRiTextField();
    ECF09 = new XRiTextField();
    ECF10 = new XRiTextField();
    ECF11 = new XRiTextField();
    ECF12 = new XRiTextField();
    NBJ02 = new XRiTextField();
    NBJ03 = new XRiTextField();
    NBJ04 = new XRiTextField();
    NBJ05 = new XRiTextField();
    NBJ06 = new XRiTextField();
    NBJ07 = new XRiTextField();
    NBJ08 = new XRiTextField();
    NBJ09 = new XRiTextField();
    NBJ10 = new XRiTextField();
    NBJ11 = new XRiTextField();
    NBJ12 = new XRiTextField();
    RGF02 = new XRiTextField();
    RGF03 = new XRiTextField();
    RGF04 = new XRiTextField();
    RGF05 = new XRiTextField();
    RGF06 = new XRiTextField();
    RGF07 = new XRiTextField();
    RGF08 = new XRiTextField();
    RGF09 = new XRiTextField();
    RGF10 = new XRiTextField();
    RGF11 = new XRiTextField();
    RGF12 = new XRiTextField();
    PRF02 = new XRiTextField();
    PRF03 = new XRiTextField();
    PRF04 = new XRiTextField();
    PRF05 = new XRiTextField();
    PRF06 = new XRiTextField();
    PRF07 = new XRiTextField();
    PRF09 = new XRiTextField();
    PRF11 = new XRiTextField();
    PRF12 = new XRiTextField();
    PRF08 = new XRiTextField();
    PRF10 = new XRiTextField();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    label40 = new JLabel();
    label41 = new JLabel();
    label42 = new JLabel();
    label43 = new JLabel();
    label44 = new JLabel();
    label45 = new JLabel();
    label46 = new JLabel();
    panel2 = new JPanel();
    NJF = new XRiTextField();
    OBJ_34_OBJ_34 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(845, 460));
    setName("this");
    setLayout(new BorderLayout(10, 10));

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Ventilation des \u00e9ch\u00e9ances"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- LDF01 ----
          LDF01.setText("@LDF01@ @DDF01@");
          LDF01.setBorder(null);
          LDF01.setHorizontalAlignment(SwingConstants.LEFT);
          LDF01.setName("LDF01");
          panel1.add(LDF01);
          LDF01.setBounds(20, 50, 155, 25);

          //---- LDF02 ----
          LDF02.setText("@LDF02@ @DDF02@");
          LDF02.setBorder(null);
          LDF02.setHorizontalAlignment(SwingConstants.LEFT);
          LDF02.setName("LDF02");
          panel1.add(LDF02);
          LDF02.setBounds(20, 80, 155, 25);

          //---- LDF03 ----
          LDF03.setText("@LDF03@ @DDF03@");
          LDF03.setBorder(null);
          LDF03.setHorizontalAlignment(SwingConstants.LEFT);
          LDF03.setName("LDF03");
          panel1.add(LDF03);
          LDF03.setBounds(20, 108, 155, 25);

          //---- LDF04 ----
          LDF04.setText("@LDF04@ @DDF04@");
          LDF04.setBorder(null);
          LDF04.setHorizontalAlignment(SwingConstants.LEFT);
          LDF04.setName("LDF04");
          panel1.add(LDF04);
          LDF04.setBounds(20, 136, 155, 25);

          //---- LDF05 ----
          LDF05.setText("@LDF05@ @DDF05@");
          LDF05.setBorder(null);
          LDF05.setHorizontalAlignment(SwingConstants.LEFT);
          LDF05.setName("LDF05");
          panel1.add(LDF05);
          LDF05.setBounds(20, 164, 155, 25);

          //---- LDF06 ----
          LDF06.setText("@LDF06@ @DDF06@");
          LDF06.setBorder(null);
          LDF06.setHorizontalAlignment(SwingConstants.LEFT);
          LDF06.setName("LDF06");
          panel1.add(LDF06);
          LDF06.setBounds(20, 192, 155, 25);

          //---- LDF07 ----
          LDF07.setText("@LDF07@ @DDF07@");
          LDF07.setBorder(null);
          LDF07.setHorizontalAlignment(SwingConstants.LEFT);
          LDF07.setName("LDF07");
          panel1.add(LDF07);
          LDF07.setBounds(20, 220, 155, 25);

          //---- LDF08 ----
          LDF08.setText("@LDF08@ @DDF08@");
          LDF08.setBorder(null);
          LDF08.setHorizontalAlignment(SwingConstants.LEFT);
          LDF08.setName("LDF08");
          panel1.add(LDF08);
          LDF08.setBounds(20, 248, 155, 25);

          //---- LDF09 ----
          LDF09.setText("@LDF09@ @DDF09@");
          LDF09.setBorder(null);
          LDF09.setHorizontalAlignment(SwingConstants.LEFT);
          LDF09.setName("LDF09");
          panel1.add(LDF09);
          LDF09.setBounds(20, 276, 155, 25);

          //---- LDF10 ----
          LDF10.setText("@LDF10@ @DDF10@");
          LDF10.setBorder(null);
          LDF10.setHorizontalAlignment(SwingConstants.LEFT);
          LDF10.setName("LDF10");
          panel1.add(LDF10);
          LDF10.setBounds(20, 304, 155, 25);

          //---- LDF11 ----
          LDF11.setText("@LDF11@ @DDF11@");
          LDF11.setBorder(null);
          LDF11.setHorizontalAlignment(SwingConstants.LEFT);
          LDF11.setName("LDF11");
          panel1.add(LDF11);
          LDF11.setBounds(20, 332, 155, 25);

          //---- LDF12 ----
          LDF12.setText("@LDF12@ @DDF12@");
          LDF12.setBorder(null);
          LDF12.setHorizontalAlignment(SwingConstants.LEFT);
          LDF12.setName("LDF12");
          panel1.add(LDF12);
          LDF12.setBounds(20, 361, 155, 25);

          //---- IND01 ----
          IND01.setText("@IND01@");
          IND01.setBorder(new BevelBorder(BevelBorder.LOWERED));
          IND01.setHorizontalAlignment(SwingConstants.RIGHT);
          IND01.setName("IND01");
          panel1.add(IND01);
          IND01.setBounds(180, 50, 30, 25);

          //---- NIF01 ----
          NIF01.setName("NIF01");
          panel1.add(NIF01);
          NIF01.setBounds(220, 50, 20, NIF01.getPreferredSize().height);

          //---- MTF01 ----
          MTF01.setName("MTF01");
          panel1.add(MTF01);
          MTF01.setBounds(250, 50, 90, MTF01.getPreferredSize().height);

          //---- ECF01 ----
          ECF01.setHorizontalAlignment(SwingConstants.RIGHT);
          ECF01.setName("ECF01");
          panel1.add(ECF01);
          ECF01.setBounds(350, 50, 60, ECF01.getPreferredSize().height);

          //---- NBJ01 ----
          NBJ01.setName("NBJ01");
          panel1.add(NBJ01);
          NBJ01.setBounds(420, 50, 50, NBJ01.getPreferredSize().height);

          //---- RGF01 ----
          RGF01.setName("RGF01");
          panel1.add(RGF01);
          RGF01.setBounds(480, 50, 30, RGF01.getPreferredSize().height);

          //---- PRF01 ----
          PRF01.setName("PRF01");
          panel1.add(PRF01);
          PRF01.setBounds(520, 50, 80, PRF01.getPreferredSize().height);

          //---- IND02 ----
          IND02.setText("@IND02@");
          IND02.setBorder(new BevelBorder(BevelBorder.LOWERED));
          IND02.setHorizontalAlignment(SwingConstants.RIGHT);
          IND02.setName("IND02");
          panel1.add(IND02);
          IND02.setBounds(180, 80, 30, 25);

          //---- IND03 ----
          IND03.setText("@IND03@");
          IND03.setBorder(new BevelBorder(BevelBorder.LOWERED));
          IND03.setHorizontalAlignment(SwingConstants.RIGHT);
          IND03.setName("IND03");
          panel1.add(IND03);
          IND03.setBounds(180, 108, 30, 25);

          //---- IND04 ----
          IND04.setText("@IND04@");
          IND04.setBorder(new BevelBorder(BevelBorder.LOWERED));
          IND04.setHorizontalAlignment(SwingConstants.RIGHT);
          IND04.setName("IND04");
          panel1.add(IND04);
          IND04.setBounds(180, 136, 30, 25);

          //---- IND05 ----
          IND05.setText("@IND05@");
          IND05.setBorder(new BevelBorder(BevelBorder.LOWERED));
          IND05.setHorizontalAlignment(SwingConstants.RIGHT);
          IND05.setName("IND05");
          panel1.add(IND05);
          IND05.setBounds(180, 164, 30, 25);

          //---- IND06 ----
          IND06.setText("@IND06@");
          IND06.setBorder(new BevelBorder(BevelBorder.LOWERED));
          IND06.setHorizontalAlignment(SwingConstants.RIGHT);
          IND06.setName("IND06");
          panel1.add(IND06);
          IND06.setBounds(180, 192, 30, 25);

          //---- IND07 ----
          IND07.setText("@IND07@");
          IND07.setBorder(new BevelBorder(BevelBorder.LOWERED));
          IND07.setHorizontalAlignment(SwingConstants.RIGHT);
          IND07.setName("IND07");
          panel1.add(IND07);
          IND07.setBounds(180, 220, 30, 25);

          //---- IND08 ----
          IND08.setText("@IND08@");
          IND08.setBorder(new BevelBorder(BevelBorder.LOWERED));
          IND08.setHorizontalAlignment(SwingConstants.RIGHT);
          IND08.setName("IND08");
          panel1.add(IND08);
          IND08.setBounds(180, 248, 30, 25);

          //---- IND09 ----
          IND09.setText("@IND09@");
          IND09.setBorder(new BevelBorder(BevelBorder.LOWERED));
          IND09.setHorizontalAlignment(SwingConstants.RIGHT);
          IND09.setName("IND09");
          panel1.add(IND09);
          IND09.setBounds(180, 276, 30, 25);

          //---- IND10 ----
          IND10.setText("@IND10@");
          IND10.setBorder(new BevelBorder(BevelBorder.LOWERED));
          IND10.setHorizontalAlignment(SwingConstants.RIGHT);
          IND10.setName("IND10");
          panel1.add(IND10);
          IND10.setBounds(180, 304, 30, 25);

          //---- IND11 ----
          IND11.setText("@IND11@");
          IND11.setBorder(new BevelBorder(BevelBorder.LOWERED));
          IND11.setHorizontalAlignment(SwingConstants.RIGHT);
          IND11.setName("IND11");
          panel1.add(IND11);
          IND11.setBounds(180, 332, 30, 25);

          //---- IND12 ----
          IND12.setText("@IND12@");
          IND12.setBorder(new BevelBorder(BevelBorder.LOWERED));
          IND12.setHorizontalAlignment(SwingConstants.RIGHT);
          IND12.setName("IND12");
          panel1.add(IND12);
          IND12.setBounds(180, 361, 30, 25);

          //---- NIF02 ----
          NIF02.setName("NIF02");
          panel1.add(NIF02);
          NIF02.setBounds(220, 78, 20, 28);

          //---- NIF03 ----
          NIF03.setName("NIF03");
          panel1.add(NIF03);
          NIF03.setBounds(220, 106, 20, 28);

          //---- NIF04 ----
          NIF04.setName("NIF04");
          panel1.add(NIF04);
          NIF04.setBounds(220, 134, 20, 28);

          //---- NIF05 ----
          NIF05.setName("NIF05");
          panel1.add(NIF05);
          NIF05.setBounds(220, 162, 20, 28);

          //---- NIF06 ----
          NIF06.setName("NIF06");
          panel1.add(NIF06);
          NIF06.setBounds(220, 190, 20, 28);

          //---- NIF07 ----
          NIF07.setName("NIF07");
          panel1.add(NIF07);
          NIF07.setBounds(220, 218, 20, 28);

          //---- NIF08 ----
          NIF08.setName("NIF08");
          panel1.add(NIF08);
          NIF08.setBounds(220, 246, 20, 28);

          //---- NIF09 ----
          NIF09.setName("NIF09");
          panel1.add(NIF09);
          NIF09.setBounds(220, 274, 20, 28);

          //---- NIF10 ----
          NIF10.setName("NIF10");
          panel1.add(NIF10);
          NIF10.setBounds(220, 302, 20, 28);

          //---- NIF11 ----
          NIF11.setName("NIF11");
          panel1.add(NIF11);
          NIF11.setBounds(220, 330, 20, 28);

          //---- NIF12 ----
          NIF12.setName("NIF12");
          panel1.add(NIF12);
          NIF12.setBounds(220, 358, 20, 28);

          //---- MTF02 ----
          MTF02.setName("MTF02");
          panel1.add(MTF02);
          MTF02.setBounds(250, 78, 90, MTF02.getPreferredSize().height);

          //---- MTF03 ----
          MTF03.setName("MTF03");
          panel1.add(MTF03);
          MTF03.setBounds(250, 106, 90, MTF03.getPreferredSize().height);

          //---- MTF04 ----
          MTF04.setName("MTF04");
          panel1.add(MTF04);
          MTF04.setBounds(250, 134, 90, MTF04.getPreferredSize().height);

          //---- MTF05 ----
          MTF05.setName("MTF05");
          panel1.add(MTF05);
          MTF05.setBounds(250, 162, 90, MTF05.getPreferredSize().height);

          //---- MTF06 ----
          MTF06.setName("MTF06");
          panel1.add(MTF06);
          MTF06.setBounds(250, 190, 90, MTF06.getPreferredSize().height);

          //---- MTF07 ----
          MTF07.setName("MTF07");
          panel1.add(MTF07);
          MTF07.setBounds(250, 218, 90, MTF07.getPreferredSize().height);

          //---- MTF08 ----
          MTF08.setName("MTF08");
          panel1.add(MTF08);
          MTF08.setBounds(250, 246, 90, MTF08.getPreferredSize().height);

          //---- MTF09 ----
          MTF09.setName("MTF09");
          panel1.add(MTF09);
          MTF09.setBounds(250, 274, 90, MTF09.getPreferredSize().height);

          //---- MTF10 ----
          MTF10.setName("MTF10");
          panel1.add(MTF10);
          MTF10.setBounds(250, 302, 90, MTF10.getPreferredSize().height);

          //---- MTF11 ----
          MTF11.setName("MTF11");
          panel1.add(MTF11);
          MTF11.setBounds(250, 330, 90, MTF11.getPreferredSize().height);

          //---- MTF12 ----
          MTF12.setName("MTF12");
          panel1.add(MTF12);
          MTF12.setBounds(250, 358, 90, MTF12.getPreferredSize().height);

          //---- ECF02 ----
          ECF02.setHorizontalAlignment(SwingConstants.RIGHT);
          ECF02.setName("ECF02");
          panel1.add(ECF02);
          ECF02.setBounds(350, 78, 60, ECF02.getPreferredSize().height);

          //---- ECF03 ----
          ECF03.setHorizontalAlignment(SwingConstants.RIGHT);
          ECF03.setName("ECF03");
          panel1.add(ECF03);
          ECF03.setBounds(350, 106, 60, ECF03.getPreferredSize().height);

          //---- ECF04 ----
          ECF04.setHorizontalAlignment(SwingConstants.RIGHT);
          ECF04.setName("ECF04");
          panel1.add(ECF04);
          ECF04.setBounds(350, 134, 60, ECF04.getPreferredSize().height);

          //---- ECF05 ----
          ECF05.setHorizontalAlignment(SwingConstants.RIGHT);
          ECF05.setName("ECF05");
          panel1.add(ECF05);
          ECF05.setBounds(350, 162, 60, ECF05.getPreferredSize().height);

          //---- ECF06 ----
          ECF06.setHorizontalAlignment(SwingConstants.RIGHT);
          ECF06.setName("ECF06");
          panel1.add(ECF06);
          ECF06.setBounds(350, 190, 60, ECF06.getPreferredSize().height);

          //---- ECF07 ----
          ECF07.setHorizontalAlignment(SwingConstants.RIGHT);
          ECF07.setName("ECF07");
          panel1.add(ECF07);
          ECF07.setBounds(350, 218, 60, ECF07.getPreferredSize().height);

          //---- ECF08 ----
          ECF08.setHorizontalAlignment(SwingConstants.RIGHT);
          ECF08.setName("ECF08");
          panel1.add(ECF08);
          ECF08.setBounds(350, 246, 60, ECF08.getPreferredSize().height);

          //---- ECF09 ----
          ECF09.setHorizontalAlignment(SwingConstants.RIGHT);
          ECF09.setName("ECF09");
          panel1.add(ECF09);
          ECF09.setBounds(350, 274, 60, ECF09.getPreferredSize().height);

          //---- ECF10 ----
          ECF10.setHorizontalAlignment(SwingConstants.RIGHT);
          ECF10.setName("ECF10");
          panel1.add(ECF10);
          ECF10.setBounds(350, 302, 60, ECF10.getPreferredSize().height);

          //---- ECF11 ----
          ECF11.setHorizontalAlignment(SwingConstants.RIGHT);
          ECF11.setName("ECF11");
          panel1.add(ECF11);
          ECF11.setBounds(350, 330, 60, ECF11.getPreferredSize().height);

          //---- ECF12 ----
          ECF12.setHorizontalAlignment(SwingConstants.RIGHT);
          ECF12.setName("ECF12");
          panel1.add(ECF12);
          ECF12.setBounds(350, 358, 60, ECF12.getPreferredSize().height);

          //---- NBJ02 ----
          NBJ02.setName("NBJ02");
          panel1.add(NBJ02);
          NBJ02.setBounds(420, 78, 50, NBJ02.getPreferredSize().height);

          //---- NBJ03 ----
          NBJ03.setName("NBJ03");
          panel1.add(NBJ03);
          NBJ03.setBounds(420, 106, 50, NBJ03.getPreferredSize().height);

          //---- NBJ04 ----
          NBJ04.setName("NBJ04");
          panel1.add(NBJ04);
          NBJ04.setBounds(420, 134, 50, NBJ04.getPreferredSize().height);

          //---- NBJ05 ----
          NBJ05.setName("NBJ05");
          panel1.add(NBJ05);
          NBJ05.setBounds(420, 162, 50, NBJ05.getPreferredSize().height);

          //---- NBJ06 ----
          NBJ06.setName("NBJ06");
          panel1.add(NBJ06);
          NBJ06.setBounds(420, 190, 50, NBJ06.getPreferredSize().height);

          //---- NBJ07 ----
          NBJ07.setName("NBJ07");
          panel1.add(NBJ07);
          NBJ07.setBounds(420, 218, 50, NBJ07.getPreferredSize().height);

          //---- NBJ08 ----
          NBJ08.setName("NBJ08");
          panel1.add(NBJ08);
          NBJ08.setBounds(420, 246, 50, NBJ08.getPreferredSize().height);

          //---- NBJ09 ----
          NBJ09.setName("NBJ09");
          panel1.add(NBJ09);
          NBJ09.setBounds(420, 274, 50, NBJ09.getPreferredSize().height);

          //---- NBJ10 ----
          NBJ10.setName("NBJ10");
          panel1.add(NBJ10);
          NBJ10.setBounds(420, 302, 50, NBJ10.getPreferredSize().height);

          //---- NBJ11 ----
          NBJ11.setName("NBJ11");
          panel1.add(NBJ11);
          NBJ11.setBounds(420, 330, 50, NBJ11.getPreferredSize().height);

          //---- NBJ12 ----
          NBJ12.setName("NBJ12");
          panel1.add(NBJ12);
          NBJ12.setBounds(420, 358, 50, NBJ12.getPreferredSize().height);

          //---- RGF02 ----
          RGF02.setName("RGF02");
          panel1.add(RGF02);
          RGF02.setBounds(480, 78, 30, RGF02.getPreferredSize().height);

          //---- RGF03 ----
          RGF03.setName("RGF03");
          panel1.add(RGF03);
          RGF03.setBounds(480, 106, 30, RGF03.getPreferredSize().height);

          //---- RGF04 ----
          RGF04.setName("RGF04");
          panel1.add(RGF04);
          RGF04.setBounds(480, 134, 30, RGF04.getPreferredSize().height);

          //---- RGF05 ----
          RGF05.setName("RGF05");
          panel1.add(RGF05);
          RGF05.setBounds(480, 162, 30, RGF05.getPreferredSize().height);

          //---- RGF06 ----
          RGF06.setName("RGF06");
          panel1.add(RGF06);
          RGF06.setBounds(480, 190, 30, RGF06.getPreferredSize().height);

          //---- RGF07 ----
          RGF07.setName("RGF07");
          panel1.add(RGF07);
          RGF07.setBounds(480, 218, 30, RGF07.getPreferredSize().height);

          //---- RGF08 ----
          RGF08.setName("RGF08");
          panel1.add(RGF08);
          RGF08.setBounds(480, 246, 30, RGF08.getPreferredSize().height);

          //---- RGF09 ----
          RGF09.setName("RGF09");
          panel1.add(RGF09);
          RGF09.setBounds(480, 274, 30, RGF09.getPreferredSize().height);

          //---- RGF10 ----
          RGF10.setName("RGF10");
          panel1.add(RGF10);
          RGF10.setBounds(480, 302, 30, RGF10.getPreferredSize().height);

          //---- RGF11 ----
          RGF11.setName("RGF11");
          panel1.add(RGF11);
          RGF11.setBounds(480, 330, 30, RGF11.getPreferredSize().height);

          //---- RGF12 ----
          RGF12.setName("RGF12");
          panel1.add(RGF12);
          RGF12.setBounds(480, 358, 30, RGF12.getPreferredSize().height);

          //---- PRF02 ----
          PRF02.setName("PRF02");
          panel1.add(PRF02);
          PRF02.setBounds(520, 78, 80, PRF02.getPreferredSize().height);

          //---- PRF03 ----
          PRF03.setName("PRF03");
          panel1.add(PRF03);
          PRF03.setBounds(520, 106, 80, PRF03.getPreferredSize().height);

          //---- PRF04 ----
          PRF04.setName("PRF04");
          panel1.add(PRF04);
          PRF04.setBounds(520, 134, 80, PRF04.getPreferredSize().height);

          //---- PRF05 ----
          PRF05.setName("PRF05");
          panel1.add(PRF05);
          PRF05.setBounds(520, 162, 80, PRF05.getPreferredSize().height);

          //---- PRF06 ----
          PRF06.setName("PRF06");
          panel1.add(PRF06);
          PRF06.setBounds(520, 190, 80, PRF06.getPreferredSize().height);

          //---- PRF07 ----
          PRF07.setName("PRF07");
          panel1.add(PRF07);
          PRF07.setBounds(520, 218, 80, PRF07.getPreferredSize().height);

          //---- PRF09 ----
          PRF09.setName("PRF09");
          panel1.add(PRF09);
          PRF09.setBounds(520, 274, 80, PRF09.getPreferredSize().height);

          //---- PRF11 ----
          PRF11.setName("PRF11");
          panel1.add(PRF11);
          PRF11.setBounds(520, 330, 80, PRF11.getPreferredSize().height);

          //---- PRF12 ----
          PRF12.setName("PRF12");
          panel1.add(PRF12);
          PRF12.setBounds(520, 358, 80, PRF12.getPreferredSize().height);

          //---- PRF08 ----
          PRF08.setName("PRF08");
          panel1.add(PRF08);
          PRF08.setBounds(520, 246, 80, PRF08.getPreferredSize().height);

          //---- PRF10 ----
          PRF10.setName("PRF10");
          panel1.add(PRF10);
          PRF10.setBounds(520, 302, 80, PRF10.getPreferredSize().height);

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          panel1.add(BT_PGUP);
          BT_PGUP.setBounds(610, 50, 25, 147);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          panel1.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(610, 240, 25, 146);

          //---- label40 ----
          label40.setText("In");
          label40.setHorizontalAlignment(SwingConstants.CENTER);
          label40.setFont(label40.getFont().deriveFont(label40.getFont().getStyle() | Font.BOLD));
          label40.setName("label40");
          panel1.add(label40);
          label40.setBounds(180, 30, 30, 18);

          //---- label41 ----
          label41.setText("N");
          label41.setHorizontalAlignment(SwingConstants.CENTER);
          label41.setFont(label41.getFont().deriveFont(label41.getFont().getStyle() | Font.BOLD));
          label41.setName("label41");
          panel1.add(label41);
          label41.setBounds(220, 30, 20, 18);

          //---- label42 ----
          label42.setText("@LIBMT1@");
          label42.setHorizontalAlignment(SwingConstants.CENTER);
          label42.setFont(label42.getFont().deriveFont(label42.getFont().getStyle() | Font.BOLD));
          label42.setName("label42");
          panel1.add(label42);
          label42.setBounds(250, 30, 90, 18);

          //---- label43 ----
          label43.setText("Ech\u00e9ance");
          label43.setHorizontalAlignment(SwingConstants.CENTER);
          label43.setFont(label43.getFont().deriveFont(label43.getFont().getStyle() | Font.BOLD));
          label43.setName("label43");
          panel1.add(label43);
          label43.setBounds(335, 30, 90, 18);

          //---- label44 ----
          label44.setText("Nb. jours");
          label44.setHorizontalAlignment(SwingConstants.CENTER);
          label44.setFont(label44.getFont().deriveFont(label44.getFont().getStyle() | Font.BOLD));
          label44.setName("label44");
          panel1.add(label44);
          label44.setBounds(420, 30, 50, 18);

          //---- label45 ----
          label45.setText("Rg");
          label45.setHorizontalAlignment(SwingConstants.CENTER);
          label45.setFont(label45.getFont().deriveFont(label45.getFont().getStyle() | Font.BOLD));
          label45.setName("label45");
          panel1.add(label45);
          label45.setBounds(480, 30, 30, 18);

          //---- label46 ----
          label46.setText("Pr\u00e9imputation");
          label46.setHorizontalAlignment(SwingConstants.CENTER);
          label46.setFont(label46.getFont().deriveFont(label46.getFont().getStyle() | Font.BOLD));
          label46.setName("label46");
          panel1.add(label46);
          label46.setBounds(520, 30, 80, 18);

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- NJF ----
            NJF.setComponentPopupMenu(BTD);
            NJF.setName("NJF");
            panel2.add(NJF);
            NJF.setBounds(240, 1, 42, NJF.getPreferredSize().height);

            //---- OBJ_34_OBJ_34 ----
            OBJ_34_OBJ_34.setText("Nombre de jours depuis la facture");
            OBJ_34_OBJ_34.setName("OBJ_34_OBJ_34");
            panel2.add(OBJ_34_OBJ_34);
            OBJ_34_OBJ_34.setBounds(10, 3, 225, 25);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel2);
          panel2.setBounds(10, 395, 435, 30);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 655, 435);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel LDF01;
  private JLabel LDF02;
  private JLabel LDF03;
  private JLabel LDF04;
  private JLabel LDF05;
  private JLabel LDF06;
  private JLabel LDF07;
  private JLabel LDF08;
  private JLabel LDF09;
  private JLabel LDF10;
  private JLabel LDF11;
  private JLabel LDF12;
  private RiZoneSortie IND01;
  private XRiTextField NIF01;
  private XRiTextField MTF01;
  private XRiTextField ECF01;
  private XRiTextField NBJ01;
  private XRiTextField RGF01;
  private XRiTextField PRF01;
  private RiZoneSortie IND02;
  private RiZoneSortie IND03;
  private RiZoneSortie IND04;
  private RiZoneSortie IND05;
  private RiZoneSortie IND06;
  private RiZoneSortie IND07;
  private RiZoneSortie IND08;
  private RiZoneSortie IND09;
  private RiZoneSortie IND10;
  private RiZoneSortie IND11;
  private RiZoneSortie IND12;
  private XRiTextField NIF02;
  private XRiTextField NIF03;
  private XRiTextField NIF04;
  private XRiTextField NIF05;
  private XRiTextField NIF06;
  private XRiTextField NIF07;
  private XRiTextField NIF08;
  private XRiTextField NIF09;
  private XRiTextField NIF10;
  private XRiTextField NIF11;
  private XRiTextField NIF12;
  private XRiTextField MTF02;
  private XRiTextField MTF03;
  private XRiTextField MTF04;
  private XRiTextField MTF05;
  private XRiTextField MTF06;
  private XRiTextField MTF07;
  private XRiTextField MTF08;
  private XRiTextField MTF09;
  private XRiTextField MTF10;
  private XRiTextField MTF11;
  private XRiTextField MTF12;
  private XRiTextField ECF02;
  private XRiTextField ECF03;
  private XRiTextField ECF04;
  private XRiTextField ECF05;
  private XRiTextField ECF06;
  private XRiTextField ECF07;
  private XRiTextField ECF08;
  private XRiTextField ECF09;
  private XRiTextField ECF10;
  private XRiTextField ECF11;
  private XRiTextField ECF12;
  private XRiTextField NBJ02;
  private XRiTextField NBJ03;
  private XRiTextField NBJ04;
  private XRiTextField NBJ05;
  private XRiTextField NBJ06;
  private XRiTextField NBJ07;
  private XRiTextField NBJ08;
  private XRiTextField NBJ09;
  private XRiTextField NBJ10;
  private XRiTextField NBJ11;
  private XRiTextField NBJ12;
  private XRiTextField RGF02;
  private XRiTextField RGF03;
  private XRiTextField RGF04;
  private XRiTextField RGF05;
  private XRiTextField RGF06;
  private XRiTextField RGF07;
  private XRiTextField RGF08;
  private XRiTextField RGF09;
  private XRiTextField RGF10;
  private XRiTextField RGF11;
  private XRiTextField RGF12;
  private XRiTextField PRF02;
  private XRiTextField PRF03;
  private XRiTextField PRF04;
  private XRiTextField PRF05;
  private XRiTextField PRF06;
  private XRiTextField PRF07;
  private XRiTextField PRF09;
  private XRiTextField PRF11;
  private XRiTextField PRF12;
  private XRiTextField PRF08;
  private XRiTextField PRF10;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JLabel label40;
  private JLabel label41;
  private JLabel label42;
  private JLabel label43;
  private JLabel label44;
  private JLabel label45;
  private JLabel label46;
  private JPanel panel2;
  private XRiTextField NJF;
  private JLabel OBJ_34_OBJ_34;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
