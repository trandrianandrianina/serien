
package ri.serien.libecranrpg.vcgm.VCGMG1FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGMG1FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _TIT1_Title = { "TITRZ", };
  private String[][] _TIT1_Data = { { "TIT1", }, { "TIT2", }, { "TIT3", }, { "TIT4", }, { "TIT5", }, };
  private int[] _TIT1_Width = { 780, };
  // private String[] _LIST_Top=null;
  
  public VCGMG1FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    ETINI1.setValeursSelection("X", " ");
    ETSAU4.setValeursSelection("X", " ");
    ETTOT4.setValeursSelection("X", " ");
    ETINI4.setValeursSelection("X", " ");
    ETSAU3.setValeursSelection("X", " ");
    ETTOT3.setValeursSelection("X", " ");
    ETINI3.setValeursSelection("X", " ");
    ETSAU2.setValeursSelection("X", " ");
    ETTOT2.setValeursSelection("X", " ");
    ETINI2.setValeursSelection("X", " ");
    ETSAU1.setValeursSelection("X", " ");
    ETTOT1.setValeursSelection("X", " ");
    ETDET.setValeursSelection("X", " ");
    ETGFCH.setValeursSelection("1", " ");
    TIT1.setAspectTable(null, _TIT1_Title, _TIT1_Data, _TIT1_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    xTitledPanel1.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBFCH@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _LIST_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    // ETINI1.setSelected(lexique.HostFieldGetData("ETINI1").equalsIgnoreCase("X"));
    // ETSAU4.setSelected(lexique.HostFieldGetData("ETSAU4").equalsIgnoreCase("X"));
    // ETTOT4.setSelected(lexique.HostFieldGetData("ETTOT4").equalsIgnoreCase("X"));
    // ETINI4.setSelected(lexique.HostFieldGetData("ETINI4").equalsIgnoreCase("X"));
    // ETSAU3.setSelected(lexique.HostFieldGetData("ETSAU3").equalsIgnoreCase("X"));
    // ETTOT3.setSelected(lexique.HostFieldGetData("ETTOT3").equalsIgnoreCase("X"));
    // ETINI3.setSelected(lexique.HostFieldGetData("ETINI3").equalsIgnoreCase("X"));
    // ETSAU2.setSelected(lexique.HostFieldGetData("ETSAU2").equalsIgnoreCase("X"));
    // ETTOT2.setSelected(lexique.HostFieldGetData("ETTOT2").equalsIgnoreCase("X"));
    // ETINI2.setSelected(lexique.HostFieldGetData("ETINI2").equalsIgnoreCase("X"));
    // ETSAU1.setSelected(lexique.HostFieldGetData("ETSAU1").equalsIgnoreCase("X"));
    // ETTOT1.setSelected(lexique.HostFieldGetData("ETTOT1").equalsIgnoreCase("X"));
    // ETDET.setSelected(lexique.HostFieldGetData("ETDET").equalsIgnoreCase("X"));
    // ETGFCH.setSelected(lexique.HostFieldGetData("ETGFCH").equalsIgnoreCase("1"));
    
    // TODO Icones
    BT_prec.setIcon(lexique.chargerImage("images/fleche_pre.png", true));
    BT_suit.setIcon(lexique.chargerImage("images/fleche_sui.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - Personnalisation d'états de @LOCGRP/-1/@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (ETINI1.isSelected())
    // lexique.HostFieldPutData("ETINI1", 0, "X");
    // else
    // lexique.HostFieldPutData("ETINI1", 0, " ");
    // if (ETSAU4.isSelected())
    // lexique.HostFieldPutData("ETSAU4", 0, "X");
    // else
    // lexique.HostFieldPutData("ETSAU4", 0, " ");
    // if (ETTOT4.isSelected())
    // lexique.HostFieldPutData("ETTOT4", 0, "X");
    // else
    // lexique.HostFieldPutData("ETTOT4", 0, " ");
    // if (ETINI4.isSelected())
    // lexique.HostFieldPutData("ETINI4", 0, "X");
    // else
    // lexique.HostFieldPutData("ETINI4", 0, " ");
    // if (ETSAU3.isSelected())
    // lexique.HostFieldPutData("ETSAU3", 0, "X");
    // else
    // lexique.HostFieldPutData("ETSAU3", 0, " ");
    // if (ETTOT3.isSelected())
    // lexique.HostFieldPutData("ETTOT3", 0, "X");
    // else
    // lexique.HostFieldPutData("ETTOT3", 0, " ");
    // if (ETINI3.isSelected())
    // lexique.HostFieldPutData("ETINI3", 0, "X");
    // else
    // lexique.HostFieldPutData("ETINI3", 0, " ");
    // if (ETSAU2.isSelected())
    // lexique.HostFieldPutData("ETSAU2", 0, "X");
    // else
    // lexique.HostFieldPutData("ETSAU2", 0, " ");
    // if (ETTOT2.isSelected())
    // lexique.HostFieldPutData("ETTOT2", 0, "X");
    // else
    // lexique.HostFieldPutData("ETTOT2", 0, " ");
    // if (ETINI2.isSelected())
    // lexique.HostFieldPutData("ETINI2", 0, "X");
    // else
    // lexique.HostFieldPutData("ETINI2", 0, " ");
    // if (ETSAU1.isSelected())
    // lexique.HostFieldPutData("ETSAU1", 0, "X");
    // else
    // lexique.HostFieldPutData("ETSAU1", 0, " ");
    // if (ETTOT1.isSelected())
    // lexique.HostFieldPutData("ETTOT1", 0, "X");
    // else
    // lexique.HostFieldPutData("ETTOT1", 0, " ");
    // if (ETDET.isSelected())
    // lexique.HostFieldPutData("ETDET", 0, "X");
    // else
    // lexique.HostFieldPutData("ETDET", 0, " ");
    // if (ETGFCH.isSelected())
    // lexique.HostFieldPutData("ETGFCH", 0, "1");
    // else
    // lexique.HostFieldPutData("ETGFCH", 0, " ");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void BT_PGUPActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", false);
  }
  
  private void BT_PGDOWNActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_44 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    SCROLLPANE_LIST2 = new JScrollPane();
    TIT1 = new XRiTable();
    ETLIB = new XRiTextField();
    ETTL1 = new XRiTextField();
    ETTL2 = new XRiTextField();
    ETTL3 = new XRiTextField();
    ETTL4 = new XRiTextField();
    OBJ_64 = new JLabel();
    ETGFCH = new XRiCheckBox();
    ETDET = new XRiCheckBox();
    OBJ_65 = new JLabel();
    OBJ_63 = new JLabel();
    OBJ_70 = new JLabel();
    ETT1 = new XRiTextField();
    ETT2 = new XRiTextField();
    ETT3 = new XRiTextField();
    ETT4 = new XRiTextField();
    ETNBL = new XRiTextField();
    ETTOT1 = new XRiCheckBox();
    ETSAU1 = new XRiCheckBox();
    ETINI2 = new XRiCheckBox();
    ETTOT2 = new XRiCheckBox();
    ETSAU2 = new XRiCheckBox();
    ETINI3 = new XRiCheckBox();
    ETTOT3 = new XRiCheckBox();
    ETSAU3 = new XRiCheckBox();
    ETINI4 = new XRiCheckBox();
    ETTOT4 = new XRiCheckBox();
    ETSAU4 = new XRiCheckBox();
    ETINI1 = new XRiCheckBox();
    BT_suit = new JButton();
    BT_prec = new JButton();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation des \u00e9tats de comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_42 ----
          OBJ_42.setText("Soci\u00e9t\u00e9");
          OBJ_42.setName("OBJ_42");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setName("INDTYP");

          //---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(840, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("@LIBFCH@");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

            //======== SCROLLPANE_LIST2 ========
            {
              SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

              //---- TIT1 ----
              TIT1.setName("TIT1");
              SCROLLPANE_LIST2.setViewportView(TIT1);
            }

            //---- ETLIB ----
            ETLIB.setComponentPopupMenu(BTD);
            ETLIB.setName("ETLIB");

            //---- ETTL1 ----
            ETTL1.setName("ETTL1");

            //---- ETTL2 ----
            ETTL2.setName("ETTL2");

            //---- ETTL3 ----
            ETTL3.setName("ETTL3");

            //---- ETTL4 ----
            ETTL4.setName("ETTL4");

            //---- OBJ_64 ----
            OBJ_64.setText("N\u00b0 de zone(s) pour tri et rupture");
            OBJ_64.setName("OBJ_64");

            //---- ETGFCH ----
            ETGFCH.setText("G\u00e9n\u00e9ration d'un fichier disque");
            ETGFCH.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ETGFCH.setName("ETGFCH");

            //---- ETDET ----
            ETDET.setText("Edition d\u00e9tail");
            ETDET.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ETDET.setName("ETDET");

            //---- OBJ_65 ----
            OBJ_65.setText("Init.    Total    Saut");
            OBJ_65.setName("OBJ_65");

            //---- OBJ_63 ----
            OBJ_63.setText("Hauteur Imprim\u00e9");
            OBJ_63.setName("OBJ_63");

            //---- OBJ_70 ----
            OBJ_70.setText("Titre");
            OBJ_70.setName("OBJ_70");

            //---- ETT1 ----
            ETT1.setComponentPopupMenu(BTD);
            ETT1.setName("ETT1");

            //---- ETT2 ----
            ETT2.setComponentPopupMenu(BTD);
            ETT2.setName("ETT2");

            //---- ETT3 ----
            ETT3.setComponentPopupMenu(BTD);
            ETT3.setName("ETT3");

            //---- ETT4 ----
            ETT4.setComponentPopupMenu(BTD);
            ETT4.setName("ETT4");

            //---- ETNBL ----
            ETNBL.setComponentPopupMenu(BTD);
            ETNBL.setName("ETNBL");

            //---- ETTOT1 ----
            ETTOT1.setText("");
            ETTOT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ETTOT1.setName("ETTOT1");

            //---- ETSAU1 ----
            ETSAU1.setText("");
            ETSAU1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ETSAU1.setName("ETSAU1");

            //---- ETINI2 ----
            ETINI2.setText("");
            ETINI2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ETINI2.setName("ETINI2");

            //---- ETTOT2 ----
            ETTOT2.setText("");
            ETTOT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ETTOT2.setName("ETTOT2");

            //---- ETSAU2 ----
            ETSAU2.setText("");
            ETSAU2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ETSAU2.setName("ETSAU2");

            //---- ETINI3 ----
            ETINI3.setText("");
            ETINI3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ETINI3.setName("ETINI3");

            //---- ETTOT3 ----
            ETTOT3.setText("");
            ETTOT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ETTOT3.setName("ETTOT3");

            //---- ETSAU3 ----
            ETSAU3.setText("");
            ETSAU3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ETSAU3.setName("ETSAU3");

            //---- ETINI4 ----
            ETINI4.setText("");
            ETINI4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ETINI4.setName("ETINI4");

            //---- ETTOT4 ----
            ETTOT4.setText("");
            ETTOT4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ETTOT4.setName("ETTOT4");

            //---- ETSAU4 ----
            ETSAU4.setText("");
            ETSAU4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ETSAU4.setName("ETSAU4");

            //---- ETINI1 ----
            ETINI1.setText("");
            ETINI1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ETINI1.setName("ETINI1");

            //---- BT_suit ----
            BT_suit.setText("");
            BT_suit.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_suit.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_suit.setName("BT_suit");
            BT_suit.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                BT_PGUPActionPerformed(e);
              }
            });

            //---- BT_prec ----
            BT_prec.setText("");
            BT_prec.setToolTipText("Page suivante");
            BT_prec.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_prec.setName("BT_prec");
            BT_prec.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                BT_PGDOWNActionPerformed(e);
              }
            });

            GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
            xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
            xTitledPanel1ContentContainerLayout.setHorizontalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_70, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(ETLIB, GroupLayout.PREFERRED_SIZE, 610, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(485, 485, 485)
                          .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE)))
                      .addGap(15, 15, 15)
                      .addComponent(ETNBL, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 217, GroupLayout.PREFERRED_SIZE)
                      .addGap(93, 93, 93)
                      .addComponent(OBJ_65, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE)
                      .addGap(67, 67, 67)
                      .addComponent(ETDET, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(ETT1, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                      .addGap(1, 1, 1)
                      .addComponent(ETTL1, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                      .addGap(40, 40, 40)
                      .addComponent(ETINI1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(17, 17, 17)
                      .addComponent(ETTOT1, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                      .addGap(13, 13, 13)
                      .addComponent(ETSAU1, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                      .addGap(78, 78, 78)
                      .addComponent(ETGFCH, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(ETT2, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                      .addGap(1, 1, 1)
                      .addComponent(ETTL2, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                      .addGap(40, 40, 40)
                      .addComponent(ETINI2, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                      .addGap(13, 13, 13)
                      .addComponent(ETTOT2, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                      .addGap(13, 13, 13)
                      .addComponent(ETSAU2, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(ETT3, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                      .addGap(1, 1, 1)
                      .addComponent(ETTL3, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                      .addGap(40, 40, 40)
                      .addComponent(ETINI3, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                      .addGap(13, 13, 13)
                      .addComponent(ETTOT3, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                      .addGap(13, 13, 13)
                      .addComponent(ETSAU3, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(ETT4, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                      .addGap(1, 1, 1)
                      .addComponent(ETTL4, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                      .addGap(40, 40, 40)
                      .addComponent(ETINI4, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                      .addGap(13, 13, 13)
                      .addComponent(ETTOT4, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                      .addGap(13, 13, 13)
                      .addComponent(ETSAU4, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
                    .addComponent(SCROLLPANE_LIST2, GroupLayout.PREFERRED_SIZE, 780, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(BT_prec, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
                      .addGap(160, 160, 160)
                      .addComponent(BT_suit, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)))
                  .addContainerGap(19, Short.MAX_VALUE))
            );
            xTitledPanel1ContentContainerLayout.setVerticalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addComponent(OBJ_70, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addGap(10, 10, 10)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(ETLIB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ETNBL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(12, 12, 12)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_65, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ETDET, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGap(10, 10, 10)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(ETT1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ETTL1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(ETINI1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ETTOT1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ETSAU1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ETGFCH, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
                  .addGap(7, 7, 7)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(ETT2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ETTL2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(ETINI2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ETTOT2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ETSAU2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
                  .addGap(7, 7, 7)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(ETT3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ETTL3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(ETINI3, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ETTOT3, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ETSAU3, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
                  .addGap(7, 7, 7)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(ETT4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ETTL4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(ETINI4, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ETTOT4, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ETSAU4, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
                  .addGap(32, 32, 32)
                  .addComponent(SCROLLPANE_LIST2, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(BT_prec, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                    .addComponent(BT_suit, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
                  .addContainerGap(23, Short.MAX_VALUE))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private XRiTextField INDETB;
  private JLabel OBJ_44;
  private XRiTextField INDTYP;
  private JLabel OBJ_46;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable TIT1;
  private XRiTextField ETLIB;
  private XRiTextField ETTL1;
  private XRiTextField ETTL2;
  private XRiTextField ETTL3;
  private XRiTextField ETTL4;
  private JLabel OBJ_64;
  private XRiCheckBox ETGFCH;
  private XRiCheckBox ETDET;
  private JLabel OBJ_65;
  private JLabel OBJ_63;
  private JLabel OBJ_70;
  private XRiTextField ETT1;
  private XRiTextField ETT2;
  private XRiTextField ETT3;
  private XRiTextField ETT4;
  private XRiTextField ETNBL;
  private XRiCheckBox ETTOT1;
  private XRiCheckBox ETSAU1;
  private XRiCheckBox ETINI2;
  private XRiCheckBox ETTOT2;
  private XRiCheckBox ETSAU2;
  private XRiCheckBox ETINI3;
  private XRiCheckBox ETTOT3;
  private XRiCheckBox ETSAU3;
  private XRiCheckBox ETINI4;
  private XRiCheckBox ETTOT4;
  private XRiCheckBox ETSAU4;
  private XRiCheckBox ETINI1;
  private JButton BT_suit;
  private JButton BT_prec;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
