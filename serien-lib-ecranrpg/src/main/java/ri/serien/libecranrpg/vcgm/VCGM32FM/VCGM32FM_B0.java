
package ri.serien.libecranrpg.vcgm.VCGM32FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGM32FM_B0 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM32FM_B0(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_11_OBJ_11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1LIB@")).trim());
    OBJ_22_OBJ_22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBNAT@")).trim());
    OBJ_20_OBJ_20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAF@")).trim());
    OBJ_18_OBJ_18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBSA@")).trim());
    L1LA1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1LA1@")).trim());
    L1LA2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1LA2@")).trim());
    L1LA3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1LA3@")).trim());
    L1LA4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1LA4@")).trim());
    L1LA5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1LA5@")).trim());
    L1LA6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1LA6@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    // riBoutonInfo1.gererAffichageV03F(lexique.isTrue("19"),lexique.getConvertInfosFiche(lexique.HostFieldGetData("V03F").trim()));
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    // if(riSousMenu_bt_export instanceof RiSousMenu_bt)
    // lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " +
    // lexique.HostFieldGetData("TITPG2").trim());
    
    
    L1SAN.setEnabled(lexique.isPresent("L1SAN"));
    L1NAT.setEnabled(lexique.isPresent("L1NAT"));
    L1AA6.setVisible(lexique.isPresent("L1AA6"));
    L1AA5.setVisible(lexique.isPresent("L1AA5"));
    L1AA4.setVisible(lexique.isPresent("L1AA4"));
    L1AA3.setVisible(lexique.isPresent("L1AA3"));
    L1AA2.setVisible(lexique.isPresent("L1AA2"));
    L1AA1.setVisible(lexique.isPresent("L1AA1"));
    L1ACT.setEnabled(lexique.isPresent("L1ACT"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@WLIB@"));
    
    

    
    
    // riMenu_bt2.setIcon(lexique.getImage("images/options.png",true));
    // riMenu_bt3.setIcon(lexique.getImage("images/outils.png", true));
    // riMenu_bt4.setIcon(lexique.getImage("images/fonctions.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_11_OBJ_11 = new RiZoneSortie();
    OBJ_12_OBJ_12 = new JLabel();
    OBJ_10_OBJ_10 = new JLabel();
    OBJ_21_OBJ_21 = new JLabel();
    OBJ_19_OBJ_19 = new JLabel();
    L1ACT = new XRiTextField();
    L1AA1 = new XRiTextField();
    L1NAT = new XRiTextField();
    L1SAN = new XRiTextField();
    OBJ_22_OBJ_22 = new RiZoneSortie();
    OBJ_20_OBJ_20 = new RiZoneSortie();
    OBJ_18_OBJ_18 = new RiZoneSortie();
    L1AA2 = new XRiTextField();
    L1AA3 = new XRiTextField();
    L1AA4 = new XRiTextField();
    L1AA5 = new XRiTextField();
    L1AA6 = new XRiTextField();
    separator1 = compFactory.createSeparator("Axes");
    L1LA1 = new RiZoneSortie();
    L1LA2 = new RiZoneSortie();
    L1LA3 = new RiZoneSortie();
    L1LA4 = new RiZoneSortie();
    L1LA5 = new RiZoneSortie();
    L1LA6 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_6 = new JMenuItem();
    OBJ_5 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(735, 450));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Modification de l'imputation analytique"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_11_OBJ_11 ----
          OBJ_11_OBJ_11.setText("@L1LIB@");
          OBJ_11_OBJ_11.setName("OBJ_11_OBJ_11");
          panel1.add(OBJ_11_OBJ_11);
          OBJ_11_OBJ_11.setBounds(105, 30, 402, 28);

          //---- OBJ_12_OBJ_12 ----
          OBJ_12_OBJ_12.setText("Section");
          OBJ_12_OBJ_12.setName("OBJ_12_OBJ_12");
          panel1.add(OBJ_12_OBJ_12);
          OBJ_12_OBJ_12.setBounds(25, 71, 67, 20);

          //---- OBJ_10_OBJ_10 ----
          OBJ_10_OBJ_10.setText("Libelle");
          OBJ_10_OBJ_10.setName("OBJ_10_OBJ_10");
          panel1.add(OBJ_10_OBJ_10);
          OBJ_10_OBJ_10.setBounds(25, 34, 61, 20);

          //---- OBJ_21_OBJ_21 ----
          OBJ_21_OBJ_21.setText("Nature");
          OBJ_21_OBJ_21.setName("OBJ_21_OBJ_21");
          panel1.add(OBJ_21_OBJ_21);
          OBJ_21_OBJ_21.setBounds(25, 145, 61, 20);

          //---- OBJ_19_OBJ_19 ----
          OBJ_19_OBJ_19.setText("Affaire");
          OBJ_19_OBJ_19.setName("OBJ_19_OBJ_19");
          panel1.add(OBJ_19_OBJ_19);
          OBJ_19_OBJ_19.setBounds(25, 108, 60, 20);

          //---- L1ACT ----
          L1ACT.setComponentPopupMenu(BTD);
          L1ACT.setName("L1ACT");
          panel1.add(L1ACT);
          L1ACT.setBounds(105, 104, 62, L1ACT.getPreferredSize().height);

          //---- L1AA1 ----
          L1AA1.setComponentPopupMenu(BTD);
          L1AA1.setName("L1AA1");
          panel1.add(L1AA1);
          L1AA1.setBounds(105, 205, 60, L1AA1.getPreferredSize().height);

          //---- L1NAT ----
          L1NAT.setComponentPopupMenu(BTD);
          L1NAT.setName("L1NAT");
          panel1.add(L1NAT);
          L1NAT.setBounds(105, 141, 57, L1NAT.getPreferredSize().height);

          //---- L1SAN ----
          L1SAN.setComponentPopupMenu(BTD);
          L1SAN.setName("L1SAN");
          panel1.add(L1SAN);
          L1SAN.setBounds(105, 67, 52, L1SAN.getPreferredSize().height);

          //---- OBJ_22_OBJ_22 ----
          OBJ_22_OBJ_22.setText("@LIBNAT@");
          OBJ_22_OBJ_22.setName("OBJ_22_OBJ_22");
          panel1.add(OBJ_22_OBJ_22);
          OBJ_22_OBJ_22.setBounds(175, 141, 269, 28);

          //---- OBJ_20_OBJ_20 ----
          OBJ_20_OBJ_20.setText("@LIBAF@");
          OBJ_20_OBJ_20.setName("OBJ_20_OBJ_20");
          panel1.add(OBJ_20_OBJ_20);
          OBJ_20_OBJ_20.setBounds(175, 104, 269, 28);

          //---- OBJ_18_OBJ_18 ----
          OBJ_18_OBJ_18.setText("@LIBSA@");
          OBJ_18_OBJ_18.setName("OBJ_18_OBJ_18");
          panel1.add(OBJ_18_OBJ_18);
          OBJ_18_OBJ_18.setBounds(175, 67, 269, 28);

          //---- L1AA2 ----
          L1AA2.setComponentPopupMenu(BTD);
          L1AA2.setName("L1AA2");
          panel1.add(L1AA2);
          L1AA2.setBounds(105, 239, 60, L1AA2.getPreferredSize().height);

          //---- L1AA3 ----
          L1AA3.setComponentPopupMenu(BTD);
          L1AA3.setName("L1AA3");
          panel1.add(L1AA3);
          L1AA3.setBounds(105, 273, 60, L1AA3.getPreferredSize().height);

          //---- L1AA4 ----
          L1AA4.setComponentPopupMenu(BTD);
          L1AA4.setName("L1AA4");
          panel1.add(L1AA4);
          L1AA4.setBounds(105, 307, 60, L1AA4.getPreferredSize().height);

          //---- L1AA5 ----
          L1AA5.setComponentPopupMenu(BTD);
          L1AA5.setName("L1AA5");
          panel1.add(L1AA5);
          L1AA5.setBounds(105, 341, 60, L1AA5.getPreferredSize().height);

          //---- L1AA6 ----
          L1AA6.setComponentPopupMenu(BTD);
          L1AA6.setName("L1AA6");
          panel1.add(L1AA6);
          L1AA6.setBounds(105, 375, 60, L1AA6.getPreferredSize().height);

          //---- separator1 ----
          separator1.setName("separator1");
          panel1.add(separator1);
          separator1.setBounds(25, 182, 500, separator1.getPreferredSize().height);

          //---- L1LA1 ----
          L1LA1.setText("@L1LA1@");
          L1LA1.setName("L1LA1");
          panel1.add(L1LA1);
          L1LA1.setBounds(175, 205, 310, 28);

          //---- L1LA2 ----
          L1LA2.setText("@L1LA2@");
          L1LA2.setName("L1LA2");
          panel1.add(L1LA2);
          L1LA2.setBounds(175, 239, 310, 28);

          //---- L1LA3 ----
          L1LA3.setText("@L1LA3@");
          L1LA3.setName("L1LA3");
          panel1.add(L1LA3);
          L1LA3.setBounds(175, 273, 310, 28);

          //---- L1LA4 ----
          L1LA4.setText("@L1LA4@");
          L1LA4.setName("L1LA4");
          panel1.add(L1LA4);
          L1LA4.setBounds(175, 307, 310, 28);

          //---- L1LA5 ----
          L1LA5.setText("@L1LA5@");
          L1LA5.setName("L1LA5");
          panel1.add(L1LA5);
          L1LA5.setBounds(175, 341, 310, 28);

          //---- L1LA6 ----
          L1LA6.setText("@L1LA6@");
          L1LA6.setName("L1LA6");
          panel1.add(L1LA6);
          L1LA6.setBounds(175, 375, 310, 28);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 545, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 428, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_6 ----
      OBJ_6.setText("Choix possibles");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      BTD.add(OBJ_6);

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTD.add(OBJ_5);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie OBJ_11_OBJ_11;
  private JLabel OBJ_12_OBJ_12;
  private JLabel OBJ_10_OBJ_10;
  private JLabel OBJ_21_OBJ_21;
  private JLabel OBJ_19_OBJ_19;
  private XRiTextField L1ACT;
  private XRiTextField L1AA1;
  private XRiTextField L1NAT;
  private XRiTextField L1SAN;
  private RiZoneSortie OBJ_22_OBJ_22;
  private RiZoneSortie OBJ_20_OBJ_20;
  private RiZoneSortie OBJ_18_OBJ_18;
  private XRiTextField L1AA2;
  private XRiTextField L1AA3;
  private XRiTextField L1AA4;
  private XRiTextField L1AA5;
  private XRiTextField L1AA6;
  private JComponent separator1;
  private RiZoneSortie L1LA1;
  private RiZoneSortie L1LA2;
  private RiZoneSortie L1LA3;
  private RiZoneSortie L1LA4;
  private RiZoneSortie L1LA5;
  private RiZoneSortie L1LA6;
  private JPopupMenu BTD;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_5;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
