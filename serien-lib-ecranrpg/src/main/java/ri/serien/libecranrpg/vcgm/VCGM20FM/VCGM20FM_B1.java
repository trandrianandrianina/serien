
package ri.serien.libecranrpg.vcgm.VCGM20FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM20FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM20FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    EREFNP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EREFNP@")).trim());
    DTNED.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DTNED@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    DTCTV.setVisible(lexique.isPresent("DTCTV"));
    OBJ_121.setVisible(lexique.isPresent("DTNC2X"));
    OBJ_121.setSelected(lexique.HostFieldGetData("DTNC2X").equalsIgnoreCase("OUI"));
    OBJ_112.setVisible(lexique.isPresent("DTNC1X"));
    OBJ_112.setSelected(lexique.HostFieldGetData("DTNC1X").equalsIgnoreCase("OUI"));
    DTBDO.setEnabled(lexique.isPresent("DTBDO"));
    DTEBP.setVisible(lexique.isPresent("DTEBP"));
    DTLIC3.setEnabled(lexique.isPresent("DTLIC3"));
    DTMRG.setEnabled(lexique.isPresent("DTMRG"));
    DTRGP.setVisible(lexique.isPresent("DTRGP"));
    DTNED.setEnabled(lexique.isPresent("DTNED"));
    DTDEV.setVisible(lexique.isPresent("DTDEV"));
    DTESC.setVisible(lexique.isPresent("DTESC"));
    DTCAA.setVisible(lexique.isPresent("DTCAA"));
    DTCGA.setVisible(lexique.isPresent("DTCGA"));
    DTNCA.setVisible(lexique.isPresent("DTNCA"));
    DTNCGX.setVisible(lexique.isPresent("DTNCGX"));
    DTDRGX.setVisible(lexique.isPresent("DTDRGX"));
    DTDMEX.setEnabled(lexique.isPresent("DTDMEX"));
    DTDDOX.setEnabled(lexique.isPresent("DTDDOX"));
    // DTDSGX.setVisible( lexique.isPresent("DTDSGX"));
    // DTDECX.setEnabled( lexique.isPresent("DTDECX"));
    // DTDCRX.setEnabled( lexique.isPresent("DTDCRX"));
    WTTVA.setVisible(lexique.isPresent("WTTVA"));
    DTNPR.setVisible(lexique.isPresent("DTNPR"));
    DTNPCG.setEnabled(lexique.isPresent("DTNPCG"));
    DTLIC4.setEnabled(lexique.isPresent("DTLIC4"));
    DTLIC2.setEnabled(lexique.isPresent("DTLIC2"));
    MTTRGP.setVisible(lexique.isPresent("MTTRGP"));
    WDTMTT.setVisible(lexique.isPresent("WDTMTT"));
    DTLIC1.setEnabled(lexique.isPresent("DTLIC1"));
    DTMTD.setVisible(lexique.isPresent("DTMTD"));
    DTMTT.setEnabled(lexique.isPresent("DTMTT"));
    OBJ_125.setVisible(lexique.isPresent("MONT4"));
    DTCLA.setVisible(lexique.isPresent("DTCLA"));
    DTCPL.setEnabled(lexique.isPresent("DTCPL"));
    WNOM.setVisible(lexique.isPresent("WNOM"));
    DTVIL.setEnabled(lexique.isPresent("DTVIL"));
    DTLOC.setEnabled(lexique.isPresent("DTLOC"));
    DTRUE.setEnabled(lexique.isPresent("DTRUE"));
    DTLIO.setEnabled(lexique.isPresent("DTLIO"));
    DTNOM.setEnabled(lexique.isPresent("DTNOM"));
    OBJ_127.setVisible(DTRGP.isVisible());
    OBJ_115.setVisible(DTCTV.isVisible());
    OBJ_105.setVisible(WDTMTT.isVisible());
    label6.setVisible(DTDEV.isVisible());
    OBJ_104.setVisible(DTESC.isVisible());
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - Gestion des dettes"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDSOC.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(INDSOC.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    if (OBJ_121.isSelected()) {
      lexique.HostFieldPutData("DTNC2X", 0, "OUI");
    }
    else {
      lexique.HostFieldPutData("DTNC2X", 0, "NON");
    }
    if (OBJ_112.isSelected()) {
      lexique.HostFieldPutData("DTNC1X", 0, "OUI");
    }
    else {
      lexique.HostFieldPutData("DTNC1X", 0, "NON");
    }
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F17");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11", false);
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23", false);
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_27ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_50 = new JLabel();
    INDSOC = new XRiTextField();
    OBJ_44 = new JLabel();
    INDNUM = new XRiTextField();
    OBJ_46 = new JLabel();
    INDSUF = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_97 = new JLabel();
    OBJ_99 = new JLabel();
    DTLIO = new XRiTextField();
    DTLIC1 = new XRiTextField();
    DTLIC2 = new XRiTextField();
    DTLIC3 = new XRiTextField();
    DTLIC4 = new XRiTextField();
    OBJ_98 = new JLabel();
    DTNPCG = new XRiTextField();
    EREFNP = new RiZoneSortie();
    xTitledPanel2 = new JXTitledPanel();
    DTDSGX = new XRiCalendrier();
    DTEBP = new XRiTextField();
    OBJ_125 = new JLabel();
    MTTRGP = new XRiTextField();
    OBJ_127 = new JLabel();
    DTRGP = new XRiTextField();
    label4 = new JLabel();
    label5 = new JLabel();
    xTitledPanel3 = new JXTitledPanel();
    WTTVA = new XRiTextField();
    WDTMTT = new XRiTextField();
    DTCTV = new XRiTextField();
    OBJ_115 = new JLabel();
    DTNED = new RiZoneSortie();
    OBJ_105 = new JLabel();
    DTESC = new XRiTextField();
    OBJ_104 = new JLabel();
    OBJ_111 = new JLabel();
    OBJ_112 = new JCheckBox();
    OBJ_113 = new JLabel();
    OBJ_121 = new JCheckBox();
    OBJ_120 = new JLabel();
    DTNPR = new XRiTextField();
    OBJ_109 = new JLabel();
    DTBDO = new XRiTextField();
    OBJ_102 = new JLabel();
    DTDDOX = new XRiTextField();
    OBJ_100 = new JLabel();
    OBJ_107 = new JLabel();
    OBJ_118 = new JLabel();
    DTDMEX = new XRiTextField();
    DTDRGX = new XRiTextField();
    xTitledPanel4 = new JXTitledPanel();
    DTCPL = new XRiTextField();
    DTRUE = new XRiTextField();
    DTLOC = new XRiTextField();
    DTVIL = new XRiTextField();
    DTCLA = new XRiTextField();
    DTNCA = new XRiTextField();
    DTNCGX = new XRiTextField();
    OBJ_135 = new JLabel();
    OBJ_133 = new JLabel();
    DTNOM = new XRiTextField();
    xTitledPanel5 = new JXTitledPanel();
    WNOM = new XRiTextField();
    OBJ_138 = new JLabel();
    DTCGA = new XRiTextField();
    DTCAA = new XRiTextField();
    panel2 = new JPanel();
    DTMTT = new XRiTextField();
    DTMTD = new XRiTextField();
    OBJ_90 = new JLabel();
    DTDCRX = new XRiCalendrier();
    DTDECX = new XRiCalendrier();
    DTDEV = new XRiTextField();
    DTMRG = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label6 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_27 = new JMenuItem();
    OBJ_26 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion des dettes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_50 ----
          OBJ_50.setText("Soci\u00e9t\u00e9");
          OBJ_50.setName("OBJ_50");

          //---- INDSOC ----
          INDSOC.setComponentPopupMenu(BTD);
          INDSOC.setName("INDSOC");

          //---- OBJ_44 ----
          OBJ_44.setText("N\u00b0 dette");
          OBJ_44.setName("OBJ_44");

          //---- INDNUM ----
          INDNUM.setComponentPopupMenu(BTD);
          INDNUM.setName("INDNUM");

          //---- OBJ_46 ----
          OBJ_46.setText("Suffixe");
          OBJ_46.setName("OBJ_46");

          //---- INDSUF ----
          INDSUF.setComponentPopupMenu(BTD);
          INDSUF.setName("INDSUF");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(INDSOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(INDSUF, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDSOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDSUF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_50)
                  .addComponent(OBJ_44)
                  .addComponent(OBJ_46)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Lettres de change");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Ch\u00e8ques \u00e0 payer");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Virements");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Dettes non domicili\u00e9es");
              riSousMenu_bt9.setToolTipText("Affichage des dettes non domicili\u00e9es par r\u00e8glement");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("R\u00e8glement partiel/dette");
              riSousMenu_bt10.setToolTipText("Saisie de r\u00e8glement partiel sur une dette");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Bloc-notes \u00e9critures");
              riSousMenu_bt14.setToolTipText("Bloc-notes");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);

            //======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");

              //---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Bloc-notes dettes");
              riSousMenu_bt15.setToolTipText("Bloc-notes des dettes");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed(e);
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(950, 560));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setTitle("Libell\u00e9");
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

            //---- OBJ_97 ----
            OBJ_97.setText("Origine comptable");
            OBJ_97.setName("OBJ_97");

            //---- OBJ_99 ----
            OBJ_99.setText("Nature op\u00e9ration");
            OBJ_99.setName("OBJ_99");

            //---- DTLIO ----
            DTLIO.setComponentPopupMenu(BTD);
            DTLIO.setName("DTLIO");

            //---- DTLIC1 ----
            DTLIC1.setComponentPopupMenu(BTD);
            DTLIC1.setName("DTLIC1");

            //---- DTLIC2 ----
            DTLIC2.setComponentPopupMenu(BTD);
            DTLIC2.setName("DTLIC2");

            //---- DTLIC3 ----
            DTLIC3.setComponentPopupMenu(BTD);
            DTLIC3.setName("DTLIC3");

            //---- DTLIC4 ----
            DTLIC4.setComponentPopupMenu(BTD);
            DTLIC4.setName("DTLIC4");

            //---- OBJ_98 ----
            OBJ_98.setText("Pi\u00e8ce");
            OBJ_98.setName("OBJ_98");

            //---- DTNPCG ----
            DTNPCG.setComponentPopupMenu(BTD);
            DTNPCG.setName("DTNPCG");

            //---- EREFNP ----
            EREFNP.setText("@EREFNP@");
            EREFNP.setMaximumSize(new Dimension(210, 20));
            EREFNP.setMinimumSize(new Dimension(210, 20));
            EREFNP.setPreferredSize(new Dimension(210, 24));
            EREFNP.setName("EREFNP");

            GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
            xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
            xTitledPanel1ContentContainerLayout.setHorizontalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(110, 110, 110)
                          .addComponent(DTLIC1, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                        .addComponent(OBJ_97, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
                      .addGap(10, 10, 10)
                      .addComponent(DTLIC2, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(DTLIC3, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(DTLIC4, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_99, GroupLayout.PREFERRED_SIZE, 103, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(DTLIO, GroupLayout.PREFERRED_SIZE, 283, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_98, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                      .addGap(71, 71, 71)
                      .addComponent(DTNPCG, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                      .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                      .addComponent(EREFNP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
            );
            xTitledPanel1ContentContainerLayout.setVerticalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(DTLIC1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_97)
                    .addComponent(DTLIC2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DTLIC3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DTLIC4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_99)
                    .addComponent(DTLIO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_98))
                    .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                      .addComponent(DTNPCG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(EREFNP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
            );
          }

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setTitle("Visa bon \u00e0 payer");
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();

            //---- DTDSGX ----
            DTDSGX.setComponentPopupMenu(BTD);
            DTDSGX.setName("DTDSGX");

            //---- DTEBP ----
            DTEBP.setComponentPopupMenu(BTD);
            DTEBP.setName("DTEBP");

            //---- OBJ_125 ----
            OBJ_125.setText("Montant initial");
            OBJ_125.setName("OBJ_125");

            //---- MTTRGP ----
            MTTRGP.setName("MTTRGP");

            //---- OBJ_127 ----
            OBJ_127.setText("suffixe");
            OBJ_127.setName("OBJ_127");

            //---- DTRGP ----
            DTRGP.setName("DTRGP");

            //---- label4 ----
            label4.setText("Emetteur");
            label4.setName("label4");

            //---- label5 ----
            label5.setText("Date");
            label5.setName("label5");

            GroupLayout xTitledPanel2ContentContainerLayout = new GroupLayout(xTitledPanel2ContentContainer);
            xTitledPanel2ContentContainer.setLayout(xTitledPanel2ContentContainerLayout);
            xTitledPanel2ContentContainerLayout.setHorizontalGroup(
              xTitledPanel2ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addComponent(label5, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                      .addGap(40, 40, 40)
                      .addComponent(DTDSGX, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
                      .addGap(30, 30, 30)
                      .addComponent(label4, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addGap(15, 15, 15)
                      .addComponent(DTEBP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_125, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(MTTRGP, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
                      .addGap(29, 29, 29)
                      .addComponent(OBJ_127, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addGap(25, 25, 25)
                      .addComponent(DTRGP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))))
            );
            xTitledPanel2ContentContainerLayout.setVerticalGroup(
              xTitledPanel2ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(DTDSGX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DTEBP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(label5)
                        .addComponent(label4))))
                  .addGap(2, 2, 2)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(MTTRGP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DTRGP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_125)
                        .addComponent(OBJ_127)))))
            );
          }

          //======== xTitledPanel3 ========
          {
            xTitledPanel3.setBorder(new DropShadowBorder());
            xTitledPanel3.setName("xTitledPanel3");
            Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();

            //---- WTTVA ----
            WTTVA.setName("WTTVA");

            //---- WDTMTT ----
            WDTMTT.setName("WDTMTT");

            //---- DTCTV ----
            DTCTV.setComponentPopupMenu(BTD);
            DTCTV.setName("DTCTV");

            //---- OBJ_115 ----
            OBJ_115.setText("Code TVA");
            OBJ_115.setName("OBJ_115");

            //---- DTNED ----
            DTNED.setText("@DTNED@");
            DTNED.setName("DTNED");

            //---- OBJ_105 ----
            OBJ_105.setText("Montant net");
            OBJ_105.setName("OBJ_105");

            //---- DTESC ----
            DTESC.setComponentPopupMenu(BTD);
            DTESC.setName("DTESC");

            //---- OBJ_104 ----
            OBJ_104.setText("%escompte");
            OBJ_104.setName("OBJ_104");

            //---- OBJ_111 ----
            OBJ_111.setText("Comptabilis\u00e9");
            OBJ_111.setName("OBJ_111");

            //---- OBJ_112 ----
            OBJ_112.setText("");
            OBJ_112.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_112.setEnabled(false);
            OBJ_112.setName("OBJ_112");

            //---- OBJ_113 ----
            OBJ_113.setText("Pas");
            OBJ_113.setName("OBJ_113");

            //---- OBJ_121 ----
            OBJ_121.setText("");
            OBJ_121.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_121.setEnabled(false);
            OBJ_121.setName("OBJ_121");

            //---- OBJ_120 ----
            OBJ_120.setText("Comptabilis\u00e9");
            OBJ_120.setName("OBJ_120");

            //---- DTNPR ----
            DTNPR.setName("DTNPR");

            //---- OBJ_109 ----
            OBJ_109.setText("Pi\u00e8ce");
            OBJ_109.setName("OBJ_109");

            //---- DTBDO ----
            DTBDO.setName("DTBDO");

            //---- OBJ_102 ----
            OBJ_102.setText("Banque");
            OBJ_102.setName("OBJ_102");

            //---- DTDDOX ----
            DTDDOX.setName("DTDDOX");

            //---- OBJ_100 ----
            OBJ_100.setText("Domiciliation");
            OBJ_100.setName("OBJ_100");

            //---- OBJ_107 ----
            OBJ_107.setText("Emission titre");
            OBJ_107.setName("OBJ_107");

            //---- OBJ_118 ----
            OBJ_118.setText("R\u00e8glement");
            OBJ_118.setName("OBJ_118");

            //---- DTDMEX ----
            DTDMEX.setName("DTDMEX");

            //---- DTDRGX ----
            DTDRGX.setName("DTDRGX");

            GroupLayout xTitledPanel3ContentContainerLayout = new GroupLayout(xTitledPanel3ContentContainer);
            xTitledPanel3ContentContainer.setLayout(xTitledPanel3ContentContainerLayout);
            xTitledPanel3ContentContainerLayout.setHorizontalGroup(
              xTitledPanel3ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_100, GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
                      .addGap(29, 29, 29)
                      .addComponent(DTDDOX, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                      .addGap(14, 14, 14)
                      .addComponent(OBJ_102, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(DTBDO, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(130, 130, 130)
                      .addComponent(OBJ_104, GroupLayout.PREFERRED_SIZE, 72, GroupLayout.PREFERRED_SIZE)
                      .addGap(8, 8, 8)
                      .addComponent(DTESC, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
                      .addGap(63, 63, 63)
                      .addComponent(OBJ_105, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(WDTMTT, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_107, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE)
                      .addGap(28, 28, 28)
                      .addComponent(DTDMEX, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                      .addGap(14, 14, 14)
                      .addComponent(OBJ_109, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                      .addGap(16, 16, 16)
                      .addComponent(DTNPR, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
                      .addGap(92, 92, 92)
                      .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_111, GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                          .addGap(80, 80, 80)
                          .addComponent(OBJ_112, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)))
                      .addGap(11, 11, 11)
                      .addComponent(OBJ_113, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(DTNED, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                      .addGap(19, 19, 19)
                      .addComponent(OBJ_115, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(DTCTV, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(WTTVA, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_118, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
                      .addGap(41, 41, 41)
                      .addComponent(DTDRGX, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                      .addGap(229, 229, 229)
                      .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                          .addGap(80, 80, 80)
                          .addComponent(OBJ_121, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE))
                        .addComponent(OBJ_120, GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)))))
            );
            xTitledPanel3ContentContainerLayout.setVerticalGroup(
              xTitledPanel3ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                  .addGap(9, 9, 9)
                  .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(7, 7, 7)
                      .addComponent(OBJ_100))
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(DTDDOX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(7, 7, 7)
                      .addComponent(OBJ_102))
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(DTBDO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(7, 7, 7)
                      .addComponent(OBJ_104))
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(DTESC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_105))
                    .addComponent(WDTMTT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_107))
                    .addComponent(DTDMEX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_109))
                    .addComponent(DTNPR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                          .addGap(1, 1, 1)
                          .addComponent(OBJ_111))
                        .addComponent(OBJ_112)))
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_113))
                    .addComponent(DTNED, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_115))
                    .addComponent(DTCTV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(WTTVA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_118))
                    .addComponent(DTDRGX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_121)
                        .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                          .addGap(1, 1, 1)
                          .addComponent(OBJ_120))))))
            );
          }

          //======== xTitledPanel4 ========
          {
            xTitledPanel4.setBorder(new DropShadowBorder());
            xTitledPanel4.setTitle("Fournisseur");
            xTitledPanel4.setName("xTitledPanel4");
            Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();

            //---- DTCPL ----
            DTCPL.setName("DTCPL");

            //---- DTRUE ----
            DTRUE.setName("DTRUE");

            //---- DTLOC ----
            DTLOC.setName("DTLOC");

            //---- DTVIL ----
            DTVIL.setName("DTVIL");

            //---- DTCLA ----
            DTCLA.setComponentPopupMenu(BTD);
            DTCLA.setName("DTCLA");

            //---- DTNCA ----
            DTNCA.setToolTipText("Auxilliaire");
            DTNCA.setComponentPopupMenu(BTD);
            DTNCA.setName("DTNCA");

            //---- DTNCGX ----
            DTNCGX.setToolTipText("Collectif");
            DTNCGX.setComponentPopupMenu(BTD);
            DTNCGX.setName("DTNCGX");

            //---- OBJ_135 ----
            OBJ_135.setText("Classement");
            OBJ_135.setName("OBJ_135");

            //---- OBJ_133 ----
            OBJ_133.setText("Compte");
            OBJ_133.setName("OBJ_133");

            //---- DTNOM ----
            DTNOM.setForeground(Color.black);
            DTNOM.setName("DTNOM");

            GroupLayout xTitledPanel4ContentContainerLayout = new GroupLayout(xTitledPanel4ContentContainer);
            xTitledPanel4ContentContainer.setLayout(xTitledPanel4ContentContainerLayout);
            xTitledPanel4ContentContainerLayout.setHorizontalGroup(
              xTitledPanel4ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(DTNOM, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addComponent(DTCPL, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_133, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
                  .addGap(34, 34, 34)
                  .addComponent(DTNCGX, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addGap(12, 12, 12)
                  .addComponent(DTNCA, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addGap(98, 98, 98)
                  .addComponent(DTRUE, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_135, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addGap(10, 10, 10)
                  .addComponent(DTCLA, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)
                  .addGap(98, 98, 98)
                  .addComponent(DTLOC, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                  .addGap(325, 325, 325)
                  .addComponent(DTVIL, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
            );
            xTitledPanel4ContentContainerLayout.setVerticalGroup(
              xTitledPanel4ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(xTitledPanel4ContentContainerLayout.createParallelGroup()
                    .addComponent(DTNOM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DTCPL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addGroup(xTitledPanel4ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_133))
                    .addComponent(DTNCGX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DTNCA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DTRUE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addGroup(xTitledPanel4ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_135))
                    .addComponent(DTCLA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DTLOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addComponent(DTVIL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            );
          }

          //======== xTitledPanel5 ========
          {
            xTitledPanel5.setBorder(new DropShadowBorder());
            xTitledPanel5.setName("xTitledPanel5");
            Container xTitledPanel5ContentContainer = xTitledPanel5.getContentContainer();

            //---- WNOM ----
            WNOM.setName("WNOM");

            //---- OBJ_138 ----
            OBJ_138.setText("Affacturage");
            OBJ_138.setName("OBJ_138");

            //---- DTCGA ----
            DTCGA.setComponentPopupMenu(BTD);
            DTCGA.setName("DTCGA");

            //---- DTCAA ----
            DTCAA.setComponentPopupMenu(BTD);
            DTCAA.setName("DTCAA");

            GroupLayout xTitledPanel5ContentContainerLayout = new GroupLayout(xTitledPanel5ContentContainer);
            xTitledPanel5ContentContainer.setLayout(xTitledPanel5ContentContainerLayout);
            xTitledPanel5ContentContainerLayout.setHorizontalGroup(
              xTitledPanel5ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel5ContentContainerLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_138, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                  .addGap(8, 8, 8)
                  .addComponent(DTCGA, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, 0)
                  .addComponent(DTCAA, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel5ContentContainerLayout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addComponent(WNOM, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE))
            );
            xTitledPanel5ContentContainerLayout.setVerticalGroup(
              xTitledPanel5ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel5ContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(xTitledPanel5ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel5ContentContainerLayout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_138))
                    .addComponent(DTCGA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DTCAA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addComponent(WNOM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            );
          }

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");

            //---- DTMTT ----
            DTMTT.setComponentPopupMenu(BTD);
            DTMTT.setForeground(Color.black);
            DTMTT.setFont(DTMTT.getFont().deriveFont(DTMTT.getFont().getStyle() | Font.BOLD));
            DTMTT.setName("DTMTT");

            //---- DTMTD ----
            DTMTD.setComponentPopupMenu(BTD);
            DTMTD.setName("DTMTD");

            //---- OBJ_90 ----
            OBJ_90.setText("Montant");
            OBJ_90.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_90.setName("OBJ_90");

            //---- DTDCRX ----
            DTDCRX.setComponentPopupMenu(BTD);
            DTDCRX.setName("DTDCRX");

            //---- DTDECX ----
            DTDECX.setComponentPopupMenu(BTD);
            DTDECX.setName("DTDECX");

            //---- DTDEV ----
            DTDEV.setComponentPopupMenu(BTD);
            DTDEV.setName("DTDEV");

            //---- DTMRG ----
            DTMRG.setComponentPopupMenu(BTD);
            DTMRG.setName("DTMRG");

            //---- label1 ----
            label1.setText("Mode de r\u00e8glement");
            label1.setName("label1");

            //---- label2 ----
            label2.setText("Cr\u00e9ation");
            label2.setName("label2");

            //---- label3 ----
            label3.setText("Ech\u00e9ance");
            label3.setName("label3");

            //---- label6 ----
            label6.setText("Devise");
            label6.setName("label6");

            GroupLayout panel2Layout = new GroupLayout(panel2);
            panel2.setLayout(panel2Layout);
            panel2Layout.setHorizontalGroup(
              panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(OBJ_90, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                  .addGap(15, 15, 15)
                  .addComponent(DTMTT, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
                  .addGap(24, 24, 24)
                  .addComponent(label2, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, 0)
                  .addComponent(DTDCRX, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
                  .addGap(15, 15, 15)
                  .addComponent(label3, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, 0)
                  .addComponent(DTDECX, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
                  .addGap(25, 25, 25)
                  .addComponent(label1, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, 0)
                  .addComponent(DTMRG, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(90, 90, 90)
                  .addComponent(DTMTD, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
                  .addGap(24, 24, 24)
                  .addComponent(label6)
                  .addGap(17, 17, 17)
                  .addComponent(DTDEV, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
            );
            panel2Layout.setVerticalGroup(
              panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_90))
                    .addComponent(DTMTT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(label2))
                    .addComponent(DTDCRX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(label3))
                    .addComponent(DTDECX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(label1))
                    .addComponent(DTMRG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addComponent(DTMTD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(label6))
                    .addComponent(DTDEV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(xTitledPanel3, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(xTitledPanel5, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                  .addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(30, 30, 30)
                    .addComponent(xTitledPanel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(xTitledPanel5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_27 ----
      OBJ_27.setText("Choix possibles");
      OBJ_27.setName("OBJ_27");
      OBJ_27.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_27ActionPerformed(e);
        }
      });
      BTD.add(OBJ_27);

      //---- OBJ_26 ----
      OBJ_26.setText("Aide en ligne");
      OBJ_26.setName("OBJ_26");
      OBJ_26.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_26ActionPerformed(e);
        }
      });
      BTD.add(OBJ_26);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_50;
  private XRiTextField INDSOC;
  private JLabel OBJ_44;
  private XRiTextField INDNUM;
  private JLabel OBJ_46;
  private XRiTextField INDSUF;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_97;
  private JLabel OBJ_99;
  private XRiTextField DTLIO;
  private XRiTextField DTLIC1;
  private XRiTextField DTLIC2;
  private XRiTextField DTLIC3;
  private XRiTextField DTLIC4;
  private JLabel OBJ_98;
  private XRiTextField DTNPCG;
  private RiZoneSortie EREFNP;
  private JXTitledPanel xTitledPanel2;
  private XRiCalendrier DTDSGX;
  private XRiTextField DTEBP;
  private JLabel OBJ_125;
  private XRiTextField MTTRGP;
  private JLabel OBJ_127;
  private XRiTextField DTRGP;
  private JLabel label4;
  private JLabel label5;
  private JXTitledPanel xTitledPanel3;
  private XRiTextField WTTVA;
  private XRiTextField WDTMTT;
  private XRiTextField DTCTV;
  private JLabel OBJ_115;
  private RiZoneSortie DTNED;
  private JLabel OBJ_105;
  private XRiTextField DTESC;
  private JLabel OBJ_104;
  private JLabel OBJ_111;
  private JCheckBox OBJ_112;
  private JLabel OBJ_113;
  private JCheckBox OBJ_121;
  private JLabel OBJ_120;
  private XRiTextField DTNPR;
  private JLabel OBJ_109;
  private XRiTextField DTBDO;
  private JLabel OBJ_102;
  private XRiTextField DTDDOX;
  private JLabel OBJ_100;
  private JLabel OBJ_107;
  private JLabel OBJ_118;
  private XRiTextField DTDMEX;
  private XRiTextField DTDRGX;
  private JXTitledPanel xTitledPanel4;
  private XRiTextField DTCPL;
  private XRiTextField DTRUE;
  private XRiTextField DTLOC;
  private XRiTextField DTVIL;
  private XRiTextField DTCLA;
  private XRiTextField DTNCA;
  private XRiTextField DTNCGX;
  private JLabel OBJ_135;
  private JLabel OBJ_133;
  private XRiTextField DTNOM;
  private JXTitledPanel xTitledPanel5;
  private XRiTextField WNOM;
  private JLabel OBJ_138;
  private XRiTextField DTCGA;
  private XRiTextField DTCAA;
  private JPanel panel2;
  private XRiTextField DTMTT;
  private XRiTextField DTMTD;
  private JLabel OBJ_90;
  private XRiCalendrier DTDCRX;
  private XRiCalendrier DTDECX;
  private XRiTextField DTDEV;
  private XRiTextField DTMRG;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label6;
  private JPopupMenu BTD;
  private JMenuItem OBJ_27;
  private JMenuItem OBJ_26;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
