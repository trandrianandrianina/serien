
package ri.serien.libecranrpg.vcgm.VCGMCAFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerListModel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiSpinner;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGMCAFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGMCAFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    p_TCI1.setRightDecoration(TCI1);
    p_TCI2.setRightDecoration(TCI2);
    p_TCI3.setRightDecoration(TCI3);
    p_TCI4.setRightDecoration(TCI4);
    
    // Ajout
    initDiverses();
    CAMTTN.setValeursSelection("OUI", "NON");
    CAQTE.setValeursSelection("X", " ");
    CAREL.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    CAECH5.setEnabled(lexique.isPresent("CAECH5"));
    CAECH4.setEnabled(lexique.isPresent("CAECH4"));
    CAECH3.setEnabled(lexique.isPresent("CAECH3"));
    CAECH2.setEnabled(lexique.isPresent("CAECH2"));
    CAECH1.setEnabled(lexique.isPresent("CAECH1"));
    CAMRG5.setEnabled(lexique.isPresent("CAMRG5"));
    CAMRG4.setEnabled(lexique.isPresent("CAMRG4"));
    CAMRG3.setEnabled(lexique.isPresent("CAMRG3"));
    CAMRG2.setEnabled(lexique.isPresent("CAMRG2"));
    CAMRG1.setEnabled(lexique.isPresent("CAMRG1"));
    CAREP5.setEnabled(lexique.isPresent("CAREP5"));
    CAREP4.setEnabled(lexique.isPresent("CAREP4"));
    CAREP3.setEnabled(lexique.isPresent("CAREP3"));
    CAREP2.setEnabled(lexique.isPresent("CAREP2"));
    CAREP1.setEnabled(lexique.isPresent("CAREP1"));
    CASENS.setEnabled(lexique.isPresent("CASENS"));
    CADEV2.setEnabled(lexique.isPresent("CADEV2"));
    CADEV1.setEnabled(lexique.isPresent("CADEV1"));
    CACDPF.setEnabled(lexique.isPresent("CACDPF"));
    CACDPD.setEnabled(lexique.isPresent("CACDPD"));
    CACOLF.setEnabled(lexique.isPresent("CACOLF"));
    CACOLD.setEnabled(lexique.isPresent("CACOLD"));
    CACS3.setEnabled(lexique.isPresent("CACS3"));
    CACS2.setEnabled(lexique.isPresent("CACS2"));
    CACS1.setEnabled(lexique.isPresent("CACS1"));
    CAAUXF.setEnabled(lexique.isPresent("CAAUXF"));
    CAAUXD.setEnabled(lexique.isPresent("CAAUXD"));
    // CAREL.setEnabled( lexique.isPresent("CAREL"));
    // CAREL.setSelected(lexique.HostFieldGetData("CAREL").equalsIgnoreCase("OUI"));
    // CAQTE.setEnabled( lexique.isPresent("CAQTE"));
    // CAQTE.setSelected(lexique.HostFieldGetData("CAQTE").equalsIgnoreCase("X"));
    TCI3.setEnabled(lexique.isPresent("TCI3"));
    TCI4.setEnabled(lexique.isPresent("TCI4"));
    TCI2.setEnabled(lexique.isPresent("TCI2"));
    TCI1.setEnabled(lexique.isPresent("TCI1"));
    // CAMTTN.setEnabled( lexique.isPresent("CAMTTN"));
    // CAMTTN.setSelected(lexique.HostFieldGetData("CAMTTN").equalsIgnoreCase("OUI"));
    CAOBS.setEnabled(lexique.isPresent("CAOBS"));
    CAOBJ.setEnabled(lexique.isPresent("CAOBJ"));
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - Critères de sélection"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (CAREL.isSelected())
    // lexique.HostFieldPutData("CAREL", 0, "OUI");
    // else
    // lexique.HostFieldPutData("CAREL", 0, "NON");
    // if (CAQTE.isSelected())
    // lexique.HostFieldPutData("CAQTE", 0, "X");
    // else
    // lexique.HostFieldPutData("CAQTE", 0, " ");
    // if (CAMTTN.isSelected())
    // lexique.HostFieldPutData("CAMTTN", 0, "OUI");
    // else
    // lexique.HostFieldPutData("CAMTTN", 0, "NON");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void TCI1ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI1").trim().equals("")) {
      lexique.HostFieldPutData("TCI1", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, "1");
    }
    
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void TCI2ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI2").trim().equals("")) {
      lexique.HostFieldPutData("TCI2", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, "2");
    }
    
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void TCI3ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI3").trim().equals("")) {
      lexique.HostFieldPutData("TCI3", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, "3");
    }
    
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void TCI4ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI4").trim().equals("")) {
      lexique.HostFieldPutData("TCI4", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, "4");
    }
    
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_49 = new JLabel();
    INDNUM = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    p_TCI1 = new JXTitledPanel();
    CAREL = new XRiCheckBox();
    OBJ_88 = new JLabel();
    OBJ_86 = new JLabel();
    CACDPD = new XRiTextField();
    CACDPF = new XRiTextField();
    CAREP1 = new XRiTextField();
    CAREP2 = new XRiTextField();
    CAREP3 = new XRiTextField();
    CAREP4 = new XRiTextField();
    CAREP5 = new XRiTextField();
    OBJ_87 = new JLabel();
    p_TCI2 = new JXTitledPanel();
    CAQTE = new XRiCheckBox();
    OBJ_91 = new JLabel();
    OBJ_89 = new JLabel();
    CAAUXD = new XRiTextField();
    CAAUXF = new XRiTextField();
    CACOLD = new XRiTextField();
    CACOLF = new XRiTextField();
    OBJ_92 = new JLabel();
    OBJ_90 = new JLabel();
    p_TCI3 = new JXTitledPanel();
    OBJ_94 = new JLabel();
    OBJ_95 = new JLabel();
    OBJ_93 = new JLabel();
    CADEV1 = new XRiTextField();
    CADEV2 = new XRiTextField();
    CAMRG1 = new XRiTextField();
    CAMRG2 = new XRiTextField();
    CAMRG3 = new XRiTextField();
    CAMRG4 = new XRiTextField();
    CAMRG5 = new XRiTextField();
    CAECH1 = new XRiTextField();
    CAECH2 = new XRiTextField();
    CAECH3 = new XRiTextField();
    CAECH4 = new XRiTextField();
    CAECH5 = new XRiTextField();
    p_TCI4 = new JXTitledPanel();
    CAMTTN = new XRiCheckBox();
    OBJ_96 = new JLabel();
    CASENS = new XRiSpinner();
    OBJ_97 = new JLabel();
    CACS1 = new XRiTextField();
    CACS2 = new XRiTextField();
    CACS3 = new XRiTextField();
    panel2 = new JPanel();
    OBJ_104 = new JLabel();
    CAOBJ = new XRiTextField();
    CAOBS = new XRiTextField();
    OBJ_105 = new JLabel();
    xTitledSeparator1 = new JXTitledSeparator();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    TCI1 = new SNBoutonDetail();
    TCI2 = new SNBoutonDetail();
    TCI3 = new SNBoutonDetail();
    TCI4 = new SNBoutonDetail();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Crit\u00e8res de s\u00e9lection");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_49 ----
          OBJ_49.setText("Num\u00e9ro");
          OBJ_49.setName("OBJ_49");
          p_tete_gauche.add(OBJ_49);
          OBJ_49.setBounds(5, 4, 52, 20);

          //---- INDNUM ----
          INDNUM.setComponentPopupMenu(BTD);
          INDNUM.setName("INDNUM");
          p_tete_gauche.add(INDNUM);
          INDNUM.setBounds(65, 0, 34, INDNUM.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(780, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== p_TCI1 ========
            {
              p_TCI1.setTitle("Identification");
              p_TCI1.setName("p_TCI1");
              Container p_TCI1ContentContainer = p_TCI1.getContentContainer();
              p_TCI1ContentContainer.setLayout(null);

              //---- CAREL ----
              CAREL.setText("A relancer");
              CAREL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CAREL.setName("CAREL");
              p_TCI1ContentContainer.add(CAREL);
              CAREL.setBounds(510, 19, 88, 20);

              //---- OBJ_88 ----
              OBJ_88.setText("Repr\u00e9sentant");
              OBJ_88.setName("OBJ_88");
              p_TCI1ContentContainer.add(OBJ_88);
              OBJ_88.setBounds(250, 19, 84, 20);

              //---- OBJ_86 ----
              OBJ_86.setText("Code postal");
              OBJ_86.setName("OBJ_86");
              p_TCI1ContentContainer.add(OBJ_86);
              OBJ_86.setBounds(20, 19, 78, 20);

              //---- CACDPD ----
              CACDPD.setComponentPopupMenu(BTD);
              CACDPD.setName("CACDPD");
              p_TCI1ContentContainer.add(CACDPD);
              CACDPD.setBounds(100, 15, 50, CACDPD.getPreferredSize().height);

              //---- CACDPF ----
              CACDPF.setComponentPopupMenu(BTD);
              CACDPF.setName("CACDPF");
              p_TCI1ContentContainer.add(CACDPF);
              CACDPF.setBounds(170, 15, 50, CACDPF.getPreferredSize().height);

              //---- CAREP1 ----
              CAREP1.setComponentPopupMenu(BTD);
              CAREP1.setName("CAREP1");
              p_TCI1ContentContainer.add(CAREP1);
              CAREP1.setBounds(335, 15, 30, CAREP1.getPreferredSize().height);

              //---- CAREP2 ----
              CAREP2.setComponentPopupMenu(BTD);
              CAREP2.setName("CAREP2");
              p_TCI1ContentContainer.add(CAREP2);
              CAREP2.setBounds(365, 15, 30, CAREP2.getPreferredSize().height);

              //---- CAREP3 ----
              CAREP3.setComponentPopupMenu(BTD);
              CAREP3.setName("CAREP3");
              p_TCI1ContentContainer.add(CAREP3);
              CAREP3.setBounds(395, 15, 30, CAREP3.getPreferredSize().height);

              //---- CAREP4 ----
              CAREP4.setComponentPopupMenu(BTD);
              CAREP4.setName("CAREP4");
              p_TCI1ContentContainer.add(CAREP4);
              CAREP4.setBounds(425, 15, 30, CAREP4.getPreferredSize().height);

              //---- CAREP5 ----
              CAREP5.setComponentPopupMenu(BTD);
              CAREP5.setName("CAREP5");
              p_TCI1ContentContainer.add(CAREP5);
              CAREP5.setBounds(455, 15, 30, CAREP5.getPreferredSize().height);

              //---- OBJ_87 ----
              OBJ_87.setText("\u00e0");
              OBJ_87.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_87.setName("OBJ_87");
              p_TCI1ContentContainer.add(OBJ_87);
              OBJ_87.setBounds(155, 19, 12, 20);
            }
            panel1.add(p_TCI1);
            p_TCI1.setBounds(25, 10, 705, 85);

            //======== p_TCI2 ========
            {
              p_TCI2.setTitle("Comptes");
              p_TCI2.setName("p_TCI2");
              Container p_TCI2ContentContainer = p_TCI2.getContentContainer();
              p_TCI2ContentContainer.setLayout(null);

              //---- CAQTE ----
              CAQTE.setText("G\u00e9r\u00e9 en quantit\u00e9");
              CAQTE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CAQTE.setName("CAQTE");
              p_TCI2ContentContainer.add(CAQTE);
              CAQTE.setBounds(510, 19, 175, 20);

              //---- OBJ_91 ----
              OBJ_91.setText("Auxiliaire");
              OBJ_91.setName("OBJ_91");
              p_TCI2ContentContainer.add(OBJ_91);
              OBJ_91.setBounds(250, 19, 57, 20);

              //---- OBJ_89 ----
              OBJ_89.setText("Collectif");
              OBJ_89.setName("OBJ_89");
              p_TCI2ContentContainer.add(OBJ_89);
              OBJ_89.setBounds(20, 19, 51, 20);

              //---- CAAUXD ----
              CAAUXD.setComponentPopupMenu(BTD);
              CAAUXD.setName("CAAUXD");
              p_TCI2ContentContainer.add(CAAUXD);
              CAAUXD.setBounds(335, 15, 58, CAAUXD.getPreferredSize().height);

              //---- CAAUXF ----
              CAAUXF.setComponentPopupMenu(BTD);
              CAAUXF.setName("CAAUXF");
              p_TCI2ContentContainer.add(CAAUXF);
              CAAUXF.setBounds(424, 15, 58, CAAUXF.getPreferredSize().height);

              //---- CACOLD ----
              CACOLD.setComponentPopupMenu(BTD);
              CACOLD.setName("CACOLD");
              p_TCI2ContentContainer.add(CACOLD);
              CACOLD.setBounds(90, 15, 58, CACOLD.getPreferredSize().height);

              //---- CACOLF ----
              CACOLF.setComponentPopupMenu(BTD);
              CACOLF.setName("CACOLF");
              p_TCI2ContentContainer.add(CACOLF);
              CACOLF.setBounds(170, 15, 58, CACOLF.getPreferredSize().height);

              //---- OBJ_92 ----
              OBJ_92.setText("\u00e0");
              OBJ_92.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_92.setName("OBJ_92");
              p_TCI2ContentContainer.add(OBJ_92);
              OBJ_92.setBounds(395, 19, 30, 20);

              //---- OBJ_90 ----
              OBJ_90.setText("\u00e0");
              OBJ_90.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_90.setName("OBJ_90");
              p_TCI2ContentContainer.add(OBJ_90);
              OBJ_90.setBounds(153, 19, 12, 20);
            }
            panel1.add(p_TCI2);
            p_TCI2.setBounds(25, 105, 705, 90);

            //======== p_TCI3 ========
            {
              p_TCI3.setTitle("R\u00e8glement");
              p_TCI3.setName("p_TCI3");
              Container p_TCI3ContentContainer = p_TCI3.getContentContainer();
              p_TCI3ContentContainer.setLayout(null);

              //---- OBJ_94 ----
              OBJ_94.setText("Ech\u00e9ance");
              OBJ_94.setName("OBJ_94");
              p_TCI3ContentContainer.add(OBJ_94);
              OBJ_94.setBounds(225, 19, 64, 20);

              //---- OBJ_95 ----
              OBJ_95.setText("Devise");
              OBJ_95.setName("OBJ_95");
              p_TCI3ContentContainer.add(OBJ_95);
              OBJ_95.setBounds(465, 19, 45, 20);

              //---- OBJ_93 ----
              OBJ_93.setText("Mode");
              OBJ_93.setName("OBJ_93");
              p_TCI3ContentContainer.add(OBJ_93);
              OBJ_93.setBounds(20, 19, 36, 20);

              //---- CADEV1 ----
              CADEV1.setComponentPopupMenu(BTD);
              CADEV1.setName("CADEV1");
              p_TCI3ContentContainer.add(CADEV1);
              CADEV1.setBounds(515, 15, 40, CADEV1.getPreferredSize().height);

              //---- CADEV2 ----
              CADEV2.setComponentPopupMenu(BTD);
              CADEV2.setName("CADEV2");
              p_TCI3ContentContainer.add(CADEV2);
              CADEV2.setBounds(555, 15, 40, CADEV2.getPreferredSize().height);

              //---- CAMRG1 ----
              CAMRG1.setComponentPopupMenu(BTD);
              CAMRG1.setName("CAMRG1");
              p_TCI3ContentContainer.add(CAMRG1);
              CAMRG1.setBounds(55, 15, 30, CAMRG1.getPreferredSize().height);

              //---- CAMRG2 ----
              CAMRG2.setComponentPopupMenu(BTD);
              CAMRG2.setName("CAMRG2");
              p_TCI3ContentContainer.add(CAMRG2);
              CAMRG2.setBounds(85, 15, 30, CAMRG2.getPreferredSize().height);

              //---- CAMRG3 ----
              CAMRG3.setComponentPopupMenu(BTD);
              CAMRG3.setName("CAMRG3");
              p_TCI3ContentContainer.add(CAMRG3);
              CAMRG3.setBounds(115, 15, 30, CAMRG3.getPreferredSize().height);

              //---- CAMRG4 ----
              CAMRG4.setComponentPopupMenu(BTD);
              CAMRG4.setName("CAMRG4");
              p_TCI3ContentContainer.add(CAMRG4);
              CAMRG4.setBounds(145, 15, 30, CAMRG4.getPreferredSize().height);

              //---- CAMRG5 ----
              CAMRG5.setComponentPopupMenu(BTD);
              CAMRG5.setName("CAMRG5");
              p_TCI3ContentContainer.add(CAMRG5);
              CAMRG5.setBounds(175, 15, 30, CAMRG5.getPreferredSize().height);

              //---- CAECH1 ----
              CAECH1.setComponentPopupMenu(BTD);
              CAECH1.setName("CAECH1");
              p_TCI3ContentContainer.add(CAECH1);
              CAECH1.setBounds(295, 15, 30, CAECH1.getPreferredSize().height);

              //---- CAECH2 ----
              CAECH2.setComponentPopupMenu(BTD);
              CAECH2.setName("CAECH2");
              p_TCI3ContentContainer.add(CAECH2);
              CAECH2.setBounds(325, 15, 30, CAECH2.getPreferredSize().height);

              //---- CAECH3 ----
              CAECH3.setComponentPopupMenu(BTD);
              CAECH3.setName("CAECH3");
              p_TCI3ContentContainer.add(CAECH3);
              CAECH3.setBounds(355, 15, 30, CAECH3.getPreferredSize().height);

              //---- CAECH4 ----
              CAECH4.setComponentPopupMenu(BTD);
              CAECH4.setName("CAECH4");
              p_TCI3ContentContainer.add(CAECH4);
              CAECH4.setBounds(385, 15, 30, CAECH4.getPreferredSize().height);

              //---- CAECH5 ----
              CAECH5.setComponentPopupMenu(BTD);
              CAECH5.setName("CAECH5");
              p_TCI3ContentContainer.add(CAECH5);
              CAECH5.setBounds(415, 15, 30, CAECH5.getPreferredSize().height);
            }
            panel1.add(p_TCI3);
            p_TCI3.setBounds(25, 205, 705, 95);

            //======== p_TCI4 ========
            {
              p_TCI4.setTitle("Divers");
              p_TCI4.setName("p_TCI4");
              Container p_TCI4ContentContainer = p_TCI4.getContentContainer();

              //---- CAMTTN ----
              CAMTTN.setText("Edition des montants nuls");
              CAMTTN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CAMTTN.setName("CAMTTN");

              //---- OBJ_96 ----
              OBJ_96.setText("Sens du solde");
              OBJ_96.setName("OBJ_96");

              //---- CASENS ----
              CASENS.setComponentPopupMenu(BTD);
              CASENS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CASENS.setModel(new SpinnerListModel(new String[] {" ", "+", "-"}) {
                { setValue("+"); }
              });
              CASENS.setName("CASENS");

              //---- OBJ_97 ----
              OBJ_97.setText("Crit\u00e8res");
              OBJ_97.setName("OBJ_97");

              //---- CACS1 ----
              CACS1.setComponentPopupMenu(BTD);
              CACS1.setName("CACS1");

              //---- CACS2 ----
              CACS2.setComponentPopupMenu(BTD);
              CACS2.setName("CACS2");

              //---- CACS3 ----
              CACS3.setComponentPopupMenu(BTD);
              CACS3.setName("CACS3");

              GroupLayout p_TCI4ContentContainerLayout = new GroupLayout(p_TCI4ContentContainer);
              p_TCI4ContentContainer.setLayout(p_TCI4ContentContainerLayout);
              p_TCI4ContentContainerLayout.setHorizontalGroup(
                p_TCI4ContentContainerLayout.createParallelGroup()
                  .addGroup(p_TCI4ContentContainerLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(CAMTTN, GroupLayout.PREFERRED_SIZE, 198, GroupLayout.PREFERRED_SIZE)
                    .addGap(35, 35, 35)
                    .addComponent(OBJ_96, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(CASENS, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 83, Short.MAX_VALUE)
                    .addComponent(OBJ_97, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
                    .addGap(1, 1, 1)
                    .addGroup(p_TCI4ContentContainerLayout.createParallelGroup()
                      .addGroup(p_TCI4ContentContainerLayout.createSequentialGroup()
                        .addGap(58, 58, 58)
                        .addComponent(CACS2, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                      .addComponent(CACS1, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addGroup(p_TCI4ContentContainerLayout.createSequentialGroup()
                        .addGap(116, 116, 116)
                        .addComponent(CACS3, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)))
                    .addContainerGap())
              );
              p_TCI4ContentContainerLayout.setVerticalGroup(
                p_TCI4ContentContainerLayout.createParallelGroup()
                  .addGroup(p_TCI4ContentContainerLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(p_TCI4ContentContainerLayout.createParallelGroup()
                      .addGroup(p_TCI4ContentContainerLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(OBJ_97, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(CACS2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(CACS1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(CACS3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(p_TCI4ContentContainerLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(CAMTTN, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_96, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CASENS, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)))
                    .addContainerGap(26, Short.MAX_VALUE))
              );
            }
            panel1.add(p_TCI4);
            p_TCI4.setBounds(25, 310, 705, 95);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_104 ----
            OBJ_104.setText("Objet s\u00e9lection");
            OBJ_104.setName("OBJ_104");
            panel2.add(OBJ_104);
            OBJ_104.setBounds(25, 50, 94, 20);

            //---- CAOBJ ----
            CAOBJ.setComponentPopupMenu(BTD);
            CAOBJ.setName("CAOBJ");
            panel2.add(CAOBJ);
            CAOBJ.setBounds(160, 45, 570, CAOBJ.getPreferredSize().height);

            //---- CAOBS ----
            CAOBS.setComponentPopupMenu(BTD);
            CAOBS.setName("CAOBS");
            panel2.add(CAOBS);
            CAOBS.setBounds(160, 80, 570, CAOBS.getPreferredSize().height);

            //---- OBJ_105 ----
            OBJ_105.setText("Observation");
            OBJ_105.setName("OBJ_105");
            panel2.add(OBJ_105);
            OBJ_105.setBounds(25, 85, 76, 20);

            //---- xTitledSeparator1 ----
            xTitledSeparator1.setTitle("Comptes auxiliaires");
            xTitledSeparator1.setName("xTitledSeparator1");
            panel2.add(xTitledSeparator1);
            xTitledSeparator1.setBounds(10, 15, 730, xTitledSeparator1.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                  .addComponent(panel1, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 754, Short.MAX_VALUE)
                  .addComponent(panel2, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 754, Short.MAX_VALUE))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 415, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }

    //---- TCI1 ----
    TCI1.setText("");
    TCI1.setToolTipText("Identification");
    TCI1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI1.setName("TCI1");
    TCI1.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI1ActionPerformed(e);
      }
    });

    //---- TCI2 ----
    TCI2.setText("");
    TCI2.setToolTipText("Comptes");
    TCI2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI2.setName("TCI2");
    TCI2.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI2ActionPerformed(e);
      }
    });

    //---- TCI3 ----
    TCI3.setText("");
    TCI3.setToolTipText("R\u00e8glement");
    TCI3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI3.setName("TCI3");
    TCI3.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI3ActionPerformed(e);
      }
    });

    //---- TCI4 ----
    TCI4.setText("");
    TCI4.setToolTipText("Divers");
    TCI4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI4.setName("TCI4");
    TCI4.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI4ActionPerformed(e);
      }
    });
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_49;
  private XRiTextField INDNUM;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JXTitledPanel p_TCI1;
  private XRiCheckBox CAREL;
  private JLabel OBJ_88;
  private JLabel OBJ_86;
  private XRiTextField CACDPD;
  private XRiTextField CACDPF;
  private XRiTextField CAREP1;
  private XRiTextField CAREP2;
  private XRiTextField CAREP3;
  private XRiTextField CAREP4;
  private XRiTextField CAREP5;
  private JLabel OBJ_87;
  private JXTitledPanel p_TCI2;
  private XRiCheckBox CAQTE;
  private JLabel OBJ_91;
  private JLabel OBJ_89;
  private XRiTextField CAAUXD;
  private XRiTextField CAAUXF;
  private XRiTextField CACOLD;
  private XRiTextField CACOLF;
  private JLabel OBJ_92;
  private JLabel OBJ_90;
  private JXTitledPanel p_TCI3;
  private JLabel OBJ_94;
  private JLabel OBJ_95;
  private JLabel OBJ_93;
  private XRiTextField CADEV1;
  private XRiTextField CADEV2;
  private XRiTextField CAMRG1;
  private XRiTextField CAMRG2;
  private XRiTextField CAMRG3;
  private XRiTextField CAMRG4;
  private XRiTextField CAMRG5;
  private XRiTextField CAECH1;
  private XRiTextField CAECH2;
  private XRiTextField CAECH3;
  private XRiTextField CAECH4;
  private XRiTextField CAECH5;
  private JXTitledPanel p_TCI4;
  private XRiCheckBox CAMTTN;
  private JLabel OBJ_96;
  private XRiSpinner CASENS;
  private JLabel OBJ_97;
  private XRiTextField CACS1;
  private XRiTextField CACS2;
  private XRiTextField CACS3;
  private JPanel panel2;
  private JLabel OBJ_104;
  private XRiTextField CAOBJ;
  private XRiTextField CAOBS;
  private JLabel OBJ_105;
  private JXTitledSeparator xTitledSeparator1;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  private SNBoutonDetail TCI1;
  private SNBoutonDetail TCI2;
  private SNBoutonDetail TCI3;
  private SNBoutonDetail TCI4;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
