
package ri.serien.libecranrpg.vcgm.VCGM11FM;
// Nom Fichier: i_VCGM11FM_FMTX3_876.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGM11FM_X3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private Border[] formeZone_value = { BorderFactory.createLoweredBevelBorder(), BorderFactory.createLoweredBevelBorder(),
      BorderFactory.createEmptyBorder(), BorderFactory.createEmptyBorder(), BorderFactory.createEmptyBorder() };
  private Border formeZone = null;
  private int nbrLigneMax = 0;
  private int nbrColonneMax = 0;
  
  public VCGM11FM_X3(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // traitement des JTextArea
    miseEnForme(XFT01, "01");
    miseEnForme(XFT02, "02");
    miseEnForme(XFT03, "03");
    miseEnForme(XFT04, "04");
    miseEnForme(XFT05, "05");
    miseEnForme(XFT06, "06");
    miseEnForme(XFT07, "07");
    miseEnForme(XFT08, "08");
    miseEnForme(XFT09, "09");
    miseEnForme(XFT10, "10");
    miseEnForme(XFT11, "11");
    miseEnForme(XFT12, "12");
    miseEnForme(XFT13, "13");
    miseEnForme(XFT14, "14");
    miseEnForme(XFT15, "15");
    miseEnForme(XFT16, "16");
    miseEnForme(XFT17, "17");
    miseEnForme(XFT18, "18");
    miseEnForme(XFT19, "19");
    miseEnForme(XFT20, "20");
    miseEnForme(XFT21, "21");
    miseEnForme(XFT22, "22");
    miseEnForme(XFT23, "23");
    miseEnForme(XFT24, "24");
    miseEnForme(XFT25, "25");
    miseEnForme(XFT26, "26");
    miseEnForme(XFT27, "27");
    miseEnForme(XFT28, "28");
    miseEnForme(XFT29, "29");
    miseEnForme(XFT30, "30");
    miseEnForme(XFT31, "31");
    miseEnForme(XFT32, "32");
    miseEnForme(XFT33, "33");
    miseEnForme(XFT34, "34");
    miseEnForme(XFT35, "35");
    miseEnForme(XFT36, "36");
    miseEnForme(XFT37, "37");
    miseEnForme(XFT38, "38");
    miseEnForme(XFT39, "39");
    miseEnForme(XFT40, "40");
    miseEnForme(XFT41, "41");
    miseEnForme(XFT42, "42");
    miseEnForme(XFT43, "43");
    miseEnForme(XFT44, "44");
    miseEnForme(XFT45, "45");
    miseEnForme(XFT46, "46");
    miseEnForme(XFT47, "47");
    miseEnForme(XFT48, "48");
    miseEnForme(XFT49, "49");
    miseEnForme(XFT50, "50");
    
    // placement des boutons
    OBJ_70.setBounds((nbrColonneMax - 378), (nbrLigneMax + 50), 54, 40);
    OBJ_71.setBounds((nbrColonneMax - 319), (nbrLigneMax + 50), 54, 40);
    OBJ_72.setBounds((nbrColonneMax - 260), (nbrLigneMax + 50), 54, 40);
    OBJ_73.setBounds((nbrColonneMax - 201), (nbrLigneMax + 50), 54, 40);
    OBJ_74.setBounds((nbrColonneMax - 142), (nbrLigneMax + 50), 54, 40);
    BT_ENTER.setBounds((nbrColonneMax - 83), (nbrLigneMax + 50), 54, 40);
    OBJ_76.setBounds((nbrColonneMax - 24), (nbrLigneMax + 50), 54, 40);
    BT_ERR.setBounds(5, (nbrLigneMax + 50), 40, 40);
    V03F.setBounds(50, (nbrLigneMax + 50), 350, 32);
    
    // panel décors
    panel1.setBounds(5, 5, (nbrColonneMax + 30), (nbrLigneMax + 35));
    
    // taille de la popup
    this.setPreferredSize(new Dimension((nbrColonneMax + 40), (nbrLigneMax + 100)));
    this.repaint();
    
    OBJ_74.setVisible(interpreteurD.analyseExpression("@V02F@").equalsIgnoreCase("ERR"));
    OBJ_73.setVisible(!interpreteurD.analyseExpression("@XFT30@").trim().equalsIgnoreCase(""));
    
    if (lexique.isTrue("61")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    // TODO Icones
    OBJ_74.setIcon(lexique.chargerImage("images/w95mbx03.gif", true));
    OBJ_73.setIcon(lexique.chargerImage("images/lies3.gif", true));
    OBJ_72.setIcon(lexique.chargerImage("images/suivi.gif", true));
    OBJ_71.setIcon(lexique.chargerImage("images/rechart.gif", true));
    OBJ_70.setIcon(lexique.chargerImage("images/bnp.gif", true));
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    OBJ_76.setIcon(lexique.chargerImage("images/retour.png", true));
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Saisie ligne comptable - @V01F@"));
  }
  
  public int calculLongueur(String longueurDeBase, String typeDeZone) {
    if (typeDeZone == "A") {
      return ((Integer.parseInt(longueurDeBase) - 1) * 10) + 20;
    }
    else {
      return ((Integer.parseInt(longueurDeBase) - 1) * 8) + 20;
    }
  }
  
  public void miseEnForme(XRiTextField zoneSGM, String numeroZone) {
    int longueur = 0;
    int hauteur = XFT01.getHeight();
    int ligne = 0;
    int colonne = 0;
    if (!interpreteurD.analyseExpression("@XFC" + numeroZone + "@").trim().equalsIgnoreCase("1") & lexique.isPresent("XFT" + numeroZone)) {
      ligne = Integer.parseInt(lexique.HostFieldGetData("XIG" + numeroZone)) * 27;
      colonne = (Integer.parseInt(lexique.HostFieldGetData("XOL" + numeroZone))) * 12;
      longueur = calculLongueur(lexique.HostFieldGetData("XFL" + numeroZone), lexique.HostFieldGetData("XFP" + numeroZone));
      formeZone = formeZone_value[Integer.parseInt(lexique.HostFieldGetData("XFD" + numeroZone)) - 1];
      if (lexique.HostFieldGetData("XFD" + numeroZone).equalsIgnoreCase("1")) {
        zoneSGM.setBackground(Color.white);
        zoneSGM.setFocusable(true);
      }
      if (lexique.HostFieldGetData("XFD" + numeroZone).equalsIgnoreCase("2")) {
        zoneSGM.setBackground(new Color(224, 227, 233));
        zoneSGM.setFocusable(false);
      }
      if (lexique.HostFieldGetData("XFD" + numeroZone).equalsIgnoreCase("3")) {
        zoneSGM.setBackground(new Color(214, 217, 223));
        zoneSGM.setFocusable(false);
      }
      if (lexique.HostFieldGetData("XFD" + numeroZone).equalsIgnoreCase("4")) {
        zoneSGM.setForeground(Color.blue);
        zoneSGM.setBackground(new Color(214, 217, 223));
        zoneSGM.setFocusable(false);
      }
      if (lexique.HostFieldGetData("XFD" + numeroZone).equalsIgnoreCase("5")) {
        zoneSGM.setForeground(Color.red);
        zoneSGM.setBackground(new Color(214, 217, 223));
        zoneSGM.setFocusable(false);
      }
      zoneSGM.setBounds(colonne, ligne, longueur, hauteur);
      zoneSGM.setBorder(formeZone);
      zoneSGM.setVisible(true);
      
      // stockage taille max
      if ((colonne + longueur) > nbrColonneMax) {
        nbrColonneMax = colonne + longueur;
      }
      if (ligne > nbrLigneMax) {
        nbrLigneMax = ligne;
      }
    }
    else {
      zoneSGM.setVisible(false);
    }
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "DUPLICATE", false);
  }
  
  private void OBJ_70ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_71ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_72ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_73ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11", false);
  }
  
  private void OBJ_74ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1", false);
  }
  
  private void OBJ_75ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_76ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    CMD = new JPopupMenu();
    OBJ_6 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    OBJ_15 = new JMenuItem();
    P_centre = new JPanel();
    XFT01 = new XRiTextField();
    XFT02 = new XRiTextField();
    XFT03 = new XRiTextField();
    XFT04 = new XRiTextField();
    XFT05 = new XRiTextField();
    XFT06 = new XRiTextField();
    XFT07 = new XRiTextField();
    XFT08 = new XRiTextField();
    XFT09 = new XRiTextField();
    XFT10 = new XRiTextField();
    XFT11 = new XRiTextField();
    XFT12 = new XRiTextField();
    XFT13 = new XRiTextField();
    XFT14 = new XRiTextField();
    XFT15 = new XRiTextField();
    XFT16 = new XRiTextField();
    XFT17 = new XRiTextField();
    XFT18 = new XRiTextField();
    XFT19 = new XRiTextField();
    XFT20 = new XRiTextField();
    XFT21 = new XRiTextField();
    XFT22 = new XRiTextField();
    XFT23 = new XRiTextField();
    XFT24 = new XRiTextField();
    XFT25 = new XRiTextField();
    XFT26 = new XRiTextField();
    XFT27 = new XRiTextField();
    XFT28 = new XRiTextField();
    XFT29 = new XRiTextField();
    XFT30 = new XRiTextField();
    XFT31 = new XRiTextField();
    XFT32 = new XRiTextField();
    XFT33 = new XRiTextField();
    XFT34 = new XRiTextField();
    XFT35 = new XRiTextField();
    XFT36 = new XRiTextField();
    XFT37 = new XRiTextField();
    XFT38 = new XRiTextField();
    XFT39 = new XRiTextField();
    XFT40 = new XRiTextField();
    XFT41 = new XRiTextField();
    XFT42 = new XRiTextField();
    XFT43 = new XRiTextField();
    XFT44 = new XRiTextField();
    XFT45 = new XRiTextField();
    XFT46 = new XRiTextField();
    XFT47 = new XRiTextField();
    XFT48 = new XRiTextField();
    XFT49 = new XRiTextField();
    XFT50 = new XRiTextField();
    OBJ_76 = new JButton();
    BT_ENTER = new JButton();
    OBJ_74 = new JButton();
    OBJ_73 = new JButton();
    OBJ_72 = new JButton();
    OBJ_71 = new JButton();
    OBJ_70 = new JButton();
    V03F = new JLabel();
    BT_ERR = new JButton();
    panel1 = new JPanel();

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_6 ----
      OBJ_6.setText("R\u00e9afficher");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      CMD.add(OBJ_6);

      //---- OBJ_7 ----
      OBJ_7.setText("Annuler");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("For\u00e7age en saisie sur comptes en devises");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      CMD.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setText("Saisie C.E.E");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      CMD.add(OBJ_9);

      //---- OBJ_10 ----
      OBJ_10.setText("Bloc-notes");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      CMD.add(OBJ_10);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Aide en ligne");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("Invite");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);

      //---- OBJ_15 ----
      OBJ_15.setText("Duplication");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(950, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_centre ========
    {
      P_centre.setBackground(new Color(214, 217, 223));
      P_centre.setName("P_centre");
      P_centre.setLayout(null);

      //---- XFT01 ----
      XFT01.setComponentPopupMenu(BTD);
      XFT01.setName("XFT01");
      P_centre.add(XFT01);
      XFT01.setBounds(11, 5, 102, XFT01.getPreferredSize().height);

      //---- XFT02 ----
      XFT02.setComponentPopupMenu(BTD);
      XFT02.setName("XFT02");
      P_centre.add(XFT02);
      XFT02.setBounds(11, 28, 102, XFT02.getPreferredSize().height);

      //---- XFT03 ----
      XFT03.setComponentPopupMenu(BTD);
      XFT03.setName("XFT03");
      P_centre.add(XFT03);
      XFT03.setBounds(11, 51, 102, XFT03.getPreferredSize().height);

      //---- XFT04 ----
      XFT04.setComponentPopupMenu(BTD);
      XFT04.setName("XFT04");
      P_centre.add(XFT04);
      XFT04.setBounds(11, 74, 102, XFT04.getPreferredSize().height);

      //---- XFT05 ----
      XFT05.setComponentPopupMenu(BTD);
      XFT05.setName("XFT05");
      P_centre.add(XFT05);
      XFT05.setBounds(11, 97, 102, XFT05.getPreferredSize().height);

      //---- XFT06 ----
      XFT06.setComponentPopupMenu(BTD);
      XFT06.setName("XFT06");
      P_centre.add(XFT06);
      XFT06.setBounds(11, 120, 102, XFT06.getPreferredSize().height);

      //---- XFT07 ----
      XFT07.setComponentPopupMenu(BTD);
      XFT07.setName("XFT07");
      P_centre.add(XFT07);
      XFT07.setBounds(11, 143, 102, XFT07.getPreferredSize().height);

      //---- XFT08 ----
      XFT08.setComponentPopupMenu(BTD);
      XFT08.setName("XFT08");
      P_centre.add(XFT08);
      XFT08.setBounds(11, 166, 102, XFT08.getPreferredSize().height);

      //---- XFT09 ----
      XFT09.setComponentPopupMenu(BTD);
      XFT09.setName("XFT09");
      P_centre.add(XFT09);
      XFT09.setBounds(11, 189, 102, XFT09.getPreferredSize().height);

      //---- XFT10 ----
      XFT10.setComponentPopupMenu(BTD);
      XFT10.setName("XFT10");
      P_centre.add(XFT10);
      XFT10.setBounds(11, 212, 102, XFT10.getPreferredSize().height);

      //---- XFT11 ----
      XFT11.setComponentPopupMenu(BTD);
      XFT11.setName("XFT11");
      P_centre.add(XFT11);
      XFT11.setBounds(11, 235, 102, XFT11.getPreferredSize().height);

      //---- XFT12 ----
      XFT12.setComponentPopupMenu(BTD);
      XFT12.setName("XFT12");
      P_centre.add(XFT12);
      XFT12.setBounds(11, 258, 102, XFT12.getPreferredSize().height);

      //---- XFT13 ----
      XFT13.setComponentPopupMenu(BTD);
      XFT13.setName("XFT13");
      P_centre.add(XFT13);
      XFT13.setBounds(11, 281, 102, XFT13.getPreferredSize().height);

      //---- XFT14 ----
      XFT14.setComponentPopupMenu(BTD);
      XFT14.setName("XFT14");
      P_centre.add(XFT14);
      XFT14.setBounds(11, 304, 102, XFT14.getPreferredSize().height);

      //---- XFT15 ----
      XFT15.setComponentPopupMenu(BTD);
      XFT15.setName("XFT15");
      P_centre.add(XFT15);
      XFT15.setBounds(11, 327, 102, XFT15.getPreferredSize().height);

      //---- XFT16 ----
      XFT16.setComponentPopupMenu(BTD);
      XFT16.setName("XFT16");
      P_centre.add(XFT16);
      XFT16.setBounds(11, 350, 102, XFT16.getPreferredSize().height);

      //---- XFT17 ----
      XFT17.setComponentPopupMenu(BTD);
      XFT17.setName("XFT17");
      P_centre.add(XFT17);
      XFT17.setBounds(11, 373, 102, XFT17.getPreferredSize().height);

      //---- XFT18 ----
      XFT18.setComponentPopupMenu(BTD);
      XFT18.setName("XFT18");
      P_centre.add(XFT18);
      XFT18.setBounds(11, 396, 102, XFT18.getPreferredSize().height);

      //---- XFT19 ----
      XFT19.setComponentPopupMenu(BTD);
      XFT19.setName("XFT19");
      P_centre.add(XFT19);
      XFT19.setBounds(11, 419, 102, XFT19.getPreferredSize().height);

      //---- XFT20 ----
      XFT20.setComponentPopupMenu(BTD);
      XFT20.setName("XFT20");
      P_centre.add(XFT20);
      XFT20.setBounds(11, 442, 102, XFT20.getPreferredSize().height);

      //---- XFT21 ----
      XFT21.setComponentPopupMenu(BTD);
      XFT21.setName("XFT21");
      P_centre.add(XFT21);
      XFT21.setBounds(119, 5, 102, XFT21.getPreferredSize().height);

      //---- XFT22 ----
      XFT22.setComponentPopupMenu(BTD);
      XFT22.setName("XFT22");
      P_centre.add(XFT22);
      XFT22.setBounds(119, 28, 102, XFT22.getPreferredSize().height);

      //---- XFT23 ----
      XFT23.setComponentPopupMenu(BTD);
      XFT23.setName("XFT23");
      P_centre.add(XFT23);
      XFT23.setBounds(119, 51, 102, XFT23.getPreferredSize().height);

      //---- XFT24 ----
      XFT24.setComponentPopupMenu(BTD);
      XFT24.setName("XFT24");
      P_centre.add(XFT24);
      XFT24.setBounds(119, 74, 102, XFT24.getPreferredSize().height);

      //---- XFT25 ----
      XFT25.setComponentPopupMenu(BTD);
      XFT25.setName("XFT25");
      P_centre.add(XFT25);
      XFT25.setBounds(119, 97, 102, XFT25.getPreferredSize().height);

      //---- XFT26 ----
      XFT26.setComponentPopupMenu(BTD);
      XFT26.setName("XFT26");
      P_centre.add(XFT26);
      XFT26.setBounds(119, 120, 102, XFT26.getPreferredSize().height);

      //---- XFT27 ----
      XFT27.setComponentPopupMenu(BTD);
      XFT27.setName("XFT27");
      P_centre.add(XFT27);
      XFT27.setBounds(119, 143, 102, XFT27.getPreferredSize().height);

      //---- XFT28 ----
      XFT28.setComponentPopupMenu(BTD);
      XFT28.setName("XFT28");
      P_centre.add(XFT28);
      XFT28.setBounds(119, 166, 102, XFT28.getPreferredSize().height);

      //---- XFT29 ----
      XFT29.setComponentPopupMenu(BTD);
      XFT29.setName("XFT29");
      P_centre.add(XFT29);
      XFT29.setBounds(119, 189, 102, XFT29.getPreferredSize().height);

      //---- XFT30 ----
      XFT30.setComponentPopupMenu(BTD);
      XFT30.setName("XFT30");
      P_centre.add(XFT30);
      XFT30.setBounds(119, 212, 102, XFT30.getPreferredSize().height);

      //---- XFT31 ----
      XFT31.setComponentPopupMenu(BTD);
      XFT31.setName("XFT31");
      P_centre.add(XFT31);
      XFT31.setBounds(119, 235, 102, XFT31.getPreferredSize().height);

      //---- XFT32 ----
      XFT32.setComponentPopupMenu(BTD);
      XFT32.setName("XFT32");
      P_centre.add(XFT32);
      XFT32.setBounds(119, 258, 102, XFT32.getPreferredSize().height);

      //---- XFT33 ----
      XFT33.setComponentPopupMenu(BTD);
      XFT33.setName("XFT33");
      P_centre.add(XFT33);
      XFT33.setBounds(119, 281, 102, XFT33.getPreferredSize().height);

      //---- XFT34 ----
      XFT34.setComponentPopupMenu(BTD);
      XFT34.setName("XFT34");
      P_centre.add(XFT34);
      XFT34.setBounds(119, 304, 102, XFT34.getPreferredSize().height);

      //---- XFT35 ----
      XFT35.setComponentPopupMenu(BTD);
      XFT35.setName("XFT35");
      P_centre.add(XFT35);
      XFT35.setBounds(119, 327, 102, XFT35.getPreferredSize().height);

      //---- XFT36 ----
      XFT36.setComponentPopupMenu(BTD);
      XFT36.setName("XFT36");
      P_centre.add(XFT36);
      XFT36.setBounds(119, 350, 102, XFT36.getPreferredSize().height);

      //---- XFT37 ----
      XFT37.setComponentPopupMenu(BTD);
      XFT37.setName("XFT37");
      P_centre.add(XFT37);
      XFT37.setBounds(119, 373, 102, XFT37.getPreferredSize().height);

      //---- XFT38 ----
      XFT38.setComponentPopupMenu(BTD);
      XFT38.setName("XFT38");
      P_centre.add(XFT38);
      XFT38.setBounds(119, 396, 102, XFT38.getPreferredSize().height);

      //---- XFT39 ----
      XFT39.setComponentPopupMenu(BTD);
      XFT39.setName("XFT39");
      P_centre.add(XFT39);
      XFT39.setBounds(119, 419, 102, XFT39.getPreferredSize().height);

      //---- XFT40 ----
      XFT40.setComponentPopupMenu(BTD);
      XFT40.setName("XFT40");
      P_centre.add(XFT40);
      XFT40.setBounds(119, 442, 102, XFT40.getPreferredSize().height);

      //---- XFT41 ----
      XFT41.setComponentPopupMenu(BTD);
      XFT41.setName("XFT41");
      P_centre.add(XFT41);
      XFT41.setBounds(227, 5, 102, XFT41.getPreferredSize().height);

      //---- XFT42 ----
      XFT42.setComponentPopupMenu(BTD);
      XFT42.setName("XFT42");
      P_centre.add(XFT42);
      XFT42.setBounds(227, 30, 102, XFT42.getPreferredSize().height);

      //---- XFT43 ----
      XFT43.setComponentPopupMenu(BTD);
      XFT43.setName("XFT43");
      P_centre.add(XFT43);
      XFT43.setBounds(227, 55, 102, XFT43.getPreferredSize().height);

      //---- XFT44 ----
      XFT44.setComponentPopupMenu(BTD);
      XFT44.setName("XFT44");
      P_centre.add(XFT44);
      XFT44.setBounds(227, 80, 102, XFT44.getPreferredSize().height);

      //---- XFT45 ----
      XFT45.setComponentPopupMenu(BTD);
      XFT45.setName("XFT45");
      P_centre.add(XFT45);
      XFT45.setBounds(227, 105, 102, XFT45.getPreferredSize().height);

      //---- XFT46 ----
      XFT46.setComponentPopupMenu(BTD);
      XFT46.setName("XFT46");
      P_centre.add(XFT46);
      XFT46.setBounds(227, 130, 102, XFT46.getPreferredSize().height);

      //---- XFT47 ----
      XFT47.setComponentPopupMenu(BTD);
      XFT47.setName("XFT47");
      P_centre.add(XFT47);
      XFT47.setBounds(227, 155, 102, XFT47.getPreferredSize().height);

      //---- XFT48 ----
      XFT48.setComponentPopupMenu(BTD);
      XFT48.setName("XFT48");
      P_centre.add(XFT48);
      XFT48.setBounds(227, 180, 102, XFT48.getPreferredSize().height);

      //---- XFT49 ----
      XFT49.setComponentPopupMenu(BTD);
      XFT49.setName("XFT49");
      P_centre.add(XFT49);
      XFT49.setBounds(227, 205, 102, XFT49.getPreferredSize().height);

      //---- XFT50 ----
      XFT50.setComponentPopupMenu(BTD);
      XFT50.setName("XFT50");
      P_centre.add(XFT50);
      XFT50.setBounds(227, 230, 102, XFT50.getPreferredSize().height);

      //---- OBJ_76 ----
      OBJ_76.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_76.setToolTipText("Annuler");
      OBJ_76.setName("OBJ_76");
      OBJ_76.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_76ActionPerformed(e);
        }
      });
      P_centre.add(OBJ_76);
      OBJ_76.setBounds(915, 635, 54, 40);

      //---- BT_ENTER ----
      BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ENTER.setToolTipText("Ok");
      BT_ENTER.setName("BT_ENTER");
      BT_ENTER.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_75ActionPerformed(e);
        }
      });
      P_centre.add(BT_ENTER);
      BT_ENTER.setBounds(855, 635, 54, 40);

      //---- OBJ_74 ----
      OBJ_74.setText("");
      OBJ_74.setToolTipText("D\u00e9tail des erreurs");
      OBJ_74.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_74.setName("OBJ_74");
      OBJ_74.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_74ActionPerformed(e);
        }
      });
      P_centre.add(OBJ_74);
      OBJ_74.setBounds(795, 635, 54, 40);

      //---- OBJ_73 ----
      OBJ_73.setText("");
      OBJ_73.setToolTipText("Documents li\u00e9s");
      OBJ_73.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_73.setName("OBJ_73");
      OBJ_73.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_73ActionPerformed(e);
        }
      });
      P_centre.add(OBJ_73);
      OBJ_73.setBounds(735, 635, 54, 40);

      //---- OBJ_72 ----
      OBJ_72.setText("");
      OBJ_72.setToolTipText("Plan comptable");
      OBJ_72.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_72.setName("OBJ_72");
      OBJ_72.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_72ActionPerformed(e);
        }
      });
      P_centre.add(OBJ_72);
      OBJ_72.setBounds(675, 635, 54, 40);

      //---- OBJ_71 ----
      OBJ_71.setText("");
      OBJ_71.setToolTipText("Vue du compte");
      OBJ_71.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_71.setName("OBJ_71");
      OBJ_71.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_71ActionPerformed(e);
        }
      });
      P_centre.add(OBJ_71);
      OBJ_71.setBounds(615, 635, 54, 40);

      //---- OBJ_70 ----
      OBJ_70.setText("");
      OBJ_70.setToolTipText("Ecriture Historis\u00e9e");
      OBJ_70.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_70.setName("OBJ_70");
      OBJ_70.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_70ActionPerformed(e);
        }
      });
      P_centre.add(OBJ_70);
      OBJ_70.setBounds(555, 635, 54, 40);

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_centre.add(V03F);
      V03F.setBounds(65, 635, 350, 32);

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_centre.add(BT_ERR);
      BT_ERR.setBounds(new Rectangle(new Point(30, 635), BT_ERR.getPreferredSize()));

      //======== panel1 ========
      {
        panel1.setBorder(new TitledBorder(""));
        panel1.setName("panel1");
        panel1.setLayout(null);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel1.getComponentCount(); i++) {
            Rectangle bounds = panel1.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel1.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel1.setMinimumSize(preferredSize);
          panel1.setPreferredSize(preferredSize);
        }
      }
      P_centre.add(panel1);
      panel1.setBounds(540, 340, 435, 225);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < P_centre.getComponentCount(); i++) {
          Rectangle bounds = P_centre.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = P_centre.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        P_centre.setMinimumSize(preferredSize);
        P_centre.setPreferredSize(preferredSize);
      }
    }
    add(P_centre, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu CMD;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_15;
  private JPanel P_centre;
  private XRiTextField XFT01;
  private XRiTextField XFT02;
  private XRiTextField XFT03;
  private XRiTextField XFT04;
  private XRiTextField XFT05;
  private XRiTextField XFT06;
  private XRiTextField XFT07;
  private XRiTextField XFT08;
  private XRiTextField XFT09;
  private XRiTextField XFT10;
  private XRiTextField XFT11;
  private XRiTextField XFT12;
  private XRiTextField XFT13;
  private XRiTextField XFT14;
  private XRiTextField XFT15;
  private XRiTextField XFT16;
  private XRiTextField XFT17;
  private XRiTextField XFT18;
  private XRiTextField XFT19;
  private XRiTextField XFT20;
  private XRiTextField XFT21;
  private XRiTextField XFT22;
  private XRiTextField XFT23;
  private XRiTextField XFT24;
  private XRiTextField XFT25;
  private XRiTextField XFT26;
  private XRiTextField XFT27;
  private XRiTextField XFT28;
  private XRiTextField XFT29;
  private XRiTextField XFT30;
  private XRiTextField XFT31;
  private XRiTextField XFT32;
  private XRiTextField XFT33;
  private XRiTextField XFT34;
  private XRiTextField XFT35;
  private XRiTextField XFT36;
  private XRiTextField XFT37;
  private XRiTextField XFT38;
  private XRiTextField XFT39;
  private XRiTextField XFT40;
  private XRiTextField XFT41;
  private XRiTextField XFT42;
  private XRiTextField XFT43;
  private XRiTextField XFT44;
  private XRiTextField XFT45;
  private XRiTextField XFT46;
  private XRiTextField XFT47;
  private XRiTextField XFT48;
  private XRiTextField XFT49;
  private XRiTextField XFT50;
  private JButton OBJ_76;
  private JButton BT_ENTER;
  private JButton OBJ_74;
  private JButton OBJ_73;
  private JButton OBJ_72;
  private JButton OBJ_71;
  private JButton OBJ_70;
  private JLabel V03F;
  private JButton BT_ERR;
  private JPanel panel1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
