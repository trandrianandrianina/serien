
package ri.serien.libecranrpg.vcgm.VCGM10FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VCGM10FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  // private String[] _LECHEANCE_Top = null;
  private String[] _NIV01_Titre = { "", "", "N", "LIBMT2", "Echéance", "Règlement", "Préimputation" };
  private String[][] _NIV01_Data = { { "LDR01", "DDR01", "NIV01", "MTT01", "ECH01", "RGL01", "PRI01" },
      { "LDR02", "DDR02", "NIV02", "MTT02", "ECH02", "RGL02", "PRI02" }, { "LDR03", "DDR03", "NIV03", "MTT03", "ECH03", "RGL03", "PRI03" },
      { "LDR04", "DDR04", "NIV04", "MTT04", "ECH04", "RGL04", "PRI04" } };
  private int[] _LECHEANCE_Width = { 80, 80, 25, 120, 100, 50, 90 };
  private int[] _LECHEANCE_Justif = { SwingConstants.LEFT, SwingConstants.LEFT, SwingConstants.CENTER, SwingConstants.RIGHT,
      SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.RIGHT };
  
  public VCGM10FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    // ((DefaultTableCellRenderer)LECHEANCE.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.LEFT);
    
    // Ajout
    initDiverses();
    NIV01.setAspectTable(null, _NIV01_Titre, _NIV01_Data, _LECHEANCE_Width, true, _LECHEANCE_Justif, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBCPT@")).trim());
    OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMT1@")).trim());
    OBJ_52.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1SNS@")).trim());
    OBJ_57.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1DEV@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LECHEANCE, LECHEANCE.get_LIST_Title_Data_Brut(), _LECHEANCE_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    OBJ_55.setEnabled(lexique.isPresent("L1MTD"));
    OBJ_55.setVisible(lexique.isTrue("75"));
    OBJ_52.setEnabled(lexique.isPresent("L1SNS"));
    OBJ_24.setEnabled(lexique.isPresent("L1SAN"));
    OBJ_71.setEnabled(lexique.isPresent("L1ACT"));
    OBJ_73.setEnabled(lexique.isPresent("L1NAT"));
    OBJ_24.setVisible(lexique.isTrue("72"));
    OBJ_71.setVisible(lexique.isTrue("72"));
    OBJ_73.setVisible(lexique.isTrue("72"));
    L1RLV.setVisible(lexique.isTrue("73"));
    OBJ_43.setEnabled(lexique.isPresent("L1RLV"));
    OBJ_43.setVisible(lexique.isTrue("73"));
    L1AA6.setVisible(lexique.isTrue("72"));
    L1AA5.setVisible(lexique.isTrue("72"));
    L1AA4.setVisible(lexique.isTrue("72"));
    L1AA3.setVisible(lexique.isTrue("72"));
    L1AA2.setVisible(lexique.isTrue("72"));
    L1AA1.setVisible(lexique.isTrue("72"));
    OBJ_39.setEnabled(lexique.isPresent("L1DVBX"));
    OBJ_39.setVisible(lexique.isTrue("80"));
    OBJ_57.setVisible(lexique.isTrue("75"));
    OBJ_53.setEnabled(lexique.isPresent("L1QTE"));
    OBJ_53.setVisible(lexique.isTrue("76"));
    OBJ_58.setEnabled(lexique.isPresent("DVCHG"));
    OBJ_58.setVisible(lexique.isTrue("75"));
    L1DVBX.setVisible(lexique.isTrue("80"));
    OBJ_35.setVisible(lexique.isPresent("L1NCPX"));
    L1MTD.setVisible(lexique.isTrue("75"));
    OBJ_50.setEnabled(lexique.isPresent("LIBMT1"));
    L1QTE.setVisible(lexique.isTrue("76"));
    OBJ_82.setVisible(lexique.isTrue("61"));
    OBJ_42.setEnabled(lexique.isPresent("L1PCE"));
    DVCHG.setVisible(lexique.isTrue("75"));
    OBJ_41.setEnabled(lexique.isPresent("L1RFC"));
    LIBTYF.setVisible(lexique.isTrue("61"));
    xTitledPanel4.setVisible(lexique.isTrue("72"));
    xTitledPanel3.setVisible(lexique.isTrue("74"));
    xTitledPanel6.setVisible(lexique.isTrue("72"));
    NIV01.setVisible(lexique.isTrue("74"));
    L1SAN.setVisible(lexique.isTrue("72"));
    L1NAT.setVisible(lexique.isTrue("72"));
    L1ACT.setVisible(lexique.isTrue("72"));
    
    if (lexique.isTrue("(N72) AND (N74)")) {
      setPreferredSize(new Dimension(1115, 340));
    }
    else {
      setPreferredSize(new Dimension(1115, 490));
    }
    
    xTitledPanel4.setVisible(lexique.isTrue("72"));
    xTitledPanel6.setVisible(lexique.isTrue("72"));
    xTitledPanel3.setVisible(lexique.isTrue("74"));
    
    // Remplace C ou D par crédit ou débit sur l'étiquette OBJ_52
    if (lexique.HostFieldGetData("L1SNS").equalsIgnoreCase("C")) {
      OBJ_52.setText("Crédit");
    }
    else {
      OBJ_52.setText("Débit");
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - Recherche d'écritures comptables"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_82ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_27 = new JLabel();
    L1NLI = new XRiTextField();
    WAFFAC = new XRiTextField();
    OBJ_29 = new JLabel();
    L1NCG = new XRiTextField();
    OBJ_32 = new RiZoneSortie();
    WLIBB1 = new XRiTextField();
    L1CLB = new XRiTextField();
    OBJ_35 = new JLabel();
    L1NCPX = new XRiTextField();
    L1DVBX = new XRiCalendrier();
    OBJ_39 = new JLabel();
    L1RFC = new XRiTextField();
    OBJ_41 = new JLabel();
    OBJ_42 = new JLabel();
    L1PCE = new XRiTextField();
    OBJ_43 = new JLabel();
    L1RLV = new XRiTextField();
    L1NCA = new XRiTextField();
    label2 = new JLabel();
    label3 = new JLabel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_50 = new JLabel();
    OBJ_55 = new JLabel();
    L1MTT = new XRiTextField();
    L1MTD = new XRiTextField();
    OBJ_52 = new RiZoneSortie();
    OBJ_57 = new RiZoneSortie();
    OBJ_53 = new JLabel();
    OBJ_58 = new JLabel();
    L1QTE = new XRiTextField();
    DVCHG = new XRiTextField();
    xTitledPanel4 = new JXTitledPanel();
    L1AA1 = new XRiTextField();
    L1AA2 = new XRiTextField();
    OBJ_82 = new JButton();
    LIBTYF = new XRiTextField();
    L1AA3 = new XRiTextField();
    L1AA4 = new XRiTextField();
    L1AA5 = new XRiTextField();
    L1AA6 = new XRiTextField();
    xTitledPanel6 = new JXTitledPanel();
    OBJ_24 = new JLabel();
    L1SAN = new XRiTextField();
    OBJ_71 = new JLabel();
    L1ACT = new XRiTextField();
    OBJ_73 = new JLabel();
    L1NAT = new XRiTextField();
    xTitledPanel3 = new JXTitledPanel();
    SCROLLPANE_LECHEANCE = new JScrollPane();
    NIV01 = new XRiTable();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1115, 490));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Acc\u00e8s aux \u00e9ch\u00e9ances");
            riSousMenu_bt6.setToolTipText("Acc\u00e8s aux \u00e9ch\u00e9ances");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Vue de la facture");
            riSousMenu_bt7.setToolTipText("Vue de la facture");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);

          //======== riMenu3 ========
          {
            riMenu3.setName("riMenu3");

            //---- riMenu_bt3 ----
            riMenu_bt3.setText("Outils");
            riMenu_bt3.setName("riMenu_bt3");
            riMenu3.add(riMenu_bt3);
          }
          menus_haut.add(riMenu3);

          //======== riSousMenu14 ========
          {
            riSousMenu14.setName("riSousMenu14");

            //---- riSousMenu_bt14 ----
            riSousMenu_bt14.setText("Documents li\u00e9s");
            riSousMenu_bt14.setToolTipText("Documents li\u00e9s");
            riSousMenu_bt14.setName("riSousMenu_bt14");
            riSousMenu_bt14.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt14ActionPerformed(e);
              }
            });
            riSousMenu14.add(riSousMenu_bt14);
          }
          menus_haut.add(riSousMenu14);

          //======== riSousMenu15 ========
          {
            riSousMenu15.setName("riSousMenu15");

            //---- riSousMenu_bt15 ----
            riSousMenu_bt15.setText("Bloc-notes");
            riSousMenu_bt15.setToolTipText("Bloc-notes");
            riSousMenu_bt15.setName("riSousMenu_bt15");
            riSousMenu_bt15.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt15ActionPerformed(e);
              }
            });
            riSousMenu15.add(riSousMenu_bt15);
          }
          menus_haut.add(riSousMenu15);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== xTitledPanel2 ========
        {
          xTitledPanel2.setTitle("Ecriture");
          xTitledPanel2.setBorder(new DropShadowBorder());
          xTitledPanel2.setName("xTitledPanel2");
          Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
          xTitledPanel2ContentContainer.setLayout(null);

          //---- OBJ_27 ----
          OBJ_27.setText("Num\u00e9ro de ligne");
          OBJ_27.setName("OBJ_27");
          xTitledPanel2ContentContainer.add(OBJ_27);
          OBJ_27.setBounds(35, 13, 102, 28);

          //---- L1NLI ----
          L1NLI.setName("L1NLI");
          xTitledPanel2ContentContainer.add(L1NLI);
          L1NLI.setBounds(219, 13, 45, L1NLI.getPreferredSize().height);

          //---- WAFFAC ----
          WAFFAC.setComponentPopupMenu(BTD);
          WAFFAC.setName("WAFFAC");
          xTitledPanel2ContentContainer.add(WAFFAC);
          WAFFAC.setBounds(719, 13, 20, WAFFAC.getPreferredSize().height);

          //---- OBJ_29 ----
          OBJ_29.setText("Compte / Tiers");
          OBJ_29.setName("OBJ_29");
          xTitledPanel2ContentContainer.add(OBJ_29);
          OBJ_29.setBounds(35, 43, 91, 28);

          //---- L1NCG ----
          L1NCG.setName("L1NCG");
          xTitledPanel2ContentContainer.add(L1NCG);
          L1NCG.setBounds(219, 43, 60, L1NCG.getPreferredSize().height);

          //---- OBJ_32 ----
          OBJ_32.setText("@LIBCPT@");
          OBJ_32.setName("OBJ_32");
          xTitledPanel2ContentContainer.add(OBJ_32);
          OBJ_32.setBounds(364, 45, 309, OBJ_32.getPreferredSize().height);

          //---- WLIBB1 ----
          WLIBB1.setComponentPopupMenu(BTD);
          WLIBB1.setName("WLIBB1");
          xTitledPanel2ContentContainer.add(WLIBB1);
          WLIBB1.setBounds(219, 73, 364, WLIBB1.getPreferredSize().height);

          //---- L1CLB ----
          L1CLB.setToolTipText("Code libell\u00e9");
          L1CLB.setComponentPopupMenu(BTD);
          L1CLB.setName("L1CLB");
          xTitledPanel2ContentContainer.add(L1CLB);
          L1CLB.setBounds(720, 73, 20, L1CLB.getPreferredSize().height);

          //---- OBJ_35 ----
          OBJ_35.setText("Contre partie");
          OBJ_35.setName("OBJ_35");
          xTitledPanel2ContentContainer.add(OBJ_35);
          OBJ_35.setBounds(380, 103, 85, 28);

          //---- L1NCPX ----
          L1NCPX.setComponentPopupMenu(BTD);
          L1NCPX.setName("L1NCPX");
          xTitledPanel2ContentContainer.add(L1NCPX);
          L1NCPX.setBounds(465, 103, 60, L1NCPX.getPreferredSize().height);

          //---- L1DVBX ----
          L1DVBX.setComponentPopupMenu(BTD);
          L1DVBX.setName("L1DVBX");
          xTitledPanel2ContentContainer.add(L1DVBX);
          L1DVBX.setBounds(719, 103, 105, L1DVBX.getPreferredSize().height);

          //---- OBJ_39 ----
          OBJ_39.setText("Date de valeur");
          OBJ_39.setName("OBJ_39");
          xTitledPanel2ContentContainer.add(OBJ_39);
          OBJ_39.setBounds(620, 103, 95, 28);

          //---- L1RFC ----
          L1RFC.setComponentPopupMenu(BTD);
          L1RFC.setName("L1RFC");
          xTitledPanel2ContentContainer.add(L1RFC);
          L1RFC.setBounds(219, 103, 110, L1RFC.getPreferredSize().height);

          //---- OBJ_41 ----
          OBJ_41.setText("R\u00e9f\u00e9rence classement");
          OBJ_41.setName("OBJ_41");
          xTitledPanel2ContentContainer.add(OBJ_41);
          OBJ_41.setBounds(35, 103, 138, 28);

          //---- OBJ_42 ----
          OBJ_42.setText("Num\u00e9ro de pi\u00e8ce");
          OBJ_42.setName("OBJ_42");
          xTitledPanel2ContentContainer.add(OBJ_42);
          OBJ_42.setBounds(35, 133, 108, 28);

          //---- L1PCE ----
          L1PCE.setComponentPopupMenu(BTD);
          L1PCE.setName("L1PCE");
          xTitledPanel2ContentContainer.add(L1PCE);
          L1PCE.setBounds(219, 133, 68, L1PCE.getPreferredSize().height);

          //---- OBJ_43 ----
          OBJ_43.setText("Relev\u00e9");
          OBJ_43.setName("OBJ_43");
          xTitledPanel2ContentContainer.add(OBJ_43);
          OBJ_43.setBounds(620, 133, 95, 28);

          //---- L1RLV ----
          L1RLV.setComponentPopupMenu(BTD);
          L1RLV.setName("L1RLV");
          xTitledPanel2ContentContainer.add(L1RLV);
          L1RLV.setBounds(719, 133, 60, L1RLV.getPreferredSize().height);

          //---- L1NCA ----
          L1NCA.setName("L1NCA");
          xTitledPanel2ContentContainer.add(L1NCA);
          L1NCA.setBounds(286, 43, 60, L1NCA.getPreferredSize().height);

          //---- label2 ----
          label2.setText("Code");
          label2.setName("label2");
          xTitledPanel2ContentContainer.add(label2);
          label2.setBounds(620, 73, 95, 28);

          //---- label3 ----
          label3.setText("Libell\u00e9 de l'\u00e9criture");
          label3.setName("label3");
          xTitledPanel2ContentContainer.add(label3);
          label3.setBounds(35, 73, label3.getPreferredSize().width, 28);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel2ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(xTitledPanel2);
        xTitledPanel2.setBounds(10, 10, 924, 205);

        //======== xTitledPanel1 ========
        {
          xTitledPanel1.setTitle("Montant");
          xTitledPanel1.setBorder(new DropShadowBorder());
          xTitledPanel1.setName("xTitledPanel1");
          Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
          xTitledPanel1ContentContainer.setLayout(null);

          //---- OBJ_50 ----
          OBJ_50.setText("@LIBMT1@");
          OBJ_50.setName("OBJ_50");
          xTitledPanel1ContentContainer.add(OBJ_50);
          OBJ_50.setBounds(37, 10, 97, 28);

          //---- OBJ_55 ----
          OBJ_55.setText("Devise");
          OBJ_55.setName("OBJ_55");
          xTitledPanel1ContentContainer.add(OBJ_55);
          OBJ_55.setBounds(37, 40, 64, 28);

          //---- L1MTT ----
          L1MTT.setComponentPopupMenu(BTD);
          L1MTT.setName("L1MTT");
          xTitledPanel1ContentContainer.add(L1MTT);
          L1MTT.setBounds(210, 10, 105, L1MTT.getPreferredSize().height);

          //---- L1MTD ----
          L1MTD.setComponentPopupMenu(BTD);
          L1MTD.setName("L1MTD");
          xTitledPanel1ContentContainer.add(L1MTD);
          L1MTD.setBounds(210, 40, 105, L1MTD.getPreferredSize().height);

          //---- OBJ_52 ----
          OBJ_52.setText("@L1SNS@");
          OBJ_52.setName("OBJ_52");
          xTitledPanel1ContentContainer.add(OBJ_52);
          OBJ_52.setBounds(322, 12, 145, OBJ_52.getPreferredSize().height);

          //---- OBJ_57 ----
          OBJ_57.setText("@L1DEV@");
          OBJ_57.setName("OBJ_57");
          xTitledPanel1ContentContainer.add(OBJ_57);
          OBJ_57.setBounds(322, 42, 145, OBJ_57.getPreferredSize().height);

          //---- OBJ_53 ----
          OBJ_53.setText("Quantit\u00e9");
          OBJ_53.setName("OBJ_53");
          xTitledPanel1ContentContainer.add(OBJ_53);
          OBJ_53.setBounds(552, 10, 140, 28);

          //---- OBJ_58 ----
          OBJ_58.setText("Taux");
          OBJ_58.setName("OBJ_58");
          xTitledPanel1ContentContainer.add(OBJ_58);
          OBJ_58.setBounds(552, 40, 140, 28);

          //---- L1QTE ----
          L1QTE.setComponentPopupMenu(BTD);
          L1QTE.setName("L1QTE");
          xTitledPanel1ContentContainer.add(L1QTE);
          L1QTE.setBounds(702, 10, 109, L1QTE.getPreferredSize().height);

          //---- DVCHG ----
          DVCHG.setComponentPopupMenu(BTD);
          DVCHG.setName("DVCHG");
          xTitledPanel1ContentContainer.add(DVCHG);
          DVCHG.setBounds(702, 40, 109, DVCHG.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel1ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(xTitledPanel1);
        xTitledPanel1.setBounds(10, 220, 926, 108);

        //======== xTitledPanel4 ========
        {
          xTitledPanel4.setTitle("Axes");
          xTitledPanel4.setBorder(new DropShadowBorder());
          xTitledPanel4.setName("xTitledPanel4");
          Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
          xTitledPanel4ContentContainer.setLayout(null);

          //---- L1AA1 ----
          L1AA1.setComponentPopupMenu(BTD);
          L1AA1.setName("L1AA1");
          xTitledPanel4ContentContainer.add(L1AA1);
          L1AA1.setBounds(23, 22, 54, L1AA1.getPreferredSize().height);

          //---- L1AA2 ----
          L1AA2.setComponentPopupMenu(BTD);
          L1AA2.setName("L1AA2");
          xTitledPanel4ContentContainer.add(L1AA2);
          L1AA2.setBounds(93, 22, 54, L1AA2.getPreferredSize().height);

          //---- OBJ_82 ----
          OBJ_82.setText("Type de frais");
          OBJ_82.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_82.setName("OBJ_82");
          OBJ_82.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_82ActionPerformed(e);
            }
          });
          xTitledPanel4ContentContainer.add(OBJ_82);
          OBJ_82.setBounds(23, 63, 100, 28);

          //---- LIBTYF ----
          LIBTYF.setComponentPopupMenu(BTD);
          LIBTYF.setName("LIBTYF");
          xTitledPanel4ContentContainer.add(LIBTYF);
          LIBTYF.setBounds(185, 63, 232, LIBTYF.getPreferredSize().height);

          //---- L1AA3 ----
          L1AA3.setComponentPopupMenu(BTD);
          L1AA3.setName("L1AA3");
          xTitledPanel4ContentContainer.add(L1AA3);
          L1AA3.setBounds(163, 22, 54, L1AA3.getPreferredSize().height);

          //---- L1AA4 ----
          L1AA4.setComponentPopupMenu(BTD);
          L1AA4.setName("L1AA4");
          xTitledPanel4ContentContainer.add(L1AA4);
          L1AA4.setBounds(228, 22, 54, L1AA4.getPreferredSize().height);

          //---- L1AA5 ----
          L1AA5.setComponentPopupMenu(BTD);
          L1AA5.setName("L1AA5");
          xTitledPanel4ContentContainer.add(L1AA5);
          L1AA5.setBounds(298, 22, 54, L1AA5.getPreferredSize().height);

          //---- L1AA6 ----
          L1AA6.setComponentPopupMenu(BTD);
          L1AA6.setName("L1AA6");
          xTitledPanel4ContentContainer.add(L1AA6);
          L1AA6.setBounds(363, 22, 54, L1AA6.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel4ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel4ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel4ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel4ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel4ContentContainer.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(xTitledPanel4);
        xTitledPanel4.setBounds(452, 335, 478, 135);

        //======== xTitledPanel6 ========
        {
          xTitledPanel6.setTitle("Affectation analytique");
          xTitledPanel6.setBorder(new DropShadowBorder());
          xTitledPanel6.setName("xTitledPanel6");
          Container xTitledPanel6ContentContainer = xTitledPanel6.getContentContainer();
          xTitledPanel6ContentContainer.setLayout(null);

          //---- OBJ_24 ----
          OBJ_24.setText("Section");
          OBJ_24.setName("OBJ_24");
          xTitledPanel6ContentContainer.add(OBJ_24);
          OBJ_24.setBounds(25, 25, 65, 28);

          //---- L1SAN ----
          L1SAN.setComponentPopupMenu(BTD);
          L1SAN.setName("L1SAN");
          xTitledPanel6ContentContainer.add(L1SAN);
          L1SAN.setBounds(25, 60, 40, L1SAN.getPreferredSize().height);

          //---- OBJ_71 ----
          OBJ_71.setText("Affaire");
          OBJ_71.setName("OBJ_71");
          xTitledPanel6ContentContainer.add(OBJ_71);
          OBJ_71.setBounds(163, 25, 64, 28);

          //---- L1ACT ----
          L1ACT.setComponentPopupMenu(BTD);
          L1ACT.setName("L1ACT");
          xTitledPanel6ContentContainer.add(L1ACT);
          L1ACT.setBounds(164, 60, 54, L1ACT.getPreferredSize().height);

          //---- OBJ_73 ----
          OBJ_73.setText("Nature");
          OBJ_73.setName("OBJ_73");
          xTitledPanel6ContentContainer.add(OBJ_73);
          OBJ_73.setBounds(300, 25, 63, 28);

          //---- L1NAT ----
          L1NAT.setName("L1NAT");
          xTitledPanel6ContentContainer.add(L1NAT);
          L1NAT.setBounds(301, 60, 49, L1NAT.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel6ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel6ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel6ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel6ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel6ContentContainer.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(xTitledPanel6);
        xTitledPanel6.setBounds(10, 335, 440, 135);

        //======== xTitledPanel3 ========
        {
          xTitledPanel3.setBorder(new DropShadowBorder());
          xTitledPanel3.setName("xTitledPanel3");
          Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();

          //======== SCROLLPANE_LECHEANCE ========
          {
            SCROLLPANE_LECHEANCE.setToolTipText("Page Up/Dwn \u00e9ch\u00e9ances suppl\u00e9mentaires");
            SCROLLPANE_LECHEANCE.setComponentPopupMenu(BTD);
            SCROLLPANE_LECHEANCE.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
            SCROLLPANE_LECHEANCE.setName("SCROLLPANE_LECHEANCE");

            //---- NIV01 ----
            NIV01.setRowSelectionAllowed(false);
            NIV01.setName("NIV01");
            SCROLLPANE_LECHEANCE.setViewportView(NIV01);
          }

          GroupLayout xTitledPanel3ContentContainerLayout = new GroupLayout(xTitledPanel3ContentContainer);
          xTitledPanel3ContentContainer.setLayout(xTitledPanel3ContentContainerLayout);
          xTitledPanel3ContentContainerLayout.setHorizontalGroup(
            xTitledPanel3ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(SCROLLPANE_LECHEANCE, GroupLayout.PREFERRED_SIZE, 890, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(17, Short.MAX_VALUE))
          );
          xTitledPanel3ContentContainerLayout.setVerticalGroup(
            xTitledPanel3ContentContainerLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, xTitledPanel3ContentContainerLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(SCROLLPANE_LECHEANCE, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6))
          );
        }
        p_contenu.add(xTitledPanel3);
        xTitledPanel3.setBounds(new Rectangle(new Point(10, 335), xTitledPanel3.getPreferredSize()));

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel2;
  private JLabel OBJ_27;
  private XRiTextField L1NLI;
  private XRiTextField WAFFAC;
  private JLabel OBJ_29;
  private XRiTextField L1NCG;
  private RiZoneSortie OBJ_32;
  private XRiTextField WLIBB1;
  private XRiTextField L1CLB;
  private JLabel OBJ_35;
  private XRiTextField L1NCPX;
  private XRiCalendrier L1DVBX;
  private JLabel OBJ_39;
  private XRiTextField L1RFC;
  private JLabel OBJ_41;
  private JLabel OBJ_42;
  private XRiTextField L1PCE;
  private JLabel OBJ_43;
  private XRiTextField L1RLV;
  private XRiTextField L1NCA;
  private JLabel label2;
  private JLabel label3;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_50;
  private JLabel OBJ_55;
  private XRiTextField L1MTT;
  private XRiTextField L1MTD;
  private RiZoneSortie OBJ_52;
  private RiZoneSortie OBJ_57;
  private JLabel OBJ_53;
  private JLabel OBJ_58;
  private XRiTextField L1QTE;
  private XRiTextField DVCHG;
  private JXTitledPanel xTitledPanel4;
  private XRiTextField L1AA1;
  private XRiTextField L1AA2;
  private JButton OBJ_82;
  private XRiTextField LIBTYF;
  private XRiTextField L1AA3;
  private XRiTextField L1AA4;
  private XRiTextField L1AA5;
  private XRiTextField L1AA6;
  private JXTitledPanel xTitledPanel6;
  private JLabel OBJ_24;
  private XRiTextField L1SAN;
  private JLabel OBJ_71;
  private XRiTextField L1ACT;
  private JLabel OBJ_73;
  private XRiTextField L1NAT;
  private JXTitledPanel xTitledPanel3;
  private JScrollPane SCROLLPANE_LECHEANCE;
  private XRiTable NIV01;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
