
package ri.serien.libecranrpg.vcgm.VCGM50FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Emmanuel MARCQ
 */
public class VCGM50FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM50FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDefaultButton(bouton_valider);
    
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDETA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETA@")).trim());
    LIBETA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBETA@")).trim());
    OBJ_48_OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("page @WPAGE@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  DECLARATION TVA"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    BTD = new JPopupMenu();
    OBJ_15 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_47_OBJ_47 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_48_OBJ_48 = new JLabel();
    INDETA = new RiZoneSortie();
    LIBETA = new RiZoneSortie();
    p_tete_droite = new JPanel();
    OBJ_48_OBJ_50 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    MT0102 = new XRiTextField();
    label1 = new JLabel();
    MT0101 = new XRiTextField();
    label16 = new JLabel();
    xTitledPanel3 = new JXTitledPanel();
    MT0202 = new XRiTextField();
    MT0302 = new XRiTextField();
    MT0402 = new XRiTextField();
    label8 = new JLabel();
    label10 = new JLabel();
    label11 = new JLabel();
    MT0201 = new XRiTextField();
    MT0301 = new XRiTextField();
    MT0401 = new XRiTextField();
    WTT04R = new XRiTextField();
    label17 = new JLabel();
    label18 = new JLabel();
    xTitledPanel2 = new JXTitledPanel();
    MT0502 = new XRiTextField();
    MT0602 = new XRiTextField();
    MT0702 = new XRiTextField();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    MT0802 = new XRiTextField();
    MT0902 = new XRiTextField();
    label5 = new JLabel();
    label6 = new JLabel();
    MT0501 = new XRiTextField();
    MT0601 = new XRiTextField();
    MT0701 = new XRiTextField();
    MT0801 = new XRiTextField();
    MT0901 = new XRiTextField();
    label19 = new JLabel();
    label20 = new JLabel();
    label21 = new JLabel();
    label22 = new JLabel();
    label23 = new JLabel();
    xTitledPanel4 = new JXTitledPanel();
    MT1002 = new XRiTextField();
    MT1102 = new XRiTextField();
    MT1202 = new XRiTextField();
    label12 = new JLabel();
    label13 = new JLabel();
    label14 = new JLabel();
    MT1001 = new XRiTextField();
    MT1101 = new XRiTextField();
    MT1201 = new XRiTextField();
    label24 = new JLabel();
    label25 = new JLabel();
    label26 = new JLabel();
    panel2 = new JPanel();
    MT1302 = new XRiTextField();
    MT1301 = new XRiTextField();
    label15 = new JLabel();
    FLD001 = new XRiTextField();
    OBJ_48_OBJ_49 = new JLabel();

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_15 ----
      OBJ_15.setText("Choix possibles");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);

      //---- OBJ_14 ----
      OBJ_14.setText("Aide en ligne");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("D\u00e9claration de TVA");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_47_OBJ_47 ----
          OBJ_47_OBJ_47.setText("Etablissement");
          OBJ_47_OBJ_47.setName("OBJ_47_OBJ_47");
          p_tete_gauche.add(OBJ_47_OBJ_47);
          OBJ_47_OBJ_47.setBounds(5, 5, 100, 21);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setText("@INDETB@");
          INDETB.setOpaque(false);
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(110, 3, 40, INDETB.getPreferredSize().height);

          //---- OBJ_48_OBJ_48 ----
          OBJ_48_OBJ_48.setText("Etat");
          OBJ_48_OBJ_48.setName("OBJ_48_OBJ_48");
          p_tete_gauche.add(OBJ_48_OBJ_48);
          OBJ_48_OBJ_48.setBounds(190, 5, 40, 21);

          //---- INDETA ----
          INDETA.setComponentPopupMenu(BTD);
          INDETA.setText("@INDETA@");
          INDETA.setOpaque(false);
          INDETA.setName("INDETA");
          p_tete_gauche.add(INDETA);
          INDETA.setBounds(240, 3, 64, INDETA.getPreferredSize().height);

          //---- LIBETA ----
          LIBETA.setText("@LIBETA@");
          LIBETA.setOpaque(false);
          LIBETA.setName("LIBETA");
          p_tete_gauche.add(LIBETA);
          LIBETA.setBounds(310, 3, 532, LIBETA.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- OBJ_48_OBJ_50 ----
          OBJ_48_OBJ_50.setText("page @WPAGE@");
          OBJ_48_OBJ_50.setFont(OBJ_48_OBJ_50.getFont().deriveFont(OBJ_48_OBJ_50.getFont().getStyle() | Font.BOLD, OBJ_48_OBJ_50.getFont().getSize() + 2f));
          OBJ_48_OBJ_50.setName("OBJ_48_OBJ_50");
          p_tete_droite.add(OBJ_48_OBJ_50);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Protection zones CGM");
              riSousMenu_bt6.setToolTipText("Push/Pull protection des zones g\u00e9n\u00e9r\u00e9es depuis CGM");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(980, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("A- RETENUE TVA SUR DROITS D'AUTEUR ET TVA DUE A TAUX PARTICULIER"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== xTitledPanel1 ========
            {
              xTitledPanel1.setTitle("R\u00e9gime du r\u00e9el ou du RSI - mini r\u00e9el                                                                                                                                   Base hors taxe                           TVA due");
              xTitledPanel1.setBorder(new DropShadowBorder());
              xTitledPanel1.setName("xTitledPanel1");
              Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
              xTitledPanel1ContentContainer.setLayout(null);

              //---- MT0102 ----
              MT0102.setName("MT0102");
              xTitledPanel1ContentContainer.add(MT0102);
              MT0102.setBounds(720, 5, 135, MT0102.getPreferredSize().height);

              //---- label1 ----
              label1.setText("35 - Retenue de TVA sur droits d'auteur");
              label1.setName("label1");
              xTitledPanel1ContentContainer.add(label1);
              label1.setBounds(12, 5, 448, 28);

              //---- MT0101 ----
              MT0101.setName("MT0101");
              xTitledPanel1ContentContainer.add(MT0101);
              MT0101.setBounds(575, 5, 135, MT0101.getPreferredSize().height);

              //---- label16 ----
              label16.setText("0990");
              label16.setFont(label16.getFont().deriveFont(label16.getFont().getStyle() | Font.BOLD));
              label16.setHorizontalAlignment(SwingConstants.RIGHT);
              label16.setName("label16");
              xTitledPanel1ContentContainer.add(label16);
              label16.setBounds(475, 5, 95, 25);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = xTitledPanel1ContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
                xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
              }
            }
            panel1.add(xTitledPanel1);
            xTitledPanel1.setBounds(15, 20, 905, 65);

            //======== xTitledPanel3 ========
            {
              xTitledPanel3.setTitle("Op\u00e9rations imposables en France continentale \u00e0 un taux particulier de :");
              xTitledPanel3.setBorder(new DropShadowBorder());
              xTitledPanel3.setName("xTitledPanel3");
              Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
              xTitledPanel3ContentContainer.setLayout(null);

              //---- MT0202 ----
              MT0202.setName("MT0202");
              xTitledPanel3ContentContainer.add(MT0202);
              MT0202.setBounds(720, 5, 135, MT0202.getPreferredSize().height);

              //---- MT0302 ----
              MT0302.setName("MT0302");
              xTitledPanel3ContentContainer.add(MT0302);
              MT0302.setBounds(720, 30, 135, MT0302.getPreferredSize().height);

              //---- MT0402 ----
              MT0402.setName("MT0402");
              xTitledPanel3ContentContainer.add(MT0402);
              MT0402.setBounds(720, 55, 135, MT0402.getPreferredSize().height);

              //---- label8 ----
              label8.setText("36 - Taux 2,10%");
              label8.setName("label8");
              xTitledPanel3ContentContainer.add(label8);
              label8.setBounds(12, 5, 448, 28);

              //---- label10 ----
              label10.setText("37 - Anciens taux");
              label10.setName("label10");
              xTitledPanel3ContentContainer.add(label10);
              label10.setBounds(12, 30, 448, 28);

              //---- label11 ----
              label11.setText("38 -");
              label11.setName("label11");
              xTitledPanel3ContentContainer.add(label11);
              label11.setBounds(12, 55, 23, 28);

              //---- MT0201 ----
              MT0201.setName("MT0201");
              xTitledPanel3ContentContainer.add(MT0201);
              MT0201.setBounds(575, 5, 135, MT0201.getPreferredSize().height);

              //---- MT0301 ----
              MT0301.setName("MT0301");
              xTitledPanel3ContentContainer.add(MT0301);
              MT0301.setBounds(575, 30, 135, MT0301.getPreferredSize().height);

              //---- MT0401 ----
              MT0401.setName("MT0401");
              xTitledPanel3ContentContainer.add(MT0401);
              MT0401.setBounds(575, 55, 135, MT0401.getPreferredSize().height);

              //---- WTT04R ----
              WTT04R.setName("WTT04R");
              xTitledPanel3ContentContainer.add(WTT04R);
              WTT04R.setBounds(35, 55, 420, WTT04R.getPreferredSize().height);

              //---- label17 ----
              label17.setText("1010");
              label17.setFont(label17.getFont().deriveFont(label17.getFont().getStyle() | Font.BOLD));
              label17.setHorizontalAlignment(SwingConstants.RIGHT);
              label17.setName("label17");
              xTitledPanel3ContentContainer.add(label17);
              label17.setBounds(475, 7, 95, 25);

              //---- label18 ----
              label18.setText("1020");
              label18.setFont(label18.getFont().deriveFont(label18.getFont().getStyle() | Font.BOLD));
              label18.setHorizontalAlignment(SwingConstants.RIGHT);
              label18.setName("label18");
              xTitledPanel3ContentContainer.add(label18);
              label18.setBounds(475, 32, 95, 25);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = xTitledPanel3ContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
                xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
              }
            }
            panel1.add(xTitledPanel3);
            xTitledPanel3.setBounds(15, 85, 905, 115);

            //======== xTitledPanel2 ========
            {
              xTitledPanel2.setTitle("Op\u00e9rations imposables en Corse \u00e0 un taux particulier de :");
              xTitledPanel2.setBorder(new DropShadowBorder());
              xTitledPanel2.setName("xTitledPanel2");
              Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
              xTitledPanel2ContentContainer.setLayout(null);

              //---- MT0502 ----
              MT0502.setName("MT0502");
              xTitledPanel2ContentContainer.add(MT0502);
              MT0502.setBounds(720, 5, 135, MT0502.getPreferredSize().height);

              //---- MT0602 ----
              MT0602.setName("MT0602");
              xTitledPanel2ContentContainer.add(MT0602);
              MT0602.setBounds(720, 30, 135, MT0602.getPreferredSize().height);

              //---- MT0702 ----
              MT0702.setName("MT0702");
              xTitledPanel2ContentContainer.add(MT0702);
              MT0702.setBounds(720, 55, 135, MT0702.getPreferredSize().height);

              //---- label2 ----
              label2.setText("39 - Taux 0,90%");
              label2.setName("label2");
              xTitledPanel2ContentContainer.add(label2);
              label2.setBounds(12, 5, 448, 28);

              //---- label3 ----
              label3.setText("40 - Taux 2,10%");
              label3.setName("label3");
              xTitledPanel2ContentContainer.add(label3);
              label3.setBounds(12, 30, 448, 28);

              //---- label4 ----
              label4.setText("41 - Taux  8%");
              label4.setName("label4");
              xTitledPanel2ContentContainer.add(label4);
              label4.setBounds(12, 55, 450, 28);

              //---- MT0802 ----
              MT0802.setName("MT0802");
              xTitledPanel2ContentContainer.add(MT0802);
              MT0802.setBounds(720, 80, 135, MT0802.getPreferredSize().height);

              //---- MT0902 ----
              MT0902.setName("MT0902");
              xTitledPanel2ContentContainer.add(MT0902);
              MT0902.setBounds(720, 105, 135, MT0902.getPreferredSize().height);

              //---- label5 ----
              label5.setText("42 - Taux 13%");
              label5.setName("label5");
              xTitledPanel2ContentContainer.add(label5);
              label5.setBounds(12, 80, 448, 28);

              //---- label6 ----
              label6.setText("43 - Anciens taux");
              label6.setName("label6");
              xTitledPanel2ContentContainer.add(label6);
              label6.setBounds(12, 105, 448, 28);

              //---- MT0501 ----
              MT0501.setName("MT0501");
              xTitledPanel2ContentContainer.add(MT0501);
              MT0501.setBounds(575, 5, 135, MT0501.getPreferredSize().height);

              //---- MT0601 ----
              MT0601.setName("MT0601");
              xTitledPanel2ContentContainer.add(MT0601);
              MT0601.setBounds(575, 30, 135, MT0601.getPreferredSize().height);

              //---- MT0701 ----
              MT0701.setName("MT0701");
              xTitledPanel2ContentContainer.add(MT0701);
              MT0701.setBounds(575, 55, 135, MT0701.getPreferredSize().height);

              //---- MT0801 ----
              MT0801.setName("MT0801");
              xTitledPanel2ContentContainer.add(MT0801);
              MT0801.setBounds(575, 80, 135, MT0801.getPreferredSize().height);

              //---- MT0901 ----
              MT0901.setName("MT0901");
              xTitledPanel2ContentContainer.add(MT0901);
              MT0901.setBounds(575, 105, 135, MT0901.getPreferredSize().height);

              //---- label19 ----
              label19.setText("1040");
              label19.setFont(label19.getFont().deriveFont(label19.getFont().getStyle() | Font.BOLD));
              label19.setHorizontalAlignment(SwingConstants.RIGHT);
              label19.setName("label19");
              xTitledPanel2ContentContainer.add(label19);
              label19.setBounds(475, 7, 95, 25);

              //---- label20 ----
              label20.setText("1050");
              label20.setFont(label20.getFont().deriveFont(label20.getFont().getStyle() | Font.BOLD));
              label20.setHorizontalAlignment(SwingConstants.RIGHT);
              label20.setName("label20");
              xTitledPanel2ContentContainer.add(label20);
              label20.setBounds(475, 32, 95, 25);

              //---- label21 ----
              label21.setText("1080");
              label21.setFont(label21.getFont().deriveFont(label21.getFont().getStyle() | Font.BOLD));
              label21.setHorizontalAlignment(SwingConstants.RIGHT);
              label21.setName("label21");
              xTitledPanel2ContentContainer.add(label21);
              label21.setBounds(475, 57, 95, 25);

              //---- label22 ----
              label22.setText("1090");
              label22.setFont(label22.getFont().deriveFont(label22.getFont().getStyle() | Font.BOLD));
              label22.setHorizontalAlignment(SwingConstants.RIGHT);
              label22.setName("label22");
              xTitledPanel2ContentContainer.add(label22);
              label22.setBounds(475, 82, 95, 25);

              //---- label23 ----
              label23.setText("1100");
              label23.setFont(label23.getFont().deriveFont(label23.getFont().getStyle() | Font.BOLD));
              label23.setHorizontalAlignment(SwingConstants.RIGHT);
              label23.setName("label23");
              xTitledPanel2ContentContainer.add(label23);
              label23.setBounds(475, 107, 95, 25);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = xTitledPanel2ContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
                xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
              }
            }
            panel1.add(xTitledPanel2);
            xTitledPanel2.setBounds(15, 200, 905, 165);

            //======== xTitledPanel4 ========
            {
              xTitledPanel4.setTitle("Op\u00e9rations imposables dans les DOM \u00e0 un taux particulier de :");
              xTitledPanel4.setBorder(new DropShadowBorder());
              xTitledPanel4.setName("xTitledPanel4");
              Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
              xTitledPanel4ContentContainer.setLayout(null);

              //---- MT1002 ----
              MT1002.setName("MT1002");
              xTitledPanel4ContentContainer.add(MT1002);
              MT1002.setBounds(720, 5, 135, MT1002.getPreferredSize().height);

              //---- MT1102 ----
              MT1102.setName("MT1102");
              xTitledPanel4ContentContainer.add(MT1102);
              MT1102.setBounds(720, 30, 135, MT1102.getPreferredSize().height);

              //---- MT1202 ----
              MT1202.setName("MT1202");
              xTitledPanel4ContentContainer.add(MT1202);
              MT1202.setBounds(720, 55, 135, MT1202.getPreferredSize().height);

              //---- label12 ----
              label12.setText("44 - Taux 1,05%");
              label12.setName("label12");
              xTitledPanel4ContentContainer.add(label12);
              label12.setBounds(12, 5, 448, 28);

              //---- label13 ----
              label13.setText("45 - Taux 1,75%");
              label13.setName("label13");
              xTitledPanel4ContentContainer.add(label13);
              label13.setBounds(12, 30, 448, 28);

              //---- label14 ----
              label14.setText("46 - Anciens taux");
              label14.setName("label14");
              xTitledPanel4ContentContainer.add(label14);
              label14.setBounds(12, 61, 450, 28);

              //---- MT1001 ----
              MT1001.setName("MT1001");
              xTitledPanel4ContentContainer.add(MT1001);
              MT1001.setBounds(575, 5, 135, MT1001.getPreferredSize().height);

              //---- MT1101 ----
              MT1101.setName("MT1101");
              xTitledPanel4ContentContainer.add(MT1101);
              MT1101.setBounds(575, 30, 135, MT1101.getPreferredSize().height);

              //---- MT1201 ----
              MT1201.setName("MT1201");
              xTitledPanel4ContentContainer.add(MT1201);
              MT1201.setBounds(575, 55, 135, MT1201.getPreferredSize().height);

              //---- label24 ----
              label24.setText("1020");
              label24.setFont(label24.getFont().deriveFont(label24.getFont().getStyle() | Font.BOLD));
              label24.setHorizontalAlignment(SwingConstants.RIGHT);
              label24.setName("label24");
              xTitledPanel4ContentContainer.add(label24);
              label24.setBounds(475, 55, 95, 25);

              //---- label25 ----
              label25.setText("1110");
              label25.setFont(label25.getFont().deriveFont(label25.getFont().getStyle() | Font.BOLD));
              label25.setHorizontalAlignment(SwingConstants.RIGHT);
              label25.setName("label25");
              xTitledPanel4ContentContainer.add(label25);
              label25.setBounds(475, 5, 95, 25);

              //---- label26 ----
              label26.setText("1120");
              label26.setFont(label26.getFont().deriveFont(label26.getFont().getStyle() | Font.BOLD));
              label26.setHorizontalAlignment(SwingConstants.RIGHT);
              label26.setName("label26");
              xTitledPanel4ContentContainer.add(label26);
              label26.setBounds(475, 30, 95, 25);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < xTitledPanel4ContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = xTitledPanel4ContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = xTitledPanel4ContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                xTitledPanel4ContentContainer.setMinimumSize(preferredSize);
                xTitledPanel4ContentContainer.setPreferredSize(preferredSize);
              }
            }
            panel1.add(xTitledPanel4);
            xTitledPanel4.setBounds(15, 365, 905, 115);

            //======== panel2 ========
            {
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- MT1302 ----
              MT1302.setName("MT1302");
              panel2.add(MT1302);
              MT1302.setBounds(720, 5, 135, MT1302.getPreferredSize().height);

              //---- MT1301 ----
              MT1301.setName("MT1301");
              panel2.add(MT1301);
              MT1301.setBounds(575, 5, 135, MT1301.getPreferredSize().height);

              //---- label15 ----
              label15.setText("TOTAL DES LIGNES 35 A 46");
              label15.setFont(label15.getFont().deriveFont(label15.getFont().getStyle() | Font.BOLD));
              label15.setName("label15");
              panel2.add(label15);
              label15.setBounds(15, 5, 450, 28);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel2);
            panel2.setBounds(15, 480, 905, 35);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //---- FLD001 ----
          FLD001.setName("FLD001");

          //---- OBJ_48_OBJ_49 ----
          OBJ_48_OBJ_49.setText("Aller \u00e0 la page");
          OBJ_48_OBJ_49.setName("OBJ_48_OBJ_49");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(0, 822, Short.MAX_VALUE)
                    .addComponent(OBJ_48_OBJ_49, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(FLD001, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                  .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 954, Short.MAX_VALUE))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 529, Short.MAX_VALUE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                  .addComponent(OBJ_48_OBJ_49, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addComponent(FLD001, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
          );
          p_contenuLayout.linkSize(SwingConstants.VERTICAL, new Component[] {FLD001, OBJ_48_OBJ_49});
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu BTD;
  private JMenuItem OBJ_15;
  private JMenuItem OBJ_14;
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_47_OBJ_47;
  private RiZoneSortie INDETB;
  private JLabel OBJ_48_OBJ_48;
  private RiZoneSortie INDETA;
  private RiZoneSortie LIBETA;
  private JPanel p_tete_droite;
  private JLabel OBJ_48_OBJ_50;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JXTitledPanel xTitledPanel1;
  private XRiTextField MT0102;
  private JLabel label1;
  private XRiTextField MT0101;
  private JLabel label16;
  private JXTitledPanel xTitledPanel3;
  private XRiTextField MT0202;
  private XRiTextField MT0302;
  private XRiTextField MT0402;
  private JLabel label8;
  private JLabel label10;
  private JLabel label11;
  private XRiTextField MT0201;
  private XRiTextField MT0301;
  private XRiTextField MT0401;
  private XRiTextField WTT04R;
  private JLabel label17;
  private JLabel label18;
  private JXTitledPanel xTitledPanel2;
  private XRiTextField MT0502;
  private XRiTextField MT0602;
  private XRiTextField MT0702;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private XRiTextField MT0802;
  private XRiTextField MT0902;
  private JLabel label5;
  private JLabel label6;
  private XRiTextField MT0501;
  private XRiTextField MT0601;
  private XRiTextField MT0701;
  private XRiTextField MT0801;
  private XRiTextField MT0901;
  private JLabel label19;
  private JLabel label20;
  private JLabel label21;
  private JLabel label22;
  private JLabel label23;
  private JXTitledPanel xTitledPanel4;
  private XRiTextField MT1002;
  private XRiTextField MT1102;
  private XRiTextField MT1202;
  private JLabel label12;
  private JLabel label13;
  private JLabel label14;
  private XRiTextField MT1001;
  private XRiTextField MT1101;
  private XRiTextField MT1201;
  private JLabel label24;
  private JLabel label25;
  private JLabel label26;
  private JPanel panel2;
  private XRiTextField MT1302;
  private XRiTextField MT1301;
  private JLabel label15;
  private XRiTextField FLD001;
  private JLabel OBJ_48_OBJ_49;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
