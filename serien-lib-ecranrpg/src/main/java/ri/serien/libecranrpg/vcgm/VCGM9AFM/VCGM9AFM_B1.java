
package ri.serien.libecranrpg.vcgm.VCGM9AFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM9AFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VCGM9AFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBHIE@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_134.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBTYP@")).trim());
    OBJ_28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB03@")).trim());
    OBJ_97.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB01@")).trim());
    OBJ_98.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB02@")).trim());
    OBJ_100.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB04@")).trim());
    OBJ_101.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB05@")).trim());
    OBJ_102.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB06@")).trim());
    OBJ_103.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB07@")).trim());
    OBJ_104.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB08@")).trim());
    OBJ_106.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB09@")).trim());
    OBJ_107.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB10@")).trim());
    OBJ_108.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB11@")).trim());
    OBJ_109.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB12@")).trim());
    OBJ_110.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB13@")).trim());
    OBJ_111.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB14@")).trim());
    OBJ_112.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB15@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    xTitledPanel1.setVisible(lexique.isTrue("N60"));
    xTitledPanel2.setVisible(lexique.isTrue("60"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  @TITRE@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDSOC.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(INDSOC.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche2 = new JPanel();
    OBJ_23 = new JLabel();
    INDSOC = new XRiTextField();
    OBJ_42 = new JLabel();
    INDHIE = new XRiTextField();
    INDNIV = new XRiTextField();
    INDGAN = new XRiTextField();
    OBJ_33 = new RiZoneSortie();
    p_tete_gauche = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    Panel1 = new JPanel();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_134 = new RiZoneSortie();
    GATYP = new XRiTextField();
    OBJ_47 = new JLabel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_28 = new RiZoneSortie();
    OBJ_97 = new RiZoneSortie();
    OBJ_98 = new RiZoneSortie();
    OBJ_100 = new RiZoneSortie();
    OBJ_101 = new RiZoneSortie();
    OBJ_102 = new RiZoneSortie();
    OBJ_103 = new RiZoneSortie();
    OBJ_104 = new RiZoneSortie();
    OBJ_106 = new RiZoneSortie();
    OBJ_107 = new RiZoneSortie();
    OBJ_108 = new RiZoneSortie();
    OBJ_109 = new RiZoneSortie();
    OBJ_110 = new RiZoneSortie();
    OBJ_111 = new RiZoneSortie();
    OBJ_112 = new RiZoneSortie();
    OBJ_95 = new JLabel();
    GACX04 = new XRiTextField();
    GACX01 = new XRiTextField();
    GACX02 = new XRiTextField();
    GACX03 = new XRiTextField();
    GACX05 = new XRiTextField();
    GACX06 = new XRiTextField();
    GACX07 = new XRiTextField();
    GACX08 = new XRiTextField();
    GACX09 = new XRiTextField();
    GACX10 = new XRiTextField();
    GACX11 = new XRiTextField();
    GACX12 = new XRiTextField();
    GACX13 = new XRiTextField();
    GACX14 = new XRiTextField();
    GACX15 = new XRiTextField();
    OBJ_94 = new JLabel();
    GANV01 = new XRiTextField();
    GANV02 = new XRiTextField();
    GANV03 = new XRiTextField();
    GANV04 = new XRiTextField();
    GANV05 = new XRiTextField();
    GANV06 = new XRiTextField();
    GANV07 = new XRiTextField();
    GANV08 = new XRiTextField();
    GANV09 = new XRiTextField();
    GANV10 = new XRiTextField();
    GANV11 = new XRiTextField();
    GANV12 = new XRiTextField();
    GANV13 = new XRiTextField();
    GANV14 = new XRiTextField();
    GANV15 = new XRiTextField();
    OBJ_118 = new JLabel();
    OBJ_119 = new JLabel();
    OBJ_120 = new JLabel();
    OBJ_121 = new JLabel();
    OBJ_122 = new JLabel();
    OBJ_123 = new JLabel();
    OBJ_124 = new JLabel();
    OBJ_125 = new JLabel();
    OBJ_126 = new JLabel();
    OBJ_127 = new JLabel();
    OBJ_128 = new JLabel();
    OBJ_129 = new JLabel();
    OBJ_130 = new JLabel();
    OBJ_131 = new JLabel();
    OBJ_132 = new JLabel();
    OBJ_46 = new JLabel();
    GALIB = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche2 ========
        {
          p_tete_gauche2.setOpaque(false);
          p_tete_gauche2.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche2.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche2.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche2.setName("p_tete_gauche2");

          //---- OBJ_23 ----
          OBJ_23.setText("Soci\u00e9t\u00e9");
          OBJ_23.setName("OBJ_23");

          //---- INDSOC ----
          INDSOC.setComponentPopupMenu(BTD);
          INDSOC.setName("INDSOC");

          //---- OBJ_42 ----
          OBJ_42.setText("Code groupe");
          OBJ_42.setName("OBJ_42");

          //---- INDHIE ----
          INDHIE.setComponentPopupMenu(BTD);
          INDHIE.setName("INDHIE");

          //---- INDNIV ----
          INDNIV.setComponentPopupMenu(BTD);
          INDNIV.setName("INDNIV");

          //---- INDGAN ----
          INDGAN.setComponentPopupMenu(BTD);
          INDGAN.setName("INDGAN");

          //---- OBJ_33 ----
          OBJ_33.setText("@LIBHIE@");
          OBJ_33.setOpaque(false);
          OBJ_33.setName("OBJ_33");

          GroupLayout p_tete_gauche2Layout = new GroupLayout(p_tete_gauche2);
          p_tete_gauche2.setLayout(p_tete_gauche2Layout);
          p_tete_gauche2Layout.setHorizontalGroup(
            p_tete_gauche2Layout.createParallelGroup()
              .addGroup(p_tete_gauche2Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_23, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDSOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDHIE, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INDNIV, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INDGAN, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 232, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gauche2Layout.setVerticalGroup(
            p_tete_gauche2Layout.createParallelGroup()
              .addGroup(p_tete_gauche2Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_23, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDSOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gauche2Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDHIE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDNIV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDGAN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gauche2Layout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche2);

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(810, 560));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //======== Panel1 ========
          {
            Panel1.setOpaque(false);
            Panel1.setName("Panel1");
            Panel1.setLayout(null);

            //======== xTitledPanel2 ========
            {
              xTitledPanel2.setTitle("Groupe analytique");
              xTitledPanel2.setBorder(new DropShadowBorder());
              xTitledPanel2.setName("xTitledPanel2");
              Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
              xTitledPanel2ContentContainer.setLayout(null);

              //---- OBJ_134 ----
              OBJ_134.setText("@LIBTYP@");
              OBJ_134.setName("OBJ_134");
              xTitledPanel2ContentContainer.add(OBJ_134);
              OBJ_134.setBounds(190, 27, 280, OBJ_134.getPreferredSize().height);

              //---- GATYP ----
              GATYP.setComponentPopupMenu(BTD);
              GATYP.setName("GATYP");
              xTitledPanel2ContentContainer.add(GATYP);
              GATYP.setBounds(155, 25, 25, GATYP.getPreferredSize().height);

              //---- OBJ_47 ----
              OBJ_47.setText("Type hi\u00e9rarchie");
              OBJ_47.setName("OBJ_47");
              xTitledPanel2ContentContainer.add(OBJ_47);
              OBJ_47.setBounds(45, 25, 98, 28);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = xTitledPanel2ContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
                xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
              }
            }
            Panel1.add(xTitledPanel2);
            xTitledPanel2.setBounds(95, 100, 515, 110);

            //======== xTitledPanel1 ========
            {
              xTitledPanel1.setTitle("Groupe analytique");
              xTitledPanel1.setBorder(new DropShadowBorder());
              xTitledPanel1.setPreferredSize(new Dimension(765, 465));
              xTitledPanel1.setName("xTitledPanel1");
              Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

              //---- OBJ_28 ----
              OBJ_28.setText("@LIB03@");
              OBJ_28.setName("OBJ_28");

              //---- OBJ_97 ----
              OBJ_97.setText("@LIB01@");
              OBJ_97.setName("OBJ_97");

              //---- OBJ_98 ----
              OBJ_98.setText("@LIB02@");
              OBJ_98.setName("OBJ_98");

              //---- OBJ_100 ----
              OBJ_100.setText("@LIB04@");
              OBJ_100.setName("OBJ_100");

              //---- OBJ_101 ----
              OBJ_101.setText("@LIB05@");
              OBJ_101.setName("OBJ_101");

              //---- OBJ_102 ----
              OBJ_102.setText("@LIB06@");
              OBJ_102.setName("OBJ_102");

              //---- OBJ_103 ----
              OBJ_103.setText("@LIB07@");
              OBJ_103.setName("OBJ_103");

              //---- OBJ_104 ----
              OBJ_104.setText("@LIB08@");
              OBJ_104.setName("OBJ_104");

              //---- OBJ_106 ----
              OBJ_106.setText("@LIB09@");
              OBJ_106.setName("OBJ_106");

              //---- OBJ_107 ----
              OBJ_107.setText("@LIB10@");
              OBJ_107.setName("OBJ_107");

              //---- OBJ_108 ----
              OBJ_108.setText("@LIB11@");
              OBJ_108.setName("OBJ_108");

              //---- OBJ_109 ----
              OBJ_109.setText("@LIB12@");
              OBJ_109.setName("OBJ_109");

              //---- OBJ_110 ----
              OBJ_110.setText("@LIB13@");
              OBJ_110.setName("OBJ_110");

              //---- OBJ_111 ----
              OBJ_111.setText("@LIB14@");
              OBJ_111.setName("OBJ_111");

              //---- OBJ_112 ----
              OBJ_112.setText("@LIB15@");
              OBJ_112.setName("OBJ_112");

              //---- OBJ_95 ----
              OBJ_95.setText("Code");
              OBJ_95.setName("OBJ_95");

              //---- GACX04 ----
              GACX04.setComponentPopupMenu(BTD);
              GACX04.setName("GACX04");

              //---- GACX01 ----
              GACX01.setComponentPopupMenu(BTD);
              GACX01.setName("GACX01");

              //---- GACX02 ----
              GACX02.setComponentPopupMenu(BTD);
              GACX02.setName("GACX02");

              //---- GACX03 ----
              GACX03.setComponentPopupMenu(BTD);
              GACX03.setName("GACX03");

              //---- GACX05 ----
              GACX05.setComponentPopupMenu(BTD);
              GACX05.setName("GACX05");

              //---- GACX06 ----
              GACX06.setComponentPopupMenu(BTD);
              GACX06.setName("GACX06");

              //---- GACX07 ----
              GACX07.setComponentPopupMenu(BTD);
              GACX07.setName("GACX07");

              //---- GACX08 ----
              GACX08.setComponentPopupMenu(BTD);
              GACX08.setName("GACX08");

              //---- GACX09 ----
              GACX09.setComponentPopupMenu(BTD);
              GACX09.setName("GACX09");

              //---- GACX10 ----
              GACX10.setComponentPopupMenu(BTD);
              GACX10.setName("GACX10");

              //---- GACX11 ----
              GACX11.setComponentPopupMenu(BTD);
              GACX11.setName("GACX11");

              //---- GACX12 ----
              GACX12.setComponentPopupMenu(BTD);
              GACX12.setName("GACX12");

              //---- GACX13 ----
              GACX13.setComponentPopupMenu(BTD);
              GACX13.setName("GACX13");

              //---- GACX14 ----
              GACX14.setComponentPopupMenu(BTD);
              GACX14.setName("GACX14");

              //---- GACX15 ----
              GACX15.setComponentPopupMenu(BTD);
              GACX15.setName("GACX15");

              //---- OBJ_94 ----
              OBJ_94.setText("Niv.");
              OBJ_94.setName("OBJ_94");

              //---- GANV01 ----
              GANV01.setComponentPopupMenu(BTD);
              GANV01.setName("GANV01");

              //---- GANV02 ----
              GANV02.setComponentPopupMenu(BTD);
              GANV02.setName("GANV02");

              //---- GANV03 ----
              GANV03.setComponentPopupMenu(BTD);
              GANV03.setName("GANV03");

              //---- GANV04 ----
              GANV04.setComponentPopupMenu(BTD);
              GANV04.setName("GANV04");

              //---- GANV05 ----
              GANV05.setComponentPopupMenu(BTD);
              GANV05.setName("GANV05");

              //---- GANV06 ----
              GANV06.setComponentPopupMenu(BTD);
              GANV06.setName("GANV06");

              //---- GANV07 ----
              GANV07.setComponentPopupMenu(BTD);
              GANV07.setName("GANV07");

              //---- GANV08 ----
              GANV08.setComponentPopupMenu(BTD);
              GANV08.setName("GANV08");

              //---- GANV09 ----
              GANV09.setComponentPopupMenu(BTD);
              GANV09.setName("GANV09");

              //---- GANV10 ----
              GANV10.setComponentPopupMenu(BTD);
              GANV10.setName("GANV10");

              //---- GANV11 ----
              GANV11.setComponentPopupMenu(BTD);
              GANV11.setName("GANV11");

              //---- GANV12 ----
              GANV12.setComponentPopupMenu(BTD);
              GANV12.setName("GANV12");

              //---- GANV13 ----
              GANV13.setComponentPopupMenu(BTD);
              GANV13.setName("GANV13");

              //---- GANV14 ----
              GANV14.setComponentPopupMenu(BTD);
              GANV14.setName("GANV14");

              //---- GANV15 ----
              GANV15.setComponentPopupMenu(BTD);
              GANV15.setName("GANV15");

              //---- OBJ_118 ----
              OBJ_118.setText("01");
              OBJ_118.setName("OBJ_118");

              //---- OBJ_119 ----
              OBJ_119.setText("02");
              OBJ_119.setName("OBJ_119");

              //---- OBJ_120 ----
              OBJ_120.setText("03");
              OBJ_120.setName("OBJ_120");

              //---- OBJ_121 ----
              OBJ_121.setText("04");
              OBJ_121.setName("OBJ_121");

              //---- OBJ_122 ----
              OBJ_122.setText("05");
              OBJ_122.setName("OBJ_122");

              //---- OBJ_123 ----
              OBJ_123.setText("06");
              OBJ_123.setName("OBJ_123");

              //---- OBJ_124 ----
              OBJ_124.setText("07");
              OBJ_124.setName("OBJ_124");

              //---- OBJ_125 ----
              OBJ_125.setText("08");
              OBJ_125.setName("OBJ_125");

              //---- OBJ_126 ----
              OBJ_126.setText("09");
              OBJ_126.setName("OBJ_126");

              //---- OBJ_127 ----
              OBJ_127.setText("10");
              OBJ_127.setName("OBJ_127");

              //---- OBJ_128 ----
              OBJ_128.setText("11");
              OBJ_128.setName("OBJ_128");

              //---- OBJ_129 ----
              OBJ_129.setText("12");
              OBJ_129.setName("OBJ_129");

              //---- OBJ_130 ----
              OBJ_130.setText("13");
              OBJ_130.setName("OBJ_130");

              //---- OBJ_131 ----
              OBJ_131.setText("14");
              OBJ_131.setName("OBJ_131");

              //---- OBJ_132 ----
              OBJ_132.setText("15");
              OBJ_132.setName("OBJ_132");

              GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
              xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
              xTitledPanel1ContentContainerLayout.setHorizontalGroup(
                xTitledPanel1ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(245, 245, 245)
                        .addComponent(OBJ_94)
                        .addGap(10, 10, 10)
                        .addComponent(OBJ_95, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(215, 215, 215)
                        .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                          .addComponent(OBJ_118, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                          .addComponent(OBJ_119, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                          .addComponent(OBJ_120, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                          .addComponent(OBJ_121, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                          .addComponent(OBJ_122, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                          .addComponent(OBJ_123, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                          .addComponent(OBJ_125, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                          .addComponent(OBJ_126, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                          .addComponent(OBJ_127, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                          .addComponent(OBJ_128, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                          .addComponent(OBJ_129, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                          .addComponent(OBJ_131, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                          .addComponent(OBJ_132, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                        .addGap(12, 12, 12)
                        .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                          .addComponent(GANV03, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(GANV02, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(GANV07, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(GANV09, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(GANV10, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(GANV12, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(GANV14, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(GANV11, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(GANV06, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(GANV08, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(GANV05, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(GANV15, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(GANV13, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                          .addComponent(GACX01, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(GACX03, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(GACX05, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(GACX11, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(GACX08, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(GACX10, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(GACX07, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(GACX12, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(GACX09, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addComponent(GACX14, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                          .addComponent(OBJ_97, GroupLayout.PREFERRED_SIZE, 232, GroupLayout.PREFERRED_SIZE)
                          .addComponent(OBJ_98, GroupLayout.PREFERRED_SIZE, 232, GroupLayout.PREFERRED_SIZE)
                          .addComponent(OBJ_100, GroupLayout.PREFERRED_SIZE, 232, GroupLayout.PREFERRED_SIZE)
                          .addComponent(OBJ_102, GroupLayout.PREFERRED_SIZE, 232, GroupLayout.PREFERRED_SIZE)
                          .addComponent(OBJ_104, GroupLayout.PREFERRED_SIZE, 232, GroupLayout.PREFERRED_SIZE)
                          .addComponent(OBJ_106, GroupLayout.PREFERRED_SIZE, 232, GroupLayout.PREFERRED_SIZE)
                          .addComponent(OBJ_107, GroupLayout.PREFERRED_SIZE, 232, GroupLayout.PREFERRED_SIZE)
                          .addComponent(OBJ_108, GroupLayout.PREFERRED_SIZE, 232, GroupLayout.PREFERRED_SIZE)
                          .addComponent(OBJ_109, GroupLayout.PREFERRED_SIZE, 232, GroupLayout.PREFERRED_SIZE)
                          .addComponent(OBJ_110, GroupLayout.PREFERRED_SIZE, 232, GroupLayout.PREFERRED_SIZE)
                          .addComponent(OBJ_111, GroupLayout.PREFERRED_SIZE, 232, GroupLayout.PREFERRED_SIZE)
                          .addComponent(OBJ_112, GroupLayout.PREFERRED_SIZE, 232, GroupLayout.PREFERRED_SIZE)))
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(215, 215, 215)
                        .addComponent(OBJ_124, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(335, 335, 335)
                        .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 232, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(275, 275, 275)
                        .addComponent(GACX15, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(215, 215, 215)
                        .addComponent(OBJ_130, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(275, 275, 275)
                        .addComponent(GACX04, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(335, 335, 335)
                        .addComponent(OBJ_101, GroupLayout.PREFERRED_SIZE, 232, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(275, 275, 275)
                        .addComponent(GACX02, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(245, 245, 245)
                        .addComponent(GANV01, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(245, 245, 245)
                        .addComponent(GANV04, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(275, 275, 275)
                        .addComponent(GACX06, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(335, 335, 335)
                        .addComponent(OBJ_103, GroupLayout.PREFERRED_SIZE, 232, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(275, 275, 275)
                        .addComponent(GACX13, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)))
                    .addGap(193, 193, 193))
              );
              xTitledPanel1ContentContainerLayout.setVerticalGroup(
                xTitledPanel1ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                      .addComponent(OBJ_94, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_95, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGap(1, 1, 1)
                    .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(OBJ_118, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(OBJ_119, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(OBJ_120, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(OBJ_121, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(OBJ_122, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(OBJ_123, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addGap(32, 32, 32)
                        .addComponent(OBJ_125, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(OBJ_126, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(OBJ_127, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(OBJ_128, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(OBJ_129, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addGap(32, 32, 32)
                        .addComponent(OBJ_131, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(OBJ_132, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                          .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                            .addGap(26, 26, 26)
                            .addComponent(GANV03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addComponent(GANV02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGap(24, 24, 24)
                        .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                          .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                            .addGap(52, 52, 52)
                            .addComponent(GANV07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                            .addGap(104, 104, 104)
                            .addComponent(GANV09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                            .addGap(130, 130, 130)
                            .addComponent(GANV10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                            .addGap(182, 182, 182)
                            .addComponent(GANV12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                            .addGap(234, 234, 234)
                            .addComponent(GANV14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                            .addGap(156, 156, 156)
                            .addComponent(GANV11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                            .addGap(26, 26, 26)
                            .addComponent(GANV06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                            .addGap(78, 78, 78)
                            .addComponent(GANV08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addComponent(GANV05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                            .addGap(260, 260, 260)
                            .addComponent(GANV15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                            .addGap(208, 208, 208)
                            .addComponent(GANV13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addComponent(GACX01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(24, 24, 24)
                        .addComponent(GACX03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(24, 24, 24)
                        .addComponent(GACX05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(24, 24, 24)
                        .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                          .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                            .addGap(104, 104, 104)
                            .addComponent(GACX11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                            .addGap(26, 26, 26)
                            .addComponent(GACX08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                            .addGap(78, 78, 78)
                            .addComponent(GACX10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addComponent(GACX07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                            .addGap(130, 130, 130)
                            .addComponent(GACX12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                            .addGap(52, 52, 52)
                            .addComponent(GACX09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                        .addGap(24, 24, 24)
                        .addComponent(GACX14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(OBJ_97, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addComponent(OBJ_98, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(28, 28, 28)
                        .addComponent(OBJ_100, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(28, 28, 28)
                        .addComponent(OBJ_102, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(28, 28, 28)
                        .addComponent(OBJ_104, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addComponent(OBJ_106, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addComponent(OBJ_107, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addComponent(OBJ_108, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addComponent(OBJ_109, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addComponent(OBJ_110, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addComponent(OBJ_111, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addComponent(OBJ_112, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(160, 160, 160)
                        .addComponent(OBJ_124, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(364, 364, 364)
                        .addComponent(GACX15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(316, 316, 316)
                        .addComponent(OBJ_130, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(78, 78, 78)
                        .addComponent(GACX04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(106, 106, 106)
                        .addComponent(OBJ_101, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(GACX02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addComponent(GANV01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(78, 78, 78)
                        .addComponent(GANV04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(130, 130, 130)
                        .addComponent(GACX06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(158, 158, 158)
                        .addComponent(OBJ_103, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                        .addGap(312, 312, 312)
                        .addComponent(GACX13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
              );
            }
            Panel1.add(xTitledPanel1);
            xTitledPanel1.setBounds(15, 55, xTitledPanel1.getPreferredSize().width, 465);

            //---- OBJ_46 ----
            OBJ_46.setText("Libell\u00e9");
            OBJ_46.setName("OBJ_46");
            Panel1.add(OBJ_46);
            OBJ_46.setBounds(95, 10, 130, 28);

            //---- GALIB ----
            GALIB.setComponentPopupMenu(BTD);
            GALIB.setName("GALIB");
            Panel1.add(GALIB);
            GALIB.setBounds(250, 10, 310, GALIB.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < Panel1.getComponentCount(); i++) {
                Rectangle bounds = Panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = Panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              Panel1.setMinimumSize(preferredSize);
              Panel1.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(Panel1);
          Panel1.setBounds(13, 14, 784, 532);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche2;
  private JLabel OBJ_23;
  private XRiTextField INDSOC;
  private JLabel OBJ_42;
  private XRiTextField INDHIE;
  private XRiTextField INDNIV;
  private XRiTextField INDGAN;
  private RiZoneSortie OBJ_33;
  private JPanel p_tete_gauche;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel Panel1;
  private JXTitledPanel xTitledPanel2;
  private RiZoneSortie OBJ_134;
  private XRiTextField GATYP;
  private JLabel OBJ_47;
  private JXTitledPanel xTitledPanel1;
  private RiZoneSortie OBJ_28;
  private RiZoneSortie OBJ_97;
  private RiZoneSortie OBJ_98;
  private RiZoneSortie OBJ_100;
  private RiZoneSortie OBJ_101;
  private RiZoneSortie OBJ_102;
  private RiZoneSortie OBJ_103;
  private RiZoneSortie OBJ_104;
  private RiZoneSortie OBJ_106;
  private RiZoneSortie OBJ_107;
  private RiZoneSortie OBJ_108;
  private RiZoneSortie OBJ_109;
  private RiZoneSortie OBJ_110;
  private RiZoneSortie OBJ_111;
  private RiZoneSortie OBJ_112;
  private JLabel OBJ_95;
  private XRiTextField GACX04;
  private XRiTextField GACX01;
  private XRiTextField GACX02;
  private XRiTextField GACX03;
  private XRiTextField GACX05;
  private XRiTextField GACX06;
  private XRiTextField GACX07;
  private XRiTextField GACX08;
  private XRiTextField GACX09;
  private XRiTextField GACX10;
  private XRiTextField GACX11;
  private XRiTextField GACX12;
  private XRiTextField GACX13;
  private XRiTextField GACX14;
  private XRiTextField GACX15;
  private JLabel OBJ_94;
  private XRiTextField GANV01;
  private XRiTextField GANV02;
  private XRiTextField GANV03;
  private XRiTextField GANV04;
  private XRiTextField GANV05;
  private XRiTextField GANV06;
  private XRiTextField GANV07;
  private XRiTextField GANV08;
  private XRiTextField GANV09;
  private XRiTextField GANV10;
  private XRiTextField GANV11;
  private XRiTextField GANV12;
  private XRiTextField GANV13;
  private XRiTextField GANV14;
  private XRiTextField GANV15;
  private JLabel OBJ_118;
  private JLabel OBJ_119;
  private JLabel OBJ_120;
  private JLabel OBJ_121;
  private JLabel OBJ_122;
  private JLabel OBJ_123;
  private JLabel OBJ_124;
  private JLabel OBJ_125;
  private JLabel OBJ_126;
  private JLabel OBJ_127;
  private JLabel OBJ_128;
  private JLabel OBJ_129;
  private JLabel OBJ_130;
  private JLabel OBJ_131;
  private JLabel OBJ_132;
  private JLabel OBJ_46;
  private XRiTextField GALIB;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
