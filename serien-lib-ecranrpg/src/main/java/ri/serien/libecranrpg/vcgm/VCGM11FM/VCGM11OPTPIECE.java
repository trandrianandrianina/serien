/*
 * Created by JFormDesigner on Fri Oct 02 14:20:32 CEST 2009
 */

package ri.serien.libecranrpg.vcgm.VCGM11FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;

/**
 * @author Stéphane Vénéri
 */
public class VCGM11OPTPIECE extends SNPanelEcranRPG implements ioFrame {
   
  
  public VCGM11OPTPIECE(SNPanelEcranRPG parent) {
    super(parent);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    initDiverses();
    WEPTYP.setValeurs("1", WEPTYP_GRP);
    WEPTYP_B.setValeurs("2");
    WEPTYP_C.setValeurs(" ");
    
    
    bouton_valider.setIcon(lexique.chargerImage("images/ok_p.png", true));
    bouton_retour.setIcon(lexique.chargerImage("images/retour_p.png", true));
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  @Override
  public void dispose() {
    getJDialog().dispose();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    this.getData();
    closePopupLinkWithBuffer(false);
    
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    closePopupLinkWithBuffer(true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    WEPTYP = new XRiRadioButton();
    WEPTYP_B = new XRiRadioButton();
    WEPTYP_C = new XRiRadioButton();
    WEPTYP_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(890, 140));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- WEPTYP ----
          WEPTYP.setText("Recherche de la derni\u00e8re pi\u00e8ce saisie et affichage avec toutes les zones renseign\u00e9es.");
          WEPTYP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WEPTYP.setName("WEPTYP");
          panel1.add(WEPTYP);
          WEPTYP.setBounds(10, 10, 650, 20);

          //---- WEPTYP_B ----
          WEPTYP_B.setText("Recherche de la derni\u00e8re pi\u00e8ce saisie et affichage avec toutes les zones renseign\u00e9es \u00e0 l'exception du montant.");
          WEPTYP_B.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WEPTYP_B.setName("WEPTYP_B");
          panel1.add(WEPTYP_B);
          WEPTYP_B.setBounds(10, 35, 650, 20);

          //---- WEPTYP_C ----
          WEPTYP_C.setText("Aucun renseignement");
          WEPTYP_C.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WEPTYP_C.setName("WEPTYP_C");
          panel1.add(WEPTYP_C);
          WEPTYP_C.setBounds(10, 60, 650, 20);
        }
        p_contenu.add(panel1);
        panel1.setBounds(15, 15, 685, 100);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //---- WEPTYP_GRP ----
    WEPTYP_GRP.add(WEPTYP);
    WEPTYP_GRP.add(WEPTYP_B);
    WEPTYP_GRP.add(WEPTYP_C);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiRadioButton WEPTYP;
  private XRiRadioButton WEPTYP_B;
  private XRiRadioButton WEPTYP_C;
  private ButtonGroup WEPTYP_GRP;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
