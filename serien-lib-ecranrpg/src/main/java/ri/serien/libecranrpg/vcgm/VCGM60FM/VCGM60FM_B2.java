
package ri.serien.libecranrpg.vcgm.VCGM60FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.LayoutStyle;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM60FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM60FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WPTTOU.setValeursSelection("X", " ");
    WPT14.setValeursSelection("X", " ");
    WPT13.setValeursSelection("X", " ");
    WPT11.setValeursSelection("X", " ");
    WPT10.setValeursSelection("X", " ");
    WPT9.setValeursSelection("X", " ");
    WPT8.setValeursSelection("X", " ");
    WPT7.setValeursSelection("X", " ");
    WPT6.setValeursSelection("X", " ");
    WPT5.setValeursSelection("X", " ");
    WPT4.setValeursSelection("X", " ");
    WPT3.setValeursSelection("X", " ");
    WPT2.setValeursSelection("X", " ");
    WPT1.setValeursSelection("X", " ");
    WPT12.setValeursSelection("X", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WPT12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("12   @LIB12@")).trim());
    WPT1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("01   @LIB01@")).trim());
    WPT2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("02   @LIB02@")).trim());
    WPT3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("03   @LIB03@")).trim());
    WPT4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("04   @LIB04@")).trim());
    WPT5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("05   @LIB05@")).trim());
    WPT6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("06   @LIB06@")).trim());
    WPT7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("07   @LIB07@")).trim());
    WPT8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("08   @LIB08@")).trim());
    WPT9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("09   @LIB09@")).trim());
    WPT10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("10   @LIB10@")).trim());
    WPT11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("11   @LIB11@")).trim());
    WPT13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("13   @LIB13@")).trim());
    WPT14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("14   @LIB14@")).trim());
    OBJ_39.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DTG01@")).trim());
    OBJ_40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DTG02@")).trim());
    OBJ_41.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DTG03@")).trim());
    OBJ_42.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DTG04@")).trim());
    OBJ_43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DTG05@")).trim());
    OBJ_44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DTG06@")).trim());
    OBJ_45.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DTG07@")).trim());
    OBJ_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DTG08@")).trim());
    OBJ_47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DTG09@")).trim());
    OBJ_48.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DTG10@")).trim());
    OBJ_49.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DTG11@")).trim());
    OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DTG12@")).trim());
    OBJ_51.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DTG13@")).trim());
    OBJ_52.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DTG14@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    // WPTTOU.setSelected(lexique.HostFieldGetData("WPTTOU").equalsIgnoreCase("X"));
    // WPT14.setSelected(lexique.HostFieldGetData("WPT14").equalsIgnoreCase("X"));
    // WPT13.setSelected(lexique.HostFieldGetData("WPT13").equalsIgnoreCase("X"));
    // WPT11.setSelected(lexique.HostFieldGetData("WPT11").equalsIgnoreCase("X"));
    // WPT10.setSelected(lexique.HostFieldGetData("WPT10").equalsIgnoreCase("X"));
    // WPT9.setSelected(lexique.HostFieldGetData("WPT9").equalsIgnoreCase("X"));
    // WPT8.setSelected(lexique.HostFieldGetData("WPT8").equalsIgnoreCase("X"));
    // WPT7.setSelected(lexique.HostFieldGetData("WPT7").equalsIgnoreCase("X"));
    // WPT6.setSelected(lexique.HostFieldGetData("WPT6").equalsIgnoreCase("X"));
    // WPT5.setSelected(lexique.HostFieldGetData("WPT5").equalsIgnoreCase("X"));
    // WPT4.setSelected(lexique.HostFieldGetData("WPT4").equalsIgnoreCase("X"));
    // WPT3.setSelected(lexique.HostFieldGetData("WPT3").equalsIgnoreCase("X"));
    // WPT2.setSelected(lexique.HostFieldGetData("WPT2").equalsIgnoreCase("X"));
    // WPT1.setSelected(lexique.HostFieldGetData("WPT1").equalsIgnoreCase("X"));
    // WPT12.setSelected(lexique.HostFieldGetData("WPT12").equalsIgnoreCase("X"));
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Regénération"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WPTTOU.isSelected())
    // lexique.HostFieldPutData("WPTTOU", 0, "X");
    // else
    // lexique.HostFieldPutData("WPTTOU", 0, " ");
    // if (WPT14.isSelected())
    // lexique.HostFieldPutData("WPT14", 0, "X");
    // else
    // lexique.HostFieldPutData("WPT14", 0, " ");
    // if (WPT13.isSelected())
    // lexique.HostFieldPutData("WPT13", 0, "X");
    // else
    // lexique.HostFieldPutData("WPT13", 0, " ");
    // if (WPT11.isSelected())
    // lexique.HostFieldPutData("WPT11", 0, "X");
    // else
    // lexique.HostFieldPutData("WPT11", 0, " ");
    // if (WPT10.isSelected())
    // lexique.HostFieldPutData("WPT10", 0, "X");
    // else
    // lexique.HostFieldPutData("WPT10", 0, " ");
    // if (WPT9.isSelected())
    // lexique.HostFieldPutData("WPT9", 0, "X");
    // else
    // lexique.HostFieldPutData("WPT9", 0, " ");
    // if (WPT8.isSelected())
    // lexique.HostFieldPutData("WPT8", 0, "X");
    // else
    // lexique.HostFieldPutData("WPT8", 0, " ");
    // if (WPT7.isSelected())
    // lexique.HostFieldPutData("WPT7", 0, "X");
    // else
    // lexique.HostFieldPutData("WPT7", 0, " ");
    // if (WPT6.isSelected())
    // lexique.HostFieldPutData("WPT6", 0, "X");
    // else
    // lexique.HostFieldPutData("WPT6", 0, " ");
    // if (WPT5.isSelected())
    // lexique.HostFieldPutData("WPT5", 0, "X");
    // else
    // lexique.HostFieldPutData("WPT5", 0, " ");
    // if (WPT4.isSelected())
    // lexique.HostFieldPutData("WPT4", 0, "X");
    // else
    // lexique.HostFieldPutData("WPT4", 0, " ");
    // if (WPT3.isSelected())
    // lexique.HostFieldPutData("WPT3", 0, "X");
    // else
    // lexique.HostFieldPutData("WPT3", 0, " ");
    // if (WPT2.isSelected())
    // lexique.HostFieldPutData("WPT2", 0, "X");
    // else
    // lexique.HostFieldPutData("WPT2", 0, " ");
    // if (WPT1.isSelected())
    // lexique.HostFieldPutData("WPT1", 0, "X");
    // else
    // lexique.HostFieldPutData("WPT1", 0, " ");
    // if (WPT12.isSelected())
    // lexique.HostFieldPutData("WPT12", 0, "X");
    // else
    // lexique.HostFieldPutData("WPT12", 0, " ");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    WPT12 = new XRiCheckBox();
    WPT1 = new XRiCheckBox();
    WPT2 = new XRiCheckBox();
    WPT3 = new XRiCheckBox();
    WPT4 = new XRiCheckBox();
    WPT5 = new XRiCheckBox();
    WPT6 = new XRiCheckBox();
    WPT7 = new XRiCheckBox();
    WPT8 = new XRiCheckBox();
    WPT9 = new XRiCheckBox();
    WPT10 = new XRiCheckBox();
    WPT11 = new XRiCheckBox();
    WPT13 = new XRiCheckBox();
    WPT14 = new XRiCheckBox();
    OBJ_39 = new RiZoneSortie();
    OBJ_40 = new RiZoneSortie();
    OBJ_41 = new RiZoneSortie();
    OBJ_42 = new RiZoneSortie();
    OBJ_43 = new RiZoneSortie();
    OBJ_44 = new RiZoneSortie();
    OBJ_45 = new RiZoneSortie();
    OBJ_46 = new RiZoneSortie();
    OBJ_47 = new RiZoneSortie();
    OBJ_48 = new RiZoneSortie();
    OBJ_49 = new RiZoneSortie();
    OBJ_50 = new RiZoneSortie();
    OBJ_51 = new RiZoneSortie();
    OBJ_52 = new RiZoneSortie();
    OBJ_19 = new JLabel();
    OBJ_20 = new JLabel();
    OBJ_18 = new JLabel();
    xTitledSeparator4 = new JXTitledSeparator();
    panel2 = new JPanel();
    panel3 = new JPanel();
    panel4 = new JPanel();
    WPTTOU = new XRiCheckBox();
    OBJ_53 = new JLabel();
    DATREF = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Tableau \u00e9ch\u00e9ancier");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(420, 510));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setTitle("Crit\u00e8res de g\u00e9n\u00e9ration \u00e9ch\u00e9ancier");
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

            //---- WPT12 ----
            WPT12.setText("12   @LIB12@");
            WPT12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WPT12.setName("WPT12");

            //---- WPT1 ----
            WPT1.setText("01   @LIB01@");
            WPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WPT1.setName("WPT1");

            //---- WPT2 ----
            WPT2.setText("02   @LIB02@");
            WPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WPT2.setName("WPT2");

            //---- WPT3 ----
            WPT3.setText("03   @LIB03@");
            WPT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WPT3.setName("WPT3");

            //---- WPT4 ----
            WPT4.setText("04   @LIB04@");
            WPT4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WPT4.setName("WPT4");

            //---- WPT5 ----
            WPT5.setText("05   @LIB05@");
            WPT5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WPT5.setName("WPT5");

            //---- WPT6 ----
            WPT6.setText("06   @LIB06@");
            WPT6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WPT6.setName("WPT6");

            //---- WPT7 ----
            WPT7.setText("07   @LIB07@");
            WPT7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WPT7.setName("WPT7");

            //---- WPT8 ----
            WPT8.setText("08   @LIB08@");
            WPT8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WPT8.setName("WPT8");

            //---- WPT9 ----
            WPT9.setText("09   @LIB09@");
            WPT9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WPT9.setName("WPT9");

            //---- WPT10 ----
            WPT10.setText("10   @LIB10@");
            WPT10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WPT10.setName("WPT10");

            //---- WPT11 ----
            WPT11.setText("11   @LIB11@");
            WPT11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WPT11.setName("WPT11");

            //---- WPT13 ----
            WPT13.setText("13   @LIB13@");
            WPT13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WPT13.setName("WPT13");

            //---- WPT14 ----
            WPT14.setText("14   @LIB14@");
            WPT14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WPT14.setName("WPT14");

            //---- OBJ_39 ----
            OBJ_39.setText("@DTG01@");
            OBJ_39.setName("OBJ_39");

            //---- OBJ_40 ----
            OBJ_40.setText("@DTG02@");
            OBJ_40.setName("OBJ_40");

            //---- OBJ_41 ----
            OBJ_41.setText("@DTG03@");
            OBJ_41.setName("OBJ_41");

            //---- OBJ_42 ----
            OBJ_42.setText("@DTG04@");
            OBJ_42.setName("OBJ_42");

            //---- OBJ_43 ----
            OBJ_43.setText("@DTG05@");
            OBJ_43.setName("OBJ_43");

            //---- OBJ_44 ----
            OBJ_44.setText("@DTG06@");
            OBJ_44.setName("OBJ_44");

            //---- OBJ_45 ----
            OBJ_45.setText("@DTG07@");
            OBJ_45.setName("OBJ_45");

            //---- OBJ_46 ----
            OBJ_46.setText("@DTG08@");
            OBJ_46.setName("OBJ_46");

            //---- OBJ_47 ----
            OBJ_47.setText("@DTG09@");
            OBJ_47.setName("OBJ_47");

            //---- OBJ_48 ----
            OBJ_48.setText("@DTG10@");
            OBJ_48.setName("OBJ_48");

            //---- OBJ_49 ----
            OBJ_49.setText("@DTG11@");
            OBJ_49.setName("OBJ_49");

            //---- OBJ_50 ----
            OBJ_50.setText("@DTG12@");
            OBJ_50.setName("OBJ_50");

            //---- OBJ_51 ----
            OBJ_51.setText("@DTG13@");
            OBJ_51.setName("OBJ_51");

            //---- OBJ_52 ----
            OBJ_52.setText("@DTG14@");
            OBJ_52.setName("OBJ_52");

            //---- OBJ_19 ----
            OBJ_19.setText("Libell\u00e9");
            OBJ_19.setName("OBJ_19");

            //---- OBJ_20 ----
            OBJ_20.setText("Date");
            OBJ_20.setName("OBJ_20");

            //---- OBJ_18 ----
            OBJ_18.setText("N\u00b0");
            OBJ_18.setName("OBJ_18");

            //---- xTitledSeparator4 ----
            xTitledSeparator4.setTitle("");
            xTitledSeparator4.setBackground(new Color(214, 217, 223));
            xTitledSeparator4.setName("xTitledSeparator4");

            //======== panel2 ========
            {
              panel2.setBackground(Color.lightGray);
              panel2.setName("panel2");

              GroupLayout panel2Layout = new GroupLayout(panel2);
              panel2.setLayout(panel2Layout);
              panel2Layout.setHorizontalGroup(
                panel2Layout.createParallelGroup()
                  .addGap(0, 2, Short.MAX_VALUE)
              );
              panel2Layout.setVerticalGroup(
                panel2Layout.createParallelGroup()
                  .addGap(0, 345, Short.MAX_VALUE)
              );
            }

            //======== panel3 ========
            {
              panel3.setBackground(Color.lightGray);
              panel3.setName("panel3");

              GroupLayout panel3Layout = new GroupLayout(panel3);
              panel3.setLayout(panel3Layout);
              panel3Layout.setHorizontalGroup(
                panel3Layout.createParallelGroup()
                  .addGap(0, 2, Short.MAX_VALUE)
              );
              panel3Layout.setVerticalGroup(
                panel3Layout.createParallelGroup()
                  .addGap(0, 345, Short.MAX_VALUE)
              );
            }

            GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
            xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
            xTitledPanel1ContentContainerLayout.setHorizontalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(45, 45, 45)
                      .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, 61, GroupLayout.PREFERRED_SIZE))
                    .addComponent(WPT1, GroupLayout.PREFERRED_SIZE, 262, GroupLayout.PREFERRED_SIZE)
                    .addComponent(WPT2, GroupLayout.PREFERRED_SIZE, 262, GroupLayout.PREFERRED_SIZE)
                    .addComponent(WPT3, GroupLayout.PREFERRED_SIZE, 262, GroupLayout.PREFERRED_SIZE)
                    .addComponent(WPT4, GroupLayout.PREFERRED_SIZE, 262, GroupLayout.PREFERRED_SIZE)
                    .addComponent(WPT5, GroupLayout.PREFERRED_SIZE, 262, GroupLayout.PREFERRED_SIZE)
                    .addComponent(WPT6, GroupLayout.PREFERRED_SIZE, 262, GroupLayout.PREFERRED_SIZE)
                    .addComponent(WPT8, GroupLayout.PREFERRED_SIZE, 262, GroupLayout.PREFERRED_SIZE)
                    .addComponent(WPT9, GroupLayout.PREFERRED_SIZE, 262, GroupLayout.PREFERRED_SIZE)
                    .addComponent(WPT10, GroupLayout.PREFERRED_SIZE, 262, GroupLayout.PREFERRED_SIZE)
                    .addComponent(WPT12, GroupLayout.PREFERRED_SIZE, 262, GroupLayout.PREFERRED_SIZE)
                    .addComponent(WPT13, GroupLayout.PREFERRED_SIZE, 262, GroupLayout.PREFERRED_SIZE)
                    .addComponent(WPT14, GroupLayout.PREFERRED_SIZE, 262, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(40, 40, 40)
                      .addComponent(panel3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(8, 8, 8)
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(23, 23, 23)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(35, 35, 35)
                  .addComponent(OBJ_18, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(310, 310, 310)
                  .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addComponent(WPT11, GroupLayout.PREFERRED_SIZE, 262, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(310, 310, 310)
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(310, 310, 310)
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addComponent(WPT7, GroupLayout.PREFERRED_SIZE, 262, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addComponent(xTitledSeparator4, GroupLayout.PREFERRED_SIZE, 360, GroupLayout.PREFERRED_SIZE))
            );
            xTitledPanel1ContentContainerLayout.setVerticalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(10, 10, 10)
                      .addComponent(OBJ_19)
                      .addGap(29, 29, 29)
                      .addComponent(WPT1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(WPT2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(WPT3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(WPT4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(WPT5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(WPT6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(24, 24, 24)
                      .addComponent(WPT8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(WPT9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(WPT10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(24, 24, 24)
                      .addComponent(WPT12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(WPT13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(WPT14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(panel3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(35, 35, 35)
                  .addComponent(OBJ_20)
                  .addGap(28, 28, 28)
                  .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addGap(42, 42, 42)
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addGap(21, 21, 21)
                  .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(35, 35, 35)
                  .addComponent(OBJ_18))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(205, 205, 205)
                  .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(290, 290, 290)
                  .addComponent(WPT11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(184, 184, 184)
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(268, 268, 268)
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(206, 206, 206)
                  .addComponent(WPT7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(55, 55, 55)
                  .addComponent(xTitledSeparator4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            );
          }

          //======== panel4 ========
          {
            panel4.setBorder(new DropShadowBorder());
            panel4.setOpaque(false);
            panel4.setName("panel4");

            //---- WPTTOU ----
            WPTTOU.setText("G\u00e9n\u00e9ration globale");
            WPTTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WPTTOU.setName("WPTTOU");

            //---- OBJ_53 ----
            OBJ_53.setText("Date de r\u00e9f\u00e9rence");
            OBJ_53.setName("OBJ_53");

            //---- DATREF ----
            DATREF.setComponentPopupMenu(BTD);
            DATREF.setName("DATREF");

            GroupLayout panel4Layout = new GroupLayout(panel4);
            panel4.setLayout(panel4Layout);
            panel4Layout.setHorizontalGroup(
              panel4Layout.createParallelGroup()
                .addGroup(panel4Layout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addComponent(WPTTOU, GroupLayout.PREFERRED_SIZE, 139, GroupLayout.PREFERRED_SIZE)
                  .addGap(21, 21, 21)
                  .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE)
                  .addGap(23, 23, 23)
                  .addComponent(DATREF, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
            );
            panel4Layout.setVerticalGroup(
              panel4Layout.createParallelGroup()
                .addGroup(panel4Layout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(WPTTOU, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel4Layout.createSequentialGroup()
                  .addGap(21, 21, 21)
                  .addComponent(OBJ_53))
                .addGroup(panel4Layout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addComponent(DATREF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                  .addComponent(panel4, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 398, Short.MAX_VALUE)
                  .addComponent(xTitledPanel1, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 398, Short.MAX_VALUE))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 415, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(16, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private XRiCheckBox WPT12;
  private XRiCheckBox WPT1;
  private XRiCheckBox WPT2;
  private XRiCheckBox WPT3;
  private XRiCheckBox WPT4;
  private XRiCheckBox WPT5;
  private XRiCheckBox WPT6;
  private XRiCheckBox WPT7;
  private XRiCheckBox WPT8;
  private XRiCheckBox WPT9;
  private XRiCheckBox WPT10;
  private XRiCheckBox WPT11;
  private XRiCheckBox WPT13;
  private XRiCheckBox WPT14;
  private RiZoneSortie OBJ_39;
  private RiZoneSortie OBJ_40;
  private RiZoneSortie OBJ_41;
  private RiZoneSortie OBJ_42;
  private RiZoneSortie OBJ_43;
  private RiZoneSortie OBJ_44;
  private RiZoneSortie OBJ_45;
  private RiZoneSortie OBJ_46;
  private RiZoneSortie OBJ_47;
  private RiZoneSortie OBJ_48;
  private RiZoneSortie OBJ_49;
  private RiZoneSortie OBJ_50;
  private RiZoneSortie OBJ_51;
  private RiZoneSortie OBJ_52;
  private JLabel OBJ_19;
  private JLabel OBJ_20;
  private JLabel OBJ_18;
  private JXTitledSeparator xTitledSeparator4;
  private JPanel panel2;
  private JPanel panel3;
  private JPanel panel4;
  private XRiCheckBox WPTTOU;
  private JLabel OBJ_53;
  private XRiTextField DATREF;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
