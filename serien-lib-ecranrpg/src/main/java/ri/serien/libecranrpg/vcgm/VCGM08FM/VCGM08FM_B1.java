
package ri.serien.libecranrpg.vcgm.VCGM08FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM08FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM08FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_97.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DAT1@")).trim());
    OBJ_55.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ULEX1@")).trim());
    OBJ_98.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DAT2@")).trim());
    OBJ_88.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ULEX2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    XDCV1.setEnabled(lexique.isPresent("XDCV1"));
    XRCV2.setEnabled(lexique.isPresent("XRCV2"));
    XRCR2.setEnabled(lexique.isPresent("XRCR2"));
    XDCV2.setEnabled(lexique.isPresent("XDCV2"));
    XDCR2.setEnabled(lexique.isPresent("XDCR2"));
    XRCV1.setEnabled(lexique.isPresent("XRCV1"));
    XRCR1.setEnabled(lexique.isPresent("XRCR1"));
    XDCR1.setEnabled(lexique.isPresent("XDCR1"));
    OBJ_88.setVisible(lexique.isPresent("ULEX2"));
    OBJ_55.setVisible(lexique.isPresent("ULEX1"));
    OBJ_98.setVisible(lexique.isPresent("DAT2"));
    OBJ_97.setVisible(lexique.isPresent("DAT1"));
    OBJ_92.setVisible(lexique.isPresent("INDCOD"));
    INDCOD.setEnabled(lexique.isPresent("INDCOD"));
    INDNCG.setEnabled(lexique.isPresent("INDNCG"));
    XRBU2.setEnabled(lexique.isPresent("XRBU2"));
    XDBU2.setEnabled(lexique.isPresent("XDBU2"));
    XRBU1.setEnabled(lexique.isPresent("XRBU1"));
    XDBU1.setEnabled(lexique.isPresent("XDBU1"));
    SALIB.setEnabled(lexique.isPresent("SALIB"));
    G1LIB.setEnabled(lexique.isPresent("G1LIB"));
    separator3.setVisible(lexique.isTrue("94"));
    separator4.setVisible(lexique.isTrue("94"));
    separator5.setVisible(lexique.isTrue("N94"));
    separator6.setVisible(lexique.isTrue("N94"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDSOC.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(INDSOC.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_28 = new JLabel();
    INDSOC = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_43 = new JLabel();
    OBJ_92 = new JLabel();
    INDCOD = new XRiTextField();
    INDNCG = new XRiTextField();
    G1LIB = new XRiTextField();
    SALIB = new XRiTextField();
    xTitledPanel2 = new JXTitledPanel();
    XDBU1 = new XRiTextField();
    XRBU1 = new XRiTextField();
    OBJ_108 = new JLabel();
    OBJ_107 = new JLabel();
    OBJ_53 = new JLabel();
    OBJ_93 = new JLabel();
    OBJ_97 = new RiZoneSortie();
    OBJ_55 = new RiZoneSortie();
    OBJ_52 = new JLabel();
    OBJ_54 = new JLabel();
    XDCR1 = new XRiTextField();
    XRCR1 = new XRiTextField();
    XRCV1 = new XRiTextField();
    XDCV1 = new XRiTextField();
    OBJ_113 = new JLabel();
    OBJ_115 = new JLabel();
    separator1 = compFactory.createSeparator("DEFINITIF");
    separator2 = compFactory.createSeparator("REACTUALISE");
    xTitledPanel3 = new JXTitledPanel();
    XDBU2 = new XRiTextField();
    XRBU2 = new XRiTextField();
    OBJ_110 = new JLabel();
    OBJ_112 = new JLabel();
    OBJ_109 = new JLabel();
    OBJ_111 = new JLabel();
    OBJ_98 = new RiZoneSortie();
    OBJ_88 = new RiZoneSortie();
    OBJ_85 = new JLabel();
    OBJ_86 = new JLabel();
    XDCR2 = new XRiTextField();
    XDCV2 = new XRiTextField();
    XRCR2 = new XRiTextField();
    XRCV2 = new XRiTextField();
    OBJ_117 = new JLabel();
    OBJ_119 = new JLabel();
    separator3 = compFactory.createSeparator("PROJET");
    separator4 = compFactory.createSeparator("");
    separator6 = compFactory.createSeparator("REACTUALISE");
    separator5 = compFactory.createSeparator("DEFINITIF");
    BTD = new JPopupMenu();
    OBJ_23 = new JMenuItem();
    OBJ_22 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion des budgets");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_28 ----
          OBJ_28.setText("Soci\u00e9t\u00e9");
          OBJ_28.setName("OBJ_28");

          //---- INDSOC ----
          INDSOC.setComponentPopupMenu(BTD);
          INDSOC.setName("INDSOC");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INDSOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(405, 405, 405))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_28))
              .addComponent(INDSOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Ann\u00e9e pr\u00e9c\u00e9dente");
              riSousMenu_bt6.setToolTipText("Ann\u00e9e pr\u00e9c\u00e9dente");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("R\u00e9partition exercice 1");
              riSousMenu_bt7.setToolTipText("R\u00e9partition budget pour exercice 1");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("R\u00e9partition exercice 2");
              riSousMenu_bt8.setToolTipText("R\u00e9partition budget pour exercice 2");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Protection du budget");
              riSousMenu_bt9.setToolTipText("Protection du budget");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(790, 420));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

            //---- OBJ_43 ----
            OBJ_43.setText("COMPTE");
            OBJ_43.setFont(OBJ_43.getFont().deriveFont(OBJ_43.getFont().getStyle() | Font.BOLD));
            OBJ_43.setName("OBJ_43");

            //---- OBJ_92 ----
            OBJ_92.setText("CODE");
            OBJ_92.setFont(OBJ_92.getFont().deriveFont(OBJ_92.getFont().getStyle() | Font.BOLD));
            OBJ_92.setName("OBJ_92");

            //---- INDCOD ----
            INDCOD.setFont(INDCOD.getFont().deriveFont(INDCOD.getFont().getStyle() | Font.BOLD));
            INDCOD.setName("INDCOD");

            //---- INDNCG ----
            INDNCG.setFont(INDNCG.getFont().deriveFont(INDNCG.getFont().getStyle() | Font.BOLD));
            INDNCG.setName("INDNCG");

            //---- G1LIB ----
            G1LIB.setFont(G1LIB.getFont().deriveFont(G1LIB.getFont().getStyle() | Font.BOLD));
            G1LIB.setName("G1LIB");

            //---- SALIB ----
            SALIB.setFont(SALIB.getFont().deriveFont(SALIB.getFont().getStyle() | Font.BOLD));
            SALIB.setName("SALIB");

            GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
            xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
            xTitledPanel1ContentContainerLayout.setHorizontalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE)
                      .addGap(8, 8, 8)
                      .addComponent(INDNCG, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(G1LIB, GroupLayout.PREFERRED_SIZE, 355, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_92, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                      .addGap(25, 25, 25)
                      .addComponent(INDCOD, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addGap(25, 25, 25)
                      .addComponent(SALIB, GroupLayout.PREFERRED_SIZE, 355, GroupLayout.PREFERRED_SIZE)))
                  .addContainerGap(249, Short.MAX_VALUE))
            );
            xTitledPanel1ContentContainerLayout.setVerticalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(14, 14, 14)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_43))
                    .addComponent(INDNCG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(G1LIB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(7, 7, 7)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_92))
                    .addComponent(INDCOD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SALIB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addContainerGap(20, Short.MAX_VALUE))
            );
          }

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setTitle("Budget exercice 1");
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //---- XDBU1 ----
            XDBU1.setComponentPopupMenu(BTD);
            XDBU1.setName("XDBU1");
            xTitledPanel2ContentContainer.add(XDBU1);
            XDBU1.setBounds(220, 55, 108, XDBU1.getPreferredSize().height);

            //---- XRBU1 ----
            XRBU1.setComponentPopupMenu(BTD);
            XRBU1.setName("XRBU1");
            xTitledPanel2ContentContainer.add(XRBU1);
            XRBU1.setBounds(505, 55, 108, XRBU1.getPreferredSize().height);

            //---- OBJ_108 ----
            OBJ_108.setText("R\u00e9partition");
            OBJ_108.setName("OBJ_108");
            xTitledPanel2ContentContainer.add(OBJ_108);
            OBJ_108.setBounds(615, 35, 68, 25);

            //---- OBJ_107 ----
            OBJ_107.setText("Ventilation");
            OBJ_107.setName("OBJ_107");
            xTitledPanel2ContentContainer.add(OBJ_107);
            OBJ_107.setBounds(685, 35, 65, 25);

            //---- OBJ_53 ----
            OBJ_53.setText("Ventilation");
            OBJ_53.setName("OBJ_53");
            xTitledPanel2ContentContainer.add(OBJ_53);
            OBJ_53.setBounds(400, 35, 62, 25);

            //---- OBJ_93 ----
            OBJ_93.setText("R\u00e9partition");
            OBJ_93.setName("OBJ_93");
            xTitledPanel2ContentContainer.add(OBJ_93);
            OBJ_93.setBounds(330, 35, 68, 25);

            //---- OBJ_97 ----
            OBJ_97.setText("@DAT1@");
            OBJ_97.setName("OBJ_97");
            xTitledPanel2ContentContainer.add(OBJ_97);
            OBJ_97.setBounds(120, 57, 70, OBJ_97.getPreferredSize().height);

            //---- OBJ_55 ----
            OBJ_55.setText("@ULEX1@");
            OBJ_55.setName("OBJ_55");
            xTitledPanel2ContentContainer.add(OBJ_55);
            OBJ_55.setBounds(20, 57, 85, OBJ_55.getPreferredSize().height);

            //---- OBJ_52 ----
            OBJ_52.setText("Montant");
            OBJ_52.setName("OBJ_52");
            xTitledPanel2ContentContainer.add(OBJ_52);
            OBJ_52.setBounds(220, 35, 51, 25);

            //---- OBJ_54 ----
            OBJ_54.setText("Montant");
            OBJ_54.setName("OBJ_54");
            xTitledPanel2ContentContainer.add(OBJ_54);
            OBJ_54.setBounds(505, 35, 51, 25);

            //---- XDCR1 ----
            XDCR1.setComponentPopupMenu(BTD);
            XDCR1.setName("XDCR1");
            xTitledPanel2ContentContainer.add(XDCR1);
            XDCR1.setBounds(335, 55, 40, XDCR1.getPreferredSize().height);

            //---- XRCR1 ----
            XRCR1.setComponentPopupMenu(BTD);
            XRCR1.setName("XRCR1");
            xTitledPanel2ContentContainer.add(XRCR1);
            XRCR1.setBounds(620, 55, 40, XRCR1.getPreferredSize().height);

            //---- XRCV1 ----
            XRCV1.setComponentPopupMenu(BTD);
            XRCV1.setName("XRCV1");
            xTitledPanel2ContentContainer.add(XRCV1);
            XRCV1.setBounds(690, 55, 40, XRCV1.getPreferredSize().height);

            //---- XDCV1 ----
            XDCV1.setComponentPopupMenu(BTD);
            XDCV1.setName("XDCV1");
            xTitledPanel2ContentContainer.add(XDCV1);
            XDCV1.setBounds(405, 55, 40, XDCV1.getPreferredSize().height);

            //---- OBJ_113 ----
            OBJ_113.setText("Cl\u00e9");
            OBJ_113.setName("OBJ_113");
            xTitledPanel2ContentContainer.add(OBJ_113);
            OBJ_113.setBounds(380, 20, 35, 25);

            //---- OBJ_115 ----
            OBJ_115.setText("Cl\u00e9");
            OBJ_115.setName("OBJ_115");
            xTitledPanel2ContentContainer.add(OBJ_115);
            OBJ_115.setBounds(670, 20, 40, 25);

            //---- separator1 ----
            separator1.setName("separator1");
            xTitledPanel2ContentContainer.add(separator1);
            separator1.setBounds(210, 10, 250, separator1.getPreferredSize().height);

            //---- separator2 ----
            separator2.setName("separator2");
            xTitledPanel2ContentContainer.add(separator2);
            separator2.setBounds(485, 10, 250, separator2.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel3 ========
          {
            xTitledPanel3.setTitle("Budget exercice 2");
            xTitledPanel3.setBorder(new DropShadowBorder());
            xTitledPanel3.setName("xTitledPanel3");
            Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
            xTitledPanel3ContentContainer.setLayout(null);

            //---- XDBU2 ----
            XDBU2.setComponentPopupMenu(BTD);
            XDBU2.setName("XDBU2");
            xTitledPanel3ContentContainer.add(XDBU2);
            XDBU2.setBounds(220, 55, 108, XDBU2.getPreferredSize().height);

            //---- XRBU2 ----
            XRBU2.setComponentPopupMenu(BTD);
            XRBU2.setName("XRBU2");
            xTitledPanel3ContentContainer.add(XRBU2);
            XRBU2.setBounds(510, 55, 108, XRBU2.getPreferredSize().height);

            //---- OBJ_110 ----
            OBJ_110.setText("R\u00e9partition");
            OBJ_110.setName("OBJ_110");
            xTitledPanel3ContentContainer.add(OBJ_110);
            OBJ_110.setBounds(620, 35, 68, 25);

            //---- OBJ_112 ----
            OBJ_112.setText("R\u00e9partition");
            OBJ_112.setName("OBJ_112");
            xTitledPanel3ContentContainer.add(OBJ_112);
            OBJ_112.setBounds(330, 35, 68, 25);

            //---- OBJ_109 ----
            OBJ_109.setText("Ventilation");
            OBJ_109.setName("OBJ_109");
            xTitledPanel3ContentContainer.add(OBJ_109);
            OBJ_109.setBounds(685, 35, 65, 25);

            //---- OBJ_111 ----
            OBJ_111.setText("Ventilation");
            OBJ_111.setName("OBJ_111");
            xTitledPanel3ContentContainer.add(OBJ_111);
            OBJ_111.setBounds(395, 35, 65, 25);

            //---- OBJ_98 ----
            OBJ_98.setText("@DAT2@");
            OBJ_98.setName("OBJ_98");
            xTitledPanel3ContentContainer.add(OBJ_98);
            OBJ_98.setBounds(120, 57, 70, OBJ_98.getPreferredSize().height);

            //---- OBJ_88 ----
            OBJ_88.setText("@ULEX2@");
            OBJ_88.setName("OBJ_88");
            xTitledPanel3ContentContainer.add(OBJ_88);
            OBJ_88.setBounds(20, 57, 85, OBJ_88.getPreferredSize().height);

            //---- OBJ_85 ----
            OBJ_85.setText("Montant");
            OBJ_85.setName("OBJ_85");
            xTitledPanel3ContentContainer.add(OBJ_85);
            OBJ_85.setBounds(220, 35, 51, 25);

            //---- OBJ_86 ----
            OBJ_86.setText("Montant");
            OBJ_86.setName("OBJ_86");
            xTitledPanel3ContentContainer.add(OBJ_86);
            OBJ_86.setBounds(510, 35, 51, 25);

            //---- XDCR2 ----
            XDCR2.setComponentPopupMenu(BTD);
            XDCR2.setName("XDCR2");
            xTitledPanel3ContentContainer.add(XDCR2);
            XDCR2.setBounds(335, 55, 40, XDCR2.getPreferredSize().height);

            //---- XDCV2 ----
            XDCV2.setComponentPopupMenu(BTD);
            XDCV2.setName("XDCV2");
            xTitledPanel3ContentContainer.add(XDCV2);
            XDCV2.setBounds(405, 55, 40, XDCV2.getPreferredSize().height);

            //---- XRCR2 ----
            XRCR2.setComponentPopupMenu(BTD);
            XRCR2.setName("XRCR2");
            xTitledPanel3ContentContainer.add(XRCR2);
            XRCR2.setBounds(625, 55, 40, XRCR2.getPreferredSize().height);

            //---- XRCV2 ----
            XRCV2.setComponentPopupMenu(BTD);
            XRCV2.setName("XRCV2");
            xTitledPanel3ContentContainer.add(XRCV2);
            XRCV2.setBounds(695, 55, 40, XRCV2.getPreferredSize().height);

            //---- OBJ_117 ----
            OBJ_117.setText("Cl\u00e9");
            OBJ_117.setName("OBJ_117");
            xTitledPanel3ContentContainer.add(OBJ_117);
            OBJ_117.setBounds(380, 20, 40, 25);

            //---- OBJ_119 ----
            OBJ_119.setText("Cl\u00e9");
            OBJ_119.setName("OBJ_119");
            xTitledPanel3ContentContainer.add(OBJ_119);
            OBJ_119.setBounds(670, 20, 30, 25);

            //---- separator3 ----
            separator3.setName("separator3");
            xTitledPanel3ContentContainer.add(separator3);
            separator3.setBounds(210, 10, 250, separator3.getPreferredSize().height);

            //---- separator4 ----
            separator4.setName("separator4");
            xTitledPanel3ContentContainer.add(separator4);
            separator4.setBounds(485, 10, 250, separator4.getPreferredSize().height);

            //---- separator6 ----
            separator6.setName("separator6");
            xTitledPanel3ContentContainer.add(separator6);
            separator6.setBounds(485, 10, 250, separator6.getPreferredSize().height);

            //---- separator5 ----
            separator5.setName("separator5");
            xTitledPanel3ContentContainer.add(separator5);
            separator5.setBounds(210, 10, 250, separator5.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel3ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 764, GroupLayout.PREFERRED_SIZE)
                  .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 764, GroupLayout.PREFERRED_SIZE)))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 126, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_23 ----
      OBJ_23.setText("Choix possibles");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD.add(OBJ_23);

      //---- OBJ_22 ----
      OBJ_22.setText("Aide en ligne");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_28;
  private XRiTextField INDSOC;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_43;
  private JLabel OBJ_92;
  private XRiTextField INDCOD;
  private XRiTextField INDNCG;
  private XRiTextField G1LIB;
  private XRiTextField SALIB;
  private JXTitledPanel xTitledPanel2;
  private XRiTextField XDBU1;
  private XRiTextField XRBU1;
  private JLabel OBJ_108;
  private JLabel OBJ_107;
  private JLabel OBJ_53;
  private JLabel OBJ_93;
  private RiZoneSortie OBJ_97;
  private RiZoneSortie OBJ_55;
  private JLabel OBJ_52;
  private JLabel OBJ_54;
  private XRiTextField XDCR1;
  private XRiTextField XRCR1;
  private XRiTextField XRCV1;
  private XRiTextField XDCV1;
  private JLabel OBJ_113;
  private JLabel OBJ_115;
  private JComponent separator1;
  private JComponent separator2;
  private JXTitledPanel xTitledPanel3;
  private XRiTextField XDBU2;
  private XRiTextField XRBU2;
  private JLabel OBJ_110;
  private JLabel OBJ_112;
  private JLabel OBJ_109;
  private JLabel OBJ_111;
  private RiZoneSortie OBJ_98;
  private RiZoneSortie OBJ_88;
  private JLabel OBJ_85;
  private JLabel OBJ_86;
  private XRiTextField XDCR2;
  private XRiTextField XDCV2;
  private XRiTextField XRCR2;
  private XRiTextField XRCV2;
  private JLabel OBJ_117;
  private JLabel OBJ_119;
  private JComponent separator3;
  private JComponent separator4;
  private JComponent separator6;
  private JComponent separator5;
  private JPopupMenu BTD;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_22;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
