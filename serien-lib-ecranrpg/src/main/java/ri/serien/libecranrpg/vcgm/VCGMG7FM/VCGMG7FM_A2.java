
package ri.serien.libecranrpg.vcgm.VCGMG7FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.vente.documentvente.snvendeur.SNVendeur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VCGMG7FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] V06F_Value = { "-10", "-05", "-03", "-02", "-01", "", "+01", "+02", "+03", "+05", "+10", "D", "F" };
  private String[] V06F_Title = { "Décalage de 10 lignes en moins", "Décalage de 5 lignes en moins", "Décalage de 3 lignes en moins",
      "Décalage de 2 lignes en moins", "Décalage de 1 ligne en moins", "  ", "Décalage de 1 ligne en plus",
      "Décalage de 2 lignes en plus", "Décalage de 3 lignes en plus", "Décalage de 5 lignes en plus", "Décalage de 10 lignes en plus",
      "Début", "Fin", };
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "HLD01", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 45, };
  
  private final static String BOUTON_DETAIL_ESPECE = "Détail des espéces";
  private final static String BOUTON_MOUVEMENTS_CAISSE = "Mouvements de caisse";
  private final static String BOUTON_VALIDER_CAISSE = "Valider la caisse";
  
  public VCGMG7FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    
    // Ajout
    initDiverses();
    V06F.setValeurs(V06F_Value, V06F_Title);
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null);
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_DETAIL_ESPECE, 'd', true);
    snBarreBouton.ajouterBouton(BOUTON_MOUVEMENTS_CAISSE, 'c', true);
    snBarreBouton.ajouterBouton(BOUTON_VALIDER_CAISSE, 'v', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "DEMETB");
    
    // Vendeur
    snVendeur.setSession(getSession());
    snVendeur.setIdEtablissement(snEtablissement.getIdSelection());
    snVendeur.setTousAutorise(true);
    snVendeur.charger(false);
    snVendeur.setSelectionParChampRPG(lexique, "DEMVDE");
    
    BT_PFIN.setIcon(lexique.chargerImage("images/pfin20.png", true));
    BT_PDEB.setIcon(lexique.chargerImage("images/pdeb20.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_DETAIL_ESPECE)) {
        lexique.HostScreenSendKey(this, "F6");
      }
      else if (pSNBouton.isBouton(BOUTON_MOUVEMENTS_CAISSE)) {
        lexique.HostScreenSendKey(this, "F7");
      }
      else if (pSNBouton.isBouton(BOUTON_VALIDER_CAISSE)) {
        lexique.HostScreenSendKey(this, "F8");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    try {
      if (WTP01.doubleClicSelection(e)) {
        WTP01.setValeurTop("1");
        lexique.HostScreenSendKey(this, "Enter");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void V06FItemStateChanged(ItemEvent e) {
    try {
      lexique.HostScreenSendKey(this, "Enter");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void miModificationActionPerformed(ActionEvent e) {
    try {
      WTP01.setValeurTop("2");
      lexique.HostScreenSendKey(this, "ENTER");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void miSuppressionActionPerformed(ActionEvent e) {
    try {
      WTP01.setValeurTop("4");
      lexique.HostScreenSendKey(this, "ENTER");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void BT_PDEBActionPerformed(ActionEvent e) {
    V06F.setSelectedItem("Début");
    lexique.HostScreenSendKey(this, "ENTER");
    
  }
  
  private void BT_PFINActionPerformed(ActionEvent e) {
    V06F.setSelectedItem("Fin");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_presentation = new JPanel();
    p_logo = new JPanel();
    logoEtb = new JLabel();
    p_titre = new JPanel();
    titre = new JLabel();
    fond_titre = new JLabel();
    snBarreBouton = new SNBarreBouton();
    sNPanelPrincipal1 = new SNPanelContenu();
    sNPanel2 = new SNPanel();
    lbDate = new SNLabelChamp();
    DEMDAT = new XRiCalendrier();
    lbVendeur = new SNLabelChamp();
    snVendeur = new SNVendeur();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    pnlDetail = new SNPanelTitre();
    lbFondDeCaisse = new SNLabelChamp();
    VALDEB = new XRiTextField();
    scrollPane1 = new JScrollPane();
    WTP01 = new XRiTable();
    sNPanel1 = new SNPanel();
    BT_PDEB = new JButton();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BT_PFIN = new JButton();
    lbPagination = new SNLabelChamp();
    V06F = new XRiComboBox();
    lbTotal = new SNLabelChamp();
    VALFIN = new XRiTextField();
    BTD = new JPopupMenu();
    miModification = new JMenuItem();
    miSuppression = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //======== p_presentation ========
      {
        p_presentation.setPreferredSize(new Dimension(1020, 55));
        p_presentation.setMinimumSize(new Dimension(950, 55));
        p_presentation.setMaximumSize(new Dimension(32767, 55));
        p_presentation.setBackground(Color.white);
        p_presentation.setAutoscrolls(true);
        p_presentation.setFocusable(false);
        p_presentation.setName("p_presentation");
        p_presentation.setLayout(new BorderLayout());

        //======== p_logo ========
        {
          p_logo.setMinimumSize(new Dimension(120, 55));
          p_logo.setPreferredSize(new Dimension(120, 55));
          p_logo.setBackground(new Color(238, 239, 241));
          p_logo.setName("p_logo");
          p_logo.setLayout(null);

          //---- logoEtb ----
          logoEtb.setBorder(null);
          logoEtb.setName("logoEtb");
          p_logo.add(logoEtb);
          logoEtb.setBounds(70, 5, 45, 45);
        }
        p_presentation.add(p_logo, BorderLayout.EAST);

        //======== p_titre ========
        {
          p_titre.setBackground(new Color(238, 239, 241));
          p_titre.setPreferredSize(new Dimension(930, 0));
          p_titre.setMinimumSize(new Dimension(930, 0));
          p_titre.setName("p_titre");
          p_titre.setLayout(null);

          //---- titre ----
          titre.setBackground(new Color(198, 198, 200));
          titre.setText("Saisie de caisse et validation");
          titre.setHorizontalAlignment(SwingConstants.LEFT);
          titre.setFont(titre.getFont().deriveFont(titre.getFont().getStyle() | Font.BOLD, titre.getFont().getSize() + 6f));
          titre.setForeground(Color.darkGray);
          titre.setName("titre");
          p_titre.add(titre);
          titre.setBounds(10, 0, 600, 55);

          //---- fond_titre ----
          fond_titre.setFont(new Font("Arial", Font.BOLD, 18));
          fond_titre.setForeground(Color.white);
          fond_titre.setName("fond_titre");
          p_titre.add(fond_titre);
          fond_titre.setBounds(0, 0, 900, 55);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_titre.getComponentCount(); i++) {
              Rectangle bounds = p_titre.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_titre.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_titre.setMinimumSize(preferredSize);
            p_titre.setPreferredSize(preferredSize);
          }
        }
        p_presentation.add(p_titre, BorderLayout.CENTER);
      }
      p_nord.add(p_presentation);
    }
    add(p_nord, BorderLayout.NORTH);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //======== sNPanelPrincipal1 ========
    {
      sNPanelPrincipal1.setAutoscrolls(true);
      sNPanelPrincipal1.setMinimumSize(new Dimension(1145, 450));
      sNPanelPrincipal1.setPreferredSize(new Dimension(1145, 450));
      sNPanelPrincipal1.setName("sNPanelPrincipal1");
      sNPanelPrincipal1.setLayout(new GridBagLayout());
      ((GridBagLayout)sNPanelPrincipal1.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)sNPanelPrincipal1.getLayout()).rowHeights = new int[] {0, 0, 0};
      ((GridBagLayout)sNPanelPrincipal1.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)sNPanelPrincipal1.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

      //======== sNPanel2 ========
      {
        sNPanel2.setName("sNPanel2");
        sNPanel2.setLayout(new GridBagLayout());
        ((GridBagLayout)sNPanel2.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0, 0};
        ((GridBagLayout)sNPanel2.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)sNPanel2.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4};
        ((GridBagLayout)sNPanel2.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //---- lbDate ----
        lbDate.setText("Date");
        lbDate.setMaximumSize(new Dimension(100, 30));
        lbDate.setMinimumSize(new Dimension(100, 30));
        lbDate.setPreferredSize(new Dimension(100, 30));
        lbDate.setName("lbDate");
        sNPanel2.add(lbDate, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- DEMDAT ----
        DEMDAT.setPreferredSize(new Dimension(110, 28));
        DEMDAT.setMaximumSize(new Dimension(110, 28));
        DEMDAT.setMinimumSize(new Dimension(110, 28));
        DEMDAT.setName("DEMDAT");
        sNPanel2.add(DEMDAT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- lbVendeur ----
        lbVendeur.setText("Vendeur");
        lbVendeur.setName("lbVendeur");
        sNPanel2.add(lbVendeur, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- snVendeur ----
        snVendeur.setEnabled(false);
        snVendeur.setName("snVendeur");
        sNPanel2.add(snVendeur, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- lbEtablissement ----
        lbEtablissement.setText("Etablissement");
        lbEtablissement.setName("lbEtablissement");
        sNPanel2.add(lbEtablissement, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- snEtablissement ----
        snEtablissement.setEnabled(false);
        snEtablissement.setName("snEtablissement");
        sNPanel2.add(snEtablissement, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      sNPanelPrincipal1.add(sNPanel2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== pnlDetail ========
      {
        pnlDetail.setTitre("D\u00e9tail de la caisse");
        pnlDetail.setName("pnlDetail");
        pnlDetail.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlDetail.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0};
        ((GridBagLayout)pnlDetail.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
        ((GridBagLayout)pnlDetail.getLayout()).columnWeights = new double[] {0.0, 1.0, 0.0, 0.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnlDetail.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

        //---- lbFondDeCaisse ----
        lbFondDeCaisse.setText("Fond de caisse");
        lbFondDeCaisse.setBackground(new Color(239, 239, 222));
        lbFondDeCaisse.setName("lbFondDeCaisse");
        pnlDetail.add(lbFondDeCaisse, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- VALDEB ----
        VALDEB.setMaximumSize(new Dimension(100, 28));
        VALDEB.setMinimumSize(new Dimension(100, 28));
        VALDEB.setPreferredSize(new Dimension(100, 28));
        VALDEB.setFont(new Font("sansserif", Font.PLAIN, 14));
        VALDEB.setName("VALDEB");
        pnlDetail.add(VALDEB, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 5), 0, 0));

        //======== scrollPane1 ========
        {
          scrollPane1.setPreferredSize(new Dimension(456, 130));
          scrollPane1.setName("scrollPane1");

          //---- WTP01 ----
          WTP01.setComponentPopupMenu(BTD);
          WTP01.setModel(new DefaultTableModel(
            new Object[][] {
              {null},
              {null},
              {null},
              {null},
              {null},
              {null},
              {null},
              {null},
              {null},
              {null},
              {null},
              {null},
              {null},
              {null},
              {null},
            },
            new String[] {
              null
            }
          ));
          WTP01.setName("WTP01");
          scrollPane1.setViewportView(WTP01);
        }
        pnlDetail.add(scrollPane1, new GridBagConstraints(0, 1, 4, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //======== sNPanel1 ========
        {
          sNPanel1.setMinimumSize(new Dimension(28, 275));
          sNPanel1.setPreferredSize(new Dimension(28, 275));
          sNPanel1.setName("sNPanel1");
          sNPanel1.setLayout(new GridBagLayout());
          ((GridBagLayout)sNPanel1.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)sNPanel1.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
          ((GridBagLayout)sNPanel1.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
          ((GridBagLayout)sNPanel1.getLayout()).rowWeights = new double[] {0.0, 1.0, 1.0, 0.0, 1.0E-4};

          //---- BT_PDEB ----
          BT_PDEB.setToolTipText("Affichage au d\u00e9but de la liste");
          BT_PDEB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PDEB.setMinimumSize(new Dimension(28, 30));
          BT_PDEB.setMaximumSize(new Dimension(28, 30));
          BT_PDEB.setPreferredSize(new Dimension(28, 30));
          BT_PDEB.setName("BT_PDEB");
          BT_PDEB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              BT_PDEBActionPerformed(e);
            }
          });
          sNPanel1.add(BT_PDEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setMinimumSize(new Dimension(28, 90));
          BT_PGUP.setMaximumSize(new Dimension(28, 90));
          BT_PGUP.setPreferredSize(new Dimension(28, 90));
          BT_PGUP.setName("BT_PGUP");
          sNPanel1.add(BT_PGUP, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setMinimumSize(new Dimension(28, 90));
          BT_PGDOWN.setMaximumSize(new Dimension(28, 90));
          BT_PGDOWN.setPreferredSize(new Dimension(28, 90));
          BT_PGDOWN.setName("BT_PGDOWN");
          sNPanel1.add(BT_PGDOWN, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- BT_PFIN ----
          BT_PFIN.setToolTipText("Affichage \u00e0 la fin de la liste");
          BT_PFIN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PFIN.setPreferredSize(new Dimension(28, 30));
          BT_PFIN.setMinimumSize(new Dimension(28, 30));
          BT_PFIN.setMaximumSize(new Dimension(28, 30));
          BT_PFIN.setName("BT_PFIN");
          BT_PFIN.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              BT_PFINActionPerformed(e);
            }
          });
          sNPanel1.add(BT_PFIN, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDetail.add(sNPanel1, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbPagination ----
        lbPagination.setText("Pagination");
        lbPagination.setMaximumSize(new Dimension(100, 30));
        lbPagination.setMinimumSize(new Dimension(100, 30));
        lbPagination.setPreferredSize(new Dimension(100, 30));
        lbPagination.setName("lbPagination");
        pnlDetail.add(lbPagination, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- V06F ----
        V06F.setModel(new DefaultComboBoxModel(new String[] {
          "D\u00e9calage de 10 lignes en moins",
          "D\u00e9calage de 5 lignes en moins",
          "D\u00e9calage de 3 lignes en moins",
          "D\u00e9calage de 2 lignes en moins",
          "D\u00e9calage de 1 ligne en moins",
          "D\u00e9calage de 1 ligne en plus",
          "D\u00e9calage de 2 lignes en plus",
          "D\u00e9calage de 3 lignes en plus",
          "D\u00e9calage de 5 lignes en plus",
          "D\u00e9calage de 10 lignes en plus"
        }));
        V06F.setFont(new Font("sansserif", Font.PLAIN, 14));
        V06F.setName("V06F");
        V06F.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            V06FItemStateChanged(e);
          }
        });
        pnlDetail.add(V06F, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- lbTotal ----
        lbTotal.setText("Total");
        lbTotal.setName("lbTotal");
        pnlDetail.add(lbTotal, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- VALFIN ----
        VALFIN.setMaximumSize(new Dimension(100, 28));
        VALFIN.setMinimumSize(new Dimension(100, 28));
        VALFIN.setPreferredSize(new Dimension(100, 28));
        VALFIN.setFont(new Font("sansserif", Font.PLAIN, 14));
        VALFIN.setName("VALFIN");
        pnlDetail.add(VALFIN, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 5), 0, 0));
      }
      sNPanelPrincipal1.add(pnlDetail, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(sNPanelPrincipal1, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- miModification ----
      miModification.setText("Modification");
      miModification.setName("miModification");
      miModification.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miModificationActionPerformed(e);
        }
      });
      BTD.add(miModification);

      //---- miSuppression ----
      miSuppression.setText("Suppression");
      miSuppression.setName("miSuppression");
      miSuppression.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miSuppressionActionPerformed(e);
        }
      });
      BTD.add(miSuppression);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private JPanel p_presentation;
  private JPanel p_logo;
  private JLabel logoEtb;
  private JPanel p_titre;
  private JLabel titre;
  private JLabel fond_titre;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu sNPanelPrincipal1;
  private SNPanel sNPanel2;
  private SNLabelChamp lbDate;
  private XRiCalendrier DEMDAT;
  private SNLabelChamp lbVendeur;
  private SNVendeur snVendeur;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNPanelTitre pnlDetail;
  private SNLabelChamp lbFondDeCaisse;
  private XRiTextField VALDEB;
  private JScrollPane scrollPane1;
  private XRiTable WTP01;
  private SNPanel sNPanel1;
  private JButton BT_PDEB;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JButton BT_PFIN;
  private SNLabelChamp lbPagination;
  private XRiComboBox V06F;
  private SNLabelChamp lbTotal;
  private XRiTextField VALFIN;
  private JPopupMenu BTD;
  private JMenuItem miModification;
  private JMenuItem miSuppression;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
