
package ri.serien.libecranrpg.vcgm.VCGM01FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM01FM_4P extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM01FM_4P(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44_OBJ_45 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_45_OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46_OBJ_47 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_40_OBJ_40 = new JLabel();
    OBJ_43_OBJ_43 = new JLabel();
    P4F1X = new XRiTextField();
    P4D2X = new XRiTextField();
    P4F2X = new XRiTextField();
    P4D3X = new XRiTextField();
    P4F3X = new XRiTextField();
    P4D4X = new XRiTextField();
    P4F4X = new XRiTextField();
    P4D5X = new XRiTextField();
    P4F5X = new XRiTextField();
    P4D6X = new XRiTextField();
    P4F6X = new XRiTextField();
    P4D7X = new XRiTextField();
    P4F7X = new XRiTextField();
    P4D8X = new XRiTextField();
    P4F8X = new XRiTextField();
    P4D9X = new XRiTextField();
    P4F9X = new XRiTextField();
    P4D10X = new XRiTextField();
    P4F10X = new XRiTextField();
    P4D11X = new XRiTextField();
    P4F11X = new XRiTextField();
    P4D12X = new XRiTextField();
    P4F12X = new XRiTextField();
    P4D1X = new XRiTextField();
    OBJ_41_OBJ_41 = new JLabel();
    OBJ_44_OBJ_44 = new JLabel();
    OBJ_77_OBJ_77 = new JLabel();
    OBJ_79_OBJ_79 = new JLabel();
    OBJ_81_OBJ_81 = new JLabel();
    OBJ_42_OBJ_42 = new JLabel();
    OBJ_45_OBJ_45 = new JLabel();
    OBJ_46_OBJ_46 = new JLabel();
    OBJ_71_OBJ_71 = new JLabel();
    OBJ_72_OBJ_72 = new JLabel();
    OBJ_73_OBJ_73 = new JLabel();
    OBJ_74_OBJ_74 = new JLabel();
    OBJ_75_OBJ_75 = new JLabel();
    OBJ_76_OBJ_76 = new JLabel();
    OBJ_78_OBJ_78 = new JLabel();
    OBJ_80_OBJ_80 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44_OBJ_45 ----
          OBJ_44_OBJ_45.setText("Soci\u00e9t\u00e9");
          OBJ_44_OBJ_45.setName("OBJ_44_OBJ_45");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setName("INDETB");

          //---- OBJ_45_OBJ_46 ----
          OBJ_45_OBJ_46.setText("Code");
          OBJ_45_OBJ_46.setName("OBJ_45_OBJ_46");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setName("INDTYP");

          //---- OBJ_46_OBJ_47 ----
          OBJ_46_OBJ_47.setText("Ordre");
          OBJ_46_OBJ_47.setName("OBJ_46_OBJ_47");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(620, 320));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("P\u00e9riodes compta 4.4.5");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

            //---- OBJ_40_OBJ_40 ----
            OBJ_40_OBJ_40.setText("P\u00e9riodes");
            OBJ_40_OBJ_40.setFont(OBJ_40_OBJ_40.getFont().deriveFont(OBJ_40_OBJ_40.getFont().getStyle() | Font.BOLD));
            OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");

            //---- OBJ_43_OBJ_43 ----
            OBJ_43_OBJ_43.setText("P\u00e9riodes");
            OBJ_43_OBJ_43.setFont(OBJ_43_OBJ_43.getFont().deriveFont(OBJ_43_OBJ_43.getFont().getStyle() | Font.BOLD));
            OBJ_43_OBJ_43.setName("OBJ_43_OBJ_43");

            //---- P4F1X ----
            P4F1X.setComponentPopupMenu(BTD);
            P4F1X.setName("P4F1X");

            //---- P4D2X ----
            P4D2X.setComponentPopupMenu(BTD);
            P4D2X.setName("P4D2X");

            //---- P4F2X ----
            P4F2X.setComponentPopupMenu(BTD);
            P4F2X.setName("P4F2X");

            //---- P4D3X ----
            P4D3X.setComponentPopupMenu(BTD);
            P4D3X.setName("P4D3X");

            //---- P4F3X ----
            P4F3X.setComponentPopupMenu(BTD);
            P4F3X.setName("P4F3X");

            //---- P4D4X ----
            P4D4X.setComponentPopupMenu(BTD);
            P4D4X.setName("P4D4X");

            //---- P4F4X ----
            P4F4X.setComponentPopupMenu(BTD);
            P4F4X.setName("P4F4X");

            //---- P4D5X ----
            P4D5X.setComponentPopupMenu(BTD);
            P4D5X.setName("P4D5X");

            //---- P4F5X ----
            P4F5X.setComponentPopupMenu(BTD);
            P4F5X.setName("P4F5X");

            //---- P4D6X ----
            P4D6X.setComponentPopupMenu(BTD);
            P4D6X.setName("P4D6X");

            //---- P4F6X ----
            P4F6X.setComponentPopupMenu(BTD);
            P4F6X.setName("P4F6X");

            //---- P4D7X ----
            P4D7X.setComponentPopupMenu(BTD);
            P4D7X.setName("P4D7X");

            //---- P4F7X ----
            P4F7X.setComponentPopupMenu(BTD);
            P4F7X.setName("P4F7X");

            //---- P4D8X ----
            P4D8X.setComponentPopupMenu(BTD);
            P4D8X.setName("P4D8X");

            //---- P4F8X ----
            P4F8X.setComponentPopupMenu(BTD);
            P4F8X.setName("P4F8X");

            //---- P4D9X ----
            P4D9X.setComponentPopupMenu(BTD);
            P4D9X.setName("P4D9X");

            //---- P4F9X ----
            P4F9X.setComponentPopupMenu(BTD);
            P4F9X.setName("P4F9X");

            //---- P4D10X ----
            P4D10X.setComponentPopupMenu(BTD);
            P4D10X.setName("P4D10X");

            //---- P4F10X ----
            P4F10X.setComponentPopupMenu(BTD);
            P4F10X.setName("P4F10X");

            //---- P4D11X ----
            P4D11X.setComponentPopupMenu(BTD);
            P4D11X.setName("P4D11X");

            //---- P4F11X ----
            P4F11X.setComponentPopupMenu(BTD);
            P4F11X.setName("P4F11X");

            //---- P4D12X ----
            P4D12X.setComponentPopupMenu(BTD);
            P4D12X.setName("P4D12X");

            //---- P4F12X ----
            P4F12X.setComponentPopupMenu(BTD);
            P4F12X.setName("P4F12X");

            //---- P4D1X ----
            P4D1X.setComponentPopupMenu(BTD);
            P4D1X.setName("P4D1X");

            //---- OBJ_41_OBJ_41 ----
            OBJ_41_OBJ_41.setText("D\u00e9but");
            OBJ_41_OBJ_41.setFont(OBJ_41_OBJ_41.getFont().deriveFont(OBJ_41_OBJ_41.getFont().getStyle() | Font.BOLD));
            OBJ_41_OBJ_41.setName("OBJ_41_OBJ_41");

            //---- OBJ_44_OBJ_44 ----
            OBJ_44_OBJ_44.setText("D\u00e9but");
            OBJ_44_OBJ_44.setFont(OBJ_44_OBJ_44.getFont().deriveFont(OBJ_44_OBJ_44.getFont().getStyle() | Font.BOLD));
            OBJ_44_OBJ_44.setName("OBJ_44_OBJ_44");

            //---- OBJ_77_OBJ_77 ----
            OBJ_77_OBJ_77.setText("P10");
            OBJ_77_OBJ_77.setName("OBJ_77_OBJ_77");

            //---- OBJ_79_OBJ_79 ----
            OBJ_79_OBJ_79.setText("P11");
            OBJ_79_OBJ_79.setName("OBJ_79_OBJ_79");

            //---- OBJ_81_OBJ_81 ----
            OBJ_81_OBJ_81.setText("P12");
            OBJ_81_OBJ_81.setName("OBJ_81_OBJ_81");

            //---- OBJ_42_OBJ_42 ----
            OBJ_42_OBJ_42.setText("Fin");
            OBJ_42_OBJ_42.setFont(OBJ_42_OBJ_42.getFont().deriveFont(OBJ_42_OBJ_42.getFont().getStyle() | Font.BOLD));
            OBJ_42_OBJ_42.setName("OBJ_42_OBJ_42");

            //---- OBJ_45_OBJ_45 ----
            OBJ_45_OBJ_45.setText("Fin");
            OBJ_45_OBJ_45.setFont(OBJ_45_OBJ_45.getFont().deriveFont(OBJ_45_OBJ_45.getFont().getStyle() | Font.BOLD));
            OBJ_45_OBJ_45.setName("OBJ_45_OBJ_45");

            //---- OBJ_46_OBJ_46 ----
            OBJ_46_OBJ_46.setText("P1");
            OBJ_46_OBJ_46.setName("OBJ_46_OBJ_46");

            //---- OBJ_71_OBJ_71 ----
            OBJ_71_OBJ_71.setText("P7");
            OBJ_71_OBJ_71.setName("OBJ_71_OBJ_71");

            //---- OBJ_72_OBJ_72 ----
            OBJ_72_OBJ_72.setText("P2");
            OBJ_72_OBJ_72.setName("OBJ_72_OBJ_72");

            //---- OBJ_73_OBJ_73 ----
            OBJ_73_OBJ_73.setText("P8");
            OBJ_73_OBJ_73.setName("OBJ_73_OBJ_73");

            //---- OBJ_74_OBJ_74 ----
            OBJ_74_OBJ_74.setText("P3");
            OBJ_74_OBJ_74.setName("OBJ_74_OBJ_74");

            //---- OBJ_75_OBJ_75 ----
            OBJ_75_OBJ_75.setText("P9");
            OBJ_75_OBJ_75.setName("OBJ_75_OBJ_75");

            //---- OBJ_76_OBJ_76 ----
            OBJ_76_OBJ_76.setText("P4");
            OBJ_76_OBJ_76.setName("OBJ_76_OBJ_76");

            //---- OBJ_78_OBJ_78 ----
            OBJ_78_OBJ_78.setText("P5");
            OBJ_78_OBJ_78.setName("OBJ_78_OBJ_78");

            //---- OBJ_80_OBJ_80 ----
            OBJ_80_OBJ_80.setText("P6");
            OBJ_80_OBJ_80.setName("OBJ_80_OBJ_80");

            GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
            xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
            xTitledPanel1ContentContainerLayout.setHorizontalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(35, 35, 35)
                      .addComponent(OBJ_40_OBJ_40, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                      .addGap(17, 17, 17)
                      .addComponent(OBJ_41_OBJ_41, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                      .addGap(36, 36, 36)
                      .addComponent(OBJ_42_OBJ_42, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                      .addGap(94, 94, 94)
                      .addComponent(OBJ_43_OBJ_43, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                      .addGap(17, 17, 17)
                      .addComponent(OBJ_44_OBJ_44, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                      .addGap(41, 41, 41)
                      .addComponent(OBJ_45_OBJ_45, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(OBJ_46_OBJ_46, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                      .addGap(31, 31, 31)
                      .addComponent(P4D1X, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addGap(20, 20, 20)
                      .addComponent(P4F1X, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addGap(95, 95, 95)
                      .addComponent(OBJ_71_OBJ_71, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(P4D7X, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addGap(20, 20, 20)
                      .addComponent(P4F7X, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(OBJ_72_OBJ_72, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                      .addGap(31, 31, 31)
                      .addComponent(P4D2X, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addGap(20, 20, 20)
                      .addComponent(P4F2X, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addGap(95, 95, 95)
                      .addComponent(OBJ_73_OBJ_73, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(P4D8X, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addGap(20, 20, 20)
                      .addComponent(P4F8X, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(OBJ_74_OBJ_74, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                      .addGap(31, 31, 31)
                      .addComponent(P4D3X, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addGap(20, 20, 20)
                      .addComponent(P4F3X, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addGap(95, 95, 95)
                      .addComponent(OBJ_75_OBJ_75, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(P4D9X, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addGap(20, 20, 20)
                      .addComponent(P4F9X, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(OBJ_76_OBJ_76, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                      .addGap(31, 31, 31)
                      .addComponent(P4D4X, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addGap(20, 20, 20)
                      .addComponent(P4F4X, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addGap(95, 95, 95)
                      .addComponent(OBJ_77_OBJ_77, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(P4D10X, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addGap(20, 20, 20)
                      .addComponent(P4F10X, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(OBJ_78_OBJ_78, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                      .addGap(31, 31, 31)
                      .addComponent(P4D5X, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addGap(20, 20, 20)
                      .addComponent(P4F5X, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addGap(95, 95, 95)
                      .addComponent(OBJ_79_OBJ_79, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(P4D11X, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addGap(20, 20, 20)
                      .addComponent(P4F11X, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(OBJ_80_OBJ_80, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                      .addGap(31, 31, 31)
                      .addComponent(P4D6X, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addGap(20, 20, 20)
                      .addComponent(P4F6X, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addGap(95, 95, 95)
                      .addComponent(OBJ_81_OBJ_81, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(P4D12X, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addGap(20, 20, 20)
                      .addComponent(P4F12X, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)))
                  .addContainerGap(99, Short.MAX_VALUE))
            );
            xTitledPanel1ContentContainerLayout.setVerticalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_40_OBJ_40)
                    .addComponent(OBJ_41_OBJ_41)
                    .addComponent(OBJ_42_OBJ_42)
                    .addComponent(OBJ_43_OBJ_43)
                    .addComponent(OBJ_44_OBJ_44)
                    .addComponent(OBJ_45_OBJ_45))
                  .addGap(9, 9, 9)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(P4D1X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P4F1X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P4D7X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P4F7X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_46_OBJ_46)
                        .addComponent(OBJ_71_OBJ_71))))
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(P4D2X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P4F2X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P4D8X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P4F8X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_72_OBJ_72)
                        .addComponent(OBJ_73_OBJ_73))))
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(P4D3X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P4F3X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P4D9X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P4F9X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_74_OBJ_74)
                        .addComponent(OBJ_75_OBJ_75))))
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(P4D4X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P4F4X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P4D10X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P4F10X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_76_OBJ_76)
                        .addComponent(OBJ_77_OBJ_77))))
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(P4D5X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P4F5X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P4D11X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P4F11X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_78_OBJ_78)
                        .addComponent(OBJ_79_OBJ_79))))
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(P4D6X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P4F6X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P4D12X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P4F12X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_80_OBJ_80)
                        .addComponent(OBJ_81_OBJ_81))))
                  .addContainerGap(40, Short.MAX_VALUE))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44_OBJ_45;
  private XRiTextField INDETB;
  private JLabel OBJ_45_OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_46_OBJ_47;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_40_OBJ_40;
  private JLabel OBJ_43_OBJ_43;
  private XRiTextField P4F1X;
  private XRiTextField P4D2X;
  private XRiTextField P4F2X;
  private XRiTextField P4D3X;
  private XRiTextField P4F3X;
  private XRiTextField P4D4X;
  private XRiTextField P4F4X;
  private XRiTextField P4D5X;
  private XRiTextField P4F5X;
  private XRiTextField P4D6X;
  private XRiTextField P4F6X;
  private XRiTextField P4D7X;
  private XRiTextField P4F7X;
  private XRiTextField P4D8X;
  private XRiTextField P4F8X;
  private XRiTextField P4D9X;
  private XRiTextField P4F9X;
  private XRiTextField P4D10X;
  private XRiTextField P4F10X;
  private XRiTextField P4D11X;
  private XRiTextField P4F11X;
  private XRiTextField P4D12X;
  private XRiTextField P4F12X;
  private XRiTextField P4D1X;
  private JLabel OBJ_41_OBJ_41;
  private JLabel OBJ_44_OBJ_44;
  private JLabel OBJ_77_OBJ_77;
  private JLabel OBJ_79_OBJ_79;
  private JLabel OBJ_81_OBJ_81;
  private JLabel OBJ_42_OBJ_42;
  private JLabel OBJ_45_OBJ_45;
  private JLabel OBJ_46_OBJ_46;
  private JLabel OBJ_71_OBJ_71;
  private JLabel OBJ_72_OBJ_72;
  private JLabel OBJ_73_OBJ_73;
  private JLabel OBJ_74_OBJ_74;
  private JLabel OBJ_75_OBJ_75;
  private JLabel OBJ_76_OBJ_76;
  private JLabel OBJ_78_OBJ_78;
  private JLabel OBJ_80_OBJ_80;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
