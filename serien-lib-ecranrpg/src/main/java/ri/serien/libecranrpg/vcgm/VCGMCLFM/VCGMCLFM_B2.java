
package ri.serien.libecranrpg.vcgm.VCGMCLFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGMCLFM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] CLETAT_Value = { "0", "1", };
  private String[] CLETAT_Title = { "Attente", "Homologuée", };
  
  public VCGMCLFM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    CLETAT.setValeurs(CLETAT_Value, CLETAT_Title);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    CLTYP3.setEnabled(lexique.isPresent("CLTYP3"));
    CLTYP2.setEnabled(lexique.isPresent("CLTYP2"));
    CLTYP1.setEnabled(lexique.isPresent("CLTYP1"));
    CLCJO5.setEnabled(lexique.isPresent("CLCJO5"));
    CLCJO4.setEnabled(lexique.isPresent("CLCJO4"));
    CLCJO3.setEnabled(lexique.isPresent("CLCJO3"));
    CLCJO2.setEnabled(lexique.isPresent("CLCJO2"));
    CLCJO1.setEnabled(lexique.isPresent("CLCJO1"));
    CLCFO5.setEnabled(lexique.isPresent("CLCFO5"));
    CLCFO4.setEnabled(lexique.isPresent("CLCFO4"));
    CLCFO3.setEnabled(lexique.isPresent("CLCFO3"));
    CLCFO2.setEnabled(lexique.isPresent("CLCFO2"));
    CLCFO1.setEnabled(lexique.isPresent("CLCFO1"));
    // CLETAT.setSelectedIndex(getIndice("CLETAT", CLETAT_Value));
    // CLETAT.setEnabled( lexique.isPresent("CLETAT"));
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Critères de sélection"));
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("CLETAT", 0, CLETAT_Value[CLETAT.getSelectedIndex()]);
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    CLETAT = new XRiComboBox();
    OBJ_42 = new JLabel();
    OBJ_43 = new JLabel();
    OBJ_44 = new JLabel();
    CLCFO1 = new XRiTextField();
    CLCFO2 = new XRiTextField();
    CLCFO3 = new XRiTextField();
    CLCFO4 = new XRiTextField();
    CLCFO5 = new XRiTextField();
    OBJ_25 = new JLabel();
    CLCJO1 = new XRiTextField();
    CLCJO2 = new XRiTextField();
    CLCJO3 = new XRiTextField();
    CLCJO4 = new XRiTextField();
    CLCJO5 = new XRiTextField();
    CLTYP1 = new XRiTextField();
    CLTYP2 = new XRiTextField();
    CLTYP3 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(610, 205));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Lignes d'\u00e9critures"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- CLETAT ----
          CLETAT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CLETAT.setName("CLETAT");
          panel1.add(CLETAT);
          CLETAT.setBounds(135, 36, 138, CLETAT.getPreferredSize().height);

          //---- OBJ_42 ----
          OBJ_42.setText("Type de Lignes");
          OBJ_42.setName("OBJ_42");
          panel1.add(OBJ_42);
          OBJ_42.setBounds(25, 69, 97, 20);

          //---- OBJ_43 ----
          OBJ_43.setText("Journaux");
          OBJ_43.setName("OBJ_43");
          panel1.add(OBJ_43);
          OBJ_43.setBounds(25, 99, 76, 20);

          //---- OBJ_44 ----
          OBJ_44.setText("Folios");
          OBJ_44.setName("OBJ_44");
          panel1.add(OBJ_44);
          OBJ_44.setBounds(25, 129, 58, 20);

          //---- CLCFO1 ----
          CLCFO1.setComponentPopupMenu(BTD);
          CLCFO1.setName("CLCFO1");
          panel1.add(CLCFO1);
          CLCFO1.setBounds(135, 125, 50, CLCFO1.getPreferredSize().height);

          //---- CLCFO2 ----
          CLCFO2.setComponentPopupMenu(BTD);
          CLCFO2.setName("CLCFO2");
          panel1.add(CLCFO2);
          CLCFO2.setBounds(186, 125, 50, CLCFO2.getPreferredSize().height);

          //---- CLCFO3 ----
          CLCFO3.setComponentPopupMenu(BTD);
          CLCFO3.setName("CLCFO3");
          panel1.add(CLCFO3);
          CLCFO3.setBounds(237, 125, 50, CLCFO3.getPreferredSize().height);

          //---- CLCFO4 ----
          CLCFO4.setComponentPopupMenu(BTD);
          CLCFO4.setName("CLCFO4");
          panel1.add(CLCFO4);
          CLCFO4.setBounds(288, 125, 50, CLCFO4.getPreferredSize().height);

          //---- CLCFO5 ----
          CLCFO5.setComponentPopupMenu(BTD);
          CLCFO5.setName("CLCFO5");
          panel1.add(CLCFO5);
          CLCFO5.setBounds(339, 125, 50, CLCFO5.getPreferredSize().height);

          //---- OBJ_25 ----
          OBJ_25.setText("Etat");
          OBJ_25.setName("OBJ_25");
          panel1.add(OBJ_25);
          OBJ_25.setBounds(25, 39, 27, 20);

          //---- CLCJO1 ----
          CLCJO1.setComponentPopupMenu(BTD);
          CLCJO1.setName("CLCJO1");
          panel1.add(CLCJO1);
          CLCJO1.setBounds(135, 95, 30, CLCJO1.getPreferredSize().height);

          //---- CLCJO2 ----
          CLCJO2.setComponentPopupMenu(BTD);
          CLCJO2.setName("CLCJO2");
          panel1.add(CLCJO2);
          CLCJO2.setBounds(166, 95, 30, CLCJO2.getPreferredSize().height);

          //---- CLCJO3 ----
          CLCJO3.setComponentPopupMenu(BTD);
          CLCJO3.setName("CLCJO3");
          panel1.add(CLCJO3);
          CLCJO3.setBounds(197, 95, 30, CLCJO3.getPreferredSize().height);

          //---- CLCJO4 ----
          CLCJO4.setComponentPopupMenu(BTD);
          CLCJO4.setName("CLCJO4");
          panel1.add(CLCJO4);
          CLCJO4.setBounds(228, 95, 30, CLCJO4.getPreferredSize().height);

          //---- CLCJO5 ----
          CLCJO5.setComponentPopupMenu(BTD);
          CLCJO5.setName("CLCJO5");
          panel1.add(CLCJO5);
          CLCJO5.setBounds(259, 95, 30, CLCJO5.getPreferredSize().height);

          //---- CLTYP1 ----
          CLTYP1.setComponentPopupMenu(BTD);
          CLTYP1.setName("CLTYP1");
          panel1.add(CLTYP1);
          CLTYP1.setBounds(135, 65, 18, CLTYP1.getPreferredSize().height);

          //---- CLTYP2 ----
          CLTYP2.setComponentPopupMenu(BTD);
          CLTYP2.setName("CLTYP2");
          panel1.add(CLTYP2);
          CLTYP2.setBounds(155, 65, 18, CLTYP2.getPreferredSize().height);

          //---- CLTYP3 ----
          CLTYP3.setComponentPopupMenu(BTD);
          CLTYP3.setName("CLTYP3");
          panel1.add(CLTYP3);
          CLTYP3.setBounds(175, 65, 18, CLTYP3.getPreferredSize().height);
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 415, 185);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_18 ----
      OBJ_18.setText("Invite");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiComboBox CLETAT;
  private JLabel OBJ_42;
  private JLabel OBJ_43;
  private JLabel OBJ_44;
  private XRiTextField CLCFO1;
  private XRiTextField CLCFO2;
  private XRiTextField CLCFO3;
  private XRiTextField CLCFO4;
  private XRiTextField CLCFO5;
  private JLabel OBJ_25;
  private XRiTextField CLCJO1;
  private XRiTextField CLCJO2;
  private XRiTextField CLCJO3;
  private XRiTextField CLCJO4;
  private XRiTextField CLCJO5;
  private XRiTextField CLTYP1;
  private XRiTextField CLTYP2;
  private XRiTextField CLTYP3;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
