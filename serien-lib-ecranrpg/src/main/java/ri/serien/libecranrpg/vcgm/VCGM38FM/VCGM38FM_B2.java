
package ri.serien.libecranrpg.vcgm.VCGM38FM;
// Nom Fichier: pop_VCGM38FM_FMTB2_834.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VCGM38FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  private int[] justif = { SwingConstants.LEFT, SwingConstants.LEFT, SwingConstants.LEFT, SwingConstants.LEFT, SwingConstants.LEFT,
      SwingConstants.RIGHT, SwingConstants.LEFT, SwingConstants.RIGHT, SwingConstants.RIGHT };
  private String[] _J01_Title = { "JO", "Fol.", "Date", "User", "Action", "Ligne", "Date act", "Total Débit", "Total Crédit", };
  private String[][] _J01_Data = { { "J01", "F01", "E01", "U01", "T01", "L01", "D01", "M01", "C01", },
      { "J02", "F02", "E02", "U02", "T02", "L02", "D02", "M02", "C02", },
      { "J03", "F03", "E03", "U03", "T03", "L03", "D03", "M03", "C03", },
      { "J04", "F04", "E04", "U04", "T04", "L04", "D04", "M04", "C04", },
      { "J05", "F05", "E05", "U05", "T05", "L05", "D05", "M05", "C05", },
      { "J06", "F06", "E06", "U06", "T06", "L06", "D06", "M06", "C06", },
      { "J07", "F07", "E07", "U07", "T07", "L07", "D07", "M07", "C07", },
      { "J08", "F08", "E08", "U08", "T08", "L08", "D08", "M08", "C08", },
      { "J09", "F09", "E09", "U09", "T09", "L09", "D09", "M09", "C09", },
      { "J10", "F10", "E10", "U10", "T10", "L10", "D10", "M10", "C10", },
      { "J11", "F11", "E11", "U11", "T11", "L11", "D11", "M11", "C11", },
      { "J12", "F12", "E12", "U12", "T12", "L12", "D12", "M12", "C12", },
      { "J13", "F13", "E13", "U13", "T13", "L13", "D13", "M13", "C13", },
      { "J14", "F14", "E14", "U14", "T14", "L14", "D14", "M14", "C14", },
      { "J15", "F15", "E15", "U15", "T15", "L15", "D15", "M15", "C15", }, };
  private int[] _J01_Width = { 23, 34, 60, 72, 45, 40, 64, 98, 98, };
  
  public VCGM38FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    J01.setAspectTable(null, _J01_Title, _J01_Data, _J01_Width, false, justif, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Ajoute à la liste des oData les variables non liées directement à un composant graphique
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), null,justif);
    
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    // WDATF.setVisible( lexique.isPresent("WDATF"));
    // WDATD.setVisible( lexique.isPresent("WDATD"));
    WUSER.setVisible(lexique.isPresent("WUSER"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Recherche des folios"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_31ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD2.getInvoker().getName());
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    int ligne = 0;
    ligne = J01.getSelectedRow() + 6;
    lexique.HostFieldPutData("XXLIG", 1, String.valueOf(ligne));
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void J01MouseClicked(MouseEvent e) {
    if ((e.getClickCount() == 2)) {
      int ligne = 0;
      ligne = J01.getSelectedRow() + 6;
      lexique.HostFieldPutData("XXLIG", 1, String.valueOf(ligne));
      lexique.HostScreenSendKey(this, "F4", true);
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    J01 = new XRiTable();
    WUSER = new XRiTextField();
    WDATD = new XRiCalendrier();
    WDATF = new XRiCalendrier();
    OBJ_17_OBJ_17 = new JLabel();
    OBJ_15_OBJ_15 = new JLabel();
    OBJ_16_OBJ_16 = new JLabel();
    xTitledSeparator1 = new JXTitledSeparator();
    BT_PGDOWN = new JButton();
    BT_PGUP = new JButton();
    BTD2 = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_31 = new JButton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1010, 375));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //======== SCROLLPANE_LIST ========
          {
            SCROLLPANE_LIST.setComponentPopupMenu(BTD);
            SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

            //---- J01 ----
            J01.setPreferredSize(new Dimension(553, 240));
            J01.setComponentPopupMenu(BTD);
            J01.setName("J01");
            J01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                J01MouseClicked(e);
              }
            });
            SCROLLPANE_LIST.setViewportView(J01);
          }
          p_recup.add(SCROLLPANE_LIST);
          SCROLLPANE_LIST.setBounds(20, 55, 753, 270);

          //---- WUSER ----
          WUSER.setComponentPopupMenu(BTD2);
          WUSER.setName("WUSER");
          p_recup.add(WUSER);
          WUSER.setBounds(455, 20, 110, WUSER.getPreferredSize().height);

          //---- WDATD ----
          WDATD.setComponentPopupMenu(BTD2);
          WDATD.setName("WDATD");
          p_recup.add(WDATD);
          WDATD.setBounds(110, 20, 105, WDATD.getPreferredSize().height);

          //---- WDATF ----
          WDATF.setComponentPopupMenu(BTD2);
          WDATF.setName("WDATF");
          p_recup.add(WDATF);
          WDATF.setBounds(255, 20, 105, WDATF.getPreferredSize().height);

          //---- OBJ_17_OBJ_17 ----
          OBJ_17_OBJ_17.setText("Utilisateur");
          OBJ_17_OBJ_17.setName("OBJ_17_OBJ_17");
          p_recup.add(OBJ_17_OBJ_17);
          OBJ_17_OBJ_17.setBounds(380, 24, 75, 20);

          //---- OBJ_15_OBJ_15 ----
          OBJ_15_OBJ_15.setText("du");
          OBJ_15_OBJ_15.setName("OBJ_15_OBJ_15");
          p_recup.add(OBJ_15_OBJ_15);
          OBJ_15_OBJ_15.setBounds(85, 24, 25, 20);

          //---- OBJ_16_OBJ_16 ----
          OBJ_16_OBJ_16.setText("au");
          OBJ_16_OBJ_16.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_16_OBJ_16.setName("OBJ_16_OBJ_16");
          p_recup.add(OBJ_16_OBJ_16);
          OBJ_16_OBJ_16.setBounds(220, 24, 30, 20);

          //---- xTitledSeparator1 ----
          xTitledSeparator1.setTitle("Folios");
          xTitledSeparator1.setName("xTitledSeparator1");
          p_recup.add(xTitledSeparator1);
          xTitledSeparator1.setBounds(20, 24, 55, 20);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          p_recup.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(780, 200, 25, 125);

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          p_recup.add(BT_PGUP);
          BT_PGUP.setBounds(780, 55, 25, 125);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_recup.getComponentCount(); i++) {
              Rectangle bounds = p_recup.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_recup.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_recup.setMinimumSize(preferredSize);
            p_recup.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 825, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 350, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_5);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_7 ----
      OBJ_7.setText("Aide en ligne");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      BTD.add(OBJ_7);
    }

    //---- OBJ_31 ----
    OBJ_31.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_31.setName("OBJ_31");
    OBJ_31.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_31ActionPerformed(e);
      }
    });
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable J01;
  private XRiTextField WUSER;
  private XRiCalendrier WDATD;
  private XRiCalendrier WDATF;
  private JLabel OBJ_17_OBJ_17;
  private JLabel OBJ_15_OBJ_15;
  private JLabel OBJ_16_OBJ_16;
  private JXTitledSeparator xTitledSeparator1;
  private JButton BT_PGDOWN;
  private JButton BT_PGUP;
  private JPopupMenu BTD2;
  private JMenuItem OBJ_5;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_7;
  private JButton OBJ_31;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
