
package ri.serien.libecranrpg.vcgm.VCGM44FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Emmanuel MARCQ
 */
public class VCGM44FM_L2 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private ImageIcon fleche = null;
  private ImageIcon fleche_bas = null;
  
  /**
   * Constructeur.
   */
  public VCGM44FM_L2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    
    // Titre
    setTitle("FOLIO COMPTABILITE");
    
    // Chargement des icones
    fleche = lexique.chargerImage("images/fleche.png", true);
    fleche_bas = lexique.chargerImage("images/fleche_bas.png", true);
    ImageIcon erreurN = lexique.chargerImage("images/erreurs_N.png", true);
    E01.setIcon(erreurN);
    E02.setIcon(erreurN);
    E03.setIcon(erreurN);
    E04.setIcon(erreurN);
    E05.setIcon(erreurN);
    E06.setIcon(erreurN);
    E07.setIcon(erreurN);
    E08.setIcon(erreurN);
    E09.setIcon(erreurN);
    E10.setIcon(erreurN);
    E11.setIcon(erreurN);
    E12.setIcon(erreurN);
    E13.setIcon(erreurN);
    E14.setIcon(erreurN);
    E15.setIcon(erreurN);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("page @WPAGE@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    if (J01.isEditable()) {
      bt_01.setIcon(fleche);
    }
    else {
      bt_01.setIcon(fleche_bas);
    }
    
    if (C01.isEditable()) {
      bt_02.setIcon(fleche);
    }
    else {
      bt_02.setIcon(fleche_bas);
    }
    
    if (B01.isEditable()) {
      bt_04.setIcon(fleche);
    }
    else {
      bt_04.setIcon(fleche_bas);
    }
    
    if (P01.isEditable()) {
      bt_05.setIcon(fleche);
    }
    else {
      bt_05.setIcon(fleche_bas);
    }
    
    if (R01.isEditable()) {
      bt_06.setIcon(fleche);
    }
    else {
      bt_06.setIcon(fleche_bas);
    }
    
    if (N01.isEditable()) {
      bt_07.setIcon(fleche);
    }
    else {
      bt_07.setIcon(fleche_bas);
    }
    
    if (S01.isEditable()) {
      bt_08.setIcon(fleche);
    }
    else {
      bt_08.setIcon(fleche_bas);
    }
    
    if (F01.isEditable()) {
      bt_09.setIcon(fleche);
    }
    else {
      bt_09.setIcon(fleche_bas);
    }
    
    if (NAT01.isEditable()) {
      bt_10.setIcon(fleche);
    }
    else {
      bt_10.setIcon(fleche_bas);
    }
    
    if (MD01.isEditable()) {
      bt_11.setIcon(fleche);
    }
    else {
      bt_11.setIcon(fleche_bas);
    }
    
    if (MC01.isEditable()) {
      bt_12.setIcon(fleche);
    }
    else {
      bt_12.setIcon(fleche_bas);
    }
    
    if (U01.isEditable()) {
      bt_13.setIcon(fleche);
    }
    else {
      bt_13.setIcon(fleche_bas);
    }
    
    E01.setVisible(!lexique.HostFieldGetData("E01").trim().equals(""));
    E02.setVisible(!lexique.HostFieldGetData("E02").trim().equals(""));
    E03.setVisible(!lexique.HostFieldGetData("E03").trim().equals(""));
    E04.setVisible(!lexique.HostFieldGetData("E04").trim().equals(""));
    E05.setVisible(!lexique.HostFieldGetData("E05").trim().equals(""));
    E06.setVisible(!lexique.HostFieldGetData("E06").trim().equals(""));
    E07.setVisible(!lexique.HostFieldGetData("E07").trim().equals(""));
    E08.setVisible(!lexique.HostFieldGetData("E08").trim().equals(""));
    E09.setVisible(!lexique.HostFieldGetData("E09").trim().equals(""));
    E10.setVisible(!lexique.HostFieldGetData("E10").trim().equals(""));
    E11.setVisible(!lexique.HostFieldGetData("E11").trim().equals(""));
    E12.setVisible(!lexique.HostFieldGetData("E12").trim().equals(""));
    E13.setVisible(!lexique.HostFieldGetData("E13").trim().equals(""));
    E14.setVisible(!lexique.HostFieldGetData("E14").trim().equals(""));
    E15.setVisible(!lexique.HostFieldGetData("E15").trim().equals(""));
    
    p_bpresentation.setCodeEtablissement(E1SOC.getText());
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTDA.getInvoker().getName());
  }
  
  private void bt_01ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void bt_02ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F16");
  }
  
  private void bt_04ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F17");
  }
  
  private void bt_05ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18");
  }
  
  private void bt_06ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19");
  }
  
  private void bt_07ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19");
  }
  
  private void bt_08ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void bt_09ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19");
  }
  
  private void bt_10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19");
  }
  
  private void bt_11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void bt_12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bt_13ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    JOLIB = new XRiTextField();
    E1DTEX = new XRiTextField();
    OBJ_32 = new JLabel();
    OBJ_34 = new JLabel();
    E1SOC = new XRiTextField();
    OBJ_37 = new JLabel();
    E1CFO = new XRiTextField();
    E1CJO = new XRiTextField();
    OBJ_39 = new JLabel();
    p_tete_droite = new JPanel();
    label1 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    J01 = new XRiTextField();
    C01 = new XRiTextField();
    T01 = new XRiTextField();
    B01 = new XRiTextField();
    P01 = new XRiTextField();
    R01 = new XRiTextField();
    N01 = new XRiTextField();
    S01 = new XRiTextField();
    F01 = new XRiTextField();
    MD01 = new XRiTextField();
    MC01 = new XRiTextField();
    J02 = new XRiTextField();
    C02 = new XRiTextField();
    T02 = new XRiTextField();
    B02 = new XRiTextField();
    P02 = new XRiTextField();
    R02 = new XRiTextField();
    N02 = new XRiTextField();
    S02 = new XRiTextField();
    F02 = new XRiTextField();
    MD02 = new XRiTextField();
    MC02 = new XRiTextField();
    J03 = new XRiTextField();
    C03 = new XRiTextField();
    T03 = new XRiTextField();
    B03 = new XRiTextField();
    P03 = new XRiTextField();
    R03 = new XRiTextField();
    N03 = new XRiTextField();
    S03 = new XRiTextField();
    F03 = new XRiTextField();
    MD03 = new XRiTextField();
    MC03 = new XRiTextField();
    J04 = new XRiTextField();
    C04 = new XRiTextField();
    T04 = new XRiTextField();
    B04 = new XRiTextField();
    P04 = new XRiTextField();
    R04 = new XRiTextField();
    N04 = new XRiTextField();
    S04 = new XRiTextField();
    F04 = new XRiTextField();
    MD04 = new XRiTextField();
    MC04 = new XRiTextField();
    J05 = new XRiTextField();
    C05 = new XRiTextField();
    T05 = new XRiTextField();
    B05 = new XRiTextField();
    P05 = new XRiTextField();
    R05 = new XRiTextField();
    N05 = new XRiTextField();
    S05 = new XRiTextField();
    F05 = new XRiTextField();
    MD05 = new XRiTextField();
    MC05 = new XRiTextField();
    J06 = new XRiTextField();
    C06 = new XRiTextField();
    T06 = new XRiTextField();
    B06 = new XRiTextField();
    P06 = new XRiTextField();
    R06 = new XRiTextField();
    N06 = new XRiTextField();
    S06 = new XRiTextField();
    F06 = new XRiTextField();
    MD06 = new XRiTextField();
    MC06 = new XRiTextField();
    J07 = new XRiTextField();
    C07 = new XRiTextField();
    T07 = new XRiTextField();
    B07 = new XRiTextField();
    P07 = new XRiTextField();
    R07 = new XRiTextField();
    N07 = new XRiTextField();
    S07 = new XRiTextField();
    F07 = new XRiTextField();
    MD07 = new XRiTextField();
    MC07 = new XRiTextField();
    J08 = new XRiTextField();
    C08 = new XRiTextField();
    T08 = new XRiTextField();
    B08 = new XRiTextField();
    P08 = new XRiTextField();
    R08 = new XRiTextField();
    N08 = new XRiTextField();
    S08 = new XRiTextField();
    F08 = new XRiTextField();
    MD08 = new XRiTextField();
    MC08 = new XRiTextField();
    J09 = new XRiTextField();
    C09 = new XRiTextField();
    T09 = new XRiTextField();
    B09 = new XRiTextField();
    P09 = new XRiTextField();
    R09 = new XRiTextField();
    N09 = new XRiTextField();
    S09 = new XRiTextField();
    F09 = new XRiTextField();
    MD09 = new XRiTextField();
    MC09 = new XRiTextField();
    C10 = new XRiTextField();
    T10 = new XRiTextField();
    B10 = new XRiTextField();
    P10 = new XRiTextField();
    R10 = new XRiTextField();
    N10 = new XRiTextField();
    S10 = new XRiTextField();
    F10 = new XRiTextField();
    MD10 = new XRiTextField();
    MC10 = new XRiTextField();
    C11 = new XRiTextField();
    T11 = new XRiTextField();
    B11 = new XRiTextField();
    P11 = new XRiTextField();
    R11 = new XRiTextField();
    N11 = new XRiTextField();
    S11 = new XRiTextField();
    F11 = new XRiTextField();
    MD11 = new XRiTextField();
    MC11 = new XRiTextField();
    J10 = new XRiTextField();
    C12 = new XRiTextField();
    T12 = new XRiTextField();
    B12 = new XRiTextField();
    P12 = new XRiTextField();
    R12 = new XRiTextField();
    N12 = new XRiTextField();
    S12 = new XRiTextField();
    F12 = new XRiTextField();
    MD12 = new XRiTextField();
    MC12 = new XRiTextField();
    J11 = new XRiTextField();
    C13 = new XRiTextField();
    T13 = new XRiTextField();
    B13 = new XRiTextField();
    P13 = new XRiTextField();
    R13 = new XRiTextField();
    N13 = new XRiTextField();
    S13 = new XRiTextField();
    F13 = new XRiTextField();
    MD13 = new XRiTextField();
    MC13 = new XRiTextField();
    J13 = new XRiTextField();
    C15 = new XRiTextField();
    T15 = new XRiTextField();
    B15 = new XRiTextField();
    P15 = new XRiTextField();
    R15 = new XRiTextField();
    N15 = new XRiTextField();
    S15 = new XRiTextField();
    F15 = new XRiTextField();
    MD15 = new XRiTextField();
    MC15 = new XRiTextField();
    J12 = new XRiTextField();
    C14 = new XRiTextField();
    T14 = new XRiTextField();
    B14 = new XRiTextField();
    P14 = new XRiTextField();
    R14 = new XRiTextField();
    N14 = new XRiTextField();
    S14 = new XRiTextField();
    F14 = new XRiTextField();
    MD14 = new XRiTextField();
    MC14 = new XRiTextField();
    bt_01 = new JButton();
    bt_02 = new JButton();
    bt_04 = new JButton();
    bt_05 = new JButton();
    bt_06 = new JButton();
    bt_07 = new JButton();
    bt_08 = new JButton();
    bt_10 = new JButton();
    bt_09 = new JButton();
    bt_11 = new JButton();
    bt_12 = new JButton();
    J14 = new XRiTextField();
    J15 = new XRiTextField();
    NAT01 = new XRiTextField();
    NAT02 = new XRiTextField();
    NAT03 = new XRiTextField();
    NAT04 = new XRiTextField();
    NAT05 = new XRiTextField();
    NAT06 = new XRiTextField();
    NAT07 = new XRiTextField();
    NAT08 = new XRiTextField();
    NAT09 = new XRiTextField();
    NAT10 = new XRiTextField();
    NAT11 = new XRiTextField();
    NAT12 = new XRiTextField();
    NAT13 = new XRiTextField();
    NAT15 = new XRiTextField();
    NAT14 = new XRiTextField();
    panel2 = new JPanel();
    E1TDB = new XRiTextField();
    E1TCR = new XRiTextField();
    WDIFDB = new XRiTextField();
    WDIFCR = new XRiTextField();
    OBJ_69 = new JLabel();
    OBJ_65 = new JLabel();
    OBJ_63 = new JLabel();
    OBJ_62 = new JLabel();
    OBJ_64 = new JLabel();
    OBJ_66 = new JLabel();
    OBJ_67 = new JLabel();
    OBJ_68 = new JLabel();
    OBJ_70 = new JLabel();
    OBJ_71 = new JLabel();
    OBJ_72 = new JLabel();
    OBJ_73 = new JLabel();
    OBJ_74 = new JLabel();
    OBJ_75 = new JLabel();
    OBJ_76 = new JLabel();
    OBJ_77 = new JLabel();
    OBJ_78 = new JLabel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    U01 = new XRiTextField();
    U02 = new XRiTextField();
    U03 = new XRiTextField();
    U04 = new XRiTextField();
    U05 = new XRiTextField();
    U06 = new XRiTextField();
    U07 = new XRiTextField();
    U08 = new XRiTextField();
    U09 = new XRiTextField();
    U10 = new XRiTextField();
    U11 = new XRiTextField();
    U13 = new XRiTextField();
    U12 = new XRiTextField();
    U15 = new XRiTextField();
    U14 = new XRiTextField();
    OBJ_79 = new JLabel();
    bt_13 = new JButton();
    E01 = new JLabel();
    E15 = new JLabel();
    E14 = new JLabel();
    E13 = new JLabel();
    E12 = new JLabel();
    E11 = new JLabel();
    E10 = new JLabel();
    E09 = new JLabel();
    E08 = new JLabel();
    E07 = new JLabel();
    E06 = new JLabel();
    E05 = new JLabel();
    E04 = new JLabel();
    E03 = new JLabel();
    E02 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    BTDA = new JPopupMenu();
    OBJ_15 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Folio de comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
          
          // ---- JOLIB ----
          JOLIB.setComponentPopupMenu(BTD);
          JOLIB.setName("JOLIB");
          p_tete_gauche.add(JOLIB);
          JOLIB.setBounds(190, 0, 310, JOLIB.getPreferredSize().height);
          
          // ---- E1DTEX ----
          E1DTEX.setComponentPopupMenu(BTD);
          E1DTEX.setName("E1DTEX");
          p_tete_gauche.add(E1DTEX);
          E1DTEX.setBounds(620, 0, 68, E1DTEX.getPreferredSize().height);
          
          // ---- OBJ_32 ----
          OBJ_32.setText("Soci\u00e9t\u00e9");
          OBJ_32.setName("OBJ_32");
          p_tete_gauche.add(OBJ_32);
          OBJ_32.setBounds(5, 5, 49, 18);
          
          // ---- OBJ_34 ----
          OBJ_34.setText("Journal");
          OBJ_34.setName("OBJ_34");
          p_tete_gauche.add(OBJ_34);
          OBJ_34.setBounds(100, 5, 46, 18);
          
          // ---- E1SOC ----
          E1SOC.setComponentPopupMenu(BTD);
          E1SOC.setName("E1SOC");
          p_tete_gauche.add(E1SOC);
          E1SOC.setBounds(55, 0, 40, E1SOC.getPreferredSize().height);
          
          // ---- OBJ_37 ----
          OBJ_37.setText("Folio");
          OBJ_37.setName("OBJ_37");
          p_tete_gauche.add(OBJ_37);
          OBJ_37.setBounds(505, 5, 33, 18);
          
          // ---- E1CFO ----
          E1CFO.setComponentPopupMenu(BTD);
          E1CFO.setName("E1CFO");
          p_tete_gauche.add(E1CFO);
          E1CFO.setBounds(540, 0, 50, E1CFO.getPreferredSize().height);
          
          // ---- E1CJO ----
          E1CJO.setComponentPopupMenu(BTD);
          E1CJO.setName("E1CJO");
          p_tete_gauche.add(E1CJO);
          E1CJO.setBounds(155, 0, 30, E1CJO.getPreferredSize().height);
          
          // ---- OBJ_39 ----
          OBJ_39.setText("du");
          OBJ_39.setName("OBJ_39");
          p_tete_gauche.add(OBJ_39);
          OBJ_39.setBounds(595, 5, 18, 18);
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
          
          // ---- label1 ----
          label1.setText("page @WPAGE@");
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD, label1.getFont().getSize() + 3f));
          label1.setName("label1");
          p_tete_droite.add(label1);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1040, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          
          // ======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Lignes du folio"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);
            
            // ---- J01 ----
            J01.setComponentPopupMenu(BTD);
            J01.setName("J01");
            panel1.add(J01);
            J01.setBounds(10, 50, 34, J01.getPreferredSize().height);
            
            // ---- C01 ----
            C01.setComponentPopupMenu(BTD);
            C01.setName("C01");
            panel1.add(C01);
            C01.setBounds(40, 50, 60, C01.getPreferredSize().height);
            
            // ---- T01 ----
            T01.setComponentPopupMenu(BTD);
            T01.setName("T01");
            panel1.add(T01);
            T01.setBounds(95, 50, 60, T01.getPreferredSize().height);
            
            // ---- B01 ----
            B01.setComponentPopupMenu(BTD);
            B01.setName("B01");
            panel1.add(B01);
            B01.setBounds(150, 50, 285, B01.getPreferredSize().height);
            
            // ---- P01 ----
            P01.setComponentPopupMenu(BTD);
            P01.setName("P01");
            panel1.add(P01);
            P01.setBounds(430, 50, 68, P01.getPreferredSize().height);
            
            // ---- R01 ----
            R01.setComponentPopupMenu(BTD);
            R01.setName("R01");
            panel1.add(R01);
            R01.setBounds(495, 50, 106, R01.getPreferredSize().height);
            
            // ---- N01 ----
            N01.setComponentPopupMenu(BTD);
            N01.setName("N01");
            panel1.add(N01);
            N01.setBounds(595, 50, 60, N01.getPreferredSize().height);
            
            // ---- S01 ----
            S01.setComponentPopupMenu(BTD);
            S01.setName("S01");
            panel1.add(S01);
            S01.setBounds(650, 50, 50, S01.getPreferredSize().height);
            
            // ---- F01 ----
            F01.setComponentPopupMenu(BTD);
            F01.setName("F01");
            panel1.add(F01);
            F01.setBounds(695, 50, 60, F01.getPreferredSize().height);
            
            // ---- MD01 ----
            MD01.setComponentPopupMenu(BTD);
            MD01.setName("MD01");
            panel1.add(MD01);
            MD01.setBounds(805, 50, 100, MD01.getPreferredSize().height);
            
            // ---- MC01 ----
            MC01.setComponentPopupMenu(BTD);
            MC01.setName("MC01");
            panel1.add(MC01);
            MC01.setBounds(900, 50, 100, MC01.getPreferredSize().height);
            
            // ---- J02 ----
            J02.setComponentPopupMenu(BTD);
            J02.setName("J02");
            panel1.add(J02);
            J02.setBounds(10, 75, 34, J02.getPreferredSize().height);
            
            // ---- C02 ----
            C02.setComponentPopupMenu(BTD);
            C02.setName("C02");
            panel1.add(C02);
            C02.setBounds(40, 75, 60, C02.getPreferredSize().height);
            
            // ---- T02 ----
            T02.setComponentPopupMenu(BTD);
            T02.setName("T02");
            panel1.add(T02);
            T02.setBounds(95, 75, 60, T02.getPreferredSize().height);
            
            // ---- B02 ----
            B02.setComponentPopupMenu(BTD);
            B02.setName("B02");
            panel1.add(B02);
            B02.setBounds(150, 75, 285, B02.getPreferredSize().height);
            
            // ---- P02 ----
            P02.setComponentPopupMenu(BTD);
            P02.setName("P02");
            panel1.add(P02);
            P02.setBounds(430, 75, 68, P02.getPreferredSize().height);
            
            // ---- R02 ----
            R02.setComponentPopupMenu(BTD);
            R02.setName("R02");
            panel1.add(R02);
            R02.setBounds(495, 75, 106, R02.getPreferredSize().height);
            
            // ---- N02 ----
            N02.setComponentPopupMenu(BTD);
            N02.setName("N02");
            panel1.add(N02);
            N02.setBounds(595, 75, 60, N02.getPreferredSize().height);
            
            // ---- S02 ----
            S02.setComponentPopupMenu(BTD);
            S02.setName("S02");
            panel1.add(S02);
            S02.setBounds(650, 75, 50, S02.getPreferredSize().height);
            
            // ---- F02 ----
            F02.setComponentPopupMenu(BTD);
            F02.setName("F02");
            panel1.add(F02);
            F02.setBounds(695, 75, 60, F02.getPreferredSize().height);
            
            // ---- MD02 ----
            MD02.setComponentPopupMenu(BTD);
            MD02.setName("MD02");
            panel1.add(MD02);
            MD02.setBounds(805, 75, 100, MD02.getPreferredSize().height);
            
            // ---- MC02 ----
            MC02.setComponentPopupMenu(BTD);
            MC02.setName("MC02");
            panel1.add(MC02);
            MC02.setBounds(900, 75, 100, MC02.getPreferredSize().height);
            
            // ---- J03 ----
            J03.setComponentPopupMenu(BTD);
            J03.setName("J03");
            panel1.add(J03);
            J03.setBounds(10, 100, 34, J03.getPreferredSize().height);
            
            // ---- C03 ----
            C03.setComponentPopupMenu(BTD);
            C03.setName("C03");
            panel1.add(C03);
            C03.setBounds(40, 100, 60, C03.getPreferredSize().height);
            
            // ---- T03 ----
            T03.setComponentPopupMenu(BTD);
            T03.setName("T03");
            panel1.add(T03);
            T03.setBounds(95, 100, 60, T03.getPreferredSize().height);
            
            // ---- B03 ----
            B03.setComponentPopupMenu(BTD);
            B03.setName("B03");
            panel1.add(B03);
            B03.setBounds(150, 100, 285, B03.getPreferredSize().height);
            
            // ---- P03 ----
            P03.setComponentPopupMenu(BTD);
            P03.setName("P03");
            panel1.add(P03);
            P03.setBounds(430, 100, 68, P03.getPreferredSize().height);
            
            // ---- R03 ----
            R03.setComponentPopupMenu(BTD);
            R03.setName("R03");
            panel1.add(R03);
            R03.setBounds(495, 100, 106, R03.getPreferredSize().height);
            
            // ---- N03 ----
            N03.setComponentPopupMenu(BTD);
            N03.setName("N03");
            panel1.add(N03);
            N03.setBounds(595, 100, 60, N03.getPreferredSize().height);
            
            // ---- S03 ----
            S03.setComponentPopupMenu(BTD);
            S03.setName("S03");
            panel1.add(S03);
            S03.setBounds(650, 100, 50, S03.getPreferredSize().height);
            
            // ---- F03 ----
            F03.setComponentPopupMenu(BTD);
            F03.setName("F03");
            panel1.add(F03);
            F03.setBounds(695, 100, 60, F03.getPreferredSize().height);
            
            // ---- MD03 ----
            MD03.setComponentPopupMenu(BTD);
            MD03.setName("MD03");
            panel1.add(MD03);
            MD03.setBounds(805, 100, 100, MD03.getPreferredSize().height);
            
            // ---- MC03 ----
            MC03.setComponentPopupMenu(BTD);
            MC03.setName("MC03");
            panel1.add(MC03);
            MC03.setBounds(900, 100, 100, MC03.getPreferredSize().height);
            
            // ---- J04 ----
            J04.setComponentPopupMenu(BTD);
            J04.setName("J04");
            panel1.add(J04);
            J04.setBounds(10, 125, 34, J04.getPreferredSize().height);
            
            // ---- C04 ----
            C04.setComponentPopupMenu(BTD);
            C04.setName("C04");
            panel1.add(C04);
            C04.setBounds(40, 125, 60, C04.getPreferredSize().height);
            
            // ---- T04 ----
            T04.setComponentPopupMenu(BTD);
            T04.setName("T04");
            panel1.add(T04);
            T04.setBounds(95, 125, 60, T04.getPreferredSize().height);
            
            // ---- B04 ----
            B04.setComponentPopupMenu(BTD);
            B04.setName("B04");
            panel1.add(B04);
            B04.setBounds(150, 125, 285, B04.getPreferredSize().height);
            
            // ---- P04 ----
            P04.setComponentPopupMenu(BTD);
            P04.setName("P04");
            panel1.add(P04);
            P04.setBounds(430, 125, 68, P04.getPreferredSize().height);
            
            // ---- R04 ----
            R04.setComponentPopupMenu(BTD);
            R04.setName("R04");
            panel1.add(R04);
            R04.setBounds(495, 125, 106, R04.getPreferredSize().height);
            
            // ---- N04 ----
            N04.setComponentPopupMenu(BTD);
            N04.setName("N04");
            panel1.add(N04);
            N04.setBounds(595, 125, 60, N04.getPreferredSize().height);
            
            // ---- S04 ----
            S04.setComponentPopupMenu(BTD);
            S04.setName("S04");
            panel1.add(S04);
            S04.setBounds(650, 125, 50, S04.getPreferredSize().height);
            
            // ---- F04 ----
            F04.setComponentPopupMenu(BTD);
            F04.setName("F04");
            panel1.add(F04);
            F04.setBounds(695, 125, 60, F04.getPreferredSize().height);
            
            // ---- MD04 ----
            MD04.setComponentPopupMenu(BTD);
            MD04.setName("MD04");
            panel1.add(MD04);
            MD04.setBounds(805, 125, 100, MD04.getPreferredSize().height);
            
            // ---- MC04 ----
            MC04.setComponentPopupMenu(BTD);
            MC04.setName("MC04");
            panel1.add(MC04);
            MC04.setBounds(900, 125, 100, MC04.getPreferredSize().height);
            
            // ---- J05 ----
            J05.setComponentPopupMenu(BTD);
            J05.setName("J05");
            panel1.add(J05);
            J05.setBounds(10, 150, 34, J05.getPreferredSize().height);
            
            // ---- C05 ----
            C05.setComponentPopupMenu(BTD);
            C05.setName("C05");
            panel1.add(C05);
            C05.setBounds(40, 150, 60, C05.getPreferredSize().height);
            
            // ---- T05 ----
            T05.setComponentPopupMenu(BTD);
            T05.setName("T05");
            panel1.add(T05);
            T05.setBounds(95, 150, 60, T05.getPreferredSize().height);
            
            // ---- B05 ----
            B05.setComponentPopupMenu(BTD);
            B05.setName("B05");
            panel1.add(B05);
            B05.setBounds(150, 150, 285, B05.getPreferredSize().height);
            
            // ---- P05 ----
            P05.setComponentPopupMenu(BTD);
            P05.setName("P05");
            panel1.add(P05);
            P05.setBounds(430, 150, 68, P05.getPreferredSize().height);
            
            // ---- R05 ----
            R05.setComponentPopupMenu(BTD);
            R05.setName("R05");
            panel1.add(R05);
            R05.setBounds(495, 150, 106, R05.getPreferredSize().height);
            
            // ---- N05 ----
            N05.setComponentPopupMenu(BTD);
            N05.setName("N05");
            panel1.add(N05);
            N05.setBounds(595, 150, 60, N05.getPreferredSize().height);
            
            // ---- S05 ----
            S05.setComponentPopupMenu(BTD);
            S05.setName("S05");
            panel1.add(S05);
            S05.setBounds(650, 150, 50, S05.getPreferredSize().height);
            
            // ---- F05 ----
            F05.setComponentPopupMenu(BTD);
            F05.setName("F05");
            panel1.add(F05);
            F05.setBounds(695, 150, 60, F05.getPreferredSize().height);
            
            // ---- MD05 ----
            MD05.setComponentPopupMenu(BTD);
            MD05.setName("MD05");
            panel1.add(MD05);
            MD05.setBounds(805, 150, 100, MD05.getPreferredSize().height);
            
            // ---- MC05 ----
            MC05.setComponentPopupMenu(BTD);
            MC05.setName("MC05");
            panel1.add(MC05);
            MC05.setBounds(900, 150, 100, MC05.getPreferredSize().height);
            
            // ---- J06 ----
            J06.setComponentPopupMenu(BTD);
            J06.setName("J06");
            panel1.add(J06);
            J06.setBounds(10, 175, 34, J06.getPreferredSize().height);
            
            // ---- C06 ----
            C06.setComponentPopupMenu(BTD);
            C06.setName("C06");
            panel1.add(C06);
            C06.setBounds(40, 175, 60, C06.getPreferredSize().height);
            
            // ---- T06 ----
            T06.setComponentPopupMenu(BTD);
            T06.setName("T06");
            panel1.add(T06);
            T06.setBounds(95, 175, 60, T06.getPreferredSize().height);
            
            // ---- B06 ----
            B06.setComponentPopupMenu(BTD);
            B06.setName("B06");
            panel1.add(B06);
            B06.setBounds(150, 175, 285, B06.getPreferredSize().height);
            
            // ---- P06 ----
            P06.setComponentPopupMenu(BTD);
            P06.setName("P06");
            panel1.add(P06);
            P06.setBounds(430, 175, 68, P06.getPreferredSize().height);
            
            // ---- R06 ----
            R06.setComponentPopupMenu(BTD);
            R06.setName("R06");
            panel1.add(R06);
            R06.setBounds(495, 175, 106, R06.getPreferredSize().height);
            
            // ---- N06 ----
            N06.setComponentPopupMenu(BTD);
            N06.setName("N06");
            panel1.add(N06);
            N06.setBounds(595, 175, 60, N06.getPreferredSize().height);
            
            // ---- S06 ----
            S06.setComponentPopupMenu(BTD);
            S06.setName("S06");
            panel1.add(S06);
            S06.setBounds(650, 175, 50, S06.getPreferredSize().height);
            
            // ---- F06 ----
            F06.setComponentPopupMenu(BTD);
            F06.setName("F06");
            panel1.add(F06);
            F06.setBounds(695, 175, 60, F06.getPreferredSize().height);
            
            // ---- MD06 ----
            MD06.setComponentPopupMenu(BTD);
            MD06.setName("MD06");
            panel1.add(MD06);
            MD06.setBounds(805, 175, 100, MD06.getPreferredSize().height);
            
            // ---- MC06 ----
            MC06.setComponentPopupMenu(BTD);
            MC06.setName("MC06");
            panel1.add(MC06);
            MC06.setBounds(900, 175, 100, MC06.getPreferredSize().height);
            
            // ---- J07 ----
            J07.setComponentPopupMenu(BTD);
            J07.setName("J07");
            panel1.add(J07);
            J07.setBounds(10, 200, 34, J07.getPreferredSize().height);
            
            // ---- C07 ----
            C07.setComponentPopupMenu(BTD);
            C07.setName("C07");
            panel1.add(C07);
            C07.setBounds(40, 200, 60, C07.getPreferredSize().height);
            
            // ---- T07 ----
            T07.setComponentPopupMenu(BTD);
            T07.setName("T07");
            panel1.add(T07);
            T07.setBounds(95, 200, 60, T07.getPreferredSize().height);
            
            // ---- B07 ----
            B07.setComponentPopupMenu(BTD);
            B07.setName("B07");
            panel1.add(B07);
            B07.setBounds(150, 200, 285, B07.getPreferredSize().height);
            
            // ---- P07 ----
            P07.setComponentPopupMenu(BTD);
            P07.setName("P07");
            panel1.add(P07);
            P07.setBounds(430, 200, 68, P07.getPreferredSize().height);
            
            // ---- R07 ----
            R07.setComponentPopupMenu(BTD);
            R07.setName("R07");
            panel1.add(R07);
            R07.setBounds(495, 200, 106, R07.getPreferredSize().height);
            
            // ---- N07 ----
            N07.setComponentPopupMenu(BTD);
            N07.setName("N07");
            panel1.add(N07);
            N07.setBounds(595, 200, 60, N07.getPreferredSize().height);
            
            // ---- S07 ----
            S07.setComponentPopupMenu(BTD);
            S07.setName("S07");
            panel1.add(S07);
            S07.setBounds(650, 200, 50, S07.getPreferredSize().height);
            
            // ---- F07 ----
            F07.setComponentPopupMenu(BTD);
            F07.setName("F07");
            panel1.add(F07);
            F07.setBounds(695, 200, 60, F07.getPreferredSize().height);
            
            // ---- MD07 ----
            MD07.setComponentPopupMenu(BTD);
            MD07.setName("MD07");
            panel1.add(MD07);
            MD07.setBounds(805, 200, 100, MD07.getPreferredSize().height);
            
            // ---- MC07 ----
            MC07.setComponentPopupMenu(BTD);
            MC07.setName("MC07");
            panel1.add(MC07);
            MC07.setBounds(900, 200, 100, MC07.getPreferredSize().height);
            
            // ---- J08 ----
            J08.setComponentPopupMenu(BTD);
            J08.setName("J08");
            panel1.add(J08);
            J08.setBounds(10, 225, 34, J08.getPreferredSize().height);
            
            // ---- C08 ----
            C08.setComponentPopupMenu(BTD);
            C08.setName("C08");
            panel1.add(C08);
            C08.setBounds(40, 225, 60, C08.getPreferredSize().height);
            
            // ---- T08 ----
            T08.setComponentPopupMenu(BTD);
            T08.setName("T08");
            panel1.add(T08);
            T08.setBounds(95, 225, 60, T08.getPreferredSize().height);
            
            // ---- B08 ----
            B08.setComponentPopupMenu(BTD);
            B08.setName("B08");
            panel1.add(B08);
            B08.setBounds(150, 225, 285, B08.getPreferredSize().height);
            
            // ---- P08 ----
            P08.setComponentPopupMenu(BTD);
            P08.setName("P08");
            panel1.add(P08);
            P08.setBounds(430, 225, 68, P08.getPreferredSize().height);
            
            // ---- R08 ----
            R08.setComponentPopupMenu(BTD);
            R08.setName("R08");
            panel1.add(R08);
            R08.setBounds(495, 225, 106, R08.getPreferredSize().height);
            
            // ---- N08 ----
            N08.setComponentPopupMenu(BTD);
            N08.setName("N08");
            panel1.add(N08);
            N08.setBounds(595, 225, 60, N08.getPreferredSize().height);
            
            // ---- S08 ----
            S08.setComponentPopupMenu(BTD);
            S08.setName("S08");
            panel1.add(S08);
            S08.setBounds(650, 225, 50, S08.getPreferredSize().height);
            
            // ---- F08 ----
            F08.setComponentPopupMenu(BTD);
            F08.setName("F08");
            panel1.add(F08);
            F08.setBounds(695, 225, 60, F08.getPreferredSize().height);
            
            // ---- MD08 ----
            MD08.setComponentPopupMenu(BTD);
            MD08.setName("MD08");
            panel1.add(MD08);
            MD08.setBounds(805, 225, 100, MD08.getPreferredSize().height);
            
            // ---- MC08 ----
            MC08.setComponentPopupMenu(BTD);
            MC08.setName("MC08");
            panel1.add(MC08);
            MC08.setBounds(900, 225, 100, MC08.getPreferredSize().height);
            
            // ---- J09 ----
            J09.setComponentPopupMenu(BTD);
            J09.setName("J09");
            panel1.add(J09);
            J09.setBounds(10, 250, 34, J09.getPreferredSize().height);
            
            // ---- C09 ----
            C09.setComponentPopupMenu(BTD);
            C09.setName("C09");
            panel1.add(C09);
            C09.setBounds(40, 250, 60, C09.getPreferredSize().height);
            
            // ---- T09 ----
            T09.setComponentPopupMenu(BTD);
            T09.setName("T09");
            panel1.add(T09);
            T09.setBounds(95, 250, 60, T09.getPreferredSize().height);
            
            // ---- B09 ----
            B09.setComponentPopupMenu(BTD);
            B09.setName("B09");
            panel1.add(B09);
            B09.setBounds(150, 250, 285, B09.getPreferredSize().height);
            
            // ---- P09 ----
            P09.setComponentPopupMenu(BTD);
            P09.setName("P09");
            panel1.add(P09);
            P09.setBounds(430, 250, 68, P09.getPreferredSize().height);
            
            // ---- R09 ----
            R09.setComponentPopupMenu(BTD);
            R09.setName("R09");
            panel1.add(R09);
            R09.setBounds(495, 250, 106, R09.getPreferredSize().height);
            
            // ---- N09 ----
            N09.setComponentPopupMenu(BTD);
            N09.setName("N09");
            panel1.add(N09);
            N09.setBounds(595, 250, 60, N09.getPreferredSize().height);
            
            // ---- S09 ----
            S09.setComponentPopupMenu(BTD);
            S09.setName("S09");
            panel1.add(S09);
            S09.setBounds(650, 250, 50, S09.getPreferredSize().height);
            
            // ---- F09 ----
            F09.setComponentPopupMenu(BTD);
            F09.setName("F09");
            panel1.add(F09);
            F09.setBounds(695, 250, 60, F09.getPreferredSize().height);
            
            // ---- MD09 ----
            MD09.setComponentPopupMenu(BTD);
            MD09.setName("MD09");
            panel1.add(MD09);
            MD09.setBounds(805, 250, 100, MD09.getPreferredSize().height);
            
            // ---- MC09 ----
            MC09.setComponentPopupMenu(BTD);
            MC09.setName("MC09");
            panel1.add(MC09);
            MC09.setBounds(900, 250, 100, MC09.getPreferredSize().height);
            
            // ---- C10 ----
            C10.setComponentPopupMenu(BTD);
            C10.setName("C10");
            panel1.add(C10);
            C10.setBounds(40, 275, 60, C10.getPreferredSize().height);
            
            // ---- T10 ----
            T10.setComponentPopupMenu(BTD);
            T10.setName("T10");
            panel1.add(T10);
            T10.setBounds(95, 275, 60, T10.getPreferredSize().height);
            
            // ---- B10 ----
            B10.setComponentPopupMenu(BTD);
            B10.setName("B10");
            panel1.add(B10);
            B10.setBounds(150, 275, 285, B10.getPreferredSize().height);
            
            // ---- P10 ----
            P10.setComponentPopupMenu(BTD);
            P10.setName("P10");
            panel1.add(P10);
            P10.setBounds(430, 275, 68, P10.getPreferredSize().height);
            
            // ---- R10 ----
            R10.setComponentPopupMenu(BTD);
            R10.setName("R10");
            panel1.add(R10);
            R10.setBounds(495, 275, 106, R10.getPreferredSize().height);
            
            // ---- N10 ----
            N10.setComponentPopupMenu(BTD);
            N10.setName("N10");
            panel1.add(N10);
            N10.setBounds(595, 275, 60, N10.getPreferredSize().height);
            
            // ---- S10 ----
            S10.setComponentPopupMenu(BTD);
            S10.setName("S10");
            panel1.add(S10);
            S10.setBounds(650, 275, 50, S10.getPreferredSize().height);
            
            // ---- F10 ----
            F10.setComponentPopupMenu(BTD);
            F10.setName("F10");
            panel1.add(F10);
            F10.setBounds(695, 275, 60, F10.getPreferredSize().height);
            
            // ---- MD10 ----
            MD10.setComponentPopupMenu(BTD);
            MD10.setName("MD10");
            panel1.add(MD10);
            MD10.setBounds(805, 275, 100, MD10.getPreferredSize().height);
            
            // ---- MC10 ----
            MC10.setComponentPopupMenu(BTD);
            MC10.setName("MC10");
            panel1.add(MC10);
            MC10.setBounds(900, 275, 100, MC10.getPreferredSize().height);
            
            // ---- C11 ----
            C11.setComponentPopupMenu(BTD);
            C11.setName("C11");
            panel1.add(C11);
            C11.setBounds(40, 300, 60, C11.getPreferredSize().height);
            
            // ---- T11 ----
            T11.setComponentPopupMenu(BTD);
            T11.setName("T11");
            panel1.add(T11);
            T11.setBounds(95, 300, 60, T11.getPreferredSize().height);
            
            // ---- B11 ----
            B11.setComponentPopupMenu(BTD);
            B11.setName("B11");
            panel1.add(B11);
            B11.setBounds(150, 300, 285, B11.getPreferredSize().height);
            
            // ---- P11 ----
            P11.setComponentPopupMenu(BTD);
            P11.setName("P11");
            panel1.add(P11);
            P11.setBounds(430, 300, 68, P11.getPreferredSize().height);
            
            // ---- R11 ----
            R11.setComponentPopupMenu(BTD);
            R11.setName("R11");
            panel1.add(R11);
            R11.setBounds(495, 300, 106, R11.getPreferredSize().height);
            
            // ---- N11 ----
            N11.setComponentPopupMenu(BTD);
            N11.setName("N11");
            panel1.add(N11);
            N11.setBounds(595, 300, 60, N11.getPreferredSize().height);
            
            // ---- S11 ----
            S11.setComponentPopupMenu(BTD);
            S11.setName("S11");
            panel1.add(S11);
            S11.setBounds(650, 300, 50, S11.getPreferredSize().height);
            
            // ---- F11 ----
            F11.setComponentPopupMenu(BTD);
            F11.setName("F11");
            panel1.add(F11);
            F11.setBounds(695, 300, 60, F11.getPreferredSize().height);
            
            // ---- MD11 ----
            MD11.setComponentPopupMenu(BTD);
            MD11.setName("MD11");
            panel1.add(MD11);
            MD11.setBounds(805, 300, 100, MD11.getPreferredSize().height);
            
            // ---- MC11 ----
            MC11.setComponentPopupMenu(BTD);
            MC11.setName("MC11");
            panel1.add(MC11);
            MC11.setBounds(900, 300, 100, MC11.getPreferredSize().height);
            
            // ---- J10 ----
            J10.setComponentPopupMenu(BTD);
            J10.setName("J10");
            panel1.add(J10);
            J10.setBounds(10, 275, 34, J10.getPreferredSize().height);
            
            // ---- C12 ----
            C12.setComponentPopupMenu(BTD);
            C12.setName("C12");
            panel1.add(C12);
            C12.setBounds(40, 325, 60, C12.getPreferredSize().height);
            
            // ---- T12 ----
            T12.setComponentPopupMenu(BTD);
            T12.setName("T12");
            panel1.add(T12);
            T12.setBounds(95, 325, 60, T12.getPreferredSize().height);
            
            // ---- B12 ----
            B12.setComponentPopupMenu(BTD);
            B12.setName("B12");
            panel1.add(B12);
            B12.setBounds(150, 325, 285, B12.getPreferredSize().height);
            
            // ---- P12 ----
            P12.setComponentPopupMenu(BTD);
            P12.setName("P12");
            panel1.add(P12);
            P12.setBounds(430, 325, 68, P12.getPreferredSize().height);
            
            // ---- R12 ----
            R12.setComponentPopupMenu(BTD);
            R12.setName("R12");
            panel1.add(R12);
            R12.setBounds(495, 325, 106, R12.getPreferredSize().height);
            
            // ---- N12 ----
            N12.setComponentPopupMenu(BTD);
            N12.setName("N12");
            panel1.add(N12);
            N12.setBounds(595, 325, 60, N12.getPreferredSize().height);
            
            // ---- S12 ----
            S12.setComponentPopupMenu(BTD);
            S12.setName("S12");
            panel1.add(S12);
            S12.setBounds(650, 325, 50, S12.getPreferredSize().height);
            
            // ---- F12 ----
            F12.setComponentPopupMenu(BTD);
            F12.setName("F12");
            panel1.add(F12);
            F12.setBounds(695, 325, 60, F12.getPreferredSize().height);
            
            // ---- MD12 ----
            MD12.setComponentPopupMenu(BTD);
            MD12.setName("MD12");
            panel1.add(MD12);
            MD12.setBounds(805, 325, 100, MD12.getPreferredSize().height);
            
            // ---- MC12 ----
            MC12.setComponentPopupMenu(BTD);
            MC12.setName("MC12");
            panel1.add(MC12);
            MC12.setBounds(900, 325, 100, MC12.getPreferredSize().height);
            
            // ---- J11 ----
            J11.setComponentPopupMenu(BTD);
            J11.setName("J11");
            panel1.add(J11);
            J11.setBounds(10, 300, 34, J11.getPreferredSize().height);
            
            // ---- C13 ----
            C13.setComponentPopupMenu(BTD);
            C13.setName("C13");
            panel1.add(C13);
            C13.setBounds(40, 350, 60, C13.getPreferredSize().height);
            
            // ---- T13 ----
            T13.setComponentPopupMenu(BTD);
            T13.setName("T13");
            panel1.add(T13);
            T13.setBounds(95, 350, 60, T13.getPreferredSize().height);
            
            // ---- B13 ----
            B13.setComponentPopupMenu(BTD);
            B13.setName("B13");
            panel1.add(B13);
            B13.setBounds(150, 350, 285, B13.getPreferredSize().height);
            
            // ---- P13 ----
            P13.setComponentPopupMenu(BTD);
            P13.setName("P13");
            panel1.add(P13);
            P13.setBounds(430, 350, 68, P13.getPreferredSize().height);
            
            // ---- R13 ----
            R13.setComponentPopupMenu(BTD);
            R13.setName("R13");
            panel1.add(R13);
            R13.setBounds(495, 350, 106, R13.getPreferredSize().height);
            
            // ---- N13 ----
            N13.setComponentPopupMenu(BTD);
            N13.setName("N13");
            panel1.add(N13);
            N13.setBounds(595, 350, 60, N13.getPreferredSize().height);
            
            // ---- S13 ----
            S13.setComponentPopupMenu(BTD);
            S13.setName("S13");
            panel1.add(S13);
            S13.setBounds(650, 350, 50, S13.getPreferredSize().height);
            
            // ---- F13 ----
            F13.setComponentPopupMenu(BTD);
            F13.setName("F13");
            panel1.add(F13);
            F13.setBounds(695, 350, 60, F13.getPreferredSize().height);
            
            // ---- MD13 ----
            MD13.setComponentPopupMenu(BTD);
            MD13.setName("MD13");
            panel1.add(MD13);
            MD13.setBounds(805, 350, 100, MD13.getPreferredSize().height);
            
            // ---- MC13 ----
            MC13.setComponentPopupMenu(BTD);
            MC13.setName("MC13");
            panel1.add(MC13);
            MC13.setBounds(900, 350, 100, MC13.getPreferredSize().height);
            
            // ---- J13 ----
            J13.setComponentPopupMenu(BTD);
            J13.setName("J13");
            panel1.add(J13);
            J13.setBounds(10, 350, 34, J13.getPreferredSize().height);
            
            // ---- C15 ----
            C15.setComponentPopupMenu(BTD);
            C15.setName("C15");
            panel1.add(C15);
            C15.setBounds(40, 400, 60, C15.getPreferredSize().height);
            
            // ---- T15 ----
            T15.setComponentPopupMenu(BTD);
            T15.setName("T15");
            panel1.add(T15);
            T15.setBounds(95, 400, 60, T15.getPreferredSize().height);
            
            // ---- B15 ----
            B15.setComponentPopupMenu(BTD);
            B15.setName("B15");
            panel1.add(B15);
            B15.setBounds(150, 400, 285, B15.getPreferredSize().height);
            
            // ---- P15 ----
            P15.setComponentPopupMenu(BTD);
            P15.setName("P15");
            panel1.add(P15);
            P15.setBounds(430, 400, 68, P15.getPreferredSize().height);
            
            // ---- R15 ----
            R15.setComponentPopupMenu(BTD);
            R15.setName("R15");
            panel1.add(R15);
            R15.setBounds(495, 400, 106, R15.getPreferredSize().height);
            
            // ---- N15 ----
            N15.setComponentPopupMenu(BTD);
            N15.setName("N15");
            panel1.add(N15);
            N15.setBounds(595, 400, 60, N15.getPreferredSize().height);
            
            // ---- S15 ----
            S15.setComponentPopupMenu(BTD);
            S15.setName("S15");
            panel1.add(S15);
            S15.setBounds(650, 400, 50, S15.getPreferredSize().height);
            
            // ---- F15 ----
            F15.setComponentPopupMenu(BTD);
            F15.setName("F15");
            panel1.add(F15);
            F15.setBounds(695, 400, 60, F15.getPreferredSize().height);
            
            // ---- MD15 ----
            MD15.setComponentPopupMenu(BTD);
            MD15.setName("MD15");
            panel1.add(MD15);
            MD15.setBounds(805, 400, 100, MD15.getPreferredSize().height);
            
            // ---- MC15 ----
            MC15.setComponentPopupMenu(BTD);
            MC15.setName("MC15");
            panel1.add(MC15);
            MC15.setBounds(900, 400, 100, MC15.getPreferredSize().height);
            
            // ---- J12 ----
            J12.setComponentPopupMenu(BTD);
            J12.setName("J12");
            panel1.add(J12);
            J12.setBounds(10, 325, 34, J12.getPreferredSize().height);
            
            // ---- C14 ----
            C14.setComponentPopupMenu(BTD);
            C14.setName("C14");
            panel1.add(C14);
            C14.setBounds(40, 375, 60, C14.getPreferredSize().height);
            
            // ---- T14 ----
            T14.setComponentPopupMenu(BTD);
            T14.setName("T14");
            panel1.add(T14);
            T14.setBounds(95, 375, 60, T14.getPreferredSize().height);
            
            // ---- B14 ----
            B14.setComponentPopupMenu(BTD);
            B14.setName("B14");
            panel1.add(B14);
            B14.setBounds(150, 375, 285, B14.getPreferredSize().height);
            
            // ---- P14 ----
            P14.setComponentPopupMenu(BTD);
            P14.setName("P14");
            panel1.add(P14);
            P14.setBounds(430, 375, 68, P14.getPreferredSize().height);
            
            // ---- R14 ----
            R14.setComponentPopupMenu(BTD);
            R14.setName("R14");
            panel1.add(R14);
            R14.setBounds(495, 375, 106, R14.getPreferredSize().height);
            
            // ---- N14 ----
            N14.setComponentPopupMenu(BTD);
            N14.setName("N14");
            panel1.add(N14);
            N14.setBounds(595, 375, 60, N14.getPreferredSize().height);
            
            // ---- S14 ----
            S14.setComponentPopupMenu(BTD);
            S14.setName("S14");
            panel1.add(S14);
            S14.setBounds(650, 375, 50, S14.getPreferredSize().height);
            
            // ---- F14 ----
            F14.setComponentPopupMenu(BTD);
            F14.setName("F14");
            panel1.add(F14);
            F14.setBounds(695, 375, 60, F14.getPreferredSize().height);
            
            // ---- MD14 ----
            MD14.setComponentPopupMenu(BTD);
            MD14.setName("MD14");
            panel1.add(MD14);
            MD14.setBounds(805, 375, 100, MD14.getPreferredSize().height);
            
            // ---- MC14 ----
            MC14.setComponentPopupMenu(BTD);
            MC14.setName("MC14");
            panel1.add(MC14);
            MC14.setBounds(900, 375, 100, MC14.getPreferredSize().height);
            
            // ---- bt_01 ----
            bt_01.setText("");
            bt_01.setToolTipText("On / Off modification jour op\u00e9ration");
            bt_01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            bt_01.setName("bt_01");
            bt_01.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bt_01ActionPerformed(e);
              }
            });
            panel1.add(bt_01);
            bt_01.setBounds(15, 430, 26, 25);
            
            // ---- bt_02 ----
            bt_02.setText("");
            bt_02.setToolTipText("On / Off modification compte");
            bt_02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            bt_02.setName("bt_02");
            bt_02.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bt_02ActionPerformed(e);
              }
            });
            panel1.add(bt_02);
            bt_02.setBounds(55, 430, 26, 25);
            
            // ---- bt_04 ----
            bt_04.setText("");
            bt_04.setToolTipText("On / Off modification pi\u00e8ce");
            bt_04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            bt_04.setName("bt_04");
            bt_04.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bt_04ActionPerformed(e);
              }
            });
            panel1.add(bt_04);
            bt_04.setBounds(290, 430, 26, 25);
            
            // ---- bt_05 ----
            bt_05.setName("bt_05");
            bt_05.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bt_05ActionPerformed(e);
              }
            });
            panel1.add(bt_05);
            bt_05.setBounds(450, 430, 26, 25);
            
            // ---- bt_06 ----
            bt_06.setName("bt_06");
            bt_06.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bt_06ActionPerformed(e);
              }
            });
            panel1.add(bt_06);
            bt_06.setBounds(530, 430, 26, 25);
            
            // ---- bt_07 ----
            bt_07.setText("");
            bt_07.setToolTipText("On / Off modification section");
            bt_07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            bt_07.setName("bt_07");
            bt_07.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bt_07ActionPerformed(e);
              }
            });
            panel1.add(bt_07);
            bt_07.setBounds(610, 430, 26, 25);
            
            // ---- bt_08 ----
            bt_08.setName("bt_08");
            bt_08.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bt_08ActionPerformed(e);
              }
            });
            panel1.add(bt_08);
            bt_08.setBounds(660, 430, 26, 25);
            
            // ---- bt_10 ----
            bt_10.setName("bt_10");
            bt_10.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bt_10ActionPerformed(e);
              }
            });
            panel1.add(bt_10);
            bt_10.setBounds(765, 430, 26, 25);
            
            // ---- bt_09 ----
            bt_09.setName("bt_09");
            bt_09.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bt_09ActionPerformed(e);
              }
            });
            panel1.add(bt_09);
            bt_09.setBounds(710, 430, 26, 25);
            
            // ---- bt_11 ----
            bt_11.setName("bt_11");
            bt_11.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bt_11ActionPerformed(e);
              }
            });
            panel1.add(bt_11);
            bt_11.setBounds(840, 430, 26, 25);
            
            // ---- bt_12 ----
            bt_12.setName("bt_12");
            bt_12.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bt_12ActionPerformed(e);
              }
            });
            panel1.add(bt_12);
            bt_12.setBounds(935, 430, 26, 25);
            
            // ---- J14 ----
            J14.setComponentPopupMenu(BTD);
            J14.setName("J14");
            panel1.add(J14);
            J14.setBounds(10, 400, 34, J14.getPreferredSize().height);
            
            // ---- J15 ----
            J15.setComponentPopupMenu(BTD);
            J15.setName("J15");
            panel1.add(J15);
            J15.setBounds(10, 375, 34, J15.getPreferredSize().height);
            
            // ---- NAT01 ----
            NAT01.setComponentPopupMenu(BTD);
            NAT01.setName("NAT01");
            panel1.add(NAT01);
            NAT01.setBounds(750, 50, 60, NAT01.getPreferredSize().height);
            
            // ---- NAT02 ----
            NAT02.setComponentPopupMenu(BTD);
            NAT02.setName("NAT02");
            panel1.add(NAT02);
            NAT02.setBounds(750, 75, 60, NAT02.getPreferredSize().height);
            
            // ---- NAT03 ----
            NAT03.setComponentPopupMenu(BTD);
            NAT03.setName("NAT03");
            panel1.add(NAT03);
            NAT03.setBounds(750, 100, 60, NAT03.getPreferredSize().height);
            
            // ---- NAT04 ----
            NAT04.setComponentPopupMenu(BTD);
            NAT04.setName("NAT04");
            panel1.add(NAT04);
            NAT04.setBounds(750, 125, 60, NAT04.getPreferredSize().height);
            
            // ---- NAT05 ----
            NAT05.setComponentPopupMenu(BTD);
            NAT05.setName("NAT05");
            panel1.add(NAT05);
            NAT05.setBounds(750, 150, 60, NAT05.getPreferredSize().height);
            
            // ---- NAT06 ----
            NAT06.setComponentPopupMenu(BTD);
            NAT06.setName("NAT06");
            panel1.add(NAT06);
            NAT06.setBounds(750, 175, 60, NAT06.getPreferredSize().height);
            
            // ---- NAT07 ----
            NAT07.setComponentPopupMenu(BTD);
            NAT07.setName("NAT07");
            panel1.add(NAT07);
            NAT07.setBounds(750, 200, 60, NAT07.getPreferredSize().height);
            
            // ---- NAT08 ----
            NAT08.setComponentPopupMenu(BTD);
            NAT08.setName("NAT08");
            panel1.add(NAT08);
            NAT08.setBounds(750, 225, 60, NAT08.getPreferredSize().height);
            
            // ---- NAT09 ----
            NAT09.setComponentPopupMenu(BTD);
            NAT09.setName("NAT09");
            panel1.add(NAT09);
            NAT09.setBounds(750, 250, 60, NAT09.getPreferredSize().height);
            
            // ---- NAT10 ----
            NAT10.setComponentPopupMenu(BTD);
            NAT10.setName("NAT10");
            panel1.add(NAT10);
            NAT10.setBounds(750, 275, 60, NAT10.getPreferredSize().height);
            
            // ---- NAT11 ----
            NAT11.setComponentPopupMenu(BTD);
            NAT11.setName("NAT11");
            panel1.add(NAT11);
            NAT11.setBounds(750, 300, 60, NAT11.getPreferredSize().height);
            
            // ---- NAT12 ----
            NAT12.setComponentPopupMenu(BTD);
            NAT12.setName("NAT12");
            panel1.add(NAT12);
            NAT12.setBounds(750, 325, 60, NAT12.getPreferredSize().height);
            
            // ---- NAT13 ----
            NAT13.setComponentPopupMenu(BTD);
            NAT13.setName("NAT13");
            panel1.add(NAT13);
            NAT13.setBounds(750, 350, 60, NAT13.getPreferredSize().height);
            
            // ---- NAT15 ----
            NAT15.setComponentPopupMenu(BTD);
            NAT15.setName("NAT15");
            panel1.add(NAT15);
            NAT15.setBounds(750, 400, 60, NAT15.getPreferredSize().height);
            
            // ---- NAT14 ----
            NAT14.setComponentPopupMenu(BTD);
            NAT14.setName("NAT14");
            panel1.add(NAT14);
            NAT14.setBounds(750, 375, 60, NAT14.getPreferredSize().height);
            
            // ======== panel2 ========
            {
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);
              
              // ---- E1TDB ----
              E1TDB.setComponentPopupMenu(BTDA);
              E1TDB.setName("E1TDB");
              panel2.add(E1TDB);
              E1TDB.setBounds(95, 30, 130, E1TDB.getPreferredSize().height);
              
              // ---- E1TCR ----
              E1TCR.setComponentPopupMenu(BTDA);
              E1TCR.setName("E1TCR");
              panel2.add(E1TCR);
              E1TCR.setBounds(230, 30, 130, E1TCR.getPreferredSize().height);
              
              // ---- WDIFDB ----
              WDIFDB.setComponentPopupMenu(BTDA);
              WDIFDB.setName("WDIFDB");
              panel2.add(WDIFDB);
              WDIFDB.setBounds(95, 60, 130, WDIFDB.getPreferredSize().height);
              
              // ---- WDIFCR ----
              WDIFCR.setComponentPopupMenu(BTDA);
              WDIFCR.setName("WDIFCR");
              panel2.add(WDIFCR);
              WDIFCR.setBounds(230, 60, 130, WDIFCR.getPreferredSize().height);
              
              // ---- OBJ_69 ----
              OBJ_69.setText("Diff\u00e9rence");
              OBJ_69.setFont(OBJ_69.getFont().deriveFont(OBJ_69.getFont().getStyle() | Font.BOLD));
              OBJ_69.setName("OBJ_69");
              panel2.add(OBJ_69);
              OBJ_69.setBounds(10, 60, 85, 28);
              
              // ---- OBJ_65 ----
              OBJ_65.setText("Totaux");
              OBJ_65.setFont(OBJ_65.getFont().deriveFont(OBJ_65.getFont().getStyle() | Font.BOLD));
              OBJ_65.setName("OBJ_65");
              panel2.add(OBJ_65);
              OBJ_65.setBounds(10, 30, 85, 28);
              
              // ---- OBJ_63 ----
              OBJ_63.setText("Cr\u00e9dit");
              OBJ_63.setFont(OBJ_63.getFont().deriveFont(OBJ_63.getFont().getStyle() | Font.BOLD));
              OBJ_63.setName("OBJ_63");
              panel2.add(OBJ_63);
              OBJ_63.setBounds(230, 5, 130, 30);
              
              // ---- OBJ_62 ----
              OBJ_62.setText("D\u00e9bit");
              OBJ_62.setFont(OBJ_62.getFont().deriveFont(OBJ_62.getFont().getStyle() | Font.BOLD));
              OBJ_62.setName("OBJ_62");
              panel2.add(OBJ_62);
              OBJ_62.setBounds(95, 5, 130, 30);
              
              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel2);
            panel2.setBounds(640, 465, 370, 95);
            
            // ---- OBJ_64 ----
            OBJ_64.setText("Jo");
            OBJ_64.setFont(OBJ_64.getFont().deriveFont(OBJ_64.getFont().getStyle() | Font.BOLD));
            OBJ_64.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_64.setName("OBJ_64");
            panel1.add(OBJ_64);
            OBJ_64.setBounds(10, 25, 34, 30);
            
            // ---- OBJ_66 ----
            OBJ_66.setText("Compte");
            OBJ_66.setFont(OBJ_66.getFont().deriveFont(OBJ_66.getFont().getStyle() | Font.BOLD));
            OBJ_66.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_66.setName("OBJ_66");
            panel1.add(OBJ_66);
            OBJ_66.setBounds(40, 25, 60, 30);
            
            // ---- OBJ_67 ----
            OBJ_67.setText("Tiers");
            OBJ_67.setFont(OBJ_67.getFont().deriveFont(OBJ_67.getFont().getStyle() | Font.BOLD));
            OBJ_67.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_67.setName("OBJ_67");
            panel1.add(OBJ_67);
            OBJ_67.setBounds(95, 25, 60, 30);
            
            // ---- OBJ_68 ----
            OBJ_68.setText("Libell\u00e9");
            OBJ_68.setFont(OBJ_68.getFont().deriveFont(OBJ_68.getFont().getStyle() | Font.BOLD));
            OBJ_68.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_68.setName("OBJ_68");
            panel1.add(OBJ_68);
            OBJ_68.setBounds(150, 25, 285, 30);
            
            // ---- OBJ_70 ----
            OBJ_70.setText("N\u00b0 Pi\u00e8ce");
            OBJ_70.setFont(OBJ_70.getFont().deriveFont(OBJ_70.getFont().getStyle() | Font.BOLD));
            OBJ_70.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_70.setName("OBJ_70");
            panel1.add(OBJ_70);
            OBJ_70.setBounds(430, 25, 68, 30);
            
            // ---- OBJ_71 ----
            OBJ_71.setText("R\u00e9f classement");
            OBJ_71.setFont(OBJ_71.getFont().deriveFont(OBJ_71.getFont().getStyle() | Font.BOLD));
            OBJ_71.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_71.setName("OBJ_71");
            panel1.add(OBJ_71);
            OBJ_71.setBounds(495, 25, 106, 30);
            
            // ---- OBJ_72 ----
            OBJ_72.setText("Contr.");
            OBJ_72.setFont(OBJ_72.getFont().deriveFont(OBJ_72.getFont().getStyle() | Font.BOLD));
            OBJ_72.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_72.setName("OBJ_72");
            panel1.add(OBJ_72);
            OBJ_72.setBounds(595, 25, 60, 30);
            
            // ---- OBJ_73 ----
            OBJ_73.setText("Section");
            OBJ_73.setFont(OBJ_73.getFont().deriveFont(OBJ_73.getFont().getStyle() | Font.BOLD));
            OBJ_73.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_73.setName("OBJ_73");
            panel1.add(OBJ_73);
            OBJ_73.setBounds(650, 25, 50, 30);
            
            // ---- OBJ_74 ----
            OBJ_74.setText("Affaire");
            OBJ_74.setFont(OBJ_74.getFont().deriveFont(OBJ_74.getFont().getStyle() | Font.BOLD));
            OBJ_74.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_74.setName("OBJ_74");
            panel1.add(OBJ_74);
            OBJ_74.setBounds(695, 25, 60, 30);
            
            // ---- OBJ_75 ----
            OBJ_75.setText("Nature");
            OBJ_75.setFont(OBJ_75.getFont().deriveFont(OBJ_75.getFont().getStyle() | Font.BOLD));
            OBJ_75.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_75.setName("OBJ_75");
            panel1.add(OBJ_75);
            OBJ_75.setBounds(750, 25, 60, 30);
            
            // ---- OBJ_76 ----
            OBJ_76.setText("D\u00e9bit");
            OBJ_76.setFont(OBJ_76.getFont().deriveFont(OBJ_76.getFont().getStyle() | Font.BOLD));
            OBJ_76.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_76.setName("OBJ_76");
            panel1.add(OBJ_76);
            OBJ_76.setBounds(805, 25, 100, 30);
            
            // ---- OBJ_77 ----
            OBJ_77.setText("Cr\u00e9dit");
            OBJ_77.setFont(OBJ_77.getFont().deriveFont(OBJ_77.getFont().getStyle() | Font.BOLD));
            OBJ_77.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_77.setName("OBJ_77");
            panel1.add(OBJ_77);
            OBJ_77.setBounds(900, 25, 100, 30);
            
            // ---- OBJ_78 ----
            OBJ_78.setText("Jr.");
            OBJ_78.setFont(OBJ_78.getFont().deriveFont(OBJ_78.getFont().getStyle() | Font.BOLD));
            OBJ_78.setName("OBJ_78");
            panel1.add(OBJ_78);
            OBJ_78.setBounds(0, 565, 34, 30);
            
            // ---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(25, 495, 55, 60);
            
            // ---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(85, 495, 55, 60);
            
            // ---- U01 ----
            U01.setComponentPopupMenu(BTD);
            U01.setName("U01");
            panel1.add(U01);
            U01.setBounds(996, 50, 20, U01.getPreferredSize().height);
            
            // ---- U02 ----
            U02.setComponentPopupMenu(BTD);
            U02.setName("U02");
            panel1.add(U02);
            U02.setBounds(996, 75, 20, U02.getPreferredSize().height);
            
            // ---- U03 ----
            U03.setComponentPopupMenu(BTD);
            U03.setName("U03");
            panel1.add(U03);
            U03.setBounds(996, 100, 20, U03.getPreferredSize().height);
            
            // ---- U04 ----
            U04.setComponentPopupMenu(BTD);
            U04.setName("U04");
            panel1.add(U04);
            U04.setBounds(996, 125, 20, U04.getPreferredSize().height);
            
            // ---- U05 ----
            U05.setComponentPopupMenu(BTD);
            U05.setName("U05");
            panel1.add(U05);
            U05.setBounds(996, 150, 20, U05.getPreferredSize().height);
            
            // ---- U06 ----
            U06.setComponentPopupMenu(BTD);
            U06.setName("U06");
            panel1.add(U06);
            U06.setBounds(996, 175, 20, U06.getPreferredSize().height);
            
            // ---- U07 ----
            U07.setComponentPopupMenu(BTD);
            U07.setName("U07");
            panel1.add(U07);
            U07.setBounds(996, 200, 20, U07.getPreferredSize().height);
            
            // ---- U08 ----
            U08.setComponentPopupMenu(BTD);
            U08.setName("U08");
            panel1.add(U08);
            U08.setBounds(996, 225, 20, U08.getPreferredSize().height);
            
            // ---- U09 ----
            U09.setComponentPopupMenu(BTD);
            U09.setName("U09");
            panel1.add(U09);
            U09.setBounds(996, 250, 20, U09.getPreferredSize().height);
            
            // ---- U10 ----
            U10.setComponentPopupMenu(BTD);
            U10.setName("U10");
            panel1.add(U10);
            U10.setBounds(996, 275, 20, U10.getPreferredSize().height);
            
            // ---- U11 ----
            U11.setComponentPopupMenu(BTD);
            U11.setName("U11");
            panel1.add(U11);
            U11.setBounds(996, 300, 20, U11.getPreferredSize().height);
            
            // ---- U13 ----
            U13.setComponentPopupMenu(BTD);
            U13.setName("U13");
            panel1.add(U13);
            U13.setBounds(996, 350, 20, U13.getPreferredSize().height);
            
            // ---- U12 ----
            U12.setComponentPopupMenu(BTD);
            U12.setName("U12");
            panel1.add(U12);
            U12.setBounds(996, 325, 20, U12.getPreferredSize().height);
            
            // ---- U15 ----
            U15.setComponentPopupMenu(BTD);
            U15.setName("U15");
            panel1.add(U15);
            U15.setBounds(996, 400, 20, U15.getPreferredSize().height);
            
            // ---- U14 ----
            U14.setComponentPopupMenu(BTD);
            U14.setName("U14");
            panel1.add(U14);
            U14.setBounds(996, 375, 20, U14.getPreferredSize().height);
            
            // ---- OBJ_79 ----
            OBJ_79.setText("T");
            OBJ_79.setFont(OBJ_79.getFont().deriveFont(OBJ_79.getFont().getStyle() | Font.BOLD));
            OBJ_79.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_79.setName("OBJ_79");
            panel1.add(OBJ_79);
            OBJ_79.setBounds(996, 25, 20, 30);
            
            // ---- bt_13 ----
            bt_13.setText("");
            bt_13.setToolTipText("On / Off modification jour op\u00e9ration");
            bt_13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            bt_13.setName("bt_13");
            bt_13.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bt_13ActionPerformed(e);
              }
            });
            panel1.add(bt_13);
            bt_13.setBounds(990, 430, 26, 25);
            
            // ---- E01 ----
            E01.setText("");
            E01.setToolTipText("Ligne en erreur");
            E01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            E01.setName("E01");
            panel1.add(E01);
            E01.setBounds(1015, 50, 23, 23);
            
            // ---- E15 ----
            E15.setText("");
            E15.setToolTipText("Ligne en erreur");
            E15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            E15.setName("E15");
            panel1.add(E15);
            E15.setBounds(1015, 400, 23, 23);
            
            // ---- E14 ----
            E14.setText("");
            E14.setToolTipText("Ligne en erreur");
            E14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            E14.setName("E14");
            panel1.add(E14);
            E14.setBounds(1015, 375, 23, 23);
            
            // ---- E13 ----
            E13.setText("");
            E13.setToolTipText("Ligne en erreur");
            E13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            E13.setName("E13");
            panel1.add(E13);
            E13.setBounds(1015, 350, 23, 23);
            
            // ---- E12 ----
            E12.setText("");
            E12.setToolTipText("Ligne en erreur");
            E12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            E12.setName("E12");
            panel1.add(E12);
            E12.setBounds(1015, 325, 23, 23);
            
            // ---- E11 ----
            E11.setText("");
            E11.setToolTipText("Ligne en erreur");
            E11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            E11.setName("E11");
            panel1.add(E11);
            E11.setBounds(1015, 300, 23, 23);
            
            // ---- E10 ----
            E10.setText("");
            E10.setToolTipText("Ligne en erreur");
            E10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            E10.setName("E10");
            panel1.add(E10);
            E10.setBounds(1015, 275, 23, 23);
            
            // ---- E09 ----
            E09.setText("");
            E09.setToolTipText("Ligne en erreur");
            E09.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            E09.setName("E09");
            panel1.add(E09);
            E09.setBounds(1015, 250, 23, 23);
            
            // ---- E08 ----
            E08.setText("");
            E08.setToolTipText("Ligne en erreur");
            E08.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            E08.setName("E08");
            panel1.add(E08);
            E08.setBounds(1015, 225, 23, 23);
            
            // ---- E07 ----
            E07.setText("");
            E07.setToolTipText("Ligne en erreur");
            E07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            E07.setName("E07");
            panel1.add(E07);
            E07.setBounds(1015, 200, 23, 23);
            
            // ---- E06 ----
            E06.setText("");
            E06.setToolTipText("Ligne en erreur");
            E06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            E06.setName("E06");
            panel1.add(E06);
            E06.setBounds(1015, 175, 23, 23);
            
            // ---- E05 ----
            E05.setText("");
            E05.setToolTipText("Ligne en erreur");
            E05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            E05.setName("E05");
            panel1.add(E05);
            E05.setBounds(1015, 150, 23, 23);
            
            // ---- E04 ----
            E04.setText("");
            E04.setToolTipText("Ligne en erreur");
            E04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            E04.setName("E04");
            panel1.add(E04);
            E04.setBounds(1015, 125, 23, 23);
            
            // ---- E03 ----
            E03.setText("");
            E03.setToolTipText("Ligne en erreur");
            E03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            E03.setName("E03");
            panel1.add(E03);
            E03.setBounds(1015, 100, 23, 23);
            
            // ---- E02 ----
            E02.setText("");
            E02.setToolTipText("Ligne en erreur");
            E02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            E02.setName("E02");
            panel1.add(E02);
            E02.setBounds(1015, 75, 23, 23);
          }
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
              p_contenuLayout.createParallelGroup().addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                  .addGap(0, 0, Short.MAX_VALUE).addComponent(panel1, GroupLayout.PREFERRED_SIZE, 1030, GroupLayout.PREFERRED_SIZE)));
          p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 575, GroupLayout.PREFERRED_SIZE).addGap(0, 0, Short.MAX_VALUE)));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_10 ----
      OBJ_10.setText("Choix possibles");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
      
      // ---- OBJ_9 ----
      OBJ_9.setText("Aide en ligne");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      BTD.add(OBJ_9);
    }
    
    // ======== BTDA ========
    {
      BTDA.setName("BTDA");
      
      // ---- OBJ_15 ----
      OBJ_15.setText("Aide en ligne");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTDA.add(OBJ_15);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private XRiTextField JOLIB;
  private XRiTextField E1DTEX;
  private JLabel OBJ_32;
  private JLabel OBJ_34;
  private XRiTextField E1SOC;
  private JLabel OBJ_37;
  private XRiTextField E1CFO;
  private XRiTextField E1CJO;
  private JLabel OBJ_39;
  private JPanel p_tete_droite;
  private JLabel label1;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField J01;
  private XRiTextField C01;
  private XRiTextField T01;
  private XRiTextField B01;
  private XRiTextField P01;
  private XRiTextField R01;
  private XRiTextField N01;
  private XRiTextField S01;
  private XRiTextField F01;
  private XRiTextField MD01;
  private XRiTextField MC01;
  private XRiTextField J02;
  private XRiTextField C02;
  private XRiTextField T02;
  private XRiTextField B02;
  private XRiTextField P02;
  private XRiTextField R02;
  private XRiTextField N02;
  private XRiTextField S02;
  private XRiTextField F02;
  private XRiTextField MD02;
  private XRiTextField MC02;
  private XRiTextField J03;
  private XRiTextField C03;
  private XRiTextField T03;
  private XRiTextField B03;
  private XRiTextField P03;
  private XRiTextField R03;
  private XRiTextField N03;
  private XRiTextField S03;
  private XRiTextField F03;
  private XRiTextField MD03;
  private XRiTextField MC03;
  private XRiTextField J04;
  private XRiTextField C04;
  private XRiTextField T04;
  private XRiTextField B04;
  private XRiTextField P04;
  private XRiTextField R04;
  private XRiTextField N04;
  private XRiTextField S04;
  private XRiTextField F04;
  private XRiTextField MD04;
  private XRiTextField MC04;
  private XRiTextField J05;
  private XRiTextField C05;
  private XRiTextField T05;
  private XRiTextField B05;
  private XRiTextField P05;
  private XRiTextField R05;
  private XRiTextField N05;
  private XRiTextField S05;
  private XRiTextField F05;
  private XRiTextField MD05;
  private XRiTextField MC05;
  private XRiTextField J06;
  private XRiTextField C06;
  private XRiTextField T06;
  private XRiTextField B06;
  private XRiTextField P06;
  private XRiTextField R06;
  private XRiTextField N06;
  private XRiTextField S06;
  private XRiTextField F06;
  private XRiTextField MD06;
  private XRiTextField MC06;
  private XRiTextField J07;
  private XRiTextField C07;
  private XRiTextField T07;
  private XRiTextField B07;
  private XRiTextField P07;
  private XRiTextField R07;
  private XRiTextField N07;
  private XRiTextField S07;
  private XRiTextField F07;
  private XRiTextField MD07;
  private XRiTextField MC07;
  private XRiTextField J08;
  private XRiTextField C08;
  private XRiTextField T08;
  private XRiTextField B08;
  private XRiTextField P08;
  private XRiTextField R08;
  private XRiTextField N08;
  private XRiTextField S08;
  private XRiTextField F08;
  private XRiTextField MD08;
  private XRiTextField MC08;
  private XRiTextField J09;
  private XRiTextField C09;
  private XRiTextField T09;
  private XRiTextField B09;
  private XRiTextField P09;
  private XRiTextField R09;
  private XRiTextField N09;
  private XRiTextField S09;
  private XRiTextField F09;
  private XRiTextField MD09;
  private XRiTextField MC09;
  private XRiTextField C10;
  private XRiTextField T10;
  private XRiTextField B10;
  private XRiTextField P10;
  private XRiTextField R10;
  private XRiTextField N10;
  private XRiTextField S10;
  private XRiTextField F10;
  private XRiTextField MD10;
  private XRiTextField MC10;
  private XRiTextField C11;
  private XRiTextField T11;
  private XRiTextField B11;
  private XRiTextField P11;
  private XRiTextField R11;
  private XRiTextField N11;
  private XRiTextField S11;
  private XRiTextField F11;
  private XRiTextField MD11;
  private XRiTextField MC11;
  private XRiTextField J10;
  private XRiTextField C12;
  private XRiTextField T12;
  private XRiTextField B12;
  private XRiTextField P12;
  private XRiTextField R12;
  private XRiTextField N12;
  private XRiTextField S12;
  private XRiTextField F12;
  private XRiTextField MD12;
  private XRiTextField MC12;
  private XRiTextField J11;
  private XRiTextField C13;
  private XRiTextField T13;
  private XRiTextField B13;
  private XRiTextField P13;
  private XRiTextField R13;
  private XRiTextField N13;
  private XRiTextField S13;
  private XRiTextField F13;
  private XRiTextField MD13;
  private XRiTextField MC13;
  private XRiTextField J13;
  private XRiTextField C15;
  private XRiTextField T15;
  private XRiTextField B15;
  private XRiTextField P15;
  private XRiTextField R15;
  private XRiTextField N15;
  private XRiTextField S15;
  private XRiTextField F15;
  private XRiTextField MD15;
  private XRiTextField MC15;
  private XRiTextField J12;
  private XRiTextField C14;
  private XRiTextField T14;
  private XRiTextField B14;
  private XRiTextField P14;
  private XRiTextField R14;
  private XRiTextField N14;
  private XRiTextField S14;
  private XRiTextField F14;
  private XRiTextField MD14;
  private XRiTextField MC14;
  private JButton bt_01;
  private JButton bt_02;
  private JButton bt_04;
  private JButton bt_05;
  private JButton bt_06;
  private JButton bt_07;
  private JButton bt_08;
  private JButton bt_10;
  private JButton bt_09;
  private JButton bt_11;
  private JButton bt_12;
  private XRiTextField J14;
  private XRiTextField J15;
  private XRiTextField NAT01;
  private XRiTextField NAT02;
  private XRiTextField NAT03;
  private XRiTextField NAT04;
  private XRiTextField NAT05;
  private XRiTextField NAT06;
  private XRiTextField NAT07;
  private XRiTextField NAT08;
  private XRiTextField NAT09;
  private XRiTextField NAT10;
  private XRiTextField NAT11;
  private XRiTextField NAT12;
  private XRiTextField NAT13;
  private XRiTextField NAT15;
  private XRiTextField NAT14;
  private JPanel panel2;
  private XRiTextField E1TDB;
  private XRiTextField E1TCR;
  private XRiTextField WDIFDB;
  private XRiTextField WDIFCR;
  private JLabel OBJ_69;
  private JLabel OBJ_65;
  private JLabel OBJ_63;
  private JLabel OBJ_62;
  private JLabel OBJ_64;
  private JLabel OBJ_66;
  private JLabel OBJ_67;
  private JLabel OBJ_68;
  private JLabel OBJ_70;
  private JLabel OBJ_71;
  private JLabel OBJ_72;
  private JLabel OBJ_73;
  private JLabel OBJ_74;
  private JLabel OBJ_75;
  private JLabel OBJ_76;
  private JLabel OBJ_77;
  private JLabel OBJ_78;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private XRiTextField U01;
  private XRiTextField U02;
  private XRiTextField U03;
  private XRiTextField U04;
  private XRiTextField U05;
  private XRiTextField U06;
  private XRiTextField U07;
  private XRiTextField U08;
  private XRiTextField U09;
  private XRiTextField U10;
  private XRiTextField U11;
  private XRiTextField U13;
  private XRiTextField U12;
  private XRiTextField U15;
  private XRiTextField U14;
  private JLabel OBJ_79;
  private JButton bt_13;
  private JLabel E01;
  private JLabel E15;
  private JLabel E14;
  private JLabel E13;
  private JLabel E12;
  private JLabel E11;
  private JLabel E10;
  private JLabel E09;
  private JLabel E08;
  private JLabel E07;
  private JLabel E06;
  private JLabel E05;
  private JLabel E04;
  private JLabel E03;
  private JLabel E02;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_9;
  private JPopupMenu BTDA;
  private JMenuItem OBJ_15;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
