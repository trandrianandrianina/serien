
package ri.serien.libecranrpg.vcgm.VCGM01FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VCGM01FM_CT extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] CTSNE_Value = { "", "C", "D", };
  private String[] CTSNE_Title = { "Débit ou Crédit", "Crédit", "Débit", };
  private String[] CTSNS_Value = { "", "C", "D", };
  private String[] CTSNS_Title = { "", "Crédit", "Débit", };
  
  public VCGM01FM_CT(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    CTSNE.setValeurs(CTSNE_Value, CTSNE_Title);
    CTSNS.setValeurs(CTSNS_Value, CTSNS_Title);
    CTCE2.setValeurs("+", CTCE2_GRP);
    CTCE2_MOINS.setValeurs("-");
    CTCE3.setValeurs("+", CTCE3_GRP);
    CTCE3_MOINS.setValeurs("-");
    CTCE4.setValeurs("+", CTCE4_GRP);
    CTCE4_MOINS.setValeurs("-");
    CTCE5.setValeurs("+", CTCE5_GRP);
    CTCE5_MOINS.setValeurs("-");
    CTCE6.setValeurs("+", CTCE6_GRP);
    CTCE6_MOINS.setValeurs("-");
    CTCE7.setValeurs("+", CTCE7_GRP);
    CTCE7_MOINS.setValeurs("-");
    CTCE8.setValeurs("+", CTCE8_GRP);
    CTCE8_MOINS.setValeurs("-");
    CTCE9.setValeurs("+", CTCE9_GRP);
    CTCE9_MOINS.setValeurs("-");
    CTCE10.setValeurs("+", CTCE10_GRP);
    CTCE10_MOINS.setValeurs("-");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    boolean isGiam = lexique.isTrue("41");
    
    panel1.setVisible(isGiam);
    panel2.setVisible(!isGiam);
    panel3.setVisible(!isGiam);
    panel4.setVisible(!isGiam);
    CTCE2_MOINS.setEnabled(lexique.getMode() != Lexical.MODE_CONSULTATION);
    CTCE3_MOINS.setEnabled(lexique.getMode() != Lexical.MODE_CONSULTATION);
    CTCE4_MOINS.setEnabled(lexique.getMode() != Lexical.MODE_CONSULTATION);
    CTCE5_MOINS.setEnabled(lexique.getMode() != Lexical.MODE_CONSULTATION);
    CTCE6_MOINS.setEnabled(lexique.getMode() != Lexical.MODE_CONSULTATION);
    CTCE7_MOINS.setEnabled(lexique.getMode() != Lexical.MODE_CONSULTATION);
    CTCE8_MOINS.setEnabled(lexique.getMode() != Lexical.MODE_CONSULTATION);
    CTCE9_MOINS.setEnabled(lexique.getMode() != Lexical.MODE_CONSULTATION);
    CTCE10_MOINS.setEnabled(lexique.getMode() != Lexical.MODE_CONSULTATION);
    
    

    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    // riMenu_bt2.setIcon(lexique.getImage("images/options.png",true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("CTSNE", 0, CTSNE_Value[CTSNE.getSelectedIndex()]);
    // lexique.HostFieldPutData("CTSNS", 0, CTSNS_Value[CTSNS.getSelectedIndex()]);
    // rb
    // if (rb11.isSelected()) lexique.HostFieldPutData("CTCE2", 0, "+");
    // if (rb21.isSelected()) lexique.HostFieldPutData("CTCE3", 0, "+");
    // if (rb31.isSelected()) lexique.HostFieldPutData("CTCE4", 0, "+");
    // if (rb41.isSelected()) lexique.HostFieldPutData("CTCE5", 0, "+");
    // if (rb51.isSelected()) lexique.HostFieldPutData("CTCE6", 0, "+");
    // if (rb61.isSelected()) lexique.HostFieldPutData("CTCE7", 0, "+");
    // if (rb71.isSelected()) lexique.HostFieldPutData("CTCE8", 0, "+");
    // if (rb81.isSelected()) lexique.HostFieldPutData("CTCE9", 0, "+");
    // if (rb91.isSelected()) lexique.HostFieldPutData("CTCE10", 0, "+");
    // if (rb12.isSelected()) lexique.HostFieldPutData("CTCE2", 0, "-");
    // if (rb22.isSelected()) lexique.HostFieldPutData("CTCE3", 0, "-");
    // if (rb32.isSelected()) lexique.HostFieldPutData("CTCE4", 0, "-");
    // if (rb42.isSelected()) lexique.HostFieldPutData("CTCE5", 0, "-");
    // if (rb52.isSelected()) lexique.HostFieldPutData("CTCE6", 0, "-");
    // if (rb62.isSelected()) lexique.HostFieldPutData("CTCE7", 0, "-");
    // if (rb72.isSelected()) lexique.HostFieldPutData("CTCE8", 0, "-");
    // if (rb82.isSelected()) lexique.HostFieldPutData("CTCE9", 0, "-");
    // if (rb92.isSelected()) lexique.HostFieldPutData("CTCE10", 0, "-");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44_OBJ_45 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_45_OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46_OBJ_47 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    CTC01 = new XRiTextField();
    CTC02 = new XRiTextField();
    CTC03 = new XRiTextField();
    CTC04 = new XRiTextField();
    CTC05 = new XRiTextField();
    CTC06 = new XRiTextField();
    CTC07 = new XRiTextField();
    CTC08 = new XRiTextField();
    CTC09 = new XRiTextField();
    CTC010 = new XRiTextField();
    label13 = new JLabel();
    panel2 = new JPanel();
    CTCE2_MOINS = new XRiRadioButton();
    CTCE2 = new XRiRadioButton();
    CTCE3 = new XRiRadioButton();
    CTCE3_MOINS = new XRiRadioButton();
    CTCE4 = new XRiRadioButton();
    CTCE4_MOINS = new XRiRadioButton();
    CTCE7 = new XRiRadioButton();
    CTCE7_MOINS = new XRiRadioButton();
    CTCE6_MOINS = new XRiRadioButton();
    CTCE6 = new XRiRadioButton();
    CTCE5 = new XRiRadioButton();
    CTCE5_MOINS = new XRiRadioButton();
    CTCE10 = new XRiRadioButton();
    CTCE10_MOINS = new XRiRadioButton();
    CTCE9_MOINS = new XRiRadioButton();
    CTCE9 = new XRiRadioButton();
    CTCE8 = new XRiRadioButton();
    CTCE8_MOINS = new XRiRadioButton();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();
    CTCD1 = new XRiTextField();
    CTCD2 = new XRiTextField();
    CTCD3 = new XRiTextField();
    CTCD4 = new XRiTextField();
    CTCD5 = new XRiTextField();
    CTCD6 = new XRiTextField();
    CTCD7 = new XRiTextField();
    CTCD8 = new XRiTextField();
    CTCD9 = new XRiTextField();
    CTCD10 = new XRiTextField();
    CTOP1 = new XRiTextField();
    CTOP2 = new XRiTextField();
    CTOP3 = new XRiTextField();
    CTOP4 = new XRiTextField();
    CTOP5 = new XRiTextField();
    CTOP6 = new XRiTextField();
    CTOP7 = new XRiTextField();
    CTOP8 = new XRiTextField();
    CTOP9 = new XRiTextField();
    CTOP10 = new XRiTextField();
    label11 = new JLabel();
    label12 = new JLabel();
    panel1 = new JPanel();
    label18 = new JLabel();
    label17 = new JLabel();
    label16 = new JLabel();
    label15 = new JLabel();
    label14 = new JLabel();
    label19 = new JLabel();
    label20 = new JLabel();
    label21 = new JLabel();
    label22 = new JLabel();
    panel3 = new JPanel();
    OBJ_58_OBJ_58 = new JLabel();
    OBJ_57_OBJ_57 = new JLabel();
    CTCOD = new XRiTextField();
    CTCOC = new XRiTextField();
    separator1 = compFactory.createSeparator("Comptabilisation de l'OD de TVA");
    panel4 = new JPanel();
    CTSNS = new XRiComboBox();
    CTSNE = new XRiComboBox();
    CTLIB = new XRiTextField();
    OBJ_54_OBJ_54 = new JLabel();
    OBJ_56_OBJ_56 = new JLabel();
    OBJ_42_OBJ_42 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    CTCE2_GRP = new ButtonGroup();
    CTCE3_GRP = new ButtonGroup();
    CTCE4_GRP = new ButtonGroup();
    CTCE7_GRP = new ButtonGroup();
    CTCE6_GRP = new ButtonGroup();
    CTCE5_GRP = new ButtonGroup();
    CTCE10_GRP = new ButtonGroup();
    CTCE9_GRP = new ButtonGroup();
    CTCE8_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44_OBJ_45 ----
          OBJ_44_OBJ_45.setText("Soci\u00e9t\u00e9");
          OBJ_44_OBJ_45.setName("OBJ_44_OBJ_45");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setName("INDETB");

          //---- OBJ_45_OBJ_46 ----
          OBJ_45_OBJ_46.setText("Code");
          OBJ_45_OBJ_46.setName("OBJ_45_OBJ_46");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setName("INDTYP");

          //---- OBJ_46_OBJ_47 ----
          OBJ_46_OBJ_47.setText("Ordre");
          OBJ_46_OBJ_47.setName("OBJ_46_OBJ_47");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(800, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Cadre pour la d\u00e9claration de TVA");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- CTC01 ----
            CTC01.setName("CTC01");
            xTitledPanel1ContentContainer.add(CTC01);
            CTC01.setBounds(370, 90, 90, CTC01.getPreferredSize().height);

            //---- CTC02 ----
            CTC02.setName("CTC02");
            xTitledPanel1ContentContainer.add(CTC02);
            CTC02.setBounds(370, 117, 90, CTC02.getPreferredSize().height);

            //---- CTC03 ----
            CTC03.setName("CTC03");
            xTitledPanel1ContentContainer.add(CTC03);
            CTC03.setBounds(370, 144, 90, CTC03.getPreferredSize().height);

            //---- CTC04 ----
            CTC04.setName("CTC04");
            xTitledPanel1ContentContainer.add(CTC04);
            CTC04.setBounds(370, 171, 90, CTC04.getPreferredSize().height);

            //---- CTC05 ----
            CTC05.setName("CTC05");
            xTitledPanel1ContentContainer.add(CTC05);
            CTC05.setBounds(370, 198, 90, CTC05.getPreferredSize().height);

            //---- CTC06 ----
            CTC06.setName("CTC06");
            xTitledPanel1ContentContainer.add(CTC06);
            CTC06.setBounds(370, 225, 90, CTC06.getPreferredSize().height);

            //---- CTC07 ----
            CTC07.setName("CTC07");
            xTitledPanel1ContentContainer.add(CTC07);
            CTC07.setBounds(370, 252, 90, CTC07.getPreferredSize().height);

            //---- CTC08 ----
            CTC08.setName("CTC08");
            xTitledPanel1ContentContainer.add(CTC08);
            CTC08.setBounds(370, 279, 90, CTC08.getPreferredSize().height);

            //---- CTC09 ----
            CTC09.setName("CTC09");
            xTitledPanel1ContentContainer.add(CTC09);
            CTC09.setBounds(370, 306, 90, CTC09.getPreferredSize().height);

            //---- CTC010 ----
            CTC010.setName("CTC010");
            xTitledPanel1ContentContainer.add(CTC010);
            CTC010.setBounds(370, 333, 90, CTC010.getPreferredSize().height);

            //---- label13 ----
            label13.setText("Coefficient");
            label13.setHorizontalAlignment(SwingConstants.CENTER);
            label13.setFont(label13.getFont().deriveFont(label13.getFont().getStyle() | Font.BOLD));
            label13.setName("label13");
            xTitledPanel1ContentContainer.add(label13);
            label13.setBounds(370, 70, 90, 25);

            //======== panel2 ========
            {
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- CTCE2_MOINS ----
              CTCE2_MOINS.setText("-");
              CTCE2_MOINS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CTCE2_MOINS.setName("CTCE2_MOINS");
              panel2.add(CTCE2_MOINS);
              CTCE2_MOINS.setBounds(115, 47, 30, 28);

              //---- CTCE2 ----
              CTCE2.setText("+");
              CTCE2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CTCE2.setName("CTCE2");
              panel2.add(CTCE2);
              CTCE2.setBounds(80, 47, 30, 28);

              //---- CTCE3 ----
              CTCE3.setText("+");
              CTCE3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CTCE3.setName("CTCE3");
              panel2.add(CTCE3);
              CTCE3.setBounds(80, 74, 30, 28);

              //---- CTCE3_MOINS ----
              CTCE3_MOINS.setText("-");
              CTCE3_MOINS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CTCE3_MOINS.setName("CTCE3_MOINS");
              panel2.add(CTCE3_MOINS);
              CTCE3_MOINS.setBounds(115, 74, 30, 28);

              //---- CTCE4 ----
              CTCE4.setText("+");
              CTCE4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CTCE4.setName("CTCE4");
              panel2.add(CTCE4);
              CTCE4.setBounds(80, 101, 30, 28);

              //---- CTCE4_MOINS ----
              CTCE4_MOINS.setText("-");
              CTCE4_MOINS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CTCE4_MOINS.setName("CTCE4_MOINS");
              panel2.add(CTCE4_MOINS);
              CTCE4_MOINS.setBounds(115, 101, 30, 28);

              //---- CTCE7 ----
              CTCE7.setText("+");
              CTCE7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CTCE7.setName("CTCE7");
              panel2.add(CTCE7);
              CTCE7.setBounds(80, 182, 30, 28);

              //---- CTCE7_MOINS ----
              CTCE7_MOINS.setText("-");
              CTCE7_MOINS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CTCE7_MOINS.setName("CTCE7_MOINS");
              panel2.add(CTCE7_MOINS);
              CTCE7_MOINS.setBounds(115, 182, 30, 28);

              //---- CTCE6_MOINS ----
              CTCE6_MOINS.setText("-");
              CTCE6_MOINS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CTCE6_MOINS.setName("CTCE6_MOINS");
              panel2.add(CTCE6_MOINS);
              CTCE6_MOINS.setBounds(115, 155, 30, 28);

              //---- CTCE6 ----
              CTCE6.setText("+");
              CTCE6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CTCE6.setName("CTCE6");
              panel2.add(CTCE6);
              CTCE6.setBounds(80, 155, 30, 28);

              //---- CTCE5 ----
              CTCE5.setText("+");
              CTCE5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CTCE5.setName("CTCE5");
              panel2.add(CTCE5);
              CTCE5.setBounds(80, 128, 30, 28);

              //---- CTCE5_MOINS ----
              CTCE5_MOINS.setText("-");
              CTCE5_MOINS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CTCE5_MOINS.setName("CTCE5_MOINS");
              panel2.add(CTCE5_MOINS);
              CTCE5_MOINS.setBounds(115, 128, 30, 28);

              //---- CTCE10 ----
              CTCE10.setText("+");
              CTCE10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CTCE10.setName("CTCE10");
              panel2.add(CTCE10);
              CTCE10.setBounds(80, 263, 30, 28);

              //---- CTCE10_MOINS ----
              CTCE10_MOINS.setText("-");
              CTCE10_MOINS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CTCE10_MOINS.setName("CTCE10_MOINS");
              panel2.add(CTCE10_MOINS);
              CTCE10_MOINS.setBounds(115, 263, 30, 28);

              //---- CTCE9_MOINS ----
              CTCE9_MOINS.setText("-");
              CTCE9_MOINS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CTCE9_MOINS.setName("CTCE9_MOINS");
              panel2.add(CTCE9_MOINS);
              CTCE9_MOINS.setBounds(115, 236, 30, 28);

              //---- CTCE9 ----
              CTCE9.setText("+");
              CTCE9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CTCE9.setName("CTCE9");
              panel2.add(CTCE9);
              CTCE9.setBounds(80, 236, 30, 28);

              //---- CTCE8 ----
              CTCE8.setText("+");
              CTCE8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CTCE8.setName("CTCE8");
              panel2.add(CTCE8);
              CTCE8.setBounds(80, 209, 30, 28);

              //---- CTCE8_MOINS ----
              CTCE8_MOINS.setText("-");
              CTCE8_MOINS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CTCE8_MOINS.setName("CTCE8_MOINS");
              panel2.add(CTCE8_MOINS);
              CTCE8_MOINS.setBounds(115, 209, 30, 28);

              //---- label1 ----
              label1.setText("1");
              label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
              label1.setHorizontalAlignment(SwingConstants.RIGHT);
              label1.setName("label1");
              panel2.add(label1);
              label1.setBounds(35, 20, 20, 28);

              //---- label2 ----
              label2.setText("2");
              label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
              label2.setHorizontalAlignment(SwingConstants.RIGHT);
              label2.setName("label2");
              panel2.add(label2);
              label2.setBounds(35, 47, 20, 28);

              //---- label3 ----
              label3.setText("3");
              label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
              label3.setHorizontalAlignment(SwingConstants.RIGHT);
              label3.setName("label3");
              panel2.add(label3);
              label3.setBounds(35, 74, 20, 28);

              //---- label4 ----
              label4.setText("4");
              label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
              label4.setHorizontalAlignment(SwingConstants.RIGHT);
              label4.setName("label4");
              panel2.add(label4);
              label4.setBounds(35, 101, 20, 28);

              //---- label5 ----
              label5.setText("5");
              label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
              label5.setHorizontalAlignment(SwingConstants.RIGHT);
              label5.setName("label5");
              panel2.add(label5);
              label5.setBounds(35, 128, 20, 28);

              //---- label6 ----
              label6.setText("6");
              label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
              label6.setHorizontalAlignment(SwingConstants.RIGHT);
              label6.setName("label6");
              panel2.add(label6);
              label6.setBounds(35, 155, 20, 28);

              //---- label7 ----
              label7.setText("7");
              label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD));
              label7.setHorizontalAlignment(SwingConstants.RIGHT);
              label7.setName("label7");
              panel2.add(label7);
              label7.setBounds(35, 182, 20, 28);

              //---- label8 ----
              label8.setText("8");
              label8.setFont(label8.getFont().deriveFont(label8.getFont().getStyle() | Font.BOLD));
              label8.setHorizontalAlignment(SwingConstants.RIGHT);
              label8.setName("label8");
              panel2.add(label8);
              label8.setBounds(35, 209, 20, 28);

              //---- label9 ----
              label9.setText("9");
              label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD));
              label9.setHorizontalAlignment(SwingConstants.RIGHT);
              label9.setName("label9");
              panel2.add(label9);
              label9.setBounds(35, 236, 20, 28);

              //---- label10 ----
              label10.setText("10");
              label10.setFont(label10.getFont().deriveFont(label10.getFont().getStyle() | Font.BOLD));
              label10.setHorizontalAlignment(SwingConstants.RIGHT);
              label10.setName("label10");
              panel2.add(label10);
              label10.setBounds(35, 263, 20, 28);

              //---- CTCD1 ----
              CTCD1.setName("CTCD1");
              panel2.add(CTCD1);
              CTCD1.setBounds(165, 20, 70, CTCD1.getPreferredSize().height);

              //---- CTCD2 ----
              CTCD2.setName("CTCD2");
              panel2.add(CTCD2);
              CTCD2.setBounds(165, 47, 70, CTCD2.getPreferredSize().height);

              //---- CTCD3 ----
              CTCD3.setName("CTCD3");
              panel2.add(CTCD3);
              CTCD3.setBounds(165, 74, 70, CTCD3.getPreferredSize().height);

              //---- CTCD4 ----
              CTCD4.setName("CTCD4");
              panel2.add(CTCD4);
              CTCD4.setBounds(165, 101, 70, CTCD4.getPreferredSize().height);

              //---- CTCD5 ----
              CTCD5.setName("CTCD5");
              panel2.add(CTCD5);
              CTCD5.setBounds(165, 128, 70, CTCD5.getPreferredSize().height);

              //---- CTCD6 ----
              CTCD6.setName("CTCD6");
              panel2.add(CTCD6);
              CTCD6.setBounds(165, 155, 70, CTCD6.getPreferredSize().height);

              //---- CTCD7 ----
              CTCD7.setName("CTCD7");
              panel2.add(CTCD7);
              CTCD7.setBounds(165, 182, 70, CTCD7.getPreferredSize().height);

              //---- CTCD8 ----
              CTCD8.setName("CTCD8");
              panel2.add(CTCD8);
              CTCD8.setBounds(165, 209, 70, CTCD8.getPreferredSize().height);

              //---- CTCD9 ----
              CTCD9.setName("CTCD9");
              panel2.add(CTCD9);
              CTCD9.setBounds(165, 236, 70, CTCD9.getPreferredSize().height);

              //---- CTCD10 ----
              CTCD10.setName("CTCD10");
              panel2.add(CTCD10);
              CTCD10.setBounds(165, 263, 70, CTCD10.getPreferredSize().height);

              //---- CTOP1 ----
              CTOP1.setName("CTOP1");
              panel2.add(CTOP1);
              CTOP1.setBounds(250, 20, 20, CTOP1.getPreferredSize().height);

              //---- CTOP2 ----
              CTOP2.setName("CTOP2");
              panel2.add(CTOP2);
              CTOP2.setBounds(250, 47, 20, CTOP2.getPreferredSize().height);

              //---- CTOP3 ----
              CTOP3.setName("CTOP3");
              panel2.add(CTOP3);
              CTOP3.setBounds(250, 74, 20, CTOP3.getPreferredSize().height);

              //---- CTOP4 ----
              CTOP4.setName("CTOP4");
              panel2.add(CTOP4);
              CTOP4.setBounds(250, 101, 20, CTOP4.getPreferredSize().height);

              //---- CTOP5 ----
              CTOP5.setName("CTOP5");
              panel2.add(CTOP5);
              CTOP5.setBounds(250, 128, 20, CTOP5.getPreferredSize().height);

              //---- CTOP6 ----
              CTOP6.setName("CTOP6");
              panel2.add(CTOP6);
              CTOP6.setBounds(250, 155, 20, CTOP6.getPreferredSize().height);

              //---- CTOP7 ----
              CTOP7.setName("CTOP7");
              panel2.add(CTOP7);
              CTOP7.setBounds(250, 182, 20, CTOP7.getPreferredSize().height);

              //---- CTOP8 ----
              CTOP8.setName("CTOP8");
              panel2.add(CTOP8);
              CTOP8.setBounds(250, 209, 20, CTOP8.getPreferredSize().height);

              //---- CTOP9 ----
              CTOP9.setName("CTOP9");
              panel2.add(CTOP9);
              CTOP9.setBounds(250, 236, 20, CTOP9.getPreferredSize().height);

              //---- CTOP10 ----
              CTOP10.setName("CTOP10");
              panel2.add(CTOP10);
              CTOP10.setBounds(250, 263, 20, CTOP10.getPreferredSize().height);

              //---- label11 ----
              label11.setText("N\u00b0compte");
              label11.setHorizontalAlignment(SwingConstants.CENTER);
              label11.setFont(label11.getFont().deriveFont(label11.getFont().getStyle() | Font.BOLD));
              label11.setName("label11");
              panel2.add(label11);
              label11.setBounds(165, 0, 70, 25);

              //---- label12 ----
              label12.setText("Op\u00e9");
              label12.setHorizontalAlignment(SwingConstants.CENTER);
              label12.setFont(label12.getFont().deriveFont(label12.getFont().getStyle() | Font.BOLD));
              label12.setName("label12");
              panel2.add(label12);
              label12.setBounds(250, 0, 25, 25);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel2);
            panel2.setBounds(65, 70, 305, 295);

            //======== panel1 ========
            {
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- label18 ----
              label18.setText("Taux de TVA n\u00b05");
              label18.setFont(label18.getFont().deriveFont(label18.getFont().getStyle() | Font.BOLD));
              label18.setName("label18");
              panel1.add(label18);
              label18.setBounds(35, 128, 110, 28);

              //---- label17 ----
              label17.setText("Taux de TVA n\u00b04");
              label17.setFont(label17.getFont().deriveFont(label17.getFont().getStyle() | Font.BOLD));
              label17.setName("label17");
              panel1.add(label17);
              label17.setBounds(35, 101, 110, 28);

              //---- label16 ----
              label16.setText("Taux de TVA n\u00b03");
              label16.setFont(label16.getFont().deriveFont(label16.getFont().getStyle() | Font.BOLD));
              label16.setName("label16");
              panel1.add(label16);
              label16.setBounds(35, 74, 110, 28);

              //---- label15 ----
              label15.setText("Taux de TVA n\u00b02");
              label15.setFont(label15.getFont().deriveFont(label15.getFont().getStyle() | Font.BOLD));
              label15.setName("label15");
              panel1.add(label15);
              label15.setBounds(35, 47, 110, 28);

              //---- label14 ----
              label14.setText("Taux de TVA n\u00b01");
              label14.setFont(label14.getFont().deriveFont(label14.getFont().getStyle() | Font.BOLD));
              label14.setName("label14");
              panel1.add(label14);
              label14.setBounds(35, 20, 110, 28);

              //---- label19 ----
              label19.setText("Taux de TVA n\u00b06");
              label19.setFont(label19.getFont().deriveFont(label19.getFont().getStyle() | Font.BOLD));
              label19.setName("label19");
              panel1.add(label19);
              label19.setBounds(35, 155, 110, 28);

              //---- label20 ----
              label20.setText("Taux de TVA n\u00b07");
              label20.setFont(label20.getFont().deriveFont(label20.getFont().getStyle() | Font.BOLD));
              label20.setName("label20");
              panel1.add(label20);
              label20.setBounds(35, 182, 110, 28);

              //---- label21 ----
              label21.setText("Taux de TVA n\u00b08");
              label21.setFont(label21.getFont().deriveFont(label21.getFont().getStyle() | Font.BOLD));
              label21.setName("label21");
              panel1.add(label21);
              label21.setBounds(35, 209, 110, 28);

              //---- label22 ----
              label22.setText("Taux de TVA n\u00b09");
              label22.setFont(label22.getFont().deriveFont(label22.getFont().getStyle() | Font.BOLD));
              label22.setName("label22");
              panel1.add(label22);
              label22.setBounds(35, 236, 110, 28);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel1);
            panel1.setBounds(220, 70, 150, 295);

            //======== panel3 ========
            {
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- OBJ_58_OBJ_58 ----
              OBJ_58_OBJ_58.setText("N\u00b0CO cr\u00e9dit");
              OBJ_58_OBJ_58.setName("OBJ_58_OBJ_58");
              panel3.add(OBJ_58_OBJ_58);
              OBJ_58_OBJ_58.setBounds(285, 30, 72, 28);

              //---- OBJ_57_OBJ_57 ----
              OBJ_57_OBJ_57.setText("N\u00b0CO d\u00e9bit");
              OBJ_57_OBJ_57.setName("OBJ_57_OBJ_57");
              panel3.add(OBJ_57_OBJ_57);
              OBJ_57_OBJ_57.setBounds(90, 30, 69, 28);

              //---- CTCOD ----
              CTCOD.setName("CTCOD");
              panel3.add(CTCOD);
              CTCOD.setBounds(175, 30, 40, CTCOD.getPreferredSize().height);

              //---- CTCOC ----
              CTCOC.setName("CTCOC");
              panel3.add(CTCOC);
              CTCOC.setBounds(365, 30, 40, CTCOC.getPreferredSize().height);

              //---- separator1 ----
              separator1.setName("separator1");
              panel3.add(separator1);
              separator1.setBounds(5, 5, 600, separator1.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel3);
            panel3.setBounds(55, 365, 620, 70);

            //======== panel4 ========
            {
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);

              //---- CTSNS ----
              CTSNS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CTSNS.setName("CTSNS");
              panel4.add(CTSNS);
              CTSNS.setBounds(180, 40, 95, CTSNS.getPreferredSize().height);

              //---- CTSNE ----
              CTSNE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CTSNE.setName("CTSNE");
              panel4.add(CTSNE);
              CTSNE.setBounds(470, 40, 125, CTSNE.getPreferredSize().height);

              //---- CTLIB ----
              CTLIB.setName("CTLIB");
              panel4.add(CTLIB);
              CTLIB.setBounds(180, 5, 310, CTLIB.getPreferredSize().height);

              //---- OBJ_54_OBJ_54 ----
              OBJ_54_OBJ_54.setText("Sens des \u00e9critures \u00e0 prendre");
              OBJ_54_OBJ_54.setName("OBJ_54_OBJ_54");
              panel4.add(OBJ_54_OBJ_54);
              OBJ_54_OBJ_54.setBounds(290, 39, 175, 28);

              //---- OBJ_56_OBJ_56 ----
              OBJ_56_OBJ_56.setText("Sens normal");
              OBJ_56_OBJ_56.setName("OBJ_56_OBJ_56");
              panel4.add(OBJ_56_OBJ_56);
              OBJ_56_OBJ_56.setBounds(45, 40, 84, 26);

              //---- OBJ_42_OBJ_42 ----
              OBJ_42_OBJ_42.setText("Libell\u00e9");
              OBJ_42_OBJ_42.setName("OBJ_42_OBJ_42");
              panel4.add(OBJ_42_OBJ_42);
              OBJ_42_OBJ_42.setBounds(45, 5, 61, 28);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel4.getComponentCount(); i++) {
                  Rectangle bounds = panel4.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel4.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel4.setMinimumSize(preferredSize);
                panel4.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel4);
            panel4.setBounds(55, 5, 655, 70);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 774, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 472, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }

    //---- CTCE2_GRP ----
    CTCE2_GRP.add(CTCE2_MOINS);
    CTCE2_GRP.add(CTCE2);

    //---- CTCE3_GRP ----
    CTCE3_GRP.add(CTCE3);
    CTCE3_GRP.add(CTCE3_MOINS);

    //---- CTCE4_GRP ----
    CTCE4_GRP.add(CTCE4);
    CTCE4_GRP.add(CTCE4_MOINS);

    //---- CTCE7_GRP ----
    CTCE7_GRP.add(CTCE7);
    CTCE7_GRP.add(CTCE7_MOINS);

    //---- CTCE6_GRP ----
    CTCE6_GRP.add(CTCE6_MOINS);
    CTCE6_GRP.add(CTCE6);

    //---- CTCE5_GRP ----
    CTCE5_GRP.add(CTCE5);
    CTCE5_GRP.add(CTCE5_MOINS);

    //---- CTCE10_GRP ----
    CTCE10_GRP.add(CTCE10);
    CTCE10_GRP.add(CTCE10_MOINS);

    //---- CTCE9_GRP ----
    CTCE9_GRP.add(CTCE9_MOINS);
    CTCE9_GRP.add(CTCE9);

    //---- CTCE8_GRP ----
    CTCE8_GRP.add(CTCE8);
    CTCE8_GRP.add(CTCE8_MOINS);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44_OBJ_45;
  private XRiTextField INDETB;
  private JLabel OBJ_45_OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_46_OBJ_47;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private XRiTextField CTC01;
  private XRiTextField CTC02;
  private XRiTextField CTC03;
  private XRiTextField CTC04;
  private XRiTextField CTC05;
  private XRiTextField CTC06;
  private XRiTextField CTC07;
  private XRiTextField CTC08;
  private XRiTextField CTC09;
  private XRiTextField CTC010;
  private JLabel label13;
  private JPanel panel2;
  private XRiRadioButton CTCE2_MOINS;
  private XRiRadioButton CTCE2;
  private XRiRadioButton CTCE3;
  private XRiRadioButton CTCE3_MOINS;
  private XRiRadioButton CTCE4;
  private XRiRadioButton CTCE4_MOINS;
  private XRiRadioButton CTCE7;
  private XRiRadioButton CTCE7_MOINS;
  private XRiRadioButton CTCE6_MOINS;
  private XRiRadioButton CTCE6;
  private XRiRadioButton CTCE5;
  private XRiRadioButton CTCE5_MOINS;
  private XRiRadioButton CTCE10;
  private XRiRadioButton CTCE10_MOINS;
  private XRiRadioButton CTCE9_MOINS;
  private XRiRadioButton CTCE9;
  private XRiRadioButton CTCE8;
  private XRiRadioButton CTCE8_MOINS;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private JLabel label10;
  private XRiTextField CTCD1;
  private XRiTextField CTCD2;
  private XRiTextField CTCD3;
  private XRiTextField CTCD4;
  private XRiTextField CTCD5;
  private XRiTextField CTCD6;
  private XRiTextField CTCD7;
  private XRiTextField CTCD8;
  private XRiTextField CTCD9;
  private XRiTextField CTCD10;
  private XRiTextField CTOP1;
  private XRiTextField CTOP2;
  private XRiTextField CTOP3;
  private XRiTextField CTOP4;
  private XRiTextField CTOP5;
  private XRiTextField CTOP6;
  private XRiTextField CTOP7;
  private XRiTextField CTOP8;
  private XRiTextField CTOP9;
  private XRiTextField CTOP10;
  private JLabel label11;
  private JLabel label12;
  private JPanel panel1;
  private JLabel label18;
  private JLabel label17;
  private JLabel label16;
  private JLabel label15;
  private JLabel label14;
  private JLabel label19;
  private JLabel label20;
  private JLabel label21;
  private JLabel label22;
  private JPanel panel3;
  private JLabel OBJ_58_OBJ_58;
  private JLabel OBJ_57_OBJ_57;
  private XRiTextField CTCOD;
  private XRiTextField CTCOC;
  private JComponent separator1;
  private JPanel panel4;
  private XRiComboBox CTSNS;
  private XRiComboBox CTSNE;
  private XRiTextField CTLIB;
  private JLabel OBJ_54_OBJ_54;
  private JLabel OBJ_56_OBJ_56;
  private JLabel OBJ_42_OBJ_42;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  private ButtonGroup CTCE2_GRP;
  private ButtonGroup CTCE3_GRP;
  private ButtonGroup CTCE4_GRP;
  private ButtonGroup CTCE7_GRP;
  private ButtonGroup CTCE6_GRP;
  private ButtonGroup CTCE5_GRP;
  private ButtonGroup CTCE10_GRP;
  private ButtonGroup CTCE9_GRP;
  private ButtonGroup CTCE8_GRP;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
