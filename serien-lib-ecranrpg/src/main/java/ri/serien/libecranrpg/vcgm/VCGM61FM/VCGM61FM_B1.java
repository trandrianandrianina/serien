
package ri.serien.libecranrpg.vcgm.VCGM61FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM61FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM61FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
    
    

    
    p_bpresentation.setCodeEtablissement(WSOC.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WSOC.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void riBoutonRecherche1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    WSOC = new XRiTextField();
    OBJ_46 = new JLabel();
    riBoutonRecherche1 = new SNBoutonRecherche();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    OBJ_29 = new JLabel();
    WCOL = new XRiTextField();
    xTitledPanel2 = new JXTitledPanel();
    LIB01 = new XRiTextField();
    LIB02 = new XRiTextField();
    LIB03 = new XRiTextField();
    LIB04 = new XRiTextField();
    LIB05 = new XRiTextField();
    LIB06 = new XRiTextField();
    LIB07 = new XRiTextField();
    LIB08 = new XRiTextField();
    LIB09 = new XRiTextField();
    LIB10 = new XRiTextField();
    LIB11 = new XRiTextField();
    LIB12 = new XRiTextField();
    LIB13 = new XRiTextField();
    LIB14 = new XRiTextField();
    OBJ_28 = new JLabel();
    OBJ_45 = new JLabel();
    R0101 = new XRiTextField();
    R0102 = new XRiTextField();
    R0103 = new XRiTextField();
    R0104 = new XRiTextField();
    R0105 = new XRiTextField();
    R0106 = new XRiTextField();
    R0107 = new XRiTextField();
    R0108 = new XRiTextField();
    R0109 = new XRiTextField();
    R0110 = new XRiTextField();
    R0201 = new XRiTextField();
    R0202 = new XRiTextField();
    R0203 = new XRiTextField();
    R0204 = new XRiTextField();
    R0205 = new XRiTextField();
    R0206 = new XRiTextField();
    R0207 = new XRiTextField();
    R0208 = new XRiTextField();
    R0209 = new XRiTextField();
    R0210 = new XRiTextField();
    R0301 = new XRiTextField();
    R0302 = new XRiTextField();
    R0303 = new XRiTextField();
    R0304 = new XRiTextField();
    R0305 = new XRiTextField();
    R0306 = new XRiTextField();
    R0307 = new XRiTextField();
    R0308 = new XRiTextField();
    R0309 = new XRiTextField();
    R0310 = new XRiTextField();
    R0401 = new XRiTextField();
    R0402 = new XRiTextField();
    R0403 = new XRiTextField();
    R0404 = new XRiTextField();
    R0405 = new XRiTextField();
    R0406 = new XRiTextField();
    R0407 = new XRiTextField();
    R0408 = new XRiTextField();
    R0409 = new XRiTextField();
    R0410 = new XRiTextField();
    R0501 = new XRiTextField();
    R0502 = new XRiTextField();
    R0503 = new XRiTextField();
    R0504 = new XRiTextField();
    R0505 = new XRiTextField();
    R0506 = new XRiTextField();
    R0507 = new XRiTextField();
    R0508 = new XRiTextField();
    R0509 = new XRiTextField();
    R0510 = new XRiTextField();
    R0601 = new XRiTextField();
    R0602 = new XRiTextField();
    R0603 = new XRiTextField();
    R0604 = new XRiTextField();
    R0605 = new XRiTextField();
    R0606 = new XRiTextField();
    R0607 = new XRiTextField();
    R0608 = new XRiTextField();
    R0609 = new XRiTextField();
    R0610 = new XRiTextField();
    R0701 = new XRiTextField();
    R0702 = new XRiTextField();
    R0703 = new XRiTextField();
    R0704 = new XRiTextField();
    R0705 = new XRiTextField();
    R0706 = new XRiTextField();
    R0707 = new XRiTextField();
    R0708 = new XRiTextField();
    R0709 = new XRiTextField();
    R0710 = new XRiTextField();
    R0801 = new XRiTextField();
    R0802 = new XRiTextField();
    R0803 = new XRiTextField();
    R0804 = new XRiTextField();
    R0805 = new XRiTextField();
    R0806 = new XRiTextField();
    R0807 = new XRiTextField();
    R0808 = new XRiTextField();
    R0809 = new XRiTextField();
    R0810 = new XRiTextField();
    R0901 = new XRiTextField();
    R0902 = new XRiTextField();
    R0903 = new XRiTextField();
    R0904 = new XRiTextField();
    R0905 = new XRiTextField();
    R0906 = new XRiTextField();
    R0907 = new XRiTextField();
    R0908 = new XRiTextField();
    R0909 = new XRiTextField();
    R0910 = new XRiTextField();
    R1001 = new XRiTextField();
    R1002 = new XRiTextField();
    R1003 = new XRiTextField();
    R1004 = new XRiTextField();
    R1005 = new XRiTextField();
    R1006 = new XRiTextField();
    R1007 = new XRiTextField();
    R1008 = new XRiTextField();
    R1009 = new XRiTextField();
    R1010 = new XRiTextField();
    R1101 = new XRiTextField();
    R1102 = new XRiTextField();
    R1103 = new XRiTextField();
    R1104 = new XRiTextField();
    R1105 = new XRiTextField();
    R1106 = new XRiTextField();
    R1107 = new XRiTextField();
    R1108 = new XRiTextField();
    R1109 = new XRiTextField();
    R1110 = new XRiTextField();
    R1201 = new XRiTextField();
    R1202 = new XRiTextField();
    R1203 = new XRiTextField();
    R1204 = new XRiTextField();
    R1205 = new XRiTextField();
    R1206 = new XRiTextField();
    R1207 = new XRiTextField();
    R1208 = new XRiTextField();
    R1209 = new XRiTextField();
    R1210 = new XRiTextField();
    R1301 = new XRiTextField();
    R1302 = new XRiTextField();
    R1303 = new XRiTextField();
    R1304 = new XRiTextField();
    R1305 = new XRiTextField();
    R1306 = new XRiTextField();
    R1307 = new XRiTextField();
    R1308 = new XRiTextField();
    R1309 = new XRiTextField();
    R1310 = new XRiTextField();
    R1401 = new XRiTextField();
    R1402 = new XRiTextField();
    R1403 = new XRiTextField();
    R1404 = new XRiTextField();
    R1405 = new XRiTextField();
    R1406 = new XRiTextField();
    R1407 = new XRiTextField();
    R1408 = new XRiTextField();
    R1409 = new XRiTextField();
    R1410 = new XRiTextField();
    OBJ_31 = new JLabel();
    OBJ_32 = new JLabel();
    OBJ_33 = new JLabel();
    OBJ_34 = new JLabel();
    OBJ_35 = new JLabel();
    OBJ_36 = new JLabel();
    OBJ_37 = new JLabel();
    OBJ_38 = new JLabel();
    OBJ_39 = new JLabel();
    OBJ_40 = new JLabel();
    OBJ_41 = new JLabel();
    OBJ_42 = new JLabel();
    OBJ_43 = new JLabel();
    OBJ_44 = new JLabel();
    OBJ_30 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de l'\u00e9ch\u00e9ancier");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- WSOC ----
          WSOC.setName("WSOC");

          //---- OBJ_46 ----
          OBJ_46.setText("Soci\u00e9t\u00e9");
          OBJ_46.setName("OBJ_46");

          //---- riBoutonRecherche1 ----
          riBoutonRecherche1.setName("riBoutonRecherche1");
          riBoutonRecherche1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonRecherche1ActionPerformed(e);
            }
          });

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(WSOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(riBoutonRecherche1, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(699, Short.MAX_VALUE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_46))
              .addGroup(p_tete_gaucheLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(WSOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(riBoutonRecherche1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(830, 560));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- OBJ_29 ----
          OBJ_29.setText("Collectif \u00e0 traiter");
          OBJ_29.setName("OBJ_29");

          //---- WCOL ----
          WCOL.setComponentPopupMenu(BTD);
          WCOL.setName("WCOL");

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setScrollableTracksViewportHeight(true);
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();

            //---- LIB01 ----
            LIB01.setComponentPopupMenu(null);
            LIB01.setName("LIB01");

            //---- LIB02 ----
            LIB02.setComponentPopupMenu(null);
            LIB02.setName("LIB02");

            //---- LIB03 ----
            LIB03.setComponentPopupMenu(null);
            LIB03.setName("LIB03");

            //---- LIB04 ----
            LIB04.setComponentPopupMenu(null);
            LIB04.setName("LIB04");

            //---- LIB05 ----
            LIB05.setComponentPopupMenu(null);
            LIB05.setName("LIB05");

            //---- LIB06 ----
            LIB06.setComponentPopupMenu(null);
            LIB06.setName("LIB06");

            //---- LIB07 ----
            LIB07.setComponentPopupMenu(null);
            LIB07.setName("LIB07");

            //---- LIB08 ----
            LIB08.setComponentPopupMenu(null);
            LIB08.setName("LIB08");

            //---- LIB09 ----
            LIB09.setComponentPopupMenu(null);
            LIB09.setName("LIB09");

            //---- LIB10 ----
            LIB10.setComponentPopupMenu(null);
            LIB10.setName("LIB10");

            //---- LIB11 ----
            LIB11.setComponentPopupMenu(null);
            LIB11.setName("LIB11");

            //---- LIB12 ----
            LIB12.setComponentPopupMenu(null);
            LIB12.setName("LIB12");

            //---- LIB13 ----
            LIB13.setComponentPopupMenu(null);
            LIB13.setName("LIB13");

            //---- LIB14 ----
            LIB14.setComponentPopupMenu(null);
            LIB14.setName("LIB14");

            //---- OBJ_28 ----
            OBJ_28.setText("Codes r\u00e8glements \u00e0 cumuler");
            OBJ_28.setName("OBJ_28");

            //---- OBJ_45 ----
            OBJ_45.setText("Libell\u00e9");
            OBJ_45.setName("OBJ_45");

            //---- R0101 ----
            R0101.setComponentPopupMenu(BTD);
            R0101.setName("R0101");

            //---- R0102 ----
            R0102.setComponentPopupMenu(BTD);
            R0102.setName("R0102");

            //---- R0103 ----
            R0103.setComponentPopupMenu(BTD);
            R0103.setName("R0103");

            //---- R0104 ----
            R0104.setComponentPopupMenu(BTD);
            R0104.setName("R0104");

            //---- R0105 ----
            R0105.setComponentPopupMenu(BTD);
            R0105.setName("R0105");

            //---- R0106 ----
            R0106.setComponentPopupMenu(BTD);
            R0106.setName("R0106");

            //---- R0107 ----
            R0107.setComponentPopupMenu(BTD);
            R0107.setName("R0107");

            //---- R0108 ----
            R0108.setComponentPopupMenu(BTD);
            R0108.setName("R0108");

            //---- R0109 ----
            R0109.setComponentPopupMenu(BTD);
            R0109.setName("R0109");

            //---- R0110 ----
            R0110.setComponentPopupMenu(BTD);
            R0110.setName("R0110");

            //---- R0201 ----
            R0201.setComponentPopupMenu(BTD);
            R0201.setName("R0201");

            //---- R0202 ----
            R0202.setComponentPopupMenu(BTD);
            R0202.setName("R0202");

            //---- R0203 ----
            R0203.setComponentPopupMenu(BTD);
            R0203.setName("R0203");

            //---- R0204 ----
            R0204.setComponentPopupMenu(BTD);
            R0204.setName("R0204");

            //---- R0205 ----
            R0205.setComponentPopupMenu(BTD);
            R0205.setName("R0205");

            //---- R0206 ----
            R0206.setComponentPopupMenu(BTD);
            R0206.setName("R0206");

            //---- R0207 ----
            R0207.setComponentPopupMenu(BTD);
            R0207.setName("R0207");

            //---- R0208 ----
            R0208.setComponentPopupMenu(BTD);
            R0208.setName("R0208");

            //---- R0209 ----
            R0209.setComponentPopupMenu(BTD);
            R0209.setName("R0209");

            //---- R0210 ----
            R0210.setComponentPopupMenu(BTD);
            R0210.setName("R0210");

            //---- R0301 ----
            R0301.setComponentPopupMenu(BTD);
            R0301.setName("R0301");

            //---- R0302 ----
            R0302.setComponentPopupMenu(BTD);
            R0302.setName("R0302");

            //---- R0303 ----
            R0303.setComponentPopupMenu(BTD);
            R0303.setName("R0303");

            //---- R0304 ----
            R0304.setComponentPopupMenu(BTD);
            R0304.setName("R0304");

            //---- R0305 ----
            R0305.setComponentPopupMenu(BTD);
            R0305.setName("R0305");

            //---- R0306 ----
            R0306.setComponentPopupMenu(BTD);
            R0306.setName("R0306");

            //---- R0307 ----
            R0307.setComponentPopupMenu(BTD);
            R0307.setName("R0307");

            //---- R0308 ----
            R0308.setComponentPopupMenu(BTD);
            R0308.setName("R0308");

            //---- R0309 ----
            R0309.setComponentPopupMenu(BTD);
            R0309.setName("R0309");

            //---- R0310 ----
            R0310.setComponentPopupMenu(BTD);
            R0310.setName("R0310");

            //---- R0401 ----
            R0401.setComponentPopupMenu(BTD);
            R0401.setName("R0401");

            //---- R0402 ----
            R0402.setComponentPopupMenu(BTD);
            R0402.setName("R0402");

            //---- R0403 ----
            R0403.setComponentPopupMenu(BTD);
            R0403.setName("R0403");

            //---- R0404 ----
            R0404.setComponentPopupMenu(BTD);
            R0404.setName("R0404");

            //---- R0405 ----
            R0405.setComponentPopupMenu(BTD);
            R0405.setName("R0405");

            //---- R0406 ----
            R0406.setComponentPopupMenu(BTD);
            R0406.setName("R0406");

            //---- R0407 ----
            R0407.setComponentPopupMenu(BTD);
            R0407.setName("R0407");

            //---- R0408 ----
            R0408.setComponentPopupMenu(BTD);
            R0408.setName("R0408");

            //---- R0409 ----
            R0409.setComponentPopupMenu(BTD);
            R0409.setName("R0409");

            //---- R0410 ----
            R0410.setComponentPopupMenu(BTD);
            R0410.setName("R0410");

            //---- R0501 ----
            R0501.setComponentPopupMenu(BTD);
            R0501.setName("R0501");

            //---- R0502 ----
            R0502.setComponentPopupMenu(BTD);
            R0502.setName("R0502");

            //---- R0503 ----
            R0503.setComponentPopupMenu(BTD);
            R0503.setName("R0503");

            //---- R0504 ----
            R0504.setComponentPopupMenu(BTD);
            R0504.setName("R0504");

            //---- R0505 ----
            R0505.setComponentPopupMenu(BTD);
            R0505.setName("R0505");

            //---- R0506 ----
            R0506.setComponentPopupMenu(BTD);
            R0506.setName("R0506");

            //---- R0507 ----
            R0507.setComponentPopupMenu(BTD);
            R0507.setName("R0507");

            //---- R0508 ----
            R0508.setComponentPopupMenu(BTD);
            R0508.setName("R0508");

            //---- R0509 ----
            R0509.setComponentPopupMenu(BTD);
            R0509.setName("R0509");

            //---- R0510 ----
            R0510.setComponentPopupMenu(BTD);
            R0510.setName("R0510");

            //---- R0601 ----
            R0601.setComponentPopupMenu(BTD);
            R0601.setName("R0601");

            //---- R0602 ----
            R0602.setComponentPopupMenu(BTD);
            R0602.setName("R0602");

            //---- R0603 ----
            R0603.setComponentPopupMenu(BTD);
            R0603.setName("R0603");

            //---- R0604 ----
            R0604.setComponentPopupMenu(BTD);
            R0604.setName("R0604");

            //---- R0605 ----
            R0605.setComponentPopupMenu(BTD);
            R0605.setName("R0605");

            //---- R0606 ----
            R0606.setComponentPopupMenu(BTD);
            R0606.setName("R0606");

            //---- R0607 ----
            R0607.setComponentPopupMenu(BTD);
            R0607.setName("R0607");

            //---- R0608 ----
            R0608.setComponentPopupMenu(BTD);
            R0608.setName("R0608");

            //---- R0609 ----
            R0609.setComponentPopupMenu(BTD);
            R0609.setName("R0609");

            //---- R0610 ----
            R0610.setComponentPopupMenu(BTD);
            R0610.setName("R0610");

            //---- R0701 ----
            R0701.setComponentPopupMenu(BTD);
            R0701.setName("R0701");

            //---- R0702 ----
            R0702.setComponentPopupMenu(BTD);
            R0702.setName("R0702");

            //---- R0703 ----
            R0703.setComponentPopupMenu(BTD);
            R0703.setName("R0703");

            //---- R0704 ----
            R0704.setComponentPopupMenu(BTD);
            R0704.setName("R0704");

            //---- R0705 ----
            R0705.setComponentPopupMenu(BTD);
            R0705.setName("R0705");

            //---- R0706 ----
            R0706.setComponentPopupMenu(BTD);
            R0706.setName("R0706");

            //---- R0707 ----
            R0707.setComponentPopupMenu(BTD);
            R0707.setName("R0707");

            //---- R0708 ----
            R0708.setComponentPopupMenu(BTD);
            R0708.setName("R0708");

            //---- R0709 ----
            R0709.setComponentPopupMenu(BTD);
            R0709.setName("R0709");

            //---- R0710 ----
            R0710.setComponentPopupMenu(BTD);
            R0710.setName("R0710");

            //---- R0801 ----
            R0801.setComponentPopupMenu(BTD);
            R0801.setName("R0801");

            //---- R0802 ----
            R0802.setComponentPopupMenu(BTD);
            R0802.setName("R0802");

            //---- R0803 ----
            R0803.setComponentPopupMenu(BTD);
            R0803.setName("R0803");

            //---- R0804 ----
            R0804.setComponentPopupMenu(BTD);
            R0804.setName("R0804");

            //---- R0805 ----
            R0805.setComponentPopupMenu(BTD);
            R0805.setName("R0805");

            //---- R0806 ----
            R0806.setComponentPopupMenu(BTD);
            R0806.setName("R0806");

            //---- R0807 ----
            R0807.setComponentPopupMenu(BTD);
            R0807.setName("R0807");

            //---- R0808 ----
            R0808.setComponentPopupMenu(BTD);
            R0808.setName("R0808");

            //---- R0809 ----
            R0809.setComponentPopupMenu(BTD);
            R0809.setName("R0809");

            //---- R0810 ----
            R0810.setComponentPopupMenu(BTD);
            R0810.setName("R0810");

            //---- R0901 ----
            R0901.setComponentPopupMenu(BTD);
            R0901.setName("R0901");

            //---- R0902 ----
            R0902.setComponentPopupMenu(BTD);
            R0902.setName("R0902");

            //---- R0903 ----
            R0903.setComponentPopupMenu(BTD);
            R0903.setName("R0903");

            //---- R0904 ----
            R0904.setComponentPopupMenu(BTD);
            R0904.setName("R0904");

            //---- R0905 ----
            R0905.setComponentPopupMenu(BTD);
            R0905.setName("R0905");

            //---- R0906 ----
            R0906.setComponentPopupMenu(BTD);
            R0906.setName("R0906");

            //---- R0907 ----
            R0907.setComponentPopupMenu(BTD);
            R0907.setName("R0907");

            //---- R0908 ----
            R0908.setComponentPopupMenu(BTD);
            R0908.setName("R0908");

            //---- R0909 ----
            R0909.setComponentPopupMenu(BTD);
            R0909.setName("R0909");

            //---- R0910 ----
            R0910.setComponentPopupMenu(BTD);
            R0910.setName("R0910");

            //---- R1001 ----
            R1001.setComponentPopupMenu(BTD);
            R1001.setName("R1001");

            //---- R1002 ----
            R1002.setComponentPopupMenu(BTD);
            R1002.setName("R1002");

            //---- R1003 ----
            R1003.setComponentPopupMenu(BTD);
            R1003.setName("R1003");

            //---- R1004 ----
            R1004.setComponentPopupMenu(BTD);
            R1004.setName("R1004");

            //---- R1005 ----
            R1005.setComponentPopupMenu(BTD);
            R1005.setName("R1005");

            //---- R1006 ----
            R1006.setComponentPopupMenu(BTD);
            R1006.setName("R1006");

            //---- R1007 ----
            R1007.setComponentPopupMenu(BTD);
            R1007.setName("R1007");

            //---- R1008 ----
            R1008.setComponentPopupMenu(BTD);
            R1008.setName("R1008");

            //---- R1009 ----
            R1009.setComponentPopupMenu(BTD);
            R1009.setName("R1009");

            //---- R1010 ----
            R1010.setComponentPopupMenu(BTD);
            R1010.setName("R1010");

            //---- R1101 ----
            R1101.setComponentPopupMenu(BTD);
            R1101.setName("R1101");

            //---- R1102 ----
            R1102.setComponentPopupMenu(BTD);
            R1102.setName("R1102");

            //---- R1103 ----
            R1103.setComponentPopupMenu(BTD);
            R1103.setName("R1103");

            //---- R1104 ----
            R1104.setComponentPopupMenu(BTD);
            R1104.setName("R1104");

            //---- R1105 ----
            R1105.setComponentPopupMenu(BTD);
            R1105.setName("R1105");

            //---- R1106 ----
            R1106.setComponentPopupMenu(BTD);
            R1106.setName("R1106");

            //---- R1107 ----
            R1107.setComponentPopupMenu(BTD);
            R1107.setName("R1107");

            //---- R1108 ----
            R1108.setComponentPopupMenu(BTD);
            R1108.setName("R1108");

            //---- R1109 ----
            R1109.setComponentPopupMenu(BTD);
            R1109.setName("R1109");

            //---- R1110 ----
            R1110.setComponentPopupMenu(BTD);
            R1110.setName("R1110");

            //---- R1201 ----
            R1201.setComponentPopupMenu(BTD);
            R1201.setName("R1201");

            //---- R1202 ----
            R1202.setComponentPopupMenu(BTD);
            R1202.setName("R1202");

            //---- R1203 ----
            R1203.setComponentPopupMenu(BTD);
            R1203.setName("R1203");

            //---- R1204 ----
            R1204.setComponentPopupMenu(BTD);
            R1204.setName("R1204");

            //---- R1205 ----
            R1205.setComponentPopupMenu(BTD);
            R1205.setName("R1205");

            //---- R1206 ----
            R1206.setComponentPopupMenu(BTD);
            R1206.setName("R1206");

            //---- R1207 ----
            R1207.setComponentPopupMenu(BTD);
            R1207.setName("R1207");

            //---- R1208 ----
            R1208.setComponentPopupMenu(BTD);
            R1208.setName("R1208");

            //---- R1209 ----
            R1209.setComponentPopupMenu(BTD);
            R1209.setName("R1209");

            //---- R1210 ----
            R1210.setComponentPopupMenu(BTD);
            R1210.setName("R1210");

            //---- R1301 ----
            R1301.setComponentPopupMenu(BTD);
            R1301.setName("R1301");

            //---- R1302 ----
            R1302.setComponentPopupMenu(BTD);
            R1302.setName("R1302");

            //---- R1303 ----
            R1303.setComponentPopupMenu(BTD);
            R1303.setName("R1303");

            //---- R1304 ----
            R1304.setComponentPopupMenu(BTD);
            R1304.setName("R1304");

            //---- R1305 ----
            R1305.setComponentPopupMenu(BTD);
            R1305.setName("R1305");

            //---- R1306 ----
            R1306.setComponentPopupMenu(BTD);
            R1306.setName("R1306");

            //---- R1307 ----
            R1307.setComponentPopupMenu(BTD);
            R1307.setName("R1307");

            //---- R1308 ----
            R1308.setComponentPopupMenu(BTD);
            R1308.setName("R1308");

            //---- R1309 ----
            R1309.setComponentPopupMenu(BTD);
            R1309.setName("R1309");

            //---- R1310 ----
            R1310.setComponentPopupMenu(BTD);
            R1310.setName("R1310");

            //---- R1401 ----
            R1401.setComponentPopupMenu(BTD);
            R1401.setName("R1401");

            //---- R1402 ----
            R1402.setComponentPopupMenu(BTD);
            R1402.setName("R1402");

            //---- R1403 ----
            R1403.setComponentPopupMenu(BTD);
            R1403.setName("R1403");

            //---- R1404 ----
            R1404.setComponentPopupMenu(BTD);
            R1404.setName("R1404");

            //---- R1405 ----
            R1405.setComponentPopupMenu(BTD);
            R1405.setName("R1405");

            //---- R1406 ----
            R1406.setComponentPopupMenu(BTD);
            R1406.setName("R1406");

            //---- R1407 ----
            R1407.setComponentPopupMenu(BTD);
            R1407.setName("R1407");

            //---- R1408 ----
            R1408.setComponentPopupMenu(BTD);
            R1408.setName("R1408");

            //---- R1409 ----
            R1409.setComponentPopupMenu(BTD);
            R1409.setName("R1409");

            //---- R1410 ----
            R1410.setComponentPopupMenu(BTD);
            R1410.setName("R1410");

            //---- OBJ_31 ----
            OBJ_31.setText("01");
            OBJ_31.setName("OBJ_31");

            //---- OBJ_32 ----
            OBJ_32.setText("02");
            OBJ_32.setName("OBJ_32");

            //---- OBJ_33 ----
            OBJ_33.setText("03");
            OBJ_33.setName("OBJ_33");

            //---- OBJ_34 ----
            OBJ_34.setText("04");
            OBJ_34.setName("OBJ_34");

            //---- OBJ_35 ----
            OBJ_35.setText("05");
            OBJ_35.setName("OBJ_35");

            //---- OBJ_36 ----
            OBJ_36.setText("06");
            OBJ_36.setName("OBJ_36");

            //---- OBJ_37 ----
            OBJ_37.setText("07");
            OBJ_37.setName("OBJ_37");

            //---- OBJ_38 ----
            OBJ_38.setText("08");
            OBJ_38.setName("OBJ_38");

            //---- OBJ_39 ----
            OBJ_39.setText("09");
            OBJ_39.setName("OBJ_39");

            //---- OBJ_40 ----
            OBJ_40.setText("10");
            OBJ_40.setName("OBJ_40");

            //---- OBJ_41 ----
            OBJ_41.setText("11");
            OBJ_41.setName("OBJ_41");

            //---- OBJ_42 ----
            OBJ_42.setText("12");
            OBJ_42.setName("OBJ_42");

            //---- OBJ_43 ----
            OBJ_43.setText("13");
            OBJ_43.setName("OBJ_43");

            //---- OBJ_44 ----
            OBJ_44.setText("14");
            OBJ_44.setName("OBJ_44");

            //---- OBJ_30 ----
            OBJ_30.setText("N\u00b0");
            OBJ_30.setName("OBJ_30");

            GroupLayout xTitledPanel2ContentContainerLayout = new GroupLayout(xTitledPanel2ContentContainer);
            xTitledPanel2ContentContainer.setLayout(xTitledPanel2ContentContainerLayout);
            xTitledPanel2ContentContainerLayout.setHorizontalGroup(
              xTitledPanel2ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(33, 33, 33)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                      .addGap(22, 22, 22)
                      .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 61, GroupLayout.PREFERRED_SIZE)
                      .addGap(274, 274, 274)
                      .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 199, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                      .addGap(22, 22, 22)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(LIB01, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB04, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB06, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB05, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB03, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB02, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB09, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB11, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB08, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB10, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB12, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB13, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB14, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB07, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
                      .addGap(25, 25, 25)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(R0601, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0801, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0901, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0401, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1101, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1001, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0501, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0701, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0201, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0101, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0301, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1301, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1401, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1201, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(R0202, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0602, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0102, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0402, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0302, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0502, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0902, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1202, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0802, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1002, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1102, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1302, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1402, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0702, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(R0103, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0203, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0903, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0803, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0703, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1003, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1203, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1103, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0403, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1403, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0603, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1303, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0503, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0303, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(R0204, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0104, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0904, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0604, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1204, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1404, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0504, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1004, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0404, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0704, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1104, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1304, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0804, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0304, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(R0405, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0305, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0705, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0905, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0205, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0105, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0505, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0605, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0805, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1205, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1105, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1305, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1405, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1005, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(R0306, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0206, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0106, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1106, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1406, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1206, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0706, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0506, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0606, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0906, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1006, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1306, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0806, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0406, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(R0307, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0607, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0107, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0507, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0207, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0407, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1007, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1407, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1307, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1207, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0807, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1107, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0907, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0707, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(R0308, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0108, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0508, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0208, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0408, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0808, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0708, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1008, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1108, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1308, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0908, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1208, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1408, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0608, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(R0709, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0809, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0609, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0409, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0509, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0909, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0209, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1109, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1009, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0309, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0109, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1309, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1409, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1209, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(R0310, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0610, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0410, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0810, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0210, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0110, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0510, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0910, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0710, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1210, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1310, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1110, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1410, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R1010, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)))))
            );
            xTitledPanel2ContentContainerLayout.setVerticalGroup(
              xTitledPanel2ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_30)
                    .addComponent(OBJ_45)
                    .addComponent(OBJ_28))
                  .addGap(6, 6, 6)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_31)
                      .addGap(9, 9, 9)
                      .addComponent(OBJ_32)
                      .addGap(9, 9, 9)
                      .addComponent(OBJ_33)
                      .addGap(9, 9, 9)
                      .addComponent(OBJ_34)
                      .addGap(9, 9, 9)
                      .addComponent(OBJ_35)
                      .addGap(9, 9, 9)
                      .addComponent(OBJ_36)
                      .addGap(9, 9, 9)
                      .addComponent(OBJ_37)
                      .addGap(9, 9, 9)
                      .addComponent(OBJ_38)
                      .addGap(9, 9, 9)
                      .addComponent(OBJ_39)
                      .addGap(9, 9, 9)
                      .addComponent(OBJ_40)
                      .addGap(9, 9, 9)
                      .addComponent(OBJ_41)
                      .addGap(9, 9, 9)
                      .addComponent(OBJ_42)
                      .addGap(9, 9, 9)
                      .addComponent(OBJ_43)
                      .addGap(9, 9, 9)
                      .addComponent(OBJ_44))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(LIB01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(LIB04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(LIB06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(LIB05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(LIB03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(LIB02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(22, 22, 22)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(LIB09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(LIB11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(LIB08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(LIB10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(LIB12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(LIB13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(150, 150, 150)
                          .addComponent(LIB14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(150, 150, 150)
                      .addComponent(LIB07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(R0601, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(175, 175, 175)
                          .addComponent(R0801, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(200, 200, 200)
                          .addComponent(R0901, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(R0401, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(250, 250, 250)
                          .addComponent(R1101, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(225, 225, 225)
                          .addComponent(R1001, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(R0501, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(150, 150, 150)
                          .addComponent(R0701, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(R0201, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(R0101, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(R0301, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(22, 22, 22)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(R1301, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(R1401, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(275, 275, 275)
                      .addComponent(R1201, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(R0202, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(R0602, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(R0102, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(R0402, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(R0302, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(R0502, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(22, 22, 22)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(R0902, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(R1202, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(R0802, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(R1002, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(R1102, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(R1302, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(325, 325, 325)
                      .addComponent(R1402, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(150, 150, 150)
                      .addComponent(R0702, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(R0103, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(R0203, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(22, 22, 22)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(R0903, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(R0803, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(R0703, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(150, 150, 150)
                          .addComponent(R1003, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(200, 200, 200)
                          .addComponent(R1203, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(175, 175, 175)
                          .addComponent(R1103, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(R0403, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(250, 250, 250)
                          .addComponent(R1403, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(R0603, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(225, 225, 225)
                          .addComponent(R1303, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(R0503, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(R0303, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(R0204, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(R0104, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGap(22, 22, 22)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(R0904, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(R0604, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(200, 200, 200)
                          .addComponent(R1204, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(250, 250, 250)
                          .addComponent(R1404, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(R0504, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(150, 150, 150)
                          .addComponent(R1004, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(R0404, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(R0704, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(175, 175, 175)
                          .addComponent(R1104, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(225, 225, 225)
                          .addComponent(R1304, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(R0804, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(R0304, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(R0405, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(R0305, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(150, 150, 150)
                          .addComponent(R0705, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(200, 200, 200)
                          .addComponent(R0905, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(R0205, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(R0105, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(R0505, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(R0605, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(175, 175, 175)
                          .addComponent(R0805, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(22, 22, 22)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(R1205, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(R1105, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(R1305, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(325, 325, 325)
                      .addComponent(R1405, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(225, 225, 225)
                      .addComponent(R1005, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(R0306, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(R0206, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(R0106, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGap(22, 22, 22)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(150, 150, 150)
                          .addComponent(R1106, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(225, 225, 225)
                          .addComponent(R1406, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(175, 175, 175)
                          .addComponent(R1206, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(R0706, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(R0506, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(R0606, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(R0906, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(R1006, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(200, 200, 200)
                          .addComponent(R1306, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(R0806, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(75, 75, 75)
                      .addComponent(R0406, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(R0307, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(R0607, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(R0107, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(R0507, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(R0207, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(R0407, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(22, 22, 22)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(R1007, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(150, 150, 150)
                          .addComponent(R1407, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(R1307, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(R1207, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(R0807, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(R1107, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(R0907, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(150, 150, 150)
                      .addComponent(R0707, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(R0308, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(R0108, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(R0508, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(R0208, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(R0408, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(22, 22, 22)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(R0808, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(R0708, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(R1008, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(R1108, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(150, 150, 150)
                          .addComponent(R1308, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(R0908, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(R1208, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(175, 175, 175)
                          .addComponent(R1408, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(125, 125, 125)
                      .addComponent(R0608, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(150, 150, 150)
                          .addComponent(R0709, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(175, 175, 175)
                          .addComponent(R0809, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(R0609, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(R0409, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(R0509, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(200, 200, 200)
                          .addComponent(R0909, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(R0209, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(250, 250, 250)
                          .addComponent(R1109, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(225, 225, 225)
                          .addComponent(R1009, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(R0309, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(R0109, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGap(22, 22, 22)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(R1309, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(R1409, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(275, 275, 275)
                      .addComponent(R1209, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(R0310, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(R0610, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(R0410, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(175, 175, 175)
                          .addComponent(R0810, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(R0210, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(R0110, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(R0510, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(200, 200, 200)
                          .addComponent(R0910, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(150, 150, 150)
                          .addComponent(R0710, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(22, 22, 22)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(R1210, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(R1310, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(R1110, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(R1410, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(225, 225, 225)
                      .addComponent(R1010, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
                    .addGap(25, 25, 25)
                    .addComponent(WCOL, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(38, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_29))
                  .addComponent(WCOL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(36, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private XRiTextField WSOC;
  private JLabel OBJ_46;
  private SNBoutonRecherche riBoutonRecherche1;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JLabel OBJ_29;
  private XRiTextField WCOL;
  private JXTitledPanel xTitledPanel2;
  private XRiTextField LIB01;
  private XRiTextField LIB02;
  private XRiTextField LIB03;
  private XRiTextField LIB04;
  private XRiTextField LIB05;
  private XRiTextField LIB06;
  private XRiTextField LIB07;
  private XRiTextField LIB08;
  private XRiTextField LIB09;
  private XRiTextField LIB10;
  private XRiTextField LIB11;
  private XRiTextField LIB12;
  private XRiTextField LIB13;
  private XRiTextField LIB14;
  private JLabel OBJ_28;
  private JLabel OBJ_45;
  private XRiTextField R0101;
  private XRiTextField R0102;
  private XRiTextField R0103;
  private XRiTextField R0104;
  private XRiTextField R0105;
  private XRiTextField R0106;
  private XRiTextField R0107;
  private XRiTextField R0108;
  private XRiTextField R0109;
  private XRiTextField R0110;
  private XRiTextField R0201;
  private XRiTextField R0202;
  private XRiTextField R0203;
  private XRiTextField R0204;
  private XRiTextField R0205;
  private XRiTextField R0206;
  private XRiTextField R0207;
  private XRiTextField R0208;
  private XRiTextField R0209;
  private XRiTextField R0210;
  private XRiTextField R0301;
  private XRiTextField R0302;
  private XRiTextField R0303;
  private XRiTextField R0304;
  private XRiTextField R0305;
  private XRiTextField R0306;
  private XRiTextField R0307;
  private XRiTextField R0308;
  private XRiTextField R0309;
  private XRiTextField R0310;
  private XRiTextField R0401;
  private XRiTextField R0402;
  private XRiTextField R0403;
  private XRiTextField R0404;
  private XRiTextField R0405;
  private XRiTextField R0406;
  private XRiTextField R0407;
  private XRiTextField R0408;
  private XRiTextField R0409;
  private XRiTextField R0410;
  private XRiTextField R0501;
  private XRiTextField R0502;
  private XRiTextField R0503;
  private XRiTextField R0504;
  private XRiTextField R0505;
  private XRiTextField R0506;
  private XRiTextField R0507;
  private XRiTextField R0508;
  private XRiTextField R0509;
  private XRiTextField R0510;
  private XRiTextField R0601;
  private XRiTextField R0602;
  private XRiTextField R0603;
  private XRiTextField R0604;
  private XRiTextField R0605;
  private XRiTextField R0606;
  private XRiTextField R0607;
  private XRiTextField R0608;
  private XRiTextField R0609;
  private XRiTextField R0610;
  private XRiTextField R0701;
  private XRiTextField R0702;
  private XRiTextField R0703;
  private XRiTextField R0704;
  private XRiTextField R0705;
  private XRiTextField R0706;
  private XRiTextField R0707;
  private XRiTextField R0708;
  private XRiTextField R0709;
  private XRiTextField R0710;
  private XRiTextField R0801;
  private XRiTextField R0802;
  private XRiTextField R0803;
  private XRiTextField R0804;
  private XRiTextField R0805;
  private XRiTextField R0806;
  private XRiTextField R0807;
  private XRiTextField R0808;
  private XRiTextField R0809;
  private XRiTextField R0810;
  private XRiTextField R0901;
  private XRiTextField R0902;
  private XRiTextField R0903;
  private XRiTextField R0904;
  private XRiTextField R0905;
  private XRiTextField R0906;
  private XRiTextField R0907;
  private XRiTextField R0908;
  private XRiTextField R0909;
  private XRiTextField R0910;
  private XRiTextField R1001;
  private XRiTextField R1002;
  private XRiTextField R1003;
  private XRiTextField R1004;
  private XRiTextField R1005;
  private XRiTextField R1006;
  private XRiTextField R1007;
  private XRiTextField R1008;
  private XRiTextField R1009;
  private XRiTextField R1010;
  private XRiTextField R1101;
  private XRiTextField R1102;
  private XRiTextField R1103;
  private XRiTextField R1104;
  private XRiTextField R1105;
  private XRiTextField R1106;
  private XRiTextField R1107;
  private XRiTextField R1108;
  private XRiTextField R1109;
  private XRiTextField R1110;
  private XRiTextField R1201;
  private XRiTextField R1202;
  private XRiTextField R1203;
  private XRiTextField R1204;
  private XRiTextField R1205;
  private XRiTextField R1206;
  private XRiTextField R1207;
  private XRiTextField R1208;
  private XRiTextField R1209;
  private XRiTextField R1210;
  private XRiTextField R1301;
  private XRiTextField R1302;
  private XRiTextField R1303;
  private XRiTextField R1304;
  private XRiTextField R1305;
  private XRiTextField R1306;
  private XRiTextField R1307;
  private XRiTextField R1308;
  private XRiTextField R1309;
  private XRiTextField R1310;
  private XRiTextField R1401;
  private XRiTextField R1402;
  private XRiTextField R1403;
  private XRiTextField R1404;
  private XRiTextField R1405;
  private XRiTextField R1406;
  private XRiTextField R1407;
  private XRiTextField R1408;
  private XRiTextField R1409;
  private XRiTextField R1410;
  private JLabel OBJ_31;
  private JLabel OBJ_32;
  private JLabel OBJ_33;
  private JLabel OBJ_34;
  private JLabel OBJ_35;
  private JLabel OBJ_36;
  private JLabel OBJ_37;
  private JLabel OBJ_38;
  private JLabel OBJ_39;
  private JLabel OBJ_40;
  private JLabel OBJ_41;
  private JLabel OBJ_42;
  private JLabel OBJ_43;
  private JLabel OBJ_44;
  private JLabel OBJ_30;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
