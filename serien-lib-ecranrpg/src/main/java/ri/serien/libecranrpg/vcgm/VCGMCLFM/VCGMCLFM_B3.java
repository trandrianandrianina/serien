
package ri.serien.libecranrpg.vcgm.VCGMCLFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

public class VCGMCLFM_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] CLSENS_Value = { "C", "D" };
  private String[] CLSENS_Title = { "Crédit", "Débit" };
  
  public VCGMCLFM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    CLSENS.setValeurs(CLSENS_Value, CLSENS_Title);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // TODO setData spécifiques...
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // TODO getDATA spécifiques
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    CLSENS = new XRiComboBox();
    OBJ_43 = new JLabel();
    OBJ_41 = new JLabel();
    OBJ_26 = new JLabel();
    CLGEND = new XRiTextField();
    CLGENF = new XRiTextField();
    CLAUXD = new XRiTextField();
    CLAUXF = new XRiTextField();
    OBJ_44 = new JLabel();
    CLDEV1 = new XRiTextField();
    CLDEV2 = new XRiTextField();
    CLDEV3 = new XRiTextField();
    CLDEV4 = new XRiTextField();
    OBJ_40 = new JLabel();
    OBJ_42 = new JLabel();
    panel3 = new JPanel();
    OBJ_45 = new JLabel();
    CLMTTD = new XRiTextField();
    CLMTTF = new XRiTextField();
    OBJ_46 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(600, 340));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Comptes"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- CLSENS ----
          CLSENS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CLSENS.setName("CLSENS");
          panel2.add(CLSENS);
          CLSENS.setBounds(125, 100, 110, CLSENS.getPreferredSize().height);

          //---- OBJ_43 ----
          OBJ_43.setText("Sens de la ligne");
          OBJ_43.setName("OBJ_43");
          panel2.add(OBJ_43);
          OBJ_43.setBounds(20, 103, 103, 20);

          //---- OBJ_41 ----
          OBJ_41.setText("Auxiliaire");
          OBJ_41.setName("OBJ_41");
          panel2.add(OBJ_41);
          OBJ_41.setBounds(20, 74, 57, 20);

          //---- OBJ_26 ----
          OBJ_26.setText("G\u00e9n\u00e9ral");
          OBJ_26.setName("OBJ_26");
          panel2.add(OBJ_26);
          OBJ_26.setBounds(20, 44, 51, 20);

          //---- CLGEND ----
          CLGEND.setComponentPopupMenu(BTD);
          CLGEND.setName("CLGEND");
          panel2.add(CLGEND);
          CLGEND.setBounds(125, 40, 58, CLGEND.getPreferredSize().height);

          //---- CLGENF ----
          CLGENF.setComponentPopupMenu(BTD);
          CLGENF.setName("CLGENF");
          panel2.add(CLGENF);
          CLGENF.setBounds(215, 40, 58, CLGENF.getPreferredSize().height);

          //---- CLAUXD ----
          CLAUXD.setComponentPopupMenu(BTD);
          CLAUXD.setName("CLAUXD");
          panel2.add(CLAUXD);
          CLAUXD.setBounds(125, 70, 58, CLAUXD.getPreferredSize().height);

          //---- CLAUXF ----
          CLAUXF.setComponentPopupMenu(BTD);
          CLAUXF.setName("CLAUXF");
          panel2.add(CLAUXF);
          CLAUXF.setBounds(215, 70, 58, CLAUXF.getPreferredSize().height);

          //---- OBJ_44 ----
          OBJ_44.setText("Devise");
          OBJ_44.setName("OBJ_44");
          panel2.add(OBJ_44);
          OBJ_44.setBounds(20, 134, 46, 20);

          //---- CLDEV1 ----
          CLDEV1.setComponentPopupMenu(BTD);
          CLDEV1.setName("CLDEV1");
          panel2.add(CLDEV1);
          CLDEV1.setBounds(125, 130, 40, CLDEV1.getPreferredSize().height);

          //---- CLDEV2 ----
          CLDEV2.setComponentPopupMenu(BTD);
          CLDEV2.setName("CLDEV2");
          panel2.add(CLDEV2);
          CLDEV2.setBounds(170, 130, 40, CLDEV2.getPreferredSize().height);

          //---- CLDEV3 ----
          CLDEV3.setComponentPopupMenu(BTD);
          CLDEV3.setName("CLDEV3");
          panel2.add(CLDEV3);
          CLDEV3.setBounds(215, 130, 40, CLDEV3.getPreferredSize().height);

          //---- CLDEV4 ----
          CLDEV4.setComponentPopupMenu(BTD);
          CLDEV4.setName("CLDEV4");
          panel2.add(CLDEV4);
          CLDEV4.setBounds(260, 130, 40, CLDEV4.getPreferredSize().height);

          //---- OBJ_40 ----
          OBJ_40.setText("\u00e0");
          OBJ_40.setName("OBJ_40");
          panel2.add(OBJ_40);
          OBJ_40.setBounds(193, 44, 12, 20);

          //---- OBJ_42 ----
          OBJ_42.setText("\u00e0");
          OBJ_42.setName("OBJ_42");
          panel2.add(OBJ_42);
          OBJ_42.setBounds(193, 74, 12, 20);
        }
        p_contenu.add(panel2);
        panel2.setBounds(10, 15, 410, 185);

        //======== panel3 ========
        {
          panel3.setBorder(new TitledBorder("Montants"));
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);

          //---- OBJ_45 ----
          OBJ_45.setText("D\u00e9but");
          OBJ_45.setName("OBJ_45");
          panel3.add(OBJ_45);
          OBJ_45.setBounds(20, 49, 39, 20);

          //---- CLMTTD ----
          CLMTTD.setComponentPopupMenu(BTD);
          CLMTTD.setName("CLMTTD");
          panel3.add(CLMTTD);
          CLMTTD.setBounds(125, 45, 114, CLMTTD.getPreferredSize().height);

          //---- CLMTTF ----
          CLMTTF.setComponentPopupMenu(BTD);
          CLMTTF.setName("CLMTTF");
          panel3.add(CLMTTF);
          CLMTTF.setBounds(274, 45, 114, CLMTTF.getPreferredSize().height);

          //---- OBJ_46 ----
          OBJ_46.setText("Fin");
          OBJ_46.setName("OBJ_46");
          panel3.add(OBJ_46);
          OBJ_46.setBounds(246, 49, 21, 20);
        }
        p_contenu.add(panel3);
        panel3.setBounds(10, 205, 410, 115);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiComboBox CLSENS;
  private JLabel OBJ_43;
  private JLabel OBJ_41;
  private JLabel OBJ_26;
  private XRiTextField CLGEND;
  private XRiTextField CLGENF;
  private XRiTextField CLAUXD;
  private XRiTextField CLAUXF;
  private JLabel OBJ_44;
  private XRiTextField CLDEV1;
  private XRiTextField CLDEV2;
  private XRiTextField CLDEV3;
  private XRiTextField CLDEV4;
  private JLabel OBJ_40;
  private JLabel OBJ_42;
  private JPanel panel3;
  private JLabel OBJ_45;
  private XRiTextField CLMTTD;
  private XRiTextField CLMTTF;
  private JLabel OBJ_46;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
