/*
 * Created by JFormDesigner on Wed Feb 24 16:24:25 CET 2010
 */

package ri.serien.libecranrpg.vcgm.VCGM30FM;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.WindowConstants;

import ri.serien.libswing.composantrpg.lexical.RiGraphe;
import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * @author Stéphane Vénéri
 * 
 * 
 *         GRAPHE DE COMPARAISON DE L'EVOLUTION DE 2 ANNEES
 * 
 */
public class VCGM30FM_GRAPHEA3 extends JFrame {
  
  
  private String[] annees = null;
  private String[] donnee = null;
  private String[] donnee1 = null;
  private RiGraphe graphe = null;
  private RiGraphe graphe2 = null;
  
  /**
   * Constructeur
   * @param annee
   * @param donnee
   */
  public VCGM30FM_GRAPHEA3(String[] annees, String[] donnee, String[] donnee1) {
    super();
    initComponents();
    
    this.annees = annees;
    this.donnee = donnee;
    this.donnee1 = donnee1;
    
    initData();
    setSize(1005, 690);
    setBackground(SNCharteGraphique.COULEUR_FOND);
    setVisible(true);
  }
  
  /**
   * Initialise les données pour les graphes
   *
   */
  private void initData() {
    // Préparation des données
    
    Object[][] data = new Object[annees.length][2];
    for (int i = 0; i < annees.length; i++) {
      data[i][0] = annees[i];
      data[i][1] = Double.parseDouble(donnee[i]);
    }
    graphe = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
    graphe.setDonnee(data, "", false);
    graphe.getGraphe("Totaux", false);
    
    Object[][] data2 = new Object[annees.length][2];
    for (int i = 0; i < annees.length; i++) {
      data2[i][0] = annees[i];
      data2[i][1] = Double.parseDouble(donnee1[i]);
    }
    graphe2 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
    graphe2.setDonnee(data2, "", false);
    graphe2.getGraphe("Ratios", false);
    
  }
  
  private void paintGraphe() {
    l_graphe.setIcon(graphe.getPicture(l_graphe.getWidth(), l_graphe.getHeight()));
    l_graphe2.setIcon(graphe2.getPicture(l_graphe2.getWidth(), l_graphe2.getHeight()));
  }
  
  private void MI_CopierActionPerformed(ActionEvent e) {
    graphe.sendToClipBoard(l_graphe.getWidth(), l_graphe.getHeight());
  }
  
  private void MI_EnregistrerActionPerformed(ActionEvent e) {
    graphe.saveGraphe(null, l_graphe.getWidth(), l_graphe.getHeight());
  }
  
  private void thisComponentResized(ComponentEvent e) {
    paintGraphe();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    l_graphe = new JLabel();
    l_graphe2 = new JLabel();
    BTD = new JPopupMenu();
    MI_Copier = new JMenuItem();
    MI_Enregistrer = new JMenuItem();

    //======== this ========
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setTitle("Analyse des comptes");
    setIconImage(null);
    setName("this");
    addComponentListener(new ComponentAdapter() {
      @Override
      public void componentResized(ComponentEvent e) {
        thisComponentResized(e);
      }
    });
    Container contentPane = getContentPane();
    contentPane.setLayout(null);

    //---- l_graphe ----
    l_graphe.setComponentPopupMenu(BTD);
    l_graphe.setName("l_graphe");
    contentPane.add(l_graphe);
    l_graphe.setBounds(0, 0, 500, 665);

    //---- l_graphe2 ----
    l_graphe2.setComponentPopupMenu(BTD);
    l_graphe2.setName("l_graphe2");
    contentPane.add(l_graphe2);
    l_graphe2.setBounds(500, 0, 500, 665);

    {
      // compute preferred size
      Dimension preferredSize = new Dimension();
      for(int i = 0; i < contentPane.getComponentCount(); i++) {
        Rectangle bounds = contentPane.getComponent(i).getBounds();
        preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
        preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
      }
      Insets insets = contentPane.getInsets();
      preferredSize.width += insets.right;
      preferredSize.height += insets.bottom;
      contentPane.setMinimumSize(preferredSize);
      contentPane.setPreferredSize(preferredSize);
    }
    pack();

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- MI_Copier ----
      MI_Copier.setText("Copier");
      MI_Copier.setName("MI_Copier");
      MI_Copier.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          MI_CopierActionPerformed(e);
        }
      });
      BTD.add(MI_Copier);

      //---- MI_Enregistrer ----
      MI_Enregistrer.setText("Enregistrer sous");
      MI_Enregistrer.setName("MI_Enregistrer");
      MI_Enregistrer.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          MI_EnregistrerActionPerformed(e);
        }
      });
      BTD.add(MI_Enregistrer);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel l_graphe;
  private JLabel l_graphe2;
  private JPopupMenu BTD;
  private JMenuItem MI_Copier;
  private JMenuItem MI_Enregistrer;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
