
package ri.serien.libecranrpg.vcgm.VCGMCLFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

public class VCGMCLFM_B4 extends SNPanelEcranRPG implements ioFrame {
  
   
  // TODO declarations classe spécifiques...
  
  public VCGMCLFM_B4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // TODO constructeur spécifiques...
    
    // Ajout
    initDiverses();
    CLLIGA.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // TODO setData spécifiques...
    // CLLIGA.setSelected(lexique.HostFieldGetData("CLLIGA").equals("OUI"));
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // TODO getDATA spécifiques
    // if (CLLIGA.isSelected())
    // lexique.HostFieldPutData("CLLIGA", 0, "OUI");
    // else
    // lexique.HostFieldPutData("CLLIGA", 0, "NON");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    CLLIGA = new XRiCheckBox();
    OBJ_42 = new JLabel();
    CLAXE1 = new XRiTextField();
    CLAXE2 = new XRiTextField();
    CLAXE3 = new XRiTextField();
    CLAXE4 = new XRiTextField();
    CLAXE5 = new XRiTextField();
    CLAXE6 = new XRiTextField();
    OBJ_44 = new JLabel();
    CLSAN1 = new XRiTextField();
    CLSAN2 = new XRiTextField();
    CLSAN3 = new XRiTextField();
    CLSAN4 = new XRiTextField();
    CLACT1 = new XRiTextField();
    CLACT2 = new XRiTextField();
    CLACT3 = new XRiTextField();
    CLACT4 = new XRiTextField();
    OBJ_43 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(515, 250));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Analytique"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- CLLIGA ----
          CLLIGA.setText("Edition des lignes analytique");
          CLLIGA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CLLIGA.setName("CLLIGA");
          panel2.add(CLLIGA);
          CLLIGA.setBounds(30, 40, 245, 20);

          //---- OBJ_42 ----
          OBJ_42.setText("Sections");
          OBJ_42.setName("OBJ_42");
          panel2.add(OBJ_42);
          OBJ_42.setBounds(30, 79, 55, 20);

          //---- CLAXE1 ----
          CLAXE1.setComponentPopupMenu(BTD);
          CLAXE1.setName("CLAXE1");
          panel2.add(CLAXE1);
          CLAXE1.setBounds(95, 105, 60, CLAXE1.getPreferredSize().height);

          //---- CLAXE2 ----
          CLAXE2.setComponentPopupMenu(BTD);
          CLAXE2.setName("CLAXE2");
          panel2.add(CLAXE2);
          CLAXE2.setBounds(155, 105, 60, CLAXE2.getPreferredSize().height);

          //---- CLAXE3 ----
          CLAXE3.setComponentPopupMenu(BTD);
          CLAXE3.setName("CLAXE3");
          panel2.add(CLAXE3);
          CLAXE3.setBounds(215, 105, 60, CLAXE3.getPreferredSize().height);

          //---- CLAXE4 ----
          CLAXE4.setComponentPopupMenu(BTD);
          CLAXE4.setName("CLAXE4");
          panel2.add(CLAXE4);
          CLAXE4.setBounds(95, 135, 60, CLAXE4.getPreferredSize().height);

          //---- CLAXE5 ----
          CLAXE5.setComponentPopupMenu(BTD);
          CLAXE5.setName("CLAXE5");
          panel2.add(CLAXE5);
          CLAXE5.setBounds(155, 135, 60, CLAXE5.getPreferredSize().height);

          //---- CLAXE6 ----
          CLAXE6.setComponentPopupMenu(BTD);
          CLAXE6.setName("CLAXE6");
          panel2.add(CLAXE6);
          CLAXE6.setBounds(215, 135, 60, CLAXE6.getPreferredSize().height);

          //---- OBJ_44 ----
          OBJ_44.setText("Affaires");
          OBJ_44.setName("OBJ_44");
          panel2.add(OBJ_44);
          OBJ_44.setBounds(30, 169, 48, 20);

          //---- CLSAN1 ----
          CLSAN1.setComponentPopupMenu(BTD);
          CLSAN1.setName("CLSAN1");
          panel2.add(CLSAN1);
          CLSAN1.setBounds(95, 75, 50, CLSAN1.getPreferredSize().height);

          //---- CLSAN2 ----
          CLSAN2.setComponentPopupMenu(BTD);
          CLSAN2.setName("CLSAN2");
          panel2.add(CLSAN2);
          CLSAN2.setBounds(146, 75, 50, CLSAN2.getPreferredSize().height);

          //---- CLSAN3 ----
          CLSAN3.setComponentPopupMenu(BTD);
          CLSAN3.setName("CLSAN3");
          panel2.add(CLSAN3);
          CLSAN3.setBounds(197, 75, 50, CLSAN3.getPreferredSize().height);

          //---- CLSAN4 ----
          CLSAN4.setComponentPopupMenu(BTD);
          CLSAN4.setName("CLSAN4");
          panel2.add(CLSAN4);
          CLSAN4.setBounds(248, 75, 50, CLSAN4.getPreferredSize().height);

          //---- CLACT1 ----
          CLACT1.setComponentPopupMenu(BTD);
          CLACT1.setName("CLACT1");
          panel2.add(CLACT1);
          CLACT1.setBounds(95, 165, 50, CLACT1.getPreferredSize().height);

          //---- CLACT2 ----
          CLACT2.setComponentPopupMenu(BTD);
          CLACT2.setName("CLACT2");
          panel2.add(CLACT2);
          CLACT2.setBounds(145, 165, 50, CLACT2.getPreferredSize().height);

          //---- CLACT3 ----
          CLACT3.setComponentPopupMenu(BTD);
          CLACT3.setName("CLACT3");
          panel2.add(CLACT3);
          CLACT3.setBounds(195, 165, 50, CLACT3.getPreferredSize().height);

          //---- CLACT4 ----
          CLACT4.setComponentPopupMenu(BTD);
          CLACT4.setName("CLACT4");
          panel2.add(CLACT4);
          CLACT4.setBounds(245, 165, 50, CLACT4.getPreferredSize().height);

          //---- OBJ_43 ----
          OBJ_43.setText("Axes");
          OBJ_43.setName("OBJ_43");
          panel2.add(OBJ_43);
          OBJ_43.setBounds(30, 109, 33, 20);
        }
        p_contenu.add(panel2);
        panel2.setBounds(10, 10, 325, 225);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiCheckBox CLLIGA;
  private JLabel OBJ_42;
  private XRiTextField CLAXE1;
  private XRiTextField CLAXE2;
  private XRiTextField CLAXE3;
  private XRiTextField CLAXE4;
  private XRiTextField CLAXE5;
  private XRiTextField CLAXE6;
  private JLabel OBJ_44;
  private XRiTextField CLSAN1;
  private XRiTextField CLSAN2;
  private XRiTextField CLSAN3;
  private XRiTextField CLSAN4;
  private XRiTextField CLACT1;
  private XRiTextField CLACT2;
  private XRiTextField CLACT3;
  private XRiTextField CLACT4;
  private JLabel OBJ_43;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
