
package ri.serien.libecranrpg.vcgm.VCGM94FM;
// Nom Fichier: i_VCGM94FM_FMTA2_347.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM94FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] V06F_Value = { "-10", "-05", "-03", "-02", "-01", "", "+01", "+02", "+03", "+05", "+10", };
  private String[] V06F_Title = { "Décalage de 10 lignes en moins", "Décalage de 5 lignes en moins", "Décalage de 3 lignes en moins",
      "Décalage de 2 lignes en moins", "Décalage de 1 ligne en moins", "", "Décalage de 1 ligne en plus", "Décalage de 2 lignes en plus",
      "Décalage de 3 lignes en plus", "Décalage de 5 lignes en plus", "Décalage de 10 lignes en plus", };
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "HLD01", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 600, };
  // private String[][] _LIST_Title_Data_Brut=null;
  
  public VCGM94FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // TODO Icones
    BT_PGFIN.setIcon(lexique.chargerImage("images/pfin20.png", true));
    BT_PGDEB.setIcon(lexique.chargerImage("images/pdeb20.png", true));
    
    // Ajout
    initDiverses();
    V06F.setValeurs(V06F_Value, V06F_Title);
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_55.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@*DATE@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    


    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  @TITRE@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void bt_DEBActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, "D");
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void bt_FINActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, "F");
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void V06FItemStateChanged(ItemEvent e) {
    if (!V06F.getSelectedItem().equals("")) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_43 = new JLabel();
    DEMETB = new XRiTextField();
    OBJ_45 = new JLabel();
    DEMDAT = new XRiTextField();
    OBJ_47 = new JLabel();
    OBJ_49 = new JLabel();
    DEMAC1 = new XRiTextField();
    DEMAC2 = new XRiTextField();
    p_tete_droite = new JPanel();
    OBJ_55 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGDOWN = new JButton();
    BT_PGUP = new JButton();
    V06F = new XRiComboBox();
    BT_PGDEB = new JButton();
    BT_PGFIN = new JButton();
    BTD = new JPopupMenu();
    OBJ_16 = new JMenuItem();
    OBJ_15 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Affichage de l'historique des comptes par affaires");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_43 ----
          OBJ_43.setText("Soci\u00e9t\u00e9");
          OBJ_43.setName("OBJ_43");

          //---- DEMETB ----
          DEMETB.setName("DEMETB");

          //---- OBJ_45 ----
          OBJ_45.setText("Date");
          OBJ_45.setName("OBJ_45");

          //---- DEMDAT ----
          DEMDAT.setComponentPopupMenu(BTD);
          DEMDAT.setName("DEMDAT");

          //---- OBJ_47 ----
          OBJ_47.setText("Affaire 1");
          OBJ_47.setName("OBJ_47");

          //---- OBJ_49 ----
          OBJ_49.setText("Affaire 2");
          OBJ_49.setName("OBJ_49");

          //---- DEMAC1 ----
          DEMAC1.setComponentPopupMenu(BTD);
          DEMAC1.setName("DEMAC1");

          //---- DEMAC2 ----
          DEMAC2.setComponentPopupMenu(BTD);
          DEMAC2.setName("DEMAC2");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(58, 58, 58)
                .addComponent(DEMETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addComponent(DEMDAT, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(DEMAC1, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(DEMAC2, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(DEMETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(DEMDAT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(DEMAC1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(DEMAC2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_43)
                  .addComponent(OBJ_45)
                  .addComponent(OBJ_47)
                  .addComponent(OBJ_49)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);

        //---- OBJ_55 ----
        OBJ_55.setText("@*DATE@");
        OBJ_55.setName("OBJ_55");
        barre_tete.add(OBJ_55);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(860, 420));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST.setViewportView(WTP01);
            }
            panel1.add(SCROLLPANE_LIST);
            SCROLLPANE_LIST.setBounds(45, 45, 710, 270);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(760, 180, 25, 95);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(760, 85, 25, 95);

            //---- V06F ----
            V06F.setToolTipText("S\u00e9lectionez le nombre de lignes \u00e0 d\u00e9caler, et validez.");
            V06F.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            V06F.setName("V06F");
            V06F.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                V06FItemStateChanged(e);
              }
            });
            panel1.add(V06F);
            V06F.setBounds(530, 325, 225, V06F.getPreferredSize().height);

            //---- BT_PGDEB ----
            BT_PGDEB.setToolTipText("Affichage au d\u00e9but de la liste");
            BT_PGDEB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDEB.setName("BT_PGDEB");
            BT_PGDEB.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bt_DEBActionPerformed(e);
              }
            });
            panel1.add(BT_PGDEB);
            BT_PGDEB.setBounds(760, 45, 25, 40);

            //---- BT_PGFIN ----
            BT_PGFIN.setToolTipText("Affichage \u00e0 la fin de la liste");
            BT_PGFIN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGFIN.setName("BT_PGFIN");
            BT_PGFIN.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bt_FINActionPerformed(e);
              }
            });
            panel1.add(BT_PGFIN);
            BT_PGFIN.setBounds(760, 275, 25, 40);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 834, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 392, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_16 ----
      OBJ_16.setText("Choix possibles");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);

      //---- OBJ_15 ----
      OBJ_15.setText("Aide en ligne");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_43;
  private XRiTextField DEMETB;
  private JLabel OBJ_45;
  private XRiTextField DEMDAT;
  private JLabel OBJ_47;
  private JLabel OBJ_49;
  private XRiTextField DEMAC1;
  private XRiTextField DEMAC2;
  private JPanel p_tete_droite;
  private JLabel OBJ_55;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JButton BT_PGDOWN;
  private JButton BT_PGUP;
  private XRiComboBox V06F;
  private JButton BT_PGDEB;
  private JButton BT_PGFIN;
  private JPopupMenu BTD;
  private JMenuItem OBJ_16;
  private JMenuItem OBJ_15;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
