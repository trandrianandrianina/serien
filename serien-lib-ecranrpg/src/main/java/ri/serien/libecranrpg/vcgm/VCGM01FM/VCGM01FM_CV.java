
package ri.serien.libecranrpg.vcgm.VCGM01FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM01FM_CV extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM01FM_CV(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_50_OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POC01@")).trim());
    OBJ_54_OBJ_54.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POC02@")).trim());
    OBJ_58_OBJ_58.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POC03@")).trim());
    OBJ_62_OBJ_62.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POC04@")).trim());
    OBJ_66_OBJ_66.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POC05@")).trim());
    OBJ_70_OBJ_70.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POC06@")).trim());
    OBJ_74_OBJ_74.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POC07@")).trim());
    OBJ_78_OBJ_78.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POC08@")).trim());
    OBJ_82_OBJ_82.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POC09@")).trim());
    OBJ_86_OBJ_86.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POC10@")).trim());
    OBJ_90_OBJ_90.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POC11@")).trim());
    OBJ_94_OBJ_94.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POC12@")).trim());
    OBJ_98_OBJ_98.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POC13@")).trim());
    OBJ_102_OBJ_102.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POC14@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    OBJ_102_OBJ_102.setVisible(lexique.isPresent("POC14"));
    OBJ_98_OBJ_98.setVisible(lexique.isPresent("POC13"));
    OBJ_94_OBJ_94.setVisible(lexique.isPresent("POC12"));
    OBJ_90_OBJ_90.setVisible(lexique.isPresent("POC11"));
    OBJ_86_OBJ_86.setVisible(lexique.isPresent("POC10"));
    OBJ_82_OBJ_82.setVisible(lexique.isPresent("POC09"));
    OBJ_78_OBJ_78.setVisible(lexique.isPresent("POC08"));
    OBJ_74_OBJ_74.setVisible(lexique.isPresent("POC07"));
    OBJ_70_OBJ_70.setVisible(lexique.isPresent("POC06"));
    OBJ_66_OBJ_66.setVisible(lexique.isPresent("POC05"));
    OBJ_62_OBJ_62.setVisible(lexique.isPresent("POC04"));
    OBJ_58_OBJ_58.setVisible(lexique.isPresent("POC03"));
    OBJ_54_OBJ_54.setVisible(lexique.isPresent("POC02"));
    OBJ_50_OBJ_50.setVisible(lexique.isPresent("POC01"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - Personnalisation de @LOCGRP/-1/@"));
    
    

    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    // riMenu_bt2.setIcon(lexique.getImage("images/options.png",true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44_OBJ_45 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_45_OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46_OBJ_47 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    CVLIB = new XRiTextField();
    OBJ_43_OBJ_43 = new JLabel();
    OBJ_46_OBJ_46 = new JLabel();
    CVS01 = new XRiTextField();
    CVS02 = new XRiTextField();
    CVS03 = new XRiTextField();
    CVS04 = new XRiTextField();
    CVS05 = new XRiTextField();
    CVS06 = new XRiTextField();
    CVS07 = new XRiTextField();
    CVS08 = new XRiTextField();
    CVS09 = new XRiTextField();
    CVS10 = new XRiTextField();
    CVS11 = new XRiTextField();
    CVS12 = new XRiTextField();
    CVS13 = new XRiTextField();
    CVS14 = new XRiTextField();
    OBJ_45_OBJ_45 = new JLabel();
    OBJ_50_OBJ_50 = new RiZoneSortie();
    OBJ_54_OBJ_54 = new RiZoneSortie();
    OBJ_58_OBJ_58 = new RiZoneSortie();
    OBJ_62_OBJ_62 = new RiZoneSortie();
    OBJ_66_OBJ_66 = new RiZoneSortie();
    OBJ_70_OBJ_70 = new RiZoneSortie();
    OBJ_74_OBJ_74 = new RiZoneSortie();
    OBJ_78_OBJ_78 = new RiZoneSortie();
    OBJ_82_OBJ_82 = new RiZoneSortie();
    OBJ_86_OBJ_86 = new RiZoneSortie();
    OBJ_90_OBJ_90 = new RiZoneSortie();
    OBJ_94_OBJ_94 = new RiZoneSortie();
    OBJ_98_OBJ_98 = new RiZoneSortie();
    OBJ_102_OBJ_102 = new RiZoneSortie();
    OBJ_41_OBJ_41 = new JLabel();
    OBJ_44_OBJ_44 = new JLabel();
    CVP01 = new XRiTextField();
    CVP02 = new XRiTextField();
    CVP03 = new XRiTextField();
    CVP04 = new XRiTextField();
    CVP05 = new XRiTextField();
    CVP06 = new XRiTextField();
    CVP07 = new XRiTextField();
    CVP08 = new XRiTextField();
    CVP09 = new XRiTextField();
    CVP10 = new XRiTextField();
    CVP11 = new XRiTextField();
    CVP12 = new XRiTextField();
    CVP13 = new XRiTextField();
    CVP14 = new XRiTextField();
    OBJ_47_OBJ_47 = new JLabel();
    OBJ_51_OBJ_51 = new JLabel();
    OBJ_55_OBJ_55 = new JLabel();
    OBJ_59_OBJ_59 = new JLabel();
    OBJ_63_OBJ_63 = new JLabel();
    OBJ_67_OBJ_67 = new JLabel();
    OBJ_71_OBJ_71 = new JLabel();
    OBJ_75_OBJ_75 = new JLabel();
    OBJ_79_OBJ_79 = new JLabel();
    OBJ_83_OBJ_83 = new JLabel();
    OBJ_87_OBJ_87 = new JLabel();
    OBJ_91_OBJ_91 = new JLabel();
    OBJ_95_OBJ_95 = new JLabel();
    OBJ_99_OBJ_99 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44_OBJ_45 ----
          OBJ_44_OBJ_45.setText("Soci\u00e9t\u00e9");
          OBJ_44_OBJ_45.setName("OBJ_44_OBJ_45");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setName("INDETB");

          //---- OBJ_45_OBJ_46 ----
          OBJ_45_OBJ_46.setText("Code");
          OBJ_45_OBJ_46.setName("OBJ_45_OBJ_46");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setName("INDTYP");

          //---- OBJ_46_OBJ_47 ----
          OBJ_46_OBJ_47.setText("Ordre");
          OBJ_46_OBJ_47.setName("OBJ_46_OBJ_47");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(680, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Cl\u00e9s de ventilation analytique");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

            //---- CVLIB ----
            CVLIB.setName("CVLIB");

            //---- OBJ_43_OBJ_43 ----
            OBJ_43_OBJ_43.setText("Code");
            OBJ_43_OBJ_43.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_43_OBJ_43.setFont(OBJ_43_OBJ_43.getFont().deriveFont(OBJ_43_OBJ_43.getFont().getStyle() | Font.BOLD));
            OBJ_43_OBJ_43.setName("OBJ_43_OBJ_43");

            //---- OBJ_46_OBJ_46 ----
            OBJ_46_OBJ_46.setText("Section");
            OBJ_46_OBJ_46.setFont(OBJ_46_OBJ_46.getFont().deriveFont(OBJ_46_OBJ_46.getFont().getStyle() | Font.BOLD));
            OBJ_46_OBJ_46.setName("OBJ_46_OBJ_46");

            //---- CVS01 ----
            CVS01.setName("CVS01");

            //---- CVS02 ----
            CVS02.setName("CVS02");

            //---- CVS03 ----
            CVS03.setName("CVS03");

            //---- CVS04 ----
            CVS04.setName("CVS04");

            //---- CVS05 ----
            CVS05.setName("CVS05");

            //---- CVS06 ----
            CVS06.setName("CVS06");

            //---- CVS07 ----
            CVS07.setName("CVS07");

            //---- CVS08 ----
            CVS08.setName("CVS08");

            //---- CVS09 ----
            CVS09.setName("CVS09");

            //---- CVS10 ----
            CVS10.setName("CVS10");

            //---- CVS11 ----
            CVS11.setName("CVS11");

            //---- CVS12 ----
            CVS12.setName("CVS12");

            //---- CVS13 ----
            CVS13.setName("CVS13");

            //---- CVS14 ----
            CVS14.setName("CVS14");

            //---- OBJ_45_OBJ_45 ----
            OBJ_45_OBJ_45.setText("%");
            OBJ_45_OBJ_45.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_45_OBJ_45.setFont(OBJ_45_OBJ_45.getFont().deriveFont(OBJ_45_OBJ_45.getFont().getStyle() | Font.BOLD));
            OBJ_45_OBJ_45.setName("OBJ_45_OBJ_45");

            //---- OBJ_50_OBJ_50 ----
            OBJ_50_OBJ_50.setText("@POC01@");
            OBJ_50_OBJ_50.setName("OBJ_50_OBJ_50");

            //---- OBJ_54_OBJ_54 ----
            OBJ_54_OBJ_54.setText("@POC02@");
            OBJ_54_OBJ_54.setName("OBJ_54_OBJ_54");

            //---- OBJ_58_OBJ_58 ----
            OBJ_58_OBJ_58.setText("@POC03@");
            OBJ_58_OBJ_58.setName("OBJ_58_OBJ_58");

            //---- OBJ_62_OBJ_62 ----
            OBJ_62_OBJ_62.setText("@POC04@");
            OBJ_62_OBJ_62.setName("OBJ_62_OBJ_62");

            //---- OBJ_66_OBJ_66 ----
            OBJ_66_OBJ_66.setText("@POC05@");
            OBJ_66_OBJ_66.setName("OBJ_66_OBJ_66");

            //---- OBJ_70_OBJ_70 ----
            OBJ_70_OBJ_70.setText("@POC06@");
            OBJ_70_OBJ_70.setName("OBJ_70_OBJ_70");

            //---- OBJ_74_OBJ_74 ----
            OBJ_74_OBJ_74.setText("@POC07@");
            OBJ_74_OBJ_74.setName("OBJ_74_OBJ_74");

            //---- OBJ_78_OBJ_78 ----
            OBJ_78_OBJ_78.setText("@POC08@");
            OBJ_78_OBJ_78.setName("OBJ_78_OBJ_78");

            //---- OBJ_82_OBJ_82 ----
            OBJ_82_OBJ_82.setText("@POC09@");
            OBJ_82_OBJ_82.setName("OBJ_82_OBJ_82");

            //---- OBJ_86_OBJ_86 ----
            OBJ_86_OBJ_86.setText("@POC10@");
            OBJ_86_OBJ_86.setName("OBJ_86_OBJ_86");

            //---- OBJ_90_OBJ_90 ----
            OBJ_90_OBJ_90.setText("@POC11@");
            OBJ_90_OBJ_90.setName("OBJ_90_OBJ_90");

            //---- OBJ_94_OBJ_94 ----
            OBJ_94_OBJ_94.setText("@POC12@");
            OBJ_94_OBJ_94.setName("OBJ_94_OBJ_94");

            //---- OBJ_98_OBJ_98 ----
            OBJ_98_OBJ_98.setText("@POC13@");
            OBJ_98_OBJ_98.setName("OBJ_98_OBJ_98");

            //---- OBJ_102_OBJ_102 ----
            OBJ_102_OBJ_102.setText("@POC14@");
            OBJ_102_OBJ_102.setName("OBJ_102_OBJ_102");

            //---- OBJ_41_OBJ_41 ----
            OBJ_41_OBJ_41.setText("Libell\u00e9");
            OBJ_41_OBJ_41.setName("OBJ_41_OBJ_41");

            //---- OBJ_44_OBJ_44 ----
            OBJ_44_OBJ_44.setText("Poids");
            OBJ_44_OBJ_44.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_44_OBJ_44.setFont(OBJ_44_OBJ_44.getFont().deriveFont(OBJ_44_OBJ_44.getFont().getStyle() | Font.BOLD));
            OBJ_44_OBJ_44.setName("OBJ_44_OBJ_44");

            //---- CVP01 ----
            CVP01.setName("CVP01");

            //---- CVP02 ----
            CVP02.setName("CVP02");

            //---- CVP03 ----
            CVP03.setName("CVP03");

            //---- CVP04 ----
            CVP04.setName("CVP04");

            //---- CVP05 ----
            CVP05.setName("CVP05");

            //---- CVP06 ----
            CVP06.setName("CVP06");

            //---- CVP07 ----
            CVP07.setName("CVP07");

            //---- CVP08 ----
            CVP08.setName("CVP08");

            //---- CVP09 ----
            CVP09.setName("CVP09");

            //---- CVP10 ----
            CVP10.setName("CVP10");

            //---- CVP11 ----
            CVP11.setName("CVP11");

            //---- CVP12 ----
            CVP12.setName("CVP12");

            //---- CVP13 ----
            CVP13.setName("CVP13");

            //---- CVP14 ----
            CVP14.setName("CVP14");

            //---- OBJ_47_OBJ_47 ----
            OBJ_47_OBJ_47.setText("01");
            OBJ_47_OBJ_47.setName("OBJ_47_OBJ_47");

            //---- OBJ_51_OBJ_51 ----
            OBJ_51_OBJ_51.setText("02");
            OBJ_51_OBJ_51.setName("OBJ_51_OBJ_51");

            //---- OBJ_55_OBJ_55 ----
            OBJ_55_OBJ_55.setText("03");
            OBJ_55_OBJ_55.setName("OBJ_55_OBJ_55");

            //---- OBJ_59_OBJ_59 ----
            OBJ_59_OBJ_59.setText("04");
            OBJ_59_OBJ_59.setName("OBJ_59_OBJ_59");

            //---- OBJ_63_OBJ_63 ----
            OBJ_63_OBJ_63.setText("05");
            OBJ_63_OBJ_63.setName("OBJ_63_OBJ_63");

            //---- OBJ_67_OBJ_67 ----
            OBJ_67_OBJ_67.setText("06");
            OBJ_67_OBJ_67.setName("OBJ_67_OBJ_67");

            //---- OBJ_71_OBJ_71 ----
            OBJ_71_OBJ_71.setText("07");
            OBJ_71_OBJ_71.setName("OBJ_71_OBJ_71");

            //---- OBJ_75_OBJ_75 ----
            OBJ_75_OBJ_75.setText("08");
            OBJ_75_OBJ_75.setName("OBJ_75_OBJ_75");

            //---- OBJ_79_OBJ_79 ----
            OBJ_79_OBJ_79.setText("09");
            OBJ_79_OBJ_79.setName("OBJ_79_OBJ_79");

            //---- OBJ_83_OBJ_83 ----
            OBJ_83_OBJ_83.setText("10");
            OBJ_83_OBJ_83.setName("OBJ_83_OBJ_83");

            //---- OBJ_87_OBJ_87 ----
            OBJ_87_OBJ_87.setText("11");
            OBJ_87_OBJ_87.setName("OBJ_87_OBJ_87");

            //---- OBJ_91_OBJ_91 ----
            OBJ_91_OBJ_91.setText("12");
            OBJ_91_OBJ_91.setName("OBJ_91_OBJ_91");

            //---- OBJ_95_OBJ_95 ----
            OBJ_95_OBJ_95.setText("13");
            OBJ_95_OBJ_95.setName("OBJ_95_OBJ_95");

            //---- OBJ_99_OBJ_99 ----
            OBJ_99_OBJ_99.setText("14");
            OBJ_99_OBJ_99.setName("OBJ_99_OBJ_99");

            GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
            xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
            xTitledPanel1ContentContainerLayout.setHorizontalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(115, 115, 115)
                  .addComponent(OBJ_41_OBJ_41, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
                  .addGap(42, 42, 42)
                  .addComponent(CVLIB, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(235, 235, 235)
                  .addComponent(OBJ_43_OBJ_43, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addGap(15, 15, 15)
                  .addComponent(OBJ_44_OBJ_44, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_45_OBJ_45, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(115, 115, 115)
                  .addComponent(OBJ_46_OBJ_46, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                  .addGap(37, 37, 37)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_47_OBJ_47, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_51_OBJ_51, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_55_OBJ_55, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_59_OBJ_59, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_63_OBJ_63, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_67_OBJ_67, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_71_OBJ_71, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_75_OBJ_75, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_79_OBJ_79, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_83_OBJ_83, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_87_OBJ_87, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_91_OBJ_91, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_95_OBJ_95, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_99_OBJ_99, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                  .addGap(17, 17, 17)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(CVS03, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVS01, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVS02, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVS05, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVS09, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVS06, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVS08, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVS07, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVS13, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVS11, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVS14, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVS12, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVS10, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVS04, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                  .addGap(15, 15, 15)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(CVP04, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVP03, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVP01, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVP02, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVP05, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVP11, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVP07, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVP08, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVP13, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVP09, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVP14, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVP10, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVP12, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVP06, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                  .addGap(10, 10, 10)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_50_OBJ_50, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_54_OBJ_54, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_58_OBJ_58, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_62_OBJ_62, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_66_OBJ_66, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_70_OBJ_70, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_74_OBJ_74, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_78_OBJ_78, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_82_OBJ_82, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_86_OBJ_86, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_90_OBJ_90, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_94_OBJ_94, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_98_OBJ_98, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_102_OBJ_102, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)))
            );
            xTitledPanel1ContentContainerLayout.setVerticalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(11, 11, 11)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_41_OBJ_41, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(CVLIB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(26, 26, 26)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_43_OBJ_43, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_44_OBJ_44, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_45_OBJ_45, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_46_OBJ_46, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_47_OBJ_47, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_51_OBJ_51, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_55_OBJ_55, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_59_OBJ_59, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_63_OBJ_63, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_67_OBJ_67, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_71_OBJ_71, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_75_OBJ_75, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_79_OBJ_79, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_83_OBJ_83, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_87_OBJ_87, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_91_OBJ_91, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_95_OBJ_95, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_99_OBJ_99, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(48, 48, 48)
                          .addComponent(CVS03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(CVS01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(24, 24, 24)
                          .addComponent(CVS02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(20, 20, 20)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(CVS05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(96, 96, 96)
                          .addComponent(CVS09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(24, 24, 24)
                          .addComponent(CVS06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(72, 72, 72)
                          .addComponent(CVS08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(48, 48, 48)
                          .addComponent(CVS07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(192, 192, 192)
                          .addComponent(CVS13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(144, 144, 144)
                          .addComponent(CVS11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(216, 216, 216)
                          .addComponent(CVS14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(168, 168, 168)
                          .addComponent(CVS12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(120, 120, 120)
                          .addComponent(CVS10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(72, 72, 72)
                      .addComponent(CVS04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(72, 72, 72)
                          .addComponent(CVP04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(48, 48, 48)
                          .addComponent(CVP03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(CVP01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(24, 24, 24)
                          .addComponent(CVP02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(96, 96, 96)
                          .addComponent(CVP05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(20, 20, 20)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(96, 96, 96)
                          .addComponent(CVP11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(CVP07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(24, 24, 24)
                          .addComponent(CVP08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(144, 144, 144)
                          .addComponent(CVP13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(48, 48, 48)
                          .addComponent(CVP09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(168, 168, 168)
                          .addComponent(CVP14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(72, 72, 72)
                          .addComponent(CVP10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(120, 120, 120)
                          .addComponent(CVP12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(120, 120, 120)
                      .addComponent(CVP06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_50_OBJ_50, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(OBJ_54_OBJ_54, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(OBJ_58_OBJ_58, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(OBJ_62_OBJ_62, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(OBJ_66_OBJ_66, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(OBJ_70_OBJ_70, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(OBJ_74_OBJ_74, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(OBJ_78_OBJ_78, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(OBJ_82_OBJ_82, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(OBJ_86_OBJ_86, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(OBJ_90_OBJ_90, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(OBJ_94_OBJ_94, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(OBJ_98_OBJ_98, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(OBJ_102_OBJ_102, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44_OBJ_45;
  private XRiTextField INDETB;
  private JLabel OBJ_45_OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_46_OBJ_47;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private XRiTextField CVLIB;
  private JLabel OBJ_43_OBJ_43;
  private JLabel OBJ_46_OBJ_46;
  private XRiTextField CVS01;
  private XRiTextField CVS02;
  private XRiTextField CVS03;
  private XRiTextField CVS04;
  private XRiTextField CVS05;
  private XRiTextField CVS06;
  private XRiTextField CVS07;
  private XRiTextField CVS08;
  private XRiTextField CVS09;
  private XRiTextField CVS10;
  private XRiTextField CVS11;
  private XRiTextField CVS12;
  private XRiTextField CVS13;
  private XRiTextField CVS14;
  private JLabel OBJ_45_OBJ_45;
  private RiZoneSortie OBJ_50_OBJ_50;
  private RiZoneSortie OBJ_54_OBJ_54;
  private RiZoneSortie OBJ_58_OBJ_58;
  private RiZoneSortie OBJ_62_OBJ_62;
  private RiZoneSortie OBJ_66_OBJ_66;
  private RiZoneSortie OBJ_70_OBJ_70;
  private RiZoneSortie OBJ_74_OBJ_74;
  private RiZoneSortie OBJ_78_OBJ_78;
  private RiZoneSortie OBJ_82_OBJ_82;
  private RiZoneSortie OBJ_86_OBJ_86;
  private RiZoneSortie OBJ_90_OBJ_90;
  private RiZoneSortie OBJ_94_OBJ_94;
  private RiZoneSortie OBJ_98_OBJ_98;
  private RiZoneSortie OBJ_102_OBJ_102;
  private JLabel OBJ_41_OBJ_41;
  private JLabel OBJ_44_OBJ_44;
  private XRiTextField CVP01;
  private XRiTextField CVP02;
  private XRiTextField CVP03;
  private XRiTextField CVP04;
  private XRiTextField CVP05;
  private XRiTextField CVP06;
  private XRiTextField CVP07;
  private XRiTextField CVP08;
  private XRiTextField CVP09;
  private XRiTextField CVP10;
  private XRiTextField CVP11;
  private XRiTextField CVP12;
  private XRiTextField CVP13;
  private XRiTextField CVP14;
  private JLabel OBJ_47_OBJ_47;
  private JLabel OBJ_51_OBJ_51;
  private JLabel OBJ_55_OBJ_55;
  private JLabel OBJ_59_OBJ_59;
  private JLabel OBJ_63_OBJ_63;
  private JLabel OBJ_67_OBJ_67;
  private JLabel OBJ_71_OBJ_71;
  private JLabel OBJ_75_OBJ_75;
  private JLabel OBJ_79_OBJ_79;
  private JLabel OBJ_83_OBJ_83;
  private JLabel OBJ_87_OBJ_87;
  private JLabel OBJ_91_OBJ_91;
  private JLabel OBJ_95_OBJ_95;
  private JLabel OBJ_99_OBJ_99;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
