
package ri.serien.libecranrpg.vcgm.VCGM10FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGM10FM_A3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM10FM_A3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    TTARG4.setValeursSelection("1", " ");
    TTARG5.setValeursSelection("1", " ");
    TTARGA.setValeursSelection("1", " ");
    EXCNLT.setValeursSelection("1", " ");
    EXCLT.setValeursSelection("1", " ");
    ARG8D.setValeursSelection("X", " ");
    SCAN6.setValeurs("X", SCAN6_GRP);
    SCAN6_TOUT.setValeurs(" ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_34.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RFRE@")).trim());
    LIBSA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBSA@")).trim());
    LIBAC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAC@")).trim());
    LIBTF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBTF@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    riSousMenu6.setEnabled(!interpreteurD.analyseExpression("@HIST@").trim().equalsIgnoreCase("/ Ecritures Historisées"));
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Recherche d'écritures"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    xTitledPanel2 = new JXTitledPanel();
    ARG8D = new XRiCheckBox();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_14 = new JLabel();
    OBJ_18 = new JLabel();
    OBJ_20 = new JLabel();
    OBJ_28 = new JLabel();
    OBJ_43 = new JLabel();
    OBJ_29 = new JLabel();
    OBJ_31 = new JLabel();
    OBJ_34 = new JLabel();
    ARG9 = new XRiTextField();
    EXCLT = new XRiCheckBox();
    EXCNLT = new XRiCheckBox();
    ARG6 = new XRiTextField();
    TTARGA = new XRiCheckBox();
    ARGA = new XRiTextField();
    ARG5 = new XRiTextField();
    TTARG5 = new XRiCheckBox();
    TTARG4 = new XRiCheckBox();
    ARG4 = new XRiTextField();
    ARG3 = new XRiTextField();
    ARG2 = new XRiTextField();
    OBJ_16 = new JLabel();
    ARG2M = new XRiTextField();
    LIBSA = new RiZoneSortie();
    LIBAC = new RiZoneSortie();
    LIBTF = new RiZoneSortie();
    SCAN6 = new XRiRadioButton();
    SCAN6_TOUT = new XRiRadioButton();
    BTD = new JPopupMenu();
    OBJ_6 = new JMenuItem();
    OBJ_5 = new JMenuItem();
    SCAN6_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(790, 490));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Ecriture historis\u00e9e");
            riSousMenu_bt6.setToolTipText("Ecriture historis\u00e9e");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== xTitledPanel2 ========
        {
          xTitledPanel2.setBorder(new DropShadowBorder());
          xTitledPanel2.setTitle("Recherche sur axes");
          xTitledPanel2.setName("xTitledPanel2");
          Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();

          //---- ARG8D ----
          ARG8D.setText("Acc\u00e8s \u00e0 la recherche");
          ARG8D.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ARG8D.setName("ARG8D");

          GroupLayout xTitledPanel2ContentContainerLayout = new GroupLayout(xTitledPanel2ContentContainer);
          xTitledPanel2ContentContainer.setLayout(xTitledPanel2ContentContainerLayout);
          xTitledPanel2ContentContainerLayout.setHorizontalGroup(
            xTitledPanel2ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(ARG8D, GroupLayout.PREFERRED_SIZE, 399, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(164, Short.MAX_VALUE))
          );
          xTitledPanel2ContentContainerLayout.setVerticalGroup(
            xTitledPanel2ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(ARG8D, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        p_contenu.add(xTitledPanel2);
        xTitledPanel2.setBounds(10, 385, 603, 75);

        //======== xTitledPanel1 ========
        {
          xTitledPanel1.setTitle("Recherche multi-crit\u00e8res");
          xTitledPanel1.setBorder(new DropShadowBorder());
          xTitledPanel1.setName("xTitledPanel1");
          Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

          //---- OBJ_14 ----
          OBJ_14.setText("Montant compris entre");
          OBJ_14.setName("OBJ_14");

          //---- OBJ_18 ----
          OBJ_18.setText("Num\u00e9ro de pi\u00e8ce");
          OBJ_18.setName("OBJ_18");

          //---- OBJ_20 ----
          OBJ_20.setText("Section analytique");
          OBJ_20.setName("OBJ_20");

          //---- OBJ_28 ----
          OBJ_28.setText("Affaire");
          OBJ_28.setName("OBJ_28");

          //---- OBJ_43 ----
          OBJ_43.setText("Type de frais");
          OBJ_43.setName("OBJ_43");

          //---- OBJ_29 ----
          OBJ_29.setText("Libell\u00e9 de l'\u00e9criture");
          OBJ_29.setName("OBJ_29");

          //---- OBJ_31 ----
          OBJ_31.setText("Exclusion des \u00e9critures");
          OBJ_31.setName("OBJ_31");

          //---- OBJ_34 ----
          OBJ_34.setText("@RFRE@");
          OBJ_34.setName("OBJ_34");

          //---- ARG9 ----
          ARG9.setComponentPopupMenu(BTD);
          ARG9.setName("ARG9");

          //---- EXCLT ----
          EXCLT.setText("lettr\u00e9es");
          EXCLT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EXCLT.setName("EXCLT");

          //---- EXCNLT ----
          EXCNLT.setText("non lettr\u00e9es");
          EXCNLT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EXCNLT.setName("EXCNLT");

          //---- ARG6 ----
          ARG6.setComponentPopupMenu(BTD);
          ARG6.setName("ARG6");

          //---- TTARGA ----
          TTARGA.setText("Tri");
          TTARGA.setToolTipText("Tri");
          TTARGA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TTARGA.setName("TTARGA");

          //---- ARGA ----
          ARGA.setComponentPopupMenu(BTD);
          ARGA.setName("ARGA");

          //---- ARG5 ----
          ARG5.setComponentPopupMenu(BTD);
          ARG5.setName("ARG5");

          //---- TTARG5 ----
          TTARG5.setText("Totalisation");
          TTARG5.setToolTipText("Totalisation par affaire");
          TTARG5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TTARG5.setName("TTARG5");

          //---- TTARG4 ----
          TTARG4.setText("Totalisation");
          TTARG4.setToolTipText("Totalisation par section");
          TTARG4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TTARG4.setName("TTARG4");

          //---- ARG4 ----
          ARG4.setComponentPopupMenu(BTD);
          ARG4.setName("ARG4");

          //---- ARG3 ----
          ARG3.setComponentPopupMenu(BTD);
          ARG3.setName("ARG3");

          //---- ARG2 ----
          ARG2.setComponentPopupMenu(BTD);
          ARG2.setName("ARG2");

          //---- OBJ_16 ----
          OBJ_16.setText("et");
          OBJ_16.setName("OBJ_16");

          //---- ARG2M ----
          ARG2M.setComponentPopupMenu(BTD);
          ARG2M.setName("ARG2M");

          //---- LIBSA ----
          LIBSA.setText("@LIBSA@");
          LIBSA.setForeground(Color.black);
          LIBSA.setName("LIBSA");

          //---- LIBAC ----
          LIBAC.setText("@LIBAC@");
          LIBAC.setForeground(Color.black);
          LIBAC.setName("LIBAC");

          //---- LIBTF ----
          LIBTF.setText("@LIBTF@");
          LIBTF.setForeground(Color.black);
          LIBTF.setName("LIBTF");

          //---- SCAN6 ----
          SCAN6.setText("Sur les premiers caract\u00e8res");
          SCAN6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCAN6.setName("SCAN6");

          //---- SCAN6_TOUT ----
          SCAN6_TOUT.setText("Sur tout le libell\u00e9");
          SCAN6_TOUT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCAN6_TOUT.setName("SCAN6_TOUT");

          GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
          xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
          xTitledPanel1ContentContainerLayout.setHorizontalGroup(
            xTitledPanel1ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(OBJ_14, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)
                .addComponent(ARG2, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addComponent(OBJ_16, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(ARG2M, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE))
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(OBJ_18, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)
                .addComponent(ARG3, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE))
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)
                .addComponent(ARG4, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(LIBSA, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(TTARG4, GroupLayout.PREFERRED_SIZE, 96, GroupLayout.PREFERRED_SIZE))
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)
                .addComponent(ARG5, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(LIBAC, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(TTARG5, GroupLayout.PREFERRED_SIZE, 96, GroupLayout.PREFERRED_SIZE))
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)
                .addComponent(ARGA, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(LIBTF, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(TTARGA, GroupLayout.PREFERRED_SIZE, 96, GroupLayout.PREFERRED_SIZE))
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)
                .addComponent(ARG6, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(SCAN6, GroupLayout.PREFERRED_SIZE, 220, GroupLayout.PREFERRED_SIZE))
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(365, 365, 365)
                .addComponent(SCAN6_TOUT, GroupLayout.PREFERRED_SIZE, 189, GroupLayout.PREFERRED_SIZE))
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)
                .addComponent(EXCLT, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(EXCNLT, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE))
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)
                .addComponent(ARG9, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
          );
          xTitledPanel1ContentContainerLayout.setVerticalGroup(
            xTitledPanel1ContentContainerLayout.createParallelGroup()
              .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_14))
                  .addComponent(ARG2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_16))
                  .addComponent(ARG2M, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_18))
                  .addComponent(ARG3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_20))
                  .addComponent(ARG4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(LIBSA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(TTARG4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(2, 2, 2)
                .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_28))
                  .addComponent(ARG5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(LIBAC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(TTARG5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(2, 2, 2)
                .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_43))
                  .addComponent(ARGA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(LIBTF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(TTARGA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(22, 22, 22)
                .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                  .addComponent(OBJ_29)
                  .addComponent(ARG6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(SCAN6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addComponent(SCAN6_TOUT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                  .addComponent(OBJ_31)
                  .addComponent(EXCLT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(EXCNLT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17)
                .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_34))
                  .addComponent(ARG9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_contenu.add(xTitledPanel1);
        xTitledPanel1.setBounds(10, 10, 603, 370);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_6 ----
      OBJ_6.setText("Choix possibles");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      BTD.add(OBJ_6);

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTD.add(OBJ_5);
    }

    //---- SCAN6_GRP ----
    SCAN6_GRP.add(SCAN6);
    SCAN6_GRP.add(SCAN6_TOUT);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel2;
  private XRiCheckBox ARG8D;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_14;
  private JLabel OBJ_18;
  private JLabel OBJ_20;
  private JLabel OBJ_28;
  private JLabel OBJ_43;
  private JLabel OBJ_29;
  private JLabel OBJ_31;
  private JLabel OBJ_34;
  private XRiTextField ARG9;
  private XRiCheckBox EXCLT;
  private XRiCheckBox EXCNLT;
  private XRiTextField ARG6;
  private XRiCheckBox TTARGA;
  private XRiTextField ARGA;
  private XRiTextField ARG5;
  private XRiCheckBox TTARG5;
  private XRiCheckBox TTARG4;
  private XRiTextField ARG4;
  private XRiTextField ARG3;
  private XRiTextField ARG2;
  private JLabel OBJ_16;
  private XRiTextField ARG2M;
  private RiZoneSortie LIBSA;
  private RiZoneSortie LIBAC;
  private RiZoneSortie LIBTF;
  private XRiRadioButton SCAN6;
  private XRiRadioButton SCAN6_TOUT;
  private JPopupMenu BTD;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_5;
  private ButtonGroup SCAN6_GRP;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
