
package ri.serien.libecranrpg.vcgm.VCGM23FM;
// Nom Fichier: pop_null_DVCGM23.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;

import org.jdesktop.swingx.JXTitledSeparator;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class DVCGM23 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public DVCGM23(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_10_OBJ_10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PARD@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    IN3ETB.setVisible(!interpreteurD.analyseExpression("@PARD@").trim().equalsIgnoreCase(""));
    IN3ETB.setEnabled(lexique.isPresent("IN3ETB"));
    INDETB.setEnabled(lexique.isPresent("INDETB"));
    IN3COD.setVisible(!interpreteurD.analyseExpression("@PARD@").trim().equalsIgnoreCase(""));
    IN3COD.setEnabled(lexique.isPresent("IN3COD"));
    INDCOD.setEnabled(lexique.isPresent("INDCOD"));
    OBJ_10_OBJ_10.setVisible(!interpreteurD.analyseExpression("@PARD@").trim().equalsIgnoreCase(""));
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Duplication"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_10_OBJ_10 = new JLabel();
    INDCOD = new XRiTextField();
    IN3COD = new XRiTextField();
    OBJ_13 = new JButton();
    OBJ_14 = new JButton();
    OBJ_7_OBJ_7 = new JLabel();
    INDETB = new XRiTextField();
    IN3ETB = new XRiTextField();
    xTitledSeparator1 = new JXTitledSeparator();

    //======== this ========
    setName("this");
    setLayout(null);

    //---- OBJ_10_OBJ_10 ----
    OBJ_10_OBJ_10.setText("@PARD@");
    OBJ_10_OBJ_10.setName("OBJ_10_OBJ_10");
    add(OBJ_10_OBJ_10);
    OBJ_10_OBJ_10.setBounds(15, 70, 126, 20);

    //---- INDCOD ----
    INDCOD.setName("INDCOD");
    add(INDCOD);
    INDCOD.setBounds(190, 40, 150, INDCOD.getPreferredSize().height);

    //---- IN3COD ----
    IN3COD.setName("IN3COD");
    add(IN3COD);
    IN3COD.setBounds(190, 70, 150, IN3COD.getPreferredSize().height);

    //---- OBJ_13 ----
    OBJ_13.setText("OK");
    OBJ_13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_13.setName("OBJ_13");
    OBJ_13.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_13ActionPerformed(e);
      }
    });
    add(OBJ_13);
    OBJ_13.setBounds(160, 110, 82, 24);

    //---- OBJ_14 ----
    OBJ_14.setText("Annuler");
    OBJ_14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_14.setName("OBJ_14");
    add(OBJ_14);
    OBJ_14.setBounds(255, 110, 82, 24);

    //---- OBJ_7_OBJ_7 ----
    OBJ_7_OBJ_7.setText("Soci\u00e9t\u00e9");
    OBJ_7_OBJ_7.setName("OBJ_7_OBJ_7");
    add(OBJ_7_OBJ_7);
    OBJ_7_OBJ_7.setBounds(15, 40, 52, 20);

    //---- INDETB ----
    INDETB.setName("INDETB");
    add(INDETB);
    INDETB.setBounds(145, 40, 40, INDETB.getPreferredSize().height);

    //---- IN3ETB ----
    IN3ETB.setName("IN3ETB");
    add(IN3ETB);
    IN3ETB.setBounds(145, 70, 40, IN3ETB.getPreferredSize().height);

    //---- xTitledSeparator1 ----
    xTitledSeparator1.setTitle("Source et destination");
    xTitledSeparator1.setName("xTitledSeparator1");
    add(xTitledSeparator1);
    xTitledSeparator1.setBounds(5, 15, 325, xTitledSeparator1.getPreferredSize().height);

    setPreferredSize(new Dimension(348, 140));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel OBJ_10_OBJ_10;
  private XRiTextField INDCOD;
  private XRiTextField IN3COD;
  private JButton OBJ_13;
  private JButton OBJ_14;
  private JLabel OBJ_7_OBJ_7;
  private XRiTextField INDETB;
  private XRiTextField IN3ETB;
  private JXTitledSeparator xTitledSeparator1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
