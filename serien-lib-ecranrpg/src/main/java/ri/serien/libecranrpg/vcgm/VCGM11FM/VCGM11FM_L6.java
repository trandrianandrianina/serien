
package ri.serien.libecranrpg.vcgm.VCGM11FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGM11FM_L6 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM11FM_L6(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    DefaultComponentFactory.setTextAndMnemonic((JLabel)separator1.getComponent(0), lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMTT@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Titre
    // setTitle(???);
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    WMEC01 = new XRiTextField();
    WDEC01 = new XRiTextField();
    WRGL01 = new XRiTextField();
    WMEC02 = new XRiTextField();
    WDEC02 = new XRiTextField();
    WRGL02 = new XRiTextField();
    WMEC03 = new XRiTextField();
    WDEC03 = new XRiTextField();
    WRGL03 = new XRiTextField();
    WMEC04 = new XRiTextField();
    WDEC04 = new XRiTextField();
    WRGL4 = new XRiTextField();
    WMEC05 = new XRiTextField();
    WDEC05 = new XRiTextField();
    WRGL05 = new XRiTextField();
    WMEC06 = new XRiTextField();
    WDEC06 = new XRiTextField();
    WRGL06 = new XRiTextField();
    WMEC07 = new XRiTextField();
    WDEC07 = new XRiTextField();
    WRGL07 = new XRiTextField();
    WMEC08 = new XRiTextField();
    WDEC08 = new XRiTextField();
    WRGL08 = new XRiTextField();
    WMEC09 = new XRiTextField();
    WDEC09 = new XRiTextField();
    WRGL09 = new XRiTextField();
    WMEC10 = new XRiTextField();
    WDEC10 = new XRiTextField();
    WRGL10 = new XRiTextField();
    WMEC11 = new XRiTextField();
    WDEC11 = new XRiTextField();
    WRGL11 = new XRiTextField();
    WMEC12 = new XRiTextField();
    WDEC12 = new XRiTextField();
    WRGL12 = new XRiTextField();
    separator1 = compFactory.createSeparator("@LIBMTT@", SwingConstants.CENTER);
    separator2 = compFactory.createSeparator("Echeance", SwingConstants.CENTER);
    separator3 = compFactory.createSeparator("Rg", SwingConstants.CENTER);

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(545, 410));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new CompoundBorder(
            new TitledBorder(""),
            new EmptyBorder(5, 5, 5, 5)));
          panel1.setOpaque(false);
          panel1.setName("panel1");

          //---- WMEC01 ----
          WMEC01.setName("WMEC01");

          //---- WDEC01 ----
          WDEC01.setName("WDEC01");

          //---- WRGL01 ----
          WRGL01.setName("WRGL01");

          //---- WMEC02 ----
          WMEC02.setName("WMEC02");

          //---- WDEC02 ----
          WDEC02.setName("WDEC02");

          //---- WRGL02 ----
          WRGL02.setName("WRGL02");

          //---- WMEC03 ----
          WMEC03.setName("WMEC03");

          //---- WDEC03 ----
          WDEC03.setName("WDEC03");

          //---- WRGL03 ----
          WRGL03.setName("WRGL03");

          //---- WMEC04 ----
          WMEC04.setName("WMEC04");

          //---- WDEC04 ----
          WDEC04.setName("WDEC04");

          //---- WRGL4 ----
          WRGL4.setName("WRGL4");

          //---- WMEC05 ----
          WMEC05.setName("WMEC05");

          //---- WDEC05 ----
          WDEC05.setName("WDEC05");

          //---- WRGL05 ----
          WRGL05.setName("WRGL05");

          //---- WMEC06 ----
          WMEC06.setName("WMEC06");

          //---- WDEC06 ----
          WDEC06.setName("WDEC06");

          //---- WRGL06 ----
          WRGL06.setName("WRGL06");

          //---- WMEC07 ----
          WMEC07.setName("WMEC07");

          //---- WDEC07 ----
          WDEC07.setName("WDEC07");

          //---- WRGL07 ----
          WRGL07.setName("WRGL07");

          //---- WMEC08 ----
          WMEC08.setName("WMEC08");

          //---- WDEC08 ----
          WDEC08.setName("WDEC08");

          //---- WRGL08 ----
          WRGL08.setName("WRGL08");

          //---- WMEC09 ----
          WMEC09.setName("WMEC09");

          //---- WDEC09 ----
          WDEC09.setName("WDEC09");

          //---- WRGL09 ----
          WRGL09.setName("WRGL09");

          //---- WMEC10 ----
          WMEC10.setName("WMEC10");

          //---- WDEC10 ----
          WDEC10.setName("WDEC10");

          //---- WRGL10 ----
          WRGL10.setName("WRGL10");

          //---- WMEC11 ----
          WMEC11.setName("WMEC11");

          //---- WDEC11 ----
          WDEC11.setName("WDEC11");

          //---- WRGL11 ----
          WRGL11.setName("WRGL11");

          //---- WMEC12 ----
          WMEC12.setName("WMEC12");

          //---- WDEC12 ----
          WDEC12.setName("WDEC12");

          //---- WRGL12 ----
          WRGL12.setName("WRGL12");

          //---- separator1 ----
          separator1.setName("separator1");

          //---- separator2 ----
          separator2.setName("separator2");

          //---- separator3 ----
          separator3.setName("separator3");

          GroupLayout panel1Layout = new GroupLayout(panel1);
          panel1.setLayout(panel1Layout);
          panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(separator1, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                    .addGap(15, 15, 15)
                    .addComponent(separator2, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                    .addGap(15, 15, 15)
                    .addComponent(separator3, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGroup(panel1Layout.createParallelGroup()
                      .addComponent(WMEC05, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WMEC02, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WMEC06, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WMEC03, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WMEC01, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WMEC04, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WMEC08, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WMEC10, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WMEC09, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WMEC11, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WMEC12, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WMEC07, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE))
                    .addGap(15, 15, 15)
                    .addGroup(panel1Layout.createParallelGroup()
                      .addComponent(WDEC04, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WDEC05, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WDEC08, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WDEC03, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WDEC06, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WDEC01, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WDEC02, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WDEC07, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WDEC10, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WDEC11, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WDEC12, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WDEC09, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE))
                    .addGap(15, 15, 15)
                    .addGroup(panel1Layout.createParallelGroup()
                      .addComponent(WRGL01, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WRGL12, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WRGL06, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WRGL09, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WRGL08, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WRGL4, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WRGL07, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WRGL05, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WRGL03, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WRGL10, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WRGL11, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WRGL02, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))))
          );
          panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(panel1Layout.createParallelGroup()
                  .addComponent(separator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(separator2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(separator3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGroup(panel1Layout.createParallelGroup()
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(100, 100, 100)
                        .addComponent(WMEC05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(WMEC02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(125, 125, 125)
                        .addComponent(WMEC06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(WMEC03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addComponent(WMEC01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(75, 75, 75)
                        .addComponent(WMEC04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                    .addGap(22, 22, 22)
                    .addGroup(panel1Layout.createParallelGroup()
                      .addComponent(WMEC08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(WMEC10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(WMEC09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(75, 75, 75)
                        .addComponent(WMEC11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(100, 100, 100)
                        .addComponent(WMEC12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(150, 150, 150)
                    .addComponent(WMEC07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGroup(panel1Layout.createParallelGroup()
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(75, 75, 75)
                        .addComponent(WDEC04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(100, 100, 100)
                        .addComponent(WDEC05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(175, 175, 175)
                        .addComponent(WDEC08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(WDEC03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(125, 125, 125)
                        .addComponent(WDEC06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addComponent(WDEC01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(WDEC02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(150, 150, 150)
                        .addComponent(WDEC07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                    .addGap(22, 22, 22)
                    .addGroup(panel1Layout.createParallelGroup()
                      .addComponent(WDEC10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(WDEC11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(WDEC12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(200, 200, 200)
                    .addComponent(WDEC09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(WRGL01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(22, 22, 22)
                    .addGroup(panel1Layout.createParallelGroup()
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(225, 225, 225)
                        .addComponent(WRGL12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(75, 75, 75)
                        .addComponent(WRGL06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(150, 150, 150)
                        .addComponent(WRGL09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(125, 125, 125)
                        .addComponent(WRGL08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(WRGL4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(100, 100, 100)
                        .addComponent(WRGL07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(WRGL05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addComponent(WRGL03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(175, 175, 175)
                        .addComponent(WRGL10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(200, 200, 200)
                        .addComponent(WRGL11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(25, 25, 25)
                    .addComponent(WRGL02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
          );
        }
        p_contenu.add(panel1);
        panel1.setBounds(15, 15, 350, 380);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField WMEC01;
  private XRiTextField WDEC01;
  private XRiTextField WRGL01;
  private XRiTextField WMEC02;
  private XRiTextField WDEC02;
  private XRiTextField WRGL02;
  private XRiTextField WMEC03;
  private XRiTextField WDEC03;
  private XRiTextField WRGL03;
  private XRiTextField WMEC04;
  private XRiTextField WDEC04;
  private XRiTextField WRGL4;
  private XRiTextField WMEC05;
  private XRiTextField WDEC05;
  private XRiTextField WRGL05;
  private XRiTextField WMEC06;
  private XRiTextField WDEC06;
  private XRiTextField WRGL06;
  private XRiTextField WMEC07;
  private XRiTextField WDEC07;
  private XRiTextField WRGL07;
  private XRiTextField WMEC08;
  private XRiTextField WDEC08;
  private XRiTextField WRGL08;
  private XRiTextField WMEC09;
  private XRiTextField WDEC09;
  private XRiTextField WRGL09;
  private XRiTextField WMEC10;
  private XRiTextField WDEC10;
  private XRiTextField WRGL10;
  private XRiTextField WMEC11;
  private XRiTextField WDEC11;
  private XRiTextField WRGL11;
  private XRiTextField WMEC12;
  private XRiTextField WDEC12;
  private XRiTextField WRGL12;
  private JComponent separator1;
  private JComponent separator2;
  private JComponent separator3;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
