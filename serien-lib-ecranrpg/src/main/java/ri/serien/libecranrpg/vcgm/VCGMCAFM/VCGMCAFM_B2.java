
package ri.serien.libecranrpg.vcgm.VCGMCAFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGMCAFM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGMCAFM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    CAREL.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    CAREP5.setEnabled(lexique.isPresent("CAREP5"));
    CAREP4.setEnabled(lexique.isPresent("CAREP4"));
    CAREP3.setEnabled(lexique.isPresent("CAREP3"));
    CAREP2.setEnabled(lexique.isPresent("CAREP2"));
    CAREP1.setEnabled(lexique.isPresent("CAREP1"));
    CACOP4.setEnabled(lexique.isPresent("CACOP4"));
    CACOP3.setEnabled(lexique.isPresent("CACOP3"));
    CACOP2.setEnabled(lexique.isPresent("CACOP2"));
    CACOP1.setEnabled(lexique.isPresent("CACOP1"));
    CACDPF.setEnabled(lexique.isPresent("CACDPF"));
    CACDPD.setEnabled(lexique.isPresent("CACDPD"));
    // CAREL.setEnabled( lexique.isPresent("CAREL"));
    // CAREL.setSelected(lexique.HostFieldGetData("CAREL").equalsIgnoreCase("OUI"));
    CACL1F.setEnabled(lexique.isPresent("CACL1F"));
    CACL1D.setEnabled(lexique.isPresent("CACL1D"));
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Critères de sélection"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (CAREL.isSelected())
    // lexique.HostFieldPutData("CAREL", 0, "OUI");
    // else
    // lexique.HostFieldPutData("CAREL", 0, "NON");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    CACL1D = new XRiTextField();
    CACL1F = new XRiTextField();
    CAREL = new XRiCheckBox();
    OBJ_49 = new JLabel();
    OBJ_27 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_45 = new JLabel();
    CACDPD = new XRiTextField();
    CACDPF = new XRiTextField();
    CACOP1 = new XRiTextField();
    CACOP2 = new XRiTextField();
    CACOP3 = new XRiTextField();
    CACOP4 = new XRiTextField();
    OBJ_46 = new JLabel();
    CAREP1 = new XRiTextField();
    CAREP2 = new XRiTextField();
    CAREP3 = new XRiTextField();
    CAREP4 = new XRiTextField();
    CAREP5 = new XRiTextField();
    OBJ_48 = new JLabel();
    OBJ_44 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(655, 245));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Identification"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- CACL1D ----
          CACL1D.setComponentPopupMenu(BTD);
          CACL1D.setName("CACL1D");
          panel2.add(CACL1D);
          CACL1D.setBounds(115, 115, 160, CACL1D.getPreferredSize().height);

          //---- CACL1F ----
          CACL1F.setComponentPopupMenu(BTD);
          CACL1F.setName("CACL1F");
          panel2.add(CACL1F);
          CACL1F.setBounds(285, 115, 160, CACL1F.getPreferredSize().height);

          //---- CAREL ----
          CAREL.setText("A relancer");
          CAREL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CAREL.setName("CAREL");
          panel2.add(CAREL);
          CAREL.setBounds(25, 180, 205, 20);

          //---- OBJ_49 ----
          OBJ_49.setText("Repr\u00e9sentant");
          OBJ_49.setName("OBJ_49");
          panel2.add(OBJ_49);
          OBJ_49.setBounds(25, 149, 84, 20);

          //---- OBJ_27 ----
          OBJ_27.setText("Code postal");
          OBJ_27.setName("OBJ_27");
          panel2.add(OBJ_27);
          OBJ_27.setBounds(25, 39, 78, 20);

          //---- OBJ_47 ----
          OBJ_47.setText("Code alpha");
          OBJ_47.setName("OBJ_47");
          panel2.add(OBJ_47);
          OBJ_47.setBounds(25, 119, 75, 20);

          //---- OBJ_45 ----
          OBJ_45.setText("Code pays");
          OBJ_45.setName("OBJ_45");
          panel2.add(OBJ_45);
          OBJ_45.setBounds(25, 69, 70, 20);

          //---- CACDPD ----
          CACDPD.setComponentPopupMenu(BTD);
          CACDPD.setName("CACDPD");
          panel2.add(CACDPD);
          CACDPD.setBounds(115, 35, 50, CACDPD.getPreferredSize().height);

          //---- CACDPF ----
          CACDPF.setComponentPopupMenu(BTD);
          CACDPF.setName("CACDPF");
          panel2.add(CACDPF);
          CACDPF.setBounds(205, 35, 50, CACDPF.getPreferredSize().height);

          //---- CACOP1 ----
          CACOP1.setComponentPopupMenu(BTD);
          CACOP1.setName("CACOP1");
          panel2.add(CACOP1);
          CACOP1.setBounds(115, 65, 40, CACOP1.getPreferredSize().height);

          //---- CACOP2 ----
          CACOP2.setComponentPopupMenu(BTD);
          CACOP2.setName("CACOP2");
          panel2.add(CACOP2);
          CACOP2.setBounds(160, 65, 40, CACOP2.getPreferredSize().height);

          //---- CACOP3 ----
          CACOP3.setComponentPopupMenu(BTD);
          CACOP3.setName("CACOP3");
          panel2.add(CACOP3);
          CACOP3.setBounds(205, 65, 40, CACOP3.getPreferredSize().height);

          //---- CACOP4 ----
          CACOP4.setComponentPopupMenu(BTD);
          CACOP4.setName("CACOP4");
          panel2.add(CACOP4);
          CACOP4.setBounds(250, 65, 40, CACOP4.getPreferredSize().height);

          //---- OBJ_46 ----
          OBJ_46.setText("D\u00e9but");
          OBJ_46.setName("OBJ_46");
          panel2.add(OBJ_46);
          OBJ_46.setBounds(117, 98, 39, 14);

          //---- CAREP1 ----
          CAREP1.setComponentPopupMenu(BTD);
          CAREP1.setName("CAREP1");
          panel2.add(CAREP1);
          CAREP1.setBounds(115, 145, 30, CAREP1.getPreferredSize().height);

          //---- CAREP2 ----
          CAREP2.setComponentPopupMenu(BTD);
          CAREP2.setName("CAREP2");
          panel2.add(CAREP2);
          CAREP2.setBounds(148, 145, 30, CAREP2.getPreferredSize().height);

          //---- CAREP3 ----
          CAREP3.setComponentPopupMenu(BTD);
          CAREP3.setName("CAREP3");
          panel2.add(CAREP3);
          CAREP3.setBounds(181, 145, 30, CAREP3.getPreferredSize().height);

          //---- CAREP4 ----
          CAREP4.setComponentPopupMenu(BTD);
          CAREP4.setName("CAREP4");
          panel2.add(CAREP4);
          CAREP4.setBounds(214, 145, 30, CAREP4.getPreferredSize().height);

          //---- CAREP5 ----
          CAREP5.setComponentPopupMenu(BTD);
          CAREP5.setName("CAREP5");
          panel2.add(CAREP5);
          CAREP5.setBounds(247, 145, 30, CAREP5.getPreferredSize().height);

          //---- OBJ_48 ----
          OBJ_48.setText("Fin");
          OBJ_48.setName("OBJ_48");
          panel2.add(OBJ_48);
          OBJ_48.setBounds(287, 98, 21, 14);

          //---- OBJ_44 ----
          OBJ_44.setText("\u00e0");
          OBJ_44.setName("OBJ_44");
          panel2.add(OBJ_44);
          OBJ_44.setBounds(180, 39, 12, 20);
        }
        p_contenu.add(panel2);
        panel2.setBounds(10, 10, 465, 225);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiTextField CACL1D;
  private XRiTextField CACL1F;
  private XRiCheckBox CAREL;
  private JLabel OBJ_49;
  private JLabel OBJ_27;
  private JLabel OBJ_47;
  private JLabel OBJ_45;
  private XRiTextField CACDPD;
  private XRiTextField CACDPF;
  private XRiTextField CACOP1;
  private XRiTextField CACOP2;
  private XRiTextField CACOP3;
  private XRiTextField CACOP4;
  private JLabel OBJ_46;
  private XRiTextField CAREP1;
  private XRiTextField CAREP2;
  private XRiTextField CAREP3;
  private XRiTextField CAREP4;
  private XRiTextField CAREP5;
  private JLabel OBJ_48;
  private JLabel OBJ_44;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
