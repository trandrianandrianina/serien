
package ri.serien.libecranrpg.vcgm.VCGMCLFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

public class VCGMCLFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  // TODO declarations classe spécifiques...
  
  public VCGMCLFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // TODO constructeur spécifiques...
    
    // Ajout
    initDiverses();
    CLLIGA.setValeursSelection("OUI", "NON");
    CLECRP.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDNUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDNUM@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // TODO setData spécifiques...
    // CLECRP.setSelected(lexique.HostFieldGetData("CLECRP").equals("OUI"));
    // CLLIGA.setSelected(lexique.HostFieldGetData("CLLIGA").equals("OUI"));
    
    

    p_bpresentation.setCodeEtablissement(INDNUM.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(INDNUM.getText()));
    p_TCI1.setRightDecoration(TCI1);
    p_TCI2.setRightDecoration(TCI2);
    p_TCI3.setRightDecoration(TCI3);
    p_TCI4.setRightDecoration(TCI4);
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // TODO getDATA spécifiques
    // if (CLECRP.isSelected())
    // lexique.HostFieldPutData("CLECRP", 0, "OUI");
    // else
    // lexique.HostFieldPutData("CLECRP", 0, "NON");
    // if (CLLIGA.isSelected())
    // lexique.HostFieldPutData("CLLIGA", 0, "OUI");
    // else
    // lexique.HostFieldPutData("CLLIGA", 0, "NON");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void TCI1ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI1").trim().equals("")) {
      lexique.HostFieldPutData("TCI1", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI1"));
    }
    
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void TCI2ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI2").trim().equals("")) {
      lexique.HostFieldPutData("TCI2", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI2"));
    }
    
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void TCI3ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI3").trim().equals("")) {
      lexique.HostFieldPutData("TCI3", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI3"));
    }
    
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void TCI4ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI4").trim().equals("")) {
      lexique.HostFieldPutData("TCI4", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI4"));
    }
    
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_50 = new JLabel();
    INDNUM = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    CLOBJ = new XRiTextField();
    CLOBS = new XRiTextField();
    OBJ_56 = new JLabel();
    OBJ_104 = new JLabel();
    xTitledSeparator1 = new JXTitledSeparator();
    p_TCI4 = new JXTitledPanel();
    CLECRP = new XRiCheckBox();
    OBJ_46 = new JLabel();
    OBJ_48 = new JLabel();
    CLNUPD = new XRiTextField();
    CLNUPF = new XRiTextField();
    CLDPDX = new XRiCalendrier();
    CLDPFX = new XRiCalendrier();
    OBJ_47 = new JLabel();
    OBJ_49 = new JLabel();
    p_TCI3 = new JXTitledPanel();
    CLLIGA = new XRiCheckBox();
    OBJ_44 = new JLabel();
    CLSAN1 = new XRiTextField();
    CLSAN2 = new XRiTextField();
    CLSAN3 = new XRiTextField();
    CLSAN4 = new XRiTextField();
    CLACT1 = new XRiTextField();
    CLACT2 = new XRiTextField();
    CLACT3 = new XRiTextField();
    CLACT4 = new XRiTextField();
    OBJ_45 = new JLabel();
    p_TCI2 = new JXTitledPanel();
    OBJ_40 = new JLabel();
    OBJ_42 = new JLabel();
    CLGEND = new XRiTextField();
    CLGENF = new XRiTextField();
    CLAUXD = new XRiTextField();
    CLAUXF = new XRiTextField();
    OBJ_41 = new JLabel();
    OBJ_43 = new JLabel();
    p_TCI1 = new JXTitledPanel();
    OBJ_37 = new JLabel();
    OBJ_38 = new JLabel();
    CLCFO1 = new XRiTextField();
    CLCFO2 = new XRiTextField();
    CLCFO3 = new XRiTextField();
    CLCFO4 = new XRiTextField();
    OBJ_36 = new JLabel();
    OBJ_39 = new JLabel();
    CLCJO1 = new XRiTextField();
    CLCJO2 = new XRiTextField();
    CLCJO3 = new XRiTextField();
    CLCJO4 = new XRiTextField();
    CLCJO5 = new XRiTextField();
    CLTYP1 = new XRiTextField();
    CLTYP2 = new XRiTextField();
    CLTYP3 = new XRiTextField();
    CLETAT = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    TCI1 = new SNBoutonDetail();
    TCI2 = new SNBoutonDetail();
    TCI3 = new SNBoutonDetail();
    TCI4 = new SNBoutonDetail();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Crit\u00e8res de s\u00e9lection");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_50 ----
          OBJ_50.setText("Num\u00e9ro");
          OBJ_50.setName("OBJ_50");
          p_tete_gauche.add(OBJ_50);
          OBJ_50.setBounds(5, 5, 52, 20);

          //---- INDNUM ----
          INDNUM.setComponentPopupMenu(BTD);
          INDNUM.setOpaque(false);
          INDNUM.setText("@INDNUM@");
          INDNUM.setName("INDNUM");
          p_tete_gauche.add(INDNUM);
          INDNUM.setBounds(110, 3, 40, INDNUM.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_tete_gauche.getComponentCount(); i++) {
              Rectangle bounds = p_tete_gauche.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_tete_gauche.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_tete_gauche.setMinimumSize(preferredSize);
            p_tete_gauche.setPreferredSize(preferredSize);
          }
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(800, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- CLOBJ ----
          CLOBJ.setComponentPopupMenu(BTD);
          CLOBJ.setName("CLOBJ");

          //---- CLOBS ----
          CLOBS.setComponentPopupMenu(BTD);
          CLOBS.setName("CLOBS");

          //---- OBJ_56 ----
          OBJ_56.setText("Objet s\u00e9lection");
          OBJ_56.setName("OBJ_56");

          //---- OBJ_104 ----
          OBJ_104.setText("Observation");
          OBJ_104.setName("OBJ_104");

          //---- xTitledSeparator1 ----
          xTitledSeparator1.setTitle("Ent\u00eate et lignes d'\u00e9critures");
          xTitledSeparator1.setName("xTitledSeparator1");

          //======== p_TCI4 ========
          {
            p_TCI4.setTitle("Divers");
            p_TCI4.setName("p_TCI4");
            Container p_TCI4ContentContainer = p_TCI4.getContentContainer();
            p_TCI4ContentContainer.setLayout(null);

            //---- CLECRP ----
            CLECRP.setText("\u00e9criture lettr\u00e9e");
            CLECRP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CLECRP.setName("CLECRP");
            p_TCI4ContentContainer.add(CLECRP);
            CLECRP.setBounds(20, 19, 129, 20);

            //---- OBJ_46 ----
            OBJ_46.setText("Date lettrage");
            OBJ_46.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_46.setName("OBJ_46");
            p_TCI4ContentContainer.add(OBJ_46);
            OBJ_46.setBounds(150, 19, 80, 20);

            //---- OBJ_48 ----
            OBJ_48.setText("Num\u00e9ro de pi\u00e8ce");
            OBJ_48.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_48.setName("OBJ_48");
            p_TCI4ContentContainer.add(OBJ_48);
            OBJ_48.setBounds(125, 54, 105, 20);

            //---- CLNUPD ----
            CLNUPD.setComponentPopupMenu(BTD);
            CLNUPD.setName("CLNUPD");
            p_TCI4ContentContainer.add(CLNUPD);
            CLNUPD.setBounds(240, 50, 66, CLNUPD.getPreferredSize().height);

            //---- CLNUPF ----
            CLNUPF.setComponentPopupMenu(BTD);
            CLNUPF.setName("CLNUPF");
            p_TCI4ContentContainer.add(CLNUPF);
            CLNUPF.setBounds(385, 50, 66, CLNUPF.getPreferredSize().height);

            //---- CLDPDX ----
            CLDPDX.setComponentPopupMenu(BTD);
            CLDPDX.setTypeSaisie(6);
            CLDPDX.setName("CLDPDX");
            p_TCI4ContentContainer.add(CLDPDX);
            CLDPDX.setBounds(240, 15, 105, CLDPDX.getPreferredSize().height);

            //---- CLDPFX ----
            CLDPFX.setComponentPopupMenu(BTD);
            CLDPFX.setTypeSaisie(6);
            CLDPFX.setName("CLDPFX");
            p_TCI4ContentContainer.add(CLDPFX);
            CLDPFX.setBounds(385, 15, 105, CLDPFX.getPreferredSize().height);

            //---- OBJ_47 ----
            OBJ_47.setText("\u00e0");
            OBJ_47.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_47.setName("OBJ_47");
            p_TCI4ContentContainer.add(OBJ_47);
            OBJ_47.setBounds(365, 20, 12, 20);

            //---- OBJ_49 ----
            OBJ_49.setText("\u00e0");
            OBJ_49.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_49.setName("OBJ_49");
            p_TCI4ContentContainer.add(OBJ_49);
            OBJ_49.setBounds(340, 55, 12, 20);
          }

          //======== p_TCI3 ========
          {
            p_TCI3.setTitle("Analytique");
            p_TCI3.setName("p_TCI3");
            Container p_TCI3ContentContainer = p_TCI3.getContentContainer();
            p_TCI3ContentContainer.setLayout(null);

            //---- CLLIGA ----
            CLLIGA.setText("ligne analytique");
            CLLIGA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CLLIGA.setName("CLLIGA");
            p_TCI3ContentContainer.add(CLLIGA);
            CLLIGA.setBounds(20, 19, 120, 20);

            //---- OBJ_44 ----
            OBJ_44.setText("Section");
            OBJ_44.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_44.setName("OBJ_44");
            p_TCI3ContentContainer.add(OBJ_44);
            OBJ_44.setBounds(180, 19, 48, 20);

            //---- CLSAN1 ----
            CLSAN1.setComponentPopupMenu(BTD);
            CLSAN1.setName("CLSAN1");
            p_TCI3ContentContainer.add(CLSAN1);
            CLSAN1.setBounds(240, 15, 50, CLSAN1.getPreferredSize().height);

            //---- CLSAN2 ----
            CLSAN2.setComponentPopupMenu(BTD);
            CLSAN2.setName("CLSAN2");
            p_TCI3ContentContainer.add(CLSAN2);
            CLSAN2.setBounds(290, 15, 50, CLSAN2.getPreferredSize().height);

            //---- CLSAN3 ----
            CLSAN3.setComponentPopupMenu(BTD);
            CLSAN3.setName("CLSAN3");
            p_TCI3ContentContainer.add(CLSAN3);
            CLSAN3.setBounds(340, 15, 50, CLSAN3.getPreferredSize().height);

            //---- CLSAN4 ----
            CLSAN4.setComponentPopupMenu(BTD);
            CLSAN4.setName("CLSAN4");
            p_TCI3ContentContainer.add(CLSAN4);
            CLSAN4.setBounds(390, 15, 50, CLSAN4.getPreferredSize().height);

            //---- CLACT1 ----
            CLACT1.setComponentPopupMenu(BTD);
            CLACT1.setName("CLACT1");
            p_TCI3ContentContainer.add(CLACT1);
            CLACT1.setBounds(475, 15, 50, CLACT1.getPreferredSize().height);

            //---- CLACT2 ----
            CLACT2.setComponentPopupMenu(BTD);
            CLACT2.setName("CLACT2");
            p_TCI3ContentContainer.add(CLACT2);
            CLACT2.setBounds(525, 15, 50, CLACT2.getPreferredSize().height);

            //---- CLACT3 ----
            CLACT3.setComponentPopupMenu(BTD);
            CLACT3.setName("CLACT3");
            p_TCI3ContentContainer.add(CLACT3);
            CLACT3.setBounds(575, 15, 50, CLACT3.getPreferredSize().height);

            //---- CLACT4 ----
            CLACT4.setComponentPopupMenu(BTD);
            CLACT4.setName("CLACT4");
            p_TCI3ContentContainer.add(CLACT4);
            CLACT4.setBounds(625, 15, 50, CLACT4.getPreferredSize().height);

            //---- OBJ_45 ----
            OBJ_45.setText("Aff");
            OBJ_45.setName("OBJ_45");
            p_TCI3ContentContainer.add(OBJ_45);
            OBJ_45.setBounds(455, 20, 18, 20);
          }

          //======== p_TCI2 ========
          {
            p_TCI2.setTitle("Comptes");
            p_TCI2.setOpaque(true);
            p_TCI2.setName("p_TCI2");
            Container p_TCI2ContentContainer = p_TCI2.getContentContainer();
            p_TCI2ContentContainer.setLayout(null);

            //---- OBJ_40 ----
            OBJ_40.setText("Compte g\u00e9n\u00e9ral");
            OBJ_40.setName("OBJ_40");
            p_TCI2ContentContainer.add(OBJ_40);
            OBJ_40.setBounds(20, 19, 118, 20);

            //---- OBJ_42 ----
            OBJ_42.setText("Compte auxiliaire");
            OBJ_42.setName("OBJ_42");
            p_TCI2ContentContainer.add(OBJ_42);
            OBJ_42.setBounds(350, 19, 118, 20);

            //---- CLGEND ----
            CLGEND.setComponentPopupMenu(BTD);
            CLGEND.setName("CLGEND");
            p_TCI2ContentContainer.add(CLGEND);
            CLGEND.setBounds(130, 15, 58, CLGEND.getPreferredSize().height);

            //---- CLGENF ----
            CLGENF.setComponentPopupMenu(BTD);
            CLGENF.setName("CLGENF");
            p_TCI2ContentContainer.add(CLGENF);
            CLGENF.setBounds(220, 15, 58, CLGENF.getPreferredSize().height);

            //---- CLAUXD ----
            CLAUXD.setComponentPopupMenu(BTD);
            CLAUXD.setName("CLAUXD");
            p_TCI2ContentContainer.add(CLAUXD);
            CLAUXD.setBounds(475, 15, 58, CLAUXD.getPreferredSize().height);

            //---- CLAUXF ----
            CLAUXF.setComponentPopupMenu(BTD);
            CLAUXF.setName("CLAUXF");
            p_TCI2ContentContainer.add(CLAUXF);
            CLAUXF.setBounds(565, 15, 58, CLAUXF.getPreferredSize().height);

            //---- OBJ_41 ----
            OBJ_41.setText("\u00e0");
            OBJ_41.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_41.setName("OBJ_41");
            p_TCI2ContentContainer.add(OBJ_41);
            OBJ_41.setBounds(195, 19, 18, 20);

            //---- OBJ_43 ----
            OBJ_43.setText("\u00e0");
            OBJ_43.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_43.setName("OBJ_43");
            p_TCI2ContentContainer.add(OBJ_43);
            OBJ_43.setBounds(540, 20, 18, 20);
          }

          //======== p_TCI1 ========
          {
            p_TCI1.setTitle("Identification");
            p_TCI1.setBorder(new DropShadowBorder());
            p_TCI1.setName("p_TCI1");
            Container p_TCI1ContentContainer = p_TCI1.getContentContainer();
            p_TCI1ContentContainer.setLayout(null);

            //---- OBJ_37 ----
            OBJ_37.setText("Journaux");
            OBJ_37.setName("OBJ_37");
            p_TCI1ContentContainer.add(OBJ_37);
            OBJ_37.setBounds(205, 20, 57, 20);

            //---- OBJ_38 ----
            OBJ_38.setText("Folios");
            OBJ_38.setName("OBJ_38");
            p_TCI1ContentContainer.add(OBJ_38);
            OBJ_38.setBounds(430, 20, 40, 20);

            //---- CLCFO1 ----
            CLCFO1.setComponentPopupMenu(BTD);
            CLCFO1.setName("CLCFO1");
            p_TCI1ContentContainer.add(CLCFO1);
            CLCFO1.setBounds(475, 15, 50, CLCFO1.getPreferredSize().height);

            //---- CLCFO2 ----
            CLCFO2.setComponentPopupMenu(BTD);
            CLCFO2.setName("CLCFO2");
            p_TCI1ContentContainer.add(CLCFO2);
            CLCFO2.setBounds(525, 15, 50, CLCFO2.getPreferredSize().height);

            //---- CLCFO3 ----
            CLCFO3.setComponentPopupMenu(BTD);
            CLCFO3.setName("CLCFO3");
            p_TCI1ContentContainer.add(CLCFO3);
            CLCFO3.setBounds(575, 15, 50, CLCFO3.getPreferredSize().height);

            //---- CLCFO4 ----
            CLCFO4.setComponentPopupMenu(BTD);
            CLCFO4.setName("CLCFO4");
            p_TCI1ContentContainer.add(CLCFO4);
            CLCFO4.setBounds(625, 15, 50, CLCFO4.getPreferredSize().height);

            //---- OBJ_36 ----
            OBJ_36.setText("Type");
            OBJ_36.setName("OBJ_36");
            p_TCI1ContentContainer.add(OBJ_36);
            OBJ_36.setBounds(95, 20, 36, 20);

            //---- OBJ_39 ----
            OBJ_39.setText("Etat");
            OBJ_39.setName("OBJ_39");
            p_TCI1ContentContainer.add(OBJ_39);
            OBJ_39.setBounds(20, 20, 27, 20);

            //---- CLCJO1 ----
            CLCJO1.setComponentPopupMenu(BTD);
            CLCJO1.setName("CLCJO1");
            p_TCI1ContentContainer.add(CLCJO1);
            CLCJO1.setBounds(265, 15, 30, CLCJO1.getPreferredSize().height);

            //---- CLCJO2 ----
            CLCJO2.setComponentPopupMenu(BTD);
            CLCJO2.setName("CLCJO2");
            p_TCI1ContentContainer.add(CLCJO2);
            CLCJO2.setBounds(295, 15, 30, CLCJO2.getPreferredSize().height);

            //---- CLCJO3 ----
            CLCJO3.setComponentPopupMenu(BTD);
            CLCJO3.setName("CLCJO3");
            p_TCI1ContentContainer.add(CLCJO3);
            CLCJO3.setBounds(325, 15, 30, CLCJO3.getPreferredSize().height);

            //---- CLCJO4 ----
            CLCJO4.setComponentPopupMenu(BTD);
            CLCJO4.setName("CLCJO4");
            p_TCI1ContentContainer.add(CLCJO4);
            CLCJO4.setBounds(350, 15, 30, CLCJO4.getPreferredSize().height);

            //---- CLCJO5 ----
            CLCJO5.setComponentPopupMenu(BTD);
            CLCJO5.setName("CLCJO5");
            p_TCI1ContentContainer.add(CLCJO5);
            CLCJO5.setBounds(380, 15, 30, CLCJO5.getPreferredSize().height);

            //---- CLTYP1 ----
            CLTYP1.setComponentPopupMenu(BTD);
            CLTYP1.setName("CLTYP1");
            p_TCI1ContentContainer.add(CLTYP1);
            CLTYP1.setBounds(130, 15, 20, CLTYP1.getPreferredSize().height);

            //---- CLTYP2 ----
            CLTYP2.setComponentPopupMenu(BTD);
            CLTYP2.setName("CLTYP2");
            p_TCI1ContentContainer.add(CLTYP2);
            CLTYP2.setBounds(150, 15, 20, CLTYP2.getPreferredSize().height);

            //---- CLTYP3 ----
            CLTYP3.setComponentPopupMenu(BTD);
            CLTYP3.setName("CLTYP3");
            p_TCI1ContentContainer.add(CLTYP3);
            CLTYP3.setBounds(170, 15, 20, CLTYP3.getPreferredSize().height);

            //---- CLETAT ----
            CLETAT.setComponentPopupMenu(BTD);
            CLETAT.setName("CLETAT");
            p_TCI1ContentContainer.add(CLETAT);
            CLETAT.setBounds(50, 15, 20, CLETAT.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < p_TCI1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = p_TCI1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = p_TCI1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              p_TCI1ContentContainer.setMinimumSize(preferredSize);
              p_TCI1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(42, 42, 42)
                    .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, 700, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(57, 57, 57)
                    .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE)
                    .addGap(16, 16, 16)
                    .addComponent(CLOBJ, GroupLayout.PREFERRED_SIZE, 570, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(57, 57, 57)
                    .addComponent(OBJ_104, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                    .addGap(34, 34, 34)
                    .addComponent(CLOBS, GroupLayout.PREFERRED_SIZE, 570, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(37, 37, 37)
                    .addComponent(p_TCI3, GroupLayout.PREFERRED_SIZE, 705, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(37, 37, 37)
                    .addComponent(p_TCI4, GroupLayout.PREFERRED_SIZE, 705, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(37, 37, 37)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(p_TCI1, GroupLayout.PREFERRED_SIZE, 705, GroupLayout.PREFERRED_SIZE)
                      .addComponent(p_TCI2, GroupLayout.PREFERRED_SIZE, 705, GroupLayout.PREFERRED_SIZE))))
                .addGap(56, 56, 56))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CLOBJ, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_104, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CLOBS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addComponent(p_TCI1, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(p_TCI2, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(p_TCI3, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(p_TCI4, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }

    //---- TCI1 ----
    TCI1.setText("");
    TCI1.setToolTipText("Identification");
    TCI1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI1.setOpaque(true);
    TCI1.setName("TCI1");
    TCI1.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI1ActionPerformed(e);
      }
    });

    //---- TCI2 ----
    TCI2.setText("");
    TCI2.setToolTipText("Comptes");
    TCI2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI2.setName("TCI2");
    TCI2.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI2ActionPerformed(e);
      }
    });

    //---- TCI3 ----
    TCI3.setText("");
    TCI3.setToolTipText("Analytique");
    TCI3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI3.setName("TCI3");
    TCI3.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI3ActionPerformed(e);
      }
    });

    //---- TCI4 ----
    TCI4.setText("");
    TCI4.setToolTipText("Divers");
    TCI4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI4.setName("TCI4");
    TCI4.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI4ActionPerformed(e);
      }
    });
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_50;
  private RiZoneSortie INDNUM;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private XRiTextField CLOBJ;
  private XRiTextField CLOBS;
  private JLabel OBJ_56;
  private JLabel OBJ_104;
  private JXTitledSeparator xTitledSeparator1;
  private JXTitledPanel p_TCI4;
  private XRiCheckBox CLECRP;
  private JLabel OBJ_46;
  private JLabel OBJ_48;
  private XRiTextField CLNUPD;
  private XRiTextField CLNUPF;
  private XRiCalendrier CLDPDX;
  private XRiCalendrier CLDPFX;
  private JLabel OBJ_47;
  private JLabel OBJ_49;
  private JXTitledPanel p_TCI3;
  private XRiCheckBox CLLIGA;
  private JLabel OBJ_44;
  private XRiTextField CLSAN1;
  private XRiTextField CLSAN2;
  private XRiTextField CLSAN3;
  private XRiTextField CLSAN4;
  private XRiTextField CLACT1;
  private XRiTextField CLACT2;
  private XRiTextField CLACT3;
  private XRiTextField CLACT4;
  private JLabel OBJ_45;
  private JXTitledPanel p_TCI2;
  private JLabel OBJ_40;
  private JLabel OBJ_42;
  private XRiTextField CLGEND;
  private XRiTextField CLGENF;
  private XRiTextField CLAUXD;
  private XRiTextField CLAUXF;
  private JLabel OBJ_41;
  private JLabel OBJ_43;
  private JXTitledPanel p_TCI1;
  private JLabel OBJ_37;
  private JLabel OBJ_38;
  private XRiTextField CLCFO1;
  private XRiTextField CLCFO2;
  private XRiTextField CLCFO3;
  private XRiTextField CLCFO4;
  private JLabel OBJ_36;
  private JLabel OBJ_39;
  private XRiTextField CLCJO1;
  private XRiTextField CLCJO2;
  private XRiTextField CLCJO3;
  private XRiTextField CLCJO4;
  private XRiTextField CLCJO5;
  private XRiTextField CLTYP1;
  private XRiTextField CLTYP2;
  private XRiTextField CLTYP3;
  private XRiTextField CLETAT;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  private SNBoutonDetail TCI1;
  private SNBoutonDetail TCI2;
  private SNBoutonDetail TCI3;
  private SNBoutonDetail TCI4;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
