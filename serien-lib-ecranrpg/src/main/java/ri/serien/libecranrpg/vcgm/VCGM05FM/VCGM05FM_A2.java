
package ri.serien.libecranrpg.vcgm.VCGM05FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM05FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "HLD01", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 541, };
  // private String[][] _LIST_Title_Data_Brut=null;
  private Color[][] _LIST_Text_Color = new Color[15][1];
  private Color[][] _LIST_Fond_Color = null;
  private Color bleu = new Color(139, 141, 234);
  private String[] WSOLDE_Value = { "", "1", "2", "3", "4", };
  private String[] WSOLDE_Title = { "", "Tous", "Solde différent de zéro", "Débiteurs", "Créditeurs", };
  
  // private boolean constructionIsFinie = false;
  
  public VCGM05FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // _LIST_Title_Data_Brut = initTable(LIST, "Courier New", Font.BOLD, 12, "Courier New", Font.PLAIN, 12);
    
    // Ajout
    initDiverses();
    WSOLDE.setValeurs(WSOLDE_Value, WSOLDE_Title);
    
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, _LIST_Text_Color, _LIST_Fond_Color, null);
    TIDX1.setValeurs("1", "RB");
    TIDX2.setValeurs("2", "RB");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_87.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA1@")).trim());
    OBJ_88.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA2@")).trim());
    OBJ_89.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA3@")).trim());
    OBJ_90.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA4@")).trim());
    OBJ_91.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA5@")).trim());
    OBJ_92.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA6@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // Couleurs de la liste (suivant présence du texte "*Désact" sur la ligne)
    for (int i = 0; i < 15; i++) {
      
      if (lexique.HostFieldGetData("LD" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))).contains("*Désact")) {
        _LIST_Text_Color[i][0] = bleu;
      }
      else {
        _LIST_Text_Color[i][0] = Color.BLACK;
      }
      
    }
    
    // Ajoute à la liste des oData les variables non liée directement à un composant graphique
    // majTable( LIST, LIST.get_LIST_Title_Data_Brut(), _WTP01_Top, null, _LIST_Text_Color, _LIST_Fond_Color, null);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    // WSOLDE.setSelectedIndex(getIndice("WSOLDE", WSOLDE_Value));
    // INDETB.setVisible( lexique.isPresent("INDETB"));
    // NCGX.setVisible( lexique.isPresent("NCGX"));
    OBJ_61.setVisible(lexique.isTrue("92"));
    IN3ETB.setVisible(lexique.isTrue("56"));
    NC3X.setVisible(lexique.isTrue("56"));
    panel1.setVisible(lexique.isTrue("57"));
    panel3.setVisible(lexique.isTrue("57"));
    // SCAN2.setSelected(lexique.HostFieldGetData("SCAN2").equals("1"));
    
    // Titre
    // setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("Résultat de la recherche")));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    
    // Titre
    setTitle(p_bpresentation.getText());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("WSOLDE", 0, WSOLDE_Value[WSOLDE.getSelectedIndex()]);
    // if(SCAN2.isSelected())
    // lexique.HostFieldPutData("SCAN2", 0, "1");
    // else
    // lexique.HostFieldPutData("SCAN2", 0, " ");
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _WTP01_Top, "1", "Enter", e);
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void OBJ_62ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGUP", false);
  }
  
  private void OBJ_68ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGDOWN", false);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "1", "Enter");
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "2", "Enter");
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "4", "Enter");
    WTP01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "5", "Enter");
    WTP01.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_27ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "E", "Enter");
    WTP01.setValeurTop("E");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_29ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_28ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_32ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD2.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_33ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD2.getInvoker().getName());
  }
  
  private void OBJ_35ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD3.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44 = new JLabel();
    INDETB = new XRiTextField();
    IN3ETB = new XRiTextField();
    OBJ_45 = new JLabel();
    NCGX = new XRiTextField();
    NC3X = new XRiTextField();
    p_tete_droite = new JPanel();
    OBJ_61 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    TIDX2 = new XRiRadioButton();
    TIDX1 = new XRiRadioButton();
    ARG2 = new XRiTextField();
    ARG1 = new XRiTextField();
    SOLD1 = new JLabel();
    WSOLDE = new XRiComboBox();
    OBJ_72 = new JLabel();
    WSDEB = new XRiTextField();
    OBJ_74 = new JLabel();
    WSFIN = new XRiTextField();
    panel3 = new JPanel();
    OBJ_81 = new JLabel();
    OBJ_79 = new JLabel();
    WSANA1 = new XRiTextField();
    OBJ_86 = new JLabel();
    WCVAA1 = new XRiTextField();
    OBJ_84 = new JLabel();
    WNATA1 = new XRiTextField();
    WAXE1 = new XRiTextField();
    OBJ_87 = new JLabel();
    OBJ_88 = new JLabel();
    WAXE2 = new XRiTextField();
    WAXE3 = new XRiTextField();
    OBJ_89 = new JLabel();
    OBJ_90 = new JLabel();
    WAXE4 = new XRiTextField();
    WAXE5 = new XRiTextField();
    OBJ_91 = new JLabel();
    OBJ_92 = new JLabel();
    WAXE6 = new XRiTextField();
    separator1 = compFactory.createSeparator("Axes");
    separator2 = compFactory.createSeparator("Section / Nature");
    panel2 = new JPanel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_20 = new JMenuItem();
    OBJ_21 = new JMenuItem();
    OBJ_22 = new JMenuItem();
    OBJ_27 = new JMenuItem();
    OBJ_29 = new JMenuItem();
    OBJ_28 = new JMenuItem();
    BTD2 = new JPopupMenu();
    OBJ_32 = new JMenuItem();
    OBJ_33 = new JMenuItem();
    BTD3 = new JPopupMenu();
    OBJ_35 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Recherche d'un compte g\u00e9n\u00e9ral");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44 ----
          OBJ_44.setText("Soci\u00e9t\u00e9");
          OBJ_44.setName("OBJ_44");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD2);
          INDETB.setName("INDETB");

          //---- IN3ETB ----
          IN3ETB.setComponentPopupMenu(BTD2);
          IN3ETB.setToolTipText("Par duplication");
          IN3ETB.setName("IN3ETB");

          //---- OBJ_45 ----
          OBJ_45.setText("Num\u00e9ro de compte");
          OBJ_45.setName("OBJ_45");

          //---- NCGX ----
          NCGX.setComponentPopupMenu(BTD2);
          NCGX.setName("NCGX");

          //---- NC3X ----
          NC3X.setComponentPopupMenu(BTD2);
          NC3X.setToolTipText("Par duplication");
          NC3X.setName("NC3X");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(IN3ETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 121, GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addComponent(NCGX, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(NC3X, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_44))
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(IN3ETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(NCGX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(NC3X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- OBJ_61 ----
          OBJ_61.setText("Attention s\u00e9lection partielle (filtre)");
          OBJ_61.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_61.setForeground(new Color(190, 34, 34));
          OBJ_61.setFont(OBJ_61.getFont().deriveFont(OBJ_61.getFont().getStyle() | Font.BOLD));
          OBJ_61.setName("OBJ_61");
          p_tete_droite.add(OBJ_61);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(e -> bouton_validerActionPerformed(e));
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(e -> bouton_retourActionPerformed(e));
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Comptes d\u00e9sactiv\u00e9s");
              riSousMenu_bt6.setToolTipText("ON/OFF visualisation des comptes d\u00e9sactiv\u00e9s");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(e -> riSousMenu_bt6ActionPerformed(e));
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(950, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Recherche des comptes g\u00e9n\u00e9raux"));
            panel1.setAutoscrolls(true);
            panel1.setMaximumSize(new Dimension(466, 160));
            panel1.setMinimumSize(new Dimension(466, 160));
            panel1.setPreferredSize(new Dimension(466, 160));
            panel1.setOpaque(false);
            panel1.setName("panel1");

            //---- TIDX2 ----
            TIDX2.setText("Recherche alphab\u00e9tique");
            TIDX2.setToolTipText("Tri\u00e9 par");
            TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX2.setName("TIDX2");

            //---- TIDX1 ----
            TIDX1.setText("Num\u00e9ro de compte");
            TIDX1.setToolTipText("Tri\u00e9 par");
            TIDX1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX1.setName("TIDX1");

            //---- ARG2 ----
            ARG2.setComponentPopupMenu(BTD3);
            ARG2.setName("ARG2");

            //---- ARG1 ----
            ARG1.setComponentPopupMenu(BTD3);
            ARG1.setName("ARG1");

            //---- SOLD1 ----
            SOLD1.setText("Affichage du solde");
            SOLD1.setComponentPopupMenu(BTD);
            SOLD1.setName("SOLD1");

            //---- WSOLDE ----
            WSOLDE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WSOLDE.setName("WSOLDE");

            //---- OBJ_72 ----
            OBJ_72.setText("Solde compris entre");
            OBJ_72.setName("OBJ_72");

            //---- WSDEB ----
            WSDEB.setComponentPopupMenu(BTD3);
            WSDEB.setName("WSDEB");

            //---- OBJ_74 ----
            OBJ_74.setText("et");
            OBJ_74.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_74.setName("OBJ_74");

            //---- WSFIN ----
            WSFIN.setComponentPopupMenu(BTD3);
            WSFIN.setName("WSFIN");

            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(9, 9, 9)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(TIDX2, GroupLayout.PREFERRED_SIZE, 173, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(170, 170, 170)
                      .addComponent(ARG2, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(TIDX1, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(ARG1, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(SOLD1, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                      .addGap(20, 20, 20)
                      .addComponent(WSOLDE, GroupLayout.PREFERRED_SIZE, 162, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(OBJ_72, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
                      .addGap(45, 45, 45)
                      .addGroup(panel1Layout.createParallelGroup()
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(95, 95, 95)
                          .addComponent(OBJ_74, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                        .addComponent(WSDEB, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                      .addComponent(WSFIN, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)))
                  .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            );
            panel1Layout.setVerticalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(36, 36, 36)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(TIDX2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ARG2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(7, 7, 7)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(TIDX1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(ARG1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(12, 12, 12)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(SOLD1))
                    .addComponent(WSOLDE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(9, 9, 9)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(WSDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(WSFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addGroup(panel1Layout.createParallelGroup()
                        .addComponent(OBJ_72)
                        .addComponent(OBJ_74)))))
            );
          }

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder("Filtres si analytique"));
            panel3.setForeground(Color.darkGray);
            panel3.setPreferredSize(new Dimension(631, 145));
            panel3.setOpaque(false);
            panel3.setName("panel3");

            //---- OBJ_81 ----
            OBJ_81.setName("OBJ_81");

            //---- OBJ_79 ----
            OBJ_79.setText("Section");
            OBJ_79.setName("OBJ_79");

            //---- WSANA1 ----
            WSANA1.setComponentPopupMenu(BTD2);
            WSANA1.setName("WSANA1");

            //---- OBJ_86 ----
            OBJ_86.setText("ou cl\u00e9");
            OBJ_86.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_86.setName("OBJ_86");

            //---- WCVAA1 ----
            WCVAA1.setComponentPopupMenu(BTD2);
            WCVAA1.setName("WCVAA1");

            //---- OBJ_84 ----
            OBJ_84.setText("Nature");
            OBJ_84.setName("OBJ_84");

            //---- WNATA1 ----
            WNATA1.setComponentPopupMenu(BTD2);
            WNATA1.setName("WNATA1");

            //---- WAXE1 ----
            WAXE1.setComponentPopupMenu(BTD2);
            WAXE1.setName("WAXE1");

            //---- OBJ_87 ----
            OBJ_87.setText("@LIBAA1@");
            OBJ_87.setForeground(Color.darkGray);
            OBJ_87.setName("OBJ_87");

            //---- OBJ_88 ----
            OBJ_88.setText("@LIBAA2@");
            OBJ_88.setForeground(Color.darkGray);
            OBJ_88.setName("OBJ_88");

            //---- WAXE2 ----
            WAXE2.setComponentPopupMenu(BTD2);
            WAXE2.setName("WAXE2");

            //---- WAXE3 ----
            WAXE3.setComponentPopupMenu(BTD2);
            WAXE3.setName("WAXE3");

            //---- OBJ_89 ----
            OBJ_89.setText("@LIBAA3@");
            OBJ_89.setForeground(Color.darkGray);
            OBJ_89.setName("OBJ_89");

            //---- OBJ_90 ----
            OBJ_90.setText("@LIBAA4@");
            OBJ_90.setForeground(Color.darkGray);
            OBJ_90.setName("OBJ_90");

            //---- WAXE4 ----
            WAXE4.setComponentPopupMenu(BTD2);
            WAXE4.setName("WAXE4");

            //---- WAXE5 ----
            WAXE5.setComponentPopupMenu(BTD2);
            WAXE5.setName("WAXE5");

            //---- OBJ_91 ----
            OBJ_91.setText("@LIBAA5@");
            OBJ_91.setForeground(Color.darkGray);
            OBJ_91.setName("OBJ_91");

            //---- OBJ_92 ----
            OBJ_92.setForeground(Color.darkGray);
            OBJ_92.setText("@LIBAA6@");
            OBJ_92.setName("OBJ_92");

            //---- WAXE6 ----
            WAXE6.setComponentPopupMenu(BTD2);
            WAXE6.setName("WAXE6");

            //---- separator1 ----
            separator1.setName("separator1");

            //---- separator2 ----
            separator2.setName("separator2");

            GroupLayout panel3Layout = new GroupLayout(panel3);
            panel3.setLayout(panel3Layout);
            panel3Layout.setHorizontalGroup(
              panel3Layout.createParallelGroup()
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(11, 11, 11)
                  .addComponent(separator2, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(31, 31, 31)
                  .addComponent(OBJ_79, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(WSANA1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addGap(14, 14, 14)
                  .addComponent(OBJ_86, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(WCVAA1, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addGap(14, 14, 14)
                  .addComponent(OBJ_84, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(WNATA1, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(11, 11, 11)
                  .addComponent(separator1, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(82, 82, 82)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addComponent(WAXE1, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_87, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
                  .addGap(45, 45, 45)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addComponent(OBJ_88, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                    .addComponent(WAXE2, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
                  .addGap(40, 40, 40)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addComponent(WAXE3, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_89, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)))
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(82, 82, 82)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addComponent(OBJ_90, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                    .addComponent(WAXE4, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
                  .addGap(45, 45, 45)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addComponent(WAXE5, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_91, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
                  .addGap(40, 40, 40)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addComponent(OBJ_92, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                    .addComponent(WAXE6, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)))
            );
            panel3Layout.setVerticalGroup(
              panel3Layout.createParallelGroup()
                .addGroup(panel3Layout.createSequentialGroup()
                  .addComponent(separator2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(16, 16, 16)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addComponent(WSANA1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(WCVAA1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(WNATA1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addGroup(panel3Layout.createParallelGroup()
                        .addComponent(OBJ_79)
                        .addComponent(OBJ_86)
                        .addComponent(OBJ_84))))
                  .addGap(12, 12, 12)
                  .addComponent(separator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(6, 6, 6)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addComponent(OBJ_87)
                    .addComponent(OBJ_88)
                    .addComponent(OBJ_89)
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(15, 15, 15)
                      .addGroup(panel3Layout.createParallelGroup()
                        .addComponent(WAXE1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WAXE2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WAXE3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGap(7, 7, 7)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addComponent(OBJ_90)
                    .addComponent(OBJ_91)
                    .addComponent(OBJ_92)
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(15, 15, 15)
                      .addGroup(panel3Layout.createParallelGroup()
                        .addComponent(WAXE4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WAXE5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WAXE6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))))
            );
          }

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            BT_PGUP.addActionListener(e -> OBJ_62ActionPerformed(e));
            panel2.add(BT_PGUP);
            BT_PGUP.setBounds(885, 20, 25, 125);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            BT_PGDOWN.addActionListener(e -> OBJ_68ActionPerformed(e));
            panel2.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(885, 165, 25, 125);

            //======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST.setViewportView(WTP01);
            }
            panel2.add(SCROLLPANE_LIST);
            SCROLLPANE_LIST.setBounds(10, 20, 865, 270);

            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap(16, Short.MAX_VALUE)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 442, GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 460, GroupLayout.PREFERRED_SIZE))
                  .addComponent(panel2, GroupLayout.Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 920, GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                  .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 235, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 235, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(e -> CHOISIRActionPerformed(e));
      BTD.add(CHOISIR);

      //---- OBJ_20 ----
      OBJ_20.setText("Modifier");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(e -> OBJ_20ActionPerformed(e));
      BTD.add(OBJ_20);

      //---- OBJ_21 ----
      OBJ_21.setText("Annuler");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(e -> OBJ_21ActionPerformed(e));
      BTD.add(OBJ_21);

      //---- OBJ_22 ----
      OBJ_22.setText("Interroger");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(e -> OBJ_22ActionPerformed(e));
      BTD.add(OBJ_22);

      //---- OBJ_27 ----
      OBJ_27.setText("Affichage des \u00e9critures");
      OBJ_27.setName("OBJ_27");
      OBJ_27.addActionListener(e -> OBJ_27ActionPerformed(e));
      BTD.add(OBJ_27);
      BTD.addSeparator();

      //---- OBJ_29 ----
      OBJ_29.setText("Choix possibles");
      OBJ_29.setName("OBJ_29");
      OBJ_29.addActionListener(e -> OBJ_29ActionPerformed(e));
      BTD.add(OBJ_29);

      //---- OBJ_28 ----
      OBJ_28.setText("Aide en ligne");
      OBJ_28.setName("OBJ_28");
      OBJ_28.addActionListener(e -> OBJ_28ActionPerformed(e));
      BTD.add(OBJ_28);
    }

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- OBJ_32 ----
      OBJ_32.setText("Choix possibles");
      OBJ_32.setName("OBJ_32");
      OBJ_32.addActionListener(e -> OBJ_32ActionPerformed(e));
      BTD2.add(OBJ_32);

      //---- OBJ_33 ----
      OBJ_33.setText("Aide en ligne");
      OBJ_33.setName("OBJ_33");
      OBJ_33.addActionListener(e -> OBJ_33ActionPerformed(e));
      BTD2.add(OBJ_33);
    }

    //======== BTD3 ========
    {
      BTD3.setName("BTD3");

      //---- OBJ_35 ----
      OBJ_35.setText("Aide en ligne");
      OBJ_35.setName("OBJ_35");
      OBJ_35.addActionListener(e -> OBJ_35ActionPerformed(e));
      BTD3.add(OBJ_35);
    }

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(TIDX2);
    RB_GRP.add(TIDX1);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44;
  private XRiTextField INDETB;
  private XRiTextField IN3ETB;
  private JLabel OBJ_45;
  private XRiTextField NCGX;
  private XRiTextField NC3X;
  private JPanel p_tete_droite;
  private JLabel OBJ_61;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiRadioButton TIDX2;
  private XRiRadioButton TIDX1;
  private XRiTextField ARG2;
  private XRiTextField ARG1;
  private JLabel SOLD1;
  private XRiComboBox WSOLDE;
  private JLabel OBJ_72;
  private XRiTextField WSDEB;
  private JLabel OBJ_74;
  private XRiTextField WSFIN;
  private JPanel panel3;
  private JLabel OBJ_81;
  private JLabel OBJ_79;
  private XRiTextField WSANA1;
  private JLabel OBJ_86;
  private XRiTextField WCVAA1;
  private JLabel OBJ_84;
  private XRiTextField WNATA1;
  private XRiTextField WAXE1;
  private JLabel OBJ_87;
  private JLabel OBJ_88;
  private XRiTextField WAXE2;
  private XRiTextField WAXE3;
  private JLabel OBJ_89;
  private JLabel OBJ_90;
  private XRiTextField WAXE4;
  private XRiTextField WAXE5;
  private JLabel OBJ_91;
  private JLabel OBJ_92;
  private XRiTextField WAXE6;
  private JComponent separator1;
  private JComponent separator2;
  private JPanel panel2;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_27;
  private JMenuItem OBJ_29;
  private JMenuItem OBJ_28;
  private JPopupMenu BTD2;
  private JMenuItem OBJ_32;
  private JMenuItem OBJ_33;
  private JPopupMenu BTD3;
  private JMenuItem OBJ_35;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
