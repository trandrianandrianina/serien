
package ri.serien.libecranrpg.vcgm.VCGM01FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.LayoutStyle;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM01FM_AC extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] ACPUR_Value = { "", "P", "R", };
  private String[] ACPUR_Title = { "", "Purge définitive", "Purge antériorité", };
  
  public VCGM01FM_AC(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    ACPUR.setValeurs(ACPUR_Value, ACPUR_Title);
    ACNAT.setValeursSelection("O", "N");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    // ACNAT.setSelected(lexique.HostFieldGetData("ACNAT").equalsIgnoreCase("O"));
    // ACPUR.setSelectedIndex(getIndice("ACPUR", ACPUR_Value));
    
    

    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (ACNAT.isSelected())
    // lexique.HostFieldPutData("ACNAT", 0, "O");
    // else
    // lexique.HostFieldPutData("ACNAT", 0, "N");
    // lexique.HostFieldPutData("ACPUR", 0, ACPUR_Value[ACPUR.getSelectedIndex()]);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44_OBJ_45 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_45_OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46_OBJ_47 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_44_OBJ_44 = new JLabel();
    ACLIB = new XRiTextField();
    ACLI2 = new XRiTextField();
    xTitledPanel2 = new JXTitledPanel();
    ACPUR = new XRiComboBox();
    ACNAT = new XRiCheckBox();
    OBJ_80_OBJ_80 = new JLabel();
    OBJ_58_OBJ_58 = new JLabel();
    OBJ_59_OBJ_59 = new JLabel();
    OBJ_60_OBJ_60 = new JLabel();
    OBJ_61_OBJ_61 = new JLabel();
    ACTOT1 = new XRiTextField();
    ACTOT2 = new XRiTextField();
    ACMAR1 = new XRiTextField();
    ACTOT3 = new XRiTextField();
    ACVAL1 = new XRiTextField();
    ACVAL4 = new XRiTextField();
    ACMAR2 = new XRiTextField();
    ACMAR3 = new XRiTextField();
    ACVAL2 = new XRiTextField();
    ACVAL3 = new XRiTextField();
    ACVAL5 = new XRiTextField();
    ACVAL6 = new XRiTextField();
    OBJ_72_OBJ_72 = new JLabel();
    ACDEBX = new XRiTextField();
    ACFINX = new XRiTextField();
    ACACR = new XRiTextField();
    OBJ_77_OBJ_77 = new JLabel();
    OBJ_70_OBJ_70 = new JLabel();
    OBJ_62_OBJ_62 = new JLabel();
    OBJ_73_OBJ_73 = new JLabel();
    OBJ_71_OBJ_71 = new JLabel();
    OBJ_74_OBJ_74 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44_OBJ_45 ----
          OBJ_44_OBJ_45.setText("Soci\u00e9t\u00e9");
          OBJ_44_OBJ_45.setName("OBJ_44_OBJ_45");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setName("INDETB");

          //---- OBJ_45_OBJ_46 ----
          OBJ_45_OBJ_46.setText("Code");
          OBJ_45_OBJ_46.setName("OBJ_45_OBJ_46");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setName("INDTYP");

          //---- OBJ_46_OBJ_47 ----
          OBJ_46_OBJ_47.setText("Ordre");
          OBJ_46_OBJ_47.setName("OBJ_46_OBJ_47");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(600, 460));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Code affaires");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

            //---- OBJ_44_OBJ_44 ----
            OBJ_44_OBJ_44.setText("Libell\u00e9");
            OBJ_44_OBJ_44.setName("OBJ_44_OBJ_44");

            //---- ACLIB ----
            ACLIB.setComponentPopupMenu(BTD);
            ACLIB.setName("ACLIB");

            //---- ACLI2 ----
            ACLI2.setComponentPopupMenu(BTD);
            ACLI2.setName("ACLI2");

            GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
            xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
            xTitledPanel1ContentContainerLayout.setHorizontalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(38, 38, 38)
                      .addComponent(OBJ_44_OBJ_44, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(ACLIB, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(100, 100, 100)
                      .addComponent(ACLI2, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)))
                  .addContainerGap(159, Short.MAX_VALUE))
            );
            xTitledPanel1ContentContainerLayout.setVerticalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(13, 13, 13)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_44_OBJ_44, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ACLIB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(4, 4, 4)
                  .addComponent(ACLI2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addContainerGap(13, Short.MAX_VALUE))
            );
          }

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setTitle("Budgets (Montants)");
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();

            //---- ACPUR ----
            ACPUR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ACPUR.setName("ACPUR");

            //---- ACNAT ----
            ACNAT.setText("Nature obligatoire");
            ACNAT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ACNAT.setName("ACNAT");

            //---- OBJ_80_OBJ_80 ----
            OBJ_80_OBJ_80.setText("Affaire de r\u00e9f\u00e9rence");
            OBJ_80_OBJ_80.setName("OBJ_80_OBJ_80");

            //---- OBJ_58_OBJ_58 ----
            OBJ_58_OBJ_58.setText("Main d'oeuvre");
            OBJ_58_OBJ_58.setName("OBJ_58_OBJ_58");

            //---- OBJ_59_OBJ_59 ----
            OBJ_59_OBJ_59.setText("Marchandises");
            OBJ_59_OBJ_59.setName("OBJ_59_OBJ_59");

            //---- OBJ_60_OBJ_60 ----
            OBJ_60_OBJ_60.setText("Divers");
            OBJ_60_OBJ_60.setName("OBJ_60_OBJ_60");

            //---- OBJ_61_OBJ_61 ----
            OBJ_61_OBJ_61.setText("Total");
            OBJ_61_OBJ_61.setName("OBJ_61_OBJ_61");

            //---- ACTOT1 ----
            ACTOT1.setComponentPopupMenu(BTD);
            ACTOT1.setName("ACTOT1");

            //---- ACTOT2 ----
            ACTOT2.setComponentPopupMenu(BTD);
            ACTOT2.setName("ACTOT2");

            //---- ACMAR1 ----
            ACMAR1.setComponentPopupMenu(BTD);
            ACMAR1.setName("ACMAR1");

            //---- ACTOT3 ----
            ACTOT3.setComponentPopupMenu(BTD);
            ACTOT3.setName("ACTOT3");

            //---- ACVAL1 ----
            ACVAL1.setComponentPopupMenu(BTD);
            ACVAL1.setName("ACVAL1");

            //---- ACVAL4 ----
            ACVAL4.setComponentPopupMenu(BTD);
            ACVAL4.setName("ACVAL4");

            //---- ACMAR2 ----
            ACMAR2.setComponentPopupMenu(BTD);
            ACMAR2.setName("ACMAR2");

            //---- ACMAR3 ----
            ACMAR3.setComponentPopupMenu(BTD);
            ACMAR3.setName("ACMAR3");

            //---- ACVAL2 ----
            ACVAL2.setComponentPopupMenu(BTD);
            ACVAL2.setName("ACVAL2");

            //---- ACVAL3 ----
            ACVAL3.setComponentPopupMenu(BTD);
            ACVAL3.setName("ACVAL3");

            //---- ACVAL5 ----
            ACVAL5.setComponentPopupMenu(BTD);
            ACVAL5.setName("ACVAL5");

            //---- ACVAL6 ----
            ACVAL6.setComponentPopupMenu(BTD);
            ACVAL6.setName("ACVAL6");

            //---- OBJ_72_OBJ_72 ----
            OBJ_72_OBJ_72.setText("Dates");
            OBJ_72_OBJ_72.setName("OBJ_72_OBJ_72");

            //---- ACDEBX ----
            ACDEBX.setComponentPopupMenu(BTD);
            ACDEBX.setName("ACDEBX");

            //---- ACFINX ----
            ACFINX.setComponentPopupMenu(BTD);
            ACFINX.setName("ACFINX");

            //---- ACACR ----
            ACACR.setComponentPopupMenu(BTD);
            ACACR.setName("ACACR");

            //---- OBJ_77_OBJ_77 ----
            OBJ_77_OBJ_77.setText("(JJMMAA)");
            OBJ_77_OBJ_77.setName("OBJ_77_OBJ_77");

            //---- OBJ_70_OBJ_70 ----
            OBJ_70_OBJ_70.setText("Charges");
            OBJ_70_OBJ_70.setName("OBJ_70_OBJ_70");

            //---- OBJ_62_OBJ_62 ----
            OBJ_62_OBJ_62.setText("Produits");
            OBJ_62_OBJ_62.setName("OBJ_62_OBJ_62");

            //---- OBJ_73_OBJ_73 ----
            OBJ_73_OBJ_73.setText("D\u00e9but");
            OBJ_73_OBJ_73.setName("OBJ_73_OBJ_73");

            //---- OBJ_71_OBJ_71 ----
            OBJ_71_OBJ_71.setText("Marge");
            OBJ_71_OBJ_71.setName("OBJ_71_OBJ_71");

            //---- OBJ_74_OBJ_74 ----
            OBJ_74_OBJ_74.setText("Fin");
            OBJ_74_OBJ_74.setName("OBJ_74_OBJ_74");

            GroupLayout xTitledPanel2ContentContainerLayout = new GroupLayout(xTitledPanel2ContentContainer);
            xTitledPanel2ContentContainer.setLayout(xTitledPanel2ContentContainerLayout);
            xTitledPanel2ContentContainerLayout.setHorizontalGroup(
              xTitledPanel2ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(120, 120, 120)
                      .addComponent(OBJ_58_OBJ_58, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
                      .addGap(13, 13, 13)
                      .addComponent(OBJ_59_OBJ_59, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(OBJ_60_OBJ_60, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(OBJ_61_OBJ_61, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(OBJ_62_OBJ_62, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                      .addGap(18, 18, 18)
                      .addComponent(ACVAL1, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                      .addGap(12, 12, 12)
                      .addComponent(ACVAL2, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                      .addGap(9, 9, 9)
                      .addComponent(ACVAL3, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                      .addGap(9, 9, 9)
                      .addComponent(ACTOT1, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(OBJ_70_OBJ_70, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
                      .addGap(16, 16, 16)
                      .addComponent(ACVAL4, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                      .addGap(12, 12, 12)
                      .addComponent(ACVAL5, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                      .addGap(9, 9, 9)
                      .addComponent(ACVAL6, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                      .addGap(9, 9, 9)
                      .addComponent(ACTOT2, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(OBJ_71_OBJ_71, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
                      .addGap(28, 28, 28)
                      .addComponent(ACMAR1, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                      .addGap(12, 12, 12)
                      .addComponent(ACMAR2, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                      .addGap(9, 9, 9)
                      .addComponent(ACMAR3, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                      .addGap(9, 9, 9)
                      .addComponent(ACTOT3, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(OBJ_72_OBJ_72, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                      .addGap(12, 12, 12)
                      .addComponent(OBJ_73_OBJ_73, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                      .addGap(58, 58, 58)
                      .addComponent(OBJ_74_OBJ_74, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(OBJ_77_OBJ_77, GroupLayout.PREFERRED_SIZE, 61, GroupLayout.PREFERRED_SIZE)
                      .addGap(9, 9, 9)
                      .addComponent(ACDEBX, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addGap(37, 37, 37)
                      .addComponent(ACFINX, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addGap(34, 34, 34)
                      .addComponent(ACPUR, GroupLayout.PREFERRED_SIZE, 179, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(OBJ_80_OBJ_80, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                      .addGap(47, 47, 47)
                      .addComponent(ACACR, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addGap(34, 34, 34)
                      .addComponent(ACNAT, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)))
                  .addContainerGap(79, Short.MAX_VALUE))
            );
            xTitledPanel2ContentContainerLayout.setVerticalGroup(
              xTitledPanel2ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(26, 26, 26)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_58_OBJ_58, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_59_OBJ_59, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_60_OBJ_60, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_61_OBJ_61, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(ACVAL1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_62_OBJ_62, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ACVAL2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ACVAL3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ACTOT1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGap(3, 3, 3)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_70_OBJ_70, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                    .addComponent(ACVAL4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ACVAL5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ACVAL6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ACTOT2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(4, 4, 4)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_71_OBJ_71, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                    .addComponent(ACMAR1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ACMAR2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ACMAR3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ACTOT3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(23, 23, 23)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_72_OBJ_72, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_73_OBJ_73, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_74_OBJ_74, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_77_OBJ_77))
                    .addComponent(ACDEBX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ACFINX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(ACPUR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(7, 7, 7)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(ACACR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_80_OBJ_80, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ACNAT, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
                  .addContainerGap(38, Short.MAX_VALUE))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                  .addComponent(xTitledPanel2, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(xTitledPanel1, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(xTitledPanel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44_OBJ_45;
  private XRiTextField INDETB;
  private JLabel OBJ_45_OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_46_OBJ_47;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_44_OBJ_44;
  private XRiTextField ACLIB;
  private XRiTextField ACLI2;
  private JXTitledPanel xTitledPanel2;
  private XRiComboBox ACPUR;
  private XRiCheckBox ACNAT;
  private JLabel OBJ_80_OBJ_80;
  private JLabel OBJ_58_OBJ_58;
  private JLabel OBJ_59_OBJ_59;
  private JLabel OBJ_60_OBJ_60;
  private JLabel OBJ_61_OBJ_61;
  private XRiTextField ACTOT1;
  private XRiTextField ACTOT2;
  private XRiTextField ACMAR1;
  private XRiTextField ACTOT3;
  private XRiTextField ACVAL1;
  private XRiTextField ACVAL4;
  private XRiTextField ACMAR2;
  private XRiTextField ACMAR3;
  private XRiTextField ACVAL2;
  private XRiTextField ACVAL3;
  private XRiTextField ACVAL5;
  private XRiTextField ACVAL6;
  private JLabel OBJ_72_OBJ_72;
  private XRiTextField ACDEBX;
  private XRiTextField ACFINX;
  private XRiTextField ACACR;
  private JLabel OBJ_77_OBJ_77;
  private JLabel OBJ_70_OBJ_70;
  private JLabel OBJ_62_OBJ_62;
  private JLabel OBJ_73_OBJ_73;
  private JLabel OBJ_71_OBJ_71;
  private JLabel OBJ_74_OBJ_74;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
