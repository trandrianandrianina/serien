
package ri.serien.libecranrpg.vcgm.VCGM01FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM01FM_P1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM01FM_P1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    

    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44_OBJ_45 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_45_OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46_OBJ_47 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    P1X01 = new XRiTextField();
    P1X02 = new XRiTextField();
    P1X03 = new XRiTextField();
    P1X04 = new XRiTextField();
    P1X05 = new XRiTextField();
    P1X06 = new XRiTextField();
    P1X07 = new XRiTextField();
    P1X08 = new XRiTextField();
    P1X09 = new XRiTextField();
    P1X10 = new XRiTextField();
    P1X11 = new XRiTextField();
    P1X12 = new XRiTextField();
    P1X13 = new XRiTextField();
    P1X14 = new XRiTextField();
    P1X15 = new XRiTextField();
    P1X16 = new XRiTextField();
    P1X17 = new XRiTextField();
    P1X18 = new XRiTextField();
    P1X19 = new XRiTextField();
    P1X20 = new XRiTextField();
    P1X21 = new XRiTextField();
    P1X22 = new XRiTextField();
    P1X23 = new XRiTextField();
    P1X24 = new XRiTextField();
    P1X25 = new XRiTextField();
    OBJ_76_OBJ_76 = new JLabel();
    OBJ_87_OBJ_87 = new JLabel();
    OBJ_88_OBJ_88 = new JLabel();
    OBJ_89_OBJ_89 = new JLabel();
    OBJ_90_OBJ_90 = new JLabel();
    OBJ_91_OBJ_91 = new JLabel();
    OBJ_92_OBJ_92 = new JLabel();
    OBJ_93_OBJ_93 = new JLabel();
    OBJ_94_OBJ_94 = new JLabel();
    OBJ_95_OBJ_95 = new JLabel();
    OBJ_96_OBJ_96 = new JLabel();
    OBJ_97_OBJ_97 = new JLabel();
    OBJ_98_OBJ_98 = new JLabel();
    OBJ_99_OBJ_99 = new JLabel();
    OBJ_100_OBJ_100 = new JLabel();
    OBJ_101_OBJ_101 = new JLabel();
    OBJ_102_OBJ_102 = new JLabel();
    OBJ_103_OBJ_103 = new JLabel();
    OBJ_104_OBJ_104 = new JLabel();
    OBJ_105_OBJ_105 = new JLabel();
    OBJ_106_OBJ_106 = new JLabel();
    OBJ_107_OBJ_107 = new JLabel();
    OBJ_108_OBJ_108 = new JLabel();
    OBJ_109_OBJ_109 = new JLabel();
    OBJ_110_OBJ_110 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44_OBJ_45 ----
          OBJ_44_OBJ_45.setText("Soci\u00e9t\u00e9");
          OBJ_44_OBJ_45.setName("OBJ_44_OBJ_45");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setName("INDETB");

          //---- OBJ_45_OBJ_46 ----
          OBJ_45_OBJ_46.setText("Code");
          OBJ_45_OBJ_46.setName("OBJ_45_OBJ_46");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setName("INDTYP");

          //---- OBJ_46_OBJ_47 ----
          OBJ_46_OBJ_47.setText("Ordre");
          OBJ_46_OBJ_47.setName("OBJ_46_OBJ_47");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(450, 450));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Table de constantes");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

            //---- P1X01 ----
            P1X01.setName("P1X01");

            //---- P1X02 ----
            P1X02.setName("P1X02");

            //---- P1X03 ----
            P1X03.setName("P1X03");

            //---- P1X04 ----
            P1X04.setName("P1X04");

            //---- P1X05 ----
            P1X05.setName("P1X05");

            //---- P1X06 ----
            P1X06.setName("P1X06");

            //---- P1X07 ----
            P1X07.setName("P1X07");

            //---- P1X08 ----
            P1X08.setName("P1X08");

            //---- P1X09 ----
            P1X09.setName("P1X09");

            //---- P1X10 ----
            P1X10.setName("P1X10");

            //---- P1X11 ----
            P1X11.setName("P1X11");

            //---- P1X12 ----
            P1X12.setName("P1X12");

            //---- P1X13 ----
            P1X13.setName("P1X13");

            //---- P1X14 ----
            P1X14.setName("P1X14");

            //---- P1X15 ----
            P1X15.setName("P1X15");

            //---- P1X16 ----
            P1X16.setName("P1X16");

            //---- P1X17 ----
            P1X17.setName("P1X17");

            //---- P1X18 ----
            P1X18.setName("P1X18");

            //---- P1X19 ----
            P1X19.setName("P1X19");

            //---- P1X20 ----
            P1X20.setName("P1X20");

            //---- P1X21 ----
            P1X21.setName("P1X21");

            //---- P1X22 ----
            P1X22.setName("P1X22");

            //---- P1X23 ----
            P1X23.setName("P1X23");

            //---- P1X24 ----
            P1X24.setName("P1X24");

            //---- P1X25 ----
            P1X25.setName("P1X25");

            //---- OBJ_76_OBJ_76 ----
            OBJ_76_OBJ_76.setText("01");
            OBJ_76_OBJ_76.setName("OBJ_76_OBJ_76");

            //---- OBJ_87_OBJ_87 ----
            OBJ_87_OBJ_87.setText("06");
            OBJ_87_OBJ_87.setName("OBJ_87_OBJ_87");

            //---- OBJ_88_OBJ_88 ----
            OBJ_88_OBJ_88.setText("11");
            OBJ_88_OBJ_88.setName("OBJ_88_OBJ_88");

            //---- OBJ_89_OBJ_89 ----
            OBJ_89_OBJ_89.setText("16");
            OBJ_89_OBJ_89.setName("OBJ_89_OBJ_89");

            //---- OBJ_90_OBJ_90 ----
            OBJ_90_OBJ_90.setText("21");
            OBJ_90_OBJ_90.setName("OBJ_90_OBJ_90");

            //---- OBJ_91_OBJ_91 ----
            OBJ_91_OBJ_91.setText("02");
            OBJ_91_OBJ_91.setName("OBJ_91_OBJ_91");

            //---- OBJ_92_OBJ_92 ----
            OBJ_92_OBJ_92.setText("03");
            OBJ_92_OBJ_92.setName("OBJ_92_OBJ_92");

            //---- OBJ_93_OBJ_93 ----
            OBJ_93_OBJ_93.setText("04");
            OBJ_93_OBJ_93.setName("OBJ_93_OBJ_93");

            //---- OBJ_94_OBJ_94 ----
            OBJ_94_OBJ_94.setText("05");
            OBJ_94_OBJ_94.setName("OBJ_94_OBJ_94");

            //---- OBJ_95_OBJ_95 ----
            OBJ_95_OBJ_95.setText("07");
            OBJ_95_OBJ_95.setName("OBJ_95_OBJ_95");

            //---- OBJ_96_OBJ_96 ----
            OBJ_96_OBJ_96.setText("08");
            OBJ_96_OBJ_96.setName("OBJ_96_OBJ_96");

            //---- OBJ_97_OBJ_97 ----
            OBJ_97_OBJ_97.setText("09");
            OBJ_97_OBJ_97.setName("OBJ_97_OBJ_97");

            //---- OBJ_98_OBJ_98 ----
            OBJ_98_OBJ_98.setText("10");
            OBJ_98_OBJ_98.setName("OBJ_98_OBJ_98");

            //---- OBJ_99_OBJ_99 ----
            OBJ_99_OBJ_99.setText("12");
            OBJ_99_OBJ_99.setName("OBJ_99_OBJ_99");

            //---- OBJ_100_OBJ_100 ----
            OBJ_100_OBJ_100.setText("13");
            OBJ_100_OBJ_100.setName("OBJ_100_OBJ_100");

            //---- OBJ_101_OBJ_101 ----
            OBJ_101_OBJ_101.setText("14");
            OBJ_101_OBJ_101.setName("OBJ_101_OBJ_101");

            //---- OBJ_102_OBJ_102 ----
            OBJ_102_OBJ_102.setText("15");
            OBJ_102_OBJ_102.setName("OBJ_102_OBJ_102");

            //---- OBJ_103_OBJ_103 ----
            OBJ_103_OBJ_103.setText("17");
            OBJ_103_OBJ_103.setName("OBJ_103_OBJ_103");

            //---- OBJ_104_OBJ_104 ----
            OBJ_104_OBJ_104.setText("18");
            OBJ_104_OBJ_104.setName("OBJ_104_OBJ_104");

            //---- OBJ_105_OBJ_105 ----
            OBJ_105_OBJ_105.setText("19");
            OBJ_105_OBJ_105.setName("OBJ_105_OBJ_105");

            //---- OBJ_106_OBJ_106 ----
            OBJ_106_OBJ_106.setText("20");
            OBJ_106_OBJ_106.setName("OBJ_106_OBJ_106");

            //---- OBJ_107_OBJ_107 ----
            OBJ_107_OBJ_107.setText("22");
            OBJ_107_OBJ_107.setName("OBJ_107_OBJ_107");

            //---- OBJ_108_OBJ_108 ----
            OBJ_108_OBJ_108.setText("23");
            OBJ_108_OBJ_108.setName("OBJ_108_OBJ_108");

            //---- OBJ_109_OBJ_109 ----
            OBJ_109_OBJ_109.setText("24");
            OBJ_109_OBJ_109.setName("OBJ_109_OBJ_109");

            //---- OBJ_110_OBJ_110 ----
            OBJ_110_OBJ_110.setText("25");
            OBJ_110_OBJ_110.setName("OBJ_110_OBJ_110");

            GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
            xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
            xTitledPanel1ContentContainerLayout.setHorizontalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_76_OBJ_76, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                      .addGap(61, 61, 61)
                      .addComponent(OBJ_91_OBJ_91, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                      .addGap(61, 61, 61)
                      .addComponent(OBJ_92_OBJ_92, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                      .addGap(61, 61, 61)
                      .addComponent(OBJ_93_OBJ_93, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                      .addGap(61, 61, 61)
                      .addComponent(OBJ_94_OBJ_94, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(P1X01, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                      .addGap(18, 18, 18)
                      .addComponent(P1X02, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                      .addGap(18, 18, 18)
                      .addComponent(P1X03, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                      .addGap(18, 18, 18)
                      .addComponent(P1X04, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                      .addGap(18, 18, 18)
                      .addComponent(P1X05, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_87_OBJ_87, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                      .addGap(61, 61, 61)
                      .addComponent(OBJ_95_OBJ_95, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                      .addGap(61, 61, 61)
                      .addComponent(OBJ_96_OBJ_96, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                      .addGap(61, 61, 61)
                      .addComponent(OBJ_97_OBJ_97, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                      .addGap(61, 61, 61)
                      .addComponent(OBJ_98_OBJ_98, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(P1X06, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                      .addGap(18, 18, 18)
                      .addComponent(P1X07, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                      .addGap(18, 18, 18)
                      .addComponent(P1X08, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                      .addGap(18, 18, 18)
                      .addComponent(P1X09, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                      .addGap(18, 18, 18)
                      .addComponent(P1X10, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(P1X11, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_88_OBJ_88, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
                      .addGap(18, 18, 18)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_99_OBJ_99, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                        .addComponent(P1X12, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
                      .addGap(18, 18, 18)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_100_OBJ_100, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                        .addComponent(P1X13, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
                      .addGap(18, 18, 18)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(P1X14, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_101_OBJ_101, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
                      .addGap(18, 18, 18)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(P1X15, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_102_OBJ_102, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_89_OBJ_89, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                      .addGap(61, 61, 61)
                      .addComponent(OBJ_103_OBJ_103, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                      .addGap(61, 61, 61)
                      .addComponent(OBJ_104_OBJ_104, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                      .addGap(61, 61, 61)
                      .addComponent(OBJ_105_OBJ_105, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                      .addGap(61, 61, 61)
                      .addComponent(OBJ_106_OBJ_106, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(P1X16, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                      .addGap(18, 18, 18)
                      .addComponent(P1X17, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                      .addGap(18, 18, 18)
                      .addComponent(P1X18, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                      .addGap(18, 18, 18)
                      .addComponent(P1X19, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                      .addGap(18, 18, 18)
                      .addComponent(P1X20, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_90_OBJ_90, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                      .addGap(61, 61, 61)
                      .addComponent(OBJ_107_OBJ_107, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                      .addGap(61, 61, 61)
                      .addComponent(OBJ_108_OBJ_108, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                      .addGap(61, 61, 61)
                      .addComponent(OBJ_109_OBJ_109, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                      .addGap(61, 61, 61)
                      .addComponent(OBJ_110_OBJ_110, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(P1X21, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                      .addGap(18, 18, 18)
                      .addComponent(P1X22, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                      .addGap(18, 18, 18)
                      .addComponent(P1X23, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                      .addGap(18, 18, 18)
                      .addComponent(P1X24, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                      .addGap(18, 18, 18)
                      .addComponent(P1X25, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)))
                  .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            );
            xTitledPanel1ContentContainerLayout.setVerticalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(11, 11, 11)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_76_OBJ_76, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_91_OBJ_91, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_92_OBJ_92, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_93_OBJ_93, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_94_OBJ_94, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(P1X01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P1X02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P1X03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P1X04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P1X05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(26, 26, 26)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_87_OBJ_87, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_95_OBJ_95, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_96_OBJ_96, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_97_OBJ_97, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_98_OBJ_98, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(P1X06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P1X07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P1X08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P1X09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P1X10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(27, 27, 27)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_88_OBJ_88, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_99_OBJ_99, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_100_OBJ_100, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_101_OBJ_101, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_102_OBJ_102, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(19, 19, 19)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(P1X11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(P1X12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(P1X13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(P1X14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(P1X15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGap(23, 23, 23)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_89_OBJ_89, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_103_OBJ_103, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_104_OBJ_104, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_105_OBJ_105, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_106_OBJ_106, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGap(3, 3, 3)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(P1X16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P1X17, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P1X18, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P1X19, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P1X20, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(24, 24, 24)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_90_OBJ_90, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_107_OBJ_107, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_108_OBJ_108, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_109_OBJ_109, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_110_OBJ_110, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(P1X21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P1X22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P1X23, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P1X24, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(P1X25, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addContainerGap(33, Short.MAX_VALUE))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44_OBJ_45;
  private XRiTextField INDETB;
  private JLabel OBJ_45_OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_46_OBJ_47;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private XRiTextField P1X01;
  private XRiTextField P1X02;
  private XRiTextField P1X03;
  private XRiTextField P1X04;
  private XRiTextField P1X05;
  private XRiTextField P1X06;
  private XRiTextField P1X07;
  private XRiTextField P1X08;
  private XRiTextField P1X09;
  private XRiTextField P1X10;
  private XRiTextField P1X11;
  private XRiTextField P1X12;
  private XRiTextField P1X13;
  private XRiTextField P1X14;
  private XRiTextField P1X15;
  private XRiTextField P1X16;
  private XRiTextField P1X17;
  private XRiTextField P1X18;
  private XRiTextField P1X19;
  private XRiTextField P1X20;
  private XRiTextField P1X21;
  private XRiTextField P1X22;
  private XRiTextField P1X23;
  private XRiTextField P1X24;
  private XRiTextField P1X25;
  private JLabel OBJ_76_OBJ_76;
  private JLabel OBJ_87_OBJ_87;
  private JLabel OBJ_88_OBJ_88;
  private JLabel OBJ_89_OBJ_89;
  private JLabel OBJ_90_OBJ_90;
  private JLabel OBJ_91_OBJ_91;
  private JLabel OBJ_92_OBJ_92;
  private JLabel OBJ_93_OBJ_93;
  private JLabel OBJ_94_OBJ_94;
  private JLabel OBJ_95_OBJ_95;
  private JLabel OBJ_96_OBJ_96;
  private JLabel OBJ_97_OBJ_97;
  private JLabel OBJ_98_OBJ_98;
  private JLabel OBJ_99_OBJ_99;
  private JLabel OBJ_100_OBJ_100;
  private JLabel OBJ_101_OBJ_101;
  private JLabel OBJ_102_OBJ_102;
  private JLabel OBJ_103_OBJ_103;
  private JLabel OBJ_104_OBJ_104;
  private JLabel OBJ_105_OBJ_105;
  private JLabel OBJ_106_OBJ_106;
  private JLabel OBJ_107_OBJ_107;
  private JLabel OBJ_108_OBJ_108;
  private JLabel OBJ_109_OBJ_109;
  private JLabel OBJ_110_OBJ_110;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
