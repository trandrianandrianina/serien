
package ri.serien.libecranrpg.vcgm.VCGMCGFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerListModel;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiSpinner;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGMCGFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGMCGFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    CGQTE.setValeursSelection("X", " ");
    CGSNUL.setValeursSelection("OUI", "NON");
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDNUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDNUM@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    // CGSNUL.setEnabled( lexique.isPresent("CGSNUL"));
    // CGSNUL.setSelected(lexique.HostFieldGetData("CGSNUL").equalsIgnoreCase("OUI"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (CGQTE.isSelected())
    // lexique.HostFieldPutData("CGQTE", 0, "X");
    // else
    // lexique.HostFieldPutData("CGQTE", 0, " ");
    // if (CGSNUL.isSelected())
    // lexique.HostFieldPutData("CGSNUL", 0, "OUI");
    // else
    // lexique.HostFieldPutData("CGSNUL", 0, "NON");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vcgmcg"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_31 = new JLabel();
    INDNUM = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    CGOBJ = new XRiTextField();
    CGOBS = new XRiTextField();
    OBJ_38 = new JLabel();
    OBJ_41 = new JLabel();
    xTitledSeparator1 = new JXTitledSeparator();
    xTitledPanel1 = new JXTitledPanel();
    CGSNUL = new XRiCheckBox();
    CGCL1D = new XRiTextField();
    CGCL1F = new XRiTextField();
    CGQTE = new XRiCheckBox();
    OBJ_48 = new JLabel();
    OBJ_52 = new JLabel();
    OBJ_55 = new JLabel();
    OBJ_51 = new JLabel();
    CGREPD = new XRiTextField();
    CGREPF = new XRiTextField();
    OBJ_45 = new JLabel();
    CGCPTD = new XRiTextField();
    CGCPTF = new XRiTextField();
    OBJ_46 = new JLabel();
    OBJ_49 = new JLabel();
    OBJ_53 = new JLabel();
    CGDEV1 = new XRiTextField();
    CGDEV2 = new XRiTextField();
    CGDEV3 = new XRiTextField();
    CGDEV4 = new XRiTextField();
    CGDEV5 = new XRiTextField();
    CGSENS = new XRiSpinner();
    OBJ_47 = new JLabel();
    OBJ_50 = new JLabel();
    OBJ_54 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Crit\u00e8res de s\u00e9lection");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_31 ----
          OBJ_31.setText("Num\u00e9ro");
          OBJ_31.setName("OBJ_31");

          //---- INDNUM ----
          INDNUM.setComponentPopupMenu(BTD);
          INDNUM.setText("@INDNUM@");
          INDNUM.setOpaque(false);
          INDNUM.setName("INDNUM");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(780, 420));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- CGOBJ ----
          CGOBJ.setComponentPopupMenu(BTD);
          CGOBJ.setName("CGOBJ");

          //---- CGOBS ----
          CGOBS.setComponentPopupMenu(BTD);
          CGOBS.setName("CGOBS");

          //---- OBJ_38 ----
          OBJ_38.setText("Objet s\u00e9lection");
          OBJ_38.setName("OBJ_38");

          //---- OBJ_41 ----
          OBJ_41.setText("Observation");
          OBJ_41.setName("OBJ_41");

          //---- xTitledSeparator1 ----
          xTitledSeparator1.setTitle("Comptes g\u00e9n\u00e9raux");
          xTitledSeparator1.setName("xTitledSeparator1");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Comptes");
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- CGSNUL ----
            CGSNUL.setText("Edition montants nuls");
            CGSNUL.setComponentPopupMenu(BTD);
            CGSNUL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CGSNUL.setName("CGSNUL");
            xTitledPanel1ContentContainer.add(CGSNUL);
            CGSNUL.setBounds(200, 159, 160, 20);

            //---- CGCL1D ----
            CGCL1D.setComponentPopupMenu(BTD);
            CGCL1D.setName("CGCL1D");
            xTitledPanel1ContentContainer.add(CGCL1D);
            CGCL1D.setBounds(200, 50, 160, CGCL1D.getPreferredSize().height);

            //---- CGCL1F ----
            CGCL1F.setComponentPopupMenu(BTD);
            CGCL1F.setName("CGCL1F");
            xTitledPanel1ContentContainer.add(CGCL1F);
            CGCL1F.setBounds(420, 50, 160, CGCL1F.getPreferredSize().height);

            //---- CGQTE ----
            CGQTE.setText("G\u00e9r\u00e9 en quantit\u00e9");
            CGQTE.setComponentPopupMenu(BTD);
            CGQTE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CGQTE.setName("CGQTE");
            xTitledPanel1ContentContainer.add(CGQTE);
            CGQTE.setBounds(395, 19, 126, 20);

            //---- OBJ_48 ----
            OBJ_48.setText("Cl\u00e9 de classement");
            OBJ_48.setName("OBJ_48");
            xTitledPanel1ContentContainer.add(OBJ_48);
            OBJ_48.setBounds(20, 54, 117, 20);

            //---- OBJ_52 ----
            OBJ_52.setText("Compte reporting");
            OBJ_52.setName("OBJ_52");
            xTitledPanel1ContentContainer.add(OBJ_52);
            OBJ_52.setBounds(20, 124, 112, 20);

            //---- OBJ_55 ----
            OBJ_55.setText("Sens du solde");
            OBJ_55.setName("OBJ_55");
            xTitledPanel1ContentContainer.add(OBJ_55);
            OBJ_55.setBounds(20, 159, 91, 20);

            //---- OBJ_51 ----
            OBJ_51.setText("Code devise");
            OBJ_51.setName("OBJ_51");
            xTitledPanel1ContentContainer.add(OBJ_51);
            OBJ_51.setBounds(20, 89, 82, 20);

            //---- CGREPD ----
            CGREPD.setComponentPopupMenu(BTD);
            CGREPD.setName("CGREPD");
            xTitledPanel1ContentContainer.add(CGREPD);
            CGREPD.setBounds(200, 120, 100, CGREPD.getPreferredSize().height);

            //---- CGREPF ----
            CGREPF.setComponentPopupMenu(BTD);
            CGREPF.setName("CGREPF");
            xTitledPanel1ContentContainer.add(CGREPF);
            CGREPF.setBounds(420, 120, 100, CGREPF.getPreferredSize().height);

            //---- OBJ_45 ----
            OBJ_45.setText("Num\u00e9ro");
            OBJ_45.setName("OBJ_45");
            xTitledPanel1ContentContainer.add(OBJ_45);
            OBJ_45.setBounds(20, 19, 51, 20);

            //---- CGCPTD ----
            CGCPTD.setComponentPopupMenu(BTD);
            CGCPTD.setName("CGCPTD");
            xTitledPanel1ContentContainer.add(CGCPTD);
            CGCPTD.setBounds(200, 15, 58, CGCPTD.getPreferredSize().height);

            //---- CGCPTF ----
            CGCPTF.setComponentPopupMenu(BTD);
            CGCPTF.setName("CGCPTF");
            xTitledPanel1ContentContainer.add(CGCPTF);
            CGCPTF.setBounds(295, 15, 58, CGCPTF.getPreferredSize().height);

            //---- OBJ_46 ----
            OBJ_46.setText("D\u00e9but");
            OBJ_46.setName("OBJ_46");
            xTitledPanel1ContentContainer.add(OBJ_46);
            OBJ_46.setBounds(151, 19, 49, 20);

            //---- OBJ_49 ----
            OBJ_49.setText("D\u00e9but");
            OBJ_49.setName("OBJ_49");
            xTitledPanel1ContentContainer.add(OBJ_49);
            OBJ_49.setBounds(151, 54, 49, 20);

            //---- OBJ_53 ----
            OBJ_53.setText("D\u00e9but");
            OBJ_53.setName("OBJ_53");
            xTitledPanel1ContentContainer.add(OBJ_53);
            OBJ_53.setBounds(151, 124, 49, 20);

            //---- CGDEV1 ----
            CGDEV1.setComponentPopupMenu(BTD);
            CGDEV1.setName("CGDEV1");
            xTitledPanel1ContentContainer.add(CGDEV1);
            CGDEV1.setBounds(151, 85, 40, CGDEV1.getPreferredSize().height);

            //---- CGDEV2 ----
            CGDEV2.setComponentPopupMenu(BTD);
            CGDEV2.setName("CGDEV2");
            xTitledPanel1ContentContainer.add(CGDEV2);
            CGDEV2.setBounds(200, 85, 40, CGDEV2.getPreferredSize().height);

            //---- CGDEV3 ----
            CGDEV3.setComponentPopupMenu(BTD);
            CGDEV3.setName("CGDEV3");
            xTitledPanel1ContentContainer.add(CGDEV3);
            CGDEV3.setBounds(244, 85, 40, CGDEV3.getPreferredSize().height);

            //---- CGDEV4 ----
            CGDEV4.setComponentPopupMenu(BTD);
            CGDEV4.setName("CGDEV4");
            xTitledPanel1ContentContainer.add(CGDEV4);
            CGDEV4.setBounds(291, 85, 40, CGDEV4.getPreferredSize().height);

            //---- CGDEV5 ----
            CGDEV5.setComponentPopupMenu(BTD);
            CGDEV5.setName("CGDEV5");
            xTitledPanel1ContentContainer.add(CGDEV5);
            CGDEV5.setBounds(338, 85, 40, CGDEV5.getPreferredSize().height);

            //---- CGSENS ----
            CGSENS.setComponentPopupMenu(BTD);
            CGSENS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CGSENS.setModel(new SpinnerListModel(new String[] {" ", "+", "-"}) {
              { setValue("+"); }
            });
            CGSENS.setName("CGSENS");
            xTitledPanel1ContentContainer.add(CGSENS);
            CGSENS.setBounds(151, 158, 40, 22);

            //---- OBJ_47 ----
            OBJ_47.setText("Fin");
            OBJ_47.setName("OBJ_47");
            xTitledPanel1ContentContainer.add(OBJ_47);
            OBJ_47.setBounds(265, 19, 30, 20);

            //---- OBJ_50 ----
            OBJ_50.setText("Fin");
            OBJ_50.setName("OBJ_50");
            xTitledPanel1ContentContainer.add(OBJ_50);
            OBJ_50.setBounds(395, 54, 25, 20);

            //---- OBJ_54 ----
            OBJ_54.setText("Fin");
            OBJ_54.setName("OBJ_54");
            xTitledPanel1ContentContainer.add(OBJ_54);
            OBJ_54.setBounds(395, 124, 25, 20);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, 730, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                    .addGap(20, 20, 20)
                    .addComponent(CGOBJ, GroupLayout.PREFERRED_SIZE, 570, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                    .addGap(20, 20, 20)
                    .addComponent(CGOBS, GroupLayout.PREFERRED_SIZE, 570, GroupLayout.PREFERRED_SIZE))
                  .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 730, GroupLayout.PREFERRED_SIZE))
                .addContainerGap(19, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CGOBJ, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(CGOBS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 235, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(47, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_31;
  private RiZoneSortie INDNUM;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private XRiTextField CGOBJ;
  private XRiTextField CGOBS;
  private JLabel OBJ_38;
  private JLabel OBJ_41;
  private JXTitledSeparator xTitledSeparator1;
  private JXTitledPanel xTitledPanel1;
  private XRiCheckBox CGSNUL;
  private XRiTextField CGCL1D;
  private XRiTextField CGCL1F;
  private XRiCheckBox CGQTE;
  private JLabel OBJ_48;
  private JLabel OBJ_52;
  private JLabel OBJ_55;
  private JLabel OBJ_51;
  private XRiTextField CGREPD;
  private XRiTextField CGREPF;
  private JLabel OBJ_45;
  private XRiTextField CGCPTD;
  private XRiTextField CGCPTF;
  private JLabel OBJ_46;
  private JLabel OBJ_49;
  private JLabel OBJ_53;
  private XRiTextField CGDEV1;
  private XRiTextField CGDEV2;
  private XRiTextField CGDEV3;
  private XRiTextField CGDEV4;
  private XRiTextField CGDEV5;
  private XRiSpinner CGSENS;
  private JLabel OBJ_47;
  private JLabel OBJ_50;
  private JLabel OBJ_54;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
