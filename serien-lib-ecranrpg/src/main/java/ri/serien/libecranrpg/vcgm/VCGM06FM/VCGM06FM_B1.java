
package ri.serien.libecranrpg.vcgm.VCGM06FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM06FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] A1PTG_Value = { " ", "C", "D", };
  private String[] A1RPT_Value = { " ", "1", "2", "3", "9", };
  private String[] A1TAT_Value = { " ", "1", "2", "9", };
  private Icon bloc_couleur = null;
  
  public VCGM06FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    A1LIB.activerModeFantome("Nom ou raison sociale");
    A1CPL.activerModeFantome("Complément de nom");
    A1RUE.activerModeFantome("Rue");
    A1LOC.activerModeFantome("Localité");
    A1CDP.activerModeFantome("00000");
    A1VIL.activerModeFantome("Ville");
    A1PAY.activerModeFantome("Pays");
    
    // Ajout
    initDiverses();
    A1TAT.setValeurs(A1TAT_Value, null);
    A1RPT.setValeurs(A1RPT_Value, null);
    A1PTG.setValeurs(A1PTG_Value, null);
    A1NPR.setValeursSelection("X", " ");
    A1QTE.setValeursSelection("X", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_55.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBPCG@")).trim());
    OBJ_64.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDEV@")).trim());
    OBJ_66.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CPSENS@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    LIBDV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBDV@")).trim());
    LIBRGL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBRGL@")).trim());
    LIBRP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBRP@")).trim());
    UCLEX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UCLEX@")).trim());
    UCLEY.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UCLEY@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    boolean isModif = (!lexique.isTrue("53"));
    
    bloc_couleur = lexique.chargerImage("images/bloc_notes.png", true);
    button1.setIcon(bloc_couleur);
    
    UCLEX.setVisible(!lexique.HostFieldGetData("UCLEX").trim().equalsIgnoreCase(""));
    UCLEY.setVisible(!lexique.HostFieldGetData("UCLEY").trim().equalsIgnoreCase(""));
    
    coffre.setEnabled(isModif);
    
    // découpe de l'IBAN pour présentation standard
    String zoneIban = lexique.HostFieldGetData("WRIE");
    if (zoneIban.length() >= 0) {
      if (zoneIban.length() <= 4) {
        WIBAN1.setText(zoneIban.substring(0, zoneIban.length()));
      }
      else {
        WIBAN1.setText(zoneIban.substring(0, 4));
      }
    }
    if (zoneIban.length() >= 4) {
      if (zoneIban.length() <= 8) {
        WIBAN2.setText(zoneIban.substring(4, zoneIban.length()));
      }
      else {
        WIBAN2.setText(zoneIban.substring(4, 8));
      }
    }
    if (zoneIban.length() >= 8) {
      if (zoneIban.length() <= 12) {
        WIBAN3.setText(zoneIban.substring(8, zoneIban.length()));
      }
      else {
        WIBAN3.setText(zoneIban.substring(8, 12));
      }
    }
    if (zoneIban.length() >= 12) {
      if (zoneIban.length() <= 16) {
        WIBAN4.setText(zoneIban.substring(12, zoneIban.length()));
      }
      else {
        WIBAN4.setText(zoneIban.substring(12, 16));
      }
    }
    if (zoneIban.length() >= 16) {
      if (zoneIban.length() <= 20) {
        WIBAN5.setText(zoneIban.substring(16, zoneIban.length()));
      }
      else {
        WIBAN5.setText(zoneIban.substring(16, 20));
      }
    }
    if (zoneIban.length() >= 20) {
      if (zoneIban.length() <= 24) {
        WIBAN6.setText(zoneIban.substring(20, zoneIban.length()));
      }
      else {
        WIBAN6.setText(zoneIban.substring(20, 24));
      }
    }
    if (zoneIban.length() >= 24) {
      if (zoneIban.length() <= 28) {
        WIBAN7.setText(zoneIban.substring(24, zoneIban.length()));
      }
      else {
        WIBAN7.setText(zoneIban.substring(24, 28));
      }
    }
    if (zoneIban.length() >= 28) {
      WIBAN8.setText(zoneIban.substring(28, zoneIban.length()));
    }
    
    WIBAN1.setEnabled(isModif);
    WIBAN2.setEnabled(isModif);
    WIBAN3.setEnabled(isModif);
    WIBAN4.setEnabled(isModif);
    WIBAN5.setEnabled(isModif);
    WIBAN6.setEnabled(isModif);
    WIBAN7.setEnabled(isModif);
    WIBAN8.setEnabled(isModif);
    
    WIBAN1.setLongueurSaisie(4);
    WIBAN2.setLongueurSaisie(4);
    WIBAN3.setLongueurSaisie(4);
    WIBAN4.setLongueurSaisie(4);
    WIBAN5.setLongueurSaisie(4);
    WIBAN6.setLongueurSaisie(4);
    WIBAN7.setLongueurSaisie(4);
    WIBAN8.setLongueurSaisie(2);
    
    OBJ_64.setVisible(lexique.isPresent("CPSOPD"));
    
    // OBJ_154.setVisible(!interpreteurD.analyseExpression("@RELA@").trim().equalsIgnoreCase(""));
    OBJ_154.setVisible(A1TYP.isVisible());
    OBJ_177.setVisible(lexique.isPresent("A1FIR"));
    OBJ_66.setVisible(lexique.isPresent("CPSOPD"));
    OBJ_63.setVisible(lexique.isPresent("CPSOPD"));
    riSousMenu15.setEnabled(lexique.isTrue("N51"));
    riSousMenu19.setEnabled(lexique.isTrue("53"));
    riSousMenu18.setEnabled(lexique.isTrue("53"));
    riSousMenu9.setEnabled(lexique.isTrue("53"));
    riSousMenu8.setEnabled(lexique.isTrue("53"));
    riSousMenu7.setEnabled(lexique.isTrue("53"));
    riSousMenu6.setEnabled(interpreteurD.analyseExpression("@WPL@").equalsIgnoreCase("P"));
    WCPT.setEnabled(!lexique.isTrue("53"));
    // A1NPR.setSelected(lexique.HostFieldGetData("A1NPR").equalsIgnoreCase("X"));
    OBJ_55.setVisible(lexique.isPresent("LIBPCG"));
    
    // Désactivé
    if (lexique.isTrue("72")) {
      desactive.setVisible(true);
      P_Centre.setComponentZOrder(p_desac, 0);
    }
    else {
      desactive.setVisible(false);
    }
    
    // Domiciliation
    xTitledPanel2.setVisible(lexique.isTrue("(70 AND 69)"));
    // Règlement
    OBJ_112.setVisible(lexique.isTrue("74"));
    A1REP.setVisible(lexique.isTrue("71"));
    xTitledPanel7.setVisible(lexique.isTrue("71"));
    LIBRP.setVisible(lexique.isTrue("71"));
    A1DEV.setVisible(lexique.isTrue("74"));
    A1DEV.setEnabled(lexique.isTrue("N73"));
    A1QTE.setVisible(lexique.isTrue("75"));
    A1NPR.setVisible(lexique.isTrue("(71) AND (67)"));
    LIBDV.setVisible(lexique.isTrue("74"));
    A1ATT.setVisible(lexique.isTrue("77"));
    xTitledPanel6.setVisible(lexique.isTrue("77"));
    xTitledPanel3.setVisible(lexique.isTrue("(71) AND (67)"));
    
    // Critere de sélection
    boolean isEtat = false;
    isEtat = lexique.isTrue("61");
    if (!lexique.isTrue("53")) {
      A1CS5.setEnabled(isEtat);
      A1CS4.setEnabled(isEtat);
      A1CS3.setEnabled(isEtat);
      A1CS2.setEnabled(isEtat);
      A1CS1.setEnabled(isEtat);
    }
    A1QTE.setEnabled(lexique.isTrue("75"));
    // A1QTE.setSelected(lexique.HostFieldGetData("A1QTE").equalsIgnoreCase("X"));
    A1NCT.setVisible(lexique.isTrue("(75) AND (80)"));
    OBJ_175.setVisible(A1NCT.isVisible());
    A1FIR.setVisible(lexique.isTrue("(75) AND (80) AND (60)"));
    OBJ_177.setVisible(A1FIR.isVisible());
    
    String chaine = null;
    chaine = lexique.HostFieldGetData("INDNCG").trim();
    if ((chaine != null) && (chaine.length() > 1)) {
      if (chaine.substring(0, 2).equals("40")) {
        riSousMenu23.setEnabled(true);
        riSousMenu10.setEnabled(false);
      }
      else {
        riSousMenu23.setEnabled(false);
        riSousMenu10.setEnabled(true);
      }
    }
    
    // FICHE REVISION
    button1.setVisible(lexique.isTrue("79"));
    
    // TODO Icones
    desactive.setIcon(lexique.chargerImage("images/desact.png", true));
    
    

    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
    riMenu_bt4.setIcon(lexique.chargerImage("images/stats.png", true));
    
    // Titre
    setTitle(p_bpresentation.getText());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    WIBAN1.setText(WIBAN1.getText().toUpperCase());
    WIBAN2.setText(WIBAN2.getText().toUpperCase());
    WIBAN3.setText(WIBAN3.getText().toUpperCase());
    WIBAN4.setText(WIBAN4.getText().toUpperCase());
    WIBAN5.setText(WIBAN5.getText().toUpperCase());
    WIBAN6.setText(WIBAN6.getText().toUpperCase());
    WIBAN7.setText(WIBAN7.getText().toUpperCase());
    WIBAN8.setText(WIBAN8.getText().toUpperCase());
    
    lexique.HostFieldPutData("WRIE", 0, WIBAN1.getText() + WIBAN2.getText() + WIBAN3.getText() + WIBAN4.getText() + WIBAN5.getText()
        + WIBAN6.getText() + WIBAN7.getText() + WIBAN8.getText());
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, "CPT");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, "SIM");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(6, 2);
    lexique.HostScreenSendKey(this, "F7");
  }
  
  private void riSousMenu_bt13ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void riSousMenu_bt22ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt23ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void riSousMenu_bt24ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    StringBuffer chaine = new StringBuffer();
    String chaine2 = "";
    
    chaine2 = lexique.HostFieldGetData("A1LIB").trim();
    if (!chaine2.equals("")) {
      chaine.append(chaine2).append("\n");
    }
    chaine2 = lexique.HostFieldGetData("A1CPL").trim();
    if (!chaine2.equals("")) {
      chaine.append(chaine2).append("\n");
    }
    chaine2 = lexique.HostFieldGetData("A1RUE").trim();
    if (!chaine2.equals("")) {
      chaine.append(chaine2).append("\n");
    }
    chaine2 = lexique.HostFieldGetData("A1LOC").trim();
    if (!chaine2.equals("")) {
      chaine.append(chaine2).append("\n");
    }
    chaine2 = lexique.HostFieldGetData("A1VIL").trim();
    if (!chaine2.equals("")) {
      chaine.append(lexique.HostFieldGetData("A1CDP")).append(" ").append(chaine2).append("\n");
    }
    chaine2 = lexique.HostFieldGetData("A1PAY").trim();
    if (!chaine2.equals("")) {
      chaine.append(chaine2).append("\n");
    }
    Clipboard cb = Toolkit.getDefaultToolkit().getSystemClipboard();
    cb.setContents(new StringSelection(chaine.toString()), null);
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void riSousMenu_bt18ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void riSousMenu_bt19ActionPerformed(ActionEvent e) {
    // lexique.HostScreenSendKey(this, "V06F");
    lexique.HostFieldPutData("V06F", 0, "GRA");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_99ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(11, 52);
    lexique.HostScreenSendKey(this, "F7", false);
  }
  
  private void OBJ_87ActionPerformed(ActionEvent e) {
    // PcCommand(4, "Numeroteur.exe " +@"@A1TEL@"+ " "+Chr(34)+@"@A1LIB@"+Chr(34)+" "+"1 "+ @"@&PREFIXE@")
  }
  
  /*
  private void OBJ_130ActionPerformed(ActionEvent e) {
    if (lexique.isTrue("53"))
    {
      lexique.HostFieldPutData("V06F",0,"DOM");
      lexique.HostScreenSendKey(this, "ENTER");
    }
    else
    {
      lexique.HostCursorPut("DOMF4");
      lexique.HostScreenSendKey(this, "F4");
    }
  }*/
  
  private void OBJ_27ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void coffreActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WDO2");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void button1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_50 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_52 = new JLabel();
    INDNCG = new XRiTextField();
    INDNCA = new XRiTextField();
    OBJ_55 = new RiZoneSortie();
    OBJ_63 = new JLabel();
    OBJ_64 = new RiZoneSortie();
    CPSOPD = new XRiTextField();
    OBJ_66 = new RiZoneSortie();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riSousMenu13 = new RiSousMenu();
    riSousMenu_bt13 = new RiSousMenu_bt();
    riSousMenu22 = new RiSousMenu();
    riSousMenu_bt22 = new RiSousMenu_bt();
    riSousMenu23 = new RiSousMenu();
    riSousMenu_bt23 = new RiSousMenu_bt();
    riSousMenu24 = new RiSousMenu();
    riSousMenu_bt24 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    riMenu4 = new RiMenu();
    riMenu_bt4 = new RiMenu_bt();
    riSousMenu18 = new RiSousMenu();
    riSousMenu_bt18 = new RiSousMenu_bt();
    riSousMenu19 = new RiSousMenu();
    riSousMenu_bt19 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    P_Centre = new JLayeredPane();
    panel1 = new JPanel();
    A1PAC = new XRiTextField();
    A1CL1 = new XRiTextField();
    A1CL2 = new XRiTextField();
    A1TEL = new XRiTextField();
    A1SRN = new XRiTextField();
    OBJ_99 = new SNBoutonDetail();
    A1SRT = new XRiTextField();
    OBJ_87 = new SNBoutonDetail();
    A1NIK = new XRiTextField();
    A1NIP = new XRiTextField();
    separator2 = compFactory.createSeparator("Num\u00e9ro intracommunautaire");
    panel2 = new JPanel();
    OBJ_94 = new JLabel();
    A1FAX = new XRiTextField();
    xTitledSeparator1 = new JXTitledSeparator();
    label1 = new JLabel();
    OBJ_95 = new JLabel();
    separator1 = compFactory.createSeparator("Clefs de classement");
    panel4 = new JPanel();
    A1LIB = new XRiTextField();
    A1CPL = new XRiTextField();
    A1RUE = new XRiTextField();
    A1LOC = new XRiTextField();
    A1VIL = new XRiTextField();
    A1PAY = new XRiTextField();
    A1COP = new XRiTextField();
    A1CDP = new XRiTextField();
    button1 = new JButton();
    panel3 = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    A1RGL = new XRiTextField();
    OBJ_107 = new JLabel();
    OBJ_112 = new JLabel();
    A1ECH = new XRiTextField();
    A1DEV = new XRiTextField();
    LIBDV = new RiZoneSortie();
    LIBRGL = new RiZoneSortie();
    xTitledPanel4 = new JXTitledPanel();
    A1QTE = new XRiCheckBox();
    A1CS1 = new XRiTextField();
    OBJ_175 = new JLabel();
    A1CS2 = new XRiTextField();
    A1CS3 = new XRiTextField();
    A1CS4 = new XRiTextField();
    A1CS5 = new XRiTextField();
    A1NCT = new XRiTextField();
    OBJ_177 = new JLabel();
    A1FIR = new XRiTextField();
    xTitledPanel3 = new JXTitledPanel();
    A1NPR = new XRiCheckBox();
    OBJ_154 = new JLabel();
    A1TYP = new XRiTextField();
    xTitledPanel5 = new JXTitledPanel();
    OBJ_147 = new JLabel();
    A1PTG = new XRiComboBox();
    OBJ_149 = new JLabel();
    A1RPT = new XRiComboBox();
    xTitledPanel6 = new JXTitledPanel();
    A1ATT = new XRiTextField();
    A1TAT = new XRiComboBox();
    xTitledPanel7 = new JXTitledPanel();
    A1REP = new XRiTextField();
    LIBRP = new RiZoneSortie();
    xTitledPanel2 = new JXTitledPanel();
    panel5 = new JPanel();
    WDO1 = new XRiTextField();
    OBJ_22_OBJ_22 = new JLabel();
    WBQE = new XRiTextField();
    WGUI = new XRiTextField();
    WCPT = new XRiTextField();
    WRIB = new XRiTextField();
    UCLEX = new RiZoneSortie();
    OBJ_22_OBJ_24 = new JLabel();
    OBJ_22_OBJ_25 = new JLabel();
    OBJ_22_OBJ_26 = new JLabel();
    OBJ_22_OBJ_27 = new JLabel();
    OBJ_22_OBJ_28 = new JLabel();
    coffre = new SNBoutonDetail();
    panel6 = new JPanel();
    WDO2 = new XRiTextField();
    OBJ_40_OBJ_40 = new JLabel();
    WIBAN1 = new XRiTextField();
    WIBAN2 = new XRiTextField();
    WIBAN3 = new XRiTextField();
    WIBAN4 = new XRiTextField();
    WIBAN5 = new XRiTextField();
    WIBAN6 = new XRiTextField();
    WIBAN7 = new XRiTextField();
    WIBAN8 = new XRiTextField();
    UCLEY = new RiZoneSortie();
    OBJ_22_OBJ_23 = new JLabel();
    p_desac = new JPanel();
    desactive = new JLabel();
    BTD = new JPopupMenu();
    OBJ_27 = new JMenuItem();
    OBJ_26 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Compte auxiliaire");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 32));
          p_tete_gauche.setMinimumSize(new Dimension(900, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_50 ----
          OBJ_50.setText("Soci\u00e9t\u00e9");
          OBJ_50.setName("OBJ_50");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_52 ----
          OBJ_52.setText("Compte");
          OBJ_52.setName("OBJ_52");

          //---- INDNCG ----
          INDNCG.setComponentPopupMenu(BTD);
          INDNCG.setName("INDNCG");

          //---- INDNCA ----
          INDNCA.setComponentPopupMenu(BTD);
          INDNCA.setName("INDNCA");

          //---- OBJ_55 ----
          OBJ_55.setText("@LIBPCG@");
          OBJ_55.setFont(OBJ_55.getFont().deriveFont(OBJ_55.getFont().getStyle() & ~Font.BOLD));
          OBJ_55.setOpaque(false);
          OBJ_55.setName("OBJ_55");

          //---- OBJ_63 ----
          OBJ_63.setText("Solde");
          OBJ_63.setName("OBJ_63");

          //---- OBJ_64 ----
          OBJ_64.setText("@WDEV@");
          OBJ_64.setOpaque(false);
          OBJ_64.setFont(OBJ_64.getFont().deriveFont(OBJ_64.getFont().getStyle() | Font.BOLD));
          OBJ_64.setName("OBJ_64");

          //---- CPSOPD ----
          CPSOPD.setComponentPopupMenu(BTD);
          CPSOPD.setHorizontalAlignment(SwingConstants.RIGHT);
          CPSOPD.setEditable(false);
          CPSOPD.setName("CPSOPD");

          //---- OBJ_66 ----
          OBJ_66.setText("@CPSENS@");
          OBJ_66.setForeground(Color.darkGray);
          OBJ_66.setOpaque(false);
          OBJ_66.setName("OBJ_66");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDNCG, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(INDNCA, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 195, GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45)
                .addComponent(OBJ_63)
                .addGap(8, 8, 8)
                .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(CPSOPD, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_66, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDNCG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDNCA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addComponent(CPSOPD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_66, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Plafond d'encours");
              riSousMenu_bt6.setToolTipText("Plafond d'encours");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Visualisation comptes");
              riSousMenu_bt7.setToolTipText("Visualisation des comptes");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Affichage des compteurs");
              riSousMenu_bt8.setToolTipText("Affichage des compteurs");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Compteurs + pr\u00e9visionnel");
              riSousMenu_bt9.setToolTipText("Affichage des compteurs plus pr\u00e9visionnel");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("R\u00e9glements et relances");
              riSousMenu_bt10.setToolTipText("Fiche r\u00e9glements et relances");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);

            //======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");

              //---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Encours administratif");
              riSousMenu_bt11.setToolTipText("Encours administratif");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);

            //======== riSousMenu12 ========
            {
              riSousMenu12.setName("riSousMenu12");

              //---- riSousMenu_bt12 ----
              riSousMenu_bt12.setText("Fiche client ou frs");
              riSousMenu_bt12.setToolTipText("Acc\u00e8s \u00e0 la fiche client ou fournisseur");
              riSousMenu_bt12.setName("riSousMenu_bt12");
              riSousMenu_bt12.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt12ActionPerformed(e);
                }
              });
              riSousMenu12.add(riSousMenu_bt12);
            }
            menus_haut.add(riSousMenu12);

            //======== riSousMenu13 ========
            {
              riSousMenu13.setName("riSousMenu13");

              //---- riSousMenu_bt13 ----
              riSousMenu_bt13.setText("Bloc-note de GVM");
              riSousMenu_bt13.setToolTipText("Acc\u00e8s au bloc-note de la gestion commerciale");
              riSousMenu_bt13.setName("riSousMenu_bt13");
              riSousMenu_bt13.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt13ActionPerformed(e);
                }
              });
              riSousMenu13.add(riSousMenu_bt13);
            }
            menus_haut.add(riSousMenu13);

            //======== riSousMenu22 ========
            {
              riSousMenu22.setName("riSousMenu22");

              //---- riSousMenu_bt22 ----
              riSousMenu_bt22.setText("Fiche de r\u00e9vision");
              riSousMenu_bt22.setToolTipText("Fiche de r\u00e9vision");
              riSousMenu_bt22.setName("riSousMenu_bt22");
              riSousMenu_bt22.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt22ActionPerformed(e);
                }
              });
              riSousMenu22.add(riSousMenu_bt22);
            }
            menus_haut.add(riSousMenu22);

            //======== riSousMenu23 ========
            {
              riSousMenu23.setName("riSousMenu23");

              //---- riSousMenu_bt23 ----
              riSousMenu_bt23.setText("Fiche honoraires");
              riSousMenu_bt23.setToolTipText("Fiche honoraires");
              riSousMenu_bt23.setName("riSousMenu_bt23");
              riSousMenu_bt23.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt23ActionPerformed(e);
                }
              });
              riSousMenu23.add(riSousMenu_bt23);
            }
            menus_haut.add(riSousMenu23);

            //======== riSousMenu24 ========
            {
              riSousMenu24.setName("riSousMenu24");

              //---- riSousMenu_bt24 ----
              riSousMenu_bt24.setText("D.E.B.");
              riSousMenu_bt24.setToolTipText("D\u00e9claration d'\u00e9change de biens");
              riSousMenu_bt24.setName("riSousMenu_bt24");
              riSousMenu_bt24.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt24ActionPerformed(e);
                }
              });
              riSousMenu24.add(riSousMenu_bt24);
            }
            menus_haut.add(riSousMenu24);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Copie du bloc adresse");
              riSousMenu_bt14.setToolTipText("Copie le bloc adresse dans le presse papier de Windows");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);

            //======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");

              //---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Documents li\u00e9s");
              riSousMenu_bt15.setToolTipText("Documents li\u00e9s");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed(e);
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);

            //======== riMenu4 ========
            {
              riMenu4.setName("riMenu4");

              //---- riMenu_bt4 ----
              riMenu_bt4.setText("Statistiques");
              riMenu_bt4.setName("riMenu_bt4");
              riMenu4.add(riMenu_bt4);
            }
            menus_haut.add(riMenu4);

            //======== riSousMenu18 ========
            {
              riSousMenu18.setName("riSousMenu18");

              //---- riSousMenu_bt18 ----
              riSousMenu_bt18.setText("Analyse des comptes");
              riSousMenu_bt18.setToolTipText("Analyse des comptes (graphes statistiques)");
              riSousMenu_bt18.setName("riSousMenu_bt18");
              riSousMenu_bt18.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt18ActionPerformed(e);
                }
              });
              riSousMenu18.add(riSousMenu_bt18);
            }
            menus_haut.add(riSousMenu18);

            //======== riSousMenu19 ========
            {
              riSousMenu19.setName("riSousMenu19");

              //---- riSousMenu_bt19 ----
              riSousMenu_bt19.setText("Evolution du solde");
              riSousMenu_bt19.setToolTipText("Evolution du solde sur ce compte (graphes statistiques)");
              riSousMenu_bt19.setName("riSousMenu_bt19");
              riSousMenu_bt19.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt19ActionPerformed(e);
                }
              });
              riSousMenu19.add(riSousMenu_bt19);
            }
            menus_haut.add(riSousMenu19);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(970, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== P_Centre ========
          {
            P_Centre.setPreferredSize(new Dimension(1000, 600));
            P_Centre.setMinimumSize(new Dimension(1000, 600));
            P_Centre.setName("P_Centre");

            //======== panel1 ========
            {
              panel1.setOpaque(false);
              panel1.setBorder(new DropShadowBorder());
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- A1PAC ----
              A1PAC.setComponentPopupMenu(BTD);
              A1PAC.setName("A1PAC");
              panel1.add(A1PAC);
              A1PAC.setBounds(557, 155, 310, A1PAC.getPreferredSize().height);

              //---- A1CL1 ----
              A1CL1.setComponentPopupMenu(BTD);
              A1CL1.setName("A1CL1");
              panel1.add(A1CL1);
              A1CL1.setBounds(480, 20, 172, A1CL1.getPreferredSize().height);

              //---- A1CL2 ----
              A1CL2.setComponentPopupMenu(BTD);
              A1CL2.setName("A1CL2");
              panel1.add(A1CL2);
              A1CL2.setBounds(695, 20, 172, A1CL2.getPreferredSize().height);

              //---- A1TEL ----
              A1TEL.setToolTipText("Num\u00e9ro de t\u00e9l\u00e9phone");
              A1TEL.setComponentPopupMenu(BTD);
              A1TEL.setName("A1TEL");
              panel1.add(A1TEL);
              A1TEL.setBounds(480, 125, 140, A1TEL.getPreferredSize().height);

              //---- A1SRN ----
              A1SRN.setToolTipText("N\u00b0 Siren");
              A1SRN.setComponentPopupMenu(BTD);
              A1SRN.setName("A1SRN");
              panel1.add(A1SRN);
              A1SRN.setBounds(540, 80, 84, A1SRN.getPreferredSize().height);

              //---- OBJ_99 ----
              OBJ_99.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_99.setToolTipText("Acc\u00e8s au contact");
              OBJ_99.setName("OBJ_99");
              OBJ_99.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_99ActionPerformed(e);
                }
              });
              panel1.add(OBJ_99);
              OBJ_99.setBounds(870, 154, 35, 30);

              //---- A1SRT ----
              A1SRT.setToolTipText("Dernier code du n\u00b0 Siret");
              A1SRT.setComponentPopupMenu(BTD);
              A1SRT.setName("A1SRT");
              panel1.add(A1SRT);
              A1SRT.setBounds(625, 80, 52, A1SRT.getPreferredSize().height);

              //---- OBJ_87 ----
              OBJ_87.setText("");
              OBJ_87.setToolTipText("Composer le num\u00e9ro de t\u00e9l\u00e9phone");
              OBJ_87.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_87.setName("OBJ_87");
              OBJ_87.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_87ActionPerformed(e);
                }
              });
              panel1.add(OBJ_87);
              OBJ_87.setBounds(620, 124, 30, 30);

              //---- A1NIK ----
              A1NIK.setToolTipText("Cl\u00e9");
              A1NIK.setComponentPopupMenu(BTD);
              A1NIK.setName("A1NIK");
              panel1.add(A1NIK);
              A1NIK.setBounds(510, 80, 30, A1NIK.getPreferredSize().height);

              //---- A1NIP ----
              A1NIP.setToolTipText("Code pays");
              A1NIP.setComponentPopupMenu(BTD);
              A1NIP.setName("A1NIP");
              panel1.add(A1NIP);
              A1NIP.setBounds(480, 80, 30, A1NIP.getPreferredSize().height);

              //---- separator2 ----
              separator2.setName("separator2");
              panel1.add(separator2);
              separator2.setBounds(440, 60, 445, separator2.getPreferredSize().height);

              //======== panel2 ========
              {
                panel2.setOpaque(false);
                panel2.setName("panel2");
                panel2.setLayout(null);

                //---- OBJ_94 ----
                OBJ_94.setIcon(null);
                OBJ_94.setToolTipText("Num\u00e9ro de fax");
                OBJ_94.setText("fax");
                OBJ_94.setName("OBJ_94");
                panel2.add(OBJ_94);
                OBJ_94.setBounds(5, 3, 35, 32);

                //---- A1FAX ----
                A1FAX.setToolTipText("Num\u00e9ro de fax");
                A1FAX.setComponentPopupMenu(BTD);
                A1FAX.setName("A1FAX");
                panel2.add(A1FAX);
                A1FAX.setBounds(40, 5, 140, A1FAX.getPreferredSize().height);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < panel2.getComponentCount(); i++) {
                    Rectangle bounds = panel2.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = panel2.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  panel2.setMinimumSize(preferredSize);
                  panel2.setPreferredSize(preferredSize);
                }
              }
              panel1.add(panel2);
              panel2.setBounds(685, 120, 195, 40);

              //---- xTitledSeparator1 ----
              xTitledSeparator1.setTitle("Contacts");
              xTitledSeparator1.setName("xTitledSeparator1");
              panel1.add(xTitledSeparator1);
              xTitledSeparator1.setBounds(440, 109, 445, xTitledSeparator1.getPreferredSize().height);

              //---- label1 ----
              label1.setText("Contact");
              label1.setName("label1");
              panel1.add(label1);
              label1.setBounds(450, 159, 75, 20);

              //---- OBJ_95 ----
              OBJ_95.setIcon(null);
              OBJ_95.setToolTipText("Num\u00e9ro de fax");
              OBJ_95.setText("t\u00e9l.");
              OBJ_95.setName("OBJ_95");
              panel1.add(OBJ_95);
              OBJ_95.setBounds(450, 130, 30, 18);

              //---- separator1 ----
              separator1.setName("separator1");
              panel1.add(separator1);
              separator1.setBounds(440, 3, 445, separator1.getPreferredSize().height);

              //======== panel4 ========
              {
                panel4.setOpaque(false);
                panel4.setName("panel4");
                panel4.setLayout(null);

                //---- A1LIB ----
                A1LIB.setComponentPopupMenu(BTD);
                A1LIB.setFont(A1LIB.getFont().deriveFont(A1LIB.getFont().getStyle() | Font.BOLD, A1LIB.getFont().getSize() + 1f));
                A1LIB.setName("A1LIB");
                panel4.add(A1LIB);
                A1LIB.setBounds(10, 10, 310, A1LIB.getPreferredSize().height);

                //---- A1CPL ----
                A1CPL.setComponentPopupMenu(BTD);
                A1CPL.setName("A1CPL");
                panel4.add(A1CPL);
                A1CPL.setBounds(10, 40, 310, A1CPL.getPreferredSize().height);

                //---- A1RUE ----
                A1RUE.setComponentPopupMenu(BTD);
                A1RUE.setName("A1RUE");
                panel4.add(A1RUE);
                A1RUE.setBounds(10, 70, 310, A1RUE.getPreferredSize().height);

                //---- A1LOC ----
                A1LOC.setComponentPopupMenu(BTD);
                A1LOC.setName("A1LOC");
                panel4.add(A1LOC);
                A1LOC.setBounds(10, 100, 310, A1LOC.getPreferredSize().height);

                //---- A1VIL ----
                A1VIL.setComponentPopupMenu(BTD);
                A1VIL.setFont(A1VIL.getFont().deriveFont(A1VIL.getFont().getStyle() | Font.BOLD, A1VIL.getFont().getSize() + 1f));
                A1VIL.setName("A1VIL");
                panel4.add(A1VIL);
                A1VIL.setBounds(65, 130, 255, A1VIL.getPreferredSize().height);

                //---- A1PAY ----
                A1PAY.setComponentPopupMenu(BTD);
                A1PAY.setFont(A1PAY.getFont().deriveFont(A1PAY.getFont().getStyle() | Font.BOLD, A1PAY.getFont().getSize() + 1f));
                A1PAY.setName("A1PAY");
                panel4.add(A1PAY);
                A1PAY.setBounds(10, 160, 270, A1PAY.getPreferredSize().height);

                //---- A1COP ----
                A1COP.setComponentPopupMenu(BTD);
                A1COP.setFont(A1COP.getFont().deriveFont(A1COP.getFont().getStyle() | Font.BOLD, A1COP.getFont().getSize() + 1f));
                A1COP.setName("A1COP");
                panel4.add(A1COP);
                A1COP.setBounds(280, 160, 40, A1COP.getPreferredSize().height);

                //---- A1CDP ----
                A1CDP.setComponentPopupMenu(BTD);
                A1CDP.setFont(A1CDP.getFont().deriveFont(A1CDP.getFont().getStyle() | Font.BOLD, A1CDP.getFont().getSize() + 1f));
                A1CDP.setToolTipText("Code postal");
                A1CDP.setName("A1CDP");
                panel4.add(A1CDP);
                A1CDP.setBounds(10, 130, 52, A1CDP.getPreferredSize().height);

                //---- button1 ----
                button1.setContentAreaFilled(false);
                button1.setToolTipText("La fiche de r\u00e9vision contient des informations");
                button1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                button1.setName("button1");
                button1.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    button1ActionPerformed(e);
                  }
                });
                panel4.add(button1);
                button1.setBounds(325, 162, 27, 27);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < panel4.getComponentCount(); i++) {
                    Rectangle bounds = panel4.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = panel4.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  panel4.setMinimumSize(preferredSize);
                  panel4.setPreferredSize(preferredSize);
                }
              }
              panel1.add(panel4);
              panel4.setBounds(5, -5, 365, 200);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            P_Centre.add(panel1, JLayeredPane.DEFAULT_LAYER);
            panel1.setBounds(5, 5, 930, 200);

            //======== panel3 ========
            {
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //======== xTitledPanel1 ========
              {
                xTitledPanel1.setTitle("R\u00e8glement");
                xTitledPanel1.setBorder(new DropShadowBorder());
                xTitledPanel1.setName("xTitledPanel1");
                Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

                //---- A1RGL ----
                A1RGL.setComponentPopupMenu(BTD);
                A1RGL.setName("A1RGL");

                //---- OBJ_107 ----
                OBJ_107.setText("Ech\u00e9ance");
                OBJ_107.setName("OBJ_107");

                //---- OBJ_112 ----
                OBJ_112.setText("Devise");
                OBJ_112.setName("OBJ_112");

                //---- A1ECH ----
                A1ECH.setComponentPopupMenu(BTD);
                A1ECH.setName("A1ECH");

                //---- A1DEV ----
                A1DEV.setComponentPopupMenu(BTD);
                A1DEV.setName("A1DEV");

                //---- LIBDV ----
                LIBDV.setText("@LIBDV@");
                LIBDV.setName("LIBDV");

                //---- LIBRGL ----
                LIBRGL.setText("@LIBRGL@");
                LIBRGL.setBackground(new Color(214, 217, 223));
                LIBRGL.setOpaque(false);
                LIBRGL.setName("LIBRGL");

                GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
                xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
                xTitledPanel1ContentContainerLayout.setHorizontalGroup(
                  xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(10, 10, 10)
                      .addComponent(A1RGL, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_107, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(A1ECH, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(15, 15, 15)
                      .addComponent(LIBRGL, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(45, 45, 45)
                      .addComponent(OBJ_112, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(A1DEV, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(LIBDV, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE))
                );
                xTitledPanel1ContentContainerLayout.setVerticalGroup(
                  xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(A1RGL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(6, 6, 6)
                          .addComponent(OBJ_107))
                        .addComponent(A1ECH, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(2, 2, 2)
                          .addComponent(LIBRGL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(2, 2, 2)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(6, 6, 6)
                          .addComponent(OBJ_112))
                        .addComponent(A1DEV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(2, 2, 2)
                          .addComponent(LIBDV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                );
              }
              panel3.add(xTitledPanel1);
              xTitledPanel1.setBounds(10, 10, 460, 95);

              //======== xTitledPanel4 ========
              {
                xTitledPanel4.setBorder(new DropShadowBorder());
                xTitledPanel4.setTitle("Crit\u00e8res de s\u00e9lection");
                xTitledPanel4.setName("xTitledPanel4");
                Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
                xTitledPanel4ContentContainer.setLayout(null);

                //---- A1QTE ----
                A1QTE.setText("Gestion en quantit\u00e9");
                A1QTE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                A1QTE.setName("A1QTE");
                xTitledPanel4ContentContainer.add(A1QTE);
                A1QTE.setBounds(5, 39, 136, 20);

                //---- A1CS1 ----
                A1CS1.setComponentPopupMenu(BTD);
                A1CS1.setName("A1CS1");
                xTitledPanel4ContentContainer.add(A1CS1);
                A1CS1.setBounds(5, 5, 60, A1CS1.getPreferredSize().height);

                //---- OBJ_175 ----
                OBJ_175.setText("Centrale");
                OBJ_175.setName("OBJ_175");
                xTitledPanel4ContentContainer.add(OBJ_175);
                OBJ_175.setBounds(150, 39, 54, 20);

                //---- A1CS2 ----
                A1CS2.setComponentPopupMenu(BTD);
                A1CS2.setName("A1CS2");
                xTitledPanel4ContentContainer.add(A1CS2);
                A1CS2.setBounds(84, 5, 60, A1CS2.getPreferredSize().height);

                //---- A1CS3 ----
                A1CS3.setComponentPopupMenu(BTD);
                A1CS3.setName("A1CS3");
                xTitledPanel4ContentContainer.add(A1CS3);
                A1CS3.setBounds(163, 5, 60, A1CS3.getPreferredSize().height);

                //---- A1CS4 ----
                A1CS4.setComponentPopupMenu(BTD);
                A1CS4.setName("A1CS4");
                xTitledPanel4ContentContainer.add(A1CS4);
                A1CS4.setBounds(242, 5, 60, A1CS4.getPreferredSize().height);

                //---- A1CS5 ----
                A1CS5.setComponentPopupMenu(BTD);
                A1CS5.setName("A1CS5");
                xTitledPanel4ContentContainer.add(A1CS5);
                A1CS5.setBounds(327, 5, 60, A1CS5.getPreferredSize().height);

                //---- A1NCT ----
                A1NCT.setComponentPopupMenu(BTD);
                A1NCT.setName("A1NCT");
                xTitledPanel4ContentContainer.add(A1NCT);
                A1NCT.setBounds(209, 35, 60, A1NCT.getPreferredSize().height);

                //---- OBJ_177 ----
                OBJ_177.setText("Firme");
                OBJ_177.setHorizontalAlignment(SwingConstants.RIGHT);
                OBJ_177.setName("OBJ_177");
                xTitledPanel4ContentContainer.add(OBJ_177);
                OBJ_177.setBounds(285, 39, 37, 20);

                //---- A1FIR ----
                A1FIR.setComponentPopupMenu(BTD);
                A1FIR.setName("A1FIR");
                xTitledPanel4ContentContainer.add(A1FIR);
                A1FIR.setBounds(327, 35, 60, A1FIR.getPreferredSize().height);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < xTitledPanel4ContentContainer.getComponentCount(); i++) {
                    Rectangle bounds = xTitledPanel4ContentContainer.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = xTitledPanel4ContentContainer.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  xTitledPanel4ContentContainer.setMinimumSize(preferredSize);
                  xTitledPanel4ContentContainer.setPreferredSize(preferredSize);
                }
              }
              panel3.add(xTitledPanel4);
              xTitledPanel4.setBounds(482, 10, 448, 95);

              //======== xTitledPanel3 ========
              {
                xTitledPanel3.setBorder(new DropShadowBorder());
                xTitledPanel3.setTitle("Relance");
                xTitledPanel3.setName("xTitledPanel3");
                Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
                xTitledPanel3ContentContainer.setLayout(null);

                //---- A1NPR ----
                A1NPR.setText("Ne pas relancer");
                A1NPR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                A1NPR.setName("A1NPR");
                xTitledPanel3ContentContainer.add(A1NPR);
                A1NPR.setBounds(12, 5, 145, 28);

                //---- OBJ_154 ----
                OBJ_154.setText("Type");
                OBJ_154.setName("OBJ_154");
                xTitledPanel3ContentContainer.add(OBJ_154);
                OBJ_154.setBounds(175, 5, 36, 28);

                //---- A1TYP ----
                A1TYP.setComponentPopupMenu(BTD);
                A1TYP.setName("A1TYP");
                xTitledPanel3ContentContainer.add(A1TYP);
                A1TYP.setBounds(223, 5, 30, A1TYP.getPreferredSize().height);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
                    Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = xTitledPanel3ContentContainer.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
                  xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
                }
              }
              panel3.add(xTitledPanel3);
              xTitledPanel3.setBounds(640, 255, 290, 65);

              //======== xTitledPanel5 ========
              {
                xTitledPanel5.setBorder(new DropShadowBorder());
                xTitledPanel5.setTitle("Lettrage");
                xTitledPanel5.setName("xTitledPanel5");
                Container xTitledPanel5ContentContainer = xTitledPanel5.getContentContainer();
                xTitledPanel5ContentContainer.setLayout(null);

                //---- OBJ_147 ----
                OBJ_147.setText("Sens normal");
                OBJ_147.setName("OBJ_147");
                xTitledPanel5ContentContainer.add(OBJ_147);
                OBJ_147.setBounds(15, 5, 80, 26);

                //---- A1PTG ----
                A1PTG.setModel(new DefaultComboBoxModel(new String[] {
                  "",
                  "Cr\u00e9dit",
                  "D\u00e9bit"
                }));
                A1PTG.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                A1PTG.setName("A1PTG");
                xTitledPanel5ContentContainer.add(A1PTG);
                A1PTG.setBounds(105, 5, 75, A1PTG.getPreferredSize().height);

                //---- OBJ_149 ----
                OBJ_149.setText("N\u00b0 r\u00e9f.");
                OBJ_149.setName("OBJ_149");
                xTitledPanel5ContentContainer.add(OBJ_149);
                OBJ_149.setBounds(195, 5, 45, 26);

                //---- A1RPT ----
                A1RPT.setModel(new DefaultComboBoxModel(new String[] {
                  "",
                  "Lettrage par num\u00e9ro de pi\u00e8ce",
                  "Lettrage par mois d'imputation",
                  "Lettrage par similitudes de sommes",
                  "Lettrage par solde \u00e0 z\u00e9ro (Comptes sold\u00e9s)"
                }));
                A1RPT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                A1RPT.setName("A1RPT");
                xTitledPanel5ContentContainer.add(A1RPT);
                A1RPT.setBounds(245, 5, 345, A1RPT.getPreferredSize().height);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < xTitledPanel5ContentContainer.getComponentCount(); i++) {
                    Rectangle bounds = xTitledPanel5ContentContainer.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = xTitledPanel5ContentContainer.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  xTitledPanel5ContentContainer.setMinimumSize(preferredSize);
                  xTitledPanel5ContentContainer.setPreferredSize(preferredSize);
                }
              }
              panel3.add(xTitledPanel5);
              xTitledPanel5.setBounds(10, 255, 610, 65);

              //======== xTitledPanel6 ========
              {
                xTitledPanel6.setBorder(new DropShadowBorder());
                xTitledPanel6.setTitle("Message");
                xTitledPanel6.setName("xTitledPanel6");
                Container xTitledPanel6ContentContainer = xTitledPanel6.getContentContainer();

                //---- A1ATT ----
                A1ATT.setComponentPopupMenu(BTD);
                A1ATT.setName("A1ATT");

                //---- A1TAT ----
                A1TAT.setModel(new DefaultComboBoxModel(new String[] {
                  "Pas de message en saisie ou visualisation",
                  "Affichage du message en saisie ou visualisation",
                  "Affichage du message en rouge",
                  "Compte d\u00e9sactiv\u00e9"
                }));
                A1TAT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                A1TAT.setName("A1TAT");

                GroupLayout xTitledPanel6ContentContainerLayout = new GroupLayout(xTitledPanel6ContentContainer);
                xTitledPanel6ContentContainer.setLayout(xTitledPanel6ContentContainerLayout);
                xTitledPanel6ContentContainerLayout.setHorizontalGroup(
                  xTitledPanel6ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel6ContentContainerLayout.createSequentialGroup()
                      .addGap(12, 12, 12)
                      .addComponent(A1ATT, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
                      .addGap(41, 41, 41)
                      .addComponent(A1TAT, GroupLayout.PREFERRED_SIZE, 345, GroupLayout.PREFERRED_SIZE))
                );
                xTitledPanel6ContentContainerLayout.setVerticalGroup(
                  xTitledPanel6ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel6ContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(A1ATT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel6ContentContainerLayout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(A1TAT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                );
              }
              panel3.add(xTitledPanel6);
              xTitledPanel6.setBounds(10, 325, 610, 68);

              //======== xTitledPanel7 ========
              {
                xTitledPanel7.setBorder(new DropShadowBorder());
                xTitledPanel7.setTitle("Repr\u00e9sentant");
                xTitledPanel7.setName("xTitledPanel7");
                Container xTitledPanel7ContentContainer = xTitledPanel7.getContentContainer();

                //---- A1REP ----
                A1REP.setComponentPopupMenu(BTD);
                A1REP.setName("A1REP");

                //---- LIBRP ----
                LIBRP.setText("@LIBRP@");
                LIBRP.setName("LIBRP");

                GroupLayout xTitledPanel7ContentContainerLayout = new GroupLayout(xTitledPanel7ContentContainer);
                xTitledPanel7ContentContainer.setLayout(xTitledPanel7ContentContainerLayout);
                xTitledPanel7ContentContainerLayout.setHorizontalGroup(
                  xTitledPanel7ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel7ContentContainerLayout.createSequentialGroup()
                      .addGap(15, 15, 15)
                      .addComponent(A1REP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(15, 15, 15)
                      .addComponent(LIBRP, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE))
                );
                xTitledPanel7ContentContainerLayout.setVerticalGroup(
                  xTitledPanel7ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel7ContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(A1REP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel7ContentContainerLayout.createSequentialGroup()
                      .addGap(7, 7, 7)
                      .addComponent(LIBRP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                );
              }
              panel3.add(xTitledPanel7);
              xTitledPanel7.setBounds(640, 325, 290, 68);

              //======== xTitledPanel2 ========
              {
                xTitledPanel2.setTitle("Domiciliation");
                xTitledPanel2.setBorder(new DropShadowBorder());
                xTitledPanel2.setName("xTitledPanel2");
                Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
                xTitledPanel2ContentContainer.setLayout(null);

                //======== panel5 ========
                {
                  panel5.setOpaque(false);
                  panel5.setName("panel5");
                  panel5.setLayout(null);

                  //---- WDO1 ----
                  WDO1.setComponentPopupMenu(BTD);
                  WDO1.setName("WDO1");
                  panel5.add(WDO1);
                  WDO1.setBounds(114, 25, 220, WDO1.getPreferredSize().height);

                  //---- OBJ_22_OBJ_22 ----
                  OBJ_22_OBJ_22.setText("RIB");
                  OBJ_22_OBJ_22.setName("OBJ_22_OBJ_22");
                  panel5.add(OBJ_22_OBJ_22);
                  OBJ_22_OBJ_22.setBounds(355, 29, 55, 20);

                  //---- WBQE ----
                  WBQE.setComponentPopupMenu(BTD);
                  WBQE.setToolTipText("num\u00e9ro de banque");
                  WBQE.setName("WBQE");
                  panel5.add(WBQE);
                  WBQE.setBounds(410, 25, 50, WBQE.getPreferredSize().height);

                  //---- WGUI ----
                  WGUI.setComponentPopupMenu(BTD);
                  WGUI.setToolTipText("num\u00e9ro de guichet");
                  WGUI.setName("WGUI");
                  panel5.add(WGUI);
                  WGUI.setBounds(463, 25, 50, WGUI.getPreferredSize().height);

                  //---- WCPT ----
                  WCPT.setComponentPopupMenu(BTD);
                  WCPT.setToolTipText("num\u00e9ro de compte");
                  WCPT.setEnabled(false);
                  WCPT.setName("WCPT");
                  panel5.add(WCPT);
                  WCPT.setBounds(516, 25, 121, WCPT.getPreferredSize().height);

                  //---- WRIB ----
                  WRIB.setComponentPopupMenu(BTD);
                  WRIB.setToolTipText("Cl\u00e9");
                  WRIB.setName("WRIB");
                  panel5.add(WRIB);
                  WRIB.setBounds(675, 25, 28, WRIB.getPreferredSize().height);

                  //---- UCLEX ----
                  UCLEX.setText("@UCLEX@");
                  UCLEX.setForeground(Color.red);
                  UCLEX.setToolTipText("Cl\u00e9 r\u00e9elle");
                  UCLEX.setName("UCLEX");
                  panel5.add(UCLEX);
                  UCLEX.setBounds(808, 27, 25, UCLEX.getPreferredSize().height);

                  //---- OBJ_22_OBJ_24 ----
                  OBJ_22_OBJ_24.setText("Banque");
                  OBJ_22_OBJ_24.setName("OBJ_22_OBJ_24");
                  panel5.add(OBJ_22_OBJ_24);
                  OBJ_22_OBJ_24.setBounds(5, 29, 110, 20);

                  //---- OBJ_22_OBJ_25 ----
                  OBJ_22_OBJ_25.setText("Banque");
                  OBJ_22_OBJ_25.setHorizontalAlignment(SwingConstants.CENTER);
                  OBJ_22_OBJ_25.setFont(OBJ_22_OBJ_25.getFont().deriveFont(OBJ_22_OBJ_25.getFont().getStyle() | Font.BOLD));
                  OBJ_22_OBJ_25.setName("OBJ_22_OBJ_25");
                  panel5.add(OBJ_22_OBJ_25);
                  OBJ_22_OBJ_25.setBounds(410, 5, 50, 20);

                  //---- OBJ_22_OBJ_26 ----
                  OBJ_22_OBJ_26.setText("Guichet");
                  OBJ_22_OBJ_26.setHorizontalAlignment(SwingConstants.CENTER);
                  OBJ_22_OBJ_26.setFont(OBJ_22_OBJ_26.getFont().deriveFont(OBJ_22_OBJ_26.getFont().getStyle() | Font.BOLD));
                  OBJ_22_OBJ_26.setName("OBJ_22_OBJ_26");
                  panel5.add(OBJ_22_OBJ_26);
                  OBJ_22_OBJ_26.setBounds(463, 5, 50, 20);

                  //---- OBJ_22_OBJ_27 ----
                  OBJ_22_OBJ_27.setText("Num\u00e9ro de compte");
                  OBJ_22_OBJ_27.setHorizontalAlignment(SwingConstants.CENTER);
                  OBJ_22_OBJ_27.setFont(OBJ_22_OBJ_27.getFont().deriveFont(OBJ_22_OBJ_27.getFont().getStyle() | Font.BOLD));
                  OBJ_22_OBJ_27.setName("OBJ_22_OBJ_27");
                  panel5.add(OBJ_22_OBJ_27);
                  OBJ_22_OBJ_27.setBounds(516, 5, 121, 20);

                  //---- OBJ_22_OBJ_28 ----
                  OBJ_22_OBJ_28.setText("Cl\u00e9");
                  OBJ_22_OBJ_28.setHorizontalAlignment(SwingConstants.CENTER);
                  OBJ_22_OBJ_28.setFont(OBJ_22_OBJ_28.getFont().deriveFont(OBJ_22_OBJ_28.getFont().getStyle() | Font.BOLD));
                  OBJ_22_OBJ_28.setName("OBJ_22_OBJ_28");
                  panel5.add(OBJ_22_OBJ_28);
                  OBJ_22_OBJ_28.setBounds(675, 5, 28, 20);

                  //---- coffre ----
                  coffre.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                  coffre.setName("coffre");
                  coffre.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                      coffreActionPerformed(e);
                    }
                  });
                  panel5.add(coffre);
                  coffre.setBounds(845, 23, 32, 32);

                  {
                    // compute preferred size
                    Dimension preferredSize = new Dimension();
                    for(int i = 0; i < panel5.getComponentCount(); i++) {
                      Rectangle bounds = panel5.getComponent(i).getBounds();
                      preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                      preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                    }
                    Insets insets = panel5.getInsets();
                    preferredSize.width += insets.right;
                    preferredSize.height += insets.bottom;
                    panel5.setMinimumSize(preferredSize);
                    panel5.setPreferredSize(preferredSize);
                  }
                }
                xTitledPanel2ContentContainer.add(panel5);
                panel5.setBounds(10, 5, 890, 55);

                //======== panel6 ========
                {
                  panel6.setOpaque(false);
                  panel6.setName("panel6");
                  panel6.setLayout(null);

                  //---- WDO2 ----
                  WDO2.setComponentPopupMenu(BTD);
                  WDO2.setName("WDO2");
                  panel6.add(WDO2);
                  WDO2.setBounds(115, 5, 220, WDO2.getPreferredSize().height);

                  //---- OBJ_40_OBJ_40 ----
                  OBJ_40_OBJ_40.setText("IBAN");
                  OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");
                  panel6.add(OBJ_40_OBJ_40);
                  OBJ_40_OBJ_40.setBounds(355, 9, 56, 20);

                  //---- WIBAN1 ----
                  WIBAN1.setComponentPopupMenu(BTD);
                  WIBAN1.setNextFocusableComponent(WIBAN2);
                  WIBAN1.setName("WIBAN1");
                  panel6.add(WIBAN1);
                  WIBAN1.setBounds(410, 5, 54, WIBAN1.getPreferredSize().height);

                  //---- WIBAN2 ----
                  WIBAN2.setComponentPopupMenu(BTD);
                  WIBAN2.setNextFocusableComponent(WIBAN3);
                  WIBAN2.setName("WIBAN2");
                  panel6.add(WIBAN2);
                  WIBAN2.setBounds(463, 5, 54, WIBAN2.getPreferredSize().height);

                  //---- WIBAN3 ----
                  WIBAN3.setComponentPopupMenu(BTD);
                  WIBAN3.setNextFocusableComponent(WIBAN4);
                  WIBAN3.setName("WIBAN3");
                  panel6.add(WIBAN3);
                  WIBAN3.setBounds(516, 5, 54, WIBAN3.getPreferredSize().height);

                  //---- WIBAN4 ----
                  WIBAN4.setComponentPopupMenu(BTD);
                  WIBAN4.setNextFocusableComponent(WIBAN5);
                  WIBAN4.setName("WIBAN4");
                  panel6.add(WIBAN4);
                  WIBAN4.setBounds(569, 5, 54, WIBAN4.getPreferredSize().height);

                  //---- WIBAN5 ----
                  WIBAN5.setComponentPopupMenu(BTD);
                  WIBAN5.setNextFocusableComponent(WIBAN6);
                  WIBAN5.setName("WIBAN5");
                  panel6.add(WIBAN5);
                  WIBAN5.setBounds(622, 5, 54, WIBAN5.getPreferredSize().height);

                  //---- WIBAN6 ----
                  WIBAN6.setComponentPopupMenu(BTD);
                  WIBAN6.setNextFocusableComponent(WIBAN7);
                  WIBAN6.setName("WIBAN6");
                  panel6.add(WIBAN6);
                  WIBAN6.setBounds(675, 5, 54, WIBAN6.getPreferredSize().height);

                  //---- WIBAN7 ----
                  WIBAN7.setComponentPopupMenu(BTD);
                  WIBAN7.setNextFocusableComponent(WIBAN8);
                  WIBAN7.setName("WIBAN7");
                  panel6.add(WIBAN7);
                  WIBAN7.setBounds(728, 5, 54, WIBAN7.getPreferredSize().height);

                  //---- WIBAN8 ----
                  WIBAN8.setComponentPopupMenu(BTD);
                  WIBAN8.setName("WIBAN8");
                  panel6.add(WIBAN8);
                  WIBAN8.setBounds(781, 5, 28, WIBAN8.getPreferredSize().height);

                  //---- UCLEY ----
                  UCLEY.setText("@UCLEY@");
                  UCLEY.setForeground(Color.red);
                  UCLEY.setName("UCLEY");
                  panel6.add(UCLEY);
                  UCLEY.setBounds(808, 7, 25, UCLEY.getPreferredSize().height);

                  //---- OBJ_22_OBJ_23 ----
                  OBJ_22_OBJ_23.setText("Guichet ou SWIFT");
                  OBJ_22_OBJ_23.setName("OBJ_22_OBJ_23");
                  panel6.add(OBJ_22_OBJ_23);
                  OBJ_22_OBJ_23.setBounds(5, 9, 110, 20);

                  {
                    // compute preferred size
                    Dimension preferredSize = new Dimension();
                    for(int i = 0; i < panel6.getComponentCount(); i++) {
                      Rectangle bounds = panel6.getComponent(i).getBounds();
                      preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                      preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                    }
                    Insets insets = panel6.getInsets();
                    preferredSize.width += insets.right;
                    preferredSize.height += insets.bottom;
                    panel6.setMinimumSize(preferredSize);
                    panel6.setPreferredSize(preferredSize);
                  }
                }
                xTitledPanel2ContentContainer.add(panel6);
                panel6.setBounds(10, 65, 875, 40);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                    Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = xTitledPanel2ContentContainer.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
                  xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
                }
              }
              panel3.add(xTitledPanel2);
              xTitledPanel2.setBounds(10, 110, 920, 140);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            P_Centre.add(panel3, JLayeredPane.DEFAULT_LAYER);
            panel3.setBounds(0, 200, 940, 395);

            //======== p_desac ========
            {
              p_desac.setOpaque(false);
              p_desac.setName("p_desac");
              p_desac.setLayout(null);

              //---- desactive ----
              desactive.setHorizontalAlignment(SwingConstants.CENTER);
              desactive.setName("desactive");
              p_desac.add(desactive);
              desactive.setBounds(10, 10, 895, 535);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < p_desac.getComponentCount(); i++) {
                  Rectangle bounds = p_desac.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = p_desac.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                p_desac.setMinimumSize(preferredSize);
                p_desac.setPreferredSize(preferredSize);
              }
            }
            P_Centre.add(p_desac, JLayeredPane.DEFAULT_LAYER);
            p_desac.setBounds(new Rectangle(new Point(5, 0), p_desac.getPreferredSize()));
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(P_Centre, GroupLayout.PREFERRED_SIZE, 944, GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addComponent(P_Centre, GroupLayout.PREFERRED_SIZE, 589, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(9, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_27 ----
      OBJ_27.setText("Choix possibles");
      OBJ_27.setName("OBJ_27");
      OBJ_27.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_27ActionPerformed(e);
        }
      });
      BTD.add(OBJ_27);

      //---- OBJ_26 ----
      OBJ_26.setText("Aide en ligne");
      OBJ_26.setName("OBJ_26");
      OBJ_26.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_26ActionPerformed(e);
        }
      });
      BTD.add(OBJ_26);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_50;
  private XRiTextField INDETB;
  private JLabel OBJ_52;
  private XRiTextField INDNCG;
  private XRiTextField INDNCA;
  private RiZoneSortie OBJ_55;
  private JLabel OBJ_63;
  private RiZoneSortie OBJ_64;
  private XRiTextField CPSOPD;
  private RiZoneSortie OBJ_66;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiSousMenu riSousMenu13;
  private RiSousMenu_bt riSousMenu_bt13;
  private RiSousMenu riSousMenu22;
  private RiSousMenu_bt riSousMenu_bt22;
  private RiSousMenu riSousMenu23;
  private RiSousMenu_bt riSousMenu_bt23;
  private RiSousMenu riSousMenu24;
  private RiSousMenu_bt riSousMenu_bt24;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private RiMenu riMenu4;
  private RiMenu_bt riMenu_bt4;
  private RiSousMenu riSousMenu18;
  private RiSousMenu_bt riSousMenu_bt18;
  private RiSousMenu riSousMenu19;
  private RiSousMenu_bt riSousMenu_bt19;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JLayeredPane P_Centre;
  private JPanel panel1;
  private XRiTextField A1PAC;
  private XRiTextField A1CL1;
  private XRiTextField A1CL2;
  private XRiTextField A1TEL;
  private XRiTextField A1SRN;
  private SNBoutonDetail OBJ_99;
  private XRiTextField A1SRT;
  private SNBoutonDetail OBJ_87;
  private XRiTextField A1NIK;
  private XRiTextField A1NIP;
  private JComponent separator2;
  private JPanel panel2;
  private JLabel OBJ_94;
  private XRiTextField A1FAX;
  private JXTitledSeparator xTitledSeparator1;
  private JLabel label1;
  private JLabel OBJ_95;
  private JComponent separator1;
  private JPanel panel4;
  private XRiTextField A1LIB;
  private XRiTextField A1CPL;
  private XRiTextField A1RUE;
  private XRiTextField A1LOC;
  private XRiTextField A1VIL;
  private XRiTextField A1PAY;
  private XRiTextField A1COP;
  private XRiTextField A1CDP;
  private JButton button1;
  private JPanel panel3;
  private JXTitledPanel xTitledPanel1;
  private XRiTextField A1RGL;
  private JLabel OBJ_107;
  private JLabel OBJ_112;
  private XRiTextField A1ECH;
  private XRiTextField A1DEV;
  private RiZoneSortie LIBDV;
  private RiZoneSortie LIBRGL;
  private JXTitledPanel xTitledPanel4;
  private XRiCheckBox A1QTE;
  private XRiTextField A1CS1;
  private JLabel OBJ_175;
  private XRiTextField A1CS2;
  private XRiTextField A1CS3;
  private XRiTextField A1CS4;
  private XRiTextField A1CS5;
  private XRiTextField A1NCT;
  private JLabel OBJ_177;
  private XRiTextField A1FIR;
  private JXTitledPanel xTitledPanel3;
  private XRiCheckBox A1NPR;
  private JLabel OBJ_154;
  private XRiTextField A1TYP;
  private JXTitledPanel xTitledPanel5;
  private JLabel OBJ_147;
  private XRiComboBox A1PTG;
  private JLabel OBJ_149;
  private XRiComboBox A1RPT;
  private JXTitledPanel xTitledPanel6;
  private XRiTextField A1ATT;
  private XRiComboBox A1TAT;
  private JXTitledPanel xTitledPanel7;
  private XRiTextField A1REP;
  private RiZoneSortie LIBRP;
  private JXTitledPanel xTitledPanel2;
  private JPanel panel5;
  private XRiTextField WDO1;
  private JLabel OBJ_22_OBJ_22;
  private XRiTextField WBQE;
  private XRiTextField WGUI;
  private XRiTextField WCPT;
  private XRiTextField WRIB;
  private RiZoneSortie UCLEX;
  private JLabel OBJ_22_OBJ_24;
  private JLabel OBJ_22_OBJ_25;
  private JLabel OBJ_22_OBJ_26;
  private JLabel OBJ_22_OBJ_27;
  private JLabel OBJ_22_OBJ_28;
  private SNBoutonDetail coffre;
  private JPanel panel6;
  private XRiTextField WDO2;
  private JLabel OBJ_40_OBJ_40;
  private XRiTextField WIBAN1;
  private XRiTextField WIBAN2;
  private XRiTextField WIBAN3;
  private XRiTextField WIBAN4;
  private XRiTextField WIBAN5;
  private XRiTextField WIBAN6;
  private XRiTextField WIBAN7;
  private XRiTextField WIBAN8;
  private RiZoneSortie UCLEY;
  private JLabel OBJ_22_OBJ_23;
  private JPanel p_desac;
  private JLabel desactive;
  private JPopupMenu BTD;
  private JMenuItem OBJ_27;
  private JMenuItem OBJ_26;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
