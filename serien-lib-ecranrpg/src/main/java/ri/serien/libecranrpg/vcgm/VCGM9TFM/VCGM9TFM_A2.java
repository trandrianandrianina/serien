
package ri.serien.libecranrpg.vcgm.VCGM9TFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM9TFM_A2 extends SNPanelEcranRPG implements ioFrame {
  
  
  // private String[] V06F_Value={"-10","-5","-3","-2","-1","+10","+5 ","+3","+2","+1","",};
  private String[] V06F_Value = { "-10", "-05", "-03", "-02", "-01", "", "+01", "+02", "+03", "+05", "+10", };
  private String[] V06F_Title = { "Décalage de 10 lignes en moins", "Décalage de 5 lignes en moins", "Décalage de 3 lignes en moins",
      "Décalage de 2 lignes en moins", "Décalage de 1 ligne en moins", "", "Décalage de 1 ligne en plus", "Décalage de 2 lignes en plus",
      "Décalage de 3 lignes en plus", "Décalage de 5 lignes en plus", "Décalage de 10 lignes en plus", };
  
  private String[] _LD01_Title = { "HLD01", };
  private String[][] _LD01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private String[] _LIST_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15" };
  // private int[] _LD01_Width = {541, };
  
  public VCGM9TFM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    V06F.setValeurs(V06F_Value, V06F_Title);
    
    LD01.setAspectTable(_LIST_Top, _LD01_Title, _LD01_Data, null, false, null, null, null, null);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    DEMETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DEMETB@")).trim());
    DEMSA1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DEMSA1@")).trim());
    DEMSA2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DEMSA2@")).trim());
    DEMDAD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DEMDAD@")).trim());
    DEMDAF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DEMDAF@")).trim());
    DEMCPD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DEMCPD@")).trim());
    DEMCPF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DEMCPF@")).trim());
    OBJ_56_OBJ_56.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIBDV@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    

    // +++++++++++++++++++++++++++++++++++++++++++++
    
    
    OBJ_58.setVisible(lexique.isPresent("V06F"));
    OBJ_58.setEnabled(lexique.isPresent("V06F"));
    OBJ_57.setVisible(lexique.isPresent("V06F"));
    OBJ_57.setEnabled(lexique.isPresent("V06F"));
    OBJ_56_OBJ_56.setVisible(lexique.isPresent("WLIBDV"));
    // V06F.setVisible( lexique.isPresent("V06F"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
    riMenu_bt4.setIcon(lexique.chargerImage("images/fonctions.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("V06F", 0, V06F_Value[V06F.getSelectedIndex()]);
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt13ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt16ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt17ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt18ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt19ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt20ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt21ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    LD01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_57ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, "D");
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void OBJ_58ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, "F");
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void LD01MouseClicked(MouseEvent e) {
    if (LD01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void V06FItemStateChanged(ItemEvent e) {
    if (!V06F.getSelectedItem().equals("")) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riSousMenu13 = new RiSousMenu();
    riSousMenu_bt13 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    riSousMenu16 = new RiSousMenu();
    riSousMenu_bt16 = new RiSousMenu_bt();
    riSousMenu17 = new RiSousMenu();
    riSousMenu_bt17 = new RiSousMenu_bt();
    riMenu4 = new RiMenu();
    riMenu_bt4 = new RiMenu_bt();
    riSousMenu18 = new RiSousMenu();
    riSousMenu_bt18 = new RiSousMenu_bt();
    riSousMenu19 = new RiSousMenu();
    riSousMenu_bt19 = new RiSousMenu_bt();
    riSousMenu20 = new RiSousMenu();
    riSousMenu_bt20 = new RiSousMenu_bt();
    riSousMenu21 = new RiSousMenu();
    riSousMenu_bt21 = new RiSousMenu_bt();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_29_OBJ_29 = new JLabel();
    DEMETB = new RiZoneSortie();
    OBJ_31_OBJ_31 = new JLabel();
    DEMSA1 = new RiZoneSortie();
    DEMSA2 = new RiZoneSortie();
    OBJ_36_OBJ_36 = new JLabel();
    DEMDAD = new RiZoneSortie();
    DEMDAF = new RiZoneSortie();
    OBJ_39_OBJ_39 = new JLabel();
    DEMCPD = new RiZoneSortie();
    DEMCPF = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    V06F = new XRiComboBox();
    OBJ_56_OBJ_56 = new RiZoneSortie();
    OBJ_57 = new SNBoutonLeger();
    OBJ_58 = new SNBoutonLeger();
    OBJ_55_OBJ_55 = new JLabel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    scrollPane1 = new JScrollPane();
    LD01 = new XRiTable();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_10 = new JMenuItem();

    //======== scroll_droite ========
    {
      scroll_droite.setBackground(new Color(238, 239, 241));
      scroll_droite.setPreferredSize(new Dimension(16, 520));
      scroll_droite.setBorder(null);
      scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
      scroll_droite.setName("scroll_droite");

      //======== menus_haut ========
      {
        menus_haut.setMinimumSize(new Dimension(160, 520));
        menus_haut.setPreferredSize(new Dimension(160, 520));
        menus_haut.setBackground(new Color(238, 239, 241));
        menus_haut.setAutoscrolls(true);
        menus_haut.setName("menus_haut");
        menus_haut.setLayout(new VerticalLayout());

        //======== riMenu_V01F ========
        {
          riMenu_V01F.setMinimumSize(new Dimension(104, 50));
          riMenu_V01F.setPreferredSize(new Dimension(170, 50));
          riMenu_V01F.setMaximumSize(new Dimension(104, 50));
          riMenu_V01F.setName("riMenu_V01F");

          //---- riMenu_bt_V01F ----
          riMenu_bt_V01F.setText("@V01F@");
          riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
          riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
          riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
          riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
          riMenu_bt_V01F.setName("riMenu_bt_V01F");
          riMenu_V01F.add(riMenu_bt_V01F);
        }
        menus_haut.add(riMenu_V01F);

        //======== riSousMenu_consult ========
        {
          riSousMenu_consult.setName("riSousMenu_consult");

          //---- riSousMenu_bt_consult ----
          riSousMenu_bt_consult.setText("Consultation");
          riSousMenu_bt_consult.setToolTipText("Consultation");
          riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
          riSousMenu_consult.add(riSousMenu_bt_consult);
        }
        menus_haut.add(riSousMenu_consult);

        //======== riSousMenu_modif ========
        {
          riSousMenu_modif.setName("riSousMenu_modif");

          //---- riSousMenu_bt_modif ----
          riSousMenu_bt_modif.setText("Modification");
          riSousMenu_bt_modif.setToolTipText("Modification");
          riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
          riSousMenu_modif.add(riSousMenu_bt_modif);
        }
        menus_haut.add(riSousMenu_modif);

        //======== riSousMenu_crea ========
        {
          riSousMenu_crea.setName("riSousMenu_crea");

          //---- riSousMenu_bt_crea ----
          riSousMenu_bt_crea.setText("Cr\u00e9ation");
          riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
          riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
          riSousMenu_crea.add(riSousMenu_bt_crea);
        }
        menus_haut.add(riSousMenu_crea);

        //======== riSousMenu_suppr ========
        {
          riSousMenu_suppr.setName("riSousMenu_suppr");

          //---- riSousMenu_bt_suppr ----
          riSousMenu_bt_suppr.setText("Annulation");
          riSousMenu_bt_suppr.setToolTipText("Annulation");
          riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
          riSousMenu_suppr.add(riSousMenu_bt_suppr);
        }
        menus_haut.add(riSousMenu_suppr);

        //======== riSousMenuF_dupli ========
        {
          riSousMenuF_dupli.setName("riSousMenuF_dupli");

          //---- riSousMenu_bt_dupli ----
          riSousMenu_bt_dupli.setText("Duplication");
          riSousMenu_bt_dupli.setToolTipText("Duplication");
          riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
          riSousMenuF_dupli.add(riSousMenu_bt_dupli);
        }
        menus_haut.add(riSousMenuF_dupli);

        //======== riSousMenu_rappel ========
        {
          riSousMenu_rappel.setName("riSousMenu_rappel");

          //---- riSousMenu_bt_rappel ----
          riSousMenu_bt_rappel.setText("Rappel");
          riSousMenu_bt_rappel.setToolTipText("Rappel");
          riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
          riSousMenu_rappel.add(riSousMenu_bt_rappel);
        }
        menus_haut.add(riSousMenu_rappel);

        //======== riSousMenu_reac ========
        {
          riSousMenu_reac.setName("riSousMenu_reac");

          //---- riSousMenu_bt_reac ----
          riSousMenu_bt_reac.setText("R\u00e9activation");
          riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
          riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
          riSousMenu_reac.add(riSousMenu_bt_reac);
        }
        menus_haut.add(riSousMenu_reac);

        //======== riSousMenu_destr ========
        {
          riSousMenu_destr.setName("riSousMenu_destr");

          //---- riSousMenu_bt_destr ----
          riSousMenu_bt_destr.setText("Suppression");
          riSousMenu_bt_destr.setToolTipText("Suppression");
          riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
          riSousMenu_destr.add(riSousMenu_bt_destr);
        }
        menus_haut.add(riSousMenu_destr);

        //======== riMenu2 ========
        {
          riMenu2.setName("riMenu2");

          //---- riMenu_bt2 ----
          riMenu_bt2.setText("Options");
          riMenu_bt2.setName("riMenu_bt2");
          riMenu2.add(riMenu_bt2);
        }
        menus_haut.add(riMenu2);

        //======== riSousMenu6 ========
        {
          riSousMenu6.setName("riSousMenu6");

          //---- riSousMenu_bt6 ----
          riSousMenu_bt6.setText("text");
          riSousMenu_bt6.setName("riSousMenu_bt6");
          riSousMenu_bt6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt6ActionPerformed(e);
            }
          });
          riSousMenu6.add(riSousMenu_bt6);
        }
        menus_haut.add(riSousMenu6);

        //======== riSousMenu7 ========
        {
          riSousMenu7.setName("riSousMenu7");

          //---- riSousMenu_bt7 ----
          riSousMenu_bt7.setText("text");
          riSousMenu_bt7.setName("riSousMenu_bt7");
          riSousMenu_bt7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt7ActionPerformed(e);
            }
          });
          riSousMenu7.add(riSousMenu_bt7);
        }
        menus_haut.add(riSousMenu7);

        //======== riSousMenu8 ========
        {
          riSousMenu8.setName("riSousMenu8");

          //---- riSousMenu_bt8 ----
          riSousMenu_bt8.setText("text");
          riSousMenu_bt8.setName("riSousMenu_bt8");
          riSousMenu_bt8.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt8ActionPerformed(e);
            }
          });
          riSousMenu8.add(riSousMenu_bt8);
        }
        menus_haut.add(riSousMenu8);

        //======== riSousMenu9 ========
        {
          riSousMenu9.setName("riSousMenu9");

          //---- riSousMenu_bt9 ----
          riSousMenu_bt9.setText("text");
          riSousMenu_bt9.setName("riSousMenu_bt9");
          riSousMenu_bt9.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt9ActionPerformed(e);
            }
          });
          riSousMenu9.add(riSousMenu_bt9);
        }
        menus_haut.add(riSousMenu9);

        //======== riSousMenu10 ========
        {
          riSousMenu10.setName("riSousMenu10");

          //---- riSousMenu_bt10 ----
          riSousMenu_bt10.setText("text");
          riSousMenu_bt10.setName("riSousMenu_bt10");
          riSousMenu_bt10.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt10ActionPerformed(e);
            }
          });
          riSousMenu10.add(riSousMenu_bt10);
        }
        menus_haut.add(riSousMenu10);

        //======== riSousMenu11 ========
        {
          riSousMenu11.setName("riSousMenu11");

          //---- riSousMenu_bt11 ----
          riSousMenu_bt11.setText("text");
          riSousMenu_bt11.setName("riSousMenu_bt11");
          riSousMenu_bt11.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt11ActionPerformed(e);
            }
          });
          riSousMenu11.add(riSousMenu_bt11);
        }
        menus_haut.add(riSousMenu11);

        //======== riSousMenu12 ========
        {
          riSousMenu12.setName("riSousMenu12");

          //---- riSousMenu_bt12 ----
          riSousMenu_bt12.setText("text");
          riSousMenu_bt12.setName("riSousMenu_bt12");
          riSousMenu_bt12.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt12ActionPerformed(e);
            }
          });
          riSousMenu12.add(riSousMenu_bt12);
        }
        menus_haut.add(riSousMenu12);

        //======== riSousMenu13 ========
        {
          riSousMenu13.setName("riSousMenu13");

          //---- riSousMenu_bt13 ----
          riSousMenu_bt13.setText("text");
          riSousMenu_bt13.setName("riSousMenu_bt13");
          riSousMenu_bt13.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt13ActionPerformed(e);
            }
          });
          riSousMenu13.add(riSousMenu_bt13);
        }
        menus_haut.add(riSousMenu13);

        //======== riMenu3 ========
        {
          riMenu3.setName("riMenu3");

          //---- riMenu_bt3 ----
          riMenu_bt3.setText("Outils");
          riMenu_bt3.setName("riMenu_bt3");
          riMenu3.add(riMenu_bt3);
        }
        menus_haut.add(riMenu3);

        //======== riSousMenu14 ========
        {
          riSousMenu14.setName("riSousMenu14");

          //---- riSousMenu_bt14 ----
          riSousMenu_bt14.setText("text");
          riSousMenu_bt14.setName("riSousMenu_bt14");
          riSousMenu_bt14.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt14ActionPerformed(e);
            }
          });
          riSousMenu14.add(riSousMenu_bt14);
        }
        menus_haut.add(riSousMenu14);

        //======== riSousMenu15 ========
        {
          riSousMenu15.setName("riSousMenu15");

          //---- riSousMenu_bt15 ----
          riSousMenu_bt15.setText("text");
          riSousMenu_bt15.setName("riSousMenu_bt15");
          riSousMenu_bt15.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt15ActionPerformed(e);
            }
          });
          riSousMenu15.add(riSousMenu_bt15);
        }
        menus_haut.add(riSousMenu15);

        //======== riSousMenu16 ========
        {
          riSousMenu16.setName("riSousMenu16");

          //---- riSousMenu_bt16 ----
          riSousMenu_bt16.setText("text");
          riSousMenu_bt16.setName("riSousMenu_bt16");
          riSousMenu_bt16.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt16ActionPerformed(e);
            }
          });
          riSousMenu16.add(riSousMenu_bt16);
        }
        menus_haut.add(riSousMenu16);

        //======== riSousMenu17 ========
        {
          riSousMenu17.setName("riSousMenu17");

          //---- riSousMenu_bt17 ----
          riSousMenu_bt17.setText("text");
          riSousMenu_bt17.setName("riSousMenu_bt17");
          riSousMenu_bt17.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt17ActionPerformed(e);
            }
          });
          riSousMenu17.add(riSousMenu_bt17);
        }
        menus_haut.add(riSousMenu17);

        //======== riMenu4 ========
        {
          riMenu4.setName("riMenu4");

          //---- riMenu_bt4 ----
          riMenu_bt4.setText("Fonctions");
          riMenu_bt4.setName("riMenu_bt4");
          riMenu4.add(riMenu_bt4);
        }
        menus_haut.add(riMenu4);

        //======== riSousMenu18 ========
        {
          riSousMenu18.setName("riSousMenu18");

          //---- riSousMenu_bt18 ----
          riSousMenu_bt18.setText("text");
          riSousMenu_bt18.setName("riSousMenu_bt18");
          riSousMenu_bt18.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt18ActionPerformed(e);
            }
          });
          riSousMenu18.add(riSousMenu_bt18);
        }
        menus_haut.add(riSousMenu18);

        //======== riSousMenu19 ========
        {
          riSousMenu19.setName("riSousMenu19");

          //---- riSousMenu_bt19 ----
          riSousMenu_bt19.setText("text");
          riSousMenu_bt19.setName("riSousMenu_bt19");
          riSousMenu_bt19.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt19ActionPerformed(e);
            }
          });
          riSousMenu19.add(riSousMenu_bt19);
        }
        menus_haut.add(riSousMenu19);

        //======== riSousMenu20 ========
        {
          riSousMenu20.setName("riSousMenu20");

          //---- riSousMenu_bt20 ----
          riSousMenu_bt20.setText("text");
          riSousMenu_bt20.setName("riSousMenu_bt20");
          riSousMenu_bt20.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt20ActionPerformed(e);
            }
          });
          riSousMenu20.add(riSousMenu_bt20);
        }
        menus_haut.add(riSousMenu20);

        //======== riSousMenu21 ========
        {
          riSousMenu21.setName("riSousMenu21");

          //---- riSousMenu_bt21 ----
          riSousMenu_bt21.setText("text");
          riSousMenu_bt21.setName("riSousMenu_bt21");
          riSousMenu_bt21.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt21ActionPerformed(e);
            }
          });
          riSousMenu21.add(riSousMenu_bt21);
        }
        menus_haut.add(riSousMenu21);
      }
      scroll_droite.setViewportView(menus_haut);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Affichage historique des comptes analytiques");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_29_OBJ_29 ----
          OBJ_29_OBJ_29.setText("Soci\u00e9t\u00e9");
          OBJ_29_OBJ_29.setName("OBJ_29_OBJ_29");

          //---- DEMETB ----
          DEMETB.setComponentPopupMenu(null);
          DEMETB.setOpaque(false);
          DEMETB.setText("@DEMETB@");
          DEMETB.setName("DEMETB");

          //---- OBJ_31_OBJ_31 ----
          OBJ_31_OBJ_31.setText("Sections");
          OBJ_31_OBJ_31.setName("OBJ_31_OBJ_31");

          //---- DEMSA1 ----
          DEMSA1.setComponentPopupMenu(null);
          DEMSA1.setOpaque(false);
          DEMSA1.setText("@DEMSA1@");
          DEMSA1.setName("DEMSA1");

          //---- DEMSA2 ----
          DEMSA2.setComponentPopupMenu(null);
          DEMSA2.setOpaque(false);
          DEMSA2.setText("@DEMSA2@");
          DEMSA2.setName("DEMSA2");

          //---- OBJ_36_OBJ_36 ----
          OBJ_36_OBJ_36.setText("P\u00e9riode");
          OBJ_36_OBJ_36.setName("OBJ_36_OBJ_36");

          //---- DEMDAD ----
          DEMDAD.setComponentPopupMenu(null);
          DEMDAD.setOpaque(false);
          DEMDAD.setText("@DEMDAD@");
          DEMDAD.setHorizontalAlignment(SwingConstants.CENTER);
          DEMDAD.setName("DEMDAD");

          //---- DEMDAF ----
          DEMDAF.setComponentPopupMenu(null);
          DEMDAF.setOpaque(false);
          DEMDAF.setText("@DEMDAF@");
          DEMDAF.setHorizontalAlignment(SwingConstants.CENTER);
          DEMDAF.setName("DEMDAF");

          //---- OBJ_39_OBJ_39 ----
          OBJ_39_OBJ_39.setText("Comptes");
          OBJ_39_OBJ_39.setName("OBJ_39_OBJ_39");

          //---- DEMCPD ----
          DEMCPD.setComponentPopupMenu(null);
          DEMCPD.setOpaque(false);
          DEMCPD.setText("@DEMCPD@");
          DEMCPD.setName("DEMCPD");

          //---- DEMCPF ----
          DEMCPF.setComponentPopupMenu(null);
          DEMCPF.setOpaque(false);
          DEMCPF.setText("@DEMCPF@");
          DEMCPF.setName("DEMCPF");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_29_OBJ_29, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(50, 50, 50)
                    .addComponent(DEMETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
                .addGap(15, 15, 15)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(55, 55, 55)
                    .addComponent(DEMSA1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_31_OBJ_31, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(DEMSA2, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_36_OBJ_36, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(50, 50, 50)
                    .addComponent(DEMDAD, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)))
                .addGap(3, 3, 3)
                .addComponent(DEMDAF, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addComponent(OBJ_39_OBJ_39, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(DEMCPD, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(DEMCPF, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(DEMETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(DEMSA1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(DEMSA2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(DEMDAD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(DEMDAF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(DEMCPD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(DEMCPF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_29_OBJ_29, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_31_OBJ_31, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_36_OBJ_36, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_39_OBJ_39, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(660, 400));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- V06F ----
          V06F.setToolTipText("<HTML>S\u00e9lectionez le nombre de lignes \u00e0 d\u00e9caler,<BR>et valider.</HTML>");
          V06F.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          V06F.setName("V06F");
          V06F.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              V06FItemStateChanged(e);
            }
          });

          //---- OBJ_56_OBJ_56 ----
          OBJ_56_OBJ_56.setText("@WLIBDV@");
          OBJ_56_OBJ_56.setName("OBJ_56_OBJ_56");

          //---- OBJ_57 ----
          OBJ_57.setText("Debut de liste");
          OBJ_57.setComponentPopupMenu(null);
          OBJ_57.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_57.setName("OBJ_57");
          OBJ_57.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_57ActionPerformed(e);
            }
          });

          //---- OBJ_58 ----
          OBJ_58.setText("Fin de liste");
          OBJ_58.setComponentPopupMenu(null);
          OBJ_58.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_58.setName("OBJ_58");
          OBJ_58.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_58ActionPerformed(e);
            }
          });

          //---- OBJ_55_OBJ_55 ----
          OBJ_55_OBJ_55.setText("Exprim\u00e9 en");
          OBJ_55_OBJ_55.setName("OBJ_55_OBJ_55");

          //---- BT_PGUP ----
          BT_PGUP.setName("BT_PGUP");

          //---- BT_PGDOWN ----
          BT_PGDOWN.setName("BT_PGDOWN");

          //======== scrollPane1 ========
          {
            scrollPane1.setName("scrollPane1");

            //---- LD01 ----
            LD01.setModel(new DefaultTableModel(
              new Object[][] {
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
              },
              new String[] {
                "HLD01"
              }
            ));
            LD01.setComponentPopupMenu(BTD);
            LD01.setName("LD01");
            LD01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01MouseClicked(e);
              }
            });
            scrollPane1.setViewportView(LD01);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(35, 35, 35)
                    .addComponent(OBJ_55_OBJ_55, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE)
                    .addGap(1, 1, 1)
                    .addComponent(OBJ_56_OBJ_56, GroupLayout.PREFERRED_SIZE, 253, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(27, 27, 27)
                    .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(V06F, GroupLayout.PREFERRED_SIZE, 239, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 565, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(p_contenuLayout.createParallelGroup()
                          .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                          .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))))))
                .addGap(37, 37, 37))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(OBJ_55_OBJ_55, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_56_OBJ_56, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                  .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                    .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
                  .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 275, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                  .addComponent(V06F, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Choisir");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- OBJ_10 ----
    OBJ_10.setText("Aide en ligne");
    OBJ_10.setName("OBJ_10");
    OBJ_10.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_10ActionPerformed(e);
      }
    });
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiSousMenu riSousMenu13;
  private RiSousMenu_bt riSousMenu_bt13;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private RiSousMenu riSousMenu16;
  private RiSousMenu_bt riSousMenu_bt16;
  private RiSousMenu riSousMenu17;
  private RiSousMenu_bt riSousMenu_bt17;
  private RiMenu riMenu4;
  private RiMenu_bt riMenu_bt4;
  private RiSousMenu riSousMenu18;
  private RiSousMenu_bt riSousMenu_bt18;
  private RiSousMenu riSousMenu19;
  private RiSousMenu_bt riSousMenu_bt19;
  private RiSousMenu riSousMenu20;
  private RiSousMenu_bt riSousMenu_bt20;
  private RiSousMenu riSousMenu21;
  private RiSousMenu_bt riSousMenu_bt21;
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_29_OBJ_29;
  private RiZoneSortie DEMETB;
  private JLabel OBJ_31_OBJ_31;
  private RiZoneSortie DEMSA1;
  private RiZoneSortie DEMSA2;
  private JLabel OBJ_36_OBJ_36;
  private RiZoneSortie DEMDAD;
  private RiZoneSortie DEMDAF;
  private JLabel OBJ_39_OBJ_39;
  private RiZoneSortie DEMCPD;
  private RiZoneSortie DEMCPF;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private XRiComboBox V06F;
  private RiZoneSortie OBJ_56_OBJ_56;
  private SNBoutonLeger OBJ_57;
  private SNBoutonLeger OBJ_58;
  private JLabel OBJ_55_OBJ_55;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JScrollPane scrollPane1;
  private XRiTable LD01;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
