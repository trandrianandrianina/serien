
package ri.serien.libecranrpg.vcgm.VCGM11FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM11FM_A3 extends SNPanelEcranRPG implements ioFrame {
  
   
  ImageIcon imageErr = null;
  
  public VCGM11FM_A3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    // setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    
    imageErr = lexique.chargerImage("images/err.png", true);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WISOC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WISOC@")).trim());
    WICJO.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WICJO@")).trim());
    WICFO.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WICFO@")).trim());
    WIDTEX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WIDTEX@")).trim());
    WCET.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCET@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMTR@")).trim());
    E1TDBR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1TDBR@")).trim());
    E1TCRR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1TCRR@")).trim());
    WSOL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSOL@")).trim());
    E1TDB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1TDB@")).trim());
    E1TCR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1TCR@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    

    
    


    
    
    WICJO.setVisible(lexique.isPresent("WICJO"));
    WICFO.setVisible(lexique.isPresent("WICFO"));
    WCET.setEnabled(lexique.isPresent("WCET"));
    WISOC.setVisible(lexique.isPresent("WISOC"));
    WIDTEX.setEnabled(lexique.isPresent("WIDTEX"));
    WSOL.setVisible(lexique.isTrue("(N78) AND (40)"));
    OBJ_70_OBJ_70.setVisible(WSOL.isVisible());
    E1TDB.setVisible(lexique.isTrue("(N87) AND (N78)"));
    E1TDBR.setVisible(!E1TDB.isVisible());
    E1TCR.setVisible(lexique.isTrue("(N87) AND (N78)"));
    E1TCRR.setVisible(!E1TCR.isVisible());
    
    err1.setVisible(lexique.isTrue("25"));
    err2.setVisible(lexique.isTrue("26"));
    err3.setVisible(lexique.isTrue("27"));
    err4.setVisible(lexique.isTrue("28"));
    err5.setVisible(lexique.isTrue("29"));
    err6.setVisible(lexique.isTrue("30"));
    err7.setVisible(lexique.isTrue("31"));
    err8.setVisible(lexique.isTrue("32"));
    err9.setVisible(lexique.isTrue("33"));
    err10.setVisible(lexique.isTrue("34"));
    err11.setVisible(lexique.isTrue("35"));
    err12.setVisible(lexique.isTrue("36"));
    err13.setVisible(lexique.isTrue("37"));
    err14.setVisible(lexique.isTrue("38"));
    
    err1.setIcon(imageErr);
    err2.setIcon(imageErr);
    err3.setIcon(imageErr);
    err4.setIcon(imageErr);
    err5.setIcon(imageErr);
    err6.setIcon(imageErr);
    err7.setIcon(imageErr);
    err8.setIcon(imageErr);
    err9.setIcon(imageErr);
    err10.setIcon(imageErr);
    err11.setIcon(imageErr);
    err12.setIcon(imageErr);
    err13.setIcon(imageErr);
    err14.setIcon(imageErr);
    
    

    
    p_bpresentation.setCodeEtablissement(WISOC.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WISOC.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_36_OBJ_36 = new JLabel();
    WISOC = new RiZoneSortie();
    OBJ_38_OBJ_38 = new JLabel();
    WICJO = new RiZoneSortie();
    OBJ_40_OBJ_40 = new JLabel();
    WICFO = new RiZoneSortie();
    OBJ_76_OBJ_76 = new JLabel();
    WIDTEX = new RiZoneSortie();
    OBJ_77_OBJ_77 = new JLabel();
    WCET = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel3 = new JPanel();
    label1 = new JLabel();
    ETPC01 = new XRiTextField();
    ETPC02 = new XRiTextField();
    ETPC03 = new XRiTextField();
    ETPC04 = new XRiTextField();
    ETPC05 = new XRiTextField();
    ETPC06 = new XRiTextField();
    ETPC07 = new XRiTextField();
    ETPC08 = new XRiTextField();
    ETPC09 = new XRiTextField();
    ETPC10 = new XRiTextField();
    ETPC11 = new XRiTextField();
    ETPC12 = new XRiTextField();
    ETPC13 = new XRiTextField();
    ETPC14 = new XRiTextField();
    label2 = new JLabel();
    ETLI01 = new XRiTextField();
    ETLI02 = new XRiTextField();
    ETLI03 = new XRiTextField();
    ETLI04 = new XRiTextField();
    ETLI05 = new XRiTextField();
    ETLI06 = new XRiTextField();
    ETLI07 = new XRiTextField();
    ETLI08 = new XRiTextField();
    ETLI09 = new XRiTextField();
    ETLI10 = new XRiTextField();
    ETLI11 = new XRiTextField();
    ETLI12 = new XRiTextField();
    ETLI13 = new XRiTextField();
    ETLI14 = new XRiTextField();
    label3 = new JLabel();
    label4 = new JLabel();
    ETAC01 = new XRiTextField();
    ETAC02 = new XRiTextField();
    ETAC03 = new XRiTextField();
    ETAC04 = new XRiTextField();
    ETAC05 = new XRiTextField();
    ETAC06 = new XRiTextField();
    ETAC07 = new XRiTextField();
    ETAC08 = new XRiTextField();
    ETAC09 = new XRiTextField();
    ETAC10 = new XRiTextField();
    ETAC11 = new XRiTextField();
    ETAC12 = new XRiTextField();
    ETAC13 = new XRiTextField();
    ETAC14 = new XRiTextField();
    ETSA01 = new XRiTextField();
    label5 = new JLabel();
    ETSA02 = new XRiTextField();
    ETSA03 = new XRiTextField();
    ETSA04 = new XRiTextField();
    ETSA05 = new XRiTextField();
    ETSA06 = new XRiTextField();
    ETSA07 = new XRiTextField();
    ETSA08 = new XRiTextField();
    ETSA09 = new XRiTextField();
    ETSA10 = new XRiTextField();
    ETSA11 = new XRiTextField();
    ETSA12 = new XRiTextField();
    ETSA13 = new XRiTextField();
    ETSA14 = new XRiTextField();
    ETCL01 = new XRiTextField();
    label6 = new JLabel();
    label10 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    ETCL02 = new XRiTextField();
    ETCL03 = new XRiTextField();
    ETCL04 = new XRiTextField();
    ETCL05 = new XRiTextField();
    ETCL06 = new XRiTextField();
    ETCL07 = new XRiTextField();
    ETCL08 = new XRiTextField();
    ETCL09 = new XRiTextField();
    ETCL10 = new XRiTextField();
    ETCL11 = new XRiTextField();
    ETCL12 = new XRiTextField();
    ETCL13 = new XRiTextField();
    ETCL14 = new XRiTextField();
    ETSS01 = new XRiTextField();
    ETSS02 = new XRiTextField();
    ETSS03 = new XRiTextField();
    ETSS04 = new XRiTextField();
    ETSS05 = new XRiTextField();
    ETSS06 = new XRiTextField();
    ETSS07 = new XRiTextField();
    ETSS08 = new XRiTextField();
    ETSS09 = new XRiTextField();
    ETSS10 = new XRiTextField();
    ETSS11 = new XRiTextField();
    ETSS12 = new XRiTextField();
    ETSS13 = new XRiTextField();
    ETSS14 = new XRiTextField();
    ETNG01 = new XRiTextField();
    ETNG02 = new XRiTextField();
    ETNG03 = new XRiTextField();
    ETNG04 = new XRiTextField();
    ETNG05 = new XRiTextField();
    ETNG06 = new XRiTextField();
    ETNG07 = new XRiTextField();
    ETNG08 = new XRiTextField();
    ETNG09 = new XRiTextField();
    ETNG10 = new XRiTextField();
    ETNG11 = new XRiTextField();
    ETNG12 = new XRiTextField();
    ETNG13 = new XRiTextField();
    ETNG14 = new XRiTextField();
    ETNA01 = new XRiTextField();
    ETNA02 = new XRiTextField();
    ETNA03 = new XRiTextField();
    ETNA04 = new XRiTextField();
    ETNA05 = new XRiTextField();
    ETNA06 = new XRiTextField();
    ETNA07 = new XRiTextField();
    ETNA08 = new XRiTextField();
    ETNA09 = new XRiTextField();
    ETNA10 = new XRiTextField();
    ETNA11 = new XRiTextField();
    ETNA12 = new XRiTextField();
    ETNA13 = new XRiTextField();
    ETNA14 = new XRiTextField();
    err1 = new JLabel();
    err2 = new JLabel();
    err3 = new JLabel();
    err4 = new JLabel();
    err5 = new JLabel();
    err6 = new JLabel();
    err7 = new JLabel();
    err8 = new JLabel();
    err9 = new JLabel();
    err10 = new JLabel();
    err11 = new JLabel();
    err13 = new JLabel();
    err14 = new JLabel();
    err12 = new JLabel();
    panel1 = new JPanel();
    ETMT01 = new XRiTextField();
    ETMT02 = new XRiTextField();
    ETMT03 = new XRiTextField();
    ETMT04 = new XRiTextField();
    ETMT05 = new XRiTextField();
    ETMT06 = new XRiTextField();
    ETMT07 = new XRiTextField();
    ETMT08 = new XRiTextField();
    ETMT09 = new XRiTextField();
    ETMT10 = new XRiTextField();
    ETMT11 = new XRiTextField();
    ETMT12 = new XRiTextField();
    ETMT13 = new XRiTextField();
    ETMT14 = new XRiTextField();
    panel2 = new JPanel();
    OBJ_70_OBJ_70 = new JLabel();
    E1TDBR = new RiZoneSortie();
    E1TCRR = new RiZoneSortie();
    WSOL = new RiZoneSortie();
    OBJ_71_OBJ_71 = new JLabel();
    OBJ_67_OBJ_67 = new JLabel();
    E1TDB = new RiZoneSortie();
    E1TCR = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_8 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Folio de comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_36_OBJ_36 ----
          OBJ_36_OBJ_36.setText("Soci\u00e9t\u00e9");
          OBJ_36_OBJ_36.setName("OBJ_36_OBJ_36");

          //---- WISOC ----
          WISOC.setComponentPopupMenu(null);
          WISOC.setOpaque(false);
          WISOC.setText("@WISOC@");
          WISOC.setName("WISOC");

          //---- OBJ_38_OBJ_38 ----
          OBJ_38_OBJ_38.setText("Journal");
          OBJ_38_OBJ_38.setName("OBJ_38_OBJ_38");

          //---- WICJO ----
          WICJO.setComponentPopupMenu(null);
          WICJO.setOpaque(false);
          WICJO.setText("@WICJO@");
          WICJO.setName("WICJO");

          //---- OBJ_40_OBJ_40 ----
          OBJ_40_OBJ_40.setText("Folio");
          OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");

          //---- WICFO ----
          WICFO.setComponentPopupMenu(null);
          WICFO.setOpaque(false);
          WICFO.setText("@WICFO@");
          WICFO.setName("WICFO");

          //---- OBJ_76_OBJ_76 ----
          OBJ_76_OBJ_76.setText("Date");
          OBJ_76_OBJ_76.setName("OBJ_76_OBJ_76");

          //---- WIDTEX ----
          WIDTEX.setComponentPopupMenu(null);
          WIDTEX.setOpaque(false);
          WIDTEX.setText("@WIDTEX@");
          WIDTEX.setHorizontalAlignment(SwingConstants.CENTER);
          WIDTEX.setName("WIDTEX");

          //---- OBJ_77_OBJ_77 ----
          OBJ_77_OBJ_77.setText("Type");
          OBJ_77_OBJ_77.setName("OBJ_77_OBJ_77");

          //---- WCET ----
          WCET.setComponentPopupMenu(null);
          WCET.setOpaque(false);
          WCET.setText("@WCET@");
          WCET.setName("WCET");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(50, 50, 50)
                    .addComponent(WISOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_36_OBJ_36, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addComponent(OBJ_38_OBJ_38, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(WICJO, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(35, 35, 35)
                    .addComponent(WICFO, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_40_OBJ_40, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_76_OBJ_76, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(35, 35, 35)
                    .addComponent(WIDTEX, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)))
                .addGap(20, 20, 20)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_77_OBJ_77, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(55, 55, 55)
                    .addComponent(WCET, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(WISOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WICJO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WICFO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WIDTEX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WCET, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_36_OBJ_36, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_38_OBJ_38, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_40_OBJ_40, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_76_OBJ_76, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_77_OBJ_77, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("G\u00e9n\u00e9ration folio");
              riSousMenu_bt7.setToolTipText("G\u00e9n\u00e9ration folio");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Comptes de contrepartie");
              riSousMenu_bt8.setToolTipText("On/off affichage comptes de contrepartie");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(880, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder("Saisie \u00e9criture Type"));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- label1 ----
            label1.setText("@LIBMTR@");
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
            label1.setHorizontalAlignment(SwingConstants.CENTER);
            label1.setName("label1");
            panel3.add(label1);
            label1.setBounds(30, 30, 90, 21);

            //---- ETPC01 ----
            ETPC01.setName("ETPC01");
            panel3.add(ETPC01);
            ETPC01.setBounds(125, 55, 70, ETPC01.getPreferredSize().height);

            //---- ETPC02 ----
            ETPC02.setName("ETPC02");
            panel3.add(ETPC02);
            ETPC02.setBounds(125, 80, 70, ETPC02.getPreferredSize().height);

            //---- ETPC03 ----
            ETPC03.setName("ETPC03");
            panel3.add(ETPC03);
            ETPC03.setBounds(125, 105, 70, ETPC03.getPreferredSize().height);

            //---- ETPC04 ----
            ETPC04.setName("ETPC04");
            panel3.add(ETPC04);
            ETPC04.setBounds(125, 130, 70, ETPC04.getPreferredSize().height);

            //---- ETPC05 ----
            ETPC05.setName("ETPC05");
            panel3.add(ETPC05);
            ETPC05.setBounds(125, 155, 70, ETPC05.getPreferredSize().height);

            //---- ETPC06 ----
            ETPC06.setName("ETPC06");
            panel3.add(ETPC06);
            ETPC06.setBounds(125, 180, 70, ETPC06.getPreferredSize().height);

            //---- ETPC07 ----
            ETPC07.setName("ETPC07");
            panel3.add(ETPC07);
            ETPC07.setBounds(125, 205, 70, ETPC07.getPreferredSize().height);

            //---- ETPC08 ----
            ETPC08.setName("ETPC08");
            panel3.add(ETPC08);
            ETPC08.setBounds(125, 230, 70, ETPC08.getPreferredSize().height);

            //---- ETPC09 ----
            ETPC09.setName("ETPC09");
            panel3.add(ETPC09);
            ETPC09.setBounds(125, 255, 70, ETPC09.getPreferredSize().height);

            //---- ETPC10 ----
            ETPC10.setName("ETPC10");
            panel3.add(ETPC10);
            ETPC10.setBounds(125, 280, 70, ETPC10.getPreferredSize().height);

            //---- ETPC11 ----
            ETPC11.setName("ETPC11");
            panel3.add(ETPC11);
            ETPC11.setBounds(125, 305, 70, ETPC11.getPreferredSize().height);

            //---- ETPC12 ----
            ETPC12.setName("ETPC12");
            panel3.add(ETPC12);
            ETPC12.setBounds(125, 330, 70, ETPC12.getPreferredSize().height);

            //---- ETPC13 ----
            ETPC13.setName("ETPC13");
            panel3.add(ETPC13);
            ETPC13.setBounds(125, 355, 70, ETPC13.getPreferredSize().height);

            //---- ETPC14 ----
            ETPC14.setName("ETPC14");
            panel3.add(ETPC14);
            ETPC14.setBounds(125, 380, 70, ETPC14.getPreferredSize().height);

            //---- label2 ----
            label2.setText("N\u00b0 pi\u00e8ce");
            label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
            label2.setHorizontalAlignment(SwingConstants.CENTER);
            label2.setName("label2");
            panel3.add(label2);
            label2.setBounds(125, 30, 70, 21);

            //---- ETLI01 ----
            ETLI01.setName("ETLI01");
            panel3.add(ETLI01);
            ETLI01.setBounds(200, 55, 270, ETLI01.getPreferredSize().height);

            //---- ETLI02 ----
            ETLI02.setName("ETLI02");
            panel3.add(ETLI02);
            ETLI02.setBounds(200, 80, 270, ETLI02.getPreferredSize().height);

            //---- ETLI03 ----
            ETLI03.setName("ETLI03");
            panel3.add(ETLI03);
            ETLI03.setBounds(200, 105, 270, ETLI03.getPreferredSize().height);

            //---- ETLI04 ----
            ETLI04.setName("ETLI04");
            panel3.add(ETLI04);
            ETLI04.setBounds(200, 130, 270, ETLI04.getPreferredSize().height);

            //---- ETLI05 ----
            ETLI05.setName("ETLI05");
            panel3.add(ETLI05);
            ETLI05.setBounds(200, 155, 270, ETLI05.getPreferredSize().height);

            //---- ETLI06 ----
            ETLI06.setName("ETLI06");
            panel3.add(ETLI06);
            ETLI06.setBounds(200, 180, 270, ETLI06.getPreferredSize().height);

            //---- ETLI07 ----
            ETLI07.setName("ETLI07");
            panel3.add(ETLI07);
            ETLI07.setBounds(200, 205, 270, ETLI07.getPreferredSize().height);

            //---- ETLI08 ----
            ETLI08.setName("ETLI08");
            panel3.add(ETLI08);
            ETLI08.setBounds(200, 230, 270, ETLI08.getPreferredSize().height);

            //---- ETLI09 ----
            ETLI09.setName("ETLI09");
            panel3.add(ETLI09);
            ETLI09.setBounds(200, 255, 270, ETLI09.getPreferredSize().height);

            //---- ETLI10 ----
            ETLI10.setName("ETLI10");
            panel3.add(ETLI10);
            ETLI10.setBounds(200, 280, 270, ETLI10.getPreferredSize().height);

            //---- ETLI11 ----
            ETLI11.setName("ETLI11");
            panel3.add(ETLI11);
            ETLI11.setBounds(200, 305, 270, ETLI11.getPreferredSize().height);

            //---- ETLI12 ----
            ETLI12.setName("ETLI12");
            panel3.add(ETLI12);
            ETLI12.setBounds(200, 330, 270, ETLI12.getPreferredSize().height);

            //---- ETLI13 ----
            ETLI13.setName("ETLI13");
            panel3.add(ETLI13);
            ETLI13.setBounds(200, 355, 270, ETLI13.getPreferredSize().height);

            //---- ETLI14 ----
            ETLI14.setName("ETLI14");
            panel3.add(ETLI14);
            ETLI14.setBounds(200, 380, 270, ETLI14.getPreferredSize().height);

            //---- label3 ----
            label3.setText("Libell\u00e9");
            label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
            label3.setHorizontalAlignment(SwingConstants.LEFT);
            label3.setName("label3");
            panel3.add(label3);
            label3.setBounds(205, 30, 260, 21);

            //---- label4 ----
            label4.setText("Affaires");
            label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
            label4.setHorizontalAlignment(SwingConstants.CENTER);
            label4.setName("label4");
            panel3.add(label4);
            label4.setBounds(475, 30, 60, 21);

            //---- ETAC01 ----
            ETAC01.setComponentPopupMenu(BTD);
            ETAC01.setName("ETAC01");
            panel3.add(ETAC01);
            ETAC01.setBounds(475, 55, 60, ETAC01.getPreferredSize().height);

            //---- ETAC02 ----
            ETAC02.setComponentPopupMenu(BTD);
            ETAC02.setName("ETAC02");
            panel3.add(ETAC02);
            ETAC02.setBounds(475, 80, 60, ETAC02.getPreferredSize().height);

            //---- ETAC03 ----
            ETAC03.setComponentPopupMenu(BTD);
            ETAC03.setName("ETAC03");
            panel3.add(ETAC03);
            ETAC03.setBounds(475, 105, 60, ETAC03.getPreferredSize().height);

            //---- ETAC04 ----
            ETAC04.setComponentPopupMenu(BTD);
            ETAC04.setName("ETAC04");
            panel3.add(ETAC04);
            ETAC04.setBounds(475, 130, 60, ETAC04.getPreferredSize().height);

            //---- ETAC05 ----
            ETAC05.setComponentPopupMenu(BTD);
            ETAC05.setName("ETAC05");
            panel3.add(ETAC05);
            ETAC05.setBounds(475, 155, 60, ETAC05.getPreferredSize().height);

            //---- ETAC06 ----
            ETAC06.setComponentPopupMenu(BTD);
            ETAC06.setName("ETAC06");
            panel3.add(ETAC06);
            ETAC06.setBounds(475, 180, 60, ETAC06.getPreferredSize().height);

            //---- ETAC07 ----
            ETAC07.setComponentPopupMenu(BTD);
            ETAC07.setName("ETAC07");
            panel3.add(ETAC07);
            ETAC07.setBounds(475, 205, 60, ETAC07.getPreferredSize().height);

            //---- ETAC08 ----
            ETAC08.setComponentPopupMenu(BTD);
            ETAC08.setName("ETAC08");
            panel3.add(ETAC08);
            ETAC08.setBounds(475, 230, 60, ETAC08.getPreferredSize().height);

            //---- ETAC09 ----
            ETAC09.setComponentPopupMenu(BTD);
            ETAC09.setName("ETAC09");
            panel3.add(ETAC09);
            ETAC09.setBounds(475, 255, 60, ETAC09.getPreferredSize().height);

            //---- ETAC10 ----
            ETAC10.setComponentPopupMenu(BTD);
            ETAC10.setName("ETAC10");
            panel3.add(ETAC10);
            ETAC10.setBounds(475, 280, 60, ETAC10.getPreferredSize().height);

            //---- ETAC11 ----
            ETAC11.setComponentPopupMenu(BTD);
            ETAC11.setName("ETAC11");
            panel3.add(ETAC11);
            ETAC11.setBounds(475, 305, 60, ETAC11.getPreferredSize().height);

            //---- ETAC12 ----
            ETAC12.setComponentPopupMenu(BTD);
            ETAC12.setName("ETAC12");
            panel3.add(ETAC12);
            ETAC12.setBounds(475, 330, 60, ETAC12.getPreferredSize().height);

            //---- ETAC13 ----
            ETAC13.setComponentPopupMenu(BTD);
            ETAC13.setName("ETAC13");
            panel3.add(ETAC13);
            ETAC13.setBounds(475, 355, 60, ETAC13.getPreferredSize().height);

            //---- ETAC14 ----
            ETAC14.setComponentPopupMenu(BTD);
            ETAC14.setName("ETAC14");
            panel3.add(ETAC14);
            ETAC14.setBounds(475, 380, 60, ETAC14.getPreferredSize().height);

            //---- ETSA01 ----
            ETSA01.setComponentPopupMenu(BTD);
            ETSA01.setName("ETSA01");
            panel3.add(ETSA01);
            ETSA01.setBounds(540, 55, 50, ETSA01.getPreferredSize().height);

            //---- label5 ----
            label5.setText("Section");
            label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
            label5.setHorizontalAlignment(SwingConstants.CENTER);
            label5.setName("label5");
            panel3.add(label5);
            label5.setBounds(540, 30, 50, 21);

            //---- ETSA02 ----
            ETSA02.setComponentPopupMenu(BTD);
            ETSA02.setName("ETSA02");
            panel3.add(ETSA02);
            ETSA02.setBounds(540, 80, 50, ETSA02.getPreferredSize().height);

            //---- ETSA03 ----
            ETSA03.setComponentPopupMenu(BTD);
            ETSA03.setName("ETSA03");
            panel3.add(ETSA03);
            ETSA03.setBounds(540, 105, 50, ETSA03.getPreferredSize().height);

            //---- ETSA04 ----
            ETSA04.setComponentPopupMenu(BTD);
            ETSA04.setName("ETSA04");
            panel3.add(ETSA04);
            ETSA04.setBounds(540, 130, 50, ETSA04.getPreferredSize().height);

            //---- ETSA05 ----
            ETSA05.setComponentPopupMenu(BTD);
            ETSA05.setName("ETSA05");
            panel3.add(ETSA05);
            ETSA05.setBounds(540, 155, 50, ETSA05.getPreferredSize().height);

            //---- ETSA06 ----
            ETSA06.setComponentPopupMenu(BTD);
            ETSA06.setName("ETSA06");
            panel3.add(ETSA06);
            ETSA06.setBounds(540, 180, 50, ETSA06.getPreferredSize().height);

            //---- ETSA07 ----
            ETSA07.setComponentPopupMenu(BTD);
            ETSA07.setName("ETSA07");
            panel3.add(ETSA07);
            ETSA07.setBounds(540, 205, 50, ETSA07.getPreferredSize().height);

            //---- ETSA08 ----
            ETSA08.setComponentPopupMenu(BTD);
            ETSA08.setName("ETSA08");
            panel3.add(ETSA08);
            ETSA08.setBounds(540, 230, 50, ETSA08.getPreferredSize().height);

            //---- ETSA09 ----
            ETSA09.setComponentPopupMenu(BTD);
            ETSA09.setName("ETSA09");
            panel3.add(ETSA09);
            ETSA09.setBounds(540, 255, 50, ETSA09.getPreferredSize().height);

            //---- ETSA10 ----
            ETSA10.setComponentPopupMenu(BTD);
            ETSA10.setName("ETSA10");
            panel3.add(ETSA10);
            ETSA10.setBounds(540, 280, 50, ETSA10.getPreferredSize().height);

            //---- ETSA11 ----
            ETSA11.setComponentPopupMenu(BTD);
            ETSA11.setName("ETSA11");
            panel3.add(ETSA11);
            ETSA11.setBounds(540, 305, 50, ETSA11.getPreferredSize().height);

            //---- ETSA12 ----
            ETSA12.setComponentPopupMenu(BTD);
            ETSA12.setName("ETSA12");
            panel3.add(ETSA12);
            ETSA12.setBounds(540, 330, 50, ETSA12.getPreferredSize().height);

            //---- ETSA13 ----
            ETSA13.setComponentPopupMenu(BTD);
            ETSA13.setName("ETSA13");
            panel3.add(ETSA13);
            ETSA13.setBounds(540, 355, 50, ETSA13.getPreferredSize().height);

            //---- ETSA14 ----
            ETSA14.setComponentPopupMenu(BTD);
            ETSA14.setName("ETSA14");
            panel3.add(ETSA14);
            ETSA14.setBounds(540, 380, 50, ETSA14.getPreferredSize().height);

            //---- ETCL01 ----
            ETCL01.setName("ETCL01");
            panel3.add(ETCL01);
            ETCL01.setBounds(595, 55, 24, ETCL01.getPreferredSize().height);

            //---- label6 ----
            label6.setText("C");
            label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
            label6.setHorizontalAlignment(SwingConstants.CENTER);
            label6.setName("label6");
            panel3.add(label6);
            label6.setBounds(595, 30, 20, 21);

            //---- label10 ----
            label10.setText("S");
            label10.setFont(label10.getFont().deriveFont(label10.getFont().getStyle() | Font.BOLD));
            label10.setHorizontalAlignment(SwingConstants.CENTER);
            label10.setName("label10");
            panel3.add(label10);
            label10.setBounds(625, 30, 20, 21);

            //---- label8 ----
            label8.setText("Compte");
            label8.setFont(label8.getFont().deriveFont(label8.getFont().getStyle() | Font.BOLD));
            label8.setHorizontalAlignment(SwingConstants.CENTER);
            label8.setName("label8");
            panel3.add(label8);
            label8.setBounds(650, 30, 60, 21);

            //---- label9 ----
            label9.setText("Tiers");
            label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD));
            label9.setHorizontalAlignment(SwingConstants.CENTER);
            label9.setName("label9");
            panel3.add(label9);
            label9.setBounds(710, 30, 60, 21);

            //---- ETCL02 ----
            ETCL02.setName("ETCL02");
            panel3.add(ETCL02);
            ETCL02.setBounds(595, 80, 24, ETCL02.getPreferredSize().height);

            //---- ETCL03 ----
            ETCL03.setName("ETCL03");
            panel3.add(ETCL03);
            ETCL03.setBounds(595, 105, 24, ETCL03.getPreferredSize().height);

            //---- ETCL04 ----
            ETCL04.setName("ETCL04");
            panel3.add(ETCL04);
            ETCL04.setBounds(595, 130, 24, ETCL04.getPreferredSize().height);

            //---- ETCL05 ----
            ETCL05.setName("ETCL05");
            panel3.add(ETCL05);
            ETCL05.setBounds(595, 155, 24, ETCL05.getPreferredSize().height);

            //---- ETCL06 ----
            ETCL06.setName("ETCL06");
            panel3.add(ETCL06);
            ETCL06.setBounds(595, 180, 24, ETCL06.getPreferredSize().height);

            //---- ETCL07 ----
            ETCL07.setName("ETCL07");
            panel3.add(ETCL07);
            ETCL07.setBounds(595, 205, 24, ETCL07.getPreferredSize().height);

            //---- ETCL08 ----
            ETCL08.setName("ETCL08");
            panel3.add(ETCL08);
            ETCL08.setBounds(595, 230, 24, ETCL08.getPreferredSize().height);

            //---- ETCL09 ----
            ETCL09.setName("ETCL09");
            panel3.add(ETCL09);
            ETCL09.setBounds(595, 255, 24, ETCL09.getPreferredSize().height);

            //---- ETCL10 ----
            ETCL10.setName("ETCL10");
            panel3.add(ETCL10);
            ETCL10.setBounds(595, 280, 24, ETCL10.getPreferredSize().height);

            //---- ETCL11 ----
            ETCL11.setName("ETCL11");
            panel3.add(ETCL11);
            ETCL11.setBounds(595, 305, 24, ETCL11.getPreferredSize().height);

            //---- ETCL12 ----
            ETCL12.setName("ETCL12");
            panel3.add(ETCL12);
            ETCL12.setBounds(595, 330, 24, ETCL12.getPreferredSize().height);

            //---- ETCL13 ----
            ETCL13.setName("ETCL13");
            panel3.add(ETCL13);
            ETCL13.setBounds(595, 355, 24, ETCL13.getPreferredSize().height);

            //---- ETCL14 ----
            ETCL14.setName("ETCL14");
            panel3.add(ETCL14);
            ETCL14.setBounds(595, 380, 24, ETCL14.getPreferredSize().height);

            //---- ETSS01 ----
            ETSS01.setName("ETSS01");
            panel3.add(ETSS01);
            ETSS01.setBounds(625, 55, 24, ETSS01.getPreferredSize().height);

            //---- ETSS02 ----
            ETSS02.setName("ETSS02");
            panel3.add(ETSS02);
            ETSS02.setBounds(625, 80, 24, ETSS02.getPreferredSize().height);

            //---- ETSS03 ----
            ETSS03.setName("ETSS03");
            panel3.add(ETSS03);
            ETSS03.setBounds(625, 105, 24, ETSS03.getPreferredSize().height);

            //---- ETSS04 ----
            ETSS04.setName("ETSS04");
            panel3.add(ETSS04);
            ETSS04.setBounds(625, 130, 24, ETSS04.getPreferredSize().height);

            //---- ETSS05 ----
            ETSS05.setName("ETSS05");
            panel3.add(ETSS05);
            ETSS05.setBounds(625, 155, 24, ETSS05.getPreferredSize().height);

            //---- ETSS06 ----
            ETSS06.setName("ETSS06");
            panel3.add(ETSS06);
            ETSS06.setBounds(625, 180, 24, ETSS06.getPreferredSize().height);

            //---- ETSS07 ----
            ETSS07.setName("ETSS07");
            panel3.add(ETSS07);
            ETSS07.setBounds(625, 205, 24, ETSS07.getPreferredSize().height);

            //---- ETSS08 ----
            ETSS08.setName("ETSS08");
            panel3.add(ETSS08);
            ETSS08.setBounds(625, 230, 24, ETSS08.getPreferredSize().height);

            //---- ETSS09 ----
            ETSS09.setName("ETSS09");
            panel3.add(ETSS09);
            ETSS09.setBounds(625, 255, 24, ETSS09.getPreferredSize().height);

            //---- ETSS10 ----
            ETSS10.setName("ETSS10");
            panel3.add(ETSS10);
            ETSS10.setBounds(625, 280, 24, ETSS10.getPreferredSize().height);

            //---- ETSS11 ----
            ETSS11.setName("ETSS11");
            panel3.add(ETSS11);
            ETSS11.setBounds(625, 305, 24, ETSS11.getPreferredSize().height);

            //---- ETSS12 ----
            ETSS12.setName("ETSS12");
            panel3.add(ETSS12);
            ETSS12.setBounds(625, 330, 24, ETSS12.getPreferredSize().height);

            //---- ETSS13 ----
            ETSS13.setName("ETSS13");
            panel3.add(ETSS13);
            ETSS13.setBounds(625, 355, 24, ETSS13.getPreferredSize().height);

            //---- ETSS14 ----
            ETSS14.setName("ETSS14");
            panel3.add(ETSS14);
            ETSS14.setBounds(625, 380, 24, ETSS14.getPreferredSize().height);

            //---- ETNG01 ----
            ETNG01.setComponentPopupMenu(BTD);
            ETNG01.setName("ETNG01");
            panel3.add(ETNG01);
            ETNG01.setBounds(655, 55, 60, ETNG01.getPreferredSize().height);

            //---- ETNG02 ----
            ETNG02.setComponentPopupMenu(BTD);
            ETNG02.setName("ETNG02");
            panel3.add(ETNG02);
            ETNG02.setBounds(655, 80, 60, ETNG02.getPreferredSize().height);

            //---- ETNG03 ----
            ETNG03.setComponentPopupMenu(BTD);
            ETNG03.setName("ETNG03");
            panel3.add(ETNG03);
            ETNG03.setBounds(655, 105, 60, ETNG03.getPreferredSize().height);

            //---- ETNG04 ----
            ETNG04.setComponentPopupMenu(BTD);
            ETNG04.setName("ETNG04");
            panel3.add(ETNG04);
            ETNG04.setBounds(655, 130, 60, ETNG04.getPreferredSize().height);

            //---- ETNG05 ----
            ETNG05.setComponentPopupMenu(BTD);
            ETNG05.setName("ETNG05");
            panel3.add(ETNG05);
            ETNG05.setBounds(655, 155, 60, ETNG05.getPreferredSize().height);

            //---- ETNG06 ----
            ETNG06.setComponentPopupMenu(BTD);
            ETNG06.setName("ETNG06");
            panel3.add(ETNG06);
            ETNG06.setBounds(655, 180, 60, ETNG06.getPreferredSize().height);

            //---- ETNG07 ----
            ETNG07.setComponentPopupMenu(BTD);
            ETNG07.setName("ETNG07");
            panel3.add(ETNG07);
            ETNG07.setBounds(655, 205, 60, ETNG07.getPreferredSize().height);

            //---- ETNG08 ----
            ETNG08.setComponentPopupMenu(BTD);
            ETNG08.setName("ETNG08");
            panel3.add(ETNG08);
            ETNG08.setBounds(655, 230, 60, ETNG08.getPreferredSize().height);

            //---- ETNG09 ----
            ETNG09.setComponentPopupMenu(BTD);
            ETNG09.setName("ETNG09");
            panel3.add(ETNG09);
            ETNG09.setBounds(655, 255, 60, ETNG09.getPreferredSize().height);

            //---- ETNG10 ----
            ETNG10.setComponentPopupMenu(BTD);
            ETNG10.setName("ETNG10");
            panel3.add(ETNG10);
            ETNG10.setBounds(655, 280, 60, ETNG10.getPreferredSize().height);

            //---- ETNG11 ----
            ETNG11.setComponentPopupMenu(BTD);
            ETNG11.setName("ETNG11");
            panel3.add(ETNG11);
            ETNG11.setBounds(655, 305, 60, ETNG11.getPreferredSize().height);

            //---- ETNG12 ----
            ETNG12.setComponentPopupMenu(BTD);
            ETNG12.setName("ETNG12");
            panel3.add(ETNG12);
            ETNG12.setBounds(655, 330, 60, ETNG12.getPreferredSize().height);

            //---- ETNG13 ----
            ETNG13.setComponentPopupMenu(BTD);
            ETNG13.setName("ETNG13");
            panel3.add(ETNG13);
            ETNG13.setBounds(655, 355, 60, ETNG13.getPreferredSize().height);

            //---- ETNG14 ----
            ETNG14.setComponentPopupMenu(BTD);
            ETNG14.setName("ETNG14");
            panel3.add(ETNG14);
            ETNG14.setBounds(655, 380, 60, ETNG14.getPreferredSize().height);

            //---- ETNA01 ----
            ETNA01.setComponentPopupMenu(BTD);
            ETNA01.setName("ETNA01");
            panel3.add(ETNA01);
            ETNA01.setBounds(720, 55, 60, ETNA01.getPreferredSize().height);

            //---- ETNA02 ----
            ETNA02.setComponentPopupMenu(BTD);
            ETNA02.setName("ETNA02");
            panel3.add(ETNA02);
            ETNA02.setBounds(720, 80, 60, ETNA02.getPreferredSize().height);

            //---- ETNA03 ----
            ETNA03.setComponentPopupMenu(BTD);
            ETNA03.setName("ETNA03");
            panel3.add(ETNA03);
            ETNA03.setBounds(720, 105, 60, ETNA03.getPreferredSize().height);

            //---- ETNA04 ----
            ETNA04.setComponentPopupMenu(BTD);
            ETNA04.setName("ETNA04");
            panel3.add(ETNA04);
            ETNA04.setBounds(720, 130, 60, ETNA04.getPreferredSize().height);

            //---- ETNA05 ----
            ETNA05.setComponentPopupMenu(BTD);
            ETNA05.setName("ETNA05");
            panel3.add(ETNA05);
            ETNA05.setBounds(720, 155, 60, ETNA05.getPreferredSize().height);

            //---- ETNA06 ----
            ETNA06.setComponentPopupMenu(BTD);
            ETNA06.setName("ETNA06");
            panel3.add(ETNA06);
            ETNA06.setBounds(720, 180, 60, ETNA06.getPreferredSize().height);

            //---- ETNA07 ----
            ETNA07.setComponentPopupMenu(BTD);
            ETNA07.setName("ETNA07");
            panel3.add(ETNA07);
            ETNA07.setBounds(720, 205, 60, ETNA07.getPreferredSize().height);

            //---- ETNA08 ----
            ETNA08.setComponentPopupMenu(BTD);
            ETNA08.setName("ETNA08");
            panel3.add(ETNA08);
            ETNA08.setBounds(720, 230, 60, ETNA08.getPreferredSize().height);

            //---- ETNA09 ----
            ETNA09.setComponentPopupMenu(BTD);
            ETNA09.setName("ETNA09");
            panel3.add(ETNA09);
            ETNA09.setBounds(720, 255, 60, ETNA09.getPreferredSize().height);

            //---- ETNA10 ----
            ETNA10.setComponentPopupMenu(BTD);
            ETNA10.setName("ETNA10");
            panel3.add(ETNA10);
            ETNA10.setBounds(720, 280, 60, ETNA10.getPreferredSize().height);

            //---- ETNA11 ----
            ETNA11.setComponentPopupMenu(BTD);
            ETNA11.setName("ETNA11");
            panel3.add(ETNA11);
            ETNA11.setBounds(720, 305, 60, ETNA11.getPreferredSize().height);

            //---- ETNA12 ----
            ETNA12.setComponentPopupMenu(BTD);
            ETNA12.setName("ETNA12");
            panel3.add(ETNA12);
            ETNA12.setBounds(720, 330, 60, ETNA12.getPreferredSize().height);

            //---- ETNA13 ----
            ETNA13.setComponentPopupMenu(BTD);
            ETNA13.setName("ETNA13");
            panel3.add(ETNA13);
            ETNA13.setBounds(720, 355, 60, ETNA13.getPreferredSize().height);

            //---- ETNA14 ----
            ETNA14.setComponentPopupMenu(BTD);
            ETNA14.setName("ETNA14");
            panel3.add(ETNA14);
            ETNA14.setBounds(720, 380, 60, ETNA14.getPreferredSize().height);

            //---- err1 ----
            err1.setToolTipText("Ligne en erreur");
            err1.setBorder(null);
            err1.setComponentPopupMenu(BTD);
            err1.setName("err1");
            panel3.add(err1);
            err1.setBounds(775, 55, 28, 28);

            //---- err2 ----
            err2.setToolTipText("Ligne en erreur");
            err2.setComponentPopupMenu(BTD);
            err2.setName("err2");
            panel3.add(err2);
            err2.setBounds(775, 80, 28, 28);

            //---- err3 ----
            err3.setToolTipText("Ligne en erreur");
            err3.setComponentPopupMenu(BTD);
            err3.setName("err3");
            panel3.add(err3);
            err3.setBounds(775, 105, 28, 28);

            //---- err4 ----
            err4.setToolTipText("Ligne en erreur");
            err4.setComponentPopupMenu(BTD);
            err4.setName("err4");
            panel3.add(err4);
            err4.setBounds(775, 130, 28, 28);

            //---- err5 ----
            err5.setToolTipText("Ligne en erreur");
            err5.setComponentPopupMenu(BTD);
            err5.setName("err5");
            panel3.add(err5);
            err5.setBounds(775, 155, 28, 28);

            //---- err6 ----
            err6.setToolTipText("Ligne en erreur");
            err6.setComponentPopupMenu(BTD);
            err6.setName("err6");
            panel3.add(err6);
            err6.setBounds(775, 180, 28, 28);

            //---- err7 ----
            err7.setToolTipText("Ligne en erreur");
            err7.setComponentPopupMenu(BTD);
            err7.setName("err7");
            panel3.add(err7);
            err7.setBounds(775, 205, 28, 28);

            //---- err8 ----
            err8.setToolTipText("Ligne en erreur");
            err8.setComponentPopupMenu(BTD);
            err8.setName("err8");
            panel3.add(err8);
            err8.setBounds(775, 230, 28, 28);

            //---- err9 ----
            err9.setToolTipText("Ligne en erreur");
            err9.setComponentPopupMenu(BTD);
            err9.setName("err9");
            panel3.add(err9);
            err9.setBounds(775, 255, 28, 28);

            //---- err10 ----
            err10.setToolTipText("Ligne en erreur");
            err10.setComponentPopupMenu(BTD);
            err10.setName("err10");
            panel3.add(err10);
            err10.setBounds(775, 280, 28, 28);

            //---- err11 ----
            err11.setToolTipText("Ligne en erreur");
            err11.setComponentPopupMenu(BTD);
            err11.setName("err11");
            panel3.add(err11);
            err11.setBounds(775, 305, 28, 28);

            //---- err13 ----
            err13.setToolTipText("Ligne en erreur");
            err13.setComponentPopupMenu(BTD);
            err13.setName("err13");
            panel3.add(err13);
            err13.setBounds(775, 355, 28, 28);

            //---- err14 ----
            err14.setToolTipText("Ligne en erreur");
            err14.setComponentPopupMenu(BTD);
            err14.setName("err14");
            panel3.add(err14);
            err14.setBounds(775, 380, 28, 28);

            //---- err12 ----
            err12.setToolTipText("Ligne en erreur");
            err12.setComponentPopupMenu(BTD);
            err12.setName("err12");
            panel3.add(err12);
            err12.setBounds(775, 330, 28, 28);

            //======== panel1 ========
            {
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- ETMT01 ----
              ETMT01.setName("ETMT01");
              panel1.add(ETMT01);
              ETMT01.setBounds(10, 5, 90, ETMT01.getPreferredSize().height);

              //---- ETMT02 ----
              ETMT02.setName("ETMT02");
              panel1.add(ETMT02);
              ETMT02.setBounds(10, 30, 90, ETMT02.getPreferredSize().height);

              //---- ETMT03 ----
              ETMT03.setName("ETMT03");
              panel1.add(ETMT03);
              ETMT03.setBounds(10, 55, 90, ETMT03.getPreferredSize().height);

              //---- ETMT04 ----
              ETMT04.setName("ETMT04");
              panel1.add(ETMT04);
              ETMT04.setBounds(10, 80, 90, ETMT04.getPreferredSize().height);

              //---- ETMT05 ----
              ETMT05.setName("ETMT05");
              panel1.add(ETMT05);
              ETMT05.setBounds(10, 105, 90, ETMT05.getPreferredSize().height);

              //---- ETMT06 ----
              ETMT06.setName("ETMT06");
              panel1.add(ETMT06);
              ETMT06.setBounds(10, 130, 90, ETMT06.getPreferredSize().height);

              //---- ETMT07 ----
              ETMT07.setName("ETMT07");
              panel1.add(ETMT07);
              ETMT07.setBounds(10, 155, 90, ETMT07.getPreferredSize().height);

              //---- ETMT08 ----
              ETMT08.setName("ETMT08");
              panel1.add(ETMT08);
              ETMT08.setBounds(10, 180, 90, ETMT08.getPreferredSize().height);

              //---- ETMT09 ----
              ETMT09.setName("ETMT09");
              panel1.add(ETMT09);
              ETMT09.setBounds(10, 205, 90, ETMT09.getPreferredSize().height);

              //---- ETMT10 ----
              ETMT10.setName("ETMT10");
              panel1.add(ETMT10);
              ETMT10.setBounds(10, 230, 90, ETMT10.getPreferredSize().height);

              //---- ETMT11 ----
              ETMT11.setName("ETMT11");
              panel1.add(ETMT11);
              ETMT11.setBounds(10, 255, 90, ETMT11.getPreferredSize().height);

              //---- ETMT12 ----
              ETMT12.setName("ETMT12");
              panel1.add(ETMT12);
              ETMT12.setBounds(10, 280, 90, ETMT12.getPreferredSize().height);

              //---- ETMT13 ----
              ETMT13.setName("ETMT13");
              panel1.add(ETMT13);
              ETMT13.setBounds(10, 305, 90, ETMT13.getPreferredSize().height);

              //---- ETMT14 ----
              ETMT14.setName("ETMT14");
              panel1.add(ETMT14);
              ETMT14.setBounds(10, 330, 90, ETMT14.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            panel3.add(panel1);
            panel1.setBounds(20, 50, 105, 365);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Totaux"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_70_OBJ_70 ----
            OBJ_70_OBJ_70.setText("<-  Diff\u00e9rence totaux");
            OBJ_70_OBJ_70.setForeground(Color.red);
            OBJ_70_OBJ_70.setFont(OBJ_70_OBJ_70.getFont().deriveFont(OBJ_70_OBJ_70.getFont().getStyle() | Font.BOLD));
            OBJ_70_OBJ_70.setName("OBJ_70_OBJ_70");
            panel2.add(OBJ_70_OBJ_70);
            OBJ_70_OBJ_70.setBounds(380, 37, 148, 20);

            //---- E1TDBR ----
            E1TDBR.setComponentPopupMenu(null);
            E1TDBR.setText("@E1TDBR@");
            E1TDBR.setHorizontalAlignment(SwingConstants.RIGHT);
            E1TDBR.setName("E1TDBR");
            panel2.add(E1TDBR);
            E1TDBR.setBounds(80, 35, 140, E1TDBR.getPreferredSize().height);

            //---- E1TCRR ----
            E1TCRR.setComponentPopupMenu(null);
            E1TCRR.setText("@E1TCRR@");
            E1TCRR.setHorizontalAlignment(SwingConstants.RIGHT);
            E1TCRR.setName("E1TCRR");
            panel2.add(E1TCRR);
            E1TCRR.setBounds(80, 65, 140, E1TCRR.getPreferredSize().height);

            //---- WSOL ----
            WSOL.setText("@WSOL@");
            WSOL.setForeground(Color.black);
            WSOL.setHorizontalAlignment(SwingConstants.RIGHT);
            WSOL.setName("WSOL");
            panel2.add(WSOL);
            WSOL.setBounds(245, 35, 120, 24);

            //---- OBJ_71_OBJ_71 ----
            OBJ_71_OBJ_71.setText("Cr\u00e9dit");
            OBJ_71_OBJ_71.setName("OBJ_71_OBJ_71");
            panel2.add(OBJ_71_OBJ_71);
            OBJ_71_OBJ_71.setBounds(30, 67, 45, 20);

            //---- OBJ_67_OBJ_67 ----
            OBJ_67_OBJ_67.setText("D\u00e9bit");
            OBJ_67_OBJ_67.setName("OBJ_67_OBJ_67");
            panel2.add(OBJ_67_OBJ_67);
            OBJ_67_OBJ_67.setBounds(30, 37, 45, 20);

            //---- E1TDB ----
            E1TDB.setComponentPopupMenu(null);
            E1TDB.setText("@E1TDB@");
            E1TDB.setHorizontalAlignment(SwingConstants.RIGHT);
            E1TDB.setName("E1TDB");
            panel2.add(E1TDB);
            E1TDB.setBounds(80, 35, 140, E1TDB.getPreferredSize().height);

            //---- E1TCR ----
            E1TCR.setComponentPopupMenu(null);
            E1TCR.setText("@E1TCR@");
            E1TCR.setHorizontalAlignment(SwingConstants.RIGHT);
            E1TCR.setName("E1TCR");
            panel2.add(E1TCR);
            E1TCR.setBounds(80, 65, 140, E1TCR.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 829, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 829, GroupLayout.PREFERRED_SIZE)))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(449, 449, 449)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 437, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- OBJ_8 ----
    OBJ_8.setText("Annuler");
    OBJ_8.setName("OBJ_8");
    OBJ_8.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_8ActionPerformed(e);
      }
    });
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_36_OBJ_36;
  private RiZoneSortie WISOC;
  private JLabel OBJ_38_OBJ_38;
  private RiZoneSortie WICJO;
  private JLabel OBJ_40_OBJ_40;
  private RiZoneSortie WICFO;
  private JLabel OBJ_76_OBJ_76;
  private RiZoneSortie WIDTEX;
  private JLabel OBJ_77_OBJ_77;
  private RiZoneSortie WCET;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel3;
  private JLabel label1;
  private XRiTextField ETPC01;
  private XRiTextField ETPC02;
  private XRiTextField ETPC03;
  private XRiTextField ETPC04;
  private XRiTextField ETPC05;
  private XRiTextField ETPC06;
  private XRiTextField ETPC07;
  private XRiTextField ETPC08;
  private XRiTextField ETPC09;
  private XRiTextField ETPC10;
  private XRiTextField ETPC11;
  private XRiTextField ETPC12;
  private XRiTextField ETPC13;
  private XRiTextField ETPC14;
  private JLabel label2;
  private XRiTextField ETLI01;
  private XRiTextField ETLI02;
  private XRiTextField ETLI03;
  private XRiTextField ETLI04;
  private XRiTextField ETLI05;
  private XRiTextField ETLI06;
  private XRiTextField ETLI07;
  private XRiTextField ETLI08;
  private XRiTextField ETLI09;
  private XRiTextField ETLI10;
  private XRiTextField ETLI11;
  private XRiTextField ETLI12;
  private XRiTextField ETLI13;
  private XRiTextField ETLI14;
  private JLabel label3;
  private JLabel label4;
  private XRiTextField ETAC01;
  private XRiTextField ETAC02;
  private XRiTextField ETAC03;
  private XRiTextField ETAC04;
  private XRiTextField ETAC05;
  private XRiTextField ETAC06;
  private XRiTextField ETAC07;
  private XRiTextField ETAC08;
  private XRiTextField ETAC09;
  private XRiTextField ETAC10;
  private XRiTextField ETAC11;
  private XRiTextField ETAC12;
  private XRiTextField ETAC13;
  private XRiTextField ETAC14;
  private XRiTextField ETSA01;
  private JLabel label5;
  private XRiTextField ETSA02;
  private XRiTextField ETSA03;
  private XRiTextField ETSA04;
  private XRiTextField ETSA05;
  private XRiTextField ETSA06;
  private XRiTextField ETSA07;
  private XRiTextField ETSA08;
  private XRiTextField ETSA09;
  private XRiTextField ETSA10;
  private XRiTextField ETSA11;
  private XRiTextField ETSA12;
  private XRiTextField ETSA13;
  private XRiTextField ETSA14;
  private XRiTextField ETCL01;
  private JLabel label6;
  private JLabel label10;
  private JLabel label8;
  private JLabel label9;
  private XRiTextField ETCL02;
  private XRiTextField ETCL03;
  private XRiTextField ETCL04;
  private XRiTextField ETCL05;
  private XRiTextField ETCL06;
  private XRiTextField ETCL07;
  private XRiTextField ETCL08;
  private XRiTextField ETCL09;
  private XRiTextField ETCL10;
  private XRiTextField ETCL11;
  private XRiTextField ETCL12;
  private XRiTextField ETCL13;
  private XRiTextField ETCL14;
  private XRiTextField ETSS01;
  private XRiTextField ETSS02;
  private XRiTextField ETSS03;
  private XRiTextField ETSS04;
  private XRiTextField ETSS05;
  private XRiTextField ETSS06;
  private XRiTextField ETSS07;
  private XRiTextField ETSS08;
  private XRiTextField ETSS09;
  private XRiTextField ETSS10;
  private XRiTextField ETSS11;
  private XRiTextField ETSS12;
  private XRiTextField ETSS13;
  private XRiTextField ETSS14;
  private XRiTextField ETNG01;
  private XRiTextField ETNG02;
  private XRiTextField ETNG03;
  private XRiTextField ETNG04;
  private XRiTextField ETNG05;
  private XRiTextField ETNG06;
  private XRiTextField ETNG07;
  private XRiTextField ETNG08;
  private XRiTextField ETNG09;
  private XRiTextField ETNG10;
  private XRiTextField ETNG11;
  private XRiTextField ETNG12;
  private XRiTextField ETNG13;
  private XRiTextField ETNG14;
  private XRiTextField ETNA01;
  private XRiTextField ETNA02;
  private XRiTextField ETNA03;
  private XRiTextField ETNA04;
  private XRiTextField ETNA05;
  private XRiTextField ETNA06;
  private XRiTextField ETNA07;
  private XRiTextField ETNA08;
  private XRiTextField ETNA09;
  private XRiTextField ETNA10;
  private XRiTextField ETNA11;
  private XRiTextField ETNA12;
  private XRiTextField ETNA13;
  private XRiTextField ETNA14;
  private JLabel err1;
  private JLabel err2;
  private JLabel err3;
  private JLabel err4;
  private JLabel err5;
  private JLabel err6;
  private JLabel err7;
  private JLabel err8;
  private JLabel err9;
  private JLabel err10;
  private JLabel err11;
  private JLabel err13;
  private JLabel err14;
  private JLabel err12;
  private JPanel panel1;
  private XRiTextField ETMT01;
  private XRiTextField ETMT02;
  private XRiTextField ETMT03;
  private XRiTextField ETMT04;
  private XRiTextField ETMT05;
  private XRiTextField ETMT06;
  private XRiTextField ETMT07;
  private XRiTextField ETMT08;
  private XRiTextField ETMT09;
  private XRiTextField ETMT10;
  private XRiTextField ETMT11;
  private XRiTextField ETMT12;
  private XRiTextField ETMT13;
  private XRiTextField ETMT14;
  private JPanel panel2;
  private JLabel OBJ_70_OBJ_70;
  private RiZoneSortie E1TDBR;
  private RiZoneSortie E1TCRR;
  private RiZoneSortie WSOL;
  private JLabel OBJ_71_OBJ_71;
  private JLabel OBJ_67_OBJ_67;
  private RiZoneSortie E1TDB;
  private RiZoneSortie E1TCR;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_8;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
