
package ri.serien.libecranrpg.vcgm.VCGM11FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.ODialog;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM11FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _WTX01_Top = { "WTX01", "WTX02", "WTX03", "WTX04", "WTX05", "WTX06", "WTX07", "WTX08", "WTX09", "WTX10", "WTX11",
      "WTX12", "WTX13", "WTX14", "WTX15", };
  private String[] _WTX01_Title = { "N° du compte  Intitulé du compte", };
  private String[][] _WTX01_Data = { { "LR01", }, { "LR02", }, { "LR03", }, { "LR04", }, { "LR05", }, { "LR06", }, { "LR07", }, { "LR08", },
      { "LR09", }, { "LR10", }, { "LR11", }, { "LR12", }, { "LR13", }, { "LR14", }, { "LR15", }, };
  private int[] _WTX01_Width = { 750, };
  private int[] _LIST_Justification = { SwingConstants.LEFT, SwingConstants.RIGHT, SwingConstants.RIGHT };
  // private VCGM11OPTPIECE option=null;
  
  public ODialog dialog_OPTPIECE = null;
  
  public VCGM11FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTX01.setAspectTable(_WTX01_Top, _WTX01_Title, _WTX01_Data, _WTX01_Width, false, _LIST_Justification, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    E1SOC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1SOC@")).trim());
    E1CJO.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1CJO@")).trim());
    JOLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@JOLIB@")).trim());
    E1CFO.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1CFO@")).trim());
    E1DTEX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1DTEX@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    

    
    


    
    
    WCOD.setVisible(lexique.isPresent("WCOD"));
    E1CJO.setVisible(lexique.isPresent("E1CJO"));
    E1CFO.setVisible(lexique.isPresent("E1CFO"));
    L1NLI.setVisible(lexique.isPresent("L1NLI"));
    E1SOC.setVisible(lexique.isPresent("E1SOC"));
    WEPCOD.setEnabled(lexique.isPresent("WEPCOD"));
    L1NCGX.setEnabled(lexique.isPresent("L1NCGX"));
    E1DTEX.setVisible(lexique.isPresent("E1DTEX"));
    L1NCA.setEnabled(lexique.isPresent("L1NCA"));
    WDIFCR.setVisible(lexique.isPresent("WDIFCR"));
    WDIFDB.setVisible(lexique.isPresent("WDIFDB"));
    E1TCR.setVisible(lexique.isPresent("E1TCR"));
    E1TDB.setVisible(lexique.isPresent("E1TDB"));
    L1RFC.setEnabled(lexique.isPresent("L1RFC"));
    WRCHA.setEnabled(lexique.isPresent("WRCHA"));
    JOLIB.setVisible(lexique.isPresent("JOLIB"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - FOLIO COMPTABILITE"));
    
    

    
    p_bpresentation.setCodeEtablissement(E1SOC.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(E1SOC.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
    // if( (dialog_OPTPIECE != null) && (dialog_OPTPIECE.getFenetre().isVisible()) )
    // dialog_OPTPIECE.setData(listeRecord);
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTX01_Top, "2", "Enter");
    WTX01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTX01_Top, "3", "Enter");
    WTX01.setValeurTop("3");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTX01_Top, "4", "Enter");
    WTX01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(FCT.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(FCT.getInvoker().getName());
  }
  
  private void WTX01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _WTX01_Top, "2", "Enter", e);
    WTX01.setValeurTop("2");
    if (WTX01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void OBJ_95ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WEPCOD");
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_104ActionPerformed(ActionEvent e) {
    /*
    option = new VCGM11OPTPIECE(lexique, interpreteurD);
    option.setWEPTYP(lexique.HostFieldGetData("WEPTYP").trim());
    option.setVisible(true);
    lexique.HostFieldPutData("WEPTYP", 0, option.getWEPTYP());
    option.dispose();
    */
    if (dialog_OPTPIECE == null) {
      dialog_OPTPIECE = new ODialog((Window) getTopLevelAncestor(), new VCGM11OPTPIECE(this));
    }
    dialog_OPTPIECE.affichePopupPerso();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_38_OBJ_38 = new JLabel();
    E1SOC = new RiZoneSortie();
    OBJ_40_OBJ_40 = new JLabel();
    E1CJO = new RiZoneSortie();
    JOLIB = new RiZoneSortie();
    OBJ_43_OBJ_43 = new JLabel();
    E1CFO = new RiZoneSortie();
    OBJ_45_OBJ_45 = new JLabel();
    E1DTEX = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    p_LigneFolio = new JPanel();
    SCROLLPANE_LIST5 = new JScrollPane();
    WTX01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    p_PiedListe = new JPanel();
    p_Totaux = new JPanel();
    OBJ_90 = new JLabel();
    OBJ_91 = new JLabel();
    OBJ_92 = new JLabel();
    OBJ_97 = new JLabel();
    E1TDB = new XRiTextField();
    E1TCR = new XRiTextField();
    WDIFCR = new XRiTextField();
    WDIFDB = new XRiTextField();
    FCT = new JPopupMenu();
    OBJ_26 = new JMenuItem();
    OBJ_25 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_20 = new JMenuItem();
    OBJ_21 = new JMenuItem();
    OBJ_23 = new JMenuItem();
    OBJ_22 = new JMenuItem();
    OBJ_87 = new JLabel();
    WCOD = new XRiTextField();
    l_NumLigne = new JLabel();
    L1NLI = new XRiTextField();
    OBJ_88 = new JLabel();
    L1NCGX = new XRiTextField();
    L1NCA = new XRiTextField();
    OBJ_89 = new JLabel();
    L1RFC = new XRiTextField();
    OBJ_95 = new JButton();
    WEPCOD = new XRiTextField();
    OBJ_104 = new SNBoutonDetail();
    OBJ_96 = new JLabel();
    WRCHA = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Folio de comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_38_OBJ_38 ----
          OBJ_38_OBJ_38.setText("Soci\u00e9t\u00e9");
          OBJ_38_OBJ_38.setName("OBJ_38_OBJ_38");

          //---- E1SOC ----
          E1SOC.setOpaque(false);
          E1SOC.setText("@E1SOC@");
          E1SOC.setName("E1SOC");

          //---- OBJ_40_OBJ_40 ----
          OBJ_40_OBJ_40.setText("Journal");
          OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");

          //---- E1CJO ----
          E1CJO.setOpaque(false);
          E1CJO.setText("@E1CJO@");
          E1CJO.setName("E1CJO");

          //---- JOLIB ----
          JOLIB.setOpaque(false);
          JOLIB.setText("@JOLIB@");
          JOLIB.setName("JOLIB");

          //---- OBJ_43_OBJ_43 ----
          OBJ_43_OBJ_43.setText("Folio");
          OBJ_43_OBJ_43.setName("OBJ_43_OBJ_43");

          //---- E1CFO ----
          E1CFO.setOpaque(false);
          E1CFO.setText("@E1CFO@");
          E1CFO.setName("E1CFO");

          //---- OBJ_45_OBJ_45 ----
          OBJ_45_OBJ_45.setText("du");
          OBJ_45_OBJ_45.setName("OBJ_45_OBJ_45");

          //---- E1DTEX ----
          E1DTEX.setOpaque(false);
          E1DTEX.setText("@E1DTEX@");
          E1DTEX.setName("E1DTEX");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_38_OBJ_38, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(E1SOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(OBJ_40_OBJ_40, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(E1CJO, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(JOLIB, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(OBJ_43_OBJ_43, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(E1CFO, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_45_OBJ_45, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(E1DTEX, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(E1SOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(E1CJO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(JOLIB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(E1CFO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(E1DTEX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_38_OBJ_38, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_40_OBJ_40, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_43_OBJ_43, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_45_OBJ_45, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Solde");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1010, 540));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== p_LigneFolio ========
          {
            p_LigneFolio.setBorder(new TitledBorder("Lignes du folio"));
            p_LigneFolio.setOpaque(false);
            p_LigneFolio.setName("p_LigneFolio");
            p_LigneFolio.setLayout(null);

            //======== SCROLLPANE_LIST5 ========
            {
              SCROLLPANE_LIST5.setName("SCROLLPANE_LIST5");

              //---- WTX01 ----
              WTX01.setName("WTX01");
              WTX01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTX01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST5.setViewportView(WTX01);
            }
            p_LigneFolio.add(SCROLLPANE_LIST5);
            SCROLLPANE_LIST5.setBounds(15, 35, 930, 270);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setFocusable(false);
            BT_PGUP.setName("BT_PGUP");
            p_LigneFolio.add(BT_PGUP);
            BT_PGUP.setBounds(950, 35, 25, 125);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setFocusable(false);
            BT_PGDOWN.setName("BT_PGDOWN");
            p_LigneFolio.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(950, 180, 25, 125);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < p_LigneFolio.getComponentCount(); i++) {
                Rectangle bounds = p_LigneFolio.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = p_LigneFolio.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              p_LigneFolio.setMinimumSize(preferredSize);
              p_LigneFolio.setPreferredSize(preferredSize);
            }
          }

          //======== p_PiedListe ========
          {
            p_PiedListe.setBorder(new TitledBorder(""));
            p_PiedListe.setOpaque(false);
            p_PiedListe.setName("p_PiedListe");
            p_PiedListe.setLayout(null);

            //======== p_Totaux ========
            {
              p_Totaux.setOpaque(false);
              p_Totaux.setName("p_Totaux");

              //---- OBJ_90 ----
              OBJ_90.setText("D\u00e9bit");
              OBJ_90.setFont(OBJ_90.getFont().deriveFont(OBJ_90.getFont().getStyle() | Font.BOLD, OBJ_90.getFont().getSize() + 1f));
              OBJ_90.setName("OBJ_90");

              //---- OBJ_91 ----
              OBJ_91.setText("Cr\u00e9dit");
              OBJ_91.setFont(OBJ_91.getFont().deriveFont(OBJ_91.getFont().getStyle() | Font.BOLD, OBJ_91.getFont().getSize() + 1f));
              OBJ_91.setName("OBJ_91");

              //---- OBJ_92 ----
              OBJ_92.setText("Totaux");
              OBJ_92.setFont(OBJ_92.getFont().deriveFont(OBJ_92.getFont().getStyle() | Font.BOLD, OBJ_92.getFont().getSize() + 1f));
              OBJ_92.setName("OBJ_92");

              //---- OBJ_97 ----
              OBJ_97.setText("Diff\u00e9rence");
              OBJ_97.setFont(OBJ_97.getFont().deriveFont(OBJ_97.getFont().getStyle() | Font.BOLD, OBJ_97.getFont().getSize() + 1f));
              OBJ_97.setName("OBJ_97");

              //---- E1TDB ----
              E1TDB.setComponentPopupMenu(FCT);
              E1TDB.setName("E1TDB");

              //---- E1TCR ----
              E1TCR.setComponentPopupMenu(FCT);
              E1TCR.setName("E1TCR");

              //---- WDIFCR ----
              WDIFCR.setComponentPopupMenu(FCT);
              WDIFCR.setName("WDIFCR");

              //---- WDIFDB ----
              WDIFDB.setComponentPopupMenu(FCT);
              WDIFDB.setName("WDIFDB");

              GroupLayout p_TotauxLayout = new GroupLayout(p_Totaux);
              p_Totaux.setLayout(p_TotauxLayout);
              p_TotauxLayout.setHorizontalGroup(
                p_TotauxLayout.createParallelGroup()
                  .addGroup(p_TotauxLayout.createSequentialGroup()
                    .addGap(92, 92, 92)
                    .addComponent(OBJ_90, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)
                    .addGap(3, 3, 3)
                    .addComponent(OBJ_91, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_TotauxLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_92, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(E1TDB, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)
                    .addGap(3, 3, 3)
                    .addComponent(E1TCR, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_TotauxLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_97, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, 0)
                    .addComponent(WDIFDB, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)
                    .addGap(3, 3, 3)
                    .addComponent(WDIFCR, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE))
              );
              p_TotauxLayout.setVerticalGroup(
                p_TotauxLayout.createParallelGroup()
                  .addGroup(p_TotauxLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addGroup(p_TotauxLayout.createParallelGroup()
                      .addComponent(OBJ_90)
                      .addComponent(OBJ_91))
                    .addGap(1, 1, 1)
                    .addGroup(p_TotauxLayout.createParallelGroup()
                      .addGroup(p_TotauxLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(OBJ_92, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(E1TDB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(E1TCR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(2, 2, 2)
                    .addGroup(p_TotauxLayout.createParallelGroup()
                      .addGroup(p_TotauxLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(OBJ_97, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(WDIFDB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WDIFCR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
              );
            }
            p_PiedListe.add(p_Totaux);
            p_Totaux.setBounds(556, 31, 370, 100);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < p_PiedListe.getComponentCount(); i++) {
                Rectangle bounds = p_PiedListe.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = p_PiedListe.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              p_PiedListe.setMinimumSize(preferredSize);
              p_PiedListe.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(p_LigneFolio, GroupLayout.PREFERRED_SIZE, 989, GroupLayout.PREFERRED_SIZE)
                  .addComponent(p_PiedListe, GroupLayout.PREFERRED_SIZE, 989, GroupLayout.PREFERRED_SIZE))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(p_LigneFolio, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(p_PiedListe, GroupLayout.PREFERRED_SIZE, 172, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(10, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== FCT ========
    {
      FCT.setName("FCT");

      //---- OBJ_26 ----
      OBJ_26.setText("Choix possibles");
      OBJ_26.setName("OBJ_26");
      OBJ_26.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_26ActionPerformed(e);
        }
      });
      FCT.add(OBJ_26);

      //---- OBJ_25 ----
      OBJ_25.setText("Aide en ligne");
      OBJ_25.setName("OBJ_25");
      OBJ_25.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_25ActionPerformed(e);
        }
      });
      FCT.add(OBJ_25);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Modifier");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_20 ----
      OBJ_20.setText("Modifier (num\u00e9ro de compte)");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);

      //---- OBJ_21 ----
      OBJ_21.setText("Annuler");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);
      BTD.addSeparator();

      //---- OBJ_23 ----
      OBJ_23.setText("Choix possibles");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD.add(OBJ_23);

      //---- OBJ_22 ----
      OBJ_22.setText("Aide en ligne");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);
    }

    //---- OBJ_87 ----
    OBJ_87.setText("C");
    OBJ_87.setHorizontalAlignment(SwingConstants.CENTER);
    OBJ_87.setName("OBJ_87");

    //---- WCOD ----
    WCOD.setComponentPopupMenu(FCT);
    WCOD.setName("WCOD");

    //---- l_NumLigne ----
    l_NumLigne.setText("N\u00b0 de ligne");
    l_NumLigne.setName("l_NumLigne");

    //---- L1NLI ----
    L1NLI.setComponentPopupMenu(FCT);
    L1NLI.setName("L1NLI");

    //---- OBJ_88 ----
    OBJ_88.setText("Num\u00e9ro compte");
    OBJ_88.setFont(OBJ_88.getFont().deriveFont(OBJ_88.getFont().getStyle() | Font.BOLD));
    OBJ_88.setName("OBJ_88");

    //---- L1NCGX ----
    L1NCGX.setComponentPopupMenu(FCT);
    L1NCGX.setName("L1NCGX");

    //---- L1NCA ----
    L1NCA.setComponentPopupMenu(FCT);
    L1NCA.setName("L1NCA");

    //---- OBJ_89 ----
    OBJ_89.setText("R\u00e9f\u00e9rence de classement");
    OBJ_89.setName("OBJ_89");

    //---- L1RFC ----
    L1RFC.setComponentPopupMenu(FCT);
    L1RFC.setName("L1RFC");

    //---- OBJ_95 ----
    OBJ_95.setText("Pi\u00e8ces");
    OBJ_95.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_95.setName("OBJ_95");
    OBJ_95.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_95ActionPerformed(e);
      }
    });

    //---- WEPCOD ----
    WEPCOD.setComponentPopupMenu(FCT);
    WEPCOD.setName("WEPCOD");

    //---- OBJ_104 ----
    OBJ_104.setText("");
    OBJ_104.setToolTipText("Options");
    OBJ_104.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_104.setName("OBJ_104");
    OBJ_104.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_104ActionPerformed(e);
      }
    });

    //---- OBJ_96 ----
    OBJ_96.setText("ou   Recherche");
    OBJ_96.setName("OBJ_96");

    //---- WRCHA ----
    WRCHA.setComponentPopupMenu(FCT);
    WRCHA.setName("WRCHA");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_38_OBJ_38;
  private RiZoneSortie E1SOC;
  private JLabel OBJ_40_OBJ_40;
  private RiZoneSortie E1CJO;
  private RiZoneSortie JOLIB;
  private JLabel OBJ_43_OBJ_43;
  private RiZoneSortie E1CFO;
  private JLabel OBJ_45_OBJ_45;
  private RiZoneSortie E1DTEX;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel p_LigneFolio;
  private JScrollPane SCROLLPANE_LIST5;
  private XRiTable WTX01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPanel p_PiedListe;
  private JPanel p_Totaux;
  private JLabel OBJ_90;
  private JLabel OBJ_91;
  private JLabel OBJ_92;
  private JLabel OBJ_97;
  private XRiTextField E1TDB;
  private XRiTextField E1TCR;
  private XRiTextField WDIFCR;
  private XRiTextField WDIFDB;
  private JPopupMenu FCT;
  private JMenuItem OBJ_26;
  private JMenuItem OBJ_25;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_22;
  private JLabel OBJ_87;
  private XRiTextField WCOD;
  private JLabel l_NumLigne;
  private XRiTextField L1NLI;
  private JLabel OBJ_88;
  private XRiTextField L1NCGX;
  private XRiTextField L1NCA;
  private JLabel OBJ_89;
  private XRiTextField L1RFC;
  private JButton OBJ_95;
  private XRiTextField WEPCOD;
  private SNBoutonDetail OBJ_104;
  private JLabel OBJ_96;
  private XRiTextField WRCHA;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
