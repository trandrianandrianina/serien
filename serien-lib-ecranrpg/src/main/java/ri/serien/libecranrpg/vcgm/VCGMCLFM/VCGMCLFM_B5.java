
package ri.serien.libecranrpg.vcgm.VCGMCLFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

public class VCGMCLFM_B5 extends SNPanelEcranRPG implements ioFrame {
  
   
  // TODO declarations classe spécifiques...
  
  public VCGMCLFM_B5(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // TODO constructeur spécifiques...
    
    // Ajout
    initDiverses();
    CLECRP.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // TODO setData spécifiques...
    // CLECRP.setSelected(lexique.HostFieldGetData("CLECRP").equals("OUI"));
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // TODO getDATA spécifiques
    // if (CLECRP.isSelected())
    // lexique.HostFieldPutData("CLECRP", 0, "OUI");
    // else
    // lexique.HostFieldPutData("CLECRP", 0, "NON");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    CLECRP = new XRiCheckBox();
    OBJ_49 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_46 = new JLabel();
    OBJ_44 = new JLabel();
    OBJ_51 = new JLabel();
    CLNUPD = new XRiTextField();
    CLNUPF = new XRiTextField();
    CLDPDX = new XRiTextField();
    CLDPFX = new XRiTextField();
    CLNCPD = new XRiTextField();
    CLNCPF = new XRiTextField();
    CLNAT1 = new XRiTextField();
    CLNAT2 = new XRiTextField();
    CLNAT3 = new XRiTextField();
    CLNAT4 = new XRiTextField();
    CLNAT5 = new XRiTextField();
    CLCLB1 = new XRiTextField();
    CLCLB2 = new XRiTextField();
    CLCLB3 = new XRiTextField();
    CLCLB4 = new XRiTextField();
    CLCLB5 = new XRiTextField();
    OBJ_45 = new JLabel();
    OBJ_48 = new JLabel();
    OBJ_50 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(695, 265));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Divers"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- CLECRP ----
          CLECRP.setText("Ecritures Lettr\u00e9es");
          CLECRP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CLECRP.setName("CLECRP");
          panel1.add(CLECRP);
          CLECRP.setBounds(25, 44, 133, 20);

          //---- OBJ_49 ----
          OBJ_49.setText("Compte de contrepartie");
          OBJ_49.setName("OBJ_49");
          panel1.add(OBJ_49);
          OBJ_49.setBounds(25, 194, 155, 20);

          //---- OBJ_47 ----
          OBJ_47.setText("Num\u00e9ro de pi\u00e8ce");
          OBJ_47.setName("OBJ_47");
          panel1.add(OBJ_47);
          OBJ_47.setBounds(25, 134, 145, 20);

          //---- OBJ_46 ----
          OBJ_46.setText("Codes libell\u00e9");
          OBJ_46.setName("OBJ_46");
          panel1.add(OBJ_46);
          OBJ_46.setBounds(25, 104, 135, 20);

          //---- OBJ_44 ----
          OBJ_44.setText("Date de lettrage");
          OBJ_44.setName("OBJ_44");
          panel1.add(OBJ_44);
          OBJ_44.setBounds(25, 74, 145, 20);

          //---- OBJ_51 ----
          OBJ_51.setText("Nature");
          OBJ_51.setName("OBJ_51");
          panel1.add(OBJ_51);
          OBJ_51.setBounds(25, 164, 130, 20);

          //---- CLNUPD ----
          CLNUPD.setComponentPopupMenu(BTD);
          CLNUPD.setName("CLNUPD");
          panel1.add(CLNUPD);
          CLNUPD.setBounds(180, 130, 66, CLNUPD.getPreferredSize().height);

          //---- CLNUPF ----
          CLNUPF.setComponentPopupMenu(BTD);
          CLNUPF.setName("CLNUPF");
          panel1.add(CLNUPF);
          CLNUPF.setBounds(265, 130, 66, CLNUPF.getPreferredSize().height);

          //---- CLDPDX ----
          CLDPDX.setComponentPopupMenu(BTD);
          CLDPDX.setName("CLDPDX");
          panel1.add(CLDPDX);
          CLDPDX.setBounds(180, 70, 60, CLDPDX.getPreferredSize().height);

          //---- CLDPFX ----
          CLDPFX.setComponentPopupMenu(BTD);
          CLDPFX.setName("CLDPFX");
          panel1.add(CLDPFX);
          CLDPFX.setBounds(265, 70, 60, CLDPFX.getPreferredSize().height);

          //---- CLNCPD ----
          CLNCPD.setComponentPopupMenu(BTD);
          CLNCPD.setName("CLNCPD");
          panel1.add(CLNCPD);
          CLNCPD.setBounds(180, 190, 58, CLNCPD.getPreferredSize().height);

          //---- CLNCPF ----
          CLNCPF.setComponentPopupMenu(BTD);
          CLNCPF.setName("CLNCPF");
          panel1.add(CLNCPF);
          CLNCPF.setBounds(265, 190, 58, CLNCPF.getPreferredSize().height);

          //---- CLNAT1 ----
          CLNAT1.setComponentPopupMenu(BTD);
          CLNAT1.setName("CLNAT1");
          panel1.add(CLNAT1);
          CLNAT1.setBounds(180, 160, 60, CLNAT1.getPreferredSize().height);

          //---- CLNAT2 ----
          CLNAT2.setComponentPopupMenu(BTD);
          CLNAT2.setName("CLNAT2");
          panel1.add(CLNAT2);
          CLNAT2.setBounds(240, 160, 60, CLNAT2.getPreferredSize().height);

          //---- CLNAT3 ----
          CLNAT3.setComponentPopupMenu(BTD);
          CLNAT3.setName("CLNAT3");
          panel1.add(CLNAT3);
          CLNAT3.setBounds(300, 160, 60, CLNAT3.getPreferredSize().height);

          //---- CLNAT4 ----
          CLNAT4.setComponentPopupMenu(BTD);
          CLNAT4.setName("CLNAT4");
          panel1.add(CLNAT4);
          CLNAT4.setBounds(360, 160, 60, CLNAT4.getPreferredSize().height);

          //---- CLNAT5 ----
          CLNAT5.setComponentPopupMenu(BTD);
          CLNAT5.setName("CLNAT5");
          panel1.add(CLNAT5);
          CLNAT5.setBounds(420, 160, 60, CLNAT5.getPreferredSize().height);

          //---- CLCLB1 ----
          CLCLB1.setComponentPopupMenu(BTD);
          CLCLB1.setName("CLCLB1");
          panel1.add(CLCLB1);
          CLCLB1.setBounds(180, 100, 20, CLCLB1.getPreferredSize().height);

          //---- CLCLB2 ----
          CLCLB2.setComponentPopupMenu(BTD);
          CLCLB2.setName("CLCLB2");
          panel1.add(CLCLB2);
          CLCLB2.setBounds(201, 100, 20, CLCLB2.getPreferredSize().height);

          //---- CLCLB3 ----
          CLCLB3.setComponentPopupMenu(BTD);
          CLCLB3.setName("CLCLB3");
          panel1.add(CLCLB3);
          CLCLB3.setBounds(222, 100, 20, CLCLB3.getPreferredSize().height);

          //---- CLCLB4 ----
          CLCLB4.setComponentPopupMenu(BTD);
          CLCLB4.setName("CLCLB4");
          panel1.add(CLCLB4);
          CLCLB4.setBounds(243, 100, 20, CLCLB4.getPreferredSize().height);

          //---- CLCLB5 ----
          CLCLB5.setComponentPopupMenu(BTD);
          CLCLB5.setName("CLCLB5");
          panel1.add(CLCLB5);
          CLCLB5.setBounds(264, 100, 20, CLCLB5.getPreferredSize().height);

          //---- OBJ_45 ----
          OBJ_45.setText("\u00e0");
          OBJ_45.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_45.setName("OBJ_45");
          panel1.add(OBJ_45);
          OBJ_45.setBounds(245, 74, 12, 20);

          //---- OBJ_48 ----
          OBJ_48.setText("\u00e0");
          OBJ_48.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_48.setName("OBJ_48");
          panel1.add(OBJ_48);
          OBJ_48.setBounds(245, 134, 12, 20);

          //---- OBJ_50 ----
          OBJ_50.setText("\u00e0");
          OBJ_50.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_50.setName("OBJ_50");
          panel1.add(OBJ_50);
          OBJ_50.setBounds(245, 194, 12, 20);
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 505, 245);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiCheckBox CLECRP;
  private JLabel OBJ_49;
  private JLabel OBJ_47;
  private JLabel OBJ_46;
  private JLabel OBJ_44;
  private JLabel OBJ_51;
  private XRiTextField CLNUPD;
  private XRiTextField CLNUPF;
  private XRiTextField CLDPDX;
  private XRiTextField CLDPFX;
  private XRiTextField CLNCPD;
  private XRiTextField CLNCPF;
  private XRiTextField CLNAT1;
  private XRiTextField CLNAT2;
  private XRiTextField CLNAT3;
  private XRiTextField CLNAT4;
  private XRiTextField CLNAT5;
  private XRiTextField CLCLB1;
  private XRiTextField CLCLB2;
  private XRiTextField CLCLB3;
  private XRiTextField CLCLB4;
  private XRiTextField CLCLB5;
  private JLabel OBJ_45;
  private JLabel OBJ_48;
  private JLabel OBJ_50;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
