
package ri.serien.libecranrpg.vcgm.VCGM01FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM01FM_CA extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM01FM_CA(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    CACND.setValeurs("ET", CACND_GRP);
    CACND_OU.setValeurs("OU");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_50_OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIAX1@")).trim());
    OBJ_52_OBJ_52.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIAA1@")).trim());
    OBJ_54_OBJ_54.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIAX2@")).trim());
    OBJ_56_OBJ_56.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIAA2@")).trim());
    OBJ_58_OBJ_58.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIAX3@")).trim());
    OBJ_60_OBJ_60.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIAA3@")).trim());
    OBJ_62_OBJ_62.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIAX4@")).trim());
    OBJ_64_OBJ_64.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIAA4@")).trim());
    OBJ_66_OBJ_66.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIAX5@")).trim());
    OBJ_68_OBJ_68.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIAA5@")).trim());
    OBJ_70_OBJ_70.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIAX6@")).trim());
    OBJ_72_OBJ_72.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIAA6@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    OBJ_72_OBJ_72.setVisible(lexique.isPresent("LIAA6"));
    OBJ_70_OBJ_70.setVisible(lexique.isPresent("LIAX6"));
    OBJ_68_OBJ_68.setVisible(lexique.isPresent("LIAA5"));
    OBJ_66_OBJ_66.setVisible(lexique.isPresent("LIAX5"));
    OBJ_64_OBJ_64.setVisible(lexique.isPresent("LIAA4"));
    OBJ_62_OBJ_62.setVisible(lexique.isPresent("LIAX4"));
    OBJ_60_OBJ_60.setVisible(lexique.isPresent("LIAA3"));
    OBJ_58_OBJ_58.setVisible(lexique.isPresent("LIAX3"));
    OBJ_56_OBJ_56.setVisible(lexique.isPresent("LIAA2"));
    OBJ_54_OBJ_54.setVisible(lexique.isPresent("LIAX2"));
    OBJ_52_OBJ_52.setVisible(lexique.isPresent("LIAA1"));
    OBJ_50_OBJ_50.setVisible(lexique.isPresent("LIAX1"));
    // radioButton1.setSelected(lexique.HostFieldGetData("CACND").equalsIgnoreCase("ET"));
    // radioButton2.setSelected(lexique.HostFieldGetData("CACND").equalsIgnoreCase("OU"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - Personnalisation de @LOCGRP/-1/@"));
    
    

    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    // riMenu_bt2.setIcon(lexique.getImage("images/options.png",true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (radioButton1.isSelected()) lexique.HostFieldPutData("CACND", 1, "ET");
    // if (radioButton2.isSelected()) lexique.HostFieldPutData("CACND", 1, "OU");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche2 = new JPanel();
    OBJ_44_OBJ_45 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_45_OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46_OBJ_47 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    CALIB = new XRiTextField();
    OBJ_50_OBJ_50 = new RiZoneSortie();
    OBJ_52_OBJ_52 = new RiZoneSortie();
    OBJ_54_OBJ_54 = new RiZoneSortie();
    OBJ_56_OBJ_56 = new RiZoneSortie();
    OBJ_58_OBJ_58 = new RiZoneSortie();
    OBJ_60_OBJ_60 = new RiZoneSortie();
    OBJ_62_OBJ_62 = new RiZoneSortie();
    OBJ_64_OBJ_64 = new RiZoneSortie();
    OBJ_66_OBJ_66 = new RiZoneSortie();
    OBJ_68_OBJ_68 = new RiZoneSortie();
    OBJ_70_OBJ_70 = new RiZoneSortie();
    OBJ_72_OBJ_72 = new RiZoneSortie();
    OBJ_73_OBJ_73 = new JLabel();
    OBJ_47_OBJ_47 = new JLabel();
    CAAX1 = new XRiTextField();
    CAAX2 = new XRiTextField();
    CAAX3 = new XRiTextField();
    CAAX4 = new XRiTextField();
    CAAX5 = new XRiTextField();
    CAAX6 = new XRiTextField();
    OBJ_49_OBJ_49 = new JLabel();
    OBJ_53_OBJ_53 = new JLabel();
    OBJ_57_OBJ_57 = new JLabel();
    OBJ_61_OBJ_61 = new JLabel();
    OBJ_65_OBJ_65 = new JLabel();
    OBJ_69_OBJ_69 = new JLabel();
    CACND = new XRiRadioButton();
    CACND_OU = new XRiRadioButton();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    CACND_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche2 ========
        {
          p_tete_gauche2.setOpaque(false);
          p_tete_gauche2.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche2.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche2.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche2.setName("p_tete_gauche2");

          //---- OBJ_44_OBJ_45 ----
          OBJ_44_OBJ_45.setText("Soci\u00e9t\u00e9");
          OBJ_44_OBJ_45.setName("OBJ_44_OBJ_45");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setName("INDETB");

          //---- OBJ_45_OBJ_46 ----
          OBJ_45_OBJ_46.setText("Code");
          OBJ_45_OBJ_46.setName("OBJ_45_OBJ_46");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setName("INDTYP");

          //---- OBJ_46_OBJ_47 ----
          OBJ_46_OBJ_47.setText("Ordre");
          OBJ_46_OBJ_47.setName("OBJ_46_OBJ_47");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gauche2Layout = new GroupLayout(p_tete_gauche2);
          p_tete_gauche2.setLayout(p_tete_gauche2Layout);
          p_tete_gauche2Layout.setHorizontalGroup(
            p_tete_gauche2Layout.createParallelGroup()
              .addGroup(p_tete_gauche2Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gauche2Layout.setVerticalGroup(
            p_tete_gauche2Layout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gauche2Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gauche2Layout.createParallelGroup()
                  .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche2);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(720, 360));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Croisement axes");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- CALIB ----
            CALIB.setName("CALIB");
            xTitledPanel1ContentContainer.add(CALIB);
            CALIB.setBounds(84, 20, 270, CALIB.getPreferredSize().height);

            //---- OBJ_50_OBJ_50 ----
            OBJ_50_OBJ_50.setText("@LIAX1@");
            OBJ_50_OBJ_50.setName("OBJ_50_OBJ_50");
            xTitledPanel1ContentContainer.add(OBJ_50_OBJ_50);
            OBJ_50_OBJ_50.setBounds(84, 72, 231, OBJ_50_OBJ_50.getPreferredSize().height);

            //---- OBJ_52_OBJ_52 ----
            OBJ_52_OBJ_52.setText("@LIAA1@");
            OBJ_52_OBJ_52.setName("OBJ_52_OBJ_52");
            xTitledPanel1ContentContainer.add(OBJ_52_OBJ_52);
            OBJ_52_OBJ_52.setBounds(410, 72, 231, OBJ_52_OBJ_52.getPreferredSize().height);

            //---- OBJ_54_OBJ_54 ----
            OBJ_54_OBJ_54.setText("@LIAX2@");
            OBJ_54_OBJ_54.setName("OBJ_54_OBJ_54");
            xTitledPanel1ContentContainer.add(OBJ_54_OBJ_54);
            OBJ_54_OBJ_54.setBounds(84, 101, 231, OBJ_54_OBJ_54.getPreferredSize().height);

            //---- OBJ_56_OBJ_56 ----
            OBJ_56_OBJ_56.setText("@LIAA2@");
            OBJ_56_OBJ_56.setName("OBJ_56_OBJ_56");
            xTitledPanel1ContentContainer.add(OBJ_56_OBJ_56);
            OBJ_56_OBJ_56.setBounds(410, 101, 231, OBJ_56_OBJ_56.getPreferredSize().height);

            //---- OBJ_58_OBJ_58 ----
            OBJ_58_OBJ_58.setText("@LIAX3@");
            OBJ_58_OBJ_58.setName("OBJ_58_OBJ_58");
            xTitledPanel1ContentContainer.add(OBJ_58_OBJ_58);
            OBJ_58_OBJ_58.setBounds(84, 130, 231, OBJ_58_OBJ_58.getPreferredSize().height);

            //---- OBJ_60_OBJ_60 ----
            OBJ_60_OBJ_60.setText("@LIAA3@");
            OBJ_60_OBJ_60.setName("OBJ_60_OBJ_60");
            xTitledPanel1ContentContainer.add(OBJ_60_OBJ_60);
            OBJ_60_OBJ_60.setBounds(410, 130, 231, OBJ_60_OBJ_60.getPreferredSize().height);

            //---- OBJ_62_OBJ_62 ----
            OBJ_62_OBJ_62.setText("@LIAX4@");
            OBJ_62_OBJ_62.setName("OBJ_62_OBJ_62");
            xTitledPanel1ContentContainer.add(OBJ_62_OBJ_62);
            OBJ_62_OBJ_62.setBounds(84, 159, 231, OBJ_62_OBJ_62.getPreferredSize().height);

            //---- OBJ_64_OBJ_64 ----
            OBJ_64_OBJ_64.setText("@LIAA4@");
            OBJ_64_OBJ_64.setName("OBJ_64_OBJ_64");
            xTitledPanel1ContentContainer.add(OBJ_64_OBJ_64);
            OBJ_64_OBJ_64.setBounds(410, 159, 231, OBJ_64_OBJ_64.getPreferredSize().height);

            //---- OBJ_66_OBJ_66 ----
            OBJ_66_OBJ_66.setText("@LIAX5@");
            OBJ_66_OBJ_66.setName("OBJ_66_OBJ_66");
            xTitledPanel1ContentContainer.add(OBJ_66_OBJ_66);
            OBJ_66_OBJ_66.setBounds(84, 188, 231, OBJ_66_OBJ_66.getPreferredSize().height);

            //---- OBJ_68_OBJ_68 ----
            OBJ_68_OBJ_68.setText("@LIAA5@");
            OBJ_68_OBJ_68.setName("OBJ_68_OBJ_68");
            xTitledPanel1ContentContainer.add(OBJ_68_OBJ_68);
            OBJ_68_OBJ_68.setBounds(410, 188, 231, OBJ_68_OBJ_68.getPreferredSize().height);

            //---- OBJ_70_OBJ_70 ----
            OBJ_70_OBJ_70.setText("@LIAX6@");
            OBJ_70_OBJ_70.setName("OBJ_70_OBJ_70");
            xTitledPanel1ContentContainer.add(OBJ_70_OBJ_70);
            OBJ_70_OBJ_70.setBounds(84, 217, 231, OBJ_70_OBJ_70.getPreferredSize().height);

            //---- OBJ_72_OBJ_72 ----
            OBJ_72_OBJ_72.setText("@LIAA6@");
            OBJ_72_OBJ_72.setName("OBJ_72_OBJ_72");
            xTitledPanel1ContentContainer.add(OBJ_72_OBJ_72);
            OBJ_72_OBJ_72.setBounds(410, 217, 231, OBJ_72_OBJ_72.getPreferredSize().height);

            //---- OBJ_73_OBJ_73 ----
            OBJ_73_OBJ_73.setText("Relation entre les axes");
            OBJ_73_OBJ_73.setName("OBJ_73_OBJ_73");
            xTitledPanel1ContentContainer.add(OBJ_73_OBJ_73);
            OBJ_73_OBJ_73.setBounds(70, 260, 190, 20);

            //---- OBJ_47_OBJ_47 ----
            OBJ_47_OBJ_47.setText("Libell\u00e9");
            OBJ_47_OBJ_47.setName("OBJ_47_OBJ_47");
            xTitledPanel1ContentContainer.add(OBJ_47_OBJ_47);
            OBJ_47_OBJ_47.setBounds(20, 24, 61, 20);

            //---- CAAX1 ----
            CAAX1.setName("CAAX1");
            xTitledPanel1ContentContainer.add(CAAX1);
            CAAX1.setBounds(330, 70, 60, CAAX1.getPreferredSize().height);

            //---- CAAX2 ----
            CAAX2.setName("CAAX2");
            xTitledPanel1ContentContainer.add(CAAX2);
            CAAX2.setBounds(330, 99, 60, CAAX2.getPreferredSize().height);

            //---- CAAX3 ----
            CAAX3.setName("CAAX3");
            xTitledPanel1ContentContainer.add(CAAX3);
            CAAX3.setBounds(330, 128, 60, CAAX3.getPreferredSize().height);

            //---- CAAX4 ----
            CAAX4.setName("CAAX4");
            xTitledPanel1ContentContainer.add(CAAX4);
            CAAX4.setBounds(330, 157, 60, CAAX4.getPreferredSize().height);

            //---- CAAX5 ----
            CAAX5.setName("CAAX5");
            xTitledPanel1ContentContainer.add(CAAX5);
            CAAX5.setBounds(330, 186, 60, CAAX5.getPreferredSize().height);

            //---- CAAX6 ----
            CAAX6.setName("CAAX6");
            xTitledPanel1ContentContainer.add(CAAX6);
            CAAX6.setBounds(330, 215, 60, CAAX6.getPreferredSize().height);

            //---- OBJ_49_OBJ_49 ----
            OBJ_49_OBJ_49.setText("Axe 1");
            OBJ_49_OBJ_49.setName("OBJ_49_OBJ_49");
            xTitledPanel1ContentContainer.add(OBJ_49_OBJ_49);
            OBJ_49_OBJ_49.setBounds(20, 74, 41, 20);

            //---- OBJ_53_OBJ_53 ----
            OBJ_53_OBJ_53.setText("Axe 2");
            OBJ_53_OBJ_53.setName("OBJ_53_OBJ_53");
            xTitledPanel1ContentContainer.add(OBJ_53_OBJ_53);
            OBJ_53_OBJ_53.setBounds(20, 103, 41, 20);

            //---- OBJ_57_OBJ_57 ----
            OBJ_57_OBJ_57.setText("Axe 3");
            OBJ_57_OBJ_57.setName("OBJ_57_OBJ_57");
            xTitledPanel1ContentContainer.add(OBJ_57_OBJ_57);
            OBJ_57_OBJ_57.setBounds(20, 132, 41, 20);

            //---- OBJ_61_OBJ_61 ----
            OBJ_61_OBJ_61.setText("Axe 4");
            OBJ_61_OBJ_61.setName("OBJ_61_OBJ_61");
            xTitledPanel1ContentContainer.add(OBJ_61_OBJ_61);
            OBJ_61_OBJ_61.setBounds(20, 161, 41, 20);

            //---- OBJ_65_OBJ_65 ----
            OBJ_65_OBJ_65.setText("Axe 5");
            OBJ_65_OBJ_65.setName("OBJ_65_OBJ_65");
            xTitledPanel1ContentContainer.add(OBJ_65_OBJ_65);
            OBJ_65_OBJ_65.setBounds(20, 190, 41, 20);

            //---- OBJ_69_OBJ_69 ----
            OBJ_69_OBJ_69.setText("Axe 6");
            OBJ_69_OBJ_69.setName("OBJ_69_OBJ_69");
            xTitledPanel1ContentContainer.add(OBJ_69_OBJ_69);
            OBJ_69_OBJ_69.setBounds(20, 219, 41, 20);

            //---- CACND ----
            CACND.setText("ET");
            CACND.setName("CACND");
            xTitledPanel1ContentContainer.add(CACND);
            CACND.setBounds(310, 261, 45, CACND.getPreferredSize().height);

            //---- CACND_OU ----
            CACND_OU.setText("OU");
            CACND_OU.setName("CACND_OU");
            xTitledPanel1ContentContainer.add(CACND_OU);
            CACND_OU.setBounds(375, 261, 50, CACND_OU.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 694, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 332, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }

    //---- CACND_GRP ----
    CACND_GRP.add(CACND);
    CACND_GRP.add(CACND_OU);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche2;
  private JLabel OBJ_44_OBJ_45;
  private XRiTextField INDETB;
  private JLabel OBJ_45_OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_46_OBJ_47;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private XRiTextField CALIB;
  private RiZoneSortie OBJ_50_OBJ_50;
  private RiZoneSortie OBJ_52_OBJ_52;
  private RiZoneSortie OBJ_54_OBJ_54;
  private RiZoneSortie OBJ_56_OBJ_56;
  private RiZoneSortie OBJ_58_OBJ_58;
  private RiZoneSortie OBJ_60_OBJ_60;
  private RiZoneSortie OBJ_62_OBJ_62;
  private RiZoneSortie OBJ_64_OBJ_64;
  private RiZoneSortie OBJ_66_OBJ_66;
  private RiZoneSortie OBJ_68_OBJ_68;
  private RiZoneSortie OBJ_70_OBJ_70;
  private RiZoneSortie OBJ_72_OBJ_72;
  private JLabel OBJ_73_OBJ_73;
  private JLabel OBJ_47_OBJ_47;
  private XRiTextField CAAX1;
  private XRiTextField CAAX2;
  private XRiTextField CAAX3;
  private XRiTextField CAAX4;
  private XRiTextField CAAX5;
  private XRiTextField CAAX6;
  private JLabel OBJ_49_OBJ_49;
  private JLabel OBJ_53_OBJ_53;
  private JLabel OBJ_57_OBJ_57;
  private JLabel OBJ_61_OBJ_61;
  private JLabel OBJ_65_OBJ_65;
  private JLabel OBJ_69_OBJ_69;
  private XRiRadioButton CACND;
  private XRiRadioButton CACND_OU;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  private ButtonGroup CACND_GRP;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
