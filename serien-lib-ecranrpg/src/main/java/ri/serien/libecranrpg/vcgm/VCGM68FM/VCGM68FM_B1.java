
package ri.serien.libecranrpg.vcgm.VCGM68FM;
// Nom Fichier: pop_VCGM68FM_FMTB1_1080.java

import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import com.jgoodies.forms.factories.DefaultComponentFactory;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGM68FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM68FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
    // setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MESERR@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    SUFFIX.setVisible(lexique.isPresent("SUFFIX"));
    PREFIX.setVisible(lexique.isPresent("PREFIX"));
    OBJ_28.setVisible(lexique.isPresent("MESERR"));
    CHEM2.setVisible(lexique.isPresent("CHEM2"));
    CHEM1.setVisible(lexique.isPresent("CHEM1"));
    
    // TODO Icones
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    OBJ_27.setIcon(lexique.chargerImage("images/retour.png", true));
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Chemin"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void VALActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="ENTER"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_27ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F10"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "scgm"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    CHEM1 = new XRiTextField();
    CHEM2 = new XRiTextField();
    OBJ_28 = new JLabel();
    OBJ_20 = new JLabel();
    OBJ_22 = new JLabel();
    PREFIX = new XRiTextField();
    SUFFIX = new XRiTextField();
    BT_ENTER = new JButton();
    OBJ_27 = new JButton();
    separator1 = compFactory.createSeparator("Chemin IFS pour relev\u00e9s de banque");
    OBJ_4 = new JMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();

    //======== this ========
    setName("this");

    //---- CHEM1 ----
    CHEM1.setComponentPopupMenu(BTD);
    CHEM1.setName("CHEM1");

    //---- CHEM2 ----
    CHEM2.setComponentPopupMenu(BTD);
    CHEM2.setName("CHEM2");

    //---- OBJ_28 ----
    OBJ_28.setText("@MESERR@");
    OBJ_28.setForeground(new Color(204, 0, 0));
    OBJ_28.setName("OBJ_28");

    //---- OBJ_20 ----
    OBJ_20.setText("Nom fichiers commencant par");
    OBJ_20.setName("OBJ_20");

    //---- OBJ_22 ----
    OBJ_22.setText("Nom fichiers finissant par");
    OBJ_22.setName("OBJ_22");

    //---- PREFIX ----
    PREFIX.setComponentPopupMenu(BTD);
    PREFIX.setName("PREFIX");

    //---- SUFFIX ----
    SUFFIX.setComponentPopupMenu(BTD);
    SUFFIX.setName("SUFFIX");

    //---- BT_ENTER ----
    BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_ENTER.setToolTipText("Ok");
    BT_ENTER.setName("BT_ENTER");
    BT_ENTER.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        VALActionPerformed(e);
      }
    });

    //---- OBJ_27 ----
    OBJ_27.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_27.setToolTipText("Retour");
    OBJ_27.setName("OBJ_27");
    OBJ_27.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_27ActionPerformed(e);
      }
    });

    //---- separator1 ----
    separator1.setName("separator1");

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(15, 15, 15)
          .addComponent(separator1, GroupLayout.PREFERRED_SIZE, 530, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(26, 26, 26)
          .addComponent(CHEM1, GroupLayout.PREFERRED_SIZE, 510, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(26, 26, 26)
          .addComponent(CHEM2, GroupLayout.PREFERRED_SIZE, 510, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(26, 26, 26)
          .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, 198, GroupLayout.PREFERRED_SIZE)
          .addGap(30, 30, 30)
          .addComponent(PREFIX, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(26, 26, 26)
          .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 169, GroupLayout.PREFERRED_SIZE)
          .addGap(59, 59, 59)
          .addComponent(SUFFIX, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(9, 9, 9)
          .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 321, GroupLayout.PREFERRED_SIZE)
          .addGap(110, 110, 110)
          .addComponent(BT_ENTER, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
          .addGap(6, 6, 6)
          .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(15, 15, 15)
          .addComponent(separator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          .addGap(19, 19, 19)
          .addComponent(CHEM1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          .addComponent(CHEM2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          .addGap(8, 8, 8)
          .addGroup(layout.createParallelGroup()
            .addGroup(layout.createSequentialGroup()
              .addGap(4, 4, 4)
              .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addComponent(PREFIX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          .addGap(1, 1, 1)
          .addGroup(layout.createParallelGroup()
            .addGroup(layout.createSequentialGroup()
              .addGap(4, 4, 4)
              .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addComponent(SUFFIX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          .addGap(11, 11, 11)
          .addGroup(layout.createParallelGroup()
            .addGroup(layout.createSequentialGroup()
              .addGap(12, 12, 12)
              .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addComponent(BT_ENTER, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
            .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
    );

    //======== OBJ_4 ========
    {
      OBJ_4.setText("Commandes");
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Choix du Papier");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setText("Annuler");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_9);
      OBJ_4.addSeparator();

      //---- OBJ_10 ----
      OBJ_10.setText("Exploitation");
      OBJ_10.setName("OBJ_10");
      OBJ_4.add(OBJ_10);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Invite");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private XRiTextField CHEM1;
  private XRiTextField CHEM2;
  private JLabel OBJ_28;
  private JLabel OBJ_20;
  private JLabel OBJ_22;
  private XRiTextField PREFIX;
  private XRiTextField SUFFIX;
  private JButton BT_ENTER;
  private JButton OBJ_27;
  private JComponent separator1;
  private JMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
