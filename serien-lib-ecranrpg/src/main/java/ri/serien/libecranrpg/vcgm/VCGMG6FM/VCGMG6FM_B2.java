
package ri.serien.libecranrpg.vcgm.VCGMG6FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;

/**
 * @author Stéphane Vénéri
 */
public class VCGMG6FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VCGMG6FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Bouton par défaut
    
    // Ajout
    initDiverses();
    
    sNBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    sNBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    sNBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    
    EDTCAI.setValeurs("1", buttonGroup1);
    EDTCAI_1.setValeurs("2");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _WTX01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Caisse"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    sNBarreBouton = new SNBarreBouton();
    p_principal = new JPanel();
    pnConteneur = new SNPanelContenu();
    sNPanelTitre1 = new SNPanelTitre();
    EDTCAI = new XRiRadioButton();
    EDTCAI_1 = new XRiRadioButton();
    buttonGroup1 = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(515, 215));
    setName("this");
    setLayout(new BorderLayout());

    //---- sNBarreBouton ----
    sNBarreBouton.setName("sNBarreBouton");
    add(sNBarreBouton, BorderLayout.SOUTH);

    //======== p_principal ========
    {
      p_principal.setBackground(new Color(239, 239, 222));
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== pnConteneur ========
      {
        pnConteneur.setBackground(new Color(239, 239, 222));
        pnConteneur.setName("pnConteneur");
        pnConteneur.setLayout(new GridLayout());

        //======== sNPanelTitre1 ========
        {
          sNPanelTitre1.setTitre("Validation de la caisse");
          sNPanelTitre1.setBackground(new Color(239, 239, 222));
          sNPanelTitre1.setName("sNPanelTitre1");
          sNPanelTitre1.setLayout(new GridBagLayout());
          ((GridBagLayout)sNPanelTitre1.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)sNPanelTitre1.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
          ((GridBagLayout)sNPanelTitre1.getLayout()).columnWeights = new double[] {1.0, 0.0, 1.0E-4};
          ((GridBagLayout)sNPanelTitre1.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

          //---- EDTCAI ----
          EDTCAI.setText("Edition de la  caisse provisoire");
          EDTCAI.setFont(EDTCAI.getFont().deriveFont(EDTCAI.getFont().getSize() + 2f));
          EDTCAI.setBackground(new Color(239, 239, 222));
          EDTCAI.setName("EDTCAI");
          sNPanelTitre1.add(EDTCAI, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- EDTCAI_1 ----
          EDTCAI_1.setText("Edition de la caisse d\u00e9finitive");
          EDTCAI_1.setFont(EDTCAI_1.getFont().deriveFont(EDTCAI_1.getFont().getSize() + 2f));
          EDTCAI_1.setBackground(new Color(239, 239, 222));
          EDTCAI_1.setName("EDTCAI_1");
          sNPanelTitre1.add(EDTCAI_1, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        }
        pnConteneur.add(sNPanelTitre1);
      }
      p_principal.add(pnConteneur, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //---- buttonGroup1 ----
    buttonGroup1.add(EDTCAI);
    buttonGroup1.add(EDTCAI_1);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBarreBouton sNBarreBouton;
  private JPanel p_principal;
  private SNPanelContenu pnConteneur;
  private SNPanelTitre sNPanelTitre1;
  private XRiRadioButton EDTCAI;
  private XRiRadioButton EDTCAI_1;
  private ButtonGroup buttonGroup1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
