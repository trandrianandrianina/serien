
package ri.serien.libecranrpg.vcgm.VCGM20FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM20FM_B6 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "HLD01", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 78, };
  
  public VCGM20FM_B6(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    REMFIL.setValeursSelection("X", "");
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, Constantes.CORRESPONDANCE_COULEURS);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    LIBTR1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBTR1@")).trim());
    LIBTR2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBTR2@")).trim());
    LIBTR3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBTR3@")).trim());
    LIBTR4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBTR4@")).trim());
    LIBTR5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBTR5@")).trim());
    LEM1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LEM1@")).trim());
    LEM2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LEM2@")).trim());
    LEM3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LEM3@")).trim());
    LEM4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LEM4@")).trim());
    LEM5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LEM5@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _WTP01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    if (lexique.isTrue("(71) AND (N72) AND (N75)")) {
      p_bpresentation.setText("Domiciliation (lettres de change) ");
    }
    else if (lexique.isTrue("(72) AND (N75)")) {
      p_bpresentation.setText("Domiciliation (virements France)");
    }
    else if (lexique.isTrue("(N71) AND (N72) AND (N75)")) {
      p_bpresentation.setText("Domiciliation (chèques à payer)");
    }
    else if (lexique.isTrue("75")) {
      p_bpresentation.setText("Domiciliation (virements étrangers)");
    }
    
    

    
    p_bpresentation.setCodeEtablissement(WEDO.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WEDO.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "+", "Enter");
    WTP01.setValeurTop("+");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "-", "Enter");
    WTP01.setValeurTop("-");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "1", "Enter");
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "2", "Enter");
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "3", "Enter");
    WTP01.setValeurTop("3");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "4", "Enter");
    WTP01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_27ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "5", "Enter");
    WTP01.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_28ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "6", "Enter");
    WTP01.setValeurTop("6");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_29ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "7", "Enter");
    WTP01.setValeurTop("7");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_30ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "8", "Enter");
    WTP01.setValeurTop("8");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_31ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "9", "Enter");
    WTP01.setValeurTop("9");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_32ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "D", "Enter");
    WTP01.setValeurTop("D");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_33ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "E", "Enter");
    WTP01.setValeurTop("E");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_34ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "M", "Enter");
    WTP01.setValeurTop("M");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_35ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "R", "Enter");
    WTP01.setValeurTop("R");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_36ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      // lexique.HostFieldGetData("WBDO"));
      if (lexique
          .HostFieldGetData("LD" + ((WTP01.getSelectedRow() + 1) < 10 ? "0" + (WTP01.getSelectedRow() + 1) : (WTP01.getSelectedRow() + 1)))
          .substring(66, 68).equalsIgnoreCase(lexique.HostFieldGetData("WBDO"))) {
        WTP01.setValeurTop("-");
        lexique.HostScreenSendKey(this, "Enter");
      }
      else {
        WTP01.setValeurTop("+");
        lexique.HostScreenSendKey(this, "Enter");
      }
    }
  }
  
  private void OBJ_170ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(2, 4);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", false);
  }
  
  private void OBJ_37ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("*");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_56 = new JLabel();
    WEDO = new XRiTextField();
    OBJ_58 = new JLabel();
    WBDO = new XRiTextField();
    JOLIBR = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_175 = new JLabel();
    P85MTT = new XRiTextField();
    OBJ_165 = new JLabel();
    WDECD = new XRiCalendrier();
    WDECF = new XRiCalendrier();
    WDECN = new XRiCalendrier();
    WNCGX = new XRiTextField();
    WNCA = new XRiTextField();
    OBJ_174 = new JLabel();
    WDNOM = new XRiTextField();
    OBJ_176 = new JLabel();
    OBJ_170 = new SNBoutonDetail();
    REMFIL = new XRiCheckBox();
    xTitledPanel2 = new JXTitledPanel();
    LIBTR1 = new RiZoneSortie();
    LIBTR2 = new RiZoneSortie();
    LIBTR3 = new RiZoneSortie();
    LIBTR4 = new RiZoneSortie();
    LIBTR5 = new RiZoneSortie();
    LEM1 = new RiZoneSortie();
    LEM2 = new RiZoneSortie();
    LEM3 = new RiZoneSortie();
    LEM4 = new RiZoneSortie();
    LEM5 = new RiZoneSortie();
    panel1 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BTD = new JPopupMenu();
    OBJ_21 = new JMenuItem();
    OBJ_37 = new JMenuItem();
    OBJ_22 = new JMenuItem();
    OBJ_23 = new JMenuItem();
    OBJ_24 = new JMenuItem();
    OBJ_25 = new JMenuItem();
    OBJ_26 = new JMenuItem();
    OBJ_27 = new JMenuItem();
    OBJ_28 = new JMenuItem();
    OBJ_29 = new JMenuItem();
    OBJ_30 = new JMenuItem();
    OBJ_31 = new JMenuItem();
    OBJ_32 = new JMenuItem();
    OBJ_33 = new JMenuItem();
    OBJ_34 = new JMenuItem();
    OBJ_35 = new JMenuItem();
    OBJ_36 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Domiciliation (lettres de change)");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_56 ----
          OBJ_56.setText("Soci\u00e9t\u00e9");
          OBJ_56.setName("OBJ_56");

          //---- WEDO ----
          WEDO.setName("WEDO");

          //---- OBJ_58 ----
          OBJ_58.setText("Banque");
          OBJ_58.setName("OBJ_58");

          //---- WBDO ----
          WBDO.setComponentPopupMenu(BTD);
          WBDO.setName("WBDO");

          //---- JOLIBR ----
          JOLIBR.setName("JOLIBR");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(WEDO, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19)
                .addComponent(WBDO, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(JOLIBR, GroupLayout.PREFERRED_SIZE, 211, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(WEDO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(WBDO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(JOLIBR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Lettres de change");
              riSousMenu_bt6.setToolTipText("Domiciliation des lettres de change");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Ch\u00e8ques \u00e0 payer");
              riSousMenu_bt7.setToolTipText("Domiciliation des ch\u00e8ques \u00e0 payer");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Virements");
              riSousMenu_bt8.setToolTipText("Domiciliation des virements");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Domiciliation automatique");
              riSousMenu_bt9.setToolTipText("Domiciliation automatique");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Autres vues");
              riSousMenu_bt10.setToolTipText("Autres vues de la liste");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 570));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setTitle("Filtres");
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- OBJ_175 ----
            OBJ_175.setText("Ech\u00e9ance");
            OBJ_175.setName("OBJ_175");
            xTitledPanel1ContentContainer.add(OBJ_175);
            OBJ_175.setBounds(30, 74, 66, 20);

            //---- P85MTT ----
            P85MTT.setComponentPopupMenu(BTD);
            P85MTT.setName("P85MTT");
            xTitledPanel1ContentContainer.add(P85MTT);
            P85MTT.setBounds(110, 10, 70, P85MTT.getPreferredSize().height);

            //---- OBJ_165 ----
            OBJ_165.setText("Montant");
            OBJ_165.setName("OBJ_165");
            xTitledPanel1ContentContainer.add(OBJ_165);
            OBJ_165.setBounds(30, 14, 55, 20);

            //---- WDECD ----
            WDECD.setComponentPopupMenu(BTD);
            WDECD.setName("WDECD");
            xTitledPanel1ContentContainer.add(WDECD);
            WDECD.setBounds(110, 70, 105, WDECD.getPreferredSize().height);

            //---- WDECF ----
            WDECF.setComponentPopupMenu(BTD);
            WDECF.setName("WDECF");
            xTitledPanel1ContentContainer.add(WDECF);
            WDECF.setBounds(210, 70, 105, WDECF.getPreferredSize().height);

            //---- WDECN ----
            WDECN.setComponentPopupMenu(BTD);
            WDECN.setName("WDECN");
            xTitledPanel1ContentContainer.add(WDECN);
            WDECN.setBounds(340, 70, 105, WDECN.getPreferredSize().height);

            //---- WNCGX ----
            WNCGX.setComponentPopupMenu(BTD);
            WNCGX.setName("WNCGX");
            xTitledPanel1ContentContainer.add(WNCGX);
            WNCGX.setBounds(110, 40, 70, WNCGX.getPreferredSize().height);

            //---- WNCA ----
            WNCA.setComponentPopupMenu(BTD);
            WNCA.setName("WNCA");
            xTitledPanel1ContentContainer.add(WNCA);
            WNCA.setBounds(185, 40, 70, WNCA.getPreferredSize().height);

            //---- OBJ_174 ----
            OBJ_174.setText("Tiers");
            OBJ_174.setName("OBJ_174");
            xTitledPanel1ContentContainer.add(OBJ_174);
            OBJ_174.setBounds(30, 44, 43, 20);

            //---- WDNOM ----
            WDNOM.setComponentPopupMenu(BTD);
            WDNOM.setName("WDNOM");
            xTitledPanel1ContentContainer.add(WDNOM);
            WDNOM.setBounds(110, 100, 210, WDNOM.getPreferredSize().height);

            //---- OBJ_176 ----
            OBJ_176.setText("Alpha");
            OBJ_176.setName("OBJ_176");
            xTitledPanel1ContentContainer.add(OBJ_176);
            OBJ_176.setBounds(30, 104, 66, 20);

            //---- OBJ_170 ----
            OBJ_170.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_170.setName("OBJ_170");
            OBJ_170.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_170ActionPerformed(e);
              }
            });
            xTitledPanel1ContentContainer.add(OBJ_170);
            OBJ_170.setBounds(305, 72, 30, 24);

            //---- REMFIL ----
            REMFIL.setText("Domicili\u00e9 seulement");
            REMFIL.setName("REMFIL");
            xTitledPanel1ContentContainer.add(REMFIL);
            REMFIL.setBounds(30, 135, 155, 25);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setTitle("R\u00e9capitulatif remise");
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //---- LIBTR1 ----
            LIBTR1.setText("@LIBTR1@");
            LIBTR1.setName("LIBTR1");
            xTitledPanel2ContentContainer.add(LIBTR1);
            LIBTR1.setBounds(15, 10, 175, LIBTR1.getPreferredSize().height);

            //---- LIBTR2 ----
            LIBTR2.setText("@LIBTR2@");
            LIBTR2.setName("LIBTR2");
            xTitledPanel2ContentContainer.add(LIBTR2);
            LIBTR2.setBounds(15, 41, 175, LIBTR2.getPreferredSize().height);

            //---- LIBTR3 ----
            LIBTR3.setText("@LIBTR3@");
            LIBTR3.setName("LIBTR3");
            xTitledPanel2ContentContainer.add(LIBTR3);
            LIBTR3.setBounds(15, 72, 175, LIBTR3.getPreferredSize().height);

            //---- LIBTR4 ----
            LIBTR4.setText("@LIBTR4@");
            LIBTR4.setName("LIBTR4");
            xTitledPanel2ContentContainer.add(LIBTR4);
            LIBTR4.setBounds(15, 103, 175, LIBTR4.getPreferredSize().height);

            //---- LIBTR5 ----
            LIBTR5.setText("@LIBTR5@");
            LIBTR5.setName("LIBTR5");
            xTitledPanel2ContentContainer.add(LIBTR5);
            LIBTR5.setBounds(15, 134, 175, LIBTR5.getPreferredSize().height);

            //---- LEM1 ----
            LEM1.setText("@LEM1@");
            LEM1.setHorizontalAlignment(SwingConstants.RIGHT);
            LEM1.setName("LEM1");
            xTitledPanel2ContentContainer.add(LEM1);
            LEM1.setBounds(210, 10, 175, LEM1.getPreferredSize().height);

            //---- LEM2 ----
            LEM2.setText("@LEM2@");
            LEM2.setHorizontalAlignment(SwingConstants.RIGHT);
            LEM2.setName("LEM2");
            xTitledPanel2ContentContainer.add(LEM2);
            LEM2.setBounds(210, 41, 175, LEM2.getPreferredSize().height);

            //---- LEM3 ----
            LEM3.setText("@LEM3@");
            LEM3.setHorizontalAlignment(SwingConstants.RIGHT);
            LEM3.setName("LEM3");
            xTitledPanel2ContentContainer.add(LEM3);
            LEM3.setBounds(210, 72, 175, LEM3.getPreferredSize().height);

            //---- LEM4 ----
            LEM4.setText("@LEM4@");
            LEM4.setHorizontalAlignment(SwingConstants.RIGHT);
            LEM4.setName("LEM4");
            xTitledPanel2ContentContainer.add(LEM4);
            LEM4.setBounds(210, 103, 175, LEM4.getPreferredSize().height);

            //---- LEM5 ----
            LEM5.setText("@LEM5@");
            LEM5.setHorizontalAlignment(SwingConstants.RIGHT);
            LEM5.setName("LEM5");
            xTitledPanel2ContentContainer.add(LEM5);
            LEM5.setBounds(210, 134, 175, LEM5.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("R\u00e9sultat de la recherche"));
            panel1.setOpaque(false);
            panel1.setComponentPopupMenu(BTD);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setToolTipText("<HTML>En s\u00e9lection multiple, seule la <BR>domiciliation est g\u00e9r\u00e9e</HTML>");
              SCROLLPANE_LIST.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST.setViewportView(WTP01);
            }
            panel1.add(SCROLLPANE_LIST);
            SCROLLPANE_LIST.setBounds(25, 40, 790, 270);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(830, 40, 25, 125);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(830, 185, 25, 125);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 452, Short.MAX_VALUE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 410, GroupLayout.PREFERRED_SIZE))
                  .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 874, Short.MAX_VALUE))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                  .addComponent(xTitledPanel2, GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 335, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_21 ----
      OBJ_21.setText("Domicilier");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);

      //---- OBJ_37 ----
      OBJ_37.setText("Domicilier (sans escompte)");
      OBJ_37.setName("OBJ_37");
      OBJ_37.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_37ActionPerformed(e);
        }
      });
      BTD.add(OBJ_37);

      //---- OBJ_22 ----
      OBJ_22.setText("Supprimer la domiciliation");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);

      //---- OBJ_23 ----
      OBJ_23.setText("1 domiciliation/regrouper dettes");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD.add(OBJ_23);

      //---- OBJ_24 ----
      OBJ_24.setText("2 domiciliation/regrouper dettes");
      OBJ_24.setName("OBJ_24");
      OBJ_24.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_24ActionPerformed(e);
        }
      });
      BTD.add(OBJ_24);

      //---- OBJ_25 ----
      OBJ_25.setText("3 domiciliation/regrouper dettes");
      OBJ_25.setName("OBJ_25");
      OBJ_25.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_25ActionPerformed(e);
        }
      });
      BTD.add(OBJ_25);

      //---- OBJ_26 ----
      OBJ_26.setText("4 domiciliation/regrouper dettes");
      OBJ_26.setName("OBJ_26");
      OBJ_26.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_26ActionPerformed(e);
        }
      });
      BTD.add(OBJ_26);

      //---- OBJ_27 ----
      OBJ_27.setText("5 domiciliation/regrouper dettes");
      OBJ_27.setName("OBJ_27");
      OBJ_27.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_27ActionPerformed(e);
        }
      });
      BTD.add(OBJ_27);

      //---- OBJ_28 ----
      OBJ_28.setText("6 domiciliation/regrouper dettes");
      OBJ_28.setName("OBJ_28");
      OBJ_28.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_28ActionPerformed(e);
        }
      });
      BTD.add(OBJ_28);

      //---- OBJ_29 ----
      OBJ_29.setText("7 domiciliation/regrouper dettes");
      OBJ_29.setName("OBJ_29");
      OBJ_29.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_29ActionPerformed(e);
        }
      });
      BTD.add(OBJ_29);

      //---- OBJ_30 ----
      OBJ_30.setText("8 domiciliation/regrouper dettes");
      OBJ_30.setName("OBJ_30");
      OBJ_30.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_30ActionPerformed(e);
        }
      });
      BTD.add(OBJ_30);

      //---- OBJ_31 ----
      OBJ_31.setText("9 domiciliation/regrouper dettes");
      OBJ_31.setName("OBJ_31");
      OBJ_31.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_31ActionPerformed(e);
        }
      });
      BTD.add(OBJ_31);

      //---- OBJ_32 ----
      OBJ_32.setText("Modif. date \u00e9ch\u00e9ance + domiciliation");
      OBJ_32.setName("OBJ_32");
      OBJ_32.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_32ActionPerformed(e);
        }
      });
      BTD.add(OBJ_32);

      //---- OBJ_33 ----
      OBJ_33.setText("Modif. date \u00e9ch. + domi. + %esc. + code TVA");
      OBJ_33.setName("OBJ_33");
      OBJ_33.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_33ActionPerformed(e);
        }
      });
      BTD.add(OBJ_33);

      //---- OBJ_34 ----
      OBJ_34.setText("Modif. date \u00e9ch\u00e9ance");
      OBJ_34.setName("OBJ_34");
      OBJ_34.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_34ActionPerformed(e);
        }
      });
      BTD.add(OBJ_34);

      //---- OBJ_35 ----
      OBJ_35.setText("RAZ date \u00e9ch\u00e9ance + domiciliation");
      OBJ_35.setName("OBJ_35");
      OBJ_35.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_35ActionPerformed(e);
        }
      });
      BTD.add(OBJ_35);
      BTD.addSeparator();

      //---- OBJ_36 ----
      OBJ_36.setText("Aide en ligne");
      OBJ_36.setName("OBJ_36");
      OBJ_36.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_36ActionPerformed(e);
        }
      });
      BTD.add(OBJ_36);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_56;
  private XRiTextField WEDO;
  private JLabel OBJ_58;
  private XRiTextField WBDO;
  private XRiTextField JOLIBR;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_175;
  private XRiTextField P85MTT;
  private JLabel OBJ_165;
  private XRiCalendrier WDECD;
  private XRiCalendrier WDECF;
  private XRiCalendrier WDECN;
  private XRiTextField WNCGX;
  private XRiTextField WNCA;
  private JLabel OBJ_174;
  private XRiTextField WDNOM;
  private JLabel OBJ_176;
  private SNBoutonDetail OBJ_170;
  private XRiCheckBox REMFIL;
  private JXTitledPanel xTitledPanel2;
  private RiZoneSortie LIBTR1;
  private RiZoneSortie LIBTR2;
  private RiZoneSortie LIBTR3;
  private RiZoneSortie LIBTR4;
  private RiZoneSortie LIBTR5;
  private RiZoneSortie LEM1;
  private RiZoneSortie LEM2;
  private RiZoneSortie LEM3;
  private RiZoneSortie LEM4;
  private RiZoneSortie LEM5;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu BTD;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_37;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_24;
  private JMenuItem OBJ_25;
  private JMenuItem OBJ_26;
  private JMenuItem OBJ_27;
  private JMenuItem OBJ_28;
  private JMenuItem OBJ_29;
  private JMenuItem OBJ_30;
  private JMenuItem OBJ_31;
  private JMenuItem OBJ_32;
  private JMenuItem OBJ_33;
  private JMenuItem OBJ_34;
  private JMenuItem OBJ_35;
  private JMenuItem OBJ_36;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
