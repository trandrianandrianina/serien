
package ri.serien.libecranrpg.vcgm.VCGM01FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.LayoutStyle;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM01FM_OC extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] OCSNS_Value = { "-", "+", };
  private String[] OCSNS_Title = { "-", "+", };
  
  public VCGM01FM_OC(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    OCSNS.setValeurs(OCSNS_Value, OCSNS_Title);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDTYP@")).trim());
    INDIND.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDIND@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OCLA1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OCLA1@")).trim());
    OCLA2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OCLA2@")).trim());
    OCLA3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OCLA3@")).trim());
    OCLA4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OCLA4@")).trim());
    OCLA5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OCLA5@")).trim());
    OCLA6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OCLA6@")).trim());
    OCLNA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OCLNA@")).trim());
    OCLSA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OCLSA@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44_OBJ_45 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_45_OBJ_46 = new JLabel();
    INDTYP = new RiZoneSortie();
    OBJ_46_OBJ_47 = new JLabel();
    INDIND = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JXTitledPanel();
    OCLIB = new XRiTextField();
    OCNCG = new XRiTextField();
    OBJ_47_OBJ_47 = new JLabel();
    OBJ_45_OBJ_45 = new JLabel();
    OBJ_47_OBJ_48 = new JLabel();
    OCSNS = new XRiComboBox();
    OBJ_25_OBJ_25 = new JLabel();
    OCNCA = new XRiTextField();
    panel2 = new JPanel();
    label1 = new JLabel();
    OCAX1 = new XRiTextField();
    OCLA1 = new RiZoneSortie();
    label2 = new JLabel();
    OCAX2 = new XRiTextField();
    OCLA2 = new RiZoneSortie();
    label3 = new JLabel();
    OCAX3 = new XRiTextField();
    OCLA3 = new RiZoneSortie();
    label4 = new JLabel();
    OCAX4 = new XRiTextField();
    OCLA4 = new RiZoneSortie();
    label5 = new JLabel();
    OCAX5 = new XRiTextField();
    OCLA5 = new RiZoneSortie();
    label6 = new JLabel();
    OCAX6 = new XRiTextField();
    OCLA6 = new RiZoneSortie();
    label7 = new JLabel();
    OCNAT = new XRiTextField();
    OCLNA = new RiZoneSortie();
    label8 = new JLabel();
    OCSA = new XRiTextField();
    OCLSA = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_44_OBJ_45 ----
          OBJ_44_OBJ_45.setText("Soci\u00e9t\u00e9");
          OBJ_44_OBJ_45.setName("OBJ_44_OBJ_45");
          p_tete_gauche.add(OBJ_44_OBJ_45);
          OBJ_44_OBJ_45.setBounds(5, 3, 52, 18);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(70, 0, 40, INDETB.getPreferredSize().height);

          //---- OBJ_45_OBJ_46 ----
          OBJ_45_OBJ_46.setText("Code");
          OBJ_45_OBJ_46.setName("OBJ_45_OBJ_46");
          p_tete_gauche.add(OBJ_45_OBJ_46);
          OBJ_45_OBJ_46.setBounds(140, 3, 45, 18);

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setOpaque(false);
          INDTYP.setText("@INDTYP@");
          INDTYP.setName("INDTYP");
          p_tete_gauche.add(INDTYP);
          INDTYP.setBounds(195, 0, 34, INDTYP.getPreferredSize().height);

          //---- OBJ_46_OBJ_47 ----
          OBJ_46_OBJ_47.setText("Ordre");
          OBJ_46_OBJ_47.setName("OBJ_46_OBJ_47");
          p_tete_gauche.add(OBJ_46_OBJ_47);
          OBJ_46_OBJ_47.setBounds(260, 3, 45, 18);

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setOpaque(false);
          INDIND.setText("@INDIND@");
          INDIND.setName("INDIND");
          p_tete_gauche.add(INDIND);
          INDIND.setBounds(310, 0, 60, INDIND.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_tete_gauche.getComponentCount(); i++) {
              Rectangle bounds = p_tete_gauche.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_tete_gauche.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_tete_gauche.setMinimumSize(preferredSize);
            p_tete_gauche.setPreferredSize(preferredSize);
          }
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 380));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(886, 350));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setTitle("Op\u00e9ration de caisse");
            panel1.setBorder(new DropShadowBorder());
            panel1.setName("panel1");
            Container panel1ContentContainer = panel1.getContentContainer();
            panel1ContentContainer.setLayout(null);

            //---- OCLIB ----
            OCLIB.setComponentPopupMenu(BTD);
            OCLIB.setName("OCLIB");
            panel1ContentContainer.add(OCLIB);
            OCLIB.setBounds(70, 25, 310, OCLIB.getPreferredSize().height);

            //---- OCNCG ----
            OCNCG.setComponentPopupMenu(BTD);
            OCNCG.setName("OCNCG");
            panel1ContentContainer.add(OCNCG);
            OCNCG.setBounds(650, 25, 60, 28);

            //---- OBJ_47_OBJ_47 ----
            OBJ_47_OBJ_47.setText("Compte g\u00e9n\u00e9ral");
            OBJ_47_OBJ_47.setName("OBJ_47_OBJ_47");
            panel1ContentContainer.add(OBJ_47_OBJ_47);
            OBJ_47_OBJ_47.setBounds(550, 25, 130, 28);

            //---- OBJ_45_OBJ_45 ----
            OBJ_45_OBJ_45.setText("Libell\u00e9");
            OBJ_45_OBJ_45.setName("OBJ_45_OBJ_45");
            panel1ContentContainer.add(OBJ_45_OBJ_45);
            OBJ_45_OBJ_45.setBounds(12, 25, 43, 28);

            //---- OBJ_47_OBJ_48 ----
            OBJ_47_OBJ_48.setText("Auxiliaire");
            OBJ_47_OBJ_48.setName("OBJ_47_OBJ_48");
            panel1ContentContainer.add(OBJ_47_OBJ_48);
            OBJ_47_OBJ_48.setBounds(730, 25, 70, 28);

            //---- OCSNS ----
            OCSNS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OCSNS.setName("OCSNS");
            panel1ContentContainer.add(OCSNS);
            OCSNS.setBounds(450, 26, 55, OCSNS.getPreferredSize().height);

            //---- OBJ_25_OBJ_25 ----
            OBJ_25_OBJ_25.setText("Sens");
            OBJ_25_OBJ_25.setName("OBJ_25_OBJ_25");
            panel1ContentContainer.add(OBJ_25_OBJ_25);
            OBJ_25_OBJ_25.setBounds(400, 25, 43, 28);

            //---- OCNCA ----
            OCNCA.setComponentPopupMenu(BTD);
            OCNCA.setName("OCNCA");
            panel1ContentContainer.add(OCNCA);
            OCNCA.setBounds(795, 25, 60, OCNCA.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = panel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1ContentContainer.setMinimumSize(preferredSize);
              panel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- label1 ----
            label1.setText("Axe 1");
            label1.setName("label1");
            panel2.add(label1);
            label1.setBounds(75, 45, 65, 28);

            //---- OCAX1 ----
            OCAX1.setName("OCAX1");
            panel2.add(OCAX1);
            OCAX1.setBounds(140, 45, 60, OCAX1.getPreferredSize().height);

            //---- OCLA1 ----
            OCLA1.setText("@OCLA1@");
            OCLA1.setName("OCLA1");
            panel2.add(OCLA1);
            OCLA1.setBounds(205, 45, 310, OCLA1.getPreferredSize().height);

            //---- label2 ----
            label2.setText("Axe 2");
            label2.setName("label2");
            panel2.add(label2);
            label2.setBounds(75, 70, 65, 28);

            //---- OCAX2 ----
            OCAX2.setName("OCAX2");
            panel2.add(OCAX2);
            OCAX2.setBounds(140, 70, 60, OCAX2.getPreferredSize().height);

            //---- OCLA2 ----
            OCLA2.setText("@OCLA2@");
            OCLA2.setName("OCLA2");
            panel2.add(OCLA2);
            OCLA2.setBounds(205, 70, 310, OCLA2.getPreferredSize().height);

            //---- label3 ----
            label3.setText("Axe 3");
            label3.setName("label3");
            panel2.add(label3);
            label3.setBounds(75, 95, 65, 28);

            //---- OCAX3 ----
            OCAX3.setName("OCAX3");
            panel2.add(OCAX3);
            OCAX3.setBounds(140, 95, 60, OCAX3.getPreferredSize().height);

            //---- OCLA3 ----
            OCLA3.setText("@OCLA3@");
            OCLA3.setName("OCLA3");
            panel2.add(OCLA3);
            OCLA3.setBounds(205, 95, 310, OCLA3.getPreferredSize().height);

            //---- label4 ----
            label4.setText("Axe 4");
            label4.setName("label4");
            panel2.add(label4);
            label4.setBounds(75, 120, 65, 28);

            //---- OCAX4 ----
            OCAX4.setName("OCAX4");
            panel2.add(OCAX4);
            OCAX4.setBounds(140, 120, 60, OCAX4.getPreferredSize().height);

            //---- OCLA4 ----
            OCLA4.setText("@OCLA4@");
            OCLA4.setName("OCLA4");
            panel2.add(OCLA4);
            OCLA4.setBounds(205, 120, 310, OCLA4.getPreferredSize().height);

            //---- label5 ----
            label5.setText("Axe 5");
            label5.setName("label5");
            panel2.add(label5);
            label5.setBounds(75, 145, 65, 28);

            //---- OCAX5 ----
            OCAX5.setName("OCAX5");
            panel2.add(OCAX5);
            OCAX5.setBounds(140, 145, 60, OCAX5.getPreferredSize().height);

            //---- OCLA5 ----
            OCLA5.setText("@OCLA5@");
            OCLA5.setName("OCLA5");
            panel2.add(OCLA5);
            OCLA5.setBounds(205, 145, 310, OCLA5.getPreferredSize().height);

            //---- label6 ----
            label6.setText("Axe 6");
            label6.setName("label6");
            panel2.add(label6);
            label6.setBounds(75, 170, 65, 28);

            //---- OCAX6 ----
            OCAX6.setName("OCAX6");
            panel2.add(OCAX6);
            OCAX6.setBounds(140, 170, 60, OCAX6.getPreferredSize().height);

            //---- OCLA6 ----
            OCLA6.setText("@OCLA6@");
            OCLA6.setName("OCLA6");
            panel2.add(OCLA6);
            OCLA6.setBounds(205, 170, 310, OCLA6.getPreferredSize().height);

            //---- label7 ----
            label7.setText("Nature");
            label7.setName("label7");
            panel2.add(label7);
            label7.setBounds(75, 205, 65, 28);

            //---- OCNAT ----
            OCNAT.setName("OCNAT");
            panel2.add(OCNAT);
            OCNAT.setBounds(140, 205, 60, OCNAT.getPreferredSize().height);

            //---- OCLNA ----
            OCLNA.setText("@OCLNA@");
            OCLNA.setName("OCLNA");
            panel2.add(OCLNA);
            OCLNA.setBounds(205, 205, 310, OCLNA.getPreferredSize().height);

            //---- label8 ----
            label8.setText("Section");
            label8.setName("label8");
            panel2.add(label8);
            label8.setBounds(75, 13, 65, 28);

            //---- OCSA ----
            OCSA.setName("OCSA");
            panel2.add(OCSA);
            OCSA.setBounds(140, 13, 60, OCSA.getPreferredSize().height);

            //---- OCLSA ----
            OCLSA.setText("@OCLSA@");
            OCLSA.setName("OCLSA");
            panel2.add(OCLSA);
            OCLSA.setBounds(205, 15, 310, OCLSA.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 874, Short.MAX_VALUE)
                  .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 874, Short.MAX_VALUE))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 243, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44_OBJ_45;
  private RiZoneSortie INDETB;
  private JLabel OBJ_45_OBJ_46;
  private RiZoneSortie INDTYP;
  private JLabel OBJ_46_OBJ_47;
  private RiZoneSortie INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel panel1;
  private XRiTextField OCLIB;
  private XRiTextField OCNCG;
  private JLabel OBJ_47_OBJ_47;
  private JLabel OBJ_45_OBJ_45;
  private JLabel OBJ_47_OBJ_48;
  private XRiComboBox OCSNS;
  private JLabel OBJ_25_OBJ_25;
  private XRiTextField OCNCA;
  private JPanel panel2;
  private JLabel label1;
  private XRiTextField OCAX1;
  private RiZoneSortie OCLA1;
  private JLabel label2;
  private XRiTextField OCAX2;
  private RiZoneSortie OCLA2;
  private JLabel label3;
  private XRiTextField OCAX3;
  private RiZoneSortie OCLA3;
  private JLabel label4;
  private XRiTextField OCAX4;
  private RiZoneSortie OCLA4;
  private JLabel label5;
  private XRiTextField OCAX5;
  private RiZoneSortie OCLA5;
  private JLabel label6;
  private XRiTextField OCAX6;
  private RiZoneSortie OCLA6;
  private JLabel label7;
  private XRiTextField OCNAT;
  private RiZoneSortie OCLNA;
  private JLabel label8;
  private XRiTextField OCSA;
  private RiZoneSortie OCLSA;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
