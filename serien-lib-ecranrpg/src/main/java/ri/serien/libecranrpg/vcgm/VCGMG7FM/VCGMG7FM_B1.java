
package ri.serien.libecranrpg.vcgm.VCGMG7FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGMG7FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VCGMG7FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Ajoute à la liste des oData les variables non liées directement à un composant graphique
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _LA01_Top);
    
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    
    // Titre
    // setTitle(???);
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    snBarreBouton = new SNBarreBouton();
    snPanelPrincipal = new SNPanelContenu();
    snModificationOpération = new SNPanelTitre();
    lbLibelle = new SNLabelChamp();
    WLIB = new XRiTextField();
    lbMontant = new SNLabelChamp();
    WMTT = new XRiTextField();
    pnAnalytique = new SNPanelTitre();
    lbNature = new SNLabelChamp();
    WNAT = new XRiTextField();
    lbAxe1 = new SNLabelChamp();
    WAX1 = new XRiTextField();
    lbAxe2 = new SNLabelChamp();
    WAX2 = new XRiTextField();
    lbAxe3 = new SNLabelChamp();
    WAX3 = new XRiTextField();
    lbAxe4 = new SNLabelChamp();
    WAX4 = new XRiTextField();
    lbAxe5 = new SNLabelChamp();
    WAX5 = new XRiTextField();
    lbAxe6 = new SNLabelChamp();
    WAX6 = new XRiTextField();

    //======== this ========
    setPreferredSize(new Dimension(703, 315));
    setMaximumSize(new Dimension(703, 315));
    setName("this");
    setLayout(new BorderLayout());

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //======== snPanelPrincipal ========
    {
      snPanelPrincipal.setName("snPanelPrincipal");
      snPanelPrincipal.setLayout(new GridBagLayout());
      ((GridBagLayout)snPanelPrincipal.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)snPanelPrincipal.getLayout()).rowHeights = new int[] {0, 0, 0};
      ((GridBagLayout)snPanelPrincipal.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)snPanelPrincipal.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

      //======== snModificationOpération ========
      {
        snModificationOpération.setTitre("Modification d'une op\u00e9ration de caisse");
        snModificationOpération.setName("snModificationOp\u00e9ration");
        snModificationOpération.setLayout(new GridBagLayout());
        ((GridBagLayout)snModificationOpération.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)snModificationOpération.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)snModificationOpération.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
        ((GridBagLayout)snModificationOpération.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //---- lbLibelle ----
        lbLibelle.setText("Libell\u00e9");
        lbLibelle.setName("lbLibelle");
        snModificationOpération.add(lbLibelle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- WLIB ----
        WLIB.setPreferredSize(new Dimension(350, 28));
        WLIB.setMinimumSize(new Dimension(350, 28));
        WLIB.setMaximumSize(new Dimension(350, 28));
        WLIB.setName("WLIB");
        snModificationOpération.add(WLIB, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbMontant ----
        lbMontant.setText("Montant");
        lbMontant.setPreferredSize(new Dimension(150, 30));
        lbMontant.setMinimumSize(new Dimension(150, 30));
        lbMontant.setMaximumSize(new Dimension(150, 30));
        lbMontant.setName("lbMontant");
        snModificationOpération.add(lbMontant, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- WMTT ----
        WMTT.setPreferredSize(new Dimension(100, 28));
        WMTT.setMaximumSize(new Dimension(100, 28));
        WMTT.setMinimumSize(new Dimension(100, 28));
        WMTT.setName("WMTT");
        snModificationOpération.add(WMTT, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      snPanelPrincipal.add(snModificationOpération, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));

      //======== pnAnalytique ========
      {
        pnAnalytique.setTitre("Modification analytique");
        pnAnalytique.setName("pnAnalytique");
        pnAnalytique.setLayout(new GridBagLayout());
        ((GridBagLayout)pnAnalytique.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0};
        ((GridBagLayout)pnAnalytique.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnAnalytique.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnAnalytique.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //---- lbNature ----
        lbNature.setText("Nature");
        lbNature.setName("lbNature");
        pnAnalytique.add(lbNature, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- WNAT ----
        WNAT.setPreferredSize(new Dimension(80, 28));
        WNAT.setMinimumSize(new Dimension(80, 28));
        WNAT.setMaximumSize(new Dimension(80, 28));
        WNAT.setName("WNAT");
        pnAnalytique.add(WNAT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- lbAxe1 ----
        lbAxe1.setText("Axe 1");
        lbAxe1.setMaximumSize(new Dimension(50, 30));
        lbAxe1.setMinimumSize(new Dimension(50, 30));
        lbAxe1.setPreferredSize(new Dimension(50, 30));
        lbAxe1.setName("lbAxe1");
        pnAnalytique.add(lbAxe1, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- WAX1 ----
        WAX1.setPreferredSize(new Dimension(80, 28));
        WAX1.setMinimumSize(new Dimension(80, 28));
        WAX1.setMaximumSize(new Dimension(80, 28));
        WAX1.setName("WAX1");
        pnAnalytique.add(WAX1, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- lbAxe2 ----
        lbAxe2.setText("Axe 2");
        lbAxe2.setMaximumSize(new Dimension(50, 30));
        lbAxe2.setMinimumSize(new Dimension(50, 30));
        lbAxe2.setPreferredSize(new Dimension(50, 30));
        lbAxe2.setName("lbAxe2");
        pnAnalytique.add(lbAxe2, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- WAX2 ----
        WAX2.setPreferredSize(new Dimension(80, 28));
        WAX2.setMinimumSize(new Dimension(80, 28));
        WAX2.setMaximumSize(new Dimension(80, 28));
        WAX2.setName("WAX2");
        pnAnalytique.add(WAX2, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- lbAxe3 ----
        lbAxe3.setText("Axe 3");
        lbAxe3.setMaximumSize(new Dimension(50, 30));
        lbAxe3.setMinimumSize(new Dimension(50, 30));
        lbAxe3.setPreferredSize(new Dimension(50, 30));
        lbAxe3.setName("lbAxe3");
        pnAnalytique.add(lbAxe3, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- WAX3 ----
        WAX3.setPreferredSize(new Dimension(80, 28));
        WAX3.setMinimumSize(new Dimension(80, 28));
        WAX3.setMaximumSize(new Dimension(80, 28));
        WAX3.setName("WAX3");
        pnAnalytique.add(WAX3, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbAxe4 ----
        lbAxe4.setText("Axe 4");
        lbAxe4.setMaximumSize(new Dimension(50, 30));
        lbAxe4.setMinimumSize(new Dimension(50, 30));
        lbAxe4.setPreferredSize(new Dimension(50, 30));
        lbAxe4.setName("lbAxe4");
        pnAnalytique.add(lbAxe4, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- WAX4 ----
        WAX4.setPreferredSize(new Dimension(80, 28));
        WAX4.setMinimumSize(new Dimension(80, 28));
        WAX4.setMaximumSize(new Dimension(80, 28));
        WAX4.setName("WAX4");
        pnAnalytique.add(WAX4, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- lbAxe5 ----
        lbAxe5.setText("Axe 5");
        lbAxe5.setMaximumSize(new Dimension(50, 30));
        lbAxe5.setMinimumSize(new Dimension(50, 30));
        lbAxe5.setPreferredSize(new Dimension(50, 30));
        lbAxe5.setName("lbAxe5");
        pnAnalytique.add(lbAxe5, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- WAX5 ----
        WAX5.setPreferredSize(new Dimension(80, 28));
        WAX5.setMinimumSize(new Dimension(80, 28));
        WAX5.setMaximumSize(new Dimension(80, 28));
        WAX5.setName("WAX5");
        pnAnalytique.add(WAX5, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- lbAxe6 ----
        lbAxe6.setText("Axe 6");
        lbAxe6.setMaximumSize(new Dimension(50, 30));
        lbAxe6.setMinimumSize(new Dimension(50, 30));
        lbAxe6.setPreferredSize(new Dimension(50, 30));
        lbAxe6.setName("lbAxe6");
        pnAnalytique.add(lbAxe6, new GridBagConstraints(6, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- WAX6 ----
        WAX6.setPreferredSize(new Dimension(80, 28));
        WAX6.setMinimumSize(new Dimension(80, 28));
        WAX6.setMaximumSize(new Dimension(80, 28));
        WAX6.setName("WAX6");
        pnAnalytique.add(WAX6, new GridBagConstraints(7, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      snPanelPrincipal.add(pnAnalytique, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(snPanelPrincipal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu snPanelPrincipal;
  private SNPanelTitre snModificationOpération;
  private SNLabelChamp lbLibelle;
  private XRiTextField WLIB;
  private SNLabelChamp lbMontant;
  private XRiTextField WMTT;
  private SNPanelTitre pnAnalytique;
  private SNLabelChamp lbNature;
  private XRiTextField WNAT;
  private SNLabelChamp lbAxe1;
  private XRiTextField WAX1;
  private SNLabelChamp lbAxe2;
  private XRiTextField WAX2;
  private SNLabelChamp lbAxe3;
  private XRiTextField WAX3;
  private SNLabelChamp lbAxe4;
  private XRiTextField WAX4;
  private SNLabelChamp lbAxe5;
  private XRiTextField WAX5;
  private SNLabelChamp lbAxe6;
  private XRiTextField WAX6;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
