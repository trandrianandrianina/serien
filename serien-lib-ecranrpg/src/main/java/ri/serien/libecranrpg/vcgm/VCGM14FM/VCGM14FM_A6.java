
package ri.serien.libecranrpg.vcgm.VCGM14FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM14FM_A6 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VCGM14FM_A6(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WFDEF.setValeurs("O", WFDEF_GRP);
    WFDEF_NON.setValeurs("N");
    ARR1X.setValeursSelection("1", "");
    ARR2X.setValeursSelection("1", "");
    ARR3X.setValeursSelection("1", "");
    ARR4X.setValeursSelection("1", "");
    ARR5X.setValeursSelection("1", "");
    ARR6X.setValeursSelection("1", "");
    ARR7X.setValeursSelection("1", "");
    ARR8X.setValeursSelection("1", "");
    ARR9X.setValeursSelection("1", "");
    ARR10X.setValeursSelection("1", "");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WDTFRA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDTFRA@")).trim());
    WDIF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDIF@")).trim());
    EPIN01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPIN01@")).trim());
    EPLI01.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPLI01@")).trim());
    EPIN02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPIN02@")).trim());
    EPLI02.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPLI02@")).trim());
    EPIN03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPIN03@")).trim());
    EPLI03.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPLI03@")).trim());
    EPIN04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPIN04@")).trim());
    EPLI04.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPLI04@")).trim());
    EPIN05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPIN05@")).trim());
    EPLI05.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPLI05@")).trim());
    EPIN06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPIN06@")).trim());
    EPLI06.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPLI06@")).trim());
    EPIN07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPIN07@")).trim());
    EPLI07.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPLI07@")).trim());
    EPIN08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPIN08@")).trim());
    EPLI08.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPLI08@")).trim());
    EPIN09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPIN09@")).trim());
    EPLI09.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPLI09@")).trim());
    EPIN10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPIN10@")).trim());
    EPLI10.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@EPLI10@")).trim());
    label6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMTR@")).trim());
    OBJ_86.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAXE1@")).trim());
    OBJ_87.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAXE2@")).trim());
    OBJ_88.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAXE3@")).trim());
    OBJ_89.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAXE4@")).trim());
    OBJ_90.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAXE5@")).trim());
    OBJ_91.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAXE6@")).trim());
    WRGL2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WBQFRS@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    

    
    


    
    
    
    panel6.setVisible(lexique.isTrue("98"));
    WDIF.setVisible(lexique.isTrue("(98) AND (19)"));
    OBJ_130.setVisible(lexique.isTrue("(98) AND (19)"));
    OBJ_80.setVisible(lexique.isTrue("N80"));
    OBJ_81.setVisible(lexique.isTrue("N80"));
    xTitledPanel2.setVisible(lexique.isTrue("N80"));
    
    panel9.setVisible(lexique.isTrue("45"));
    
    OBJ_121.setVisible(!lexique.HostFieldGetData("EPMT10").trim().equalsIgnoreCase(""));
    OBJ_119.setVisible(!lexique.HostFieldGetData("EPMT09").trim().equalsIgnoreCase(""));
    OBJ_118.setVisible(!lexique.HostFieldGetData("EPMT08").trim().equalsIgnoreCase(""));
    OBJ_116.setVisible(!lexique.HostFieldGetData("EPMT07").trim().equalsIgnoreCase(""));
    OBJ_115.setVisible(!lexique.HostFieldGetData("EPMT06").trim().equalsIgnoreCase(""));
    OBJ_114.setVisible(!lexique.HostFieldGetData("EPMT05").trim().equalsIgnoreCase(""));
    OBJ_112.setVisible(!lexique.HostFieldGetData("EPMT04").trim().equalsIgnoreCase(""));
    OBJ_111.setVisible(!lexique.HostFieldGetData("EPMT03").trim().equalsIgnoreCase(""));
    OBJ_110.setVisible(!lexique.HostFieldGetData("EPMT02").trim().equalsIgnoreCase(""));
    OBJ_107.setVisible(!lexique.HostFieldGetData("EPMT01").trim().equalsIgnoreCase(""));
    
    OBJ_123.setVisible(lexique.isPresent("WTDB"));
    OBJ_128.setVisible(lexique.isPresent("WTCR"));
    OBJ_55.setVisible(lexique.isPresent("USOLDE"));
    OBJ_130.setVisible(lexique.isPresent("DIFF"));
    
    if (lexique.isTrue("25")) {
      EPIN01.setForeground(Constantes.COULEUR_ERREURS);
    }
    else {
      EPIN01.setForeground(Color.BLACK);
    }
    if (lexique.isTrue("26")) {
      EPIN02.setForeground(Constantes.COULEUR_ERREURS);
    }
    else {
      EPIN02.setForeground(Color.BLACK);
    }
    if (lexique.isTrue("27")) {
      EPIN03.setForeground(Constantes.COULEUR_ERREURS);
    }
    else {
      EPIN03.setForeground(Color.BLACK);
    }
    if (lexique.isTrue("28")) {
      EPIN04.setForeground(Constantes.COULEUR_ERREURS);
    }
    else {
      EPIN04.setForeground(Color.BLACK);
    }
    if (lexique.isTrue("29")) {
      EPIN05.setForeground(Constantes.COULEUR_ERREURS);
    }
    else {
      EPIN05.setForeground(Color.BLACK);
    }
    if (lexique.isTrue("30")) {
      EPIN06.setForeground(Constantes.COULEUR_ERREURS);
    }
    else {
      EPIN06.setForeground(Color.BLACK);
    }
    if (lexique.isTrue("31")) {
      EPIN07.setForeground(Constantes.COULEUR_ERREURS);
    }
    else {
      EPIN07.setForeground(Color.BLACK);
    }
    if (lexique.isTrue("32")) {
      EPIN08.setForeground(Constantes.COULEUR_ERREURS);
    }
    else {
      EPIN08.setForeground(Color.BLACK);
    }
    if (lexique.isTrue("33")) {
      EPIN09.setForeground(Constantes.COULEUR_ERREURS);
    }
    else {
      EPIN09.setForeground(Color.BLACK);
    }
    if (lexique.isTrue("34")) {
      EPIN10.setForeground(Constantes.COULEUR_ERREURS);
    }
    else {
      EPIN10.setForeground(Color.BLACK);
    }
    
    WFDEF_NON.setVisible(WFDEF.isVisible());
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @WLIBPC@"));
    
    

    
    p_bpresentation.setCodeEtablissement(WISOC.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WISOC.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23", false);
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_107ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(11, 1);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_110ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(12, 1);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_111ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(13, 1);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_112ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(14, 1);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_114ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(15, 1);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_115ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(16, 1);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_116ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(17, 1);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_118ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(18, 1);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_119ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(19, 1);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_121ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(20, 1);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_39 = new JLabel();
    WISOC = new XRiTextField();
    OBJ_41 = new JLabel();
    WCEP = new XRiTextField();
    OBJ_43 = new JLabel();
    WINCGX = new XRiTextField();
    WINCA = new XRiTextField();
    OBJ_46 = new JLabel();
    WIDTEX = new XRiTextField();
    WDTFRA = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel6 = new JPanel();
    WTDB = new XRiTextField();
    OBJ_123 = new JLabel();
    WTCR = new XRiTextField();
    OBJ_128 = new JLabel();
    WDIF = new RiZoneSortie();
    OBJ_130 = new JLabel();
    panel5 = new JPanel();
    EPIN01 = new RiZoneSortie();
    NCG1X = new XRiTextField();
    EPNA01 = new XRiTextField();
    EPSN01 = new XRiTextField();
    EPLI01 = new XRiTextField();
    SAN1X = new XRiTextField();
    EPIN02 = new RiZoneSortie();
    NCG2X = new XRiTextField();
    EPNA02 = new XRiTextField();
    EPSN02 = new XRiTextField();
    EPLI02 = new XRiTextField();
    SAN2X = new XRiTextField();
    EPIN03 = new RiZoneSortie();
    NCG3X = new XRiTextField();
    EPNA03 = new XRiTextField();
    EPSN03 = new XRiTextField();
    EPLI03 = new XRiTextField();
    SAN3X = new XRiTextField();
    EPIN04 = new RiZoneSortie();
    NCG4X = new XRiTextField();
    EPNA04 = new XRiTextField();
    EPSN04 = new XRiTextField();
    EPLI04 = new XRiTextField();
    SAN4X = new XRiTextField();
    EPIN05 = new RiZoneSortie();
    NCG5X = new XRiTextField();
    EPNA05 = new XRiTextField();
    EPSN05 = new XRiTextField();
    EPLI05 = new XRiTextField();
    SAN5X = new XRiTextField();
    EPIN06 = new RiZoneSortie();
    NCG6X = new XRiTextField();
    EPNA06 = new XRiTextField();
    EPSN06 = new XRiTextField();
    EPLI06 = new XRiTextField();
    SAN6X = new XRiTextField();
    EPIN07 = new RiZoneSortie();
    NCG7X = new XRiTextField();
    EPNA07 = new XRiTextField();
    EPSN07 = new XRiTextField();
    EPLI07 = new XRiTextField();
    SAN7X = new XRiTextField();
    EPIN08 = new RiZoneSortie();
    NCG8X = new XRiTextField();
    EPNA08 = new XRiTextField();
    EPSN08 = new XRiTextField();
    EPLI08 = new XRiTextField();
    SAN8X = new XRiTextField();
    EPIN09 = new RiZoneSortie();
    NCG9X = new XRiTextField();
    EPNA09 = new XRiTextField();
    EPSN09 = new XRiTextField();
    EPLI09 = new XRiTextField();
    SAN9X = new XRiTextField();
    EPIN10 = new RiZoneSortie();
    NCG10X = new XRiTextField();
    EPNA10 = new XRiTextField();
    EPSN10 = new XRiTextField();
    EPLI10 = new XRiTextField();
    SAN10X = new XRiTextField();
    AFF1X = new XRiTextField();
    EPCP01 = new XRiTextField();
    QTE1X = new XRiTextField();
    AFF2X = new XRiTextField();
    AFF3X = new XRiTextField();
    AFF4X = new XRiTextField();
    AFF5X = new XRiTextField();
    AFF6X = new XRiTextField();
    AFF7X = new XRiTextField();
    AFF8X = new XRiTextField();
    AFF9X = new XRiTextField();
    AFF10X = new XRiTextField();
    EPCP02 = new XRiTextField();
    EPCP03 = new XRiTextField();
    EPCP04 = new XRiTextField();
    EPCP05 = new XRiTextField();
    EPCP06 = new XRiTextField();
    EPCP07 = new XRiTextField();
    EPCP08 = new XRiTextField();
    EPCP09 = new XRiTextField();
    EPCP10 = new XRiTextField();
    QTE2X = new XRiTextField();
    QTE3X = new XRiTextField();
    QTE4X = new XRiTextField();
    QTE5x = new XRiTextField();
    QTE6X = new XRiTextField();
    QTE7X = new XRiTextField();
    QTE8X = new XRiTextField();
    QTE9X = new XRiTextField();
    QTE10X = new XRiTextField();
    NAT1X = new XRiTextField();
    NAT2X = new XRiTextField();
    NAT3X = new XRiTextField();
    NAT4X = new XRiTextField();
    NAT5X = new XRiTextField();
    NAT6X = new XRiTextField();
    NAT7X = new XRiTextField();
    NAT8X = new XRiTextField();
    NAT9X = new XRiTextField();
    NAT10X = new XRiTextField();
    panel7 = new JPanel();
    OBJ_107 = new SNBoutonDetail();
    OBJ_110 = new SNBoutonDetail();
    OBJ_111 = new SNBoutonDetail();
    OBJ_112 = new SNBoutonDetail();
    OBJ_114 = new SNBoutonDetail();
    OBJ_115 = new SNBoutonDetail();
    OBJ_116 = new SNBoutonDetail();
    OBJ_118 = new SNBoutonDetail();
    OBJ_119 = new SNBoutonDetail();
    OBJ_121 = new SNBoutonDetail();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();
    label11 = new JLabel();
    label12 = new JLabel();
    label13 = new JLabel();
    label14 = new JLabel();
    label15 = new JLabel();
    label5 = new JLabel();
    ARR1X = new XRiCheckBox();
    ARR2X = new XRiCheckBox();
    ARR3X = new XRiCheckBox();
    ARR4X = new XRiCheckBox();
    ARR5X = new XRiCheckBox();
    ARR6X = new XRiCheckBox();
    ARR7X = new XRiCheckBox();
    ARR8X = new XRiCheckBox();
    ARR9X = new XRiCheckBox();
    ARR10X = new XRiCheckBox();
    TVA1 = new XRiTextField();
    TVA2 = new XRiTextField();
    TVA3 = new XRiTextField();
    TVA4 = new XRiTextField();
    TVA5 = new XRiTextField();
    TVA6 = new XRiTextField();
    TVA7 = new XRiTextField();
    TVA8 = new XRiTextField();
    TVA9 = new XRiTextField();
    TVA10 = new XRiTextField();
    label16 = new JLabel();
    EPMT01 = new XRiTextField();
    EPMT02 = new XRiTextField();
    EPMT03 = new XRiTextField();
    EPMT04 = new XRiTextField();
    EPMT05 = new XRiTextField();
    EPMT06 = new XRiTextField();
    EPMT07 = new XRiTextField();
    EPMT08 = new XRiTextField();
    EPMT09 = new XRiTextField();
    EPMT10 = new XRiTextField();
    panel4 = new JPanel();
    EPLIT = new XRiTextField();
    WOPCE = new XRiTextField();
    OBJ_96 = new JLabel();
    OBJ_67 = new JLabel();
    OBJ_58 = new JLabel();
    EPRFC = new XRiTextField();
    WREFP = new XRiTextField();
    OBJ_59 = new JLabel();
    WECH01 = new XRiTextField();
    EPACT = new XRiTextField();
    EPSAN = new XRiTextField();
    EPNAT = new XRiTextField();
    WRGL01 = new XRiTextField();
    OBJ_81 = new JLabel();
    OBJ_82 = new JLabel();
    OBJ_84 = new JLabel();
    OBJ_80 = new JLabel();
    OBJ_83 = new JLabel();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_61 = new JLabel();
    WMTTP = new XRiTextField();
    WDEV = new XRiTextField();
    WCHGX = new XRiTextField();
    WMTD = new XRiTextField();
    OBJ_86 = new JLabel();
    OBJ_87 = new JLabel();
    OBJ_88 = new JLabel();
    OBJ_89 = new JLabel();
    OBJ_90 = new JLabel();
    OBJ_91 = new JLabel();
    EPAA1 = new XRiTextField();
    EPAA2 = new XRiTextField();
    EPAA3 = new XRiTextField();
    EPAA4 = new XRiTextField();
    EPAA5 = new XRiTextField();
    EPAA6 = new XRiTextField();
    panel9 = new JPanel();
    OBJ_60 = new JLabel();
    WFDEF = new XRiRadioButton();
    WFDEF_NON = new XRiRadioButton();
    panel3 = new JPanel();
    panel1 = new JPanel();
    label1 = new JLabel();
    WIPCE = new XRiTextField();
    OBJ_55 = new JLabel();
    USOLDE = new XRiTextField();
    W1SNS = new XRiTextField();
    WTNOM = new XRiTextField();
    A1CPL = new XRiTextField();
    A1RUE = new XRiTextField();
    A1LOC = new XRiTextField();
    A1CDP = new XRiTextField();
    A1VIL = new XRiTextField();
    WRGL2 = new RiZoneSortie();
    OBJ_85 = new JLabel();
    EPJO = new XRiTextField();
    OBJ_92 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    WFDEF_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Saisie par pi\u00e8ces");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_39 ----
          OBJ_39.setText("Soci\u00e9t\u00e9");
          OBJ_39.setName("OBJ_39");

          //---- WISOC ----
          WISOC.setName("WISOC");

          //---- OBJ_41 ----
          OBJ_41.setText("Type");
          OBJ_41.setName("OBJ_41");

          //---- WCEP ----
          WCEP.setName("WCEP");

          //---- OBJ_43 ----
          OBJ_43.setText("Compte");
          OBJ_43.setName("OBJ_43");

          //---- WINCGX ----
          WINCGX.setName("WINCGX");

          //---- WINCA ----
          WINCA.setName("WINCA");

          //---- OBJ_46 ----
          OBJ_46.setText("Date");
          OBJ_46.setName("OBJ_46");

          //---- WIDTEX ----
          WIDTEX.setName("WIDTEX");

          //---- WDTFRA ----
          WDTFRA.setText("@WDTFRA@");
          WDTFRA.setOpaque(false);
          WDTFRA.setName("WDTFRA");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(WISOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(WCEP, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(WINCGX, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(WINCA, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(WIDTEX, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(WDTFRA, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_39))
              .addComponent(WISOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_41))
              .addComponent(WCEP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_43))
              .addComponent(WINCGX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WINCA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_46))
              .addComponent(WIDTEX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(WDTFRA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Modification autres zones");
              riSousMenu_bt6.setToolTipText("Modification zones autres que montants");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Saisie en devises");
              riSousMenu_bt8.setToolTipText("Saisie de pi\u00e8ce enti\u00e8rement en devises");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("G\u00e9n\u00e9ration automatique");
              riSousMenu_bt9.setToolTipText("Saisie de pi\u00e8ce enti\u00e8rement en devises");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");

              //---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Bloc notes");
              riSousMenu_bt15.setToolTipText("Bloc notes");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed(e);
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1020, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(1001, 600));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //======== panel6 ========
          {
            panel6.setBorder(new TitledBorder(null, "Totaux", TitledBorder.LEADING, TitledBorder.ABOVE_TOP));
            panel6.setOpaque(false);
            panel6.setName("panel6");
            panel6.setLayout(null);

            //---- WTDB ----
            WTDB.setName("WTDB");
            panel6.add(WTDB);
            WTDB.setBounds(150, 25, 108, WTDB.getPreferredSize().height);

            //---- OBJ_123 ----
            OBJ_123.setText("D\u00e9bit");
            OBJ_123.setName("OBJ_123");
            panel6.add(OBJ_123);
            OBJ_123.setBounds(268, 25, 36, 28);

            //---- WTCR ----
            WTCR.setName("WTCR");
            panel6.add(WTCR);
            WTCR.setBounds(368, 25, 108, WTCR.getPreferredSize().height);

            //---- OBJ_128 ----
            OBJ_128.setText("Cr\u00e9dit");
            OBJ_128.setName("OBJ_128");
            panel6.add(OBJ_128);
            OBJ_128.setBounds(478, 25, 39, 28);

            //---- WDIF ----
            WDIF.setForeground(Color.red);
            WDIF.setText("@WDIF@");
            WDIF.setName("WDIF");
            panel6.add(WDIF);
            WDIF.setBounds(593, 27, 116, WDIF.getPreferredSize().height);

            //---- OBJ_130 ----
            OBJ_130.setText("Diff\u00e9rence totaux");
            OBJ_130.setForeground(Color.red);
            OBJ_130.setName("OBJ_130");
            panel6.add(OBJ_130);
            OBJ_130.setBounds(713, 25, 110, 28);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel6.getComponentCount(); i++) {
                Rectangle bounds = panel6.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel6.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel6.setMinimumSize(preferredSize);
              panel6.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel6);
          panel6.setBounds(5, 530, 1010, 65);

          //======== panel5 ========
          {
            panel5.setOpaque(false);
            panel5.setBorder(new DropShadowBorder());
            panel5.setName("panel5");
            panel5.setLayout(null);

            //---- EPIN01 ----
            EPIN01.setText("@EPIN01@");
            EPIN01.setBorder(new BevelBorder(BevelBorder.LOWERED));
            EPIN01.setName("EPIN01");
            panel5.add(EPIN01);
            EPIN01.setBounds(25, 30, 225, 23);

            //---- NCG1X ----
            NCG1X.setName("NCG1X");
            panel5.add(NCG1X);
            NCG1X.setBounds(343, 27, 60, NCG1X.getPreferredSize().height);

            //---- EPNA01 ----
            EPNA01.setName("EPNA01");
            panel5.add(EPNA01);
            EPNA01.setBounds(401, 27, 60, EPNA01.getPreferredSize().height);

            //---- EPSN01 ----
            EPSN01.setName("EPSN01");
            panel5.add(EPSN01);
            EPSN01.setBounds(460, 27, 25, EPSN01.getPreferredSize().height);

            //---- EPLI01 ----
            EPLI01.setToolTipText("@EPLI01@");
            EPLI01.setFont(EPLI01.getFont().deriveFont(EPLI01.getFont().getSize() - 2f));
            EPLI01.setName("EPLI01");
            panel5.add(EPLI01);
            EPLI01.setBounds(484, 28, 210, EPLI01.getPreferredSize().height);

            //---- SAN1X ----
            SAN1X.setComponentPopupMenu(BTD);
            SAN1X.setName("SAN1X");
            panel5.add(SAN1X);
            SAN1X.setBounds(752, 27, 50, SAN1X.getPreferredSize().height);

            //---- EPIN02 ----
            EPIN02.setText("@EPIN02@");
            EPIN02.setBorder(new BevelBorder(BevelBorder.LOWERED));
            EPIN02.setName("EPIN02");
            panel5.add(EPIN02);
            EPIN02.setBounds(25, 55, 225, 23);

            //---- NCG2X ----
            NCG2X.setName("NCG2X");
            panel5.add(NCG2X);
            NCG2X.setBounds(343, 52, 60, NCG2X.getPreferredSize().height);

            //---- EPNA02 ----
            EPNA02.setName("EPNA02");
            panel5.add(EPNA02);
            EPNA02.setBounds(401, 52, 60, EPNA02.getPreferredSize().height);

            //---- EPSN02 ----
            EPSN02.setName("EPSN02");
            panel5.add(EPSN02);
            EPSN02.setBounds(460, 52, 25, EPSN02.getPreferredSize().height);

            //---- EPLI02 ----
            EPLI02.setToolTipText("@EPLI02@");
            EPLI02.setFont(EPLI02.getFont().deriveFont(EPLI02.getFont().getSize() - 2f));
            EPLI02.setName("EPLI02");
            panel5.add(EPLI02);
            EPLI02.setBounds(484, 53, 210, EPLI02.getPreferredSize().height);

            //---- SAN2X ----
            SAN2X.setComponentPopupMenu(BTD);
            SAN2X.setName("SAN2X");
            panel5.add(SAN2X);
            SAN2X.setBounds(752, 52, 50, SAN2X.getPreferredSize().height);

            //---- EPIN03 ----
            EPIN03.setText("@EPIN03@");
            EPIN03.setBorder(new BevelBorder(BevelBorder.LOWERED));
            EPIN03.setName("EPIN03");
            panel5.add(EPIN03);
            EPIN03.setBounds(25, 80, 225, 23);

            //---- NCG3X ----
            NCG3X.setName("NCG3X");
            panel5.add(NCG3X);
            NCG3X.setBounds(343, 77, 60, NCG3X.getPreferredSize().height);

            //---- EPNA03 ----
            EPNA03.setName("EPNA03");
            panel5.add(EPNA03);
            EPNA03.setBounds(401, 77, 60, EPNA03.getPreferredSize().height);

            //---- EPSN03 ----
            EPSN03.setName("EPSN03");
            panel5.add(EPSN03);
            EPSN03.setBounds(460, 77, 25, EPSN03.getPreferredSize().height);

            //---- EPLI03 ----
            EPLI03.setToolTipText("@EPLI03@");
            EPLI03.setFont(EPLI03.getFont().deriveFont(EPLI03.getFont().getSize() - 2f));
            EPLI03.setName("EPLI03");
            panel5.add(EPLI03);
            EPLI03.setBounds(484, 78, 210, EPLI03.getPreferredSize().height);

            //---- SAN3X ----
            SAN3X.setComponentPopupMenu(BTD);
            SAN3X.setName("SAN3X");
            panel5.add(SAN3X);
            SAN3X.setBounds(752, 77, 50, SAN3X.getPreferredSize().height);

            //---- EPIN04 ----
            EPIN04.setText("@EPIN04@");
            EPIN04.setBorder(new BevelBorder(BevelBorder.LOWERED));
            EPIN04.setName("EPIN04");
            panel5.add(EPIN04);
            EPIN04.setBounds(25, 105, 225, 23);

            //---- NCG4X ----
            NCG4X.setName("NCG4X");
            panel5.add(NCG4X);
            NCG4X.setBounds(343, 102, 60, NCG4X.getPreferredSize().height);

            //---- EPNA04 ----
            EPNA04.setName("EPNA04");
            panel5.add(EPNA04);
            EPNA04.setBounds(401, 102, 60, EPNA04.getPreferredSize().height);

            //---- EPSN04 ----
            EPSN04.setName("EPSN04");
            panel5.add(EPSN04);
            EPSN04.setBounds(460, 102, 25, EPSN04.getPreferredSize().height);

            //---- EPLI04 ----
            EPLI04.setToolTipText("@EPLI04@");
            EPLI04.setFont(EPLI04.getFont().deriveFont(EPLI04.getFont().getSize() - 2f));
            EPLI04.setName("EPLI04");
            panel5.add(EPLI04);
            EPLI04.setBounds(484, 103, 210, EPLI04.getPreferredSize().height);

            //---- SAN4X ----
            SAN4X.setComponentPopupMenu(BTD);
            SAN4X.setName("SAN4X");
            panel5.add(SAN4X);
            SAN4X.setBounds(752, 102, 50, SAN4X.getPreferredSize().height);

            //---- EPIN05 ----
            EPIN05.setText("@EPIN05@");
            EPIN05.setBorder(new BevelBorder(BevelBorder.LOWERED));
            EPIN05.setName("EPIN05");
            panel5.add(EPIN05);
            EPIN05.setBounds(25, 130, 225, 23);

            //---- NCG5X ----
            NCG5X.setName("NCG5X");
            panel5.add(NCG5X);
            NCG5X.setBounds(343, 127, 60, NCG5X.getPreferredSize().height);

            //---- EPNA05 ----
            EPNA05.setName("EPNA05");
            panel5.add(EPNA05);
            EPNA05.setBounds(401, 127, 60, EPNA05.getPreferredSize().height);

            //---- EPSN05 ----
            EPSN05.setName("EPSN05");
            panel5.add(EPSN05);
            EPSN05.setBounds(460, 127, 25, EPSN05.getPreferredSize().height);

            //---- EPLI05 ----
            EPLI05.setToolTipText("@EPLI05@");
            EPLI05.setFont(EPLI05.getFont().deriveFont(EPLI05.getFont().getSize() - 2f));
            EPLI05.setName("EPLI05");
            panel5.add(EPLI05);
            EPLI05.setBounds(484, 128, 210, EPLI05.getPreferredSize().height);

            //---- SAN5X ----
            SAN5X.setComponentPopupMenu(BTD);
            SAN5X.setName("SAN5X");
            panel5.add(SAN5X);
            SAN5X.setBounds(752, 127, 50, SAN5X.getPreferredSize().height);

            //---- EPIN06 ----
            EPIN06.setText("@EPIN06@");
            EPIN06.setBorder(new BevelBorder(BevelBorder.LOWERED));
            EPIN06.setName("EPIN06");
            panel5.add(EPIN06);
            EPIN06.setBounds(25, 155, 225, 23);

            //---- NCG6X ----
            NCG6X.setName("NCG6X");
            panel5.add(NCG6X);
            NCG6X.setBounds(343, 152, 60, NCG6X.getPreferredSize().height);

            //---- EPNA06 ----
            EPNA06.setName("EPNA06");
            panel5.add(EPNA06);
            EPNA06.setBounds(401, 152, 60, EPNA06.getPreferredSize().height);

            //---- EPSN06 ----
            EPSN06.setName("EPSN06");
            panel5.add(EPSN06);
            EPSN06.setBounds(460, 152, 25, EPSN06.getPreferredSize().height);

            //---- EPLI06 ----
            EPLI06.setToolTipText("@EPLI06@");
            EPLI06.setFont(EPLI06.getFont().deriveFont(EPLI06.getFont().getSize() - 2f));
            EPLI06.setName("EPLI06");
            panel5.add(EPLI06);
            EPLI06.setBounds(484, 153, 210, EPLI06.getPreferredSize().height);

            //---- SAN6X ----
            SAN6X.setComponentPopupMenu(BTD);
            SAN6X.setName("SAN6X");
            panel5.add(SAN6X);
            SAN6X.setBounds(752, 152, 50, SAN6X.getPreferredSize().height);

            //---- EPIN07 ----
            EPIN07.setText("@EPIN07@");
            EPIN07.setBorder(new BevelBorder(BevelBorder.LOWERED));
            EPIN07.setName("EPIN07");
            panel5.add(EPIN07);
            EPIN07.setBounds(25, 180, 225, 23);

            //---- NCG7X ----
            NCG7X.setName("NCG7X");
            panel5.add(NCG7X);
            NCG7X.setBounds(343, 177, 60, NCG7X.getPreferredSize().height);

            //---- EPNA07 ----
            EPNA07.setName("EPNA07");
            panel5.add(EPNA07);
            EPNA07.setBounds(401, 177, 60, EPNA07.getPreferredSize().height);

            //---- EPSN07 ----
            EPSN07.setName("EPSN07");
            panel5.add(EPSN07);
            EPSN07.setBounds(460, 177, 25, EPSN07.getPreferredSize().height);

            //---- EPLI07 ----
            EPLI07.setToolTipText("@EPLI07@");
            EPLI07.setFont(EPLI07.getFont().deriveFont(EPLI07.getFont().getSize() - 2f));
            EPLI07.setName("EPLI07");
            panel5.add(EPLI07);
            EPLI07.setBounds(484, 178, 210, EPLI07.getPreferredSize().height);

            //---- SAN7X ----
            SAN7X.setComponentPopupMenu(BTD);
            SAN7X.setName("SAN7X");
            panel5.add(SAN7X);
            SAN7X.setBounds(752, 177, 50, SAN7X.getPreferredSize().height);

            //---- EPIN08 ----
            EPIN08.setText("@EPIN08@");
            EPIN08.setBorder(new BevelBorder(BevelBorder.LOWERED));
            EPIN08.setName("EPIN08");
            panel5.add(EPIN08);
            EPIN08.setBounds(25, 205, 225, 23);

            //---- NCG8X ----
            NCG8X.setName("NCG8X");
            panel5.add(NCG8X);
            NCG8X.setBounds(343, 202, 60, NCG8X.getPreferredSize().height);

            //---- EPNA08 ----
            EPNA08.setName("EPNA08");
            panel5.add(EPNA08);
            EPNA08.setBounds(401, 202, 60, EPNA08.getPreferredSize().height);

            //---- EPSN08 ----
            EPSN08.setName("EPSN08");
            panel5.add(EPSN08);
            EPSN08.setBounds(460, 202, 25, EPSN08.getPreferredSize().height);

            //---- EPLI08 ----
            EPLI08.setToolTipText("@EPLI08@");
            EPLI08.setFont(EPLI08.getFont().deriveFont(EPLI08.getFont().getSize() - 2f));
            EPLI08.setName("EPLI08");
            panel5.add(EPLI08);
            EPLI08.setBounds(484, 203, 210, EPLI08.getPreferredSize().height);

            //---- SAN8X ----
            SAN8X.setComponentPopupMenu(BTD);
            SAN8X.setName("SAN8X");
            panel5.add(SAN8X);
            SAN8X.setBounds(752, 202, 50, SAN8X.getPreferredSize().height);

            //---- EPIN09 ----
            EPIN09.setText("@EPIN09@");
            EPIN09.setBorder(new BevelBorder(BevelBorder.LOWERED));
            EPIN09.setName("EPIN09");
            panel5.add(EPIN09);
            EPIN09.setBounds(25, 230, 225, 23);

            //---- NCG9X ----
            NCG9X.setName("NCG9X");
            panel5.add(NCG9X);
            NCG9X.setBounds(343, 227, 60, NCG9X.getPreferredSize().height);

            //---- EPNA09 ----
            EPNA09.setName("EPNA09");
            panel5.add(EPNA09);
            EPNA09.setBounds(401, 227, 60, EPNA09.getPreferredSize().height);

            //---- EPSN09 ----
            EPSN09.setName("EPSN09");
            panel5.add(EPSN09);
            EPSN09.setBounds(460, 227, 25, EPSN09.getPreferredSize().height);

            //---- EPLI09 ----
            EPLI09.setToolTipText("@EPLI09@");
            EPLI09.setFont(EPLI09.getFont().deriveFont(EPLI09.getFont().getSize() - 2f));
            EPLI09.setName("EPLI09");
            panel5.add(EPLI09);
            EPLI09.setBounds(484, 228, 210, EPLI09.getPreferredSize().height);

            //---- SAN9X ----
            SAN9X.setComponentPopupMenu(BTD);
            SAN9X.setName("SAN9X");
            panel5.add(SAN9X);
            SAN9X.setBounds(752, 227, 50, SAN9X.getPreferredSize().height);

            //---- EPIN10 ----
            EPIN10.setText("@EPIN10@");
            EPIN10.setBorder(new BevelBorder(BevelBorder.LOWERED));
            EPIN10.setName("EPIN10");
            panel5.add(EPIN10);
            EPIN10.setBounds(25, 255, 225, 23);

            //---- NCG10X ----
            NCG10X.setName("NCG10X");
            panel5.add(NCG10X);
            NCG10X.setBounds(343, 252, 60, NCG10X.getPreferredSize().height);

            //---- EPNA10 ----
            EPNA10.setName("EPNA10");
            panel5.add(EPNA10);
            EPNA10.setBounds(401, 252, 60, EPNA10.getPreferredSize().height);

            //---- EPSN10 ----
            EPSN10.setName("EPSN10");
            panel5.add(EPSN10);
            EPSN10.setBounds(460, 252, 25, EPSN10.getPreferredSize().height);

            //---- EPLI10 ----
            EPLI10.setToolTipText("@EPLI10@");
            EPLI10.setFont(EPLI10.getFont().deriveFont(EPLI10.getFont().getSize() - 2f));
            EPLI10.setName("EPLI10");
            panel5.add(EPLI10);
            EPLI10.setBounds(484, 253, 210, EPLI10.getPreferredSize().height);

            //---- SAN10X ----
            SAN10X.setComponentPopupMenu(BTD);
            SAN10X.setName("SAN10X");
            panel5.add(SAN10X);
            SAN10X.setBounds(752, 252, 50, SAN10X.getPreferredSize().height);

            //---- AFF1X ----
            AFF1X.setComponentPopupMenu(BTD);
            AFF1X.setName("AFF1X");
            panel5.add(AFF1X);
            AFF1X.setBounds(800, 27, 60, AFF1X.getPreferredSize().height);

            //---- EPCP01 ----
            EPCP01.setName("EPCP01");
            panel5.add(EPCP01);
            EPCP01.setBounds(693, 27, 60, EPCP01.getPreferredSize().height);

            //---- QTE1X ----
            QTE1X.setName("QTE1X");
            panel5.add(QTE1X);
            QTE1X.setBounds(918, 27, 44, QTE1X.getPreferredSize().height);

            //---- AFF2X ----
            AFF2X.setComponentPopupMenu(BTD);
            AFF2X.setName("AFF2X");
            panel5.add(AFF2X);
            AFF2X.setBounds(800, 52, 60, AFF2X.getPreferredSize().height);

            //---- AFF3X ----
            AFF3X.setComponentPopupMenu(BTD);
            AFF3X.setName("AFF3X");
            panel5.add(AFF3X);
            AFF3X.setBounds(800, 77, 60, AFF3X.getPreferredSize().height);

            //---- AFF4X ----
            AFF4X.setComponentPopupMenu(BTD);
            AFF4X.setName("AFF4X");
            panel5.add(AFF4X);
            AFF4X.setBounds(800, 102, 60, AFF4X.getPreferredSize().height);

            //---- AFF5X ----
            AFF5X.setComponentPopupMenu(BTD);
            AFF5X.setName("AFF5X");
            panel5.add(AFF5X);
            AFF5X.setBounds(800, 127, 60, AFF5X.getPreferredSize().height);

            //---- AFF6X ----
            AFF6X.setComponentPopupMenu(BTD);
            AFF6X.setName("AFF6X");
            panel5.add(AFF6X);
            AFF6X.setBounds(800, 152, 60, AFF6X.getPreferredSize().height);

            //---- AFF7X ----
            AFF7X.setComponentPopupMenu(BTD);
            AFF7X.setName("AFF7X");
            panel5.add(AFF7X);
            AFF7X.setBounds(800, 177, 60, AFF7X.getPreferredSize().height);

            //---- AFF8X ----
            AFF8X.setComponentPopupMenu(BTD);
            AFF8X.setName("AFF8X");
            panel5.add(AFF8X);
            AFF8X.setBounds(800, 202, 60, AFF8X.getPreferredSize().height);

            //---- AFF9X ----
            AFF9X.setComponentPopupMenu(BTD);
            AFF9X.setName("AFF9X");
            panel5.add(AFF9X);
            AFF9X.setBounds(800, 227, 60, AFF9X.getPreferredSize().height);

            //---- AFF10X ----
            AFF10X.setComponentPopupMenu(BTD);
            AFF10X.setName("AFF10X");
            panel5.add(AFF10X);
            AFF10X.setBounds(800, 252, 60, AFF10X.getPreferredSize().height);

            //---- EPCP02 ----
            EPCP02.setName("EPCP02");
            panel5.add(EPCP02);
            EPCP02.setBounds(693, 52, 60, EPCP02.getPreferredSize().height);

            //---- EPCP03 ----
            EPCP03.setName("EPCP03");
            panel5.add(EPCP03);
            EPCP03.setBounds(693, 77, 60, EPCP03.getPreferredSize().height);

            //---- EPCP04 ----
            EPCP04.setName("EPCP04");
            panel5.add(EPCP04);
            EPCP04.setBounds(693, 102, 60, EPCP04.getPreferredSize().height);

            //---- EPCP05 ----
            EPCP05.setName("EPCP05");
            panel5.add(EPCP05);
            EPCP05.setBounds(693, 127, 60, EPCP05.getPreferredSize().height);

            //---- EPCP06 ----
            EPCP06.setName("EPCP06");
            panel5.add(EPCP06);
            EPCP06.setBounds(693, 152, 60, EPCP06.getPreferredSize().height);

            //---- EPCP07 ----
            EPCP07.setName("EPCP07");
            panel5.add(EPCP07);
            EPCP07.setBounds(693, 177, 60, EPCP07.getPreferredSize().height);

            //---- EPCP08 ----
            EPCP08.setName("EPCP08");
            panel5.add(EPCP08);
            EPCP08.setBounds(693, 202, 60, EPCP08.getPreferredSize().height);

            //---- EPCP09 ----
            EPCP09.setName("EPCP09");
            panel5.add(EPCP09);
            EPCP09.setBounds(693, 227, 60, EPCP09.getPreferredSize().height);

            //---- EPCP10 ----
            EPCP10.setName("EPCP10");
            panel5.add(EPCP10);
            EPCP10.setBounds(693, 252, 60, EPCP10.getPreferredSize().height);

            //---- QTE2X ----
            QTE2X.setName("QTE2X");
            panel5.add(QTE2X);
            QTE2X.setBounds(918, 52, 44, QTE2X.getPreferredSize().height);

            //---- QTE3X ----
            QTE3X.setName("QTE3X");
            panel5.add(QTE3X);
            QTE3X.setBounds(918, 77, 44, QTE3X.getPreferredSize().height);

            //---- QTE4X ----
            QTE4X.setName("QTE4X");
            panel5.add(QTE4X);
            QTE4X.setBounds(918, 102, 44, QTE4X.getPreferredSize().height);

            //---- QTE5x ----
            QTE5x.setName("QTE5x");
            panel5.add(QTE5x);
            QTE5x.setBounds(918, 127, 44, QTE5x.getPreferredSize().height);

            //---- QTE6X ----
            QTE6X.setName("QTE6X");
            panel5.add(QTE6X);
            QTE6X.setBounds(918, 152, 44, QTE6X.getPreferredSize().height);

            //---- QTE7X ----
            QTE7X.setName("QTE7X");
            panel5.add(QTE7X);
            QTE7X.setBounds(918, 177, 44, QTE7X.getPreferredSize().height);

            //---- QTE8X ----
            QTE8X.setName("QTE8X");
            panel5.add(QTE8X);
            QTE8X.setBounds(918, 202, 44, QTE8X.getPreferredSize().height);

            //---- QTE9X ----
            QTE9X.setName("QTE9X");
            panel5.add(QTE9X);
            QTE9X.setBounds(918, 227, 44, QTE9X.getPreferredSize().height);

            //---- QTE10X ----
            QTE10X.setName("QTE10X");
            panel5.add(QTE10X);
            QTE10X.setBounds(918, 252, 44, QTE10X.getPreferredSize().height);

            //---- NAT1X ----
            NAT1X.setComponentPopupMenu(BTD);
            NAT1X.setName("NAT1X");
            panel5.add(NAT1X);
            NAT1X.setBounds(859, 27, 60, NAT1X.getPreferredSize().height);

            //---- NAT2X ----
            NAT2X.setComponentPopupMenu(BTD);
            NAT2X.setName("NAT2X");
            panel5.add(NAT2X);
            NAT2X.setBounds(859, 52, 60, NAT2X.getPreferredSize().height);

            //---- NAT3X ----
            NAT3X.setComponentPopupMenu(BTD);
            NAT3X.setName("NAT3X");
            panel5.add(NAT3X);
            NAT3X.setBounds(859, 77, 60, NAT3X.getPreferredSize().height);

            //---- NAT4X ----
            NAT4X.setComponentPopupMenu(BTD);
            NAT4X.setName("NAT4X");
            panel5.add(NAT4X);
            NAT4X.setBounds(859, 102, 60, NAT4X.getPreferredSize().height);

            //---- NAT5X ----
            NAT5X.setComponentPopupMenu(BTD);
            NAT5X.setName("NAT5X");
            panel5.add(NAT5X);
            NAT5X.setBounds(859, 127, 60, NAT5X.getPreferredSize().height);

            //---- NAT6X ----
            NAT6X.setComponentPopupMenu(BTD);
            NAT6X.setName("NAT6X");
            panel5.add(NAT6X);
            NAT6X.setBounds(859, 152, 60, NAT6X.getPreferredSize().height);

            //---- NAT7X ----
            NAT7X.setComponentPopupMenu(BTD);
            NAT7X.setName("NAT7X");
            panel5.add(NAT7X);
            NAT7X.setBounds(859, 177, 60, NAT7X.getPreferredSize().height);

            //---- NAT8X ----
            NAT8X.setComponentPopupMenu(BTD);
            NAT8X.setName("NAT8X");
            panel5.add(NAT8X);
            NAT8X.setBounds(859, 202, 60, NAT8X.getPreferredSize().height);

            //---- NAT9X ----
            NAT9X.setComponentPopupMenu(BTD);
            NAT9X.setName("NAT9X");
            panel5.add(NAT9X);
            NAT9X.setBounds(859, 227, 60, NAT9X.getPreferredSize().height);

            //---- NAT10X ----
            NAT10X.setComponentPopupMenu(BTD);
            NAT10X.setName("NAT10X");
            panel5.add(NAT10X);
            NAT10X.setBounds(859, 252, 60, NAT10X.getPreferredSize().height);

            //======== panel7 ========
            {
              panel7.setOpaque(false);
              panel7.setName("panel7");
              panel7.setLayout(null);

              //---- OBJ_107 ----
              OBJ_107.setText("");
              OBJ_107.setToolTipText("Axes analytiques");
              OBJ_107.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_107.setName("OBJ_107");
              OBJ_107.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_107ActionPerformed(e);
                }
              });
              panel7.add(OBJ_107);
              OBJ_107.setBounds(0, 10, 23, 23);

              //---- OBJ_110 ----
              OBJ_110.setText("");
              OBJ_110.setToolTipText("Axes analytiques");
              OBJ_110.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_110.setName("OBJ_110");
              OBJ_110.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_110ActionPerformed(e);
                }
              });
              panel7.add(OBJ_110);
              OBJ_110.setBounds(0, 35, 23, 23);

              //---- OBJ_111 ----
              OBJ_111.setText("");
              OBJ_111.setToolTipText("Axes analytiques");
              OBJ_111.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_111.setName("OBJ_111");
              OBJ_111.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_111ActionPerformed(e);
                }
              });
              panel7.add(OBJ_111);
              OBJ_111.setBounds(0, 60, 23, 23);

              //---- OBJ_112 ----
              OBJ_112.setText("");
              OBJ_112.setToolTipText("Axes analytiques");
              OBJ_112.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_112.setName("OBJ_112");
              OBJ_112.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_112ActionPerformed(e);
                }
              });
              panel7.add(OBJ_112);
              OBJ_112.setBounds(0, 85, 23, 23);

              //---- OBJ_114 ----
              OBJ_114.setText("");
              OBJ_114.setToolTipText("Axes analytiques");
              OBJ_114.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_114.setName("OBJ_114");
              OBJ_114.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_114ActionPerformed(e);
                }
              });
              panel7.add(OBJ_114);
              OBJ_114.setBounds(0, 110, 23, 23);

              //---- OBJ_115 ----
              OBJ_115.setText("");
              OBJ_115.setToolTipText("Axes analytiques");
              OBJ_115.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_115.setName("OBJ_115");
              OBJ_115.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_115ActionPerformed(e);
                }
              });
              panel7.add(OBJ_115);
              OBJ_115.setBounds(0, 135, 23, 23);

              //---- OBJ_116 ----
              OBJ_116.setText("");
              OBJ_116.setToolTipText("Axes analytiques");
              OBJ_116.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_116.setName("OBJ_116");
              OBJ_116.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_116ActionPerformed(e);
                }
              });
              panel7.add(OBJ_116);
              OBJ_116.setBounds(0, 160, 23, 23);

              //---- OBJ_118 ----
              OBJ_118.setText("");
              OBJ_118.setToolTipText("Axes analytiques");
              OBJ_118.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_118.setName("OBJ_118");
              OBJ_118.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_118ActionPerformed(e);
                }
              });
              panel7.add(OBJ_118);
              OBJ_118.setBounds(0, 210, 23, 23);

              //---- OBJ_119 ----
              OBJ_119.setText("");
              OBJ_119.setToolTipText("Axes analytiques");
              OBJ_119.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_119.setName("OBJ_119");
              OBJ_119.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_119ActionPerformed(e);
                }
              });
              panel7.add(OBJ_119);
              OBJ_119.setBounds(0, 185, 23, 23);

              //---- OBJ_121 ----
              OBJ_121.setText("");
              OBJ_121.setToolTipText("Axes analytiques");
              OBJ_121.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_121.setName("OBJ_121");
              OBJ_121.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_121ActionPerformed(e);
                }
              });
              panel7.add(OBJ_121);
              OBJ_121.setBounds(0, 235, 23, 23);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel7.getComponentCount(); i++) {
                  Rectangle bounds = panel7.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel7.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel7.setMinimumSize(preferredSize);
                panel7.setPreferredSize(preferredSize);
              }
            }
            panel5.add(panel7);
            panel7.setBounds(0, 20, panel7.getPreferredSize().width, 260);

            //---- label6 ----
            label6.setText("@LIBMTR@");
            label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
            label6.setName("label6");
            panel5.add(label6);
            label6.setBounds(250, 5, 90, 25);

            //---- label7 ----
            label7.setText("Compte");
            label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD));
            label7.setName("label7");
            panel5.add(label7);
            label7.setBounds(343, 5, 55, 25);

            //---- label8 ----
            label8.setText("Tiers");
            label8.setFont(label8.getFont().deriveFont(label8.getFont().getStyle() | Font.BOLD));
            label8.setName("label8");
            panel5.add(label8);
            label8.setBounds(401, 5, 55, 25);

            //---- label9 ----
            label9.setText("S");
            label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD));
            label9.setName("label9");
            panel5.add(label9);
            label9.setBounds(460, 5, 20, 25);

            //---- label10 ----
            label10.setText("Libell\u00e9 de contrepartie");
            label10.setFont(label10.getFont().deriveFont(label10.getFont().getStyle() | Font.BOLD));
            label10.setName("label10");
            panel5.add(label10);
            label10.setBounds(484, 5, 155, 25);

            //---- label11 ----
            label11.setText("N\u00b0 contr.");
            label11.setFont(label11.getFont().deriveFont(label11.getFont().getStyle() | Font.BOLD));
            label11.setName("label11");
            panel5.add(label11);
            label11.setBounds(693, 5, 55, 25);

            //---- label12 ----
            label12.setText("Section");
            label12.setFont(label12.getFont().deriveFont(label12.getFont().getStyle() | Font.BOLD));
            label12.setHorizontalAlignment(SwingConstants.CENTER);
            label12.setName("label12");
            panel5.add(label12);
            label12.setBounds(752, 5, 50, 25);

            //---- label13 ----
            label13.setText("Affaire ");
            label13.setFont(label13.getFont().deriveFont(label13.getFont().getStyle() | Font.BOLD));
            label13.setHorizontalAlignment(SwingConstants.CENTER);
            label13.setName("label13");
            panel5.add(label13);
            label13.setBounds(800, 5, 60, 25);

            //---- label14 ----
            label14.setText("Nature");
            label14.setFont(label14.getFont().deriveFont(label14.getFont().getStyle() | Font.BOLD));
            label14.setHorizontalAlignment(SwingConstants.CENTER);
            label14.setName("label14");
            panel5.add(label14);
            label14.setBounds(859, 5, 60, 25);

            //---- label15 ----
            label15.setText("Qt\u00e9.");
            label15.setFont(label15.getFont().deriveFont(label15.getFont().getStyle() | Font.BOLD));
            label15.setName("label15");
            panel5.add(label15);
            label15.setBounds(918, 5, 44, 25);

            //---- label5 ----
            label5.setText("Intitul\u00e9");
            label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
            label5.setName("label5");
            panel5.add(label5);
            label5.setBounds(25, 5, 125, 25);

            //---- ARR1X ----
            ARR1X.setToolTipText("Cochez cette case pour ne pas recalculer le montant de la TVA");
            ARR1X.setName("ARR1X");
            panel5.add(ARR1X);
            ARR1X.setBounds(new Rectangle(new Point(985, 32), ARR1X.getPreferredSize()));

            //---- ARR2X ----
            ARR2X.setToolTipText("Cochez cette case pour ne pas recalculer le montant de la TVA");
            ARR2X.setName("ARR2X");
            panel5.add(ARR2X);
            ARR2X.setBounds(new Rectangle(new Point(985, 57), ARR2X.getPreferredSize()));

            //---- ARR3X ----
            ARR3X.setToolTipText("Cochez cette case pour ne pas recalculer le montant de la TVA");
            ARR3X.setName("ARR3X");
            panel5.add(ARR3X);
            ARR3X.setBounds(new Rectangle(new Point(985, 82), ARR3X.getPreferredSize()));

            //---- ARR4X ----
            ARR4X.setToolTipText("Cochez cette case pour ne pas recalculer le montant de la TVA");
            ARR4X.setName("ARR4X");
            panel5.add(ARR4X);
            ARR4X.setBounds(new Rectangle(new Point(985, 107), ARR4X.getPreferredSize()));

            //---- ARR5X ----
            ARR5X.setToolTipText("Cochez cette case pour ne pas recalculer le montant de la TVA");
            ARR5X.setName("ARR5X");
            panel5.add(ARR5X);
            ARR5X.setBounds(new Rectangle(new Point(985, 132), ARR5X.getPreferredSize()));

            //---- ARR6X ----
            ARR6X.setToolTipText("Cochez cette case pour ne pas recalculer le montant de la TVA");
            ARR6X.setName("ARR6X");
            panel5.add(ARR6X);
            ARR6X.setBounds(new Rectangle(new Point(985, 157), ARR6X.getPreferredSize()));

            //---- ARR7X ----
            ARR7X.setToolTipText("Cochez cette case pour ne pas recalculer le montant de la TVA");
            ARR7X.setName("ARR7X");
            panel5.add(ARR7X);
            ARR7X.setBounds(new Rectangle(new Point(985, 182), ARR7X.getPreferredSize()));

            //---- ARR8X ----
            ARR8X.setToolTipText("Cochez cette case pour ne pas recalculer le montant de la TVA");
            ARR8X.setName("ARR8X");
            panel5.add(ARR8X);
            ARR8X.setBounds(new Rectangle(new Point(985, 207), ARR8X.getPreferredSize()));

            //---- ARR9X ----
            ARR9X.setToolTipText("Cochez cette case pour ne pas recalculer le montant de la TVA");
            ARR9X.setName("ARR9X");
            panel5.add(ARR9X);
            ARR9X.setBounds(new Rectangle(new Point(985, 232), ARR9X.getPreferredSize()));

            //---- ARR10X ----
            ARR10X.setToolTipText("Cochez cette case pour ne pas recalculer le montant de la TVA");
            ARR10X.setName("ARR10X");
            panel5.add(ARR10X);
            ARR10X.setBounds(new Rectangle(new Point(985, 257), ARR10X.getPreferredSize()));

            //---- TVA1 ----
            TVA1.setToolTipText("Cadre TVA");
            TVA1.setName("TVA1");
            panel5.add(TVA1);
            TVA1.setBounds(960, 27, 24, TVA1.getPreferredSize().height);

            //---- TVA2 ----
            TVA2.setToolTipText("Cadre TVA");
            TVA2.setName("TVA2");
            panel5.add(TVA2);
            TVA2.setBounds(960, 52, 24, TVA2.getPreferredSize().height);

            //---- TVA3 ----
            TVA3.setToolTipText("Cadre TVA");
            TVA3.setName("TVA3");
            panel5.add(TVA3);
            TVA3.setBounds(960, 77, 24, TVA3.getPreferredSize().height);

            //---- TVA4 ----
            TVA4.setToolTipText("Cadre TVA");
            TVA4.setName("TVA4");
            panel5.add(TVA4);
            TVA4.setBounds(960, 102, 24, TVA4.getPreferredSize().height);

            //---- TVA5 ----
            TVA5.setToolTipText("Cadre TVA");
            TVA5.setName("TVA5");
            panel5.add(TVA5);
            TVA5.setBounds(960, 127, 24, TVA5.getPreferredSize().height);

            //---- TVA6 ----
            TVA6.setToolTipText("Cadre TVA");
            TVA6.setName("TVA6");
            panel5.add(TVA6);
            TVA6.setBounds(960, 152, 24, TVA6.getPreferredSize().height);

            //---- TVA7 ----
            TVA7.setToolTipText("Cadre TVA");
            TVA7.setName("TVA7");
            panel5.add(TVA7);
            TVA7.setBounds(960, 177, 24, TVA7.getPreferredSize().height);

            //---- TVA8 ----
            TVA8.setToolTipText("Cadre TVA");
            TVA8.setName("TVA8");
            panel5.add(TVA8);
            TVA8.setBounds(960, 202, 24, TVA8.getPreferredSize().height);

            //---- TVA9 ----
            TVA9.setToolTipText("Cadre TVA");
            TVA9.setName("TVA9");
            panel5.add(TVA9);
            TVA9.setBounds(960, 227, 24, TVA9.getPreferredSize().height);

            //---- TVA10 ----
            TVA10.setToolTipText("Cadre TVA");
            TVA10.setName("TVA10");
            panel5.add(TVA10);
            TVA10.setBounds(960, 252, 24, TVA10.getPreferredSize().height);

            //---- label16 ----
            label16.setText("TVA");
            label16.setFont(label16.getFont().deriveFont(label16.getFont().getStyle() | Font.BOLD));
            label16.setName("label16");
            panel5.add(label16);
            label16.setBounds(960, 5, 40, 25);

            //---- EPMT01 ----
            EPMT01.setName("EPMT01");
            panel5.add(EPMT01);
            EPMT01.setBounds(250, 27, 95, EPMT01.getPreferredSize().height);

            //---- EPMT02 ----
            EPMT02.setName("EPMT02");
            panel5.add(EPMT02);
            EPMT02.setBounds(250, 52, 95, EPMT02.getPreferredSize().height);

            //---- EPMT03 ----
            EPMT03.setName("EPMT03");
            panel5.add(EPMT03);
            EPMT03.setBounds(250, 77, 95, EPMT03.getPreferredSize().height);

            //---- EPMT04 ----
            EPMT04.setName("EPMT04");
            panel5.add(EPMT04);
            EPMT04.setBounds(250, 102, 95, EPMT04.getPreferredSize().height);

            //---- EPMT05 ----
            EPMT05.setName("EPMT05");
            panel5.add(EPMT05);
            EPMT05.setBounds(250, 127, 95, EPMT05.getPreferredSize().height);

            //---- EPMT06 ----
            EPMT06.setName("EPMT06");
            panel5.add(EPMT06);
            EPMT06.setBounds(250, 152, 95, EPMT06.getPreferredSize().height);

            //---- EPMT07 ----
            EPMT07.setName("EPMT07");
            panel5.add(EPMT07);
            EPMT07.setBounds(250, 177, 95, EPMT07.getPreferredSize().height);

            //---- EPMT08 ----
            EPMT08.setName("EPMT08");
            panel5.add(EPMT08);
            EPMT08.setBounds(250, 202, 95, EPMT08.getPreferredSize().height);

            //---- EPMT09 ----
            EPMT09.setName("EPMT09");
            panel5.add(EPMT09);
            EPMT09.setBounds(250, 227, 95, EPMT09.getPreferredSize().height);

            //---- EPMT10 ----
            EPMT10.setName("EPMT10");
            panel5.add(EPMT10);
            EPMT10.setBounds(250, 252, 95, EPMT10.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel5.getComponentCount(); i++) {
                Rectangle bounds = panel5.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel5.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel5.setMinimumSize(preferredSize);
              panel5.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel5);
          panel5.setBounds(5, 235, 1010, 290);

          //======== panel4 ========
          {
            panel4.setBorder(new DropShadowBorder());
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- EPLIT ----
            EPLIT.setComponentPopupMenu(BTD);
            EPLIT.setName("EPLIT");
            panel4.add(EPLIT);
            EPLIT.setBounds(165, 95, 260, EPLIT.getPreferredSize().height);

            //---- WOPCE ----
            WOPCE.setComponentPopupMenu(BTD);
            WOPCE.setName("WOPCE");
            panel4.add(WOPCE);
            WOPCE.setBounds(115, 5, 364, WOPCE.getPreferredSize().height);

            //---- OBJ_96 ----
            OBJ_96.setText("R\u00e9f. classement");
            OBJ_96.setName("OBJ_96");
            panel4.add(OBJ_96);
            OBJ_96.setBounds(250, 45, 105, 20);

            //---- OBJ_67 ----
            OBJ_67.setText("Libell\u00e9 \u00e9criture de tiers");
            OBJ_67.setName("OBJ_67");
            panel4.add(OBJ_67);
            OBJ_67.setBounds(165, 75, 156, 20);

            //---- OBJ_58 ----
            OBJ_58.setText("Objet de la pi\u00e8ce");
            OBJ_58.setName("OBJ_58");
            panel4.add(OBJ_58);
            OBJ_58.setBounds(15, 10, 100, 20);

            //---- EPRFC ----
            EPRFC.setName("EPRFC");
            panel4.add(EPRFC);
            EPRFC.setBounds(370, 40, 110, EPRFC.getPreferredSize().height);

            //---- WREFP ----
            WREFP.setComponentPopupMenu(BTD);
            WREFP.setName("WREFP");
            panel4.add(WREFP);
            WREFP.setBounds(115, 40, 92, WREFP.getPreferredSize().height);

            //---- OBJ_59 ----
            OBJ_59.setText("R\u00e9f\u00e9rence");
            OBJ_59.setName("OBJ_59");
            panel4.add(OBJ_59);
            OBJ_59.setBounds(15, 45, 85, 20);

            //---- WECH01 ----
            WECH01.setComponentPopupMenu(BTD);
            WECH01.setName("WECH01");
            panel4.add(WECH01);
            WECH01.setBounds(490, 95, 105, WECH01.getPreferredSize().height);

            //---- EPACT ----
            EPACT.setComponentPopupMenu(BTD);
            EPACT.setName("EPACT");
            panel4.add(EPACT);
            EPACT.setBounds(380, 125, 52, EPACT.getPreferredSize().height);

            //---- EPSAN ----
            EPSAN.setComponentPopupMenu(BTD);
            EPSAN.setName("EPSAN");
            panel4.add(EPSAN);
            EPSAN.setBounds(240, 125, 44, EPSAN.getPreferredSize().height);

            //---- EPNAT ----
            EPNAT.setComponentPopupMenu(BTD);
            EPNAT.setName("EPNAT");
            panel4.add(EPNAT);
            EPNAT.setBounds(525, 125, 52, EPNAT.getPreferredSize().height);

            //---- WRGL01 ----
            WRGL01.setComponentPopupMenu(BTD);
            WRGL01.setName("WRGL01");
            panel4.add(WRGL01);
            WRGL01.setBounds(455, 95, 30, WRGL01.getPreferredSize().height);

            //---- OBJ_81 ----
            OBJ_81.setText("Ech\u00e9ance");
            OBJ_81.setName("OBJ_81");
            panel4.add(OBJ_81);
            OBJ_81.setBounds(495, 75, 75, 20);

            //---- OBJ_82 ----
            OBJ_82.setText("Section");
            OBJ_82.setName("OBJ_82");
            panel4.add(OBJ_82);
            OBJ_82.setBounds(165, 130, 50, 20);

            //---- OBJ_84 ----
            OBJ_84.setText("Nature");
            OBJ_84.setName("OBJ_84");
            panel4.add(OBJ_84);
            OBJ_84.setBounds(455, 130, 40, 20);

            //---- OBJ_80 ----
            OBJ_80.setText("Rgl.");
            OBJ_80.setName("OBJ_80");
            panel4.add(OBJ_80);
            OBJ_80.setBounds(455, 75, 30, 20);

            //---- OBJ_83 ----
            OBJ_83.setText("Affaires");
            OBJ_83.setName("OBJ_83");
            panel4.add(OBJ_83);
            OBJ_83.setBounds(310, 130, 45, 20);

            //======== xTitledPanel2 ========
            {
              xTitledPanel2.setBorder(new DropShadowBorder());
              xTitledPanel2.setTitle("Montant TTC");
              xTitledPanel2.setName("xTitledPanel2");
              Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
              xTitledPanel2ContentContainer.setLayout(null);

              //---- OBJ_61 ----
              OBJ_61.setText("Section");
              OBJ_61.setName("OBJ_61");
              xTitledPanel2ContentContainer.add(OBJ_61);
              OBJ_61.setBounds(0, 0, 0, 0);

              //---- WMTTP ----
              WMTTP.setComponentPopupMenu(BTD);
              WMTTP.setName("WMTTP");
              xTitledPanel2ContentContainer.add(WMTTP);
              WMTTP.setBounds(15, 5, 116, WMTTP.getPreferredSize().height);

              //---- WDEV ----
              WDEV.setComponentPopupMenu(BTD);
              WDEV.setName("WDEV");
              xTitledPanel2ContentContainer.add(WDEV);
              WDEV.setBounds(15, 35, 40, WDEV.getPreferredSize().height);

              //---- WCHGX ----
              WCHGX.setComponentPopupMenu(BTD);
              WCHGX.setName("WCHGX");
              xTitledPanel2ContentContainer.add(WCHGX);
              WCHGX.setBounds(55, 35, 76, WCHGX.getPreferredSize().height);

              //---- WMTD ----
              WMTD.setComponentPopupMenu(BTD);
              WMTD.setName("WMTD");
              xTitledPanel2ContentContainer.add(WMTD);
              WMTD.setBounds(15, 65, 116, WMTD.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = xTitledPanel2ContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
                xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
              }
            }
            panel4.add(xTitledPanel2);
            xTitledPanel2.setBounds(10, 80, 145, 130);

            //---- OBJ_86 ----
            OBJ_86.setText("@LAXE1@");
            OBJ_86.setForeground(Color.darkGray);
            OBJ_86.setHorizontalTextPosition(SwingConstants.CENTER);
            OBJ_86.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_86.setFont(OBJ_86.getFont().deriveFont(OBJ_86.getFont().getStyle() | Font.BOLD));
            OBJ_86.setName("OBJ_86");
            panel4.add(OBJ_86);
            OBJ_86.setBounds(165, 155, 70, 25);

            //---- OBJ_87 ----
            OBJ_87.setText("@LAXE2@");
            OBJ_87.setForeground(Color.darkGray);
            OBJ_87.setHorizontalTextPosition(SwingConstants.CENTER);
            OBJ_87.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_87.setFont(OBJ_87.getFont().deriveFont(OBJ_87.getFont().getStyle() | Font.BOLD));
            OBJ_87.setName("OBJ_87");
            panel4.add(OBJ_87);
            OBJ_87.setBounds(237, 155, 70, 25);

            //---- OBJ_88 ----
            OBJ_88.setText("@LAXE3@");
            OBJ_88.setForeground(Color.darkGray);
            OBJ_88.setHorizontalTextPosition(SwingConstants.CENTER);
            OBJ_88.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_88.setFont(OBJ_88.getFont().deriveFont(OBJ_88.getFont().getStyle() | Font.BOLD));
            OBJ_88.setName("OBJ_88");
            panel4.add(OBJ_88);
            OBJ_88.setBounds(309, 155, 70, 25);

            //---- OBJ_89 ----
            OBJ_89.setText("@LAXE4@");
            OBJ_89.setForeground(Color.darkGray);
            OBJ_89.setHorizontalTextPosition(SwingConstants.CENTER);
            OBJ_89.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_89.setFont(OBJ_89.getFont().deriveFont(OBJ_89.getFont().getStyle() | Font.BOLD));
            OBJ_89.setName("OBJ_89");
            panel4.add(OBJ_89);
            OBJ_89.setBounds(381, 155, 70, 25);

            //---- OBJ_90 ----
            OBJ_90.setText("@LAXE5@");
            OBJ_90.setForeground(Color.darkGray);
            OBJ_90.setHorizontalTextPosition(SwingConstants.CENTER);
            OBJ_90.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_90.setFont(OBJ_90.getFont().deriveFont(OBJ_90.getFont().getStyle() | Font.BOLD));
            OBJ_90.setName("OBJ_90");
            panel4.add(OBJ_90);
            OBJ_90.setBounds(453, 155, 70, 25);

            //---- OBJ_91 ----
            OBJ_91.setText("@LAXE6@");
            OBJ_91.setForeground(Color.darkGray);
            OBJ_91.setHorizontalTextPosition(SwingConstants.CENTER);
            OBJ_91.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_91.setFont(OBJ_91.getFont().deriveFont(OBJ_91.getFont().getStyle() | Font.BOLD));
            OBJ_91.setName("OBJ_91");
            panel4.add(OBJ_91);
            OBJ_91.setBounds(525, 155, 70, 25);

            //---- EPAA1 ----
            EPAA1.setComponentPopupMenu(BTD);
            EPAA1.setName("EPAA1");
            panel4.add(EPAA1);
            EPAA1.setBounds(165, 180, 70, EPAA1.getPreferredSize().height);

            //---- EPAA2 ----
            EPAA2.setComponentPopupMenu(BTD);
            EPAA2.setName("EPAA2");
            panel4.add(EPAA2);
            EPAA2.setBounds(237, 180, 70, EPAA2.getPreferredSize().height);

            //---- EPAA3 ----
            EPAA3.setComponentPopupMenu(BTD);
            EPAA3.setName("EPAA3");
            panel4.add(EPAA3);
            EPAA3.setBounds(309, 180, 70, EPAA3.getPreferredSize().height);

            //---- EPAA4 ----
            EPAA4.setComponentPopupMenu(BTD);
            EPAA4.setName("EPAA4");
            panel4.add(EPAA4);
            EPAA4.setBounds(381, 180, 70, EPAA4.getPreferredSize().height);

            //---- EPAA5 ----
            EPAA5.setComponentPopupMenu(BTD);
            EPAA5.setName("EPAA5");
            panel4.add(EPAA5);
            EPAA5.setBounds(453, 180, 70, EPAA5.getPreferredSize().height);

            //---- EPAA6 ----
            EPAA6.setComponentPopupMenu(BTD);
            EPAA6.setName("EPAA6");
            panel4.add(EPAA6);
            EPAA6.setBounds(525, 180, 70, EPAA6.getPreferredSize().height);

            //======== panel9 ========
            {
              panel9.setOpaque(false);
              panel9.setName("panel9");
              panel9.setLayout(null);

              //---- OBJ_60 ----
              OBJ_60.setText("Facture d\u00e9finitive");
              OBJ_60.setName("OBJ_60");
              panel9.add(OBJ_60);
              OBJ_60.setBounds(5, 5, 95, 25);

              //---- WFDEF ----
              WFDEF.setText("oui");
              WFDEF.setName("WFDEF");
              panel9.add(WFDEF);
              WFDEF.setBounds(5, 30, WFDEF.getPreferredSize().width, 25);

              //---- WFDEF_NON ----
              WFDEF_NON.setText("non");
              WFDEF_NON.setName("WFDEF_NON");
              panel9.add(WFDEF_NON);
              WFDEF_NON.setBounds(55, 30, WFDEF_NON.getPreferredSize().width, 25);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel9.getComponentCount(); i++) {
                  Rectangle bounds = panel9.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel9.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel9.setMinimumSize(preferredSize);
                panel9.setPreferredSize(preferredSize);
              }
            }
            panel4.add(panel9);
            panel9.setBounds(490, 5, 105, 55);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel4);
          panel4.setBounds(405, 15, 610, 220);

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.ABOVE_TOP));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //======== panel1 ========
            {
              panel1.setOpaque(false);
              panel1.setBorder(new LineBorder(Color.lightGray));
              panel1.setForeground(new Color(204, 204, 204));
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- label1 ----
              label1.setText("Pi\u00e8ce");
              label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
              label1.setName("label1");
              panel1.add(label1);
              label1.setBounds(5, 5, 40, 28);

              //---- WIPCE ----
              WIPCE.setFont(WIPCE.getFont().deriveFont(WIPCE.getFont().getStyle() | Font.BOLD));
              WIPCE.setName("WIPCE");
              panel1.add(WIPCE);
              WIPCE.setBounds(45, 5, 75, 28);

              //---- OBJ_55 ----
              OBJ_55.setText("Solde");
              OBJ_55.setFont(OBJ_55.getFont().deriveFont(OBJ_55.getFont().getStyle() | Font.BOLD));
              OBJ_55.setName("OBJ_55");
              panel1.add(OBJ_55);
              OBJ_55.setBounds(135, 5, 34, 28);

              //---- USOLDE ----
              USOLDE.setFont(USOLDE.getFont().deriveFont(USOLDE.getFont().getStyle() | Font.BOLD));
              USOLDE.setName("USOLDE");
              panel1.add(USOLDE);
              USOLDE.setBounds(170, 5, 124, 28);

              //---- W1SNS ----
              W1SNS.setFont(W1SNS.getFont().deriveFont(W1SNS.getFont().getStyle() | Font.BOLD));
              W1SNS.setName("W1SNS");
              panel1.add(W1SNS);
              W1SNS.setBounds(295, 5, 30, W1SNS.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            panel3.add(panel1);
            panel1.setBounds(15, 6, 335, 39);

            //---- WTNOM ----
            WTNOM.setFont(WTNOM.getFont().deriveFont(WTNOM.getFont().getStyle() | Font.BOLD));
            WTNOM.setName("WTNOM");
            panel3.add(WTNOM);
            WTNOM.setBounds(20, 50, 310, WTNOM.getPreferredSize().height);

            //---- A1CPL ----
            A1CPL.setName("A1CPL");
            panel3.add(A1CPL);
            A1CPL.setBounds(20, 75, 310, A1CPL.getPreferredSize().height);

            //---- A1RUE ----
            A1RUE.setName("A1RUE");
            panel3.add(A1RUE);
            A1RUE.setBounds(20, 100, 310, A1RUE.getPreferredSize().height);

            //---- A1LOC ----
            A1LOC.setName("A1LOC");
            panel3.add(A1LOC);
            A1LOC.setBounds(20, 125, 310, A1LOC.getPreferredSize().height);

            //---- A1CDP ----
            A1CDP.setName("A1CDP");
            panel3.add(A1CDP);
            A1CDP.setBounds(20, 150, 55, A1CDP.getPreferredSize().height);

            //---- A1VIL ----
            A1VIL.setName("A1VIL");
            panel3.add(A1VIL);
            A1VIL.setBounds(75, 150, 255, A1VIL.getPreferredSize().height);

            //---- WRGL2 ----
            WRGL2.setComponentPopupMenu(BTD);
            WRGL2.setText("@WBQFRS@");
            WRGL2.setName("WRGL2");
            panel3.add(WRGL2);
            WRGL2.setBounds(75, 182, 30, WRGL2.getPreferredSize().height);

            //---- OBJ_85 ----
            OBJ_85.setText("Banque");
            OBJ_85.setName("OBJ_85");
            panel3.add(OBJ_85);
            OBJ_85.setBounds(20, 180, 50, 28);

            //---- EPJO ----
            EPJO.setComponentPopupMenu(BTD);
            EPJO.setName("EPJO");
            panel3.add(EPJO);
            EPJO.setBounds(300, 180, 30, EPJO.getPreferredSize().height);

            //---- OBJ_92 ----
            OBJ_92.setText("Code journal");
            OBJ_92.setName("OBJ_92");
            panel3.add(OBJ_92);
            OBJ_92.setBounds(205, 180, 90, 28);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel3);
          panel3.setBounds(5, 14, 359, 220);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }

    //---- WFDEF_GRP ----
    WFDEF_GRP.add(WFDEF);
    WFDEF_GRP.add(WFDEF_NON);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_39;
  private XRiTextField WISOC;
  private JLabel OBJ_41;
  private XRiTextField WCEP;
  private JLabel OBJ_43;
  private XRiTextField WINCGX;
  private XRiTextField WINCA;
  private JLabel OBJ_46;
  private XRiTextField WIDTEX;
  private RiZoneSortie WDTFRA;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel6;
  private XRiTextField WTDB;
  private JLabel OBJ_123;
  private XRiTextField WTCR;
  private JLabel OBJ_128;
  private RiZoneSortie WDIF;
  private JLabel OBJ_130;
  private JPanel panel5;
  private RiZoneSortie EPIN01;
  private XRiTextField NCG1X;
  private XRiTextField EPNA01;
  private XRiTextField EPSN01;
  private XRiTextField EPLI01;
  private XRiTextField SAN1X;
  private RiZoneSortie EPIN02;
  private XRiTextField NCG2X;
  private XRiTextField EPNA02;
  private XRiTextField EPSN02;
  private XRiTextField EPLI02;
  private XRiTextField SAN2X;
  private RiZoneSortie EPIN03;
  private XRiTextField NCG3X;
  private XRiTextField EPNA03;
  private XRiTextField EPSN03;
  private XRiTextField EPLI03;
  private XRiTextField SAN3X;
  private RiZoneSortie EPIN04;
  private XRiTextField NCG4X;
  private XRiTextField EPNA04;
  private XRiTextField EPSN04;
  private XRiTextField EPLI04;
  private XRiTextField SAN4X;
  private RiZoneSortie EPIN05;
  private XRiTextField NCG5X;
  private XRiTextField EPNA05;
  private XRiTextField EPSN05;
  private XRiTextField EPLI05;
  private XRiTextField SAN5X;
  private RiZoneSortie EPIN06;
  private XRiTextField NCG6X;
  private XRiTextField EPNA06;
  private XRiTextField EPSN06;
  private XRiTextField EPLI06;
  private XRiTextField SAN6X;
  private RiZoneSortie EPIN07;
  private XRiTextField NCG7X;
  private XRiTextField EPNA07;
  private XRiTextField EPSN07;
  private XRiTextField EPLI07;
  private XRiTextField SAN7X;
  private RiZoneSortie EPIN08;
  private XRiTextField NCG8X;
  private XRiTextField EPNA08;
  private XRiTextField EPSN08;
  private XRiTextField EPLI08;
  private XRiTextField SAN8X;
  private RiZoneSortie EPIN09;
  private XRiTextField NCG9X;
  private XRiTextField EPNA09;
  private XRiTextField EPSN09;
  private XRiTextField EPLI09;
  private XRiTextField SAN9X;
  private RiZoneSortie EPIN10;
  private XRiTextField NCG10X;
  private XRiTextField EPNA10;
  private XRiTextField EPSN10;
  private XRiTextField EPLI10;
  private XRiTextField SAN10X;
  private XRiTextField AFF1X;
  private XRiTextField EPCP01;
  private XRiTextField QTE1X;
  private XRiTextField AFF2X;
  private XRiTextField AFF3X;
  private XRiTextField AFF4X;
  private XRiTextField AFF5X;
  private XRiTextField AFF6X;
  private XRiTextField AFF7X;
  private XRiTextField AFF8X;
  private XRiTextField AFF9X;
  private XRiTextField AFF10X;
  private XRiTextField EPCP02;
  private XRiTextField EPCP03;
  private XRiTextField EPCP04;
  private XRiTextField EPCP05;
  private XRiTextField EPCP06;
  private XRiTextField EPCP07;
  private XRiTextField EPCP08;
  private XRiTextField EPCP09;
  private XRiTextField EPCP10;
  private XRiTextField QTE2X;
  private XRiTextField QTE3X;
  private XRiTextField QTE4X;
  private XRiTextField QTE5x;
  private XRiTextField QTE6X;
  private XRiTextField QTE7X;
  private XRiTextField QTE8X;
  private XRiTextField QTE9X;
  private XRiTextField QTE10X;
  private XRiTextField NAT1X;
  private XRiTextField NAT2X;
  private XRiTextField NAT3X;
  private XRiTextField NAT4X;
  private XRiTextField NAT5X;
  private XRiTextField NAT6X;
  private XRiTextField NAT7X;
  private XRiTextField NAT8X;
  private XRiTextField NAT9X;
  private XRiTextField NAT10X;
  private JPanel panel7;
  private SNBoutonDetail OBJ_107;
  private SNBoutonDetail OBJ_110;
  private SNBoutonDetail OBJ_111;
  private SNBoutonDetail OBJ_112;
  private SNBoutonDetail OBJ_114;
  private SNBoutonDetail OBJ_115;
  private SNBoutonDetail OBJ_116;
  private SNBoutonDetail OBJ_118;
  private SNBoutonDetail OBJ_119;
  private SNBoutonDetail OBJ_121;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private JLabel label10;
  private JLabel label11;
  private JLabel label12;
  private JLabel label13;
  private JLabel label14;
  private JLabel label15;
  private JLabel label5;
  private XRiCheckBox ARR1X;
  private XRiCheckBox ARR2X;
  private XRiCheckBox ARR3X;
  private XRiCheckBox ARR4X;
  private XRiCheckBox ARR5X;
  private XRiCheckBox ARR6X;
  private XRiCheckBox ARR7X;
  private XRiCheckBox ARR8X;
  private XRiCheckBox ARR9X;
  private XRiCheckBox ARR10X;
  private XRiTextField TVA1;
  private XRiTextField TVA2;
  private XRiTextField TVA3;
  private XRiTextField TVA4;
  private XRiTextField TVA5;
  private XRiTextField TVA6;
  private XRiTextField TVA7;
  private XRiTextField TVA8;
  private XRiTextField TVA9;
  private XRiTextField TVA10;
  private JLabel label16;
  private XRiTextField EPMT01;
  private XRiTextField EPMT02;
  private XRiTextField EPMT03;
  private XRiTextField EPMT04;
  private XRiTextField EPMT05;
  private XRiTextField EPMT06;
  private XRiTextField EPMT07;
  private XRiTextField EPMT08;
  private XRiTextField EPMT09;
  private XRiTextField EPMT10;
  private JPanel panel4;
  private XRiTextField EPLIT;
  private XRiTextField WOPCE;
  private JLabel OBJ_96;
  private JLabel OBJ_67;
  private JLabel OBJ_58;
  private XRiTextField EPRFC;
  private XRiTextField WREFP;
  private JLabel OBJ_59;
  private XRiTextField WECH01;
  private XRiTextField EPACT;
  private XRiTextField EPSAN;
  private XRiTextField EPNAT;
  private XRiTextField WRGL01;
  private JLabel OBJ_81;
  private JLabel OBJ_82;
  private JLabel OBJ_84;
  private JLabel OBJ_80;
  private JLabel OBJ_83;
  private JXTitledPanel xTitledPanel2;
  private JLabel OBJ_61;
  private XRiTextField WMTTP;
  private XRiTextField WDEV;
  private XRiTextField WCHGX;
  private XRiTextField WMTD;
  private JLabel OBJ_86;
  private JLabel OBJ_87;
  private JLabel OBJ_88;
  private JLabel OBJ_89;
  private JLabel OBJ_90;
  private JLabel OBJ_91;
  private XRiTextField EPAA1;
  private XRiTextField EPAA2;
  private XRiTextField EPAA3;
  private XRiTextField EPAA4;
  private XRiTextField EPAA5;
  private XRiTextField EPAA6;
  private JPanel panel9;
  private JLabel OBJ_60;
  private XRiRadioButton WFDEF;
  private XRiRadioButton WFDEF_NON;
  private JPanel panel3;
  private JPanel panel1;
  private JLabel label1;
  private XRiTextField WIPCE;
  private JLabel OBJ_55;
  private XRiTextField USOLDE;
  private XRiTextField W1SNS;
  private XRiTextField WTNOM;
  private XRiTextField A1CPL;
  private XRiTextField A1RUE;
  private XRiTextField A1LOC;
  private XRiTextField A1CDP;
  private XRiTextField A1VIL;
  private RiZoneSortie WRGL2;
  private JLabel OBJ_85;
  private XRiTextField EPJO;
  private JLabel OBJ_92;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  private ButtonGroup WFDEF_GRP;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
