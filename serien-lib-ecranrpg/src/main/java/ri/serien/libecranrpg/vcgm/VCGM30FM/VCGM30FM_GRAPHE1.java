/*
 * Created by JFormDesigner on Wed Feb 24 16:24:25 CET 2010
 */

package ri.serien.libecranrpg.vcgm.VCGM30FM;

import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.WindowConstants;

import ri.serien.libswing.composantrpg.lexical.RiGraphe;
import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * @author Stéphane Vénéri
 */
public class VCGM30FM_GRAPHE1 extends JFrame {
  
  
  private String[] annee = null;
  private String[] donnee = null;
  private RiGraphe[] graphe = new RiGraphe[12];
  private String[] moisLib =
      { "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre" };
  
  /**
   * Constructeur
   * @param annee
   * @param donnee
   */
  public VCGM30FM_GRAPHE1(String[] annee, String[] donnee) {
    super();
    initComponents();
    
    this.annee = annee;
    this.donnee = donnee;
    
    initData();
    setSize(1000, 700);
    setBackground(SNCharteGraphique.COULEUR_FOND);
    setVisible(true);
  }
  
  /**
   * Initialise les données pour les graphes
   *
   */
  private void initData() {
    // Préparation des données
    for (int mois = 0; mois < 12; mois++) {
      Object[][] data = new Object[annee.length][2];
      for (int i = 0; i < annee.length; i++) {
        data[i][0] = annee[i];
        data[i][1] = Double.parseDouble(donnee[(i * 12) + mois]);
      }
      graphe[mois] = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
      graphe[mois].setDonnee(data, moisLib[mois], false);
      graphe[mois].getGraphe(moisLib[mois], false);
    }
  }
  
  /**
   * Dessine les graphes
   *
   */
  private void paintGraphe() {
    l_Janvier.setIcon(graphe[0].getPicture(l_Janvier.getWidth(), l_Janvier.getHeight()));
    l_Fevrier.setIcon(graphe[1].getPicture(l_Fevrier.getWidth(), l_Fevrier.getHeight()));
    l_Mars.setIcon(graphe[2].getPicture(l_Mars.getWidth(), l_Mars.getHeight()));
    l_Avril.setIcon(graphe[3].getPicture(l_Avril.getWidth(), l_Avril.getHeight()));
    l_Mai.setIcon(graphe[4].getPicture(l_Mai.getWidth(), l_Mai.getHeight()));
    l_Juin.setIcon(graphe[5].getPicture(l_Juin.getWidth(), l_Juin.getHeight()));
    l_Juillet.setIcon(graphe[6].getPicture(l_Juillet.getWidth(), l_Juillet.getHeight()));
    l_Aout.setIcon(graphe[7].getPicture(l_Aout.getWidth(), l_Aout.getHeight()));
    l_Septembre.setIcon(graphe[8].getPicture(l_Septembre.getWidth(), l_Septembre.getHeight()));
    l_Octobre.setIcon(graphe[9].getPicture(l_Octobre.getWidth(), l_Octobre.getHeight()));
    l_Novembre.setIcon(graphe[10].getPicture(l_Novembre.getWidth(), l_Novembre.getHeight()));
    l_Decembre.setIcon(graphe[11].getPicture(l_Decembre.getWidth(), l_Decembre.getHeight()));
  }
  
  /**
   * Récupère l'indice du mois à partir du libellé
   * @param nomobj
   * @return
   */
  private int getIndice(String nomobj) {
    for (int i = 0; i < moisLib.length; i++) {
      if (moisLib[i].equals(nomobj)) {
        return i;
      }
    }
    
    return -1;
  }
  
  private void thisComponentResized(ComponentEvent e) {
    paintGraphe();
  }
  
  private void MI_CopierActionPerformed(ActionEvent e) {
    int indice = getIndice(BTD.getInvoker().getName().substring(2));
    if ((indice >= 0) && (indice < moisLib.length)) {
      graphe[indice].sendToClipBoard(l_Janvier.getWidth(), l_Janvier.getHeight());
    }
  }
  
  private void MI_EnregistrerActionPerformed(ActionEvent e) {
    int indice = getIndice(BTD.getInvoker().getName().substring(2));
    if ((indice >= 0) && (indice < moisLib.length)) {
      graphe[indice].saveGraphe(null, l_Janvier.getWidth(), l_Janvier.getHeight());
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    l_Janvier = new JLabel();
    l_Fevrier = new JLabel();
    l_Mars = new JLabel();
    l_Avril = new JLabel();
    l_Mai = new JLabel();
    l_Juin = new JLabel();
    l_Juillet = new JLabel();
    l_Aout = new JLabel();
    l_Septembre = new JLabel();
    l_Octobre = new JLabel();
    l_Novembre = new JLabel();
    l_Decembre = new JLabel();
    BTD = new JPopupMenu();
    MI_Copier = new JMenuItem();
    MI_Enregistrer = new JMenuItem();

    //======== this ========
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setTitle("Analyse de l'\u00e9volution des comptes pour un mois donn\u00e9e");
    setIconImage(null);
    setBackground(new Color(238, 238, 210));
    setName("this");
    addComponentListener(new ComponentAdapter() {
      @Override
      public void componentResized(ComponentEvent e) {
        thisComponentResized(e);
      }
    });
    Container contentPane = getContentPane();
    contentPane.setLayout(new GridLayout(3, 4));

    //---- l_Janvier ----
    l_Janvier.setComponentPopupMenu(BTD);
    l_Janvier.setName("l_Janvier");
    contentPane.add(l_Janvier);

    //---- l_Fevrier ----
    l_Fevrier.setComponentPopupMenu(BTD);
    l_Fevrier.setName("l_Fevrier");
    contentPane.add(l_Fevrier);

    //---- l_Mars ----
    l_Mars.setComponentPopupMenu(BTD);
    l_Mars.setName("l_Mars");
    contentPane.add(l_Mars);

    //---- l_Avril ----
    l_Avril.setComponentPopupMenu(BTD);
    l_Avril.setName("l_Avril");
    contentPane.add(l_Avril);

    //---- l_Mai ----
    l_Mai.setComponentPopupMenu(BTD);
    l_Mai.setName("l_Mai");
    contentPane.add(l_Mai);

    //---- l_Juin ----
    l_Juin.setComponentPopupMenu(BTD);
    l_Juin.setName("l_Juin");
    contentPane.add(l_Juin);

    //---- l_Juillet ----
    l_Juillet.setComponentPopupMenu(BTD);
    l_Juillet.setName("l_Juillet");
    contentPane.add(l_Juillet);

    //---- l_Aout ----
    l_Aout.setComponentPopupMenu(BTD);
    l_Aout.setName("l_Aout");
    contentPane.add(l_Aout);

    //---- l_Septembre ----
    l_Septembre.setComponentPopupMenu(BTD);
    l_Septembre.setName("l_Septembre");
    contentPane.add(l_Septembre);

    //---- l_Octobre ----
    l_Octobre.setComponentPopupMenu(BTD);
    l_Octobre.setName("l_Octobre");
    contentPane.add(l_Octobre);

    //---- l_Novembre ----
    l_Novembre.setComponentPopupMenu(BTD);
    l_Novembre.setName("l_Novembre");
    contentPane.add(l_Novembre);

    //---- l_Decembre ----
    l_Decembre.setComponentPopupMenu(BTD);
    l_Decembre.setName("l_Decembre");
    contentPane.add(l_Decembre);
    pack();

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- MI_Copier ----
      MI_Copier.setText("Copier");
      MI_Copier.setName("MI_Copier");
      MI_Copier.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          MI_CopierActionPerformed(e);
        }
      });
      BTD.add(MI_Copier);

      //---- MI_Enregistrer ----
      MI_Enregistrer.setText("Enregistrer sous");
      MI_Enregistrer.setName("MI_Enregistrer");
      MI_Enregistrer.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          MI_EnregistrerActionPerformed(e);
        }
      });
      BTD.add(MI_Enregistrer);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel l_Janvier;
  private JLabel l_Fevrier;
  private JLabel l_Mars;
  private JLabel l_Avril;
  private JLabel l_Mai;
  private JLabel l_Juin;
  private JLabel l_Juillet;
  private JLabel l_Aout;
  private JLabel l_Septembre;
  private JLabel l_Octobre;
  private JLabel l_Novembre;
  private JLabel l_Decembre;
  private JPopupMenu BTD;
  private JMenuItem MI_Copier;
  private JMenuItem MI_Enregistrer;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
