
package ri.serien.libecranrpg.vcgm.VCGM50FM;
// Nom Fichier: pop_VCGM50FM_FMTMD_716.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComponent;

import com.jgoodies.forms.factories.DefaultComponentFactory;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class VCGM50FM_MD extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM50FM_MD(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(OBJ_7);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Choix fonctions"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "R", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "M", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "I", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    OBJ_7 = new JButton();
    OBJ_8 = new JButton();
    OBJ_9 = new JButton();
    separator1 = compFactory.createSeparator("Fonctions");

    //======== this ========
    setName("this");
    setLayout(null);

    //---- OBJ_7 ----
    OBJ_7.setText("Retour");
    OBJ_7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_7.setName("OBJ_7");
    OBJ_7.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_7ActionPerformed(e);
      }
    });
    add(OBJ_7);
    OBJ_7.setBounds(31, 46, 130, 24);

    //---- OBJ_8 ----
    OBJ_8.setText("Modification");
    OBJ_8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_8.setName("OBJ_8");
    OBJ_8.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_8ActionPerformed(e);
      }
    });
    add(OBJ_8);
    OBJ_8.setBounds(31, 78, 130, 24);

    //---- OBJ_9 ----
    OBJ_9.setText("Interrogation");
    OBJ_9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_9.setName("OBJ_9");
    OBJ_9.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_9ActionPerformed(e);
      }
    });
    add(OBJ_9);
    OBJ_9.setBounds(31, 110, 130, 24);

    //---- separator1 ----
    separator1.setName("separator1");
    add(separator1);
    separator1.setBounds(15, 15, 160, separator1.getPreferredSize().height);

    setPreferredSize(new Dimension(192, 156));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JButton OBJ_7;
  private JButton OBJ_8;
  private JButton OBJ_9;
  private JComponent separator1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
