
package ri.serien.libecranrpg.vcgm.VCGM9AFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM9AFM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM9AFM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBHIE@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_106.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB01@")).trim());
    OBJ_107.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB02@")).trim());
    OBJ_108.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB03@")).trim());
    OBJ_109.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB04@")).trim());
    OBJ_110.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB05@")).trim());
    OBJ_111.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB06@")).trim());
    OBJ_112.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB07@")).trim());
    OBJ_113.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB08@")).trim());
    OBJ_114.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB09@")).trim());
    OBJ_115.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB10@")).trim());
    OBJ_116.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB11@")).trim());
    OBJ_117.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB12@")).trim());
    OBJ_118.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB13@")).trim());
    OBJ_119.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB14@")).trim());
    OBJ_120.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB15@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    xTitledPanel1.setVisible(lexique.isTrue("N60"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDSOC.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(INDSOC.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche2 = new JPanel();
    OBJ_23 = new JLabel();
    INDSOC = new XRiTextField();
    OBJ_42 = new JLabel();
    INDHIE = new XRiTextField();
    INDNIV = new XRiTextField();
    INDGAN = new XRiTextField();
    OBJ_33 = new RiZoneSortie();
    p_tete_gauche = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    GALIB = new XRiTextField();
    OBJ_46 = new JLabel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_106 = new RiZoneSortie();
    OBJ_107 = new RiZoneSortie();
    OBJ_108 = new RiZoneSortie();
    OBJ_109 = new RiZoneSortie();
    OBJ_110 = new RiZoneSortie();
    OBJ_111 = new RiZoneSortie();
    OBJ_112 = new RiZoneSortie();
    OBJ_113 = new RiZoneSortie();
    OBJ_114 = new RiZoneSortie();
    OBJ_115 = new RiZoneSortie();
    OBJ_116 = new RiZoneSortie();
    OBJ_117 = new RiZoneSortie();
    OBJ_118 = new RiZoneSortie();
    OBJ_119 = new RiZoneSortie();
    OBJ_120 = new RiZoneSortie();
    OBJ_49 = new JLabel();
    GACD01 = new XRiTextField();
    GACD02 = new XRiTextField();
    GACD03 = new XRiTextField();
    GACD04 = new XRiTextField();
    GACD05 = new XRiTextField();
    GACD06 = new XRiTextField();
    GACD07 = new XRiTextField();
    GACD08 = new XRiTextField();
    GACD09 = new XRiTextField();
    GACD10 = new XRiTextField();
    GACD11 = new XRiTextField();
    GACD12 = new XRiTextField();
    GACD13 = new XRiTextField();
    GACD14 = new XRiTextField();
    GACD15 = new XRiTextField();
    OBJ_48 = new JLabel();
    GANV01 = new XRiTextField();
    GANV02 = new XRiTextField();
    GANV03 = new XRiTextField();
    GANV04 = new XRiTextField();
    GANV05 = new XRiTextField();
    GANV06 = new XRiTextField();
    GANV07 = new XRiTextField();
    GANV08 = new XRiTextField();
    GANV09 = new XRiTextField();
    GANV10 = new XRiTextField();
    GANV11 = new XRiTextField();
    GANV12 = new XRiTextField();
    GANV13 = new XRiTextField();
    GANV14 = new XRiTextField();
    GANV15 = new XRiTextField();
    OBJ_91 = new JLabel();
    OBJ_92 = new JLabel();
    OBJ_93 = new JLabel();
    OBJ_94 = new JLabel();
    OBJ_95 = new JLabel();
    OBJ_96 = new JLabel();
    OBJ_97 = new JLabel();
    OBJ_98 = new JLabel();
    OBJ_99 = new JLabel();
    OBJ_100 = new JLabel();
    OBJ_101 = new JLabel();
    OBJ_102 = new JLabel();
    OBJ_103 = new JLabel();
    OBJ_104 = new JLabel();
    OBJ_105 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche2 ========
        {
          p_tete_gauche2.setOpaque(false);
          p_tete_gauche2.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche2.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche2.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche2.setName("p_tete_gauche2");

          //---- OBJ_23 ----
          OBJ_23.setText("Soci\u00e9t\u00e9");
          OBJ_23.setName("OBJ_23");

          //---- INDSOC ----
          INDSOC.setComponentPopupMenu(BTD);
          INDSOC.setName("INDSOC");

          //---- OBJ_42 ----
          OBJ_42.setText("Code groupe");
          OBJ_42.setName("OBJ_42");

          //---- INDHIE ----
          INDHIE.setComponentPopupMenu(BTD);
          INDHIE.setName("INDHIE");

          //---- INDNIV ----
          INDNIV.setComponentPopupMenu(BTD);
          INDNIV.setName("INDNIV");

          //---- INDGAN ----
          INDGAN.setComponentPopupMenu(BTD);
          INDGAN.setName("INDGAN");

          //---- OBJ_33 ----
          OBJ_33.setText("@LIBHIE@");
          OBJ_33.setOpaque(false);
          OBJ_33.setName("OBJ_33");

          GroupLayout p_tete_gauche2Layout = new GroupLayout(p_tete_gauche2);
          p_tete_gauche2.setLayout(p_tete_gauche2Layout);
          p_tete_gauche2Layout.setHorizontalGroup(
            p_tete_gauche2Layout.createParallelGroup()
              .addGroup(p_tete_gauche2Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_23, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDSOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDHIE, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INDNIV, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INDGAN, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 232, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gauche2Layout.setVerticalGroup(
            p_tete_gauche2Layout.createParallelGroup()
              .addGroup(p_tete_gauche2Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_23, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDSOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gauche2Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDHIE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDNIV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDGAN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gauche2Layout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche2);

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(810, 560));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- GALIB ----
            GALIB.setComponentPopupMenu(BTD);
            GALIB.setName("GALIB");
            panel1.add(GALIB);
            GALIB.setBounds(275, 20, 310, GALIB.getPreferredSize().height);

            //---- OBJ_46 ----
            OBJ_46.setText("Libell\u00e9");
            OBJ_46.setName("OBJ_46");
            panel1.add(OBJ_46);
            OBJ_46.setBounds(165, 25, 61, 20);

            //======== xTitledPanel1 ========
            {
              xTitledPanel1.setTitle("Composant");
              xTitledPanel1.setBorder(new DropShadowBorder());
              xTitledPanel1.setPreferredSize(new Dimension(750, 457));
              xTitledPanel1.setName("xTitledPanel1");
              Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
              xTitledPanel1ContentContainer.setLayout(null);

              //---- OBJ_106 ----
              OBJ_106.setText("@LIB01@");
              OBJ_106.setName("OBJ_106");
              xTitledPanel1ContentContainer.add(OBJ_106);
              OBJ_106.setBounds(365, 38, 232, OBJ_106.getPreferredSize().height);

              //---- OBJ_107 ----
              OBJ_107.setText("@LIB02@");
              OBJ_107.setName("OBJ_107");
              xTitledPanel1ContentContainer.add(OBJ_107);
              OBJ_107.setBounds(365, 64, 232, OBJ_107.getPreferredSize().height);

              //---- OBJ_108 ----
              OBJ_108.setText("@LIB03@");
              OBJ_108.setName("OBJ_108");
              xTitledPanel1ContentContainer.add(OBJ_108);
              OBJ_108.setBounds(365, 90, 232, OBJ_108.getPreferredSize().height);

              //---- OBJ_109 ----
              OBJ_109.setText("@LIB04@");
              OBJ_109.setName("OBJ_109");
              xTitledPanel1ContentContainer.add(OBJ_109);
              OBJ_109.setBounds(365, 116, 232, OBJ_109.getPreferredSize().height);

              //---- OBJ_110 ----
              OBJ_110.setText("@LIB05@");
              OBJ_110.setName("OBJ_110");
              xTitledPanel1ContentContainer.add(OBJ_110);
              OBJ_110.setBounds(365, 142, 232, OBJ_110.getPreferredSize().height);

              //---- OBJ_111 ----
              OBJ_111.setText("@LIB06@");
              OBJ_111.setName("OBJ_111");
              xTitledPanel1ContentContainer.add(OBJ_111);
              OBJ_111.setBounds(365, 168, 232, OBJ_111.getPreferredSize().height);

              //---- OBJ_112 ----
              OBJ_112.setText("@LIB07@");
              OBJ_112.setName("OBJ_112");
              xTitledPanel1ContentContainer.add(OBJ_112);
              OBJ_112.setBounds(365, 194, 232, OBJ_112.getPreferredSize().height);

              //---- OBJ_113 ----
              OBJ_113.setText("@LIB08@");
              OBJ_113.setName("OBJ_113");
              xTitledPanel1ContentContainer.add(OBJ_113);
              OBJ_113.setBounds(365, 220, 232, OBJ_113.getPreferredSize().height);

              //---- OBJ_114 ----
              OBJ_114.setText("@LIB09@");
              OBJ_114.setName("OBJ_114");
              xTitledPanel1ContentContainer.add(OBJ_114);
              OBJ_114.setBounds(365, 246, 232, OBJ_114.getPreferredSize().height);

              //---- OBJ_115 ----
              OBJ_115.setText("@LIB10@");
              OBJ_115.setName("OBJ_115");
              xTitledPanel1ContentContainer.add(OBJ_115);
              OBJ_115.setBounds(365, 272, 232, OBJ_115.getPreferredSize().height);

              //---- OBJ_116 ----
              OBJ_116.setText("@LIB11@");
              OBJ_116.setName("OBJ_116");
              xTitledPanel1ContentContainer.add(OBJ_116);
              OBJ_116.setBounds(365, 298, 232, OBJ_116.getPreferredSize().height);

              //---- OBJ_117 ----
              OBJ_117.setText("@LIB12@");
              OBJ_117.setName("OBJ_117");
              xTitledPanel1ContentContainer.add(OBJ_117);
              OBJ_117.setBounds(365, 324, 232, OBJ_117.getPreferredSize().height);

              //---- OBJ_118 ----
              OBJ_118.setText("@LIB13@");
              OBJ_118.setName("OBJ_118");
              xTitledPanel1ContentContainer.add(OBJ_118);
              OBJ_118.setBounds(365, 350, 232, OBJ_118.getPreferredSize().height);

              //---- OBJ_119 ----
              OBJ_119.setText("@LIB14@");
              OBJ_119.setName("OBJ_119");
              xTitledPanel1ContentContainer.add(OBJ_119);
              OBJ_119.setBounds(365, 376, 232, OBJ_119.getPreferredSize().height);

              //---- OBJ_120 ----
              OBJ_120.setText("@LIB15@");
              OBJ_120.setName("OBJ_120");
              xTitledPanel1ContentContainer.add(OBJ_120);
              OBJ_120.setBounds(365, 402, 232, OBJ_120.getPreferredSize().height);

              //---- OBJ_49 ----
              OBJ_49.setText("Code");
              OBJ_49.setName("OBJ_49");
              xTitledPanel1ContentContainer.add(OBJ_49);
              OBJ_49.setBounds(295, 10, 55, 25);

              //---- GACD01 ----
              GACD01.setComponentPopupMenu(BTD);
              GACD01.setName("GACD01");
              xTitledPanel1ContentContainer.add(GACD01);
              GACD01.setBounds(295, 36, 60, GACD01.getPreferredSize().height);

              //---- GACD02 ----
              GACD02.setComponentPopupMenu(BTD);
              GACD02.setName("GACD02");
              xTitledPanel1ContentContainer.add(GACD02);
              GACD02.setBounds(295, 62, 60, GACD02.getPreferredSize().height);

              //---- GACD03 ----
              GACD03.setComponentPopupMenu(BTD);
              GACD03.setName("GACD03");
              xTitledPanel1ContentContainer.add(GACD03);
              GACD03.setBounds(295, 88, 60, GACD03.getPreferredSize().height);

              //---- GACD04 ----
              GACD04.setComponentPopupMenu(BTD);
              GACD04.setName("GACD04");
              xTitledPanel1ContentContainer.add(GACD04);
              GACD04.setBounds(295, 114, 60, GACD04.getPreferredSize().height);

              //---- GACD05 ----
              GACD05.setComponentPopupMenu(BTD);
              GACD05.setName("GACD05");
              xTitledPanel1ContentContainer.add(GACD05);
              GACD05.setBounds(295, 140, 60, GACD05.getPreferredSize().height);

              //---- GACD06 ----
              GACD06.setComponentPopupMenu(BTD);
              GACD06.setName("GACD06");
              xTitledPanel1ContentContainer.add(GACD06);
              GACD06.setBounds(295, 166, 60, GACD06.getPreferredSize().height);

              //---- GACD07 ----
              GACD07.setComponentPopupMenu(BTD);
              GACD07.setName("GACD07");
              xTitledPanel1ContentContainer.add(GACD07);
              GACD07.setBounds(295, 192, 60, GACD07.getPreferredSize().height);

              //---- GACD08 ----
              GACD08.setComponentPopupMenu(BTD);
              GACD08.setName("GACD08");
              xTitledPanel1ContentContainer.add(GACD08);
              GACD08.setBounds(295, 218, 60, GACD08.getPreferredSize().height);

              //---- GACD09 ----
              GACD09.setComponentPopupMenu(BTD);
              GACD09.setName("GACD09");
              xTitledPanel1ContentContainer.add(GACD09);
              GACD09.setBounds(295, 244, 60, GACD09.getPreferredSize().height);

              //---- GACD10 ----
              GACD10.setComponentPopupMenu(BTD);
              GACD10.setName("GACD10");
              xTitledPanel1ContentContainer.add(GACD10);
              GACD10.setBounds(295, 270, 60, GACD10.getPreferredSize().height);

              //---- GACD11 ----
              GACD11.setComponentPopupMenu(BTD);
              GACD11.setName("GACD11");
              xTitledPanel1ContentContainer.add(GACD11);
              GACD11.setBounds(295, 296, 60, GACD11.getPreferredSize().height);

              //---- GACD12 ----
              GACD12.setComponentPopupMenu(BTD);
              GACD12.setName("GACD12");
              xTitledPanel1ContentContainer.add(GACD12);
              GACD12.setBounds(295, 322, 60, GACD12.getPreferredSize().height);

              //---- GACD13 ----
              GACD13.setComponentPopupMenu(BTD);
              GACD13.setName("GACD13");
              xTitledPanel1ContentContainer.add(GACD13);
              GACD13.setBounds(295, 348, 60, GACD13.getPreferredSize().height);

              //---- GACD14 ----
              GACD14.setComponentPopupMenu(BTD);
              GACD14.setName("GACD14");
              xTitledPanel1ContentContainer.add(GACD14);
              GACD14.setBounds(295, 374, 60, GACD14.getPreferredSize().height);

              //---- GACD15 ----
              GACD15.setComponentPopupMenu(BTD);
              GACD15.setName("GACD15");
              xTitledPanel1ContentContainer.add(GACD15);
              GACD15.setBounds(295, 400, 60, GACD15.getPreferredSize().height);

              //---- OBJ_48 ----
              OBJ_48.setText("Niv");
              OBJ_48.setName("OBJ_48");
              xTitledPanel1ContentContainer.add(OBJ_48);
              OBJ_48.setBounds(265, 10, 24, 25);

              //---- GANV01 ----
              GANV01.setComponentPopupMenu(BTD);
              GANV01.setName("GANV01");
              xTitledPanel1ContentContainer.add(GANV01);
              GANV01.setBounds(265, 36, 20, GANV01.getPreferredSize().height);

              //---- GANV02 ----
              GANV02.setComponentPopupMenu(BTD);
              GANV02.setName("GANV02");
              xTitledPanel1ContentContainer.add(GANV02);
              GANV02.setBounds(265, 62, 20, GANV02.getPreferredSize().height);

              //---- GANV03 ----
              GANV03.setComponentPopupMenu(BTD);
              GANV03.setName("GANV03");
              xTitledPanel1ContentContainer.add(GANV03);
              GANV03.setBounds(265, 88, 20, GANV03.getPreferredSize().height);

              //---- GANV04 ----
              GANV04.setComponentPopupMenu(BTD);
              GANV04.setName("GANV04");
              xTitledPanel1ContentContainer.add(GANV04);
              GANV04.setBounds(265, 114, 20, GANV04.getPreferredSize().height);

              //---- GANV05 ----
              GANV05.setComponentPopupMenu(BTD);
              GANV05.setName("GANV05");
              xTitledPanel1ContentContainer.add(GANV05);
              GANV05.setBounds(265, 140, 20, GANV05.getPreferredSize().height);

              //---- GANV06 ----
              GANV06.setComponentPopupMenu(BTD);
              GANV06.setName("GANV06");
              xTitledPanel1ContentContainer.add(GANV06);
              GANV06.setBounds(265, 166, 20, GANV06.getPreferredSize().height);

              //---- GANV07 ----
              GANV07.setComponentPopupMenu(BTD);
              GANV07.setName("GANV07");
              xTitledPanel1ContentContainer.add(GANV07);
              GANV07.setBounds(265, 192, 20, GANV07.getPreferredSize().height);

              //---- GANV08 ----
              GANV08.setComponentPopupMenu(BTD);
              GANV08.setName("GANV08");
              xTitledPanel1ContentContainer.add(GANV08);
              GANV08.setBounds(265, 218, 20, GANV08.getPreferredSize().height);

              //---- GANV09 ----
              GANV09.setComponentPopupMenu(BTD);
              GANV09.setName("GANV09");
              xTitledPanel1ContentContainer.add(GANV09);
              GANV09.setBounds(265, 244, 20, GANV09.getPreferredSize().height);

              //---- GANV10 ----
              GANV10.setComponentPopupMenu(BTD);
              GANV10.setName("GANV10");
              xTitledPanel1ContentContainer.add(GANV10);
              GANV10.setBounds(265, 270, 20, GANV10.getPreferredSize().height);

              //---- GANV11 ----
              GANV11.setComponentPopupMenu(BTD);
              GANV11.setName("GANV11");
              xTitledPanel1ContentContainer.add(GANV11);
              GANV11.setBounds(265, 296, 20, GANV11.getPreferredSize().height);

              //---- GANV12 ----
              GANV12.setComponentPopupMenu(BTD);
              GANV12.setName("GANV12");
              xTitledPanel1ContentContainer.add(GANV12);
              GANV12.setBounds(265, 322, 20, GANV12.getPreferredSize().height);

              //---- GANV13 ----
              GANV13.setComponentPopupMenu(BTD);
              GANV13.setName("GANV13");
              xTitledPanel1ContentContainer.add(GANV13);
              GANV13.setBounds(265, 348, 20, GANV13.getPreferredSize().height);

              //---- GANV14 ----
              GANV14.setComponentPopupMenu(BTD);
              GANV14.setName("GANV14");
              xTitledPanel1ContentContainer.add(GANV14);
              GANV14.setBounds(265, 374, 20, GANV14.getPreferredSize().height);

              //---- GANV15 ----
              GANV15.setComponentPopupMenu(BTD);
              GANV15.setName("GANV15");
              xTitledPanel1ContentContainer.add(GANV15);
              GANV15.setBounds(265, 400, 20, GANV15.getPreferredSize().height);

              //---- OBJ_91 ----
              OBJ_91.setText("01");
              OBJ_91.setName("OBJ_91");
              xTitledPanel1ContentContainer.add(OBJ_91);
              OBJ_91.setBounds(235, 36, 18, 28);

              //---- OBJ_92 ----
              OBJ_92.setText("02");
              OBJ_92.setName("OBJ_92");
              xTitledPanel1ContentContainer.add(OBJ_92);
              OBJ_92.setBounds(235, 62, 18, 28);

              //---- OBJ_93 ----
              OBJ_93.setText("03");
              OBJ_93.setName("OBJ_93");
              xTitledPanel1ContentContainer.add(OBJ_93);
              OBJ_93.setBounds(235, 88, 18, 28);

              //---- OBJ_94 ----
              OBJ_94.setText("04");
              OBJ_94.setName("OBJ_94");
              xTitledPanel1ContentContainer.add(OBJ_94);
              OBJ_94.setBounds(235, 114, 18, 28);

              //---- OBJ_95 ----
              OBJ_95.setText("05");
              OBJ_95.setName("OBJ_95");
              xTitledPanel1ContentContainer.add(OBJ_95);
              OBJ_95.setBounds(235, 140, 18, 28);

              //---- OBJ_96 ----
              OBJ_96.setText("06");
              OBJ_96.setName("OBJ_96");
              xTitledPanel1ContentContainer.add(OBJ_96);
              OBJ_96.setBounds(235, 166, 18, 28);

              //---- OBJ_97 ----
              OBJ_97.setText("07");
              OBJ_97.setName("OBJ_97");
              xTitledPanel1ContentContainer.add(OBJ_97);
              OBJ_97.setBounds(235, 192, 18, 28);

              //---- OBJ_98 ----
              OBJ_98.setText("08");
              OBJ_98.setName("OBJ_98");
              xTitledPanel1ContentContainer.add(OBJ_98);
              OBJ_98.setBounds(235, 218, 18, 28);

              //---- OBJ_99 ----
              OBJ_99.setText("09");
              OBJ_99.setName("OBJ_99");
              xTitledPanel1ContentContainer.add(OBJ_99);
              OBJ_99.setBounds(235, 244, 18, 28);

              //---- OBJ_100 ----
              OBJ_100.setText("10");
              OBJ_100.setName("OBJ_100");
              xTitledPanel1ContentContainer.add(OBJ_100);
              OBJ_100.setBounds(235, 270, 18, 28);

              //---- OBJ_101 ----
              OBJ_101.setText("11");
              OBJ_101.setName("OBJ_101");
              xTitledPanel1ContentContainer.add(OBJ_101);
              OBJ_101.setBounds(235, 296, 18, 28);

              //---- OBJ_102 ----
              OBJ_102.setText("12");
              OBJ_102.setName("OBJ_102");
              xTitledPanel1ContentContainer.add(OBJ_102);
              OBJ_102.setBounds(235, 322, 18, 28);

              //---- OBJ_103 ----
              OBJ_103.setText("13");
              OBJ_103.setName("OBJ_103");
              xTitledPanel1ContentContainer.add(OBJ_103);
              OBJ_103.setBounds(235, 348, 18, 28);

              //---- OBJ_104 ----
              OBJ_104.setText("14");
              OBJ_104.setName("OBJ_104");
              xTitledPanel1ContentContainer.add(OBJ_104);
              OBJ_104.setBounds(235, 374, 18, 28);

              //---- OBJ_105 ----
              OBJ_105.setText("15");
              OBJ_105.setName("OBJ_105");
              xTitledPanel1ContentContainer.add(OBJ_105);
              OBJ_105.setBounds(235, 400, 18, 28);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = xTitledPanel1ContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
                xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
              }
            }
            panel1.add(xTitledPanel1);
            xTitledPanel1.setBounds(12, 55, 753, 464);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 784, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 532, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche2;
  private JLabel OBJ_23;
  private XRiTextField INDSOC;
  private JLabel OBJ_42;
  private XRiTextField INDHIE;
  private XRiTextField INDNIV;
  private XRiTextField INDGAN;
  private RiZoneSortie OBJ_33;
  private JPanel p_tete_gauche;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField GALIB;
  private JLabel OBJ_46;
  private JXTitledPanel xTitledPanel1;
  private RiZoneSortie OBJ_106;
  private RiZoneSortie OBJ_107;
  private RiZoneSortie OBJ_108;
  private RiZoneSortie OBJ_109;
  private RiZoneSortie OBJ_110;
  private RiZoneSortie OBJ_111;
  private RiZoneSortie OBJ_112;
  private RiZoneSortie OBJ_113;
  private RiZoneSortie OBJ_114;
  private RiZoneSortie OBJ_115;
  private RiZoneSortie OBJ_116;
  private RiZoneSortie OBJ_117;
  private RiZoneSortie OBJ_118;
  private RiZoneSortie OBJ_119;
  private RiZoneSortie OBJ_120;
  private JLabel OBJ_49;
  private XRiTextField GACD01;
  private XRiTextField GACD02;
  private XRiTextField GACD03;
  private XRiTextField GACD04;
  private XRiTextField GACD05;
  private XRiTextField GACD06;
  private XRiTextField GACD07;
  private XRiTextField GACD08;
  private XRiTextField GACD09;
  private XRiTextField GACD10;
  private XRiTextField GACD11;
  private XRiTextField GACD12;
  private XRiTextField GACD13;
  private XRiTextField GACD14;
  private XRiTextField GACD15;
  private JLabel OBJ_48;
  private XRiTextField GANV01;
  private XRiTextField GANV02;
  private XRiTextField GANV03;
  private XRiTextField GANV04;
  private XRiTextField GANV05;
  private XRiTextField GANV06;
  private XRiTextField GANV07;
  private XRiTextField GANV08;
  private XRiTextField GANV09;
  private XRiTextField GANV10;
  private XRiTextField GANV11;
  private XRiTextField GANV12;
  private XRiTextField GANV13;
  private XRiTextField GANV14;
  private XRiTextField GANV15;
  private JLabel OBJ_91;
  private JLabel OBJ_92;
  private JLabel OBJ_93;
  private JLabel OBJ_94;
  private JLabel OBJ_95;
  private JLabel OBJ_96;
  private JLabel OBJ_97;
  private JLabel OBJ_98;
  private JLabel OBJ_99;
  private JLabel OBJ_100;
  private JLabel OBJ_101;
  private JLabel OBJ_102;
  private JLabel OBJ_103;
  private JLabel OBJ_104;
  private JLabel OBJ_105;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
