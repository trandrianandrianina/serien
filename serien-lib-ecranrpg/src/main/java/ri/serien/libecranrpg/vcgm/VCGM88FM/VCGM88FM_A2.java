
package ri.serien.libecranrpg.vcgm.VCGM88FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM88FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] V06F_Value = { "-10", "-05", "-03", "-02", "-01", "", "+01", "+02", "+03", "+05", "+10", };
  private String[] V06F_Title = { "Décalage de 10 lignes en moins", "Décalage de 5 lignes en moins", "Décalage de 3 lignes en moins",
      "Décalage de 2 lignes en moins", "Décalage de 1 ligne en moins", "", "Décalage de 1 ligne en plus", "Décalage de 2 lignes en plus",
      "Décalage de 3 lignes en plus", "Décalage de 5 lignes en plus", "Décalage de 10 lignes en plus", };
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", "WTP16", "WTP17", "WTP18", };
  private String[] _WTP01_Title = { "<html><font face=\"Courier New\" color=\"#000000\" size=\"3\"> @HLD01@<br>@HLD02@</font></html>", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, { "LD16", }, { "LD17", }, { "LD18", }, };
  private int[] _WTP01_Width = { 925, };
  
  public VCGM88FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // TODO Icones
    BT_PGFIN.setIcon(lexique.chargerImage("images/pfin20.png", true));
    BT_PGDEB.setIcon(lexique.chargerImage("images/pdeb20.png", true));
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
    V06F.setValeurs(V06F_Value, V06F_Title);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    GAN.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@GAN@")).trim());
    DEMSA2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DEMSA2@")).trim());
    DEMSA1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DEMSA1@")).trim());
    OBJ_30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WFONAF@")).trim());
    DEMETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DEMETB@")).trim());
    DEMDAT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DEMDAT@")).trim());
    HIE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@HIE@")).trim());
    NIVH.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NIVH@")).trim());
    OBJ_44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@*DATE@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MESPRE@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    
    
    HIE.setVisible(lexique.isPresent("HIE"));
    NIVH.setVisible(HIE.isVisible());
    GAN.setVisible(HIE.isVisible());
    OBJ_28.setVisible(HIE.isVisible());
    
    DEMSA1.setVisible(lexique.isPresent("DEMSA1"));
    DEMSA1.setVisible(DEMSA1.isVisible());
    OBJ_27.setVisible(DEMSA1.isVisible());
    OBJ_29.setVisible(DEMSA1.isVisible());
    
    OBJ_30.setVisible(lexique.isPresent("WFONAF"));
    
    // Titre
    // setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - Analyse Budgétaire"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _WTP01_Top, "1", "Enter", e);
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "1", "Enter");
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "2", "Enter");
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void V06FItemStateChanged(ItemEvent e) {
    if (!V06F.getSelectedItem().equals("")) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void bt_DEBActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, "D");
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void bt_FINActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, "F");
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    P_Infos = new JPanel();
    OBJ_28 = new JLabel();
    GAN = new RiZoneSortie();
    OBJ_27 = new JLabel();
    OBJ_29 = new JLabel();
    DEMSA2 = new RiZoneSortie();
    DEMSA1 = new RiZoneSortie();
    OBJ_30 = new JLabel();
    OBJ_31 = new JLabel();
    DEMETB = new RiZoneSortie();
    DEMDAT = new RiZoneSortie();
    HIE = new RiZoneSortie();
    OBJ_26 = new JLabel();
    NIVH = new RiZoneSortie();
    OBJ_44 = new JLabel();
    label1 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    V06F = new XRiComboBox();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGDEB = new JButton();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BT_PGFIN = new JButton();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Affichage de l'analyse budg\u00e9taire");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setPreferredSize(new Dimension(111, 32));
        barre_tete.setMinimumSize(new Dimension(78, 30));
        barre_tete.setName("barre_tete");

        //======== P_Infos ========
        {
          P_Infos.setBorder(null);
          P_Infos.setMinimumSize(new Dimension(66, 22));
          P_Infos.setPreferredSize(new Dimension(66, 40));
          P_Infos.setOpaque(false);
          P_Infos.setName("P_Infos");

          //---- OBJ_28 ----
          OBJ_28.setText("Groupe");
          OBJ_28.setName("OBJ_28");

          //---- GAN ----
          GAN.setComponentPopupMenu(BTD);
          GAN.setOpaque(false);
          GAN.setText("@GAN@");
          GAN.setName("GAN");

          //---- OBJ_27 ----
          OBJ_27.setText("Sec 1");
          OBJ_27.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_27.setName("OBJ_27");

          //---- OBJ_29 ----
          OBJ_29.setText("Sec 2");
          OBJ_29.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_29.setName("OBJ_29");

          //---- DEMSA2 ----
          DEMSA2.setComponentPopupMenu(BTD);
          DEMSA2.setOpaque(false);
          DEMSA2.setText("@DEMSA2@");
          DEMSA2.setName("DEMSA2");

          //---- DEMSA1 ----
          DEMSA1.setComponentPopupMenu(BTD);
          DEMSA1.setOpaque(false);
          DEMSA1.setText("@DEMSA1@");
          DEMSA1.setName("DEMSA1");

          //---- OBJ_30 ----
          OBJ_30.setText("@WFONAF@");
          OBJ_30.setName("OBJ_30");

          //---- OBJ_31 ----
          OBJ_31.setText("Soci\u00e9t\u00e9");
          OBJ_31.setName("OBJ_31");

          //---- DEMETB ----
          DEMETB.setComponentPopupMenu(BTD);
          DEMETB.setOpaque(false);
          DEMETB.setText("@DEMETB@");
          DEMETB.setName("DEMETB");

          //---- DEMDAT ----
          DEMDAT.setComponentPopupMenu(BTD);
          DEMDAT.setOpaque(false);
          DEMDAT.setText("@DEMDAT@");
          DEMDAT.setName("DEMDAT");

          //---- HIE ----
          HIE.setComponentPopupMenu(BTD);
          HIE.setOpaque(false);
          HIE.setText("@HIE@");
          HIE.setName("HIE");

          //---- OBJ_26 ----
          OBJ_26.setText("Date");
          OBJ_26.setName("OBJ_26");

          //---- NIVH ----
          NIVH.setComponentPopupMenu(BTD);
          NIVH.setOpaque(false);
          NIVH.setText("@NIVH@");
          NIVH.setName("NIVH");

          //---- OBJ_44 ----
          OBJ_44.setText("@*DATE@");
          OBJ_44.setName("OBJ_44");

          //---- label1 ----
          label1.setText("@MESPRE@");
          label1.setName("label1");

          GroupLayout P_InfosLayout = new GroupLayout(P_Infos);
          P_Infos.setLayout(P_InfosLayout);
          P_InfosLayout.setHorizontalGroup(
            P_InfosLayout.createParallelGroup()
              .addGroup(P_InfosLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(DEMETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addGroup(P_InfosLayout.createParallelGroup()
                  .addComponent(OBJ_26, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                  .addGroup(P_InfosLayout.createSequentialGroup()
                    .addGap(40, 40, 40)
                    .addComponent(DEMDAT, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)))
                .addGap(8, 8, 8)
                .addGroup(P_InfosLayout.createParallelGroup()
                  .addGroup(P_InfosLayout.createSequentialGroup()
                    .addGap(50, 50, 50)
                    .addComponent(HIE, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addComponent(NIVH, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(GAN, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(DEMSA1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(DEMSA2, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(label1, GroupLayout.PREFERRED_SIZE, 314, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 79, Short.MAX_VALUE)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE))
          );
          P_InfosLayout.setVerticalGroup(
            P_InfosLayout.createParallelGroup()
              .addComponent(DEMETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(DEMDAT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(HIE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(NIVH, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(GAN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(DEMSA1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(DEMSA2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(P_InfosLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addGroup(P_InfosLayout.createParallelGroup()
                  .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_26, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                  .addGroup(P_InfosLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                    .addComponent(label1))))
          );
        }
        barre_tete.add(P_Infos);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1015, 420));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(1025, 420));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- V06F ----
            V06F.setToolTipText("S\u00e9lectionez le nombre de lignes \u00e0 d\u00e9caler, et validez.");
            V06F.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            V06F.setName("V06F");
            V06F.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                V06FItemStateChanged(e);
              }
            });
            panel1.add(V06F);
            V06F.setBounds(720, 355, 225, V06F.getPreferredSize().height);

            //======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST.setViewportView(WTP01);
            }
            panel1.add(SCROLLPANE_LIST);
            SCROLLPANE_LIST.setBounds(20, 20, 925, 330);

            //---- BT_PGDEB ----
            BT_PGDEB.setToolTipText("Affichage au d\u00e9but de la liste");
            BT_PGDEB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDEB.setName("BT_PGDEB");
            BT_PGDEB.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bt_DEBActionPerformed(e);
              }
            });
            panel1.add(BT_PGDEB);
            BT_PGDEB.setBounds(955, 55, 25, 40);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(955, 95, 25, 105);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(955, 200, 25, 105);

            //---- BT_PGFIN ----
            BT_PGFIN.setToolTipText("Affichage \u00e0 la fin de la liste");
            BT_PGFIN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGFIN.setName("BT_PGFIN");
            BT_PGFIN.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bt_FINActionPerformed(e);
              }
            });
            panel1.add(BT_PGFIN);
            BT_PGFIN.setBounds(955, 305, 25, 40);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 987, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 392, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("D\u00e9tail");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_12 ----
      OBJ_12.setText("Budget");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
      BTD.addSeparator();

      //---- OBJ_13 ----
      OBJ_13.setText("Aide en ligne");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel P_Infos;
  private JLabel OBJ_28;
  private RiZoneSortie GAN;
  private JLabel OBJ_27;
  private JLabel OBJ_29;
  private RiZoneSortie DEMSA2;
  private RiZoneSortie DEMSA1;
  private JLabel OBJ_30;
  private JLabel OBJ_31;
  private RiZoneSortie DEMETB;
  private RiZoneSortie DEMDAT;
  private RiZoneSortie HIE;
  private JLabel OBJ_26;
  private RiZoneSortie NIVH;
  private JLabel OBJ_44;
  private JLabel label1;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiComboBox V06F;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JButton BT_PGDEB;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JButton BT_PGFIN;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
