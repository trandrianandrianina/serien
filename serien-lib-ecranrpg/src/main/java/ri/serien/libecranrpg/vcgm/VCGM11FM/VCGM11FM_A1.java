
package ri.serien.libecranrpg.vcgm.VCGM11FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.ODialog;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.autonome.recherchejournaux.GfxRechercheJournaux;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM11FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _WTX01_Top = { "WTX01", "WTX02", "WTX03", "WTX04", "WTX05", "WTX06", "WTX07", "WTX08", "WTX09", "WTX10", "WTX11",
      "WTX12", "WTX13", "WTX14", "WTX15", "WTX16", "WTX17", "WTX18", "WTX19", "WTX20", "WTX21", "WTX22", "WTX23", "WTX24", "WTX25", };
  private String[] _WTX01_Title = { "HLD02", };
  private String[][] _WTX01_Data = { { "LR01", }, { "LR02", }, { "LR03", }, { "LR04", }, { "LR05", }, { "LR06", }, { "LR07", }, { "LR08", },
      { "LR09", }, { "LR10", }, { "LR11", }, { "LR12", }, { "LR13", }, { "LR14", }, { "LR15", }, { "LR16", }, { "LR17", }, { "LR18", },
      { "LR19", }, { "LR20", }, { "LR21", }, { "LR22", }, { "LR23", }, { "LR24", }, { "LR25", }, };
  private int[] _WTX01_Width = { 57, };
  
  // Popup dépendantes
  // private VCGM11CON contrepassation=null;
  
  public ODialog dialog_CON = null;
  public ODialog dialog_JO = null;
  
  public VCGM11FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WIRCHI.setValeursSelection("1", " ");
    WTX01.setAspectTable(_WTX01_Top, _WTX01_Title, _WTX01_Data, _WTX01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    

    
    


    
    
    OBJ_88.setVisible(TOTDB.isVisible());
    OBJ_66.setVisible(WIRCHI.isVisible());
    panel2.setVisible(lexique.isTrue("46"));
    
    

    
    p_bpresentation.setCodeEtablissement(WISOC.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WISOC.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
    // Gestion de la popup sur panel avec buffer commun (à améliorer)
    // if( (dialog_CON != null) && (dialog_CON.getFenetre().isVisible()) )
    // dialog_CON.setData(listeRecord);
  }
  
  @Override
  public void getData() {
    super.getData();
    /*
        for (int i=0; i<9; i++)
    */
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(1, 27);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTX01_Top, "1", "Enter");
    WTX01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    // Recherche de la ligne selectionné dans la liste
    int ligne = WTX01.getSelectedRow();
    if (ligne == -1) {
      return;
    }
    ligne++;
    lexique.addVariableGlobale("VCGM11FM_A1.TopSelected", ligne < 10 ? "WTX0" + ligne : "WTX" + ligne);
    lexique.addVariableGlobale("VCGM11FM_A1.Value4Line", "3");
    WTX01.setValeurTop("3");
    
    // Ouverture de la popup
    if (dialog_CON == null) {
      lexique.addVariableGlobale("VCGM11FMCON.title", "Duplication folio");
      dialog_CON = new ODialog((Window) getTopLevelAncestor(), new VCGM11CON(this));
      // dialog_CON.getFenetre().setTitle("Duplication folio");
      
    }
    dialog_CON.affichePopupPerso();
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    // Recherche de la ligne selectionné dans la liste
    int ligne = WTX01.getSelectedRow();
    if (ligne == -1) {
      return;
    }
    ligne++;
    lexique.addVariableGlobale("VCGM11FM_A1.TopSelected", ligne < 10 ? "WTX0" + ligne : "WTX" + ligne);
    lexique.addVariableGlobale("VCGM11FM_A1.Value4Line", "4");
    WTX01.setValeurTop("4");
    
    if (dialog_CON == null) {
      lexique.addVariableGlobale("VCGM11FMCON.title", "Contrepassation folio");
      dialog_CON = new ODialog((Window) getTopLevelAncestor(), new VCGM11CON(this));
      // dialog_CON.getFenetre().setTitle("Contrepassation folio");
    }
    dialog_CON.affichePopupPerso();
  }
  
  public void actionListe(String val) {
    WTX01.setValeurTop(val);
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTX01_Top, "C", "Enter");
    WTX01.setValeurTop("C");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTX01_Top, "Y", "Enter");
    WTX01.setValeurTop("Y");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTX01_Top, "H", "Enter");
    WTX01.setValeurTop("H");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTX01_Top, "M", "Enter");
    WTX01.setValeurTop("M");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(popupMenu1.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WIRCHIMouseClicked(MouseEvent e) {
    // TODO add your code here
  }
  
  private void WTX01MouseClicked(MouseEvent e) {
    if (WTX01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void OBJ_56ActionPerformed(ActionEvent e) {
    // Ouverture de la popup
    lexique.addVariableGlobale("CODE_SOC", WISOC.getText());
    lexique.addVariableGlobale("ZONE_JO", "WICJO");
    if (dialog_JO == null) {
      // dialog_JO = new oDialog((Window)getTopLevelAncestor(), new GfxRechercheJournaux(this, true,
      // RechercheJournaux.TYPE_ANALYTIQUE));
      dialog_JO = new ODialog((Window) getTopLevelAncestor(), new GfxRechercheJournaux(this));
    }
    dialog_JO.affichePopupPerso();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_49 = new JLabel();
    WISOC = new XRiTextField();
    OBJ_56 = new JButton();
    WICJO = new XRiTextField();
    OBJ_57 = new JLabel();
    WICFO = new XRiTextField();
    OBJ_58 = new JLabel();
    WIDTEX = new XRiCalendrier();
    OBJ_59 = new JLabel();
    WCET = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    SCROLLPANE_LISTA = new JScrollPane();
    WTX01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    panel3 = new JPanel();
    OBJ_88 = new JLabel();
    TOTDB = new XRiTextField();
    TOTCR = new XRiTextField();
    OBJ_66 = new JLabel();
    WIRCHI = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    OBJ_15 = new JMenuItem();
    OBJ_21 = new JMenuItem();
    OBJ_22 = new JMenuItem();
    OBJ_23 = new JMenuItem();
    OBJ_24 = new JMenuItem();
    popupMenu1 = new JPopupMenu();
    OBJ_26 = new JMenuItem();
    OBJ_25 = new JMenuItem();
    popupMenu2 = new JPopupMenu();
    OBJ_27 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Folio de comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 40));
          p_tete_gauche.setMinimumSize(new Dimension(900, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_49 ----
          OBJ_49.setText("Soci\u00e9t\u00e9");
          OBJ_49.setName("OBJ_49");

          //---- WISOC ----
          WISOC.setComponentPopupMenu(popupMenu2);
          WISOC.setName("WISOC");

          //---- OBJ_56 ----
          OBJ_56.setText("Journal");
          OBJ_56.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_56.setToolTipText("Recherche d'un journal");
          OBJ_56.setName("OBJ_56");
          OBJ_56.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_56ActionPerformed(e);
            }
          });

          //---- WICJO ----
          WICJO.setComponentPopupMenu(popupMenu1);
          WICJO.setName("WICJO");

          //---- OBJ_57 ----
          OBJ_57.setText("Folio");
          OBJ_57.setName("OBJ_57");

          //---- WICFO ----
          WICFO.setComponentPopupMenu(popupMenu2);
          WICFO.setName("WICFO");

          //---- OBJ_58 ----
          OBJ_58.setText("Date");
          OBJ_58.setName("OBJ_58");

          //---- WIDTEX ----
          WIDTEX.setComponentPopupMenu(popupMenu2);
          WIDTEX.setName("WIDTEX");

          //---- OBJ_59 ----
          OBJ_59.setText("Type");
          OBJ_59.setName("OBJ_59");

          //---- WCET ----
          WCET.setComponentPopupMenu(popupMenu1);
          WCET.setName("WCET");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(WISOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(WICJO, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21)
                .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(WICFO, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(WIDTEX, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(WCET, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(250, 250, 250))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(WISOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_56)
              .addComponent(WICJO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WICFO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WIDTEX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WCET, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Recherche des folios");
              riSousMenu_bt6.setToolTipText("Recherche des folios");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Totalisation");
              riSousMenu_bt7.setToolTipText("Totalisation");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(850, 550));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Liste des folios"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //======== SCROLLPANE_LISTA ========
            {
              SCROLLPANE_LISTA.setComponentPopupMenu(BTD);
              SCROLLPANE_LISTA.setName("SCROLLPANE_LISTA");

              //---- WTX01 ----
              WTX01.setComponentPopupMenu(BTD);
              WTX01.setName("WTX01");
              WTX01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTX01MouseClicked(e);
                }
              });
              SCROLLPANE_LISTA.setViewportView(WTX01);
            }
            panel2.add(SCROLLPANE_LISTA);
            SCROLLPANE_LISTA.setBounds(25, 40, 740, 430);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel2.add(BT_PGUP);
            BT_PGUP.setBounds(775, 35, 25, 190);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel2.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(775, 280, 25, 190);

            //======== panel3 ========
            {
              panel3.setOpaque(false);
              panel3.setName("panel3");

              //---- OBJ_88 ----
              OBJ_88.setText("Totaux");
              OBJ_88.setFont(OBJ_88.getFont().deriveFont(OBJ_88.getFont().getSize() + 3f));
              OBJ_88.setName("OBJ_88");

              //---- TOTDB ----
              TOTDB.setName("TOTDB");

              //---- TOTCR ----
              TOTCR.setName("TOTCR");

              GroupLayout panel3Layout = new GroupLayout(panel3);
              panel3.setLayout(panel3Layout);
              panel3Layout.setHorizontalGroup(
                panel3Layout.createParallelGroup()
                  .addGroup(panel3Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_88, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE)
                    .addGap(8, 8, 8)
                    .addComponent(TOTDB, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(TOTCR, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                    .addGap(322, 322, 322))
              );
              panel3Layout.setVerticalGroup(
                panel3Layout.createParallelGroup()
                  .addGroup(panel3Layout.createSequentialGroup()
                    .addGap(9, 9, 9)
                    .addComponent(OBJ_88))
                  .addGroup(panel3Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(TOTDB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel3Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(TOTCR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              );
            }
            panel2.add(panel3);
            panel3.setBounds(25, 475, 320, 35);

            //---- OBJ_66 ----
            OBJ_66.setText("(non homologu\u00e9s");
            OBJ_66.setName("OBJ_66");
            panel2.add(OBJ_66);
            OBJ_66.setBounds(470, 480, 127, 20);

            //---- WIRCHI ----
            WIRCHI.setText(")");
            WIRCHI.setToolTipText("Il faut double-cliquer pour que votre demande soit prise en compte.");
            WIRCHI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WIRCHI.setName("WIRCHI");
            WIRCHI.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                WIRCHIMouseClicked(e);
              }
            });
            panel2.add(WIRCHI);
            WIRCHI.setBounds(604, 480, 27, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 824, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 522, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choisir");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("Dupliquer");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);

      //---- OBJ_15 ----
      OBJ_15.setText("Contrepasser");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);

      //---- OBJ_21 ----
      OBJ_21.setText("Regroupement des \u00e9critures");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);

      //---- OBJ_22 ----
      OBJ_22.setText("Exportation folio vers tableur");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);

      //---- OBJ_23 ----
      OBJ_23.setText("Historique folio");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD.add(OBJ_23);

      //---- OBJ_24 ----
      OBJ_24.setText("Modification de l'indicatif du folio");
      OBJ_24.setName("OBJ_24");
      OBJ_24.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_24ActionPerformed(e);
        }
      });
      BTD.add(OBJ_24);
    }

    //======== popupMenu1 ========
    {
      popupMenu1.setName("popupMenu1");

      //---- OBJ_26 ----
      OBJ_26.setText("Choix possibles");
      OBJ_26.setName("OBJ_26");
      OBJ_26.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_26ActionPerformed(e);
        }
      });
      popupMenu1.add(OBJ_26);

      //---- OBJ_25 ----
      OBJ_25.setText("Aide en ligne");
      OBJ_25.setName("OBJ_25");
      OBJ_25.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_25ActionPerformed(e);
        }
      });
      popupMenu1.add(OBJ_25);
    }

    //======== popupMenu2 ========
    {
      popupMenu2.setName("popupMenu2");

      //---- OBJ_27 ----
      OBJ_27.setText("Aide en ligne");
      OBJ_27.setName("OBJ_27");
      OBJ_27.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_25ActionPerformed(e);
        }
      });
      popupMenu2.add(OBJ_27);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_49;
  private XRiTextField WISOC;
  private JButton OBJ_56;
  private XRiTextField WICJO;
  private JLabel OBJ_57;
  private XRiTextField WICFO;
  private JLabel OBJ_58;
  private XRiCalendrier WIDTEX;
  private JLabel OBJ_59;
  private XRiTextField WCET;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel2;
  private JScrollPane SCROLLPANE_LISTA;
  private XRiTable WTX01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPanel panel3;
  private JLabel OBJ_88;
  private XRiTextField TOTDB;
  private XRiTextField TOTCR;
  private JLabel OBJ_66;
  private XRiCheckBox WIRCHI;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_15;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_24;
  private JPopupMenu popupMenu1;
  private JMenuItem OBJ_26;
  private JMenuItem OBJ_25;
  private JPopupMenu popupMenu2;
  private JMenuItem OBJ_27;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
