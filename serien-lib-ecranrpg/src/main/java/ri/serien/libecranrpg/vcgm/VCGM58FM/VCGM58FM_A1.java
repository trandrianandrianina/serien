
package ri.serien.libecranrpg.vcgm.VCGM58FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM58FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM58FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TIDXC.setValeurs("12", "RB");
    TIDXB.setValeurs("11", "RB");
    TIDX7.setValeurs("7", "RB");
    TIDX9.setValeurs("9", "RB");
    TIDX8.setValeurs("8", "RB");
    TIDX6.setValeurs("6", "RB");
    TIDX4.setValeurs("4", "RB");
    TIDX3.setValeurs("3", "RB");
    TIDXA.setValeurs("10", "RB");
    TIDX2.setValeurs("2", "RB");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Relances @LIBSEL@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    // TIDXC.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("12"));
    // TIDXB.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("11"));
    // TIDX7.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("7"));
    // TIDX9.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("9"));
    // TIDX8.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("8"));
    // TIDX6.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("6"));
    // TIDX4.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("4"));
    // TIDX3.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("3"));
    // TIDXA.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("10"));
    // TIDX2.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("2"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  Fichier relance @LIBSEL@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (TIDXC.isSelected())
    // lexique.HostFieldPutData("RB", 0, "12");
    // if (TIDXB.isSelected())
    // lexique.HostFieldPutData("RB", 0, "11");
    // if (TIDX7.isSelected())
    // lexique.HostFieldPutData("RB", 0, "7");
    // if (TIDX9.isSelected())
    // lexique.HostFieldPutData("RB", 0, "9");
    // if (TIDX8.isSelected())
    // lexique.HostFieldPutData("RB", 0, "8");
    // if (TIDX6.isSelected())
    // lexique.HostFieldPutData("RB", 0, "6");
    // if (TIDX4.isSelected())
    // lexique.HostFieldPutData("RB", 0, "4");
    // if (TIDX3.isSelected())
    // lexique.HostFieldPutData("RB", 0, "3");
    // if (TIDXA.isSelected())
    // lexique.HostFieldPutData("RB", 0, "10");
    // if (TIDX2.isSelected())
    // lexique.HostFieldPutData("RB", 0, "2");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_36_OBJ_36 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_37_OBJ_37 = new JLabel();
    RLDTRX = new XRiCalendrier();
    OBJ_38_OBJ_38 = new JLabel();
    NCGX = new XRiTextField();
    OBJ_39_OBJ_39 = new JLabel();
    INDNCA = new XRiTextField();
    OBJ_40_OBJ_40 = new JLabel();
    INDPCE = new XRiTextField();
    OBJ_41_OBJ_41 = new JLabel();
    INDRAN = new XRiTextField();
    OBJ_42_OBJ_42 = new JLabel();
    WETAT = new XRiTextField();
    OBJ_43_OBJ_43 = new JLabel();
    WGES = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    TIDX2 = new XRiRadioButton();
    ARG2 = new XRiTextField();
    TIDXA = new XRiRadioButton();
    ARGA = new XRiTextField();
    TIDX3 = new XRiRadioButton();
    ARG3 = new XRiTextField();
    TIDX4 = new XRiRadioButton();
    ARG4 = new XRiTextField();
    TIDX6 = new XRiRadioButton();
    ARG6 = new XRiTextField();
    TIDX8 = new XRiRadioButton();
    ARG8 = new XRiTextField();
    TIDX9 = new XRiRadioButton();
    ARG9 = new XRiTextField();
    TIDX7 = new XRiRadioButton();
    ARG7 = new XRiTextField();
    OBJ_78_OBJ_78 = new JLabel();
    FACFX = new XRiTextField();
    TIDXB = new XRiRadioButton();
    ARGL = new XRiTextField();
    TIDXC = new XRiRadioButton();
    ARGM = new XRiTextField();
    RAPFX = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_16 = new JMenuItem();
    OBJ_15 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Relances @LIBSEL@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(950, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_36_OBJ_36 ----
          OBJ_36_OBJ_36.setText("Soci\u00e9t\u00e9");
          OBJ_36_OBJ_36.setName("OBJ_36_OBJ_36");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setName("INDETB");

          //---- OBJ_37_OBJ_37 ----
          OBJ_37_OBJ_37.setText("Date relance");
          OBJ_37_OBJ_37.setName("OBJ_37_OBJ_37");

          //---- RLDTRX ----
          RLDTRX.setComponentPopupMenu(null);
          RLDTRX.setName("RLDTRX");

          //---- OBJ_38_OBJ_38 ----
          OBJ_38_OBJ_38.setText("Collectif");
          OBJ_38_OBJ_38.setName("OBJ_38_OBJ_38");

          //---- NCGX ----
          NCGX.setComponentPopupMenu(null);
          NCGX.setName("NCGX");

          //---- OBJ_39_OBJ_39 ----
          OBJ_39_OBJ_39.setText("Auxiliaire");
          OBJ_39_OBJ_39.setName("OBJ_39_OBJ_39");

          //---- INDNCA ----
          INDNCA.setComponentPopupMenu(null);
          INDNCA.setName("INDNCA");

          //---- OBJ_40_OBJ_40 ----
          OBJ_40_OBJ_40.setText("Pi\u00e8ce");
          OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");

          //---- INDPCE ----
          INDPCE.setComponentPopupMenu(null);
          INDPCE.setName("INDPCE");

          //---- OBJ_41_OBJ_41 ----
          OBJ_41_OBJ_41.setText("R\u00e8glement");
          OBJ_41_OBJ_41.setName("OBJ_41_OBJ_41");

          //---- INDRAN ----
          INDRAN.setComponentPopupMenu(null);
          INDRAN.setName("INDRAN");

          //---- OBJ_42_OBJ_42 ----
          OBJ_42_OBJ_42.setText("S");
          OBJ_42_OBJ_42.setName("OBJ_42_OBJ_42");

          //---- WETAT ----
          WETAT.setToolTipText("Filtre / s\u00e9lection (F4 pour liste)");
          WETAT.setComponentPopupMenu(null);
          WETAT.setName("WETAT");

          //---- OBJ_43_OBJ_43 ----
          OBJ_43_OBJ_43.setText("Rc");
          OBJ_43_OBJ_43.setName("OBJ_43_OBJ_43");

          //---- WGES ----
          WGES.setToolTipText("Responsable du compte dans l'entreprise");
          WGES.setComponentPopupMenu(null);
          WGES.setName("WGES");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_36_OBJ_36, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_37_OBJ_37, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(RLDTRX, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(50, 50, 50)
                    .addComponent(NCGX, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_38_OBJ_38, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(OBJ_39_OBJ_39, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INDNCA, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_40_OBJ_40, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDPCE, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(OBJ_41_OBJ_41, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDRAN, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_42_OBJ_42, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(WETAT, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_43_OBJ_43, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(WGES, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(RLDTRX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(NCGX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDNCA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDPCE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDRAN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WETAT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WGES, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_36_OBJ_36)
                  .addComponent(OBJ_37_OBJ_37)
                  .addComponent(OBJ_38_OBJ_38)
                  .addComponent(OBJ_39_OBJ_39)
                  .addComponent(OBJ_40_OBJ_40)
                  .addComponent(OBJ_41_OBJ_41)
                  .addComponent(OBJ_42_OBJ_42)
                  .addComponent(OBJ_43_OBJ_43)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(620, 440));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Recherche multi-crit\u00e8res sur historique relance"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- TIDX2 ----
            TIDX2.setText("Recherche alphab\u00e9tique 1");
            TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX2.setName("TIDX2");
            panel1.add(TIDX2);
            TIDX2.setBounds(43, 52, 220, 20);

            //---- ARG2 ----
            ARG2.setComponentPopupMenu(null);
            ARG2.setName("ARG2");
            panel1.add(ARG2);
            ARG2.setBounds(264, 48, 160, ARG2.getPreferredSize().height);

            //---- TIDXA ----
            TIDXA.setText("Recherche alphab\u00e9tique 2");
            TIDXA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDXA.setName("TIDXA");
            panel1.add(TIDXA);
            TIDXA.setBounds(43, 86, 220, 20);

            //---- ARGA ----
            ARGA.setComponentPopupMenu(null);
            ARGA.setName("ARGA");
            panel1.add(ARGA);
            ARGA.setBounds(264, 82, 160, ARGA.getPreferredSize().height);

            //---- TIDX3 ----
            TIDX3.setText("Code postal");
            TIDX3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX3.setName("TIDX3");
            panel1.add(TIDX3);
            TIDX3.setBounds(43, 120, 220, 20);

            //---- ARG3 ----
            ARG3.setComponentPopupMenu(null);
            ARG3.setName("ARG3");
            panel1.add(ARG3);
            ARG3.setBounds(264, 116, 52, ARG3.getPreferredSize().height);

            //---- TIDX4 ----
            TIDX4.setText("Code repr\u00e9sentant");
            TIDX4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX4.setName("TIDX4");
            panel1.add(TIDX4);
            TIDX4.setBounds(43, 154, 220, 20);

            //---- ARG4 ----
            ARG4.setComponentPopupMenu(null);
            ARG4.setName("ARG4");
            panel1.add(ARG4);
            ARG4.setBounds(264, 150, 30, ARG4.getPreferredSize().height);

            //---- TIDX6 ----
            TIDX6.setText("Date d'\u00e9ch\u00e9ance");
            TIDX6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX6.setName("TIDX6");
            panel1.add(TIDX6);
            TIDX6.setBounds(43, 188, 220, 20);

            //---- ARG6 ----
            ARG6.setComponentPopupMenu(null);
            ARG6.setName("ARG6");
            panel1.add(ARG6);
            ARG6.setBounds(264, 184, 76, ARG6.getPreferredSize().height);

            //---- TIDX8 ----
            TIDX8.setText("Niveau de relance");
            TIDX8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX8.setName("TIDX8");
            panel1.add(TIDX8);
            TIDX8.setBounds(43, 222, 220, 20);

            //---- ARG8 ----
            ARG8.setComponentPopupMenu(null);
            ARG8.setName("ARG8");
            panel1.add(ARG8);
            ARG8.setBounds(264, 218, 20, ARG8.getPreferredSize().height);

            //---- TIDX9 ----
            TIDX9.setText("Montant");
            TIDX9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX9.setName("TIDX9");
            panel1.add(TIDX9);
            TIDX9.setBounds(43, 256, 220, 20);

            //---- ARG9 ----
            ARG9.setComponentPopupMenu(null);
            ARG9.setName("ARG9");
            panel1.add(ARG9);
            ARG9.setBounds(264, 252, 108, ARG9.getPreferredSize().height);

            //---- TIDX7 ----
            TIDX7.setText("Date de facture d\u00e9but/fin");
            TIDX7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX7.setName("TIDX7");
            panel1.add(TIDX7);
            TIDX7.setBounds(43, 290, 220, 20);

            //---- ARG7 ----
            ARG7.setComponentPopupMenu(null);
            ARG7.setName("ARG7");
            panel1.add(ARG7);
            ARG7.setBounds(264, 286, 76, ARG7.getPreferredSize().height);

            //---- OBJ_78_OBJ_78 ----
            OBJ_78_OBJ_78.setText("\u00e0");
            OBJ_78_OBJ_78.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_78_OBJ_78.setName("OBJ_78_OBJ_78");
            panel1.add(OBJ_78_OBJ_78);
            OBJ_78_OBJ_78.setBounds(343, 290, 15, 20);

            //---- FACFX ----
            FACFX.setComponentPopupMenu(null);
            FACFX.setName("FACFX");
            panel1.add(FACFX);
            FACFX.setBounds(363, 286, 76, FACFX.getPreferredSize().height);

            //---- TIDXB ----
            TIDXB.setText("Recherche par litige");
            TIDXB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDXB.setName("TIDXB");
            panel1.add(TIDXB);
            TIDXB.setBounds(43, 324, 220, 20);

            //---- ARGL ----
            ARGL.setComponentPopupMenu(null);
            ARGL.setName("ARGL");
            panel1.add(ARGL);
            ARGL.setBounds(264, 320, 60, ARGL.getPreferredSize().height);

            //---- TIDXC ----
            TIDXC.setText("A rappeler le");
            TIDXC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDXC.setName("TIDXC");
            panel1.add(TIDXC);
            TIDXC.setBounds(43, 358, 220, 20);

            //---- ARGM ----
            ARGM.setComponentPopupMenu(null);
            ARGM.setName("ARGM");
            panel1.add(ARGM);
            ARGM.setBounds(263, 354, 76, ARGM.getPreferredSize().height);

            //---- RAPFX ----
            RAPFX.setComponentPopupMenu(null);
            RAPFX.setName("RAPFX");
            panel1.add(RAPFX);
            RAPFX.setBounds(363, 354, 76, RAPFX.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 594, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 412, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_16 ----
      OBJ_16.setText("Choix possibles");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);

      //---- OBJ_15 ----
      OBJ_15.setText("Aide en ligne");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);
    }

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(TIDX2);
    RB_GRP.add(TIDXA);
    RB_GRP.add(TIDX3);
    RB_GRP.add(TIDX4);
    RB_GRP.add(TIDX6);
    RB_GRP.add(TIDX8);
    RB_GRP.add(TIDX9);
    RB_GRP.add(TIDX7);
    RB_GRP.add(TIDXB);
    RB_GRP.add(TIDXC);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_36_OBJ_36;
  private XRiTextField INDETB;
  private JLabel OBJ_37_OBJ_37;
  private XRiCalendrier RLDTRX;
  private JLabel OBJ_38_OBJ_38;
  private XRiTextField NCGX;
  private JLabel OBJ_39_OBJ_39;
  private XRiTextField INDNCA;
  private JLabel OBJ_40_OBJ_40;
  private XRiTextField INDPCE;
  private JLabel OBJ_41_OBJ_41;
  private XRiTextField INDRAN;
  private JLabel OBJ_42_OBJ_42;
  private XRiTextField WETAT;
  private JLabel OBJ_43_OBJ_43;
  private XRiTextField WGES;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiRadioButton TIDX2;
  private XRiTextField ARG2;
  private XRiRadioButton TIDXA;
  private XRiTextField ARGA;
  private XRiRadioButton TIDX3;
  private XRiTextField ARG3;
  private XRiRadioButton TIDX4;
  private XRiTextField ARG4;
  private XRiRadioButton TIDX6;
  private XRiTextField ARG6;
  private XRiRadioButton TIDX8;
  private XRiTextField ARG8;
  private XRiRadioButton TIDX9;
  private XRiTextField ARG9;
  private XRiRadioButton TIDX7;
  private XRiTextField ARG7;
  private JLabel OBJ_78_OBJ_78;
  private XRiTextField FACFX;
  private XRiRadioButton TIDXB;
  private XRiTextField ARGL;
  private XRiRadioButton TIDXC;
  private XRiTextField ARGM;
  private XRiTextField RAPFX;
  private JPopupMenu BTD;
  private JMenuItem OBJ_16;
  private JMenuItem OBJ_15;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
