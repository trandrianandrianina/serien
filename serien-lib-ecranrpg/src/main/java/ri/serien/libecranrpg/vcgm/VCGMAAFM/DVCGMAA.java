
package ri.serien.libecranrpg.vcgm.VCGMAAFM;
// Nom Fichier: pop_null_DVCGMAA.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;

import org.jdesktop.swingx.JXTitledSeparator;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class DVCGMAA extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public DVCGMAA(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Duplication"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_8 = new JLabel();
    OBJ_9 = new JButton();
    OBJ_10 = new JButton();
    IND3 = new XRiTextField();
    IND = new XRiTextField();
    OBJ_19 = new JLabel();
    ETB3 = new XRiTextField();
    ETB = new XRiTextField();
    OBJ_7 = new JLabel();
    OBJ_17 = new JLabel();
    TYP = new XRiTextField();
    TYP3 = new XRiTextField();
    OBJ_18 = new JLabel();
    xTitledSeparator1 = new JXTitledSeparator();

    //======== this ========
    setName("this");
    setLayout(null);

    //---- OBJ_8 ----
    OBJ_8.setText("Par duplication de");
    OBJ_8.setName("OBJ_8");
    add(OBJ_8);
    OBJ_8.setBounds(10, 90, 111, 20);

    //---- OBJ_9 ----
    OBJ_9.setText("OK");
    OBJ_9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_9.setName("OBJ_9");
    OBJ_9.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_9ActionPerformed(e);
      }
    });
    add(OBJ_9);
    OBJ_9.setBounds(155, 120, 82, 24);

    //---- OBJ_10 ----
    OBJ_10.setText("Annuler");
    OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_10.setName("OBJ_10");
    OBJ_10.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_10ActionPerformed(e);
      }
    });
    add(OBJ_10);
    OBJ_10.setBounds(240, 120, 82, 24);

    //---- IND3 ----
    IND3.setName("IND3");
    add(IND3);
    IND3.setBounds(220, 85, 60, IND3.getPreferredSize().height);

    //---- IND ----
    IND.setName("IND");
    add(IND);
    IND.setBounds(220, 50, 60, IND.getPreferredSize().height);

    //---- OBJ_19 ----
    OBJ_19.setText("Ordre");
    OBJ_19.setName("OBJ_19");
    add(OBJ_19);
    OBJ_19.setBounds(220, 30, 60, 16);

    //---- ETB3 ----
    ETB3.setName("ETB3");
    add(ETB3);
    ETB3.setBounds(135, 85, 39, ETB3.getPreferredSize().height);

    //---- ETB ----
    ETB.setName("ETB");
    add(ETB);
    ETB.setBounds(135, 50, 40, ETB.getPreferredSize().height);

    //---- OBJ_7 ----
    OBJ_7.setText("Fiche");
    OBJ_7.setName("OBJ_7");
    add(OBJ_7);
    OBJ_7.setBounds(20, 50, 36, 20);

    //---- OBJ_17 ----
    OBJ_17.setText("Soc");
    OBJ_17.setName("OBJ_17");
    add(OBJ_17);
    OBJ_17.setBounds(140, 30, 39, 16);

    //---- TYP ----
    TYP.setName("TYP");
    add(TYP);
    TYP.setBounds(180, 50, 30, TYP.getPreferredSize().height);

    //---- TYP3 ----
    TYP3.setName("TYP3");
    add(TYP3);
    TYP3.setBounds(180, 85, 30, TYP3.getPreferredSize().height);

    //---- OBJ_18 ----
    OBJ_18.setText("Cd");
    OBJ_18.setName("OBJ_18");
    add(OBJ_18);
    OBJ_18.setBounds(185, 30, 24, 16);

    //---- xTitledSeparator1 ----
    xTitledSeparator1.setTitle("Source et destination");
    xTitledSeparator1.setName("xTitledSeparator1");
    add(xTitledSeparator1);
    xTitledSeparator1.setBounds(10, 10, 315, xTitledSeparator1.getPreferredSize().height);

    setPreferredSize(new Dimension(330, 155));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel OBJ_8;
  private JButton OBJ_9;
  private JButton OBJ_10;
  private XRiTextField IND3;
  private XRiTextField IND;
  private JLabel OBJ_19;
  private XRiTextField ETB3;
  private XRiTextField ETB;
  private JLabel OBJ_7;
  private JLabel OBJ_17;
  private XRiTextField TYP;
  private XRiTextField TYP3;
  private JLabel OBJ_18;
  private JXTitledSeparator xTitledSeparator1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
