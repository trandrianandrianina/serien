
package ri.serien.libecranrpg.vcgm.VCGMG2FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGMG2FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGMG2FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_38.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PG01LI@")).trim());
    label41.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPAGE@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    xTitledPanel1.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBFCH@")).trim());
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDL01@")).trim());
    label3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDN01@")).trim());
    label4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDL02@")).trim());
    label5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDL03@")).trim());
    label6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDL04@")).trim());
    label7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDL05@")).trim());
    label8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDL06@")).trim());
    label9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDL07@")).trim());
    label10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDL08@")).trim());
    label21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDL09@")).trim());
    label22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDL10@")).trim());
    label23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDN02@")).trim());
    label24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDN03@")).trim());
    label25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDN04@")).trim());
    label26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDN05@")).trim());
    label27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDN06@")).trim());
    label28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDN07@")).trim());
    label29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDN08@")).trim());
    label30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDN09@")).trim());
    label31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDN10@")).trim());
    label43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDL11@")).trim());
    label44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDN11@")).trim());
    label45.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDL12@")).trim());
    label46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDL13@")).trim());
    label47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDL14@")).trim());
    label48.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDL15@")).trim());
    label49.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDL16@")).trim());
    label50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDL17@")).trim());
    label51.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDL18@")).trim());
    label52.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDL19@")).trim());
    label53.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDL20@")).trim());
    label54.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDN12@")).trim());
    label55.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDN13@")).trim());
    label56.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDN14@")).trim());
    label57.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDN15@")).trim());
    label58.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDN16@")).trim());
    label59.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDN17@")).trim());
    label60.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDN18@")).trim());
    label61.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDN19@")).trim());
    label62.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDN20@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    label41.setVisible(lexique.isPresent("WPAGE"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_55ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_44 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46 = new JLabel();
    INDIND = new XRiTextField();
    OBJ_38 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    label41 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    TITZ = new XRiTextField();
    OBJ_40 = new JLabel();
    separator1 = compFactory.createSeparator("Repr\u00e9sentation de la ligne");
    panel1 = new JPanel();
    label11 = new JLabel();
    label12 = new JLabel();
    label13 = new JLabel();
    label14 = new JLabel();
    label15 = new JLabel();
    label16 = new JLabel();
    label17 = new JLabel();
    label18 = new JLabel();
    label19 = new JLabel();
    label1 = new JLabel();
    DLDZ01 = new XRiTextField();
    DLDZ02 = new XRiTextField();
    DLDZ03 = new XRiTextField();
    DLDZ04 = new XRiTextField();
    DLDZ05 = new XRiTextField();
    DLDZ06 = new XRiTextField();
    DLDZ07 = new XRiTextField();
    DLDZ08 = new XRiTextField();
    DLDZ09 = new XRiTextField();
    DLDZ10 = new XRiTextField();
    DLDD01 = new XRiTextField();
    DLDF01 = new XRiTextField();
    label2 = new RiZoneSortie();
    label3 = new RiZoneSortie();
    DLDD02 = new XRiTextField();
    DLDD03 = new XRiTextField();
    DLDD04 = new XRiTextField();
    DLDD05 = new XRiTextField();
    DLDD06 = new XRiTextField();
    DLDD07 = new XRiTextField();
    DLDD08 = new XRiTextField();
    DLDD09 = new XRiTextField();
    DLDD10 = new XRiTextField();
    DLDF02 = new XRiTextField();
    DLDF03 = new XRiTextField();
    DLDF04 = new XRiTextField();
    DLDF05 = new XRiTextField();
    DLDF06 = new XRiTextField();
    DLDF07 = new XRiTextField();
    DLDF08 = new XRiTextField();
    DLDF09 = new XRiTextField();
    DLDF10 = new XRiTextField();
    label4 = new RiZoneSortie();
    label5 = new RiZoneSortie();
    label6 = new RiZoneSortie();
    label7 = new RiZoneSortie();
    label8 = new RiZoneSortie();
    label9 = new RiZoneSortie();
    label10 = new RiZoneSortie();
    label21 = new RiZoneSortie();
    label22 = new RiZoneSortie();
    label23 = new RiZoneSortie();
    label24 = new RiZoneSortie();
    label25 = new RiZoneSortie();
    label26 = new RiZoneSortie();
    label27 = new RiZoneSortie();
    label28 = new RiZoneSortie();
    label29 = new RiZoneSortie();
    label30 = new RiZoneSortie();
    label31 = new RiZoneSortie();
    DLDZ11 = new XRiTextField();
    DLDZ12 = new XRiTextField();
    DLDZ13 = new XRiTextField();
    DLDZ14 = new XRiTextField();
    DLDZ15 = new XRiTextField();
    DLDZ16 = new XRiTextField();
    DLDZ17 = new XRiTextField();
    DLDZ18 = new XRiTextField();
    DLDZ19 = new XRiTextField();
    DLDZ20 = new XRiTextField();
    DLDD11 = new XRiTextField();
    DLDF11 = new XRiTextField();
    label43 = new RiZoneSortie();
    label44 = new RiZoneSortie();
    DLDD12 = new XRiTextField();
    DLDD13 = new XRiTextField();
    DLDD14 = new XRiTextField();
    DLDD15 = new XRiTextField();
    DLDD16 = new XRiTextField();
    DLDD17 = new XRiTextField();
    DLDD18 = new XRiTextField();
    DLDD19 = new XRiTextField();
    DLDD20 = new XRiTextField();
    DLDF12 = new XRiTextField();
    DLDF13 = new XRiTextField();
    DLDF14 = new XRiTextField();
    DLDF15 = new XRiTextField();
    DLDF16 = new XRiTextField();
    DLDF17 = new XRiTextField();
    DLDF18 = new XRiTextField();
    DLDF19 = new XRiTextField();
    DLDF20 = new XRiTextField();
    label45 = new RiZoneSortie();
    label46 = new RiZoneSortie();
    label47 = new RiZoneSortie();
    label48 = new RiZoneSortie();
    label49 = new RiZoneSortie();
    label50 = new RiZoneSortie();
    label51 = new RiZoneSortie();
    label52 = new RiZoneSortie();
    label53 = new RiZoneSortie();
    label54 = new RiZoneSortie();
    label55 = new RiZoneSortie();
    label56 = new RiZoneSortie();
    label57 = new RiZoneSortie();
    label58 = new RiZoneSortie();
    label59 = new RiZoneSortie();
    label60 = new RiZoneSortie();
    label61 = new RiZoneSortie();
    label62 = new RiZoneSortie();
    label20 = new JLabel();
    label32 = new JLabel();
    label33 = new JLabel();
    label34 = new JLabel();
    label35 = new JLabel();
    label36 = new JLabel();
    label37 = new JLabel();
    label38 = new JLabel();
    label39 = new JLabel();
    label40 = new JLabel();
    OBJ_54 = new JLabel();
    OBJ_56 = new JLabel();
    OBJ_57 = new JLabel();
    OBJ_58 = new JLabel();
    OBJ_59 = new JLabel();
    OBJ_60 = new JLabel();
    OBJ_61 = new JLabel();
    OBJ_62 = new JLabel();
    OBJ_63 = new JLabel();
    OBJ_64 = new JLabel();
    OBJ_55 = new JButton();
    V06F = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation des \u00e9tats de comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_42 ----
          OBJ_42.setText("Soci\u00e9t\u00e9");
          OBJ_42.setName("OBJ_42");
          p_tete_gauche.add(OBJ_42);
          OBJ_42.setBounds(5, 5, 52, 18);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(80, 0, 40, INDETB.getPreferredSize().height);

          //---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");
          p_tete_gauche.add(OBJ_44);
          OBJ_44.setBounds(150, 5, 36, 18);

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setName("INDTYP");
          p_tete_gauche.add(INDTYP);
          INDTYP.setBounds(200, 0, 30, INDTYP.getPreferredSize().height);

          //---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");
          p_tete_gauche.add(OBJ_46);
          OBJ_46.setBounds(260, 5, 39, 18);

          //---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setName("INDIND");
          p_tete_gauche.add(INDIND);
          INDIND.setBounds(310, 0, 60, INDIND.getPreferredSize().height);

          //---- OBJ_38 ----
          OBJ_38.setText("@PG01LI@");
          OBJ_38.setOpaque(false);
          OBJ_38.setName("OBJ_38");
          p_tete_gauche.add(OBJ_38);
          OBJ_38.setBounds(388, 2, 330, OBJ_38.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_tete_gauche.getComponentCount(); i++) {
              Rectangle bounds = p_tete_gauche.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_tete_gauche.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_tete_gauche.setMinimumSize(preferredSize);
            p_tete_gauche.setPreferredSize(preferredSize);
          }
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- label41 ----
          label41.setText("@WPAGE@");
          label41.setName("label41");
          p_tete_droite.add(label41);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Calcul position colonnes");
              riSousMenu_bt6.setToolTipText("Calcul de position de colonnes");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1050, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("@LIBFCH@");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

            //---- TITZ ----
            TITZ.setFont(new Font("Courier New", Font.PLAIN, 12));
            TITZ.setName("TITZ");

            //---- OBJ_40 ----
            OBJ_40.setText("....5...10...15...20...25...30...35...40...45...50...55...60...65...70...75...80...85...90...95..100..105..110..115..120..125..130..135..140..145..150..155..160");
            OBJ_40.setFont(new Font("Courier New", Font.PLAIN, 10));
            OBJ_40.setName("OBJ_40");

            //---- separator1 ----
            separator1.setName("separator1");

            //======== panel1 ========
            {
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- label11 ----
              label11.setText("2");
              label11.setHorizontalAlignment(SwingConstants.RIGHT);
              label11.setName("label11");
              panel1.add(label11);
              label11.setBounds(5, 80, 20, label11.getPreferredSize().height);

              //---- label12 ----
              label12.setText("3");
              label12.setHorizontalAlignment(SwingConstants.RIGHT);
              label12.setName("label12");
              panel1.add(label12);
              label12.setBounds(5, 110, 20, label12.getPreferredSize().height);

              //---- label13 ----
              label13.setText("4");
              label13.setHorizontalAlignment(SwingConstants.RIGHT);
              label13.setName("label13");
              panel1.add(label13);
              label13.setBounds(5, 140, 20, label13.getPreferredSize().height);

              //---- label14 ----
              label14.setText("5");
              label14.setHorizontalAlignment(SwingConstants.RIGHT);
              label14.setName("label14");
              panel1.add(label14);
              label14.setBounds(5, 170, 20, label14.getPreferredSize().height);

              //---- label15 ----
              label15.setText("6");
              label15.setHorizontalAlignment(SwingConstants.RIGHT);
              label15.setName("label15");
              panel1.add(label15);
              label15.setBounds(5, 200, 20, label15.getPreferredSize().height);

              //---- label16 ----
              label16.setText("7");
              label16.setHorizontalAlignment(SwingConstants.RIGHT);
              label16.setName("label16");
              panel1.add(label16);
              label16.setBounds(5, 230, 20, label16.getPreferredSize().height);

              //---- label17 ----
              label17.setText("8");
              label17.setHorizontalAlignment(SwingConstants.RIGHT);
              label17.setName("label17");
              panel1.add(label17);
              label17.setBounds(5, 260, 20, label17.getPreferredSize().height);

              //---- label18 ----
              label18.setText("9");
              label18.setHorizontalAlignment(SwingConstants.RIGHT);
              label18.setName("label18");
              panel1.add(label18);
              label18.setBounds(5, 290, 20, label18.getPreferredSize().height);

              //---- label19 ----
              label19.setText("10");
              label19.setHorizontalAlignment(SwingConstants.RIGHT);
              label19.setName("label19");
              panel1.add(label19);
              label19.setBounds(5, 320, 20, label19.getPreferredSize().height);

              //---- label1 ----
              label1.setText("1");
              label1.setHorizontalAlignment(SwingConstants.RIGHT);
              label1.setName("label1");
              panel1.add(label1);
              label1.setBounds(5, 50, 20, label1.getPreferredSize().height);

              //---- DLDZ01 ----
              DLDZ01.setComponentPopupMenu(BTD);
              DLDZ01.setName("DLDZ01");
              panel1.add(DLDZ01);
              DLDZ01.setBounds(35, 45, 40, DLDZ01.getPreferredSize().height);

              //---- DLDZ02 ----
              DLDZ02.setComponentPopupMenu(BTD);
              DLDZ02.setName("DLDZ02");
              panel1.add(DLDZ02);
              DLDZ02.setBounds(35, 75, 40, DLDZ02.getPreferredSize().height);

              //---- DLDZ03 ----
              DLDZ03.setComponentPopupMenu(BTD);
              DLDZ03.setName("DLDZ03");
              panel1.add(DLDZ03);
              DLDZ03.setBounds(35, 105, 40, DLDZ03.getPreferredSize().height);

              //---- DLDZ04 ----
              DLDZ04.setComponentPopupMenu(BTD);
              DLDZ04.setName("DLDZ04");
              panel1.add(DLDZ04);
              DLDZ04.setBounds(35, 135, 40, DLDZ04.getPreferredSize().height);

              //---- DLDZ05 ----
              DLDZ05.setComponentPopupMenu(BTD);
              DLDZ05.setName("DLDZ05");
              panel1.add(DLDZ05);
              DLDZ05.setBounds(35, 165, 40, DLDZ05.getPreferredSize().height);

              //---- DLDZ06 ----
              DLDZ06.setComponentPopupMenu(BTD);
              DLDZ06.setName("DLDZ06");
              panel1.add(DLDZ06);
              DLDZ06.setBounds(35, 195, 40, DLDZ06.getPreferredSize().height);

              //---- DLDZ07 ----
              DLDZ07.setComponentPopupMenu(BTD);
              DLDZ07.setName("DLDZ07");
              panel1.add(DLDZ07);
              DLDZ07.setBounds(35, 225, 40, DLDZ07.getPreferredSize().height);

              //---- DLDZ08 ----
              DLDZ08.setComponentPopupMenu(BTD);
              DLDZ08.setName("DLDZ08");
              panel1.add(DLDZ08);
              DLDZ08.setBounds(35, 255, 40, DLDZ08.getPreferredSize().height);

              //---- DLDZ09 ----
              DLDZ09.setComponentPopupMenu(BTD);
              DLDZ09.setName("DLDZ09");
              panel1.add(DLDZ09);
              DLDZ09.setBounds(35, 285, 40, DLDZ09.getPreferredSize().height);

              //---- DLDZ10 ----
              DLDZ10.setComponentPopupMenu(BTD);
              DLDZ10.setName("DLDZ10");
              panel1.add(DLDZ10);
              DLDZ10.setBounds(35, 315, 40, DLDZ10.getPreferredSize().height);

              //---- DLDD01 ----
              DLDD01.setName("DLDD01");
              panel1.add(DLDD01);
              DLDD01.setBounds(75, 45, 40, DLDD01.getPreferredSize().height);

              //---- DLDF01 ----
              DLDF01.setName("DLDF01");
              panel1.add(DLDF01);
              DLDF01.setBounds(115, 45, 40, DLDF01.getPreferredSize().height);

              //---- label2 ----
              label2.setText("@DLDL01@");
              label2.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label2.setFont(new Font("Courier New", Font.PLAIN, 12));
              label2.setName("label2");
              panel1.add(label2);
              label2.setBounds(160, 45, 210, 28);

              //---- label3 ----
              label3.setText("@DLDN01@");
              label3.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label3.setName("label3");
              panel1.add(label3);
              label3.setBounds(375, 45, 30, 28);

              //---- DLDD02 ----
              DLDD02.setName("DLDD02");
              panel1.add(DLDD02);
              DLDD02.setBounds(75, 75, 40, DLDD02.getPreferredSize().height);

              //---- DLDD03 ----
              DLDD03.setName("DLDD03");
              panel1.add(DLDD03);
              DLDD03.setBounds(75, 105, 40, DLDD03.getPreferredSize().height);

              //---- DLDD04 ----
              DLDD04.setName("DLDD04");
              panel1.add(DLDD04);
              DLDD04.setBounds(75, 135, 40, DLDD04.getPreferredSize().height);

              //---- DLDD05 ----
              DLDD05.setName("DLDD05");
              panel1.add(DLDD05);
              DLDD05.setBounds(75, 165, 40, DLDD05.getPreferredSize().height);

              //---- DLDD06 ----
              DLDD06.setName("DLDD06");
              panel1.add(DLDD06);
              DLDD06.setBounds(75, 195, 40, DLDD06.getPreferredSize().height);

              //---- DLDD07 ----
              DLDD07.setName("DLDD07");
              panel1.add(DLDD07);
              DLDD07.setBounds(75, 225, 40, DLDD07.getPreferredSize().height);

              //---- DLDD08 ----
              DLDD08.setName("DLDD08");
              panel1.add(DLDD08);
              DLDD08.setBounds(75, 255, 40, DLDD08.getPreferredSize().height);

              //---- DLDD09 ----
              DLDD09.setName("DLDD09");
              panel1.add(DLDD09);
              DLDD09.setBounds(75, 285, 40, DLDD09.getPreferredSize().height);

              //---- DLDD10 ----
              DLDD10.setName("DLDD10");
              panel1.add(DLDD10);
              DLDD10.setBounds(75, 315, 40, DLDD10.getPreferredSize().height);

              //---- DLDF02 ----
              DLDF02.setName("DLDF02");
              panel1.add(DLDF02);
              DLDF02.setBounds(115, 75, 40, DLDF02.getPreferredSize().height);

              //---- DLDF03 ----
              DLDF03.setName("DLDF03");
              panel1.add(DLDF03);
              DLDF03.setBounds(115, 105, 40, DLDF03.getPreferredSize().height);

              //---- DLDF04 ----
              DLDF04.setName("DLDF04");
              panel1.add(DLDF04);
              DLDF04.setBounds(115, 135, 40, DLDF04.getPreferredSize().height);

              //---- DLDF05 ----
              DLDF05.setName("DLDF05");
              panel1.add(DLDF05);
              DLDF05.setBounds(115, 165, 40, DLDF05.getPreferredSize().height);

              //---- DLDF06 ----
              DLDF06.setName("DLDF06");
              panel1.add(DLDF06);
              DLDF06.setBounds(115, 195, 40, DLDF06.getPreferredSize().height);

              //---- DLDF07 ----
              DLDF07.setName("DLDF07");
              panel1.add(DLDF07);
              DLDF07.setBounds(115, 225, 40, DLDF07.getPreferredSize().height);

              //---- DLDF08 ----
              DLDF08.setName("DLDF08");
              panel1.add(DLDF08);
              DLDF08.setBounds(115, 255, 40, DLDF08.getPreferredSize().height);

              //---- DLDF09 ----
              DLDF09.setName("DLDF09");
              panel1.add(DLDF09);
              DLDF09.setBounds(115, 285, 40, DLDF09.getPreferredSize().height);

              //---- DLDF10 ----
              DLDF10.setName("DLDF10");
              panel1.add(DLDF10);
              DLDF10.setBounds(115, 315, 40, DLDF10.getPreferredSize().height);

              //---- label4 ----
              label4.setText("@DLDL02@");
              label4.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label4.setFont(new Font("Courier New", Font.PLAIN, 12));
              label4.setName("label4");
              panel1.add(label4);
              label4.setBounds(160, 75, 210, 28);

              //---- label5 ----
              label5.setText("@DLDL03@");
              label5.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label5.setFont(new Font("Courier New", Font.PLAIN, 12));
              label5.setName("label5");
              panel1.add(label5);
              label5.setBounds(160, 105, 210, 28);

              //---- label6 ----
              label6.setText("@DLDL04@");
              label6.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label6.setFont(new Font("Courier New", Font.PLAIN, 12));
              label6.setName("label6");
              panel1.add(label6);
              label6.setBounds(160, 135, 210, 28);

              //---- label7 ----
              label7.setText("@DLDL05@");
              label7.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label7.setFont(new Font("Courier New", Font.PLAIN, 12));
              label7.setName("label7");
              panel1.add(label7);
              label7.setBounds(160, 165, 210, 28);

              //---- label8 ----
              label8.setText("@DLDL06@");
              label8.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label8.setFont(new Font("Courier New", Font.PLAIN, 12));
              label8.setName("label8");
              panel1.add(label8);
              label8.setBounds(160, 195, 210, 28);

              //---- label9 ----
              label9.setText("@DLDL07@");
              label9.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label9.setFont(new Font("Courier New", Font.PLAIN, 12));
              label9.setName("label9");
              panel1.add(label9);
              label9.setBounds(160, 225, 210, 28);

              //---- label10 ----
              label10.setText("@DLDL08@");
              label10.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label10.setFont(new Font("Courier New", Font.PLAIN, 12));
              label10.setName("label10");
              panel1.add(label10);
              label10.setBounds(160, 255, 210, 28);

              //---- label21 ----
              label21.setText("@DLDL09@");
              label21.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label21.setFont(new Font("Courier New", Font.PLAIN, 12));
              label21.setName("label21");
              panel1.add(label21);
              label21.setBounds(160, 285, 210, 28);

              //---- label22 ----
              label22.setText("@DLDL10@");
              label22.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label22.setFont(new Font("Courier New", Font.PLAIN, 12));
              label22.setName("label22");
              panel1.add(label22);
              label22.setBounds(160, 315, 210, 28);

              //---- label23 ----
              label23.setText("@DLDN02@");
              label23.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label23.setName("label23");
              panel1.add(label23);
              label23.setBounds(375, 75, 30, 28);

              //---- label24 ----
              label24.setText("@DLDN03@");
              label24.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label24.setName("label24");
              panel1.add(label24);
              label24.setBounds(375, 105, 30, 28);

              //---- label25 ----
              label25.setText("@DLDN04@");
              label25.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label25.setName("label25");
              panel1.add(label25);
              label25.setBounds(375, 135, 30, 28);

              //---- label26 ----
              label26.setText("@DLDN05@");
              label26.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label26.setName("label26");
              panel1.add(label26);
              label26.setBounds(375, 165, 30, 28);

              //---- label27 ----
              label27.setText("@DLDN06@");
              label27.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label27.setName("label27");
              panel1.add(label27);
              label27.setBounds(375, 195, 30, 28);

              //---- label28 ----
              label28.setText("@DLDN07@");
              label28.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label28.setName("label28");
              panel1.add(label28);
              label28.setBounds(375, 225, 30, 28);

              //---- label29 ----
              label29.setText("@DLDN08@");
              label29.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label29.setName("label29");
              panel1.add(label29);
              label29.setBounds(375, 255, 30, 28);

              //---- label30 ----
              label30.setText("@DLDN09@");
              label30.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label30.setName("label30");
              panel1.add(label30);
              label30.setBounds(375, 285, 30, 28);

              //---- label31 ----
              label31.setText("@DLDN10@");
              label31.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label31.setName("label31");
              panel1.add(label31);
              label31.setBounds(375, 315, 30, 28);

              //---- DLDZ11 ----
              DLDZ11.setComponentPopupMenu(BTD);
              DLDZ11.setName("DLDZ11");
              panel1.add(DLDZ11);
              DLDZ11.setBounds(570, 45, 40, DLDZ11.getPreferredSize().height);

              //---- DLDZ12 ----
              DLDZ12.setComponentPopupMenu(BTD);
              DLDZ12.setName("DLDZ12");
              panel1.add(DLDZ12);
              DLDZ12.setBounds(570, 75, 40, DLDZ12.getPreferredSize().height);

              //---- DLDZ13 ----
              DLDZ13.setComponentPopupMenu(BTD);
              DLDZ13.setName("DLDZ13");
              panel1.add(DLDZ13);
              DLDZ13.setBounds(570, 105, 40, DLDZ13.getPreferredSize().height);

              //---- DLDZ14 ----
              DLDZ14.setComponentPopupMenu(BTD);
              DLDZ14.setName("DLDZ14");
              panel1.add(DLDZ14);
              DLDZ14.setBounds(570, 135, 40, DLDZ14.getPreferredSize().height);

              //---- DLDZ15 ----
              DLDZ15.setComponentPopupMenu(BTD);
              DLDZ15.setName("DLDZ15");
              panel1.add(DLDZ15);
              DLDZ15.setBounds(570, 165, 40, DLDZ15.getPreferredSize().height);

              //---- DLDZ16 ----
              DLDZ16.setComponentPopupMenu(BTD);
              DLDZ16.setName("DLDZ16");
              panel1.add(DLDZ16);
              DLDZ16.setBounds(570, 195, 40, DLDZ16.getPreferredSize().height);

              //---- DLDZ17 ----
              DLDZ17.setComponentPopupMenu(BTD);
              DLDZ17.setName("DLDZ17");
              panel1.add(DLDZ17);
              DLDZ17.setBounds(570, 225, 40, DLDZ17.getPreferredSize().height);

              //---- DLDZ18 ----
              DLDZ18.setComponentPopupMenu(BTD);
              DLDZ18.setName("DLDZ18");
              panel1.add(DLDZ18);
              DLDZ18.setBounds(570, 255, 40, DLDZ18.getPreferredSize().height);

              //---- DLDZ19 ----
              DLDZ19.setComponentPopupMenu(BTD);
              DLDZ19.setName("DLDZ19");
              panel1.add(DLDZ19);
              DLDZ19.setBounds(570, 285, 40, DLDZ19.getPreferredSize().height);

              //---- DLDZ20 ----
              DLDZ20.setComponentPopupMenu(BTD);
              DLDZ20.setName("DLDZ20");
              panel1.add(DLDZ20);
              DLDZ20.setBounds(570, 315, 40, DLDZ20.getPreferredSize().height);

              //---- DLDD11 ----
              DLDD11.setName("DLDD11");
              panel1.add(DLDD11);
              DLDD11.setBounds(610, 45, 40, DLDD11.getPreferredSize().height);

              //---- DLDF11 ----
              DLDF11.setName("DLDF11");
              panel1.add(DLDF11);
              DLDF11.setBounds(650, 45, 40, DLDF11.getPreferredSize().height);

              //---- label43 ----
              label43.setText("@DLDL11@");
              label43.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label43.setFont(new Font("Courier New", Font.PLAIN, 12));
              label43.setName("label43");
              panel1.add(label43);
              label43.setBounds(695, 45, 210, 28);

              //---- label44 ----
              label44.setText("@DLDN11@");
              label44.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label44.setName("label44");
              panel1.add(label44);
              label44.setBounds(910, 45, 30, 28);

              //---- DLDD12 ----
              DLDD12.setName("DLDD12");
              panel1.add(DLDD12);
              DLDD12.setBounds(610, 75, 40, DLDD12.getPreferredSize().height);

              //---- DLDD13 ----
              DLDD13.setName("DLDD13");
              panel1.add(DLDD13);
              DLDD13.setBounds(610, 105, 40, DLDD13.getPreferredSize().height);

              //---- DLDD14 ----
              DLDD14.setName("DLDD14");
              panel1.add(DLDD14);
              DLDD14.setBounds(610, 135, 40, DLDD14.getPreferredSize().height);

              //---- DLDD15 ----
              DLDD15.setName("DLDD15");
              panel1.add(DLDD15);
              DLDD15.setBounds(610, 165, 40, DLDD15.getPreferredSize().height);

              //---- DLDD16 ----
              DLDD16.setName("DLDD16");
              panel1.add(DLDD16);
              DLDD16.setBounds(610, 195, 40, DLDD16.getPreferredSize().height);

              //---- DLDD17 ----
              DLDD17.setName("DLDD17");
              panel1.add(DLDD17);
              DLDD17.setBounds(610, 225, 40, DLDD17.getPreferredSize().height);

              //---- DLDD18 ----
              DLDD18.setName("DLDD18");
              panel1.add(DLDD18);
              DLDD18.setBounds(610, 255, 40, DLDD18.getPreferredSize().height);

              //---- DLDD19 ----
              DLDD19.setName("DLDD19");
              panel1.add(DLDD19);
              DLDD19.setBounds(610, 285, 40, DLDD19.getPreferredSize().height);

              //---- DLDD20 ----
              DLDD20.setName("DLDD20");
              panel1.add(DLDD20);
              DLDD20.setBounds(610, 315, 40, DLDD20.getPreferredSize().height);

              //---- DLDF12 ----
              DLDF12.setName("DLDF12");
              panel1.add(DLDF12);
              DLDF12.setBounds(650, 75, 40, DLDF12.getPreferredSize().height);

              //---- DLDF13 ----
              DLDF13.setName("DLDF13");
              panel1.add(DLDF13);
              DLDF13.setBounds(650, 105, 40, DLDF13.getPreferredSize().height);

              //---- DLDF14 ----
              DLDF14.setName("DLDF14");
              panel1.add(DLDF14);
              DLDF14.setBounds(650, 135, 40, DLDF14.getPreferredSize().height);

              //---- DLDF15 ----
              DLDF15.setName("DLDF15");
              panel1.add(DLDF15);
              DLDF15.setBounds(650, 165, 40, DLDF15.getPreferredSize().height);

              //---- DLDF16 ----
              DLDF16.setName("DLDF16");
              panel1.add(DLDF16);
              DLDF16.setBounds(650, 195, 40, DLDF16.getPreferredSize().height);

              //---- DLDF17 ----
              DLDF17.setName("DLDF17");
              panel1.add(DLDF17);
              DLDF17.setBounds(650, 225, 40, DLDF17.getPreferredSize().height);

              //---- DLDF18 ----
              DLDF18.setName("DLDF18");
              panel1.add(DLDF18);
              DLDF18.setBounds(650, 255, 40, DLDF18.getPreferredSize().height);

              //---- DLDF19 ----
              DLDF19.setName("DLDF19");
              panel1.add(DLDF19);
              DLDF19.setBounds(650, 285, 40, DLDF19.getPreferredSize().height);

              //---- DLDF20 ----
              DLDF20.setName("DLDF20");
              panel1.add(DLDF20);
              DLDF20.setBounds(650, 315, 40, DLDF20.getPreferredSize().height);

              //---- label45 ----
              label45.setText("@DLDL12@");
              label45.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label45.setFont(new Font("Courier New", Font.PLAIN, 12));
              label45.setName("label45");
              panel1.add(label45);
              label45.setBounds(695, 75, 210, 28);

              //---- label46 ----
              label46.setText("@DLDL13@");
              label46.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label46.setFont(new Font("Courier New", Font.PLAIN, 12));
              label46.setName("label46");
              panel1.add(label46);
              label46.setBounds(695, 105, 210, 28);

              //---- label47 ----
              label47.setText("@DLDL14@");
              label47.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label47.setFont(new Font("Courier New", Font.PLAIN, 12));
              label47.setName("label47");
              panel1.add(label47);
              label47.setBounds(695, 135, 210, 28);

              //---- label48 ----
              label48.setText("@DLDL15@");
              label48.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label48.setFont(new Font("Courier New", Font.PLAIN, 12));
              label48.setName("label48");
              panel1.add(label48);
              label48.setBounds(695, 165, 210, 28);

              //---- label49 ----
              label49.setText("@DLDL16@");
              label49.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label49.setFont(new Font("Courier New", Font.PLAIN, 12));
              label49.setName("label49");
              panel1.add(label49);
              label49.setBounds(695, 195, 210, 28);

              //---- label50 ----
              label50.setText("@DLDL17@");
              label50.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label50.setFont(new Font("Courier New", Font.PLAIN, 12));
              label50.setName("label50");
              panel1.add(label50);
              label50.setBounds(695, 225, 210, 28);

              //---- label51 ----
              label51.setText("@DLDL18@");
              label51.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label51.setFont(new Font("Courier New", Font.PLAIN, 12));
              label51.setName("label51");
              panel1.add(label51);
              label51.setBounds(695, 255, 210, 28);

              //---- label52 ----
              label52.setText("@DLDL19@");
              label52.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label52.setFont(new Font("Courier New", Font.PLAIN, 12));
              label52.setName("label52");
              panel1.add(label52);
              label52.setBounds(695, 285, 210, 28);

              //---- label53 ----
              label53.setText("@DLDL20@");
              label53.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label53.setFont(new Font("Courier New", Font.PLAIN, 12));
              label53.setName("label53");
              panel1.add(label53);
              label53.setBounds(695, 315, 210, 28);

              //---- label54 ----
              label54.setText("@DLDN12@");
              label54.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label54.setName("label54");
              panel1.add(label54);
              label54.setBounds(910, 75, 30, 28);

              //---- label55 ----
              label55.setText("@DLDN13@");
              label55.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label55.setName("label55");
              panel1.add(label55);
              label55.setBounds(910, 105, 30, 28);

              //---- label56 ----
              label56.setText("@DLDN14@");
              label56.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label56.setName("label56");
              panel1.add(label56);
              label56.setBounds(910, 135, 30, 28);

              //---- label57 ----
              label57.setText("@DLDN15@");
              label57.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label57.setName("label57");
              panel1.add(label57);
              label57.setBounds(910, 165, 30, 28);

              //---- label58 ----
              label58.setText("@DLDN16@");
              label58.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label58.setName("label58");
              panel1.add(label58);
              label58.setBounds(910, 195, 30, 28);

              //---- label59 ----
              label59.setText("@DLDN17@");
              label59.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label59.setName("label59");
              panel1.add(label59);
              label59.setBounds(910, 225, 30, 28);

              //---- label60 ----
              label60.setText("@DLDN18@");
              label60.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label60.setName("label60");
              panel1.add(label60);
              label60.setBounds(910, 255, 30, 28);

              //---- label61 ----
              label61.setText("@DLDN19@");
              label61.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label61.setName("label61");
              panel1.add(label61);
              label61.setBounds(910, 285, 30, 28);

              //---- label62 ----
              label62.setText("@DLDN20@");
              label62.setBorder(new BevelBorder(BevelBorder.LOWERED));
              label62.setName("label62");
              panel1.add(label62);
              label62.setBounds(910, 315, 30, 28);

              //---- label20 ----
              label20.setText("12");
              label20.setHorizontalAlignment(SwingConstants.RIGHT);
              label20.setName("label20");
              panel1.add(label20);
              label20.setBounds(540, 80, 20, label20.getPreferredSize().height);

              //---- label32 ----
              label32.setText("13");
              label32.setHorizontalAlignment(SwingConstants.RIGHT);
              label32.setName("label32");
              panel1.add(label32);
              label32.setBounds(540, 110, 20, label32.getPreferredSize().height);

              //---- label33 ----
              label33.setText("14");
              label33.setHorizontalAlignment(SwingConstants.RIGHT);
              label33.setName("label33");
              panel1.add(label33);
              label33.setBounds(540, 140, 20, label33.getPreferredSize().height);

              //---- label34 ----
              label34.setText("15");
              label34.setHorizontalAlignment(SwingConstants.RIGHT);
              label34.setName("label34");
              panel1.add(label34);
              label34.setBounds(540, 170, 20, label34.getPreferredSize().height);

              //---- label35 ----
              label35.setText("16");
              label35.setHorizontalAlignment(SwingConstants.RIGHT);
              label35.setName("label35");
              panel1.add(label35);
              label35.setBounds(540, 200, 20, label35.getPreferredSize().height);

              //---- label36 ----
              label36.setText("17");
              label36.setHorizontalAlignment(SwingConstants.RIGHT);
              label36.setName("label36");
              panel1.add(label36);
              label36.setBounds(540, 230, 20, label36.getPreferredSize().height);

              //---- label37 ----
              label37.setText("18");
              label37.setHorizontalAlignment(SwingConstants.RIGHT);
              label37.setName("label37");
              panel1.add(label37);
              label37.setBounds(540, 260, 20, label37.getPreferredSize().height);

              //---- label38 ----
              label38.setText("19");
              label38.setHorizontalAlignment(SwingConstants.RIGHT);
              label38.setName("label38");
              panel1.add(label38);
              label38.setBounds(540, 290, 20, label38.getPreferredSize().height);

              //---- label39 ----
              label39.setText("20");
              label39.setHorizontalAlignment(SwingConstants.RIGHT);
              label39.setName("label39");
              panel1.add(label39);
              label39.setBounds(540, 320, 20, label39.getPreferredSize().height);

              //---- label40 ----
              label40.setText("11");
              label40.setHorizontalAlignment(SwingConstants.RIGHT);
              label40.setName("label40");
              panel1.add(label40);
              label40.setBounds(540, 50, 20, label40.getPreferredSize().height);

              //---- OBJ_54 ----
              OBJ_54.setText("Zone");
              OBJ_54.setFont(OBJ_54.getFont().deriveFont(OBJ_54.getFont().getStyle() | Font.BOLD));
              OBJ_54.setName("OBJ_54");
              panel1.add(OBJ_54);
              OBJ_54.setBounds(35, 25, 35, 19);

              //---- OBJ_56 ----
              OBJ_56.setText("D\u00e9but");
              OBJ_56.setFont(OBJ_56.getFont().deriveFont(OBJ_56.getFont().getStyle() | Font.BOLD));
              OBJ_56.setName("OBJ_56");
              panel1.add(OBJ_56);
              OBJ_56.setBounds(80, 25, 35, 19);

              //---- OBJ_57 ----
              OBJ_57.setText("Fin");
              OBJ_57.setFont(OBJ_57.getFont().deriveFont(OBJ_57.getFont().getStyle() | Font.BOLD));
              OBJ_57.setName("OBJ_57");
              panel1.add(OBJ_57);
              OBJ_57.setBounds(125, 25, 25, 19);

              //---- OBJ_58 ----
              OBJ_58.setText("Titre de colonne");
              OBJ_58.setFont(OBJ_58.getFont().deriveFont(OBJ_58.getFont().getStyle() | Font.BOLD));
              OBJ_58.setName("OBJ_58");
              panel1.add(OBJ_58);
              OBJ_58.setBounds(160, 25, 200, 19);

              //---- OBJ_59 ----
              OBJ_59.setText("Nc");
              OBJ_59.setFont(OBJ_59.getFont().deriveFont(OBJ_59.getFont().getStyle() | Font.BOLD));
              OBJ_59.setName("OBJ_59");
              panel1.add(OBJ_59);
              OBJ_59.setBounds(375, 25, 25, 19);

              //---- OBJ_60 ----
              OBJ_60.setText("Nc");
              OBJ_60.setFont(OBJ_60.getFont().deriveFont(OBJ_60.getFont().getStyle() | Font.BOLD));
              OBJ_60.setName("OBJ_60");
              panel1.add(OBJ_60);
              OBJ_60.setBounds(910, 25, 25, 19);

              //---- OBJ_61 ----
              OBJ_61.setText("Titre de colonne");
              OBJ_61.setFont(OBJ_61.getFont().deriveFont(OBJ_61.getFont().getStyle() | Font.BOLD));
              OBJ_61.setName("OBJ_61");
              panel1.add(OBJ_61);
              OBJ_61.setBounds(695, 25, 200, 19);

              //---- OBJ_62 ----
              OBJ_62.setText("Fin");
              OBJ_62.setFont(OBJ_62.getFont().deriveFont(OBJ_62.getFont().getStyle() | Font.BOLD));
              OBJ_62.setName("OBJ_62");
              panel1.add(OBJ_62);
              OBJ_62.setBounds(660, 25, 25, 19);

              //---- OBJ_63 ----
              OBJ_63.setText("D\u00e9but");
              OBJ_63.setFont(OBJ_63.getFont().deriveFont(OBJ_63.getFont().getStyle() | Font.BOLD));
              OBJ_63.setName("OBJ_63");
              panel1.add(OBJ_63);
              OBJ_63.setBounds(615, 25, 35, 19);

              //---- OBJ_64 ----
              OBJ_64.setText("Zone");
              OBJ_64.setFont(OBJ_64.getFont().deriveFont(OBJ_64.getFont().getStyle() | Font.BOLD));
              OBJ_64.setName("OBJ_64");
              panel1.add(OBJ_64);
              OBJ_64.setBounds(570, 25, 35, 19);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }

            //---- OBJ_55 ----
            OBJ_55.setText("Aller \u00e0 la page");
            OBJ_55.setToolTipText("Aller \u00e0 la page indiqu\u00e9e dans le champs suivant (F pour fin)");
            OBJ_55.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_55.setName("OBJ_55");
            OBJ_55.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_55ActionPerformed(e);
              }
            });

            //---- V06F ----
            V06F.setToolTipText("Num\u00e9ro de la prochaine page (F pour fin)");
            V06F.setComponentPopupMenu(BTD);
            V06F.setName("V06F");

            GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
            xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
            xTitledPanel1ContentContainerLayout.setHorizontalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 970, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addComponent(separator1, GroupLayout.PREFERRED_SIZE, 975, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 970, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addComponent(TITZ, GroupLayout.PREFERRED_SIZE, 970, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(830, 830, 830)
                  .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE)
                  .addGap(13, 13, 13)
                  .addComponent(V06F, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            );
            xTitledPanel1ContentContainerLayout.setVerticalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 350, GroupLayout.PREFERRED_SIZE)
                  .addGap(10, 10, 10)
                  .addComponent(separator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(6, 6, 6)
                  .addComponent(OBJ_40)
                  .addGap(3, 3, 3)
                  .addComponent(TITZ, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addGap(10, 10, 10)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                    .addComponent(V06F, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 454, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private XRiTextField INDETB;
  private JLabel OBJ_44;
  private XRiTextField INDTYP;
  private JLabel OBJ_46;
  private XRiTextField INDIND;
  private RiZoneSortie OBJ_38;
  private JPanel p_tete_droite;
  private JLabel label41;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private XRiTextField TITZ;
  private JLabel OBJ_40;
  private JComponent separator1;
  private JPanel panel1;
  private JLabel label11;
  private JLabel label12;
  private JLabel label13;
  private JLabel label14;
  private JLabel label15;
  private JLabel label16;
  private JLabel label17;
  private JLabel label18;
  private JLabel label19;
  private JLabel label1;
  private XRiTextField DLDZ01;
  private XRiTextField DLDZ02;
  private XRiTextField DLDZ03;
  private XRiTextField DLDZ04;
  private XRiTextField DLDZ05;
  private XRiTextField DLDZ06;
  private XRiTextField DLDZ07;
  private XRiTextField DLDZ08;
  private XRiTextField DLDZ09;
  private XRiTextField DLDZ10;
  private XRiTextField DLDD01;
  private XRiTextField DLDF01;
  private RiZoneSortie label2;
  private RiZoneSortie label3;
  private XRiTextField DLDD02;
  private XRiTextField DLDD03;
  private XRiTextField DLDD04;
  private XRiTextField DLDD05;
  private XRiTextField DLDD06;
  private XRiTextField DLDD07;
  private XRiTextField DLDD08;
  private XRiTextField DLDD09;
  private XRiTextField DLDD10;
  private XRiTextField DLDF02;
  private XRiTextField DLDF03;
  private XRiTextField DLDF04;
  private XRiTextField DLDF05;
  private XRiTextField DLDF06;
  private XRiTextField DLDF07;
  private XRiTextField DLDF08;
  private XRiTextField DLDF09;
  private XRiTextField DLDF10;
  private RiZoneSortie label4;
  private RiZoneSortie label5;
  private RiZoneSortie label6;
  private RiZoneSortie label7;
  private RiZoneSortie label8;
  private RiZoneSortie label9;
  private RiZoneSortie label10;
  private RiZoneSortie label21;
  private RiZoneSortie label22;
  private RiZoneSortie label23;
  private RiZoneSortie label24;
  private RiZoneSortie label25;
  private RiZoneSortie label26;
  private RiZoneSortie label27;
  private RiZoneSortie label28;
  private RiZoneSortie label29;
  private RiZoneSortie label30;
  private RiZoneSortie label31;
  private XRiTextField DLDZ11;
  private XRiTextField DLDZ12;
  private XRiTextField DLDZ13;
  private XRiTextField DLDZ14;
  private XRiTextField DLDZ15;
  private XRiTextField DLDZ16;
  private XRiTextField DLDZ17;
  private XRiTextField DLDZ18;
  private XRiTextField DLDZ19;
  private XRiTextField DLDZ20;
  private XRiTextField DLDD11;
  private XRiTextField DLDF11;
  private RiZoneSortie label43;
  private RiZoneSortie label44;
  private XRiTextField DLDD12;
  private XRiTextField DLDD13;
  private XRiTextField DLDD14;
  private XRiTextField DLDD15;
  private XRiTextField DLDD16;
  private XRiTextField DLDD17;
  private XRiTextField DLDD18;
  private XRiTextField DLDD19;
  private XRiTextField DLDD20;
  private XRiTextField DLDF12;
  private XRiTextField DLDF13;
  private XRiTextField DLDF14;
  private XRiTextField DLDF15;
  private XRiTextField DLDF16;
  private XRiTextField DLDF17;
  private XRiTextField DLDF18;
  private XRiTextField DLDF19;
  private XRiTextField DLDF20;
  private RiZoneSortie label45;
  private RiZoneSortie label46;
  private RiZoneSortie label47;
  private RiZoneSortie label48;
  private RiZoneSortie label49;
  private RiZoneSortie label50;
  private RiZoneSortie label51;
  private RiZoneSortie label52;
  private RiZoneSortie label53;
  private RiZoneSortie label54;
  private RiZoneSortie label55;
  private RiZoneSortie label56;
  private RiZoneSortie label57;
  private RiZoneSortie label58;
  private RiZoneSortie label59;
  private RiZoneSortie label60;
  private RiZoneSortie label61;
  private RiZoneSortie label62;
  private JLabel label20;
  private JLabel label32;
  private JLabel label33;
  private JLabel label34;
  private JLabel label35;
  private JLabel label36;
  private JLabel label37;
  private JLabel label38;
  private JLabel label39;
  private JLabel label40;
  private JLabel OBJ_54;
  private JLabel OBJ_56;
  private JLabel OBJ_57;
  private JLabel OBJ_58;
  private JLabel OBJ_59;
  private JLabel OBJ_60;
  private JLabel OBJ_61;
  private JLabel OBJ_62;
  private JLabel OBJ_63;
  private JLabel OBJ_64;
  private JButton OBJ_55;
  private XRiTextField V06F;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
