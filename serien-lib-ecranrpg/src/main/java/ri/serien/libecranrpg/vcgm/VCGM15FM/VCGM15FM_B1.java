
package ri.serien.libecranrpg.vcgm.VCGM15FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerListModel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiSpinner;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM15FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] EPTYP_Value = { "", "W", "V", "B", "A", "O" };
  private String[] EPTYP_Title = { "", "Vente abonnement", "Vente", "Achat abonnement", "Achat", "OD" };
  private String[] EPTPC_Value = { "", "P", "N", "O", };
  private String[] EPTPC_Title = { "", "Choix numéro", "Pas de génération", "Génération automatique", };
  
  public VCGM15FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    EPTYP.setValeurs(EPTYP_Value, EPTYP_Title);
    EPTPC.setValeurs(EPTPC_Value, EPTPC_Title);
    EPB106.setValeursSelection("X", " ");
    EPB105.setValeursSelection("X", " ");
    EPB104.setValeursSelection("X", " ");
    EPB103.setValeursSelection("X", " ");
    EPB102.setValeursSelection("X", " ");
    EPB101.setValeursSelection("X", " ");
    EPB096.setValeursSelection("X", " ");
    EPB095.setValeursSelection("X", " ");
    EPB094.setValeursSelection("X", " ");
    EPB093.setValeursSelection("X", " ");
    EPB092.setValeursSelection("X", " ");
    EPB091.setValeursSelection("X", " ");
    EPB086.setValeursSelection("X", " ");
    EPB085.setValeursSelection("X", " ");
    EPB084.setValeursSelection("X", " ");
    EPB083.setValeursSelection("X", " ");
    EPB082.setValeursSelection("X", " ");
    EPB081.setValeursSelection("X", " ");
    EPB076.setValeursSelection("X", " ");
    EPB075.setValeursSelection("X", " ");
    EPB074.setValeursSelection("X", " ");
    EPB073.setValeursSelection("X", " ");
    EPB072.setValeursSelection("X", " ");
    EPB071.setValeursSelection("X", " ");
    EPB066.setValeursSelection("X", " ");
    EPB065.setValeursSelection("X", " ");
    EPB064.setValeursSelection("X", " ");
    EPB063.setValeursSelection("X", " ");
    EPB062.setValeursSelection("X", " ");
    EPB061.setValeursSelection("X", " ");
    EPB056.setValeursSelection("X", " ");
    EPB055.setValeursSelection("X", " ");
    EPB054.setValeursSelection("X", " ");
    EPB053.setValeursSelection("X", " ");
    EPB052.setValeursSelection("X", " ");
    EPB051.setValeursSelection("X", " ");
    EPB046.setValeursSelection("X", " ");
    EPB045.setValeursSelection("X", " ");
    EPB044.setValeursSelection("X", " ");
    EPB043.setValeursSelection("X", " ");
    EPB042.setValeursSelection("X", " ");
    EPB041.setValeursSelection("X", " ");
    EPB036.setValeursSelection("X", " ");
    EPB035.setValeursSelection("X", " ");
    EPB034.setValeursSelection("X", " ");
    EPB033.setValeursSelection("X", " ");
    EPB032.setValeursSelection("X", " ");
    EPB031.setValeursSelection("X", " ");
    EPB026.setValeursSelection("X", " ");
    EPB025.setValeursSelection("X", " ");
    EPB024.setValeursSelection("X", " ");
    EPB023.setValeursSelection("X", " ");
    EPB022.setValeursSelection("X", " ");
    EPB021.setValeursSelection("X", " ");
    EPB016.setValeursSelection("X", " ");
    EPB015.setValeursSelection("X", " ");
    EPB014.setValeursSelection("X", " ");
    EPB013.setValeursSelection("X", " ");
    EPB012.setValeursSelection("X", " ");
    EPB011.setValeursSelection("X", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    LAXE1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAXE1@")).trim());
    LAXE2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAXE2@")).trim());
    LAXE3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAXE3@")).trim());
    LAXE4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAXE4@")).trim());
    label5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAXE5@")).trim());
    label6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAXE6@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_48ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_40 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_33 = new JLabel();
    INDCOD = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel3 = new JXTitledPanel();
    EPTPC = new XRiComboBox();
    EPTYP = new XRiComboBox();
    EPLIB = new XRiTextField();
    EPLIT = new XRiTextField();
    OBJ_229 = new JLabel();
    OBJ_199 = new JLabel();
    OBJ_230 = new JLabel();
    OBJ_200 = new JLabel();
    OBJ_36 = new JLabel();
    OBJ_38 = new JLabel();
    OBJ_201 = new JLabel();
    OBJ_37 = new JLabel();
    OBJ_48 = new SNBoutonDetail();
    EPNAT = new XRiTextField();
    EPACT = new XRiTextField();
    OBJ_202 = new JLabel();
    EPSAN = new XRiTextField();
    EPJO = new XRiTextField();
    EPCLB = new XRiTextField();
    LAXE1 = new JLabel();
    EPAA1 = new XRiTextField();
    LAXE2 = new JLabel();
    EPAA2 = new XRiTextField();
    LAXE3 = new JLabel();
    EPAA3 = new XRiTextField();
    LAXE4 = new JLabel();
    EPAA4 = new XRiTextField();
    label5 = new JLabel();
    EPAA5 = new XRiTextField();
    label6 = new JLabel();
    EPAA6 = new XRiTextField();
    label8 = new JLabel();
    label7 = new JLabel();
    EPRCG = new XRiTextField();
    EPRCA = new XRiTextField();
    xTitledPanel1 = new JXTitledPanel();
    EPLI02 = new XRiTextField();
    EPLI03 = new XRiTextField();
    EPLI04 = new XRiTextField();
    EPLI05 = new XRiTextField();
    EPLI06 = new XRiTextField();
    EPLI07 = new XRiTextField();
    EPLI08 = new XRiTextField();
    EPLI09 = new XRiTextField();
    EPLI10 = new XRiTextField();
    EPIN01 = new XRiTextField();
    EPIN02 = new XRiTextField();
    EPIN03 = new XRiTextField();
    EPIN04 = new XRiTextField();
    EPIN05 = new XRiTextField();
    EPIN06 = new XRiTextField();
    EPIN07 = new XRiTextField();
    EPIN08 = new XRiTextField();
    EPIN09 = new XRiTextField();
    EPIN10 = new XRiTextField();
    OBJ_207 = new JLabel();
    OBJ_210 = new JLabel();
    NCG1X = new XRiTextField();
    EPNA01 = new XRiTextField();
    EPCP01 = new XRiTextField();
    NCG2X = new XRiTextField();
    EPNA02 = new XRiTextField();
    EPCP02 = new XRiTextField();
    NCG3X = new XRiTextField();
    EPNA03 = new XRiTextField();
    EPCP03 = new XRiTextField();
    NCG4X = new XRiTextField();
    EPNA04 = new XRiTextField();
    EPCP04 = new XRiTextField();
    NCG5X = new XRiTextField();
    EPNA05 = new XRiTextField();
    EPCP05 = new XRiTextField();
    NCG6X = new XRiTextField();
    EPNA06 = new XRiTextField();
    EPCP06 = new XRiTextField();
    NCG7X = new XRiTextField();
    EPNA07 = new XRiTextField();
    EPCP07 = new XRiTextField();
    NCG8X = new XRiTextField();
    EPNA08 = new XRiTextField();
    EPCP08 = new XRiTextField();
    NCG9X = new XRiTextField();
    EPNA09 = new XRiTextField();
    EPCP09 = new XRiTextField();
    NCG10X = new XRiTextField();
    EPNA10 = new XRiTextField();
    EPCP10 = new XRiTextField();
    OBJ_203 = new JLabel();
    OBJ_204 = new JLabel();
    OBJ_205 = new JLabel();
    OBJ_206 = new JLabel();
    EPSN01 = new XRiSpinner();
    EPOP01 = new XRiSpinner();
    EPSN02 = new XRiSpinner();
    EPOP02 = new XRiSpinner();
    EPSN03 = new XRiSpinner();
    EPOP03 = new XRiSpinner();
    EPSN04 = new XRiSpinner();
    EPOP04 = new XRiSpinner();
    EPSN05 = new XRiSpinner();
    EPOP05 = new XRiSpinner();
    EPSN06 = new XRiSpinner();
    EPOP06 = new XRiSpinner();
    EPSN07 = new XRiSpinner();
    EPOP07 = new XRiSpinner();
    EPSN08 = new XRiSpinner();
    EPOP08 = new XRiSpinner();
    EPSN09 = new XRiSpinner();
    EPOP09 = new XRiSpinner();
    EPSN10 = new XRiSpinner();
    EPOP10 = new XRiSpinner();
    EPB011 = new XRiCheckBox();
    EPB012 = new XRiCheckBox();
    EPB013 = new XRiCheckBox();
    EPB014 = new XRiCheckBox();
    EPB015 = new XRiCheckBox();
    EPB016 = new XRiCheckBox();
    EPB021 = new XRiCheckBox();
    EPB022 = new XRiCheckBox();
    EPB023 = new XRiCheckBox();
    EPB024 = new XRiCheckBox();
    EPB025 = new XRiCheckBox();
    EPB026 = new XRiCheckBox();
    EPB031 = new XRiCheckBox();
    EPB032 = new XRiCheckBox();
    EPB033 = new XRiCheckBox();
    EPB034 = new XRiCheckBox();
    EPB035 = new XRiCheckBox();
    EPB036 = new XRiCheckBox();
    EPB041 = new XRiCheckBox();
    EPB042 = new XRiCheckBox();
    EPB043 = new XRiCheckBox();
    EPB044 = new XRiCheckBox();
    EPB045 = new XRiCheckBox();
    EPB046 = new XRiCheckBox();
    EPB051 = new XRiCheckBox();
    EPB052 = new XRiCheckBox();
    EPB053 = new XRiCheckBox();
    EPB054 = new XRiCheckBox();
    EPB055 = new XRiCheckBox();
    EPB056 = new XRiCheckBox();
    EPB061 = new XRiCheckBox();
    EPB062 = new XRiCheckBox();
    EPB063 = new XRiCheckBox();
    EPB064 = new XRiCheckBox();
    EPB065 = new XRiCheckBox();
    EPB066 = new XRiCheckBox();
    EPB071 = new XRiCheckBox();
    EPB072 = new XRiCheckBox();
    EPB073 = new XRiCheckBox();
    EPB074 = new XRiCheckBox();
    EPB075 = new XRiCheckBox();
    EPB076 = new XRiCheckBox();
    EPB081 = new XRiCheckBox();
    EPB082 = new XRiCheckBox();
    EPB083 = new XRiCheckBox();
    EPB084 = new XRiCheckBox();
    EPB085 = new XRiCheckBox();
    EPB086 = new XRiCheckBox();
    EPB091 = new XRiCheckBox();
    EPB092 = new XRiCheckBox();
    EPB093 = new XRiCheckBox();
    EPB094 = new XRiCheckBox();
    EPB095 = new XRiCheckBox();
    EPB096 = new XRiCheckBox();
    EPB101 = new XRiCheckBox();
    EPB102 = new XRiCheckBox();
    EPB103 = new XRiCheckBox();
    EPB104 = new XRiCheckBox();
    EPB105 = new XRiCheckBox();
    EPB106 = new XRiCheckBox();
    OBJ_208 = new JLabel();
    OBJ_209 = new JLabel();
    OBJ_233 = new JLabel();
    OBJ_234 = new JLabel();
    OBJ_235 = new JLabel();
    OBJ_236 = new JLabel();
    OBJ_237 = new JLabel();
    EPLI01 = new XRiTextField();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_212 = new JLabel();
    EPTA01 = new XRiTextField();
    EPTA02 = new XRiTextField();
    EPTA03 = new XRiTextField();
    EPTA04 = new XRiTextField();
    EPTA05 = new XRiTextField();
    EPTA06 = new XRiTextField();
    OBJ_219 = new JLabel();
    OBJ_213 = new JLabel();
    OBJ_214 = new JLabel();
    OBJ_215 = new JLabel();
    OBJ_216 = new JLabel();
    OBJ_217 = new JLabel();
    OBJ_218 = new JLabel();
    OBJ_220 = new JLabel();
    OBJ_221 = new JLabel();
    OBJ_222 = new JLabel();
    OBJ_223 = new JLabel();
    OBJ_224 = new JLabel();
    OBJ_225 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Pi\u00e8ces types");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_40 ----
          OBJ_40.setText("Soci\u00e9t\u00e9");
          OBJ_40.setName("OBJ_40");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_33 ----
          OBJ_33.setText("Code de pi\u00e8ce type");
          OBJ_33.setName("OBJ_33");

          //---- INDCOD ----
          INDCOD.setComponentPopupMenu(BTD);
          INDCOD.setName("INDCOD");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 121, GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addComponent(INDCOD, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDCOD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_40)
                  .addComponent(OBJ_33)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1020, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //======== xTitledPanel3 ========
          {
            xTitledPanel3.setBorder(new DropShadowBorder());
            xTitledPanel3.setName("xTitledPanel3");
            Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
            xTitledPanel3ContentContainer.setLayout(null);

            //---- EPTPC ----
            EPTPC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPTPC.setName("EPTPC");
            xTitledPanel3ContentContainer.add(EPTPC);
            EPTPC.setBounds(145, 69, 238, EPTPC.getPreferredSize().height);

            //---- EPTYP ----
            EPTYP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPTYP.setName("EPTYP");
            xTitledPanel3ContentContainer.add(EPTYP);
            EPTYP.setBounds(610, 38, 160, EPTYP.getPreferredSize().height);

            //---- EPLIB ----
            EPLIB.setComponentPopupMenu(BTD);
            EPLIB.setName("EPLIB");
            xTitledPanel3ContentContainer.add(EPLIB);
            EPLIB.setBounds(145, 5, 270, EPLIB.getPreferredSize().height);

            //---- EPLIT ----
            EPLIT.setComponentPopupMenu(BTD);
            EPLIT.setName("EPLIT");
            xTitledPanel3ContentContainer.add(EPLIT);
            EPLIT.setBounds(145, 37, 364, EPLIT.getPreferredSize().height);

            //---- OBJ_229 ----
            OBJ_229.setText("Libell\u00e9 de la pi\u00e8ce type");
            OBJ_229.setName("OBJ_229");
            xTitledPanel3ContentContainer.add(OBJ_229);
            OBJ_229.setBounds(10, 6, 135, 26);

            //---- OBJ_199 ----
            OBJ_199.setText("Libell\u00e9 \u00e9criture tiers");
            OBJ_199.setName("OBJ_199");
            xTitledPanel3ContentContainer.add(OBJ_199);
            OBJ_199.setBounds(10, 38, 135, 26);

            //---- OBJ_230 ----
            OBJ_230.setText("Pi\u00e8ce automatique");
            OBJ_230.setName("OBJ_230");
            xTitledPanel3ContentContainer.add(OBJ_230);
            OBJ_230.setBounds(10, 69, 135, 26);

            //---- OBJ_200 ----
            OBJ_200.setText("Section analytique");
            OBJ_200.setName("OBJ_200");
            xTitledPanel3ContentContainer.add(OBJ_200);
            OBJ_200.setBounds(445, 69, 115, 26);

            //---- OBJ_36 ----
            OBJ_36.setText("Code journal");
            OBJ_36.setName("OBJ_36");
            xTitledPanel3ContentContainer.add(OBJ_36);
            OBJ_36.setBounds(480, 6, 95, 26);

            //---- OBJ_38 ----
            OBJ_38.setText("Type");
            OBJ_38.setName("OBJ_38");
            xTitledPanel3ContentContainer.add(OBJ_38);
            OBJ_38.setBounds(560, 38, 45, 26);

            //---- OBJ_201 ----
            OBJ_201.setText("Code affaire");
            OBJ_201.setName("OBJ_201");
            xTitledPanel3ContentContainer.add(OBJ_201);
            OBJ_201.setBounds(630, 69, 75, 26);

            //---- OBJ_37 ----
            OBJ_37.setText("Code libell\u00e9");
            OBJ_37.setName("OBJ_37");
            xTitledPanel3ContentContainer.add(OBJ_37);
            OBJ_37.setBounds(655, 6, 75, 26);

            //---- OBJ_48 ----
            OBJ_48.setText("");
            OBJ_48.setToolTipText("Acc\u00e8s \u00e0 la saisie du num\u00e9ro de pi\u00e8ce d\u00e9but");
            OBJ_48.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_48.setName("OBJ_48");
            OBJ_48.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_48ActionPerformed(e);
              }
            });
            xTitledPanel3ContentContainer.add(OBJ_48);
            OBJ_48.setBounds(385, 70, 25, 25);

            //---- EPNAT ----
            EPNAT.setComponentPopupMenu(BTD);
            EPNAT.setName("EPNAT");
            xTitledPanel3ContentContainer.add(EPNAT);
            EPNAT.setBounds(862, 68, 52, EPNAT.getPreferredSize().height);

            //---- EPACT ----
            EPACT.setComponentPopupMenu(BTD);
            EPACT.setName("EPACT");
            xTitledPanel3ContentContainer.add(EPACT);
            EPACT.setBounds(718, 68, 52, EPACT.getPreferredSize().height);

            //---- OBJ_202 ----
            OBJ_202.setText("Nature");
            OBJ_202.setName("OBJ_202");
            xTitledPanel3ContentContainer.add(OBJ_202);
            OBJ_202.setBounds(810, 69, 55, 26);

            //---- EPSAN ----
            EPSAN.setComponentPopupMenu(BTD);
            EPSAN.setName("EPSAN");
            xTitledPanel3ContentContainer.add(EPSAN);
            EPSAN.setBounds(560, 68, 44, EPSAN.getPreferredSize().height);

            //---- EPJO ----
            EPJO.setComponentPopupMenu(BTD);
            EPJO.setName("EPJO");
            xTitledPanel3ContentContainer.add(EPJO);
            EPJO.setBounds(574, 5, 30, EPJO.getPreferredSize().height);

            //---- EPCLB ----
            EPCLB.setComponentPopupMenu(BTD);
            EPCLB.setName("EPCLB");
            xTitledPanel3ContentContainer.add(EPCLB);
            EPCLB.setBounds(750, 5, 20, EPCLB.getPreferredSize().height);

            //---- LAXE1 ----
            LAXE1.setText("@LAXE1@");
            LAXE1.setName("LAXE1");
            xTitledPanel3ContentContainer.add(LAXE1);
            LAXE1.setBounds(10, 105, 70, 28);

            //---- EPAA1 ----
            EPAA1.setComponentPopupMenu(BTD);
            EPAA1.setName("EPAA1");
            xTitledPanel3ContentContainer.add(EPAA1);
            EPAA1.setBounds(75, 105, 64, EPAA1.getPreferredSize().height);

            //---- LAXE2 ----
            LAXE2.setText("@LAXE2@");
            LAXE2.setName("LAXE2");
            xTitledPanel3ContentContainer.add(LAXE2);
            LAXE2.setBounds(165, 105, 70, 28);

            //---- EPAA2 ----
            EPAA2.setComponentPopupMenu(BTD);
            EPAA2.setName("EPAA2");
            xTitledPanel3ContentContainer.add(EPAA2);
            EPAA2.setBounds(230, 105, 64, EPAA2.getPreferredSize().height);

            //---- LAXE3 ----
            LAXE3.setText("@LAXE3@");
            LAXE3.setName("LAXE3");
            xTitledPanel3ContentContainer.add(LAXE3);
            LAXE3.setBounds(320, 105, 70, 28);

            //---- EPAA3 ----
            EPAA3.setComponentPopupMenu(BTD);
            EPAA3.setName("EPAA3");
            xTitledPanel3ContentContainer.add(EPAA3);
            EPAA3.setBounds(385, 105, 64, EPAA3.getPreferredSize().height);

            //---- LAXE4 ----
            LAXE4.setText("@LAXE4@");
            LAXE4.setName("LAXE4");
            xTitledPanel3ContentContainer.add(LAXE4);
            LAXE4.setBounds(475, 105, 70, 28);

            //---- EPAA4 ----
            EPAA4.setComponentPopupMenu(BTD);
            EPAA4.setName("EPAA4");
            xTitledPanel3ContentContainer.add(EPAA4);
            EPAA4.setBounds(540, 105, 64, EPAA4.getPreferredSize().height);

            //---- label5 ----
            label5.setText("@LAXE5@");
            label5.setName("label5");
            xTitledPanel3ContentContainer.add(label5);
            label5.setBounds(630, 105, 70, 28);

            //---- EPAA5 ----
            EPAA5.setComponentPopupMenu(BTD);
            EPAA5.setName("EPAA5");
            xTitledPanel3ContentContainer.add(EPAA5);
            EPAA5.setBounds(706, 105, 64, EPAA5.getPreferredSize().height);

            //---- label6 ----
            label6.setText("@LAXE6@");
            label6.setName("label6");
            xTitledPanel3ContentContainer.add(label6);
            label6.setBounds(785, 105, 70, 28);

            //---- EPAA6 ----
            EPAA6.setComponentPopupMenu(BTD);
            EPAA6.setName("EPAA6");
            xTitledPanel3ContentContainer.add(EPAA6);
            EPAA6.setBounds(850, 105, 64, EPAA6.getPreferredSize().height);

            //---- label8 ----
            label8.setText("Rattachement NCG");
            label8.setName("label8");
            xTitledPanel3ContentContainer.add(label8);
            label8.setBounds(795, 6, 118, 26);

            //---- label7 ----
            label7.setText("Rattachement NCA");
            label7.setName("label7");
            xTitledPanel3ContentContainer.add(label7);
            label7.setBounds(795, 38, 120, 26);

            //---- EPRCG ----
            EPRCG.setComponentPopupMenu(BTD);
            EPRCG.setName("EPRCG");
            xTitledPanel3ContentContainer.add(EPRCG);
            EPRCG.setBounds(925, 5, 64, EPRCG.getPreferredSize().height);

            //---- EPRCA ----
            EPRCA.setComponentPopupMenu(BTD);
            EPRCA.setName("EPRCA");
            xTitledPanel3ContentContainer.add(EPRCA);
            EPRCA.setBounds(925, 37, 64, EPRCA.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel3ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(xTitledPanel3);
          xTitledPanel3.setBounds(10, 14, 1000, xTitledPanel3.getPreferredSize().height);

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- EPLI02 ----
            EPLI02.setComponentPopupMenu(BTD);
            EPLI02.setFont(EPLI02.getFont().deriveFont(EPLI02.getFont().getSize() - 2f));
            EPLI02.setName("EPLI02");
            xTitledPanel1ContentContainer.add(EPLI02);
            EPLI02.setBounds(395, 55, 364, EPLI02.getPreferredSize().height);

            //---- EPLI03 ----
            EPLI03.setComponentPopupMenu(BTD);
            EPLI03.setFont(EPLI03.getFont().deriveFont(EPLI03.getFont().getSize() - 2f));
            EPLI03.setName("EPLI03");
            xTitledPanel1ContentContainer.add(EPLI03);
            EPLI03.setBounds(395, 80, 364, EPLI03.getPreferredSize().height);

            //---- EPLI04 ----
            EPLI04.setComponentPopupMenu(BTD);
            EPLI04.setFont(EPLI04.getFont().deriveFont(EPLI04.getFont().getSize() - 2f));
            EPLI04.setName("EPLI04");
            xTitledPanel1ContentContainer.add(EPLI04);
            EPLI04.setBounds(395, 105, 364, EPLI04.getPreferredSize().height);

            //---- EPLI05 ----
            EPLI05.setComponentPopupMenu(BTD);
            EPLI05.setFont(EPLI05.getFont().deriveFont(EPLI05.getFont().getSize() - 2f));
            EPLI05.setName("EPLI05");
            xTitledPanel1ContentContainer.add(EPLI05);
            EPLI05.setBounds(395, 130, 364, EPLI05.getPreferredSize().height);

            //---- EPLI06 ----
            EPLI06.setComponentPopupMenu(BTD);
            EPLI06.setFont(EPLI06.getFont().deriveFont(EPLI06.getFont().getSize() - 2f));
            EPLI06.setName("EPLI06");
            xTitledPanel1ContentContainer.add(EPLI06);
            EPLI06.setBounds(395, 155, 364, EPLI06.getPreferredSize().height);

            //---- EPLI07 ----
            EPLI07.setComponentPopupMenu(BTD);
            EPLI07.setFont(EPLI07.getFont().deriveFont(EPLI07.getFont().getSize() - 2f));
            EPLI07.setName("EPLI07");
            xTitledPanel1ContentContainer.add(EPLI07);
            EPLI07.setBounds(395, 180, 364, EPLI07.getPreferredSize().height);

            //---- EPLI08 ----
            EPLI08.setComponentPopupMenu(BTD);
            EPLI08.setFont(EPLI08.getFont().deriveFont(EPLI08.getFont().getSize() - 2f));
            EPLI08.setName("EPLI08");
            xTitledPanel1ContentContainer.add(EPLI08);
            EPLI08.setBounds(395, 205, 364, EPLI08.getPreferredSize().height);

            //---- EPLI09 ----
            EPLI09.setComponentPopupMenu(BTD);
            EPLI09.setFont(EPLI09.getFont().deriveFont(EPLI09.getFont().getSize() - 2f));
            EPLI09.setName("EPLI09");
            xTitledPanel1ContentContainer.add(EPLI09);
            EPLI09.setBounds(395, 230, 364, EPLI09.getPreferredSize().height);

            //---- EPLI10 ----
            EPLI10.setComponentPopupMenu(BTD);
            EPLI10.setFont(EPLI10.getFont().deriveFont(EPLI10.getFont().getSize() - 2f));
            EPLI10.setName("EPLI10");
            xTitledPanel1ContentContainer.add(EPLI10);
            EPLI10.setBounds(395, 255, 364, EPLI10.getPreferredSize().height);

            //---- EPIN01 ----
            EPIN01.setComponentPopupMenu(BTD);
            EPIN01.setFont(EPIN01.getFont().deriveFont(EPIN01.getFont().getSize() - 2f));
            EPIN01.setName("EPIN01");
            xTitledPanel1ContentContainer.add(EPIN01);
            EPIN01.setBounds(5, 30, 225, EPIN01.getPreferredSize().height);

            //---- EPIN02 ----
            EPIN02.setComponentPopupMenu(BTD);
            EPIN02.setFont(EPIN02.getFont().deriveFont(EPIN02.getFont().getSize() - 2f));
            EPIN02.setName("EPIN02");
            xTitledPanel1ContentContainer.add(EPIN02);
            EPIN02.setBounds(5, 55, 225, EPIN02.getPreferredSize().height);

            //---- EPIN03 ----
            EPIN03.setComponentPopupMenu(BTD);
            EPIN03.setFont(EPIN03.getFont().deriveFont(EPIN03.getFont().getSize() - 2f));
            EPIN03.setName("EPIN03");
            xTitledPanel1ContentContainer.add(EPIN03);
            EPIN03.setBounds(5, 80, 225, EPIN03.getPreferredSize().height);

            //---- EPIN04 ----
            EPIN04.setComponentPopupMenu(BTD);
            EPIN04.setFont(EPIN04.getFont().deriveFont(EPIN04.getFont().getSize() - 2f));
            EPIN04.setName("EPIN04");
            xTitledPanel1ContentContainer.add(EPIN04);
            EPIN04.setBounds(5, 105, 225, EPIN04.getPreferredSize().height);

            //---- EPIN05 ----
            EPIN05.setComponentPopupMenu(BTD);
            EPIN05.setFont(EPIN05.getFont().deriveFont(EPIN05.getFont().getSize() - 2f));
            EPIN05.setName("EPIN05");
            xTitledPanel1ContentContainer.add(EPIN05);
            EPIN05.setBounds(5, 130, 225, EPIN05.getPreferredSize().height);

            //---- EPIN06 ----
            EPIN06.setComponentPopupMenu(BTD);
            EPIN06.setFont(EPIN06.getFont().deriveFont(EPIN06.getFont().getSize() - 2f));
            EPIN06.setName("EPIN06");
            xTitledPanel1ContentContainer.add(EPIN06);
            EPIN06.setBounds(5, 155, 225, EPIN06.getPreferredSize().height);

            //---- EPIN07 ----
            EPIN07.setComponentPopupMenu(BTD);
            EPIN07.setFont(EPIN07.getFont().deriveFont(EPIN07.getFont().getSize() - 2f));
            EPIN07.setName("EPIN07");
            xTitledPanel1ContentContainer.add(EPIN07);
            EPIN07.setBounds(5, 180, 225, EPIN07.getPreferredSize().height);

            //---- EPIN08 ----
            EPIN08.setComponentPopupMenu(BTD);
            EPIN08.setFont(EPIN08.getFont().deriveFont(EPIN08.getFont().getSize() - 2f));
            EPIN08.setName("EPIN08");
            xTitledPanel1ContentContainer.add(EPIN08);
            EPIN08.setBounds(5, 205, 225, EPIN08.getPreferredSize().height);

            //---- EPIN09 ----
            EPIN09.setComponentPopupMenu(BTD);
            EPIN09.setFont(EPIN09.getFont().deriveFont(EPIN09.getFont().getSize() - 2f));
            EPIN09.setName("EPIN09");
            xTitledPanel1ContentContainer.add(EPIN09);
            EPIN09.setBounds(5, 230, 225, EPIN09.getPreferredSize().height);

            //---- EPIN10 ----
            EPIN10.setComponentPopupMenu(BTD);
            EPIN10.setFont(EPIN10.getFont().deriveFont(EPIN10.getFont().getSize() - 2f));
            EPIN10.setName("EPIN10");
            xTitledPanel1ContentContainer.add(EPIN10);
            EPIN10.setBounds(5, 255, 225, EPIN10.getPreferredSize().height);

            //---- OBJ_207 ----
            OBJ_207.setText("Libell\u00e9 contrepartie");
            OBJ_207.setName("OBJ_207");
            xTitledPanel1ContentContainer.add(OBJ_207);
            OBJ_207.setBounds(395, 5, 138, 25);

            //---- OBJ_210 ----
            OBJ_210.setText("Contrepartie");
            OBJ_210.setName("OBJ_210");
            xTitledPanel1ContentContainer.add(OBJ_210);
            OBJ_210.setBounds(920, 5, 72, 25);

            //---- NCG1X ----
            NCG1X.setComponentPopupMenu(BTD);
            NCG1X.setName("NCG1X");
            xTitledPanel1ContentContainer.add(NCG1X);
            NCG1X.setBounds(230, 30, 60, NCG1X.getPreferredSize().height);

            //---- EPNA01 ----
            EPNA01.setComponentPopupMenu(BTD);
            EPNA01.setName("EPNA01");
            xTitledPanel1ContentContainer.add(EPNA01);
            EPNA01.setBounds(290, 30, 60, EPNA01.getPreferredSize().height);

            //---- EPCP01 ----
            EPCP01.setComponentPopupMenu(BTD);
            EPCP01.setName("EPCP01");
            xTitledPanel1ContentContainer.add(EPCP01);
            EPCP01.setBounds(925, 30, 60, EPCP01.getPreferredSize().height);

            //---- NCG2X ----
            NCG2X.setComponentPopupMenu(BTD);
            NCG2X.setName("NCG2X");
            xTitledPanel1ContentContainer.add(NCG2X);
            NCG2X.setBounds(230, 55, 60, NCG2X.getPreferredSize().height);

            //---- EPNA02 ----
            EPNA02.setComponentPopupMenu(BTD);
            EPNA02.setName("EPNA02");
            xTitledPanel1ContentContainer.add(EPNA02);
            EPNA02.setBounds(290, 55, 60, EPNA02.getPreferredSize().height);

            //---- EPCP02 ----
            EPCP02.setComponentPopupMenu(BTD);
            EPCP02.setName("EPCP02");
            xTitledPanel1ContentContainer.add(EPCP02);
            EPCP02.setBounds(925, 55, 60, EPCP02.getPreferredSize().height);

            //---- NCG3X ----
            NCG3X.setComponentPopupMenu(BTD);
            NCG3X.setName("NCG3X");
            xTitledPanel1ContentContainer.add(NCG3X);
            NCG3X.setBounds(230, 80, 60, NCG3X.getPreferredSize().height);

            //---- EPNA03 ----
            EPNA03.setComponentPopupMenu(BTD);
            EPNA03.setName("EPNA03");
            xTitledPanel1ContentContainer.add(EPNA03);
            EPNA03.setBounds(290, 80, 60, EPNA03.getPreferredSize().height);

            //---- EPCP03 ----
            EPCP03.setComponentPopupMenu(BTD);
            EPCP03.setName("EPCP03");
            xTitledPanel1ContentContainer.add(EPCP03);
            EPCP03.setBounds(925, 80, 60, EPCP03.getPreferredSize().height);

            //---- NCG4X ----
            NCG4X.setComponentPopupMenu(BTD);
            NCG4X.setName("NCG4X");
            xTitledPanel1ContentContainer.add(NCG4X);
            NCG4X.setBounds(230, 105, 60, NCG4X.getPreferredSize().height);

            //---- EPNA04 ----
            EPNA04.setComponentPopupMenu(BTD);
            EPNA04.setName("EPNA04");
            xTitledPanel1ContentContainer.add(EPNA04);
            EPNA04.setBounds(290, 105, 60, EPNA04.getPreferredSize().height);

            //---- EPCP04 ----
            EPCP04.setComponentPopupMenu(BTD);
            EPCP04.setName("EPCP04");
            xTitledPanel1ContentContainer.add(EPCP04);
            EPCP04.setBounds(925, 105, 60, EPCP04.getPreferredSize().height);

            //---- NCG5X ----
            NCG5X.setComponentPopupMenu(BTD);
            NCG5X.setName("NCG5X");
            xTitledPanel1ContentContainer.add(NCG5X);
            NCG5X.setBounds(230, 130, 60, NCG5X.getPreferredSize().height);

            //---- EPNA05 ----
            EPNA05.setComponentPopupMenu(BTD);
            EPNA05.setName("EPNA05");
            xTitledPanel1ContentContainer.add(EPNA05);
            EPNA05.setBounds(290, 130, 60, EPNA05.getPreferredSize().height);

            //---- EPCP05 ----
            EPCP05.setComponentPopupMenu(BTD);
            EPCP05.setName("EPCP05");
            xTitledPanel1ContentContainer.add(EPCP05);
            EPCP05.setBounds(925, 130, 60, EPCP05.getPreferredSize().height);

            //---- NCG6X ----
            NCG6X.setComponentPopupMenu(BTD);
            NCG6X.setName("NCG6X");
            xTitledPanel1ContentContainer.add(NCG6X);
            NCG6X.setBounds(230, 155, 60, NCG6X.getPreferredSize().height);

            //---- EPNA06 ----
            EPNA06.setComponentPopupMenu(BTD);
            EPNA06.setName("EPNA06");
            xTitledPanel1ContentContainer.add(EPNA06);
            EPNA06.setBounds(290, 155, 60, EPNA06.getPreferredSize().height);

            //---- EPCP06 ----
            EPCP06.setComponentPopupMenu(BTD);
            EPCP06.setName("EPCP06");
            xTitledPanel1ContentContainer.add(EPCP06);
            EPCP06.setBounds(925, 155, 60, EPCP06.getPreferredSize().height);

            //---- NCG7X ----
            NCG7X.setComponentPopupMenu(BTD);
            NCG7X.setName("NCG7X");
            xTitledPanel1ContentContainer.add(NCG7X);
            NCG7X.setBounds(230, 180, 60, NCG7X.getPreferredSize().height);

            //---- EPNA07 ----
            EPNA07.setComponentPopupMenu(BTD);
            EPNA07.setName("EPNA07");
            xTitledPanel1ContentContainer.add(EPNA07);
            EPNA07.setBounds(290, 180, 60, EPNA07.getPreferredSize().height);

            //---- EPCP07 ----
            EPCP07.setComponentPopupMenu(BTD);
            EPCP07.setName("EPCP07");
            xTitledPanel1ContentContainer.add(EPCP07);
            EPCP07.setBounds(925, 180, 60, EPCP07.getPreferredSize().height);

            //---- NCG8X ----
            NCG8X.setComponentPopupMenu(BTD);
            NCG8X.setName("NCG8X");
            xTitledPanel1ContentContainer.add(NCG8X);
            NCG8X.setBounds(230, 205, 60, NCG8X.getPreferredSize().height);

            //---- EPNA08 ----
            EPNA08.setComponentPopupMenu(BTD);
            EPNA08.setName("EPNA08");
            xTitledPanel1ContentContainer.add(EPNA08);
            EPNA08.setBounds(290, 205, 60, EPNA08.getPreferredSize().height);

            //---- EPCP08 ----
            EPCP08.setComponentPopupMenu(BTD);
            EPCP08.setName("EPCP08");
            xTitledPanel1ContentContainer.add(EPCP08);
            EPCP08.setBounds(925, 205, 60, EPCP08.getPreferredSize().height);

            //---- NCG9X ----
            NCG9X.setComponentPopupMenu(BTD);
            NCG9X.setName("NCG9X");
            xTitledPanel1ContentContainer.add(NCG9X);
            NCG9X.setBounds(230, 230, 60, NCG9X.getPreferredSize().height);

            //---- EPNA09 ----
            EPNA09.setComponentPopupMenu(BTD);
            EPNA09.setName("EPNA09");
            xTitledPanel1ContentContainer.add(EPNA09);
            EPNA09.setBounds(290, 230, 60, EPNA09.getPreferredSize().height);

            //---- EPCP09 ----
            EPCP09.setComponentPopupMenu(BTD);
            EPCP09.setName("EPCP09");
            xTitledPanel1ContentContainer.add(EPCP09);
            EPCP09.setBounds(925, 230, 60, EPCP09.getPreferredSize().height);

            //---- NCG10X ----
            NCG10X.setComponentPopupMenu(BTD);
            NCG10X.setName("NCG10X");
            xTitledPanel1ContentContainer.add(NCG10X);
            NCG10X.setBounds(230, 255, 60, NCG10X.getPreferredSize().height);

            //---- EPNA10 ----
            EPNA10.setComponentPopupMenu(BTD);
            EPNA10.setName("EPNA10");
            xTitledPanel1ContentContainer.add(EPNA10);
            EPNA10.setBounds(290, 255, 60, EPNA10.getPreferredSize().height);

            //---- EPCP10 ----
            EPCP10.setComponentPopupMenu(BTD);
            EPCP10.setName("EPCP10");
            xTitledPanel1ContentContainer.add(EPCP10);
            EPCP10.setBounds(925, 255, 60, EPCP10.getPreferredSize().height);

            //---- OBJ_203 ----
            OBJ_203.setText("Intitul\u00e9");
            OBJ_203.setName("OBJ_203");
            xTitledPanel1ContentContainer.add(OBJ_203);
            OBJ_203.setBounds(10, 5, 58, 25);

            //---- OBJ_204 ----
            OBJ_204.setText("Compte");
            OBJ_204.setName("OBJ_204");
            xTitledPanel1ContentContainer.add(OBJ_204);
            OBJ_204.setBounds(230, 5, 52, 25);

            //---- OBJ_205 ----
            OBJ_205.setText("Tiers");
            OBJ_205.setName("OBJ_205");
            xTitledPanel1ContentContainer.add(OBJ_205);
            OBJ_205.setBounds(290, 5, 34, 25);

            //---- OBJ_206 ----
            OBJ_206.setText("S");
            OBJ_206.setName("OBJ_206");
            xTitledPanel1ContentContainer.add(OBJ_206);
            OBJ_206.setBounds(350, 5, 31, 25);

            //---- EPSN01 ----
            EPSN01.setComponentPopupMenu(BTD);
            EPSN01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPSN01.setModel(new SpinnerListModel(new String[] {" ", "C", "D"}) {
              { setValue("C"); }
            });
            EPSN01.setName("EPSN01");
            xTitledPanel1ContentContainer.add(EPSN01);
            EPSN01.setBounds(350, 30, 44, EPSN01.getPreferredSize().height);

            //---- EPOP01 ----
            EPOP01.setComponentPopupMenu(BTD);
            EPOP01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPOP01.setModel(new SpinnerListModel(new String[] {" ", "%", "-", "+"}));
            EPOP01.setName("EPOP01");
            xTitledPanel1ContentContainer.add(EPOP01);
            EPOP01.setBounds(760, 30, 44, EPOP01.getPreferredSize().height);

            //---- EPSN02 ----
            EPSN02.setComponentPopupMenu(BTD);
            EPSN02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPSN02.setModel(new SpinnerListModel(new String[] {" ", "C", "D"}) {
              { setValue("C"); }
            });
            EPSN02.setName("EPSN02");
            xTitledPanel1ContentContainer.add(EPSN02);
            EPSN02.setBounds(350, 55, 44, EPSN02.getPreferredSize().height);

            //---- EPOP02 ----
            EPOP02.setComponentPopupMenu(BTD);
            EPOP02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPOP02.setModel(new SpinnerListModel(new String[] {" ", "%", "-", "+"}));
            EPOP02.setName("EPOP02");
            xTitledPanel1ContentContainer.add(EPOP02);
            EPOP02.setBounds(760, 55, 44, EPOP02.getPreferredSize().height);

            //---- EPSN03 ----
            EPSN03.setComponentPopupMenu(BTD);
            EPSN03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPSN03.setModel(new SpinnerListModel(new String[] {" ", "C", "D"}) {
              { setValue("C"); }
            });
            EPSN03.setName("EPSN03");
            xTitledPanel1ContentContainer.add(EPSN03);
            EPSN03.setBounds(350, 80, 44, EPSN03.getPreferredSize().height);

            //---- EPOP03 ----
            EPOP03.setComponentPopupMenu(BTD);
            EPOP03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPOP03.setModel(new SpinnerListModel(new String[] {" ", "%", "-", "+"}));
            EPOP03.setName("EPOP03");
            xTitledPanel1ContentContainer.add(EPOP03);
            EPOP03.setBounds(760, 80, 44, EPOP03.getPreferredSize().height);

            //---- EPSN04 ----
            EPSN04.setComponentPopupMenu(BTD);
            EPSN04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPSN04.setModel(new SpinnerListModel(new String[] {" ", "C", "D"}) {
              { setValue("C"); }
            });
            EPSN04.setName("EPSN04");
            xTitledPanel1ContentContainer.add(EPSN04);
            EPSN04.setBounds(350, 105, 44, EPSN04.getPreferredSize().height);

            //---- EPOP04 ----
            EPOP04.setComponentPopupMenu(BTD);
            EPOP04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPOP04.setModel(new SpinnerListModel(new String[] {" ", "%", "-", "+"}));
            EPOP04.setName("EPOP04");
            xTitledPanel1ContentContainer.add(EPOP04);
            EPOP04.setBounds(760, 105, 44, EPOP04.getPreferredSize().height);

            //---- EPSN05 ----
            EPSN05.setComponentPopupMenu(BTD);
            EPSN05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPSN05.setModel(new SpinnerListModel(new String[] {" ", "C", "D"}) {
              { setValue("C"); }
            });
            EPSN05.setName("EPSN05");
            xTitledPanel1ContentContainer.add(EPSN05);
            EPSN05.setBounds(350, 130, 44, EPSN05.getPreferredSize().height);

            //---- EPOP05 ----
            EPOP05.setComponentPopupMenu(BTD);
            EPOP05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPOP05.setModel(new SpinnerListModel(new String[] {" ", "%", "-", "+"}));
            EPOP05.setName("EPOP05");
            xTitledPanel1ContentContainer.add(EPOP05);
            EPOP05.setBounds(760, 130, 44, EPOP05.getPreferredSize().height);

            //---- EPSN06 ----
            EPSN06.setComponentPopupMenu(BTD);
            EPSN06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPSN06.setModel(new SpinnerListModel(new String[] {" ", "C", "D"}) {
              { setValue("C"); }
            });
            EPSN06.setName("EPSN06");
            xTitledPanel1ContentContainer.add(EPSN06);
            EPSN06.setBounds(350, 155, 44, EPSN06.getPreferredSize().height);

            //---- EPOP06 ----
            EPOP06.setComponentPopupMenu(BTD);
            EPOP06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPOP06.setModel(new SpinnerListModel(new String[] {" ", "%", "-", "+"}));
            EPOP06.setName("EPOP06");
            xTitledPanel1ContentContainer.add(EPOP06);
            EPOP06.setBounds(760, 155, 44, EPOP06.getPreferredSize().height);

            //---- EPSN07 ----
            EPSN07.setComponentPopupMenu(BTD);
            EPSN07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPSN07.setModel(new SpinnerListModel(new String[] {" ", "C", "D"}) {
              { setValue("C"); }
            });
            EPSN07.setName("EPSN07");
            xTitledPanel1ContentContainer.add(EPSN07);
            EPSN07.setBounds(350, 180, 44, EPSN07.getPreferredSize().height);

            //---- EPOP07 ----
            EPOP07.setComponentPopupMenu(BTD);
            EPOP07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPOP07.setModel(new SpinnerListModel(new String[] {" ", "%", "-", "+"}));
            EPOP07.setName("EPOP07");
            xTitledPanel1ContentContainer.add(EPOP07);
            EPOP07.setBounds(760, 180, 44, EPOP07.getPreferredSize().height);

            //---- EPSN08 ----
            EPSN08.setComponentPopupMenu(BTD);
            EPSN08.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPSN08.setModel(new SpinnerListModel(new String[] {" ", "C", "D"}) {
              { setValue("C"); }
            });
            EPSN08.setName("EPSN08");
            xTitledPanel1ContentContainer.add(EPSN08);
            EPSN08.setBounds(350, 205, 44, EPSN08.getPreferredSize().height);

            //---- EPOP08 ----
            EPOP08.setComponentPopupMenu(BTD);
            EPOP08.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPOP08.setModel(new SpinnerListModel(new String[] {" ", "%", "-", "+"}));
            EPOP08.setName("EPOP08");
            xTitledPanel1ContentContainer.add(EPOP08);
            EPOP08.setBounds(760, 205, 44, EPOP08.getPreferredSize().height);

            //---- EPSN09 ----
            EPSN09.setComponentPopupMenu(BTD);
            EPSN09.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPSN09.setModel(new SpinnerListModel(new String[] {" ", "C", "D"}) {
              { setValue("C"); }
            });
            EPSN09.setName("EPSN09");
            xTitledPanel1ContentContainer.add(EPSN09);
            EPSN09.setBounds(350, 230, 44, EPSN09.getPreferredSize().height);

            //---- EPOP09 ----
            EPOP09.setComponentPopupMenu(BTD);
            EPOP09.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPOP09.setModel(new SpinnerListModel(new String[] {" ", "%", "-", "+"}));
            EPOP09.setName("EPOP09");
            xTitledPanel1ContentContainer.add(EPOP09);
            EPOP09.setBounds(760, 230, 44, EPOP09.getPreferredSize().height);

            //---- EPSN10 ----
            EPSN10.setComponentPopupMenu(BTD);
            EPSN10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPSN10.setModel(new SpinnerListModel(new String[] {" ", "C", "D"}) {
              { setValue("C"); }
            });
            EPSN10.setName("EPSN10");
            xTitledPanel1ContentContainer.add(EPSN10);
            EPSN10.setBounds(350, 255, 44, EPSN10.getPreferredSize().height);

            //---- EPOP10 ----
            EPOP10.setComponentPopupMenu(BTD);
            EPOP10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPOP10.setModel(new SpinnerListModel(new String[] {" ", "%", "-", "+"}));
            EPOP10.setName("EPOP10");
            xTitledPanel1ContentContainer.add(EPOP10);
            EPOP10.setBounds(760, 255, 44, EPOP10.getPreferredSize().height);

            //---- EPB011 ----
            EPB011.setText("");
            EPB011.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB011.setName("EPB011");
            xTitledPanel1ContentContainer.add(EPB011);
            EPB011.setBounds(new Rectangle(new Point(810, 30), EPB011.getPreferredSize()));

            //---- EPB012 ----
            EPB012.setText("");
            EPB012.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB012.setName("EPB012");
            xTitledPanel1ContentContainer.add(EPB012);
            EPB012.setBounds(new Rectangle(new Point(828, 30), EPB012.getPreferredSize()));

            //---- EPB013 ----
            EPB013.setText("");
            EPB013.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB013.setName("EPB013");
            xTitledPanel1ContentContainer.add(EPB013);
            EPB013.setBounds(new Rectangle(new Point(846, 30), EPB013.getPreferredSize()));

            //---- EPB014 ----
            EPB014.setText("");
            EPB014.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB014.setName("EPB014");
            xTitledPanel1ContentContainer.add(EPB014);
            EPB014.setBounds(new Rectangle(new Point(864, 30), EPB014.getPreferredSize()));

            //---- EPB015 ----
            EPB015.setText("");
            EPB015.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB015.setName("EPB015");
            xTitledPanel1ContentContainer.add(EPB015);
            EPB015.setBounds(new Rectangle(new Point(882, 30), EPB015.getPreferredSize()));

            //---- EPB016 ----
            EPB016.setText("");
            EPB016.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB016.setName("EPB016");
            xTitledPanel1ContentContainer.add(EPB016);
            EPB016.setBounds(new Rectangle(new Point(900, 30), EPB016.getPreferredSize()));

            //---- EPB021 ----
            EPB021.setText("");
            EPB021.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB021.setName("EPB021");
            xTitledPanel1ContentContainer.add(EPB021);
            EPB021.setBounds(new Rectangle(new Point(810, 55), EPB021.getPreferredSize()));

            //---- EPB022 ----
            EPB022.setText("");
            EPB022.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB022.setName("EPB022");
            xTitledPanel1ContentContainer.add(EPB022);
            EPB022.setBounds(new Rectangle(new Point(828, 55), EPB022.getPreferredSize()));

            //---- EPB023 ----
            EPB023.setText("");
            EPB023.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB023.setName("EPB023");
            xTitledPanel1ContentContainer.add(EPB023);
            EPB023.setBounds(new Rectangle(new Point(846, 55), EPB023.getPreferredSize()));

            //---- EPB024 ----
            EPB024.setText("");
            EPB024.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB024.setName("EPB024");
            xTitledPanel1ContentContainer.add(EPB024);
            EPB024.setBounds(new Rectangle(new Point(864, 55), EPB024.getPreferredSize()));

            //---- EPB025 ----
            EPB025.setText("");
            EPB025.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB025.setName("EPB025");
            xTitledPanel1ContentContainer.add(EPB025);
            EPB025.setBounds(new Rectangle(new Point(882, 55), EPB025.getPreferredSize()));

            //---- EPB026 ----
            EPB026.setText("");
            EPB026.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB026.setName("EPB026");
            xTitledPanel1ContentContainer.add(EPB026);
            EPB026.setBounds(new Rectangle(new Point(900, 55), EPB026.getPreferredSize()));

            //---- EPB031 ----
            EPB031.setText("");
            EPB031.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB031.setName("EPB031");
            xTitledPanel1ContentContainer.add(EPB031);
            EPB031.setBounds(new Rectangle(new Point(810, 80), EPB031.getPreferredSize()));

            //---- EPB032 ----
            EPB032.setText("");
            EPB032.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB032.setName("EPB032");
            xTitledPanel1ContentContainer.add(EPB032);
            EPB032.setBounds(new Rectangle(new Point(828, 80), EPB032.getPreferredSize()));

            //---- EPB033 ----
            EPB033.setText("");
            EPB033.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB033.setName("EPB033");
            xTitledPanel1ContentContainer.add(EPB033);
            EPB033.setBounds(new Rectangle(new Point(846, 80), EPB033.getPreferredSize()));

            //---- EPB034 ----
            EPB034.setText("");
            EPB034.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB034.setName("EPB034");
            xTitledPanel1ContentContainer.add(EPB034);
            EPB034.setBounds(new Rectangle(new Point(864, 80), EPB034.getPreferredSize()));

            //---- EPB035 ----
            EPB035.setText("");
            EPB035.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB035.setName("EPB035");
            xTitledPanel1ContentContainer.add(EPB035);
            EPB035.setBounds(new Rectangle(new Point(882, 80), EPB035.getPreferredSize()));

            //---- EPB036 ----
            EPB036.setText("");
            EPB036.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB036.setName("EPB036");
            xTitledPanel1ContentContainer.add(EPB036);
            EPB036.setBounds(new Rectangle(new Point(900, 80), EPB036.getPreferredSize()));

            //---- EPB041 ----
            EPB041.setText("");
            EPB041.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB041.setName("EPB041");
            xTitledPanel1ContentContainer.add(EPB041);
            EPB041.setBounds(new Rectangle(new Point(810, 105), EPB041.getPreferredSize()));

            //---- EPB042 ----
            EPB042.setText("");
            EPB042.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB042.setName("EPB042");
            xTitledPanel1ContentContainer.add(EPB042);
            EPB042.setBounds(new Rectangle(new Point(828, 105), EPB042.getPreferredSize()));

            //---- EPB043 ----
            EPB043.setText("");
            EPB043.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB043.setName("EPB043");
            xTitledPanel1ContentContainer.add(EPB043);
            EPB043.setBounds(new Rectangle(new Point(846, 105), EPB043.getPreferredSize()));

            //---- EPB044 ----
            EPB044.setText("");
            EPB044.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB044.setName("EPB044");
            xTitledPanel1ContentContainer.add(EPB044);
            EPB044.setBounds(new Rectangle(new Point(864, 105), EPB044.getPreferredSize()));

            //---- EPB045 ----
            EPB045.setText("");
            EPB045.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB045.setName("EPB045");
            xTitledPanel1ContentContainer.add(EPB045);
            EPB045.setBounds(new Rectangle(new Point(882, 105), EPB045.getPreferredSize()));

            //---- EPB046 ----
            EPB046.setText("");
            EPB046.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB046.setName("EPB046");
            xTitledPanel1ContentContainer.add(EPB046);
            EPB046.setBounds(new Rectangle(new Point(900, 105), EPB046.getPreferredSize()));

            //---- EPB051 ----
            EPB051.setText("");
            EPB051.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB051.setName("EPB051");
            xTitledPanel1ContentContainer.add(EPB051);
            EPB051.setBounds(new Rectangle(new Point(810, 130), EPB051.getPreferredSize()));

            //---- EPB052 ----
            EPB052.setText("");
            EPB052.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB052.setName("EPB052");
            xTitledPanel1ContentContainer.add(EPB052);
            EPB052.setBounds(new Rectangle(new Point(828, 130), EPB052.getPreferredSize()));

            //---- EPB053 ----
            EPB053.setText("");
            EPB053.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB053.setName("EPB053");
            xTitledPanel1ContentContainer.add(EPB053);
            EPB053.setBounds(new Rectangle(new Point(846, 130), EPB053.getPreferredSize()));

            //---- EPB054 ----
            EPB054.setText("");
            EPB054.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB054.setName("EPB054");
            xTitledPanel1ContentContainer.add(EPB054);
            EPB054.setBounds(new Rectangle(new Point(864, 130), EPB054.getPreferredSize()));

            //---- EPB055 ----
            EPB055.setText("");
            EPB055.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB055.setName("EPB055");
            xTitledPanel1ContentContainer.add(EPB055);
            EPB055.setBounds(new Rectangle(new Point(882, 130), EPB055.getPreferredSize()));

            //---- EPB056 ----
            EPB056.setText("");
            EPB056.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB056.setName("EPB056");
            xTitledPanel1ContentContainer.add(EPB056);
            EPB056.setBounds(new Rectangle(new Point(900, 130), EPB056.getPreferredSize()));

            //---- EPB061 ----
            EPB061.setText("");
            EPB061.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB061.setName("EPB061");
            xTitledPanel1ContentContainer.add(EPB061);
            EPB061.setBounds(new Rectangle(new Point(810, 155), EPB061.getPreferredSize()));

            //---- EPB062 ----
            EPB062.setText("");
            EPB062.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB062.setName("EPB062");
            xTitledPanel1ContentContainer.add(EPB062);
            EPB062.setBounds(new Rectangle(new Point(828, 155), EPB062.getPreferredSize()));

            //---- EPB063 ----
            EPB063.setText("");
            EPB063.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB063.setName("EPB063");
            xTitledPanel1ContentContainer.add(EPB063);
            EPB063.setBounds(new Rectangle(new Point(846, 155), EPB063.getPreferredSize()));

            //---- EPB064 ----
            EPB064.setText("");
            EPB064.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB064.setName("EPB064");
            xTitledPanel1ContentContainer.add(EPB064);
            EPB064.setBounds(new Rectangle(new Point(864, 155), EPB064.getPreferredSize()));

            //---- EPB065 ----
            EPB065.setText("");
            EPB065.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB065.setName("EPB065");
            xTitledPanel1ContentContainer.add(EPB065);
            EPB065.setBounds(new Rectangle(new Point(882, 155), EPB065.getPreferredSize()));

            //---- EPB066 ----
            EPB066.setText("");
            EPB066.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB066.setName("EPB066");
            xTitledPanel1ContentContainer.add(EPB066);
            EPB066.setBounds(new Rectangle(new Point(900, 155), EPB066.getPreferredSize()));

            //---- EPB071 ----
            EPB071.setText("");
            EPB071.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB071.setName("EPB071");
            xTitledPanel1ContentContainer.add(EPB071);
            EPB071.setBounds(new Rectangle(new Point(810, 180), EPB071.getPreferredSize()));

            //---- EPB072 ----
            EPB072.setText("");
            EPB072.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB072.setName("EPB072");
            xTitledPanel1ContentContainer.add(EPB072);
            EPB072.setBounds(new Rectangle(new Point(828, 180), EPB072.getPreferredSize()));

            //---- EPB073 ----
            EPB073.setText("");
            EPB073.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB073.setName("EPB073");
            xTitledPanel1ContentContainer.add(EPB073);
            EPB073.setBounds(new Rectangle(new Point(846, 180), EPB073.getPreferredSize()));

            //---- EPB074 ----
            EPB074.setText("");
            EPB074.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB074.setName("EPB074");
            xTitledPanel1ContentContainer.add(EPB074);
            EPB074.setBounds(new Rectangle(new Point(864, 180), EPB074.getPreferredSize()));

            //---- EPB075 ----
            EPB075.setText("");
            EPB075.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB075.setName("EPB075");
            xTitledPanel1ContentContainer.add(EPB075);
            EPB075.setBounds(new Rectangle(new Point(882, 180), EPB075.getPreferredSize()));

            //---- EPB076 ----
            EPB076.setText("");
            EPB076.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB076.setName("EPB076");
            xTitledPanel1ContentContainer.add(EPB076);
            EPB076.setBounds(new Rectangle(new Point(900, 180), EPB076.getPreferredSize()));

            //---- EPB081 ----
            EPB081.setText("");
            EPB081.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB081.setName("EPB081");
            xTitledPanel1ContentContainer.add(EPB081);
            EPB081.setBounds(new Rectangle(new Point(810, 205), EPB081.getPreferredSize()));

            //---- EPB082 ----
            EPB082.setText("");
            EPB082.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB082.setName("EPB082");
            xTitledPanel1ContentContainer.add(EPB082);
            EPB082.setBounds(new Rectangle(new Point(828, 205), EPB082.getPreferredSize()));

            //---- EPB083 ----
            EPB083.setText("");
            EPB083.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB083.setName("EPB083");
            xTitledPanel1ContentContainer.add(EPB083);
            EPB083.setBounds(new Rectangle(new Point(846, 205), EPB083.getPreferredSize()));

            //---- EPB084 ----
            EPB084.setText("");
            EPB084.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB084.setName("EPB084");
            xTitledPanel1ContentContainer.add(EPB084);
            EPB084.setBounds(new Rectangle(new Point(864, 205), EPB084.getPreferredSize()));

            //---- EPB085 ----
            EPB085.setText("");
            EPB085.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB085.setName("EPB085");
            xTitledPanel1ContentContainer.add(EPB085);
            EPB085.setBounds(new Rectangle(new Point(882, 205), EPB085.getPreferredSize()));

            //---- EPB086 ----
            EPB086.setText("");
            EPB086.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB086.setName("EPB086");
            xTitledPanel1ContentContainer.add(EPB086);
            EPB086.setBounds(new Rectangle(new Point(900, 205), EPB086.getPreferredSize()));

            //---- EPB091 ----
            EPB091.setText("");
            EPB091.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB091.setName("EPB091");
            xTitledPanel1ContentContainer.add(EPB091);
            EPB091.setBounds(new Rectangle(new Point(810, 230), EPB091.getPreferredSize()));

            //---- EPB092 ----
            EPB092.setText("");
            EPB092.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB092.setName("EPB092");
            xTitledPanel1ContentContainer.add(EPB092);
            EPB092.setBounds(new Rectangle(new Point(828, 230), EPB092.getPreferredSize()));

            //---- EPB093 ----
            EPB093.setText("");
            EPB093.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB093.setName("EPB093");
            xTitledPanel1ContentContainer.add(EPB093);
            EPB093.setBounds(new Rectangle(new Point(846, 230), EPB093.getPreferredSize()));

            //---- EPB094 ----
            EPB094.setText("");
            EPB094.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB094.setName("EPB094");
            xTitledPanel1ContentContainer.add(EPB094);
            EPB094.setBounds(new Rectangle(new Point(864, 230), EPB094.getPreferredSize()));

            //---- EPB095 ----
            EPB095.setText("");
            EPB095.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB095.setName("EPB095");
            xTitledPanel1ContentContainer.add(EPB095);
            EPB095.setBounds(new Rectangle(new Point(882, 230), EPB095.getPreferredSize()));

            //---- EPB096 ----
            EPB096.setText("");
            EPB096.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB096.setName("EPB096");
            xTitledPanel1ContentContainer.add(EPB096);
            EPB096.setBounds(new Rectangle(new Point(900, 230), EPB096.getPreferredSize()));

            //---- EPB101 ----
            EPB101.setText("");
            EPB101.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB101.setName("EPB101");
            xTitledPanel1ContentContainer.add(EPB101);
            EPB101.setBounds(new Rectangle(new Point(810, 255), EPB101.getPreferredSize()));

            //---- EPB102 ----
            EPB102.setText("");
            EPB102.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB102.setName("EPB102");
            xTitledPanel1ContentContainer.add(EPB102);
            EPB102.setBounds(new Rectangle(new Point(828, 255), EPB102.getPreferredSize()));

            //---- EPB103 ----
            EPB103.setText("");
            EPB103.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB103.setName("EPB103");
            xTitledPanel1ContentContainer.add(EPB103);
            EPB103.setBounds(new Rectangle(new Point(846, 255), EPB103.getPreferredSize()));

            //---- EPB104 ----
            EPB104.setText("");
            EPB104.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB104.setName("EPB104");
            xTitledPanel1ContentContainer.add(EPB104);
            EPB104.setBounds(new Rectangle(new Point(864, 255), EPB104.getPreferredSize()));

            //---- EPB105 ----
            EPB105.setText("");
            EPB105.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB105.setName("EPB105");
            xTitledPanel1ContentContainer.add(EPB105);
            EPB105.setBounds(new Rectangle(new Point(882, 255), EPB105.getPreferredSize()));

            //---- EPB106 ----
            EPB106.setText("");
            EPB106.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPB106.setName("EPB106");
            xTitledPanel1ContentContainer.add(EPB106);
            EPB106.setBounds(new Rectangle(new Point(900, 255), EPB106.getPreferredSize()));

            //---- OBJ_208 ----
            OBJ_208.setText("O");
            OBJ_208.setName("OBJ_208");
            xTitledPanel1ContentContainer.add(OBJ_208);
            OBJ_208.setBounds(760, 5, 13, 25);

            //---- OBJ_209 ----
            OBJ_209.setText("1");
            OBJ_209.setName("OBJ_209");
            xTitledPanel1ContentContainer.add(OBJ_209);
            OBJ_209.setBounds(813, 5, 12, 25);

            //---- OBJ_233 ----
            OBJ_233.setText("2");
            OBJ_233.setName("OBJ_233");
            xTitledPanel1ContentContainer.add(OBJ_233);
            OBJ_233.setBounds(831, 5, 12, 25);

            //---- OBJ_234 ----
            OBJ_234.setText("3");
            OBJ_234.setName("OBJ_234");
            xTitledPanel1ContentContainer.add(OBJ_234);
            OBJ_234.setBounds(849, 5, 12, 25);

            //---- OBJ_235 ----
            OBJ_235.setText("4");
            OBJ_235.setName("OBJ_235");
            xTitledPanel1ContentContainer.add(OBJ_235);
            OBJ_235.setBounds(867, 5, 12, 25);

            //---- OBJ_236 ----
            OBJ_236.setText("5");
            OBJ_236.setName("OBJ_236");
            xTitledPanel1ContentContainer.add(OBJ_236);
            OBJ_236.setBounds(885, 5, 12, 25);

            //---- OBJ_237 ----
            OBJ_237.setText("6");
            OBJ_237.setName("OBJ_237");
            xTitledPanel1ContentContainer.add(OBJ_237);
            OBJ_237.setBounds(903, 5, 12, 25);

            //---- EPLI01 ----
            EPLI01.setComponentPopupMenu(BTD);
            EPLI01.setFont(EPLI01.getFont().deriveFont(EPLI01.getFont().getSize() - 2f));
            EPLI01.setName("EPLI01");
            xTitledPanel1ContentContainer.add(EPLI01);
            EPLI01.setBounds(395, 30, 364, EPLI01.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(xTitledPanel1);
          xTitledPanel1.setBounds(10, 181, 1000, xTitledPanel1.getPreferredSize().height);

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //---- OBJ_212 ----
            OBJ_212.setText("N\u00b0base");
            OBJ_212.setFont(OBJ_212.getFont().deriveFont(OBJ_212.getFont().getStyle() | Font.BOLD));
            OBJ_212.setName("OBJ_212");
            xTitledPanel2ContentContainer.add(OBJ_212);
            OBJ_212.setBounds(25, 5, 67, 20);

            //---- EPTA01 ----
            EPTA01.setName("EPTA01");
            xTitledPanel2ContentContainer.add(EPTA01);
            EPTA01.setBounds(105, 25, 68, EPTA01.getPreferredSize().height);

            //---- EPTA02 ----
            EPTA02.setName("EPTA02");
            xTitledPanel2ContentContainer.add(EPTA02);
            EPTA02.setBounds(214, 25, 68, EPTA02.getPreferredSize().height);

            //---- EPTA03 ----
            EPTA03.setName("EPTA03");
            xTitledPanel2ContentContainer.add(EPTA03);
            EPTA03.setBounds(323, 25, 68, EPTA03.getPreferredSize().height);

            //---- EPTA04 ----
            EPTA04.setName("EPTA04");
            xTitledPanel2ContentContainer.add(EPTA04);
            EPTA04.setBounds(432, 25, 68, EPTA04.getPreferredSize().height);

            //---- EPTA05 ----
            EPTA05.setName("EPTA05");
            xTitledPanel2ContentContainer.add(EPTA05);
            EPTA05.setBounds(541, 25, 68, EPTA05.getPreferredSize().height);

            //---- EPTA06 ----
            EPTA06.setName("EPTA06");
            xTitledPanel2ContentContainer.add(EPTA06);
            EPTA06.setBounds(650, 25, 68, EPTA06.getPreferredSize().height);

            //---- OBJ_219 ----
            OBJ_219.setText("Taux");
            OBJ_219.setFont(OBJ_219.getFont().deriveFont(OBJ_219.getFont().getStyle() | Font.BOLD));
            OBJ_219.setName("OBJ_219");
            xTitledPanel2ContentContainer.add(OBJ_219);
            OBJ_219.setBounds(25, 25, 52, 28);

            //---- OBJ_213 ----
            OBJ_213.setText("1");
            OBJ_213.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_213.setFont(OBJ_213.getFont().deriveFont(OBJ_213.getFont().getStyle() | Font.BOLD));
            OBJ_213.setName("OBJ_213");
            xTitledPanel2ContentContainer.add(OBJ_213);
            OBJ_213.setBounds(105, 5, 68, 20);

            //---- OBJ_214 ----
            OBJ_214.setText("2");
            OBJ_214.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_214.setFont(OBJ_214.getFont().deriveFont(OBJ_214.getFont().getStyle() | Font.BOLD));
            OBJ_214.setName("OBJ_214");
            xTitledPanel2ContentContainer.add(OBJ_214);
            OBJ_214.setBounds(214, 5, 68, 20);

            //---- OBJ_215 ----
            OBJ_215.setText("3");
            OBJ_215.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_215.setFont(OBJ_215.getFont().deriveFont(OBJ_215.getFont().getStyle() | Font.BOLD));
            OBJ_215.setName("OBJ_215");
            xTitledPanel2ContentContainer.add(OBJ_215);
            OBJ_215.setBounds(323, 5, 68, 20);

            //---- OBJ_216 ----
            OBJ_216.setText("4");
            OBJ_216.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_216.setFont(OBJ_216.getFont().deriveFont(OBJ_216.getFont().getStyle() | Font.BOLD));
            OBJ_216.setName("OBJ_216");
            xTitledPanel2ContentContainer.add(OBJ_216);
            OBJ_216.setBounds(432, 5, 68, 20);

            //---- OBJ_217 ----
            OBJ_217.setText("5");
            OBJ_217.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_217.setFont(OBJ_217.getFont().deriveFont(OBJ_217.getFont().getStyle() | Font.BOLD));
            OBJ_217.setName("OBJ_217");
            xTitledPanel2ContentContainer.add(OBJ_217);
            OBJ_217.setBounds(541, 5, 68, 20);

            //---- OBJ_218 ----
            OBJ_218.setText("6");
            OBJ_218.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_218.setFont(OBJ_218.getFont().deriveFont(OBJ_218.getFont().getStyle() | Font.BOLD));
            OBJ_218.setName("OBJ_218");
            xTitledPanel2ContentContainer.add(OBJ_218);
            OBJ_218.setBounds(650, 5, 68, 20);

            //---- OBJ_220 ----
            OBJ_220.setText("%");
            OBJ_220.setName("OBJ_220");
            xTitledPanel2ContentContainer.add(OBJ_220);
            OBJ_220.setBounds(175, 25, 15, 28);

            //---- OBJ_221 ----
            OBJ_221.setText("%");
            OBJ_221.setName("OBJ_221");
            xTitledPanel2ContentContainer.add(OBJ_221);
            OBJ_221.setBounds(284, 25, 15, 28);

            //---- OBJ_222 ----
            OBJ_222.setText("%");
            OBJ_222.setName("OBJ_222");
            xTitledPanel2ContentContainer.add(OBJ_222);
            OBJ_222.setBounds(393, 25, 15, 28);

            //---- OBJ_223 ----
            OBJ_223.setText("%");
            OBJ_223.setName("OBJ_223");
            xTitledPanel2ContentContainer.add(OBJ_223);
            OBJ_223.setBounds(502, 25, 15, 28);

            //---- OBJ_224 ----
            OBJ_224.setText("%");
            OBJ_224.setName("OBJ_224");
            xTitledPanel2ContentContainer.add(OBJ_224);
            OBJ_224.setBounds(611, 25, 15, 28);

            //---- OBJ_225 ----
            OBJ_225.setText("%");
            OBJ_225.setName("OBJ_225");
            xTitledPanel2ContentContainer.add(OBJ_225);
            OBJ_225.setBounds(720, 25, 15, 28);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(xTitledPanel2);
          xTitledPanel2.setBounds(10, 499, 1000, 87);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_40;
  private XRiTextField INDETB;
  private JLabel OBJ_33;
  private XRiTextField INDCOD;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel3;
  private XRiComboBox EPTPC;
  private XRiComboBox EPTYP;
  private XRiTextField EPLIB;
  private XRiTextField EPLIT;
  private JLabel OBJ_229;
  private JLabel OBJ_199;
  private JLabel OBJ_230;
  private JLabel OBJ_200;
  private JLabel OBJ_36;
  private JLabel OBJ_38;
  private JLabel OBJ_201;
  private JLabel OBJ_37;
  private SNBoutonDetail OBJ_48;
  private XRiTextField EPNAT;
  private XRiTextField EPACT;
  private JLabel OBJ_202;
  private XRiTextField EPSAN;
  private XRiTextField EPJO;
  private XRiTextField EPCLB;
  private JLabel LAXE1;
  private XRiTextField EPAA1;
  private JLabel LAXE2;
  private XRiTextField EPAA2;
  private JLabel LAXE3;
  private XRiTextField EPAA3;
  private JLabel LAXE4;
  private XRiTextField EPAA4;
  private JLabel label5;
  private XRiTextField EPAA5;
  private JLabel label6;
  private XRiTextField EPAA6;
  private JLabel label8;
  private JLabel label7;
  private XRiTextField EPRCG;
  private XRiTextField EPRCA;
  private JXTitledPanel xTitledPanel1;
  private XRiTextField EPLI02;
  private XRiTextField EPLI03;
  private XRiTextField EPLI04;
  private XRiTextField EPLI05;
  private XRiTextField EPLI06;
  private XRiTextField EPLI07;
  private XRiTextField EPLI08;
  private XRiTextField EPLI09;
  private XRiTextField EPLI10;
  private XRiTextField EPIN01;
  private XRiTextField EPIN02;
  private XRiTextField EPIN03;
  private XRiTextField EPIN04;
  private XRiTextField EPIN05;
  private XRiTextField EPIN06;
  private XRiTextField EPIN07;
  private XRiTextField EPIN08;
  private XRiTextField EPIN09;
  private XRiTextField EPIN10;
  private JLabel OBJ_207;
  private JLabel OBJ_210;
  private XRiTextField NCG1X;
  private XRiTextField EPNA01;
  private XRiTextField EPCP01;
  private XRiTextField NCG2X;
  private XRiTextField EPNA02;
  private XRiTextField EPCP02;
  private XRiTextField NCG3X;
  private XRiTextField EPNA03;
  private XRiTextField EPCP03;
  private XRiTextField NCG4X;
  private XRiTextField EPNA04;
  private XRiTextField EPCP04;
  private XRiTextField NCG5X;
  private XRiTextField EPNA05;
  private XRiTextField EPCP05;
  private XRiTextField NCG6X;
  private XRiTextField EPNA06;
  private XRiTextField EPCP06;
  private XRiTextField NCG7X;
  private XRiTextField EPNA07;
  private XRiTextField EPCP07;
  private XRiTextField NCG8X;
  private XRiTextField EPNA08;
  private XRiTextField EPCP08;
  private XRiTextField NCG9X;
  private XRiTextField EPNA09;
  private XRiTextField EPCP09;
  private XRiTextField NCG10X;
  private XRiTextField EPNA10;
  private XRiTextField EPCP10;
  private JLabel OBJ_203;
  private JLabel OBJ_204;
  private JLabel OBJ_205;
  private JLabel OBJ_206;
  private XRiSpinner EPSN01;
  private XRiSpinner EPOP01;
  private XRiSpinner EPSN02;
  private XRiSpinner EPOP02;
  private XRiSpinner EPSN03;
  private XRiSpinner EPOP03;
  private XRiSpinner EPSN04;
  private XRiSpinner EPOP04;
  private XRiSpinner EPSN05;
  private XRiSpinner EPOP05;
  private XRiSpinner EPSN06;
  private XRiSpinner EPOP06;
  private XRiSpinner EPSN07;
  private XRiSpinner EPOP07;
  private XRiSpinner EPSN08;
  private XRiSpinner EPOP08;
  private XRiSpinner EPSN09;
  private XRiSpinner EPOP09;
  private XRiSpinner EPSN10;
  private XRiSpinner EPOP10;
  private XRiCheckBox EPB011;
  private XRiCheckBox EPB012;
  private XRiCheckBox EPB013;
  private XRiCheckBox EPB014;
  private XRiCheckBox EPB015;
  private XRiCheckBox EPB016;
  private XRiCheckBox EPB021;
  private XRiCheckBox EPB022;
  private XRiCheckBox EPB023;
  private XRiCheckBox EPB024;
  private XRiCheckBox EPB025;
  private XRiCheckBox EPB026;
  private XRiCheckBox EPB031;
  private XRiCheckBox EPB032;
  private XRiCheckBox EPB033;
  private XRiCheckBox EPB034;
  private XRiCheckBox EPB035;
  private XRiCheckBox EPB036;
  private XRiCheckBox EPB041;
  private XRiCheckBox EPB042;
  private XRiCheckBox EPB043;
  private XRiCheckBox EPB044;
  private XRiCheckBox EPB045;
  private XRiCheckBox EPB046;
  private XRiCheckBox EPB051;
  private XRiCheckBox EPB052;
  private XRiCheckBox EPB053;
  private XRiCheckBox EPB054;
  private XRiCheckBox EPB055;
  private XRiCheckBox EPB056;
  private XRiCheckBox EPB061;
  private XRiCheckBox EPB062;
  private XRiCheckBox EPB063;
  private XRiCheckBox EPB064;
  private XRiCheckBox EPB065;
  private XRiCheckBox EPB066;
  private XRiCheckBox EPB071;
  private XRiCheckBox EPB072;
  private XRiCheckBox EPB073;
  private XRiCheckBox EPB074;
  private XRiCheckBox EPB075;
  private XRiCheckBox EPB076;
  private XRiCheckBox EPB081;
  private XRiCheckBox EPB082;
  private XRiCheckBox EPB083;
  private XRiCheckBox EPB084;
  private XRiCheckBox EPB085;
  private XRiCheckBox EPB086;
  private XRiCheckBox EPB091;
  private XRiCheckBox EPB092;
  private XRiCheckBox EPB093;
  private XRiCheckBox EPB094;
  private XRiCheckBox EPB095;
  private XRiCheckBox EPB096;
  private XRiCheckBox EPB101;
  private XRiCheckBox EPB102;
  private XRiCheckBox EPB103;
  private XRiCheckBox EPB104;
  private XRiCheckBox EPB105;
  private XRiCheckBox EPB106;
  private JLabel OBJ_208;
  private JLabel OBJ_209;
  private JLabel OBJ_233;
  private JLabel OBJ_234;
  private JLabel OBJ_235;
  private JLabel OBJ_236;
  private JLabel OBJ_237;
  private XRiTextField EPLI01;
  private JXTitledPanel xTitledPanel2;
  private JLabel OBJ_212;
  private XRiTextField EPTA01;
  private XRiTextField EPTA02;
  private XRiTextField EPTA03;
  private XRiTextField EPTA04;
  private XRiTextField EPTA05;
  private XRiTextField EPTA06;
  private JLabel OBJ_219;
  private JLabel OBJ_213;
  private JLabel OBJ_214;
  private JLabel OBJ_215;
  private JLabel OBJ_216;
  private JLabel OBJ_217;
  private JLabel OBJ_218;
  private JLabel OBJ_220;
  private JLabel OBJ_221;
  private JLabel OBJ_222;
  private JLabel OBJ_223;
  private JLabel OBJ_224;
  private JLabel OBJ_225;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
