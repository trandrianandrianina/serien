
package ri.serien.libecranrpg.vcgm.VCGM67FM;
// Nom Fichier: pop_VCGM67FM_FMTF2_FMTF1_1079.java

import java.awt.Font;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class VCGM67FM_F2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM67FM_F2(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    // setDefaultButton(BT_ENTER);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Veuillez patienter..."));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_5 = new JLabel();

    //======== this ========
    setName("this");

    //---- OBJ_5 ----
    OBJ_5.setText("Pr\u00e9paration de l'affichage en cours");
    OBJ_5.setFont(OBJ_5.getFont().deriveFont(OBJ_5.getFont().getStyle() | Font.BOLD, OBJ_5.getFont().getSize() + 3f));
    OBJ_5.setName("OBJ_5");

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(35, 35, 35)
          .addComponent(OBJ_5, GroupLayout.PREFERRED_SIZE, 255, GroupLayout.PREFERRED_SIZE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(35, 35, 35)
          .addComponent(OBJ_5))
    );
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel OBJ_5;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
