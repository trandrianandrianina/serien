
package ri.serien.libecranrpg.vcgm.VCGMG7FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.vente.documentvente.snvendeur.SNVendeur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGMG7FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VCGMG7FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    
    // Ajout
    initDiverses();
    VALNUL.setValeursSelection("X", "");
    
    sNBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    sNBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    sNBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    


    pnlCaisse.setVisible(VALDEB.isVisible());
    
    // Etablissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "DEMETB");
    
    // Magasin
    sNMagasin.setSession(getSession());
    sNMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    sNMagasin.setTousAutorise(false);
    sNMagasin.setAucunAutorise(true);
    sNMagasin.charger(true);
    sNMagasin.setSelectionParChampRPG(lexique, "DEMMAG");
    
    // Vendeur
    snVendeur.setSession(getSession());
    snVendeur.setIdEtablissement(snEtablissement.getIdSelection());
    snVendeur.setTousAutorise(true);
    snVendeur.charger(false);
    snVendeur.setSelectionParChampRPG(lexique, "DEMVDE");
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "DEMETB");
    sNMagasin.renseignerChampRPG(lexique, "DEMMAG");
    snVendeur.renseignerChampRPG(lexique, "DEMVDE");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_presentation = new JPanel();
    p_logo = new JPanel();
    logoEtb = new JLabel();
    p_titre = new JPanel();
    titre = new JLabel();
    fond_titre = new JLabel();
    sNBarreBouton = new SNBarreBouton();
    sNPanelPrincipal1 = new SNPanelContenu();
    pnlGauche = new JPanel();
    pnlSelection = new SNPanelTitre();
    lbDate = new SNLabelChamp();
    DEMDAT = new XRiCalendrier();
    lbVendeur = new SNLabelChamp();
    snVendeur = new SNVendeur();
    pnlCaisse = new SNPanelTitre();
    lbFondDeCaisse = new SNLabelChamp();
    VALDEB = new XRiTextField();
    VALNUL = new XRiCheckBox();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbMagasin = new SNLabelChamp();
    sNMagasin = new SNMagasin();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //======== p_presentation ========
      {
        p_presentation.setPreferredSize(new Dimension(1020, 55));
        p_presentation.setMinimumSize(new Dimension(950, 55));
        p_presentation.setMaximumSize(new Dimension(32767, 55));
        p_presentation.setBackground(Color.white);
        p_presentation.setAutoscrolls(true);
        p_presentation.setFocusable(false);
        p_presentation.setName("p_presentation");
        p_presentation.setLayout(new BorderLayout());

        //======== p_logo ========
        {
          p_logo.setMinimumSize(new Dimension(120, 55));
          p_logo.setPreferredSize(new Dimension(120, 55));
          p_logo.setBackground(new Color(238, 239, 241));
          p_logo.setName("p_logo");
          p_logo.setLayout(null);

          //---- logoEtb ----
          logoEtb.setBorder(null);
          logoEtb.setName("logoEtb");
          p_logo.add(logoEtb);
          logoEtb.setBounds(70, 5, 45, 45);
        }
        p_presentation.add(p_logo, BorderLayout.EAST);

        //======== p_titre ========
        {
          p_titre.setBackground(new Color(238, 239, 241));
          p_titre.setPreferredSize(new Dimension(930, 0));
          p_titre.setMinimumSize(new Dimension(930, 0));
          p_titre.setName("p_titre");
          p_titre.setLayout(null);

          //---- titre ----
          titre.setBackground(new Color(198, 198, 200));
          titre.setText("Saisie de caisse et validation");
          titre.setHorizontalAlignment(SwingConstants.LEFT);
          titre.setFont(titre.getFont().deriveFont(titre.getFont().getStyle() | Font.BOLD, titre.getFont().getSize() + 6f));
          titre.setForeground(Color.darkGray);
          titre.setName("titre");
          p_titre.add(titre);
          titre.setBounds(10, 0, 600, 55);

          //---- fond_titre ----
          fond_titre.setFont(new Font("Arial", Font.BOLD, 18));
          fond_titre.setForeground(Color.white);
          fond_titre.setName("fond_titre");
          p_titre.add(fond_titre);
          fond_titre.setBounds(0, 0, 900, 55);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_titre.getComponentCount(); i++) {
              Rectangle bounds = p_titre.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_titre.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_titre.setMinimumSize(preferredSize);
            p_titre.setPreferredSize(preferredSize);
          }
        }
        p_presentation.add(p_titre, BorderLayout.CENTER);
      }
      p_nord.add(p_presentation);
    }
    add(p_nord, BorderLayout.PAGE_START);

    //---- sNBarreBouton ----
    sNBarreBouton.setName("sNBarreBouton");
    add(sNBarreBouton, BorderLayout.PAGE_END);

    //======== sNPanelPrincipal1 ========
    {
      sNPanelPrincipal1.setName("sNPanelPrincipal1");
      sNPanelPrincipal1.setLayout(new GridLayout());

      //======== pnlGauche ========
      {
        pnlGauche.setBackground(new Color(239, 239, 222));
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //======== pnlSelection ========
        {
          pnlSelection.setTitre("Crit\u00e8res de s\u00e9lection");
          pnlSelection.setName("pnlSelection");
          pnlSelection.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlSelection.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlSelection.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlSelection.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlSelection.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbDate ----
          lbDate.setText("Date");
          lbDate.setName("lbDate");
          pnlSelection.add(lbDate, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- DEMDAT ----
          DEMDAT.setPreferredSize(new Dimension(110, 30));
          DEMDAT.setMaximumSize(new Dimension(110, 30));
          DEMDAT.setMinimumSize(new Dimension(110, 30));
          DEMDAT.setName("DEMDAT");
          pnlSelection.add(DEMDAT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbVendeur ----
          lbVendeur.setText("Vendeur");
          lbVendeur.setName("lbVendeur");
          pnlSelection.add(lbVendeur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snVendeur ----
          snVendeur.setName("snVendeur");
          pnlSelection.add(snVendeur, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlCaisse ========
        {
          pnlCaisse.setTitre("Saisie");
          pnlCaisse.setName("pnlCaisse");
          pnlCaisse.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlCaisse.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlCaisse.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlCaisse.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlCaisse.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbFondDeCaisse ----
          lbFondDeCaisse.setText("Fond de caisse");
          lbFondDeCaisse.setName("lbFondDeCaisse");
          pnlCaisse.add(lbFondDeCaisse, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- VALDEB ----
          VALDEB.setPreferredSize(new Dimension(150, 28));
          VALDEB.setMinimumSize(new Dimension(150, 28));
          VALDEB.setMaximumSize(new Dimension(150, 28));
          VALDEB.setName("VALDEB");
          pnlCaisse.add(VALDEB, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- VALNUL ----
          VALNUL.setText("Ou cocher si le fond de caisse \u00e9gale z\u00e9ro");
          VALNUL.setFont(new Font("sansserif", Font.PLAIN, 14));
          VALNUL.setName("VALNUL");
          pnlCaisse.add(VALNUL, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlCaisse, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      sNPanelPrincipal1.add(pnlGauche);

      //======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //======== pnlEtablissement ========
        {
          pnlEtablissement.setTitre("Etablissement");
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbEtablissement ----
          lbEtablissement.setText("Etablissement");
          lbEtablissement.setName("lbEtablissement");
          pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snEtablissement ----
          snEtablissement.setName("snEtablissement");
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbMagasin ----
          lbMagasin.setText("Magasin");
          lbMagasin.setName("lbMagasin");
          pnlEtablissement.add(lbMagasin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- sNMagasin ----
          sNMagasin.setName("sNMagasin");
          pnlEtablissement.add(sNMagasin, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      sNPanelPrincipal1.add(pnlDroite);
    }
    add(sNPanelPrincipal1, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private JPanel p_presentation;
  private JPanel p_logo;
  private JLabel logoEtb;
  private JPanel p_titre;
  private JLabel titre;
  private JLabel fond_titre;
  private SNBarreBouton sNBarreBouton;
  private SNPanelContenu sNPanelPrincipal1;
  private JPanel pnlGauche;
  private SNPanelTitre pnlSelection;
  private SNLabelChamp lbDate;
  private XRiCalendrier DEMDAT;
  private SNLabelChamp lbVendeur;
  private SNVendeur snVendeur;
  private SNPanelTitre pnlCaisse;
  private SNLabelChamp lbFondDeCaisse;
  private XRiTextField VALDEB;
  private XRiCheckBox VALNUL;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbMagasin;
  private SNMagasin sNMagasin;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
