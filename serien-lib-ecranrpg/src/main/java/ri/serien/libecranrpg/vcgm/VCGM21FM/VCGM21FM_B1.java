
package ri.serien.libecranrpg.vcgm.VCGM21FM;
// Nom Fichier: i_VCGM21FM_FMTB1_FMTF1_492.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM21FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM21FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDefaultButton(bouton_valider);
    // setDialog(true);
    initDiverses();
    
    // Ajout
    initDiverses();
    DTNC1X.setValeursSelection("OUI", "NON");
    DTNC2X.setValeursSelection("OUI", "NON");
    
    

    
    p_bpresentation.setCodeEtablissement(INDSOC.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(INDSOC.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_105_OBJ_105.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ESC@")).trim());
    OBJ_106_OBJ_106.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MONT2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    gererLesErreurs("19");
    
    
    DTESC.setEnabled(lexique.isTrue("34"));
    DTNED.setEnabled(lexique.isTrue("34"));
    WDTMTT.setEnabled(lexique.isTrue("34"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    BTD = new JPopupMenu();
    OBJ_21 = new JMenuItem();
    OBJ_22 = new JMenuItem();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44_OBJ_44 = new JLabel();
    INDSOC = new XRiTextField();
    OBJ_45_OBJ_45 = new JLabel();
    INDNUM = new XRiTextField();
    OBJ_41_OBJ_41 = new JLabel();
    INDSUF = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    DTMTT = new XRiTextField();
    OBJ_48_OBJ_48 = new JLabel();
    DTDCRX = new XRiCalendrier();
    DTDECX = new XRiCalendrier();
    DTMRG = new XRiTextField();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_51_OBJ_51 = new JLabel();
    DTLIC = new XRiTextField();
    OBJ_52_OBJ_52 = new JLabel();
    DTNPCG = new XRiTextField();
    xTitledPanel2 = new JXTitledPanel();
    DENOM = new XRiTextField();
    DERUE = new XRiTextField();
    DELOC = new XRiTextField();
    DTNCGX = new XRiTextField();
    DTNCA = new XRiTextField();
    OBJ_58_OBJ_58 = new JLabel();
    DTCLA = new XRiTextField();
    DECDP = new XRiTextField();
    DEVIL = new XRiTextField();
    DEPAY = new XRiTextField();
    label7 = new JLabel();
    xTitledPanel3 = new JXTitledPanel();
    DTDRGX = new XRiTextField();
    DTDMEX = new XRiTextField();
    DTDDOX = new XRiTextField();
    OBJ_102_OBJ_102 = new JLabel();
    DTBDO = new XRiTextField();
    OBJ_103_OBJ_103 = new JLabel();
    DTNPR = new XRiTextField();
    DTNC2X = new XRiCheckBox();
    DTNC1X = new XRiCheckBox();
    DTNED = new XRiTextField();
    OBJ_104_OBJ_104 = new JLabel();
    OBJ_105_OBJ_105 = new JLabel();
    DTESC = new XRiTextField();
    OBJ_106_OBJ_106 = new JLabel();
    WDTMTT = new XRiTextField();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_21 ----
      OBJ_21.setText("Aide en ligne");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);

      //---- OBJ_22 ----
      OBJ_22.setText("Invite");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Saisie des ch\u00e8ques");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_44_OBJ_44 ----
          OBJ_44_OBJ_44.setText("Soci\u00e9t\u00e9");
          OBJ_44_OBJ_44.setName("OBJ_44_OBJ_44");
          p_tete_gauche.add(OBJ_44_OBJ_44);
          OBJ_44_OBJ_44.setBounds(5, 5, 50, 18);

          //---- INDSOC ----
          INDSOC.setComponentPopupMenu(BTD);
          INDSOC.setName("INDSOC");
          p_tete_gauche.add(INDSOC);
          INDSOC.setBounds(120, 0, 40, INDSOC.getPreferredSize().height);

          //---- OBJ_45_OBJ_45 ----
          OBJ_45_OBJ_45.setText("Num\u00e9ro indicatif");
          OBJ_45_OBJ_45.setName("OBJ_45_OBJ_45");
          p_tete_gauche.add(OBJ_45_OBJ_45);
          OBJ_45_OBJ_45.setBounds(200, 5, 110, 18);

          //---- INDNUM ----
          INDNUM.setComponentPopupMenu(BTD);
          INDNUM.setName("INDNUM");
          p_tete_gauche.add(INDNUM);
          INDNUM.setBounds(320, 0, 68, INDNUM.getPreferredSize().height);

          //---- OBJ_41_OBJ_41 ----
          OBJ_41_OBJ_41.setText("Suffixe");
          OBJ_41_OBJ_41.setName("OBJ_41_OBJ_41");
          p_tete_gauche.add(OBJ_41_OBJ_41);
          OBJ_41_OBJ_41.setBounds(440, 5, 49, 18);

          //---- INDSUF ----
          INDSUF.setComponentPopupMenu(BTD);
          INDSUF.setName("INDSUF");
          p_tete_gauche.add(INDSUF);
          INDSUF.setBounds(530, 0, 30, INDSUF.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Domiciliation manuelle");
            riSousMenu_bt6.setToolTipText("Domiciliation manuelle des ch\u00e8ques");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(850, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Ch\u00e8que"));
            panel1.setOpaque(false);
            panel1.setName("panel1");

            //---- DTMTT ----
            DTMTT.setComponentPopupMenu(BTD);
            DTMTT.setFont(DTMTT.getFont().deriveFont(DTMTT.getFont().getStyle() | Font.BOLD));
            DTMTT.setHorizontalAlignment(SwingConstants.RIGHT);
            DTMTT.setName("DTMTT");

            //---- OBJ_48_OBJ_48 ----
            OBJ_48_OBJ_48.setText("Montant");
            OBJ_48_OBJ_48.setName("OBJ_48_OBJ_48");

            //---- DTDCRX ----
            DTDCRX.setComponentPopupMenu(BTD);
            DTDCRX.setName("DTDCRX");

            //---- DTDECX ----
            DTDECX.setComponentPopupMenu(BTD);
            DTDECX.setName("DTDECX");

            //---- DTMRG ----
            DTMRG.setComponentPopupMenu(BTD);
            DTMRG.setName("DTMRG");

            //======== xTitledPanel1 ========
            {
              xTitledPanel1.setBorder(new DropShadowBorder());
              xTitledPanel1.setTitle("Libell\u00e9");
              xTitledPanel1.setName("xTitledPanel1");
              Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

              //---- OBJ_51_OBJ_51 ----
              OBJ_51_OBJ_51.setText("Origine comptable");
              OBJ_51_OBJ_51.setName("OBJ_51_OBJ_51");

              //---- DTLIC ----
              DTLIC.setComponentPopupMenu(BTD);
              DTLIC.setName("DTLIC");

              //---- OBJ_52_OBJ_52 ----
              OBJ_52_OBJ_52.setText("Num\u00e9ro de pi\u00e8ce");
              OBJ_52_OBJ_52.setName("OBJ_52_OBJ_52");

              //---- DTNPCG ----
              DTNPCG.setComponentPopupMenu(BTD);
              DTNPCG.setName("DTNPCG");

              GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
              xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
              xTitledPanel1ContentContainerLayout.setHorizontalGroup(
                xTitledPanel1ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(OBJ_51_OBJ_51, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(DTLIC, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                    .addGap(30, 30, 30)
                    .addComponent(OBJ_52_OBJ_52, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2)
                    .addComponent(DTNPCG, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE))
              );
              xTitledPanel1ContentContainerLayout.setVerticalGroup(
                xTitledPanel1ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(OBJ_51_OBJ_51))
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(DTLIC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(OBJ_52_OBJ_52))
                  .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(DTNPCG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              );
            }

            //======== xTitledPanel2 ========
            {
              xTitledPanel2.setBorder(new DropShadowBorder());
              xTitledPanel2.setTitle("Tiers");
              xTitledPanel2.setName("xTitledPanel2");
              Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();

              //---- DENOM ----
              DENOM.setComponentPopupMenu(BTD);
              DENOM.setName("DENOM");

              //---- DERUE ----
              DERUE.setComponentPopupMenu(BTD);
              DERUE.setName("DERUE");

              //---- DELOC ----
              DELOC.setComponentPopupMenu(BTD);
              DELOC.setName("DELOC");

              //---- DTNCGX ----
              DTNCGX.setToolTipText("Collectif");
              DTNCGX.setComponentPopupMenu(BTD);
              DTNCGX.setName("DTNCGX");

              //---- DTNCA ----
              DTNCA.setToolTipText("Auxilliaire");
              DTNCA.setComponentPopupMenu(BTD);
              DTNCA.setName("DTNCA");

              //---- OBJ_58_OBJ_58 ----
              OBJ_58_OBJ_58.setText("Classement");
              OBJ_58_OBJ_58.setName("OBJ_58_OBJ_58");

              //---- DTCLA ----
              DTCLA.setComponentPopupMenu(BTD);
              DTCLA.setName("DTCLA");

              //---- DECDP ----
              DECDP.setComponentPopupMenu(BTD);
              DECDP.setName("DECDP");

              //---- DEVIL ----
              DEVIL.setComponentPopupMenu(BTD);
              DEVIL.setName("DEVIL");

              //---- DEPAY ----
              DEPAY.setComponentPopupMenu(BTD);
              DEPAY.setName("DEPAY");

              //---- label7 ----
              label7.setText("Compte");
              label7.setName("label7");

              GroupLayout xTitledPanel2ContentContainerLayout = new GroupLayout(xTitledPanel2ContentContainer);
              xTitledPanel2ContentContainer.setLayout(xTitledPanel2ContentContainerLayout);
              xTitledPanel2ContentContainerLayout.setHorizontalGroup(
                xTitledPanel2ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(DENOM, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
                    .addGap(105, 105, 105)
                    .addComponent(DERUE, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(25, 25, 25)
                    .addComponent(label7)
                    .addGap(51, 51, 51)
                    .addComponent(DTNCGX, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(DTNCA, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addGap(185, 185, 185)
                    .addComponent(DELOC, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(25, 25, 25)
                    .addComponent(OBJ_58_OBJ_58, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                    .addGap(20, 20, 20)
                    .addComponent(DTCLA, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
                    .addGap(155, 155, 155)
                    .addComponent(DECDP, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                    .addGap(8, 8, 8)
                    .addComponent(DEVIL, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(435, 435, 435)
                    .addComponent(DEPAY, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
              );
              xTitledPanel2ContentContainerLayout.setVerticalGroup(
                xTitledPanel2ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                      .addComponent(DENOM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(DERUE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(7, 7, 7)
                    .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                      .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(label7))
                      .addComponent(DTNCGX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(DTNCA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(DELOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(2, 2, 2)
                    .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                      .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(OBJ_58_OBJ_58, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(DTCLA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(DECDP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(DEVIL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(2, 2, 2)
                    .addComponent(DEPAY, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              );
            }

            //======== xTitledPanel3 ========
            {
              xTitledPanel3.setBorder(new DropShadowBorder());
              xTitledPanel3.setName("xTitledPanel3");
              Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();

              //---- DTDRGX ----
              DTDRGX.setComponentPopupMenu(BTD);
              DTDRGX.setName("DTDRGX");

              //---- DTDMEX ----
              DTDMEX.setComponentPopupMenu(BTD);
              DTDMEX.setName("DTDMEX");

              //---- DTDDOX ----
              DTDDOX.setComponentPopupMenu(BTD);
              DTDDOX.setName("DTDDOX");

              //---- OBJ_102_OBJ_102 ----
              OBJ_102_OBJ_102.setText("Banque");
              OBJ_102_OBJ_102.setName("OBJ_102_OBJ_102");

              //---- DTBDO ----
              DTBDO.setComponentPopupMenu(BTD);
              DTBDO.setName("DTBDO");

              //---- OBJ_103_OBJ_103 ----
              OBJ_103_OBJ_103.setText("Pi\u00e8ce");
              OBJ_103_OBJ_103.setName("OBJ_103_OBJ_103");

              //---- DTNPR ----
              DTNPR.setComponentPopupMenu(BTD);
              DTNPR.setName("DTNPR");

              //---- DTNC2X ----
              DTNC2X.setText("Comptabilis\u00e9");
              DTNC2X.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              DTNC2X.setName("DTNC2X");

              //---- DTNC1X ----
              DTNC1X.setText("Comptabilis\u00e9");
              DTNC1X.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              DTNC1X.setName("DTNC1X");

              //---- DTNED ----
              DTNED.setComponentPopupMenu(BTD);
              DTNED.setName("DTNED");

              //---- OBJ_104_OBJ_104 ----
              OBJ_104_OBJ_104.setText("Pas.");
              OBJ_104_OBJ_104.setName("OBJ_104_OBJ_104");

              //---- OBJ_105_OBJ_105 ----
              OBJ_105_OBJ_105.setText("@ESC@");
              OBJ_105_OBJ_105.setName("OBJ_105_OBJ_105");

              //---- DTESC ----
              DTESC.setComponentPopupMenu(BTD);
              DTESC.setName("DTESC");

              //---- OBJ_106_OBJ_106 ----
              OBJ_106_OBJ_106.setText("@MONT2@");
              OBJ_106_OBJ_106.setName("OBJ_106_OBJ_106");

              //---- WDTMTT ----
              WDTMTT.setComponentPopupMenu(BTD);
              WDTMTT.setName("WDTMTT");

              //---- label4 ----
              label4.setText("Domiciliation");
              label4.setName("label4");

              //---- label5 ----
              label5.setText("Emission titre");
              label5.setName("label5");

              //---- label6 ----
              label6.setText("R\u00e9glement");
              label6.setName("label6");

              GroupLayout xTitledPanel3ContentContainerLayout = new GroupLayout(xTitledPanel3ContentContainer);
              xTitledPanel3ContentContainer.setLayout(xTitledPanel3ContentContainerLayout);
              xTitledPanel3ContentContainerLayout.setHorizontalGroup(
                xTitledPanel3ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                    .addGap(25, 25, 25)
                    .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                      .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                        .addComponent(label4)
                        .addGap(43, 43, 43)
                        .addComponent(DTDDOX, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                        .addGap(19, 19, 19)
                        .addComponent(OBJ_102_OBJ_102, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addComponent(DTBDO, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(OBJ_103_OBJ_103, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE)
                        .addGap(118, 118, 118)
                        .addComponent(OBJ_104_OBJ_104, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
                        .addGap(13, 13, 13)
                        .addComponent(OBJ_105_OBJ_105, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(OBJ_106_OBJ_106, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                        .addComponent(label5)
                        .addGap(39, 39, 39)
                        .addComponent(DTDMEX, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                        .addGap(129, 129, 129)
                        .addComponent(DTNPR, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)
                        .addComponent(DTNC1X, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(DTNED, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(DTESC, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(WDTMTT, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
                      .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                        .addComponent(label6)
                        .addGap(54, 54, 54)
                        .addComponent(DTDRGX, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                        .addGap(189, 189, 189)
                        .addComponent(DTNC2X, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE))))
              );
              xTitledPanel3ContentContainerLayout.setVerticalGroup(
                xTitledPanel3ContentContainerLayout.createParallelGroup()
                  .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                      .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(label4))
                      .addComponent(DTDDOX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(OBJ_102_OBJ_102))
                      .addComponent(DTBDO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(OBJ_103_OBJ_103))
                      .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(OBJ_104_OBJ_104))
                      .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(OBJ_105_OBJ_105))
                      .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(OBJ_106_OBJ_106)))
                    .addGap(4, 4, 4)
                    .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                      .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(label5))
                      .addComponent(DTDMEX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(DTNPR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(DTNC1X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addComponent(DTNED, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(DTESC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WDTMTT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(7, 7, 7)
                    .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                      .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(label6))
                      .addComponent(DTDRGX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(DTNC2X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
              );
            }

            //---- label1 ----
            label1.setText("Cr\u00e9ation");
            label1.setName("label1");

            //---- label2 ----
            label2.setText("Ech\u00e9ance");
            label2.setName("label2");

            //---- label3 ----
            label3.setText("Mode de r\u00e9glement");
            label3.setName("label3");

            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addContainerGap()
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(label1, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(DTDCRX, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                      .addGap(25, 25, 25)
                      .addComponent(label2, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(DTDECX, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                      .addGap(20, 20, 20)
                      .addComponent(OBJ_48_OBJ_48, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(DTMTT, GroupLayout.PREFERRED_SIZE, 126, GroupLayout.PREFERRED_SIZE)
                      .addGap(14, 14, 14)
                      .addComponent(label3, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(DTMRG, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                    .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addContainerGap(12, Short.MAX_VALUE))
            );
            panel1Layout.setVerticalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addContainerGap()
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(DTDCRX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DTDECX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DTMTT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DTMRG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addGroup(panel1Layout.createParallelGroup()
                        .addComponent(label1)
                        .addComponent(label2)
                        .addComponent(OBJ_48_OBJ_48)
                        .addComponent(label3))))
                  .addGap(18, 18, 18)
                  .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(10, 10, 10)
                  .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(10, 10, 10)
                  .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addContainerGap(13, Short.MAX_VALUE))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu BTD;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_22;
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44_OBJ_44;
  private XRiTextField INDSOC;
  private JLabel OBJ_45_OBJ_45;
  private XRiTextField INDNUM;
  private JLabel OBJ_41_OBJ_41;
  private XRiTextField INDSUF;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField DTMTT;
  private JLabel OBJ_48_OBJ_48;
  private XRiCalendrier DTDCRX;
  private XRiCalendrier DTDECX;
  private XRiTextField DTMRG;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_51_OBJ_51;
  private XRiTextField DTLIC;
  private JLabel OBJ_52_OBJ_52;
  private XRiTextField DTNPCG;
  private JXTitledPanel xTitledPanel2;
  private XRiTextField DENOM;
  private XRiTextField DERUE;
  private XRiTextField DELOC;
  private XRiTextField DTNCGX;
  private XRiTextField DTNCA;
  private JLabel OBJ_58_OBJ_58;
  private XRiTextField DTCLA;
  private XRiTextField DECDP;
  private XRiTextField DEVIL;
  private XRiTextField DEPAY;
  private JLabel label7;
  private JXTitledPanel xTitledPanel3;
  private XRiTextField DTDRGX;
  private XRiTextField DTDMEX;
  private XRiTextField DTDDOX;
  private JLabel OBJ_102_OBJ_102;
  private XRiTextField DTBDO;
  private JLabel OBJ_103_OBJ_103;
  private XRiTextField DTNPR;
  private XRiCheckBox DTNC2X;
  private XRiCheckBox DTNC1X;
  private XRiTextField DTNED;
  private JLabel OBJ_104_OBJ_104;
  private JLabel OBJ_105_OBJ_105;
  private XRiTextField DTESC;
  private JLabel OBJ_106_OBJ_106;
  private XRiTextField WDTMTT;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
