
package ri.serien.libecranrpg.vcgm.VCGM14FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGM14FM_A3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM14FM_A3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMTT@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Modification"));
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    WMTT01 = new XRiTextField();
    WMTT02 = new XRiTextField();
    WMTT03 = new XRiTextField();
    WMTT04 = new XRiTextField();
    WMTT05 = new XRiTextField();
    WMTT06 = new XRiTextField();
    WMTT07 = new XRiTextField();
    WMTT08 = new XRiTextField();
    WMTT09 = new XRiTextField();
    WMTT10 = new XRiTextField();
    WMTT11 = new XRiTextField();
    WMTT12 = new XRiTextField();
    WECH01 = new XRiTextField();
    WRGL01 = new XRiTextField();
    WECH02 = new XRiTextField();
    WRGL02 = new XRiTextField();
    WECH03 = new XRiTextField();
    WRGL03 = new XRiTextField();
    WECH04 = new XRiTextField();
    WRGL04 = new XRiTextField();
    WECH05 = new XRiTextField();
    WRGL05 = new XRiTextField();
    WECH06 = new XRiTextField();
    WRGL06 = new XRiTextField();
    WECH07 = new XRiTextField();
    WRGL07 = new XRiTextField();
    WECH08 = new XRiTextField();
    WRGL08 = new XRiTextField();
    WECH09 = new XRiTextField();
    WRGL09 = new XRiTextField();
    WECH10 = new XRiTextField();
    WRGL10 = new XRiTextField();
    WECH11 = new XRiTextField();
    WRGL11 = new XRiTextField();
    WECH12 = new XRiTextField();
    WRGL12 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(465, 435));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- WMTT01 ----
          WMTT01.setName("WMTT01");
          panel1.add(WMTT01);
          WMTT01.setBounds(15, 40, 108, WMTT01.getPreferredSize().height);

          //---- WMTT02 ----
          WMTT02.setName("WMTT02");
          panel1.add(WMTT02);
          WMTT02.setBounds(15, 70, 108, WMTT02.getPreferredSize().height);

          //---- WMTT03 ----
          WMTT03.setName("WMTT03");
          panel1.add(WMTT03);
          WMTT03.setBounds(15, 100, 108, WMTT03.getPreferredSize().height);

          //---- WMTT04 ----
          WMTT04.setName("WMTT04");
          panel1.add(WMTT04);
          WMTT04.setBounds(15, 130, 108, WMTT04.getPreferredSize().height);

          //---- WMTT05 ----
          WMTT05.setName("WMTT05");
          panel1.add(WMTT05);
          WMTT05.setBounds(15, 160, 108, WMTT05.getPreferredSize().height);

          //---- WMTT06 ----
          WMTT06.setName("WMTT06");
          panel1.add(WMTT06);
          WMTT06.setBounds(15, 190, 108, WMTT06.getPreferredSize().height);

          //---- WMTT07 ----
          WMTT07.setName("WMTT07");
          panel1.add(WMTT07);
          WMTT07.setBounds(15, 220, 108, WMTT07.getPreferredSize().height);

          //---- WMTT08 ----
          WMTT08.setName("WMTT08");
          panel1.add(WMTT08);
          WMTT08.setBounds(15, 250, 108, WMTT08.getPreferredSize().height);

          //---- WMTT09 ----
          WMTT09.setName("WMTT09");
          panel1.add(WMTT09);
          WMTT09.setBounds(15, 280, 108, WMTT09.getPreferredSize().height);

          //---- WMTT10 ----
          WMTT10.setName("WMTT10");
          panel1.add(WMTT10);
          WMTT10.setBounds(15, 310, 108, WMTT10.getPreferredSize().height);

          //---- WMTT11 ----
          WMTT11.setName("WMTT11");
          panel1.add(WMTT11);
          WMTT11.setBounds(15, 340, 108, WMTT11.getPreferredSize().height);

          //---- WMTT12 ----
          WMTT12.setName("WMTT12");
          panel1.add(WMTT12);
          WMTT12.setBounds(15, 370, 108, WMTT12.getPreferredSize().height);

          //---- WECH01 ----
          WECH01.setName("WECH01");
          panel1.add(WECH01);
          WECH01.setBounds(124, 40, 94, WECH01.getPreferredSize().height);

          //---- WRGL01 ----
          WRGL01.setName("WRGL01");
          panel1.add(WRGL01);
          WRGL01.setBounds(219, 40, 34, WRGL01.getPreferredSize().height);

          //---- WECH02 ----
          WECH02.setName("WECH02");
          panel1.add(WECH02);
          WECH02.setBounds(124, 70, 94, WECH02.getPreferredSize().height);

          //---- WRGL02 ----
          WRGL02.setName("WRGL02");
          panel1.add(WRGL02);
          WRGL02.setBounds(219, 70, 34, WRGL02.getPreferredSize().height);

          //---- WECH03 ----
          WECH03.setName("WECH03");
          panel1.add(WECH03);
          WECH03.setBounds(124, 100, 94, WECH03.getPreferredSize().height);

          //---- WRGL03 ----
          WRGL03.setName("WRGL03");
          panel1.add(WRGL03);
          WRGL03.setBounds(219, 100, 34, WRGL03.getPreferredSize().height);

          //---- WECH04 ----
          WECH04.setName("WECH04");
          panel1.add(WECH04);
          WECH04.setBounds(124, 130, 94, WECH04.getPreferredSize().height);

          //---- WRGL04 ----
          WRGL04.setName("WRGL04");
          panel1.add(WRGL04);
          WRGL04.setBounds(219, 130, 34, WRGL04.getPreferredSize().height);

          //---- WECH05 ----
          WECH05.setName("WECH05");
          panel1.add(WECH05);
          WECH05.setBounds(124, 160, 94, WECH05.getPreferredSize().height);

          //---- WRGL05 ----
          WRGL05.setName("WRGL05");
          panel1.add(WRGL05);
          WRGL05.setBounds(219, 160, 34, WRGL05.getPreferredSize().height);

          //---- WECH06 ----
          WECH06.setName("WECH06");
          panel1.add(WECH06);
          WECH06.setBounds(124, 190, 94, WECH06.getPreferredSize().height);

          //---- WRGL06 ----
          WRGL06.setName("WRGL06");
          panel1.add(WRGL06);
          WRGL06.setBounds(219, 190, 34, WRGL06.getPreferredSize().height);

          //---- WECH07 ----
          WECH07.setName("WECH07");
          panel1.add(WECH07);
          WECH07.setBounds(124, 220, 94, WECH07.getPreferredSize().height);

          //---- WRGL07 ----
          WRGL07.setName("WRGL07");
          panel1.add(WRGL07);
          WRGL07.setBounds(219, 220, 34, WRGL07.getPreferredSize().height);

          //---- WECH08 ----
          WECH08.setName("WECH08");
          panel1.add(WECH08);
          WECH08.setBounds(124, 250, 94, WECH08.getPreferredSize().height);

          //---- WRGL08 ----
          WRGL08.setName("WRGL08");
          panel1.add(WRGL08);
          WRGL08.setBounds(219, 250, 34, WRGL08.getPreferredSize().height);

          //---- WECH09 ----
          WECH09.setName("WECH09");
          panel1.add(WECH09);
          WECH09.setBounds(124, 280, 94, WECH09.getPreferredSize().height);

          //---- WRGL09 ----
          WRGL09.setName("WRGL09");
          panel1.add(WRGL09);
          WRGL09.setBounds(219, 280, 34, WRGL09.getPreferredSize().height);

          //---- WECH10 ----
          WECH10.setName("WECH10");
          panel1.add(WECH10);
          WECH10.setBounds(124, 310, 94, WECH10.getPreferredSize().height);

          //---- WRGL10 ----
          WRGL10.setName("WRGL10");
          panel1.add(WRGL10);
          WRGL10.setBounds(219, 310, 34, WRGL10.getPreferredSize().height);

          //---- WECH11 ----
          WECH11.setName("WECH11");
          panel1.add(WECH11);
          WECH11.setBounds(124, 340, 94, WECH11.getPreferredSize().height);

          //---- WRGL11 ----
          WRGL11.setName("WRGL11");
          panel1.add(WRGL11);
          WRGL11.setBounds(219, 340, 34, WRGL11.getPreferredSize().height);

          //---- WECH12 ----
          WECH12.setName("WECH12");
          panel1.add(WECH12);
          WECH12.setBounds(124, 370, 94, WECH12.getPreferredSize().height);

          //---- WRGL12 ----
          WRGL12.setName("WRGL12");
          panel1.add(WRGL12);
          WRGL12.setBounds(219, 370, 34, WRGL12.getPreferredSize().height);

          //---- label1 ----
          label1.setText("@LIBMTT@");
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(15, 10, 108, 30);

          //---- label2 ----
          label2.setText("Ech\u00e9ance");
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(125, 10, 94, 30);

          //---- label3 ----
          label3.setText("Rg");
          label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
          label3.setName("label3");
          panel1.add(label3);
          label3.setBounds(220, 10, 34, 30);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 275, 415);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField WMTT01;
  private XRiTextField WMTT02;
  private XRiTextField WMTT03;
  private XRiTextField WMTT04;
  private XRiTextField WMTT05;
  private XRiTextField WMTT06;
  private XRiTextField WMTT07;
  private XRiTextField WMTT08;
  private XRiTextField WMTT09;
  private XRiTextField WMTT10;
  private XRiTextField WMTT11;
  private XRiTextField WMTT12;
  private XRiTextField WECH01;
  private XRiTextField WRGL01;
  private XRiTextField WECH02;
  private XRiTextField WRGL02;
  private XRiTextField WECH03;
  private XRiTextField WRGL03;
  private XRiTextField WECH04;
  private XRiTextField WRGL04;
  private XRiTextField WECH05;
  private XRiTextField WRGL05;
  private XRiTextField WECH06;
  private XRiTextField WRGL06;
  private XRiTextField WECH07;
  private XRiTextField WRGL07;
  private XRiTextField WECH08;
  private XRiTextField WRGL08;
  private XRiTextField WECH09;
  private XRiTextField WRGL09;
  private XRiTextField WECH10;
  private XRiTextField WRGL10;
  private XRiTextField WECH11;
  private XRiTextField WRGL11;
  private XRiTextField WECH12;
  private XRiTextField WRGL12;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
