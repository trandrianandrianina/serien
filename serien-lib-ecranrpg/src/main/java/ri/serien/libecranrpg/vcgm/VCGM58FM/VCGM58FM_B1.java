
package ri.serien.libecranrpg.vcgm.VCGM58FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM58FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VCGM58FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    LIBLIT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBLIT@")).trim());
    OBJ_108_OBJ_108.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ACLIB@")).trim());
    OBJ_105_OBJ_105.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RGLIP@")).trim());
    OBJ_67_OBJ_67.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RLPCE@")).trim());
    OBJ_68_OBJ_68.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RLRAN@")).trim());
    OBJ_61_OBJ_61.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RLCJO@")).trim());
    OBJ_62_OBJ_62.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RLCFO@")).trim());
    OBJ_63_OBJ_63.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RLNLL@")).trim());
    OBJ_70_OBJ_70.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RLLIB@")).trim());
    OBJ_77_OBJ_77.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RLMTTX@")).trim());
    OBJ_79_OBJ_79.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSENS@")).trim());
    WDV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDV@")).trim());
    OBJ_82_OBJ_82.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RGLIF@")).trim());
    OBJ_84_OBJ_84.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NJF@")).trim());
    OBJ_87_OBJ_87.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RLDFAX@")).trim());
    OBJ_88_OBJ_88.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBJ@")).trim());
    OBJ_52_OBJ_52.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBETR@")).trim());
    OBJ_48_OBJ_48.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RLMTTX@")).trim());
    OBJ_49_OBJ_49.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSENS@")).trim());
    OBJ_29_OBJ_29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RLLIBC@")).trim());
    OBJ_33_OBJ_33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RLCPL@")).trim());
    OBJ_36_OBJ_36.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RLRUE@")).trim());
    OBJ_39_OBJ_39.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RLLOC@")).trim());
    OBJ_42_OBJ_42.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RLCDP@")).trim());
    OBJ_43_OBJ_43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RLVIL@")).trim());
    OBJ_44_OBJ_44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RLPAY@")).trim());
    OBJ_45_OBJ_45.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RLCOP@")).trim());
    OBJ_31_OBJ_31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RLREP@")).trim());
    OBJ_32_OBJ_32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RLREPN@")).trim());
    OBJ_35_OBJ_35.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RLTEL@")).trim());
    OBJ_38_OBJ_38.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RLFAX@")).trim());
    OBJ_41_OBJ_41.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RLPAC@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    OBJ_79_OBJ_79.setVisible(lexique.isPresent("WSENS"));
    OBJ_68_OBJ_68.setVisible(lexique.isPresent("RLRAN"));
    OBJ_49_OBJ_49.setVisible(lexique.isPresent("WSENS"));
    OBJ_31_OBJ_31.setVisible(lexique.isPresent("RLREP"));
    OBJ_61_OBJ_61.setVisible(lexique.isPresent("RLCJO"));
    OBJ_45_OBJ_45.setVisible(lexique.isPresent("RLCOP"));
    OBJ_88_OBJ_88.setVisible(lexique.isPresent("NBJ"));
    OBJ_84_OBJ_84.setVisible(lexique.isPresent("NJF"));
    OBJ_63_OBJ_63.setVisible(lexique.isPresent("RLNLL"));
    OBJ_42_OBJ_42.setVisible(lexique.isPresent("RLCDP"));
    OBJ_87_OBJ_87.setVisible(lexique.isPresent("RLDFAX"));
    OBJ_67_OBJ_67.setVisible(lexique.isPresent("RLPCE"));
    OBJ_62_OBJ_62.setVisible(lexique.isPresent("RLCFO"));
    OBJ_77_OBJ_77.setVisible(lexique.isPresent("RLMTTX"));
    OBJ_48_OBJ_48.setVisible(lexique.isPresent("RLMTTX"));
    OBJ_38_OBJ_38.setVisible(lexique.isPresent("RLFAX"));
    OBJ_35_OBJ_35.setVisible(lexique.isPresent("RLTEL"));
    OBJ_52_OBJ_52.setVisible(lexique.isPresent("LIBETR"));
    OBJ_108_OBJ_108.setVisible(lexique.isPresent("ACLIB"));
    OBJ_32_OBJ_32.setVisible(lexique.isPresent("RLREPN"));
    OBJ_43_OBJ_43.setVisible(lexique.isPresent("RLVIL"));
    OBJ_41_OBJ_41.setVisible(lexique.isPresent("RLPAC"));
    OBJ_44_OBJ_44.setVisible(lexique.isPresent("RLPAY"));
    LIBLIT.setVisible(lexique.isPresent("LIBLIT"));
    OBJ_39_OBJ_39.setVisible(lexique.isPresent("RLLOC"));
    OBJ_36_OBJ_36.setVisible(lexique.isPresent("RLRUE"));
    OBJ_33_OBJ_33.setVisible(lexique.isPresent("RLCPL"));
    OBJ_29_OBJ_29.setVisible(lexique.isPresent("RLLIBC"));
    OBJ_82_OBJ_82.setVisible(lexique.isPresent("RGLIF"));
    OBJ_70_OBJ_70.setVisible(lexique.isPresent("RLLIB"));
    OBJ_115_OBJ_115.setVisible(lexique.isPresent("RLDLYX"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  Fichier relance"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(5, 3);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(1, 1);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(5, 2);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(5, 4);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_36_OBJ_37 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_37_OBJ_37 = new JLabel();
    RLDTRX = new XRiCalendrier();
    OBJ_38_OBJ_39 = new JLabel();
    INDNCG = new XRiTextField();
    OBJ_39_OBJ_40 = new JLabel();
    INDNCA = new XRiTextField();
    OBJ_40_OBJ_41 = new JLabel();
    INDPCE = new XRiTextField();
    OBJ_41_OBJ_42 = new JLabel();
    INDRAN = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_96_OBJ_96 = new JLabel();
    RLTYL = new XRiTextField();
    LIBLIT = new RiZoneSortie();
    OBJ_102_OBJ_102 = new JLabel();
    RLDRPX = new XRiCalendrier();
    RLACT = new XRiTextField();
    OBJ_108_OBJ_108 = new RiZoneSortie();
    RLMOL = new XRiTextField();
    RLMOL1 = new XRiTextField();
    RLMOL2 = new XRiTextField();
    xTitledSeparator3 = new JXTitledSeparator();
    separator1 = compFactory.createSeparator("Code affaire");
    panel4 = new JPanel();
    OBJ_115_OBJ_115 = new JLabel();
    RLDLYX = new XRiCalendrier();
    OBJ_101_OBJ_101 = new JLabel();
    RLMTLX = new XRiTextField();
    RLRGP = new XRiTextField();
    OBJ_105_OBJ_105 = new RiZoneSortie();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_66_OBJ_66 = new JLabel();
    OBJ_67_OBJ_67 = new RiZoneSortie();
    OBJ_68_OBJ_68 = new RiZoneSortie();
    OBJ_64_OBJ_64 = new JLabel();
    RLDECX = new XRiTextField();
    OBJ_65_OBJ_65 = new JLabel();
    OBJ_61_OBJ_61 = new RiZoneSortie();
    OBJ_62_OBJ_62 = new RiZoneSortie();
    OBJ_63_OBJ_63 = new RiZoneSortie();
    OBJ_70_OBJ_70 = new RiZoneSortie();
    xTitledPanel3 = new JXTitledPanel();
    OBJ_74_OBJ_74 = new JLabel();
    RLNIV = new XRiTextField();
    OBJ_76_OBJ_76 = new JLabel();
    OBJ_77_OBJ_77 = new RiZoneSortie();
    OBJ_79_OBJ_79 = new JLabel();
    WDV = new RiZoneSortie();
    OBJ_80_OBJ_80 = new JLabel();
    RLRGL = new XRiTextField();
    OBJ_82_OBJ_82 = new RiZoneSortie();
    OBJ_84_OBJ_84 = new RiZoneSortie();
    OBJ_85_OBJ_85 = new JLabel();
    OBJ_86_OBJ_86 = new JLabel();
    OBJ_87_OBJ_87 = new RiZoneSortie();
    OBJ_88_OBJ_88 = new RiZoneSortie();
    OBJ_89_OBJ_89 = new JLabel();
    OBJ_90_OBJ_90 = new JLabel();
    RLECHX = new XRiTextField();
    panel2 = new JPanel();
    OBJ_50_OBJ_50 = new JLabel();
    RLTOR = new XRiTextField();
    OBJ_52_OBJ_52 = new RiZoneSortie();
    RLARLX = new XRiCalendrier();
    OBJ_55_OBJ_55 = new JLabel();
    OBJ_47_OBJ_47 = new JLabel();
    OBJ_48_OBJ_48 = new RiZoneSortie();
    OBJ_49_OBJ_49 = new RiZoneSortie();
    RLDDRX = new XRiCalendrier();
    OBJ_53_OBJ_53 = new JLabel();
    panel3 = new JPanel();
    xTitledSeparator1 = new JXTitledSeparator();
    OBJ_29_OBJ_29 = new RiZoneSortie();
    OBJ_33_OBJ_33 = new RiZoneSortie();
    OBJ_36_OBJ_36 = new RiZoneSortie();
    OBJ_39_OBJ_39 = new RiZoneSortie();
    OBJ_42_OBJ_42 = new RiZoneSortie();
    OBJ_43_OBJ_43 = new RiZoneSortie();
    OBJ_44_OBJ_44 = new RiZoneSortie();
    OBJ_45_OBJ_45 = new RiZoneSortie();
    xTitledSeparator2 = new JXTitledSeparator();
    OBJ_30_OBJ_30 = new JLabel();
    OBJ_31_OBJ_31 = new RiZoneSortie();
    OBJ_32_OBJ_32 = new RiZoneSortie();
    OBJ_35_OBJ_35 = new RiZoneSortie();
    OBJ_34 = new JLabel();
    OBJ_37 = new JLabel();
    OBJ_38_OBJ_38 = new RiZoneSortie();
    OBJ_41_OBJ_41 = new RiZoneSortie();
    OBJ_40_OBJ_40 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_16 = new JMenuItem();
    OBJ_15 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Relances");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(950, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_36_OBJ_37 ----
          OBJ_36_OBJ_37.setText("Soci\u00e9t\u00e9");
          OBJ_36_OBJ_37.setName("OBJ_36_OBJ_37");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setName("INDETB");

          //---- OBJ_37_OBJ_37 ----
          OBJ_37_OBJ_37.setText("Date relance");
          OBJ_37_OBJ_37.setName("OBJ_37_OBJ_37");

          //---- RLDTRX ----
          RLDTRX.setComponentPopupMenu(null);
          RLDTRX.setName("RLDTRX");

          //---- OBJ_38_OBJ_39 ----
          OBJ_38_OBJ_39.setText("Collectif");
          OBJ_38_OBJ_39.setName("OBJ_38_OBJ_39");

          //---- INDNCG ----
          INDNCG.setComponentPopupMenu(null);
          INDNCG.setName("INDNCG");

          //---- OBJ_39_OBJ_40 ----
          OBJ_39_OBJ_40.setText("Auxiliaire");
          OBJ_39_OBJ_40.setName("OBJ_39_OBJ_40");

          //---- INDNCA ----
          INDNCA.setComponentPopupMenu(null);
          INDNCA.setName("INDNCA");

          //---- OBJ_40_OBJ_41 ----
          OBJ_40_OBJ_41.setText("Pi\u00e8ce");
          OBJ_40_OBJ_41.setName("OBJ_40_OBJ_41");

          //---- INDPCE ----
          INDPCE.setComponentPopupMenu(null);
          INDPCE.setName("INDPCE");

          //---- OBJ_41_OBJ_42 ----
          OBJ_41_OBJ_42.setText("R\u00e8glement");
          OBJ_41_OBJ_42.setName("OBJ_41_OBJ_42");

          //---- INDRAN ----
          INDRAN.setComponentPopupMenu(null);
          INDRAN.setName("INDRAN");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_36_OBJ_37, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_37_OBJ_37, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(RLDTRX, GroupLayout.PREFERRED_SIZE, 117, GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(50, 50, 50)
                    .addComponent(INDNCG, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_38_OBJ_39, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(OBJ_39_OBJ_40, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INDNCA, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_40_OBJ_41, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDPCE, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(OBJ_41_OBJ_42, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDRAN, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(110, 110, 110))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(RLDTRX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDNCG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDNCA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDPCE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDRAN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_36_OBJ_37)
                  .addComponent(OBJ_37_OBJ_37)
                  .addComponent(OBJ_38_OBJ_39)
                  .addComponent(OBJ_39_OBJ_40)
                  .addComponent(OBJ_40_OBJ_41)
                  .addComponent(OBJ_41_OBJ_42)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Visualisation compte");
              riSousMenu_bt6.setToolTipText("Visualisation du compte");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Vue facture GVM");
              riSousMenu_bt7.setToolTipText("Vue de la facture en gestion des ventes (GVM)");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Encours client");
              riSousMenu_bt8.setToolTipText("Encours client");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Extension relance PCA");
              riSousMenu_bt9.setToolTipText("Vue de l'extension de relance issue du plan comptable auxiliaire");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Analyse des comptes");
              riSousMenu_bt10.setToolTipText("Analyse statistique des comptes");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);

            //======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");

              //---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Acc\u00e8s fiche client");
              riSousMenu_bt11.setToolTipText("Acc\u00e8s \u00e0 la fiche client");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);

            //======== riSousMenu12 ========
            {
              riSousMenu12.setName("riSousMenu12");

              //---- riSousMenu_bt12 ----
              riSousMenu_bt12.setText("Edition fiche litige");
              riSousMenu_bt12.setToolTipText("Edition de la fiche litige");
              riSousMenu_bt12.setName("riSousMenu_bt12");
              riSousMenu_bt12.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt12ActionPerformed(e);
                }
              });
              riSousMenu12.add(riSousMenu_bt12);
            }
            menus_haut.add(riSousMenu12);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Bloc-notes");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);

            //======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");

              //---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Fiche de r\u00e9vision");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed(e);
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 620));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(890, 620));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setTitle("Litige");
            xTitledPanel1.setTitleFont(xTitledPanel1.getTitleFont().deriveFont(xTitledPanel1.getTitleFont().getStyle() & ~Font.BOLD));
            xTitledPanel1.setForeground(Color.black);
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

            //---- OBJ_96_OBJ_96 ----
            OBJ_96_OBJ_96.setText("Type de litige");
            OBJ_96_OBJ_96.setName("OBJ_96_OBJ_96");

            //---- RLTYL ----
            RLTYL.setComponentPopupMenu(BTD);
            RLTYL.setName("RLTYL");

            //---- LIBLIT ----
            LIBLIT.setText("@LIBLIT@");
            LIBLIT.setName("LIBLIT");

            //---- OBJ_102_OBJ_102 ----
            OBJ_102_OBJ_102.setText("R\u00e8glement pr\u00e9vu le");
            OBJ_102_OBJ_102.setName("OBJ_102_OBJ_102");

            //---- RLDRPX ----
            RLDRPX.setComponentPopupMenu(BTD);
            RLDRPX.setName("RLDRPX");

            //---- RLACT ----
            RLACT.setComponentPopupMenu(BTD);
            RLACT.setName("RLACT");

            //---- OBJ_108_OBJ_108 ----
            OBJ_108_OBJ_108.setText("@ACLIB@");
            OBJ_108_OBJ_108.setName("OBJ_108_OBJ_108");

            //---- RLMOL ----
            RLMOL.setComponentPopupMenu(BTD);
            RLMOL.setName("RLMOL");

            //---- RLMOL1 ----
            RLMOL1.setComponentPopupMenu(BTD);
            RLMOL1.setName("RLMOL1");

            //---- RLMOL2 ----
            RLMOL2.setComponentPopupMenu(BTD);
            RLMOL2.setName("RLMOL2");

            //---- xTitledSeparator3 ----
            xTitledSeparator3.setTitle("Observations du Client");
            xTitledSeparator3.setName("xTitledSeparator3");

            //---- separator1 ----
            separator1.setName("separator1");

            //======== panel4 ========
            {
              panel4.setOpaque(false);
              panel4.setName("panel4");

              //---- OBJ_115_OBJ_115 ----
              OBJ_115_OBJ_115.setText("Date du litige");
              OBJ_115_OBJ_115.setName("OBJ_115_OBJ_115");

              //---- RLDLYX ----
              RLDLYX.setComponentPopupMenu(BTD);
              RLDLYX.setName("RLDLYX");

              GroupLayout panel4Layout = new GroupLayout(panel4);
              panel4.setLayout(panel4Layout);
              panel4Layout.setHorizontalGroup(
                panel4Layout.createParallelGroup()
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addComponent(OBJ_115_OBJ_115, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(RLDLYX, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                    .addGap(590, 590, 590))
              );
              panel4Layout.setVerticalGroup(
                panel4Layout.createParallelGroup()
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(OBJ_115_OBJ_115))
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(RLDLYX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              );
            }

            //---- OBJ_101_OBJ_101 ----
            OBJ_101_OBJ_101.setText("Montant du litige");
            OBJ_101_OBJ_101.setName("OBJ_101_OBJ_101");

            //---- RLMTLX ----
            RLMTLX.setToolTipText("Montant du litige si diff\u00e9rent de la facture");
            RLMTLX.setComponentPopupMenu(BTD);
            RLMTLX.setName("RLMTLX");

            //---- RLRGP ----
            RLRGP.setComponentPopupMenu(BTD);
            RLRGP.setName("RLRGP");

            //---- OBJ_105_OBJ_105 ----
            OBJ_105_OBJ_105.setText("@RGLIP@");
            OBJ_105_OBJ_105.setName("OBJ_105_OBJ_105");

            GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
            xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
            xTitledPanel1ContentContainerLayout.setHorizontalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addComponent(OBJ_96_OBJ_96, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                          .addGap(45, 45, 45)
                          .addComponent(RLTYL, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                          .addGap(3, 3, 3)
                          .addComponent(LIBLIT, GroupLayout.PREFERRED_SIZE, 265, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(250, 250, 250)
                          .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                            .addComponent(OBJ_101_OBJ_101, GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE)
                            .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                              .addGap(96, 96, 96)
                              .addComponent(RLMTLX, GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE))))
                        .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 245, GroupLayout.PREFERRED_SIZE))
                      .addGap(80, 80, 80)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addComponent(OBJ_102_OBJ_102, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                          .addGap(79, 79, 79)
                          .addComponent(RLDRPX, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addComponent(RLRGP, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                          .addGap(6, 6, 6)
                          .addComponent(OBJ_105_OBJ_105, GroupLayout.PREFERRED_SIZE, 259, GroupLayout.PREFERRED_SIZE))))
                    .addComponent(xTitledSeparator3, GroupLayout.PREFERRED_SIZE, 495, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(RLMOL, GroupLayout.PREFERRED_SIZE, 489, GroupLayout.PREFERRED_SIZE)
                        .addComponent(RLMOL1, GroupLayout.PREFERRED_SIZE, 489, GroupLayout.PREFERRED_SIZE)
                        .addComponent(RLMOL2, GroupLayout.PREFERRED_SIZE, 489, GroupLayout.PREFERRED_SIZE))
                      .addGap(91, 91, 91)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(separator1, GroupLayout.PREFERRED_SIZE, 255, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addComponent(RLACT, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                          .addGap(5, 5, 5)
                          .addComponent(OBJ_108_OBJ_108, GroupLayout.PREFERRED_SIZE, 194, GroupLayout.PREFERRED_SIZE))))))
            );
            xTitledPanel1ContentContainerLayout.setVerticalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(6, 6, 6)
                          .addComponent(OBJ_96_OBJ_96))
                        .addComponent(RLTYL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(2, 2, 2)
                          .addComponent(LIBLIT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(2, 2, 2)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(6, 6, 6)
                          .addComponent(OBJ_101_OBJ_101))
                        .addComponent(RLMTLX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(panel4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(6, 6, 6)
                          .addComponent(OBJ_102_OBJ_102))
                        .addComponent(RLDRPX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGap(2, 2, 2)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(RLRGP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(2, 2, 2)
                          .addComponent(OBJ_105_OBJ_105, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))))
                  .addComponent(xTitledSeparator3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(RLMOL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(RLMOL1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(RLMOL2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(7, 7, 7)
                      .addComponent(separator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(RLACT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(2, 2, 2)
                          .addComponent(OBJ_108_OBJ_108, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))))
            );
          }

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setTitle("Ecriture comptable");
            xTitledPanel2.setTitleFont(xTitledPanel2.getTitleFont().deriveFont(xTitledPanel2.getTitleFont().getStyle() & ~Font.BOLD));
            xTitledPanel2.setForeground(Color.gray);
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();

            //---- OBJ_66_OBJ_66 ----
            OBJ_66_OBJ_66.setText("N\u00b0 Pi\u00e8ce");
            OBJ_66_OBJ_66.setName("OBJ_66_OBJ_66");

            //---- OBJ_67_OBJ_67 ----
            OBJ_67_OBJ_67.setText("@RLPCE@");
            OBJ_67_OBJ_67.setName("OBJ_67_OBJ_67");

            //---- OBJ_68_OBJ_68 ----
            OBJ_68_OBJ_68.setText("@RLRAN@");
            OBJ_68_OBJ_68.setName("OBJ_68_OBJ_68");

            //---- OBJ_64_OBJ_64 ----
            OBJ_64_OBJ_64.setText("Date \u00e9criture");
            OBJ_64_OBJ_64.setName("OBJ_64_OBJ_64");

            //---- RLDECX ----
            RLDECX.setComponentPopupMenu(BTD);
            RLDECX.setName("RLDECX");

            //---- OBJ_65_OBJ_65 ----
            OBJ_65_OBJ_65.setText("Journal / Folio / Ligne");
            OBJ_65_OBJ_65.setName("OBJ_65_OBJ_65");

            //---- OBJ_61_OBJ_61 ----
            OBJ_61_OBJ_61.setText("@RLCJO@");
            OBJ_61_OBJ_61.setName("OBJ_61_OBJ_61");

            //---- OBJ_62_OBJ_62 ----
            OBJ_62_OBJ_62.setText("@RLCFO@");
            OBJ_62_OBJ_62.setName("OBJ_62_OBJ_62");

            //---- OBJ_63_OBJ_63 ----
            OBJ_63_OBJ_63.setText("@RLNLL@");
            OBJ_63_OBJ_63.setName("OBJ_63_OBJ_63");

            //---- OBJ_70_OBJ_70 ----
            OBJ_70_OBJ_70.setText("@RLLIB@");
            OBJ_70_OBJ_70.setForeground(Color.black);
            OBJ_70_OBJ_70.setName("OBJ_70_OBJ_70");

            GroupLayout xTitledPanel2ContentContainerLayout = new GroupLayout(xTitledPanel2ContentContainer);
            xTitledPanel2ContentContainer.setLayout(xTitledPanel2ContentContainerLayout);
            xTitledPanel2ContentContainerLayout.setHorizontalGroup(
              xTitledPanel2ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_66_OBJ_66, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(OBJ_67_OBJ_67, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_68_OBJ_68, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                      .addGap(31, 31, 31)
                      .addComponent(OBJ_64_OBJ_64, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(RLDECX, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                      .addGap(15, 15, 15)
                      .addComponent(OBJ_70_OBJ_70, GroupLayout.PREFERRED_SIZE, 441, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_65_OBJ_65, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
                      .addGap(35, 35, 35)
                      .addComponent(OBJ_61_OBJ_61, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
                      .addGap(11, 11, 11)
                      .addComponent(OBJ_62_OBJ_62, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
                      .addGap(11, 11, 11)
                      .addComponent(OBJ_63_OBJ_63, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)))
                  .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            );
            xTitledPanel2ContentContainerLayout.setVerticalGroup(
              xTitledPanel2ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(3, 3, 3)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_66_OBJ_66, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_67_OBJ_67, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_68_OBJ_68, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_64_OBJ_64, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(RLDECX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_70_OBJ_70, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(4, 4, 4)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_65_OBJ_65, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(OBJ_61_OBJ_61, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_62_OBJ_62, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_63_OBJ_63, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }

          //======== xTitledPanel3 ========
          {
            xTitledPanel3.setBorder(new DropShadowBorder());
            xTitledPanel3.setTitle("D\u00e9tail de la relance");
            xTitledPanel3.setPaintBorderInsets(false);
            xTitledPanel3.setTitleFont(xTitledPanel3.getTitleFont().deriveFont(xTitledPanel3.getTitleFont().getStyle() & ~Font.BOLD));
            xTitledPanel3.setName("xTitledPanel3");
            Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();

            //---- OBJ_74_OBJ_74 ----
            OBJ_74_OBJ_74.setText("Niveau de relance");
            OBJ_74_OBJ_74.setName("OBJ_74_OBJ_74");

            //---- RLNIV ----
            RLNIV.setComponentPopupMenu(BTD);
            RLNIV.setName("RLNIV");

            //---- OBJ_76_OBJ_76 ----
            OBJ_76_OBJ_76.setText("Montant");
            OBJ_76_OBJ_76.setName("OBJ_76_OBJ_76");

            //---- OBJ_77_OBJ_77 ----
            OBJ_77_OBJ_77.setText("@RLMTTX@");
            OBJ_77_OBJ_77.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_77_OBJ_77.setName("OBJ_77_OBJ_77");

            //---- OBJ_79_OBJ_79 ----
            OBJ_79_OBJ_79.setText("@WSENS@");
            OBJ_79_OBJ_79.setName("OBJ_79_OBJ_79");

            //---- WDV ----
            WDV.setText("@WDV@");
            WDV.setName("WDV");

            //---- OBJ_80_OBJ_80 ----
            OBJ_80_OBJ_80.setText("R\u00e9glement");
            OBJ_80_OBJ_80.setName("OBJ_80_OBJ_80");

            //---- RLRGL ----
            RLRGL.setComponentPopupMenu(BTD);
            RLRGL.setName("RLRGL");

            //---- OBJ_82_OBJ_82 ----
            OBJ_82_OBJ_82.setText("@RGLIF@");
            OBJ_82_OBJ_82.setName("OBJ_82_OBJ_82");

            //---- OBJ_84_OBJ_84 ----
            OBJ_84_OBJ_84.setText("@NJF@");
            OBJ_84_OBJ_84.setName("OBJ_84_OBJ_84");

            //---- OBJ_85_OBJ_85 ----
            OBJ_85_OBJ_85.setText("Jours depuis");
            OBJ_85_OBJ_85.setName("OBJ_85_OBJ_85");

            //---- OBJ_86_OBJ_86 ----
            OBJ_86_OBJ_86.setText("date de facture");
            OBJ_86_OBJ_86.setName("OBJ_86_OBJ_86");

            //---- OBJ_87_OBJ_87 ----
            OBJ_87_OBJ_87.setText("@RLDFAX@");
            OBJ_87_OBJ_87.setName("OBJ_87_OBJ_87");

            //---- OBJ_88_OBJ_88 ----
            OBJ_88_OBJ_88.setText("@NBJ@");
            OBJ_88_OBJ_88.setName("OBJ_88_OBJ_88");

            //---- OBJ_89_OBJ_89 ----
            OBJ_89_OBJ_89.setText("Jours depuis");
            OBJ_89_OBJ_89.setName("OBJ_89_OBJ_89");

            //---- OBJ_90_OBJ_90 ----
            OBJ_90_OBJ_90.setText("date d'\u00e9ch\u00e9ance");
            OBJ_90_OBJ_90.setName("OBJ_90_OBJ_90");

            //---- RLECHX ----
            RLECHX.setComponentPopupMenu(BTD);
            RLECHX.setName("RLECHX");

            GroupLayout xTitledPanel3ContentContainerLayout = new GroupLayout(xTitledPanel3ContentContainer);
            xTitledPanel3ContentContainer.setLayout(xTitledPanel3ContentContainerLayout);
            xTitledPanel3ContentContainerLayout.setHorizontalGroup(
              xTitledPanel3ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_74_OBJ_74, GroupLayout.PREFERRED_SIZE, 113, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(RLNIV, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_76_OBJ_76, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_77_OBJ_77, GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE)
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_79_OBJ_79, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(WDV, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(OBJ_80_OBJ_80, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
                      .addGap(6, 6, 6)
                      .addComponent(RLRGL, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_82_OBJ_82, GroupLayout.PREFERRED_SIZE, 307, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_84_OBJ_84, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_85_OBJ_85, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE)
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_86_OBJ_86, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                      .addGap(20, 20, 20)
                      .addComponent(OBJ_87_OBJ_87, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                      .addGap(50, 50, 50)
                      .addComponent(OBJ_88_OBJ_88, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                      .addGap(15, 15, 15)
                      .addComponent(OBJ_89_OBJ_89, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                      .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                          .addGap(105, 105, 105)
                          .addComponent(RLECHX, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
                        .addComponent(OBJ_90_OBJ_90, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE))))
                  .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            );
            xTitledPanel3ContentContainerLayout.setVerticalGroup(
              xTitledPanel3ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_74_OBJ_74, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(RLNIV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_76_OBJ_76, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_77_OBJ_77, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_79_OBJ_79, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(WDV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_80_OBJ_80, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(RLRGL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_82_OBJ_82, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel3ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_84_OBJ_84, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_85_OBJ_85, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_86_OBJ_86, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_87_OBJ_87, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_88_OBJ_88, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_89_OBJ_89, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                    .addComponent(RLECHX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_90_OBJ_90, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))))
            );
          }

          //======== panel2 ========
          {
            panel2.setBorder(new DropShadowBorder());
            panel2.setOpaque(false);
            panel2.setName("panel2");

            //---- OBJ_50_OBJ_50 ----
            OBJ_50_OBJ_50.setText("Etat relance");
            OBJ_50_OBJ_50.setName("OBJ_50_OBJ_50");

            //---- RLTOR ----
            RLTOR.setComponentPopupMenu(BTD);
            RLTOR.setName("RLTOR");

            //---- OBJ_52_OBJ_52 ----
            OBJ_52_OBJ_52.setText("@LIBETR@");
            OBJ_52_OBJ_52.setName("OBJ_52_OBJ_52");

            //---- RLARLX ----
            RLARLX.setComponentPopupMenu(BTD);
            RLARLX.setName("RLARLX");

            //---- OBJ_55_OBJ_55 ----
            OBJ_55_OBJ_55.setText("A rappeler le");
            OBJ_55_OBJ_55.setName("OBJ_55_OBJ_55");

            //---- OBJ_47_OBJ_47 ----
            OBJ_47_OBJ_47.setText("Montant de la relance");
            OBJ_47_OBJ_47.setName("OBJ_47_OBJ_47");

            //---- OBJ_48_OBJ_48 ----
            OBJ_48_OBJ_48.setText("@RLMTTX@");
            OBJ_48_OBJ_48.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_48_OBJ_48.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_48_OBJ_48.setName("OBJ_48_OBJ_48");

            //---- OBJ_49_OBJ_49 ----
            OBJ_49_OBJ_49.setText("@WSENS@");
            OBJ_49_OBJ_49.setName("OBJ_49_OBJ_49");

            //---- RLDDRX ----
            RLDDRX.setName("RLDDRX");

            //---- OBJ_53_OBJ_53 ----
            OBJ_53_OBJ_53.setText("R\u00e9gl\u00e9 en comptabilit\u00e9 le");
            OBJ_53_OBJ_53.setName("OBJ_53_OBJ_53");

            GroupLayout panel2Layout = new GroupLayout(panel2);
            panel2.setLayout(panel2Layout);
            panel2Layout.setHorizontalGroup(
              panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(4, 4, 4)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(OBJ_50_OBJ_50, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(RLTOR, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_52_OBJ_52, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(OBJ_55_OBJ_55, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
                      .addGap(56, 56, 56)
                      .addComponent(RLARLX, GroupLayout.PREFERRED_SIZE, 117, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(OBJ_47_OBJ_47, GroupLayout.PREFERRED_SIZE, 131, GroupLayout.PREFERRED_SIZE)
                      .addGap(9, 9, 9)
                      .addComponent(OBJ_48_OBJ_48, GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE)
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_49_OBJ_49, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(OBJ_53_OBJ_53, GroupLayout.PREFERRED_SIZE, 136, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(RLDDRX, GroupLayout.PREFERRED_SIZE, 117, GroupLayout.PREFERRED_SIZE))))
            );
            panel2Layout.setVerticalGroup(
              panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_50_OBJ_50, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(RLTOR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_52_OBJ_52, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(7, 7, 7)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_55_OBJ_55, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(RLARLX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(9, 9, 9)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_47_OBJ_47, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(OBJ_48_OBJ_48, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_49_OBJ_49, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(14, 14, 14)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_53_OBJ_53, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(RLDDRX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            );
          }

          //======== panel3 ========
          {
            panel3.setBorder(new DropShadowBorder());
            panel3.setOpaque(false);
            panel3.setBackground(new Color(214, 217, 223));
            panel3.setName("panel3");

            //---- xTitledSeparator1 ----
            xTitledSeparator1.setTitle("Bloc adresse");
            xTitledSeparator1.setName("xTitledSeparator1");

            //---- OBJ_29_OBJ_29 ----
            OBJ_29_OBJ_29.setText("@RLLIBC@");
            OBJ_29_OBJ_29.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_29_OBJ_29.setFont(OBJ_29_OBJ_29.getFont().deriveFont(OBJ_29_OBJ_29.getFont().getStyle() | Font.BOLD));
            OBJ_29_OBJ_29.setName("OBJ_29_OBJ_29");

            //---- OBJ_33_OBJ_33 ----
            OBJ_33_OBJ_33.setText("@RLCPL@");
            OBJ_33_OBJ_33.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_33_OBJ_33.setName("OBJ_33_OBJ_33");

            //---- OBJ_36_OBJ_36 ----
            OBJ_36_OBJ_36.setText("@RLRUE@");
            OBJ_36_OBJ_36.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_36_OBJ_36.setName("OBJ_36_OBJ_36");

            //---- OBJ_39_OBJ_39 ----
            OBJ_39_OBJ_39.setText("@RLLOC@");
            OBJ_39_OBJ_39.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_39_OBJ_39.setName("OBJ_39_OBJ_39");

            //---- OBJ_42_OBJ_42 ----
            OBJ_42_OBJ_42.setText("@RLCDP@");
            OBJ_42_OBJ_42.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_42_OBJ_42.setName("OBJ_42_OBJ_42");

            //---- OBJ_43_OBJ_43 ----
            OBJ_43_OBJ_43.setText("@RLVIL@");
            OBJ_43_OBJ_43.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_43_OBJ_43.setName("OBJ_43_OBJ_43");

            //---- OBJ_44_OBJ_44 ----
            OBJ_44_OBJ_44.setText("@RLPAY@");
            OBJ_44_OBJ_44.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_44_OBJ_44.setName("OBJ_44_OBJ_44");

            //---- OBJ_45_OBJ_45 ----
            OBJ_45_OBJ_45.setText("@RLCOP@");
            OBJ_45_OBJ_45.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_45_OBJ_45.setName("OBJ_45_OBJ_45");

            //---- xTitledSeparator2 ----
            xTitledSeparator2.setTitle("Repr\u00e9sentant");
            xTitledSeparator2.setName("xTitledSeparator2");

            //---- OBJ_30_OBJ_30 ----
            OBJ_30_OBJ_30.setText("Nom");
            OBJ_30_OBJ_30.setName("OBJ_30_OBJ_30");

            //---- OBJ_31_OBJ_31 ----
            OBJ_31_OBJ_31.setText("@RLREP@");
            OBJ_31_OBJ_31.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_31_OBJ_31.setName("OBJ_31_OBJ_31");

            //---- OBJ_32_OBJ_32 ----
            OBJ_32_OBJ_32.setText("@RLREPN@");
            OBJ_32_OBJ_32.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_32_OBJ_32.setName("OBJ_32_OBJ_32");

            //---- OBJ_35_OBJ_35 ----
            OBJ_35_OBJ_35.setText("@RLTEL@");
            OBJ_35_OBJ_35.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_35_OBJ_35.setName("OBJ_35_OBJ_35");

            //---- OBJ_34 ----
            OBJ_34.setIcon(null);
            OBJ_34.setText("Tel.");
            OBJ_34.setName("OBJ_34");

            //---- OBJ_37 ----
            OBJ_37.setIcon(null);
            OBJ_37.setText("Fax");
            OBJ_37.setName("OBJ_37");

            //---- OBJ_38_OBJ_38 ----
            OBJ_38_OBJ_38.setText("@RLFAX@");
            OBJ_38_OBJ_38.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_38_OBJ_38.setName("OBJ_38_OBJ_38");

            //---- OBJ_41_OBJ_41 ----
            OBJ_41_OBJ_41.setText("@RLPAC@");
            OBJ_41_OBJ_41.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_41_OBJ_41.setName("OBJ_41_OBJ_41");

            //---- OBJ_40_OBJ_40 ----
            OBJ_40_OBJ_40.setText("Contact");
            OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");

            GroupLayout panel3Layout = new GroupLayout(panel3);
            panel3.setLayout(panel3Layout);
            panel3Layout.setHorizontalGroup(
              panel3Layout.createParallelGroup()
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGroup(panel3Layout.createParallelGroup()
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, 285, GroupLayout.PREFERRED_SIZE)
                      .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                      .addComponent(xTitledSeparator2, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(7, 7, 7)
                      .addComponent(OBJ_29_OBJ_29, GroupLayout.PREFERRED_SIZE, 279, GroupLayout.PREFERRED_SIZE)
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_30_OBJ_30, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                      .addGap(23, 23, 23)
                      .addComponent(OBJ_31_OBJ_31, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_32_OBJ_32, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(7, 7, 7)
                      .addComponent(OBJ_33_OBJ_33, GroupLayout.PREFERRED_SIZE, 279, GroupLayout.PREFERRED_SIZE)
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                      .addGap(23, 23, 23)
                      .addComponent(OBJ_35_OBJ_35, GroupLayout.PREFERRED_SIZE, 113, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(7, 7, 7)
                      .addComponent(OBJ_36_OBJ_36, GroupLayout.PREFERRED_SIZE, 279, GroupLayout.PREFERRED_SIZE)
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                      .addGap(23, 23, 23)
                      .addComponent(OBJ_38_OBJ_38, GroupLayout.PREFERRED_SIZE, 113, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(7, 7, 7)
                      .addComponent(OBJ_39_OBJ_39, GroupLayout.PREFERRED_SIZE, 279, GroupLayout.PREFERRED_SIZE)
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_40_OBJ_40, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(OBJ_41_OBJ_41, GroupLayout.PREFERRED_SIZE, 195, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(7, 7, 7)
                      .addComponent(OBJ_42_OBJ_42, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_43_OBJ_43, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(7, 7, 7)
                      .addComponent(OBJ_44_OBJ_44, GroupLayout.PREFERRED_SIZE, 245, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_45_OBJ_45, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)))
                  .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            );
            panel3Layout.setVerticalGroup(
              panel3Layout.createParallelGroup()
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGroup(panel3Layout.createParallelGroup()
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(xTitledSeparator2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(6, 6, 6)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addComponent(OBJ_29_OBJ_29, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addGroup(panel3Layout.createParallelGroup()
                        .addComponent(OBJ_30_OBJ_30, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_31_OBJ_31, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_32_OBJ_32, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGap(1, 1, 1)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addComponent(OBJ_33_OBJ_33, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addGroup(panel3Layout.createParallelGroup()
                        .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_35_OBJ_35, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGap(1, 1, 1)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addComponent(OBJ_36_OBJ_36, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addGroup(panel3Layout.createParallelGroup()
                        .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_38_OBJ_38, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGap(1, 1, 1)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addComponent(OBJ_39_OBJ_39, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addGroup(panel3Layout.createParallelGroup()
                        .addComponent(OBJ_40_OBJ_40, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_41_OBJ_41, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGap(1, 1, 1)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addComponent(OBJ_42_OBJ_42, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_43_OBJ_43, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addComponent(OBJ_44_OBJ_44, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_45_OBJ_45, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
                  .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                    .addComponent(panel3, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(xTitledPanel2, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(xTitledPanel3, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel3, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_16 ----
      OBJ_16.setText("Choix possibles");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);

      //---- OBJ_15 ----
      OBJ_15.setText("Aide en ligne");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_36_OBJ_37;
  private XRiTextField INDETB;
  private JLabel OBJ_37_OBJ_37;
  private XRiCalendrier RLDTRX;
  private JLabel OBJ_38_OBJ_39;
  private XRiTextField INDNCG;
  private JLabel OBJ_39_OBJ_40;
  private XRiTextField INDNCA;
  private JLabel OBJ_40_OBJ_41;
  private XRiTextField INDPCE;
  private JLabel OBJ_41_OBJ_42;
  private XRiTextField INDRAN;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_96_OBJ_96;
  private XRiTextField RLTYL;
  private RiZoneSortie LIBLIT;
  private JLabel OBJ_102_OBJ_102;
  private XRiCalendrier RLDRPX;
  private XRiTextField RLACT;
  private RiZoneSortie OBJ_108_OBJ_108;
  private XRiTextField RLMOL;
  private XRiTextField RLMOL1;
  private XRiTextField RLMOL2;
  private JXTitledSeparator xTitledSeparator3;
  private JComponent separator1;
  private JPanel panel4;
  private JLabel OBJ_115_OBJ_115;
  private XRiCalendrier RLDLYX;
  private JLabel OBJ_101_OBJ_101;
  private XRiTextField RLMTLX;
  private XRiTextField RLRGP;
  private RiZoneSortie OBJ_105_OBJ_105;
  private JXTitledPanel xTitledPanel2;
  private JLabel OBJ_66_OBJ_66;
  private RiZoneSortie OBJ_67_OBJ_67;
  private RiZoneSortie OBJ_68_OBJ_68;
  private JLabel OBJ_64_OBJ_64;
  private XRiTextField RLDECX;
  private JLabel OBJ_65_OBJ_65;
  private RiZoneSortie OBJ_61_OBJ_61;
  private RiZoneSortie OBJ_62_OBJ_62;
  private RiZoneSortie OBJ_63_OBJ_63;
  private RiZoneSortie OBJ_70_OBJ_70;
  private JXTitledPanel xTitledPanel3;
  private JLabel OBJ_74_OBJ_74;
  private XRiTextField RLNIV;
  private JLabel OBJ_76_OBJ_76;
  private RiZoneSortie OBJ_77_OBJ_77;
  private JLabel OBJ_79_OBJ_79;
  private RiZoneSortie WDV;
  private JLabel OBJ_80_OBJ_80;
  private XRiTextField RLRGL;
  private RiZoneSortie OBJ_82_OBJ_82;
  private RiZoneSortie OBJ_84_OBJ_84;
  private JLabel OBJ_85_OBJ_85;
  private JLabel OBJ_86_OBJ_86;
  private RiZoneSortie OBJ_87_OBJ_87;
  private RiZoneSortie OBJ_88_OBJ_88;
  private JLabel OBJ_89_OBJ_89;
  private JLabel OBJ_90_OBJ_90;
  private XRiTextField RLECHX;
  private JPanel panel2;
  private JLabel OBJ_50_OBJ_50;
  private XRiTextField RLTOR;
  private RiZoneSortie OBJ_52_OBJ_52;
  private XRiCalendrier RLARLX;
  private JLabel OBJ_55_OBJ_55;
  private JLabel OBJ_47_OBJ_47;
  private RiZoneSortie OBJ_48_OBJ_48;
  private RiZoneSortie OBJ_49_OBJ_49;
  private XRiCalendrier RLDDRX;
  private JLabel OBJ_53_OBJ_53;
  private JPanel panel3;
  private JXTitledSeparator xTitledSeparator1;
  private RiZoneSortie OBJ_29_OBJ_29;
  private RiZoneSortie OBJ_33_OBJ_33;
  private RiZoneSortie OBJ_36_OBJ_36;
  private RiZoneSortie OBJ_39_OBJ_39;
  private RiZoneSortie OBJ_42_OBJ_42;
  private RiZoneSortie OBJ_43_OBJ_43;
  private RiZoneSortie OBJ_44_OBJ_44;
  private RiZoneSortie OBJ_45_OBJ_45;
  private JXTitledSeparator xTitledSeparator2;
  private JLabel OBJ_30_OBJ_30;
  private RiZoneSortie OBJ_31_OBJ_31;
  private RiZoneSortie OBJ_32_OBJ_32;
  private RiZoneSortie OBJ_35_OBJ_35;
  private JLabel OBJ_34;
  private JLabel OBJ_37;
  private RiZoneSortie OBJ_38_OBJ_38;
  private RiZoneSortie OBJ_41_OBJ_41;
  private JLabel OBJ_40_OBJ_40;
  private JPopupMenu BTD;
  private JMenuItem OBJ_16;
  private JMenuItem OBJ_15;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
