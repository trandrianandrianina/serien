
package ri.serien.libecranrpg.vcgm.VCGM01FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM01FM_JO extends SNPanelEcranRPG implements ioFrame {
  
  private String[] JOCEN_Value = { "", "1", "2", "3", "4", "5", "6", };
  private String[] JOTPR_Value = { "", "1", "2", };
  private String[] JOTPR_Title = { "A facturer", "Engagé", "Reste à payer", };
  private String[] JOTYP_Value = { "1", "2", "3", };
  private String[] JOTYP_Title = { "Journal normal", "Journal provisionnel ou simulation", "Journal analytique", };
  private String[] JOTRE_Value = { " ", "B", "C", };
  private String[] JOTRE_Title = { "Non applicable", "Journal de banque", "Journal de caisse", };
  
  public VCGM01FM_JO(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    ZOAGB.activerModeFantome("Agence bancaire");
    
    // Ajout
    initDiverses();
    JOTRE.setValeurs(JOTRE_Value, JOTRE_Title);
    JOCEN.setValeurs(JOCEN_Value, null);
    JOTPR.setValeurs(JOTPR_Value, JOTPR_Title);
    JOTYP.setValeurs(JOTYP_Value, JOTYP_Title);
    JORVID.setValeursSelection("O", "");
    JONDE.setValeursSelection("1", "");
    JOAUT.setValeursSelection("1", "");
    
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDTYP@")).trim());
    INDIND.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDIND@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_88_OBJ_88.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UCLEX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    boolean isN72 = lexique.isTrue("N72");
    
    
    // JORVID.setSelected(lexique.HostFieldGetData("JORVID").equalsIgnoreCase("O"));
    // JOAUT.setSelected(lexique.HostFieldGetData("JOAUT").equalsIgnoreCase("1"));
    OBJ_89_OBJ_89.setVisible(lexique.isTrue("N71"));
    JONCG.setVisible(lexique.isTrue("N71"));
    JONCA.setVisible(lexique.isTrue("N71"));
    OBJ_77_OBJ_77.setVisible(isN72);
    OBJ_78_OBJ_78.setVisible(isN72);
    OBJ_79_OBJ_79.setVisible(isN72);
    OBJ_83_OBJ_83.setVisible(isN72);
    OBJ_90_OBJ_90.setVisible(isN72);
    OBJ_92_OBJ_92.setVisible(isN72);
    OBJ_94_OBJ_94.setVisible(isN72);
    OBJ_104_OBJ_104.setVisible(isN72);
    JOCBQ.setVisible(isN72);
    JOGUI.setVisible(isN72);
    JONCB.setVisible(isN72);
    JOCLE.setVisible(isN72);
    JOSWIF.setVisible(isN72);
    JOPIBA.setVisible(isN72);
    ZOAGB.setVisible(isN72);
    ZOEME.setVisible(isN72);
    PLAF.setVisible(isN72);
    JOTIR.setVisible(isN72);
    // JOCEN.setSelectedIndex(getIndice("JOCEN", JOCEN_Value));
    // JOTPR.setSelectedIndex(getIndice("JOTPR", JOTPR_Value));
    // JOTYP.setSelectedIndex(getIndice("JOTYP", JOTYP_Value));
    // JOTRE.setSelectedIndex(getIndice("JOTRE", JOTRE_Value));
    OBJ_106_OBJ_106.setVisible(isN72);
    JOCAL.setVisible(isN72);
    OBJ_108_OBJ_108.setVisible(isN72);
    CALVA.setVisible(isN72);
    OBJ_98_OBJ_98.setVisible(isN72);
    JOSJO.setVisible(isN72);
    OBJ_89_OBJ_89.setVisible(JONCG.isVisible());
    label1.setVisible(ZOAGB.isVisible());
    OBJ_92_OBJ_93.setVisible(ZOEME.isVisible());
    OBJ_89_OBJ_90.setVisible(JONCA.isVisible());
    
    if (!lexique.HostFieldGetData("JORVID").trim().isEmpty() && !lexique.HostFieldGetData("JORVID").equals("1")) {
      lexique.HostFieldPutData("JORVID", 0, "O");
    }
    if (!lexique.HostFieldGetData("JONDE").trim().isEmpty() && !lexique.HostFieldGetData("JONDE").equals("1")) {
      lexique.HostFieldPutData("JONDE", 0, "1");
    }
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTDI.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTDI.getInvoker().getName());
  }
  
  private void JOTREItemStateChanged(ItemEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44_OBJ_45 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_45_OBJ_46 = new JLabel();
    INDTYP = new RiZoneSortie();
    OBJ_46_OBJ_47 = new JLabel();
    INDIND = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    JOTYP = new XRiComboBox();
    JOTPR = new XRiComboBox();
    JOLIB = new XRiTextField();
    OBJ_56_OBJ_56 = new JLabel();
    OBJ_50_OBJ_50 = new JLabel();
    OBJ_54_OBJ_54 = new JLabel();
    OBJ_46_OBJ_46 = new JLabel();
    OBJ_52_OBJ_52 = new JLabel();
    OBJ_44_OBJ_44 = new JLabel();
    JOFMTC = new XRiTextField();
    JOFMTB = new XRiTextField();
    OBJ_48_OBJ_48 = new JLabel();
    JOEME = new XRiTextField();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_60_OBJ_60 = new JLabel();
    JOPCE = new XRiTextField();
    JONDE = new XRiCheckBox();
    JORVID = new XRiCheckBox();
    JOAUT = new XRiCheckBox();
    xTitledPanel3 = new JXTitledPanel();
    OBJ_67_OBJ_67 = new JLabel();
    OBJ_70_OBJ_70 = new JLabel();
    JOCEN = new XRiComboBox();
    OBJ_69_OBJ_69 = new JLabel();
    OBJ_73_OBJ_73 = new JLabel();
    OBJ_71_OBJ_71 = new JLabel();
    JOLIG = new XRiTextField();
    JOCOL = new XRiTextField();
    xTitledPanel4 = new JXTitledPanel();
    ZOAGB = new XRiTextField();
    OBJ_108_OBJ_108 = new JLabel();
    OBJ_94_OBJ_94 = new JLabel();
    JOSWIF = new XRiTextField();
    JOTIR = new XRiTextField();
    OBJ_98_OBJ_98 = new JLabel();
    JONCB = new XRiTextField();
    PLAF = new XRiTextField();
    OBJ_80_OBJ_80 = new JLabel();
    OBJ_106_OBJ_106 = new JLabel();
    OBJ_79_OBJ_79 = new JLabel();
    OBJ_92_OBJ_92 = new JLabel();
    JONCG = new XRiTextField();
    JONCA = new XRiTextField();
    ZOEME = new XRiTextField();
    OBJ_83_OBJ_83 = new JLabel();
    OBJ_77_OBJ_77 = new JLabel();
    OBJ_104_OBJ_104 = new JLabel();
    OBJ_78_OBJ_78 = new JLabel();
    JOCBQ = new XRiTextField();
    JOGUI = new XRiTextField();
    OBJ_90_OBJ_90 = new JLabel();
    OBJ_89_OBJ_89 = new JLabel();
    JOCLE = new XRiTextField();
    OBJ_88_OBJ_88 = new JLabel();
    JOSJO = new XRiTextField();
    JOPIBA = new XRiTextField();
    JOCAL = new XRiTextField();
    CALVA = new XRiTextField();
    OBJ_92_OBJ_93 = new JLabel();
    OBJ_89_OBJ_90 = new JLabel();
    label1 = new JLabel();
    JOTRE = new XRiComboBox();
    BTDI = new JPopupMenu();
    OBJ_16 = new JMenuItem();
    OBJ_15 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- OBJ_44_OBJ_45 ----
          OBJ_44_OBJ_45.setText("Soci\u00e9t\u00e9");
          OBJ_44_OBJ_45.setName("OBJ_44_OBJ_45");
          
          // ---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");
          
          // ---- OBJ_45_OBJ_46 ----
          OBJ_45_OBJ_46.setText("Code");
          OBJ_45_OBJ_46.setName("OBJ_45_OBJ_46");
          
          // ---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setOpaque(false);
          INDTYP.setText("@INDTYP@");
          INDTYP.setName("INDTYP");
          
          // ---- OBJ_46_OBJ_47 ----
          OBJ_46_OBJ_47.setText("Ordre");
          OBJ_46_OBJ_47.setName("OBJ_46_OBJ_47");
          
          // ---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setOpaque(false);
          INDIND.setText("@INDIND@");
          INDIND.setName("INDIND");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5)
                  .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE).addGap(13, 13, 13)
                  .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(40, 40, 40)
                  .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE).addGap(35, 35, 35)
                  .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE).addGap(10, 10, 10)
                  .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(3, 3, 3)
                  .addGroup(p_tete_gaucheLayout.createParallelGroup()
                      .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
          
          // ======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");
            
            // ---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);
          
          // ======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");
            
            // ---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);
          
          // ======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");
            
            // ---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);
          
          // ======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");
            
            // ---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);
          
          // ======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");
            
            // ---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);
          
          // ======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");
            
            // ---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);
          
          // ======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");
            
            // ---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);
          
          // ======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");
            
            // ---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);
          
          // ======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");
            
            // ---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(950, 590));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(920, 590));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);
          
          // ======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Journal comptable");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);
            
            // ---- JOTYP ----
            JOTYP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            JOTYP.setName("JOTYP");
            xTitledPanel1ContentContainer.add(JOTYP);
            JOTYP.setBounds(75, 46, 225, JOTYP.getPreferredSize().height);
            
            // ---- JOTPR ----
            JOTPR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            JOTPR.setName("JOTPR");
            xTitledPanel1ContentContainer.add(JOTPR);
            JOTPR.setBounds(425, 46, 145, JOTPR.getPreferredSize().height);
            
            // ---- JOLIB ----
            JOLIB.setName("JOLIB");
            xTitledPanel1ContentContainer.add(JOLIB);
            JOLIB.setBounds(75, 15, 310, JOLIB.getPreferredSize().height);
            
            // ---- OBJ_56_OBJ_56 ----
            OBJ_56_OBJ_56.setText("Si bon \u00e0 payer automatique");
            OBJ_56_OBJ_56.setName("OBJ_56_OBJ_56");
            xTitledPanel1ContentContainer.add(OBJ_56_OBJ_56);
            OBJ_56_OBJ_56.setBounds(155, 75, 167, 28);
            
            // ---- OBJ_50_OBJ_50 ----
            OBJ_50_OBJ_50.setText("Type pr\u00e9visionnel");
            OBJ_50_OBJ_50.setName("OBJ_50_OBJ_50");
            xTitledPanel1ContentContainer.add(OBJ_50_OBJ_50);
            OBJ_50_OBJ_50.setBounds(315, 45, 107, 28);
            
            // ---- OBJ_54_OBJ_54 ----
            OBJ_54_OBJ_54.setText("Code emetteur");
            OBJ_54_OBJ_54.setName("OBJ_54_OBJ_54");
            xTitledPanel1ContentContainer.add(OBJ_54_OBJ_54);
            OBJ_54_OBJ_54.setBounds(20, 75, 91, 28);
            
            // ---- OBJ_46_OBJ_46 ----
            OBJ_46_OBJ_46.setText("Format cpt 1/5");
            OBJ_46_OBJ_46.setName("OBJ_46_OBJ_46");
            xTitledPanel1ContentContainer.add(OBJ_46_OBJ_46);
            OBJ_46_OBJ_46.setBounds(720, 15, 87, 28);
            
            // ---- OBJ_52_OBJ_52 ----
            OBJ_52_OBJ_52.setText("Format cpt 6/7");
            OBJ_52_OBJ_52.setName("OBJ_52_OBJ_52");
            xTitledPanel1ContentContainer.add(OBJ_52_OBJ_52);
            OBJ_52_OBJ_52.setBounds(720, 45, 87, 28);
            
            // ---- OBJ_44_OBJ_44 ----
            OBJ_44_OBJ_44.setText("Libell\u00e9");
            OBJ_44_OBJ_44.setName("OBJ_44_OBJ_44");
            xTitledPanel1ContentContainer.add(OBJ_44_OBJ_44);
            OBJ_44_OBJ_44.setBounds(20, 15, 43, 28);
            
            // ---- JOFMTC ----
            JOFMTC.setName("JOFMTC");
            xTitledPanel1ContentContainer.add(JOFMTC);
            JOFMTC.setBounds(810, 45, 50, JOFMTC.getPreferredSize().height);
            
            // ---- JOFMTB ----
            JOFMTB.setName("JOFMTB");
            xTitledPanel1ContentContainer.add(JOFMTB);
            JOFMTB.setBounds(810, 15, 50, JOFMTB.getPreferredSize().height);
            
            // ---- OBJ_48_OBJ_48 ----
            OBJ_48_OBJ_48.setText("Type");
            OBJ_48_OBJ_48.setName("OBJ_48_OBJ_48");
            xTitledPanel1ContentContainer.add(OBJ_48_OBJ_48);
            OBJ_48_OBJ_48.setBounds(20, 45, 36, 28);
            
            // ---- JOEME ----
            JOEME.setComponentPopupMenu(BTDI);
            JOEME.setName("JOEME");
            xTitledPanel1ContentContainer.add(JOEME);
            JOEME.setBounds(115, 75, 30, JOEME.getPreferredSize().height);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(xTitledPanel1);
          xTitledPanel1.setBounds(25, 20, 880, 140);
          
          // ======== xTitledPanel2 ========
          {
            xTitledPanel2.setTitle("Num\u00e9rotation");
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);
            
            // ---- OBJ_60_OBJ_60 ----
            OBJ_60_OBJ_60.setText("Codage pi\u00e8ce");
            OBJ_60_OBJ_60.setName("OBJ_60_OBJ_60");
            xTitledPanel2ContentContainer.add(OBJ_60_OBJ_60);
            OBJ_60_OBJ_60.setBounds(15, 88, 90, 28);
            
            // ---- JOPCE ----
            JOPCE.setName("JOPCE");
            xTitledPanel2ContentContainer.add(JOPCE);
            JOPCE.setBounds(135, 88, 20, JOPCE.getPreferredSize().height);
            
            // ---- JONDE ----
            JONDE.setText("Non d\u00e9coupage sur rapprochement bancaire");
            JONDE.setName("JONDE");
            xTitledPanel2ContentContainer.add(JONDE);
            JONDE.setBounds(15, 36, 335, JONDE.getPreferredSize().height);
            
            // ---- JORVID ----
            JORVID.setText("Rapprochement bancaire sans bordereau");
            JORVID.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            JORVID.setName("JORVID");
            xTitledPanel2ContentContainer.add(JORVID);
            JORVID.setBounds(15, 62, 335, JORVID.getPreferredSize().height);
            
            // ---- JOAUT ----
            JOAUT.setText("Num\u00e9rotation automatique des folios sur quatre chiffres");
            JOAUT.setName("JOAUT");
            xTitledPanel2ContentContainer.add(JOAUT);
            JOAUT.setBounds(15, 10, 335, JOAUT.getPreferredSize().height);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(xTitledPanel2);
          xTitledPanel2.setBounds(25, 170, 433, 159);
          
          // ======== xTitledPanel3 ========
          {
            xTitledPanel3.setTitle("Centralisation");
            xTitledPanel3.setBorder(new DropShadowBorder());
            xTitledPanel3.setName("xTitledPanel3");
            Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
            xTitledPanel3ContentContainer.setLayout(null);
            
            // ---- OBJ_67_OBJ_67 ----
            OBJ_67_OBJ_67.setText("Edition sur journaux d'une centralisation de");
            OBJ_67_OBJ_67.setName("OBJ_67_OBJ_67");
            xTitledPanel3ContentContainer.add(OBJ_67_OBJ_67);
            OBJ_67_OBJ_67.setBounds(20, 5, 259, 26);
            
            // ---- OBJ_70_OBJ_70 ----
            OBJ_70_OBJ_70.setText("Position du curseur en saisie d'\u00e9criture");
            OBJ_70_OBJ_70.setName("OBJ_70_OBJ_70");
            xTitledPanel3ContentContainer.add(OBJ_70_OBJ_70);
            OBJ_70_OBJ_70.setBounds(20, 60, 231, 28);
            
            // ---- JOCEN ----
            JOCEN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            JOCEN.setName("JOCEN");
            xTitledPanel3ContentContainer.add(JOCEN);
            JOCEN.setBounds(285, 5, 47, JOCEN.getPreferredSize().height);
            
            // ---- OBJ_69_OBJ_69 ----
            OBJ_69_OBJ_69.setText("chiffre(s)");
            OBJ_69_OBJ_69.setName("OBJ_69_OBJ_69");
            xTitledPanel3ContentContainer.add(OBJ_69_OBJ_69);
            OBJ_69_OBJ_69.setBounds(340, 5, 54, 26);
            
            // ---- OBJ_73_OBJ_73 ----
            OBJ_73_OBJ_73.setText("colonne");
            OBJ_73_OBJ_73.setName("OBJ_73_OBJ_73");
            xTitledPanel3ContentContainer.add(OBJ_73_OBJ_73);
            OBJ_73_OBJ_73.setBounds(345, 60, 53, 28);
            
            // ---- OBJ_71_OBJ_71 ----
            OBJ_71_OBJ_71.setText("ligne");
            OBJ_71_OBJ_71.setName("OBJ_71_OBJ_71");
            xTitledPanel3ContentContainer.add(OBJ_71_OBJ_71);
            OBJ_71_OBJ_71.setBounds(270, 60, 32, 28);
            
            // ---- JOLIG ----
            JOLIG.setName("JOLIG");
            xTitledPanel3ContentContainer.add(JOLIG);
            JOLIG.setBounds(310, 60, 24, JOLIG.getPreferredSize().height);
            
            // ---- JOCOL ----
            JOCOL.setName("JOCOL");
            xTitledPanel3ContentContainer.add(JOCOL);
            JOCOL.setBounds(400, 60, 24, 28);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel3ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(xTitledPanel3);
          xTitledPanel3.setBounds(470, 170, 435, 159);
          
          // ======== xTitledPanel4 ========
          {
            xTitledPanel4.setTitle("Tr\u00e9sorerie");
            xTitledPanel4.setBorder(new DropShadowBorder());
            xTitledPanel4.setName("xTitledPanel4");
            Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
            xTitledPanel4ContentContainer.setLayout(null);
            
            // ---- ZOAGB ----
            ZOAGB.setName("ZOAGB");
            xTitledPanel4ContentContainer.add(ZOAGB);
            ZOAGB.setBounds(550, 85, 266, ZOAGB.getPreferredSize().height);
            
            // ---- OBJ_108_OBJ_108 ----
            OBJ_108_OBJ_108.setText("Calcul date de valeur");
            OBJ_108_OBJ_108.setName("OBJ_108_OBJ_108");
            xTitledPanel4ContentContainer.add(OBJ_108_OBJ_108);
            OBJ_108_OBJ_108.setBounds(575, 150, 135, 18);
            
            // ---- OBJ_94_OBJ_94 ----
            OBJ_94_OBJ_94.setText("Plafond d'escompte");
            OBJ_94_OBJ_94.setName("OBJ_94_OBJ_94");
            xTitledPanel4ContentContainer.add(OBJ_94_OBJ_94);
            OBJ_94_OBJ_94.setBounds(15, 90, 125, 19);
            
            // ---- JOSWIF ----
            JOSWIF.setName("JOSWIF");
            xTitledPanel4ContentContainer.add(JOSWIF);
            JOSWIF.setBounds(550, 55, 120, JOSWIF.getPreferredSize().height);
            
            // ---- JOTIR ----
            JOTIR.setName("JOTIR");
            xTitledPanel4ContentContainer.add(JOTIR);
            JOTIR.setBounds(135, 145, 125, JOTIR.getPreferredSize().height);
            
            // ---- OBJ_98_OBJ_98 ----
            OBJ_98_OBJ_98.setText("sous journal de");
            OBJ_98_OBJ_98.setName("OBJ_98_OBJ_98");
            xTitledPanel4ContentContainer.add(OBJ_98_OBJ_98);
            OBJ_98_OBJ_98.setBounds(15, 120, 125, 19);
            
            // ---- JONCB ----
            JONCB.setName("JONCB");
            xTitledPanel4ContentContainer.add(JONCB);
            JONCB.setBounds(660, 25, 100, JONCB.getPreferredSize().height);
            
            // ---- PLAF ----
            PLAF.setName("PLAF");
            xTitledPanel4ContentContainer.add(PLAF);
            PLAF.setBounds(135, 85, 125, PLAF.getPreferredSize().height);
            
            // ---- OBJ_80_OBJ_80 ----
            OBJ_80_OBJ_80.setText("Tr\u00e9sorerie");
            OBJ_80_OBJ_80.setName("OBJ_80_OBJ_80");
            xTitledPanel4ContentContainer.add(OBJ_80_OBJ_80);
            OBJ_80_OBJ_80.setBounds(15, 30, 123, 19);
            
            // ---- OBJ_106_OBJ_106 ----
            OBJ_106_OBJ_106.setText("Calendrier");
            OBJ_106_OBJ_106.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_106_OBJ_106.setName("OBJ_106_OBJ_106");
            xTitledPanel4ContentContainer.add(OBJ_106_OBJ_106);
            OBJ_106_OBJ_106.setBounds(475, 150, 70, 18);
            
            // ---- OBJ_79_OBJ_79 ----
            OBJ_79_OBJ_79.setText("Compte");
            OBJ_79_OBJ_79.setName("OBJ_79_OBJ_79");
            xTitledPanel4ContentContainer.add(OBJ_79_OBJ_79);
            OBJ_79_OBJ_79.setBounds(660, 10, 82, 18);
            
            // ---- OBJ_92_OBJ_92 ----
            OBJ_92_OBJ_92.setText("Pays IBAN");
            OBJ_92_OBJ_92.setName("OBJ_92_OBJ_92");
            xTitledPanel4ContentContainer.add(OBJ_92_OBJ_92);
            OBJ_92_OBJ_92.setBounds(695, 60, 74, 18);
            
            // ---- JONCG ----
            JONCG.setName("JONCG");
            xTitledPanel4ContentContainer.add(JONCG);
            JONCG.setBounds(135, 55, 70, JONCG.getPreferredSize().height);
            
            // ---- JONCA ----
            JONCA.setName("JONCA");
            xTitledPanel4ContentContainer.add(JONCA);
            JONCA.setBounds(315, 55, 70, JONCA.getPreferredSize().height);
            
            // ---- ZOEME ----
            ZOEME.setName("ZOEME");
            xTitledPanel4ContentContainer.add(ZOEME);
            ZOEME.setBounds(550, 115, 70, ZOEME.getPreferredSize().height);
            
            // ---- OBJ_83_OBJ_83 ----
            OBJ_83_OBJ_83.setText("RIB");
            OBJ_83_OBJ_83.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_83_OBJ_83.setName("OBJ_83_OBJ_83");
            xTitledPanel4ContentContainer.add(OBJ_83_OBJ_83);
            OBJ_83_OBJ_83.setBounds(500, 30, 43, 18);
            
            // ---- OBJ_77_OBJ_77 ----
            OBJ_77_OBJ_77.setText("Banque");
            OBJ_77_OBJ_77.setName("OBJ_77_OBJ_77");
            xTitledPanel4ContentContainer.add(OBJ_77_OBJ_77);
            OBJ_77_OBJ_77.setBounds(550, 10, 51, 18);
            
            // ---- OBJ_104_OBJ_104 ----
            OBJ_104_OBJ_104.setText("Tireur");
            OBJ_104_OBJ_104.setName("OBJ_104_OBJ_104");
            xTitledPanel4ContentContainer.add(OBJ_104_OBJ_104);
            OBJ_104_OBJ_104.setBounds(15, 150, 125, 19);
            
            // ---- OBJ_78_OBJ_78 ----
            OBJ_78_OBJ_78.setText("Guichet");
            OBJ_78_OBJ_78.setName("OBJ_78_OBJ_78");
            xTitledPanel4ContentContainer.add(OBJ_78_OBJ_78);
            OBJ_78_OBJ_78.setBounds(605, 10, 48, 18);
            
            // ---- JOCBQ ----
            JOCBQ.setName("JOCBQ");
            xTitledPanel4ContentContainer.add(JOCBQ);
            JOCBQ.setBounds(550, 25, 52, JOCBQ.getPreferredSize().height);
            
            // ---- JOGUI ----
            JOGUI.setName("JOGUI");
            xTitledPanel4ContentContainer.add(JOGUI);
            JOGUI.setBounds(605, 25, 52, JOGUI.getPreferredSize().height);
            
            // ---- OBJ_90_OBJ_90 ----
            OBJ_90_OBJ_90.setText("SWIFT");
            OBJ_90_OBJ_90.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_90_OBJ_90.setName("OBJ_90_OBJ_90");
            xTitledPanel4ContentContainer.add(OBJ_90_OBJ_90);
            OBJ_90_OBJ_90.setBounds(495, 60, 50, 18);
            
            // ---- OBJ_89_OBJ_89 ----
            OBJ_89_OBJ_89.setText("Compte g\u00e9n\u00e9ral");
            OBJ_89_OBJ_89.setName("OBJ_89_OBJ_89");
            xTitledPanel4ContentContainer.add(OBJ_89_OBJ_89);
            OBJ_89_OBJ_89.setBounds(15, 59, 125, 21);
            
            // ---- JOCLE ----
            JOCLE.setName("JOCLE");
            xTitledPanel4ContentContainer.add(JOCLE);
            JOCLE.setBounds(785, 25, 30, JOCLE.getPreferredSize().height);
            
            // ---- OBJ_88_OBJ_88 ----
            OBJ_88_OBJ_88.setText("@UCLEX@");
            OBJ_88_OBJ_88.setForeground(Color.red);
            OBJ_88_OBJ_88.setFont(OBJ_88_OBJ_88.getFont().deriveFont(OBJ_88_OBJ_88.getFont().getStyle() | Font.BOLD));
            OBJ_88_OBJ_88.setName("OBJ_88_OBJ_88");
            xTitledPanel4ContentContainer.add(OBJ_88_OBJ_88);
            OBJ_88_OBJ_88.setBounds(820, 27, 30, 25);
            
            // ---- JOSJO ----
            JOSJO.setName("JOSJO");
            xTitledPanel4ContentContainer.add(JOSJO);
            JOSJO.setBounds(135, 115, 24, JOSJO.getPreferredSize().height);
            
            // ---- JOPIBA ----
            JOPIBA.setName("JOPIBA");
            xTitledPanel4ContentContainer.add(JOPIBA);
            JOPIBA.setBounds(785, 55, 30, JOPIBA.getPreferredSize().height);
            
            // ---- JOCAL ----
            JOCAL.setComponentPopupMenu(BTDI);
            JOCAL.setName("JOCAL");
            xTitledPanel4ContentContainer.add(JOCAL);
            JOCAL.setBounds(550, 145, 20, JOCAL.getPreferredSize().height);
            
            // ---- CALVA ----
            CALVA.setComponentPopupMenu(BTDI);
            CALVA.setName("CALVA");
            xTitledPanel4ContentContainer.add(CALVA);
            CALVA.setBounds(705, 145, 20, CALVA.getPreferredSize().height);
            
            // ---- OBJ_92_OBJ_93 ----
            OBJ_92_OBJ_93.setText("N\u00b0 \u00e9metteur");
            OBJ_92_OBJ_93.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_92_OBJ_93.setName("OBJ_92_OBJ_93");
            xTitledPanel4ContentContainer.add(OBJ_92_OBJ_93);
            OBJ_92_OBJ_93.setBounds(465, 120, 80, 18);
            
            // ---- OBJ_89_OBJ_90 ----
            OBJ_89_OBJ_90.setText("Compte auxiliaire");
            OBJ_89_OBJ_90.setName("OBJ_89_OBJ_90");
            xTitledPanel4ContentContainer.add(OBJ_89_OBJ_90);
            OBJ_89_OBJ_90.setBounds(210, 59, 105, 21);
            
            // ---- label1 ----
            label1.setText("Agence bancaire");
            label1.setHorizontalAlignment(SwingConstants.RIGHT);
            label1.setName("label1");
            xTitledPanel4ContentContainer.add(label1);
            label1.setBounds(440, 90, 105, 20);
            
            // ---- JOTRE ----
            JOTRE.setName("JOTRE");
            JOTRE.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                JOTREItemStateChanged(e);
              }
            });
            xTitledPanel4ContentContainer.add(JOTRE);
            JOTRE.setBounds(135, 26, 145, JOTRE.getPreferredSize().height);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < xTitledPanel4ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel4ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel4ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel4ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel4ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(xTitledPanel4);
          xTitledPanel4.setBounds(25, 340, 880, 220);
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTDI ========
    {
      BTDI.setName("BTDI");
      
      // ---- OBJ_16 ----
      OBJ_16.setText("Choix possibles");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTDI.add(OBJ_16);
      
      // ---- OBJ_15 ----
      OBJ_15.setText("Aide en ligne");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTDI.add(OBJ_15);
    }
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
      
      // ---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44_OBJ_45;
  private RiZoneSortie INDETB;
  private JLabel OBJ_45_OBJ_46;
  private RiZoneSortie INDTYP;
  private JLabel OBJ_46_OBJ_47;
  private RiZoneSortie INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private XRiComboBox JOTYP;
  private XRiComboBox JOTPR;
  private XRiTextField JOLIB;
  private JLabel OBJ_56_OBJ_56;
  private JLabel OBJ_50_OBJ_50;
  private JLabel OBJ_54_OBJ_54;
  private JLabel OBJ_46_OBJ_46;
  private JLabel OBJ_52_OBJ_52;
  private JLabel OBJ_44_OBJ_44;
  private XRiTextField JOFMTC;
  private XRiTextField JOFMTB;
  private JLabel OBJ_48_OBJ_48;
  private XRiTextField JOEME;
  private JXTitledPanel xTitledPanel2;
  private JLabel OBJ_60_OBJ_60;
  private XRiTextField JOPCE;
  private XRiCheckBox JONDE;
  private XRiCheckBox JORVID;
  private XRiCheckBox JOAUT;
  private JXTitledPanel xTitledPanel3;
  private JLabel OBJ_67_OBJ_67;
  private JLabel OBJ_70_OBJ_70;
  private XRiComboBox JOCEN;
  private JLabel OBJ_69_OBJ_69;
  private JLabel OBJ_73_OBJ_73;
  private JLabel OBJ_71_OBJ_71;
  private XRiTextField JOLIG;
  private XRiTextField JOCOL;
  private JXTitledPanel xTitledPanel4;
  private XRiTextField ZOAGB;
  private JLabel OBJ_108_OBJ_108;
  private JLabel OBJ_94_OBJ_94;
  private XRiTextField JOSWIF;
  private XRiTextField JOTIR;
  private JLabel OBJ_98_OBJ_98;
  private XRiTextField JONCB;
  private XRiTextField PLAF;
  private JLabel OBJ_80_OBJ_80;
  private JLabel OBJ_106_OBJ_106;
  private JLabel OBJ_79_OBJ_79;
  private JLabel OBJ_92_OBJ_92;
  private XRiTextField JONCG;
  private XRiTextField JONCA;
  private XRiTextField ZOEME;
  private JLabel OBJ_83_OBJ_83;
  private JLabel OBJ_77_OBJ_77;
  private JLabel OBJ_104_OBJ_104;
  private JLabel OBJ_78_OBJ_78;
  private XRiTextField JOCBQ;
  private XRiTextField JOGUI;
  private JLabel OBJ_90_OBJ_90;
  private JLabel OBJ_89_OBJ_89;
  private XRiTextField JOCLE;
  private JLabel OBJ_88_OBJ_88;
  private XRiTextField JOSJO;
  private XRiTextField JOPIBA;
  private XRiTextField JOCAL;
  private XRiTextField CALVA;
  private JLabel OBJ_92_OBJ_93;
  private JLabel OBJ_89_OBJ_90;
  private JLabel label1;
  private XRiComboBox JOTRE;
  private JPopupMenu BTDI;
  private JMenuItem OBJ_16;
  private JMenuItem OBJ_15;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
