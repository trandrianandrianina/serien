
package ri.serien.libecranrpg.vcgm.VCGM01FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM01FM_EA extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] EASNS_Value = { "*", "C", "D", };
  private String[] EASNS_Title = { "Tous sens autorisés", "Crédit", "Débit", };
  
  public VCGM01FM_EA(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    EASNS.setValeurs(EASNS_Value, EASNS_Title);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    // EASNS.setSelectedIndex(getIndice("EASNS", EASNS_Value));
    
    EAJ01_CHK.setSelected(lexique.HostFieldGetData("EAJ01").equalsIgnoreCase("**"));
    P_SEL0.setVisible(!EAJ01_CHK.isSelected());
    EAL01_CHK.setSelected(lexique.HostFieldGetData("EAL01").equalsIgnoreCase("*"));
    P_SEL1.setVisible(!EAL01_CHK.isSelected());
    EAG01_CHK.setSelected(lexique.HostFieldGetData("EAG01").equalsIgnoreCase("**"));
    P_SEL2.setVisible(!EAG01_CHK.isSelected());
    
    

    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    // riMenu_bt2.setIcon(lexique.getImage("images/options.png",true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("EASNS", 0, EASNS_Value[EASNS.getSelectedIndex()]);
    
    if (EAJ01_CHK.isSelected()) {
      lexique.HostFieldPutData("EAJ01", 0, "**");
    }
    if (EAL01_CHK.isSelected()) {
      lexique.HostFieldPutData("EAL01", 0, "*");
    }
    if (EAG01_CHK.isSelected()) {
      lexique.HostFieldPutData("EAG01", 0, "**");
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void EAJ01_CHKActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
    if (!EAJ01_CHK.isSelected()) {
      EAJ01.setText("");
    }
  }
  
  private void EAL01_CHKActionPerformed(ActionEvent e) {
    P_SEL1.setVisible(!P_SEL1.isVisible());
    if (!EAL01_CHK.isSelected()) {
      EAL01.setText("");
    }
  }
  
  private void EAG01_CHKActionPerformed(ActionEvent e) {
    P_SEL2.setVisible(!P_SEL2.isVisible());
    if (!EAG01_CHK.isSelected()) {
      EAG01.setText("");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44_OBJ_45 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_45_OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46_OBJ_47 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_86_OBJ_86 = new JLabel();
    EAJ01_CHK = new JCheckBox();
    EAL01_CHK = new JCheckBox();
    EASNS = new XRiComboBox();
    P_SEL0 = new JPanel();
    OBJ_36_OBJ_36 = new JLabel();
    EAJ01 = new XRiTextField();
    EAJ02 = new XRiTextField();
    EAJ03 = new XRiTextField();
    EAJ04 = new XRiTextField();
    EAJ05 = new XRiTextField();
    P_SEL1 = new JPanel();
    OBJ_85_OBJ_86 = new JLabel();
    EAL01 = new XRiTextField();
    EAL02 = new XRiTextField();
    EAL03 = new XRiTextField();
    EAL04 = new XRiTextField();
    EAL05 = new XRiTextField();
    xTitledSeparator1 = new JXTitledSeparator();
    EAC01 = new XRiTextField();
    EAC02 = new XRiTextField();
    EAC03 = new XRiTextField();
    EAC04 = new XRiTextField();
    EAC05 = new XRiTextField();
    EAC06 = new XRiTextField();
    EAC07 = new XRiTextField();
    EAC08 = new XRiTextField();
    xTitledSeparator2 = new JXTitledSeparator();
    EAG01_CHK = new JCheckBox();
    P_SEL2 = new JPanel();
    EAG01 = new XRiTextField();
    EAG02 = new XRiTextField();
    EAG03 = new XRiTextField();
    EAG04 = new XRiTextField();
    EAG05 = new XRiTextField();
    OBJ_90_OBJ_90 = new JLabel();
    EARGL = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44_OBJ_45 ----
          OBJ_44_OBJ_45.setText("Soci\u00e9t\u00e9");
          OBJ_44_OBJ_45.setName("OBJ_44_OBJ_45");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setName("INDETB");

          //---- OBJ_45_OBJ_46 ----
          OBJ_45_OBJ_46.setText("Code");
          OBJ_45_OBJ_46.setName("OBJ_45_OBJ_46");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setName("INDTYP");

          //---- OBJ_46_OBJ_47 ----
          OBJ_46_OBJ_47.setText("Ordre");
          OBJ_46_OBJ_47.setName("OBJ_46_OBJ_47");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(780, 480));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setTitle("Extraction affacturage");
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //---- OBJ_86_OBJ_86 ----
            OBJ_86_OBJ_86.setText("Sens \u00e9critures comptables");
            OBJ_86_OBJ_86.setName("OBJ_86_OBJ_86");
            xTitledPanel2ContentContainer.add(OBJ_86_OBJ_86);
            OBJ_86_OBJ_86.setBounds(15, 123, 165, 20);

            //---- EAJ01_CHK ----
            EAJ01_CHK.setText("Tous les journaux");
            EAJ01_CHK.setName("EAJ01_CHK");
            EAJ01_CHK.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                EAJ01_CHKActionPerformed(e);
              }
            });
            xTitledPanel2ContentContainer.add(EAJ01_CHK);
            EAJ01_CHK.setBounds(15, 20, 160, 24);

            //---- EAL01_CHK ----
            EAL01_CHK.setText("Tous les libell\u00e9s");
            EAL01_CHK.setName("EAL01_CHK");
            EAL01_CHK.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                EAL01_CHKActionPerformed(e);
              }
            });
            xTitledPanel2ContentContainer.add(EAL01_CHK);
            EAL01_CHK.setBounds(15, 75, 160, 24);

            //---- EASNS ----
            EASNS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EASNS.setName("EASNS");
            xTitledPanel2ContentContainer.add(EASNS);
            EASNS.setBounds(190, 120, 165, EASNS.getPreferredSize().height);

            //======== P_SEL0 ========
            {
              P_SEL0.setOpaque(false);
              P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL0.setName("P_SEL0");
              P_SEL0.setLayout(null);

              //---- OBJ_36_OBJ_36 ----
              OBJ_36_OBJ_36.setText("Codes journaux");
              OBJ_36_OBJ_36.setName("OBJ_36_OBJ_36");
              P_SEL0.add(OBJ_36_OBJ_36);
              OBJ_36_OBJ_36.setBounds(10, 14, 110, 20);

              //---- EAJ01 ----
              EAJ01.setName("EAJ01");
              P_SEL0.add(EAJ01);
              EAJ01.setBounds(120, 10, 34, EAJ01.getPreferredSize().height);

              //---- EAJ02 ----
              EAJ02.setName("EAJ02");
              P_SEL0.add(EAJ02);
              EAJ02.setBounds(160, 10, 34, EAJ02.getPreferredSize().height);

              //---- EAJ03 ----
              EAJ03.setName("EAJ03");
              P_SEL0.add(EAJ03);
              EAJ03.setBounds(200, 10, 34, EAJ03.getPreferredSize().height);

              //---- EAJ04 ----
              EAJ04.setName("EAJ04");
              P_SEL0.add(EAJ04);
              EAJ04.setBounds(240, 10, 34, EAJ04.getPreferredSize().height);

              //---- EAJ05 ----
              EAJ05.setName("EAJ05");
              P_SEL0.add(EAJ05);
              EAJ05.setBounds(280, 10, 34, EAJ05.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < P_SEL0.getComponentCount(); i++) {
                  Rectangle bounds = P_SEL0.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = P_SEL0.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                P_SEL0.setMinimumSize(preferredSize);
                P_SEL0.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel2ContentContainer.add(P_SEL0);
            P_SEL0.setBounds(190, 10, 330, 45);

            //======== P_SEL1 ========
            {
              P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL1.setOpaque(false);
              P_SEL1.setName("P_SEL1");
              P_SEL1.setLayout(null);

              //---- OBJ_85_OBJ_86 ----
              OBJ_85_OBJ_86.setText("Codes libell\u00e9s");
              OBJ_85_OBJ_86.setName("OBJ_85_OBJ_86");
              P_SEL1.add(OBJ_85_OBJ_86);
              OBJ_85_OBJ_86.setBounds(10, 14, 110, 20);

              //---- EAL01 ----
              EAL01.setName("EAL01");
              P_SEL1.add(EAL01);
              EAL01.setBounds(160, 10, 20, EAL01.getPreferredSize().height);

              //---- EAL02 ----
              EAL02.setName("EAL02");
              P_SEL1.add(EAL02);
              EAL02.setBounds(120, 10, 20, EAL02.getPreferredSize().height);

              //---- EAL03 ----
              EAL03.setName("EAL03");
              P_SEL1.add(EAL03);
              EAL03.setBounds(200, 10, 20, EAL03.getPreferredSize().height);

              //---- EAL04 ----
              EAL04.setName("EAL04");
              P_SEL1.add(EAL04);
              EAL04.setBounds(240, 10, 20, EAL04.getPreferredSize().height);

              //---- EAL05 ----
              EAL05.setName("EAL05");
              P_SEL1.add(EAL05);
              EAL05.setBounds(280, 10, 20, EAL05.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < P_SEL1.getComponentCount(); i++) {
                  Rectangle bounds = P_SEL1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = P_SEL1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                P_SEL1.setMinimumSize(preferredSize);
                P_SEL1.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel2ContentContainer.add(P_SEL1);
            P_SEL1.setBounds(190, 65, 330, 45);

            //---- xTitledSeparator1 ----
            xTitledSeparator1.setTitle("Collectifs \u00e0 s\u00e9lectionner");
            xTitledSeparator1.setName("xTitledSeparator1");
            xTitledPanel2ContentContainer.add(xTitledSeparator1);
            xTitledSeparator1.setBounds(10, 300, 680, xTitledSeparator1.getPreferredSize().height);

            //---- EAC01 ----
            EAC01.setName("EAC01");
            xTitledPanel2ContentContainer.add(EAC01);
            EAC01.setBounds(45, 335, 70, EAC01.getPreferredSize().height);

            //---- EAC02 ----
            EAC02.setName("EAC02");
            xTitledPanel2ContentContainer.add(EAC02);
            EAC02.setBounds(125, 335, 70, EAC02.getPreferredSize().height);

            //---- EAC03 ----
            EAC03.setName("EAC03");
            xTitledPanel2ContentContainer.add(EAC03);
            EAC03.setBounds(210, 335, 70, EAC03.getPreferredSize().height);

            //---- EAC04 ----
            EAC04.setName("EAC04");
            xTitledPanel2ContentContainer.add(EAC04);
            EAC04.setBounds(290, 335, 70, EAC04.getPreferredSize().height);

            //---- EAC05 ----
            EAC05.setName("EAC05");
            xTitledPanel2ContentContainer.add(EAC05);
            EAC05.setBounds(370, 335, 70, EAC05.getPreferredSize().height);

            //---- EAC06 ----
            EAC06.setName("EAC06");
            xTitledPanel2ContentContainer.add(EAC06);
            EAC06.setBounds(455, 335, 70, EAC06.getPreferredSize().height);

            //---- EAC07 ----
            EAC07.setName("EAC07");
            xTitledPanel2ContentContainer.add(EAC07);
            EAC07.setBounds(535, 335, 70, EAC07.getPreferredSize().height);

            //---- EAC08 ----
            EAC08.setName("EAC08");
            xTitledPanel2ContentContainer.add(EAC08);
            EAC08.setBounds(620, 335, 70, EAC08.getPreferredSize().height);

            //---- xTitledSeparator2 ----
            xTitledSeparator2.setTitle("R\u00e8glements \u00e0 s\u00e9lectionner");
            xTitledSeparator2.setName("xTitledSeparator2");
            xTitledPanel2ContentContainer.add(xTitledSeparator2);
            xTitledSeparator2.setBounds(10, 165, 680, xTitledSeparator2.getPreferredSize().height);

            //---- EAG01_CHK ----
            EAG01_CHK.setText("Tous les r\u00e8glements");
            EAG01_CHK.setName("EAG01_CHK");
            EAG01_CHK.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                EAG01_CHKActionPerformed(e);
              }
            });
            xTitledPanel2ContentContainer.add(EAG01_CHK);
            EAG01_CHK.setBounds(15, 210, 160, EAG01_CHK.getPreferredSize().height);

            //======== P_SEL2 ========
            {
              P_SEL2.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL2.setOpaque(false);
              P_SEL2.setName("P_SEL2");
              P_SEL2.setLayout(null);

              //---- EAG01 ----
              EAG01.setName("EAG01");
              P_SEL2.add(EAG01);
              EAG01.setBounds(20, 15, 34, EAG01.getPreferredSize().height);

              //---- EAG02 ----
              EAG02.setName("EAG02");
              P_SEL2.add(EAG02);
              EAG02.setBounds(55, 15, 34, EAG02.getPreferredSize().height);

              //---- EAG03 ----
              EAG03.setName("EAG03");
              P_SEL2.add(EAG03);
              EAG03.setBounds(90, 15, 34, EAG03.getPreferredSize().height);

              //---- EAG04 ----
              EAG04.setName("EAG04");
              P_SEL2.add(EAG04);
              EAG04.setBounds(125, 15, 34, EAG04.getPreferredSize().height);

              //---- EAG05 ----
              EAG05.setName("EAG05");
              P_SEL2.add(EAG05);
              EAG05.setBounds(160, 15, 34, EAG05.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < P_SEL2.getComponentCount(); i++) {
                  Rectangle bounds = P_SEL2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = P_SEL2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                P_SEL2.setMinimumSize(preferredSize);
                P_SEL2.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel2ContentContainer.add(P_SEL2);
            P_SEL2.setBounds(190, 190, 215, 55);

            //---- OBJ_90_OBJ_90 ----
            OBJ_90_OBJ_90.setText("R\u00e8glement par d\u00e9faut");
            OBJ_90_OBJ_90.setName("OBJ_90_OBJ_90");
            xTitledPanel2ContentContainer.add(OBJ_90_OBJ_90);
            OBJ_90_OBJ_90.setBounds(20, 260, 132, 20);

            //---- EARGL ----
            EARGL.setName("EARGL");
            xTitledPanel2ContentContainer.add(EARGL);
            EARGL.setBounds(190, 255, 34, EARGL.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(xTitledPanel2);
          xTitledPanel2.setBounds(30, 30, 717, 417);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44_OBJ_45;
  private XRiTextField INDETB;
  private JLabel OBJ_45_OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_46_OBJ_47;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel2;
  private JLabel OBJ_86_OBJ_86;
  private JCheckBox EAJ01_CHK;
  private JCheckBox EAL01_CHK;
  private XRiComboBox EASNS;
  private JPanel P_SEL0;
  private JLabel OBJ_36_OBJ_36;
  private XRiTextField EAJ01;
  private XRiTextField EAJ02;
  private XRiTextField EAJ03;
  private XRiTextField EAJ04;
  private XRiTextField EAJ05;
  private JPanel P_SEL1;
  private JLabel OBJ_85_OBJ_86;
  private XRiTextField EAL01;
  private XRiTextField EAL02;
  private XRiTextField EAL03;
  private XRiTextField EAL04;
  private XRiTextField EAL05;
  private JXTitledSeparator xTitledSeparator1;
  private XRiTextField EAC01;
  private XRiTextField EAC02;
  private XRiTextField EAC03;
  private XRiTextField EAC04;
  private XRiTextField EAC05;
  private XRiTextField EAC06;
  private XRiTextField EAC07;
  private XRiTextField EAC08;
  private JXTitledSeparator xTitledSeparator2;
  private JCheckBox EAG01_CHK;
  private JPanel P_SEL2;
  private XRiTextField EAG01;
  private XRiTextField EAG02;
  private XRiTextField EAG03;
  private XRiTextField EAG04;
  private XRiTextField EAG05;
  private JLabel OBJ_90_OBJ_90;
  private XRiTextField EARGL;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
