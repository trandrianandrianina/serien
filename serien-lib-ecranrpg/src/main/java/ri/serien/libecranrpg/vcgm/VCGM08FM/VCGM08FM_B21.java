
package ri.serien.libecranrpg.vcgm.VCGM08FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGM08FM_B21 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM08FM_B21(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    DT101.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT101@")).trim());
    DT102.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT102@")).trim());
    DT103.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT103@")).trim());
    DT104.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT104@")).trim());
    DT105.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT105@")).trim());
    DT106.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT106@")).trim());
    DT107.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT107@")).trim());
    DT108.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT108@")).trim());
    DT109.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT109@")).trim());
    DT110.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT110@")).trim());
    DT111.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT111@")).trim());
    DT112.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DT112@")).trim());
    WTOT1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTOT1@")).trim());
    POU101.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU101@")).trim());
    POU102.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU102@")).trim());
    POU103.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU103@")).trim());
    POU104.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU104@")).trim());
    POU105.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU105@")).trim());
    POU106.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU106@")).trim());
    POU107.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU107@")).trim());
    POU108.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU108@")).trim());
    POU109.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU109@")).trim());
    POU110.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU110@")).trim());
    POU111.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU111@")).trim());
    POU112.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POU112@")).trim());
    POR101.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR101@")).trim());
    POR102.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR102@")).trim());
    POR103.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR103@")).trim());
    POR104.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR104@")).trim());
    POR105.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR105@")).trim());
    POR106.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR106@")).trim());
    POR107.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR107@")).trim());
    POR108.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR108@")).trim());
    POR109.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR109@")).trim());
    POR110.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR110@")).trim());
    POR111.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR111@")).trim());
    POR112.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@POR112@")).trim());
    WTOR1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTOR1@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITRE@"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    DT101 = new RiZoneSortie();
    DT102 = new RiZoneSortie();
    DT103 = new RiZoneSortie();
    DT104 = new RiZoneSortie();
    DT105 = new RiZoneSortie();
    DT106 = new RiZoneSortie();
    DT107 = new RiZoneSortie();
    DT108 = new RiZoneSortie();
    DT109 = new RiZoneSortie();
    DT110 = new RiZoneSortie();
    DT111 = new RiZoneSortie();
    DT112 = new RiZoneSortie();
    Total = new RiZoneSortie();
    XBM101 = new XRiTextField();
    XBM102 = new XRiTextField();
    XBM103 = new XRiTextField();
    XBM104 = new XRiTextField();
    XBM105 = new XRiTextField();
    XBM106 = new XRiTextField();
    XBM107 = new XRiTextField();
    XBM108 = new XRiTextField();
    XBM109 = new XRiTextField();
    XBM110 = new XRiTextField();
    XBM111 = new XRiTextField();
    XBM112 = new XRiTextField();
    WTOT1 = new RiZoneSortie();
    POU101 = new RiZoneSortie();
    POU102 = new RiZoneSortie();
    POU103 = new RiZoneSortie();
    POU104 = new RiZoneSortie();
    POU105 = new RiZoneSortie();
    POU106 = new RiZoneSortie();
    POU107 = new RiZoneSortie();
    POU108 = new RiZoneSortie();
    POU109 = new RiZoneSortie();
    POU110 = new RiZoneSortie();
    POU111 = new RiZoneSortie();
    POU112 = new RiZoneSortie();
    XBR101 = new XRiTextField();
    POR101 = new RiZoneSortie();
    XBR102 = new XRiTextField();
    POR102 = new RiZoneSortie();
    XBR103 = new XRiTextField();
    POR103 = new RiZoneSortie();
    XBR104 = new XRiTextField();
    POR104 = new RiZoneSortie();
    XBR105 = new XRiTextField();
    POR105 = new RiZoneSortie();
    XBR106 = new XRiTextField();
    POR106 = new RiZoneSortie();
    XBR107 = new XRiTextField();
    POR107 = new RiZoneSortie();
    XBR108 = new XRiTextField();
    POR108 = new RiZoneSortie();
    XBR109 = new XRiTextField();
    POR109 = new RiZoneSortie();
    XBR110 = new XRiTextField();
    POR110 = new RiZoneSortie();
    XBR111 = new XRiTextField();
    POR111 = new RiZoneSortie();
    XBR112 = new XRiTextField();
    POR112 = new RiZoneSortie();
    WTOR1 = new RiZoneSortie();
    separator1 = compFactory.createSeparator("Projet");
    separator2 = compFactory.createSeparator("R\u00e9actualis\u00e9");
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(740, 485));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- DT101 ----
          DT101.setText("@DT101@");
          DT101.setName("DT101");
          panel1.add(DT101);
          DT101.setBounds(new Rectangle(new Point(20, 50), DT101.getPreferredSize()));

          //---- DT102 ----
          DT102.setText("@DT102@");
          DT102.setName("DT102");
          panel1.add(DT102);
          DT102.setBounds(new Rectangle(new Point(20, 82), DT102.getPreferredSize()));

          //---- DT103 ----
          DT103.setText("@DT103@");
          DT103.setName("DT103");
          panel1.add(DT103);
          DT103.setBounds(new Rectangle(new Point(20, 114), DT103.getPreferredSize()));

          //---- DT104 ----
          DT104.setText("@DT104@");
          DT104.setName("DT104");
          panel1.add(DT104);
          DT104.setBounds(new Rectangle(new Point(20, 146), DT104.getPreferredSize()));

          //---- DT105 ----
          DT105.setText("@DT105@");
          DT105.setName("DT105");
          panel1.add(DT105);
          DT105.setBounds(new Rectangle(new Point(20, 178), DT105.getPreferredSize()));

          //---- DT106 ----
          DT106.setText("@DT106@");
          DT106.setName("DT106");
          panel1.add(DT106);
          DT106.setBounds(new Rectangle(new Point(20, 210), DT106.getPreferredSize()));

          //---- DT107 ----
          DT107.setText("@DT107@");
          DT107.setName("DT107");
          panel1.add(DT107);
          DT107.setBounds(new Rectangle(new Point(20, 242), DT107.getPreferredSize()));

          //---- DT108 ----
          DT108.setText("@DT108@");
          DT108.setName("DT108");
          panel1.add(DT108);
          DT108.setBounds(new Rectangle(new Point(20, 274), DT108.getPreferredSize()));

          //---- DT109 ----
          DT109.setText("@DT109@");
          DT109.setName("DT109");
          panel1.add(DT109);
          DT109.setBounds(new Rectangle(new Point(20, 306), DT109.getPreferredSize()));

          //---- DT110 ----
          DT110.setText("@DT110@");
          DT110.setName("DT110");
          panel1.add(DT110);
          DT110.setBounds(new Rectangle(new Point(20, 338), DT110.getPreferredSize()));

          //---- DT111 ----
          DT111.setText("@DT111@");
          DT111.setName("DT111");
          panel1.add(DT111);
          DT111.setBounds(new Rectangle(new Point(20, 370), DT111.getPreferredSize()));

          //---- DT112 ----
          DT112.setText("@DT112@");
          DT112.setName("DT112");
          panel1.add(DT112);
          DT112.setBounds(new Rectangle(new Point(20, 402), DT112.getPreferredSize()));

          //---- Total ----
          Total.setText("TOTAL");
          Total.setName("Total");
          panel1.add(Total);
          Total.setBounds(new Rectangle(new Point(20, 434), Total.getPreferredSize()));

          //---- XBM101 ----
          XBM101.setName("XBM101");
          panel1.add(XBM101);
          XBM101.setBounds(130, 48, 110, XBM101.getPreferredSize().height);

          //---- XBM102 ----
          XBM102.setName("XBM102");
          panel1.add(XBM102);
          XBM102.setBounds(130, 80, 110, XBM102.getPreferredSize().height);

          //---- XBM103 ----
          XBM103.setName("XBM103");
          panel1.add(XBM103);
          XBM103.setBounds(130, 112, 110, XBM103.getPreferredSize().height);

          //---- XBM104 ----
          XBM104.setName("XBM104");
          panel1.add(XBM104);
          XBM104.setBounds(130, 144, 110, XBM104.getPreferredSize().height);

          //---- XBM105 ----
          XBM105.setName("XBM105");
          panel1.add(XBM105);
          XBM105.setBounds(130, 176, 110, XBM105.getPreferredSize().height);

          //---- XBM106 ----
          XBM106.setName("XBM106");
          panel1.add(XBM106);
          XBM106.setBounds(130, 208, 110, XBM106.getPreferredSize().height);

          //---- XBM107 ----
          XBM107.setName("XBM107");
          panel1.add(XBM107);
          XBM107.setBounds(130, 240, 110, XBM107.getPreferredSize().height);

          //---- XBM108 ----
          XBM108.setName("XBM108");
          panel1.add(XBM108);
          XBM108.setBounds(130, 272, 110, XBM108.getPreferredSize().height);

          //---- XBM109 ----
          XBM109.setName("XBM109");
          panel1.add(XBM109);
          XBM109.setBounds(130, 304, 110, XBM109.getPreferredSize().height);

          //---- XBM110 ----
          XBM110.setName("XBM110");
          panel1.add(XBM110);
          XBM110.setBounds(130, 336, 110, XBM110.getPreferredSize().height);

          //---- XBM111 ----
          XBM111.setName("XBM111");
          panel1.add(XBM111);
          XBM111.setBounds(130, 368, 110, XBM111.getPreferredSize().height);

          //---- XBM112 ----
          XBM112.setName("XBM112");
          panel1.add(XBM112);
          XBM112.setBounds(130, 400, 110, XBM112.getPreferredSize().height);

          //---- WTOT1 ----
          WTOT1.setText("@WTOT1@");
          WTOT1.setName("WTOT1");
          panel1.add(WTOT1);
          WTOT1.setBounds(130, 434, 110, WTOT1.getPreferredSize().height);

          //---- POU101 ----
          POU101.setText("@POU101@");
          POU101.setName("POU101");
          panel1.add(POU101);
          POU101.setBounds(245, 50, 50, POU101.getPreferredSize().height);

          //---- POU102 ----
          POU102.setText("@POU102@");
          POU102.setName("POU102");
          panel1.add(POU102);
          POU102.setBounds(245, 82, 50, POU102.getPreferredSize().height);

          //---- POU103 ----
          POU103.setText("@POU103@");
          POU103.setName("POU103");
          panel1.add(POU103);
          POU103.setBounds(245, 114, 50, POU103.getPreferredSize().height);

          //---- POU104 ----
          POU104.setText("@POU104@");
          POU104.setName("POU104");
          panel1.add(POU104);
          POU104.setBounds(245, 146, 50, POU104.getPreferredSize().height);

          //---- POU105 ----
          POU105.setText("@POU105@");
          POU105.setName("POU105");
          panel1.add(POU105);
          POU105.setBounds(245, 178, 50, POU105.getPreferredSize().height);

          //---- POU106 ----
          POU106.setText("@POU106@");
          POU106.setName("POU106");
          panel1.add(POU106);
          POU106.setBounds(245, 210, 50, POU106.getPreferredSize().height);

          //---- POU107 ----
          POU107.setText("@POU107@");
          POU107.setName("POU107");
          panel1.add(POU107);
          POU107.setBounds(245, 242, 50, POU107.getPreferredSize().height);

          //---- POU108 ----
          POU108.setText("@POU108@");
          POU108.setName("POU108");
          panel1.add(POU108);
          POU108.setBounds(245, 274, 50, POU108.getPreferredSize().height);

          //---- POU109 ----
          POU109.setText("@POU109@");
          POU109.setName("POU109");
          panel1.add(POU109);
          POU109.setBounds(245, 306, 50, POU109.getPreferredSize().height);

          //---- POU110 ----
          POU110.setText("@POU110@");
          POU110.setName("POU110");
          panel1.add(POU110);
          POU110.setBounds(245, 338, 50, POU110.getPreferredSize().height);

          //---- POU111 ----
          POU111.setText("@POU111@");
          POU111.setName("POU111");
          panel1.add(POU111);
          POU111.setBounds(245, 370, 50, POU111.getPreferredSize().height);

          //---- POU112 ----
          POU112.setText("@POU112@");
          POU112.setName("POU112");
          panel1.add(POU112);
          POU112.setBounds(245, 402, 50, POU112.getPreferredSize().height);

          //---- XBR101 ----
          XBR101.setName("XBR101");
          panel1.add(XBR101);
          XBR101.setBounds(360, 48, 110, XBR101.getPreferredSize().height);

          //---- POR101 ----
          POR101.setText("@POR101@");
          POR101.setName("POR101");
          panel1.add(POR101);
          POR101.setBounds(475, 50, 50, POR101.getPreferredSize().height);

          //---- XBR102 ----
          XBR102.setName("XBR102");
          panel1.add(XBR102);
          XBR102.setBounds(360, 80, 110, XBR102.getPreferredSize().height);

          //---- POR102 ----
          POR102.setText("@POR102@");
          POR102.setName("POR102");
          panel1.add(POR102);
          POR102.setBounds(475, 82, 50, POR102.getPreferredSize().height);

          //---- XBR103 ----
          XBR103.setName("XBR103");
          panel1.add(XBR103);
          XBR103.setBounds(360, 112, 110, XBR103.getPreferredSize().height);

          //---- POR103 ----
          POR103.setText("@POR103@");
          POR103.setName("POR103");
          panel1.add(POR103);
          POR103.setBounds(475, 114, 50, POR103.getPreferredSize().height);

          //---- XBR104 ----
          XBR104.setName("XBR104");
          panel1.add(XBR104);
          XBR104.setBounds(360, 144, 110, XBR104.getPreferredSize().height);

          //---- POR104 ----
          POR104.setText("@POR104@");
          POR104.setName("POR104");
          panel1.add(POR104);
          POR104.setBounds(475, 146, 50, POR104.getPreferredSize().height);

          //---- XBR105 ----
          XBR105.setName("XBR105");
          panel1.add(XBR105);
          XBR105.setBounds(360, 176, 110, XBR105.getPreferredSize().height);

          //---- POR105 ----
          POR105.setText("@POR105@");
          POR105.setName("POR105");
          panel1.add(POR105);
          POR105.setBounds(475, 178, 50, POR105.getPreferredSize().height);

          //---- XBR106 ----
          XBR106.setName("XBR106");
          panel1.add(XBR106);
          XBR106.setBounds(360, 208, 110, XBR106.getPreferredSize().height);

          //---- POR106 ----
          POR106.setText("@POR106@");
          POR106.setName("POR106");
          panel1.add(POR106);
          POR106.setBounds(475, 210, 50, POR106.getPreferredSize().height);

          //---- XBR107 ----
          XBR107.setName("XBR107");
          panel1.add(XBR107);
          XBR107.setBounds(360, 240, 110, XBR107.getPreferredSize().height);

          //---- POR107 ----
          POR107.setText("@POR107@");
          POR107.setName("POR107");
          panel1.add(POR107);
          POR107.setBounds(475, 242, 50, POR107.getPreferredSize().height);

          //---- XBR108 ----
          XBR108.setName("XBR108");
          panel1.add(XBR108);
          XBR108.setBounds(360, 272, 110, XBR108.getPreferredSize().height);

          //---- POR108 ----
          POR108.setText("@POR108@");
          POR108.setName("POR108");
          panel1.add(POR108);
          POR108.setBounds(475, 274, 50, POR108.getPreferredSize().height);

          //---- XBR109 ----
          XBR109.setName("XBR109");
          panel1.add(XBR109);
          XBR109.setBounds(360, 304, 110, XBR109.getPreferredSize().height);

          //---- POR109 ----
          POR109.setText("@POR109@");
          POR109.setName("POR109");
          panel1.add(POR109);
          POR109.setBounds(475, 306, 50, POR109.getPreferredSize().height);

          //---- XBR110 ----
          XBR110.setName("XBR110");
          panel1.add(XBR110);
          XBR110.setBounds(360, 336, 110, XBR110.getPreferredSize().height);

          //---- POR110 ----
          POR110.setText("@POR110@");
          POR110.setName("POR110");
          panel1.add(POR110);
          POR110.setBounds(475, 338, 50, POR110.getPreferredSize().height);

          //---- XBR111 ----
          XBR111.setName("XBR111");
          panel1.add(XBR111);
          XBR111.setBounds(360, 368, 110, XBR111.getPreferredSize().height);

          //---- POR111 ----
          POR111.setText("@POR111@");
          POR111.setName("POR111");
          panel1.add(POR111);
          POR111.setBounds(475, 370, 50, POR111.getPreferredSize().height);

          //---- XBR112 ----
          XBR112.setName("XBR112");
          panel1.add(XBR112);
          XBR112.setBounds(360, 400, 110, XBR112.getPreferredSize().height);

          //---- POR112 ----
          POR112.setText("@POR112@");
          POR112.setName("POR112");
          panel1.add(POR112);
          POR112.setBounds(475, 402, 50, POR112.getPreferredSize().height);

          //---- WTOR1 ----
          WTOR1.setText("@WTOR1@");
          WTOR1.setName("WTOR1");
          panel1.add(WTOR1);
          WTOR1.setBounds(360, 434, 110, WTOR1.getPreferredSize().height);

          //---- separator1 ----
          separator1.setName("separator1");
          panel1.add(separator1);
          separator1.setBounds(130, 25, 175, separator1.getPreferredSize().height);

          //---- separator2 ----
          separator2.setName("separator2");
          panel1.add(separator2);
          separator2.setBounds(360, 25, 175, separator2.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(5, 5, 560, 475);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie DT101;
  private RiZoneSortie DT102;
  private RiZoneSortie DT103;
  private RiZoneSortie DT104;
  private RiZoneSortie DT105;
  private RiZoneSortie DT106;
  private RiZoneSortie DT107;
  private RiZoneSortie DT108;
  private RiZoneSortie DT109;
  private RiZoneSortie DT110;
  private RiZoneSortie DT111;
  private RiZoneSortie DT112;
  private RiZoneSortie Total;
  private XRiTextField XBM101;
  private XRiTextField XBM102;
  private XRiTextField XBM103;
  private XRiTextField XBM104;
  private XRiTextField XBM105;
  private XRiTextField XBM106;
  private XRiTextField XBM107;
  private XRiTextField XBM108;
  private XRiTextField XBM109;
  private XRiTextField XBM110;
  private XRiTextField XBM111;
  private XRiTextField XBM112;
  private RiZoneSortie WTOT1;
  private RiZoneSortie POU101;
  private RiZoneSortie POU102;
  private RiZoneSortie POU103;
  private RiZoneSortie POU104;
  private RiZoneSortie POU105;
  private RiZoneSortie POU106;
  private RiZoneSortie POU107;
  private RiZoneSortie POU108;
  private RiZoneSortie POU109;
  private RiZoneSortie POU110;
  private RiZoneSortie POU111;
  private RiZoneSortie POU112;
  private XRiTextField XBR101;
  private RiZoneSortie POR101;
  private XRiTextField XBR102;
  private RiZoneSortie POR102;
  private XRiTextField XBR103;
  private RiZoneSortie POR103;
  private XRiTextField XBR104;
  private RiZoneSortie POR104;
  private XRiTextField XBR105;
  private RiZoneSortie POR105;
  private XRiTextField XBR106;
  private RiZoneSortie POR106;
  private XRiTextField XBR107;
  private RiZoneSortie POR107;
  private XRiTextField XBR108;
  private RiZoneSortie POR108;
  private XRiTextField XBR109;
  private RiZoneSortie POR109;
  private XRiTextField XBR110;
  private RiZoneSortie POR110;
  private XRiTextField XBR111;
  private RiZoneSortie POR111;
  private XRiTextField XBR112;
  private RiZoneSortie POR112;
  private RiZoneSortie WTOR1;
  private JComponent separator1;
  private JComponent separator2;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
