
package ri.serien.libecranrpg.vcgm.VCGM27FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

public class VCGM27FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  // TODO declarations classe spécifiques...
  
  public VCGM27FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // TODO constructeur spécifiques...
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    

    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // TODO getDATA spécifiques
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    OBJ_31 = new JLabel();
    DECO14 = new XRiTextField();
    OBJ_29 = new JLabel();
    OBJ_42 = new JLabel();
    OBJ_44 = new JLabel();
    OBJ_35 = new JLabel();
    OBJ_46 = new JLabel();
    OBJ_52 = new JLabel();
    OBJ_48 = new JLabel();
    OBJ_26 = new JLabel();
    OBJ_37 = new JLabel();
    DECO4 = new XRiTextField();
    DECO6 = new XRiTextField();
    OBJ_53 = new JLabel();
    OBJ_33 = new JLabel();
    DECO8 = new XRiTextField();
    OBJ_50 = new JLabel();
    OBJ_39 = new JLabel();
    DECO7 = new XRiTextField();
    DECO2 = new XRiTextField();
    DECO10 = new XRiTextField();
    BMOIX = new XRiTextField();
    DECO3 = new XRiTextField();
    DECO13 = new XRiTextField();
    DECO5 = new XRiTextField();
    DECO9 = new XRiTextField();
    DECO11 = new XRiTextField();
    DECO12 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(925, 275));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel2 ========
        {
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- OBJ_31 ----
          OBJ_31.setText("Pays destination/provenance");
          OBJ_31.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 12));
          OBJ_31.setName("OBJ_31");
          panel2.add(OBJ_31);
          OBJ_31.setBounds(380, 19, 195, 20);

          //---- DECO14 ----
          DECO14.setComponentPopupMenu(BTD);
          DECO14.setName("DECO14");
          panel2.add(DECO14);
          DECO14.setBounds(580, 160, 156, DECO14.getPreferredSize().height);

          //---- OBJ_29 ----
          OBJ_29.setText("Nomenclature du produit");
          OBJ_29.setName("OBJ_29");
          panel2.add(OBJ_29);
          OBJ_29.setBounds(20, 48, 195, 20);

          //---- OBJ_42 ----
          OBJ_42.setText("Unit\u00e9s suppl\u00e9mentaires");
          OBJ_42.setName("OBJ_42");
          panel2.add(OBJ_42);
          OBJ_42.setBounds(20, 135, 195, 20);

          //---- OBJ_44 ----
          OBJ_44.setText("Nature de la transaction");
          OBJ_44.setName("OBJ_44");
          panel2.add(OBJ_44);
          OBJ_44.setBounds(380, 106, 195, 20);

          //---- OBJ_35 ----
          OBJ_35.setText("R\u00e9gime de l'op\u00e9ration");
          OBJ_35.setName("OBJ_35");
          panel2.add(OBJ_35);
          OBJ_35.setBounds(380, 48, 195, 20);

          //---- OBJ_46 ----
          OBJ_46.setText("Condition de livraison");
          OBJ_46.setName("OBJ_46");
          panel2.add(OBJ_46);
          OBJ_46.setBounds(20, 164, 195, 20);

          //---- OBJ_52 ----
          OBJ_52.setText("N\u00b0TVA Intracommunautaire");
          OBJ_52.setName("OBJ_52");
          panel2.add(OBJ_52);
          OBJ_52.setBounds(380, 164, 195, 20);

          //---- OBJ_48 ----
          OBJ_48.setText("Mode de transport");
          OBJ_48.setName("OBJ_48");
          panel2.add(OBJ_48);
          OBJ_48.setBounds(380, 135, 195, 20);

          //---- OBJ_26 ----
          OBJ_26.setText("Mois d'imputation");
          OBJ_26.setName("OBJ_26");
          panel2.add(OBJ_26);
          OBJ_26.setBounds(20, 19, 195, 20);

          //---- OBJ_37 ----
          OBJ_37.setText("Valeur statistique");
          OBJ_37.setName("OBJ_37");
          panel2.add(OBJ_37);
          OBJ_37.setBounds(20, 106, 195, 20);

          //---- DECO4 ----
          DECO4.setComponentPopupMenu(BTD);
          DECO4.setName("DECO4");
          panel2.add(DECO4);
          DECO4.setBounds(220, 73, 108, DECO4.getPreferredSize().height);

          //---- DECO6 ----
          DECO6.setComponentPopupMenu(BTD);
          DECO6.setName("DECO6");
          panel2.add(DECO6);
          DECO6.setBounds(220, 102, 108, DECO6.getPreferredSize().height);

          //---- OBJ_53 ----
          OBJ_53.setText("Pays d'origine");
          OBJ_53.setName("OBJ_53");
          panel2.add(OBJ_53);
          OBJ_53.setBounds(20, 222, 195, 20);

          //---- OBJ_33 ----
          OBJ_33.setText("Valeur fiscale");
          OBJ_33.setName("OBJ_33");
          panel2.add(OBJ_33);
          OBJ_33.setBounds(20, 77, 195, 20);

          //---- DECO8 ----
          DECO8.setComponentPopupMenu(BTD);
          DECO8.setName("DECO8");
          panel2.add(DECO8);
          DECO8.setBounds(220, 131, 100, DECO8.getPreferredSize().height);

          //---- OBJ_50 ----
          OBJ_50.setText("D\u00e9partement");
          OBJ_50.setName("OBJ_50");
          panel2.add(OBJ_50);
          OBJ_50.setBounds(20, 193, 195, 20);

          //---- OBJ_39 ----
          OBJ_39.setText("Masse nette");
          OBJ_39.setName("OBJ_39");
          panel2.add(OBJ_39);
          OBJ_39.setBounds(380, 77, 195, 20);

          //---- DECO7 ----
          DECO7.setComponentPopupMenu(BTD);
          DECO7.setName("DECO7");
          panel2.add(DECO7);
          DECO7.setBounds(580, 73, 92, DECO7.getPreferredSize().height);

          //---- DECO2 ----
          DECO2.setComponentPopupMenu(BTD);
          DECO2.setName("DECO2");
          panel2.add(DECO2);
          DECO2.setBounds(220, 44, 100, DECO2.getPreferredSize().height);

          //---- DECO10 ----
          DECO10.setComponentPopupMenu(BTD);
          DECO10.setName("DECO10");
          panel2.add(DECO10);
          DECO10.setBounds(220, 160, 48, DECO10.getPreferredSize().height);

          //---- BMOIX ----
          BMOIX.setComponentPopupMenu(BTD);
          BMOIX.setName("BMOIX");
          panel2.add(BMOIX);
          BMOIX.setBounds(220, 15, 52, BMOIX.getPreferredSize().height);

          //---- DECO3 ----
          DECO3.setComponentPopupMenu(BTD);
          DECO3.setName("DECO3");
          panel2.add(DECO3);
          DECO3.setBounds(580, 15, 36, DECO3.getPreferredSize().height);

          //---- DECO13 ----
          DECO13.setComponentPopupMenu(BTD);
          DECO13.setName("DECO13");
          panel2.add(DECO13);
          DECO13.setBounds(220, 218, 36, DECO13.getPreferredSize().height);

          //---- DECO5 ----
          DECO5.setComponentPopupMenu(BTD);
          DECO5.setName("DECO5");
          panel2.add(DECO5);
          DECO5.setBounds(580, 44, 30, DECO5.getPreferredSize().height);

          //---- DECO9 ----
          DECO9.setComponentPopupMenu(BTD);
          DECO9.setName("DECO9");
          panel2.add(DECO9);
          DECO9.setBounds(580, 102, 30, DECO9.getPreferredSize().height);

          //---- DECO11 ----
          DECO11.setComponentPopupMenu(BTD);
          DECO11.setName("DECO11");
          panel2.add(DECO11);
          DECO11.setBounds(580, 131, 20, DECO11.getPreferredSize().height);

          //---- DECO12 ----
          DECO12.setComponentPopupMenu(BTD);
          DECO12.setName("DECO12");
          panel2.add(DECO12);
          DECO12.setBounds(220, 189, 30, DECO12.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel2);
        panel2.setBounds(5, 5, 745, 265);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Choix possibles");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel2;
  private JLabel OBJ_31;
  private XRiTextField DECO14;
  private JLabel OBJ_29;
  private JLabel OBJ_42;
  private JLabel OBJ_44;
  private JLabel OBJ_35;
  private JLabel OBJ_46;
  private JLabel OBJ_52;
  private JLabel OBJ_48;
  private JLabel OBJ_26;
  private JLabel OBJ_37;
  private XRiTextField DECO4;
  private XRiTextField DECO6;
  private JLabel OBJ_53;
  private JLabel OBJ_33;
  private XRiTextField DECO8;
  private JLabel OBJ_50;
  private JLabel OBJ_39;
  private XRiTextField DECO7;
  private XRiTextField DECO2;
  private XRiTextField DECO10;
  private XRiTextField BMOIX;
  private XRiTextField DECO3;
  private XRiTextField DECO13;
  private XRiTextField DECO5;
  private XRiTextField DECO9;
  private XRiTextField DECO11;
  private XRiTextField DECO12;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
