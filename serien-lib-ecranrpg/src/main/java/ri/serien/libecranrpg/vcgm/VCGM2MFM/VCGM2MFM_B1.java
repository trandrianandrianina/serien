
package ri.serien.libecranrpg.vcgm.VCGM2MFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGM2MFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM2MFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDefaultButton(bouton_valider);
    setDialog(true);
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    gererLesErreurs("19");
    
    
    
    // TODO Icones
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_15_OBJ_15 = new JLabel();
    P0DTEX = new XRiTextField();
    OBJ_17_OBJ_17 = new JLabel();
    P0CJOL = new XRiTextField();
    OBJ_19_OBJ_19 = new JLabel();
    P0CFOL = new XRiTextField();
    OBJ_21_OBJ_21 = new JLabel();
    P0NLIL = new XRiTextField();
    OBJ_23_OBJ_23 = new JLabel();
    P0SUFL = new XRiTextField();
    OBJ_25_OBJ_25 = new JLabel();
    P0PCE = new XRiTextField();
    OBJ_27_OBJ_27 = new JLabel();
    L1MTT = new XRiTextField();
    panel2 = new JPanel();
    P0IN1 = new XRiCheckBox();
    P0IN2 = new XRiCheckBox();
    P0IN3 = new XRiCheckBox();
    OBJ_33_OBJ_33 = new JLabel();
    P0DTDX = new XRiTextField();
    OBJ_35_OBJ_35 = new JLabel();
    P0EBPD = new XRiTextField();
    OBJ_37_OBJ_37 = new JLabel();
    P0DTAX = new XRiTextField();
    OBJ_39_OBJ_39 = new JLabel();
    P0EBPA = new XRiTextField();
    OBJ_41_OBJ_41 = new JLabel();
    OBJ_43_OBJ_43 = new JLabel();
    P0DTLX = new XRiTextField();
    P0DTCX = new XRiTextField();
    OBJ_45_OBJ_45 = new JLabel();
    P0EBPC = new XRiTextField();
    OBJ_47_OBJ_47 = new JLabel();
    P0MTT = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(750, 485));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("R\u00e9capitulatif d'\u00e9criture"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_15_OBJ_15 ----
          OBJ_15_OBJ_15.setText("Date");
          OBJ_15_OBJ_15.setName("OBJ_15_OBJ_15");
          panel1.add(OBJ_15_OBJ_15);
          OBJ_15_OBJ_15.setBounds(15, 40, 70, 20);

          //---- P0DTEX ----
          P0DTEX.setName("P0DTEX");
          panel1.add(P0DTEX);
          P0DTEX.setBounds(85, 35, 52, P0DTEX.getPreferredSize().height);

          //---- OBJ_17_OBJ_17 ----
          OBJ_17_OBJ_17.setText("Journal");
          OBJ_17_OBJ_17.setName("OBJ_17_OBJ_17");
          panel1.add(OBJ_17_OBJ_17);
          OBJ_17_OBJ_17.setBounds(150, 40, 42, 20);

          //---- P0CJOL ----
          P0CJOL.setName("P0CJOL");
          panel1.add(P0CJOL);
          P0CJOL.setBounds(205, 35, 30, P0CJOL.getPreferredSize().height);

          //---- OBJ_19_OBJ_19 ----
          OBJ_19_OBJ_19.setText("Folio");
          OBJ_19_OBJ_19.setName("OBJ_19_OBJ_19");
          panel1.add(OBJ_19_OBJ_19);
          OBJ_19_OBJ_19.setBounds(245, 40, 33, 20);

          //---- P0CFOL ----
          P0CFOL.setName("P0CFOL");
          panel1.add(P0CFOL);
          P0CFOL.setBounds(290, 35, 44, P0CFOL.getPreferredSize().height);

          //---- OBJ_21_OBJ_21 ----
          OBJ_21_OBJ_21.setText("Ligne");
          OBJ_21_OBJ_21.setName("OBJ_21_OBJ_21");
          panel1.add(OBJ_21_OBJ_21);
          OBJ_21_OBJ_21.setBounds(350, 40, 36, 20);

          //---- P0NLIL ----
          P0NLIL.setName("P0NLIL");
          panel1.add(P0NLIL);
          P0NLIL.setBounds(400, 35, 44, P0NLIL.getPreferredSize().height);

          //---- OBJ_23_OBJ_23 ----
          OBJ_23_OBJ_23.setText("Suffixe");
          OBJ_23_OBJ_23.setName("OBJ_23_OBJ_23");
          panel1.add(OBJ_23_OBJ_23);
          OBJ_23_OBJ_23.setBounds(455, 40, 40, 20);

          //---- P0SUFL ----
          P0SUFL.setName("P0SUFL");
          panel1.add(P0SUFL);
          P0SUFL.setBounds(510, 35, 30, P0SUFL.getPreferredSize().height);

          //---- OBJ_25_OBJ_25 ----
          OBJ_25_OBJ_25.setText("N\u00b0 de pi\u00e8ce");
          OBJ_25_OBJ_25.setName("OBJ_25_OBJ_25");
          panel1.add(OBJ_25_OBJ_25);
          OBJ_25_OBJ_25.setBounds(15, 80, 70, 20);

          //---- P0PCE ----
          P0PCE.setName("P0PCE");
          panel1.add(P0PCE);
          P0PCE.setBounds(85, 75, 68, P0PCE.getPreferredSize().height);

          //---- OBJ_27_OBJ_27 ----
          OBJ_27_OBJ_27.setText("Montant");
          OBJ_27_OBJ_27.setName("OBJ_27_OBJ_27");
          panel1.add(OBJ_27_OBJ_27);
          OBJ_27_OBJ_27.setBounds(15, 120, 70, 20);

          //---- L1MTT ----
          L1MTT.setName("L1MTT");
          panel1.add(L1MTT);
          L1MTT.setBounds(85, 120, 130, L1MTT.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 560, 176);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Prorogation"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- P0IN1 ----
          P0IN1.setText("Demand\u00e9e");
          P0IN1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          P0IN1.setName("P0IN1");
          panel2.add(P0IN1);
          P0IN1.setBounds(15, 40, 93, 20);

          //---- P0IN2 ----
          P0IN2.setText("Accept\u00e9e");
          P0IN2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          P0IN2.setName("P0IN2");
          panel2.add(P0IN2);
          P0IN2.setBounds(215, 40, 83, 20);

          //---- P0IN3 ----
          P0IN3.setText("A ne pas d\u00e9clarer");
          P0IN3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          P0IN3.setName("P0IN3");
          panel2.add(P0IN3);
          P0IN3.setBounds(395, 40, 131, 20);

          //---- OBJ_33_OBJ_33 ----
          OBJ_33_OBJ_33.setText("Date de demande");
          OBJ_33_OBJ_33.setName("OBJ_33_OBJ_33");
          panel2.add(OBJ_33_OBJ_33);
          OBJ_33_OBJ_33.setBounds(15, 100, 180, 20);

          //---- P0DTDX ----
          P0DTDX.setName("P0DTDX");
          panel2.add(P0DTDX);
          P0DTDX.setBounds(215, 100, 76, P0DTDX.getPreferredSize().height);

          //---- OBJ_35_OBJ_35 ----
          OBJ_35_OBJ_35.setText("Emetteur");
          OBJ_35_OBJ_35.setName("OBJ_35_OBJ_35");
          panel2.add(OBJ_35_OBJ_35);
          OBJ_35_OBJ_35.setBounds(395, 100, 56, 20);

          //---- P0EBPD ----
          P0EBPD.setName("P0EBPD");
          panel2.add(P0EBPD);
          P0EBPD.setBounds(500, 100, 30, P0EBPD.getPreferredSize().height);

          //---- OBJ_37_OBJ_37 ----
          OBJ_37_OBJ_37.setText("Date d'acceptation");
          OBJ_37_OBJ_37.setName("OBJ_37_OBJ_37");
          panel2.add(OBJ_37_OBJ_37);
          OBJ_37_OBJ_37.setBounds(15, 130, 180, 20);

          //---- P0DTAX ----
          P0DTAX.setName("P0DTAX");
          panel2.add(P0DTAX);
          P0DTAX.setBounds(215, 130, 76, P0DTAX.getPreferredSize().height);

          //---- OBJ_39_OBJ_39 ----
          OBJ_39_OBJ_39.setText("Emetteur");
          OBJ_39_OBJ_39.setName("OBJ_39_OBJ_39");
          panel2.add(OBJ_39_OBJ_39);
          OBJ_39_OBJ_39.setBounds(395, 130, 56, 20);

          //---- P0EBPA ----
          P0EBPA.setName("P0EBPA");
          panel2.add(P0EBPA);
          P0EBPA.setBounds(500, 130, 30, P0EBPA.getPreferredSize().height);

          //---- OBJ_41_OBJ_41 ----
          OBJ_41_OBJ_41.setText("Date d'acceptation limite");
          OBJ_41_OBJ_41.setName("OBJ_41_OBJ_41");
          panel2.add(OBJ_41_OBJ_41);
          OBJ_41_OBJ_41.setBounds(15, 160, 180, 20);

          //---- OBJ_43_OBJ_43 ----
          OBJ_43_OBJ_43.setText("D\u00e9claration contentieux");
          OBJ_43_OBJ_43.setName("OBJ_43_OBJ_43");
          panel2.add(OBJ_43_OBJ_43);
          OBJ_43_OBJ_43.setBounds(15, 190, 180, 20);

          //---- P0DTLX ----
          P0DTLX.setName("P0DTLX");
          panel2.add(P0DTLX);
          P0DTLX.setBounds(215, 160, 76, P0DTLX.getPreferredSize().height);

          //---- P0DTCX ----
          P0DTCX.setName("P0DTCX");
          panel2.add(P0DTCX);
          P0DTCX.setBounds(215, 190, 76, P0DTCX.getPreferredSize().height);

          //---- OBJ_45_OBJ_45 ----
          OBJ_45_OBJ_45.setText("Emetteur");
          OBJ_45_OBJ_45.setName("OBJ_45_OBJ_45");
          panel2.add(OBJ_45_OBJ_45);
          OBJ_45_OBJ_45.setBounds(395, 190, 56, 20);

          //---- P0EBPC ----
          P0EBPC.setName("P0EBPC");
          panel2.add(P0EBPC);
          P0EBPC.setBounds(500, 190, 30, P0EBPC.getPreferredSize().height);

          //---- OBJ_47_OBJ_47 ----
          OBJ_47_OBJ_47.setText("Montant");
          OBJ_47_OBJ_47.setName("OBJ_47_OBJ_47");
          panel2.add(OBJ_47_OBJ_47);
          OBJ_47_OBJ_47.setBounds(15, 235, 180, 20);

          //---- P0MTT ----
          P0MTT.setName("P0MTT");
          panel2.add(P0MTT);
          P0MTT.setBounds(215, 230, 110, P0MTT.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel2);
        panel2.setBounds(10, 195, 560, 275);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_15_OBJ_15;
  private XRiTextField P0DTEX;
  private JLabel OBJ_17_OBJ_17;
  private XRiTextField P0CJOL;
  private JLabel OBJ_19_OBJ_19;
  private XRiTextField P0CFOL;
  private JLabel OBJ_21_OBJ_21;
  private XRiTextField P0NLIL;
  private JLabel OBJ_23_OBJ_23;
  private XRiTextField P0SUFL;
  private JLabel OBJ_25_OBJ_25;
  private XRiTextField P0PCE;
  private JLabel OBJ_27_OBJ_27;
  private XRiTextField L1MTT;
  private JPanel panel2;
  private XRiCheckBox P0IN1;
  private XRiCheckBox P0IN2;
  private XRiCheckBox P0IN3;
  private JLabel OBJ_33_OBJ_33;
  private XRiTextField P0DTDX;
  private JLabel OBJ_35_OBJ_35;
  private XRiTextField P0EBPD;
  private JLabel OBJ_37_OBJ_37;
  private XRiTextField P0DTAX;
  private JLabel OBJ_39_OBJ_39;
  private XRiTextField P0EBPA;
  private JLabel OBJ_41_OBJ_41;
  private JLabel OBJ_43_OBJ_43;
  private XRiTextField P0DTLX;
  private XRiTextField P0DTCX;
  private JLabel OBJ_45_OBJ_45;
  private XRiTextField P0EBPC;
  private JLabel OBJ_47_OBJ_47;
  private XRiTextField P0MTT;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

}
