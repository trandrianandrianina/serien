
package ri.serien.libecranrpg.vcgm.VCGMAGFM;
// Nom Fichier: pop_805.java

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VCGMAGFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  // private String[] _LIST2_Top=null;
  private String[] _YD01_Title = { "Date", "", "Type de frais", "Montant", };
  private String[][] _YD01_Data =
      { { "YD01", "YC01", "YL01", "YM01", }, { "YD02", "YC02", "YL02", "YM02", }, { "YD03", "YC03", "YL03", "YM03", },
          { "YD04", "YC04", "YL04", "YM04", }, { "YD05", "YC05", "YL05", "YM05", }, { "YD06", "YC06", "YL06", "YM06", },
          { "YD07", "YC07", "YL07", "YM07", }, { "YD08", "YC08", "YL08", "YM08", }, { "YD09", "YC09", "YL09", "YM09", },
          { "YD10", "YC10", "YL10", "YM10", }, { "YD11", "YC11", "YL11", "YM11", }, { "YD12", "YC12", "YL12", "YM12", },
          { "YD13", "YC13", "YL13", "YM13", }, { "YD14", "YC14", "YL14", "YM14", }, { "YD15", "YC15", "YL15", "YM15", }, };
  private int[] _YD01_Width = { 43, 39, 216, 90, };
  private String[][] _LIST2_Title_Data_Brut = null;
  
  public VCGMAGFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    // _LIST2_Title_Data_Brut = initTable(LIST2, "Courier New", Font.BOLD, 12, "Courier New", Font.PLAIN, 12);
    setDialog(true);
    
    // Ajout
    initDiverses();
    YD01.setAspectTable(null, _YD01_Title, _YD01_Data, _YD01_Width, false, null, null, null, null);
    
    // Bouton par défaut
    setDefaultButton(VAL);
    
    // Menu Command
    // setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST2, _LIST2_Title_Data_Brut, _LIST2_Top);
    
    
    
    // TODO Icones
    BT_PGDOWN.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    BT_PGUP.setIcon(lexique.chargerImage("images/pgup20.png", true));
    VAL.setIcon(lexique.chargerImage("images/OK.png", true));
    OBJ_32.setIcon(lexique.chargerImage("images/retour.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void VALActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="enter"
    // ScriptCall("G_Touche")
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_32ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_Touche")
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void BT_PGUPActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGUP", false);
  }
  
  private void BT_PGDOWNActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGDOWN", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", true);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F8"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F8", true);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F10"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F10", true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F13"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F13", true);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F14"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F14", true);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F15"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F15", true);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F16"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F16", true);
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F18"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F18", true);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vcgmag"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    panel2 = new JPanel();
    SCROLLPANE_LIST2 = new JScrollPane();
    YD01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    panel1 = new JPanel();
    VAL = new JButton();
    OBJ_32 = new JButton();
    OBJ_4 = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    OBJ_15 = new JMenuItem();
    OBJ_16 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();

    //======== this ========
    setName("this");

    //======== panel2 ========
    {
      panel2.setBorder(new TitledBorder("Ventilation par types de frais"));
      panel2.setName("panel2");
      panel2.setLayout(null);

      //======== SCROLLPANE_LIST2 ========
      {
        SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

        //---- YD01 ----
        YD01.setName("YD01");
        SCROLLPANE_LIST2.setViewportView(YD01);
      }
      panel2.add(SCROLLPANE_LIST2);
      SCROLLPANE_LIST2.setBounds(25, 40, 430, 267);

      //---- BT_PGUP ----
      BT_PGUP.setText("");
      BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
      BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_PGUP.setName("BT_PGUP");
      BT_PGUP.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_PGUPActionPerformed(e);
        }
      });
      panel2.add(BT_PGUP);
      BT_PGUP.setBounds(460, 40, 25, 120);

      //---- BT_PGDOWN ----
      BT_PGDOWN.setText("");
      BT_PGDOWN.setToolTipText("Page suivante");
      BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_PGDOWN.setName("BT_PGDOWN");
      BT_PGDOWN.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_PGDOWNActionPerformed(e);
        }
      });
      panel2.add(BT_PGDOWN);
      BT_PGDOWN.setBounds(460, 185, 25, 120);
    }

    //======== panel1 ========
    {
      panel1.setName("panel1");
      panel1.setLayout(null);

      //---- VAL ----
      VAL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      VAL.setToolTipText("Ok");
      VAL.setName("VAL");
      VAL.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          VALActionPerformed(e);
        }
      });
      panel1.add(VAL);
      VAL.setBounds(10, 5, 56, 40);

      //---- OBJ_32 ----
      OBJ_32.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_32.setToolTipText("Retour");
      OBJ_32.setName("OBJ_32");
      OBJ_32.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_32ActionPerformed(e);
        }
      });
      panel1.add(OBJ_32);
      OBJ_32.setBounds(70, 5, 56, 40);
    }

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGroup(layout.createParallelGroup()
            .addGroup(layout.createSequentialGroup()
              .addGap(405, 405, 405)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
              .addGap(25, 25, 25)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 505, GroupLayout.PREFERRED_SIZE)))
          .addContainerGap(20, Short.MAX_VALUE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(20, 20, 20)
          .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 345, GroupLayout.PREFERRED_SIZE)
          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
          .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
    );

    //======== OBJ_4 ========
    {
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Recherche multi-crit\u00e8res");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setText("Fonctions");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_9);

      //---- OBJ_10 ----
      OBJ_10.setText("Annuler");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_10);
      OBJ_4.addSeparator();

      //---- OBJ_11 ----
      OBJ_11.setText("Cr\u00e9ation");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Modification");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Interrogation");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("Annulation");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_14);

      //---- OBJ_15 ----
      OBJ_15.setText("Duplication");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_15);
      OBJ_4.addSeparator();

      //---- OBJ_16 ----
      OBJ_16.setText("Exploitation");
      OBJ_16.setName("OBJ_16");
      OBJ_4.add(OBJ_16);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_19 ----
      OBJ_19.setText("Invite");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel panel2;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable YD01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPanel panel1;
  private JButton VAL;
  private JButton OBJ_32;
  private JPopupMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_15;
  private JMenuItem OBJ_16;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
