
package ri.serien.libecranrpg.vcgm.VCGM53FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VCGM53FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] _LA01_Title = { "HLD01", };
  private String[][] _LA01_Data = { { "LA01", }, { "LA02", }, { "LA03", }, { "LA04", }, { "LA05", }, { "LA06", }, { "LA07", },
      { "LA08", }, { "LA09", }, { "LA10", }, { "LA11", }, { "LA12", }, { "LA13", }, { "LA14", }, { "LA15", }, { "LA16", }, };
  private int[] _LA01_Width = { 474, };
  
  public VCGM53FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    setDialog(true);
    
    // Ajout
    initDiverses();
    LA01.setAspectTable(null, _LA01_Title, _LA01_Data, _LA01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA01@")).trim());
    label3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA02@")).trim());
    label4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA03@")).trim());
    label5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA04@")).trim());
    label6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA05@")).trim());
    label7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA06@")).trim());
    label8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA07@")).trim());
    label9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA08@")).trim());
    label10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA09@")).trim());
    label11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA10@")).trim());
    label12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA11@")).trim());
    label13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA12@")).trim());
    label14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA13@")).trim());
    label15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA14@")).trim());
    label16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA15@")).trim());
    label17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA16@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    


    
    
    OBJ_68.setVisible(interpreteurD.analyseExpression("@LA16@").contains(">"));
    OBJ_68.setText(changerBouton(MTA16, OBJ_68));
    OBJ_67.setVisible(interpreteurD.analyseExpression("@LA15@").contains(">"));
    OBJ_67.setText(changerBouton(MTA15, OBJ_67));
    OBJ_66.setVisible(interpreteurD.analyseExpression("@LA14@").contains(">"));
    OBJ_66.setText(changerBouton(MTA14, OBJ_66));
    OBJ_65.setVisible(interpreteurD.analyseExpression("@LA13@").contains(">"));
    OBJ_65.setText(changerBouton(MTA13, OBJ_65));
    OBJ_64.setVisible(interpreteurD.analyseExpression("@LA12@").contains(">"));
    OBJ_64.setText(changerBouton(MTA12, OBJ_64));
    OBJ_63.setVisible(interpreteurD.analyseExpression("@LA11@").contains(">"));
    OBJ_63.setText(changerBouton(MTA11, OBJ_63));
    OBJ_62.setVisible(interpreteurD.analyseExpression("@LA10@").contains(">"));
    OBJ_62.setText(changerBouton(MTA10, OBJ_62));
    OBJ_61.setVisible(interpreteurD.analyseExpression("@LA09@").contains(">"));
    OBJ_61.setText(changerBouton(MTA09, OBJ_61));
    OBJ_60.setVisible(interpreteurD.analyseExpression("@LA08@").contains(">"));
    OBJ_60.setText(changerBouton(MTA08, OBJ_60));
    OBJ_59.setVisible(interpreteurD.analyseExpression("@LA07@").contains(">"));
    OBJ_59.setText(changerBouton(MTA07, OBJ_59));
    OBJ_58.setVisible(interpreteurD.analyseExpression("@LA06@").contains(">"));
    OBJ_58.setText(changerBouton(MTA06, OBJ_58));
    OBJ_57.setVisible(interpreteurD.analyseExpression("@LA05@").contains(">"));
    OBJ_57.setText(changerBouton(MTA05, OBJ_57));
    OBJ_56.setVisible(interpreteurD.analyseExpression("@LA04@").contains(">"));
    OBJ_56.setText(changerBouton(MTA04, OBJ_56));
    OBJ_55.setVisible(interpreteurD.analyseExpression("@LA03@").contains(">"));
    OBJ_55.setText(changerBouton(MTA03, OBJ_55));
    OBJ_54.setVisible(interpreteurD.analyseExpression("@LA02@").contains(">"));
    OBJ_54.setText(changerBouton(MTA02, OBJ_54));
    OBJ_53.setVisible(interpreteurD.analyseExpression("@LA01@").contains(">"));
    OBJ_53.setText(changerBouton(MTA01, OBJ_53));
    WDIFE.setVisible(lexique.isTrue("31"));
    OBJ_69.setVisible(lexique.isTrue("31"));
    label18.setVisible(lexique.isTrue("61"));
    // panel2.setEnabled(lexique.isTrue("40"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Affectation ou vue des réglements"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void dupliquerValeur(JButton bouton, XRiTextField zone, String variable) {
    if (bouton.getText().equals(">")) {
      zone.setText(lexique.HostFieldGetData(variable).substring(50, 61));
      bouton.setText("<");
    }
    else {
      zone.setText("");
      bouton.setText(">");
    }
  }
  
  private String changerBouton(XRiTextField zone, JButton button) {
    zone.setVisible(button.isVisible());
    button.setEnabled(zone.isEnabled());
    if (zone.getText().equals("")) {
      return ">";
    }
    else {
      return "<";
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F16");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void LA01MouseClicked(MouseEvent e) {
    lexique.HostScreenSendKey(this, "DUPLICATE");
  }
  
  private void OBJ_53ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_53, MTA01, "LA01");
  }
  
  private void OBJ_54ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_54, MTA02, "LA02");
  }
  
  private void OBJ_55ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_55, MTA03, "LA03");
  }
  
  private void OBJ_56ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_56, MTA04, "LA04");
  }
  
  private void OBJ_57ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_57, MTA05, "LA05");
  }
  
  private void OBJ_58ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_58, MTA06, "LA06");
  }
  
  private void OBJ_59ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_59, MTA07, "LA07");
  }
  
  private void OBJ_60ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_60, MTA08, "LA08");
  }
  
  private void OBJ_61ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_61, MTA09, "LA09");
  }
  
  private void OBJ_62ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_62, MTA10, "LA10");
  }
  
  private void OBJ_63ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_63, MTA11, "LA11");
  }
  
  private void OBJ_64ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_64, MTA12, "LA12");
  }
  
  private void OBJ_65ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_65, MTA13, "LA13");
  }
  
  private void OBJ_66ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_66, MTA14, "LA14");
  }
  
  private void OBJ_67ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_67, MTA15, "LA15");
  }
  
  private void OBJ_68ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_68, MTA16, "LA16");
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "DUPLICATE");
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F17");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F13");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel3 = new JPanel();
    WDIFE = new XRiTextField();
    TOTAFF = new XRiTextField();
    OBJ_69 = new JLabel();
    SCROLLPANE_LIST = new JScrollPane();
    LA01 = new XRiTable();
    MTA01 = new XRiTextField();
    MTA02 = new XRiTextField();
    MTA03 = new XRiTextField();
    MTA04 = new XRiTextField();
    MTA05 = new XRiTextField();
    MTA06 = new XRiTextField();
    MTA07 = new XRiTextField();
    MTA08 = new XRiTextField();
    MTA09 = new XRiTextField();
    MTA10 = new XRiTextField();
    MTA16 = new XRiTextField();
    MTA14 = new XRiTextField();
    MTA15 = new XRiTextField();
    MTA13 = new XRiTextField();
    MTA12 = new XRiTextField();
    MTA11 = new XRiTextField();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();
    label11 = new JLabel();
    label12 = new JLabel();
    label13 = new JLabel();
    label14 = new JLabel();
    label15 = new JLabel();
    label16 = new JLabel();
    label17 = new JLabel();
    label1 = new JLabel();
    label18 = new JLabel();
    panel2 = new JPanel();
    OBJ_53 = new JButton();
    OBJ_54 = new JButton();
    OBJ_55 = new JButton();
    OBJ_56 = new JButton();
    OBJ_57 = new JButton();
    OBJ_58 = new JButton();
    OBJ_59 = new JButton();
    OBJ_60 = new JButton();
    OBJ_61 = new JButton();
    OBJ_62 = new JButton();
    OBJ_63 = new JButton();
    OBJ_64 = new JButton();
    OBJ_65 = new JButton();
    OBJ_66 = new JButton();
    OBJ_67 = new JButton();
    OBJ_68 = new JButton();
    label19 = new JLabel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BTD = new JPopupMenu();
    OBJ_14 = new JMenuItem();
    OBJ_15 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    OBJ_16 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(960, 425));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Diff\u00e9rence de r\u00e8glement");
            riSousMenu_bt6.setToolTipText("Demande d'acceptation de diff\u00e9rence r\u00e8glement (s'il existe un param\u00e8tre EP)");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Annulation affectations");
            riSousMenu_bt7.setToolTipText("Demande d'annulation des affectations");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);

          //======== riSousMenu8 ========
          {
            riSousMenu8.setName("riSousMenu8");

            //---- riSousMenu_bt8 ----
            riSousMenu_bt8.setText("Autre affichage");
            riSousMenu_bt8.setToolTipText("Changement affichage des \u00e9critures");
            riSousMenu_bt8.setName("riSousMenu_bt8");
            riSousMenu_bt8.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt8ActionPerformed(e);
              }
            });
            riSousMenu8.add(riSousMenu_bt8);
          }
          menus_haut.add(riSousMenu8);

          //======== riSousMenu9 ========
          {
            riSousMenu9.setName("riSousMenu9");

            //---- riSousMenu_bt9 ----
            riSousMenu_bt9.setText("S\u00e9lection d'une \u00e9ch\u00e9ance");
            riSousMenu_bt9.setToolTipText("S\u00e9lection d'une \u00e9ch\u00e9ance");
            riSousMenu_bt9.setName("riSousMenu_bt9");
            riSousMenu_bt9.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt9ActionPerformed(e);
              }
            });
            riSousMenu9.add(riSousMenu_bt9);
          }
          menus_haut.add(riSousMenu9);

          //======== riSousMenu10 ========
          {
            riSousMenu10.setName("riSousMenu10");

            //---- riSousMenu_bt10 ----
            riSousMenu_bt10.setText("S\u00e9lection globale");
            riSousMenu_bt10.setToolTipText("S\u00e9lection globale");
            riSousMenu_bt10.setName("riSousMenu_bt10");
            riSousMenu_bt10.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt10ActionPerformed(e);
              }
            });
            riSousMenu10.add(riSousMenu_bt10);
          }
          menus_haut.add(riSousMenu10);

          //======== riSousMenu11 ========
          {
            riSousMenu11.setName("riSousMenu11");

            //---- riSousMenu_bt11 ----
            riSousMenu_bt11.setText("Ventilation r\u00e8glement");
            riSousMenu_bt11.setToolTipText("Ventilation r\u00e8glement");
            riSousMenu_bt11.setName("riSousMenu_bt11");
            riSousMenu_bt11.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt11ActionPerformed(e);
              }
            });
            riSousMenu11.add(riSousMenu_bt11);
          }
          menus_haut.add(riSousMenu11);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel3 ========
        {
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);

          //---- WDIFE ----
          WDIFE.setName("WDIFE");
          panel3.add(WDIFE);
          WDIFE.setBounds(430, 365, 100, WDIFE.getPreferredSize().height);

          //---- TOTAFF ----
          TOTAFF.setName("TOTAFF");
          panel3.add(TOTAFF);
          TOTAFF.setBounds(590, 365, 100, TOTAFF.getPreferredSize().height);

          //---- OBJ_69 ----
          OBJ_69.setText("Ecart");
          OBJ_69.setHorizontalTextPosition(SwingConstants.CENTER);
          OBJ_69.setFont(OBJ_69.getFont().deriveFont(OBJ_69.getFont().getStyle() | Font.BOLD, OBJ_69.getFont().getSize() + 1f));
          OBJ_69.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_69.setName("OBJ_69");
          panel3.add(OBJ_69);
          OBJ_69.setBounds(360, 369, 60, 20);

          //======== SCROLLPANE_LIST ========
          {
            SCROLLPANE_LIST.setComponentPopupMenu(BTD);
            SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

            //---- LA01 ----
            LA01.setRowHeight(20);
            LA01.setComponentPopupMenu(BTD);
            LA01.setName("LA01");
            LA01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LA01MouseClicked(e);
              }
            });
            SCROLLPANE_LIST.setViewportView(LA01);
          }
          panel3.add(SCROLLPANE_LIST);
          SCROLLPANE_LIST.setBounds(15, 10, 515, 350);

          //---- MTA01 ----
          MTA01.setName("MTA01");
          panel3.add(MTA01);
          MTA01.setBounds(590, 30, 100, 25);

          //---- MTA02 ----
          MTA02.setName("MTA02");
          panel3.add(MTA02);
          MTA02.setBounds(590, 50, 100, 25);

          //---- MTA03 ----
          MTA03.setName("MTA03");
          panel3.add(MTA03);
          MTA03.setBounds(590, 70, 100, 25);

          //---- MTA04 ----
          MTA04.setName("MTA04");
          panel3.add(MTA04);
          MTA04.setBounds(590, 90, 100, 25);

          //---- MTA05 ----
          MTA05.setName("MTA05");
          panel3.add(MTA05);
          MTA05.setBounds(590, 110, 100, 25);

          //---- MTA06 ----
          MTA06.setName("MTA06");
          panel3.add(MTA06);
          MTA06.setBounds(590, 130, 100, 25);

          //---- MTA07 ----
          MTA07.setName("MTA07");
          panel3.add(MTA07);
          MTA07.setBounds(590, 150, 100, 25);

          //---- MTA08 ----
          MTA08.setName("MTA08");
          panel3.add(MTA08);
          MTA08.setBounds(590, 170, 100, 25);

          //---- MTA09 ----
          MTA09.setName("MTA09");
          panel3.add(MTA09);
          MTA09.setBounds(590, 190, 100, 25);

          //---- MTA10 ----
          MTA10.setName("MTA10");
          panel3.add(MTA10);
          MTA10.setBounds(590, 210, 100, 25);

          //---- MTA16 ----
          MTA16.setName("MTA16");
          panel3.add(MTA16);
          MTA16.setBounds(590, 330, 100, 25);

          //---- MTA14 ----
          MTA14.setName("MTA14");
          panel3.add(MTA14);
          MTA14.setBounds(590, 290, 100, 25);

          //---- MTA15 ----
          MTA15.setName("MTA15");
          panel3.add(MTA15);
          MTA15.setBounds(590, 310, 100, 25);

          //---- MTA13 ----
          MTA13.setName("MTA13");
          panel3.add(MTA13);
          MTA13.setBounds(590, 270, 100, 25);

          //---- MTA12 ----
          MTA12.setName("MTA12");
          panel3.add(MTA12);
          MTA12.setBounds(590, 250, 100, 25);

          //---- MTA11 ----
          MTA11.setName("MTA11");
          panel3.add(MTA11);
          MTA11.setBounds(590, 230, 100, 25);

          //---- label2 ----
          label2.setText("@ERA01@");
          label2.setHorizontalAlignment(SwingConstants.CENTER);
          label2.setForeground(new Color(204, 0, 0));
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
          label2.setName("label2");
          panel3.add(label2);
          label2.setBounds(695, 30, 20, 25);

          //---- label3 ----
          label3.setText("@ERA02@");
          label3.setHorizontalAlignment(SwingConstants.CENTER);
          label3.setForeground(new Color(204, 0, 0));
          label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
          label3.setName("label3");
          panel3.add(label3);
          label3.setBounds(695, 50, 20, 25);

          //---- label4 ----
          label4.setText("@ERA03@");
          label4.setHorizontalAlignment(SwingConstants.CENTER);
          label4.setForeground(new Color(204, 0, 0));
          label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
          label4.setName("label4");
          panel3.add(label4);
          label4.setBounds(695, 70, 20, 25);

          //---- label5 ----
          label5.setText("@ERA04@");
          label5.setHorizontalAlignment(SwingConstants.CENTER);
          label5.setForeground(new Color(204, 0, 0));
          label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
          label5.setName("label5");
          panel3.add(label5);
          label5.setBounds(695, 90, 20, 25);

          //---- label6 ----
          label6.setText("@ERA05@");
          label6.setHorizontalAlignment(SwingConstants.CENTER);
          label6.setForeground(new Color(204, 0, 0));
          label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
          label6.setName("label6");
          panel3.add(label6);
          label6.setBounds(695, 110, 20, 25);

          //---- label7 ----
          label7.setText("@ERA06@");
          label7.setHorizontalAlignment(SwingConstants.CENTER);
          label7.setForeground(new Color(204, 0, 0));
          label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD));
          label7.setName("label7");
          panel3.add(label7);
          label7.setBounds(695, 130, 20, 25);

          //---- label8 ----
          label8.setText("@ERA07@");
          label8.setHorizontalAlignment(SwingConstants.CENTER);
          label8.setForeground(new Color(204, 0, 0));
          label8.setFont(label8.getFont().deriveFont(label8.getFont().getStyle() | Font.BOLD));
          label8.setName("label8");
          panel3.add(label8);
          label8.setBounds(695, 150, 20, 25);

          //---- label9 ----
          label9.setText("@ERA08@");
          label9.setHorizontalAlignment(SwingConstants.CENTER);
          label9.setForeground(new Color(204, 0, 0));
          label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD));
          label9.setName("label9");
          panel3.add(label9);
          label9.setBounds(695, 170, 20, 25);

          //---- label10 ----
          label10.setText("@ERA09@");
          label10.setHorizontalAlignment(SwingConstants.CENTER);
          label10.setForeground(new Color(204, 0, 0));
          label10.setFont(label10.getFont().deriveFont(label10.getFont().getStyle() | Font.BOLD));
          label10.setName("label10");
          panel3.add(label10);
          label10.setBounds(695, 190, 20, 25);

          //---- label11 ----
          label11.setText("@ERA10@");
          label11.setHorizontalAlignment(SwingConstants.CENTER);
          label11.setForeground(new Color(204, 0, 0));
          label11.setFont(label11.getFont().deriveFont(label11.getFont().getStyle() | Font.BOLD));
          label11.setName("label11");
          panel3.add(label11);
          label11.setBounds(695, 210, 20, 25);

          //---- label12 ----
          label12.setText("@ERA11@");
          label12.setHorizontalAlignment(SwingConstants.CENTER);
          label12.setForeground(new Color(204, 0, 0));
          label12.setFont(label12.getFont().deriveFont(label12.getFont().getStyle() | Font.BOLD));
          label12.setName("label12");
          panel3.add(label12);
          label12.setBounds(695, 230, 20, 25);

          //---- label13 ----
          label13.setText("@ERA12@");
          label13.setHorizontalAlignment(SwingConstants.CENTER);
          label13.setForeground(new Color(204, 0, 0));
          label13.setFont(label13.getFont().deriveFont(label13.getFont().getStyle() | Font.BOLD));
          label13.setName("label13");
          panel3.add(label13);
          label13.setBounds(695, 250, 20, 25);

          //---- label14 ----
          label14.setText("@ERA13@");
          label14.setHorizontalAlignment(SwingConstants.CENTER);
          label14.setForeground(new Color(204, 0, 0));
          label14.setFont(label14.getFont().deriveFont(label14.getFont().getStyle() | Font.BOLD));
          label14.setName("label14");
          panel3.add(label14);
          label14.setBounds(695, 270, 20, 25);

          //---- label15 ----
          label15.setText("@ERA14@");
          label15.setHorizontalAlignment(SwingConstants.CENTER);
          label15.setForeground(new Color(204, 0, 0));
          label15.setFont(label15.getFont().deriveFont(label15.getFont().getStyle() | Font.BOLD));
          label15.setName("label15");
          panel3.add(label15);
          label15.setBounds(695, 290, 20, 25);

          //---- label16 ----
          label16.setText("@ERA15@");
          label16.setHorizontalAlignment(SwingConstants.CENTER);
          label16.setForeground(new Color(204, 0, 0));
          label16.setFont(label16.getFont().deriveFont(label16.getFont().getStyle() | Font.BOLD));
          label16.setName("label16");
          panel3.add(label16);
          label16.setBounds(695, 310, 20, 25);

          //---- label17 ----
          label17.setText("@ERA16@");
          label17.setHorizontalAlignment(SwingConstants.CENTER);
          label17.setForeground(new Color(204, 0, 0));
          label17.setFont(label17.getFont().deriveFont(label17.getFont().getStyle() | Font.BOLD));
          label17.setName("label17");
          panel3.add(label17);
          label17.setBounds(695, 330, 20, 25);

          //---- label1 ----
          label1.setText("Montant affect\u00e9");
          label1.setHorizontalAlignment(SwingConstants.CENTER);
          label1.setName("label1");
          panel3.add(label1);
          label1.setBounds(590, 10, 100, 20);

          //---- label18 ----
          label18.setText("E");
          label18.setName("label18");
          panel3.add(label18);
          label18.setBounds(700, 10, 13, 20);

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_53 ----
            OBJ_53.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_53.setFont(OBJ_53.getFont().deriveFont(OBJ_53.getFont().getStyle() | Font.BOLD));
            OBJ_53.setText(">");
            OBJ_53.setName("OBJ_53");
            OBJ_53.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_53ActionPerformed(e);
              }
            });
            panel2.add(OBJ_53);
            OBJ_53.setBounds(5, 10, 54, 25);

            //---- OBJ_54 ----
            OBJ_54.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_54.setFont(OBJ_54.getFont().deriveFont(OBJ_54.getFont().getStyle() | Font.BOLD));
            OBJ_54.setText(">");
            OBJ_54.setName("OBJ_54");
            OBJ_54.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_54ActionPerformed(e);
              }
            });
            panel2.add(OBJ_54);
            OBJ_54.setBounds(5, 30, 54, 25);

            //---- OBJ_55 ----
            OBJ_55.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_55.setFont(OBJ_55.getFont().deriveFont(OBJ_55.getFont().getStyle() | Font.BOLD));
            OBJ_55.setText(">");
            OBJ_55.setName("OBJ_55");
            OBJ_55.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_55ActionPerformed(e);
              }
            });
            panel2.add(OBJ_55);
            OBJ_55.setBounds(5, 50, 54, 25);

            //---- OBJ_56 ----
            OBJ_56.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_56.setFont(OBJ_56.getFont().deriveFont(OBJ_56.getFont().getStyle() | Font.BOLD));
            OBJ_56.setText(">");
            OBJ_56.setName("OBJ_56");
            OBJ_56.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_56ActionPerformed(e);
              }
            });
            panel2.add(OBJ_56);
            OBJ_56.setBounds(5, 70, 54, 25);

            //---- OBJ_57 ----
            OBJ_57.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_57.setFont(OBJ_57.getFont().deriveFont(OBJ_57.getFont().getStyle() | Font.BOLD));
            OBJ_57.setText(">");
            OBJ_57.setName("OBJ_57");
            OBJ_57.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_57ActionPerformed(e);
              }
            });
            panel2.add(OBJ_57);
            OBJ_57.setBounds(5, 90, 54, 25);

            //---- OBJ_58 ----
            OBJ_58.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_58.setFont(OBJ_58.getFont().deriveFont(OBJ_58.getFont().getStyle() | Font.BOLD));
            OBJ_58.setText(">");
            OBJ_58.setName("OBJ_58");
            OBJ_58.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_58ActionPerformed(e);
              }
            });
            panel2.add(OBJ_58);
            OBJ_58.setBounds(5, 110, 54, 25);

            //---- OBJ_59 ----
            OBJ_59.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_59.setFont(OBJ_59.getFont().deriveFont(OBJ_59.getFont().getStyle() | Font.BOLD));
            OBJ_59.setText(">");
            OBJ_59.setName("OBJ_59");
            OBJ_59.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_59ActionPerformed(e);
              }
            });
            panel2.add(OBJ_59);
            OBJ_59.setBounds(5, 130, 54, 25);

            //---- OBJ_60 ----
            OBJ_60.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_60.setFont(OBJ_60.getFont().deriveFont(OBJ_60.getFont().getStyle() | Font.BOLD));
            OBJ_60.setText(">");
            OBJ_60.setName("OBJ_60");
            OBJ_60.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_60ActionPerformed(e);
              }
            });
            panel2.add(OBJ_60);
            OBJ_60.setBounds(5, 150, 54, 25);

            //---- OBJ_61 ----
            OBJ_61.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_61.setFont(OBJ_61.getFont().deriveFont(OBJ_61.getFont().getStyle() | Font.BOLD));
            OBJ_61.setText(">");
            OBJ_61.setName("OBJ_61");
            OBJ_61.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_61ActionPerformed(e);
              }
            });
            panel2.add(OBJ_61);
            OBJ_61.setBounds(5, 170, 54, 25);

            //---- OBJ_62 ----
            OBJ_62.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_62.setFont(OBJ_62.getFont().deriveFont(OBJ_62.getFont().getStyle() | Font.BOLD));
            OBJ_62.setText(">");
            OBJ_62.setName("OBJ_62");
            OBJ_62.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_62ActionPerformed(e);
              }
            });
            panel2.add(OBJ_62);
            OBJ_62.setBounds(5, 190, 54, 25);

            //---- OBJ_63 ----
            OBJ_63.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_63.setFont(OBJ_63.getFont().deriveFont(OBJ_63.getFont().getStyle() | Font.BOLD));
            OBJ_63.setText(">");
            OBJ_63.setName("OBJ_63");
            OBJ_63.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_63ActionPerformed(e);
              }
            });
            panel2.add(OBJ_63);
            OBJ_63.setBounds(5, 210, 54, 25);

            //---- OBJ_64 ----
            OBJ_64.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_64.setFont(OBJ_64.getFont().deriveFont(OBJ_64.getFont().getStyle() | Font.BOLD));
            OBJ_64.setText(">");
            OBJ_64.setName("OBJ_64");
            OBJ_64.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_64ActionPerformed(e);
              }
            });
            panel2.add(OBJ_64);
            OBJ_64.setBounds(5, 230, 54, 25);

            //---- OBJ_65 ----
            OBJ_65.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_65.setFont(OBJ_65.getFont().deriveFont(OBJ_65.getFont().getStyle() | Font.BOLD));
            OBJ_65.setText(">");
            OBJ_65.setName("OBJ_65");
            OBJ_65.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_65ActionPerformed(e);
              }
            });
            panel2.add(OBJ_65);
            OBJ_65.setBounds(5, 250, 54, 25);

            //---- OBJ_66 ----
            OBJ_66.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_66.setFont(OBJ_66.getFont().deriveFont(OBJ_66.getFont().getStyle() | Font.BOLD));
            OBJ_66.setText(">");
            OBJ_66.setName("OBJ_66");
            OBJ_66.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_66ActionPerformed(e);
              }
            });
            panel2.add(OBJ_66);
            OBJ_66.setBounds(5, 270, 54, 25);

            //---- OBJ_67 ----
            OBJ_67.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_67.setFont(OBJ_67.getFont().deriveFont(OBJ_67.getFont().getStyle() | Font.BOLD));
            OBJ_67.setText(">");
            OBJ_67.setName("OBJ_67");
            OBJ_67.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_67ActionPerformed(e);
              }
            });
            panel2.add(OBJ_67);
            OBJ_67.setBounds(5, 290, 54, 25);

            //---- OBJ_68 ----
            OBJ_68.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_68.setFont(OBJ_68.getFont().deriveFont(OBJ_68.getFont().getStyle() | Font.BOLD));
            OBJ_68.setText(">");
            OBJ_68.setName("OBJ_68");
            OBJ_68.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_68ActionPerformed(e);
              }
            });
            panel2.add(OBJ_68);
            OBJ_68.setBounds(5, 310, 54, 25);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          panel3.add(panel2);
          panel2.setBounds(530, 20, 60, 345);

          //---- label19 ----
          label19.setText("Affect.");
          label19.setHorizontalAlignment(SwingConstants.CENTER);
          label19.setName("label19");
          panel3.add(label19);
          label19.setBounds(535, 10, 50, 20);

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          panel3.add(BT_PGUP);
          BT_PGUP.setBounds(730, 30, 25, 160);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          panel3.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(730, 195, 25, 160);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel3.getComponentCount(); i++) {
              Rectangle bounds = panel3.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel3.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel3.setMinimumSize(preferredSize);
            panel3.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel3);
        panel3.setBounds(10, 10, 770, 405);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_14 ----
      OBJ_14.setText("Duplication");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);

      //---- OBJ_15 ----
      OBJ_15.setText("D\u00e9coupage \u00e9criture");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);
      BTD.addSeparator();

      //---- OBJ_17 ----
      OBJ_17.setText("Choix possibles");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_16 ----
      OBJ_16.setText("Aide en ligne");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private JPanel p_contenu;
  private JPanel panel3;
  private XRiTextField WDIFE;
  private XRiTextField TOTAFF;
  private JLabel OBJ_69;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable LA01;
  private XRiTextField MTA01;
  private XRiTextField MTA02;
  private XRiTextField MTA03;
  private XRiTextField MTA04;
  private XRiTextField MTA05;
  private XRiTextField MTA06;
  private XRiTextField MTA07;
  private XRiTextField MTA08;
  private XRiTextField MTA09;
  private XRiTextField MTA10;
  private XRiTextField MTA16;
  private XRiTextField MTA14;
  private XRiTextField MTA15;
  private XRiTextField MTA13;
  private XRiTextField MTA12;
  private XRiTextField MTA11;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private JLabel label10;
  private JLabel label11;
  private JLabel label12;
  private JLabel label13;
  private JLabel label14;
  private JLabel label15;
  private JLabel label16;
  private JLabel label17;
  private JLabel label1;
  private JLabel label18;
  private JPanel panel2;
  private JButton OBJ_53;
  private JButton OBJ_54;
  private JButton OBJ_55;
  private JButton OBJ_56;
  private JButton OBJ_57;
  private JButton OBJ_58;
  private JButton OBJ_59;
  private JButton OBJ_60;
  private JButton OBJ_61;
  private JButton OBJ_62;
  private JButton OBJ_63;
  private JButton OBJ_64;
  private JButton OBJ_65;
  private JButton OBJ_66;
  private JButton OBJ_67;
  private JButton OBJ_68;
  private JLabel label19;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu BTD;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_15;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_16;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
