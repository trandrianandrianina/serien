
package ri.serien.libecranrpg.vcgm.VCGM01FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM01FM_EX extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM01FM_EX(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_41_OBJ_41.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LJOEX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    OBJ_41_OBJ_41.setVisible(lexique.isPresent("LJOEX"));
    
    // TODO Icones
    label1.setIcon(lexique.chargerImage("images/accolade.png", true));
    
    

    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    // riMenu_bt2.setIcon(lexique.getImage("images/options.png",true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44_OBJ_45 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_45_OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46_OBJ_47 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_41_OBJ_41 = new JLabel();
    LCEX1 = new XRiTextField();
    LCEX2 = new XRiTextField();
    LCEX3 = new XRiTextField();
    LCEX4 = new XRiTextField();
    LCEX5 = new XRiTextField();
    LCEX6 = new XRiTextField();
    LCEX7 = new XRiTextField();
    LCEX8 = new XRiTextField();
    LCEX9 = new XRiTextField();
    LCEX10 = new XRiTextField();
    LCEX11 = new XRiTextField();
    LCEX12 = new XRiTextField();
    LCEX13 = new XRiTextField();
    LCEX14 = new XRiTextField();
    LCEX15 = new XRiTextField();
    LJO = new XRiTextField();
    OBJ_106_OBJ_106 = new JLabel();
    OBJ_127_OBJ_127 = new JLabel();
    OBJ_42_OBJ_42 = new JLabel();
    OBJ_109_OBJ_109 = new JLabel();
    EXC1 = new XRiTextField();
    EXC2 = new XRiTextField();
    EXC3 = new XRiTextField();
    EXC4 = new XRiTextField();
    EXC5 = new XRiTextField();
    EXC6 = new XRiTextField();
    EXC7 = new XRiTextField();
    EXC8 = new XRiTextField();
    EXC9 = new XRiTextField();
    EXC10 = new XRiTextField();
    EXC11 = new XRiTextField();
    EXC12 = new XRiTextField();
    EXC13 = new XRiTextField();
    EXC14 = new XRiTextField();
    EXC15 = new XRiTextField();
    EXF1 = new XRiTextField();
    EXF2 = new XRiTextField();
    EXF3 = new XRiTextField();
    EXF4 = new XRiTextField();
    EXF5 = new XRiTextField();
    EXF6 = new XRiTextField();
    EXF7 = new XRiTextField();
    EXF8 = new XRiTextField();
    EXF9 = new XRiTextField();
    EXF10 = new XRiTextField();
    EXF11 = new XRiTextField();
    EXF12 = new XRiTextField();
    EXF13 = new XRiTextField();
    EXF14 = new XRiTextField();
    EXF15 = new XRiTextField();
    OBJ_108_OBJ_108 = new JLabel();
    OBJ_110_OBJ_110 = new JLabel();
    EXPERX = new XRiTextField();
    OBJ_111_OBJ_111 = new JLabel();
    OBJ_112_OBJ_112 = new JLabel();
    EXJO = new XRiTextField();
    EXJ1 = new XRiTextField();
    EXJ2 = new XRiTextField();
    EXJ3 = new XRiTextField();
    EXJ4 = new XRiTextField();
    EXJ5 = new XRiTextField();
    EXJ6 = new XRiTextField();
    EXJ7 = new XRiTextField();
    EXJ8 = new XRiTextField();
    EXJ9 = new XRiTextField();
    EXJ10 = new XRiTextField();
    EXJ11 = new XRiTextField();
    EXJ12 = new XRiTextField();
    EXJ13 = new XRiTextField();
    EXJ14 = new XRiTextField();
    EXJ15 = new XRiTextField();
    SJO = new XRiTextField();
    EXS1 = new XRiTextField();
    EXS2 = new XRiTextField();
    EXS3 = new XRiTextField();
    EXS4 = new XRiTextField();
    EXS5 = new XRiTextField();
    EXS6 = new XRiTextField();
    EXS7 = new XRiTextField();
    EXS8 = new XRiTextField();
    EXS9 = new XRiTextField();
    EXS10 = new XRiTextField();
    EXS11 = new XRiTextField();
    EXS12 = new XRiTextField();
    EXS13 = new XRiTextField();
    EXS14 = new XRiTextField();
    EXS15 = new XRiTextField();
    label1 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44_OBJ_45 ----
          OBJ_44_OBJ_45.setText("Soci\u00e9t\u00e9");
          OBJ_44_OBJ_45.setName("OBJ_44_OBJ_45");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setName("INDETB");

          //---- OBJ_45_OBJ_46 ----
          OBJ_45_OBJ_46.setText("Code");
          OBJ_45_OBJ_46.setName("OBJ_45_OBJ_46");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setName("INDTYP");

          //---- OBJ_46_OBJ_47 ----
          OBJ_46_OBJ_47.setText("Ordre");
          OBJ_46_OBJ_47.setName("OBJ_46_OBJ_47");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(800, 550));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Journal d'extourne");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setBackground(new Color(239, 239, 222));
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

            //---- OBJ_41_OBJ_41 ----
            OBJ_41_OBJ_41.setText("@LJOEX@");
            OBJ_41_OBJ_41.setName("OBJ_41_OBJ_41");

            //---- LCEX1 ----
            LCEX1.setName("LCEX1");

            //---- LCEX2 ----
            LCEX2.setName("LCEX2");

            //---- LCEX3 ----
            LCEX3.setName("LCEX3");

            //---- LCEX4 ----
            LCEX4.setName("LCEX4");

            //---- LCEX5 ----
            LCEX5.setName("LCEX5");

            //---- LCEX6 ----
            LCEX6.setName("LCEX6");

            //---- LCEX7 ----
            LCEX7.setName("LCEX7");

            //---- LCEX8 ----
            LCEX8.setName("LCEX8");

            //---- LCEX9 ----
            LCEX9.setName("LCEX9");

            //---- LCEX10 ----
            LCEX10.setName("LCEX10");

            //---- LCEX11 ----
            LCEX11.setName("LCEX11");

            //---- LCEX12 ----
            LCEX12.setName("LCEX12");

            //---- LCEX13 ----
            LCEX13.setName("LCEX13");

            //---- LCEX14 ----
            LCEX14.setName("LCEX14");

            //---- LCEX15 ----
            LCEX15.setName("LCEX15");

            //---- LJO ----
            LJO.setName("LJO");

            //---- OBJ_106_OBJ_106 ----
            OBJ_106_OBJ_106.setText("S\u00e9lection d'\u00e9critures");
            OBJ_106_OBJ_106.setForeground(Color.black);
            OBJ_106_OBJ_106.setName("OBJ_106_OBJ_106");

            //---- OBJ_127_OBJ_127 ----
            OBJ_127_OBJ_127.setText("ET/OU S\u00e9lection JO");
            OBJ_127_OBJ_127.setName("OBJ_127_OBJ_127");

            //---- OBJ_42_OBJ_42 ----
            OBJ_42_OBJ_42.setText("P\u00e9riode \u00e0 extourner");
            OBJ_42_OBJ_42.setName("OBJ_42_OBJ_42");

            //---- OBJ_109_OBJ_109 ----
            OBJ_109_OBJ_109.setText("Libell\u00e9 du compte");
            OBJ_109_OBJ_109.setName("OBJ_109_OBJ_109");

            //---- EXC1 ----
            EXC1.setName("EXC1");

            //---- EXC2 ----
            EXC2.setName("EXC2");

            //---- EXC3 ----
            EXC3.setName("EXC3");

            //---- EXC4 ----
            EXC4.setName("EXC4");

            //---- EXC5 ----
            EXC5.setName("EXC5");

            //---- EXC6 ----
            EXC6.setName("EXC6");

            //---- EXC7 ----
            EXC7.setName("EXC7");

            //---- EXC8 ----
            EXC8.setName("EXC8");

            //---- EXC9 ----
            EXC9.setName("EXC9");

            //---- EXC10 ----
            EXC10.setName("EXC10");

            //---- EXC11 ----
            EXC11.setName("EXC11");

            //---- EXC12 ----
            EXC12.setName("EXC12");

            //---- EXC13 ----
            EXC13.setName("EXC13");

            //---- EXC14 ----
            EXC14.setName("EXC14");

            //---- EXC15 ----
            EXC15.setName("EXC15");

            //---- EXF1 ----
            EXF1.setName("EXF1");

            //---- EXF2 ----
            EXF2.setName("EXF2");

            //---- EXF3 ----
            EXF3.setName("EXF3");

            //---- EXF4 ----
            EXF4.setName("EXF4");

            //---- EXF5 ----
            EXF5.setName("EXF5");

            //---- EXF6 ----
            EXF6.setName("EXF6");

            //---- EXF7 ----
            EXF7.setName("EXF7");

            //---- EXF8 ----
            EXF8.setName("EXF8");

            //---- EXF9 ----
            EXF9.setName("EXF9");

            //---- EXF10 ----
            EXF10.setName("EXF10");

            //---- EXF11 ----
            EXF11.setName("EXF11");

            //---- EXF12 ----
            EXF12.setName("EXF12");

            //---- EXF13 ----
            EXF13.setName("EXF13");

            //---- EXF14 ----
            EXF14.setName("EXF14");

            //---- EXF15 ----
            EXF15.setName("EXF15");

            //---- OBJ_108_OBJ_108 ----
            OBJ_108_OBJ_108.setText("N\u00b0 Cpte");
            OBJ_108_OBJ_108.setName("OBJ_108_OBJ_108");

            //---- OBJ_110_OBJ_110 ----
            OBJ_110_OBJ_110.setText("Journal");
            OBJ_110_OBJ_110.setName("OBJ_110_OBJ_110");

            //---- EXPERX ----
            EXPERX.setName("EXPERX");

            //---- OBJ_111_OBJ_111 ----
            OBJ_111_OBJ_111.setText("Folio");
            OBJ_111_OBJ_111.setName("OBJ_111_OBJ_111");

            //---- OBJ_112_OBJ_112 ----
            OBJ_112_OBJ_112.setText("Sens");
            OBJ_112_OBJ_112.setName("OBJ_112_OBJ_112");

            //---- EXJO ----
            EXJO.setName("EXJO");

            //---- EXJ1 ----
            EXJ1.setName("EXJ1");

            //---- EXJ2 ----
            EXJ2.setName("EXJ2");

            //---- EXJ3 ----
            EXJ3.setName("EXJ3");

            //---- EXJ4 ----
            EXJ4.setName("EXJ4");

            //---- EXJ5 ----
            EXJ5.setName("EXJ5");

            //---- EXJ6 ----
            EXJ6.setName("EXJ6");

            //---- EXJ7 ----
            EXJ7.setName("EXJ7");

            //---- EXJ8 ----
            EXJ8.setName("EXJ8");

            //---- EXJ9 ----
            EXJ9.setName("EXJ9");

            //---- EXJ10 ----
            EXJ10.setName("EXJ10");

            //---- EXJ11 ----
            EXJ11.setName("EXJ11");

            //---- EXJ12 ----
            EXJ12.setName("EXJ12");

            //---- EXJ13 ----
            EXJ13.setName("EXJ13");

            //---- EXJ14 ----
            EXJ14.setName("EXJ14");

            //---- EXJ15 ----
            EXJ15.setName("EXJ15");

            //---- SJO ----
            SJO.setName("SJO");

            //---- EXS1 ----
            EXS1.setName("EXS1");

            //---- EXS2 ----
            EXS2.setName("EXS2");

            //---- EXS3 ----
            EXS3.setName("EXS3");

            //---- EXS4 ----
            EXS4.setName("EXS4");

            //---- EXS5 ----
            EXS5.setName("EXS5");

            //---- EXS6 ----
            EXS6.setName("EXS6");

            //---- EXS7 ----
            EXS7.setName("EXS7");

            //---- EXS8 ----
            EXS8.setName("EXS8");

            //---- EXS9 ----
            EXS9.setName("EXS9");

            //---- EXS10 ----
            EXS10.setName("EXS10");

            //---- EXS11 ----
            EXS11.setName("EXS11");

            //---- EXS12 ----
            EXS12.setName("EXS12");

            //---- EXS13 ----
            EXS13.setName("EXS13");

            //---- EXS14 ----
            EXS14.setName("EXS14");

            //---- EXS15 ----
            EXS15.setName("EXS15");

            //---- label1 ----
            label1.setName("label1");

            GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
            xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
            xTitledPanel1ContentContainerLayout.setHorizontalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(EXJO, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_41_OBJ_41, GroupLayout.PREFERRED_SIZE, 257, GroupLayout.PREFERRED_SIZE)
                      .addGap(158, 158, 158)
                      .addComponent(OBJ_42_OBJ_42, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(EXPERX, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(235, 235, 235)
                      .addComponent(OBJ_108_OBJ_108, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                      .addGap(22, 22, 22)
                      .addComponent(OBJ_109_OBJ_109, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE)
                      .addGap(133, 133, 133)
                      .addComponent(OBJ_110_OBJ_110, GroupLayout.PREFERRED_SIZE, 47, GroupLayout.PREFERRED_SIZE)
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_111_OBJ_111, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                      .addGap(22, 22, 22)
                      .addComponent(OBJ_112_OBJ_112, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(10, 10, 10)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_106_OBJ_106, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_127_OBJ_127, GroupLayout.PREFERRED_SIZE, 134, GroupLayout.PREFERRED_SIZE))
                      .addGap(10, 10, 10)
                      .addComponent(label1, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(EXC1, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXC2, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXC6, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXC7, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXC8, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXC4, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXC5, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXC11, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXC12, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(SJO, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXC15, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXC13, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXC10, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXC14, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXC9, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXC3, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                      .addGap(10, 10, 10)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(LCEX2, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LCEX1, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LCEX3, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LCEX6, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LCEX5, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LCEX8, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LCEX13, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LCEX9, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LCEX10, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LCEX12, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LCEX14, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LCEX11, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LJO, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LCEX15, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LCEX7, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LCEX4, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE))
                      .addGap(4, 4, 4)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(EXJ5, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXJ4, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXJ7, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXJ6, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXJ2, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXJ3, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXJ1, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXJ13, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXJ11, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXJ15, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXJ9, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXJ10, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXJ14, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXJ12, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXJ8, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                      .addGap(10, 10, 10)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(EXF2, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXF1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXF10, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXF7, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXF14, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXF11, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXF12, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXF13, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXF4, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXF5, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXF8, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXF9, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXF15, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXF6, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXF3, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                      .addGap(5, 5, 5)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(EXS7, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXS2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXS1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXS12, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXS9, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXS11, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXS8, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXS3, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXS4, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXS5, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXS6, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXS10, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXS15, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXS14, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EXS13, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
                  .addContainerGap(74, Short.MAX_VALUE))
            );
            xTitledPanel1ContentContainerLayout.setVerticalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(6, 6, 6)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(EXJO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(EXPERX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_41_OBJ_41, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_42_OBJ_42, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_108_OBJ_108, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_109_OBJ_109, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_110_OBJ_110, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_111_OBJ_111, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_112_OBJ_112, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE))
                  .addGap(3, 3, 3)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(180, 180, 180)
                      .addComponent(OBJ_106_OBJ_106, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                      .addGap(190, 190, 190)
                      .addComponent(OBJ_127_OBJ_127, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(label1, GroupLayout.PREFERRED_SIZE, 390, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(EXC1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(26, 26, 26)
                          .addComponent(EXC2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(24, 24, 24)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(52, 52, 52)
                          .addComponent(EXC6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(78, 78, 78)
                          .addComponent(EXC7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(104, 104, 104)
                          .addComponent(EXC8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(EXC4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(26, 26, 26)
                          .addComponent(EXC5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(182, 182, 182)
                          .addComponent(EXC11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(208, 208, 208)
                          .addComponent(EXC12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(312, 312, 312)
                          .addComponent(SJO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(286, 286, 286)
                          .addComponent(EXC15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(234, 234, 234)
                          .addComponent(EXC13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(156, 156, 156)
                          .addComponent(EXC10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(260, 260, 260)
                          .addComponent(EXC14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(130, 130, 130)
                          .addComponent(EXC9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(52, 52, 52)
                      .addComponent(EXC3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(26, 26, 26)
                          .addComponent(LCEX2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(LCEX1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(52, 52, 52)
                          .addComponent(LCEX3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(24, 24, 24)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(26, 26, 26)
                          .addComponent(LCEX6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(LCEX5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(78, 78, 78)
                          .addComponent(LCEX8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(208, 208, 208)
                          .addComponent(LCEX13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(104, 104, 104)
                          .addComponent(LCEX9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(130, 130, 130)
                          .addComponent(LCEX10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(182, 182, 182)
                          .addComponent(LCEX12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(234, 234, 234)
                          .addComponent(LCEX14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(156, 156, 156)
                          .addComponent(LCEX11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(286, 286, 286)
                          .addComponent(LJO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(260, 260, 260)
                          .addComponent(LCEX15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(52, 52, 52)
                          .addComponent(LCEX7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(78, 78, 78)
                      .addComponent(LCEX4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(104, 104, 104)
                          .addComponent(EXJ5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(78, 78, 78)
                          .addComponent(EXJ4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(156, 156, 156)
                          .addComponent(EXJ7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(130, 130, 130)
                          .addComponent(EXJ6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(26, 26, 26)
                          .addComponent(EXJ2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(52, 52, 52)
                          .addComponent(EXJ3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(EXJ1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGap(24, 24, 24)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(104, 104, 104)
                          .addComponent(EXJ13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(52, 52, 52)
                          .addComponent(EXJ11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(156, 156, 156)
                          .addComponent(EXJ15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(EXJ9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(26, 26, 26)
                          .addComponent(EXJ10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(130, 130, 130)
                          .addComponent(EXJ14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(78, 78, 78)
                          .addComponent(EXJ12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(182, 182, 182)
                      .addComponent(EXJ8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(26, 26, 26)
                          .addComponent(EXF2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(EXF1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGap(24, 24, 24)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(156, 156, 156)
                          .addComponent(EXF10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(78, 78, 78)
                          .addComponent(EXF7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(260, 260, 260)
                          .addComponent(EXF14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(182, 182, 182)
                          .addComponent(EXF11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(208, 208, 208)
                          .addComponent(EXF12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(234, 234, 234)
                          .addComponent(EXF13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(EXF4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(26, 26, 26)
                          .addComponent(EXF5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(104, 104, 104)
                          .addComponent(EXF8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(130, 130, 130)
                          .addComponent(EXF9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(286, 286, 286)
                          .addComponent(EXF15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(52, 52, 52)
                          .addComponent(EXF6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(52, 52, 52)
                      .addComponent(EXF3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(156, 156, 156)
                          .addComponent(EXS7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(26, 26, 26)
                          .addComponent(EXS2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(EXS1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(286, 286, 286)
                          .addComponent(EXS12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(208, 208, 208)
                          .addComponent(EXS9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(260, 260, 260)
                          .addComponent(EXS11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(182, 182, 182)
                          .addComponent(EXS8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(52, 52, 52)
                          .addComponent(EXS3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(78, 78, 78)
                          .addComponent(EXS4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(104, 104, 104)
                          .addComponent(EXS5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(130, 130, 130)
                          .addComponent(EXS6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(234, 234, 234)
                          .addComponent(EXS10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(24, 24, 24)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(26, 26, 26)
                          .addComponent(EXS15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(EXS14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(312, 312, 312)
                      .addComponent(EXS13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addContainerGap(20, Short.MAX_VALUE))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44_OBJ_45;
  private XRiTextField INDETB;
  private JLabel OBJ_45_OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_46_OBJ_47;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_41_OBJ_41;
  private XRiTextField LCEX1;
  private XRiTextField LCEX2;
  private XRiTextField LCEX3;
  private XRiTextField LCEX4;
  private XRiTextField LCEX5;
  private XRiTextField LCEX6;
  private XRiTextField LCEX7;
  private XRiTextField LCEX8;
  private XRiTextField LCEX9;
  private XRiTextField LCEX10;
  private XRiTextField LCEX11;
  private XRiTextField LCEX12;
  private XRiTextField LCEX13;
  private XRiTextField LCEX14;
  private XRiTextField LCEX15;
  private XRiTextField LJO;
  private JLabel OBJ_106_OBJ_106;
  private JLabel OBJ_127_OBJ_127;
  private JLabel OBJ_42_OBJ_42;
  private JLabel OBJ_109_OBJ_109;
  private XRiTextField EXC1;
  private XRiTextField EXC2;
  private XRiTextField EXC3;
  private XRiTextField EXC4;
  private XRiTextField EXC5;
  private XRiTextField EXC6;
  private XRiTextField EXC7;
  private XRiTextField EXC8;
  private XRiTextField EXC9;
  private XRiTextField EXC10;
  private XRiTextField EXC11;
  private XRiTextField EXC12;
  private XRiTextField EXC13;
  private XRiTextField EXC14;
  private XRiTextField EXC15;
  private XRiTextField EXF1;
  private XRiTextField EXF2;
  private XRiTextField EXF3;
  private XRiTextField EXF4;
  private XRiTextField EXF5;
  private XRiTextField EXF6;
  private XRiTextField EXF7;
  private XRiTextField EXF8;
  private XRiTextField EXF9;
  private XRiTextField EXF10;
  private XRiTextField EXF11;
  private XRiTextField EXF12;
  private XRiTextField EXF13;
  private XRiTextField EXF14;
  private XRiTextField EXF15;
  private JLabel OBJ_108_OBJ_108;
  private JLabel OBJ_110_OBJ_110;
  private XRiTextField EXPERX;
  private JLabel OBJ_111_OBJ_111;
  private JLabel OBJ_112_OBJ_112;
  private XRiTextField EXJO;
  private XRiTextField EXJ1;
  private XRiTextField EXJ2;
  private XRiTextField EXJ3;
  private XRiTextField EXJ4;
  private XRiTextField EXJ5;
  private XRiTextField EXJ6;
  private XRiTextField EXJ7;
  private XRiTextField EXJ8;
  private XRiTextField EXJ9;
  private XRiTextField EXJ10;
  private XRiTextField EXJ11;
  private XRiTextField EXJ12;
  private XRiTextField EXJ13;
  private XRiTextField EXJ14;
  private XRiTextField EXJ15;
  private XRiTextField SJO;
  private XRiTextField EXS1;
  private XRiTextField EXS2;
  private XRiTextField EXS3;
  private XRiTextField EXS4;
  private XRiTextField EXS5;
  private XRiTextField EXS6;
  private XRiTextField EXS7;
  private XRiTextField EXS8;
  private XRiTextField EXS9;
  private XRiTextField EXS10;
  private XRiTextField EXS11;
  private XRiTextField EXS12;
  private XRiTextField EXS13;
  private XRiTextField EXS14;
  private XRiTextField EXS15;
  private JLabel label1;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
