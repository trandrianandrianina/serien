
package ri.serien.libecranrpg.vcgm.VCGM06FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGM06FM_PL extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] PLTAS_Value = { "", "D", "N", "R", };
  
  public VCGM06FM_PL(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    PLTAS.setValeurs(PLTAS_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ELIBBQ@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Plafond d'encours"));
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_5 = new JLabel();
    PLENC = new XRiTextField();
    OBJ_7 = new JLabel();
    PLAUT = new XRiTextField();
    PLASC = new XRiTextField();
    OBJ_8 = new JLabel();
    OBJ_9 = new JLabel();
    PLTAS = new XRiComboBox();
    panel2 = new JPanel();
    OBJ_6 = new JLabel();
    EFAC60 = new XRiTextField();
    OBJ_10 = new JLabel();
    EFAC30 = new XRiTextField();
    EFAC00 = new XRiTextField();
    OBJ_11 = new JLabel();
    EFACNE = new XRiTextField();
    OBJ_12 = new JLabel();
    label1 = new JLabel();
    panel3 = new JPanel();
    OBJ_15 = new JLabel();
    ESOLD = new XRiTextField();
    panel4 = new JPanel();
    OBJ_13 = new JLabel();
    EBLNF = new XRiTextField();
    OBJ_14 = new JLabel();
    ECDINS = new XRiTextField();
    OBJ_16 = new JLabel();
    EIMP = new XRiTextField();
    OBJ_17 = new JLabel();
    ERGIN = new XRiTextField();
    panel5 = new JPanel();
    OBJ_18 = new JLabel();
    ERGLB = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(825, 370));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Plafond"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_5 ----
          OBJ_5.setText("Plafond d'encours");
          OBJ_5.setName("OBJ_5");
          panel1.add(OBJ_5);
          OBJ_5.setBounds(20, 40, 140, 28);

          //---- PLENC ----
          PLENC.setName("PLENC");
          panel1.add(PLENC);
          PLENC.setBounds(170, 40, 130, PLENC.getPreferredSize().height);

          //---- OBJ_7 ----
          OBJ_7.setText("Montant autoris\u00e9");
          OBJ_7.setName("OBJ_7");
          panel1.add(OBJ_7);
          OBJ_7.setBounds(20, 75, 140, 28);

          //---- PLAUT ----
          PLAUT.setName("PLAUT");
          panel1.add(PLAUT);
          PLAUT.setBounds(170, 75, 130, PLAUT.getPreferredSize().height);

          //---- PLASC ----
          PLASC.setName("PLASC");
          panel1.add(PLASC);
          PLASC.setBounds(170, 110, 130, PLASC.getPreferredSize().height);

          //---- OBJ_8 ----
          OBJ_8.setText("Assurance cr\u00e9dit");
          OBJ_8.setName("OBJ_8");
          panel1.add(OBJ_8);
          OBJ_8.setBounds(20, 110, 140, 28);

          //---- OBJ_9 ----
          OBJ_9.setText("Type d'assurance cr\u00e9dit");
          OBJ_9.setName("OBJ_9");
          panel1.add(OBJ_9);
          OBJ_9.setBounds(20, 143, 140, 28);

          //---- PLTAS ----
          PLTAS.setComponentPopupMenu(null);
          PLTAS.setModel(new DefaultComboBoxModel(new String[] {
            "  ",
            "D\u00e9nomm\u00e9",
            "Non d\u00e9nomm\u00e9",
            "Refus\u00e9"
          }));
          PLTAS.setEditable(true);
          PLTAS.setName("PLTAS");
          panel1.add(PLTAS);
          PLTAS.setBounds(170, 145, 130, 24);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(330, 110, 315, 185);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Factures dues"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- OBJ_6 ----
          OBJ_6.setText("Plus de 60 jours");
          OBJ_6.setName("OBJ_6");
          panel2.add(OBJ_6);
          OBJ_6.setBounds(20, 30, 150, 28);

          //---- EFAC60 ----
          EFAC60.setName("EFAC60");
          panel2.add(EFAC60);
          EFAC60.setBounds(170, 30, 130, EFAC60.getPreferredSize().height);

          //---- OBJ_10 ----
          OBJ_10.setText("De 30 \u00e0 60 jours");
          OBJ_10.setName("OBJ_10");
          panel2.add(OBJ_10);
          OBJ_10.setBounds(20, 60, 150, 28);

          //---- EFAC30 ----
          EFAC30.setName("EFAC30");
          panel2.add(EFAC30);
          EFAC30.setBounds(170, 60, 130, EFAC30.getPreferredSize().height);

          //---- EFAC00 ----
          EFAC00.setName("EFAC00");
          panel2.add(EFAC00);
          EFAC00.setBounds(170, 90, 130, EFAC00.getPreferredSize().height);

          //---- OBJ_11 ----
          OBJ_11.setText("De 0 \u00e0 30 jours");
          OBJ_11.setName("OBJ_11");
          panel2.add(OBJ_11);
          OBJ_11.setBounds(20, 90, 150, 28);

          //---- EFACNE ----
          EFACNE.setName("EFACNE");
          panel2.add(EFACNE);
          EFACNE.setBounds(170, 120, 130, EFACNE.getPreferredSize().height);

          //---- OBJ_12 ----
          OBJ_12.setText("Non \u00e9chues");
          OBJ_12.setName("OBJ_12");
          panel2.add(OBJ_12);
          OBJ_12.setBounds(20, 120, 150, 28);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel2);
        panel2.setBounds(10, 35, 315, 160);

        //---- label1 ----
        label1.setText("@ELIBBQ@");
        label1.setHorizontalAlignment(SwingConstants.CENTER);
        label1.setForeground(new Color(255, 0, 51));
        label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD, label1.getFont().getSize() + 3f));
        label1.setName("label1");
        p_contenu.add(label1);
        label1.setBounds(10, 5, 630, 30);

        //======== panel3 ========
        {
          panel3.setBorder(new TitledBorder("Solde comptable"));
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);

          //---- OBJ_15 ----
          OBJ_15.setText("Solde");
          OBJ_15.setName("OBJ_15");
          panel3.add(OBJ_15);
          OBJ_15.setBounds(20, 30, 140, 28);

          //---- ESOLD ----
          ESOLD.setName("ESOLD");
          panel3.add(ESOLD);
          ESOLD.setBounds(170, 30, 130, ESOLD.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel3.getComponentCount(); i++) {
              Rectangle bounds = panel3.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel3.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel3.setMinimumSize(preferredSize);
            panel3.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel3);
        panel3.setBounds(330, 35, 315, 75);

        //======== panel4 ========
        {
          panel4.setBorder(new TitledBorder("reste d\u00fb"));
          panel4.setOpaque(false);
          panel4.setName("panel4");
          panel4.setLayout(null);

          //---- OBJ_13 ----
          OBJ_13.setText("Livraisons non factur\u00e9s");
          OBJ_13.setName("OBJ_13");
          panel4.add(OBJ_13);
          OBJ_13.setBounds(20, 60, 150, 28);

          //---- EBLNF ----
          EBLNF.setName("EBLNF");
          panel4.add(EBLNF);
          EBLNF.setBounds(170, 60, 130, EBLNF.getPreferredSize().height);

          //---- OBJ_14 ----
          OBJ_14.setText("Commandes en instance");
          OBJ_14.setName("OBJ_14");
          panel4.add(OBJ_14);
          OBJ_14.setBounds(20, 90, 150, 28);

          //---- ECDINS ----
          ECDINS.setName("ECDINS");
          panel4.add(ECDINS);
          ECDINS.setBounds(170, 90, 130, ECDINS.getPreferredSize().height);

          //---- OBJ_16 ----
          OBJ_16.setText("Impay\u00e9s");
          OBJ_16.setName("OBJ_16");
          panel4.add(OBJ_16);
          OBJ_16.setBounds(20, 30, 150, 28);

          //---- EIMP ----
          EIMP.setName("EIMP");
          panel4.add(EIMP);
          EIMP.setBounds(170, 30, 130, EIMP.getPreferredSize().height);

          //---- OBJ_17 ----
          OBJ_17.setText("R\u00e8glements en instance");
          OBJ_17.setName("OBJ_17");
          panel4.add(OBJ_17);
          OBJ_17.setBounds(20, 120, 150, 28);

          //---- ERGIN ----
          ERGIN.setName("ERGIN");
          panel4.add(ERGIN);
          ERGIN.setBounds(170, 120, 130, ERGIN.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel4.getComponentCount(); i++) {
              Rectangle bounds = panel4.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel4.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel4.setMinimumSize(preferredSize);
            panel4.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel4);
        panel4.setBounds(10, 195, 315, 160);

        //======== panel5 ========
        {
          panel5.setBorder(new TitledBorder(""));
          panel5.setOpaque(false);
          panel5.setName("panel5");
          panel5.setLayout(null);

          //---- OBJ_18 ----
          OBJ_18.setText("Risque global du client");
          OBJ_18.setForeground(new Color(204, 0, 0));
          OBJ_18.setFont(OBJ_18.getFont().deriveFont(OBJ_18.getFont().getStyle() | Font.BOLD));
          OBJ_18.setName("OBJ_18");
          panel5.add(OBJ_18);
          OBJ_18.setBounds(20, 10, 150, 28);

          //---- ERGLB ----
          ERGLB.setName("ERGLB");
          panel5.add(ERGLB);
          ERGLB.setBounds(170, 10, 130, ERGLB.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel5.getComponentCount(); i++) {
              Rectangle bounds = panel5.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel5.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel5.setMinimumSize(preferredSize);
            panel5.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel5);
        panel5.setBounds(330, 300, 315, 53);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_5;
  private XRiTextField PLENC;
  private JLabel OBJ_7;
  private XRiTextField PLAUT;
  private XRiTextField PLASC;
  private JLabel OBJ_8;
  private JLabel OBJ_9;
  private XRiComboBox PLTAS;
  private JPanel panel2;
  private JLabel OBJ_6;
  private XRiTextField EFAC60;
  private JLabel OBJ_10;
  private XRiTextField EFAC30;
  private XRiTextField EFAC00;
  private JLabel OBJ_11;
  private XRiTextField EFACNE;
  private JLabel OBJ_12;
  private JLabel label1;
  private JPanel panel3;
  private JLabel OBJ_15;
  private XRiTextField ESOLD;
  private JPanel panel4;
  private JLabel OBJ_13;
  private XRiTextField EBLNF;
  private JLabel OBJ_14;
  private XRiTextField ECDINS;
  private JLabel OBJ_16;
  private XRiTextField EIMP;
  private JLabel OBJ_17;
  private XRiTextField ERGIN;
  private JPanel panel5;
  private JLabel OBJ_18;
  private XRiTextField ERGLB;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
