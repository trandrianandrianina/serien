
package ri.serien.libecranrpg.vcgm.VCGM51FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Emmanuel MARCQ
 */
public class VCGM51FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] DXPAI_Value = { "E", "F", };
  private String[] DXPAI_Title = { "Euro", "Franc", };
  private String[] DXTPA_Value = { "T", "V", "C", "E", };
  private String[] DXTPA_Title = { "Télérèglement", "Virement", "Chèque", "Espèces", };
  
  public VCGM51FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    DXPAI.setValeurs(DXPAI_Value, DXPAI_Title);
    DXTPA.setValeurs(DXTPA_Value, DXTPA_Title);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    LIBPG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBPG@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_87.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBJO@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    // TODO Icones
    OBJ_91.setIcon(lexique.chargerImage("images/tel.gif", true));
    
    

    
    p_bpresentation.setCodeEtablissement(DXSOC.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(DXSOC.getText()));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_301 = new JLabel();
    DXSOC = new XRiTextField();
    OBJ_341 = new JLabel();
    DXNUM = new XRiTextField();
    LIBPG = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_95 = new JLabel();
    DXGEX = new XRiCalendrier();
    OBJ_97 = new JLabel();
    DXDEBX = new XRiCalendrier();
    OBJ_98 = new JLabel();
    DXFINX = new XRiCalendrier();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_99 = new JLabel();
    DGEUR = new XRiTextField();
    OBJ_101 = new JLabel();
    DXEUR = new XRiTextField();
    xTitledPanel3 = new JXTitledPanel();
    OBJ_86 = new JLabel();
    OBJ_88 = new JLabel();
    DXCJO = new XRiTextField();
    OBJ_87 = new RiZoneSortie();
    DXCFO = new XRiTextField();
    OBJ_89 = new JLabel();
    WCPT = new XRiTextField();
    xTitledPanel4 = new JXTitledPanel();
    OBJ_102 = new JLabel();
    DXTPA = new XRiComboBox();
    OBJ_103 = new JLabel();
    DXNPA = new XRiTextField();
    OBJ_104 = new JLabel();
    DXPAI = new XRiComboBox();
    xTitledPanel5 = new JXTitledPanel();
    DXCO1 = new XRiTextField();
    DXCO2 = new XRiTextField();
    DXCO3 = new XRiTextField();
    DXCO4 = new XRiTextField();
    DXCO5 = new XRiTextField();
    xTitledPanel6 = new JXTitledPanel();
    DXNOM = new XRiTextField();
    DXRUE = new XRiTextField();
    DXLOC = new XRiTextField();
    DXVILR = new XRiTextField();
    DXTEL = new XRiTextField();
    OBJ_91 = new JLabel();
    DXCDP = new XRiTextField();
    OBJ_82 = new JLabel();
    DXSIG = new XRiTextField();
    DXSIR = new XRiTextField();
    DXFRP = new XRiTextField();
    DXTIC = new XRiTextField();
    OBJ_81 = new JLabel();
    OBJ_80 = new JLabel();
    OBJ_92 = new JLabel();
    DXAPE = new XRiTextField();
    OBJ_85 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Description g\u00e9n\u00e9rale de la TVA");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_301 ----
          OBJ_301.setText("Soci\u00e9t\u00e9");
          OBJ_301.setName("OBJ_301");
          p_tete_gauche.add(OBJ_301);
          OBJ_301.setBounds(10, 4, 75, 20);

          //---- DXSOC ----
          DXSOC.setComponentPopupMenu(BTD);
          DXSOC.setName("DXSOC");
          p_tete_gauche.add(DXSOC);
          DXSOC.setBounds(135, 0, 40, DXSOC.getPreferredSize().height);

          //---- OBJ_341 ----
          OBJ_341.setText("Num\u00e9ro description");
          OBJ_341.setName("OBJ_341");
          p_tete_gauche.add(OBJ_341);
          OBJ_341.setBounds(205, 4, 121, 20);

          //---- DXNUM ----
          DXNUM.setComponentPopupMenu(BTD);
          DXNUM.setName("DXNUM");
          p_tete_gauche.add(DXNUM);
          DXNUM.setBounds(330, 0, 52, DXNUM.getPreferredSize().height);

          //---- LIBPG ----
          LIBPG.setText("@LIBPG@");
          LIBPG.setOpaque(false);
          LIBPG.setName("LIBPG");
          p_tete_gauche.add(LIBPG);
          LIBPG.setBounds(390, 2, 300, LIBPG.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(970, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setTitle("Param\u00e8tres G\u00e9n\u00e9ration");
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- OBJ_95 ----
            OBJ_95.setText("Date de g\u00e9n\u00e9ration");
            OBJ_95.setName("OBJ_95");
            xTitledPanel1ContentContainer.add(OBJ_95);
            OBJ_95.setBounds(15, 10, 115, 21);

            //---- DXGEX ----
            DXGEX.setName("DXGEX");
            xTitledPanel1ContentContainer.add(DXGEX);
            DXGEX.setBounds(135, 5, 105, DXGEX.getPreferredSize().height);

            //---- OBJ_97 ----
            OBJ_97.setText("P\u00e9riode");
            OBJ_97.setName("OBJ_97");
            xTitledPanel1ContentContainer.add(OBJ_97);
            OBJ_97.setBounds(280, 10, 51, 21);

            //---- DXDEBX ----
            DXDEBX.setComponentPopupMenu(BTD);
            DXDEBX.setName("DXDEBX");
            xTitledPanel1ContentContainer.add(DXDEBX);
            DXDEBX.setBounds(335, 6, 105, DXDEBX.getPreferredSize().height);

            //---- OBJ_98 ----
            OBJ_98.setText("\u00e0");
            OBJ_98.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_98.setName("OBJ_98");
            xTitledPanel1ContentContainer.add(OBJ_98);
            OBJ_98.setBounds(444, 10, 25, 21);

            //---- DXFINX ----
            DXFINX.setComponentPopupMenu(BTD);
            DXFINX.setName("DXFINX");
            xTitledPanel1ContentContainer.add(DXFINX);
            DXFINX.setBounds(473, 6, 105, DXFINX.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setTitle("Indicateurs Euro");
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //---- OBJ_99 ----
            OBJ_99.setText("Comptabilit\u00e9");
            OBJ_99.setName("OBJ_99");
            xTitledPanel2ContentContainer.add(OBJ_99);
            OBJ_99.setBounds(5, 9, 78, 20);

            //---- DGEUR ----
            DGEUR.setName("DGEUR");
            xTitledPanel2ContentContainer.add(DGEUR);
            DGEUR.setBounds(90, 5, 20, DGEUR.getPreferredSize().height);

            //---- OBJ_101 ----
            OBJ_101.setText("D\u00e9claration");
            OBJ_101.setName("OBJ_101");
            xTitledPanel2ContentContainer.add(OBJ_101);
            OBJ_101.setBounds(170, 9, 72, 20);

            //---- DXEUR ----
            DXEUR.setComponentPopupMenu(BTD);
            DXEUR.setName("DXEUR");
            xTitledPanel2ContentContainer.add(DXEUR);
            DXEUR.setBounds(255, 5, 20, DXEUR.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel3 ========
          {
            xTitledPanel3.setBorder(new DropShadowBorder());
            xTitledPanel3.setTitle("Param\u00e8tres Comptabilisation");
            xTitledPanel3.setName("xTitledPanel3");
            Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
            xTitledPanel3ContentContainer.setLayout(null);

            //---- OBJ_86 ----
            OBJ_86.setText("Journal");
            OBJ_86.setName("OBJ_86");
            xTitledPanel3ContentContainer.add(OBJ_86);
            OBJ_86.setBounds(10, 7, 66, 24);

            //---- OBJ_88 ----
            OBJ_88.setText("Folio");
            OBJ_88.setName("OBJ_88");
            xTitledPanel3ContentContainer.add(OBJ_88);
            OBJ_88.setBounds(10, 42, 52, 24);

            //---- DXCJO ----
            DXCJO.setComponentPopupMenu(BTD);
            DXCJO.setName("DXCJO");
            xTitledPanel3ContentContainer.add(DXCJO);
            DXCJO.setBounds(65, 5, 30, DXCJO.getPreferredSize().height);

            //---- OBJ_87 ----
            OBJ_87.setText("@LIBJO@");
            OBJ_87.setName("OBJ_87");
            xTitledPanel3ContentContainer.add(OBJ_87);
            OBJ_87.setBounds(135, 7, 283, 25);

            //---- DXCFO ----
            DXCFO.setComponentPopupMenu(BTD);
            DXCFO.setName("DXCFO");
            xTitledPanel3ContentContainer.add(DXCFO);
            DXCFO.setBounds(65, 40, 44, DXCFO.getPreferredSize().height);

            //---- OBJ_89 ----
            OBJ_89.setText("Comptabilisation");
            OBJ_89.setName("OBJ_89");
            xTitledPanel3ContentContainer.add(OBJ_89);
            OBJ_89.setBounds(135, 42, 103, 24);

            //---- WCPT ----
            WCPT.setName("WCPT");
            xTitledPanel3ContentContainer.add(WCPT);
            WCPT.setBounds(240, 40, 36, WCPT.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel3ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel4 ========
          {
            xTitledPanel4.setTitle("Moyen de paiement");
            xTitledPanel4.setName("xTitledPanel4");
            Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
            xTitledPanel4ContentContainer.setLayout(null);

            //---- OBJ_102 ----
            OBJ_102.setText("Type paiement");
            OBJ_102.setName("OBJ_102");
            xTitledPanel4ContentContainer.add(OBJ_102);
            OBJ_102.setBounds(8, 9, 94, 20);

            //---- DXTPA ----
            DXTPA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DXTPA.setName("DXTPA");
            xTitledPanel4ContentContainer.add(DXTPA);
            DXTPA.setBounds(145, 6, 150, DXTPA.getPreferredSize().height);

            //---- OBJ_103 ----
            OBJ_103.setText("Nombre");
            OBJ_103.setName("OBJ_103");
            xTitledPanel4ContentContainer.add(OBJ_103);
            OBJ_103.setBounds(345, 9, 77, 20);

            //---- DXNPA ----
            DXNPA.setComponentPopupMenu(BTD);
            DXNPA.setName("DXNPA");
            xTitledPanel4ContentContainer.add(DXNPA);
            DXNPA.setBounds(425, 5, 21, DXNPA.getPreferredSize().height);

            //---- OBJ_104 ----
            OBJ_104.setText("Monnaie de paiement");
            OBJ_104.setName("OBJ_104");
            xTitledPanel4ContentContainer.add(OBJ_104);
            OBJ_104.setBounds(8, 41, 133, 20);

            //---- DXPAI ----
            DXPAI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DXPAI.setName("DXPAI");
            xTitledPanel4ContentContainer.add(DXPAI);
            DXPAI.setBounds(143, 38, 85, DXPAI.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel4ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel4ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel4ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel4ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel4ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel5 ========
          {
            xTitledPanel5.setBorder(new DropShadowBorder());
            xTitledPanel5.setName("xTitledPanel5");
            Container xTitledPanel5ContentContainer = xTitledPanel5.getContentContainer();
            xTitledPanel5ContentContainer.setLayout(null);

            //---- DXCO1 ----
            DXCO1.setName("DXCO1");
            xTitledPanel5ContentContainer.add(DXCO1);
            DXCO1.setBounds(75, 5, 764, DXCO1.getPreferredSize().height);

            //---- DXCO2 ----
            DXCO2.setName("DXCO2");
            xTitledPanel5ContentContainer.add(DXCO2);
            DXCO2.setBounds(75, 30, 764, DXCO2.getPreferredSize().height);

            //---- DXCO3 ----
            DXCO3.setName("DXCO3");
            xTitledPanel5ContentContainer.add(DXCO3);
            DXCO3.setBounds(75, 55, 764, DXCO3.getPreferredSize().height);

            //---- DXCO4 ----
            DXCO4.setName("DXCO4");
            xTitledPanel5ContentContainer.add(DXCO4);
            DXCO4.setBounds(75, 80, 764, DXCO4.getPreferredSize().height);

            //---- DXCO5 ----
            DXCO5.setName("DXCO5");
            xTitledPanel5ContentContainer.add(DXCO5);
            DXCO5.setBounds(75, 105, 764, DXCO5.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel5ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel5ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel5ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel5ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel5ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel6 ========
          {
            xTitledPanel6.setBorder(new DropShadowBorder());
            xTitledPanel6.setTitle("Identification");
            xTitledPanel6.setName("xTitledPanel6");
            Container xTitledPanel6ContentContainer = xTitledPanel6.getContentContainer();
            xTitledPanel6ContentContainer.setLayout(null);

            //---- DXNOM ----
            DXNOM.setComponentPopupMenu(BTD);
            DXNOM.setName("DXNOM");
            xTitledPanel6ContentContainer.add(DXNOM);
            DXNOM.setBounds(12, 13, 370, DXNOM.getPreferredSize().height);

            //---- DXRUE ----
            DXRUE.setComponentPopupMenu(BTD);
            DXRUE.setName("DXRUE");
            xTitledPanel6ContentContainer.add(DXRUE);
            DXRUE.setBounds(12, 44, 370, DXRUE.getPreferredSize().height);

            //---- DXLOC ----
            DXLOC.setComponentPopupMenu(BTD);
            DXLOC.setName("DXLOC");
            xTitledPanel6ContentContainer.add(DXLOC);
            DXLOC.setBounds(12, 75, 370, DXLOC.getPreferredSize().height);

            //---- DXVILR ----
            DXVILR.setComponentPopupMenu(BTD);
            DXVILR.setName("DXVILR");
            xTitledPanel6ContentContainer.add(DXVILR);
            DXVILR.setBounds(85, 106, 297, DXVILR.getPreferredSize().height);

            //---- DXTEL ----
            DXTEL.setComponentPopupMenu(BTD);
            DXTEL.setName("DXTEL");
            xTitledPanel6ContentContainer.add(DXTEL);
            DXTEL.setBounds(85, 137, 297, DXTEL.getPreferredSize().height);

            //---- OBJ_91 ----
            OBJ_91.setIcon(new ImageIcon("images/tel.gif"));
            OBJ_91.setName("OBJ_91");
            xTitledPanel6ContentContainer.add(OBJ_91);
            OBJ_91.setBounds(12, 135, 45, 33);

            //---- DXCDP ----
            DXCDP.setComponentPopupMenu(BTD);
            DXCDP.setName("DXCDP");
            xTitledPanel6ContentContainer.add(DXCDP);
            DXCDP.setBounds(12, 106, 64, DXCDP.getPreferredSize().height);

            //---- OBJ_82 ----
            OBJ_82.setText("N\u00b0TVA intracommunautaire");
            OBJ_82.setName("OBJ_82");
            xTitledPanel6ContentContainer.add(OBJ_82);
            OBJ_82.setBounds(475, 78, 174, 22);

            //---- DXSIG ----
            DXSIG.setComponentPopupMenu(BTD);
            DXSIG.setName("DXSIG");
            xTitledPanel6ContentContainer.add(DXSIG);
            DXSIG.setBounds(655, 137, 253, DXSIG.getPreferredSize().height);

            //---- DXSIR ----
            DXSIR.setComponentPopupMenu(BTD);
            DXSIR.setName("DXSIR");
            xTitledPanel6ContentContainer.add(DXSIR);
            DXSIR.setBounds(655, 13, 133, DXSIR.getPreferredSize().height);

            //---- DXFRP ----
            DXFRP.setComponentPopupMenu(BTD);
            DXFRP.setName("DXFRP");
            xTitledPanel6ContentContainer.add(DXFRP);
            DXFRP.setBounds(655, 44, 133, DXFRP.getPreferredSize().height);

            //---- DXTIC ----
            DXTIC.setComponentPopupMenu(BTD);
            DXTIC.setName("DXTIC");
            xTitledPanel6ContentContainer.add(DXTIC);
            DXTIC.setBounds(655, 75, 133, DXTIC.getPreferredSize().height);

            //---- OBJ_81 ----
            OBJ_81.setText("Num\u00e9ro FRP");
            OBJ_81.setName("OBJ_81");
            xTitledPanel6ContentContainer.add(OBJ_81);
            OBJ_81.setBounds(475, 47, 100, 22);

            //---- OBJ_80 ----
            OBJ_80.setText("Code SIRET");
            OBJ_80.setName("OBJ_80");
            xTitledPanel6ContentContainer.add(OBJ_80);
            OBJ_80.setBounds(475, 16, 97, 22);

            //---- OBJ_92 ----
            OBJ_92.setText("Signataire");
            OBJ_92.setName("OBJ_92");
            xTitledPanel6ContentContainer.add(OBJ_92);
            OBJ_92.setBounds(475, 140, 82, 22);

            //---- DXAPE ----
            DXAPE.setComponentPopupMenu(BTD);
            DXAPE.setName("DXAPE");
            xTitledPanel6ContentContainer.add(DXAPE);
            DXAPE.setBounds(655, 106, 67, DXAPE.getPreferredSize().height);

            //---- OBJ_85 ----
            OBJ_85.setText("APE");
            OBJ_85.setName("OBJ_85");
            xTitledPanel6ContentContainer.add(OBJ_85);
            OBJ_85.setBounds(475, 109, 49, 22);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel6ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel6ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel6ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel6ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel6ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(xTitledPanel6, GroupLayout.PREFERRED_SIZE, 940, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 605, GroupLayout.PREFERRED_SIZE)
                    .addGap(15, 15, 15)
                    .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 320, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 440, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 490, GroupLayout.PREFERRED_SIZE))
                  .addComponent(xTitledPanel5, GroupLayout.PREFERRED_SIZE, 940, GroupLayout.PREFERRED_SIZE)))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(xTitledPanel6, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addComponent(xTitledPanel5, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_301;
  private XRiTextField DXSOC;
  private JLabel OBJ_341;
  private XRiTextField DXNUM;
  private RiZoneSortie LIBPG;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_95;
  private XRiCalendrier DXGEX;
  private JLabel OBJ_97;
  private XRiCalendrier DXDEBX;
  private JLabel OBJ_98;
  private XRiCalendrier DXFINX;
  private JXTitledPanel xTitledPanel2;
  private JLabel OBJ_99;
  private XRiTextField DGEUR;
  private JLabel OBJ_101;
  private XRiTextField DXEUR;
  private JXTitledPanel xTitledPanel3;
  private JLabel OBJ_86;
  private JLabel OBJ_88;
  private XRiTextField DXCJO;
  private RiZoneSortie OBJ_87;
  private XRiTextField DXCFO;
  private JLabel OBJ_89;
  private XRiTextField WCPT;
  private JXTitledPanel xTitledPanel4;
  private JLabel OBJ_102;
  private XRiComboBox DXTPA;
  private JLabel OBJ_103;
  private XRiTextField DXNPA;
  private JLabel OBJ_104;
  private XRiComboBox DXPAI;
  private JXTitledPanel xTitledPanel5;
  private XRiTextField DXCO1;
  private XRiTextField DXCO2;
  private XRiTextField DXCO3;
  private XRiTextField DXCO4;
  private XRiTextField DXCO5;
  private JXTitledPanel xTitledPanel6;
  private XRiTextField DXNOM;
  private XRiTextField DXRUE;
  private XRiTextField DXLOC;
  private XRiTextField DXVILR;
  private XRiTextField DXTEL;
  private JLabel OBJ_91;
  private XRiTextField DXCDP;
  private JLabel OBJ_82;
  private XRiTextField DXSIG;
  private XRiTextField DXSIR;
  private XRiTextField DXFRP;
  private XRiTextField DXTIC;
  private JLabel OBJ_81;
  private JLabel OBJ_80;
  private JLabel OBJ_92;
  private XRiTextField DXAPE;
  private JLabel OBJ_85;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
