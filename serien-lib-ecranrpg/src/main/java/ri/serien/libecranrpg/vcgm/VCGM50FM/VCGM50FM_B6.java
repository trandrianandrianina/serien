
package ri.serien.libecranrpg.vcgm.VCGM50FM;
// Nom Fichier: i_VCGM50FM_F02B3_FMTF1_723.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM50FM_B6 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM50FM_B6(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDefaultButton(bouton_valider);
    
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDETA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETA@")).trim());
    LIBETA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBETA@")).trim());
    OBJ_48_OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("page @WPAGE@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  DECLARATION TVA"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    BTD = new JPopupMenu();
    OBJ_15 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_47_OBJ_47 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_48_OBJ_48 = new JLabel();
    INDETA = new RiZoneSortie();
    LIBETA = new RiZoneSortie();
    p_tete_droite = new JPanel();
    OBJ_48_OBJ_50 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    MT1901 = new XRiTextField();
    MT2001 = new XRiTextField();
    MT2101 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    MT2201 = new XRiTextField();
    MT2301 = new XRiTextField();
    MT2401 = new XRiTextField();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    MT2501 = new XRiTextField();
    MT2601 = new XRiTextField();
    MT2701 = new XRiTextField();
    label7 = new JLabel();
    label8 = new JLabel();
    label10 = new JLabel();
    MT2801 = new XRiTextField();
    MT2901 = new XRiTextField();
    MT3001 = new XRiTextField();
    label11 = new JLabel();
    label12 = new JLabel();
    label13 = new JLabel();
    MT3101 = new XRiTextField();
    MT3201 = new XRiTextField();
    MT3401 = new XRiTextField();
    MT3501 = new XRiTextField();
    MT3301 = new XRiTextField();
    label14 = new JLabel();
    label15 = new JLabel();
    label16 = new JLabel();
    label20 = new JLabel();
    label21 = new JLabel();
    WTT31R = new XRiTextField();
    WTT35R = new XRiTextField();
    label17 = new JLabel();
    label22 = new JLabel();
    label23 = new JLabel();
    label18 = new JLabel();
    label24 = new JLabel();
    label25 = new JLabel();
    label27 = new JLabel();
    label19 = new JLabel();
    label28 = new JLabel();
    label29 = new JLabel();
    label30 = new JLabel();
    label33 = new JLabel();
    label34 = new JLabel();
    label35 = new JLabel();
    label26 = new JLabel();
    FLD001 = new XRiTextField();
    OBJ_48_OBJ_49 = new JLabel();

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_15 ----
      OBJ_15.setText("Choix possibles");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);

      //---- OBJ_14 ----
      OBJ_14.setText("Aide en ligne");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("D\u00e9claration de TVA");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_47_OBJ_47 ----
          OBJ_47_OBJ_47.setText("Etablissement");
          OBJ_47_OBJ_47.setName("OBJ_47_OBJ_47");
          p_tete_gauche.add(OBJ_47_OBJ_47);
          OBJ_47_OBJ_47.setBounds(5, 5, 100, 21);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setText("@INDETB@");
          INDETB.setOpaque(false);
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(110, 3, 40, INDETB.getPreferredSize().height);

          //---- OBJ_48_OBJ_48 ----
          OBJ_48_OBJ_48.setText("Etat");
          OBJ_48_OBJ_48.setName("OBJ_48_OBJ_48");
          p_tete_gauche.add(OBJ_48_OBJ_48);
          OBJ_48_OBJ_48.setBounds(190, 5, 40, 21);

          //---- INDETA ----
          INDETA.setComponentPopupMenu(BTD);
          INDETA.setText("@INDETA@");
          INDETA.setOpaque(false);
          INDETA.setName("INDETA");
          p_tete_gauche.add(INDETA);
          INDETA.setBounds(240, 3, 64, INDETA.getPreferredSize().height);

          //---- LIBETA ----
          LIBETA.setText("@LIBETA@");
          LIBETA.setOpaque(false);
          LIBETA.setName("LIBETA");
          p_tete_gauche.add(LIBETA);
          LIBETA.setBounds(310, 3, 532, LIBETA.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- OBJ_48_OBJ_50 ----
          OBJ_48_OBJ_50.setText("page @WPAGE@");
          OBJ_48_OBJ_50.setFont(OBJ_48_OBJ_50.getFont().deriveFont(OBJ_48_OBJ_50.getFont().getStyle() | Font.BOLD, OBJ_48_OBJ_50.getFont().getSize() + 2f));
          OBJ_48_OBJ_50.setName("OBJ_48_OBJ_50");
          p_tete_droite.add(OBJ_48_OBJ_50);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Protection zones CGM");
              riSousMenu_bt6.setToolTipText("Push/Pull protection des zones g\u00e9n\u00e9r\u00e9es depuis CGM");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(980, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("B- DECOMPTE DES TAXES ASSIMILEES"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== xTitledPanel1 ========
            {
              xTitledPanel1.setTitle("Nature des taxes                                                                                                                                                                                                                          Net \u00e0 payer");
              xTitledPanel1.setBorder(new DropShadowBorder());
              xTitledPanel1.setName("xTitledPanel1");
              Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
              xTitledPanel1ContentContainer.setLayout(null);

              //---- MT1901 ----
              MT1901.setName("MT1901");
              xTitledPanel1ContentContainer.add(MT1901);
              MT1901.setBounds(720, 5, 135, MT1901.getPreferredSize().height);

              //---- MT2001 ----
              MT2001.setName("MT2001");
              xTitledPanel1ContentContainer.add(MT2001);
              MT2001.setBounds(720, 30, 135, MT2001.getPreferredSize().height);

              //---- MT2101 ----
              MT2101.setName("MT2101");
              xTitledPanel1ContentContainer.add(MT2101);
              MT2101.setBounds(720, 55, 135, MT2101.getPreferredSize().height);

              //---- label1 ----
              label1.setText("47 - Taxe sur certaines d\u00e9penses de publicit\u00e9 (art 302bis MA)");
              label1.setName("label1");
              xTitledPanel1ContentContainer.add(label1);
              label1.setBounds(12, 5, 550, 28);

              //---- label2 ----
              label2.setText("48 - Taxe sur les retransmissions sportives   (art 302bis ZE) 4215");
              label2.setName("label2");
              xTitledPanel1ContentContainer.add(label2);
              label2.setBounds(12, 30, 550, 28);

              //---- label3 ----
              label3.setText("49 - Taxe sur les achats de viande            (art 302bis ZD) 4210");
              label3.setName("label3");
              xTitledPanel1ContentContainer.add(label3);
              label3.setBounds(12, 55, 550, 28);

              //---- MT2201 ----
              MT2201.setName("MT2201");
              xTitledPanel1ContentContainer.add(MT2201);
              MT2201.setBounds(720, 80, 135, MT2201.getPreferredSize().height);

              //---- MT2301 ----
              MT2301.setName("MT2301");
              xTitledPanel1ContentContainer.add(MT2301);
              MT2301.setBounds(720, 105, 135, MT2301.getPreferredSize().height);

              //---- MT2401 ----
              MT2401.setName("MT2401");
              xTitledPanel1ContentContainer.add(MT2401);
              MT2401.setBounds(720, 130, 135, MT2401.getPreferredSize().height);

              //---- label4 ----
              label4.setText("50 - Taxe sur les produits d'horlogerie, de bijouterie,... (ann.II, 345+s)");
              label4.setName("label4");
              xTitledPanel1ContentContainer.add(label4);
              label4.setBounds(12, 80, 550, 28);

              //---- label5 ----
              label5.setText("51 - R\u00e9duction sur ouvrages librairie \u00e9dit\u00e9s  (art 1609 u+s )");
              label5.setName("label5");
              xTitledPanel1ContentContainer.add(label5);
              label5.setBounds(12, 105, 550, 28);

              //---- label6 ----
              label6.setText("52 - R\u00e9duction sur l'emploi de la reprographie(art 1609 u+s )");
              label6.setName("label6");
              xTitledPanel1ContentContainer.add(label6);
              label6.setBounds(12, 130, 550, 28);

              //---- MT2501 ----
              MT2501.setName("MT2501");
              xTitledPanel1ContentContainer.add(MT2501);
              MT2501.setBounds(720, 155, 135, MT2501.getPreferredSize().height);

              //---- MT2601 ----
              MT2601.setName("MT2601");
              xTitledPanel1ContentContainer.add(MT2601);
              MT2601.setBounds(720, 180, 135, MT2601.getPreferredSize().height);

              //---- MT2701 ----
              MT2701.setName("MT2701");
              xTitledPanel1ContentContainer.add(MT2701);
              MT2701.setBounds(720, 205, 135, MT2701.getPreferredSize().height);

              //---- label7 ----
              label7.setText("53 - Taxe sur les huiles alimentaires BAPSA (art 1609 vicies)");
              label7.setName("label7");
              xTitledPanel1ContentContainer.add(label7);
              label7.setBounds(12, 155, 550, 28);

              //---- label8 ----
              label8.setText("54  - Taxe due sur les tabacs BAPSA        (art 1609 unvicies)");
              label8.setName("label8");
              xTitledPanel1ContentContainer.add(label8);
              label8.setBounds(12, 180, 550, 28);

              //---- label10 ----
              label10.setText("55 - Taxe par concessionnaires d'autoroutes   (art 302bis ZB)");
              label10.setName("label10");
              xTitledPanel1ContentContainer.add(label10);
              label10.setBounds(12, 205, 550, 28);

              //---- MT2801 ----
              MT2801.setName("MT2801");
              xTitledPanel1ContentContainer.add(MT2801);
              MT2801.setBounds(720, 230, 135, MT2801.getPreferredSize().height);

              //---- MT2901 ----
              MT2901.setName("MT2901");
              xTitledPanel1ContentContainer.add(MT2901);
              MT2901.setBounds(720, 255, 135, MT2901.getPreferredSize().height);

              //---- MT3001 ----
              MT3001.setName("MT3001");
              xTitledPanel1ContentContainer.add(MT3001);
              MT3001.setBounds(720, 280, 135, MT3001.getPreferredSize().height);

              //---- label11 ----
              label11.setText("56 - Taxe due par tit. d'ouvrages hydro\u00e9lec.  (art 302bis ZA)");
              label11.setName("label11");
              xTitledPanel1ContentContainer.add(label11);
              label11.setBounds(12, 230, 550, 28);

              //---- label12 ----
              label12.setText("57 - Taxe parafis. forfait. au prof.ANDA (ann II, art 363 DA)");
              label12.setName("label12");
              xTitledPanel1ContentContainer.add(label12);
              label12.setBounds(12, 255, 550, 28);

              //---- label13 ----
              label13.setText("58 - Taxe sur les prdts de l'horticulture (ann II,art 363 DB)");
              label13.setName("label13");
              xTitledPanel1ContentContainer.add(label13);
              label13.setBounds(12, 280, 550, 28);

              //---- MT3101 ----
              MT3101.setName("MT3101");
              xTitledPanel1ContentContainer.add(MT3101);
              MT3101.setBounds(720, 305, 135, MT3101.getPreferredSize().height);

              //---- MT3201 ----
              MT3201.setName("MT3201");
              xTitledPanel1ContentContainer.add(MT3201);
              MT3201.setBounds(720, 330, 135, MT3201.getPreferredSize().height);

              //---- MT3401 ----
              MT3401.setName("MT3401");
              xTitledPanel1ContentContainer.add(MT3401);
              MT3401.setBounds(720, 380, 135, MT3401.getPreferredSize().height);

              //---- MT3501 ----
              MT3501.setName("MT3501");
              xTitledPanel1ContentContainer.add(MT3501);
              MT3501.setBounds(720, 405, 135, MT3501.getPreferredSize().height);

              //---- MT3301 ----
              MT3301.setName("MT3301");
              xTitledPanel1ContentContainer.add(MT3301);
              MT3301.setBounds(720, 355, 135, MT3301.getPreferredSize().height);

              //---- label14 ----
              label14.setText("61 - Taxe sur pub.radio. et t\u00e9l\u00e9. au prof.FSER (ann II, 365 )");
              label14.setName("label14");
              xTitledPanel1ContentContainer.add(label14);
              label14.setBounds(12, 355, 550, 28);

              //---- label15 ----
              label15.setText("62 - Taxe sur la publicit\u00e9 t\u00e9l\u00e9vis\u00e9e");
              label15.setName("label15");
              xTitledPanel1ContentContainer.add(label15);
              label15.setBounds(12, 380, 550, 28);

              //---- label16 ----
              label16.setText("63 -");
              label16.setName("label16");
              xTitledPanel1ContentContainer.add(label16);
              label16.setBounds(12, 405, 23, 28);

              //---- label20 ----
              label20.setText("60 - Taxe sur les services de t\u00e9l\u00e9vision  (art 302bis KB+KC)");
              label20.setName("label20");
              xTitledPanel1ContentContainer.add(label20);
              label20.setBounds(12, 330, 550, 28);

              //---- label21 ----
              label21.setText("59 -");
              label21.setName("label21");
              xTitledPanel1ContentContainer.add(label21);
              label21.setBounds(12, 305, 23, 28);

              //---- WTT31R ----
              WTT31R.setName("WTT31R");
              xTitledPanel1ContentContainer.add(WTT31R);
              WTT31R.setBounds(35, 305, 420, WTT31R.getPreferredSize().height);

              //---- WTT35R ----
              WTT35R.setName("WTT35R");
              xTitledPanel1ContentContainer.add(WTT35R);
              WTT35R.setBounds(35, 405, 420, WTT35R.getPreferredSize().height);

              //---- label17 ----
              label17.setText("4213");
              label17.setFont(label17.getFont().deriveFont(label17.getFont().getStyle() | Font.BOLD));
              label17.setHorizontalAlignment(SwingConstants.RIGHT);
              label17.setName("label17");
              xTitledPanel1ContentContainer.add(label17);
              label17.setBounds(620, 7, 95, 25);

              //---- label22 ----
              label22.setText("4215");
              label22.setFont(label22.getFont().deriveFont(label22.getFont().getStyle() | Font.BOLD));
              label22.setHorizontalAlignment(SwingConstants.RIGHT);
              label22.setName("label22");
              xTitledPanel1ContentContainer.add(label22);
              label22.setBounds(620, 32, 95, 25);

              //---- label23 ----
              label23.setText("4210");
              label23.setFont(label23.getFont().deriveFont(label23.getFont().getStyle() | Font.BOLD));
              label23.setHorizontalAlignment(SwingConstants.RIGHT);
              label23.setName("label23");
              xTitledPanel1ContentContainer.add(label23);
              label23.setBounds(620, 57, 95, 25);

              //---- label18 ----
              label18.setText("3910");
              label18.setFont(label18.getFont().deriveFont(label18.getFont().getStyle() | Font.BOLD));
              label18.setHorizontalAlignment(SwingConstants.RIGHT);
              label18.setName("label18");
              xTitledPanel1ContentContainer.add(label18);
              label18.setBounds(620, 82, 95, 25);

              //---- label24 ----
              label24.setText("3510");
              label24.setFont(label24.getFont().deriveFont(label24.getFont().getStyle() | Font.BOLD));
              label24.setHorizontalAlignment(SwingConstants.RIGHT);
              label24.setName("label24");
              xTitledPanel1ContentContainer.add(label24);
              label24.setBounds(620, 107, 95, 25);

              //---- label25 ----
              label25.setText("3520");
              label25.setFont(label25.getFont().deriveFont(label25.getFont().getStyle() | Font.BOLD));
              label25.setHorizontalAlignment(SwingConstants.RIGHT);
              label25.setName("label25");
              xTitledPanel1ContentContainer.add(label25);
              label25.setBounds(620, 132, 95, 25);

              //---- label27 ----
              label27.setText("4211");
              label27.setFont(label27.getFont().deriveFont(label27.getFont().getStyle() | Font.BOLD));
              label27.setHorizontalAlignment(SwingConstants.RIGHT);
              label27.setName("label27");
              xTitledPanel1ContentContainer.add(label27);
              label27.setBounds(620, 282, 95, 25);

              //---- label19 ----
              label19.setText("4209");
              label19.setFont(label19.getFont().deriveFont(label19.getFont().getStyle() | Font.BOLD));
              label19.setHorizontalAlignment(SwingConstants.RIGHT);
              label19.setName("label19");
              xTitledPanel1ContentContainer.add(label19);
              label19.setBounds(620, 257, 95, 25);

              //---- label28 ----
              label28.setText("4208");
              label28.setFont(label28.getFont().deriveFont(label28.getFont().getStyle() | Font.BOLD));
              label28.setHorizontalAlignment(SwingConstants.RIGHT);
              label28.setName("label28");
              xTitledPanel1ContentContainer.add(label28);
              label28.setBounds(620, 232, 95, 25);

              //---- label29 ----
              label29.setText("4207");
              label29.setFont(label29.getFont().deriveFont(label29.getFont().getStyle() | Font.BOLD));
              label29.setHorizontalAlignment(SwingConstants.RIGHT);
              label29.setName("label29");
              xTitledPanel1ContentContainer.add(label29);
              label29.setBounds(620, 207, 95, 25);

              //---- label30 ----
              label30.setText("3260");
              label30.setFont(label30.getFont().deriveFont(label30.getFont().getStyle() | Font.BOLD));
              label30.setHorizontalAlignment(SwingConstants.RIGHT);
              label30.setName("label30");
              xTitledPanel1ContentContainer.add(label30);
              label30.setBounds(620, 182, 95, 25);

              //---- label33 ----
              label33.setText("4201");
              label33.setFont(label33.getFont().deriveFont(label33.getFont().getStyle() | Font.BOLD));
              label33.setHorizontalAlignment(SwingConstants.RIGHT);
              label33.setName("label33");
              xTitledPanel1ContentContainer.add(label33);
              label33.setBounds(620, 382, 95, 25);

              //---- label34 ----
              label34.setText("4200");
              label34.setFont(label34.getFont().deriveFont(label34.getFont().getStyle() | Font.BOLD));
              label34.setHorizontalAlignment(SwingConstants.RIGHT);
              label34.setName("label34");
              xTitledPanel1ContentContainer.add(label34);
              label34.setBounds(620, 357, 95, 25);

              //---- label35 ----
              label35.setText("4212");
              label35.setFont(label35.getFont().deriveFont(label35.getFont().getStyle() | Font.BOLD));
              label35.setHorizontalAlignment(SwingConstants.RIGHT);
              label35.setName("label35");
              xTitledPanel1ContentContainer.add(label35);
              label35.setBounds(620, 332, 95, 25);

              //---- label26 ----
              label26.setText("3240");
              label26.setFont(label26.getFont().deriveFont(label26.getFont().getStyle() | Font.BOLD));
              label26.setHorizontalAlignment(SwingConstants.RIGHT);
              label26.setName("label26");
              xTitledPanel1ContentContainer.add(label26);
              label26.setBounds(620, 157, 95, 25);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = xTitledPanel1ContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
                xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
              }
            }
            panel1.add(xTitledPanel1);
            xTitledPanel1.setBounds(15, 25, 905, 485);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //---- FLD001 ----
          FLD001.setName("FLD001");

          //---- OBJ_48_OBJ_49 ----
          OBJ_48_OBJ_49.setText("Aller \u00e0 la page");
          OBJ_48_OBJ_49.setName("OBJ_48_OBJ_49");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                  .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 954, Short.MAX_VALUE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(OBJ_48_OBJ_49, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(FLD001, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 526, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                  .addComponent(FLD001, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_48_OBJ_49, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
          );
          p_contenuLayout.linkSize(SwingConstants.VERTICAL, new Component[] {FLD001, OBJ_48_OBJ_49});
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu BTD;
  private JMenuItem OBJ_15;
  private JMenuItem OBJ_14;
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_47_OBJ_47;
  private RiZoneSortie INDETB;
  private JLabel OBJ_48_OBJ_48;
  private RiZoneSortie INDETA;
  private RiZoneSortie LIBETA;
  private JPanel p_tete_droite;
  private JLabel OBJ_48_OBJ_50;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JXTitledPanel xTitledPanel1;
  private XRiTextField MT1901;
  private XRiTextField MT2001;
  private XRiTextField MT2101;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private XRiTextField MT2201;
  private XRiTextField MT2301;
  private XRiTextField MT2401;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private XRiTextField MT2501;
  private XRiTextField MT2601;
  private XRiTextField MT2701;
  private JLabel label7;
  private JLabel label8;
  private JLabel label10;
  private XRiTextField MT2801;
  private XRiTextField MT2901;
  private XRiTextField MT3001;
  private JLabel label11;
  private JLabel label12;
  private JLabel label13;
  private XRiTextField MT3101;
  private XRiTextField MT3201;
  private XRiTextField MT3401;
  private XRiTextField MT3501;
  private XRiTextField MT3301;
  private JLabel label14;
  private JLabel label15;
  private JLabel label16;
  private JLabel label20;
  private JLabel label21;
  private XRiTextField WTT31R;
  private XRiTextField WTT35R;
  private JLabel label17;
  private JLabel label22;
  private JLabel label23;
  private JLabel label18;
  private JLabel label24;
  private JLabel label25;
  private JLabel label27;
  private JLabel label19;
  private JLabel label28;
  private JLabel label29;
  private JLabel label30;
  private JLabel label33;
  private JLabel label34;
  private JLabel label35;
  private JLabel label26;
  private XRiTextField FLD001;
  private JLabel OBJ_48_OBJ_49;
  // JFormDesigner - End of variables declaration  //GEN-END:variables


}
