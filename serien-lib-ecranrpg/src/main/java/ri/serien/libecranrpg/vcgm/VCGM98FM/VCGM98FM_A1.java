
package ri.serien.libecranrpg.vcgm.VCGM98FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM98FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM98FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    DEMOPE.setValeurs("OU", DEMOPE_GRP);
    DEMOPE_ET.setValeurs("ET");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_69_OBJ_69.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA1@")).trim());
    OBJ_70_OBJ_70.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA2@")).trim());
    OBJ_71_OBJ_71.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA3@")).trim());
    OBJ_72_OBJ_72.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA4@")).trim());
    OBJ_73_OBJ_73.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA5@")).trim());
    OBJ_74_OBJ_74.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA6@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
    
    

    
    p_bpresentation.setCodeEtablissement(DEMETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(DEMETB.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (DEMOPE_OBJ_37.isSelected())
    // lexique.HostFieldPutData("DEMOPE", 0, "ET");
    // if (DEMOPE1.isSelected())
    // lexique.HostFieldPutData("DEMOPE", 0, "OU");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_20_OBJ_20 = new JLabel();
    DEMETB = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel3 = new JPanel();
    OBJ_56_OBJ_56 = new JLabel();
    OBJ_59_OBJ_59 = new JLabel();
    OBJ_61_OBJ_61 = new JLabel();
    OBJ_62_OBJ_62 = new JLabel();
    OBJ_63_OBJ_63 = new JLabel();
    OBJ_64_OBJ_64 = new JLabel();
    OBJ_65_OBJ_65 = new JLabel();
    OBJ_68_OBJ_68 = new JLabel();
    OBJ_66_OBJ_66 = new JLabel();
    DEMDAT = new XRiTextField();
    OBJ_69_OBJ_69 = new RiZoneSortie();
    OBJ_70_OBJ_70 = new RiZoneSortie();
    OBJ_71_OBJ_71 = new RiZoneSortie();
    OBJ_72_OBJ_72 = new RiZoneSortie();
    OBJ_73_OBJ_73 = new RiZoneSortie();
    OBJ_74_OBJ_74 = new RiZoneSortie();
    DEMAA1 = new XRiTextField();
    DEMAA2 = new XRiTextField();
    DEMAA3 = new XRiTextField();
    DEMAA4 = new XRiTextField();
    DEMAA5 = new XRiTextField();
    DEMAA6 = new XRiTextField();
    DEMOPE = new XRiRadioButton();
    DEMREP = new XRiTextField();
    DEMOPE_ET = new XRiRadioButton();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    DEMOPE_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Affichage d'analyse par axe");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_20_OBJ_20 ----
          OBJ_20_OBJ_20.setText("Soci\u00e9t\u00e9");
          OBJ_20_OBJ_20.setName("OBJ_20_OBJ_20");

          //---- DEMETB ----
          DEMETB.setComponentPopupMenu(BTD);
          DEMETB.setName("DEMETB");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_20_OBJ_20, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addComponent(DEMETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_20_OBJ_20, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(DEMETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(600, 420));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder("Option d'analyse"));
            panel3.setForeground(Color.darkGray);
            panel3.setOpaque(false);
            panel3.setName("panel3");

            //---- OBJ_56_OBJ_56 ----
            OBJ_56_OBJ_56.setText("Date d'analyse");
            OBJ_56_OBJ_56.setName("OBJ_56_OBJ_56");

            //---- OBJ_59_OBJ_59 ----
            OBJ_59_OBJ_59.setText("Axes d'analyse 1");
            OBJ_59_OBJ_59.setName("OBJ_59_OBJ_59");

            //---- OBJ_61_OBJ_61 ----
            OBJ_61_OBJ_61.setText("Axes d'analyse 2");
            OBJ_61_OBJ_61.setName("OBJ_61_OBJ_61");

            //---- OBJ_62_OBJ_62 ----
            OBJ_62_OBJ_62.setText("Axes d'analyse 3");
            OBJ_62_OBJ_62.setName("OBJ_62_OBJ_62");

            //---- OBJ_63_OBJ_63 ----
            OBJ_63_OBJ_63.setText("Axes d'analyse 4");
            OBJ_63_OBJ_63.setName("OBJ_63_OBJ_63");

            //---- OBJ_64_OBJ_64 ----
            OBJ_64_OBJ_64.setText("Axes d'analyse 5");
            OBJ_64_OBJ_64.setName("OBJ_64_OBJ_64");

            //---- OBJ_65_OBJ_65 ----
            OBJ_65_OBJ_65.setText("Axes d'analyse 6");
            OBJ_65_OBJ_65.setName("OBJ_65_OBJ_65");

            //---- OBJ_68_OBJ_68 ----
            OBJ_68_OBJ_68.setText("Date de reprise");
            OBJ_68_OBJ_68.setName("OBJ_68_OBJ_68");

            //---- OBJ_66_OBJ_66 ----
            OBJ_66_OBJ_66.setText("Relation entre axes");
            OBJ_66_OBJ_66.setName("OBJ_66_OBJ_66");

            //---- DEMDAT ----
            DEMDAT.setComponentPopupMenu(BTD);
            DEMDAT.setName("DEMDAT");

            //---- OBJ_69_OBJ_69 ----
            OBJ_69_OBJ_69.setText("@LIBAA1@");
            OBJ_69_OBJ_69.setName("OBJ_69_OBJ_69");

            //---- OBJ_70_OBJ_70 ----
            OBJ_70_OBJ_70.setText("@LIBAA2@");
            OBJ_70_OBJ_70.setName("OBJ_70_OBJ_70");

            //---- OBJ_71_OBJ_71 ----
            OBJ_71_OBJ_71.setText("@LIBAA3@");
            OBJ_71_OBJ_71.setName("OBJ_71_OBJ_71");

            //---- OBJ_72_OBJ_72 ----
            OBJ_72_OBJ_72.setText("@LIBAA4@");
            OBJ_72_OBJ_72.setName("OBJ_72_OBJ_72");

            //---- OBJ_73_OBJ_73 ----
            OBJ_73_OBJ_73.setText("@LIBAA5@");
            OBJ_73_OBJ_73.setName("OBJ_73_OBJ_73");

            //---- OBJ_74_OBJ_74 ----
            OBJ_74_OBJ_74.setText("@LIBAA6@");
            OBJ_74_OBJ_74.setName("OBJ_74_OBJ_74");

            //---- DEMAA1 ----
            DEMAA1.setComponentPopupMenu(BTD);
            DEMAA1.setName("DEMAA1");

            //---- DEMAA2 ----
            DEMAA2.setComponentPopupMenu(BTD);
            DEMAA2.setName("DEMAA2");

            //---- DEMAA3 ----
            DEMAA3.setComponentPopupMenu(BTD);
            DEMAA3.setName("DEMAA3");

            //---- DEMAA4 ----
            DEMAA4.setComponentPopupMenu(BTD);
            DEMAA4.setName("DEMAA4");

            //---- DEMAA5 ----
            DEMAA5.setComponentPopupMenu(BTD);
            DEMAA5.setName("DEMAA5");

            //---- DEMAA6 ----
            DEMAA6.setComponentPopupMenu(BTD);
            DEMAA6.setName("DEMAA6");

            //---- DEMOPE ----
            DEMOPE.setText("Ou");
            DEMOPE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DEMOPE.setName("DEMOPE");

            //---- DEMREP ----
            DEMREP.setComponentPopupMenu(BTD);
            DEMREP.setName("DEMREP");

            //---- DEMOPE_ET ----
            DEMOPE_ET.setText("Et");
            DEMOPE_ET.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DEMOPE_ET.setName("DEMOPE_ET");

            GroupLayout panel3Layout = new GroupLayout(panel3);
            panel3.setLayout(panel3Layout);
            panel3Layout.setHorizontalGroup(
              panel3Layout.createParallelGroup()
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(29, 29, 29)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addComponent(OBJ_56_OBJ_56, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
                      .addGap(56, 56, 56)
                      .addComponent(DEMDAT, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addComponent(OBJ_59_OBJ_59, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
                      .addGap(56, 56, 56)
                      .addComponent(DEMAA1, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_69_OBJ_69, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addComponent(OBJ_61_OBJ_61, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
                      .addGap(56, 56, 56)
                      .addComponent(DEMAA2, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_70_OBJ_70, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addComponent(OBJ_62_OBJ_62, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
                      .addGap(56, 56, 56)
                      .addComponent(DEMAA3, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_71_OBJ_71, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addComponent(OBJ_63_OBJ_63, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
                      .addGap(56, 56, 56)
                      .addComponent(DEMAA4, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_72_OBJ_72, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addComponent(OBJ_64_OBJ_64, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
                      .addGap(56, 56, 56)
                      .addComponent(DEMAA5, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_73_OBJ_73, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addComponent(OBJ_65_OBJ_65, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
                      .addGap(56, 56, 56)
                      .addComponent(DEMAA6, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_74_OBJ_74, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addComponent(OBJ_68_OBJ_68, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
                      .addGap(56, 56, 56)
                      .addComponent(DEMREP, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addComponent(OBJ_66_OBJ_66, GroupLayout.PREFERRED_SIZE, 129, GroupLayout.PREFERRED_SIZE)
                      .addGap(41, 41, 41)
                      .addComponent(DEMOPE, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
                      .addGap(13, 13, 13)
                      .addComponent(DEMOPE_ET, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE))))
            );
            panel3Layout.setVerticalGroup(
              panel3Layout.createParallelGroup()
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(17, 17, 17)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_56_OBJ_56, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(DEMDAT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(22, 22, 22)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_59_OBJ_59, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(DEMAA1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_69_OBJ_69, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(1, 1, 1)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_61_OBJ_61, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(DEMAA2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_70_OBJ_70, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(1, 1, 1)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_62_OBJ_62, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(DEMAA3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_71_OBJ_71, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(1, 1, 1)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_63_OBJ_63, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(DEMAA4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_72_OBJ_72, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(1, 1, 1)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_64_OBJ_64, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(DEMAA5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_73_OBJ_73, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(1, 1, 1)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_65_OBJ_65, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(DEMAA6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_74_OBJ_74, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(22, 22, 22)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_68_OBJ_68, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(DEMREP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(12, 12, 12)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_66_OBJ_66, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                    .addComponent(DEMOPE, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DEMOPE_ET, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel3, GroupLayout.DEFAULT_SIZE, 574, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel3, GroupLayout.DEFAULT_SIZE, 392, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Choix possibles");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }

    //---- DEMOPE_GRP ----
    DEMOPE_GRP.add(DEMOPE);
    DEMOPE_GRP.add(DEMOPE_ET);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_20_OBJ_20;
  private XRiTextField DEMETB;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel3;
  private JLabel OBJ_56_OBJ_56;
  private JLabel OBJ_59_OBJ_59;
  private JLabel OBJ_61_OBJ_61;
  private JLabel OBJ_62_OBJ_62;
  private JLabel OBJ_63_OBJ_63;
  private JLabel OBJ_64_OBJ_64;
  private JLabel OBJ_65_OBJ_65;
  private JLabel OBJ_68_OBJ_68;
  private JLabel OBJ_66_OBJ_66;
  private XRiTextField DEMDAT;
  private RiZoneSortie OBJ_69_OBJ_69;
  private RiZoneSortie OBJ_70_OBJ_70;
  private RiZoneSortie OBJ_71_OBJ_71;
  private RiZoneSortie OBJ_72_OBJ_72;
  private RiZoneSortie OBJ_73_OBJ_73;
  private RiZoneSortie OBJ_74_OBJ_74;
  private XRiTextField DEMAA1;
  private XRiTextField DEMAA2;
  private XRiTextField DEMAA3;
  private XRiTextField DEMAA4;
  private XRiTextField DEMAA5;
  private XRiTextField DEMAA6;
  private XRiRadioButton DEMOPE;
  private XRiTextField DEMREP;
  private XRiRadioButton DEMOPE_ET;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_10;
  private ButtonGroup DEMOPE_GRP;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
