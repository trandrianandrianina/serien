
package ri.serien.libecranrpg.vcgm.VCGM10FM;
// Nom Fichier: pop_VCGM05FM_FMTERR_367.java

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class VCGM10FM_F1 extends SNPanelEcranRPG implements ioFrame {
  
   
  private ArrayList<String> listeOptions = new ArrayList<String>();
  private ArrayList<String> listeLibellesOptions = new ArrayList<String>();
  
  public VCGM10FM_F1(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    this.setBackground(Constantes.COULEUR_F1);
    p_Erreur.setBackground(Constantes.COULEUR_ERREURS);
    
    gererLeClavier();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    OBJ_5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V05F@")).trim());
    OBJ_14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    OBJ_9_OBJ_10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F/0,17^@")).trim());
    bt_inc1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F/19,3^@")).trim());
    bt_inc2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F/23,3^@")).trim());
    bt_inc3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F/27,3^@")).trim());
    bt_inc4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F/31,3^@")).trim());
    bt_opt1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F/11,3^@")).trim());
    bt_opt2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F/15,3^@")).trim());
    bt_opt3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F/19,3^@")).trim());
    bt_opt4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F/23,3^@")).trim());
    ERR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Erreur(s) : @V03F@")).trim());
  }
  
  @Override
  public void setData() {
    String chaine = "";
    
    boolean isErreur = false;
    boolean isConf = false;
    boolean isIncorrect = false;
    boolean isOption = false;
    boolean isEntree = false;
    
    super.setData();
    
    
    chaine = lexique.HostFieldGetData("V02F").trim();
    
    // affichage du bon panel ++++++++++++++++++++
    
    // MESSAGE
    if (chaine.equals("OKJ")) {
      isEntree = true;
      this.setTitle("Etat du folio");
      this.setPreferredSize(new Dimension(340, 105));
      bt_ENTER.setIcon(lexique.chargerImage("images/OK_p.png", true));
    }
    
    // LES ERREURS
    if (chaine.equals("ERR")) {
      isErreur = true;
      // Bouton par défaut
      if (lexique.HostFieldGetData("V06F").trim().equals("NON")) {
        setDefaultButton(ANN2);
      }
      else {
        setDefaultButton(OK2);
      }
      this.setPreferredSize(new Dimension(340, 190));
      this.setTitle("Validation");
      OK2.setIcon(lexique.chargerImage("images/OK_p.png", true));
      ANN2.setIcon(lexique.chargerImage("images/retour_p.png", true));
      Exit2.setIcon(lexique.chargerImage("images/fin_p.png", true));
    }
    
    if (chaine.trim().equals("")) {
      isConf = true;
      this.setTitle("Attention");
      this.setPreferredSize(new Dimension(340, 150));
      setDefaultButton(ANN);
      OK.setText("OUI");
      ANN.setText("NON");
      OK.setIcon(lexique.chargerImage("images/OK_p.png", true));
      ANN.setIcon(lexique.chargerImage("images/retour_p.png", true));
    }
    
    // DEMANDE CONF
    else if (chaine.equals("O.K") || chaine.equals("DAT")) {
      isConf = true;
      // Bouton par défaut
      if (lexique.HostFieldGetData("V06F").trim().equals("NON")) {
        setDefaultButton(ANN);
      }
      else {
        setDefaultButton(OK);
      }
      this.setPreferredSize(new Dimension(340, 150));
      this.setTitle("Confirmation");
      OK.setIcon(lexique.chargerImage("images/OK_p.png", true));
      ANN.setIcon(lexique.chargerImage("images/retour_p.png", true));
    }
    // LES OPTIONS
    else if (chaine.equals("011")) {
      this.setPreferredSize(new Dimension(340, 190));
      this.setTitle("Confirmation");
      
      // INCORRECT
      if (lexique.isTrue("61")) {
        isIncorrect = true;
        this.setPreferredSize(new Dimension(340, 190));
        initListeOptions();
        JButton[] boutons = { bt_inc1, bt_inc2, bt_inc3, bt_inc4 };
        for (int i = 0; i < boutons.length; i++) {
          boutons[i].setVisible(false);
        }
        for (int i = 0; i < listeOptions.size(); i++) {
          if (!listeOptions.get(i).trim().equalsIgnoreCase("")) {
            boutons[i].setText(listeLibellesOptions.get(i));
            boutons[i].setVisible(true);
          }
        }
      }
      // OPTIONS
      else {
        isOption = true;
        this.setPreferredSize(new Dimension(340, 190));
        initListeOptions();
        JButton[] boutons = { bt_opt1, bt_opt2, bt_opt3, bt_opt4 };
        for (int i = 0; i < boutons.length; i++) {
          boutons[i].setVisible(false);
        }
        for (int i = 0; i < listeOptions.size(); i++) {
          if (!listeOptions.get(i).trim().equalsIgnoreCase("") || !listeOptions.get(i).equalsIgnoreCase(null)) {
            boutons[i].setText(listeLibellesOptions.get(i));
            boutons[i].setVisible(true);
          }
        }
      }
      
    }
    // APPUI SUR ENTREE
    else if (chaine.equals("OKK")) {
      isEntree = true;
      this.setPreferredSize(new Dimension(340, 105));
      bt_ENTER.setIcon(lexique.chargerImage("images/OK_p.png", true));
    }
    
    // else
    
    p_Erreur.setVisible(isErreur);
    p_DemandeConf.setVisible(isConf);
    p_Option.setVisible(isOption);
    p_AppuyerEntree.setVisible(isEntree);
    p_incorrect.setVisible(isIncorrect);
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void initListeOptions() {
    listeOptions.clear();
    listeLibellesOptions.clear();
    
    String opt = lexique.HostFieldGetData("V03F").substring(11);
    String[] lsto = opt.split(" ");
    for (int i = 0; i < lsto.length; i++) {
      if (!lsto[i].trim().equalsIgnoreCase("")) {
        listeOptions.add(lsto[i]);
      }
    }
    for (int i = 0; i < listeOptions.size(); i++) {
      if (listeOptions.get(i).equalsIgnoreCase("ATT")) {
        listeLibellesOptions.add("Mettre le folio en attente");
      }
      if (listeOptions.get(i).equalsIgnoreCase("ANN")) {
        listeLibellesOptions.add("Annuler le folio");
      }
      if (listeOptions.get(i).equalsIgnoreCase("HOM")) {
        listeLibellesOptions.add("Entrer le folio en comptabilité");
      }
      if (listeOptions.get(i).equalsIgnoreCase("COR")) {
        listeLibellesOptions.add("Corriger le folio");
      }
    }
  }
  
  private void bt_opt2ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, listeOptions.get(1));
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void bt_opt3ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, listeOptions.get(2));
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void bt_opt4ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, listeOptions.get(3));
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void bt_opt1ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, listeOptions.get(0));
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void bt_inc1ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, listeOptions.get(0));
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void bt_inc2ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, listeOptions.get(1));
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void bt_inc3ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, listeOptions.get(2));
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void bt_inc4ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, listeOptions.get(3));
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OKActionPerformed() {
    lexique.HostFieldPutData("V06F", 0, "OUI");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void ANNActionPerformed() {
    lexique.HostFieldPutData("V06F", 0, "NON");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void ExitActionPerformed() {
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void ERRActionPerformed() {
    lexique.HostScreenSendKey(this, "F1", false);
  }
  
  private void gererLeClavier() {
    Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {
      @Override
      public void eventDispatched(AWTEvent event) {
        KeyEvent key = (KeyEvent) event;
        if (key.getID() == KeyEvent.KEY_PRESSED) {
          if (key.getKeyCode() == KeyEvent.VK_ENTER) {
            if (OK2.hasFocus() || OK.hasFocus()) {
              /*setDefaultButton(OK2);
              OK2.doClick();*/
              lexique.HostFieldPutData("V06F", 0, "OUI");
              lexique.HostScreenSendKey(lexique.getPanel(), "ENTER");
            }
            else if (ERR.hasFocus()) {
              lexique.HostScreenSendKey(lexique.getPanel(), "F1", true);
            }
            else if (Exit2.hasFocus()) {
              lexique.HostScreenSendKey(lexique.getPanel(), "F3", true);
            }
            else if (bt_ENTER.hasFocus()) {
              lexique.HostScreenSendKey(lexique.getPanel(), "ENTER");
            }
            else if (ANN.hasFocus()) {
              lexique.HostFieldPutData("V06F", 0, "NON");
              lexique.HostScreenSendKey(lexique.getPanel(), "ENTER");
            }
          }
        }
      }
    }, AWTEvent.KEY_EVENT_MASK);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_AppuyerEntree = new JPanel();
    bt_ENTER = new JButton();
    OBJ_18 = new JLabel();
    p_DemandeConf = new JPanel();
    OBJ_5 = new JLabel();
    OBJ_14 = new JLabel();
    OK = new JButton();
    ANN = new JButton();
    p_incorrect = new JPanel();
    OBJ_9_OBJ_10 = new JLabel();
    panel2 = new JPanel();
    bt_inc1 = new JButton();
    bt_inc2 = new JButton();
    bt_inc3 = new JButton();
    bt_inc4 = new JButton();
    p_Option = new JPanel();
    OBJ_9_OBJ_9 = new JLabel();
    panel1 = new JPanel();
    bt_opt1 = new JButton();
    bt_opt2 = new JButton();
    bt_opt3 = new JButton();
    bt_opt4 = new JButton();
    p_Erreur = new JPanel();
    OK2 = new JButton();
    ANN2 = new JButton();
    Exit2 = new JButton();
    ERR = new JButton();

    //======== this ========
    setPreferredSize(new Dimension(340, 190));
    setBackground(new Color(90, 90, 90));
    setForeground(Color.black);
    setName("this");
    setLayout(null);

    //======== p_AppuyerEntree ========
    {
      p_AppuyerEntree.setBackground(new Color(90, 90, 90));
      p_AppuyerEntree.setOpaque(false);
      p_AppuyerEntree.setName("p_AppuyerEntree");
      p_AppuyerEntree.setLayout(null);

      //---- bt_ENTER ----
      bt_ENTER.setText("Valider");
      bt_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_ENTER.setPreferredSize(new Dimension(110, 19));
      bt_ENTER.setMinimumSize(new Dimension(110, 1));
      bt_ENTER.setFont(bt_ENTER.getFont().deriveFont(bt_ENTER.getFont().getStyle() | Font.BOLD, bt_ENTER.getFont().getSize() + 2f));
      bt_ENTER.setIconTextGap(25);
      bt_ENTER.setHorizontalAlignment(SwingConstants.LEADING);
      bt_ENTER.setName("bt_ENTER");
      bt_ENTER.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_ENTERActionPerformed();
        }
      });
      p_AppuyerEntree.add(bt_ENTER);
      bt_ENTER.setBounds(75, 50, 190, 40);

      //---- OBJ_18 ----
      OBJ_18.setText("@V03F@");
      OBJ_18.setHorizontalAlignment(SwingConstants.CENTER);
      OBJ_18.setRequestFocusEnabled(false);
      OBJ_18.setFont(new Font("sansserif", Font.PLAIN, 16));
      OBJ_18.setForeground(Color.white);
      OBJ_18.setName("OBJ_18");
      p_AppuyerEntree.add(OBJ_18);
      OBJ_18.setBounds(0, 15, 340, 25);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < p_AppuyerEntree.getComponentCount(); i++) {
          Rectangle bounds = p_AppuyerEntree.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = p_AppuyerEntree.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        p_AppuyerEntree.setMinimumSize(preferredSize);
        p_AppuyerEntree.setPreferredSize(preferredSize);
      }
    }
    add(p_AppuyerEntree);
    p_AppuyerEntree.setBounds(0, 0, p_AppuyerEntree.getPreferredSize().width, 105);

    //======== p_DemandeConf ========
    {
      p_DemandeConf.setPreferredSize(new Dimension(340, 140));
      p_DemandeConf.setBackground(new Color(90, 90, 90));
      p_DemandeConf.setOpaque(false);
      p_DemandeConf.setName("p_DemandeConf");
      p_DemandeConf.setLayout(null);

      //---- OBJ_5 ----
      OBJ_5.setText("@V05F@");
      OBJ_5.setHorizontalAlignment(SwingConstants.CENTER);
      OBJ_5.setRequestFocusEnabled(false);
      OBJ_5.setFont(new Font("sansserif", Font.PLAIN, 16));
      OBJ_5.setForeground(Color.white);
      OBJ_5.setName("OBJ_5");
      p_DemandeConf.add(OBJ_5);
      OBJ_5.setBounds(0, 30, 340, 25);

      //---- OBJ_14 ----
      OBJ_14.setText("@V03F@");
      OBJ_14.setHorizontalAlignment(SwingConstants.CENTER);
      OBJ_14.setRequestFocusEnabled(false);
      OBJ_14.setForeground(Color.white);
      OBJ_14.setFont(new Font("sansserif", Font.PLAIN, 15));
      OBJ_14.setName("OBJ_14");
      p_DemandeConf.add(OBJ_14);
      OBJ_14.setBounds(0, 5, 340, 25);

      //---- OK ----
      OK.setText("Valider");
      OK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OK.setPreferredSize(new Dimension(110, 19));
      OK.setMinimumSize(new Dimension(110, 1));
      OK.setFont(OK.getFont().deriveFont(OK.getFont().getStyle() | Font.BOLD, OK.getFont().getSize() + 2f));
      OK.setIconTextGap(25);
      OK.setHorizontalAlignment(SwingConstants.LEADING);
      OK.setName("OK");
      OK.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OKActionPerformed();
        }
      });
      p_DemandeConf.add(OK);
      OK.setBounds(75, 60, 190, 40);

      //---- ANN ----
      ANN.setText(" Retour");
      ANN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      ANN.setPreferredSize(new Dimension(120, 19));
      ANN.setMinimumSize(new Dimension(120, 1));
      ANN.setFont(new Font("sansserif", Font.BOLD, 14));
      ANN.setIconTextGap(25);
      ANN.setHorizontalAlignment(SwingConstants.LEADING);
      ANN.setName("ANN");
      ANN.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          ANNActionPerformed();
        }
      });
      p_DemandeConf.add(ANN);
      ANN.setBounds(75, 100, 190, 40);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < p_DemandeConf.getComponentCount(); i++) {
          Rectangle bounds = p_DemandeConf.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = p_DemandeConf.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        p_DemandeConf.setMinimumSize(preferredSize);
        p_DemandeConf.setPreferredSize(preferredSize);
      }
    }
    add(p_DemandeConf);
    p_DemandeConf.setBounds(0, 0, p_DemandeConf.getPreferredSize().width, 150);

    //======== p_incorrect ========
    {
      p_incorrect.setBackground(new Color(90, 90, 90));
      p_incorrect.setPreferredSize(new Dimension(340, 140));
      p_incorrect.setOpaque(false);
      p_incorrect.setName("p_incorrect");
      p_incorrect.setLayout(null);

      //---- OBJ_9_OBJ_10 ----
      OBJ_9_OBJ_10.setHorizontalAlignment(SwingConstants.CENTER);
      OBJ_9_OBJ_10.setText("@V03F/0,17^@");
      OBJ_9_OBJ_10.setFont(new Font("sansserif", Font.PLAIN, 16));
      OBJ_9_OBJ_10.setForeground(Color.white);
      OBJ_9_OBJ_10.setName("OBJ_9_OBJ_10");
      p_incorrect.add(OBJ_9_OBJ_10);
      OBJ_9_OBJ_10.setBounds(0, 5, 340, 30);

      //======== panel2 ========
      {
        panel2.setOpaque(false);
        panel2.setName("panel2");
        panel2.setLayout(new VerticalLayout());

        //---- bt_inc1 ----
        bt_inc1.setText("@V03F/19,3^@");
        bt_inc1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_inc1.setFont(bt_inc1.getFont().deriveFont(bt_inc1.getFont().getStyle() | Font.BOLD, bt_inc1.getFont().getSize() + 2f));
        bt_inc1.setName("bt_inc1");
        bt_inc1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_inc1ActionPerformed(e);
          }
        });
        panel2.add(bt_inc1);

        //---- bt_inc2 ----
        bt_inc2.setText("@V03F/23,3^@");
        bt_inc2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_inc2.setFont(bt_inc2.getFont().deriveFont(bt_inc2.getFont().getStyle() | Font.BOLD, bt_inc2.getFont().getSize() + 2f));
        bt_inc2.setName("bt_inc2");
        bt_inc2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_inc2ActionPerformed(e);
          }
        });
        panel2.add(bt_inc2);

        //---- bt_inc3 ----
        bt_inc3.setText("@V03F/27,3^@");
        bt_inc3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_inc3.setFont(bt_inc3.getFont().deriveFont(bt_inc3.getFont().getStyle() | Font.BOLD, bt_inc3.getFont().getSize() + 2f));
        bt_inc3.setName("bt_inc3");
        bt_inc3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_inc3ActionPerformed(e);
          }
        });
        panel2.add(bt_inc3);

        //---- bt_inc4 ----
        bt_inc4.setText("@V03F/31,3^@");
        bt_inc4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_inc4.setFont(bt_inc4.getFont().deriveFont(bt_inc4.getFont().getStyle() | Font.BOLD, bt_inc4.getFont().getSize() + 2f));
        bt_inc4.setName("bt_inc4");
        bt_inc4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_inc4ActionPerformed(e);
          }
        });
        panel2.add(bt_inc4);
      }
      p_incorrect.add(panel2);
      panel2.setBounds(20, 50, 300, 125);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < p_incorrect.getComponentCount(); i++) {
          Rectangle bounds = p_incorrect.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = p_incorrect.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        p_incorrect.setMinimumSize(preferredSize);
        p_incorrect.setPreferredSize(preferredSize);
      }
    }
    add(p_incorrect);
    p_incorrect.setBounds(0, 0, p_incorrect.getPreferredSize().width, 190);

    //======== p_Option ========
    {
      p_Option.setBackground(new Color(90, 90, 90));
      p_Option.setPreferredSize(new Dimension(340, 140));
      p_Option.setOpaque(false);
      p_Option.setName("p_Option");
      p_Option.setLayout(null);

      //---- OBJ_9_OBJ_9 ----
      OBJ_9_OBJ_9.setText("OPTIONS");
      OBJ_9_OBJ_9.setHorizontalAlignment(SwingConstants.CENTER);
      OBJ_9_OBJ_9.setFont(new Font("sansserif", Font.PLAIN, 16));
      OBJ_9_OBJ_9.setForeground(Color.white);
      OBJ_9_OBJ_9.setName("OBJ_9_OBJ_9");
      p_Option.add(OBJ_9_OBJ_9);
      OBJ_9_OBJ_9.setBounds(0, 5, 340, 25);

      //======== panel1 ========
      {
        panel1.setOpaque(false);
        panel1.setName("panel1");
        panel1.setLayout(new VerticalLayout());

        //---- bt_opt1 ----
        bt_opt1.setText("@V03F/11,3^@");
        bt_opt1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_opt1.setFont(bt_opt1.getFont().deriveFont(bt_opt1.getFont().getStyle() | Font.BOLD, bt_opt1.getFont().getSize() + 2f));
        bt_opt1.setName("bt_opt1");
        bt_opt1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_opt1ActionPerformed(e);
          }
        });
        panel1.add(bt_opt1);

        //---- bt_opt2 ----
        bt_opt2.setText("@V03F/15,3^@");
        bt_opt2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_opt2.setFont(bt_opt2.getFont().deriveFont(bt_opt2.getFont().getStyle() | Font.BOLD, bt_opt2.getFont().getSize() + 2f));
        bt_opt2.setName("bt_opt2");
        bt_opt2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_opt2ActionPerformed(e);
          }
        });
        panel1.add(bt_opt2);

        //---- bt_opt3 ----
        bt_opt3.setText("@V03F/19,3^@");
        bt_opt3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_opt3.setFont(bt_opt3.getFont().deriveFont(bt_opt3.getFont().getStyle() | Font.BOLD, bt_opt3.getFont().getSize() + 2f));
        bt_opt3.setName("bt_opt3");
        bt_opt3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_opt3ActionPerformed(e);
          }
        });
        panel1.add(bt_opt3);

        //---- bt_opt4 ----
        bt_opt4.setText("@V03F/23,3^@");
        bt_opt4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_opt4.setFont(bt_opt4.getFont().deriveFont(bt_opt4.getFont().getStyle() | Font.BOLD, bt_opt4.getFont().getSize() + 2f));
        bt_opt4.setName("bt_opt4");
        bt_opt4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_opt4ActionPerformed(e);
          }
        });
        panel1.add(bt_opt4);
      }
      p_Option.add(panel1);
      panel1.setBounds(20, 50, 300, 125);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < p_Option.getComponentCount(); i++) {
          Rectangle bounds = p_Option.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = p_Option.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        p_Option.setMinimumSize(preferredSize);
        p_Option.setPreferredSize(preferredSize);
      }
    }
    add(p_Option);
    p_Option.setBounds(0, 0, p_Option.getPreferredSize().width, 190);

    //======== p_Erreur ========
    {
      p_Erreur.setPreferredSize(new Dimension(340, 140));
      p_Erreur.setBackground(new Color(106, 23, 21));
      p_Erreur.setName("p_Erreur");
      p_Erreur.setLayout(null);

      //---- OK2 ----
      OK2.setText("Enregistrer erron\u00e9");
      OK2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OK2.setPreferredSize(new Dimension(110, 19));
      OK2.setMinimumSize(new Dimension(110, 1));
      OK2.setFont(OK2.getFont().deriveFont(OK2.getFont().getStyle() | Font.BOLD, OK2.getFont().getSize() + 2f));
      OK2.setIconTextGap(5);
      OK2.setIcon(null);
      OK2.setHorizontalAlignment(SwingConstants.LEADING);
      OK2.setName("OK2");
      OK2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OKActionPerformed();
        }
      });
      p_Erreur.add(OK2);
      OK2.setBounds(70, 60, 200, 40);

      //---- ANN2 ----
      ANN2.setText("Corriger la fiche");
      ANN2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      ANN2.setPreferredSize(new Dimension(120, 19));
      ANN2.setMinimumSize(new Dimension(120, 1));
      ANN2.setFont(new Font("sansserif", Font.BOLD, 14));
      ANN2.setIconTextGap(5);
      ANN2.setHorizontalAlignment(SwingConstants.LEADING);
      ANN2.setName("ANN2");
      ANN2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          ANNActionPerformed();
        }
      });
      p_Erreur.add(ANN2);
      ANN2.setBounds(70, 100, 200, 40);

      //---- Exit2 ----
      Exit2.setText("Abandonner");
      Exit2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      Exit2.setPreferredSize(new Dimension(120, 19));
      Exit2.setMinimumSize(new Dimension(120, 1));
      Exit2.setFont(new Font("sansserif", Font.BOLD, 14));
      Exit2.setIconTextGap(5);
      Exit2.setHorizontalAlignment(SwingConstants.LEADING);
      Exit2.setName("Exit2");
      Exit2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          ExitActionPerformed();
        }
      });
      p_Erreur.add(Exit2);
      Exit2.setBounds(70, 140, 200, 40);

      //---- ERR ----
      ERR.setText("Erreur(s) : @V03F@");
      ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      ERR.setPreferredSize(new Dimension(120, 19));
      ERR.setMinimumSize(new Dimension(120, 1));
      ERR.setFont(new Font("sansserif", Font.BOLD, 14));
      ERR.setIconTextGap(0);
      ERR.setToolTipText("Cliquez pour le d\u00e9tail des erreurs");
      ERR.setBackground(new Color(106, 23, 21));
      ERR.setForeground(Color.white);
      ERR.setName("ERR");
      ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          ERRActionPerformed();
        }
      });
      p_Erreur.add(ERR);
      ERR.setBounds(0, 5, 340, 40);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < p_Erreur.getComponentCount(); i++) {
          Rectangle bounds = p_Erreur.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = p_Erreur.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        p_Erreur.setMinimumSize(preferredSize);
        p_Erreur.setPreferredSize(preferredSize);
      }
    }
    add(p_Erreur);
    p_Erreur.setBounds(0, 0, p_Erreur.getPreferredSize().width, 190);

    {
      // compute preferred size
      Dimension preferredSize = new Dimension();
      for(int i = 0; i < getComponentCount(); i++) {
        Rectangle bounds = getComponent(i).getBounds();
        preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
        preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
      }
      Insets insets = getInsets();
      preferredSize.width += insets.right;
      preferredSize.height += insets.bottom;
      setMinimumSize(preferredSize);
      setPreferredSize(preferredSize);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_AppuyerEntree;
  private JButton bt_ENTER;
  private JLabel OBJ_18;
  private JPanel p_DemandeConf;
  private JLabel OBJ_5;
  private JLabel OBJ_14;
  private JButton OK;
  private JButton ANN;
  private JPanel p_incorrect;
  private JLabel OBJ_9_OBJ_10;
  private JPanel panel2;
  private JButton bt_inc1;
  private JButton bt_inc2;
  private JButton bt_inc3;
  private JButton bt_inc4;
  private JPanel p_Option;
  private JLabel OBJ_9_OBJ_9;
  private JPanel panel1;
  private JButton bt_opt1;
  private JButton bt_opt2;
  private JButton bt_opt3;
  private JButton bt_opt4;
  private JPanel p_Erreur;
  private JButton OK2;
  private JButton ANN2;
  private JButton Exit2;
  private JButton ERR;
  // JFormDesigner - End of variables declaration  //GEN-END:variables



}
