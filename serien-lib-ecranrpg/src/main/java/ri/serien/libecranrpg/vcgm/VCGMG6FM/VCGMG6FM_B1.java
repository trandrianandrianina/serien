
package ri.serien.libecranrpg.vcgm.VCGMG6FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGMG6FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  private final static String BOUTON_VALIDER_CAISSE = "Valider la caisse";
  
  public VCGMG6FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Bouton par défaut
    
    // Ajout
    initDiverses();
    
    sNBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    sNBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    sNBarreBouton.ajouterBouton(BOUTON_VALIDER_CAISSE, 'V', true);
    sNBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _WTX01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    // menus_haut.setVisible(lexique.isTrue("N60"));
    if (lexique.isTrue("60")) {
      ECART.setForeground(Color.red);
    }
    else {
      ECART.setForeground(Color.black);
    }
    
    LIE01.setVisible(!lexique.HostFieldGetData("LIE01").trim().equals(""));
    LIE02.setVisible(!lexique.HostFieldGetData("LIE02").trim().equals(""));
    LIE03.setVisible(!lexique.HostFieldGetData("LIE03").trim().equals(""));
    LIE04.setVisible(!lexique.HostFieldGetData("LIE04").trim().equals(""));
    LIE05.setVisible(!lexique.HostFieldGetData("LIE05").trim().equals(""));
    LIE06.setVisible(!lexique.HostFieldGetData("LIE06").trim().equals(""));
    LIE07.setVisible(!lexique.HostFieldGetData("LIE07").trim().equals(""));
    LIE08.setVisible(!lexique.HostFieldGetData("LIE08").trim().equals(""));
    LIE09.setVisible(!lexique.HostFieldGetData("LIE09").trim().equals(""));
    LIE10.setVisible(!lexique.HostFieldGetData("LIE10").trim().equals(""));
    LIE11.setVisible(!lexique.HostFieldGetData("LIE11").trim().equals(""));
    LIE12.setVisible(!lexique.HostFieldGetData("LIE12").trim().equals(""));
    LIE13.setVisible(!lexique.HostFieldGetData("LIE13").trim().equals(""));
    LIE14.setVisible(!lexique.HostFieldGetData("LIE14").trim().equals(""));
    LIE15.setVisible(!lexique.HostFieldGetData("LIE15").trim().equals(""));
    LIE16.setVisible(!lexique.HostFieldGetData("LIE16").trim().equals(""));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Caisse"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    sNBarreBouton = new SNBarreBouton();
    sNPanelPrincipal1 = new SNPanelContenu();
    pnTableau = new SNPanelTitre();
    lbType = new SNLabelUnite();
    lbMontant = new SNLabelUnite();
    LIE01 = new XRiTextField();
    MTE01 = new XRiTextField();
    LIE02 = new XRiTextField();
    MTE02 = new XRiTextField();
    LIE03 = new XRiTextField();
    MTE03 = new XRiTextField();
    LIE04 = new XRiTextField();
    MTE04 = new XRiTextField();
    LIE05 = new XRiTextField();
    MTE05 = new XRiTextField();
    LIE06 = new XRiTextField();
    MTE06 = new XRiTextField();
    LIE07 = new XRiTextField();
    MTE07 = new XRiTextField();
    LIE08 = new XRiTextField();
    MTE08 = new XRiTextField();
    LIE09 = new XRiTextField();
    MTE09 = new XRiTextField();
    LIE10 = new XRiTextField();
    MTE10 = new XRiTextField();
    MTE11 = new XRiTextField();
    MTE12 = new XRiTextField();
    MTE13 = new XRiTextField();
    MTE14 = new XRiTextField();
    MTE15 = new XRiTextField();
    MTE16 = new XRiTextField();
    LIE11 = new XRiTextField();
    LIE12 = new XRiTextField();
    LIE13 = new XRiTextField();
    LIE14 = new XRiTextField();
    LIE15 = new XRiTextField();
    LIE16 = new XRiTextField();
    panel3 = new JPanel();
    label4 = new JLabel();
    TOTC = new XRiTextField();
    sNLabelChamp1 = new SNLabelChamp();
    TOTCAI = new XRiTextField();
    sNLabelChamp5 = new SNLabelChamp();
    ECART = new XRiTextField();
    BT_PGUP2 = new JButton();
    BT_PGDOWN2 = new JButton();

    //======== this ========
    setMinimumSize(new Dimension(810, 730));
    setPreferredSize(new Dimension(810, 730));
    setMaximumSize(new Dimension(810, 730));
    setName("this");
    setLayout(new BorderLayout());

    //---- sNBarreBouton ----
    sNBarreBouton.setName("sNBarreBouton");
    add(sNBarreBouton, BorderLayout.SOUTH);

    //======== sNPanelPrincipal1 ========
    {
      sNPanelPrincipal1.setName("sNPanelPrincipal1");
      sNPanelPrincipal1.setLayout(new GridBagLayout());
      ((GridBagLayout)sNPanelPrincipal1.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)sNPanelPrincipal1.getLayout()).rowHeights = new int[] {0, 0};
      ((GridBagLayout)sNPanelPrincipal1.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)sNPanelPrincipal1.getLayout()).rowWeights = new double[] {1.0, 1.0E-4};

      //======== pnTableau ========
      {
        pnTableau.setAutoscrolls(true);
        pnTableau.setBackground(new Color(239, 239, 222));
        pnTableau.setName("pnTableau");
        pnTableau.setLayout(new GridBagLayout());
        ((GridBagLayout)pnTableau.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0};
        ((GridBagLayout)pnTableau.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        ((GridBagLayout)pnTableau.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnTableau.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

        //---- lbType ----
        lbType.setText("Type de pi\u00e8ce ou de billet");
        lbType.setName("lbType");
        pnTableau.add(lbType, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- lbMontant ----
        lbMontant.setText("Montant");
        lbMontant.setName("lbMontant");
        pnTableau.add(lbMontant, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE01 ----
        LIE01.setMaximumSize(new Dimension(500, 28));
        LIE01.setMinimumSize(new Dimension(500, 28));
        LIE01.setPreferredSize(new Dimension(500, 28));
        LIE01.setName("LIE01");
        pnTableau.add(LIE01, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE01 ----
        MTE01.setPreferredSize(new Dimension(100, 28));
        MTE01.setMinimumSize(new Dimension(100, 28));
        MTE01.setMaximumSize(new Dimension(100, 28));
        MTE01.setName("MTE01");
        pnTableau.add(MTE01, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE02 ----
        LIE02.setMaximumSize(new Dimension(500, 28));
        LIE02.setMinimumSize(new Dimension(500, 28));
        LIE02.setPreferredSize(new Dimension(500, 28));
        LIE02.setName("LIE02");
        pnTableau.add(LIE02, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE02 ----
        MTE02.setMaximumSize(new Dimension(100, 28));
        MTE02.setMinimumSize(new Dimension(100, 28));
        MTE02.setPreferredSize(new Dimension(100, 28));
        MTE02.setName("MTE02");
        pnTableau.add(MTE02, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE03 ----
        LIE03.setMaximumSize(new Dimension(500, 28));
        LIE03.setMinimumSize(new Dimension(500, 28));
        LIE03.setPreferredSize(new Dimension(500, 28));
        LIE03.setName("LIE03");
        pnTableau.add(LIE03, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE03 ----
        MTE03.setMaximumSize(new Dimension(100, 28));
        MTE03.setMinimumSize(new Dimension(100, 28));
        MTE03.setPreferredSize(new Dimension(100, 28));
        MTE03.setName("MTE03");
        pnTableau.add(MTE03, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE04 ----
        LIE04.setMaximumSize(new Dimension(500, 28));
        LIE04.setMinimumSize(new Dimension(500, 28));
        LIE04.setPreferredSize(new Dimension(500, 28));
        LIE04.setName("LIE04");
        pnTableau.add(LIE04, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE04 ----
        MTE04.setMaximumSize(new Dimension(100, 28));
        MTE04.setMinimumSize(new Dimension(100, 28));
        MTE04.setPreferredSize(new Dimension(100, 28));
        MTE04.setName("MTE04");
        pnTableau.add(MTE04, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE05 ----
        LIE05.setMaximumSize(new Dimension(500, 28));
        LIE05.setMinimumSize(new Dimension(500, 28));
        LIE05.setPreferredSize(new Dimension(500, 28));
        LIE05.setName("LIE05");
        pnTableau.add(LIE05, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE05 ----
        MTE05.setMaximumSize(new Dimension(100, 28));
        MTE05.setMinimumSize(new Dimension(100, 28));
        MTE05.setPreferredSize(new Dimension(100, 28));
        MTE05.setName("MTE05");
        pnTableau.add(MTE05, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE06 ----
        LIE06.setMaximumSize(new Dimension(500, 28));
        LIE06.setMinimumSize(new Dimension(500, 28));
        LIE06.setPreferredSize(new Dimension(500, 28));
        LIE06.setName("LIE06");
        pnTableau.add(LIE06, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE06 ----
        MTE06.setMaximumSize(new Dimension(100, 28));
        MTE06.setMinimumSize(new Dimension(100, 28));
        MTE06.setPreferredSize(new Dimension(100, 28));
        MTE06.setName("MTE06");
        pnTableau.add(MTE06, new GridBagConstraints(2, 6, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE07 ----
        LIE07.setMaximumSize(new Dimension(500, 28));
        LIE07.setMinimumSize(new Dimension(500, 28));
        LIE07.setPreferredSize(new Dimension(500, 28));
        LIE07.setName("LIE07");
        pnTableau.add(LIE07, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE07 ----
        MTE07.setMaximumSize(new Dimension(100, 28));
        MTE07.setMinimumSize(new Dimension(100, 28));
        MTE07.setPreferredSize(new Dimension(100, 28));
        MTE07.setName("MTE07");
        pnTableau.add(MTE07, new GridBagConstraints(2, 7, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE08 ----
        LIE08.setMaximumSize(new Dimension(500, 28));
        LIE08.setMinimumSize(new Dimension(500, 28));
        LIE08.setPreferredSize(new Dimension(500, 28));
        LIE08.setName("LIE08");
        pnTableau.add(LIE08, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE08 ----
        MTE08.setMaximumSize(new Dimension(100, 28));
        MTE08.setMinimumSize(new Dimension(100, 28));
        MTE08.setPreferredSize(new Dimension(100, 28));
        MTE08.setName("MTE08");
        pnTableau.add(MTE08, new GridBagConstraints(2, 8, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE09 ----
        LIE09.setMaximumSize(new Dimension(500, 28));
        LIE09.setMinimumSize(new Dimension(500, 28));
        LIE09.setPreferredSize(new Dimension(500, 28));
        LIE09.setName("LIE09");
        pnTableau.add(LIE09, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE09 ----
        MTE09.setMaximumSize(new Dimension(100, 28));
        MTE09.setMinimumSize(new Dimension(100, 28));
        MTE09.setPreferredSize(new Dimension(100, 28));
        MTE09.setName("MTE09");
        pnTableau.add(MTE09, new GridBagConstraints(2, 9, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE10 ----
        LIE10.setMaximumSize(new Dimension(500, 28));
        LIE10.setMinimumSize(new Dimension(500, 28));
        LIE10.setPreferredSize(new Dimension(500, 28));
        LIE10.setName("LIE10");
        pnTableau.add(LIE10, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE10 ----
        MTE10.setMaximumSize(new Dimension(100, 28));
        MTE10.setMinimumSize(new Dimension(100, 28));
        MTE10.setPreferredSize(new Dimension(100, 28));
        MTE10.setName("MTE10");
        pnTableau.add(MTE10, new GridBagConstraints(2, 10, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE11 ----
        MTE11.setMaximumSize(new Dimension(100, 28));
        MTE11.setMinimumSize(new Dimension(100, 28));
        MTE11.setPreferredSize(new Dimension(100, 28));
        MTE11.setName("MTE11");
        pnTableau.add(MTE11, new GridBagConstraints(2, 11, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE12 ----
        MTE12.setMaximumSize(new Dimension(100, 28));
        MTE12.setMinimumSize(new Dimension(100, 28));
        MTE12.setPreferredSize(new Dimension(100, 28));
        MTE12.setName("MTE12");
        pnTableau.add(MTE12, new GridBagConstraints(2, 12, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE13 ----
        MTE13.setMaximumSize(new Dimension(100, 28));
        MTE13.setMinimumSize(new Dimension(100, 28));
        MTE13.setPreferredSize(new Dimension(100, 28));
        MTE13.setName("MTE13");
        pnTableau.add(MTE13, new GridBagConstraints(2, 13, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE14 ----
        MTE14.setMaximumSize(new Dimension(100, 28));
        MTE14.setMinimumSize(new Dimension(100, 28));
        MTE14.setPreferredSize(new Dimension(100, 28));
        MTE14.setName("MTE14");
        pnTableau.add(MTE14, new GridBagConstraints(2, 14, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE15 ----
        MTE15.setMaximumSize(new Dimension(100, 28));
        MTE15.setMinimumSize(new Dimension(100, 28));
        MTE15.setPreferredSize(new Dimension(100, 28));
        MTE15.setName("MTE15");
        pnTableau.add(MTE15, new GridBagConstraints(2, 15, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- MTE16 ----
        MTE16.setMaximumSize(new Dimension(100, 28));
        MTE16.setMinimumSize(new Dimension(100, 28));
        MTE16.setPreferredSize(new Dimension(100, 28));
        MTE16.setName("MTE16");
        pnTableau.add(MTE16, new GridBagConstraints(2, 16, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE11 ----
        LIE11.setMaximumSize(new Dimension(500, 28));
        LIE11.setMinimumSize(new Dimension(500, 28));
        LIE11.setPreferredSize(new Dimension(500, 28));
        LIE11.setName("LIE11");
        pnTableau.add(LIE11, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE12 ----
        LIE12.setMaximumSize(new Dimension(500, 28));
        LIE12.setMinimumSize(new Dimension(500, 28));
        LIE12.setPreferredSize(new Dimension(500, 28));
        LIE12.setName("LIE12");
        pnTableau.add(LIE12, new GridBagConstraints(1, 12, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE13 ----
        LIE13.setMaximumSize(new Dimension(500, 28));
        LIE13.setMinimumSize(new Dimension(500, 28));
        LIE13.setPreferredSize(new Dimension(500, 28));
        LIE13.setName("LIE13");
        pnTableau.add(LIE13, new GridBagConstraints(1, 13, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE14 ----
        LIE14.setMaximumSize(new Dimension(500, 28));
        LIE14.setMinimumSize(new Dimension(500, 28));
        LIE14.setPreferredSize(new Dimension(500, 28));
        LIE14.setName("LIE14");
        pnTableau.add(LIE14, new GridBagConstraints(1, 14, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE15 ----
        LIE15.setMaximumSize(new Dimension(500, 28));
        LIE15.setMinimumSize(new Dimension(500, 28));
        LIE15.setPreferredSize(new Dimension(500, 28));
        LIE15.setName("LIE15");
        pnTableau.add(LIE15, new GridBagConstraints(1, 15, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- LIE16 ----
        LIE16.setMaximumSize(new Dimension(500, 28));
        LIE16.setMinimumSize(new Dimension(500, 28));
        LIE16.setPreferredSize(new Dimension(500, 28));
        LIE16.setName("LIE16");
        pnTableau.add(LIE16, new GridBagConstraints(1, 16, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //======== panel3 ========
        {
          panel3.setBackground(new Color(239, 239, 222));
          panel3.setName("panel3");
          panel3.setLayout(new GridBagLayout());
          ((GridBagLayout)panel3.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)panel3.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)panel3.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)panel3.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- label4 ----
          label4.setText("Total caisse");
          label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD, label4.getFont().getSize() + 3f));
          label4.setPreferredSize(new Dimension(400, 20));
          label4.setMinimumSize(new Dimension(400, 20));
          label4.setMaximumSize(new Dimension(400, 20));
          label4.setName("label4");
          panel3.add(label4, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- TOTC ----
          TOTC.setMaximumSize(new Dimension(100, 28));
          TOTC.setMinimumSize(new Dimension(100, 28));
          TOTC.setPreferredSize(new Dimension(100, 28));
          TOTC.setName("TOTC");
          panel3.add(TOTC, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnTableau.add(panel3, new GridBagConstraints(1, 18, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- sNLabelChamp1 ----
        sNLabelChamp1.setText("Saisie");
        sNLabelChamp1.setName("sNLabelChamp1");
        pnTableau.add(sNLabelChamp1, new GridBagConstraints(1, 19, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- TOTCAI ----
        TOTCAI.setMaximumSize(new Dimension(100, 28));
        TOTCAI.setMinimumSize(new Dimension(100, 28));
        TOTCAI.setPreferredSize(new Dimension(100, 28));
        TOTCAI.setName("TOTCAI");
        pnTableau.add(TOTCAI, new GridBagConstraints(2, 19, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- sNLabelChamp5 ----
        sNLabelChamp5.setText("Ecart");
        sNLabelChamp5.setName("sNLabelChamp5");
        pnTableau.add(sNLabelChamp5, new GridBagConstraints(1, 20, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 2, 2), 0, 0));

        //---- ECART ----
        ECART.setMaximumSize(new Dimension(100, 28));
        ECART.setMinimumSize(new Dimension(100, 28));
        ECART.setPreferredSize(new Dimension(100, 28));
        ECART.setName("ECART");
        pnTableau.add(ECART, new GridBagConstraints(2, 20, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 2, 2), 0, 0));
      }
      sNPanelPrincipal1.add(pnTableau, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(sNPanelPrincipal1, BorderLayout.CENTER);

    //---- BT_PGUP2 ----
    BT_PGUP2.setText("");
    BT_PGUP2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_PGUP2.setName("BT_PGUP2");

    //---- BT_PGDOWN2 ----
    BT_PGDOWN2.setText("");
    BT_PGDOWN2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_PGDOWN2.setName("BT_PGDOWN2");
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_VALIDER_CAISSE)) {
        lexique.HostScreenSendKey(this, "F2");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBarreBouton sNBarreBouton;
  private SNPanelContenu sNPanelPrincipal1;
  private SNPanelTitre pnTableau;
  private SNLabelUnite lbType;
  private SNLabelUnite lbMontant;
  private XRiTextField LIE01;
  private XRiTextField MTE01;
  private XRiTextField LIE02;
  private XRiTextField MTE02;
  private XRiTextField LIE03;
  private XRiTextField MTE03;
  private XRiTextField LIE04;
  private XRiTextField MTE04;
  private XRiTextField LIE05;
  private XRiTextField MTE05;
  private XRiTextField LIE06;
  private XRiTextField MTE06;
  private XRiTextField LIE07;
  private XRiTextField MTE07;
  private XRiTextField LIE08;
  private XRiTextField MTE08;
  private XRiTextField LIE09;
  private XRiTextField MTE09;
  private XRiTextField LIE10;
  private XRiTextField MTE10;
  private XRiTextField MTE11;
  private XRiTextField MTE12;
  private XRiTextField MTE13;
  private XRiTextField MTE14;
  private XRiTextField MTE15;
  private XRiTextField MTE16;
  private XRiTextField LIE11;
  private XRiTextField LIE12;
  private XRiTextField LIE13;
  private XRiTextField LIE14;
  private XRiTextField LIE15;
  private XRiTextField LIE16;
  private JPanel panel3;
  private JLabel label4;
  private XRiTextField TOTC;
  private SNLabelChamp sNLabelChamp1;
  private XRiTextField TOTCAI;
  private SNLabelChamp sNLabelChamp5;
  private XRiTextField ECART;
  private JButton BT_PGUP2;
  private JButton BT_PGDOWN2;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
