
package ri.serien.libecranrpg.vcgm.VCGM01FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.LayoutStyle;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM01FM_EP extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM01FM_EP(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    EPNMT2.setValeursSelection("X", " ");
    EPNMT.setValeursSelection("X", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    

    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    // riMenu_bt2.setIcon(lexique.getImage("images/options.png",true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (EPNMT.isSelected())
    // lexique.HostFieldPutData("EPNMT", 0, "X");
    // else
    // lexique.HostFieldPutData("EPNMT", 0, " ");
    // if (EPNMT2.isSelected())
    // lexique.HostFieldPutData("EPNMT2", 0, "X");
    // else
    // lexique.HostFieldPutData("EPNMT2", 0, " ");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44_OBJ_45 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_45_OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46_OBJ_47 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_43_OBJ_43 = new JLabel();
    OBJ_41_OBJ_41 = new JLabel();
    EPSAN = new XRiTextField();
    EPJO = new XRiTextField();
    EPLIBD = new XRiTextField();
    EPLIBC = new XRiTextField();
    OBJ_61_OBJ_61 = new JLabel();
    EPNMT = new XRiCheckBox();
    OBJ_49_OBJ_49 = new JLabel();
    OBJ_50_OBJ_50 = new JLabel();
    EPNCGD = new XRiTextField();
    EPNCGC = new XRiTextField();
    OBJ_58_OBJ_58 = new JLabel();
    EPMTT = new XRiTextField();
    OBJ_53_OBJ_53 = new JLabel();
    panel1 = new JPanel();
    OBJ_51_OBJ_51 = new JLabel();
    EPNTVA = new XRiTextField();
    OBJ_52_OBJ_52 = new JLabel();
    EPTTVA = new XRiTextField();
    OBJ_43_OBJ_44 = new JLabel();
    EPNAT = new XRiTextField();
    xTitledPanel2 = new JXTitledPanel();
    EPLDCD = new XRiTextField();
    EPLDCC = new XRiTextField();
    OBJ_75_OBJ_75 = new JLabel();
    EPNMT2 = new XRiCheckBox();
    OBJ_67_OBJ_67 = new JLabel();
    OBJ_68_OBJ_68 = new JLabel();
    EPNDCD = new XRiTextField();
    EPNDCC = new XRiTextField();
    OBJ_72_OBJ_72 = new JLabel();
    EPMTT2 = new XRiTextField();
    OBJ_69_OBJ_69 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44_OBJ_45 ----
          OBJ_44_OBJ_45.setText("Soci\u00e9t\u00e9");
          OBJ_44_OBJ_45.setName("OBJ_44_OBJ_45");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setName("INDETB");

          //---- OBJ_45_OBJ_46 ----
          OBJ_45_OBJ_46.setText("Code");
          OBJ_45_OBJ_46.setName("OBJ_45_OBJ_46");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setName("INDTYP");

          //---- OBJ_46_OBJ_47 ----
          OBJ_46_OBJ_47.setText("Ordre");
          OBJ_46_OBJ_47.setName("OBJ_46_OBJ_47");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(800, 450));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Diff\u00e9rence de r\u00e8glement");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- OBJ_43_OBJ_43 ----
            OBJ_43_OBJ_43.setText("Section analytique");
            OBJ_43_OBJ_43.setName("OBJ_43_OBJ_43");
            xTitledPanel1ContentContainer.add(OBJ_43_OBJ_43);
            OBJ_43_OBJ_43.setBounds(135, 11, 114, 28);

            //---- OBJ_41_OBJ_41 ----
            OBJ_41_OBJ_41.setText("Journal");
            OBJ_41_OBJ_41.setName("OBJ_41_OBJ_41");
            xTitledPanel1ContentContainer.add(OBJ_41_OBJ_41);
            OBJ_41_OBJ_41.setBounds(30, 11, 48, 28);

            //---- EPSAN ----
            EPSAN.setName("EPSAN");
            xTitledPanel1ContentContainer.add(EPSAN);
            EPSAN.setBounds(245, 11, 50, EPSAN.getPreferredSize().height);

            //---- EPJO ----
            EPJO.setName("EPJO");
            xTitledPanel1ContentContainer.add(EPJO);
            EPJO.setBounds(85, 11, 30, EPJO.getPreferredSize().height);

            //---- EPLIBD ----
            EPLIBD.setName("EPLIBD");
            xTitledPanel1ContentContainer.add(EPLIBD);
            EPLIBD.setBounds(200, 85, 270, EPLIBD.getPreferredSize().height);

            //---- EPLIBC ----
            EPLIBC.setName("EPLIBC");
            xTitledPanel1ContentContainer.add(EPLIBC);
            EPLIBC.setBounds(200, 116, 270, EPLIBC.getPreferredSize().height);

            //---- OBJ_61_OBJ_61 ----
            OBJ_61_OBJ_61.setText("Limite maximum");
            OBJ_61_OBJ_61.setName("OBJ_61_OBJ_61");
            xTitledPanel1ContentContainer.add(OBJ_61_OBJ_61);
            OBJ_61_OBJ_61.setBounds(85, 155, 115, 28);

            //---- EPNMT ----
            EPNMT.setText("Pas de limites");
            EPNMT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPNMT.setName("EPNMT");
            xTitledPanel1ContentContainer.add(EPNMT);
            EPNMT.setBounds(305, 160, 114, EPNMT.getPreferredSize().height);

            //---- OBJ_49_OBJ_49 ----
            OBJ_49_OBJ_49.setText("Num\u00e9ro compte");
            OBJ_49_OBJ_49.setName("OBJ_49_OBJ_49");
            xTitledPanel1ContentContainer.add(OBJ_49_OBJ_49);
            OBJ_49_OBJ_49.setBounds(85, 60, 99, 30);

            //---- OBJ_50_OBJ_50 ----
            OBJ_50_OBJ_50.setText("Libell\u00e9 \u00e9criture");
            OBJ_50_OBJ_50.setName("OBJ_50_OBJ_50");
            xTitledPanel1ContentContainer.add(OBJ_50_OBJ_50);
            OBJ_50_OBJ_50.setBounds(200, 60, 90, 30);

            //---- EPNCGD ----
            EPNCGD.setName("EPNCGD");
            xTitledPanel1ContentContainer.add(EPNCGD);
            EPNCGD.setBounds(85, 85, 60, EPNCGD.getPreferredSize().height);

            //---- EPNCGC ----
            EPNCGC.setName("EPNCGC");
            xTitledPanel1ContentContainer.add(EPNCGC);
            EPNCGC.setBounds(85, 116, 60, EPNCGC.getPreferredSize().height);

            //---- OBJ_58_OBJ_58 ----
            OBJ_58_OBJ_58.setText("Cr\u00e9dit");
            OBJ_58_OBJ_58.setName("OBJ_58_OBJ_58");
            xTitledPanel1ContentContainer.add(OBJ_58_OBJ_58);
            OBJ_58_OBJ_58.setBounds(40, 116, 39, 28);

            //---- EPMTT ----
            EPMTT.setName("EPMTT");
            xTitledPanel1ContentContainer.add(EPMTT);
            EPMTT.setBounds(200, 155, 55, EPMTT.getPreferredSize().height);

            //---- OBJ_53_OBJ_53 ----
            OBJ_53_OBJ_53.setText("D\u00e9bit");
            OBJ_53_OBJ_53.setName("OBJ_53_OBJ_53");
            xTitledPanel1ContentContainer.add(OBJ_53_OBJ_53);
            OBJ_53_OBJ_53.setBounds(40, 85, 36, 28);

            //======== panel1 ========
            {
              panel1.setBorder(new TitledBorder("TVA"));
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- OBJ_51_OBJ_51 ----
              OBJ_51_OBJ_51.setText("Compte");
              OBJ_51_OBJ_51.setName("OBJ_51_OBJ_51");
              panel1.add(OBJ_51_OBJ_51);
              OBJ_51_OBJ_51.setBounds(15, 30, 49, 25);

              //---- EPNTVA ----
              EPNTVA.setName("EPNTVA");
              panel1.add(EPNTVA);
              EPNTVA.setBounds(15, 55, 70, EPNTVA.getPreferredSize().height);

              //---- OBJ_52_OBJ_52 ----
              OBJ_52_OBJ_52.setText("Taux");
              OBJ_52_OBJ_52.setName("OBJ_52_OBJ_52");
              panel1.add(OBJ_52_OBJ_52);
              OBJ_52_OBJ_52.setBounds(110, 30, 43, 25);

              //---- EPTTVA ----
              EPTTVA.setName("EPTTVA");
              panel1.add(EPTTVA);
              EPTTVA.setBounds(110, 55, 43, EPTTVA.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel1);
            panel1.setBounds(515, 60, 180, 95);

            //---- OBJ_43_OBJ_44 ----
            OBJ_43_OBJ_44.setText("Nature");
            OBJ_43_OBJ_44.setName("OBJ_43_OBJ_44");
            xTitledPanel1ContentContainer.add(OBJ_43_OBJ_44);
            OBJ_43_OBJ_44.setBounds(330, 10, 69, 28);

            //---- EPNAT ----
            EPNAT.setName("EPNAT");
            xTitledPanel1ContentContainer.add(EPNAT);
            EPNAT.setBounds(395, 10, 64, EPNAT.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setTitle("Diff\u00e9rence de change");
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //---- EPLDCD ----
            EPLDCD.setName("EPLDCD");
            xTitledPanel2ContentContainer.add(EPLDCD);
            EPLDCD.setBounds(195, 40, 270, EPLDCD.getPreferredSize().height);

            //---- EPLDCC ----
            EPLDCC.setName("EPLDCC");
            xTitledPanel2ContentContainer.add(EPLDCC);
            EPLDCC.setBounds(195, 71, 270, EPLDCC.getPreferredSize().height);

            //---- OBJ_75_OBJ_75 ----
            OBJ_75_OBJ_75.setText("Limite maximum");
            OBJ_75_OBJ_75.setName("OBJ_75_OBJ_75");
            xTitledPanel2ContentContainer.add(OBJ_75_OBJ_75);
            OBJ_75_OBJ_75.setBounds(90, 107, 105, 25);

            //---- EPNMT2 ----
            EPNMT2.setText("Pas de limites");
            EPNMT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EPNMT2.setName("EPNMT2");
            xTitledPanel2ContentContainer.add(EPNMT2);
            EPNMT2.setBounds(305, 110, 121, EPNMT2.getPreferredSize().height);

            //---- OBJ_67_OBJ_67 ----
            OBJ_67_OBJ_67.setText("Num\u00e9ro compte");
            OBJ_67_OBJ_67.setName("OBJ_67_OBJ_67");
            xTitledPanel2ContentContainer.add(OBJ_67_OBJ_67);
            OBJ_67_OBJ_67.setBounds(90, 15, 99, 25);

            //---- OBJ_68_OBJ_68 ----
            OBJ_68_OBJ_68.setText("Libell\u00e9 \u00e9criture");
            OBJ_68_OBJ_68.setName("OBJ_68_OBJ_68");
            xTitledPanel2ContentContainer.add(OBJ_68_OBJ_68);
            OBJ_68_OBJ_68.setBounds(195, 15, 90, 25);

            //---- EPNDCD ----
            EPNDCD.setName("EPNDCD");
            xTitledPanel2ContentContainer.add(EPNDCD);
            EPNDCD.setBounds(90, 40, 60, EPNDCD.getPreferredSize().height);

            //---- EPNDCC ----
            EPNDCC.setName("EPNDCC");
            xTitledPanel2ContentContainer.add(EPNDCC);
            EPNDCC.setBounds(90, 71, 60, EPNDCC.getPreferredSize().height);

            //---- OBJ_72_OBJ_72 ----
            OBJ_72_OBJ_72.setText("Cr\u00e9dit");
            OBJ_72_OBJ_72.setName("OBJ_72_OBJ_72");
            xTitledPanel2ContentContainer.add(OBJ_72_OBJ_72);
            OBJ_72_OBJ_72.setBounds(45, 73, 39, 25);

            //---- EPMTT2 ----
            EPMTT2.setName("EPMTT2");
            xTitledPanel2ContentContainer.add(EPMTT2);
            EPMTT2.setBounds(195, 105, 55, EPMTT2.getPreferredSize().height);

            //---- OBJ_69_OBJ_69 ----
            OBJ_69_OBJ_69.setText("D\u00e9bit");
            OBJ_69_OBJ_69.setName("OBJ_69_OBJ_69");
            xTitledPanel2ContentContainer.add(OBJ_69_OBJ_69);
            OBJ_69_OBJ_69.setBounds(45, 42, 36, 25);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 774, Short.MAX_VALUE)
                  .addComponent(xTitledPanel2, GroupLayout.DEFAULT_SIZE, 774, Short.MAX_VALUE))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 235, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(xTitledPanel2, GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44_OBJ_45;
  private XRiTextField INDETB;
  private JLabel OBJ_45_OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_46_OBJ_47;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_43_OBJ_43;
  private JLabel OBJ_41_OBJ_41;
  private XRiTextField EPSAN;
  private XRiTextField EPJO;
  private XRiTextField EPLIBD;
  private XRiTextField EPLIBC;
  private JLabel OBJ_61_OBJ_61;
  private XRiCheckBox EPNMT;
  private JLabel OBJ_49_OBJ_49;
  private JLabel OBJ_50_OBJ_50;
  private XRiTextField EPNCGD;
  private XRiTextField EPNCGC;
  private JLabel OBJ_58_OBJ_58;
  private XRiTextField EPMTT;
  private JLabel OBJ_53_OBJ_53;
  private JPanel panel1;
  private JLabel OBJ_51_OBJ_51;
  private XRiTextField EPNTVA;
  private JLabel OBJ_52_OBJ_52;
  private XRiTextField EPTTVA;
  private JLabel OBJ_43_OBJ_44;
  private XRiTextField EPNAT;
  private JXTitledPanel xTitledPanel2;
  private XRiTextField EPLDCD;
  private XRiTextField EPLDCC;
  private JLabel OBJ_75_OBJ_75;
  private XRiCheckBox EPNMT2;
  private JLabel OBJ_67_OBJ_67;
  private JLabel OBJ_68_OBJ_68;
  private XRiTextField EPNDCD;
  private XRiTextField EPNDCC;
  private JLabel OBJ_72_OBJ_72;
  private XRiTextField EPMTT2;
  private JLabel OBJ_69_OBJ_69;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
