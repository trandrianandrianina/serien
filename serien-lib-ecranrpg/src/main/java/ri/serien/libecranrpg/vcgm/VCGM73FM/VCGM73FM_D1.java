
package ri.serien.libecranrpg.vcgm.VCGM73FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerListModel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.outil.font.CustomFont;

/**
 * @author Stéphane Vénéri
 */
public class VCGM73FM_D1 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] _TE01_Top = { "TE01", "TE02", "TE03", "TE04", "TE05", "TE06", "TE07", "TE08", "TE09", "TE10", "TE11", "TE12", "TE13",
      "TE14", "TE15", "TE16", "TE17", "TE18", "TE19", "TE20", };
  private String[] _TE01_Title = { "HLD01", };
  private String[][] _TE01_Data = { { "LE01", }, { "LE02", }, { "LE03", }, { "LE04", }, { "LE05", }, { "LE06", }, { "LE07", },
      { "LE08", }, { "LE09", }, { "LE10", }, { "LE11", }, { "LE12", }, { "LE13", }, { "LE14", }, { "LE15", }, { "LE16", }, { "LE17", },
      { "LE18", }, { "LE19", }, { "LE20", }, };
  private int[] _TE01_Width = { 1050, };
  private Color[][] _LIST_Text_Color = new Color[20][1];
  
  private Color[][] _LIST_Fond_Color = null;
  
  private Color violet = new Color(155, 155, 255);
  private Color couleurRapp = new Color(53, 100, 25);
  private Color couleurLiti = new Color(255, 100, 0);
  
  private Font[][] police = new Font[20][1];
  
  private Font normal = CustomFont.loadFont("fonts/VeraMono.ttf", 12);// new Font("Courier New", Font.PLAIN, 12);
  private Font gras = CustomFont.loadFont("fonts/VeraMoBd.ttf", 12); // new Font("Courier New", Font.BOLD, 12);
  
  // calculatrice solde partiel
  String[][] enTete;
  
  int[] indexs = { 6, 7 };
  
  ArrayList<String> donnees;
  
  public VCGM73FM_D1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    
    TE01.setAspectTable(_TE01_Top, _TE01_Title, _TE01_Data, _TE01_Width, false, null, _LIST_Text_Color, _LIST_Fond_Color, police);
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_120.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIB@")).trim());
    WCLA1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCLA1@")).trim());
    WCLA2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCLA2@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_203.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W1ATT@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    JLabel[] tabRap = { rap01, rap02, rap03, rap04, rap05, rap06, rap07, rap08, rap09, rap10, rap11, rap12, rap13, rap14, rap15, rap16,
        rap17, rap18, rap19, rap20 };
    JLabel[] tabLit = { lit01, lit02, lit03, lit04, lit05, lit06, lit07, lit08, lit09, lit10, lit11, lit12, lit13, lit14, lit15, lit16,
        lit17, lit18, lit19, lit20 };
    
    // Couleurs de la liste (suivant indicateurs de 21 à 35) POUR
    // LETTRAGE
    for (int i = 0; i < 15; i++) {
      if (lexique.isTrue(String.valueOf(i + 21))) {
        if (lexique.isTrue("(N56)")) {
          // si ligne de reprise
          if ((lexique.HostFieldGetData("LE" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))).contains("Reprise"))
              || (lexique.HostFieldGetData("LE" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))).contains("Position"))) {
            _LIST_Text_Color[i][0] = Constantes.COULEUR_LISTE_COMMENTAIRE;
            police[i][0] = gras;
          }
          // sinon ligne pointage
          else {
            _LIST_Text_Color[i][0] = violet;
            police[i][0] = gras;
          }
        }
        else {
          _LIST_Text_Color[i][0] = Color.BLACK;
          police[i][0] = gras;
        }
      }
      else {
        _LIST_Text_Color[i][0] = Color.BLACK;
        police[i][0] = normal;
      }
    }
    
    // Couleurs de la liste (suivant indicateurs de 41 à 45) POUR
    // LETTRAGE
    for (int i = 0; i < 5; i++) {
      if (lexique.isTrue(String.valueOf(i + 41))) {
        if (lexique.isTrue("(N56)")) {
          // si ligne de reprise
          if ((lexique.HostFieldGetData("LE" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))).contains("Reprise"))
              || (lexique.HostFieldGetData("LE" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))).contains("Position"))) {
            _LIST_Text_Color[i + 15][0] = Constantes.COULEUR_LISTE_COMMENTAIRE;
            police[i + 15][0] = gras;
          }
          // sinon ligne pointage
          else {
            _LIST_Text_Color[i + 15][0] = violet;
            police[i + 15][0] = gras;
          }
        }
        else {
          _LIST_Text_Color[i + 15][0] = Color.BLACK;
          police[i + 15][0] = gras;
        }
      }
      
      else {
        _LIST_Text_Color[i + 15][0] = Color.BLACK;
        police[i + 15][0] = normal;
      }
    }
    
    // Couleurs de la liste si "r" en position 17 de la ligne (écriture rapprochée)
    for (int i = 0; i < 20; i++) {
      if ((lexique.HostFieldGetData("LE" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))).substring(16, 17).equalsIgnoreCase("r"))) {
        _LIST_Text_Color[i][0] = couleurRapp;
        tabRap[i].setVisible(true);
      }
      else {
        tabRap[i].setVisible(false);
      }
    }
    
    // Couleurs de la liste si "l" en position 19 de la ligne (écriture en litige)
    for (int i = 0; i < 20; i++) {
      if ((lexique.HostFieldGetData("LE" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))).substring(18, 19).equalsIgnoreCase("l"))) {
        _LIST_Text_Color[i][0] = couleurLiti;
        tabLit[i].setVisible(true);
      }
      // Couleurs de la liste si "!" en position 2 de la ligne (litige)
      else if ((lexique.HostFieldGetData("LE" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))).substring(2, 3).equalsIgnoreCase("!"))) {
        _LIST_Text_Color[i][0] = couleurLiti;
        police[i][0] = gras;
      }
      else {
        tabLit[i].setVisible(false);
      }
    }
    
    WCLA1.setVisible(lexique.isPresent("WCLA1"));
    WCLA2.setVisible(lexique.isPresent("WCLA2"));
    
    // Ajoute à la liste des oData les variables non liée directement à un
    // composant graphique
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _TE01_Top, null, _LIST_Text_Color, _LIST_Fond_Color, police);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    String chaine = "";
    // Contruction du nom du fichier si F10 (VEXP0A)
    // if(riSousMenu_bt_export instanceof RiSousMenu_bt)
    // lexique.setNomFichierTableur( lexique.HostFieldGetData("TITPG1").trim() + " " +
    // lexique.HostFieldGetData("TITPG2").trim());
    
    
    BT_CPTAP.setVisible(!interpreteurD.analyseExpression("@V01F@").trim().equalsIgnoreCase("LETTRAGE"));
    BT_CPTAV.setVisible(!interpreteurD.analyseExpression("@V01F@").trim().equalsIgnoreCase("LETTRAGE"));
    
    // 1° groupe
    WPTG.setVisible(lexique.isTrue("36"));
    OBJ_195.setVisible(WPTG.isVisible());
    OBJ_203.setVisible(lexique.isTrue("77"));
    OBJ_199.setVisible(!lexique.HostFieldGetData("WTEX2").trim().equalsIgnoreCase("1"));
    OBJ_198.setVisible(lexique.HostFieldGetData("WTEX2").trim().equalsIgnoreCase("1"));
    
    // Groupe des icones
    // OBJ_202.setVisible(!interpreteurD.analyseExpression("@&&CONVFIN@").trim().equalsIgnoreCase(""));
    // OBJ_210.setEnabled(!interpreteurD.analyseExpression("@&&LIB01@").trim().equalsIgnoreCase(""));
    // OBJ_210.setEnabled(false);
    riSousMenu7.setEnabled(lexique.isTrue("(52) OR (53)"));
    riSousMenu16.setEnabled(lexique.isTrue("(51) OR (52)"));
    riSousMenu8.setEnabled(lexique.isTrue("52"));
    chaine = lexique.HostFieldGetData("INDNCG");
    if ((chaine != null) && (chaine.length() > 0)) {
      riSousMenu9.setEnabled((chaine.charAt(0) == '6') || (chaine.charAt(0) == '7'));
    }
    if ((chaine != null) && (chaine.length() > 1)) {
      riSousMenu10.setEnabled((chaine.substring(0, 2).equals("41")) || (chaine.substring(0, 2).equals("45")));
    }
    riSousMenu11.setEnabled(lexique.isTrue("(51) OR (52) OR (53)"));
    if (lexique.HostFieldGetData("WSOGL").equalsIgnoreCase("X")) {
      riSousMenu_bt12.setToolTipText("Visualisation classique");
      riSousMenu_bt12.setText("Visualisation classique");
    }
    
    else {
      riSousMenu_bt12.setToolTipText("Visualisation des soldes glissés");
      riSousMenu_bt12.setText("Soldes glissés");
    }
    
    // 52 = historique /
    riSousMenu17.setEnabled(lexique.isTrue("52"));
    riSousMenu22.setEnabled(lexique.isTrue("52"));
    panel3.setVisible(lexique.isTrue("52"));
    
    // N52 = en cours
    panel2.setVisible(lexique.isTrue("N52"));
    
    // OBJ_271.setEnabled( lexique.isPresent("WMOIX"));
    
    // Bouton rouge du bloc note
    
    BT_TE01.setVisible(lexique.HostFieldGetData("LE01").charAt(2) == 'o');
    BT_TE02.setVisible(lexique.HostFieldGetData("LE02").charAt(2) == 'o');
    BT_TE03.setVisible(lexique.HostFieldGetData("LE03").charAt(2) == 'o');
    BT_TE04.setVisible(lexique.HostFieldGetData("LE04").charAt(2) == 'o');
    BT_TE05.setVisible(lexique.HostFieldGetData("LE05").charAt(2) == 'o');
    BT_TE06.setVisible(lexique.HostFieldGetData("LE06").charAt(2) == 'o');
    BT_TE07.setVisible(lexique.HostFieldGetData("LE07").charAt(2) == 'o');
    BT_TE08.setVisible(lexique.HostFieldGetData("LE08").charAt(2) == 'o');
    BT_TE09.setVisible(lexique.HostFieldGetData("LE09").charAt(2) == 'o');
    BT_TE10.setVisible(lexique.HostFieldGetData("LE10").charAt(2) == 'o');
    BT_TE11.setVisible(lexique.HostFieldGetData("LE11").charAt(2) == 'o');
    BT_TE12.setVisible(lexique.HostFieldGetData("LE12").charAt(2) == 'o');
    BT_TE13.setVisible(lexique.HostFieldGetData("LE13").charAt(2) == 'o');
    BT_TE14.setVisible(lexique.HostFieldGetData("LE14").charAt(2) == 'o');
    BT_TE15.setVisible(lexique.HostFieldGetData("LE15").charAt(2) == 'o');
    BT_TE16.setVisible(lexique.HostFieldGetData("LE16").charAt(2) == 'o');
    BT_TE17.setVisible(lexique.HostFieldGetData("LE17").charAt(2) == 'o');
    BT_TE18.setVisible(lexique.HostFieldGetData("LE18").charAt(2) == 'o');
    BT_TE19.setVisible(lexique.HostFieldGetData("LE19").charAt(2) == 'o');
    BT_TE20.setVisible(lexique.HostFieldGetData("LE20").charAt(2) == 'o');
    
    OBJ_119.setVisible(!interpreteurD.analyseExpression("@INDNCA@").trim().equalsIgnoreCase(""));
    
    // TODO Icones
    BT_CPTAV.setIcon(lexique.chargerImage("images/fleche_pre.png", true));
    BT_CPTAP.setIcon(lexique.chargerImage("images/fleche_sui.png", true));
    BT_PDEB.setIcon(lexique.chargerImage("images/pdeb20.png", true));
    BT_PFIN.setIcon(lexique.chargerImage("images/pfin20.png", true));
    OBJ_268.setIcon(lexique.chargerImage("images/plus_petit.png", true));
    OBJ_272.setIcon(lexique.chargerImage("images/moins_petit.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITRE@ @PG13LR@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
    riMenu_bt4.setIcon(lexique.chargerImage("images/fonctions.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(4, 2);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WMOIX");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    // WMOIX.setText("*DET");
    lexique.HostFieldPutData("WMOIX", 0, "*DET");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(1, 1);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(2, 27);
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void riSousMenu_bt13ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WDOC", 0, "1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    enTete = new String[][] { { "Jo", "3" }, { "Folio", "5" }, { "Date", "9" }, { "C", "2" }, { "Libell\u00e9", "27" },
        { "N\u00b0 pi\u00e8ce", "8" }, { "D\u00e9bit", "14" }, { "Cr\u00e9dit", "14" }, { "Aff", "2" } };
    
    donnees = new ArrayList<String>();
    
    int[] indices = TE01.getSelectedRows();
    
    for (int i = 0; i < indices.length; i++) {
      String valeur = "LE";
      
      if (indices[i] < 9) {
        valeur += "0";
      }
      
      int ii = indices[i] + 1;
      valeur += ii;
      
      donnees.add(lexique.HostFieldGetData(valeur));
    }
    lexique.openCalculatriceSP(enTete, donnees, indexs);
  }
  
  private void riSousMenu_bt16ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(23, 20);
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void riSousMenu_bt17ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(1, 1);
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void riSousMenu_bt22ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(10, 1);
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void riSousMenu_bt18ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void riSousMenu_bt19ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void riSousMenu_bt20ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt21ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_57ActionPerformed(ActionEvent e) {
    TE01.setClearTop(false);
    TE01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_58ActionPerformed(ActionEvent e) {
    TE01.setClearTop(false);
    TE01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_59ActionPerformed(ActionEvent e) {
    TE01.setClearTop(false);
    TE01.setValeurTop("3");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_60ActionPerformed(ActionEvent e) {
    TE01.setClearTop(false);
    TE01.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_61ActionPerformed(ActionEvent e) {
    TE01.setClearTop(false);
    TE01.setValeurTop("F");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_63ActionPerformed(ActionEvent e) {
    TE01.setClearTop(false);
    TE01.setValeurTop("9");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_64ActionPerformed(ActionEvent e) {
    TE01.setClearTop(false);
    TE01.setValeurTop("S");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_65ActionPerformed(ActionEvent e) {
    TE01.setClearTop(false);
    TE01.setValeurTop("T");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_67ActionPerformed(ActionEvent e) {
    TE01.setClearTop(false);
    TE01.setValeurTop("A");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_69ActionPerformed(ActionEvent e) {
    TE01.setClearTop(false);
    TE01.setValeurTop("E");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_70ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_71ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_73ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD2.getInvoker().getName());
  }
  
  private void OBJ_74ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD2.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_198ActionPerformed(ActionEvent e) {
    // lexique.HostFieldPutData("WMOIX", 0, "E1");
    WMOIX.setText("E1");
    lexique.HostScreenSendKey(this, "enter");
  }
  
  private void OBJ_199ActionPerformed(ActionEvent e) {
    // lexique.HostFieldPutData("WMOIX", 0, "E2");
    WMOIX.setText("E2");
    lexique.HostScreenSendKey(this, "enter");
  }
  
  private void OBJ_192ActionPerformed(ActionEvent e) {
    // PcCommand(4, "Numeroteur.exe " +@"@WTEL@"+ "
    // "+Chr(34)+@"@WLIB@"+Chr(34)+" "+"1 "+ @"@&PREFIXE@")
  }
  
  private void OBJ_268ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WMOIX", 0, "+" + WMOIX_SP.getValue());
    lexique.HostScreenSendKey(this, "enter");
  }
  
  private void OBJ_272ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WMOIX", 0, "-" + WMOIX_SP.getValue());
    lexique.HostScreenSendKey(this, "enter");
  }
  
  private void OBJ_208ActionPerformed(ActionEvent e) {
    TE01.setClearTop(false);
    lexique.HostFieldPutData("TE01", 0, "5");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_213ActionPerformed(ActionEvent e) {
    TE01.setClearTop(false);
    lexique.HostFieldPutData("TE02", 0, "5");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_216ActionPerformed(ActionEvent e) {
    TE01.setClearTop(false);
    lexique.HostFieldPutData("TE03", 0, "5");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_219ActionPerformed(ActionEvent e) {
    TE01.setClearTop(false);
    lexique.HostFieldPutData("TE04", 0, "5");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_225ActionPerformed(ActionEvent e) {
    TE01.setClearTop(false);
    lexique.HostFieldPutData("TE05", 0, "5");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_228ActionPerformed(ActionEvent e) {
    TE01.setClearTop(false);
    lexique.HostFieldPutData("TE06", 0, "5");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_234ActionPerformed(ActionEvent e) {
    TE01.setClearTop(false);
    lexique.HostFieldPutData("TE07", 0, "5");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_237ActionPerformed(ActionEvent e) {
    TE01.setClearTop(false);
    lexique.HostFieldPutData("TE08", 0, "5");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_240ActionPerformed(ActionEvent e) {
    TE01.setClearTop(false);
    lexique.HostFieldPutData("TE09", 0, "5");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_247ActionPerformed(ActionEvent e) {
    TE01.setClearTop(false);
    lexique.HostFieldPutData("TE10", 0, "5");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_250ActionPerformed(ActionEvent e) {
    TE01.setClearTop(false);
    lexique.HostFieldPutData("TE11", 0, "5");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_254ActionPerformed(ActionEvent e) {
    TE01.setClearTop(false);
    lexique.HostFieldPutData("TE12", 0, "5");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_257ActionPerformed(ActionEvent e) {
    TE01.setClearTop(false);
    lexique.HostFieldPutData("TE13", 0, "5");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_260ActionPerformed(ActionEvent e) {
    TE01.setClearTop(false);
    lexique.HostFieldPutData("TE14", 0, "5");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_264ActionPerformed(ActionEvent e) {
    TE01.setClearTop(false);
    lexique.HostFieldPutData("TE15", 0, "5");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_62ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _TE01_Top, "8", "Enter");
    TE01.setValeurTop("8");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_68ActionPerformed(ActionEvent e) {
    enTete = new String[][] { { "Jo", "3" }, { "Folio", "5" }, { "Date", "9" }, { "C", "2" }, { "Libell\u00e9", "27" },
        { "N\u00b0 pi\u00e8ce", "8" }, { "D\u00e9bit", "14" }, { "Cr\u00e9dit", "14" }, { "Aff", "2" } };
    
    donnees = new ArrayList<String>();
    
    int[] indices = TE01.getSelectedRows();
    
    for (int i = 0; i < indices.length; i++) {
      String valeur = "LE";
      
      if (indices[i] < 9) {
        valeur += "0";
      }
      
      int ii = indices[i] + 1;
      valeur += ii;
      
      donnees.add(lexique.HostFieldGetData(valeur));
    }
    
    lexique.openCalculatriceSP(enTete, donnees, indexs);
  }
  
  private void OBJ_77ActionPerformed(ActionEvent e) {
    // lexique.HostFieldPutData("WMOIX", 0, "D ");
    WMOIX.setText("D    ");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_79ActionPerformed(ActionEvent e) {
    // lexique.HostFieldPutData("WMOIX", 0, "F ");
    WMOIX.setText("F    ");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void TE01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _TE01_Top, "1", "ENTER", e);
    if (TE01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void OBJ_265ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("TE20", 0, "5");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_261ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("TE19", 0, "5");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_258ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("TE18", 0, "5");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_255ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("TE17", 0, "5");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_251ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("TE16", 0, "5");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_119ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WMOIX", 0, "*BA");
    // WMOIX.setText("*BA");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void BT_CPTAVActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(1, 6);
    lexique.HostScreenSendKey(this, "F7");
  }
  
  private void BT_CPTAPActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(6, 6);
    lexique.HostScreenSendKey(this, "F7");
  }
  
  private void riSousMenu_bt_dupliActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F17");
  }
  
  private void mi_NumPieceActionPerformed(ActionEvent e) {
    TE01.setValeurTop("C");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_66ActionPerformed(ActionEvent e) {
    TE01.setValeurTop("W");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void riSousMenu_bt30ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void OBJ_72ActionPerformed(ActionEvent e) {
    TE01.setValeurTop("U");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void MILienUrlActionPerformed(ActionEvent e) {
    TE01.setValeurTop("R");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    INDETB = new XRiTextField();
    INDNCG = new XRiTextField();
    INDNCA = new XRiTextField();
    OBJ_120 = new RiZoneSortie();
    OBJ_119 = new SNBoutonDetail();
    BT_CPTAV = new JButton();
    BT_CPTAP = new JButton();
    label1 = new JLabel();
    label2 = new JLabel();
    WCLA1 = new RiZoneSortie();
    WCLA2 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_del = new RiSousMenu();
    riSousMenu_bt_del = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu13 = new RiSousMenu();
    riSousMenu_bt13 = new RiSousMenu_bt();
    riSousMenu30 = new RiSousMenu();
    riSousMenu_bt30 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    riSousMenu16 = new RiSousMenu();
    riSousMenu_bt16 = new RiSousMenu_bt();
    riSousMenu17 = new RiSousMenu();
    riSousMenu_bt17 = new RiSousMenu_bt();
    riSousMenu22 = new RiSousMenu();
    riSousMenu_bt22 = new RiSousMenu_bt();
    riMenu4 = new RiMenu();
    riMenu_bt4 = new RiMenu_bt();
    riSousMenu18 = new RiSousMenu();
    riSousMenu_bt18 = new RiSousMenu_bt();
    riSousMenu19 = new RiSousMenu();
    riSousMenu_bt19 = new RiSousMenu_bt();
    riSousMenu20 = new RiSousMenu();
    riSousMenu_bt20 = new RiSousMenu_bt();
    riSousMenu21 = new RiSousMenu();
    riSousMenu_bt21 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_195 = new JLabel();
    WPTG = new XRiTextField();
    OBJ_196 = new JLabel();
    WLDEV = new XRiTextField();
    OBJ_192 = new SNBoutonDetail();
    WTEL = new XRiTextField();
    OBJ_203 = new JLabel();
    OBJ_198 = new JButton();
    OBJ_199 = new JButton();
    CPSOCD = new XRiTextField();
    CPSENS = new XRiTextField();
    xTitledPanel2 = new JXTitledPanel();
    rap01 = new JLabel();
    rap02 = new JLabel();
    rap03 = new JLabel();
    rap04 = new JLabel();
    rap05 = new JLabel();
    rap06 = new JLabel();
    rap07 = new JLabel();
    rap08 = new JLabel();
    rap09 = new JLabel();
    rap10 = new JLabel();
    rap11 = new JLabel();
    rap12 = new JLabel();
    rap13 = new JLabel();
    rap14 = new JLabel();
    rap15 = new JLabel();
    rap16 = new JLabel();
    rap17 = new JLabel();
    rap18 = new JLabel();
    rap19 = new JLabel();
    rap20 = new JLabel();
    lit01 = new JLabel();
    lit02 = new JLabel();
    lit03 = new JLabel();
    lit04 = new JLabel();
    lit05 = new JLabel();
    lit06 = new JLabel();
    lit07 = new JLabel();
    lit08 = new JLabel();
    lit09 = new JLabel();
    lit10 = new JLabel();
    lit11 = new JLabel();
    lit12 = new JLabel();
    lit13 = new JLabel();
    lit14 = new JLabel();
    lit15 = new JLabel();
    lit16 = new JLabel();
    lit17 = new JLabel();
    lit18 = new JLabel();
    lit19 = new JLabel();
    lit20 = new JLabel();
    SCROLLPANE_LIST = new JScrollPane();
    TE01 = new XRiTable();
    BT_PDEB = new JButton();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BT_PFIN = new JButton();
    panel1 = new JPanel();
    BT_TE01 = new SNBoutonDetail();
    BT_TE02 = new SNBoutonDetail();
    BT_TE03 = new SNBoutonDetail();
    BT_TE04 = new SNBoutonDetail();
    BT_TE05 = new SNBoutonDetail();
    BT_TE06 = new SNBoutonDetail();
    BT_TE07 = new SNBoutonDetail();
    BT_TE08 = new SNBoutonDetail();
    BT_TE09 = new SNBoutonDetail();
    BT_TE10 = new SNBoutonDetail();
    BT_TE11 = new SNBoutonDetail();
    BT_TE12 = new SNBoutonDetail();
    BT_TE13 = new SNBoutonDetail();
    BT_TE14 = new SNBoutonDetail();
    BT_TE15 = new SNBoutonDetail();
    BT_TE20 = new SNBoutonDetail();
    BT_TE19 = new SNBoutonDetail();
    BT_TE18 = new SNBoutonDetail();
    BT_TE17 = new SNBoutonDetail();
    BT_TE16 = new SNBoutonDetail();
    panel2 = new JPanel();
    OBJ_272 = new JButton();
    OBJ_268 = new JButton();
    WMOIX_SP = new JSpinner();
    panel3 = new JPanel();
    label5 = new JLabel();
    WMOIX = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_57 = new JMenuItem();
    OBJ_58 = new JMenuItem();
    OBJ_66 = new JMenuItem();
    OBJ_59 = new JMenuItem();
    OBJ_60 = new JMenuItem();
    OBJ_61 = new JMenuItem();
    OBJ_62 = new JMenuItem();
    OBJ_63 = new JMenuItem();
    OBJ_64 = new JMenuItem();
    OBJ_72 = new JMenuItem();
    mi_NumPiece = new JMenuItem();
    OBJ_67 = new JMenuItem();
    OBJ_65 = new JMenuItem();
    OBJ_68 = new JMenuItem();
    OBJ_69 = new JMenuItem();
    MILienUrl = new JMenuItem();
    OBJ_71 = new JMenuItem();
    OBJ_70 = new JMenuItem();
    BTD2 = new JPopupMenu();
    OBJ_74 = new JMenuItem();
    OBJ_73 = new JMenuItem();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1200, 700));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Visualisation des comptes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(1100, 40));
          p_tete_gauche.setMinimumSize(new Dimension(900, 40));
          p_tete_gauche.setMaximumSize(new Dimension(900, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- INDNCG ----
          INDNCG.setComponentPopupMenu(BTD);
          INDNCG.setName("INDNCG");

          //---- INDNCA ----
          INDNCA.setComponentPopupMenu(BTD);
          INDNCA.setName("INDNCA");

          //---- OBJ_120 ----
          OBJ_120.setText("@WLIB@");
          OBJ_120.setToolTipText("Informations compl\u00e9mentaires");
          OBJ_120.setFont(OBJ_120.getFont().deriveFont(OBJ_120.getFont().getStyle() | Font.BOLD));
          OBJ_120.setOpaque(false);
          OBJ_120.setName("OBJ_120");

          //---- OBJ_119 ----
          OBJ_119.setToolTipText("Informations compl\u00e9mentaires");
          OBJ_119.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_119.setName("OBJ_119");
          OBJ_119.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_119ActionPerformed(e);
            }
          });

          //---- BT_CPTAV ----
          BT_CPTAV.setText("");
          BT_CPTAV.setToolTipText("Compte pr\u00e9c\u00e9dent");
          BT_CPTAV.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_CPTAV.setName("BT_CPTAV");
          BT_CPTAV.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              BT_CPTAVActionPerformed(e);
            }
          });

          //---- BT_CPTAP ----
          BT_CPTAP.setText("");
          BT_CPTAP.setToolTipText("Compte suivant");
          BT_CPTAP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_CPTAP.setName("BT_CPTAP");
          BT_CPTAP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              BT_CPTAPActionPerformed(e);
            }
          });

          //---- label1 ----
          label1.setText("Soci\u00e9t\u00e9");
          label1.setName("label1");

          //---- label2 ----
          label2.setText("Num\u00e9ro de compte");
          label2.setName("label2");

          //---- WCLA1 ----
          WCLA1.setText("@WCLA1@");
          WCLA1.setToolTipText("Informations compl\u00e9mentaires");
          WCLA1.setFont(WCLA1.getFont().deriveFont(WCLA1.getFont().getStyle() | Font.BOLD));
          WCLA1.setOpaque(false);
          WCLA1.setName("WCLA1");

          //---- WCLA2 ----
          WCLA2.setText("@WCLA2@");
          WCLA2.setToolTipText("Informations compl\u00e9mentaires");
          WCLA2.setFont(WCLA2.getFont().deriveFont(WCLA2.getFont().getStyle() | Font.BOLD));
          WCLA2.setOpaque(false);
          WCLA2.setName("WCLA2");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(label1)
                .addGap(9, 9, 9)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(label2)
                .addGap(9, 9, 9)
                .addComponent(INDNCG, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INDNCA, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_119, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22)
                .addComponent(OBJ_120, GroupLayout.PREFERRED_SIZE, 276, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(BT_CPTAV, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(BT_CPTAP, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addComponent(WCLA1, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(WCLA2, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(label1, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(label2, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDNCG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDNCA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_119, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addComponent(OBJ_120, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(BT_CPTAV, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(BT_CPTAP, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addComponent(WCLA1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addComponent(WCLA2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Justification");
              riSousMenu_bt_consult.setToolTipText("Justification");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Historique");
              riSousMenu_bt_modif.setToolTipText("Historique");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("En cours");
              riSousMenu_bt_crea.setToolTipText("En cours");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Lettrage d'\u00e9critures");
              riSousMenu_bt_suppr.setToolTipText("Lettrage d'\u00e9critures");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_del ========
            {
              riSousMenuF_del.setName("riSousMenuF_del");

              //---- riSousMenu_bt_del ----
              riSousMenu_bt_del.setText("D\u00e9lettrage d'\u00e9critures");
              riSousMenu_bt_del.setToolTipText("D\u00e9lettrage d'\u00e9critures");
              riSousMenu_bt_del.setName("riSousMenu_bt_del");
              riSousMenu_bt_del.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_dupliActionPerformed(e);
                }
              });
              riSousMenuF_del.add(riSousMenu_bt_del);
            }
            menus_haut.add(riSousMenuF_del);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Recherche \u00e9critures");
              riSousMenu_bt6.setToolTipText("Recherche \u00e9critures");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Options de visualisation");
              riSousMenu_bt7.setToolTipText("Options de visualisation");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("On / Off total p\u00e9riode");
              riSousMenu_bt8.setToolTipText("On / Off total p\u00e9riode");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Analyse budg\u00e9taire");
              riSousMenu_bt9.setToolTipText("Visualisation analyse budg\u00e9taire");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("En cours administratif");
              riSousMenu_bt10.setToolTipText("Visualisation en cours administratif (client)");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);

            //======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");

              //---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Ecritures sur ex. N-1");
              riSousMenu_bt11.setToolTipText("Ecritures sur l'exercice pr\u00e9c\u00e9dent");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);

            //======== riSousMenu13 ========
            {
              riSousMenu13.setName("riSousMenu13");

              //---- riSousMenu_bt13 ----
              riSousMenu_bt13.setText("Affichage en devise");
              riSousMenu_bt13.setToolTipText("Affichage en devise");
              riSousMenu_bt13.setName("riSousMenu_bt13");
              riSousMenu_bt13.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt13ActionPerformed(e);
                }
              });
              riSousMenu13.add(riSousMenu_bt13);
            }
            menus_haut.add(riSousMenu13);

            //======== riSousMenu30 ========
            {
              riSousMenu30.setName("riSousMenu30");

              //---- riSousMenu_bt30 ----
              riSousMenu_bt30.setText("Edition");
              riSousMenu_bt30.setToolTipText("Edition du compte");
              riSousMenu_bt30.setName("riSousMenu_bt30");
              riSousMenu_bt30.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt30ActionPerformed(e);
                }
              });
              riSousMenu30.add(riSousMenu_bt30);
            }
            menus_haut.add(riSousMenu30);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Documents li\u00e9s");
              riSousMenu_bt14.setToolTipText("Documents li\u00e9s");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);

            //======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");

              //---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Calculette de solde partiel");
              riSousMenu_bt15.setToolTipText("Calculette de solde partiel");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed(e);
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);

            //======== riSousMenu16 ========
            {
              riSousMenu16.setName("riSousMenu16");

              //---- riSousMenu_bt16 ----
              riSousMenu_bt16.setText("Edition tableur");
              riSousMenu_bt16.setToolTipText("Edition tableur");
              riSousMenu_bt16.setName("riSousMenu_bt16");
              riSousMenu_bt16.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt16ActionPerformed(e);
                }
              });
              riSousMenu16.add(riSousMenu_bt16);
            }
            menus_haut.add(riSousMenu16);

            //======== riSousMenu17 ========
            {
              riSousMenu17.setName("riSousMenu17");

              //---- riSousMenu_bt17 ----
              riSousMenu_bt17.setText("Impression exercice 1");
              riSousMenu_bt17.setToolTipText("Impression exercice 1");
              riSousMenu_bt17.setName("riSousMenu_bt17");
              riSousMenu_bt17.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt17ActionPerformed(e);
                }
              });
              riSousMenu17.add(riSousMenu_bt17);
            }
            menus_haut.add(riSousMenu17);

            //======== riSousMenu22 ========
            {
              riSousMenu22.setName("riSousMenu22");

              //---- riSousMenu_bt22 ----
              riSousMenu_bt22.setText("Impression exercice 2");
              riSousMenu_bt22.setToolTipText("Impression exercice 2");
              riSousMenu_bt22.setName("riSousMenu_bt22");
              riSousMenu_bt22.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt22ActionPerformed(e);
                }
              });
              riSousMenu22.add(riSousMenu_bt22);
            }
            menus_haut.add(riSousMenu22);

            //======== riMenu4 ========
            {
              riMenu4.setName("riMenu4");

              //---- riMenu_bt4 ----
              riMenu_bt4.setText("Fonctions");
              riMenu_bt4.setName("riMenu_bt4");
              riMenu4.add(riMenu_bt4);
            }
            menus_haut.add(riMenu4);

            //======== riSousMenu18 ========
            {
              riSousMenu18.setName("riSousMenu18");

              //---- riSousMenu_bt18 ----
              riSousMenu_bt18.setText("Plan comptable");
              riSousMenu_bt18.setToolTipText("Plan comptable");
              riSousMenu_bt18.setName("riSousMenu_bt18");
              riSousMenu_bt18.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt18ActionPerformed(e);
                }
              });
              riSousMenu18.add(riSousMenu_bt18);
            }
            menus_haut.add(riSousMenu18);

            //======== riSousMenu19 ========
            {
              riSousMenu19.setName("riSousMenu19");

              //---- riSousMenu_bt19 ----
              riSousMenu_bt19.setText("Fiche r\u00e9vision");
              riSousMenu_bt19.setToolTipText("Fiche r\u00e9vision");
              riSousMenu_bt19.setName("riSousMenu_bt19");
              riSousMenu_bt19.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt19ActionPerformed(e);
                }
              });
              riSousMenu19.add(riSousMenu_bt19);
            }
            menus_haut.add(riSousMenu19);

            //======== riSousMenu20 ========
            {
              riSousMenu20.setName("riSousMenu20");

              //---- riSousMenu_bt20 ----
              riSousMenu_bt20.setText("Ech\u00e9ance ou analytique");
              riSousMenu_bt20.setToolTipText("Affichage \u00e9ch\u00e9ance ou analytique/libell\u00e9");
              riSousMenu_bt20.setName("riSousMenu_bt20");
              riSousMenu_bt20.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt20ActionPerformed(e);
                }
              });
              riSousMenu20.add(riSousMenu_bt20);
            }
            menus_haut.add(riSousMenu20);

            //======== riSousMenu21 ========
            {
              riSousMenu21.setName("riSousMenu21");

              //---- riSousMenu_bt21 ----
              riSousMenu_bt21.setText("Ecritures pr\u00e9visonnelles");
              riSousMenu_bt21.setToolTipText("Ecritures pr\u00e9visonnelles");
              riSousMenu_bt21.setName("riSousMenu_bt21");
              riSousMenu_bt21.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt21ActionPerformed(e);
                }
              });
              riSousMenu21.add(riSousMenu_bt21);
            }
            menus_haut.add(riSousMenu21);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1070, 585));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(1070, 585));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

            //---- OBJ_195 ----
            OBJ_195.setText("Lettrage");
            OBJ_195.setName("OBJ_195");

            //---- WPTG ----
            WPTG.setName("WPTG");

            //---- OBJ_196 ----
            OBJ_196.setText("Monnaie");
            OBJ_196.setName("OBJ_196");

            //---- WLDEV ----
            WLDEV.setName("WLDEV");

            //---- OBJ_192 ----
            OBJ_192.setText("");
            OBJ_192.setToolTipText("Composer le num\u00e9ro de t\u00e9l\u00e9phone");
            OBJ_192.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_192.setName("OBJ_192");
            OBJ_192.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_192ActionPerformed(e);
              }
            });

            //---- WTEL ----
            WTEL.setToolTipText("Num\u00e9ro de t\u00e9l\u00e9phone");
            WTEL.setName("WTEL");

            //---- OBJ_203 ----
            OBJ_203.setText("@W1ATT@");
            OBJ_203.setForeground(Color.red);
            OBJ_203.setName("OBJ_203");

            //---- OBJ_198 ----
            OBJ_198.setText("Exercice 1");
            OBJ_198.setToolTipText("Solde exercice 2");
            OBJ_198.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_198.setFont(OBJ_198.getFont().deriveFont(OBJ_198.getFont().getStyle() & ~Font.BOLD));
            OBJ_198.setName("OBJ_198");
            OBJ_198.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_198ActionPerformed(e);
              }
            });

            //---- OBJ_199 ----
            OBJ_199.setText("Exercice 2");
            OBJ_199.setToolTipText("Solde exercice 1");
            OBJ_199.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_199.setName("OBJ_199");
            OBJ_199.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_199ActionPerformed(e);
              }
            });

            //---- CPSOCD ----
            CPSOCD.setHorizontalAlignment(SwingConstants.RIGHT);
            CPSOCD.setName("CPSOCD");

            //---- CPSENS ----
            CPSENS.setToolTipText("*** = Pr\u00e9visionnel");
            CPSENS.setName("CPSENS");

            GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
            xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
            xTitledPanel1ContentContainerLayout.setHorizontalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(OBJ_195, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                  .addGap(8, 8, 8)
                  .addComponent(WPTG, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addGap(15, 15, 15)
                  .addComponent(OBJ_196, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                  .addGap(2, 2, 2)
                  .addComponent(WLDEV, GroupLayout.PREFERRED_SIZE, 178, GroupLayout.PREFERRED_SIZE)
                  .addGap(12, 12, 12)
                  .addComponent(WTEL, GroupLayout.PREFERRED_SIZE, 169, GroupLayout.PREFERRED_SIZE)
                  .addGap(6, 6, 6)
                  .addComponent(OBJ_192, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                  .addGap(12, 12, 12)
                  .addComponent(OBJ_203, GroupLayout.PREFERRED_SIZE, 177, GroupLayout.PREFERRED_SIZE)
                  .addGap(13, 13, 13)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_198, GroupLayout.PREFERRED_SIZE, 101, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_199, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addComponent(CPSOCD, GroupLayout.PREFERRED_SIZE, 129, GroupLayout.PREFERRED_SIZE)
                  .addGap(6, 6, 6)
                  .addComponent(CPSENS, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
            );
            xTitledPanel1ContentContainerLayout.setVerticalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_195, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(6, 6, 6)
                  .addComponent(WPTG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_196, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(6, 6, 6)
                  .addComponent(WLDEV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(6, 6, 6)
                  .addComponent(WTEL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_192, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(11, 11, 11)
                  .addComponent(OBJ_203, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(6, 6, 6)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_198)
                    .addComponent(OBJ_199)))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(6, 6, 6)
                  .addComponent(CPSOCD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(6, 6, 6)
                  .addComponent(CPSENS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            );
          }
          p_contenu.add(xTitledPanel1);
          xTitledPanel1.setBounds(13, 14, 1060, 63);

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //---- rap01 ----
            rap01.setBackground(new Color(0, 102, 102));
            rap01.setOpaque(true);
            rap01.setToolTipText("Ecriture rapproch\u00e9e");
            rap01.setName("rap01");
            xTitledPanel2ContentContainer.add(rap01);
            rap01.setBounds(160, 32, 8, 9);

            //---- rap02 ----
            rap02.setBackground(new Color(0, 102, 102));
            rap02.setOpaque(true);
            rap02.setToolTipText("Ecriture rapproch\u00e9e");
            rap02.setName("rap02");
            xTitledPanel2ContentContainer.add(rap02);
            rap02.setBounds(160, 48, 8, 9);

            //---- rap03 ----
            rap03.setBackground(new Color(0, 102, 102));
            rap03.setOpaque(true);
            rap03.setToolTipText("Ecriture rapproch\u00e9e");
            rap03.setName("rap03");
            xTitledPanel2ContentContainer.add(rap03);
            rap03.setBounds(160, 64, 8, 9);

            //---- rap04 ----
            rap04.setBackground(new Color(0, 102, 102));
            rap04.setOpaque(true);
            rap04.setToolTipText("Ecriture rapproch\u00e9e");
            rap04.setName("rap04");
            xTitledPanel2ContentContainer.add(rap04);
            rap04.setBounds(160, 80, 8, 9);

            //---- rap05 ----
            rap05.setBackground(new Color(0, 102, 102));
            rap05.setOpaque(true);
            rap05.setToolTipText("Ecriture rapproch\u00e9e");
            rap05.setName("rap05");
            xTitledPanel2ContentContainer.add(rap05);
            rap05.setBounds(160, 96, 8, 9);

            //---- rap06 ----
            rap06.setBackground(new Color(0, 102, 102));
            rap06.setOpaque(true);
            rap06.setToolTipText("Ecriture rapproch\u00e9e");
            rap06.setName("rap06");
            xTitledPanel2ContentContainer.add(rap06);
            rap06.setBounds(160, 112, 8, 9);

            //---- rap07 ----
            rap07.setBackground(new Color(0, 102, 102));
            rap07.setOpaque(true);
            rap07.setToolTipText("Ecriture rapproch\u00e9e");
            rap07.setName("rap07");
            xTitledPanel2ContentContainer.add(rap07);
            rap07.setBounds(160, 128, 8, 9);

            //---- rap08 ----
            rap08.setBackground(new Color(0, 102, 102));
            rap08.setOpaque(true);
            rap08.setToolTipText("Ecriture rapproch\u00e9e");
            rap08.setName("rap08");
            xTitledPanel2ContentContainer.add(rap08);
            rap08.setBounds(160, 144, 8, 9);

            //---- rap09 ----
            rap09.setBackground(new Color(0, 102, 102));
            rap09.setOpaque(true);
            rap09.setToolTipText("Ecriture rapproch\u00e9e");
            rap09.setName("rap09");
            xTitledPanel2ContentContainer.add(rap09);
            rap09.setBounds(160, 160, 8, 9);

            //---- rap10 ----
            rap10.setBackground(new Color(0, 102, 102));
            rap10.setOpaque(true);
            rap10.setToolTipText("Ecriture rapproch\u00e9e");
            rap10.setName("rap10");
            xTitledPanel2ContentContainer.add(rap10);
            rap10.setBounds(160, 176, 8, 9);

            //---- rap11 ----
            rap11.setBackground(new Color(0, 102, 102));
            rap11.setOpaque(true);
            rap11.setToolTipText("Ecriture rapproch\u00e9e");
            rap11.setName("rap11");
            xTitledPanel2ContentContainer.add(rap11);
            rap11.setBounds(160, 192, 8, 9);

            //---- rap12 ----
            rap12.setBackground(new Color(0, 102, 102));
            rap12.setOpaque(true);
            rap12.setToolTipText("Ecriture rapproch\u00e9e");
            rap12.setName("rap12");
            xTitledPanel2ContentContainer.add(rap12);
            rap12.setBounds(160, 208, 8, 9);

            //---- rap13 ----
            rap13.setBackground(new Color(0, 102, 102));
            rap13.setOpaque(true);
            rap13.setToolTipText("Ecriture rapproch\u00e9e");
            rap13.setName("rap13");
            xTitledPanel2ContentContainer.add(rap13);
            rap13.setBounds(160, 224, 8, 9);

            //---- rap14 ----
            rap14.setBackground(new Color(0, 102, 102));
            rap14.setOpaque(true);
            rap14.setToolTipText("Ecriture rapproch\u00e9e");
            rap14.setName("rap14");
            xTitledPanel2ContentContainer.add(rap14);
            rap14.setBounds(160, 240, 8, 9);

            //---- rap15 ----
            rap15.setBackground(new Color(0, 102, 102));
            rap15.setOpaque(true);
            rap15.setToolTipText("Ecriture rapproch\u00e9e");
            rap15.setName("rap15");
            xTitledPanel2ContentContainer.add(rap15);
            rap15.setBounds(160, 256, 8, 9);

            //---- rap16 ----
            rap16.setBackground(new Color(0, 102, 102));
            rap16.setOpaque(true);
            rap16.setToolTipText("Ecriture rapproch\u00e9e");
            rap16.setName("rap16");
            xTitledPanel2ContentContainer.add(rap16);
            rap16.setBounds(160, 272, 8, 9);

            //---- rap17 ----
            rap17.setBackground(new Color(0, 102, 102));
            rap17.setOpaque(true);
            rap17.setToolTipText("Ecriture rapproch\u00e9e");
            rap17.setName("rap17");
            xTitledPanel2ContentContainer.add(rap17);
            rap17.setBounds(160, 288, 8, 9);

            //---- rap18 ----
            rap18.setBackground(new Color(0, 102, 102));
            rap18.setOpaque(true);
            rap18.setToolTipText("Ecriture rapproch\u00e9e");
            rap18.setName("rap18");
            xTitledPanel2ContentContainer.add(rap18);
            rap18.setBounds(160, 304, 8, 9);

            //---- rap19 ----
            rap19.setBackground(new Color(0, 102, 102));
            rap19.setOpaque(true);
            rap19.setToolTipText("Ecriture rapproch\u00e9e");
            rap19.setName("rap19");
            xTitledPanel2ContentContainer.add(rap19);
            rap19.setBounds(160, 320, 8, 9);

            //---- rap20 ----
            rap20.setBackground(new Color(0, 102, 102));
            rap20.setOpaque(true);
            rap20.setToolTipText("Ecriture rapproch\u00e9e");
            rap20.setName("rap20");
            xTitledPanel2ContentContainer.add(rap20);
            rap20.setBounds(160, 336, 8, 9);

            //---- lit01 ----
            lit01.setBackground(new Color(255, 51, 0));
            lit01.setOpaque(true);
            lit01.setToolTipText("Ecriture en litige");
            lit01.setName("lit01");
            xTitledPanel2ContentContainer.add(lit01);
            lit01.setBounds(175, 32, 8, 9);

            //---- lit02 ----
            lit02.setBackground(new Color(255, 51, 0));
            lit02.setOpaque(true);
            lit02.setToolTipText("Ecriture en litige");
            lit02.setName("lit02");
            xTitledPanel2ContentContainer.add(lit02);
            lit02.setBounds(175, 48, 8, 9);

            //---- lit03 ----
            lit03.setBackground(new Color(255, 51, 0));
            lit03.setOpaque(true);
            lit03.setToolTipText("Ecriture en litige");
            lit03.setName("lit03");
            xTitledPanel2ContentContainer.add(lit03);
            lit03.setBounds(175, 64, 8, 9);

            //---- lit04 ----
            lit04.setBackground(new Color(255, 51, 0));
            lit04.setOpaque(true);
            lit04.setToolTipText("Ecriture en litige");
            lit04.setName("lit04");
            xTitledPanel2ContentContainer.add(lit04);
            lit04.setBounds(175, 80, 8, 9);

            //---- lit05 ----
            lit05.setBackground(new Color(255, 51, 0));
            lit05.setOpaque(true);
            lit05.setToolTipText("Ecriture en litige");
            lit05.setName("lit05");
            xTitledPanel2ContentContainer.add(lit05);
            lit05.setBounds(175, 96, 8, 9);

            //---- lit06 ----
            lit06.setBackground(new Color(255, 51, 0));
            lit06.setOpaque(true);
            lit06.setToolTipText("Ecriture en litige");
            lit06.setName("lit06");
            xTitledPanel2ContentContainer.add(lit06);
            lit06.setBounds(175, 112, 8, 9);

            //---- lit07 ----
            lit07.setBackground(new Color(255, 51, 0));
            lit07.setOpaque(true);
            lit07.setToolTipText("Ecriture en litige");
            lit07.setName("lit07");
            xTitledPanel2ContentContainer.add(lit07);
            lit07.setBounds(175, 128, 8, 9);

            //---- lit08 ----
            lit08.setBackground(new Color(255, 51, 0));
            lit08.setOpaque(true);
            lit08.setToolTipText("Ecriture en litige");
            lit08.setName("lit08");
            xTitledPanel2ContentContainer.add(lit08);
            lit08.setBounds(175, 144, 8, 9);

            //---- lit09 ----
            lit09.setBackground(new Color(255, 51, 0));
            lit09.setOpaque(true);
            lit09.setToolTipText("Ecriture en litige");
            lit09.setName("lit09");
            xTitledPanel2ContentContainer.add(lit09);
            lit09.setBounds(175, 160, 8, 9);

            //---- lit10 ----
            lit10.setBackground(new Color(255, 51, 0));
            lit10.setOpaque(true);
            lit10.setToolTipText("Ecriture en litige");
            lit10.setName("lit10");
            xTitledPanel2ContentContainer.add(lit10);
            lit10.setBounds(175, 176, 8, 9);

            //---- lit11 ----
            lit11.setBackground(new Color(255, 51, 0));
            lit11.setOpaque(true);
            lit11.setToolTipText("Ecriture en litige");
            lit11.setName("lit11");
            xTitledPanel2ContentContainer.add(lit11);
            lit11.setBounds(175, 192, 8, 9);

            //---- lit12 ----
            lit12.setBackground(new Color(255, 51, 0));
            lit12.setOpaque(true);
            lit12.setToolTipText("Ecriture en litige");
            lit12.setName("lit12");
            xTitledPanel2ContentContainer.add(lit12);
            lit12.setBounds(175, 208, 8, 9);

            //---- lit13 ----
            lit13.setBackground(new Color(255, 51, 0));
            lit13.setOpaque(true);
            lit13.setToolTipText("Ecriture en litige");
            lit13.setName("lit13");
            xTitledPanel2ContentContainer.add(lit13);
            lit13.setBounds(175, 224, 8, 9);

            //---- lit14 ----
            lit14.setBackground(new Color(255, 51, 0));
            lit14.setOpaque(true);
            lit14.setToolTipText("Ecriture en litige");
            lit14.setName("lit14");
            xTitledPanel2ContentContainer.add(lit14);
            lit14.setBounds(175, 240, 8, 9);

            //---- lit15 ----
            lit15.setBackground(new Color(255, 51, 0));
            lit15.setOpaque(true);
            lit15.setToolTipText("Ecriture en litige");
            lit15.setName("lit15");
            xTitledPanel2ContentContainer.add(lit15);
            lit15.setBounds(175, 256, 8, 9);

            //---- lit16 ----
            lit16.setBackground(new Color(255, 51, 0));
            lit16.setOpaque(true);
            lit16.setToolTipText("Ecriture en litige");
            lit16.setName("lit16");
            xTitledPanel2ContentContainer.add(lit16);
            lit16.setBounds(175, 272, 8, 9);

            //---- lit17 ----
            lit17.setBackground(new Color(255, 51, 0));
            lit17.setOpaque(true);
            lit17.setToolTipText("Ecriture en litige");
            lit17.setName("lit17");
            xTitledPanel2ContentContainer.add(lit17);
            lit17.setBounds(175, 288, 8, 9);

            //---- lit18 ----
            lit18.setBackground(new Color(255, 51, 0));
            lit18.setOpaque(true);
            lit18.setToolTipText("Ecriture en litige");
            lit18.setName("lit18");
            xTitledPanel2ContentContainer.add(lit18);
            lit18.setBounds(175, 304, 8, 9);

            //---- lit19 ----
            lit19.setBackground(new Color(255, 51, 0));
            lit19.setOpaque(true);
            lit19.setToolTipText("Ecriture en litige");
            lit19.setName("lit19");
            xTitledPanel2ContentContainer.add(lit19);
            lit19.setBounds(175, 320, 8, 9);

            //---- lit20 ----
            lit20.setBackground(new Color(255, 51, 0));
            lit20.setOpaque(true);
            lit20.setToolTipText("Ecriture en litige");
            lit20.setName("lit20");
            xTitledPanel2ContentContainer.add(lit20);
            lit20.setBounds(175, 336, 8, 9);

            //======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

              //---- TE01 ----
              TE01.setComponentPopupMenu(BTD);
              TE01.setPreferredSize(new Dimension(1050, 320));
              TE01.setName("TE01");
              TE01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  TE01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST.setViewportView(TE01);
            }
            xTitledPanel2ContentContainer.add(SCROLLPANE_LIST);
            SCROLLPANE_LIST.setBounds(15, 5, 1000, 350);

            //---- BT_PDEB ----
            BT_PDEB.setText("");
            BT_PDEB.setToolTipText("D\u00e9but de la liste");
            BT_PDEB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PDEB.setName("BT_PDEB");
            BT_PDEB.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_77ActionPerformed(e);
              }
            });
            xTitledPanel2ContentContainer.add(BT_PDEB);
            BT_PDEB.setBounds(1020, 5, 25, 30);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            xTitledPanel2ContentContainer.add(BT_PGUP);
            BT_PGUP.setBounds(1020, 40, 25, 130);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            xTitledPanel2ContentContainer.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(1020, 190, 25, 130);

            //---- BT_PFIN ----
            BT_PFIN.setText("");
            BT_PFIN.setToolTipText("Fin de la liste");
            BT_PFIN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PFIN.setName("BT_PFIN");
            BT_PFIN.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_79ActionPerformed(e);
              }
            });
            xTitledPanel2ContentContainer.add(BT_PFIN);
            BT_PFIN.setBounds(1020, 325, 25, 30);

            //======== panel1 ========
            {
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- BT_TE01 ----
              BT_TE01.setText("");
              BT_TE01.setToolTipText("<HTML>Le bloc-notes associ\u00e9 \u00e0<BR>cette ligne contient un texte</HTML>");
              BT_TE01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_TE01.setName("BT_TE01");
              BT_TE01.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_208ActionPerformed(e);
                }
              });
              panel1.add(BT_TE01);
              BT_TE01.setBounds(0, 25, 13, 16);

              //---- BT_TE02 ----
              BT_TE02.setText("");
              BT_TE02.setToolTipText("<HTML>Le bloc-notes associ\u00e9 \u00e0<BR>cette ligne contient un texte</HTML>");
              BT_TE02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_TE02.setName("BT_TE02");
              BT_TE02.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_213ActionPerformed(e);
                }
              });
              panel1.add(BT_TE02);
              BT_TE02.setBounds(0, 41, 13, 16);

              //---- BT_TE03 ----
              BT_TE03.setText("");
              BT_TE03.setToolTipText("<HTML>Le bloc-notes associ\u00e9 \u00e0<BR>cette ligne contient un texte</HTML>");
              BT_TE03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_TE03.setName("BT_TE03");
              BT_TE03.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_216ActionPerformed(e);
                }
              });
              panel1.add(BT_TE03);
              BT_TE03.setBounds(0, 57, 13, 16);

              //---- BT_TE04 ----
              BT_TE04.setText("");
              BT_TE04.setToolTipText("<HTML>Le bloc-notes associ\u00e9 \u00e0<BR>cette ligne contient un texte</HTML>");
              BT_TE04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_TE04.setName("BT_TE04");
              BT_TE04.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_219ActionPerformed(e);
                }
              });
              panel1.add(BT_TE04);
              BT_TE04.setBounds(0, 73, 13, 16);

              //---- BT_TE05 ----
              BT_TE05.setText("");
              BT_TE05.setToolTipText("<HTML>Le bloc-notes associ\u00e9 \u00e0<BR>cette ligne contient un texte</HTML>");
              BT_TE05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_TE05.setName("BT_TE05");
              BT_TE05.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_225ActionPerformed(e);
                }
              });
              panel1.add(BT_TE05);
              BT_TE05.setBounds(0, 89, 13, 16);

              //---- BT_TE06 ----
              BT_TE06.setText("");
              BT_TE06.setToolTipText("<HTML>Le bloc-notes associ\u00e9 \u00e0<BR>cette ligne contient un texte</HTML>");
              BT_TE06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_TE06.setName("BT_TE06");
              BT_TE06.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_228ActionPerformed(e);
                }
              });
              panel1.add(BT_TE06);
              BT_TE06.setBounds(0, 105, 13, 16);

              //---- BT_TE07 ----
              BT_TE07.setText("");
              BT_TE07.setToolTipText("<HTML>Le bloc-notes associ\u00e9 \u00e0<BR>cette ligne contient un texte</HTML>");
              BT_TE07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_TE07.setName("BT_TE07");
              BT_TE07.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_234ActionPerformed(e);
                }
              });
              panel1.add(BT_TE07);
              BT_TE07.setBounds(0, 121, 13, 16);

              //---- BT_TE08 ----
              BT_TE08.setText("");
              BT_TE08.setToolTipText("<HTML>Le bloc-notes associ\u00e9 \u00e0<BR>cette ligne contient un texte</HTML>");
              BT_TE08.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_TE08.setName("BT_TE08");
              BT_TE08.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_237ActionPerformed(e);
                }
              });
              panel1.add(BT_TE08);
              BT_TE08.setBounds(0, 137, 13, 16);

              //---- BT_TE09 ----
              BT_TE09.setText("");
              BT_TE09.setToolTipText("<HTML>Le bloc-notes associ\u00e9 \u00e0<BR>cette ligne contient un texte</HTML>");
              BT_TE09.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_TE09.setName("BT_TE09");
              BT_TE09.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_240ActionPerformed(e);
                }
              });
              panel1.add(BT_TE09);
              BT_TE09.setBounds(0, 153, 13, 16);

              //---- BT_TE10 ----
              BT_TE10.setText("");
              BT_TE10.setToolTipText("<HTML>Le bloc-notes associ\u00e9 \u00e0<BR>cette ligne contient un texte</HTML>");
              BT_TE10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_TE10.setName("BT_TE10");
              BT_TE10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_247ActionPerformed(e);
                }
              });
              panel1.add(BT_TE10);
              BT_TE10.setBounds(0, 169, 13, 16);

              //---- BT_TE11 ----
              BT_TE11.setText("");
              BT_TE11.setToolTipText("<HTML>Le bloc-notes associ\u00e9 \u00e0<BR>cette ligne contient un texte</HTML>");
              BT_TE11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_TE11.setName("BT_TE11");
              BT_TE11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_250ActionPerformed(e);
                }
              });
              panel1.add(BT_TE11);
              BT_TE11.setBounds(0, 185, 13, 16);

              //---- BT_TE12 ----
              BT_TE12.setText("");
              BT_TE12.setToolTipText("<HTML>Le bloc-notes associ\u00e9 \u00e0<BR>cette ligne contient un texte</HTML>");
              BT_TE12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_TE12.setName("BT_TE12");
              BT_TE12.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_254ActionPerformed(e);
                }
              });
              panel1.add(BT_TE12);
              BT_TE12.setBounds(0, 201, 13, 16);

              //---- BT_TE13 ----
              BT_TE13.setText("");
              BT_TE13.setToolTipText("<HTML>Le bloc-notes associ\u00e9 \u00e0<BR>cette ligne contient un texte</HTML>");
              BT_TE13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_TE13.setName("BT_TE13");
              BT_TE13.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_257ActionPerformed(e);
                }
              });
              panel1.add(BT_TE13);
              BT_TE13.setBounds(0, 217, 13, 16);

              //---- BT_TE14 ----
              BT_TE14.setText("");
              BT_TE14.setToolTipText("<HTML>Le bloc-notes associ\u00e9 \u00e0<BR>cette ligne contient un texte</HTML>");
              BT_TE14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_TE14.setName("BT_TE14");
              BT_TE14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_260ActionPerformed(e);
                }
              });
              panel1.add(BT_TE14);
              BT_TE14.setBounds(0, 233, 13, 16);

              //---- BT_TE15 ----
              BT_TE15.setText("");
              BT_TE15.setToolTipText("<HTML>Le bloc-notes associ\u00e9 \u00e0<BR>cette ligne contient un texte</HTML>");
              BT_TE15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_TE15.setName("BT_TE15");
              BT_TE15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_264ActionPerformed(e);
                }
              });
              panel1.add(BT_TE15);
              BT_TE15.setBounds(0, 249, 13, 16);

              //---- BT_TE20 ----
              BT_TE20.setText("");
              BT_TE20.setToolTipText("<HTML>Le bloc-notes associ\u00e9 \u00e0<BR>cette ligne contient un texte</HTML>");
              BT_TE20.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_TE20.setName("BT_TE20");
              BT_TE20.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_265ActionPerformed(e);
                }
              });
              panel1.add(BT_TE20);
              BT_TE20.setBounds(0, 329, 13, 16);

              //---- BT_TE19 ----
              BT_TE19.setText("");
              BT_TE19.setToolTipText("<HTML>Le bloc-notes associ\u00e9 \u00e0<BR>cette ligne contient un texte</HTML>");
              BT_TE19.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_TE19.setName("BT_TE19");
              BT_TE19.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_261ActionPerformed(e);
                }
              });
              panel1.add(BT_TE19);
              BT_TE19.setBounds(0, 313, 13, 16);

              //---- BT_TE18 ----
              BT_TE18.setText("");
              BT_TE18.setToolTipText("<HTML>Le bloc-notes associ\u00e9 \u00e0<BR>cette ligne contient un texte</HTML>");
              BT_TE18.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_TE18.setName("BT_TE18");
              BT_TE18.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_258ActionPerformed(e);
                }
              });
              panel1.add(BT_TE18);
              BT_TE18.setBounds(0, 297, 13, 16);

              //---- BT_TE17 ----
              BT_TE17.setText("");
              BT_TE17.setToolTipText("<HTML>Le bloc-notes associ\u00e9 \u00e0<BR>cette ligne contient un texte</HTML>");
              BT_TE17.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_TE17.setName("BT_TE17");
              BT_TE17.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_255ActionPerformed(e);
                }
              });
              panel1.add(BT_TE17);
              BT_TE17.setBounds(0, 281, 13, 16);

              //---- BT_TE16 ----
              BT_TE16.setText("");
              BT_TE16.setToolTipText("<HTML>Le bloc-notes associ\u00e9 \u00e0<BR>cette ligne contient un texte</HTML>");
              BT_TE16.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_TE16.setName("BT_TE16");
              BT_TE16.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_251ActionPerformed(e);
                }
              });
              panel1.add(BT_TE16);
              BT_TE16.setBounds(0, 265, 13, 16);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel2ContentContainer.add(panel1);
            panel1.setBounds(0, 5, 14, 350);

            //======== panel2 ========
            {
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- OBJ_272 ----
              OBJ_272.setToolTipText("D\u00e9calage en moins dans la liste");
              OBJ_272.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_272.setName("OBJ_272");
              OBJ_272.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_272ActionPerformed(e);
                }
              });
              panel2.add(OBJ_272);
              OBJ_272.setBounds(180, 45, 30, 30);

              //---- OBJ_268 ----
              OBJ_268.setToolTipText("D\u00e9calage en plus dans la liste");
              OBJ_268.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_268.setName("OBJ_268");
              OBJ_268.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_268ActionPerformed(e);
                }
              });
              panel2.add(OBJ_268);
              OBJ_268.setBounds(210, 45, 30, 30);

              //---- WMOIX_SP ----
              WMOIX_SP.setToolTipText("Valeur de d\u00e9calage dans la liste");
              WMOIX_SP.setComponentPopupMenu(BTD2);
              WMOIX_SP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              WMOIX_SP.setModel(new SpinnerListModel(new String[] {" ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "90", "91", "92", "93", "94", "95", "96", "97", "98", "99"}) {
                { setValue("1"); }
              });
              WMOIX_SP.setName("WMOIX_SP");
              panel2.add(WMOIX_SP);
              WMOIX_SP.setBounds(255, 48, 50, 25);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel2ContentContainer.add(panel2);
            panel2.setBounds(735, 360, 310, 85);

            //======== panel3 ========
            {
              panel3.setOpaque(false);
              panel3.setName("panel3");

              //---- label5 ----
              label5.setText("Date d'\u00e9criture");
              label5.setName("label5");

              //---- WMOIX ----
              WMOIX.setToolTipText("Choisissez la date d'affichage des \u00e9critures (sous la forme MMAA)");
              WMOIX.setComponentPopupMenu(BTD2);
              WMOIX.setName("WMOIX");

              GroupLayout panel3Layout = new GroupLayout(panel3);
              panel3.setLayout(panel3Layout);
              panel3Layout.setHorizontalGroup(
                panel3Layout.createParallelGroup()
                  .addGroup(panel3Layout.createSequentialGroup()
                    .addGap(120, 120, 120)
                    .addComponent(label5, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                    .addGap(12, 12, 12)
                    .addComponent(WMOIX, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
              );
              panel3Layout.setVerticalGroup(
                panel3Layout.createParallelGroup()
                  .addGroup(panel3Layout.createSequentialGroup()
                    .addGap(51, 51, 51)
                    .addComponent(label5))
                  .addGroup(panel3Layout.createSequentialGroup()
                    .addGap(45, 45, 45)
                    .addComponent(WMOIX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              );
            }
            xTitledPanel2ContentContainer.add(panel3);
            panel3.setBounds(735, 360, 310, 85);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(xTitledPanel2);
          xTitledPanel2.setBounds(13, 84, 1060, 500);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_57 ----
      OBJ_57.setText("Choisir");
      OBJ_57.setName("OBJ_57");
      OBJ_57.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_57ActionPerformed(e);
        }
      });
      BTD.add(OBJ_57);

      //---- OBJ_58 ----
      OBJ_58.setText("Modifier");
      OBJ_58.setName("OBJ_58");
      OBJ_58.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_58ActionPerformed(e);
        }
      });
      BTD.add(OBJ_58);

      //---- OBJ_66 ----
      OBJ_66.setText("Affichage sur code lettrage");
      OBJ_66.setName("OBJ_66");
      OBJ_66.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_66ActionPerformed(e);
        }
      });
      BTD.add(OBJ_66);

      //---- OBJ_59 ----
      OBJ_59.setText("Saisie CEE");
      OBJ_59.setName("OBJ_59");
      OBJ_59.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_59ActionPerformed(e);
        }
      });
      BTD.add(OBJ_59);

      //---- OBJ_60 ----
      OBJ_60.setText("Note");
      OBJ_60.setName("OBJ_60");
      OBJ_60.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_60ActionPerformed(e);
        }
      });
      BTD.add(OBJ_60);

      //---- OBJ_61 ----
      OBJ_61.setText("Vue facture");
      OBJ_61.setName("OBJ_61");
      OBJ_61.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_61ActionPerformed(e);
        }
      });
      BTD.add(OBJ_61);

      //---- OBJ_62 ----
      OBJ_62.setText("Axes");
      OBJ_62.setName("OBJ_62");
      OBJ_62.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_62ActionPerformed(e);
        }
      });
      BTD.add(OBJ_62);

      //---- OBJ_63 ----
      OBJ_63.setText("R\u00e9imputation");
      OBJ_63.setName("OBJ_63");
      OBJ_63.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_63ActionPerformed(e);
        }
      });
      BTD.add(OBJ_63);

      //---- OBJ_64 ----
      OBJ_64.setText("Folios (s\u00e9quentiel)");
      OBJ_64.setName("OBJ_64");
      OBJ_64.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_64ActionPerformed(e);
        }
      });
      BTD.add(OBJ_64);

      //---- OBJ_72 ----
      OBJ_72.setText("Folios (par n\u00b0 de pi\u00e8ce)");
      OBJ_72.setName("OBJ_72");
      OBJ_72.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_72ActionPerformed(e);
        }
      });
      BTD.add(OBJ_72);

      //---- mi_NumPiece ----
      mi_NumPiece.setText("Num\u00e9ro de pi\u00e8ce");
      mi_NumPiece.setName("mi_NumPiece");
      mi_NumPiece.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_NumPieceActionPerformed(e);
        }
      });
      BTD.add(mi_NumPiece);

      //---- OBJ_67 ----
      OBJ_67.setText("Annulation folio pr\u00e9visionnel");
      OBJ_67.setName("OBJ_67");
      OBJ_67.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_67ActionPerformed(e);
        }
      });
      BTD.add(OBJ_67);

      //---- OBJ_65 ----
      OBJ_65.setText("G\u00e9n\u00e9ration d'une OD");
      OBJ_65.setName("OBJ_65");
      OBJ_65.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_65ActionPerformed(e);
        }
      });
      BTD.add(OBJ_65);

      //---- OBJ_68 ----
      OBJ_68.setText("Calculette de solde partiel");
      OBJ_68.setName("OBJ_68");
      OBJ_68.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_68ActionPerformed(e);
        }
      });
      BTD.add(OBJ_68);

      //---- OBJ_69 ----
      OBJ_69.setText("Vue effet");
      OBJ_69.setName("OBJ_69");
      OBJ_69.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_69ActionPerformed(e);
        }
      });
      BTD.add(OBJ_69);

      //---- MILienUrl ----
      MILienUrl.setText("Lien URL");
      MILienUrl.setName("MILienUrl");
      MILienUrl.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          MILienUrlActionPerformed(e);
        }
      });
      BTD.add(MILienUrl);
      BTD.addSeparator();

      //---- OBJ_71 ----
      OBJ_71.setText("Choix possibles");
      OBJ_71.setName("OBJ_71");
      OBJ_71.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_71ActionPerformed(e);
        }
      });
      BTD.add(OBJ_71);

      //---- OBJ_70 ----
      OBJ_70.setText("Aide en ligne");
      OBJ_70.setName("OBJ_70");
      OBJ_70.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_70ActionPerformed(e);
        }
      });
      BTD.add(OBJ_70);
    }

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- OBJ_74 ----
      OBJ_74.setText("Choix possibles");
      OBJ_74.setName("OBJ_74");
      OBJ_74.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_74ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_74);

      //---- OBJ_73 ----
      OBJ_73.setText("Aide en ligne");
      OBJ_73.setName("OBJ_73");
      OBJ_73.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_73ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_73);
    }

    //======== riSousMenu12 ========
    {
      riSousMenu12.setName("riSousMenu12");

      //---- riSousMenu_bt12 ----
      riSousMenu_bt12.setText("Soldes gliss\u00e9s");
      riSousMenu_bt12.setToolTipText("Visualisation des soldes gliss\u00e9s");
      riSousMenu_bt12.setName("riSousMenu_bt12");
      riSousMenu_bt12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt12ActionPerformed(e);
        }
      });
      riSousMenu12.add(riSousMenu_bt12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private XRiTextField INDETB;
  private XRiTextField INDNCG;
  private XRiTextField INDNCA;
  private RiZoneSortie OBJ_120;
  private SNBoutonDetail OBJ_119;
  private JButton BT_CPTAV;
  private JButton BT_CPTAP;
  private JLabel label1;
  private JLabel label2;
  private RiZoneSortie WCLA1;
  private RiZoneSortie WCLA2;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_del;
  private RiSousMenu_bt riSousMenu_bt_del;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu13;
  private RiSousMenu_bt riSousMenu_bt13;
  private RiSousMenu riSousMenu30;
  private RiSousMenu_bt riSousMenu_bt30;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private RiSousMenu riSousMenu16;
  private RiSousMenu_bt riSousMenu_bt16;
  private RiSousMenu riSousMenu17;
  private RiSousMenu_bt riSousMenu_bt17;
  private RiSousMenu riSousMenu22;
  private RiSousMenu_bt riSousMenu_bt22;
  private RiMenu riMenu4;
  private RiMenu_bt riMenu_bt4;
  private RiSousMenu riSousMenu18;
  private RiSousMenu_bt riSousMenu_bt18;
  private RiSousMenu riSousMenu19;
  private RiSousMenu_bt riSousMenu_bt19;
  private RiSousMenu riSousMenu20;
  private RiSousMenu_bt riSousMenu_bt20;
  private RiSousMenu riSousMenu21;
  private RiSousMenu_bt riSousMenu_bt21;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_195;
  private XRiTextField WPTG;
  private JLabel OBJ_196;
  private XRiTextField WLDEV;
  private SNBoutonDetail OBJ_192;
  private XRiTextField WTEL;
  private JLabel OBJ_203;
  private JButton OBJ_198;
  private JButton OBJ_199;
  private XRiTextField CPSOCD;
  private XRiTextField CPSENS;
  private JXTitledPanel xTitledPanel2;
  private JLabel rap01;
  private JLabel rap02;
  private JLabel rap03;
  private JLabel rap04;
  private JLabel rap05;
  private JLabel rap06;
  private JLabel rap07;
  private JLabel rap08;
  private JLabel rap09;
  private JLabel rap10;
  private JLabel rap11;
  private JLabel rap12;
  private JLabel rap13;
  private JLabel rap14;
  private JLabel rap15;
  private JLabel rap16;
  private JLabel rap17;
  private JLabel rap18;
  private JLabel rap19;
  private JLabel rap20;
  private JLabel lit01;
  private JLabel lit02;
  private JLabel lit03;
  private JLabel lit04;
  private JLabel lit05;
  private JLabel lit06;
  private JLabel lit07;
  private JLabel lit08;
  private JLabel lit09;
  private JLabel lit10;
  private JLabel lit11;
  private JLabel lit12;
  private JLabel lit13;
  private JLabel lit14;
  private JLabel lit15;
  private JLabel lit16;
  private JLabel lit17;
  private JLabel lit18;
  private JLabel lit19;
  private JLabel lit20;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable TE01;
  private JButton BT_PDEB;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JButton BT_PFIN;
  private JPanel panel1;
  private SNBoutonDetail BT_TE01;
  private SNBoutonDetail BT_TE02;
  private SNBoutonDetail BT_TE03;
  private SNBoutonDetail BT_TE04;
  private SNBoutonDetail BT_TE05;
  private SNBoutonDetail BT_TE06;
  private SNBoutonDetail BT_TE07;
  private SNBoutonDetail BT_TE08;
  private SNBoutonDetail BT_TE09;
  private SNBoutonDetail BT_TE10;
  private SNBoutonDetail BT_TE11;
  private SNBoutonDetail BT_TE12;
  private SNBoutonDetail BT_TE13;
  private SNBoutonDetail BT_TE14;
  private SNBoutonDetail BT_TE15;
  private SNBoutonDetail BT_TE20;
  private SNBoutonDetail BT_TE19;
  private SNBoutonDetail BT_TE18;
  private SNBoutonDetail BT_TE17;
  private SNBoutonDetail BT_TE16;
  private JPanel panel2;
  private JButton OBJ_272;
  private JButton OBJ_268;
  private JSpinner WMOIX_SP;
  private JPanel panel3;
  private JLabel label5;
  private XRiTextField WMOIX;
  private JPopupMenu BTD;
  private JMenuItem OBJ_57;
  private JMenuItem OBJ_58;
  private JMenuItem OBJ_66;
  private JMenuItem OBJ_59;
  private JMenuItem OBJ_60;
  private JMenuItem OBJ_61;
  private JMenuItem OBJ_62;
  private JMenuItem OBJ_63;
  private JMenuItem OBJ_64;
  private JMenuItem OBJ_72;
  private JMenuItem mi_NumPiece;
  private JMenuItem OBJ_67;
  private JMenuItem OBJ_65;
  private JMenuItem OBJ_68;
  private JMenuItem OBJ_69;
  private JMenuItem MILienUrl;
  private JMenuItem OBJ_71;
  private JMenuItem OBJ_70;
  private JPopupMenu BTD2;
  private JMenuItem OBJ_74;
  private JMenuItem OBJ_73;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
