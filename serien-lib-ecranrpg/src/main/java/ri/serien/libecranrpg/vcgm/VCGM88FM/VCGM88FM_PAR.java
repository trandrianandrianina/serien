
package ri.serien.libecranrpg.vcgm.VCGM88FM;
// Nom Fichier: pop_VCGM88FM_FMTPAR_746.java

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;

import org.jdesktop.swingx.JXTitledSeparator;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VCGM88FM_PAR extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] _T01_Title = { "A", "", };
  private String[][] _T01_Data = { { "T01", "L01", }, { "T02", "L02", }, { "T03", "L03", }, { "T04", "L04", }, { "T05", "L05", },
      { "T06", "L06", }, { "T07", "L07", }, { "T08", "L08", }, };
  private int[] _T01_Width = { 75, 291, };
  
  public VCGM88FM_PAR(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@R40LIB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    
    // V07F
    // Titre
    // setTitle(???);
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void VALActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "Attention");
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    panel1 = new JPanel();
    xTitledSeparator1 = new JXTitledSeparator();
    SCROLLPANE_LIST = new JScrollPane();
    T01 = new XRiTable();
    BT_ENTER = new JButton();
    OBJ_18 = new JButton();
    label1 = new JLabel();
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setName("this");
    setLayout(new BorderLayout());

    //======== panel1 ========
    {
      panel1.setName("panel1");

      //---- xTitledSeparator1 ----
      xTitledSeparator1.setTitle("Param\u00e8tre ");
      xTitledSeparator1.setName("xTitledSeparator1");

      //======== SCROLLPANE_LIST ========
      {
        SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

        //---- T01 ----
        T01.setName("T01");
        SCROLLPANE_LIST.setViewportView(T01);
      }

      //---- BT_ENTER ----
      BT_ENTER.setText("Ok");
      BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ENTER.setName("BT_ENTER");
      BT_ENTER.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          VALActionPerformed(e);
        }
      });

      //---- OBJ_18 ----
      OBJ_18.setText("Retour");
      OBJ_18.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });

      //---- label1 ----
      label1.setText("@R40LIB@");
      label1.setName("label1");

      GroupLayout panel1Layout = new GroupLayout(panel1);
      panel1.setLayout(panel1Layout);
      panel1Layout.setHorizontalGroup(
        panel1Layout.createParallelGroup()
          .addGroup(panel1Layout.createSequentialGroup()
            .addGroup(panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, 600, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                  .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 575, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(BT_ENTER, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE)
                    .addGap(8, 8, 8)
                    .addComponent(OBJ_18, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE))))
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(81, 81, 81)
                .addComponent(label1, GroupLayout.PREFERRED_SIZE, 468, GroupLayout.PREFERRED_SIZE)))
            .addContainerGap(20, Short.MAX_VALUE))
      );
      panel1Layout.setVerticalGroup(
        panel1Layout.createParallelGroup()
          .addGroup(panel1Layout.createSequentialGroup()
            .addGap(15, 15, 15)
            .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(label1)
            .addGap(24, 24, 24)
            .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)
            .addGap(54, 54, 54)
            .addGroup(panel1Layout.createParallelGroup()
              .addComponent(BT_ENTER, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_18, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)))
      );
    }
    add(panel1, BorderLayout.CENTER);

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("Annuler");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      CMD.add(OBJ_6);

      //---- OBJ_7 ----
      OBJ_7.setText("Exploitation");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_9 ----
      OBJ_9.setText("Aide en ligne");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      BTD.add(OBJ_9);

      //---- OBJ_10 ----
      OBJ_10.setText("Invite");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel panel1;
  private JXTitledSeparator xTitledSeparator1;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable T01;
  private JButton BT_ENTER;
  private JButton OBJ_18;
  private JLabel label1;
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_7;
  private JPopupMenu BTD;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
