
package ri.serien.libecranrpg.vcgm.VCGM57FM;
// Nom Fichier: pop_VCGM57FM_FMTB1_847.java

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VCGM57FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _T01_Top =
      { "T01", "T02", "T03", "T04", "T05", "T06", "T07", "T08", "T09", "T10", "T11", "T12", "T13", "T14", "T15", "T16", };
  private String[] _T01_Title = { "HLD01", };
  private String[][] _T01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, { "LD16", }, };
  private int[] _T01_Width = { 481, };
  
  public VCGM57FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    // _LIST_Title_Data_Brut = initTable(LIST, "Courier New", Font.BOLD, 12, "Courier New", Font.PLAIN, 12);
    
    // Ajout
    initDiverses();
    T01.setAspectTable(_T01_Top, _T01_Title, _T01_Data, _T01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, _LIST_Title_Data_Brut, _T01_Top);
    
    
    ARG1.setVisible(lexique.isPresent("ARG1"));
    
    // TODO Icones
    VAL.setIcon(lexique.chargerImage("images/OK.png", true));
    OBJ_20.setIcon(lexique.chargerImage("images/retour.png", true));
    OBJ_62.setIcon(lexique.chargerImage("images/pgup20.png", true));
    OBJ_68.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Recherche"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void VALActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="enter"
    // ScriptCall("G_Touche")
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_Touche")
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.HostScreenSendKey(this, "ENTER", true);
    // lexique.validSelection(LIST, _T01_Top, "1", "Enter");
    T01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_62ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGUP");
  }
  
  private void OBJ_68ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGDOWN");
  }
  
  private void T01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _T01_Top, "1", "ENTER", e);
    if (T01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    panel1 = new JPanel();
    VAL = new JButton();
    OBJ_20 = new JButton();
    panel2 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    T01 = new XRiTable();
    ARG1 = new XRiTextField();
    OBJ_21_OBJ_21 = new JLabel();
    OBJ_62 = new JButton();
    OBJ_68 = new JButton();
    OBJ_4 = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();

    //======== this ========
    setName("this");

    //======== panel1 ========
    {
      panel1.setName("panel1");
      panel1.setLayout(null);

      //---- VAL ----
      VAL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      VAL.setName("VAL");
      VAL.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          VALActionPerformed(e);
        }
      });
      panel1.add(VAL);
      VAL.setBounds(10, 10, 56, 40);

      //---- OBJ_20 ----
      OBJ_20.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      panel1.add(OBJ_20);
      OBJ_20.setBounds(70, 10, 56, 40);
    }

    //======== panel2 ========
    {
      panel2.setBorder(new TitledBorder("Recherche tiers"));
      panel2.setName("panel2");

      //======== SCROLLPANE_LIST ========
      {
        SCROLLPANE_LIST.setComponentPopupMenu(BTD);
        SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

        //---- T01 ----
        T01.setComponentPopupMenu(BTD);
        T01.setName("T01");
        T01.addMouseListener(new MouseAdapter() {
          @Override
          public void mouseClicked(MouseEvent e) {
            T01MouseClicked(e);
          }
        });
        SCROLLPANE_LIST.setViewportView(T01);
      }

      //---- ARG1 ----
      ARG1.setComponentPopupMenu(BTD);
      ARG1.setName("ARG1");

      //---- OBJ_21_OBJ_21 ----
      OBJ_21_OBJ_21.setText("Recherche");
      OBJ_21_OBJ_21.setName("OBJ_21_OBJ_21");

      //---- OBJ_62 ----
      OBJ_62.setText("");
      OBJ_62.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_62.setName("OBJ_62");
      OBJ_62.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_62ActionPerformed(e);
        }
      });

      //---- OBJ_68 ----
      OBJ_68.setText("");
      OBJ_68.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_68.setName("OBJ_68");
      OBJ_68.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_68ActionPerformed(e);
        }
      });

      GroupLayout panel2Layout = new GroupLayout(panel2);
      panel2.setLayout(panel2Layout);
      panel2Layout.setHorizontalGroup(
        panel2Layout.createParallelGroup()
          .addGroup(panel2Layout.createSequentialGroup()
            .addGap(18, 18, 18)
            .addGroup(panel2Layout.createParallelGroup()
              .addGroup(panel2Layout.createSequentialGroup()
                .addComponent(OBJ_21_OBJ_21, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ARG1, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel2Layout.createSequentialGroup()
                .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 460, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel2Layout.createParallelGroup()
                  .addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))))
            .addContainerGap(19, Short.MAX_VALUE))
      );
      panel2Layout.setVerticalGroup(
        panel2Layout.createParallelGroup()
          .addGroup(panel2Layout.createSequentialGroup()
            .addGap(12, 12, 12)
            .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
              .addComponent(OBJ_21_OBJ_21, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(ARG1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
              .addGroup(panel2Layout.createSequentialGroup()
                .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE))
              .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 290, GroupLayout.PREFERRED_SIZE))
            .addGap(17, 17, 17))
      );
    }

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(20, 20, 20)
          .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
          .addContainerGap(450, Short.MAX_VALUE)
          .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(20, 20, 20)
          .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 385, GroupLayout.PREFERRED_SIZE)
          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
    );

    //======== OBJ_4 ========
    {
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("Annuler");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_6);

      //---- OBJ_7 ----
      OBJ_7.setText("Exploitation");
      OBJ_7.setName("OBJ_7");
      OBJ_4.add(OBJ_7);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel panel1;
  private JButton VAL;
  private JButton OBJ_20;
  private JPanel panel2;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable T01;
  private XRiTextField ARG1;
  private JLabel OBJ_21_OBJ_21;
  private JButton OBJ_62;
  private JButton OBJ_68;
  private JPopupMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_7;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
