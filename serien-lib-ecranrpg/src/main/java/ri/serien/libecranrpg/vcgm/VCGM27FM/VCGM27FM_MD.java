
package ri.serien.libecranrpg.vcgm.VCGM27FM;
// Nom Fichier: pop_VCGM27FM_FMTMD_315.java

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;

import org.jdesktop.swingx.JXTitledSeparator;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class VCGM27FM_MD extends SNPanelEcranRPG implements ioFrame {
  
  
  public VCGM27FM_MD(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Choix fonctions"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "R", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "C", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "M", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "I", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "A", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    OBJ_7 = new JButton();
    OBJ_8 = new JButton();
    OBJ_9 = new JButton();
    OBJ_10 = new JButton();
    OBJ_11 = new JButton();
    xTitledSeparator1 = new JXTitledSeparator();

    //======== this ========
    setName("this");

    //---- OBJ_7 ----
    OBJ_7.setText("Retour");
    OBJ_7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_7.setName("OBJ_7");
    OBJ_7.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_7ActionPerformed(e);
      }
    });

    //---- OBJ_8 ----
    OBJ_8.setText("Cr\u00e9ation");
    OBJ_8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_8.setName("OBJ_8");
    OBJ_8.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_8ActionPerformed(e);
      }
    });

    //---- OBJ_9 ----
    OBJ_9.setText("Modification");
    OBJ_9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_9.setName("OBJ_9");
    OBJ_9.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_9ActionPerformed(e);
      }
    });

    //---- OBJ_10 ----
    OBJ_10.setText("Interrogation");
    OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_10.setName("OBJ_10");
    OBJ_10.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_10ActionPerformed(e);
      }
    });

    //---- OBJ_11 ----
    OBJ_11.setText("Annulation");
    OBJ_11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_11.setName("OBJ_11");
    OBJ_11.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_11ActionPerformed(e);
      }
    });

    //---- xTitledSeparator1 ----
    xTitledSeparator1.setTitle("Fonctions");
    xTitledSeparator1.setName("xTitledSeparator1");

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(20, 20, 20)
          .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(30, 30, 30)
          .addComponent(OBJ_7, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(30, 30, 30)
          .addComponent(OBJ_8, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(30, 30, 30)
          .addComponent(OBJ_9, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(30, 30, 30)
          .addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(30, 30, 30)
          .addComponent(OBJ_11, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(10, 10, 10)
          .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          .addGap(16, 16, 16)
          .addComponent(OBJ_7, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
          .addGap(11, 11, 11)
          .addComponent(OBJ_8, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
          .addGap(6, 6, 6)
          .addComponent(OBJ_9, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
          .addGap(6, 6, 6)
          .addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
          .addGap(11, 11, 11)
          .addComponent(OBJ_11, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
    );
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JButton OBJ_7;
  private JButton OBJ_8;
  private JButton OBJ_9;
  private JButton OBJ_10;
  private JButton OBJ_11;
  private JXTitledSeparator xTitledSeparator1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
