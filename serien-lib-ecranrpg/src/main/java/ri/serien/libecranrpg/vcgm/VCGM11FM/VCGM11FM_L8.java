
package ri.serien.libecranrpg.vcgm.VCGM11FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGM11FM_L8 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] L1SNS_Value = { "", "D", "C", };
  private String[] L1SNS_Title = { "", "Débit", "Crédit", };
  private boolean dup = false;
  
  public VCGM11FM_L8(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    // setRefreshPanelUnderMe(false);
    
    // Ajout
    initDiverses();
    L1SNS.setValeurs(L1SNS_Value, L1SNS_Title);
    L1VTL.setValeursSelection("1", " ");
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    message.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W1ATT@")).trim());
    W2SNS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W2SNS@")).trim());
    OBJ_17_OBJ_17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMTT@")).trim());
    OBJ_50_OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAXE1@")).trim());
    OBJ_51_OBJ_51.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAXE2@")).trim());
    OBJ_52_OBJ_52.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAXE3@")).trim());
    OBJ_53_OBJ_53.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAXE4@")).trim());
    OBJ_54_OBJ_54.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAXE5@")).trim());
    OBJ_55_OBJ_55.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAXE6@")).trim());
    TTVA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TTVA@")).trim());
    LIBAX1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAX1@")).trim());
    LIBAX2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAX2@")).trim());
    LIBAX3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAX3@")).trim());
    LIBAX4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAX4@")).trim());
    LIBAX5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAX5@")).trim());
    LIBAX6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAX6@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    


    
    
    OBJ_80_OBJ_80.setVisible(lexique.isPresent("L1DTJ"));
    OBJ_56_OBJ_56.setVisible(L1QTE.isVisible());
    OBJ_17_OBJ_17.setVisible(lexique.isPresent("LIBMTT"));
    OBJ_43_OBJ_43.setVisible(lexique.isPresent("L1DVBX"));
    btn_dup.setForeground((Color) lexique.getSystemDefaultColor("Label.Foreground"));
    
    if (lexique.isTrue("79")) {
      message.setForeground(Color.RED);
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("LIGNE DE FOLIO"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (dup) lexique.HostFieldPutData("L1DUPX", 0, "*");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(18, 33);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(17, 9);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11", false);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    if (btn_dup.getForeground().equals(Color.GREEN)) {
      btn_dup.setForeground((Color) lexique.getSystemDefaultColor("Label.Foreground"));
      lexique.HostFieldPutData("L1DUPX", 0, "");
    }
    else {
      btn_dup.setForeground(Color.GREEN);
      lexique.HostFieldPutData("L1DUPX", 0, "*");
      // btn_dup.setNextFocusableComponent(L1SAN);
      L1SAN.requestFocusInWindow();
    }
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "DUPLICATE", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    LIBCPT = new XRiTextField();
    message = new RiZoneSortie();
    USOLDE = new XRiTextField();
    OBJ_64_OBJ_64 = new JLabel();
    L1NCA = new XRiTextField();
    L1NCGX = new XRiTextField();
    L1NLI = new XRiTextField();
    L1DTJ = new XRiTextField();
    W2SNS = new RiZoneSortie();
    OBJ_80_OBJ_80 = new JLabel();
    WCOD = new XRiTextField();
    panel2 = new JPanel();
    L1SNS = new XRiComboBox();
    L1MTT = new XRiTextField();
    OBJ_17_OBJ_17 = new JLabel();
    OBJ_18_OBJ_18 = new JLabel();
    OBJ_41_OBJ_41 = new JLabel();
    L1RFC = new XRiTextField();
    OBJ_42_OBJ_42 = new JLabel();
    OBJ_40_OBJ_40 = new JLabel();
    OBJ_39_OBJ_39 = new JLabel();
    L1LIB = new XRiTextField();
    L1CLB = new XRiTextField();
    L1PCE = new XRiTextField();
    OBJ_56_OBJ_56 = new JLabel();
    OBJ_50_OBJ_50 = new JLabel();
    OBJ_51_OBJ_51 = new JLabel();
    OBJ_52_OBJ_52 = new JLabel();
    OBJ_53_OBJ_53 = new JLabel();
    OBJ_54_OBJ_54 = new JLabel();
    OBJ_55_OBJ_55 = new JLabel();
    L1QTE = new XRiTextField();
    L1AA1 = new XRiTextField();
    L1AA2 = new XRiTextField();
    L1AA3 = new XRiTextField();
    L1AA4 = new XRiTextField();
    L1AA5 = new XRiTextField();
    L1AA6 = new XRiTextField();
    W1LIBS = new XRiTextField();
    OBJ_43_OBJ_43 = new JLabel();
    L1DVBX = new XRiTextField();
    xTitledSeparator2 = new JXTitledSeparator();
    OBJ_20 = new JLabel();
    L1SAN = new XRiTextField();
    OBJ_21 = new JLabel();
    L1ACT = new XRiTextField();
    OBJ_22 = new JLabel();
    L1NAT = new XRiTextField();
    L1VTL = new XRiCheckBox();
    OBJ_57_OBJ_57 = new JLabel();
    L1NCPX = new XRiTextField();
    WLNCPT = new XRiTextField();
    btn_dup = new JButton();
    OBJ_23 = new JLabel();
    panel3 = new JPanel();
    L1IN6 = new XRiTextField();
    OBJ_53 = new JLabel();
    TTVA = new RiZoneSortie();
    OBJ_51 = new JLabel();
    L1AFAL = new XRiTextField();
    LIBAX1 = new RiZoneSortie();
    LIBAX2 = new RiZoneSortie();
    LIBAX3 = new RiZoneSortie();
    LIBAX4 = new RiZoneSortie();
    LIBAX5 = new RiZoneSortie();
    LIBAX6 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1210, 440));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Vue du compte");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Plan comptable");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("For\u00e7age en devises");
              riSousMenu_bt8.setToolTipText("For\u00e7age en saisie sur comptes en devises");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Demande de lettrage");
              riSousMenu_bt9.setToolTipText("Demande de lettrage en cours de saisie");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Bloc-notes");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);

            //======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");

              //---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Documents li\u00e9s");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed(e);
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- LIBCPT ----
          LIBCPT.setComponentPopupMenu(BTD);
          LIBCPT.setName("LIBCPT");
          panel1.add(LIBCPT);
          LIBCPT.setBounds(21, 86, 310, LIBCPT.getPreferredSize().height);

          //---- message ----
          message.setText("@W1ATT@");
          message.setForeground(Color.black);
          message.setFont(message.getFont().deriveFont(message.getFont().getStyle() | Font.BOLD));
          message.setName("message");
          panel1.add(message);
          message.setBounds(21, 121, 309, message.getPreferredSize().height);

          //---- USOLDE ----
          USOLDE.setComponentPopupMenu(BTD);
          USOLDE.setName("USOLDE");
          panel1.add(USOLDE);
          USOLDE.setBounds(120, 230, 124, USOLDE.getPreferredSize().height);

          //---- OBJ_64_OBJ_64 ----
          OBJ_64_OBJ_64.setText("Solde compte");
          OBJ_64_OBJ_64.setFont(OBJ_64_OBJ_64.getFont().deriveFont(OBJ_64_OBJ_64.getFont().getStyle() | Font.BOLD));
          OBJ_64_OBJ_64.setName("OBJ_64_OBJ_64");
          panel1.add(OBJ_64_OBJ_64);
          OBJ_64_OBJ_64.setBounds(25, 230, 90, 28);

          //---- L1NCA ----
          L1NCA.setComponentPopupMenu(BTD);
          L1NCA.setEditable(false);
          L1NCA.setName("L1NCA");
          panel1.add(L1NCA);
          L1NCA.setBounds(156, 51, 68, L1NCA.getPreferredSize().height);

          //---- L1NCGX ----
          L1NCGX.setComponentPopupMenu(BTD);
          L1NCGX.setEditable(false);
          L1NCGX.setName("L1NCGX");
          panel1.add(L1NCGX);
          L1NCGX.setBounds(86, 51, 68, L1NCGX.getPreferredSize().height);

          //---- L1NLI ----
          L1NLI.setComponentPopupMenu(BTD);
          L1NLI.setEditable(false);
          L1NLI.setName("L1NLI");
          panel1.add(L1NLI);
          L1NLI.setBounds(46, 51, 36, L1NLI.getPreferredSize().height);

          //---- L1DTJ ----
          L1DTJ.setComponentPopupMenu(BTD);
          L1DTJ.setNextFocusableComponent(L1MTT);
          L1DTJ.setName("L1DTJ");
          panel1.add(L1DTJ);
          L1DTJ.setBounds(256, 51, 30, L1DTJ.getPreferredSize().height);

          //---- W2SNS ----
          W2SNS.setText("@W2SNS@");
          W2SNS.setFont(W2SNS.getFont().deriveFont(W2SNS.getFont().getStyle() | Font.BOLD));
          W2SNS.setName("W2SNS");
          panel1.add(W2SNS);
          W2SNS.setBounds(260, 232, 40, W2SNS.getPreferredSize().height);

          //---- OBJ_80_OBJ_80 ----
          OBJ_80_OBJ_80.setText("Le");
          OBJ_80_OBJ_80.setName("OBJ_80_OBJ_80");
          panel1.add(OBJ_80_OBJ_80);
          OBJ_80_OBJ_80.setBounds(231, 51, 29, 28);

          //---- WCOD ----
          WCOD.setComponentPopupMenu(BTD);
          WCOD.setEditable(false);
          WCOD.setName("WCOD");
          panel1.add(WCOD);
          WCOD.setBounds(21, 51, 20, WCOD.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 385, 420);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder(""));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- L1SNS ----
          L1SNS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1SNS.setName("L1SNS");
          panel2.add(L1SNS);
          L1SNS.setBounds(150, 31, 81, L1SNS.getPreferredSize().height);

          //---- L1MTT ----
          L1MTT.setComponentPopupMenu(BTD);
          L1MTT.setName("L1MTT");
          panel2.add(L1MTT);
          L1MTT.setBounds(30, 30, 116, L1MTT.getPreferredSize().height);

          //---- OBJ_17_OBJ_17 ----
          OBJ_17_OBJ_17.setText("@LIBMTT@");
          OBJ_17_OBJ_17.setName("OBJ_17_OBJ_17");
          panel2.add(OBJ_17_OBJ_17);
          OBJ_17_OBJ_17.setBounds(30, 5, 115, 28);

          //---- OBJ_18_OBJ_18 ----
          OBJ_18_OBJ_18.setText("Sens");
          OBJ_18_OBJ_18.setName("OBJ_18_OBJ_18");
          panel2.add(OBJ_18_OBJ_18);
          OBJ_18_OBJ_18.setBounds(150, 5, 65, 28);

          //---- OBJ_41_OBJ_41 ----
          OBJ_41_OBJ_41.setText("R\u00e9f classement");
          OBJ_41_OBJ_41.setName("OBJ_41_OBJ_41");
          panel2.add(OBJ_41_OBJ_41);
          OBJ_41_OBJ_41.setBounds(520, 65, 100, 28);

          //---- L1RFC ----
          L1RFC.setComponentPopupMenu(BTD);
          L1RFC.setName("L1RFC");
          panel2.add(L1RFC);
          L1RFC.setBounds(520, 90, 92, L1RFC.getPreferredSize().height);

          //---- OBJ_42_OBJ_42 ----
          OBJ_42_OBJ_42.setText("N\u00b0Pi\u00e8ce");
          OBJ_42_OBJ_42.setName("OBJ_42_OBJ_42");
          panel2.add(OBJ_42_OBJ_42);
          OBJ_42_OBJ_42.setBounds(450, 65, 52, 28);

          //---- OBJ_40_OBJ_40 ----
          OBJ_40_OBJ_40.setText("Libell\u00e9");
          OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");
          panel2.add(OBJ_40_OBJ_40);
          OBJ_40_OBJ_40.setBounds(60, 65, 43, 28);

          //---- OBJ_39_OBJ_39 ----
          OBJ_39_OBJ_39.setText("C");
          OBJ_39_OBJ_39.setName("OBJ_39_OBJ_39");
          panel2.add(OBJ_39_OBJ_39);
          OBJ_39_OBJ_39.setBounds(30, 65, 20, 28);

          //---- L1LIB ----
          L1LIB.setComponentPopupMenu(BTD);
          L1LIB.setName("L1LIB");
          panel2.add(L1LIB);
          L1LIB.setBounds(60, 90, 291, L1LIB.getPreferredSize().height);

          //---- L1CLB ----
          L1CLB.setComponentPopupMenu(BTD);
          L1CLB.setName("L1CLB");
          panel2.add(L1CLB);
          L1CLB.setBounds(30, 90, 20, L1CLB.getPreferredSize().height);

          //---- L1PCE ----
          L1PCE.setComponentPopupMenu(BTD);
          L1PCE.setName("L1PCE");
          panel2.add(L1PCE);
          L1PCE.setBounds(450, 90, 68, L1PCE.getPreferredSize().height);

          //---- OBJ_56_OBJ_56 ----
          OBJ_56_OBJ_56.setText("Quantit\u00e9");
          OBJ_56_OBJ_56.setName("OBJ_56_OBJ_56");
          panel2.add(OBJ_56_OBJ_56);
          OBJ_56_OBJ_56.setBounds(485, 120, 53, 28);

          //---- OBJ_50_OBJ_50 ----
          OBJ_50_OBJ_50.setText("@LAXE1@");
          OBJ_50_OBJ_50.setName("OBJ_50_OBJ_50");
          panel2.add(OBJ_50_OBJ_50);
          OBJ_50_OBJ_50.setBounds(125, 230, 55, 28);

          //---- OBJ_51_OBJ_51 ----
          OBJ_51_OBJ_51.setText("@LAXE2@");
          OBJ_51_OBJ_51.setName("OBJ_51_OBJ_51");
          panel2.add(OBJ_51_OBJ_51);
          OBJ_51_OBJ_51.setBounds(125, 260, 55, 28);

          //---- OBJ_52_OBJ_52 ----
          OBJ_52_OBJ_52.setText("@LAXE3@");
          OBJ_52_OBJ_52.setName("OBJ_52_OBJ_52");
          panel2.add(OBJ_52_OBJ_52);
          OBJ_52_OBJ_52.setBounds(125, 290, 55, 28);

          //---- OBJ_53_OBJ_53 ----
          OBJ_53_OBJ_53.setText("@LAXE4@");
          OBJ_53_OBJ_53.setName("OBJ_53_OBJ_53");
          panel2.add(OBJ_53_OBJ_53);
          OBJ_53_OBJ_53.setBounds(125, 320, 55, 28);

          //---- OBJ_54_OBJ_54 ----
          OBJ_54_OBJ_54.setText("@LAXE5@");
          OBJ_54_OBJ_54.setName("OBJ_54_OBJ_54");
          panel2.add(OBJ_54_OBJ_54);
          OBJ_54_OBJ_54.setBounds(125, 350, 55, 28);

          //---- OBJ_55_OBJ_55 ----
          OBJ_55_OBJ_55.setText("@LAXE6@");
          OBJ_55_OBJ_55.setName("OBJ_55_OBJ_55");
          panel2.add(OBJ_55_OBJ_55);
          OBJ_55_OBJ_55.setBounds(125, 380, 55, 28);

          //---- L1QTE ----
          L1QTE.setComponentPopupMenu(BTD);
          L1QTE.setName("L1QTE");
          panel2.add(L1QTE);
          L1QTE.setBounds(540, 120, 73, L1QTE.getPreferredSize().height);

          //---- L1AA1 ----
          L1AA1.setComponentPopupMenu(BTD);
          L1AA1.setName("L1AA1");
          panel2.add(L1AA1);
          L1AA1.setBounds(180, 230, 60, L1AA1.getPreferredSize().height);

          //---- L1AA2 ----
          L1AA2.setComponentPopupMenu(BTD);
          L1AA2.setName("L1AA2");
          panel2.add(L1AA2);
          L1AA2.setBounds(180, 260, 60, L1AA2.getPreferredSize().height);

          //---- L1AA3 ----
          L1AA3.setComponentPopupMenu(BTD);
          L1AA3.setName("L1AA3");
          panel2.add(L1AA3);
          L1AA3.setBounds(180, 290, 60, L1AA3.getPreferredSize().height);

          //---- L1AA4 ----
          L1AA4.setComponentPopupMenu(BTD);
          L1AA4.setName("L1AA4");
          panel2.add(L1AA4);
          L1AA4.setBounds(180, 320, 60, L1AA4.getPreferredSize().height);

          //---- L1AA5 ----
          L1AA5.setComponentPopupMenu(BTD);
          L1AA5.setName("L1AA5");
          panel2.add(L1AA5);
          L1AA5.setBounds(180, 350, 60, L1AA5.getPreferredSize().height);

          //---- L1AA6 ----
          L1AA6.setComponentPopupMenu(BTD);
          L1AA6.setName("L1AA6");
          panel2.add(L1AA6);
          L1AA6.setBounds(180, 380, 60, L1AA6.getPreferredSize().height);

          //---- W1LIBS ----
          W1LIBS.setComponentPopupMenu(BTD);
          W1LIBS.setName("W1LIBS");
          panel2.add(W1LIBS);
          W1LIBS.setBounds(350, 90, 99, W1LIBS.getPreferredSize().height);

          //---- OBJ_43_OBJ_43 ----
          OBJ_43_OBJ_43.setText("Date de valeur");
          OBJ_43_OBJ_43.setName("OBJ_43_OBJ_43");
          panel2.add(OBJ_43_OBJ_43);
          OBJ_43_OBJ_43.setBounds(295, 120, 90, 28);

          //---- L1DVBX ----
          L1DVBX.setComponentPopupMenu(BTD);
          L1DVBX.setName("L1DVBX");
          panel2.add(L1DVBX);
          L1DVBX.setBounds(380, 120, 70, L1DVBX.getPreferredSize().height);

          //---- xTitledSeparator2 ----
          xTitledSeparator2.setTitle("Analytique");
          xTitledSeparator2.setName("xTitledSeparator2");
          panel2.add(xTitledSeparator2);
          xTitledSeparator2.setBounds(25, 150, 575, 28);

          //---- OBJ_20 ----
          OBJ_20.setText("Sect");
          OBJ_20.setName("OBJ_20");
          panel2.add(OBJ_20);
          OBJ_20.setBounds(30, 180, 40, 28);

          //---- L1SAN ----
          L1SAN.setComponentPopupMenu(BTD);
          L1SAN.setName("L1SAN");
          panel2.add(L1SAN);
          L1SAN.setBounds(60, 180, 50, L1SAN.getPreferredSize().height);

          //---- OBJ_21 ----
          OBJ_21.setText("Affaire");
          OBJ_21.setName("OBJ_21");
          panel2.add(OBJ_21);
          OBJ_21.setBounds(130, 180, 45, 28);

          //---- L1ACT ----
          L1ACT.setComponentPopupMenu(BTD);
          L1ACT.setName("L1ACT");
          panel2.add(L1ACT);
          L1ACT.setBounds(180, 180, 60, L1ACT.getPreferredSize().height);

          //---- OBJ_22 ----
          OBJ_22.setText("Nature");
          OBJ_22.setName("OBJ_22");
          panel2.add(OBJ_22);
          OBJ_22.setBounds(270, 180, 45, 28);

          //---- L1NAT ----
          L1NAT.setComponentPopupMenu(BTD);
          L1NAT.setName("L1NAT");
          panel2.add(L1NAT);
          L1NAT.setBounds(330, 180, 60, L1NAT.getPreferredSize().height);

          //---- L1VTL ----
          L1VTL.setText("Ventilation");
          L1VTL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1VTL.setName("L1VTL");
          panel2.add(L1VTL);
          L1VTL.setBounds(205, 120, 82, 28);

          //---- OBJ_57_OBJ_57 ----
          OBJ_57_OBJ_57.setText("Contrepartie /T");
          OBJ_57_OBJ_57.setName("OBJ_57_OBJ_57");
          panel2.add(OBJ_57_OBJ_57);
          OBJ_57_OBJ_57.setBounds(30, 120, 91, 28);

          //---- L1NCPX ----
          L1NCPX.setComponentPopupMenu(BTD);
          L1NCPX.setName("L1NCPX");
          panel2.add(L1NCPX);
          L1NCPX.setBounds(120, 120, 60, L1NCPX.getPreferredSize().height);

          //---- WLNCPT ----
          WLNCPT.setComponentPopupMenu(BTD);
          WLNCPT.setToolTipText("Num\u00e9ro de compte de contrepartie \u00e0 r\u00e9percuter sur la ligne.  Types de r\u00e9percutions possibles.  \"1\" = La ligne suivante de contrepartie est propos\u00e9e automatiquement avec montant, code libell\u00e9, N\u00b0de pi\u00e8ce...  \"2\" = Ligne suivante de contrepartie g\u00e9n\u00e9r\u00e9e automatiquement avec montant, code libell\u00e9, n\u00b0de pi\u00e8ce...");
          WLNCPT.setName("WLNCPT");
          panel2.add(WLNCPT);
          WLNCPT.setBounds(180, 120, 19, WLNCPT.getPreferredSize().height);

          //---- btn_dup ----
          btn_dup.setText("Dup");
          btn_dup.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          btn_dup.setName("btn_dup");
          btn_dup.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_19ActionPerformed(e);
            }
          });
          panel2.add(btn_dup);
          btn_dup.setBounds(235, 27, 55, 35);

          //---- OBJ_23 ----
          OBJ_23.setText("Axes");
          OBJ_23.setFont(OBJ_23.getFont().deriveFont(OBJ_23.getFont().getStyle() | Font.BOLD));
          OBJ_23.setName("OBJ_23");
          panel2.add(OBJ_23);
          OBJ_23.setBounds(30, 230, 45, 28);

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder("TVA"));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- L1IN6 ----
            L1IN6.setName("L1IN6");
            panel3.add(L1IN6);
            L1IN6.setBounds(20, 30, 24, L1IN6.getPreferredSize().height);

            //---- OBJ_53 ----
            OBJ_53.setText("Taux");
            OBJ_53.setName("OBJ_53");
            panel3.add(OBJ_53);
            OBJ_53.setBounds(55, 35, 45, 19);

            //---- TTVA ----
            TTVA.setText("@TTVA@");
            TTVA.setHorizontalAlignment(SwingConstants.RIGHT);
            TTVA.setName("TTVA");
            panel3.add(TTVA);
            TTVA.setBounds(100, 32, 44, TTVA.getPreferredSize().height);

            //---- OBJ_51 ----
            OBJ_51.setText("/");
            OBJ_51.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_51.setName("OBJ_51");
            panel3.add(OBJ_51);
            OBJ_51.setBounds(145, 35, 20, 19);

            //---- L1AFAL ----
            L1AFAL.setName("L1AFAL");
            panel3.add(L1AFAL);
            L1AFAL.setBounds(165, 30, 34, L1AFAL.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }
          panel2.add(panel3);
          panel3.setBounds(385, 5, 225, 70);

          //---- LIBAX1 ----
          LIBAX1.setText("@LIBAX1@");
          LIBAX1.setName("LIBAX1");
          panel2.add(LIBAX1);
          LIBAX1.setBounds(245, 232, 310, LIBAX1.getPreferredSize().height);

          //---- LIBAX2 ----
          LIBAX2.setText("@LIBAX2@");
          LIBAX2.setName("LIBAX2");
          panel2.add(LIBAX2);
          LIBAX2.setBounds(245, 262, 310, LIBAX2.getPreferredSize().height);

          //---- LIBAX3 ----
          LIBAX3.setText("@LIBAX3@");
          LIBAX3.setName("LIBAX3");
          panel2.add(LIBAX3);
          LIBAX3.setBounds(245, 292, 310, LIBAX3.getPreferredSize().height);

          //---- LIBAX4 ----
          LIBAX4.setText("@LIBAX4@");
          LIBAX4.setName("LIBAX4");
          panel2.add(LIBAX4);
          LIBAX4.setBounds(245, 322, 310, LIBAX4.getPreferredSize().height);

          //---- LIBAX5 ----
          LIBAX5.setText("@LIBAX5@");
          LIBAX5.setName("LIBAX5");
          panel2.add(LIBAX5);
          LIBAX5.setBounds(245, 352, 310, LIBAX5.getPreferredSize().height);

          //---- LIBAX6 ----
          LIBAX6.setText("@LIBAX6@");
          LIBAX6.setName("LIBAX6");
          panel2.add(LIBAX6);
          LIBAX6.setBounds(245, 382, 310, LIBAX6.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel2);
        panel2.setBounds(405, 10, 625, 420);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("Duplication");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField LIBCPT;
  private RiZoneSortie message;
  private XRiTextField USOLDE;
  private JLabel OBJ_64_OBJ_64;
  private XRiTextField L1NCA;
  private XRiTextField L1NCGX;
  private XRiTextField L1NLI;
  private XRiTextField L1DTJ;
  private RiZoneSortie W2SNS;
  private JLabel OBJ_80_OBJ_80;
  private XRiTextField WCOD;
  private JPanel panel2;
  private XRiComboBox L1SNS;
  private XRiTextField L1MTT;
  private JLabel OBJ_17_OBJ_17;
  private JLabel OBJ_18_OBJ_18;
  private JLabel OBJ_41_OBJ_41;
  private XRiTextField L1RFC;
  private JLabel OBJ_42_OBJ_42;
  private JLabel OBJ_40_OBJ_40;
  private JLabel OBJ_39_OBJ_39;
  private XRiTextField L1LIB;
  private XRiTextField L1CLB;
  private XRiTextField L1PCE;
  private JLabel OBJ_56_OBJ_56;
  private JLabel OBJ_50_OBJ_50;
  private JLabel OBJ_51_OBJ_51;
  private JLabel OBJ_52_OBJ_52;
  private JLabel OBJ_53_OBJ_53;
  private JLabel OBJ_54_OBJ_54;
  private JLabel OBJ_55_OBJ_55;
  private XRiTextField L1QTE;
  private XRiTextField L1AA1;
  private XRiTextField L1AA2;
  private XRiTextField L1AA3;
  private XRiTextField L1AA4;
  private XRiTextField L1AA5;
  private XRiTextField L1AA6;
  private XRiTextField W1LIBS;
  private JLabel OBJ_43_OBJ_43;
  private XRiTextField L1DVBX;
  private JXTitledSeparator xTitledSeparator2;
  private JLabel OBJ_20;
  private XRiTextField L1SAN;
  private JLabel OBJ_21;
  private XRiTextField L1ACT;
  private JLabel OBJ_22;
  private XRiTextField L1NAT;
  private XRiCheckBox L1VTL;
  private JLabel OBJ_57_OBJ_57;
  private XRiTextField L1NCPX;
  private XRiTextField WLNCPT;
  private JButton btn_dup;
  private JLabel OBJ_23;
  private JPanel panel3;
  private XRiTextField L1IN6;
  private JLabel OBJ_53;
  private RiZoneSortie TTVA;
  private JLabel OBJ_51;
  private XRiTextField L1AFAL;
  private RiZoneSortie LIBAX1;
  private RiZoneSortie LIBAX2;
  private RiZoneSortie LIBAX3;
  private RiZoneSortie LIBAX4;
  private RiZoneSortie LIBAX5;
  private RiZoneSortie LIBAX6;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
