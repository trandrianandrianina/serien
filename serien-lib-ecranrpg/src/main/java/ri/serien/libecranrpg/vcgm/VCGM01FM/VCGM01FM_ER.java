
package ri.serien.libecranrpg.vcgm.VCGM01FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM01FM_ER extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] ERSNS_Value = { "*", "C", "D", };
  private String[] ERSNS_Title = { "Tous sens autorisés", "Crédit", "Débit", };
  
  public VCGM01FM_ER(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    ERSNS.setValeurs(ERSNS_Value, ERSNS_Title);
    ERREL.setValeursSelection("O", "N");
    ERCRD.setValeursSelection("X", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    ERJ01_CHK.setSelected(lexique.HostFieldGetData("ERJ01").equalsIgnoreCase("**"));
    P_SEL0.setVisible(!ERJ01_CHK.isSelected());
    ERL01_CHK.setSelected(lexique.HostFieldGetData("ERL01").equalsIgnoreCase("*"));
    P_SEL1.setVisible(!ERL01_CHK.isSelected());
    ERG01_CHK.setSelected(lexique.HostFieldGetData("ERG01").equalsIgnoreCase("**"));
    P_SEL2.setVisible(!ERG01_CHK.isSelected());
    // ERREL.setSelected(lexique.HostFieldGetData("ERREL").equalsIgnoreCase("O"));
    // ERCRD.setSelected(lexique.HostFieldGetData("ERCRD").equalsIgnoreCase("X"));
    // ERSNS.setSelectedIndex(getIndice("ERSNS", ERSNS_Value));
    
    

    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    // riMenu_bt2.setIcon(lexique.getImage("images/options.png",true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    if (ERJ01_CHK.isSelected()) {
      lexique.HostFieldPutData("ERJ01", 0, "**");
    }
    if (ERL01_CHK.isSelected()) {
      lexique.HostFieldPutData("ERL01", 0, "*");
    }
    if (ERG01_CHK.isSelected()) {
      lexique.HostFieldPutData("ERG01", 0, "**");
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void ERJ01_CHKActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
    if (!ERJ01_CHK.isSelected()) {
      ERJ01.setText("");
    }
  }
  
  private void ERL01_CHKActionPerformed(ActionEvent e) {
    P_SEL1.setVisible(!P_SEL1.isVisible());
    if (!ERL01_CHK.isSelected()) {
      ERL01.setText("");
    }
  }
  
  private void ERG01_CHKActionPerformed(ActionEvent e) {
    P_SEL2.setVisible(!P_SEL2.isVisible());
    if (!ERG01_CHK.isSelected()) {
      ERG01.setText("");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44_OBJ_45 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_45_OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46_OBJ_47 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_86_OBJ_87 = new JLabel();
    panel1 = new JPanel();
    OBJ_90_OBJ_90 = new JLabel();
    ERG01_CHK = new JCheckBox();
    ERRGL = new XRiTextField();
    OBJ_81_OBJ_81 = new JLabel();
    ERRGL1 = new XRiTextField();
    OBJ_83_OBJ_83 = new JLabel();
    ERSEUI = new XRiTextField();
    OBJ_85_OBJ_85 = new JLabel();
    P_SEL2 = new JPanel();
    ERG01 = new XRiTextField();
    ERG02 = new XRiTextField();
    ERG03 = new XRiTextField();
    ERG04 = new XRiTextField();
    ERG05 = new XRiTextField();
    panel2 = new JPanel();
    ERC01 = new XRiTextField();
    ERC02 = new XRiTextField();
    ERC03 = new XRiTextField();
    ERC04 = new XRiTextField();
    ERC05 = new XRiTextField();
    ERJ01_CHK = new JCheckBox();
    ERL01_CHK = new JCheckBox();
    ERSNS = new XRiComboBox();
    ERCRD = new XRiCheckBox();
    OBJ_94_OBJ_94 = new JLabel();
    OBJ_92_OBJ_92 = new JLabel();
    ERRFC = new XRiTextField();
    OBJ_97_OBJ_97 = new JLabel();
    ERVAL = new XRiTextField();
    ERREL = new XRiCheckBox();
    ERCRI = new XRiTextField();
    P_SEL0 = new JPanel();
    OBJ_36_OBJ_36 = new JLabel();
    ERJ01 = new XRiTextField();
    ERJ02 = new XRiTextField();
    ERJ03 = new XRiTextField();
    ERJ04 = new XRiTextField();
    ERJ05 = new XRiTextField();
    P_SEL1 = new JPanel();
    OBJ_85_OBJ_86 = new JLabel();
    ERL01 = new XRiTextField();
    ERL02 = new XRiTextField();
    ERL03 = new XRiTextField();
    ERL04 = new XRiTextField();
    ERL05 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44_OBJ_45 ----
          OBJ_44_OBJ_45.setText("Soci\u00e9t\u00e9");
          OBJ_44_OBJ_45.setName("OBJ_44_OBJ_45");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setName("INDETB");

          //---- OBJ_45_OBJ_46 ----
          OBJ_45_OBJ_46.setText("Code");
          OBJ_45_OBJ_46.setName("OBJ_45_OBJ_46");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setName("INDTYP");

          //---- OBJ_46_OBJ_47 ----
          OBJ_46_OBJ_47.setText("Ordre");
          OBJ_46_OBJ_47.setName("OBJ_46_OBJ_47");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44_OBJ_45, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_45_OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46_OBJ_47, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(800, 550));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setTitle("Extraction des relev\u00e9s");
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //---- OBJ_86_OBJ_87 ----
            OBJ_86_OBJ_87.setText("Sens \u00e9critures comptables");
            OBJ_86_OBJ_87.setName("OBJ_86_OBJ_87");
            xTitledPanel2ContentContainer.add(OBJ_86_OBJ_87);
            OBJ_86_OBJ_87.setBounds(18, 128, 165, 20);

            //======== panel1 ========
            {
              panel1.setBorder(new TitledBorder("R\u00e8glements \u00e0 s\u00e9lectionner"));
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- OBJ_90_OBJ_90 ----
              OBJ_90_OBJ_90.setText("R\u00e8glement par d\u00e9faut");
              OBJ_90_OBJ_90.setName("OBJ_90_OBJ_90");
              panel1.add(OBJ_90_OBJ_90);
              OBJ_90_OBJ_90.setBounds(328, 39, 132, 20);

              //---- ERG01_CHK ----
              ERG01_CHK.setText("Tous les r\u00e8glements");
              ERG01_CHK.setName("ERG01_CHK");
              ERG01_CHK.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  ERG01_CHKActionPerformed(e);
                }
              });
              panel1.add(ERG01_CHK);
              ERG01_CHK.setBounds(30, 35, 150, ERG01_CHK.getPreferredSize().height);

              //---- ERRGL ----
              ERRGL.setName("ERRGL");
              panel1.add(ERRGL);
              ERRGL.setBounds(463, 35, 30, ERRGL.getPreferredSize().height);

              //---- OBJ_81_OBJ_81 ----
              OBJ_81_OBJ_81.setText("R\u00e8glement appliqu\u00e9");
              OBJ_81_OBJ_81.setName("OBJ_81_OBJ_81");
              panel1.add(OBJ_81_OBJ_81);
              OBJ_81_OBJ_81.setBounds(328, 74, 126, 20);

              //---- ERRGL1 ----
              ERRGL1.setName("ERRGL1");
              panel1.add(ERRGL1);
              ERRGL1.setBounds(463, 70, 30, ERRGL1.getPreferredSize().height);

              //---- OBJ_83_OBJ_83 ----
              OBJ_83_OBJ_83.setText("si seuil");
              OBJ_83_OBJ_83.setName("OBJ_83_OBJ_83");
              panel1.add(OBJ_83_OBJ_83);
              OBJ_83_OBJ_83.setBounds(503, 74, 45, 20);

              //---- ERSEUI ----
              ERSEUI.setName("ERSEUI");
              panel1.add(ERSEUI);
              ERSEUI.setBounds(553, 70, 64, ERSEUI.getPreferredSize().height);

              //---- OBJ_85_OBJ_85 ----
              OBJ_85_OBJ_85.setText("non atteint");
              OBJ_85_OBJ_85.setName("OBJ_85_OBJ_85");
              panel1.add(OBJ_85_OBJ_85);
              OBJ_85_OBJ_85.setBounds(628, 74, 69, 20);

              //======== P_SEL2 ========
              {
                P_SEL2.setOpaque(false);
                P_SEL2.setBorder(new BevelBorder(BevelBorder.LOWERED));
                P_SEL2.setName("P_SEL2");
                P_SEL2.setLayout(null);

                //---- ERG01 ----
                ERG01.setName("ERG01");
                P_SEL2.add(ERG01);
                ERG01.setBounds(10, 10, 30, ERG01.getPreferredSize().height);

                //---- ERG02 ----
                ERG02.setName("ERG02");
                P_SEL2.add(ERG02);
                ERG02.setBounds(45, 10, 30, ERG02.getPreferredSize().height);

                //---- ERG03 ----
                ERG03.setName("ERG03");
                P_SEL2.add(ERG03);
                ERG03.setBounds(80, 10, 30, ERG03.getPreferredSize().height);

                //---- ERG04 ----
                ERG04.setName("ERG04");
                P_SEL2.add(ERG04);
                ERG04.setBounds(115, 10, 30, ERG04.getPreferredSize().height);

                //---- ERG05 ----
                ERG05.setName("ERG05");
                P_SEL2.add(ERG05);
                ERG05.setBounds(150, 10, 30, ERG05.getPreferredSize().height);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < P_SEL2.getComponentCount(); i++) {
                    Rectangle bounds = P_SEL2.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = P_SEL2.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  P_SEL2.setMinimumSize(preferredSize);
                  P_SEL2.setPreferredSize(preferredSize);
                }
              }
              panel1.add(P_SEL2);
              P_SEL2.setBounds(30, 55, 200, 45);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel2ContentContainer.add(panel1);
            panel1.setBounds(15, 165, 735, 120);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("Collectifs \u00e0 s\u00e9lectionner"));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- ERC01 ----
              ERC01.setName("ERC01");
              panel2.add(ERC01);
              ERC01.setBounds(30, 40, 70, ERC01.getPreferredSize().height);

              //---- ERC02 ----
              ERC02.setName("ERC02");
              panel2.add(ERC02);
              ERC02.setBounds(125, 40, 70, ERC02.getPreferredSize().height);

              //---- ERC03 ----
              ERC03.setName("ERC03");
              panel2.add(ERC03);
              ERC03.setBounds(220, 40, 70, ERC03.getPreferredSize().height);

              //---- ERC04 ----
              ERC04.setName("ERC04");
              panel2.add(ERC04);
              ERC04.setBounds(315, 40, 70, ERC04.getPreferredSize().height);

              //---- ERC05 ----
              ERC05.setName("ERC05");
              panel2.add(ERC05);
              ERC05.setBounds(410, 40, 70, ERC05.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel2ContentContainer.add(panel2);
            panel2.setBounds(15, 290, 735, 90);

            //---- ERJ01_CHK ----
            ERJ01_CHK.setText("Tous les journaux");
            ERJ01_CHK.setName("ERJ01_CHK");
            ERJ01_CHK.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                ERJ01_CHKActionPerformed(e);
              }
            });
            xTitledPanel2ContentContainer.add(ERJ01_CHK);
            ERJ01_CHK.setBounds(15, 18, 160, 24);

            //---- ERL01_CHK ----
            ERL01_CHK.setText("Tous les libell\u00e9s");
            ERL01_CHK.setName("ERL01_CHK");
            ERL01_CHK.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                ERL01_CHKActionPerformed(e);
              }
            });
            xTitledPanel2ContentContainer.add(ERL01_CHK);
            ERL01_CHK.setBounds(15, 78, 160, 24);

            //---- ERSNS ----
            ERSNS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ERSNS.setName("ERSNS");
            xTitledPanel2ContentContainer.add(ERSNS);
            ERSNS.setBounds(190, 125, 180, ERSNS.getPreferredSize().height);

            //---- ERCRD ----
            ERCRD.setText("Gestion des relev\u00e9s cr\u00e9diteurs");
            ERCRD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ERCRD.setName("ERCRD");
            xTitledPanel2ContentContainer.add(ERCRD);
            ERCRD.setBounds(15, 455, 207, 20);

            //---- OBJ_94_OBJ_94 ----
            OBJ_94_OBJ_94.setText("Num\u00e9ro crit\u00e8re de s\u00e9lection/PCA");
            OBJ_94_OBJ_94.setName("OBJ_94_OBJ_94");
            xTitledPanel2ContentContainer.add(OBJ_94_OBJ_94);
            OBJ_94_OBJ_94.setBounds(15, 425, 202, 20);

            //---- OBJ_92_OBJ_92 ----
            OBJ_92_OBJ_92.setText("R\u00e9f\u00e9rence classement");
            OBJ_92_OBJ_92.setName("OBJ_92_OBJ_92");
            xTitledPanel2ContentContainer.add(OBJ_92_OBJ_92);
            OBJ_92_OBJ_92.setBounds(15, 385, 138, 20);

            //---- ERRFC ----
            ERRFC.setName("ERRFC");
            xTitledPanel2ContentContainer.add(ERRFC);
            ERRFC.setBounds(240, 385, 110, ERRFC.getPreferredSize().height);

            //---- OBJ_97_OBJ_97 ----
            OBJ_97_OBJ_97.setText("Valeur");
            OBJ_97_OBJ_97.setName("OBJ_97_OBJ_97");
            xTitledPanel2ContentContainer.add(OBJ_97_OBJ_97);
            OBJ_97_OBJ_97.setBounds(390, 425, 42, 20);

            //---- ERVAL ----
            ERVAL.setName("ERVAL");
            xTitledPanel2ContentContainer.add(ERVAL);
            ERVAL.setBounds(440, 420, 60, ERVAL.getPreferredSize().height);

            //---- ERREL ----
            ERREL.setText("O/N");
            ERREL.setName("ERREL");
            xTitledPanel2ContentContainer.add(ERREL);
            ERREL.setBounds(290, 425, 55, 20);

            //---- ERCRI ----
            ERCRI.setName("ERCRI");
            xTitledPanel2ContentContainer.add(ERCRI);
            ERCRI.setBounds(240, 420, 20, ERCRI.getPreferredSize().height);

            //======== P_SEL0 ========
            {
              P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL0.setOpaque(false);
              P_SEL0.setName("P_SEL0");
              P_SEL0.setLayout(null);

              //---- OBJ_36_OBJ_36 ----
              OBJ_36_OBJ_36.setText("Codes journaux");
              OBJ_36_OBJ_36.setName("OBJ_36_OBJ_36");
              P_SEL0.add(OBJ_36_OBJ_36);
              OBJ_36_OBJ_36.setBounds(10, 15, 100, 20);

              //---- ERJ01 ----
              ERJ01.setName("ERJ01");
              P_SEL0.add(ERJ01);
              ERJ01.setBounds(150, 11, 30, ERJ01.getPreferredSize().height);

              //---- ERJ02 ----
              ERJ02.setName("ERJ02");
              P_SEL0.add(ERJ02);
              ERJ02.setBounds(183, 11, 30, ERJ02.getPreferredSize().height);

              //---- ERJ03 ----
              ERJ03.setName("ERJ03");
              P_SEL0.add(ERJ03);
              ERJ03.setBounds(216, 11, 30, ERJ03.getPreferredSize().height);

              //---- ERJ04 ----
              ERJ04.setName("ERJ04");
              P_SEL0.add(ERJ04);
              ERJ04.setBounds(249, 11, 30, ERJ04.getPreferredSize().height);

              //---- ERJ05 ----
              ERJ05.setName("ERJ05");
              P_SEL0.add(ERJ05);
              ERJ05.setBounds(282, 11, 30, ERJ05.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < P_SEL0.getComponentCount(); i++) {
                  Rectangle bounds = P_SEL0.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = P_SEL0.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                P_SEL0.setMinimumSize(preferredSize);
                P_SEL0.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel2ContentContainer.add(P_SEL0);
            P_SEL0.setBounds(190, 5, 365, 50);

            //======== P_SEL1 ========
            {
              P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL1.setOpaque(false);
              P_SEL1.setName("P_SEL1");
              P_SEL1.setLayout(null);

              //---- OBJ_85_OBJ_86 ----
              OBJ_85_OBJ_86.setText("Codes libell\u00e9s");
              OBJ_85_OBJ_86.setName("OBJ_85_OBJ_86");
              P_SEL1.add(OBJ_85_OBJ_86);
              OBJ_85_OBJ_86.setBounds(15, 14, 93, 20);

              //---- ERL01 ----
              ERL01.setName("ERL01");
              P_SEL1.add(ERL01);
              ERL01.setBounds(150, 10, 20, ERL01.getPreferredSize().height);

              //---- ERL02 ----
              ERL02.setName("ERL02");
              P_SEL1.add(ERL02);
              ERL02.setBounds(183, 10, 20, ERL02.getPreferredSize().height);

              //---- ERL03 ----
              ERL03.setName("ERL03");
              P_SEL1.add(ERL03);
              ERL03.setBounds(216, 10, 20, ERL03.getPreferredSize().height);

              //---- ERL04 ----
              ERL04.setName("ERL04");
              P_SEL1.add(ERL04);
              ERL04.setBounds(249, 10, 20, ERL04.getPreferredSize().height);

              //---- ERL05 ----
              ERL05.setName("ERL05");
              P_SEL1.add(ERL05);
              ERL05.setBounds(282, 10, 20, ERL05.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < P_SEL1.getComponentCount(); i++) {
                  Rectangle bounds = P_SEL1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = P_SEL1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                P_SEL1.setMinimumSize(preferredSize);
                P_SEL1.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel2ContentContainer.add(P_SEL1);
            P_SEL1.setBounds(190, 65, 365, 50);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel2, GroupLayout.DEFAULT_SIZE, 774, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel2, GroupLayout.DEFAULT_SIZE, 522, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44_OBJ_45;
  private XRiTextField INDETB;
  private JLabel OBJ_45_OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_46_OBJ_47;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel2;
  private JLabel OBJ_86_OBJ_87;
  private JPanel panel1;
  private JLabel OBJ_90_OBJ_90;
  private JCheckBox ERG01_CHK;
  private XRiTextField ERRGL;
  private JLabel OBJ_81_OBJ_81;
  private XRiTextField ERRGL1;
  private JLabel OBJ_83_OBJ_83;
  private XRiTextField ERSEUI;
  private JLabel OBJ_85_OBJ_85;
  private JPanel P_SEL2;
  private XRiTextField ERG01;
  private XRiTextField ERG02;
  private XRiTextField ERG03;
  private XRiTextField ERG04;
  private XRiTextField ERG05;
  private JPanel panel2;
  private XRiTextField ERC01;
  private XRiTextField ERC02;
  private XRiTextField ERC03;
  private XRiTextField ERC04;
  private XRiTextField ERC05;
  private JCheckBox ERJ01_CHK;
  private JCheckBox ERL01_CHK;
  private XRiComboBox ERSNS;
  private XRiCheckBox ERCRD;
  private JLabel OBJ_94_OBJ_94;
  private JLabel OBJ_92_OBJ_92;
  private XRiTextField ERRFC;
  private JLabel OBJ_97_OBJ_97;
  private XRiTextField ERVAL;
  private XRiCheckBox ERREL;
  private XRiTextField ERCRI;
  private JPanel P_SEL0;
  private JLabel OBJ_36_OBJ_36;
  private XRiTextField ERJ01;
  private XRiTextField ERJ02;
  private XRiTextField ERJ03;
  private XRiTextField ERJ04;
  private XRiTextField ERJ05;
  private JPanel P_SEL1;
  private JLabel OBJ_85_OBJ_86;
  private XRiTextField ERL01;
  private XRiTextField ERL02;
  private XRiTextField ERL03;
  private XRiTextField ERL04;
  private XRiTextField ERL05;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
