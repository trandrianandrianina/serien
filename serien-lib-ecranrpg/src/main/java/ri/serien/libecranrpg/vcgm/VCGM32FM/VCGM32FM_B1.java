/*
 * Created by JFormDesigner on Wed Jan 23 18:00:47 CET 2013
 */

package ri.serien.libecranrpg.vcgm.VCGM32FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGM32FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM32FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMTT@")).trim());
    WMTVTL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMTVTL@")).trim());
    WECVTL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WECVTL@")).trim());
    label4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMT2@")).trim());
    label5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMT3@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    WECVTL.setVisible(lexique.isPresent("WECVTL"));
    WMTVTL.setVisible(lexique.isPresent("WMTVTL"));
    
    panel4.setVisible(lexique.isTrue("19"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@WLIB@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel3 = new JPanel();
    label1 = new JLabel();
    WVM01 = new XRiTextField();
    WVM04 = new XRiTextField();
    WVM07 = new XRiTextField();
    WVM34 = new XRiTextField();
    WVM37 = new XRiTextField();
    WVM16 = new XRiTextField();
    WVM19 = new XRiTextField();
    WVM22 = new XRiTextField();
    WVM25 = new XRiTextField();
    WVM10 = new XRiTextField();
    WVM05 = new XRiTextField();
    WVM28 = new XRiTextField();
    WVM13 = new XRiTextField();
    WVM31 = new XRiTextField();
    label2 = new JLabel();
    WVS01 = new XRiTextField();
    WVS02 = new XRiTextField();
    WVS03 = new XRiTextField();
    WVS04 = new XRiTextField();
    WVS05 = new XRiTextField();
    WVS06 = new XRiTextField();
    WVS07 = new XRiTextField();
    WVS08 = new XRiTextField();
    WVS09 = new XRiTextField();
    WVS10 = new XRiTextField();
    WVS11 = new XRiTextField();
    WVS12 = new XRiTextField();
    WVS13 = new XRiTextField();
    WVS14 = new XRiTextField();
    WVA01 = new XRiTextField();
    WVA02 = new XRiTextField();
    WVA03 = new XRiTextField();
    WVA04 = new XRiTextField();
    WVA05 = new XRiTextField();
    WVA06 = new XRiTextField();
    WVA07 = new XRiTextField();
    WVA08 = new XRiTextField();
    WVA09 = new XRiTextField();
    WVA10 = new XRiTextField();
    WVA11 = new XRiTextField();
    WVA12 = new XRiTextField();
    WVA13 = new XRiTextField();
    WVA14 = new XRiTextField();
    label3 = new JLabel();
    WVM02 = new XRiTextField();
    WVM40 = new XRiTextField();
    WVM06 = new XRiTextField();
    WVM03 = new XRiTextField();
    WVM08 = new XRiTextField();
    WVM09 = new XRiTextField();
    WVM11 = new XRiTextField();
    WVM12 = new XRiTextField();
    WVM14 = new XRiTextField();
    WVM15 = new XRiTextField();
    WVM17 = new XRiTextField();
    WVM18 = new XRiTextField();
    WVM20 = new XRiTextField();
    WVS15 = new XRiTextField();
    WVA15 = new XRiTextField();
    WVS16 = new XRiTextField();
    WVS17 = new XRiTextField();
    WVS18 = new XRiTextField();
    WVS19 = new XRiTextField();
    WVS20 = new XRiTextField();
    WVS21 = new XRiTextField();
    WVS22 = new XRiTextField();
    WVS23 = new XRiTextField();
    WVS24 = new XRiTextField();
    WVS25 = new XRiTextField();
    WVS26 = new XRiTextField();
    WVS27 = new XRiTextField();
    WVS28 = new XRiTextField();
    WVS29 = new XRiTextField();
    WVS30 = new XRiTextField();
    WVS31 = new XRiTextField();
    WVS32 = new XRiTextField();
    WVS33 = new XRiTextField();
    WVS34 = new XRiTextField();
    WVS35 = new XRiTextField();
    WVS36 = new XRiTextField();
    WVS37 = new XRiTextField();
    WVS38 = new XRiTextField();
    WVS39 = new XRiTextField();
    WVS40 = new XRiTextField();
    WVS41 = new XRiTextField();
    WVS42 = new XRiTextField();
    WVA16 = new XRiTextField();
    WVA17 = new XRiTextField();
    WVA18 = new XRiTextField();
    WVA19 = new XRiTextField();
    WVA20 = new XRiTextField();
    WVA21 = new XRiTextField();
    WVA22 = new XRiTextField();
    WVA23 = new XRiTextField();
    WVA24 = new XRiTextField();
    WVA25 = new XRiTextField();
    WVM21 = new XRiTextField();
    WVM23 = new XRiTextField();
    WVM24 = new XRiTextField();
    WVM26 = new XRiTextField();
    WVM27 = new XRiTextField();
    WVM29 = new XRiTextField();
    WVM30 = new XRiTextField();
    WVM32 = new XRiTextField();
    WVM33 = new XRiTextField();
    WVM35 = new XRiTextField();
    WVM36 = new XRiTextField();
    WVM38 = new XRiTextField();
    WVM39 = new XRiTextField();
    WVM41 = new XRiTextField();
    WVM42 = new XRiTextField();
    WVA26 = new XRiTextField();
    WVA27 = new XRiTextField();
    WVA28 = new XRiTextField();
    WVA29 = new XRiTextField();
    WVA30 = new XRiTextField();
    WVA31 = new XRiTextField();
    WVA32 = new XRiTextField();
    WVA33 = new XRiTextField();
    WVA34 = new XRiTextField();
    WVA35 = new XRiTextField();
    WVA36 = new XRiTextField();
    WVA37 = new XRiTextField();
    WVA38 = new XRiTextField();
    WVA39 = new XRiTextField();
    WVA40 = new XRiTextField();
    WVA41 = new XRiTextField();
    WVA42 = new XRiTextField();
    panel4 = new JPanel();
    WMTVTL = new RiZoneSortie();
    WECVTL = new RiZoneSortie();
    OBJ_29 = new JLabel();
    OBJ_30 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_6 = new JMenuItem();
    OBJ_5 = new JMenuItem();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(940, 615));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel3 ========
        {
          panel3.setBorder(new TitledBorder("Modification imputation analytique"));
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);

          //---- label1 ----
          label1.setText("@LIBMTT@");
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
          label1.setName("label1");
          panel3.add(label1);
          label1.setBounds(40, 50, 100, 20);

          //---- WVM01 ----
          WVM01.setName("WVM01");
          panel3.add(WVM01);
          WVM01.setBounds(35, 70, 100, WVM01.getPreferredSize().height);

          //---- WVM04 ----
          WVM04.setName("WVM04");
          panel3.add(WVM04);
          WVM04.setBounds(35, 100, 100, 28);

          //---- WVM07 ----
          WVM07.setName("WVM07");
          panel3.add(WVM07);
          WVM07.setBounds(35, 130, 100, 28);

          //---- WVM34 ----
          WVM34.setName("WVM34");
          panel3.add(WVM34);
          WVM34.setBounds(35, 400, 100, 28);

          //---- WVM37 ----
          WVM37.setName("WVM37");
          panel3.add(WVM37);
          WVM37.setBounds(35, 430, 100, 28);

          //---- WVM16 ----
          WVM16.setName("WVM16");
          panel3.add(WVM16);
          WVM16.setBounds(35, 220, 100, 28);

          //---- WVM19 ----
          WVM19.setName("WVM19");
          panel3.add(WVM19);
          WVM19.setBounds(35, 250, 100, 28);

          //---- WVM22 ----
          WVM22.setName("WVM22");
          panel3.add(WVM22);
          WVM22.setBounds(35, 280, 100, 28);

          //---- WVM25 ----
          WVM25.setName("WVM25");
          panel3.add(WVM25);
          WVM25.setBounds(35, 310, 100, 28);

          //---- WVM10 ----
          WVM10.setName("WVM10");
          panel3.add(WVM10);
          WVM10.setBounds(35, 160, 100, 28);

          //---- WVM05 ----
          WVM05.setName("WVM05");
          panel3.add(WVM05);
          WVM05.setBounds(260, 100, 100, 28);

          //---- WVM28 ----
          WVM28.setName("WVM28");
          panel3.add(WVM28);
          WVM28.setBounds(35, 340, 100, 28);

          //---- WVM13 ----
          WVM13.setName("WVM13");
          panel3.add(WVM13);
          WVM13.setBounds(35, 190, 100, 28);

          //---- WVM31 ----
          WVM31.setName("WVM31");
          panel3.add(WVM31);
          WVM31.setBounds(35, 370, 100, 28);

          //---- label2 ----
          label2.setText("Sect.");
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
          label2.setName("label2");
          panel3.add(label2);
          label2.setBounds(145, 50, 45, 20);

          //---- WVS01 ----
          WVS01.setComponentPopupMenu(BTD);
          WVS01.setName("WVS01");
          panel3.add(WVS01);
          WVS01.setBounds(140, 70, 50, WVS01.getPreferredSize().height);

          //---- WVS02 ----
          WVS02.setComponentPopupMenu(BTD);
          WVS02.setName("WVS02");
          panel3.add(WVS02);
          WVS02.setBounds(365, 70, 50, 28);

          //---- WVS03 ----
          WVS03.setComponentPopupMenu(BTD);
          WVS03.setName("WVS03");
          panel3.add(WVS03);
          WVS03.setBounds(590, 70, 50, 28);

          //---- WVS04 ----
          WVS04.setComponentPopupMenu(BTD);
          WVS04.setName("WVS04");
          panel3.add(WVS04);
          WVS04.setBounds(140, 100, 50, 28);

          //---- WVS05 ----
          WVS05.setComponentPopupMenu(BTD);
          WVS05.setName("WVS05");
          panel3.add(WVS05);
          WVS05.setBounds(365, 100, 50, 28);

          //---- WVS06 ----
          WVS06.setComponentPopupMenu(BTD);
          WVS06.setName("WVS06");
          panel3.add(WVS06);
          WVS06.setBounds(590, 100, 50, 28);

          //---- WVS07 ----
          WVS07.setComponentPopupMenu(BTD);
          WVS07.setName("WVS07");
          panel3.add(WVS07);
          WVS07.setBounds(140, 130, 50, 28);

          //---- WVS08 ----
          WVS08.setComponentPopupMenu(BTD);
          WVS08.setName("WVS08");
          panel3.add(WVS08);
          WVS08.setBounds(365, 130, 50, 28);

          //---- WVS09 ----
          WVS09.setComponentPopupMenu(BTD);
          WVS09.setName("WVS09");
          panel3.add(WVS09);
          WVS09.setBounds(590, 130, 50, 28);

          //---- WVS10 ----
          WVS10.setComponentPopupMenu(BTD);
          WVS10.setName("WVS10");
          panel3.add(WVS10);
          WVS10.setBounds(140, 160, 50, 28);

          //---- WVS11 ----
          WVS11.setComponentPopupMenu(BTD);
          WVS11.setName("WVS11");
          panel3.add(WVS11);
          WVS11.setBounds(365, 160, 50, 28);

          //---- WVS12 ----
          WVS12.setComponentPopupMenu(BTD);
          WVS12.setName("WVS12");
          panel3.add(WVS12);
          WVS12.setBounds(590, 160, 50, 28);

          //---- WVS13 ----
          WVS13.setComponentPopupMenu(BTD);
          WVS13.setName("WVS13");
          panel3.add(WVS13);
          WVS13.setBounds(140, 190, 50, 28);

          //---- WVS14 ----
          WVS14.setComponentPopupMenu(BTD);
          WVS14.setName("WVS14");
          panel3.add(WVS14);
          WVS14.setBounds(365, 190, 50, 28);

          //---- WVA01 ----
          WVA01.setComponentPopupMenu(BTD);
          WVA01.setName("WVA01");
          panel3.add(WVA01);
          WVA01.setBounds(195, 70, 60, WVA01.getPreferredSize().height);

          //---- WVA02 ----
          WVA02.setComponentPopupMenu(BTD);
          WVA02.setName("WVA02");
          panel3.add(WVA02);
          WVA02.setBounds(420, 70, 60, 28);

          //---- WVA03 ----
          WVA03.setComponentPopupMenu(BTD);
          WVA03.setName("WVA03");
          panel3.add(WVA03);
          WVA03.setBounds(645, 70, 60, 28);

          //---- WVA04 ----
          WVA04.setComponentPopupMenu(BTD);
          WVA04.setName("WVA04");
          panel3.add(WVA04);
          WVA04.setBounds(195, 100, 60, 28);

          //---- WVA05 ----
          WVA05.setComponentPopupMenu(BTD);
          WVA05.setName("WVA05");
          panel3.add(WVA05);
          WVA05.setBounds(420, 100, 60, 28);

          //---- WVA06 ----
          WVA06.setComponentPopupMenu(BTD);
          WVA06.setName("WVA06");
          panel3.add(WVA06);
          WVA06.setBounds(645, 100, 60, 28);

          //---- WVA07 ----
          WVA07.setComponentPopupMenu(BTD);
          WVA07.setName("WVA07");
          panel3.add(WVA07);
          WVA07.setBounds(195, 130, 60, 28);

          //---- WVA08 ----
          WVA08.setComponentPopupMenu(BTD);
          WVA08.setName("WVA08");
          panel3.add(WVA08);
          WVA08.setBounds(420, 130, 60, 28);

          //---- WVA09 ----
          WVA09.setComponentPopupMenu(BTD);
          WVA09.setName("WVA09");
          panel3.add(WVA09);
          WVA09.setBounds(645, 130, 60, 28);

          //---- WVA10 ----
          WVA10.setComponentPopupMenu(BTD);
          WVA10.setName("WVA10");
          panel3.add(WVA10);
          WVA10.setBounds(195, 160, 60, 28);

          //---- WVA11 ----
          WVA11.setComponentPopupMenu(BTD);
          WVA11.setName("WVA11");
          panel3.add(WVA11);
          WVA11.setBounds(420, 160, 60, 28);

          //---- WVA12 ----
          WVA12.setComponentPopupMenu(BTD);
          WVA12.setName("WVA12");
          panel3.add(WVA12);
          WVA12.setBounds(645, 160, 60, 28);

          //---- WVA13 ----
          WVA13.setComponentPopupMenu(BTD);
          WVA13.setName("WVA13");
          panel3.add(WVA13);
          WVA13.setBounds(195, 190, 60, 28);

          //---- WVA14 ----
          WVA14.setComponentPopupMenu(BTD);
          WVA14.setName("WVA14");
          panel3.add(WVA14);
          WVA14.setBounds(420, 190, 60, 28);

          //---- label3 ----
          label3.setText("Aff.");
          label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
          label3.setName("label3");
          panel3.add(label3);
          label3.setBounds(205, 50, 45, 20);

          //---- WVM02 ----
          WVM02.setName("WVM02");
          panel3.add(WVM02);
          WVM02.setBounds(260, 70, 100, 28);

          //---- WVM40 ----
          WVM40.setName("WVM40");
          panel3.add(WVM40);
          WVM40.setBounds(35, 460, 100, 28);

          //---- WVM06 ----
          WVM06.setName("WVM06");
          panel3.add(WVM06);
          WVM06.setBounds(485, 100, 100, 28);

          //---- WVM03 ----
          WVM03.setName("WVM03");
          panel3.add(WVM03);
          WVM03.setBounds(485, 70, 100, 28);

          //---- WVM08 ----
          WVM08.setName("WVM08");
          panel3.add(WVM08);
          WVM08.setBounds(260, 130, 100, WVM08.getPreferredSize().height);

          //---- WVM09 ----
          WVM09.setName("WVM09");
          panel3.add(WVM09);
          WVM09.setBounds(485, 130, 100, 28);

          //---- WVM11 ----
          WVM11.setName("WVM11");
          panel3.add(WVM11);
          WVM11.setBounds(260, 160, 100, 28);

          //---- WVM12 ----
          WVM12.setName("WVM12");
          panel3.add(WVM12);
          WVM12.setBounds(485, 160, 100, 28);

          //---- WVM14 ----
          WVM14.setName("WVM14");
          panel3.add(WVM14);
          WVM14.setBounds(260, 190, 100, 28);

          //---- WVM15 ----
          WVM15.setName("WVM15");
          panel3.add(WVM15);
          WVM15.setBounds(485, 190, 100, 28);

          //---- WVM17 ----
          WVM17.setName("WVM17");
          panel3.add(WVM17);
          WVM17.setBounds(260, 220, 100, 28);

          //---- WVM18 ----
          WVM18.setName("WVM18");
          panel3.add(WVM18);
          WVM18.setBounds(485, 220, 100, 28);

          //---- WVM20 ----
          WVM20.setName("WVM20");
          panel3.add(WVM20);
          WVM20.setBounds(260, 250, 100, 28);

          //---- WVS15 ----
          WVS15.setComponentPopupMenu(BTD);
          WVS15.setName("WVS15");
          panel3.add(WVS15);
          WVS15.setBounds(590, 190, 50, 28);

          //---- WVA15 ----
          WVA15.setComponentPopupMenu(BTD);
          WVA15.setName("WVA15");
          panel3.add(WVA15);
          WVA15.setBounds(645, 190, 60, 28);

          //---- WVS16 ----
          WVS16.setComponentPopupMenu(BTD);
          WVS16.setName("WVS16");
          panel3.add(WVS16);
          WVS16.setBounds(140, 220, 50, 28);

          //---- WVS17 ----
          WVS17.setComponentPopupMenu(BTD);
          WVS17.setName("WVS17");
          panel3.add(WVS17);
          WVS17.setBounds(365, 220, 50, 28);

          //---- WVS18 ----
          WVS18.setComponentPopupMenu(BTD);
          WVS18.setName("WVS18");
          panel3.add(WVS18);
          WVS18.setBounds(590, 220, 50, 28);

          //---- WVS19 ----
          WVS19.setComponentPopupMenu(BTD);
          WVS19.setName("WVS19");
          panel3.add(WVS19);
          WVS19.setBounds(140, 250, 50, 28);

          //---- WVS20 ----
          WVS20.setComponentPopupMenu(BTD);
          WVS20.setName("WVS20");
          panel3.add(WVS20);
          WVS20.setBounds(365, 250, 50, 28);

          //---- WVS21 ----
          WVS21.setComponentPopupMenu(BTD);
          WVS21.setName("WVS21");
          panel3.add(WVS21);
          WVS21.setBounds(590, 250, 50, 28);

          //---- WVS22 ----
          WVS22.setComponentPopupMenu(BTD);
          WVS22.setName("WVS22");
          panel3.add(WVS22);
          WVS22.setBounds(140, 280, 50, 28);

          //---- WVS23 ----
          WVS23.setComponentPopupMenu(BTD);
          WVS23.setName("WVS23");
          panel3.add(WVS23);
          WVS23.setBounds(365, 280, 50, 28);

          //---- WVS24 ----
          WVS24.setComponentPopupMenu(BTD);
          WVS24.setName("WVS24");
          panel3.add(WVS24);
          WVS24.setBounds(590, 280, 50, 28);

          //---- WVS25 ----
          WVS25.setComponentPopupMenu(BTD);
          WVS25.setName("WVS25");
          panel3.add(WVS25);
          WVS25.setBounds(140, 310, 50, 28);

          //---- WVS26 ----
          WVS26.setComponentPopupMenu(BTD);
          WVS26.setName("WVS26");
          panel3.add(WVS26);
          WVS26.setBounds(365, 310, 50, 28);

          //---- WVS27 ----
          WVS27.setComponentPopupMenu(BTD);
          WVS27.setName("WVS27");
          panel3.add(WVS27);
          WVS27.setBounds(590, 310, 50, 28);

          //---- WVS28 ----
          WVS28.setComponentPopupMenu(BTD);
          WVS28.setName("WVS28");
          panel3.add(WVS28);
          WVS28.setBounds(140, 340, 50, 28);

          //---- WVS29 ----
          WVS29.setComponentPopupMenu(BTD);
          WVS29.setName("WVS29");
          panel3.add(WVS29);
          WVS29.setBounds(365, 340, 50, 28);

          //---- WVS30 ----
          WVS30.setComponentPopupMenu(BTD);
          WVS30.setName("WVS30");
          panel3.add(WVS30);
          WVS30.setBounds(590, 340, 50, 28);

          //---- WVS31 ----
          WVS31.setComponentPopupMenu(BTD);
          WVS31.setName("WVS31");
          panel3.add(WVS31);
          WVS31.setBounds(140, 370, 50, 28);

          //---- WVS32 ----
          WVS32.setComponentPopupMenu(BTD);
          WVS32.setName("WVS32");
          panel3.add(WVS32);
          WVS32.setBounds(365, 370, 50, 28);

          //---- WVS33 ----
          WVS33.setComponentPopupMenu(BTD);
          WVS33.setName("WVS33");
          panel3.add(WVS33);
          WVS33.setBounds(590, 370, 50, 28);

          //---- WVS34 ----
          WVS34.setComponentPopupMenu(BTD);
          WVS34.setName("WVS34");
          panel3.add(WVS34);
          WVS34.setBounds(140, 400, 50, 28);

          //---- WVS35 ----
          WVS35.setComponentPopupMenu(BTD);
          WVS35.setName("WVS35");
          panel3.add(WVS35);
          WVS35.setBounds(365, 400, 50, 28);

          //---- WVS36 ----
          WVS36.setComponentPopupMenu(BTD);
          WVS36.setName("WVS36");
          panel3.add(WVS36);
          WVS36.setBounds(590, 400, 50, 28);

          //---- WVS37 ----
          WVS37.setComponentPopupMenu(BTD);
          WVS37.setName("WVS37");
          panel3.add(WVS37);
          WVS37.setBounds(140, 430, 50, 28);

          //---- WVS38 ----
          WVS38.setComponentPopupMenu(BTD);
          WVS38.setName("WVS38");
          panel3.add(WVS38);
          WVS38.setBounds(365, 430, 50, 28);

          //---- WVS39 ----
          WVS39.setComponentPopupMenu(BTD);
          WVS39.setName("WVS39");
          panel3.add(WVS39);
          WVS39.setBounds(590, 430, 50, 28);

          //---- WVS40 ----
          WVS40.setComponentPopupMenu(BTD);
          WVS40.setName("WVS40");
          panel3.add(WVS40);
          WVS40.setBounds(140, 460, 50, 28);

          //---- WVS41 ----
          WVS41.setComponentPopupMenu(BTD);
          WVS41.setName("WVS41");
          panel3.add(WVS41);
          WVS41.setBounds(365, 460, 50, 28);

          //---- WVS42 ----
          WVS42.setComponentPopupMenu(BTD);
          WVS42.setName("WVS42");
          panel3.add(WVS42);
          WVS42.setBounds(590, 460, 50, 28);

          //---- WVA16 ----
          WVA16.setComponentPopupMenu(BTD);
          WVA16.setName("WVA16");
          panel3.add(WVA16);
          WVA16.setBounds(195, 220, 60, 28);

          //---- WVA17 ----
          WVA17.setComponentPopupMenu(BTD);
          WVA17.setName("WVA17");
          panel3.add(WVA17);
          WVA17.setBounds(420, 220, 60, 28);

          //---- WVA18 ----
          WVA18.setComponentPopupMenu(BTD);
          WVA18.setName("WVA18");
          panel3.add(WVA18);
          WVA18.setBounds(645, 220, 60, 28);

          //---- WVA19 ----
          WVA19.setComponentPopupMenu(BTD);
          WVA19.setName("WVA19");
          panel3.add(WVA19);
          WVA19.setBounds(195, 250, 60, 28);

          //---- WVA20 ----
          WVA20.setComponentPopupMenu(BTD);
          WVA20.setName("WVA20");
          panel3.add(WVA20);
          WVA20.setBounds(420, 250, 60, 28);

          //---- WVA21 ----
          WVA21.setComponentPopupMenu(BTD);
          WVA21.setName("WVA21");
          panel3.add(WVA21);
          WVA21.setBounds(645, 250, 60, 28);

          //---- WVA22 ----
          WVA22.setComponentPopupMenu(BTD);
          WVA22.setName("WVA22");
          panel3.add(WVA22);
          WVA22.setBounds(195, 280, 60, 28);

          //---- WVA23 ----
          WVA23.setComponentPopupMenu(BTD);
          WVA23.setName("WVA23");
          panel3.add(WVA23);
          WVA23.setBounds(420, 280, 60, 28);

          //---- WVA24 ----
          WVA24.setComponentPopupMenu(BTD);
          WVA24.setName("WVA24");
          panel3.add(WVA24);
          WVA24.setBounds(645, 280, 60, 28);

          //---- WVA25 ----
          WVA25.setComponentPopupMenu(BTD);
          WVA25.setName("WVA25");
          panel3.add(WVA25);
          WVA25.setBounds(195, 310, 60, 28);

          //---- WVM21 ----
          WVM21.setName("WVM21");
          panel3.add(WVM21);
          WVM21.setBounds(485, 250, 100, 28);

          //---- WVM23 ----
          WVM23.setName("WVM23");
          panel3.add(WVM23);
          WVM23.setBounds(260, 280, 100, 28);

          //---- WVM24 ----
          WVM24.setName("WVM24");
          panel3.add(WVM24);
          WVM24.setBounds(485, 280, 100, 28);

          //---- WVM26 ----
          WVM26.setName("WVM26");
          panel3.add(WVM26);
          WVM26.setBounds(260, 310, 100, 28);

          //---- WVM27 ----
          WVM27.setName("WVM27");
          panel3.add(WVM27);
          WVM27.setBounds(485, 310, 100, 28);

          //---- WVM29 ----
          WVM29.setName("WVM29");
          panel3.add(WVM29);
          WVM29.setBounds(260, 340, 100, 28);

          //---- WVM30 ----
          WVM30.setName("WVM30");
          panel3.add(WVM30);
          WVM30.setBounds(485, 340, 100, 28);

          //---- WVM32 ----
          WVM32.setName("WVM32");
          panel3.add(WVM32);
          WVM32.setBounds(260, 370, 100, 28);

          //---- WVM33 ----
          WVM33.setName("WVM33");
          panel3.add(WVM33);
          WVM33.setBounds(485, 370, 100, 28);

          //---- WVM35 ----
          WVM35.setName("WVM35");
          panel3.add(WVM35);
          WVM35.setBounds(260, 400, 100, 28);

          //---- WVM36 ----
          WVM36.setName("WVM36");
          panel3.add(WVM36);
          WVM36.setBounds(485, 400, 100, 28);

          //---- WVM38 ----
          WVM38.setName("WVM38");
          panel3.add(WVM38);
          WVM38.setBounds(260, 430, 100, 28);

          //---- WVM39 ----
          WVM39.setName("WVM39");
          panel3.add(WVM39);
          WVM39.setBounds(485, 430, 100, 28);

          //---- WVM41 ----
          WVM41.setName("WVM41");
          panel3.add(WVM41);
          WVM41.setBounds(260, 460, 100, 28);

          //---- WVM42 ----
          WVM42.setName("WVM42");
          panel3.add(WVM42);
          WVM42.setBounds(485, 460, 100, 28);

          //---- WVA26 ----
          WVA26.setComponentPopupMenu(BTD);
          WVA26.setName("WVA26");
          panel3.add(WVA26);
          WVA26.setBounds(420, 310, 60, 28);

          //---- WVA27 ----
          WVA27.setComponentPopupMenu(BTD);
          WVA27.setName("WVA27");
          panel3.add(WVA27);
          WVA27.setBounds(645, 310, 60, 28);

          //---- WVA28 ----
          WVA28.setComponentPopupMenu(BTD);
          WVA28.setName("WVA28");
          panel3.add(WVA28);
          WVA28.setBounds(195, 340, 60, 28);

          //---- WVA29 ----
          WVA29.setComponentPopupMenu(BTD);
          WVA29.setName("WVA29");
          panel3.add(WVA29);
          WVA29.setBounds(420, 340, 60, 28);

          //---- WVA30 ----
          WVA30.setComponentPopupMenu(BTD);
          WVA30.setName("WVA30");
          panel3.add(WVA30);
          WVA30.setBounds(645, 340, 60, 28);

          //---- WVA31 ----
          WVA31.setComponentPopupMenu(BTD);
          WVA31.setName("WVA31");
          panel3.add(WVA31);
          WVA31.setBounds(195, 370, 60, 28);

          //---- WVA32 ----
          WVA32.setComponentPopupMenu(BTD);
          WVA32.setName("WVA32");
          panel3.add(WVA32);
          WVA32.setBounds(420, 370, 60, 28);

          //---- WVA33 ----
          WVA33.setComponentPopupMenu(BTD);
          WVA33.setName("WVA33");
          panel3.add(WVA33);
          WVA33.setBounds(645, 370, 60, 28);

          //---- WVA34 ----
          WVA34.setComponentPopupMenu(BTD);
          WVA34.setName("WVA34");
          panel3.add(WVA34);
          WVA34.setBounds(195, 400, 60, 28);

          //---- WVA35 ----
          WVA35.setComponentPopupMenu(BTD);
          WVA35.setName("WVA35");
          panel3.add(WVA35);
          WVA35.setBounds(420, 400, 60, 28);

          //---- WVA36 ----
          WVA36.setComponentPopupMenu(BTD);
          WVA36.setName("WVA36");
          panel3.add(WVA36);
          WVA36.setBounds(645, 400, 60, 28);

          //---- WVA37 ----
          WVA37.setComponentPopupMenu(BTD);
          WVA37.setName("WVA37");
          panel3.add(WVA37);
          WVA37.setBounds(195, 430, 60, 28);

          //---- WVA38 ----
          WVA38.setComponentPopupMenu(BTD);
          WVA38.setName("WVA38");
          panel3.add(WVA38);
          WVA38.setBounds(420, 430, 60, 28);

          //---- WVA39 ----
          WVA39.setComponentPopupMenu(BTD);
          WVA39.setName("WVA39");
          panel3.add(WVA39);
          WVA39.setBounds(645, 430, 60, 28);

          //---- WVA40 ----
          WVA40.setComponentPopupMenu(BTD);
          WVA40.setName("WVA40");
          panel3.add(WVA40);
          WVA40.setBounds(195, 460, 60, 28);

          //---- WVA41 ----
          WVA41.setComponentPopupMenu(BTD);
          WVA41.setName("WVA41");
          panel3.add(WVA41);
          WVA41.setBounds(420, 460, 60, 28);

          //---- WVA42 ----
          WVA42.setComponentPopupMenu(BTD);
          WVA42.setName("WVA42");
          panel3.add(WVA42);
          WVA42.setBounds(645, 460, 60, 28);

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- WMTVTL ----
            WMTVTL.setComponentPopupMenu(BTD);
            WMTVTL.setText("@WMTVTL@");
            WMTVTL.setHorizontalAlignment(SwingConstants.RIGHT);
            WMTVTL.setName("WMTVTL");
            panel4.add(WMTVTL);
            WMTVTL.setBounds(125, 5, 100, WMTVTL.getPreferredSize().height);

            //---- WECVTL ----
            WECVTL.setComponentPopupMenu(BTD);
            WECVTL.setText("@WECVTL@");
            WECVTL.setHorizontalAlignment(SwingConstants.RIGHT);
            WECVTL.setForeground(Color.red);
            WECVTL.setFont(WECVTL.getFont().deriveFont(WECVTL.getFont().getStyle() | Font.BOLD));
            WECVTL.setName("WECVTL");
            panel4.add(WECVTL);
            WECVTL.setBounds(125, 35, 100, WECVTL.getPreferredSize().height);

            //---- OBJ_29 ----
            OBJ_29.setText("Montant ventillation");
            OBJ_29.setName("OBJ_29");
            panel4.add(OBJ_29);
            OBJ_29.setBounds(10, 8, 117, 18);

            //---- OBJ_30 ----
            OBJ_30.setText("Ecart");
            OBJ_30.setName("OBJ_30");
            panel4.add(OBJ_30);
            OBJ_30.setBounds(10, 38, 52, 18);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }
          panel3.add(panel4);
          panel4.setBounds(480, 495, 245, 70);

          //---- label4 ----
          label4.setText("@LIBMT2@");
          label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
          label4.setName("label4");
          panel3.add(label4);
          label4.setBounds(265, 50, 100, 20);

          //---- label5 ----
          label5.setText("@LIBMT3@");
          label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
          label5.setName("label5");
          panel3.add(label5);
          label5.setBounds(490, 50, 100, 20);

          //---- label6 ----
          label6.setText("Sect.");
          label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
          label6.setName("label6");
          panel3.add(label6);
          label6.setBounds(370, 50, 45, 20);

          //---- label7 ----
          label7.setText("Aff.");
          label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD));
          label7.setName("label7");
          panel3.add(label7);
          label7.setBounds(430, 50, 45, 20);

          //---- label8 ----
          label8.setText("Aff.");
          label8.setFont(label8.getFont().deriveFont(label8.getFont().getStyle() | Font.BOLD));
          label8.setName("label8");
          panel3.add(label8);
          label8.setBounds(655, 50, 45, 20);

          //---- label9 ----
          label9.setText("Sect.");
          label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD));
          label9.setName("label9");
          panel3.add(label9);
          label9.setBounds(595, 50, 45, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel3.getComponentCount(); i++) {
              Rectangle bounds = panel3.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel3.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel3.setMinimumSize(preferredSize);
            panel3.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 739, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(19, Short.MAX_VALUE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 585, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(17, Short.MAX_VALUE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_6 ----
      OBJ_6.setText("Choix possibles");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      BTD.add(OBJ_6);

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTD.add(OBJ_5);
    }

    //======== menus_haut ========
    {
      menus_haut.setMinimumSize(new Dimension(160, 520));
      menus_haut.setPreferredSize(new Dimension(160, 520));
      menus_haut.setBackground(new Color(238, 239, 241));
      menus_haut.setAutoscrolls(true);
      menus_haut.setName("menus_haut");
      menus_haut.setLayout(new VerticalLayout());

      //======== riMenu_V01F ========
      {
        riMenu_V01F.setMinimumSize(new Dimension(104, 50));
        riMenu_V01F.setPreferredSize(new Dimension(170, 50));
        riMenu_V01F.setMaximumSize(new Dimension(104, 50));
        riMenu_V01F.setName("riMenu_V01F");

        //---- riMenu_bt_V01F ----
        riMenu_bt_V01F.setText("@V01F@");
        riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
        riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
        riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
        riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
        riMenu_bt_V01F.setName("riMenu_bt_V01F");
        riMenu_V01F.add(riMenu_bt_V01F);
      }
      menus_haut.add(riMenu_V01F);

      //======== riSousMenu_consult ========
      {
        riSousMenu_consult.setName("riSousMenu_consult");

        //---- riSousMenu_bt_consult ----
        riSousMenu_bt_consult.setText("Consultation");
        riSousMenu_bt_consult.setToolTipText("Consultation");
        riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
        riSousMenu_consult.add(riSousMenu_bt_consult);
      }
      menus_haut.add(riSousMenu_consult);

      //======== riSousMenu_modif ========
      {
        riSousMenu_modif.setName("riSousMenu_modif");

        //---- riSousMenu_bt_modif ----
        riSousMenu_bt_modif.setText("Modification");
        riSousMenu_bt_modif.setToolTipText("Modification");
        riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
        riSousMenu_modif.add(riSousMenu_bt_modif);
      }
      menus_haut.add(riSousMenu_modif);

      //======== riSousMenu_crea ========
      {
        riSousMenu_crea.setName("riSousMenu_crea");

        //---- riSousMenu_bt_crea ----
        riSousMenu_bt_crea.setText("Cr\u00e9ation");
        riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
        riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
        riSousMenu_crea.add(riSousMenu_bt_crea);
      }
      menus_haut.add(riSousMenu_crea);

      //======== riSousMenu_suppr ========
      {
        riSousMenu_suppr.setName("riSousMenu_suppr");

        //---- riSousMenu_bt_suppr ----
        riSousMenu_bt_suppr.setText("Annulation");
        riSousMenu_bt_suppr.setToolTipText("Annulation");
        riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
        riSousMenu_suppr.add(riSousMenu_bt_suppr);
      }
      menus_haut.add(riSousMenu_suppr);

      //======== riSousMenuF_dupli ========
      {
        riSousMenuF_dupli.setName("riSousMenuF_dupli");

        //---- riSousMenu_bt_dupli ----
        riSousMenu_bt_dupli.setText("Duplication");
        riSousMenu_bt_dupli.setToolTipText("Duplication");
        riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
        riSousMenuF_dupli.add(riSousMenu_bt_dupli);
      }
      menus_haut.add(riSousMenuF_dupli);

      //======== riSousMenu_rappel ========
      {
        riSousMenu_rappel.setName("riSousMenu_rappel");

        //---- riSousMenu_bt_rappel ----
        riSousMenu_bt_rappel.setText("Rappel");
        riSousMenu_bt_rappel.setToolTipText("Rappel");
        riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
        riSousMenu_rappel.add(riSousMenu_bt_rappel);
      }
      menus_haut.add(riSousMenu_rappel);

      //======== riSousMenu_reac ========
      {
        riSousMenu_reac.setName("riSousMenu_reac");

        //---- riSousMenu_bt_reac ----
        riSousMenu_bt_reac.setText("R\u00e9activation");
        riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
        riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
        riSousMenu_reac.add(riSousMenu_bt_reac);
      }
      menus_haut.add(riSousMenu_reac);

      //======== riSousMenu_destr ========
      {
        riSousMenu_destr.setName("riSousMenu_destr");

        //---- riSousMenu_bt_destr ----
        riSousMenu_bt_destr.setText("Suppression");
        riSousMenu_bt_destr.setToolTipText("Suppression");
        riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
        riSousMenu_destr.add(riSousMenu_bt_destr);
      }
      menus_haut.add(riSousMenu_destr);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel3;
  private JLabel label1;
  private XRiTextField WVM01;
  private XRiTextField WVM04;
  private XRiTextField WVM07;
  private XRiTextField WVM34;
  private XRiTextField WVM37;
  private XRiTextField WVM16;
  private XRiTextField WVM19;
  private XRiTextField WVM22;
  private XRiTextField WVM25;
  private XRiTextField WVM10;
  private XRiTextField WVM05;
  private XRiTextField WVM28;
  private XRiTextField WVM13;
  private XRiTextField WVM31;
  private JLabel label2;
  private XRiTextField WVS01;
  private XRiTextField WVS02;
  private XRiTextField WVS03;
  private XRiTextField WVS04;
  private XRiTextField WVS05;
  private XRiTextField WVS06;
  private XRiTextField WVS07;
  private XRiTextField WVS08;
  private XRiTextField WVS09;
  private XRiTextField WVS10;
  private XRiTextField WVS11;
  private XRiTextField WVS12;
  private XRiTextField WVS13;
  private XRiTextField WVS14;
  private XRiTextField WVA01;
  private XRiTextField WVA02;
  private XRiTextField WVA03;
  private XRiTextField WVA04;
  private XRiTextField WVA05;
  private XRiTextField WVA06;
  private XRiTextField WVA07;
  private XRiTextField WVA08;
  private XRiTextField WVA09;
  private XRiTextField WVA10;
  private XRiTextField WVA11;
  private XRiTextField WVA12;
  private XRiTextField WVA13;
  private XRiTextField WVA14;
  private JLabel label3;
  private XRiTextField WVM02;
  private XRiTextField WVM40;
  private XRiTextField WVM06;
  private XRiTextField WVM03;
  private XRiTextField WVM08;
  private XRiTextField WVM09;
  private XRiTextField WVM11;
  private XRiTextField WVM12;
  private XRiTextField WVM14;
  private XRiTextField WVM15;
  private XRiTextField WVM17;
  private XRiTextField WVM18;
  private XRiTextField WVM20;
  private XRiTextField WVS15;
  private XRiTextField WVA15;
  private XRiTextField WVS16;
  private XRiTextField WVS17;
  private XRiTextField WVS18;
  private XRiTextField WVS19;
  private XRiTextField WVS20;
  private XRiTextField WVS21;
  private XRiTextField WVS22;
  private XRiTextField WVS23;
  private XRiTextField WVS24;
  private XRiTextField WVS25;
  private XRiTextField WVS26;
  private XRiTextField WVS27;
  private XRiTextField WVS28;
  private XRiTextField WVS29;
  private XRiTextField WVS30;
  private XRiTextField WVS31;
  private XRiTextField WVS32;
  private XRiTextField WVS33;
  private XRiTextField WVS34;
  private XRiTextField WVS35;
  private XRiTextField WVS36;
  private XRiTextField WVS37;
  private XRiTextField WVS38;
  private XRiTextField WVS39;
  private XRiTextField WVS40;
  private XRiTextField WVS41;
  private XRiTextField WVS42;
  private XRiTextField WVA16;
  private XRiTextField WVA17;
  private XRiTextField WVA18;
  private XRiTextField WVA19;
  private XRiTextField WVA20;
  private XRiTextField WVA21;
  private XRiTextField WVA22;
  private XRiTextField WVA23;
  private XRiTextField WVA24;
  private XRiTextField WVA25;
  private XRiTextField WVM21;
  private XRiTextField WVM23;
  private XRiTextField WVM24;
  private XRiTextField WVM26;
  private XRiTextField WVM27;
  private XRiTextField WVM29;
  private XRiTextField WVM30;
  private XRiTextField WVM32;
  private XRiTextField WVM33;
  private XRiTextField WVM35;
  private XRiTextField WVM36;
  private XRiTextField WVM38;
  private XRiTextField WVM39;
  private XRiTextField WVM41;
  private XRiTextField WVM42;
  private XRiTextField WVA26;
  private XRiTextField WVA27;
  private XRiTextField WVA28;
  private XRiTextField WVA29;
  private XRiTextField WVA30;
  private XRiTextField WVA31;
  private XRiTextField WVA32;
  private XRiTextField WVA33;
  private XRiTextField WVA34;
  private XRiTextField WVA35;
  private XRiTextField WVA36;
  private XRiTextField WVA37;
  private XRiTextField WVA38;
  private XRiTextField WVA39;
  private XRiTextField WVA40;
  private XRiTextField WVA41;
  private XRiTextField WVA42;
  private JPanel panel4;
  private RiZoneSortie WMTVTL;
  private RiZoneSortie WECVTL;
  private JLabel OBJ_29;
  private JLabel OBJ_30;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private JPopupMenu BTD;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_5;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
