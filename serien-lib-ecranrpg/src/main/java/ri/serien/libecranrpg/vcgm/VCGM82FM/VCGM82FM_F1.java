
package ri.serien.libecranrpg.vcgm.VCGM82FM;
// Nom Fichier: pop_VCGM05FM_FMTERR_367.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class VCGM82FM_F1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM82FM_F1(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    // setDefaultButton(OK);
    p_DemandeConf.setVisible(false);
    p_Option.setVisible(false);
    p_AppuyerEntree.setVisible(false);
    p_incorrect.setVisible(false);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V05F@")).trim());
    OBJ_10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F/10,3^@")).trim());
    OBJ_6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F/14,3^@")).trim());
    OBJ_7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F/18,3^@")).trim());
    OBJ_8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F/22,3^@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V05F@")).trim());
    OBJ_9_OBJ_10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F/0,17^@")).trim());
    OBJ_11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F/19,3^@")).trim());
    OBJ_9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F/23,3^@")).trim());
    OBJ_12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F/27,3^@")).trim());
    OBJ_13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F/31,3^@")).trim());
  }
  
  @Override
  public void setData() {
    String chaine = "";
    
    super.setData();
    
    
    
    
    chaine = lexique.HostFieldGetData("V02F").trim();
    
    if (chaine.equals("O.K") || chaine.equals("")) {
      // Bouton par défaut
      if (lexique.HostFieldGetData("V06F").trim().equals("NON")) {
        setDefaultButton(button1);
      }
      else {
        setDefaultButton(OK);
      }
      p_DemandeConf.setVisible(true);
      p_Option.setVisible(false);
      p_AppuyerEntree.setVisible(false);
      p_incorrect.setVisible(false);
    }
    else if (chaine.equals("O51")) {
      
      if (lexique.isTrue("61")) {
        OBJ_11.setVisible(!interpreteurD.analyseExpression("@V03F/10,3^@").trim().equalsIgnoreCase(""));
        OBJ_9.setVisible(!interpreteurD.analyseExpression("@V03F/14,3^@").trim().equalsIgnoreCase(""));
        OBJ_12.setVisible(!interpreteurD.analyseExpression("@V03F/18,3^@").trim().equalsIgnoreCase(""));
        OBJ_13.setVisible(!interpreteurD.analyseExpression("@V03F/22,3^@").trim().equalsIgnoreCase(""));
        p_DemandeConf.setVisible(false);
        p_Option.setVisible(false);
        p_incorrect.setVisible(true);
        p_AppuyerEntree.setVisible(false);
      }
      else {
        OBJ_10.setVisible(!interpreteurD.analyseExpression("@V03F/10,3^@").trim().equalsIgnoreCase(""));
        OBJ_6.setVisible(!interpreteurD.analyseExpression("@V03F/14,3^@").trim().equalsIgnoreCase(""));
        OBJ_7.setVisible(!interpreteurD.analyseExpression("@V03F/18,3^@").trim().equalsIgnoreCase(""));
        OBJ_8.setVisible(!interpreteurD.analyseExpression("@V03F/22,3^@").trim().equalsIgnoreCase(""));
        p_DemandeConf.setVisible(false);
        p_Option.setVisible(true);
        p_AppuyerEntree.setVisible(false);
        p_incorrect.setVisible(false);
      }
      
    }
    else if (chaine.equals("OKK") || chaine.equals("ERR")) {
      // setDefaultButton(bt_ENTER);
      p_DemandeConf.setVisible(false);
      p_Option.setVisible(false);
      p_AppuyerEntree.setVisible(true);
      p_incorrect.setVisible(false);
    }
    else {
      // TODO Icones
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@V03F@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OKActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, "OUI");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void button1ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, "NON");
    lexique.HostScreenSendKey(this, "ENTER");
    
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, OBJ_6.getText());
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, OBJ_7.getText());
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, OBJ_8.getText());
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, OBJ_10.getText());
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void bt_ENTERActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, OBJ_11.getText());
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, OBJ_9.getText());
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, OBJ_12.getText());
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, OBJ_13.getText());
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_DemandeConf = new JPanel();
    OBJ_5 = new JLabel();
    OK = new JButton();
    button1 = new JButton();
    p_Option = new JPanel();
    OBJ_9_OBJ_9 = new JLabel();
    OBJ_10 = new JButton();
    OBJ_6 = new JButton();
    OBJ_7 = new JButton();
    OBJ_8 = new JButton();
    p_AppuyerEntree = new JPanel();
    bt_ENTER = new JButton();
    label1 = new JLabel();
    p_incorrect = new JPanel();
    OBJ_9_OBJ_10 = new JLabel();
    OBJ_11 = new JButton();
    OBJ_9 = new JButton();
    OBJ_12 = new JButton();
    OBJ_13 = new JButton();

    //======== this ========
    setName("this");
    setLayout(null);

    //======== p_DemandeConf ========
    {
      p_DemandeConf.setName("p_DemandeConf");

      //---- OBJ_5 ----
      OBJ_5.setText("@V05F@");
      OBJ_5.setHorizontalAlignment(SwingConstants.CENTER);
      OBJ_5.setRequestFocusEnabled(false);
      OBJ_5.setName("OBJ_5");

      //---- OK ----
      OK.setText("Oui");
      OK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OK.setName("OK");
      OK.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OKActionPerformed(e);
        }
      });

      //---- button1 ----
      button1.setText("Non");
      button1.setName("button1");
      button1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          button1ActionPerformed(e);
        }
      });

      GroupLayout p_DemandeConfLayout = new GroupLayout(p_DemandeConf);
      p_DemandeConf.setLayout(p_DemandeConfLayout);
      p_DemandeConfLayout.setHorizontalGroup(
        p_DemandeConfLayout.createParallelGroup()
          .addGroup(p_DemandeConfLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addComponent(OBJ_5, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
          .addGroup(p_DemandeConfLayout.createSequentialGroup()
            .addGap(70, 70, 70)
            .addComponent(OK, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE)
            .addGap(33, 33, 33)
            .addComponent(button1, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE))
      );
      p_DemandeConfLayout.setVerticalGroup(
        p_DemandeConfLayout.createParallelGroup()
          .addGroup(p_DemandeConfLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addComponent(OBJ_5, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
            .addGap(25, 25, 25)
            .addGroup(p_DemandeConfLayout.createParallelGroup()
              .addComponent(OK, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
              .addComponent(button1, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)))
      );
    }
    add(p_DemandeConf);
    p_DemandeConf.setBounds(15, 0, 325, 100);

    //======== p_Option ========
    {
      p_Option.setName("p_Option");

      //---- OBJ_9_OBJ_9 ----
      OBJ_9_OBJ_9.setText("Option ?");
      OBJ_9_OBJ_9.setHorizontalAlignment(SwingConstants.CENTER);
      OBJ_9_OBJ_9.setName("OBJ_9_OBJ_9");

      //---- OBJ_10 ----
      OBJ_10.setText("@V03F/10,3^@");
      OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });

      //---- OBJ_6 ----
      OBJ_6.setText("@V03F/14,3^@");
      OBJ_6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });

      //---- OBJ_7 ----
      OBJ_7.setText("@V03F/18,3^@");
      OBJ_7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });

      //---- OBJ_8 ----
      OBJ_8.setText("@V03F/22,3^@");
      OBJ_8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });

      GroupLayout p_OptionLayout = new GroupLayout(p_Option);
      p_Option.setLayout(p_OptionLayout);
      p_OptionLayout.setHorizontalGroup(
        p_OptionLayout.createParallelGroup()
          .addGroup(p_OptionLayout.createSequentialGroup()
            .addGap(20, 20, 20)
            .addComponent(OBJ_9_OBJ_9, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE))
          .addGroup(p_OptionLayout.createSequentialGroup()
            .addGap(60, 60, 60)
            .addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE)
            .addGap(16, 16, 16)
            .addComponent(OBJ_6, GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE))
          .addGroup(p_OptionLayout.createSequentialGroup()
            .addGap(60, 60, 60)
            .addComponent(OBJ_7, GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE)
            .addGap(16, 16, 16)
            .addComponent(OBJ_8, GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE))
      );
      p_OptionLayout.setVerticalGroup(
        p_OptionLayout.createParallelGroup()
          .addGroup(p_OptionLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addComponent(OBJ_9_OBJ_9, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
            .addGap(10, 10, 10)
            .addGroup(p_OptionLayout.createParallelGroup()
              .addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_6, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
            .addGap(6, 6, 6)
            .addGroup(p_OptionLayout.createParallelGroup()
              .addComponent(OBJ_7, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_8, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)))
      );
    }
    add(p_Option);
    p_Option.setBounds(10, 0, 325, 100);

    //======== p_AppuyerEntree ========
    {
      p_AppuyerEntree.setName("p_AppuyerEntree");

      //---- bt_ENTER ----
      bt_ENTER.setText("Ok");
      bt_ENTER.setName("bt_ENTER");
      bt_ENTER.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_ENTERActionPerformed(e);
        }
      });

      //---- label1 ----
      label1.setText("@V05F@");
      label1.setHorizontalAlignment(SwingConstants.CENTER);
      label1.setName("label1");

      GroupLayout p_AppuyerEntreeLayout = new GroupLayout(p_AppuyerEntree);
      p_AppuyerEntree.setLayout(p_AppuyerEntreeLayout);
      p_AppuyerEntreeLayout.setHorizontalGroup(
        p_AppuyerEntreeLayout.createParallelGroup()
          .addGroup(p_AppuyerEntreeLayout.createSequentialGroup()
            .addGap(15, 15, 15)
            .addComponent(label1, GroupLayout.PREFERRED_SIZE, 285, GroupLayout.PREFERRED_SIZE))
          .addGroup(p_AppuyerEntreeLayout.createSequentialGroup()
            .addGap(135, 135, 135)
            .addComponent(bt_ENTER))
      );
      p_AppuyerEntreeLayout.setVerticalGroup(
        p_AppuyerEntreeLayout.createParallelGroup()
          .addGroup(p_AppuyerEntreeLayout.createSequentialGroup()
            .addGap(15, 15, 15)
            .addComponent(label1)
            .addGap(24, 24, 24)
            .addComponent(bt_ENTER))
      );
    }
    add(p_AppuyerEntree);
    p_AppuyerEntree.setBounds(5, 0, 335, 105);

    //======== p_incorrect ========
    {
      p_incorrect.setName("p_incorrect");

      //---- OBJ_9_OBJ_10 ----
      OBJ_9_OBJ_10.setHorizontalAlignment(SwingConstants.CENTER);
      OBJ_9_OBJ_10.setText("@V03F/0,17^@");
      OBJ_9_OBJ_10.setName("OBJ_9_OBJ_10");

      //---- OBJ_11 ----
      OBJ_11.setText("@V03F/19,3^@");
      OBJ_11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });

      //---- OBJ_9 ----
      OBJ_9.setText("@V03F/23,3^@");
      OBJ_9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });

      //---- OBJ_12 ----
      OBJ_12.setText("@V03F/27,3^@");
      OBJ_12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });

      //---- OBJ_13 ----
      OBJ_13.setText("@V03F/31,3^@");
      OBJ_13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });

      GroupLayout p_incorrectLayout = new GroupLayout(p_incorrect);
      p_incorrect.setLayout(p_incorrectLayout);
      p_incorrectLayout.setHorizontalGroup(
        p_incorrectLayout.createParallelGroup()
          .addGroup(p_incorrectLayout.createSequentialGroup()
            .addGap(20, 20, 20)
            .addComponent(OBJ_9_OBJ_10, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE))
          .addGroup(p_incorrectLayout.createSequentialGroup()
            .addGap(60, 60, 60)
            .addComponent(OBJ_11, GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE)
            .addGap(16, 16, 16)
            .addComponent(OBJ_9, GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE))
          .addGroup(p_incorrectLayout.createSequentialGroup()
            .addGap(60, 60, 60)
            .addComponent(OBJ_12, GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE)
            .addGap(16, 16, 16)
            .addComponent(OBJ_13, GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE))
      );
      p_incorrectLayout.setVerticalGroup(
        p_incorrectLayout.createParallelGroup()
          .addGroup(p_incorrectLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addComponent(OBJ_9_OBJ_10, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
            .addGap(10, 10, 10)
            .addGroup(p_incorrectLayout.createParallelGroup()
              .addComponent(OBJ_11, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_9, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
            .addGap(6, 6, 6)
            .addGroup(p_incorrectLayout.createParallelGroup()
              .addComponent(OBJ_12, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_13, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)))
      );
    }
    add(p_incorrect);
    p_incorrect.setBounds(5, 0, 325, 100);

    {
      // compute preferred size
      Dimension preferredSize = new Dimension();
      for(int i = 0; i < getComponentCount(); i++) {
        Rectangle bounds = getComponent(i).getBounds();
        preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
        preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
      }
      Insets insets = getInsets();
      preferredSize.width += insets.right;
      preferredSize.height += insets.bottom;
      setMinimumSize(preferredSize);
      setPreferredSize(preferredSize);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_DemandeConf;
  private JLabel OBJ_5;
  private JButton OK;
  private JButton button1;
  private JPanel p_Option;
  private JLabel OBJ_9_OBJ_9;
  private JButton OBJ_10;
  private JButton OBJ_6;
  private JButton OBJ_7;
  private JButton OBJ_8;
  private JPanel p_AppuyerEntree;
  private JButton bt_ENTER;
  private JLabel label1;
  private JPanel p_incorrect;
  private JLabel OBJ_9_OBJ_10;
  private JButton OBJ_11;
  private JButton OBJ_9;
  private JButton OBJ_12;
  private JButton OBJ_13;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
