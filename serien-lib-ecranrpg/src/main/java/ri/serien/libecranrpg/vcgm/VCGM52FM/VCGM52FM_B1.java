
package ri.serien.libecranrpg.vcgm.VCGM52FM;
// Nom Fichier: pop_VCGM52FM_FMTB1_328.java

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.JXTitledSeparator;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VCGM52FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  // private String[][] _LIST1_Title_Data_Brut=null, _LIST2_Title_Data_Brut=null;
  private String[] _DAT09_Title = { "Date", "Solde", };
  private String[][] _DAT09_Data = { { "DAT09", "SOL09", }, { "DAT10", "SOL10", }, { "DAT11", "SOL11", }, { "DAT12", "SOL12", },
      { "DAT13", "SOL13", }, { "DAT14", "SOL14", }, { "DAT15", "SOL15", }, { "DAT16", "SOL16", }, };
  private int[] _DAT09_Width = { 66, 108, };
  private String[] _DAT01_Title = { "Date", "Solde", };
  private String[][] _DAT01_Data = { { "DAT01", "SOL01", }, { "DAT02", "SOL02", }, { "DAT03", "SOL03", }, { "DAT04", "SOL04", },
      { "DAT05", "SOL05", }, { "DAT06", "SOL06", }, { "DAT07", "SOL07", }, { "DAT08", "SOL08", }, };
  private int[] _DAT01_Width = { 66, 108, };
  private int[] _LIST1_Justification = { SwingConstants.LEFT, SwingConstants.RIGHT };
  private int[] _LIST2_Justification = { SwingConstants.LEFT, SwingConstants.RIGHT };
  
  public VCGM52FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Gestion des listes
    // DAT01.getTableHeader().setFont(new Font("Courier New", Font.BOLD, 12));
    // ((DefaultTableCellRenderer)DAT01.getTableHeader().getDefaultRenderer()).setHorizontalAlignment( JLabel.LEFT );
    // _LIST1_Title_Data_Brut = initTable(LIST1);
    // DAT09.getTableHeader().setFont(new Font("Courier New", Font.BOLD, 12));
    // ((DefaultTableCellRenderer)DAT09.getTableHeader().getDefaultRenderer()).setHorizontalAlignment( JLabel.LEFT );
    // _LIST2_Title_Data_Brut = initTable(LIST2);
    
    // Ajout
    initDiverses();
    DAT09.setAspectTable(null, _DAT09_Title, _DAT09_Data, _DAT09_Width, false, _LIST2_Justification, null, null, null);
    DAT01.setAspectTable(null, _DAT01_Title, _DAT01_Data, _DAT01_Width, false, _LIST1_Justification, null, null, null);
    
    // Bouton par défaut
    setDefaultButton(OBJ_14);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Ajoute à la liste des oData les variables non liée directement à un composant graphique
    // majTable(LIST1, _LIST1_Title_Data_Brut, null, _LIST1_Justification);
    // majTable(LIST2, _LIST2_Title_Data_Brut, null, _LIST2_Justification);
    
    
    
    
    // TODO Icones
    OBJ_14.setIcon(lexique.chargerImage("images/retour.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Solde d'un compte bancaire"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_14 = new JButton();
    xTitledSeparator1 = new JXTitledSeparator();
    SCROLLPANE_LIST1 = new JScrollPane();
    DAT01 = new XRiTable();
    SCROLLPANE_LIST2 = new JScrollPane();
    DAT09 = new XRiTable();

    //======== this ========
    setName("this");

    //---- OBJ_14 ----
    OBJ_14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_14.setName("OBJ_14");
    OBJ_14.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_14ActionPerformed(e);
      }
    });

    //---- xTitledSeparator1 ----
    xTitledSeparator1.setTitle("Soldes en dates de valeur");
    xTitledSeparator1.setName("xTitledSeparator1");

    //======== SCROLLPANE_LIST1 ========
    {
      SCROLLPANE_LIST1.setName("SCROLLPANE_LIST1");

      //---- DAT01 ----
      DAT01.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      DAT01.setName("DAT01");
      SCROLLPANE_LIST1.setViewportView(DAT01);
    }

    //======== SCROLLPANE_LIST2 ========
    {
      SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

      //---- DAT09 ----
      DAT09.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      DAT09.setName("DAT09");
      SCROLLPANE_LIST2.setViewportView(DAT09);
    }

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(10, 10, 10)
          .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, 405, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(20, 20, 20)
          .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
            .addComponent(OBJ_14)
            .addGroup(layout.createSequentialGroup()
              .addComponent(SCROLLPANE_LIST1, GroupLayout.PREFERRED_SIZE, 195, GroupLayout.PREFERRED_SIZE)
              .addGap(10, 10, 10)
              .addComponent(SCROLLPANE_LIST2, GroupLayout.PREFERRED_SIZE, 195, GroupLayout.PREFERRED_SIZE))))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(10, 10, 10)
          .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          .addGap(6, 6, 6)
          .addGroup(layout.createParallelGroup()
            .addComponent(SCROLLPANE_LIST1, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)
            .addComponent(SCROLLPANE_LIST2, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE))
          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 48, Short.MAX_VALUE)
          .addComponent(OBJ_14))
    );
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JButton OBJ_14;
  private JXTitledSeparator xTitledSeparator1;
  private JScrollPane SCROLLPANE_LIST1;
  private XRiTable DAT01;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable DAT09;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
