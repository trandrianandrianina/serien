
package ri.serien.libecranrpg.vcgm.VCGM09FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerListModel;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.ODialog;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.autonome.recherchejournaux.GfxRechercheJournaux;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM09FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  public ODialog dialog_JO = null;
  
  public VCGM09FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    ETLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETLIB@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    FL1.setVisible((lexique.isTrue("N31")));
    FL2.setVisible((lexique.isTrue("N32")));
    FL3.setVisible((lexique.isTrue("N33")));
    FL4.setVisible((lexique.isTrue("N34")));
    FL5.setVisible((lexique.isTrue("N35")));
    FL6.setVisible((lexique.isTrue("N36")));
    FL7.setVisible((lexique.isTrue("N37")));
    FL8.setVisible((lexique.isTrue("N38")));
    FL9.setVisible((lexique.isTrue("N39")));
    FL10.setVisible((lexique.isTrue("N40")));
    FL11.setVisible((lexique.isTrue("N41")));
    FL12.setVisible((lexique.isTrue("N42")));
    FL13.setVisible((lexique.isTrue("N43")));
    FL14.setVisible((lexique.isTrue("N44")));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "DUPLICATE");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void BTN_JOActionPerformed(ActionEvent e) {
    lexique.addVariableGlobale("CODE_SOC", INDETB.getText());
    lexique.addVariableGlobale("ZONE_JO", "ETJO");
    if (dialog_JO == null) {
      dialog_JO = new ODialog((Window) getTopLevelAncestor(), new GfxRechercheJournaux(this));
    }
    dialog_JO.affichePopupPerso();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    INDETB = new XRiTextField();
    OBJ_34 = new JLabel();
    OBJ_69 = new JLabel();
    INDCOD = new XRiTextField();
    ETLIB = new RiZoneSortie();
    ETJO = new XRiTextField();
    BTN_JO = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_193 = new JLabel();
    OBJ_200 = new JLabel();
    NCG1X = new XRiTextField();
    TIE1 = new XRiTextField();
    SNS1 = new JSpinner();
    CLB1 = new XRiTextField();
    LIB1 = new XRiTextField();
    AFF01 = new XRiTextField();
    SAN1 = new XRiTextField();
    NCP1 = new XRiTextField();
    ZON1 = new XRiTextField();
    FL1 = new JLabel();
    NCG2X = new XRiTextField();
    TIE2 = new XRiTextField();
    SNS2 = new JSpinner();
    CLB2 = new XRiTextField();
    LIB2 = new XRiTextField();
    AFF02 = new XRiTextField();
    SAN2 = new XRiTextField();
    NCP2 = new XRiTextField();
    ZON2 = new XRiTextField();
    FL2 = new JLabel();
    NCG3X = new XRiTextField();
    TIE3 = new XRiTextField();
    SNS3 = new JSpinner();
    CLB3 = new XRiTextField();
    LIB3 = new XRiTextField();
    AFF03 = new XRiTextField();
    SAN3 = new XRiTextField();
    NCP3 = new XRiTextField();
    ZON3 = new XRiTextField();
    FL3 = new JLabel();
    NCG4X = new XRiTextField();
    TIE4 = new XRiTextField();
    SNS4 = new JSpinner();
    CLB4 = new XRiTextField();
    LIB4 = new XRiTextField();
    AFF04 = new XRiTextField();
    SAN4 = new XRiTextField();
    NCP4 = new XRiTextField();
    ZON4 = new XRiTextField();
    FL4 = new JLabel();
    NCG5X = new XRiTextField();
    TIE5 = new XRiTextField();
    SNS5 = new JSpinner();
    CLB5 = new XRiTextField();
    LIB5 = new XRiTextField();
    AFF05 = new XRiTextField();
    SAN5 = new XRiTextField();
    NCP5 = new XRiTextField();
    ZON5 = new XRiTextField();
    FL5 = new JLabel();
    NCG6X = new XRiTextField();
    TIE6 = new XRiTextField();
    SNS6 = new JSpinner();
    CLB6 = new XRiTextField();
    LIB6 = new XRiTextField();
    AFF06 = new XRiTextField();
    SAN6 = new XRiTextField();
    NCP6 = new XRiTextField();
    ZON6 = new XRiTextField();
    FL6 = new JLabel();
    NCG7X = new XRiTextField();
    TIE7 = new XRiTextField();
    SNS7 = new JSpinner();
    CLB7 = new XRiTextField();
    LIB7 = new XRiTextField();
    AFF07 = new XRiTextField();
    SAN7 = new XRiTextField();
    NCP7 = new XRiTextField();
    ZON7 = new XRiTextField();
    FL7 = new JLabel();
    NCG8X = new XRiTextField();
    TIE8 = new XRiTextField();
    SNS8 = new JSpinner();
    CLB8 = new XRiTextField();
    LIB8 = new XRiTextField();
    AFF08 = new XRiTextField();
    SAN8 = new XRiTextField();
    NCP8 = new XRiTextField();
    ZON8 = new XRiTextField();
    FL8 = new JLabel();
    NCG9X = new XRiTextField();
    TIE9 = new XRiTextField();
    SNS9 = new JSpinner();
    CLB9 = new XRiTextField();
    LIB9 = new XRiTextField();
    AFF09 = new XRiTextField();
    SAN9 = new XRiTextField();
    NCP9 = new XRiTextField();
    ZON9 = new XRiTextField();
    FL9 = new JLabel();
    NCG10X = new XRiTextField();
    TIE10 = new XRiTextField();
    SNS10 = new JSpinner();
    CLB10 = new XRiTextField();
    LIB10 = new XRiTextField();
    AFF10 = new XRiTextField();
    SAN10 = new XRiTextField();
    NCP10 = new XRiTextField();
    ZON10 = new XRiTextField();
    FL10 = new JLabel();
    NCG11X = new XRiTextField();
    TIE11 = new XRiTextField();
    SNS11 = new JSpinner();
    CLB11 = new XRiTextField();
    LIB11 = new XRiTextField();
    AFF11 = new XRiTextField();
    SAN11 = new XRiTextField();
    NCP11 = new XRiTextField();
    ZON11 = new XRiTextField();
    FL11 = new JLabel();
    NCG12X = new XRiTextField();
    TIE12 = new XRiTextField();
    SNS12 = new JSpinner();
    CLB12 = new XRiTextField();
    LIB12 = new XRiTextField();
    AFF12 = new XRiTextField();
    SAN12 = new XRiTextField();
    NCP12 = new XRiTextField();
    ZON12 = new XRiTextField();
    FL12 = new JLabel();
    NCG13X = new XRiTextField();
    TIE13 = new XRiTextField();
    SNS13 = new JSpinner();
    CLB13 = new XRiTextField();
    LIB13 = new XRiTextField();
    AFF13 = new XRiTextField();
    SAN13 = new XRiTextField();
    NCP13 = new XRiTextField();
    ZON13 = new XRiTextField();
    FL13 = new JLabel();
    NCG14X = new XRiTextField();
    TIE14 = new XRiTextField();
    SNS14 = new JSpinner();
    CLB14 = new XRiTextField();
    LIB14 = new XRiTextField();
    AFF14 = new XRiTextField();
    SAN14 = new XRiTextField();
    NCP14 = new XRiTextField();
    ZON14 = new XRiTextField();
    FL14 = new JLabel();
    OBJ_197 = new JLabel();
    OBJ_201 = new JLabel();
    OBJ_194 = new JLabel();
    OBJ_199 = new JLabel();
    OBJ_198 = new JLabel();
    OBJ_195 = new JLabel();
    OBJ_196 = new JLabel();
    TIE15 = new XRiTextField();
    TIE16 = new XRiTextField();
    TIE17 = new XRiTextField();
    TIE18 = new XRiTextField();
    TIE19 = new XRiTextField();
    TIE20 = new XRiTextField();
    TIE21 = new XRiTextField();
    xTitledPanel2 = new JXTitledPanel();
    ETO1 = new XRiTextField();
    ETO2 = new XRiTextField();
    ETO3 = new XRiTextField();
    ETO4 = new XRiTextField();
    ETO5 = new XRiTextField();
    ETO6 = new XRiTextField();
    ETO7 = new XRiTextField();
    ETO8 = new XRiTextField();
    ETO9 = new XRiTextField();
    ETO10 = new XRiTextField();
    ETO11 = new XRiTextField();
    ETO12 = new XRiTextField();
    ETO13 = new XRiTextField();
    ETO14 = new XRiTextField();
    ETO15 = new XRiTextField();
    ETO16 = new XRiTextField();
    ETO17 = new XRiTextField();
    ETO18 = new XRiTextField();
    OBJ_218 = new JLabel();
    OBJ_219 = new JLabel();
    OBJ_204 = new JLabel();
    OBJ_208 = new JLabel();
    OBJ_234 = new JLabel();
    OBJ_235 = new JLabel();
    OBJ_237 = new JLabel();
    OBJ_236 = new JLabel();
    OBJ_223 = new JLabel();
    OBJ_222 = new JLabel();
    OBJ_205 = new JLabel();
    OBJ_209 = new JLabel();
    OBJ_233 = new JLabel();
    OBJ_232 = new JLabel();
    OBJ_238 = new JLabel();
    OBJ_239 = new JLabel();
    OBJ_206 = new JLabel();
    OBJ_210 = new JLabel();
    OBJ_225 = new JLabel();
    OBJ_224 = new JLabel();
    OBJ_230 = new JLabel();
    OBJ_231 = new JLabel();
    OBJ_240 = new JLabel();
    OBJ_241 = new JLabel();
    OBJ_226 = new JLabel();
    OBJ_227 = new JLabel();
    OBJ_211 = new JLabel();
    OBJ_207 = new JLabel();
    OBJ_229 = new JLabel();
    OBJ_228 = new JLabel();
    OBJ_220 = new JLabel();
    OBJ_221 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_16 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Ecritures types");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_34 ----
          OBJ_34.setText("Soci\u00e9t\u00e9");
          OBJ_34.setName("OBJ_34");

          //---- OBJ_69 ----
          OBJ_69.setText("Code de pi\u00e8ce type");
          OBJ_69.setName("OBJ_69");

          //---- INDCOD ----
          INDCOD.setName("INDCOD");

          //---- ETLIB ----
          ETLIB.setComponentPopupMenu(BTD);
          ETLIB.setOpaque(false);
          ETLIB.setText("@ETLIB@");
          ETLIB.setName("ETLIB");

          //---- ETJO ----
          ETJO.setComponentPopupMenu(BTD);
          ETJO.setName("ETJO");

          //---- BTN_JO ----
          BTN_JO.setText("Journal");
          BTN_JO.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BTN_JO.setToolTipText("Recherche d'un journal");
          BTN_JO.setName("BTN_JO");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(OBJ_69, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(INDCOD, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(ETLIB, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(BTN_JO, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(ETJO, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_34))
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_69))
              .addComponent(INDCOD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(ETLIB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(ETJO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(BTN_JO))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

            //---- OBJ_193 ----
            OBJ_193.setText("Compte");
            OBJ_193.setName("OBJ_193");

            //---- OBJ_200 ----
            OBJ_200.setText("Montant");
            OBJ_200.setName("OBJ_200");

            //---- NCG1X ----
            NCG1X.setComponentPopupMenu(BTD);
            NCG1X.setName("NCG1X");

            //---- TIE1 ----
            TIE1.setComponentPopupMenu(BTD);
            TIE1.setName("TIE1");

            //---- SNS1 ----
            SNS1.setComponentPopupMenu(BTD);
            SNS1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNS1.setModel(new SpinnerListModel(new String[] {"D", "C"}));
            SNS1.setName("SNS1");

            //---- CLB1 ----
            CLB1.setComponentPopupMenu(BTD);
            CLB1.setName("CLB1");

            //---- LIB1 ----
            LIB1.setComponentPopupMenu(BTD);
            LIB1.setName("LIB1");

            //---- AFF01 ----
            AFF01.setComponentPopupMenu(BTD);
            AFF01.setName("AFF01");

            //---- SAN1 ----
            SAN1.setComponentPopupMenu(BTD);
            SAN1.setName("SAN1");

            //---- NCP1 ----
            NCP1.setComponentPopupMenu(BTD);
            NCP1.setName("NCP1");

            //---- ZON1 ----
            ZON1.setComponentPopupMenu(BTD);
            ZON1.setName("ZON1");

            //---- FL1 ----
            FL1.setText("<--");
            FL1.setFont(new Font("Courier New", Font.BOLD, 17));
            FL1.setForeground(Color.red);
            FL1.setName("FL1");

            //---- NCG2X ----
            NCG2X.setComponentPopupMenu(BTD);
            NCG2X.setName("NCG2X");

            //---- TIE2 ----
            TIE2.setComponentPopupMenu(BTD);
            TIE2.setName("TIE2");

            //---- SNS2 ----
            SNS2.setComponentPopupMenu(BTD);
            SNS2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNS2.setModel(new SpinnerListModel(new String[] {"D", "C"}));
            SNS2.setName("SNS2");

            //---- CLB2 ----
            CLB2.setComponentPopupMenu(BTD);
            CLB2.setName("CLB2");

            //---- LIB2 ----
            LIB2.setComponentPopupMenu(BTD);
            LIB2.setName("LIB2");

            //---- AFF02 ----
            AFF02.setComponentPopupMenu(BTD);
            AFF02.setName("AFF02");

            //---- SAN2 ----
            SAN2.setComponentPopupMenu(BTD);
            SAN2.setName("SAN2");

            //---- NCP2 ----
            NCP2.setComponentPopupMenu(BTD);
            NCP2.setName("NCP2");

            //---- ZON2 ----
            ZON2.setComponentPopupMenu(BTD);
            ZON2.setName("ZON2");

            //---- FL2 ----
            FL2.setText("<--");
            FL2.setFont(new Font("Courier New", Font.BOLD, 17));
            FL2.setForeground(Color.red);
            FL2.setName("FL2");

            //---- NCG3X ----
            NCG3X.setComponentPopupMenu(BTD);
            NCG3X.setName("NCG3X");

            //---- TIE3 ----
            TIE3.setComponentPopupMenu(BTD);
            TIE3.setName("TIE3");

            //---- SNS3 ----
            SNS3.setComponentPopupMenu(BTD);
            SNS3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNS3.setModel(new SpinnerListModel(new String[] {"D", "C"}));
            SNS3.setName("SNS3");

            //---- CLB3 ----
            CLB3.setComponentPopupMenu(BTD);
            CLB3.setName("CLB3");

            //---- LIB3 ----
            LIB3.setComponentPopupMenu(BTD);
            LIB3.setName("LIB3");

            //---- AFF03 ----
            AFF03.setComponentPopupMenu(BTD);
            AFF03.setName("AFF03");

            //---- SAN3 ----
            SAN3.setComponentPopupMenu(BTD);
            SAN3.setName("SAN3");

            //---- NCP3 ----
            NCP3.setComponentPopupMenu(BTD);
            NCP3.setName("NCP3");

            //---- ZON3 ----
            ZON3.setComponentPopupMenu(BTD);
            ZON3.setName("ZON3");

            //---- FL3 ----
            FL3.setText("<--");
            FL3.setFont(new Font("Courier New", Font.BOLD, 17));
            FL3.setForeground(Color.red);
            FL3.setName("FL3");

            //---- NCG4X ----
            NCG4X.setComponentPopupMenu(BTD);
            NCG4X.setName("NCG4X");

            //---- TIE4 ----
            TIE4.setComponentPopupMenu(BTD);
            TIE4.setName("TIE4");

            //---- SNS4 ----
            SNS4.setComponentPopupMenu(BTD);
            SNS4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNS4.setModel(new SpinnerListModel(new String[] {"D", "C"}));
            SNS4.setName("SNS4");

            //---- CLB4 ----
            CLB4.setComponentPopupMenu(BTD);
            CLB4.setName("CLB4");

            //---- LIB4 ----
            LIB4.setComponentPopupMenu(BTD);
            LIB4.setName("LIB4");

            //---- AFF04 ----
            AFF04.setComponentPopupMenu(BTD);
            AFF04.setName("AFF04");

            //---- SAN4 ----
            SAN4.setComponentPopupMenu(BTD);
            SAN4.setName("SAN4");

            //---- NCP4 ----
            NCP4.setComponentPopupMenu(BTD);
            NCP4.setName("NCP4");

            //---- ZON4 ----
            ZON4.setComponentPopupMenu(BTD);
            ZON4.setName("ZON4");

            //---- FL4 ----
            FL4.setText("<--");
            FL4.setFont(new Font("Courier New", Font.BOLD, 17));
            FL4.setForeground(Color.red);
            FL4.setName("FL4");

            //---- NCG5X ----
            NCG5X.setComponentPopupMenu(BTD);
            NCG5X.setName("NCG5X");

            //---- TIE5 ----
            TIE5.setComponentPopupMenu(BTD);
            TIE5.setName("TIE5");

            //---- SNS5 ----
            SNS5.setComponentPopupMenu(BTD);
            SNS5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNS5.setModel(new SpinnerListModel(new String[] {"D", "C"}));
            SNS5.setName("SNS5");

            //---- CLB5 ----
            CLB5.setComponentPopupMenu(BTD);
            CLB5.setName("CLB5");

            //---- LIB5 ----
            LIB5.setComponentPopupMenu(BTD);
            LIB5.setName("LIB5");

            //---- AFF05 ----
            AFF05.setComponentPopupMenu(BTD);
            AFF05.setName("AFF05");

            //---- SAN5 ----
            SAN5.setComponentPopupMenu(BTD);
            SAN5.setName("SAN5");

            //---- NCP5 ----
            NCP5.setComponentPopupMenu(BTD);
            NCP5.setName("NCP5");

            //---- ZON5 ----
            ZON5.setComponentPopupMenu(BTD);
            ZON5.setName("ZON5");

            //---- FL5 ----
            FL5.setText("<--");
            FL5.setFont(new Font("Courier New", Font.BOLD, 17));
            FL5.setForeground(Color.red);
            FL5.setName("FL5");

            //---- NCG6X ----
            NCG6X.setComponentPopupMenu(BTD);
            NCG6X.setName("NCG6X");

            //---- TIE6 ----
            TIE6.setComponentPopupMenu(BTD);
            TIE6.setName("TIE6");

            //---- SNS6 ----
            SNS6.setComponentPopupMenu(BTD);
            SNS6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNS6.setModel(new SpinnerListModel(new String[] {"D", "C"}));
            SNS6.setName("SNS6");

            //---- CLB6 ----
            CLB6.setComponentPopupMenu(BTD);
            CLB6.setName("CLB6");

            //---- LIB6 ----
            LIB6.setComponentPopupMenu(BTD);
            LIB6.setName("LIB6");

            //---- AFF06 ----
            AFF06.setComponentPopupMenu(BTD);
            AFF06.setName("AFF06");

            //---- SAN6 ----
            SAN6.setComponentPopupMenu(BTD);
            SAN6.setName("SAN6");

            //---- NCP6 ----
            NCP6.setComponentPopupMenu(BTD);
            NCP6.setName("NCP6");

            //---- ZON6 ----
            ZON6.setComponentPopupMenu(BTD);
            ZON6.setName("ZON6");

            //---- FL6 ----
            FL6.setText("<--");
            FL6.setFont(new Font("Courier New", Font.BOLD, 17));
            FL6.setForeground(Color.red);
            FL6.setName("FL6");

            //---- NCG7X ----
            NCG7X.setComponentPopupMenu(BTD);
            NCG7X.setName("NCG7X");

            //---- TIE7 ----
            TIE7.setComponentPopupMenu(BTD);
            TIE7.setName("TIE7");

            //---- SNS7 ----
            SNS7.setComponentPopupMenu(BTD);
            SNS7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNS7.setModel(new SpinnerListModel(new String[] {"D", "C"}));
            SNS7.setName("SNS7");

            //---- CLB7 ----
            CLB7.setComponentPopupMenu(BTD);
            CLB7.setName("CLB7");

            //---- LIB7 ----
            LIB7.setComponentPopupMenu(BTD);
            LIB7.setName("LIB7");

            //---- AFF07 ----
            AFF07.setComponentPopupMenu(BTD);
            AFF07.setName("AFF07");

            //---- SAN7 ----
            SAN7.setComponentPopupMenu(BTD);
            SAN7.setName("SAN7");

            //---- NCP7 ----
            NCP7.setComponentPopupMenu(BTD);
            NCP7.setName("NCP7");

            //---- ZON7 ----
            ZON7.setComponentPopupMenu(BTD);
            ZON7.setName("ZON7");

            //---- FL7 ----
            FL7.setText("<--");
            FL7.setFont(new Font("Courier New", Font.BOLD, 17));
            FL7.setForeground(Color.red);
            FL7.setName("FL7");

            //---- NCG8X ----
            NCG8X.setComponentPopupMenu(BTD);
            NCG8X.setName("NCG8X");

            //---- TIE8 ----
            TIE8.setComponentPopupMenu(BTD);
            TIE8.setName("TIE8");

            //---- SNS8 ----
            SNS8.setComponentPopupMenu(BTD);
            SNS8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNS8.setModel(new SpinnerListModel(new String[] {"D", "C"}));
            SNS8.setName("SNS8");

            //---- CLB8 ----
            CLB8.setComponentPopupMenu(BTD);
            CLB8.setName("CLB8");

            //---- LIB8 ----
            LIB8.setComponentPopupMenu(BTD);
            LIB8.setName("LIB8");

            //---- AFF08 ----
            AFF08.setComponentPopupMenu(BTD);
            AFF08.setName("AFF08");

            //---- SAN8 ----
            SAN8.setComponentPopupMenu(BTD);
            SAN8.setName("SAN8");

            //---- NCP8 ----
            NCP8.setComponentPopupMenu(BTD);
            NCP8.setName("NCP8");

            //---- ZON8 ----
            ZON8.setComponentPopupMenu(BTD);
            ZON8.setName("ZON8");

            //---- FL8 ----
            FL8.setText("<--");
            FL8.setFont(new Font("Courier New", Font.BOLD, 17));
            FL8.setForeground(Color.red);
            FL8.setName("FL8");

            //---- NCG9X ----
            NCG9X.setComponentPopupMenu(BTD);
            NCG9X.setName("NCG9X");

            //---- TIE9 ----
            TIE9.setComponentPopupMenu(BTD);
            TIE9.setName("TIE9");

            //---- SNS9 ----
            SNS9.setComponentPopupMenu(BTD);
            SNS9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNS9.setModel(new SpinnerListModel(new String[] {"D", "C"}));
            SNS9.setName("SNS9");

            //---- CLB9 ----
            CLB9.setComponentPopupMenu(BTD);
            CLB9.setName("CLB9");

            //---- LIB9 ----
            LIB9.setComponentPopupMenu(BTD);
            LIB9.setName("LIB9");

            //---- AFF09 ----
            AFF09.setComponentPopupMenu(BTD);
            AFF09.setName("AFF09");

            //---- SAN9 ----
            SAN9.setComponentPopupMenu(BTD);
            SAN9.setName("SAN9");

            //---- NCP9 ----
            NCP9.setComponentPopupMenu(BTD);
            NCP9.setName("NCP9");

            //---- ZON9 ----
            ZON9.setComponentPopupMenu(BTD);
            ZON9.setName("ZON9");

            //---- FL9 ----
            FL9.setText("<--");
            FL9.setFont(new Font("Courier New", Font.BOLD, 17));
            FL9.setForeground(Color.red);
            FL9.setName("FL9");

            //---- NCG10X ----
            NCG10X.setComponentPopupMenu(BTD);
            NCG10X.setName("NCG10X");

            //---- TIE10 ----
            TIE10.setComponentPopupMenu(BTD);
            TIE10.setName("TIE10");

            //---- SNS10 ----
            SNS10.setComponentPopupMenu(BTD);
            SNS10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNS10.setModel(new SpinnerListModel(new String[] {"D", "C"}));
            SNS10.setName("SNS10");

            //---- CLB10 ----
            CLB10.setComponentPopupMenu(BTD);
            CLB10.setName("CLB10");

            //---- LIB10 ----
            LIB10.setComponentPopupMenu(BTD);
            LIB10.setName("LIB10");

            //---- AFF10 ----
            AFF10.setComponentPopupMenu(BTD);
            AFF10.setName("AFF10");

            //---- SAN10 ----
            SAN10.setComponentPopupMenu(BTD);
            SAN10.setName("SAN10");

            //---- NCP10 ----
            NCP10.setComponentPopupMenu(BTD);
            NCP10.setName("NCP10");

            //---- ZON10 ----
            ZON10.setComponentPopupMenu(BTD);
            ZON10.setName("ZON10");

            //---- FL10 ----
            FL10.setText("<--");
            FL10.setFont(new Font("Courier New", Font.BOLD, 17));
            FL10.setForeground(Color.red);
            FL10.setName("FL10");

            //---- NCG11X ----
            NCG11X.setComponentPopupMenu(BTD);
            NCG11X.setName("NCG11X");

            //---- TIE11 ----
            TIE11.setComponentPopupMenu(BTD);
            TIE11.setName("TIE11");

            //---- SNS11 ----
            SNS11.setComponentPopupMenu(BTD);
            SNS11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNS11.setModel(new SpinnerListModel(new String[] {"D", "C"}));
            SNS11.setName("SNS11");

            //---- CLB11 ----
            CLB11.setComponentPopupMenu(BTD);
            CLB11.setName("CLB11");

            //---- LIB11 ----
            LIB11.setComponentPopupMenu(BTD);
            LIB11.setName("LIB11");

            //---- AFF11 ----
            AFF11.setComponentPopupMenu(BTD);
            AFF11.setName("AFF11");

            //---- SAN11 ----
            SAN11.setComponentPopupMenu(BTD);
            SAN11.setName("SAN11");

            //---- NCP11 ----
            NCP11.setComponentPopupMenu(BTD);
            NCP11.setName("NCP11");

            //---- ZON11 ----
            ZON11.setComponentPopupMenu(BTD);
            ZON11.setName("ZON11");

            //---- FL11 ----
            FL11.setText("<--");
            FL11.setFont(new Font("Courier New", Font.BOLD, 17));
            FL11.setForeground(Color.red);
            FL11.setName("FL11");

            //---- NCG12X ----
            NCG12X.setComponentPopupMenu(BTD);
            NCG12X.setName("NCG12X");

            //---- TIE12 ----
            TIE12.setComponentPopupMenu(BTD);
            TIE12.setName("TIE12");

            //---- SNS12 ----
            SNS12.setComponentPopupMenu(BTD);
            SNS12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNS12.setModel(new SpinnerListModel(new String[] {"D", "C"}));
            SNS12.setName("SNS12");

            //---- CLB12 ----
            CLB12.setComponentPopupMenu(BTD);
            CLB12.setName("CLB12");

            //---- LIB12 ----
            LIB12.setComponentPopupMenu(BTD);
            LIB12.setName("LIB12");

            //---- AFF12 ----
            AFF12.setComponentPopupMenu(BTD);
            AFF12.setName("AFF12");

            //---- SAN12 ----
            SAN12.setComponentPopupMenu(BTD);
            SAN12.setName("SAN12");

            //---- NCP12 ----
            NCP12.setComponentPopupMenu(BTD);
            NCP12.setName("NCP12");

            //---- ZON12 ----
            ZON12.setComponentPopupMenu(BTD);
            ZON12.setName("ZON12");

            //---- FL12 ----
            FL12.setText("<--");
            FL12.setFont(new Font("Courier New", Font.BOLD, 17));
            FL12.setForeground(Color.red);
            FL12.setName("FL12");

            //---- NCG13X ----
            NCG13X.setComponentPopupMenu(BTD);
            NCG13X.setName("NCG13X");

            //---- TIE13 ----
            TIE13.setComponentPopupMenu(BTD);
            TIE13.setName("TIE13");

            //---- SNS13 ----
            SNS13.setComponentPopupMenu(BTD);
            SNS13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNS13.setModel(new SpinnerListModel(new String[] {"D", "C"}));
            SNS13.setName("SNS13");

            //---- CLB13 ----
            CLB13.setComponentPopupMenu(BTD);
            CLB13.setName("CLB13");

            //---- LIB13 ----
            LIB13.setComponentPopupMenu(BTD);
            LIB13.setName("LIB13");

            //---- AFF13 ----
            AFF13.setComponentPopupMenu(BTD);
            AFF13.setName("AFF13");

            //---- SAN13 ----
            SAN13.setComponentPopupMenu(BTD);
            SAN13.setName("SAN13");

            //---- NCP13 ----
            NCP13.setComponentPopupMenu(BTD);
            NCP13.setName("NCP13");

            //---- ZON13 ----
            ZON13.setComponentPopupMenu(BTD);
            ZON13.setName("ZON13");

            //---- FL13 ----
            FL13.setText("<--");
            FL13.setFont(new Font("Courier New", Font.BOLD, 17));
            FL13.setForeground(Color.red);
            FL13.setName("FL13");

            //---- NCG14X ----
            NCG14X.setComponentPopupMenu(BTD);
            NCG14X.setName("NCG14X");

            //---- TIE14 ----
            TIE14.setComponentPopupMenu(BTD);
            TIE14.setName("TIE14");

            //---- SNS14 ----
            SNS14.setComponentPopupMenu(BTD);
            SNS14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNS14.setModel(new SpinnerListModel(new String[] {"D", "C"}));
            SNS14.setName("SNS14");

            //---- CLB14 ----
            CLB14.setComponentPopupMenu(BTD);
            CLB14.setName("CLB14");

            //---- LIB14 ----
            LIB14.setComponentPopupMenu(BTD);
            LIB14.setName("LIB14");

            //---- AFF14 ----
            AFF14.setComponentPopupMenu(BTD);
            AFF14.setName("AFF14");

            //---- SAN14 ----
            SAN14.setComponentPopupMenu(BTD);
            SAN14.setName("SAN14");

            //---- NCP14 ----
            NCP14.setComponentPopupMenu(BTD);
            NCP14.setName("NCP14");

            //---- ZON14 ----
            ZON14.setComponentPopupMenu(BTD);
            ZON14.setName("ZON14");

            //---- FL14 ----
            FL14.setText("<--");
            FL14.setFont(new Font("Courier New", Font.BOLD, 17));
            FL14.setForeground(Color.red);
            FL14.setName("FL14");

            //---- OBJ_197 ----
            OBJ_197.setText("Libell\u00e9");
            OBJ_197.setName("OBJ_197");

            //---- OBJ_201 ----
            OBJ_201.setText("OZnn");
            OBJ_201.setName("OBJ_201");

            //---- OBJ_194 ----
            OBJ_194.setText("Tiers");
            OBJ_194.setName("OBJ_194");

            //---- OBJ_199 ----
            OBJ_199.setText("Sect");
            OBJ_199.setName("OBJ_199");

            //---- OBJ_198 ----
            OBJ_198.setText("Aff");
            OBJ_198.setName("OBJ_198");

            //---- OBJ_195 ----
            OBJ_195.setText("S");
            OBJ_195.setName("OBJ_195");

            //---- OBJ_196 ----
            OBJ_196.setText("C");
            OBJ_196.setName("OBJ_196");

            //---- TIE15 ----
            TIE15.setComponentPopupMenu(BTD);
            TIE15.setName("TIE15");

            //---- TIE16 ----
            TIE16.setComponentPopupMenu(BTD);
            TIE16.setName("TIE16");

            //---- TIE17 ----
            TIE17.setComponentPopupMenu(BTD);
            TIE17.setName("TIE17");

            //---- TIE18 ----
            TIE18.setComponentPopupMenu(BTD);
            TIE18.setName("TIE18");

            //---- TIE19 ----
            TIE19.setComponentPopupMenu(BTD);
            TIE19.setName("TIE19");

            //---- TIE20 ----
            TIE20.setComponentPopupMenu(BTD);
            TIE20.setName("TIE20");

            //---- TIE21 ----
            TIE21.setComponentPopupMenu(BTD);
            TIE21.setName("TIE21");

            GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
            xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
            xTitledPanel1ContentContainerLayout.setHorizontalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(8, 8, 8)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(NCG8X, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NCG2X, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_193, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NCG6X, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NCG7X, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NCG1X, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NCG4X, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NCG3X, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NCG5X, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NCG11X, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NCG13X, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NCG10X, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NCG12X, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NCG14X, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NCG9X, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(TIE1, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIE2, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_194, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIE4, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIE18, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIE16, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIE6, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIE21, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIE12, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIE11, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIE10, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIE9, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIE20, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIE17, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIE8, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIE14, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIE5, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIE19, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIE15, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIE7, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIE13, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIE3, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addGap(10, 10, 10)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(SNS3, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SNS1, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SNS2, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_195, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SNS14, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SNS12, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SNS5, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SNS7, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SNS8, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SNS10, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SNS9, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SNS13, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SNS6, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SNS11, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SNS4, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_196, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CLB3, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CLB2, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CLB4, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CLB1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CLB14, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CLB8, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CLB9, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CLB12, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CLB7, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CLB13, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CLB11, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CLB6, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CLB10, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CLB5, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(LIB7, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LIB1, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LIB6, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LIB2, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LIB3, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LIB5, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_197, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LIB4, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LIB12, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LIB9, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LIB14, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LIB10, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LIB13, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LIB11, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LIB8, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE))
                  .addGap(7, 7, 7)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(AFF04, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_198, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addComponent(AFF03, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(AFF05, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(AFF01, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(AFF02, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(AFF11, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(AFF10, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(AFF09, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(AFF13, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(AFF12, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(AFF14, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(AFF08, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(AFF07, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(AFF06, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addGap(8, 8, 8)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_199, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SAN2, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SAN1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SAN4, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SAN12, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SAN8, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SAN9, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SAN14, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SAN13, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SAN7, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SAN5, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SAN11, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SAN6, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SAN10, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SAN3, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                  .addGap(7, 7, 7)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(NCP6, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_200, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NCP5, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NCP3, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NCP7, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NCP2, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NCP8, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NCP4, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NCP1, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NCP12, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NCP13, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NCP14, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NCP10, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NCP11, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NCP9, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addGap(10, 10, 10)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(ZON7, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ZON4, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ZON5, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ZON8, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ZON6, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ZON3, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ZON1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_201, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ZON2, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ZON11, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ZON13, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ZON14, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ZON10, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ZON12, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ZON9, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                  .addGap(10, 10, 10)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(FL1, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(FL2, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(FL7, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(FL3, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(FL9, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(FL8, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(FL4, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(FL5, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(FL10, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(FL6, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(FL12, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(FL13, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(FL14, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(FL11, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)))
            );
            xTitledPanel1ContentContainerLayout.setVerticalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(190, 190, 190)
                      .addComponent(NCG8X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(40, 40, 40)
                      .addComponent(NCG2X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(OBJ_193)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(140, 140, 140)
                      .addComponent(NCG6X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(165, 165, 165)
                      .addComponent(NCG7X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(15, 15, 15)
                      .addComponent(NCG1X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(90, 90, 90)
                      .addComponent(NCG4X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(65, 65, 65)
                      .addComponent(NCG3X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(115, 115, 115)
                      .addComponent(NCG5X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(22, 22, 22)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(NCG11X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(75, 75, 75)
                      .addComponent(NCG13X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(NCG10X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(NCG12X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(100, 100, 100)
                      .addComponent(NCG14X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(215, 215, 215)
                  .addComponent(NCG9X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(15, 15, 15)
                      .addComponent(TIE1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(40, 40, 40)
                      .addComponent(TIE2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(OBJ_194))
                  .addGap(22, 22, 22)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(TIE4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(170, 170, 170)
                      .addComponent(TIE18, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(220, 220, 220)
                      .addComponent(TIE16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(TIE6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(95, 95, 95)
                      .addComponent(TIE21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(200, 200, 200)
                      .addComponent(TIE12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(175, 175, 175)
                      .addComponent(TIE11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(150, 150, 150)
                      .addComponent(TIE10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(125, 125, 125)
                      .addComponent(TIE9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(120, 120, 120)
                      .addComponent(TIE20, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(195, 195, 195)
                      .addComponent(TIE17, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(100, 100, 100)
                      .addComponent(TIE8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(250, 250, 250)
                      .addComponent(TIE14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(TIE5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(145, 145, 145)
                      .addComponent(TIE19, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(245, 245, 245)
                      .addComponent(TIE15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(75, 75, 75)
                      .addComponent(TIE7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(225, 225, 225)
                      .addComponent(TIE13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(65, 65, 65)
                  .addComponent(TIE3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(65, 65, 65)
                      .addComponent(SNS3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(15, 15, 15)
                      .addComponent(SNS1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(40, 40, 40)
                      .addComponent(SNS2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(OBJ_195))
                  .addGap(22, 22, 22)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(225, 225, 225)
                      .addComponent(SNS14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(175, 175, 175)
                      .addComponent(SNS12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(SNS5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(SNS7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(75, 75, 75)
                      .addComponent(SNS8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(125, 125, 125)
                      .addComponent(SNS10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(100, 100, 100)
                      .addComponent(SNS9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(200, 200, 200)
                      .addComponent(SNS13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(SNS6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(150, 150, 150)
                      .addComponent(SNS11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(90, 90, 90)
                  .addComponent(SNS4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_196)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(65, 65, 65)
                      .addComponent(CLB3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(40, 40, 40)
                      .addComponent(CLB2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(90, 90, 90)
                      .addComponent(CLB4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(15, 15, 15)
                      .addComponent(CLB1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(22, 22, 22)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(200, 200, 200)
                      .addComponent(CLB14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(CLB8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(75, 75, 75)
                      .addComponent(CLB9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(150, 150, 150)
                      .addComponent(CLB12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(CLB7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(175, 175, 175)
                      .addComponent(CLB13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(125, 125, 125)
                      .addComponent(CLB11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(CLB6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(100, 100, 100)
                      .addComponent(CLB10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(115, 115, 115)
                  .addComponent(CLB5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(165, 165, 165)
                      .addComponent(LIB7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(15, 15, 15)
                      .addComponent(LIB1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(140, 140, 140)
                      .addComponent(LIB6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(40, 40, 40)
                      .addComponent(LIB2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(65, 65, 65)
                      .addComponent(LIB3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(115, 115, 115)
                      .addComponent(LIB5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(OBJ_197)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(90, 90, 90)
                      .addComponent(LIB4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(22, 22, 22)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(75, 75, 75)
                      .addComponent(LIB12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(LIB9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(125, 125, 125)
                      .addComponent(LIB14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(LIB10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(100, 100, 100)
                      .addComponent(LIB13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(LIB11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(190, 190, 190)
                  .addComponent(LIB8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(90, 90, 90)
                      .addComponent(AFF04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(OBJ_198)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(65, 65, 65)
                      .addComponent(AFF03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(115, 115, 115)
                      .addComponent(AFF05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(15, 15, 15)
                      .addComponent(AFF01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(40, 40, 40)
                      .addComponent(AFF02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(22, 22, 22)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(100, 100, 100)
                      .addComponent(AFF11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(75, 75, 75)
                      .addComponent(AFF10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(AFF09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(150, 150, 150)
                      .addComponent(AFF13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(125, 125, 125)
                      .addComponent(AFF12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(175, 175, 175)
                      .addComponent(AFF14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(AFF08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(AFF07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(140, 140, 140)
                  .addComponent(AFF06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_199)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(40, 40, 40)
                      .addComponent(SAN2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(15, 15, 15)
                      .addComponent(SAN1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(22, 22, 22)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(SAN4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(200, 200, 200)
                      .addComponent(SAN12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(100, 100, 100)
                      .addComponent(SAN8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(125, 125, 125)
                      .addComponent(SAN9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(250, 250, 250)
                      .addComponent(SAN14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(225, 225, 225)
                      .addComponent(SAN13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(75, 75, 75)
                      .addComponent(SAN7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(SAN5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(175, 175, 175)
                      .addComponent(SAN11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(SAN6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(150, 150, 150)
                      .addComponent(SAN10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(65, 65, 65)
                  .addComponent(SAN3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(140, 140, 140)
                      .addComponent(NCP6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(OBJ_200)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(115, 115, 115)
                      .addComponent(NCP5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(65, 65, 65)
                      .addComponent(NCP3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(165, 165, 165)
                      .addComponent(NCP7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(40, 40, 40)
                      .addComponent(NCP2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(190, 190, 190)
                      .addComponent(NCP8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(90, 90, 90)
                      .addComponent(NCP4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(15, 15, 15)
                      .addComponent(NCP1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(22, 22, 22)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(NCP12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(75, 75, 75)
                      .addComponent(NCP13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(100, 100, 100)
                      .addComponent(NCP14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(NCP10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(NCP11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(215, 215, 215)
                  .addComponent(NCP9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(165, 165, 165)
                      .addComponent(ZON7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(90, 90, 90)
                      .addComponent(ZON4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(115, 115, 115)
                      .addComponent(ZON5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(190, 190, 190)
                      .addComponent(ZON8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(140, 140, 140)
                      .addComponent(ZON6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(65, 65, 65)
                      .addComponent(ZON3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(15, 15, 15)
                      .addComponent(ZON1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(OBJ_201)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(40, 40, 40)
                      .addComponent(ZON2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(22, 22, 22)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(ZON11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(75, 75, 75)
                      .addComponent(ZON13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(100, 100, 100)
                      .addComponent(ZON14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(ZON10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(ZON12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(215, 215, 215)
                  .addComponent(ZON9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(FL1, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(FL2, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(150, 150, 150)
                          .addComponent(FL7, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(FL3, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(200, 200, 200)
                          .addComponent(FL9, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(175, 175, 175)
                          .addComponent(FL8, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(FL4, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(FL5, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(225, 225, 225)
                          .addComponent(FL10, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(FL6, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)))
                      .addGap(24, 24, 24)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(FL12, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(FL13, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(FL14, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(250, 250, 250)
                      .addComponent(FL11, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))))
            );
          }

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();

            //---- ETO1 ----
            ETO1.setName("ETO1");

            //---- ETO2 ----
            ETO2.setName("ETO2");

            //---- ETO3 ----
            ETO3.setName("ETO3");

            //---- ETO4 ----
            ETO4.setName("ETO4");

            //---- ETO5 ----
            ETO5.setName("ETO5");

            //---- ETO6 ----
            ETO6.setName("ETO6");

            //---- ETO7 ----
            ETO7.setName("ETO7");

            //---- ETO8 ----
            ETO8.setName("ETO8");

            //---- ETO9 ----
            ETO9.setName("ETO9");

            //---- ETO10 ----
            ETO10.setName("ETO10");

            //---- ETO11 ----
            ETO11.setName("ETO11");

            //---- ETO12 ----
            ETO12.setName("ETO12");

            //---- ETO13 ----
            ETO13.setName("ETO13");

            //---- ETO14 ----
            ETO14.setName("ETO14");

            //---- ETO15 ----
            ETO15.setName("ETO15");

            //---- ETO16 ----
            ETO16.setName("ETO16");

            //---- ETO17 ----
            ETO17.setName("ETO17");

            //---- ETO18 ----
            ETO18.setName("ETO18");

            //---- OBJ_218 ----
            OBJ_218.setText("=");
            OBJ_218.setName("OBJ_218");

            //---- OBJ_219 ----
            OBJ_219.setText("=");
            OBJ_219.setName("OBJ_219");

            //---- OBJ_204 ----
            OBJ_204.setText("X");
            OBJ_204.setName("OBJ_204");

            //---- OBJ_208 ----
            OBJ_208.setText("X");
            OBJ_208.setName("OBJ_208");

            //---- OBJ_234 ----
            OBJ_234.setText(")");
            OBJ_234.setName("OBJ_234");

            //---- OBJ_235 ----
            OBJ_235.setText(")");
            OBJ_235.setName("OBJ_235");

            //---- OBJ_237 ----
            OBJ_237.setText("+");
            OBJ_237.setName("OBJ_237");

            //---- OBJ_236 ----
            OBJ_236.setText("+");
            OBJ_236.setName("OBJ_236");

            //---- OBJ_223 ----
            OBJ_223.setText("(");
            OBJ_223.setName("OBJ_223");

            //---- OBJ_222 ----
            OBJ_222.setText("(");
            OBJ_222.setName("OBJ_222");

            //---- OBJ_205 ----
            OBJ_205.setText("X");
            OBJ_205.setName("OBJ_205");

            //---- OBJ_209 ----
            OBJ_209.setText("X");
            OBJ_209.setName("OBJ_209");

            //---- OBJ_233 ----
            OBJ_233.setText(")");
            OBJ_233.setName("OBJ_233");

            //---- OBJ_232 ----
            OBJ_232.setText(")");
            OBJ_232.setName("OBJ_232");

            //---- OBJ_238 ----
            OBJ_238.setText("+");
            OBJ_238.setName("OBJ_238");

            //---- OBJ_239 ----
            OBJ_239.setText("+");
            OBJ_239.setName("OBJ_239");

            //---- OBJ_206 ----
            OBJ_206.setText("X");
            OBJ_206.setName("OBJ_206");

            //---- OBJ_210 ----
            OBJ_210.setText("X");
            OBJ_210.setName("OBJ_210");

            //---- OBJ_225 ----
            OBJ_225.setText("(");
            OBJ_225.setName("OBJ_225");

            //---- OBJ_224 ----
            OBJ_224.setText("(");
            OBJ_224.setName("OBJ_224");

            //---- OBJ_230 ----
            OBJ_230.setText(")");
            OBJ_230.setName("OBJ_230");

            //---- OBJ_231 ----
            OBJ_231.setText(")");
            OBJ_231.setName("OBJ_231");

            //---- OBJ_240 ----
            OBJ_240.setText("+");
            OBJ_240.setName("OBJ_240");

            //---- OBJ_241 ----
            OBJ_241.setText("+");
            OBJ_241.setName("OBJ_241");

            //---- OBJ_226 ----
            OBJ_226.setText("(");
            OBJ_226.setName("OBJ_226");

            //---- OBJ_227 ----
            OBJ_227.setText("(");
            OBJ_227.setName("OBJ_227");

            //---- OBJ_211 ----
            OBJ_211.setText("X");
            OBJ_211.setName("OBJ_211");

            //---- OBJ_207 ----
            OBJ_207.setText("X");
            OBJ_207.setName("OBJ_207");

            //---- OBJ_229 ----
            OBJ_229.setText(")");
            OBJ_229.setName("OBJ_229");

            //---- OBJ_228 ----
            OBJ_228.setText(")");
            OBJ_228.setName("OBJ_228");

            //---- OBJ_220 ----
            OBJ_220.setText("(");
            OBJ_220.setName("OBJ_220");

            //---- OBJ_221 ----
            OBJ_221.setText("(");
            OBJ_221.setName("OBJ_221");

            GroupLayout xTitledPanel2ContentContainerLayout = new GroupLayout(xTitledPanel2ContentContainer);
            xTitledPanel2ContentContainer.setLayout(xTitledPanel2ContentContainerLayout);
            xTitledPanel2ContentContainerLayout.setHorizontalGroup(
              xTitledPanel2ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(8, 8, 8)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(ETO10, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ETO1, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_218, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_219, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE))
                  .addGap(8, 8, 8)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_220, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_221, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(10, 10, 10)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(ETO11, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ETO2, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))))
                  .addGap(10, 10, 10)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_204, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_208, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE))
                  .addGap(3, 3, 3)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(ETO12, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ETO3, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_234, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_235, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_236, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_237, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE))
                  .addGap(8, 8, 8)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_223, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_222, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(10, 10, 10)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(ETO4, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ETO13, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))))
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_205, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_209, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE))
                  .addGap(3, 3, 3)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(ETO14, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ETO5, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_233, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_232, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_239, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_238, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(10, 10, 10)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_224, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_225, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE))))
                  .addGap(3, 3, 3)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(ETO15, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ETO6, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_206, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_210, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(10, 10, 10)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(ETO16, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ETO7, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))))
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_231, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_230, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_240, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_241, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(10, 10, 10)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_227, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_226, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE))))
                  .addGap(3, 3, 3)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(ETO8, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ETO17, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_207, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_211, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(10, 10, 10)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(ETO18, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ETO9, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))))
                  .addGap(10, 10, 10)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_228, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_229, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)))
            );
            xTitledPanel2ContentContainerLayout.setVerticalGroup(
              xTitledPanel2ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(ETO10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(ETO1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_218)
                  .addGap(9, 9, 9)
                  .addComponent(OBJ_219))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(6, 6, 6)
                  .addComponent(OBJ_220)
                  .addGap(4, 4, 4)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_221))
                    .addComponent(ETO11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addComponent(ETO2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(6, 6, 6)
                  .addComponent(OBJ_204)
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_208))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(ETO12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(ETO3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_234)
                  .addGap(9, 9, 9)
                  .addComponent(OBJ_235))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_236)
                  .addGap(9, 9, 9)
                  .addComponent(OBJ_237))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(ETO4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(6, 6, 6)
                          .addComponent(OBJ_223)))
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_222))
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(ETO13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_205)
                  .addGap(9, 9, 9)
                  .addComponent(OBJ_209))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(ETO14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(ETO5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_233)
                  .addGap(9, 9, 9)
                  .addComponent(OBJ_232))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_224)
                    .addComponent(OBJ_239))
                  .addGap(9, 9, 9)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_238)
                    .addComponent(OBJ_225)))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(ETO15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(ETO6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_206)
                      .addGap(3, 3, 3)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(6, 6, 6)
                          .addComponent(OBJ_210))
                        .addComponent(ETO16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                    .addComponent(ETO7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_231)
                  .addGap(9, 9, 9)
                  .addComponent(OBJ_230))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_227)
                    .addComponent(OBJ_240))
                  .addGap(9, 9, 9)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_241)
                    .addComponent(OBJ_226)))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(ETO8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(ETO17, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(1, 1, 1)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_207)
                      .addGap(3, 3, 3)
                      .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                        .addComponent(ETO18, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                          .addGap(6, 6, 6)
                          .addComponent(OBJ_211))))
                    .addComponent(ETO9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_228)
                  .addGap(9, 9, 9)
                  .addComponent(OBJ_229))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                  .addComponent(xTitledPanel2, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(xTitledPanel1, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(56, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(75, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_16 ----
      OBJ_16.setText("Duplication");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private XRiTextField INDETB;
  private JLabel OBJ_34;
  private JLabel OBJ_69;
  private XRiTextField INDCOD;
  private RiZoneSortie ETLIB;
  private XRiTextField ETJO;
  private JLabel BTN_JO;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_193;
  private JLabel OBJ_200;
  private XRiTextField NCG1X;
  private XRiTextField TIE1;
  private JSpinner SNS1;
  private XRiTextField CLB1;
  private XRiTextField LIB1;
  private XRiTextField AFF01;
  private XRiTextField SAN1;
  private XRiTextField NCP1;
  private XRiTextField ZON1;
  private JLabel FL1;
  private XRiTextField NCG2X;
  private XRiTextField TIE2;
  private JSpinner SNS2;
  private XRiTextField CLB2;
  private XRiTextField LIB2;
  private XRiTextField AFF02;
  private XRiTextField SAN2;
  private XRiTextField NCP2;
  private XRiTextField ZON2;
  private JLabel FL2;
  private XRiTextField NCG3X;
  private XRiTextField TIE3;
  private JSpinner SNS3;
  private XRiTextField CLB3;
  private XRiTextField LIB3;
  private XRiTextField AFF03;
  private XRiTextField SAN3;
  private XRiTextField NCP3;
  private XRiTextField ZON3;
  private JLabel FL3;
  private XRiTextField NCG4X;
  private XRiTextField TIE4;
  private JSpinner SNS4;
  private XRiTextField CLB4;
  private XRiTextField LIB4;
  private XRiTextField AFF04;
  private XRiTextField SAN4;
  private XRiTextField NCP4;
  private XRiTextField ZON4;
  private JLabel FL4;
  private XRiTextField NCG5X;
  private XRiTextField TIE5;
  private JSpinner SNS5;
  private XRiTextField CLB5;
  private XRiTextField LIB5;
  private XRiTextField AFF05;
  private XRiTextField SAN5;
  private XRiTextField NCP5;
  private XRiTextField ZON5;
  private JLabel FL5;
  private XRiTextField NCG6X;
  private XRiTextField TIE6;
  private JSpinner SNS6;
  private XRiTextField CLB6;
  private XRiTextField LIB6;
  private XRiTextField AFF06;
  private XRiTextField SAN6;
  private XRiTextField NCP6;
  private XRiTextField ZON6;
  private JLabel FL6;
  private XRiTextField NCG7X;
  private XRiTextField TIE7;
  private JSpinner SNS7;
  private XRiTextField CLB7;
  private XRiTextField LIB7;
  private XRiTextField AFF07;
  private XRiTextField SAN7;
  private XRiTextField NCP7;
  private XRiTextField ZON7;
  private JLabel FL7;
  private XRiTextField NCG8X;
  private XRiTextField TIE8;
  private JSpinner SNS8;
  private XRiTextField CLB8;
  private XRiTextField LIB8;
  private XRiTextField AFF08;
  private XRiTextField SAN8;
  private XRiTextField NCP8;
  private XRiTextField ZON8;
  private JLabel FL8;
  private XRiTextField NCG9X;
  private XRiTextField TIE9;
  private JSpinner SNS9;
  private XRiTextField CLB9;
  private XRiTextField LIB9;
  private XRiTextField AFF09;
  private XRiTextField SAN9;
  private XRiTextField NCP9;
  private XRiTextField ZON9;
  private JLabel FL9;
  private XRiTextField NCG10X;
  private XRiTextField TIE10;
  private JSpinner SNS10;
  private XRiTextField CLB10;
  private XRiTextField LIB10;
  private XRiTextField AFF10;
  private XRiTextField SAN10;
  private XRiTextField NCP10;
  private XRiTextField ZON10;
  private JLabel FL10;
  private XRiTextField NCG11X;
  private XRiTextField TIE11;
  private JSpinner SNS11;
  private XRiTextField CLB11;
  private XRiTextField LIB11;
  private XRiTextField AFF11;
  private XRiTextField SAN11;
  private XRiTextField NCP11;
  private XRiTextField ZON11;
  private JLabel FL11;
  private XRiTextField NCG12X;
  private XRiTextField TIE12;
  private JSpinner SNS12;
  private XRiTextField CLB12;
  private XRiTextField LIB12;
  private XRiTextField AFF12;
  private XRiTextField SAN12;
  private XRiTextField NCP12;
  private XRiTextField ZON12;
  private JLabel FL12;
  private XRiTextField NCG13X;
  private XRiTextField TIE13;
  private JSpinner SNS13;
  private XRiTextField CLB13;
  private XRiTextField LIB13;
  private XRiTextField AFF13;
  private XRiTextField SAN13;
  private XRiTextField NCP13;
  private XRiTextField ZON13;
  private JLabel FL13;
  private XRiTextField NCG14X;
  private XRiTextField TIE14;
  private JSpinner SNS14;
  private XRiTextField CLB14;
  private XRiTextField LIB14;
  private XRiTextField AFF14;
  private XRiTextField SAN14;
  private XRiTextField NCP14;
  private XRiTextField ZON14;
  private JLabel FL14;
  private JLabel OBJ_197;
  private JLabel OBJ_201;
  private JLabel OBJ_194;
  private JLabel OBJ_199;
  private JLabel OBJ_198;
  private JLabel OBJ_195;
  private JLabel OBJ_196;
  private XRiTextField TIE15;
  private XRiTextField TIE16;
  private XRiTextField TIE17;
  private XRiTextField TIE18;
  private XRiTextField TIE19;
  private XRiTextField TIE20;
  private XRiTextField TIE21;
  private JXTitledPanel xTitledPanel2;
  private XRiTextField ETO1;
  private XRiTextField ETO2;
  private XRiTextField ETO3;
  private XRiTextField ETO4;
  private XRiTextField ETO5;
  private XRiTextField ETO6;
  private XRiTextField ETO7;
  private XRiTextField ETO8;
  private XRiTextField ETO9;
  private XRiTextField ETO10;
  private XRiTextField ETO11;
  private XRiTextField ETO12;
  private XRiTextField ETO13;
  private XRiTextField ETO14;
  private XRiTextField ETO15;
  private XRiTextField ETO16;
  private XRiTextField ETO17;
  private XRiTextField ETO18;
  private JLabel OBJ_218;
  private JLabel OBJ_219;
  private JLabel OBJ_204;
  private JLabel OBJ_208;
  private JLabel OBJ_234;
  private JLabel OBJ_235;
  private JLabel OBJ_237;
  private JLabel OBJ_236;
  private JLabel OBJ_223;
  private JLabel OBJ_222;
  private JLabel OBJ_205;
  private JLabel OBJ_209;
  private JLabel OBJ_233;
  private JLabel OBJ_232;
  private JLabel OBJ_238;
  private JLabel OBJ_239;
  private JLabel OBJ_206;
  private JLabel OBJ_210;
  private JLabel OBJ_225;
  private JLabel OBJ_224;
  private JLabel OBJ_230;
  private JLabel OBJ_231;
  private JLabel OBJ_240;
  private JLabel OBJ_241;
  private JLabel OBJ_226;
  private JLabel OBJ_227;
  private JLabel OBJ_211;
  private JLabel OBJ_207;
  private JLabel OBJ_229;
  private JLabel OBJ_228;
  private JLabel OBJ_220;
  private JLabel OBJ_221;
  private JPopupMenu BTD;
  private JMenuItem OBJ_16;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
