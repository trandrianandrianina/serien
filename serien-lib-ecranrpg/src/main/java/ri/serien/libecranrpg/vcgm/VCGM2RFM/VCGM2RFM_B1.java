
package ri.serien.libecranrpg.vcgm.VCGM2RFM;
// Nom Fichier: pop_VCGM2RFM_FMTB1_FMTF1_643.java

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGM2RFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM2RFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
    // setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    WSUF.setEnabled(lexique.isPresent("WSUF"));
    WNUM.setEnabled(lexique.isPresent("WNUM"));
    WMTTD.setEnabled(lexique.isPresent("WMTTD"));
    WMTT.setEnabled(lexique.isPresent("WMTT"));
    OBJ_22.setEnabled(lexique.isPresent("WMTTD"));
    
    // TODO Icones
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    OBJ_21.setIcon(lexique.chargerImage("images/retour.png", true));
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Saisie règlement"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_14 = new JLabel();
    OBJ_17 = new JLabel();
    OBJ_22 = new JLabel();
    OBJ_21 = new JButton();
    WMTT = new XRiTextField();
    WMTTD = new XRiTextField();
    WNUM = new XRiTextField();
    WSUF = new XRiTextField();
    BT_ENTER = new JButton();
    OBJ_4 = new JMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_8 = new JMenuItem();

    //======== this ========
    setName("this");

    //---- OBJ_14 ----
    OBJ_14.setText("Num\u00e9ro dette g\u00e9n\u00e9r\u00e9");
    OBJ_14.setName("OBJ_14");

    //---- OBJ_17 ----
    OBJ_17.setText("Montant acompte");
    OBJ_17.setName("OBJ_17");

    //---- OBJ_22 ----
    OBJ_22.setText("Acompte devise");
    OBJ_22.setName("OBJ_22");

    //---- OBJ_21 ----
    OBJ_21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_21.setSelectedIcon(null);
    OBJ_21.setToolTipText("Retour");
    OBJ_21.setName("OBJ_21");
    OBJ_21.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_21ActionPerformed(e);
      }
    });

    //---- WMTT ----
    WMTT.setComponentPopupMenu(BTD);
    WMTT.setName("WMTT");

    //---- WMTTD ----
    WMTTD.setComponentPopupMenu(BTD);
    WMTTD.setName("WMTTD");

    //---- WNUM ----
    WNUM.setComponentPopupMenu(BTD);
    WNUM.setName("WNUM");

    //---- WSUF ----
    WSUF.setComponentPopupMenu(BTD);
    WSUF.setName("WSUF");

    //---- BT_ENTER ----
    BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_ENTER.setToolTipText("Ok");
    BT_ENTER.setName("BT_ENTER");
    BT_ENTER.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_20ActionPerformed(e);
      }
    });

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(27, 27, 27)
          .addComponent(OBJ_14, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
          .addGap(19, 19, 19)
          .addComponent(WNUM, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
          .addGap(3, 3, 3)
          .addComponent(WSUF, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(27, 27, 27)
          .addComponent(OBJ_17, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE)
          .addGap(43, 43, 43)
          .addComponent(WMTT, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(27, 27, 27)
          .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 103, GroupLayout.PREFERRED_SIZE)
          .addGap(46, 46, 46)
          .addComponent(WMTTD, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(205, 205, 205)
          .addComponent(BT_ENTER, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
          .addGap(6, 6, 6)
          .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(38, 38, 38)
          .addGroup(layout.createParallelGroup()
            .addComponent(OBJ_14, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
            .addComponent(WNUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addComponent(WSUF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          .addGap(1, 1, 1)
          .addGroup(layout.createParallelGroup()
            .addComponent(OBJ_17, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
            .addComponent(WMTT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          .addGap(1, 1, 1)
          .addGroup(layout.createParallelGroup()
            .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
            .addComponent(WMTTD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          .addGap(31, 31, 31)
          .addGroup(layout.createParallelGroup()
            .addComponent(BT_ENTER, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
            .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
    );

    //======== OBJ_4 ========
    {
      OBJ_4.setText("Commandes");
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("Exploitation");
      OBJ_6.setName("OBJ_6");
      OBJ_4.add(OBJ_6);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_8 ----
      OBJ_8.setText("Aide en ligne");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      BTD.add(OBJ_8);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel OBJ_14;
  private JLabel OBJ_17;
  private JLabel OBJ_22;
  private JButton OBJ_21;
  private XRiTextField WMTT;
  private XRiTextField WMTTD;
  private XRiTextField WNUM;
  private XRiTextField WSUF;
  private JButton BT_ENTER;
  private JMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JPopupMenu BTD;
  private JMenuItem OBJ_8;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
