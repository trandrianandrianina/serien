
package ri.serien.libecranrpg.vcgm.VCGMAFFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VCGMAFFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _T01_Top = { "T01", "T02", "T03", "T04", "T05", "T06", "T07", "T08", "T09", "T10", "T11", "T12", "T13", "T14", "T15", };
  private String[] _T01_Title = { "Types de frais (Paramètre TF)", };
  private String[][] _T01_Data = { { "TY01", }, { "TY02", }, { "TY03", }, { "TY04", }, { "TY05", }, { "TY06", }, { "TY07", }, { "TY08", },
      { "TY09", }, { "TY10", }, { "TY11", }, { "TY12", }, { "TY13", }, { "TY14", }, { "TY15", }, };
  private int[] _T01_Width = { 216, };
  private String[] _C01_Top = { "C01", "C02", "C03", "C04", "C05", "C06", "C07", "C08", "C09", "C10", "C11", "C12", "C13", "C14", "C15", };
  private String[] _C01_Title = { "LIBSEL", };
  private String[][] _C01_Data = { { "CF01", }, { "CF02", }, { "CF03", }, { "CF04", }, { "CF05", }, { "CF06", }, { "CF07", }, { "CF08", },
      { "CF09", }, { "CF10", }, { "CF11", }, { "CF12", }, { "CF13", }, { "CF14", }, { "CF15", }, };
  private int[] _C01_Width = { 214, };
  
  public VCGMAFFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    T01.setAspectTable(_T01_Top, _T01_Title, _T01_Data, _T01_Width, false, null, null, null, null);
    C01.setAspectTable(_C01_Top, _C01_Title, _C01_Data, _C01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riZoneSortie1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBNCG@")).trim());
    riZoneSortie2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMTT@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _T01_Top);
    // majTable(C01, C01.get_LIST_Title_Data_Brut(), _C01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    T01.setVisible(lexique.isTrue("N80"));
    
    // Titre
    // setTitle(???);
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void T01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _T01_Top, "1", "F13", e);
    if (T01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "F13");
    }
  }
  
  private void C01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST2, _C01_Top, "4", "Enter", e);
    C01.setValeurTop("4");
    if (C01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void bt_UPActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGUP");
  }
  
  private void bt_DOWNActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGDOWN");
  }
  
  private void bt_UP2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGUP");
  }
  
  private void bt_DOWN2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGDOWN");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _T01_Top, "1", "F13");
    T01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "F13");
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD2.getInvoker().getName());
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST2, _C01_Top, "4", "Enter");
    C01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    T01 = new XRiTable();
    SCROLLPANE_LIST2 = new JScrollPane();
    C01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BT_PGUP2 = new JButton();
    BT_PGDOWN2 = new JButton();
    barre_tete = new JMenuBar();
    panel2 = new JPanel();
    label1 = new JLabel();
    riZoneSortie1 = new RiZoneSortie();
    label2 = new JLabel();
    riZoneSortie2 = new RiZoneSortie();
    BTD2 = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_16 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 420));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //======== SCROLLPANE_LIST ========
          {
            SCROLLPANE_LIST.setComponentPopupMenu(BTD);
            SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

            //---- T01 ----
            T01.setComponentPopupMenu(BTD2);
            T01.setName("T01");
            T01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                T01MouseClicked(e);
              }
            });
            SCROLLPANE_LIST.setViewportView(T01);
          }
          panel1.add(SCROLLPANE_LIST);
          SCROLLPANE_LIST.setBounds(25, 35, 235, 270);

          //======== SCROLLPANE_LIST2 ========
          {
            SCROLLPANE_LIST2.setComponentPopupMenu(BTD2);
            SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

            //---- C01 ----
            C01.setComponentPopupMenu(BTD);
            C01.setName("C01");
            C01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                C01MouseClicked(e);
              }
            });
            SCROLLPANE_LIST2.setViewportView(C01);
          }
          panel1.add(SCROLLPANE_LIST2);
          SCROLLPANE_LIST2.setBounds(305, 35, 235, 270);

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          BT_PGUP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bt_UPActionPerformed(e);
            }
          });
          panel1.add(BT_PGUP);
          BT_PGUP.setBounds(265, 40, 25, 125);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          BT_PGDOWN.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bt_DOWNActionPerformed(e);
            }
          });
          panel1.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(265, 180, 25, 125);

          //---- BT_PGUP2 ----
          BT_PGUP2.setText("");
          BT_PGUP2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP2.setName("BT_PGUP2");
          BT_PGUP2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bt_UPActionPerformed(e);
              bt_UP2ActionPerformed(e);
            }
          });
          panel1.add(BT_PGUP2);
          BT_PGUP2.setBounds(545, 40, 25, 125);

          //---- BT_PGDOWN2 ----
          BT_PGDOWN2.setText("");
          BT_PGDOWN2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN2.setName("BT_PGDOWN2");
          BT_PGDOWN2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bt_DOWNActionPerformed(e);
              bt_DOWN2ActionPerformed(e);
            }
          });
          panel1.add(BT_PGDOWN2);
          BT_PGDOWN2.setBounds(545, 180, 25, 125);
        }
        p_contenu.add(panel1);
        panel1.setBounds(20, 20, 590, 355);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== barre_tete ========
    {
      barre_tete.setPreferredSize(new Dimension(111, 30));
      barre_tete.setName("barre_tete");

      //======== panel2 ========
      {
        panel2.setOpaque(false);
        panel2.setPreferredSize(new Dimension(380, 32));
        panel2.setMinimumSize(new Dimension(380, 32));
        panel2.setMaximumSize(new Dimension(380, 32));
        panel2.setName("panel2");

        //---- label1 ----
        label1.setText("Compte");
        label1.setName("label1");

        //---- riZoneSortie1 ----
        riZoneSortie1.setText("@LIBNCG@");
        riZoneSortie1.setOpaque(false);
        riZoneSortie1.setName("riZoneSortie1");

        //---- label2 ----
        label2.setText("Montant");
        label2.setName("label2");

        //---- riZoneSortie2 ----
        riZoneSortie2.setText("@LIBMTT@");
        riZoneSortie2.setOpaque(false);
        riZoneSortie2.setHorizontalAlignment(SwingConstants.RIGHT);
        riZoneSortie2.setName("riZoneSortie2");

        GroupLayout panel2Layout = new GroupLayout(panel2);
        panel2.setLayout(panel2Layout);
        panel2Layout.setHorizontalGroup(
          panel2Layout.createParallelGroup()
            .addGroup(panel2Layout.createSequentialGroup()
              .addGap(5, 5, 5)
              .addComponent(label1, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
              .addGap(10, 10, 10)
              .addComponent(riZoneSortie1, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
              .addGap(35, 35, 35)
              .addComponent(label2, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
              .addComponent(riZoneSortie2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        );
        panel2Layout.setVerticalGroup(
          panel2Layout.createParallelGroup()
            .addGroup(panel2Layout.createSequentialGroup()
              .addGap(4, 4, 4)
              .addComponent(label1))
            .addComponent(riZoneSortie1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGroup(panel2Layout.createSequentialGroup()
              .addGap(4, 4, 4)
              .addComponent(label2))
            .addComponent(riZoneSortie2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        );
      }
      barre_tete.add(panel2);
    }
    add(barre_tete, BorderLayout.NORTH);

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- OBJ_5 ----
      OBJ_5.setText("Associer");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("Aide en ligne");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_6);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_16 ----
      OBJ_16.setText("Supprimer");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable T01;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable C01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JButton BT_PGUP2;
  private JButton BT_PGDOWN2;
  private JMenuBar barre_tete;
  private JPanel panel2;
  private JLabel label1;
  private RiZoneSortie riZoneSortie1;
  private JLabel label2;
  private RiZoneSortie riZoneSortie2;
  private JPopupMenu BTD2;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JPopupMenu BTD;
  private JMenuItem OBJ_16;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
