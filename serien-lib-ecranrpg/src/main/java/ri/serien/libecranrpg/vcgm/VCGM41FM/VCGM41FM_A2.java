
package ri.serien.libecranrpg.vcgm.VCGM41FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VCGM41FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _WL201_Title = { "HLD01", };
  private String[][] _WL201_Data = { { "WL201", }, { "WL202", }, { "WL203", }, { "WL204", }, { "WL205", }, { "WL206", }, };
  private int[] _WL201_Width = { 390, };
  
  public VCGM41FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    WL201.setAspectTable(null, _WL201_Title, _WL201_Data, _WL201_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LDLIB@")).trim());
    OBJ_14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LDDAT@")).trim());
    OBJ_12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LDFOL@")).trim());
    OBJ_10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LDJO@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), null);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    OBJ_10.setVisible(lexique.isPresent("LDJO"));
    OBJ_12.setVisible(lexique.isPresent("LDFOL"));
    OBJ_14.setVisible(lexique.isPresent("LDDAT"));
    OBJ_8.setVisible(lexique.isPresent("LDLIB"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITRE@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WL201 = new XRiTable();
    OBJ_8 = new RiZoneSortie();
    OBJ_14 = new RiZoneSortie();
    OBJ_9 = new JLabel();
    OBJ_7 = new JLabel();
    OBJ_11 = new JLabel();
    OBJ_12 = new RiZoneSortie();
    OBJ_13 = new JLabel();
    OBJ_10 = new RiZoneSortie();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(665, 400));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder("D\u00e9tail analyse sur axes"));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //======== SCROLLPANE_LIST ========
          {
            SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

            //---- WL201 ----
            WL201.setName("WL201");
            SCROLLPANE_LIST.setViewportView(WL201);
          }
          p_recup.add(SCROLLPANE_LIST);
          SCROLLPANE_LIST.setBounds(35, 185, 345, 125);

          //---- OBJ_8 ----
          OBJ_8.setText("@LDLIB@");
          OBJ_8.setName("OBJ_8");
          p_recup.add(OBJ_8);
          OBJ_8.setBounds(115, 40, 295, OBJ_8.getPreferredSize().height);

          //---- OBJ_14 ----
          OBJ_14.setText("@LDDAT@");
          OBJ_14.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_14.setName("OBJ_14");
          p_recup.add(OBJ_14);
          OBJ_14.setBounds(115, 145, 70, OBJ_14.getPreferredSize().height);

          //---- OBJ_9 ----
          OBJ_9.setText("Journal");
          OBJ_9.setName("OBJ_9");
          p_recup.add(OBJ_9);
          OBJ_9.setBounds(35, 77, 66, 20);

          //---- OBJ_7 ----
          OBJ_7.setText("Libell\u00e9");
          OBJ_7.setName("OBJ_7");
          p_recup.add(OBJ_7);
          OBJ_7.setBounds(35, 42, 66, 20);

          //---- OBJ_11 ----
          OBJ_11.setText("Folio");
          OBJ_11.setName("OBJ_11");
          p_recup.add(OBJ_11);
          OBJ_11.setBounds(35, 112, 66, 20);

          //---- OBJ_12 ----
          OBJ_12.setText("@LDFOL@");
          OBJ_12.setName("OBJ_12");
          p_recup.add(OBJ_12);
          OBJ_12.setBounds(115, 110, 40, OBJ_12.getPreferredSize().height);

          //---- OBJ_13 ----
          OBJ_13.setText("Date");
          OBJ_13.setName("OBJ_13");
          p_recup.add(OBJ_13);
          OBJ_13.setBounds(35, 147, 66, 20);

          //---- OBJ_10 ----
          OBJ_10.setText("@LDJO@");
          OBJ_10.setName("OBJ_10");
          p_recup.add(OBJ_10);
          OBJ_10.setBounds(115, 75, 34, OBJ_10.getPreferredSize().height);

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          p_recup.add(BT_PGUP);
          BT_PGUP.setBounds(385, 185, 25, 55);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setToolTipText("Page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          p_recup.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(385, 255, 25, 55);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_recup.getComponentCount(); i++) {
              Rectangle bounds = p_recup.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_recup.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_recup.setMinimumSize(preferredSize);
            p_recup.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 455, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 345, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WL201;
  private RiZoneSortie OBJ_8;
  private RiZoneSortie OBJ_14;
  private JLabel OBJ_9;
  private JLabel OBJ_7;
  private JLabel OBJ_11;
  private RiZoneSortie OBJ_12;
  private JLabel OBJ_13;
  private RiZoneSortie OBJ_10;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
