
package ri.serien.libecranrpg.vcgm.VCGM18FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Emmanuel MARCQ
 */
public class VCGM18FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM18FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    OBJ_44_OBJ_44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGPERX@")).trim());
    OBJ_45_OBJ_45.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MESCLO@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    JOLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@JOLIB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    panel1.setVisible(lexique.isTrue("21"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_19 = new JXTitledSeparator();
    OBJ_23 = new JLabel();
    OBJ_22 = new JLabel();
    AADEM = new XRiTextField();
    OBJ_21 = new JLabel();
    MMDEM = new XRiTextField();
    OBJ_43_OBJ_43 = new JLabel();
    OBJ_44_OBJ_44 = new RiZoneSortie();
    OBJ_45_OBJ_45 = new RiZoneSortie();
    z_dgnom_ = new RiZoneSortie();
    OBJ_25 = new JLabel();
    panel1 = new JPanel();
    OBJ_20 = new JXTitledSeparator();
    JOLIB = new RiZoneSortie();
    OBJ_24 = new JLabel();
    XPARJO = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(650, 320));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");
          p_contenu.add(sep_etablissement);
          sep_etablissement.setBounds(35, 35, 585, sep_etablissement.getPreferredSize().height);

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");
          p_contenu.add(z_etablissement_);
          z_etablissement_.setBounds(50, 77, 40, z_etablissement_.getPreferredSize().height);

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });
          p_contenu.add(bouton_etablissement);
          bouton_etablissement.setBounds(new Rectangle(new Point(95, 75), bouton_etablissement.getPreferredSize()));

          //---- OBJ_19 ----
          OBJ_19.setTitle("");
          OBJ_19.setName("OBJ_19");
          p_contenu.add(OBJ_19);
          OBJ_19.setBounds(35, 130, 585, OBJ_19.getPreferredSize().height);

          //---- OBJ_23 ----
          OBJ_23.setText("P\u00e9riode \u00e0 traiter");
          OBJ_23.setName("OBJ_23");
          p_contenu.add(OBJ_23);
          OBJ_23.setBounds(50, 165, 111, 28);

          //---- OBJ_22 ----
          OBJ_22.setText("aaaa");
          OBJ_22.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_22.setName("OBJ_22");
          p_contenu.add(OBJ_22);
          OBJ_22.setBounds(265, 145, 50, 20);

          //---- AADEM ----
          AADEM.setComponentPopupMenu(null);
          AADEM.setName("AADEM");
          p_contenu.add(AADEM);
          AADEM.setBounds(265, 165, 50, AADEM.getPreferredSize().height);

          //---- OBJ_21 ----
          OBJ_21.setText("mm");
          OBJ_21.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_21.setName("OBJ_21");
          p_contenu.add(OBJ_21);
          OBJ_21.setBounds(205, 145, 30, 20);

          //---- MMDEM ----
          MMDEM.setComponentPopupMenu(null);
          MMDEM.setName("MMDEM");
          p_contenu.add(MMDEM);
          MMDEM.setBounds(205, 165, 30, MMDEM.getPreferredSize().height);

          //---- OBJ_43_OBJ_43 ----
          OBJ_43_OBJ_43.setText("En cours");
          OBJ_43_OBJ_43.setName("OBJ_43_OBJ_43");
          p_contenu.add(OBJ_43_OBJ_43);
          OBJ_43_OBJ_43.setBounds(205, 93, 65, 24);

          //---- OBJ_44_OBJ_44 ----
          OBJ_44_OBJ_44.setText("@DGPERX@");
          OBJ_44_OBJ_44.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_44_OBJ_44.setName("OBJ_44_OBJ_44");
          p_contenu.add(OBJ_44_OBJ_44);
          OBJ_44_OBJ_44.setBounds(265, 93, 60, OBJ_44_OBJ_44.getPreferredSize().height);

          //---- OBJ_45_OBJ_45 ----
          OBJ_45_OBJ_45.setText("@MESCLO@");
          OBJ_45_OBJ_45.setName("OBJ_45_OBJ_45");
          p_contenu.add(OBJ_45_OBJ_45);
          OBJ_45_OBJ_45.setBounds(335, 93, 170, OBJ_45_OBJ_45.getPreferredSize().height);

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");
          p_contenu.add(z_dgnom_);
          z_dgnom_.setBounds(205, 60, 300, z_dgnom_.getPreferredSize().height);

          //---- OBJ_25 ----
          OBJ_25.setText("/");
          OBJ_25.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_25.setName("OBJ_25");
          p_contenu.add(OBJ_25);
          OBJ_25.setBounds(235, 169, 30, 20);

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_20 ----
            OBJ_20.setTitle("");
            OBJ_20.setName("OBJ_20");
            panel1.add(OBJ_20);
            OBJ_20.setBounds(5, 10, 585, OBJ_20.getPreferredSize().height);

            //---- JOLIB ----
            JOLIB.setText("@JOLIB@");
            JOLIB.setName("JOLIB");
            panel1.add(JOLIB);
            JOLIB.setBounds(235, 40, 235, 28);

            //---- OBJ_24 ----
            OBJ_24.setText("Code journal de banque");
            OBJ_24.setName("OBJ_24");
            panel1.add(OBJ_24);
            OBJ_24.setBounds(20, 40, 155, 28);

            //---- XPARJO ----
            XPARJO.setComponentPopupMenu(BTD);
            XPARJO.setName("XPARJO");
            panel1.add(XPARJO);
            XPARJO.setBounds(175, 40, 34, XPARJO.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel1);
          panel1.setBounds(30, 200, 600, 90);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_19;
  private JLabel OBJ_23;
  private JLabel OBJ_22;
  private XRiTextField AADEM;
  private JLabel OBJ_21;
  private XRiTextField MMDEM;
  private JLabel OBJ_43_OBJ_43;
  private RiZoneSortie OBJ_44_OBJ_44;
  private RiZoneSortie OBJ_45_OBJ_45;
  private RiZoneSortie z_dgnom_;
  private JLabel OBJ_25;
  private JPanel panel1;
  private JXTitledSeparator OBJ_20;
  private RiZoneSortie JOLIB;
  private JLabel OBJ_24;
  private XRiTextField XPARJO;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
