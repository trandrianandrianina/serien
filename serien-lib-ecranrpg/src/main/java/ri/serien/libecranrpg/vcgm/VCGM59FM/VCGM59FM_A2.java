
package ri.serien.libecranrpg.vcgm.VCGM59FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGM59FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  private ArrayList<XRiTextField> listeComposants = null;
  private ArrayList<String> listeErreurs = null;
  private String[] listeZonesErreurs = { "ERR01", "ERR02", "ERR03", "ERR04", "ERR05", "ERR06", "ERR07", "ERR08", "ERR09", "ERR10" };
  private Color rouge = new Color(249, 102, 102);
  
  public VCGM59FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    initierLesComposants();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMTD@")).trim());
    OBJ_34.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMTC@")).trim());
    OBJ_36.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSOLD@")).trim());
    OBJ_37.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSNS@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    OBJ_36.setVisible(lexique.isPresent("WSOLD"));
    OBJ_34.setVisible(lexique.isPresent("WMTC"));
    OBJ_32.setVisible(lexique.isPresent("WMTD"));
    
    listeErreurs = new ArrayList<String>();
    for (int i = 0; i < listeZonesErreurs.length; i++) {
      if (lexique.HostFieldGetData(listeZonesErreurs[i]).trim().equals("<")) {
        listeErreurs.add(listeZonesErreurs[i].substring(listeZonesErreurs[i].length() - 2, listeZonesErreurs[i].length()));
      }
      
    }
    
    for (int i = 0; i < listeComposants.size(); i++) {
      listeComposants.get(i).setBackground(Color.white);
      for (int j = 0; j < listeErreurs.size(); j++) {
        if (listeComposants.get(i).getName().endsWith(listeErreurs.get(j))) {
          listeComposants.get(i).setBackground(rouge);
        }
      }
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Saisie des écritures comptables en date du @DATFOX@"));
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(popupMenu1.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void initierLesComposants() {
    listeComposants = new ArrayList<XRiTextField>();
    
    for (int i = 0; i < panel1.getComponentCount(); i++) {
      if (panel1.getComponent(i) instanceof XRiTextField) {
        listeComposants.add((XRiTextField) panel1.getComponent(i));
      }
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    ECG01 = new XRiTextField();
    ECG02 = new XRiTextField();
    ECG03 = new XRiTextField();
    ECG04 = new XRiTextField();
    ECG05 = new XRiTextField();
    ECG06 = new XRiTextField();
    ECG07 = new XRiTextField();
    ECG08 = new XRiTextField();
    ECG09 = new XRiTextField();
    ECG10 = new XRiTextField();
    ECA01 = new XRiTextField();
    ECA02 = new XRiTextField();
    ECA03 = new XRiTextField();
    ECA04 = new XRiTextField();
    ECA05 = new XRiTextField();
    ECA06 = new XRiTextField();
    ECA07 = new XRiTextField();
    ECA08 = new XRiTextField();
    ECA09 = new XRiTextField();
    ECA10 = new XRiTextField();
    EDC01 = new XRiTextField();
    EDC02 = new XRiTextField();
    EDC03 = new XRiTextField();
    EDC04 = new XRiTextField();
    EDC05 = new XRiTextField();
    EDC06 = new XRiTextField();
    EDC07 = new XRiTextField();
    EDC08 = new XRiTextField();
    EDC09 = new XRiTextField();
    EDC10 = new XRiTextField();
    ECM01 = new XRiTextField();
    ECM02 = new XRiTextField();
    ECM03 = new XRiTextField();
    ECM04 = new XRiTextField();
    ECM05 = new XRiTextField();
    ECM06 = new XRiTextField();
    ECM07 = new XRiTextField();
    ECM08 = new XRiTextField();
    ECM09 = new XRiTextField();
    ECM10 = new XRiTextField();
    ECP01 = new XRiTextField();
    ECP02 = new XRiTextField();
    ECP03 = new XRiTextField();
    ECP04 = new XRiTextField();
    ECP05 = new XRiTextField();
    ECP06 = new XRiTextField();
    ECP07 = new XRiTextField();
    ECP08 = new XRiTextField();
    ECP09 = new XRiTextField();
    ECP10 = new XRiTextField();
    LIB01 = new XRiTextField();
    LIB02 = new XRiTextField();
    LIB03 = new XRiTextField();
    LIB04 = new XRiTextField();
    LIB05 = new XRiTextField();
    LIB06 = new XRiTextField();
    LIB07 = new XRiTextField();
    LIB08 = new XRiTextField();
    LIB09 = new XRiTextField();
    LIB10 = new XRiTextField();
    ECS01 = new XRiTextField();
    ECS02 = new XRiTextField();
    ECS03 = new XRiTextField();
    ECS04 = new XRiTextField();
    ECS05 = new XRiTextField();
    ECS06 = new XRiTextField();
    ECS07 = new XRiTextField();
    ECS08 = new XRiTextField();
    ECS09 = new XRiTextField();
    ECS10 = new XRiTextField();
    EAF01 = new XRiTextField();
    EAF02 = new XRiTextField();
    EAF03 = new XRiTextField();
    EAF04 = new XRiTextField();
    EAF05 = new XRiTextField();
    EAF06 = new XRiTextField();
    EAF07 = new XRiTextField();
    EAF08 = new XRiTextField();
    EAF09 = new XRiTextField();
    EAF10 = new XRiTextField();
    ECN01 = new XRiTextField();
    ECN02 = new XRiTextField();
    ECN03 = new XRiTextField();
    ECN04 = new XRiTextField();
    ECN05 = new XRiTextField();
    ECN06 = new XRiTextField();
    ECN07 = new XRiTextField();
    ECN08 = new XRiTextField();
    ECN09 = new XRiTextField();
    ECN10 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    OBJ_32 = new RiZoneSortie();
    OBJ_34 = new RiZoneSortie();
    OBJ_36 = new RiZoneSortie();
    OBJ_33 = new JLabel();
    OBJ_31 = new JLabel();
    OBJ_35 = new JLabel();
    OBJ_37 = new RiZoneSortie();
    popupMenu1 = new JPopupMenu();
    menuItem1 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1005, 400));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Solde compte bancaire");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- ECG01 ----
          ECG01.setComponentPopupMenu(popupMenu1);
          ECG01.setName("ECG01");
          panel1.add(ECG01);
          ECG01.setBounds(25, 35, 70, ECG01.getPreferredSize().height);

          //---- ECG02 ----
          ECG02.setComponentPopupMenu(popupMenu1);
          ECG02.setName("ECG02");
          panel1.add(ECG02);
          ECG02.setBounds(25, 60, 70, 28);

          //---- ECG03 ----
          ECG03.setComponentPopupMenu(popupMenu1);
          ECG03.setName("ECG03");
          panel1.add(ECG03);
          ECG03.setBounds(25, 85, 70, 28);

          //---- ECG04 ----
          ECG04.setComponentPopupMenu(popupMenu1);
          ECG04.setName("ECG04");
          panel1.add(ECG04);
          ECG04.setBounds(25, 110, 70, 28);

          //---- ECG05 ----
          ECG05.setComponentPopupMenu(popupMenu1);
          ECG05.setName("ECG05");
          panel1.add(ECG05);
          ECG05.setBounds(25, 135, 70, 28);

          //---- ECG06 ----
          ECG06.setComponentPopupMenu(popupMenu1);
          ECG06.setName("ECG06");
          panel1.add(ECG06);
          ECG06.setBounds(25, 160, 70, 28);

          //---- ECG07 ----
          ECG07.setComponentPopupMenu(popupMenu1);
          ECG07.setName("ECG07");
          panel1.add(ECG07);
          ECG07.setBounds(25, 185, 70, 28);

          //---- ECG08 ----
          ECG08.setComponentPopupMenu(popupMenu1);
          ECG08.setName("ECG08");
          panel1.add(ECG08);
          ECG08.setBounds(25, 210, 70, 28);

          //---- ECG09 ----
          ECG09.setComponentPopupMenu(popupMenu1);
          ECG09.setName("ECG09");
          panel1.add(ECG09);
          ECG09.setBounds(25, 235, 70, 28);

          //---- ECG10 ----
          ECG10.setComponentPopupMenu(popupMenu1);
          ECG10.setName("ECG10");
          panel1.add(ECG10);
          ECG10.setBounds(25, 260, 70, 28);

          //---- ECA01 ----
          ECA01.setComponentPopupMenu(popupMenu1);
          ECA01.setName("ECA01");
          panel1.add(ECA01);
          ECA01.setBounds(95, 35, 60, 28);

          //---- ECA02 ----
          ECA02.setComponentPopupMenu(popupMenu1);
          ECA02.setName("ECA02");
          panel1.add(ECA02);
          ECA02.setBounds(95, 60, 60, 28);

          //---- ECA03 ----
          ECA03.setComponentPopupMenu(popupMenu1);
          ECA03.setName("ECA03");
          panel1.add(ECA03);
          ECA03.setBounds(95, 85, 60, 28);

          //---- ECA04 ----
          ECA04.setComponentPopupMenu(popupMenu1);
          ECA04.setName("ECA04");
          panel1.add(ECA04);
          ECA04.setBounds(95, 110, 60, 28);

          //---- ECA05 ----
          ECA05.setComponentPopupMenu(popupMenu1);
          ECA05.setName("ECA05");
          panel1.add(ECA05);
          ECA05.setBounds(95, 135, 60, 28);

          //---- ECA06 ----
          ECA06.setComponentPopupMenu(popupMenu1);
          ECA06.setName("ECA06");
          panel1.add(ECA06);
          ECA06.setBounds(95, 160, 60, 28);

          //---- ECA07 ----
          ECA07.setComponentPopupMenu(popupMenu1);
          ECA07.setName("ECA07");
          panel1.add(ECA07);
          ECA07.setBounds(95, 185, 60, 28);

          //---- ECA08 ----
          ECA08.setComponentPopupMenu(popupMenu1);
          ECA08.setName("ECA08");
          panel1.add(ECA08);
          ECA08.setBounds(95, 210, 60, 28);

          //---- ECA09 ----
          ECA09.setComponentPopupMenu(popupMenu1);
          ECA09.setName("ECA09");
          panel1.add(ECA09);
          ECA09.setBounds(95, 235, 60, 28);

          //---- ECA10 ----
          ECA10.setComponentPopupMenu(popupMenu1);
          ECA10.setName("ECA10");
          panel1.add(ECA10);
          ECA10.setBounds(95, 260, 60, 28);

          //---- EDC01 ----
          EDC01.setComponentPopupMenu(popupMenu1);
          EDC01.setName("EDC01");
          panel1.add(EDC01);
          EDC01.setBounds(155, 35, 24, EDC01.getPreferredSize().height);

          //---- EDC02 ----
          EDC02.setComponentPopupMenu(popupMenu1);
          EDC02.setName("EDC02");
          panel1.add(EDC02);
          EDC02.setBounds(155, 60, 24, 28);

          //---- EDC03 ----
          EDC03.setComponentPopupMenu(popupMenu1);
          EDC03.setName("EDC03");
          panel1.add(EDC03);
          EDC03.setBounds(155, 85, 24, 28);

          //---- EDC04 ----
          EDC04.setComponentPopupMenu(popupMenu1);
          EDC04.setName("EDC04");
          panel1.add(EDC04);
          EDC04.setBounds(155, 110, 24, 28);

          //---- EDC05 ----
          EDC05.setComponentPopupMenu(popupMenu1);
          EDC05.setName("EDC05");
          panel1.add(EDC05);
          EDC05.setBounds(155, 135, 24, 28);

          //---- EDC06 ----
          EDC06.setComponentPopupMenu(popupMenu1);
          EDC06.setName("EDC06");
          panel1.add(EDC06);
          EDC06.setBounds(155, 160, 24, 28);

          //---- EDC07 ----
          EDC07.setComponentPopupMenu(popupMenu1);
          EDC07.setName("EDC07");
          panel1.add(EDC07);
          EDC07.setBounds(155, 185, 24, 28);

          //---- EDC08 ----
          EDC08.setComponentPopupMenu(popupMenu1);
          EDC08.setName("EDC08");
          panel1.add(EDC08);
          EDC08.setBounds(155, 210, 24, 28);

          //---- EDC09 ----
          EDC09.setComponentPopupMenu(popupMenu1);
          EDC09.setName("EDC09");
          panel1.add(EDC09);
          EDC09.setBounds(155, 235, 24, 28);

          //---- EDC10 ----
          EDC10.setComponentPopupMenu(popupMenu1);
          EDC10.setName("EDC10");
          panel1.add(EDC10);
          EDC10.setBounds(155, 260, 24, 28);

          //---- ECM01 ----
          ECM01.setComponentPopupMenu(popupMenu1);
          ECM01.setName("ECM01");
          panel1.add(ECM01);
          ECM01.setBounds(180, 35, 85, ECM01.getPreferredSize().height);

          //---- ECM02 ----
          ECM02.setComponentPopupMenu(popupMenu1);
          ECM02.setName("ECM02");
          panel1.add(ECM02);
          ECM02.setBounds(180, 60, 85, 28);

          //---- ECM03 ----
          ECM03.setComponentPopupMenu(popupMenu1);
          ECM03.setName("ECM03");
          panel1.add(ECM03);
          ECM03.setBounds(180, 85, 85, 28);

          //---- ECM04 ----
          ECM04.setComponentPopupMenu(popupMenu1);
          ECM04.setName("ECM04");
          panel1.add(ECM04);
          ECM04.setBounds(180, 110, 85, 28);

          //---- ECM05 ----
          ECM05.setComponentPopupMenu(popupMenu1);
          ECM05.setName("ECM05");
          panel1.add(ECM05);
          ECM05.setBounds(180, 135, 85, 28);

          //---- ECM06 ----
          ECM06.setComponentPopupMenu(popupMenu1);
          ECM06.setName("ECM06");
          panel1.add(ECM06);
          ECM06.setBounds(180, 160, 85, 28);

          //---- ECM07 ----
          ECM07.setComponentPopupMenu(popupMenu1);
          ECM07.setName("ECM07");
          panel1.add(ECM07);
          ECM07.setBounds(180, 185, 85, 28);

          //---- ECM08 ----
          ECM08.setComponentPopupMenu(popupMenu1);
          ECM08.setName("ECM08");
          panel1.add(ECM08);
          ECM08.setBounds(180, 210, 85, 28);

          //---- ECM09 ----
          ECM09.setComponentPopupMenu(popupMenu1);
          ECM09.setName("ECM09");
          panel1.add(ECM09);
          ECM09.setBounds(180, 235, 85, 28);

          //---- ECM10 ----
          ECM10.setComponentPopupMenu(popupMenu1);
          ECM10.setName("ECM10");
          panel1.add(ECM10);
          ECM10.setBounds(180, 260, 85, 28);

          //---- ECP01 ----
          ECP01.setComponentPopupMenu(popupMenu1);
          ECP01.setName("ECP01");
          panel1.add(ECP01);
          ECP01.setBounds(265, 35, 70, ECP01.getPreferredSize().height);

          //---- ECP02 ----
          ECP02.setComponentPopupMenu(popupMenu1);
          ECP02.setName("ECP02");
          panel1.add(ECP02);
          ECP02.setBounds(265, 60, 70, 28);

          //---- ECP03 ----
          ECP03.setComponentPopupMenu(popupMenu1);
          ECP03.setName("ECP03");
          panel1.add(ECP03);
          ECP03.setBounds(265, 85, 70, 28);

          //---- ECP04 ----
          ECP04.setComponentPopupMenu(popupMenu1);
          ECP04.setName("ECP04");
          panel1.add(ECP04);
          ECP04.setBounds(265, 110, 70, 28);

          //---- ECP05 ----
          ECP05.setComponentPopupMenu(popupMenu1);
          ECP05.setName("ECP05");
          panel1.add(ECP05);
          ECP05.setBounds(265, 135, 70, 28);

          //---- ECP06 ----
          ECP06.setComponentPopupMenu(popupMenu1);
          ECP06.setName("ECP06");
          panel1.add(ECP06);
          ECP06.setBounds(265, 160, 70, 28);

          //---- ECP07 ----
          ECP07.setComponentPopupMenu(popupMenu1);
          ECP07.setName("ECP07");
          panel1.add(ECP07);
          ECP07.setBounds(265, 185, 70, 28);

          //---- ECP08 ----
          ECP08.setComponentPopupMenu(popupMenu1);
          ECP08.setName("ECP08");
          panel1.add(ECP08);
          ECP08.setBounds(265, 210, 70, 28);

          //---- ECP09 ----
          ECP09.setComponentPopupMenu(popupMenu1);
          ECP09.setName("ECP09");
          panel1.add(ECP09);
          ECP09.setBounds(265, 235, 70, 28);

          //---- ECP10 ----
          ECP10.setComponentPopupMenu(popupMenu1);
          ECP10.setName("ECP10");
          panel1.add(ECP10);
          ECP10.setBounds(265, 260, 70, 28);

          //---- LIB01 ----
          LIB01.setComponentPopupMenu(popupMenu1);
          LIB01.setName("LIB01");
          panel1.add(LIB01);
          LIB01.setBounds(335, 35, 270, LIB01.getPreferredSize().height);

          //---- LIB02 ----
          LIB02.setComponentPopupMenu(popupMenu1);
          LIB02.setName("LIB02");
          panel1.add(LIB02);
          LIB02.setBounds(335, 60, 270, 28);

          //---- LIB03 ----
          LIB03.setComponentPopupMenu(popupMenu1);
          LIB03.setName("LIB03");
          panel1.add(LIB03);
          LIB03.setBounds(335, 85, 270, 28);

          //---- LIB04 ----
          LIB04.setComponentPopupMenu(popupMenu1);
          LIB04.setName("LIB04");
          panel1.add(LIB04);
          LIB04.setBounds(335, 110, 270, 28);

          //---- LIB05 ----
          LIB05.setComponentPopupMenu(popupMenu1);
          LIB05.setName("LIB05");
          panel1.add(LIB05);
          LIB05.setBounds(335, 135, 270, 28);

          //---- LIB06 ----
          LIB06.setComponentPopupMenu(popupMenu1);
          LIB06.setName("LIB06");
          panel1.add(LIB06);
          LIB06.setBounds(335, 160, 270, 28);

          //---- LIB07 ----
          LIB07.setComponentPopupMenu(popupMenu1);
          LIB07.setName("LIB07");
          panel1.add(LIB07);
          LIB07.setBounds(335, 185, 270, 28);

          //---- LIB08 ----
          LIB08.setComponentPopupMenu(popupMenu1);
          LIB08.setName("LIB08");
          panel1.add(LIB08);
          LIB08.setBounds(335, 210, 270, 28);

          //---- LIB09 ----
          LIB09.setComponentPopupMenu(popupMenu1);
          LIB09.setName("LIB09");
          panel1.add(LIB09);
          LIB09.setBounds(335, 235, 270, 28);

          //---- LIB10 ----
          LIB10.setComponentPopupMenu(popupMenu1);
          LIB10.setName("LIB10");
          panel1.add(LIB10);
          LIB10.setBounds(335, 260, 270, 28);

          //---- ECS01 ----
          ECS01.setComponentPopupMenu(popupMenu1);
          ECS01.setName("ECS01");
          panel1.add(ECS01);
          ECS01.setBounds(605, 35, 50, ECS01.getPreferredSize().height);

          //---- ECS02 ----
          ECS02.setComponentPopupMenu(popupMenu1);
          ECS02.setName("ECS02");
          panel1.add(ECS02);
          ECS02.setBounds(605, 60, 50, 28);

          //---- ECS03 ----
          ECS03.setComponentPopupMenu(popupMenu1);
          ECS03.setName("ECS03");
          panel1.add(ECS03);
          ECS03.setBounds(605, 85, 50, 28);

          //---- ECS04 ----
          ECS04.setComponentPopupMenu(popupMenu1);
          ECS04.setName("ECS04");
          panel1.add(ECS04);
          ECS04.setBounds(605, 110, 50, 28);

          //---- ECS05 ----
          ECS05.setComponentPopupMenu(popupMenu1);
          ECS05.setName("ECS05");
          panel1.add(ECS05);
          ECS05.setBounds(605, 135, 50, 28);

          //---- ECS06 ----
          ECS06.setComponentPopupMenu(popupMenu1);
          ECS06.setName("ECS06");
          panel1.add(ECS06);
          ECS06.setBounds(605, 160, 50, 28);

          //---- ECS07 ----
          ECS07.setComponentPopupMenu(popupMenu1);
          ECS07.setName("ECS07");
          panel1.add(ECS07);
          ECS07.setBounds(605, 185, 50, 28);

          //---- ECS08 ----
          ECS08.setComponentPopupMenu(popupMenu1);
          ECS08.setName("ECS08");
          panel1.add(ECS08);
          ECS08.setBounds(605, 210, 50, 28);

          //---- ECS09 ----
          ECS09.setComponentPopupMenu(popupMenu1);
          ECS09.setName("ECS09");
          panel1.add(ECS09);
          ECS09.setBounds(605, 235, 50, 28);

          //---- ECS10 ----
          ECS10.setComponentPopupMenu(popupMenu1);
          ECS10.setName("ECS10");
          panel1.add(ECS10);
          ECS10.setBounds(605, 260, 50, 28);

          //---- EAF01 ----
          EAF01.setComponentPopupMenu(popupMenu1);
          EAF01.setName("EAF01");
          panel1.add(EAF01);
          EAF01.setBounds(655, 35, 60, EAF01.getPreferredSize().height);

          //---- EAF02 ----
          EAF02.setComponentPopupMenu(popupMenu1);
          EAF02.setName("EAF02");
          panel1.add(EAF02);
          EAF02.setBounds(655, 60, 60, 28);

          //---- EAF03 ----
          EAF03.setComponentPopupMenu(popupMenu1);
          EAF03.setName("EAF03");
          panel1.add(EAF03);
          EAF03.setBounds(655, 85, 60, 28);

          //---- EAF04 ----
          EAF04.setComponentPopupMenu(popupMenu1);
          EAF04.setName("EAF04");
          panel1.add(EAF04);
          EAF04.setBounds(655, 110, 60, 28);

          //---- EAF05 ----
          EAF05.setComponentPopupMenu(popupMenu1);
          EAF05.setName("EAF05");
          panel1.add(EAF05);
          EAF05.setBounds(655, 135, 60, 28);

          //---- EAF06 ----
          EAF06.setComponentPopupMenu(popupMenu1);
          EAF06.setName("EAF06");
          panel1.add(EAF06);
          EAF06.setBounds(655, 160, 60, 28);

          //---- EAF07 ----
          EAF07.setComponentPopupMenu(popupMenu1);
          EAF07.setName("EAF07");
          panel1.add(EAF07);
          EAF07.setBounds(655, 185, 60, 28);

          //---- EAF08 ----
          EAF08.setComponentPopupMenu(popupMenu1);
          EAF08.setName("EAF08");
          panel1.add(EAF08);
          EAF08.setBounds(655, 210, 60, 28);

          //---- EAF09 ----
          EAF09.setComponentPopupMenu(popupMenu1);
          EAF09.setName("EAF09");
          panel1.add(EAF09);
          EAF09.setBounds(655, 235, 60, 28);

          //---- EAF10 ----
          EAF10.setComponentPopupMenu(popupMenu1);
          EAF10.setName("EAF10");
          panel1.add(EAF10);
          EAF10.setBounds(655, 260, 60, 28);

          //---- ECN01 ----
          ECN01.setComponentPopupMenu(popupMenu1);
          ECN01.setName("ECN01");
          panel1.add(ECN01);
          ECN01.setBounds(715, 35, 60, ECN01.getPreferredSize().height);

          //---- ECN02 ----
          ECN02.setComponentPopupMenu(popupMenu1);
          ECN02.setName("ECN02");
          panel1.add(ECN02);
          ECN02.setBounds(715, 60, 60, 28);

          //---- ECN03 ----
          ECN03.setComponentPopupMenu(popupMenu1);
          ECN03.setName("ECN03");
          panel1.add(ECN03);
          ECN03.setBounds(715, 85, 60, 28);

          //---- ECN04 ----
          ECN04.setComponentPopupMenu(popupMenu1);
          ECN04.setName("ECN04");
          panel1.add(ECN04);
          ECN04.setBounds(715, 110, 60, 28);

          //---- ECN05 ----
          ECN05.setComponentPopupMenu(popupMenu1);
          ECN05.setName("ECN05");
          panel1.add(ECN05);
          ECN05.setBounds(715, 135, 60, 28);

          //---- ECN06 ----
          ECN06.setComponentPopupMenu(popupMenu1);
          ECN06.setName("ECN06");
          panel1.add(ECN06);
          ECN06.setBounds(715, 160, 60, 28);

          //---- ECN07 ----
          ECN07.setComponentPopupMenu(popupMenu1);
          ECN07.setName("ECN07");
          panel1.add(ECN07);
          ECN07.setBounds(715, 185, 60, 28);

          //---- ECN08 ----
          ECN08.setComponentPopupMenu(popupMenu1);
          ECN08.setName("ECN08");
          panel1.add(ECN08);
          ECN08.setBounds(715, 210, 60, 28);

          //---- ECN09 ----
          ECN09.setComponentPopupMenu(popupMenu1);
          ECN09.setName("ECN09");
          panel1.add(ECN09);
          ECN09.setBounds(715, 235, 60, 28);

          //---- ECN10 ----
          ECN10.setComponentPopupMenu(popupMenu1);
          ECN10.setName("ECN10");
          panel1.add(ECN10);
          ECN10.setBounds(715, 260, 60, 28);

          //---- label1 ----
          label1.setText("N\u00b0 compte");
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(30, 12, 65, 21);

          //---- label2 ----
          label2.setText("Auxiliaire");
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(97, 12, 54, 21);

          //---- label3 ----
          label3.setText("S");
          label3.setName("label3");
          panel1.add(label3);
          label3.setBounds(155, 12, 24, 21);

          //---- label4 ----
          label4.setText("Montant");
          label4.setName("label4");
          panel1.add(label4);
          label4.setBounds(180, 12, 75, 21);

          //---- label5 ----
          label5.setText("N\u00b0 pi\u00e8ce");
          label5.setName("label5");
          panel1.add(label5);
          label5.setBounds(265, 12, 75, 21);

          //---- label6 ----
          label6.setText("Libell\u00e9");
          label6.setName("label6");
          panel1.add(label6);
          label6.setBounds(335, 12, 220, 21);

          //---- label7 ----
          label7.setText("SAN.");
          label7.setName("label7");
          panel1.add(label7);
          label7.setBounds(605, 12, 40, 21);

          //---- label8 ----
          label8.setText("Affaire");
          label8.setName("label8");
          panel1.add(label8);
          label8.setBounds(655, 12, 50, 21);

          //---- label9 ----
          label9.setText("Nature");
          label9.setName("label9");
          panel1.add(label9);
          label9.setBounds(715, 12, 50, 21);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        //---- OBJ_32 ----
        OBJ_32.setText("@WMTD@");
        OBJ_32.setHorizontalAlignment(SwingConstants.RIGHT);
        OBJ_32.setName("OBJ_32");

        //---- OBJ_34 ----
        OBJ_34.setText("@WMTC@");
        OBJ_34.setHorizontalAlignment(SwingConstants.RIGHT);
        OBJ_34.setName("OBJ_34");

        //---- OBJ_36 ----
        OBJ_36.setText("@WSOLD@");
        OBJ_36.setHorizontalAlignment(SwingConstants.RIGHT);
        OBJ_36.setName("OBJ_36");

        //---- OBJ_33 ----
        OBJ_33.setText("Total Cr\u00e9dit");
        OBJ_33.setName("OBJ_33");

        //---- OBJ_31 ----
        OBJ_31.setText("Total D\u00e9bit");
        OBJ_31.setName("OBJ_31");

        //---- OBJ_35 ----
        OBJ_35.setText("Solde");
        OBJ_35.setName("OBJ_35");

        //---- OBJ_37 ----
        OBJ_37.setText("@WSNS@");
        OBJ_37.setHorizontalAlignment(SwingConstants.RIGHT);
        OBJ_37.setName("OBJ_37");

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 790, GroupLayout.PREFERRED_SIZE))
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(40, 40, 40)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(75, 75, 75)
                  .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE))
                .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE))
              .addGap(25, 25, 25)
              .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
              .addGap(3, 3, 3)
              .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
              .addGap(70, 70, 70)
              .addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
              .addGap(5, 5, 5)
              .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
              .addGap(10, 10, 10)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(2, 2, 2)
                  .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(2, 2, 2)
                  .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(2, 2, 2)
                  .addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== popupMenu1 ========
    {
      popupMenu1.setName("popupMenu1");

      //---- menuItem1 ----
      menuItem1.setText("Choix possibles");
      menuItem1.setName("menuItem1");
      menuItem1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem1ActionPerformed(e);
        }
      });
      popupMenu1.add(menuItem1);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField ECG01;
  private XRiTextField ECG02;
  private XRiTextField ECG03;
  private XRiTextField ECG04;
  private XRiTextField ECG05;
  private XRiTextField ECG06;
  private XRiTextField ECG07;
  private XRiTextField ECG08;
  private XRiTextField ECG09;
  private XRiTextField ECG10;
  private XRiTextField ECA01;
  private XRiTextField ECA02;
  private XRiTextField ECA03;
  private XRiTextField ECA04;
  private XRiTextField ECA05;
  private XRiTextField ECA06;
  private XRiTextField ECA07;
  private XRiTextField ECA08;
  private XRiTextField ECA09;
  private XRiTextField ECA10;
  private XRiTextField EDC01;
  private XRiTextField EDC02;
  private XRiTextField EDC03;
  private XRiTextField EDC04;
  private XRiTextField EDC05;
  private XRiTextField EDC06;
  private XRiTextField EDC07;
  private XRiTextField EDC08;
  private XRiTextField EDC09;
  private XRiTextField EDC10;
  private XRiTextField ECM01;
  private XRiTextField ECM02;
  private XRiTextField ECM03;
  private XRiTextField ECM04;
  private XRiTextField ECM05;
  private XRiTextField ECM06;
  private XRiTextField ECM07;
  private XRiTextField ECM08;
  private XRiTextField ECM09;
  private XRiTextField ECM10;
  private XRiTextField ECP01;
  private XRiTextField ECP02;
  private XRiTextField ECP03;
  private XRiTextField ECP04;
  private XRiTextField ECP05;
  private XRiTextField ECP06;
  private XRiTextField ECP07;
  private XRiTextField ECP08;
  private XRiTextField ECP09;
  private XRiTextField ECP10;
  private XRiTextField LIB01;
  private XRiTextField LIB02;
  private XRiTextField LIB03;
  private XRiTextField LIB04;
  private XRiTextField LIB05;
  private XRiTextField LIB06;
  private XRiTextField LIB07;
  private XRiTextField LIB08;
  private XRiTextField LIB09;
  private XRiTextField LIB10;
  private XRiTextField ECS01;
  private XRiTextField ECS02;
  private XRiTextField ECS03;
  private XRiTextField ECS04;
  private XRiTextField ECS05;
  private XRiTextField ECS06;
  private XRiTextField ECS07;
  private XRiTextField ECS08;
  private XRiTextField ECS09;
  private XRiTextField ECS10;
  private XRiTextField EAF01;
  private XRiTextField EAF02;
  private XRiTextField EAF03;
  private XRiTextField EAF04;
  private XRiTextField EAF05;
  private XRiTextField EAF06;
  private XRiTextField EAF07;
  private XRiTextField EAF08;
  private XRiTextField EAF09;
  private XRiTextField EAF10;
  private XRiTextField ECN01;
  private XRiTextField ECN02;
  private XRiTextField ECN03;
  private XRiTextField ECN04;
  private XRiTextField ECN05;
  private XRiTextField ECN06;
  private XRiTextField ECN07;
  private XRiTextField ECN08;
  private XRiTextField ECN09;
  private XRiTextField ECN10;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private RiZoneSortie OBJ_32;
  private RiZoneSortie OBJ_34;
  private RiZoneSortie OBJ_36;
  private JLabel OBJ_33;
  private JLabel OBJ_31;
  private JLabel OBJ_35;
  private RiZoneSortie OBJ_37;
  private JPopupMenu popupMenu1;
  private JMenuItem menuItem1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
