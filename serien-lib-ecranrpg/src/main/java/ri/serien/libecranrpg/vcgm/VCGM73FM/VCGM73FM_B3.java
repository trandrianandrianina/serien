
package ri.serien.libecranrpg.vcgm.VCGM73FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VCGM73FM_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGM73FM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAXE1@")).trim());
    OBJ_25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAXE2@")).trim());
    OBJ_26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAXE3@")).trim());
    OBJ_27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAXE4@")).trim());
    OBJ_28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAXE5@")).trim());
    OBJ_29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAXE6@")).trim());
    LIBA1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBA1@")).trim());
    LIBA2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBA2@")).trim());
    LIBA3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBA3@")).trim());
    LIBA4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBA4@")).trim());
    LIBA5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBA5@")).trim());
    LIBA6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBA6@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Visualisation et lettrage"));
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData(BTD.getInvoker().getName(), 0, "*");
    lexique.HostScreenSendKey(this, "Enter", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_24 = new JLabel();
    L1AA1 = new XRiTextField();
    OBJ_25 = new JLabel();
    OBJ_26 = new JLabel();
    OBJ_27 = new JLabel();
    OBJ_28 = new JLabel();
    OBJ_29 = new JLabel();
    L1AA2 = new XRiTextField();
    L1AA3 = new XRiTextField();
    L1AA4 = new XRiTextField();
    L1AA5 = new XRiTextField();
    L1AA6 = new XRiTextField();
    LIBA1 = new RiZoneSortie();
    LIBA2 = new RiZoneSortie();
    LIBA3 = new RiZoneSortie();
    LIBA4 = new RiZoneSortie();
    LIBA5 = new RiZoneSortie();
    LIBA6 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();
    separator1 = new JSeparator();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(700, 270));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_24 ----
          OBJ_24.setText("@LAXE1@");
          OBJ_24.setName("OBJ_24");
          panel1.add(OBJ_24);
          OBJ_24.setBounds(15, 17, 60, 25);

          //---- L1AA1 ----
          L1AA1.setComponentPopupMenu(BTD);
          L1AA1.setName("L1AA1");
          panel1.add(L1AA1);
          L1AA1.setBounds(100, 15, 60, L1AA1.getPreferredSize().height);

          //---- OBJ_25 ----
          OBJ_25.setText("@LAXE2@");
          OBJ_25.setName("OBJ_25");
          panel1.add(OBJ_25);
          OBJ_25.setBounds(15, 54, 60, 25);

          //---- OBJ_26 ----
          OBJ_26.setText("@LAXE3@");
          OBJ_26.setName("OBJ_26");
          panel1.add(OBJ_26);
          OBJ_26.setBounds(15, 91, 60, 25);

          //---- OBJ_27 ----
          OBJ_27.setText("@LAXE4@");
          OBJ_27.setName("OBJ_27");
          panel1.add(OBJ_27);
          OBJ_27.setBounds(15, 128, 60, 25);

          //---- OBJ_28 ----
          OBJ_28.setText("@LAXE5@");
          OBJ_28.setName("OBJ_28");
          panel1.add(OBJ_28);
          OBJ_28.setBounds(15, 165, 60, 25);

          //---- OBJ_29 ----
          OBJ_29.setText("@LAXE6@");
          OBJ_29.setName("OBJ_29");
          panel1.add(OBJ_29);
          OBJ_29.setBounds(15, 202, 60, 25);

          //---- L1AA2 ----
          L1AA2.setComponentPopupMenu(BTD);
          L1AA2.setName("L1AA2");
          panel1.add(L1AA2);
          L1AA2.setBounds(100, 52, 60, L1AA2.getPreferredSize().height);

          //---- L1AA3 ----
          L1AA3.setComponentPopupMenu(BTD);
          L1AA3.setName("L1AA3");
          panel1.add(L1AA3);
          L1AA3.setBounds(100, 89, 60, L1AA3.getPreferredSize().height);

          //---- L1AA4 ----
          L1AA4.setComponentPopupMenu(BTD);
          L1AA4.setName("L1AA4");
          panel1.add(L1AA4);
          L1AA4.setBounds(100, 126, 60, L1AA4.getPreferredSize().height);

          //---- L1AA5 ----
          L1AA5.setComponentPopupMenu(BTD);
          L1AA5.setName("L1AA5");
          panel1.add(L1AA5);
          L1AA5.setBounds(100, 163, 60, L1AA5.getPreferredSize().height);

          //---- L1AA6 ----
          L1AA6.setComponentPopupMenu(BTD);
          L1AA6.setName("L1AA6");
          panel1.add(L1AA6);
          L1AA6.setBounds(100, 200, 60, L1AA6.getPreferredSize().height);

          //---- LIBA1 ----
          LIBA1.setText("@LIBA1@");
          LIBA1.setName("LIBA1");
          panel1.add(LIBA1);
          LIBA1.setBounds(170, 17, 310, LIBA1.getPreferredSize().height);

          //---- LIBA2 ----
          LIBA2.setText("@LIBA2@");
          LIBA2.setName("LIBA2");
          panel1.add(LIBA2);
          LIBA2.setBounds(170, 54, 310, LIBA2.getPreferredSize().height);

          //---- LIBA3 ----
          LIBA3.setText("@LIBA3@");
          LIBA3.setName("LIBA3");
          panel1.add(LIBA3);
          LIBA3.setBounds(170, 91, 310, LIBA3.getPreferredSize().height);

          //---- LIBA4 ----
          LIBA4.setText("@LIBA4@");
          LIBA4.setName("LIBA4");
          panel1.add(LIBA4);
          LIBA4.setBounds(170, 128, 310, LIBA4.getPreferredSize().height);

          //---- LIBA5 ----
          LIBA5.setText("@LIBA5@");
          LIBA5.setName("LIBA5");
          panel1.add(LIBA5);
          LIBA5.setBounds(170, 165, 310, LIBA5.getPreferredSize().height);

          //---- LIBA6 ----
          LIBA6.setText("@LIBA6@");
          LIBA6.setName("LIBA6");
          panel1.add(LIBA6);
          LIBA6.setBounds(170, 202, 310, LIBA6.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 510, 250);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Ventilation sur axe");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);

      //---- separator1 ----
      separator1.setName("separator1");
      BTD.add(separator1);

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_24;
  private XRiTextField L1AA1;
  private JLabel OBJ_25;
  private JLabel OBJ_26;
  private JLabel OBJ_27;
  private JLabel OBJ_28;
  private JLabel OBJ_29;
  private XRiTextField L1AA2;
  private XRiTextField L1AA3;
  private XRiTextField L1AA4;
  private XRiTextField L1AA5;
  private XRiTextField L1AA6;
  private RiZoneSortie LIBA1;
  private RiZoneSortie LIBA2;
  private RiZoneSortie LIBA3;
  private RiZoneSortie LIBA4;
  private RiZoneSortie LIBA5;
  private RiZoneSortie LIBA6;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  private JSeparator separator1;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
