
package ri.serien.libecranrpg.vcgm.VCGM60FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM60FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _LD011_Title = { "Rubriques", "PER01", "PER02", "PER03", "PER04", "PER05", "PER06", };
  private String[][] _LD011_Data = { { "LD011", "LD012", "LD013", "LD014", "LD015", "LD016", "LD017", },
      { "LD021", "LD022", "LD023", "LD024", "LD025", "LD026", "LD027", },
      { "LD031", "LD032", "LD033", "LD034", "LD035", "LD036", "LD037", },
      { "LD041", "LD042", "LD043", "LD044", "LD045", "LD046", "LD047", },
      { "LD051", "LD052", "LD053", "LD054", "LD055", "LD056", "LD057", },
      { "LD061", "LD062", "LD063", "LD064", "LD065", "LD066", "LD067", },
      { "LD071", "LD072", "LD073", "LD074", "LD075", "LD076", "LD077", },
      { "LD081", "LD082", "LD083", "LD084", "LD085", "LD086", "LD087", },
      { "LD091", "LD092", "LD093", "LD094", "LD095", "LD096", "LD097", },
      { "LD101", "LD102", "LD103", "LD104", "LD105", "LD106", "LD107", },
      { "LD111", "LD112", "LD113", "LD114", "LD115", "LD116", "LD117", },
      { "LD121", "LD122", "LD123", "LD124", "LD125", "LD126", "LD127", },
      { "LD131", "LD132", "LD133", "LD134", "LD135", "LD136", "LD137", },
      { "LD141", "LD142", "LD143", "LD144", "LD145", "LD146", "LD147", }, };
  private int[] _LD011_Width = { 159, 75, 75, 75, 75, 75, 75, };
  // private String[] _LIST_Top= null;
  private int[] _LIST_Justification = { SwingConstants.LEFT, SwingConstants.RIGHT, SwingConstants.RIGHT, SwingConstants.RIGHT,
      SwingConstants.RIGHT, SwingConstants.RIGHT, SwingConstants.RIGHT };
  
  public VCGM60FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    LD011.setAspectTable(null, _LD011_Title, _LD011_Data, _LD011_Width, false, _LIST_Justification, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIBDV@")).trim());
    OBJ_62.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT02@")).trim());
    OBJ_63.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT03@")).trim());
    OBJ_64.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT04@")).trim());
    OBJ_65.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT05@")).trim());
    OBJ_66.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT06@")).trim());
    OBJ_67.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT07@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // Ajoute à la liste des oData les variables non liée directement à un composant graphique
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _LIST_Top, _LIST_Justification);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    // WI.setSelectedItem(lexique.HostFieldGetData("WI"));
    
    // TODO Icones
    OBJ_47.setIcon(lexique.chargerImage("images/fleche_sui.png", true));
    OBJ_45.setIcon(lexique.chargerImage("images/fleche_pre.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
    
    

    
    p_bpresentation.setCodeEtablissement(WSOC.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WSOC.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("WI", 0, (String)WI.getSelectedItem());
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F13");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_45ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8");
  }
  
  private void OBJ_47ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_40 = new JLabel();
    WSOC = new XRiTextField();
    OBJ_27 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_45 = new JButton();
    WI = new XRiComboBox();
    OBJ_47 = new JButton();
    xTitledPanel1 = new JXTitledPanel();
    SCROLLPANE_LIST = new JScrollPane();
    LD011 = new XRiTable();
    OBJ_75 = new JLabel();
    OBJ_62 = new JLabel();
    OBJ_63 = new JLabel();
    OBJ_64 = new JLabel();
    OBJ_65 = new JLabel();
    OBJ_66 = new JLabel();
    OBJ_67 = new JLabel();
    OBJ_74 = new JLabel();
    LD152 = new XRiTextField();
    LD153 = new XRiTextField();
    LD154 = new XRiTextField();
    LD155 = new XRiTextField();
    LD156 = new XRiTextField();
    LD157 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_14 = new JMenuItem();
    OBJ_13 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Tableau \u00e9ch\u00e9ancier");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_40 ----
          OBJ_40.setText("Soci\u00e9t\u00e9");
          OBJ_40.setName("OBJ_40");

          //---- WSOC ----
          WSOC.setName("WSOC");

          //---- OBJ_27 ----
          OBJ_27.setText("@WLIBDV@");
          OBJ_27.setForeground(Color.black);
          OBJ_27.setOpaque(false);
          OBJ_27.setName("OBJ_27");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(WSOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_40))
              .addComponent(WSOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("R\u00e9g\u00e9n\u00e9ration");
              riSousMenu_bt6.setToolTipText("R\u00e9g\u00e9n\u00e9ration");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(700, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setTitle("Tranche");
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();

            //---- OBJ_45 ----
            OBJ_45.setText("");
            OBJ_45.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_45.setToolTipText("p\u00e9riode pr\u00e9c\u00e9dente");
            OBJ_45.setName("OBJ_45");
            OBJ_45.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_45ActionPerformed(e);
              }
            });

            //---- WI ----
            WI.setModel(new DefaultComboBoxModel(new String[] {
              "1",
              "5",
              "10",
              "15",
              "30"
            }));
            WI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WI.setName("WI");

            //---- OBJ_47 ----
            OBJ_47.setText("");
            OBJ_47.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_47.setToolTipText("p\u00e9riode suivante");
            OBJ_47.setName("OBJ_47");
            OBJ_47.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_47ActionPerformed(e);
              }
            });

            GroupLayout xTitledPanel2ContentContainerLayout = new GroupLayout(xTitledPanel2ContentContainer);
            xTitledPanel2ContentContainer.setLayout(xTitledPanel2ContentContainerLayout);
            xTitledPanel2ContentContainerLayout.setHorizontalGroup(
              xTitledPanel2ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(104, 104, 104)
                  .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 177, GroupLayout.PREFERRED_SIZE)
                  .addGap(10, 10, 10)
                  .addComponent(WI, GroupLayout.PREFERRED_SIZE, 53, GroupLayout.PREFERRED_SIZE)
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 177, GroupLayout.PREFERRED_SIZE)
                  .addContainerGap(137, Short.MAX_VALUE))
            );
            xTitledPanel2ContentContainerLayout.setVerticalGroup(
              xTitledPanel2ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(12, 12, 12)
                  .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(11, 11, 11)
                  .addComponent(WI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(11, 11, 11)
                  .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
            );
          }

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setTitle("Ech\u00e9ancier");
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

            //======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

              //---- LD011 ----
              LD011.setShowVerticalLines(true);
              LD011.setName("LD011");
              SCROLLPANE_LIST.setViewportView(LD011);
            }

            //---- OBJ_75 ----
            OBJ_75.setText("Total \u00e9ch\u00e9ances");
            OBJ_75.setFont(OBJ_75.getFont().deriveFont(OBJ_75.getFont().getStyle() | Font.BOLD));
            OBJ_75.setName("OBJ_75");

            //---- OBJ_62 ----
            OBJ_62.setText("@TOT02@");
            OBJ_62.setFont(new Font("Courier New", Font.PLAIN, 12));
            OBJ_62.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_62.setName("OBJ_62");

            //---- OBJ_63 ----
            OBJ_63.setText("@TOT03@");
            OBJ_63.setFont(new Font("Courier New", Font.PLAIN, 12));
            OBJ_63.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_63.setName("OBJ_63");

            //---- OBJ_64 ----
            OBJ_64.setText("@TOT04@");
            OBJ_64.setFont(new Font("Courier New", Font.PLAIN, 12));
            OBJ_64.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_64.setName("OBJ_64");

            //---- OBJ_65 ----
            OBJ_65.setText("@TOT05@");
            OBJ_65.setFont(new Font("Courier New", Font.PLAIN, 12));
            OBJ_65.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_65.setName("OBJ_65");

            //---- OBJ_66 ----
            OBJ_66.setText("@TOT06@");
            OBJ_66.setFont(new Font("Courier New", Font.PLAIN, 12));
            OBJ_66.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_66.setName("OBJ_66");

            //---- OBJ_67 ----
            OBJ_67.setText("@TOT07@");
            OBJ_67.setFont(new Font("Courier New", Font.PLAIN, 12));
            OBJ_67.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_67.setName("OBJ_67");

            //---- OBJ_74 ----
            OBJ_74.setText("POSITION");
            OBJ_74.setFont(OBJ_74.getFont().deriveFont(OBJ_74.getFont().getStyle() | Font.BOLD));
            OBJ_74.setName("OBJ_74");

            //---- LD152 ----
            LD152.setHorizontalAlignment(SwingConstants.RIGHT);
            LD152.setName("LD152");

            //---- LD153 ----
            LD153.setHorizontalAlignment(SwingConstants.RIGHT);
            LD153.setName("LD153");

            //---- LD154 ----
            LD154.setHorizontalAlignment(SwingConstants.RIGHT);
            LD154.setName("LD154");

            //---- LD155 ----
            LD155.setHorizontalAlignment(SwingConstants.RIGHT);
            LD155.setName("LD155");

            //---- LD156 ----
            LD156.setHorizontalAlignment(SwingConstants.RIGHT);
            LD156.setName("LD156");

            //---- LD157 ----
            LD157.setHorizontalAlignment(SwingConstants.RIGHT);
            LD157.setName("LD157");

            GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
            xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
            xTitledPanel1ContentContainerLayout.setHorizontalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 630, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_75, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_65, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_66, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_74, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
                      .addGap(87, 87, 87)
                      .addComponent(LD152, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
                      .addGap(6, 6, 6)
                      .addComponent(LD153, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
                      .addGap(6, 6, 6)
                      .addComponent(LD154, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
                      .addGap(6, 6, 6)
                      .addComponent(LD155, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
                      .addGap(6, 6, 6)
                      .addComponent(LD156, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
                      .addGap(6, 6, 6)
                      .addComponent(LD157, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE))))
            );
            xTitledPanel1ContentContainerLayout.setVerticalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                  .addGap(10, 10, 10)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_75)
                    .addComponent(OBJ_62)
                    .addComponent(OBJ_63)
                    .addComponent(OBJ_64)
                    .addComponent(OBJ_65)
                    .addComponent(OBJ_66)
                    .addComponent(OBJ_67))
                  .addGap(9, 9, 9)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_74))
                    .addComponent(LD152, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD153, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD154, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD155, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD156, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD157, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                  .addComponent(xTitledPanel2, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(xTitledPanel1, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(56, 56, 56))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_14 ----
      OBJ_14.setText("Choix possibles");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);

      //---- OBJ_13 ----
      OBJ_13.setText("Aide en ligne");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_40;
  private XRiTextField WSOC;
  private RiZoneSortie OBJ_27;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel2;
  private JButton OBJ_45;
  private XRiComboBox WI;
  private JButton OBJ_47;
  private JXTitledPanel xTitledPanel1;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable LD011;
  private JLabel OBJ_75;
  private JLabel OBJ_62;
  private JLabel OBJ_63;
  private JLabel OBJ_64;
  private JLabel OBJ_65;
  private JLabel OBJ_66;
  private JLabel OBJ_67;
  private JLabel OBJ_74;
  private XRiTextField LD152;
  private XRiTextField LD153;
  private XRiTextField LD154;
  private XRiTextField LD155;
  private XRiTextField LD156;
  private XRiTextField LD157;
  private JPopupMenu BTD;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_13;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
