
package ri.serien.libecranrpg.vcgm.VCGM25FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM25FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] _MOD1_Top = { "MOD1", "MOD2", "MOD3", "MOD4", "MOD5", "MOD6", "MOD7", "MOD8", "MOD9", "MOD10", "MOD11", "MOD12",
      "MOD13", "MOD14", "MOD15", "MOD16", "MOD17", };
  private String[] _MOD1_Title = { "", };
  private String[][] _MOD1_Data =
      { { "TIT1", }, { "TIT2", }, { "TIT3", }, { "TIT4", }, { "TIT5", }, { "TIT6", }, { "TIT7", }, { "TIT8", }, { "TIT9", }, { "TIT10", },
          { "TIT11", }, { "TIT12", }, { "TIT13", }, { "TIT14", }, { "TIT15", }, { "TIT16", }, { "TIT17", }, };
  private int[] _MOD1_Width = { 273, };
  
  public VCGM25FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    EDTFAX.setValeursSelection("OUI", "NON");
    RELEDT.setValeursSelection("OUI", "NON");
    REPFCH.setValeursSelection("OUI", "NON");
    RELCDP.setValeursSelection("OUI", "NON");
    RELALP.setValeursSelection("OUI", "NON");
    RELTRI.setValeursSelection("OUI", "NON");
    RELECH.setValeursSelection("OUI", "NON");
    RELCRE.setValeursSelection("OUI", "NON");
    RELBLC.setValeursSelection("OUI", "NON");
    RELNPR.setValeursSelection("OUI", "NON");
    MOD1.setAspectTable(_MOD1_Top, _MOD1_Title, _MOD1_Data, _MOD1_Width, false, null, null, null, null);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RELTIT@")).trim());
    panel2.setBorder(
        new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("Critères de relance pour lettre @LIBLET@")).trim()));
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @RELTIT@"));
    
    

    
    p_bpresentation.setCodeEtablissement(RELSOC.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(RELSOC.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void MOD1MouseClicked(MouseEvent e) {
    MOD1.setValeurTop("X");
    if (MOD1.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    try {
      lexique.HostScreenSendKey(this, "F10");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_24 = new JLabel();
    RELSOC = new XRiTextField();
    OBJ_25 = new JLabel();
    RELCOL = new XRiTextField();
    OBJ_26 = new JLabel();
    RELDEB = new XRiTextField();
    OBJ_27 = new JLabel();
    RELFIN = new XRiTextField();
    OBJ_28 = new JLabel();
    RELLIM = new XRiCalendrier();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_75 = new JLabel();
    OBJ_76 = new JLabel();
    OBJ_18 = new JLabel();
    RELLET = new XRiTextField();
    RELRGL = new XRiTextField();
    RELMTT = new XRiTextField();
    RELCRE = new XRiCheckBox();
    RELECH = new XRiCheckBox();
    RELTRI = new XRiCheckBox();
    OBJ_80 = new JLabel();
    OBJ_81 = new JLabel();
    OBJ_82 = new JLabel();
    OBJ_83 = new JLabel();
    RELDEV = new XRiTextField();
    RELREP = new XRiTextField();
    RELCRI = new XRiTextField();
    RELCLA = new XRiTextField();
    RELALP = new XRiCheckBox();
    RELCDP = new XRiCheckBox();
    REPFCH = new XRiCheckBox();
    RELEDT = new XRiCheckBox();
    OBJ_77 = new JLabel();
    PCEDEB = new XRiTextField();
    OBJ_89 = new JLabel();
    PCEFIN = new XRiTextField();
    EDTFAX = new XRiCheckBox();
    RELBLC = new XRiCheckBox();
    RELNPR = new XRiCheckBox();
    panel2 = new JPanel();
    SCROLLPANE_MOD1 = new JScrollPane();
    MOD1 = new XRiTable();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("@RELTIT@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- OBJ_24 ----
          OBJ_24.setText("Soci\u00e9t\u00e9");
          OBJ_24.setName("OBJ_24");
          
          // ---- RELSOC ----
          RELSOC.setComponentPopupMenu(BTD);
          RELSOC.setName("RELSOC");
          
          // ---- OBJ_25 ----
          OBJ_25.setText("Collectif");
          OBJ_25.setName("OBJ_25");
          
          // ---- RELCOL ----
          RELCOL.setComponentPopupMenu(BTD);
          RELCOL.setName("RELCOL");
          
          // ---- OBJ_26 ----
          OBJ_26.setText("Auxiliaire de d\u00e9but");
          OBJ_26.setName("OBJ_26");
          
          // ---- RELDEB ----
          RELDEB.setComponentPopupMenu(BTD);
          RELDEB.setName("RELDEB");
          
          // ---- OBJ_27 ----
          OBJ_27.setText("Auxiliaire de fin");
          OBJ_27.setName("OBJ_27");
          
          // ---- RELFIN ----
          RELFIN.setComponentPopupMenu(BTD);
          RELFIN.setName("RELFIN");
          
          // ---- OBJ_28 ----
          OBJ_28.setText("Date limite");
          OBJ_28.setName("OBJ_28");
          
          // ---- RELLIM ----
          RELLIM.setComponentPopupMenu(BTD);
          RELLIM.setName("RELLIM");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5)
                  .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE).addGap(23, 23, 23)
                  .addComponent(RELSOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(30, 30, 30)
                  .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(RELCOL, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(30, 30, 30)
                  .addComponent(OBJ_26, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(RELDEB, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(20, 20, 20)
                  .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(RELFIN, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(20, 20, 20)
                  .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(RELLIM, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addComponent(RELSOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(RELCOL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(RELDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(RELFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(RELLIM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(6, 6, 6).addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_24).addComponent(OBJ_25).addComponent(OBJ_26).addComponent(OBJ_27).addComponent(OBJ_28))));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1000, 450));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          
          // ======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Options"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);
            
            // ---- OBJ_75 ----
            OBJ_75.setText("Type de lettre \u00e0 \u00e9diter");
            OBJ_75.setName("OBJ_75");
            panel1.add(OBJ_75);
            OBJ_75.setBounds(25, 35, 260, 22);
            
            // ---- OBJ_76 ----
            OBJ_76.setText("R\u00e8glement \u00e0 relancer");
            OBJ_76.setName("OBJ_76");
            panel1.add(OBJ_76);
            OBJ_76.setBounds(25, 65, 260, 22);
            
            // ---- OBJ_18 ----
            OBJ_18.setText("Minimum \u00e0 relancer");
            OBJ_18.setName("OBJ_18");
            panel1.add(OBJ_18);
            OBJ_18.setBounds(25, 95, 260, 22);
            
            // ---- RELLET ----
            RELLET.setComponentPopupMenu(BTD);
            RELLET.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RELLET.setName("RELLET");
            panel1.add(RELLET);
            RELLET.setBounds(290, 32, 30, RELLET.getPreferredSize().height);
            
            // ---- RELRGL ----
            RELRGL.setComponentPopupMenu(BTD);
            RELRGL.setName("RELRGL");
            panel1.add(RELRGL);
            RELRGL.setBounds(290, 62, 30, RELRGL.getPreferredSize().height);
            
            // ---- RELMTT ----
            RELMTT.setComponentPopupMenu(BTD);
            RELMTT.setName("RELMTT");
            panel1.add(RELMTT);
            RELMTT.setBounds(290, 92, 88, RELMTT.getPreferredSize().height);
            
            // ---- RELCRE ----
            RELCRE.setText("Cr\u00e9dits non imput\u00e9s");
            RELCRE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RELCRE.setName("RELCRE");
            panel1.add(RELCRE);
            RELCRE.setBounds(25, 277, 175, 20);
            
            // ---- RELECH ----
            RELECH.setText("Uniquement factures non \u00e9chues");
            RELECH.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RELECH.setName("RELECH");
            panel1.add(RELECH);
            RELECH.setBounds(210, 277, 275, 20);
            
            // ---- RELTRI ----
            RELTRI.setText("Tri par repr\u00e9sentant");
            RELTRI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RELTRI.setName("RELTRI");
            panel1.add(RELTRI);
            RELTRI.setBounds(25, 303, 175, 20);
            
            // ---- OBJ_80 ----
            OBJ_80.setText("S\u00e9lection par r\u00e9f\u00e9rence de classement");
            OBJ_80.setName("OBJ_80");
            panel1.add(OBJ_80);
            OBJ_80.setBounds(25, 125, 260, 22);
            
            // ---- OBJ_81 ----
            OBJ_81.setText("Crit\u00e8re de s\u00e9lection num\u00e9ro 1");
            OBJ_81.setName("OBJ_81");
            panel1.add(OBJ_81);
            OBJ_81.setBounds(25, 155, 260, 22);
            
            // ---- OBJ_82 ----
            OBJ_82.setText("Code repr\u00e9sentant");
            OBJ_82.setName("OBJ_82");
            panel1.add(OBJ_82);
            OBJ_82.setBounds(25, 185, 260, 22);
            
            // ---- OBJ_83 ----
            OBJ_83.setText("Comptes dont le code devise est");
            OBJ_83.setName("OBJ_83");
            panel1.add(OBJ_83);
            OBJ_83.setBounds(25, 215, 260, 22);
            
            // ---- RELDEV ----
            RELDEV.setComponentPopupMenu(BTD);
            RELDEV.setName("RELDEV");
            panel1.add(RELDEV);
            RELDEV.setBounds(290, 212, 36, RELDEV.getPreferredSize().height);
            
            // ---- RELREP ----
            RELREP.setComponentPopupMenu(BTD);
            RELREP.setName("RELREP");
            panel1.add(RELREP);
            RELREP.setBounds(290, 182, 30, RELREP.getPreferredSize().height);
            
            // ---- RELCRI ----
            RELCRI.setComponentPopupMenu(BTD);
            RELCRI.setName("RELCRI");
            panel1.add(RELCRI);
            RELCRI.setBounds(290, 152, 70, RELCRI.getPreferredSize().height);
            
            // ---- RELCLA ----
            RELCLA.setComponentPopupMenu(BTD);
            RELCLA.setName("RELCLA");
            panel1.add(RELCLA);
            RELCLA.setBounds(290, 122, 110, RELCLA.getPreferredSize().height);
            
            // ---- RELALP ----
            RELALP.setText("Tri alphab\u00e9tique");
            RELALP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RELALP.setName("RELALP");
            panel1.add(RELALP);
            RELALP.setBounds(25, 355, 175, 20);
            
            // ---- RELCDP ----
            RELCDP.setText("Tri par code postal");
            RELCDP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RELCDP.setName("RELCDP");
            panel1.add(RELCDP);
            RELCDP.setBounds(25, 329, 175, 20);
            
            // ---- REPFCH ----
            REPFCH.setText("G\u00e9n\u00e9ration du fichier de relance");
            REPFCH.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REPFCH.setName("REPFCH");
            panel1.add(REPFCH);
            REPFCH.setBounds(210, 329, 275, 20);
            
            // ---- RELEDT ----
            RELEDT.setText("Justificatif factures non relanc\u00e9es");
            RELEDT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RELEDT.setName("RELEDT");
            panel1.add(RELEDT);
            RELEDT.setBounds(210, 303, 275, 20);
            
            // ---- OBJ_77 ----
            OBJ_77.setText("Num\u00e9ro(s) de pi\u00e8ce");
            OBJ_77.setName("OBJ_77");
            panel1.add(OBJ_77);
            OBJ_77.setBounds(25, 245, 260, 22);
            
            // ---- PCEDEB ----
            PCEDEB.setComponentPopupMenu(BTD);
            PCEDEB.setHorizontalAlignment(SwingConstants.CENTER);
            PCEDEB.setName("PCEDEB");
            panel1.add(PCEDEB);
            PCEDEB.setBounds(290, 242, 68, PCEDEB.getPreferredSize().height);
            
            // ---- OBJ_89 ----
            OBJ_89.setText("/");
            OBJ_89.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_89.setName("OBJ_89");
            panel1.add(OBJ_89);
            OBJ_89.setBounds(360, 242, 13, 28);
            
            // ---- PCEFIN ----
            PCEFIN.setComponentPopupMenu(BTD);
            PCEFIN.setName("PCEFIN");
            panel1.add(PCEFIN);
            PCEFIN.setBounds(375, 242, 68, PCEFIN.getPreferredSize().height);
            
            // ---- EDTFAX ----
            EDTFAX.setText("Envoi par t\u00e9l\u00e9copie ou par email");
            EDTFAX.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            EDTFAX.setName("EDTFAX");
            panel1.add(EDTFAX);
            EDTFAX.setBounds(210, 380, 275, 20);
            
            // ---- RELBLC ----
            RELBLC.setText("Edition du bloc-notes");
            RELBLC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RELBLC.setName("RELBLC");
            panel1.add(RELBLC);
            RELBLC.setBounds(25, 380, 175, 20);
            
            // ---- RELNPR ----
            RELNPR.setText("Edition des clients \u00e0 ne pas relancer");
            RELNPR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RELNPR.setName("RELNPR");
            panel1.add(RELNPR);
            RELNPR.setBounds(210, 355, 275, 20);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          
          // ======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Crit\u00e8res de relance pour lettre @LIBLET@"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);
            
            // ======== SCROLLPANE_MOD1 ========
            {
              SCROLLPANE_MOD1.setToolTipText("<HTML>Cr\u00e9ation ou modification<BR>d'un crit\u00e8re de relance</HTML>");
              SCROLLPANE_MOD1.setName("SCROLLPANE_MOD1");
              
              // ---- MOD1 ----
              MOD1.setToolTipText("Double clic pour modifier");
              MOD1.setName("MOD1");
              MOD1.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  MOD1MouseClicked(e);
                }
              });
              SCROLLPANE_MOD1.setViewportView(MOD1);
            }
            panel2.add(SCROLLPANE_MOD1);
            SCROLLPANE_MOD1.setBounds(15, 60, 315, 285);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup().addGroup(GroupLayout.Alignment.TRAILING,
              p_contenuLayout.createSequentialGroup().addContainerGap()
                  .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 629, Short.MAX_VALUE)
                  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 345, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)));
          p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup().addContainerGap()
                  .addGroup(p_contenuLayout.createParallelGroup().addComponent(panel2, GroupLayout.DEFAULT_SIZE, 422, Short.MAX_VALUE)
                      .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 422, Short.MAX_VALUE))
                  .addContainerGap()));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
      
      // ---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_24;
  private XRiTextField RELSOC;
  private JLabel OBJ_25;
  private XRiTextField RELCOL;
  private JLabel OBJ_26;
  private XRiTextField RELDEB;
  private JLabel OBJ_27;
  private XRiTextField RELFIN;
  private JLabel OBJ_28;
  private XRiCalendrier RELLIM;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_75;
  private JLabel OBJ_76;
  private JLabel OBJ_18;
  private XRiTextField RELLET;
  private XRiTextField RELRGL;
  private XRiTextField RELMTT;
  private XRiCheckBox RELCRE;
  private XRiCheckBox RELECH;
  private XRiCheckBox RELTRI;
  private JLabel OBJ_80;
  private JLabel OBJ_81;
  private JLabel OBJ_82;
  private JLabel OBJ_83;
  private XRiTextField RELDEV;
  private XRiTextField RELREP;
  private XRiTextField RELCRI;
  private XRiTextField RELCLA;
  private XRiCheckBox RELALP;
  private XRiCheckBox RELCDP;
  private XRiCheckBox REPFCH;
  private XRiCheckBox RELEDT;
  private JLabel OBJ_77;
  private XRiTextField PCEDEB;
  private JLabel OBJ_89;
  private XRiTextField PCEFIN;
  private XRiCheckBox EDTFAX;
  private XRiCheckBox RELBLC;
  private XRiCheckBox RELNPR;
  private JPanel panel2;
  private JScrollPane SCROLLPANE_MOD1;
  private XRiTable MOD1;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
