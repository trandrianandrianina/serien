
package ri.serien.libecranrpg.vcgm.VCGMG3FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGMG3FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCGMG3FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    xTitledPanel1.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBFCH@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT01@")).trim());
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBC01@")).trim());
    label3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBD01@")).trim());
    label4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT02@")).trim());
    label5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT03@")).trim());
    label6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT04@")).trim());
    label7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT05@")).trim());
    label8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT06@")).trim());
    label9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT07@")).trim());
    label10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT08@")).trim());
    label11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT09@")).trim());
    label12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT10@")).trim());
    label13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT11@")).trim());
    label14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT12@")).trim());
    label15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT13@")).trim());
    label16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT14@")).trim());
    label17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBC02@")).trim());
    label18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBC03@")).trim());
    label19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBC04@")).trim());
    label20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBC05@")).trim());
    label21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBC06@")).trim());
    label22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBC07@")).trim());
    label23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBC08@")).trim());
    label24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBC09@")).trim());
    label25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBC10@")).trim());
    label26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBC11@")).trim());
    label27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBC12@")).trim());
    label28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBC13@")).trim());
    label29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBC14@")).trim());
    label30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBC15@")).trim());
    label31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT15@")).trim());
    label32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBD02@")).trim());
    label33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBD03@")).trim());
    label34.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBD04@")).trim());
    label35.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBD05@")).trim());
    label36.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBD06@")).trim());
    label37.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBD07@")).trim());
    label38.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBD08@")).trim());
    label39.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBD09@")).trim());
    label40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBD10@")).trim());
    label41.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBD11@")).trim());
    label42.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBD12@")).trim());
    label43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBD13@")).trim());
    label44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBD14@")).trim());
    label45.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBD15@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    DFTIT1.setEnabled(lexique.isPresent("DFTIT1"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - Personnalisation Etat"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_44 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_53 = new JLabel();
    DFTIT1 = new XRiTextField();
    DFZ01 = new XRiTextField();
    DFZ02 = new XRiTextField();
    DFZ03 = new XRiTextField();
    DFZ04 = new XRiTextField();
    DFZ05 = new XRiTextField();
    DFZ06 = new XRiTextField();
    DFZ07 = new XRiTextField();
    DFZ08 = new XRiTextField();
    DFZ09 = new XRiTextField();
    DFZ10 = new XRiTextField();
    DFZ11 = new XRiTextField();
    DFZ12 = new XRiTextField();
    DFZ13 = new XRiTextField();
    DFZ14 = new XRiTextField();
    DFZ15 = new XRiTextField();
    LIG01 = new XRiTextField();
    LIG02 = new XRiTextField();
    LIG03 = new XRiTextField();
    LIG04 = new XRiTextField();
    LIG05 = new XRiTextField();
    LIG06 = new XRiTextField();
    LIG07 = new XRiTextField();
    LIG08 = new XRiTextField();
    LIG09 = new XRiTextField();
    LIG10 = new XRiTextField();
    LIG11 = new XRiTextField();
    LIG12 = new XRiTextField();
    LIG13 = new XRiTextField();
    LIG14 = new XRiTextField();
    LIG15 = new XRiTextField();
    COL01 = new XRiTextField();
    COL02 = new XRiTextField();
    COL03 = new XRiTextField();
    COL04 = new XRiTextField();
    COL05 = new XRiTextField();
    COL06 = new XRiTextField();
    COL07 = new XRiTextField();
    COL08 = new XRiTextField();
    COL09 = new XRiTextField();
    COL10 = new XRiTextField();
    COL11 = new XRiTextField();
    COL12 = new XRiTextField();
    COL13 = new XRiTextField();
    COL14 = new XRiTextField();
    COL15 = new XRiTextField();
    DFS01 = new XRiTextField();
    DFS02 = new XRiTextField();
    DFS03 = new XRiTextField();
    DFS04 = new XRiTextField();
    DFS05 = new XRiTextField();
    DFS06 = new XRiTextField();
    DFS07 = new XRiTextField();
    DFS08 = new XRiTextField();
    DFS09 = new XRiTextField();
    DFS10 = new XRiTextField();
    DFS11 = new XRiTextField();
    DFS12 = new XRiTextField();
    DFS13 = new XRiTextField();
    DFS14 = new XRiTextField();
    DFS15 = new XRiTextField();
    label1 = new RiZoneSortie();
    label2 = new RiZoneSortie();
    label3 = new RiZoneSortie();
    label4 = new RiZoneSortie();
    label5 = new RiZoneSortie();
    label6 = new RiZoneSortie();
    label7 = new RiZoneSortie();
    label8 = new RiZoneSortie();
    label9 = new RiZoneSortie();
    label10 = new RiZoneSortie();
    label11 = new RiZoneSortie();
    label12 = new RiZoneSortie();
    label13 = new RiZoneSortie();
    label14 = new RiZoneSortie();
    label15 = new RiZoneSortie();
    label16 = new RiZoneSortie();
    label17 = new RiZoneSortie();
    label18 = new RiZoneSortie();
    label19 = new RiZoneSortie();
    label20 = new RiZoneSortie();
    label21 = new RiZoneSortie();
    label22 = new RiZoneSortie();
    label23 = new RiZoneSortie();
    label24 = new RiZoneSortie();
    label25 = new RiZoneSortie();
    label26 = new RiZoneSortie();
    label27 = new RiZoneSortie();
    label28 = new RiZoneSortie();
    label29 = new RiZoneSortie();
    label30 = new RiZoneSortie();
    label31 = new RiZoneSortie();
    label32 = new RiZoneSortie();
    label33 = new RiZoneSortie();
    label34 = new RiZoneSortie();
    label35 = new RiZoneSortie();
    label36 = new RiZoneSortie();
    label37 = new RiZoneSortie();
    label38 = new RiZoneSortie();
    label39 = new RiZoneSortie();
    label40 = new RiZoneSortie();
    label41 = new RiZoneSortie();
    label42 = new RiZoneSortie();
    label43 = new RiZoneSortie();
    label44 = new RiZoneSortie();
    label45 = new RiZoneSortie();
    OBJ_54 = new JLabel();
    OBJ_55 = new JLabel();
    OBJ_56 = new JLabel();
    OBJ_57 = new JLabel();
    OBJ_58 = new JLabel();
    OBJ_59 = new JLabel();
    OBJ_60 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation des \u00e9tats de comptabilit\u00e9");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_42 ----
          OBJ_42.setText("Soci\u00e9t\u00e9");
          OBJ_42.setName("OBJ_42");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setName("INDTYP");

          //---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(720, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("@LIBFCH@");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- OBJ_53 ----
            OBJ_53.setText("Description de format");
            OBJ_53.setName("OBJ_53");
            xTitledPanel1ContentContainer.add(OBJ_53);
            OBJ_53.setBounds(45, 10, 180, 24);

            //---- DFTIT1 ----
            DFTIT1.setComponentPopupMenu(BTD);
            DFTIT1.setName("DFTIT1");
            xTitledPanel1ContentContainer.add(DFTIT1);
            DFTIT1.setBounds(230, 5, 310, DFTIT1.getPreferredSize().height);

            //---- DFZ01 ----
            DFZ01.setName("DFZ01");
            xTitledPanel1ContentContainer.add(DFZ01);
            DFZ01.setBounds(45, 60, 40, DFZ01.getPreferredSize().height);

            //---- DFZ02 ----
            DFZ02.setName("DFZ02");
            xTitledPanel1ContentContainer.add(DFZ02);
            DFZ02.setBounds(45, 90, 40, DFZ02.getPreferredSize().height);

            //---- DFZ03 ----
            DFZ03.setName("DFZ03");
            xTitledPanel1ContentContainer.add(DFZ03);
            DFZ03.setBounds(45, 120, 40, DFZ03.getPreferredSize().height);

            //---- DFZ04 ----
            DFZ04.setName("DFZ04");
            xTitledPanel1ContentContainer.add(DFZ04);
            DFZ04.setBounds(45, 150, 40, DFZ04.getPreferredSize().height);

            //---- DFZ05 ----
            DFZ05.setName("DFZ05");
            xTitledPanel1ContentContainer.add(DFZ05);
            DFZ05.setBounds(45, 180, 40, DFZ05.getPreferredSize().height);

            //---- DFZ06 ----
            DFZ06.setName("DFZ06");
            xTitledPanel1ContentContainer.add(DFZ06);
            DFZ06.setBounds(45, 210, 40, DFZ06.getPreferredSize().height);

            //---- DFZ07 ----
            DFZ07.setName("DFZ07");
            xTitledPanel1ContentContainer.add(DFZ07);
            DFZ07.setBounds(45, 240, 40, DFZ07.getPreferredSize().height);

            //---- DFZ08 ----
            DFZ08.setName("DFZ08");
            xTitledPanel1ContentContainer.add(DFZ08);
            DFZ08.setBounds(45, 270, 40, DFZ08.getPreferredSize().height);

            //---- DFZ09 ----
            DFZ09.setName("DFZ09");
            xTitledPanel1ContentContainer.add(DFZ09);
            DFZ09.setBounds(45, 300, 40, DFZ09.getPreferredSize().height);

            //---- DFZ10 ----
            DFZ10.setName("DFZ10");
            xTitledPanel1ContentContainer.add(DFZ10);
            DFZ10.setBounds(45, 330, 40, DFZ10.getPreferredSize().height);

            //---- DFZ11 ----
            DFZ11.setName("DFZ11");
            xTitledPanel1ContentContainer.add(DFZ11);
            DFZ11.setBounds(45, 360, 40, DFZ11.getPreferredSize().height);

            //---- DFZ12 ----
            DFZ12.setName("DFZ12");
            xTitledPanel1ContentContainer.add(DFZ12);
            DFZ12.setBounds(45, 390, 40, DFZ12.getPreferredSize().height);

            //---- DFZ13 ----
            DFZ13.setName("DFZ13");
            xTitledPanel1ContentContainer.add(DFZ13);
            DFZ13.setBounds(45, 420, 40, DFZ13.getPreferredSize().height);

            //---- DFZ14 ----
            DFZ14.setName("DFZ14");
            xTitledPanel1ContentContainer.add(DFZ14);
            DFZ14.setBounds(45, 450, 40, DFZ14.getPreferredSize().height);

            //---- DFZ15 ----
            DFZ15.setName("DFZ15");
            xTitledPanel1ContentContainer.add(DFZ15);
            DFZ15.setBounds(45, 480, 40, DFZ15.getPreferredSize().height);

            //---- LIG01 ----
            LIG01.setName("LIG01");
            xTitledPanel1ContentContainer.add(LIG01);
            LIG01.setBounds(100, 60, 30, LIG01.getPreferredSize().height);

            //---- LIG02 ----
            LIG02.setName("LIG02");
            xTitledPanel1ContentContainer.add(LIG02);
            LIG02.setBounds(100, 90, 30, LIG02.getPreferredSize().height);

            //---- LIG03 ----
            LIG03.setName("LIG03");
            xTitledPanel1ContentContainer.add(LIG03);
            LIG03.setBounds(100, 120, 30, LIG03.getPreferredSize().height);

            //---- LIG04 ----
            LIG04.setName("LIG04");
            xTitledPanel1ContentContainer.add(LIG04);
            LIG04.setBounds(100, 150, 30, LIG04.getPreferredSize().height);

            //---- LIG05 ----
            LIG05.setName("LIG05");
            xTitledPanel1ContentContainer.add(LIG05);
            LIG05.setBounds(100, 180, 30, LIG05.getPreferredSize().height);

            //---- LIG06 ----
            LIG06.setName("LIG06");
            xTitledPanel1ContentContainer.add(LIG06);
            LIG06.setBounds(100, 210, 30, LIG06.getPreferredSize().height);

            //---- LIG07 ----
            LIG07.setName("LIG07");
            xTitledPanel1ContentContainer.add(LIG07);
            LIG07.setBounds(100, 240, 30, LIG07.getPreferredSize().height);

            //---- LIG08 ----
            LIG08.setName("LIG08");
            xTitledPanel1ContentContainer.add(LIG08);
            LIG08.setBounds(100, 270, 30, LIG08.getPreferredSize().height);

            //---- LIG09 ----
            LIG09.setName("LIG09");
            xTitledPanel1ContentContainer.add(LIG09);
            LIG09.setBounds(100, 300, 30, LIG09.getPreferredSize().height);

            //---- LIG10 ----
            LIG10.setName("LIG10");
            xTitledPanel1ContentContainer.add(LIG10);
            LIG10.setBounds(100, 330, 30, LIG10.getPreferredSize().height);

            //---- LIG11 ----
            LIG11.setName("LIG11");
            xTitledPanel1ContentContainer.add(LIG11);
            LIG11.setBounds(100, 360, 30, LIG11.getPreferredSize().height);

            //---- LIG12 ----
            LIG12.setName("LIG12");
            xTitledPanel1ContentContainer.add(LIG12);
            LIG12.setBounds(100, 390, 30, LIG12.getPreferredSize().height);

            //---- LIG13 ----
            LIG13.setName("LIG13");
            xTitledPanel1ContentContainer.add(LIG13);
            LIG13.setBounds(100, 420, 30, LIG13.getPreferredSize().height);

            //---- LIG14 ----
            LIG14.setName("LIG14");
            xTitledPanel1ContentContainer.add(LIG14);
            LIG14.setBounds(100, 450, 30, LIG14.getPreferredSize().height);

            //---- LIG15 ----
            LIG15.setName("LIG15");
            xTitledPanel1ContentContainer.add(LIG15);
            LIG15.setBounds(100, 480, 30, LIG15.getPreferredSize().height);

            //---- COL01 ----
            COL01.setName("COL01");
            xTitledPanel1ContentContainer.add(COL01);
            COL01.setBounds(140, 60, 40, COL01.getPreferredSize().height);

            //---- COL02 ----
            COL02.setName("COL02");
            xTitledPanel1ContentContainer.add(COL02);
            COL02.setBounds(140, 90, 40, COL02.getPreferredSize().height);

            //---- COL03 ----
            COL03.setName("COL03");
            xTitledPanel1ContentContainer.add(COL03);
            COL03.setBounds(140, 120, 40, COL03.getPreferredSize().height);

            //---- COL04 ----
            COL04.setName("COL04");
            xTitledPanel1ContentContainer.add(COL04);
            COL04.setBounds(140, 150, 40, COL04.getPreferredSize().height);

            //---- COL05 ----
            COL05.setName("COL05");
            xTitledPanel1ContentContainer.add(COL05);
            COL05.setBounds(140, 180, 40, COL05.getPreferredSize().height);

            //---- COL06 ----
            COL06.setName("COL06");
            xTitledPanel1ContentContainer.add(COL06);
            COL06.setBounds(140, 210, 40, COL06.getPreferredSize().height);

            //---- COL07 ----
            COL07.setName("COL07");
            xTitledPanel1ContentContainer.add(COL07);
            COL07.setBounds(140, 240, 40, COL07.getPreferredSize().height);

            //---- COL08 ----
            COL08.setName("COL08");
            xTitledPanel1ContentContainer.add(COL08);
            COL08.setBounds(140, 270, 40, COL08.getPreferredSize().height);

            //---- COL09 ----
            COL09.setName("COL09");
            xTitledPanel1ContentContainer.add(COL09);
            COL09.setBounds(140, 300, 40, COL09.getPreferredSize().height);

            //---- COL10 ----
            COL10.setName("COL10");
            xTitledPanel1ContentContainer.add(COL10);
            COL10.setBounds(140, 330, 40, COL10.getPreferredSize().height);

            //---- COL11 ----
            COL11.setName("COL11");
            xTitledPanel1ContentContainer.add(COL11);
            COL11.setBounds(140, 360, 40, COL11.getPreferredSize().height);

            //---- COL12 ----
            COL12.setName("COL12");
            xTitledPanel1ContentContainer.add(COL12);
            COL12.setBounds(140, 390, 40, COL12.getPreferredSize().height);

            //---- COL13 ----
            COL13.setName("COL13");
            xTitledPanel1ContentContainer.add(COL13);
            COL13.setBounds(140, 420, 40, COL13.getPreferredSize().height);

            //---- COL14 ----
            COL14.setName("COL14");
            xTitledPanel1ContentContainer.add(COL14);
            COL14.setBounds(140, 450, 40, COL14.getPreferredSize().height);

            //---- COL15 ----
            COL15.setName("COL15");
            xTitledPanel1ContentContainer.add(COL15);
            COL15.setBounds(140, 480, 40, COL15.getPreferredSize().height);

            //---- DFS01 ----
            DFS01.setName("DFS01");
            xTitledPanel1ContentContainer.add(DFS01);
            DFS01.setBounds(195, 60, 20, DFS01.getPreferredSize().height);

            //---- DFS02 ----
            DFS02.setName("DFS02");
            xTitledPanel1ContentContainer.add(DFS02);
            DFS02.setBounds(195, 90, 20, DFS02.getPreferredSize().height);

            //---- DFS03 ----
            DFS03.setName("DFS03");
            xTitledPanel1ContentContainer.add(DFS03);
            DFS03.setBounds(195, 120, 20, DFS03.getPreferredSize().height);

            //---- DFS04 ----
            DFS04.setName("DFS04");
            xTitledPanel1ContentContainer.add(DFS04);
            DFS04.setBounds(195, 150, 20, DFS04.getPreferredSize().height);

            //---- DFS05 ----
            DFS05.setName("DFS05");
            xTitledPanel1ContentContainer.add(DFS05);
            DFS05.setBounds(195, 180, 20, DFS05.getPreferredSize().height);

            //---- DFS06 ----
            DFS06.setName("DFS06");
            xTitledPanel1ContentContainer.add(DFS06);
            DFS06.setBounds(195, 210, 20, DFS06.getPreferredSize().height);

            //---- DFS07 ----
            DFS07.setName("DFS07");
            xTitledPanel1ContentContainer.add(DFS07);
            DFS07.setBounds(195, 240, 20, DFS07.getPreferredSize().height);

            //---- DFS08 ----
            DFS08.setName("DFS08");
            xTitledPanel1ContentContainer.add(DFS08);
            DFS08.setBounds(195, 270, 20, DFS08.getPreferredSize().height);

            //---- DFS09 ----
            DFS09.setName("DFS09");
            xTitledPanel1ContentContainer.add(DFS09);
            DFS09.setBounds(195, 300, 20, DFS09.getPreferredSize().height);

            //---- DFS10 ----
            DFS10.setName("DFS10");
            xTitledPanel1ContentContainer.add(DFS10);
            DFS10.setBounds(195, 330, 20, DFS10.getPreferredSize().height);

            //---- DFS11 ----
            DFS11.setName("DFS11");
            xTitledPanel1ContentContainer.add(DFS11);
            DFS11.setBounds(195, 360, 20, DFS11.getPreferredSize().height);

            //---- DFS12 ----
            DFS12.setName("DFS12");
            xTitledPanel1ContentContainer.add(DFS12);
            DFS12.setBounds(195, 390, 20, DFS12.getPreferredSize().height);

            //---- DFS13 ----
            DFS13.setName("DFS13");
            xTitledPanel1ContentContainer.add(DFS13);
            DFS13.setBounds(195, 420, 20, DFS13.getPreferredSize().height);

            //---- DFS14 ----
            DFS14.setName("DFS14");
            xTitledPanel1ContentContainer.add(DFS14);
            DFS14.setBounds(195, 450, 20, DFS14.getPreferredSize().height);

            //---- DFS15 ----
            DFS15.setName("DFS15");
            xTitledPanel1ContentContainer.add(DFS15);
            DFS15.setBounds(195, 480, 20, DFS15.getPreferredSize().height);

            //---- label1 ----
            label1.setText("@TIT01@");
            label1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label1.setName("label1");
            xTitledPanel1ContentContainer.add(label1);
            label1.setBounds(230, 60, 310, 28);

            //---- label2 ----
            label2.setText("@NBC01@");
            label2.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label2.setName("label2");
            xTitledPanel1ContentContainer.add(label2);
            label2.setBounds(550, 60, 30, 28);

            //---- label3 ----
            label3.setText("@NBD01@");
            label3.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label3.setName("label3");
            xTitledPanel1ContentContainer.add(label3);
            label3.setBounds(595, 60, 20, 28);

            //---- label4 ----
            label4.setText("@TIT02@");
            label4.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label4.setName("label4");
            xTitledPanel1ContentContainer.add(label4);
            label4.setBounds(230, 90, 310, 28);

            //---- label5 ----
            label5.setText("@TIT03@");
            label5.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label5.setName("label5");
            xTitledPanel1ContentContainer.add(label5);
            label5.setBounds(230, 120, 310, 28);

            //---- label6 ----
            label6.setText("@TIT04@");
            label6.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label6.setName("label6");
            xTitledPanel1ContentContainer.add(label6);
            label6.setBounds(230, 150, 310, 28);

            //---- label7 ----
            label7.setText("@TIT05@");
            label7.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label7.setName("label7");
            xTitledPanel1ContentContainer.add(label7);
            label7.setBounds(230, 180, 310, 28);

            //---- label8 ----
            label8.setText("@TIT06@");
            label8.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label8.setName("label8");
            xTitledPanel1ContentContainer.add(label8);
            label8.setBounds(230, 210, 310, 28);

            //---- label9 ----
            label9.setText("@TIT07@");
            label9.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label9.setName("label9");
            xTitledPanel1ContentContainer.add(label9);
            label9.setBounds(230, 240, 310, 28);

            //---- label10 ----
            label10.setText("@TIT08@");
            label10.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label10.setName("label10");
            xTitledPanel1ContentContainer.add(label10);
            label10.setBounds(230, 270, 310, 28);

            //---- label11 ----
            label11.setText("@TIT09@");
            label11.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label11.setName("label11");
            xTitledPanel1ContentContainer.add(label11);
            label11.setBounds(230, 300, 310, 28);

            //---- label12 ----
            label12.setText("@TIT10@");
            label12.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label12.setName("label12");
            xTitledPanel1ContentContainer.add(label12);
            label12.setBounds(230, 330, 310, 28);

            //---- label13 ----
            label13.setText("@TIT11@");
            label13.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label13.setName("label13");
            xTitledPanel1ContentContainer.add(label13);
            label13.setBounds(230, 360, 310, 28);

            //---- label14 ----
            label14.setText("@TIT12@");
            label14.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label14.setName("label14");
            xTitledPanel1ContentContainer.add(label14);
            label14.setBounds(230, 390, 310, 28);

            //---- label15 ----
            label15.setText("@TIT13@");
            label15.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label15.setName("label15");
            xTitledPanel1ContentContainer.add(label15);
            label15.setBounds(230, 420, 310, 28);

            //---- label16 ----
            label16.setText("@TIT14@");
            label16.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label16.setName("label16");
            xTitledPanel1ContentContainer.add(label16);
            label16.setBounds(230, 450, 310, 28);

            //---- label17 ----
            label17.setText("@NBC02@");
            label17.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label17.setName("label17");
            xTitledPanel1ContentContainer.add(label17);
            label17.setBounds(550, 90, 30, 28);

            //---- label18 ----
            label18.setText("@NBC03@");
            label18.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label18.setName("label18");
            xTitledPanel1ContentContainer.add(label18);
            label18.setBounds(550, 120, 30, 28);

            //---- label19 ----
            label19.setText("@NBC04@");
            label19.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label19.setName("label19");
            xTitledPanel1ContentContainer.add(label19);
            label19.setBounds(550, 150, 30, 28);

            //---- label20 ----
            label20.setText("@NBC05@");
            label20.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label20.setName("label20");
            xTitledPanel1ContentContainer.add(label20);
            label20.setBounds(550, 180, 30, 28);

            //---- label21 ----
            label21.setText("@NBC06@");
            label21.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label21.setName("label21");
            xTitledPanel1ContentContainer.add(label21);
            label21.setBounds(550, 210, 30, 28);

            //---- label22 ----
            label22.setText("@NBC07@");
            label22.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label22.setName("label22");
            xTitledPanel1ContentContainer.add(label22);
            label22.setBounds(550, 240, 30, 28);

            //---- label23 ----
            label23.setText("@NBC08@");
            label23.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label23.setName("label23");
            xTitledPanel1ContentContainer.add(label23);
            label23.setBounds(550, 270, 30, 28);

            //---- label24 ----
            label24.setText("@NBC09@");
            label24.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label24.setName("label24");
            xTitledPanel1ContentContainer.add(label24);
            label24.setBounds(550, 300, 30, 28);

            //---- label25 ----
            label25.setText("@NBC10@");
            label25.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label25.setName("label25");
            xTitledPanel1ContentContainer.add(label25);
            label25.setBounds(550, 330, 30, 28);

            //---- label26 ----
            label26.setText("@NBC11@");
            label26.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label26.setName("label26");
            xTitledPanel1ContentContainer.add(label26);
            label26.setBounds(550, 360, 30, 28);

            //---- label27 ----
            label27.setText("@NBC12@");
            label27.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label27.setName("label27");
            xTitledPanel1ContentContainer.add(label27);
            label27.setBounds(550, 390, 30, 28);

            //---- label28 ----
            label28.setText("@NBC13@");
            label28.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label28.setName("label28");
            xTitledPanel1ContentContainer.add(label28);
            label28.setBounds(550, 420, 30, 28);

            //---- label29 ----
            label29.setText("@NBC14@");
            label29.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label29.setName("label29");
            xTitledPanel1ContentContainer.add(label29);
            label29.setBounds(550, 450, 30, 28);

            //---- label30 ----
            label30.setText("@NBC15@");
            label30.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label30.setName("label30");
            xTitledPanel1ContentContainer.add(label30);
            label30.setBounds(550, 480, 30, 28);

            //---- label31 ----
            label31.setText("@TIT15@");
            label31.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label31.setName("label31");
            xTitledPanel1ContentContainer.add(label31);
            label31.setBounds(230, 480, 310, 28);

            //---- label32 ----
            label32.setText("@NBD02@");
            label32.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label32.setName("label32");
            xTitledPanel1ContentContainer.add(label32);
            label32.setBounds(595, 90, 20, 28);

            //---- label33 ----
            label33.setText("@NBD03@");
            label33.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label33.setName("label33");
            xTitledPanel1ContentContainer.add(label33);
            label33.setBounds(595, 120, 20, 28);

            //---- label34 ----
            label34.setText("@NBD04@");
            label34.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label34.setName("label34");
            xTitledPanel1ContentContainer.add(label34);
            label34.setBounds(595, 150, 20, 28);

            //---- label35 ----
            label35.setText("@NBD05@");
            label35.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label35.setName("label35");
            xTitledPanel1ContentContainer.add(label35);
            label35.setBounds(595, 180, 20, 28);

            //---- label36 ----
            label36.setText("@NBD06@");
            label36.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label36.setName("label36");
            xTitledPanel1ContentContainer.add(label36);
            label36.setBounds(595, 210, 20, 28);

            //---- label37 ----
            label37.setText("@NBD07@");
            label37.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label37.setName("label37");
            xTitledPanel1ContentContainer.add(label37);
            label37.setBounds(595, 240, 20, 28);

            //---- label38 ----
            label38.setText("@NBD08@");
            label38.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label38.setName("label38");
            xTitledPanel1ContentContainer.add(label38);
            label38.setBounds(595, 270, 20, 28);

            //---- label39 ----
            label39.setText("@NBD09@");
            label39.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label39.setName("label39");
            xTitledPanel1ContentContainer.add(label39);
            label39.setBounds(595, 300, 20, 28);

            //---- label40 ----
            label40.setText("@NBD10@");
            label40.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label40.setName("label40");
            xTitledPanel1ContentContainer.add(label40);
            label40.setBounds(595, 330, 20, 28);

            //---- label41 ----
            label41.setText("@NBD11@");
            label41.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label41.setName("label41");
            xTitledPanel1ContentContainer.add(label41);
            label41.setBounds(595, 360, 20, 28);

            //---- label42 ----
            label42.setText("@NBD12@");
            label42.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label42.setName("label42");
            xTitledPanel1ContentContainer.add(label42);
            label42.setBounds(595, 390, 20, 28);

            //---- label43 ----
            label43.setText("@NBD13@");
            label43.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label43.setName("label43");
            xTitledPanel1ContentContainer.add(label43);
            label43.setBounds(595, 420, 20, 28);

            //---- label44 ----
            label44.setText("@NBD14@");
            label44.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label44.setName("label44");
            xTitledPanel1ContentContainer.add(label44);
            label44.setBounds(595, 450, 20, 28);

            //---- label45 ----
            label45.setText("@NBD15@");
            label45.setBorder(new BevelBorder(BevelBorder.LOWERED));
            label45.setName("label45");
            xTitledPanel1ContentContainer.add(label45);
            label45.setBounds(595, 480, 20, 28);

            //---- OBJ_54 ----
            OBJ_54.setText("n\u00b0 zone");
            OBJ_54.setFont(OBJ_54.getFont().deriveFont(OBJ_54.getFont().getStyle() | Font.BOLD));
            OBJ_54.setName("OBJ_54");
            xTitledPanel1ContentContainer.add(OBJ_54);
            OBJ_54.setBounds(40, 40, 45, 19);

            //---- OBJ_55 ----
            OBJ_55.setText("ligne");
            OBJ_55.setFont(OBJ_55.getFont().deriveFont(OBJ_55.getFont().getStyle() | Font.BOLD));
            OBJ_55.setName("OBJ_55");
            xTitledPanel1ContentContainer.add(OBJ_55);
            OBJ_55.setBounds(95, 40, 35, 19);

            //---- OBJ_56 ----
            OBJ_56.setText("colonne");
            OBJ_56.setFont(OBJ_56.getFont().deriveFont(OBJ_56.getFont().getStyle() | Font.BOLD));
            OBJ_56.setName("OBJ_56");
            xTitledPanel1ContentContainer.add(OBJ_56);
            OBJ_56.setBounds(135, 40, OBJ_56.getPreferredSize().width, 19);

            //---- OBJ_57 ----
            OBJ_57.setText("CO");
            OBJ_57.setFont(OBJ_57.getFont().deriveFont(OBJ_57.getFont().getStyle() | Font.BOLD));
            OBJ_57.setName("OBJ_57");
            xTitledPanel1ContentContainer.add(OBJ_57);
            OBJ_57.setBounds(190, 40, 25, 19);

            //---- OBJ_58 ----
            OBJ_58.setText("Titre de zone");
            OBJ_58.setFont(OBJ_58.getFont().deriveFont(OBJ_58.getFont().getStyle() | Font.BOLD));
            OBJ_58.setName("OBJ_58");
            xTitledPanel1ContentContainer.add(OBJ_58);
            OBJ_58.setBounds(230, 40, 310, 19);

            //---- OBJ_59 ----
            OBJ_59.setText("Lo");
            OBJ_59.setFont(OBJ_59.getFont().deriveFont(OBJ_59.getFont().getStyle() | Font.BOLD));
            OBJ_59.setName("OBJ_59");
            xTitledPanel1ContentContainer.add(OBJ_59);
            OBJ_59.setBounds(550, 40, 25, 19);

            //---- OBJ_60 ----
            OBJ_60.setText("d");
            OBJ_60.setFont(OBJ_60.getFont().deriveFont(OBJ_60.getFont().getStyle() | Font.BOLD));
            OBJ_60.setName("OBJ_60");
            xTitledPanel1ContentContainer.add(OBJ_60);
            OBJ_60.setBounds(595, 40, 20, 19);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 694, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 552, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private XRiTextField INDETB;
  private JLabel OBJ_44;
  private XRiTextField INDTYP;
  private JLabel OBJ_46;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_53;
  private XRiTextField DFTIT1;
  private XRiTextField DFZ01;
  private XRiTextField DFZ02;
  private XRiTextField DFZ03;
  private XRiTextField DFZ04;
  private XRiTextField DFZ05;
  private XRiTextField DFZ06;
  private XRiTextField DFZ07;
  private XRiTextField DFZ08;
  private XRiTextField DFZ09;
  private XRiTextField DFZ10;
  private XRiTextField DFZ11;
  private XRiTextField DFZ12;
  private XRiTextField DFZ13;
  private XRiTextField DFZ14;
  private XRiTextField DFZ15;
  private XRiTextField LIG01;
  private XRiTextField LIG02;
  private XRiTextField LIG03;
  private XRiTextField LIG04;
  private XRiTextField LIG05;
  private XRiTextField LIG06;
  private XRiTextField LIG07;
  private XRiTextField LIG08;
  private XRiTextField LIG09;
  private XRiTextField LIG10;
  private XRiTextField LIG11;
  private XRiTextField LIG12;
  private XRiTextField LIG13;
  private XRiTextField LIG14;
  private XRiTextField LIG15;
  private XRiTextField COL01;
  private XRiTextField COL02;
  private XRiTextField COL03;
  private XRiTextField COL04;
  private XRiTextField COL05;
  private XRiTextField COL06;
  private XRiTextField COL07;
  private XRiTextField COL08;
  private XRiTextField COL09;
  private XRiTextField COL10;
  private XRiTextField COL11;
  private XRiTextField COL12;
  private XRiTextField COL13;
  private XRiTextField COL14;
  private XRiTextField COL15;
  private XRiTextField DFS01;
  private XRiTextField DFS02;
  private XRiTextField DFS03;
  private XRiTextField DFS04;
  private XRiTextField DFS05;
  private XRiTextField DFS06;
  private XRiTextField DFS07;
  private XRiTextField DFS08;
  private XRiTextField DFS09;
  private XRiTextField DFS10;
  private XRiTextField DFS11;
  private XRiTextField DFS12;
  private XRiTextField DFS13;
  private XRiTextField DFS14;
  private XRiTextField DFS15;
  private RiZoneSortie label1;
  private RiZoneSortie label2;
  private RiZoneSortie label3;
  private RiZoneSortie label4;
  private RiZoneSortie label5;
  private RiZoneSortie label6;
  private RiZoneSortie label7;
  private RiZoneSortie label8;
  private RiZoneSortie label9;
  private RiZoneSortie label10;
  private RiZoneSortie label11;
  private RiZoneSortie label12;
  private RiZoneSortie label13;
  private RiZoneSortie label14;
  private RiZoneSortie label15;
  private RiZoneSortie label16;
  private RiZoneSortie label17;
  private RiZoneSortie label18;
  private RiZoneSortie label19;
  private RiZoneSortie label20;
  private RiZoneSortie label21;
  private RiZoneSortie label22;
  private RiZoneSortie label23;
  private RiZoneSortie label24;
  private RiZoneSortie label25;
  private RiZoneSortie label26;
  private RiZoneSortie label27;
  private RiZoneSortie label28;
  private RiZoneSortie label29;
  private RiZoneSortie label30;
  private RiZoneSortie label31;
  private RiZoneSortie label32;
  private RiZoneSortie label33;
  private RiZoneSortie label34;
  private RiZoneSortie label35;
  private RiZoneSortie label36;
  private RiZoneSortie label37;
  private RiZoneSortie label38;
  private RiZoneSortie label39;
  private RiZoneSortie label40;
  private RiZoneSortie label41;
  private RiZoneSortie label42;
  private RiZoneSortie label43;
  private RiZoneSortie label44;
  private RiZoneSortie label45;
  private JLabel OBJ_54;
  private JLabel OBJ_55;
  private JLabel OBJ_56;
  private JLabel OBJ_57;
  private JLabel OBJ_58;
  private JLabel OBJ_59;
  private JLabel OBJ_60;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
