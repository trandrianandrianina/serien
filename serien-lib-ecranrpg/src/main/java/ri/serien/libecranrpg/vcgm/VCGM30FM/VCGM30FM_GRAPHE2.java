/*
 * Created by JFormDesigner on Wed Feb 24 16:24:25 CET 2010
 */

package ri.serien.libecranrpg.vcgm.VCGM30FM;

import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.WindowConstants;

import ri.serien.libswing.composantrpg.lexical.RiGraphe;
import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * @author Stéphane Vénéri
 */
public class VCGM30FM_GRAPHE2 extends JFrame {
  
  
  private String[] annee = null;
  private String[] donnee = null;
  private RiGraphe[] graphe = new RiGraphe[4];
  private String[] moisLib =
      { "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre" };
  
  /**
   * Constructeur
   * @param annee
   * @param donnee
   */
  public VCGM30FM_GRAPHE2(String[] annee, String[] donnee) {
    super();
    initComponents();
    
    this.annee = annee;
    this.donnee = donnee;
    
    initData();
    setSize(1000, 700);
    setBackground(SNCharteGraphique.COULEUR_FOND);
    setVisible(true);
  }
  
  /**
   * Initialise les données pour les graphes
   *
   */
  private void initData() {
    // Préparation des données
    for (int an = 0; an < annee.length - 1; an++) {
      Object[][] data = new Object[moisLib.length][2];
      for (int i = 0; i < moisLib.length; i++) {
        data[i][0] = moisLib[i];
        data[i][1] = Double.parseDouble(donnee[i + (an * 12)]);
      }
      graphe[an] = new RiGraphe(RiGraphe.GRAPHE_LINE3D);
      graphe[an].setDonnee(data, annee[an], false);
      graphe[an].getGraphe(annee[an], false);
    }
  }
  
  /**
   * Dessine les graphes
   *
   */
  private void paintGraphe() {
    l_Annee1.setIcon(graphe[0].getPicture(l_Annee1.getWidth(), l_Annee1.getHeight()));
    l_Annee2.setIcon(graphe[1].getPicture(l_Annee2.getWidth(), l_Annee2.getHeight()));
    l_Annee3.setIcon(graphe[2].getPicture(l_Annee3.getWidth(), l_Annee3.getHeight()));
    l_Annee4.setIcon(graphe[3].getPicture(l_Annee4.getWidth(), l_Annee4.getHeight()));
  }
  
  private void thisComponentResized(ComponentEvent e) {
    paintGraphe();
  }
  
  private void MI_CopierActionPerformed(ActionEvent e) {
    if (BTD.getInvoker().getName().equals("l_Annee1")) {
      graphe[0].sendToClipBoard(l_Annee1.getWidth(), l_Annee1.getHeight());
    }
    else if (BTD.getInvoker().getName().equals("l_Annee2")) {
      graphe[1].sendToClipBoard(l_Annee2.getWidth(), l_Annee2.getHeight());
    }
    else if (BTD.getInvoker().getName().equals("l_Annee3")) {
      graphe[2].sendToClipBoard(l_Annee3.getWidth(), l_Annee3.getHeight());
    }
    else if (BTD.getInvoker().getName().equals("l_Annee4")) {
      graphe[3].sendToClipBoard(l_Annee4.getWidth(), l_Annee4.getHeight());
    }
  }
  
  private void MI_EnregistrerActionPerformed(ActionEvent e) {
    if (BTD.getInvoker().getName().equals("l_Annee1")) {
      graphe[0].saveGraphe(null, l_Annee1.getWidth(), l_Annee1.getHeight());
    }
    else if (BTD.getInvoker().getName().equals("l_Annee2")) {
      graphe[1].saveGraphe(null, l_Annee2.getWidth(), l_Annee2.getHeight());
    }
    else if (BTD.getInvoker().getName().equals("l_Annee3")) {
      graphe[2].saveGraphe(null, l_Annee3.getWidth(), l_Annee3.getHeight());
    }
    else if (BTD.getInvoker().getName().equals("l_Annee4")) {
      graphe[3].saveGraphe(null, l_Annee4.getWidth(), l_Annee4.getHeight());
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    l_Annee1 = new JLabel();
    l_Annee2 = new JLabel();
    l_Annee3 = new JLabel();
    l_Annee4 = new JLabel();
    BTD = new JPopupMenu();
    MI_Copier = new JMenuItem();
    MI_Enregistrer = new JMenuItem();

    //======== this ========
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setTitle("Analyse de l'\u00e9volution mensuelle sur une ann\u00e9e");
    setIconImage(null);
    setBackground(new Color(238, 238, 210));
    setVisible(true);
    setName("this");
    addComponentListener(new ComponentAdapter() {
      @Override
      public void componentResized(ComponentEvent e) {
        thisComponentResized(e);
      }
    });
    Container contentPane = getContentPane();
    contentPane.setLayout(new GridLayout(2, 2));

    //---- l_Annee1 ----
    l_Annee1.setComponentPopupMenu(BTD);
    l_Annee1.setName("l_Annee1");
    contentPane.add(l_Annee1);

    //---- l_Annee2 ----
    l_Annee2.setComponentPopupMenu(BTD);
    l_Annee2.setName("l_Annee2");
    contentPane.add(l_Annee2);

    //---- l_Annee3 ----
    l_Annee3.setComponentPopupMenu(BTD);
    l_Annee3.setName("l_Annee3");
    contentPane.add(l_Annee3);

    //---- l_Annee4 ----
    l_Annee4.setComponentPopupMenu(BTD);
    l_Annee4.setName("l_Annee4");
    contentPane.add(l_Annee4);
    pack();

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- MI_Copier ----
      MI_Copier.setText("Copier");
      MI_Copier.setName("MI_Copier");
      MI_Copier.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          MI_CopierActionPerformed(e);
        }
      });
      BTD.add(MI_Copier);

      //---- MI_Enregistrer ----
      MI_Enregistrer.setText("Enregistrer sous");
      MI_Enregistrer.setName("MI_Enregistrer");
      MI_Enregistrer.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          MI_EnregistrerActionPerformed(e);
        }
      });
      BTD.add(MI_Enregistrer);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel l_Annee1;
  private JLabel l_Annee2;
  private JLabel l_Annee3;
  private JLabel l_Annee4;
  private JPopupMenu BTD;
  private JMenuItem MI_Copier;
  private JMenuItem MI_Enregistrer;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
