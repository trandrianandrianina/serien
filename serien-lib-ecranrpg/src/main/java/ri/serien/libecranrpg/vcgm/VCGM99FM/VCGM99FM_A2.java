
package ri.serien.libecranrpg.vcgm.VCGM99FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VCGM99FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "HLD1", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 51, };
  private String[][] _LIST_Title_Data_Brut = null;
  private String[] WRECH_Value = { "-10", "-05", "-03", "-02", "-01", "", "+01", "+02", "+03", "+05", "+10", };
  private String[] WRECH_Title = { "Décalage de 10 lignes en moins", "Décalage de 5 lignes en moins", "Décalage de 3 lignes en moins",
      "Décalage de 2 lignes en moins", "Décalage de 1 ligne en moins", "", "Décalage de 1 ligne en plus", "Décalage de 2 lignes en plus",
      "Décalage de 3 lignes en plus", "Décalage de 5 lignes en plus", "Décalage de 10 lignes en plus", };
  
  public VCGM99FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // WTP01.getTableHeader().setFont(new Font("Courier New", Font.BOLD, 12));
    // ((DefaultTableCellRenderer)WTP01.getTableHeader().getDefaultRenderer()).setHorizontalAlignment( JLabel.LEFT );
    // _LIST_Title_Data_Brut = initTable(LIST);
    
    // Ajout
    initDiverses();
    WRECH.setValeurs(WRECH_Value, WRECH_Title);
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WLIBDV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DEMOPE@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    _LIST_Title_Data_Brut[0][0] =
        "Compte Axe    Date    Jo Fol. Libellé         " + lexique.HostFieldGetData("TIT1") + " " + lexique.HostFieldGetData("TIT2");
    // majTable(LIST, _LIST_Title_Data_Brut, _WTP01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    

    
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    DEMAA6.setVisible(lexique.isPresent("DEMAA6"));
    DEMAA5.setVisible(lexique.isPresent("DEMAA5"));
    DEMAA4.setVisible(lexique.isPresent("DEMAA4"));
    DEMAA3.setVisible(lexique.isPresent("DEMAA3"));
    DEMAA2.setVisible(lexique.isPresent("DEMAA2"));
    
    // TODO Icones
    bt_FIN.setIcon(lexique.chargerImage("images/pfin20.png", true));
    bt_DEB.setIcon(lexique.chargerImage("images/pdeb20.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  @TITRE@"));
    
    

    
    p_bpresentation.setCodeEtablissement(DEMETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(DEMETB.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("WRECH", 0, WRECH_Value[WRECH.getSelectedIndex()]);
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_35ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "1", "Enter");
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _WTP01_Top, "1", "ENTER", e);
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void bt_FINActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, "F");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_DEBActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, "D");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void WRECHActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, WRECH_Value[WRECH.getSelectedIndex()]);
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_36ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_45_OBJ_45 = new JLabel();
    DEMDAT = new XRiTextField();
    OBJ_47_OBJ_47 = new JLabel();
    DEMAA1 = new XRiTextField();
    DEMAA2 = new XRiTextField();
    DEMAA3 = new XRiTextField();
    DEMAA4 = new XRiTextField();
    DEMAA5 = new XRiTextField();
    DEMAA6 = new XRiTextField();
    OBJ_20_OBJ_20 = new JLabel();
    DEMETB = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    SCROLLPANE_LIST1 = new JScrollPane();
    WTP01 = new XRiTable();
    WRECH = new XRiComboBox();
    bt_FIN = new JButton();
    bt_DEB = new JButton();
    WLIBDV = new RiZoneSortie();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    label1 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_35 = new JMenuItem();
    OBJ_36 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Historique d'analyse sur axes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_45_OBJ_45 ----
          OBJ_45_OBJ_45.setText("Date");
          OBJ_45_OBJ_45.setName("OBJ_45_OBJ_45");

          //---- DEMDAT ----
          DEMDAT.setName("DEMDAT");

          //---- OBJ_47_OBJ_47 ----
          OBJ_47_OBJ_47.setText("Axes");
          OBJ_47_OBJ_47.setName("OBJ_47_OBJ_47");

          //---- DEMAA1 ----
          DEMAA1.setName("DEMAA1");

          //---- DEMAA2 ----
          DEMAA2.setName("DEMAA2");

          //---- DEMAA3 ----
          DEMAA3.setName("DEMAA3");

          //---- DEMAA4 ----
          DEMAA4.setName("DEMAA4");

          //---- DEMAA5 ----
          DEMAA5.setName("DEMAA5");

          //---- DEMAA6 ----
          DEMAA6.setName("DEMAA6");

          //---- OBJ_20_OBJ_20 ----
          OBJ_20_OBJ_20.setText("Soci\u00e9t\u00e9");
          OBJ_20_OBJ_20.setName("OBJ_20_OBJ_20");

          //---- DEMETB ----
          DEMETB.setName("DEMETB");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_20_OBJ_20, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(DEMETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(OBJ_45_OBJ_45, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(DEMDAT, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45)
                .addComponent(OBJ_47_OBJ_47, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addComponent(DEMAA1, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(DEMAA2, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(DEMAA3, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(DEMAA4, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(DEMAA5, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(DEMAA6, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(DEMETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(DEMDAT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(DEMAA1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(DEMAA2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(DEMAA3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(DEMAA4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(DEMAA5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(DEMAA6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_20_OBJ_20, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_45_OBJ_45)
                  .addComponent(OBJ_47_OBJ_47)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Affichage");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText(" Mois, Cumuls, Soldes");
              riSousMenu_bt6.setToolTipText("Affichage sur 3 lignes : Mois, Cumuls, Soldes");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Mois");
              riSousMenu_bt7.setToolTipText("Affichage sur 1 ligne  : Mois");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Cumuls");
              riSousMenu_bt8.setToolTipText("Affichage sur 1 ligne  : Cumuls");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Soldes");
              riSousMenu_bt9.setToolTipText("Affichage sur 1 ligne  : Soldes");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Options");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Type de sortie");
              riSousMenu_bt_export.setToolTipText("Type de sortie");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu10);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(850, 420));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("                     "));
            panel1.setOpaque(false);
            panel1.setName("panel1");

            //======== SCROLLPANE_LIST1 ========
            {
              SCROLLPANE_LIST1.setToolTipText("<HTML>\"Double clic\" sur un  compte \u00e0 6 chiffres<BR>pour visualiser le d\u00e9tail</HTML>");
              SCROLLPANE_LIST1.setName("SCROLLPANE_LIST1");

              //---- WTP01 ----
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST1.setViewportView(WTP01);
            }

            //---- WRECH ----
            WRECH.setToolTipText("<HTML>S\u00e9lectionez le nombre de lignes \u00e0 d\u00e9caler,<BR>et valider.</HTML>");
            WRECH.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WRECH.setName("WRECH");
            WRECH.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WRECHActionPerformed(e);
              }
            });

            //---- bt_FIN ----
            bt_FIN.setToolTipText("Affichage \u00e0 la fin de la liste");
            bt_FIN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            bt_FIN.setName("bt_FIN");
            bt_FIN.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bt_FINActionPerformed(e);
              }
            });

            //---- bt_DEB ----
            bt_DEB.setToolTipText("Affichage au d\u00e9but de la liste");
            bt_DEB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            bt_DEB.setName("bt_DEB");
            bt_DEB.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bt_DEBActionPerformed(e);
              }
            });

            //---- WLIBDV ----
            WLIBDV.setText("@DEMOPE@");
            WLIBDV.setName("WLIBDV");

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");

            //---- label1 ----
            label1.setText("Relation entre axes");
            label1.setName("label1");

            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(524, 524, 524)
                      .addComponent(WRECH, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(14, 14, 14)
                      .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addComponent(label1, GroupLayout.PREFERRED_SIZE, 194, GroupLayout.PREFERRED_SIZE)
                          .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                          .addComponent(WLIBDV, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE))
                        .addComponent(SCROLLPANE_LIST1, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 740, GroupLayout.PREFERRED_SIZE))
                      .addGap(5, 5, 5)
                      .addGroup(panel1Layout.createParallelGroup()
                        .addComponent(bt_DEB, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                        .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                        .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                        .addComponent(bt_FIN, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))))
                  .addContainerGap(12, Short.MAX_VALUE))
            );
            panel1Layout.setVerticalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(9, 9, 9)
                  .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(WLIBDV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(label1))
                  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(SCROLLPANE_LIST1, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(bt_DEB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(bt_FIN, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
                  .addGap(10, 10, 10)
                  .addComponent(WRECH, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addContainerGap(13, Short.MAX_VALUE))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_35 ----
      OBJ_35.setText("D\u00e9tail");
      OBJ_35.setName("OBJ_35");
      OBJ_35.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_35ActionPerformed(e);
        }
      });
      BTD.add(OBJ_35);

      //---- OBJ_36 ----
      OBJ_36.setText("Aide en ligne");
      OBJ_36.setName("OBJ_36");
      OBJ_36.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_36ActionPerformed(e);
        }
      });
      BTD.add(OBJ_36);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_45_OBJ_45;
  private XRiTextField DEMDAT;
  private JLabel OBJ_47_OBJ_47;
  private XRiTextField DEMAA1;
  private XRiTextField DEMAA2;
  private XRiTextField DEMAA3;
  private XRiTextField DEMAA4;
  private XRiTextField DEMAA5;
  private XRiTextField DEMAA6;
  private JLabel OBJ_20_OBJ_20;
  private XRiTextField DEMETB;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_LIST1;
  private XRiTable WTP01;
  private XRiComboBox WRECH;
  private JButton bt_FIN;
  private JButton bt_DEB;
  private RiZoneSortie WLIBDV;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JLabel label1;
  private JPopupMenu BTD;
  private JMenuItem OBJ_35;
  private JMenuItem OBJ_36;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
