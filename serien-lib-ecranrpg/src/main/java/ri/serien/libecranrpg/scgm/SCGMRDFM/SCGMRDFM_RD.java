
package ri.serien.libecranrpg.scgm.SCGMRDFM;
// Nom Fichier: pop_SCGMRDFM_FMTRD_956.java

import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JScrollPane;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class SCGMRDFM_RD extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] _RR01_Title = { "Rang", "Date Référente", };
  private String[][] _RR01_Data = { { "RR01", "DT01", }, { "RR02", "DT02", }, { "RR03", "DT03", }, { "RR04", "DT04", },
      { "RR05", "DT05", }, { "RR06", "DT06", }, { "RR07", "DT07", }, { "RR08", "DT08", }, { "RR09", "DT09", }, { "RR10", "DT10", },
      { "RR11", "DT11", }, { "RR12", "DT12", }, { "RR13", "DT13", }, { "RR14", "DT14", }, { "RR15", "DT15", }, };
  private int[] _RR01_Width = { 32, 73, };
  
  public SCGMRDFM_RD(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    RR01.setAspectTable(null, _RR01_Title, _RR01_Data, _RR01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    SCROLLPANE_LIST2 = new JScrollPane();
    RR01 = new XRiTable();
    OBJ_12 = new JLabel();
    OBJ_14 = new JLabel();
    ANNEE = new XRiTextField();
    PPEC = new XRiTextField();

    //======== this ========
    setName("this");
    setLayout(null);

    //======== SCROLLPANE_LIST2 ========
    {
      SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

      //---- RR01 ----
      RR01.setName("RR01");
      SCROLLPANE_LIST2.setViewportView(RR01);
    }
    add(SCROLLPANE_LIST2);
    SCROLLPANE_LIST2.setBounds(21, 69, 125, 139);

    //---- OBJ_12 ----
    OBJ_12.setText("Ann\u00e9e");
    OBJ_12.setName("OBJ_12");
    add(OBJ_12);
    OBJ_12.setBounds(21, 20, 60, 20);

    //---- OBJ_14 ----
    OBJ_14.setText("P\u00e9riode");
    OBJ_14.setName("OBJ_14");
    add(OBJ_14);
    OBJ_14.setBounds(21, 44, 51, 20);

    //---- ANNEE ----
    ANNEE.setName("ANNEE");
    add(ANNEE);
    ANNEE.setBounds(90, 20, 38, 20);

    //---- PPEC ----
    PPEC.setName("PPEC");
    add(PPEC);
    PPEC.setBounds(90, 44, 26, 20);

    setPreferredSize(new Dimension(171, 222));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable RR01;
  private JLabel OBJ_12;
  private JLabel OBJ_14;
  private XRiTextField ANNEE;
  private XRiTextField PPEC;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
