
package ri.serien.libecranrpg.scgm.SCGM81FM;
// Nom Fichier: b_SCGM81FM_FMTB1_FMTF1_225.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.LayoutStyle;
import javax.swing.border.BevelBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class SCGM81FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public SCGM81FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    l_LOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
    OBJ_41.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SON001@")).trim());
    OBJ_43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGAD1X@")).trim());
    OBJ_40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SOS001@")).trim());
    OBJ_49.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGDE1X@")).trim());
    OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGDE2X@")).trim());
    OBJ_53.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGFE1X@")).trim());
    OBJ_54.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGFE2X@")).trim());
    OBJ_56.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGDP1X@")).trim());
    OBJ_57.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGDP2X@")).trim());
    OBJ_62.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUMPAS@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    OBJ_62.setVisible(lexique.isPresent("NUMPAS"));
    OBJ_57.setVisible(lexique.isPresent("DGDP2X"));
    OBJ_56.setVisible(lexique.isPresent("DGDP1X"));
    OBJ_54.setVisible(lexique.isPresent("DGFE2X"));
    OBJ_53.setVisible(lexique.isPresent("DGFE1X"));
    OBJ_50.setVisible(lexique.isPresent("DGDE2X"));
    OBJ_49.setVisible(lexique.isPresent("DGDE1X"));
    OBJ_40.setVisible(lexique.isPresent("SOS001"));
    OBJ_43.setVisible(lexique.isPresent("DGAD1X"));
    OBJ_41.setVisible(lexique.isPresent("SON001"));
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    // TODO Icones
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    xH_Titre.setIcon(ManagerSessionClient.getInstance().chargerLogoEtablissement(OBJ_40.getText()));
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "Attention");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1");
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@");
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@");
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@");
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@");
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@");
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@");
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_FonctionsActionPerformed(ActionEvent e) {
    CMD.show(bt_Fonctions, 0, bt_Fonctions.getHeight());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    bt_Fonctions = new JButton();
    l_LOCTP = new JLabel();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    P_Centre = new JPanel();
    OBJ_41 = new JLabel();
    OBJ_43 = new JLabel();
    OBJ_61 = new JLabel();
    OBJ_42 = new JLabel();
    OBJ_39 = new JLabel();
    OBJ_40 = new JLabel();
    OBJ_55 = new JLabel();
    OBJ_48 = new JLabel();
    OBJ_52 = new JLabel();
    OBJ_49 = new JLabel();
    OBJ_50 = new JLabel();
    OBJ_53 = new JLabel();
    OBJ_54 = new JLabel();
    OBJ_56 = new JLabel();
    OBJ_57 = new JLabel();
    OBJ_62 = new JLabel();
    OBJ_51 = new JPanel();
    xTitledSeparator1 = new JXTitledSeparator();
    xTitledSeparator2 = new JXTitledSeparator();
    xTitledSeparator4 = new JXTitledSeparator();
    OBJ_58 = new JLabel();
    OBJ_59 = new JLabel();
    OBJ_60 = new JLabel();
    xTitledSeparator5 = new JXTitledSeparator();
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    CellConstraints cc = new CellConstraints();

    //======== this ========
    setPreferredSize(new Dimension(740, 645));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("@TITPG1@ @TITPG2@");
      xH_Titre.setDescription("FM@LOCGRP/+1/@");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 22));
        P_Infos.setPreferredSize(new Dimension(66, 35));
        P_Infos.setName("P_Infos");

        //---- bt_Fonctions ----
        bt_Fonctions.setText("Fonctions");
        bt_Fonctions.setName("bt_Fonctions");
        bt_Fonctions.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_FonctionsActionPerformed(e);
          }
        });

        //---- l_LOCTP ----
        l_LOCTP.setText("@LOCTP@");
        l_LOCTP.setFont(new Font("sansserif", Font.BOLD, 12));
        l_LOCTP.setName("l_LOCTP");

        GroupLayout P_InfosLayout = new GroupLayout(P_Infos);
        P_Infos.setLayout(P_InfosLayout);
        P_InfosLayout.setHorizontalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, P_InfosLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(l_LOCTP, GroupLayout.PREFERRED_SIZE, 176, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 681, Short.MAX_VALUE)
              .addComponent(bt_Fonctions, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
              .addContainerGap())
        );
        P_InfosLayout.setVerticalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGroup(P_InfosLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(bt_Fonctions)
                .addComponent(l_LOCTP, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(141)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");

      //---- OBJ_41 ----
      OBJ_41.setText("@SON001@");
      OBJ_41.setForeground(Color.black);
      OBJ_41.setName("OBJ_41");

      //---- OBJ_43 ----
      OBJ_43.setText("@DGAD1X@");
      OBJ_43.setForeground(Color.black);
      OBJ_43.setName("OBJ_43");

      //---- OBJ_61 ----
      OBJ_61.setText("Dernier num\u00e9ro de passage en compte");
      OBJ_61.setName("OBJ_61");

      //---- OBJ_42 ----
      OBJ_42.setText("Nom ou Raison Sociale");
      OBJ_42.setName("OBJ_42");

      //---- OBJ_39 ----
      OBJ_39.setText("Soci\u00e9t\u00e9");
      OBJ_39.setName("OBJ_39");

      //---- OBJ_40 ----
      OBJ_40.setText("@SOS001@");
      OBJ_40.setForeground(Color.black);
      OBJ_40.setName("OBJ_40");

      //---- OBJ_55 ----
      OBJ_55.setText("Encours");
      OBJ_55.setName("OBJ_55");

      //---- OBJ_48 ----
      OBJ_48.setText("D\u00e9but");
      OBJ_48.setName("OBJ_48");

      //---- OBJ_52 ----
      OBJ_52.setText("Fin");
      OBJ_52.setName("OBJ_52");

      //---- OBJ_49 ----
      OBJ_49.setText("@DGDE1X@");
      OBJ_49.setForeground(Color.black);
      OBJ_49.setName("OBJ_49");

      //---- OBJ_50 ----
      OBJ_50.setText("@DGDE2X@");
      OBJ_50.setForeground(Color.black);
      OBJ_50.setName("OBJ_50");

      //---- OBJ_53 ----
      OBJ_53.setText("@DGFE1X@");
      OBJ_53.setForeground(Color.black);
      OBJ_53.setName("OBJ_53");

      //---- OBJ_54 ----
      OBJ_54.setText("@DGFE2X@");
      OBJ_54.setForeground(Color.black);
      OBJ_54.setName("OBJ_54");

      //---- OBJ_56 ----
      OBJ_56.setText("@DGDP1X@");
      OBJ_56.setForeground(Color.black);
      OBJ_56.setName("OBJ_56");

      //---- OBJ_57 ----
      OBJ_57.setText("@DGDP2X@");
      OBJ_57.setForeground(Color.black);
      OBJ_57.setName("OBJ_57");

      //---- OBJ_62 ----
      OBJ_62.setText("@NUMPAS@");
      OBJ_62.setForeground(Color.black);
      OBJ_62.setName("OBJ_62");

      //======== OBJ_51 ========
      {
        OBJ_51.setName("OBJ_51");
        OBJ_51.setLayout(null);
      }

      //---- xTitledSeparator1 ----
      xTitledSeparator1.setTitle("Soci\u00e9t\u00e9");
      xTitledSeparator1.setName("xTitledSeparator1");

      //---- xTitledSeparator2 ----
      xTitledSeparator2.setTitle("Exercice en cours");
      xTitledSeparator2.setName("xTitledSeparator2");

      //---- xTitledSeparator4 ----
      xTitledSeparator4.setTitle("Exercice suivant");
      xTitledSeparator4.setName("xTitledSeparator4");

      //---- OBJ_58 ----
      OBJ_58.setText("D\u00e9but");
      OBJ_58.setName("OBJ_58");

      //---- OBJ_59 ----
      OBJ_59.setText("Fin");
      OBJ_59.setName("OBJ_59");

      //---- OBJ_60 ----
      OBJ_60.setText("Encours");
      OBJ_60.setName("OBJ_60");

      //---- xTitledSeparator5 ----
      xTitledSeparator5.setTitle(" ");
      xTitledSeparator5.setName("xTitledSeparator5");

      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, 950, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(45, 45, 45)
            .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
            .addGap(103, 103, 103)
            .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 275, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(45, 45, 45)
            .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 163, GroupLayout.PREFERRED_SIZE)
            .addGap(7, 7, 7)
            .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 345, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(215, 215, 215)
            .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 345, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(xTitledSeparator2, GroupLayout.PREFERRED_SIZE, 950, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(45, 45, 45)
            .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
            .addGap(128, 128, 128)
            .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(45, 45, 45)
            .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
            .addGap(128, 128, 128)
            .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 285, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(45, 45, 45)
            .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
            .addGap(118, 118, 118)
            .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 320, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(180, 180, 180)
                .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 320, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(180, 180, 180)
                .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 320, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(436, 436, 436)
                .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 3, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE))
              .addComponent(xTitledSeparator4, GroupLayout.PREFERRED_SIZE, 950, GroupLayout.PREFERRED_SIZE)
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(180, 180, 180)
                .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 320, GroupLayout.PREFERRED_SIZE))))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(xTitledSeparator5, GroupLayout.PREFERRED_SIZE, 950, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(45, 45, 45)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(245, 245, 245)
                .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 285, GroupLayout.PREFERRED_SIZE))
              .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 252, GroupLayout.PREFERRED_SIZE)))
      );
      P_CentreLayout.setVerticalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(30, 30, 30)
            .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(11, 11, 11)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addGap(5, 5, 5)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addGap(5, 5, 5)
            .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
            .addGap(10, 10, 10)
            .addComponent(xTitledSeparator2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(6, 6, 6)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addGap(5, 5, 5)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addGap(5, 5, 5)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addGap(9, 9, 9)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(56, 56, 56)
                .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(81, 81, 81)
                .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(56, 56, 56)
                .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(xTitledSeparator4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(81, 81, 81)
                .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
            .addComponent(xTitledSeparator5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(6, 6, 6)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
      );
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Choix du Papier");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      CMD.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setText("Annuler");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      CMD.add(OBJ_9);
      CMD.addSeparator();

      //---- OBJ_10 ----
      OBJ_10.setText("Exploitation");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      CMD.add(OBJ_10);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Invite");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private JButton bt_Fonctions;
  private JLabel l_LOCTP;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  private JPanel P_Centre;
  private JLabel OBJ_41;
  private JLabel OBJ_43;
  private JLabel OBJ_61;
  private JLabel OBJ_42;
  private JLabel OBJ_39;
  private JLabel OBJ_40;
  private JLabel OBJ_55;
  private JLabel OBJ_48;
  private JLabel OBJ_52;
  private JLabel OBJ_49;
  private JLabel OBJ_50;
  private JLabel OBJ_53;
  private JLabel OBJ_54;
  private JLabel OBJ_56;
  private JLabel OBJ_57;
  private JLabel OBJ_62;
  private JPanel OBJ_51;
  private JXTitledSeparator xTitledSeparator1;
  private JXTitledSeparator xTitledSeparator2;
  private JXTitledSeparator xTitledSeparator4;
  private JLabel OBJ_58;
  private JLabel OBJ_59;
  private JLabel OBJ_60;
  private JXTitledSeparator xTitledSeparator5;
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
