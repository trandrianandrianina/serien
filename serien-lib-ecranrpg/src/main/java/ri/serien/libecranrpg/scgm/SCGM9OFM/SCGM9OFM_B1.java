
package ri.serien.libecranrpg.scgm.SCGM9OFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SCGM9OFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SCGM9OFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    WOPE.setValeurs("ET", WOPE_GRP);
    WOPE_OU.setValeurs("OU");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    // WOPE0.setSelected(lexique.HostFieldGetData("WOPE").equalsIgnoreCase("ET"));
    // WOPE1.setSelected(lexique.HostFieldGetData("WOPE").equalsIgnoreCase("OU"));
    AA6.setVisible(lexique.isTrue("N86"));
    AA5.setVisible(lexique.isTrue("N85"));
    AA4.setVisible(lexique.isTrue("N84"));
    AA3.setVisible(lexique.isTrue("N83"));
    AA2.setVisible(lexique.isTrue("N82"));
    AA1.setVisible(lexique.isTrue("N81"));
    // WOPE0.setVisible( lexique.isTrue("N81"));
    // WOPE1.setVisible( lexique.isTrue("N81"));
    OBJ_18.setVisible(lexique.isTrue("(81) AND (82) AND (83) AND (84) AND (85) AND (86)"));
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITRE@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WOPE0.isSelected())
    // lexique.HostFieldPutData("WOPE", 0, "ET");
    // if (WOPE1.isSelected())
    // lexique.HostFieldPutData("WOPE", 0, "OU");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_30 = new JLabel();
    AA1 = new XRiTextField();
    AA2 = new XRiTextField();
    AA3 = new XRiTextField();
    AA4 = new XRiTextField();
    AA5 = new XRiTextField();
    AA6 = new XRiTextField();
    WOPE_OU = new XRiRadioButton();
    WOPE = new XRiRadioButton();
    OBJ_18 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    WOPE_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(610, 150));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Recherche sur axes"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_30 ----
          OBJ_30.setText("Relation entre les axes");
          OBJ_30.setName("OBJ_30");
          panel1.add(OBJ_30);
          OBJ_30.setBounds(25, 80, 180, 20);

          //---- AA1 ----
          AA1.setComponentPopupMenu(BTD);
          AA1.setName("AA1");
          panel1.add(AA1);
          AA1.setBounds(25, 35, 54, AA1.getPreferredSize().height);

          //---- AA2 ----
          AA2.setComponentPopupMenu(BTD);
          AA2.setName("AA2");
          panel1.add(AA2);
          AA2.setBounds(90, 35, 54, AA2.getPreferredSize().height);

          //---- AA3 ----
          AA3.setComponentPopupMenu(BTD);
          AA3.setName("AA3");
          panel1.add(AA3);
          AA3.setBounds(150, 35, 54, AA3.getPreferredSize().height);

          //---- AA4 ----
          AA4.setComponentPopupMenu(BTD);
          AA4.setName("AA4");
          panel1.add(AA4);
          AA4.setBounds(215, 35, 54, AA4.getPreferredSize().height);

          //---- AA5 ----
          AA5.setComponentPopupMenu(BTD);
          AA5.setName("AA5");
          panel1.add(AA5);
          AA5.setBounds(280, 35, 54, AA5.getPreferredSize().height);

          //---- AA6 ----
          AA6.setComponentPopupMenu(BTD);
          AA6.setName("AA6");
          panel1.add(AA6);
          AA6.setBounds(340, 35, 54, AA6.getPreferredSize().height);

          //---- WOPE_OU ----
          WOPE_OU.setText("Ou");
          WOPE_OU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WOPE_OU.setName("WOPE_OU");
          panel1.add(WOPE_OU);
          WOPE_OU.setBounds(280, 80, 42, 20);

          //---- WOPE ----
          WOPE.setText("Et");
          WOPE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WOPE.setName("WOPE");
          panel1.add(WOPE);
          WOPE.setBounds(215, 80, 37, 20);

          //---- OBJ_18 ----
          OBJ_18.setText("Pas de gestion par axe sur cet \u00e9tablissement");
          OBJ_18.setFont(OBJ_18.getFont().deriveFont(OBJ_18.getFont().getStyle() | Font.BOLD, OBJ_18.getFont().getSize() + 3f));
          OBJ_18.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_18.setName("OBJ_18");
          panel1.add(OBJ_18);
          OBJ_18.setBounds(5, 55, 410, 30);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 420, 130);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- WOPE_GRP ----
    WOPE_GRP.add(WOPE_OU);
    WOPE_GRP.add(WOPE);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_30;
  private XRiTextField AA1;
  private XRiTextField AA2;
  private XRiTextField AA3;
  private XRiTextField AA4;
  private XRiTextField AA5;
  private XRiTextField AA6;
  private XRiRadioButton WOPE_OU;
  private XRiRadioButton WOPE;
  private JLabel OBJ_18;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  private ButtonGroup WOPE_GRP;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
