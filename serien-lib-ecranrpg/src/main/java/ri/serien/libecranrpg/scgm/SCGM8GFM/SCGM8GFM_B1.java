
package ri.serien.libecranrpg.scgm.SCGM8GFM;
// Nom Fichier: b_SCGM8GFM_FMTB1_FMTF1_788.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.LayoutStyle;
import javax.swing.border.BevelBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SCGM8GFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SCGM8GFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    TRT2.setValeursSelection("1", " ");
    TRT1.setValeursSelection("1", " ");
    WTOU.setValeursSelection("**", "  ");
    EDTOBS.setValeursSelection("OUI", "NON");
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    l_LOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    OBJ_41_OBJ_41.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    OBJ_48_OBJ_48.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBCOL@")).trim());
    OBJ_44_OBJ_44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MESCLO@")).trim());
    OBJ_43_OBJ_43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGPERX@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    CODJO.setVisible(!lexique.isTrue("91"));
    WSOC.setVisible(lexique.isPresent("WSOC"));
    AUXIL2.setVisible(!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**") & lexique.isPresent("AUXIL2"));
    AUXIL1.setVisible(!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**") & lexique.isPresent("AUXIL1"));
    NUMCHQ.setVisible(lexique.isPresent("NUMCHQ"));
    COLLEC.setEnabled(lexique.isPresent("COLLEC"));
    TYPCH.setVisible(lexique.isPresent("TYPCH"));
    OBJ_43_OBJ_43.setVisible(lexique.isPresent("DGPERX"));
    OBJ_44_OBJ_44.setVisible(lexique.isPresent("MESCLO"));
    // WTOU.setVisible( lexique.isPresent("WTOU"));
    // WTOU.setSelected(lexique.HostFieldGetData("WTOU").equalsIgnoreCase("**"));
    OBJ_47_OBJ_47.setVisible(!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**"));
    OBJ_45_OBJ_45.setVisible(!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**"));
    OBJ_55_OBJ_55.setVisible(!lexique.isTrue("91"));
    // TRT2.setSelected(lexique.HostFieldGetData("TRT2").equalsIgnoreCase("1"));
    // EDTOBS.setVisible( lexique.isPresent("EDTOBS"));
    // EDTOBS.setSelected(lexique.HostFieldGetData("EDTOBS").equalsIgnoreCase("OUI"));
    // TRT1.setSelected(lexique.HostFieldGetData("TRT1").equalsIgnoreCase("1"));
    OBJ_48_OBJ_48.setVisible(lexique.isPresent("LIBCOL"));
    OBJ_41_OBJ_41.setVisible(lexique.isPresent("DGNOM"));
    OBJ_50_OBJ_50.setVisible(false);
    OBJ_53_OBJ_53.setVisible(false);
    OBJ_26.setVisible(!lexique.HostFieldGetData("NUMR").trim().equalsIgnoreCase("1- Liste de Contrôle seulement"));
    panel1.setVisible(lexique.isTrue("91"));
    panel2.setVisible(!lexique.isTrue("91"));
    
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    // TODO Icones
    BT_ChgSoc.setIcon(lexique.chargerImage("images/changer.png", true));
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    xH_Titre.setIcon(lexique.chargerImage("images/logo32.png", true));
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @(TITLE)@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // if (WTOU.isSelected())
    // lexique.HostFieldPutData("WTOU", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU", 0, " ");
    // if (TRT2.isSelected())
    // lexique.HostFieldPutData("TRT2", 0, "1");
    // else
    // lexique.HostFieldPutData("TRT2", 0, " ");
    // if (EDTOBS.isSelected())
    // lexique.HostFieldPutData("EDTOBS", 0, "OUI");
    // else
    // lexique.HostFieldPutData("EDTOBS", 0, "NON");
    // if (TRT1.isSelected())
    // lexique.HostFieldPutData("TRT1", 0, "1");
    // else
    // lexique.HostFieldPutData("TRT1", 0, " ");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F10"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "scgm8g"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_FonctionsActionPerformed(ActionEvent e) {
    CMD.show(bt_Fonctions, 0, bt_Fonctions.getHeight());
  }
  
  private void BT_ChgSocActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1", false);
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@", false);
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@", false);
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@", false);
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@", false);
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@", false);
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@", false);
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void TRT1ActionPerformed(ActionEvent e) {
    if (TRT1.isSelected()) {
      OBJ_50_OBJ_50.setVisible(true);
      TRT2.setVisible(false);
    }
    else {
      OBJ_50_OBJ_50.setVisible(false);
      TRT2.setVisible(true);
    }
  }
  
  private void TRT2ActionPerformed(ActionEvent e) {
    if (TRT2.isSelected()) {
      OBJ_53_OBJ_53.setVisible(true);
      TRT1.setVisible(false);
    }
    else {
      OBJ_53_OBJ_53.setVisible(false);
      TRT1.setVisible(true);
    }
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    if (WTOU.isSelected()) {
      panel3.setVisible(false);
    }
    else {
      panel3.setVisible(true);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    l_LOCTP = new JLabel();
    bt_Fonctions = new JButton();
    P_Centre = new JPanel();
    WSOC = new XRiTextField();
    BT_ChgSoc = new JButton();
    OBJ_22 = new JXTitledSeparator();
    OBJ_39 = new JXTitledSeparator();
    OBJ_25 = new JXTitledSeparator();
    OBJ_24 = new JXTitledSeparator();
    OBJ_41_OBJ_41 = new JLabel();
    OBJ_48_OBJ_48 = new JLabel();
    EDTOBS = new XRiCheckBox();
    OBJ_54_OBJ_54 = new JLabel();
    OBJ_52_OBJ_52 = new JLabel();
    OBJ_55_OBJ_55 = new JLabel();
    WTOU = new XRiCheckBox();
    OBJ_44_OBJ_44 = new JLabel();
    OBJ_46_OBJ_46 = new JLabel();
    OBJ_43_OBJ_43 = new JLabel();
    OBJ_42_OBJ_42 = new JLabel();
    panel2 = new JPanel();
    OBJ_27 = new JXTitledSeparator();
    TRT1 = new XRiCheckBox();
    TRT2 = new XRiCheckBox();
    OBJ_50_OBJ_50 = new JLabel();
    OBJ_53_OBJ_53 = new JLabel();
    panel1 = new JPanel();
    OBJ_26 = new JXTitledSeparator();
    OBJ_51_OBJ_51 = new JLabel();
    NUMPAS = new XRiTextField();
    TYPCH = new XRiTextField();
    CODJO = new XRiTextField();
    COLLEC = new XRiTextField();
    NUMCHQ = new XRiTextField();
    panel3 = new JPanel();
    OBJ_45_OBJ_45 = new JLabel();
    OBJ_47_OBJ_47 = new JLabel();
    AUXIL1 = new XRiTextField();
    AUXIL2 = new XRiTextField();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    CellConstraints cc = new CellConstraints();

    //======== this ========
    setPreferredSize(new Dimension(740, 645));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("@TITPG1@ @TITPG2@");
      xH_Titre.setDescription("FM@LOCGRP/+1/@");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 22));
        P_Infos.setPreferredSize(new Dimension(66, 35));
        P_Infos.setName("P_Infos");

        //---- l_LOCTP ----
        l_LOCTP.setText("@LOCTP@");
        l_LOCTP.setFont(new Font("sansserif", Font.BOLD, 12));
        l_LOCTP.setName("l_LOCTP");

        //---- bt_Fonctions ----
        bt_Fonctions.setText("Fonctions");
        bt_Fonctions.setName("bt_Fonctions");
        bt_Fonctions.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_FonctionsActionPerformed(e);
          }
        });

        GroupLayout P_InfosLayout = new GroupLayout(P_Infos);
        P_Infos.setLayout(P_InfosLayout);
        P_InfosLayout.setHorizontalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, P_InfosLayout.createSequentialGroup()
              .addGap(22, 22, 22)
              .addComponent(l_LOCTP, GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 515, Short.MAX_VALUE)
              .addComponent(bt_Fonctions, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
        );
        P_InfosLayout.setVerticalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGroup(P_InfosLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(bt_Fonctions)
                .addComponent(l_LOCTP, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");

      //---- WSOC ----
      WSOC.setName("WSOC");

      //---- BT_ChgSoc ----
      BT_ChgSoc.setText("");
      BT_ChgSoc.setToolTipText("Changement d'\u00e9tablissement");
      BT_ChgSoc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ChgSoc.setName("BT_ChgSoc");
      BT_ChgSoc.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ChgSocActionPerformed(e);
        }
      });

      //---- OBJ_22 ----
      OBJ_22.setTitle("Plage de Comptes \u00e0 \u00e9diter");
      OBJ_22.setName("OBJ_22");

      //---- OBJ_39 ----
      OBJ_39.setTitle("Soci\u00e9t\u00e9 s\u00e9lectionn\u00e9e");
      OBJ_39.setName("OBJ_39");

      //---- OBJ_25 ----
      OBJ_25.setTitle(" ");
      OBJ_25.setName("OBJ_25");

      //---- OBJ_24 ----
      OBJ_24.setTitle(" ");
      OBJ_24.setName("OBJ_24");

      //---- OBJ_41_OBJ_41 ----
      OBJ_41_OBJ_41.setText("@DGNOM@");
      OBJ_41_OBJ_41.setName("OBJ_41_OBJ_41");

      //---- OBJ_48_OBJ_48 ----
      OBJ_48_OBJ_48.setText("@LIBCOL@");
      OBJ_48_OBJ_48.setName("OBJ_48_OBJ_48");

      //---- EDTOBS ----
      EDTOBS.setText("Edition du bloc-notes \u00e9criture");
      EDTOBS.setComponentPopupMenu(BTD);
      EDTOBS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      EDTOBS.setName("EDTOBS");

      //---- OBJ_54_OBJ_54 ----
      OBJ_54_OBJ_54.setText("Type de Lettre ch\u00e8que \u00e0 utiliser");
      OBJ_54_OBJ_54.setName("OBJ_54_OBJ_54");

      //---- OBJ_52_OBJ_52 ----
      OBJ_52_OBJ_52.setText("N\u00b0 du Premier Ch\u00e8que \u00e0 utiliser");
      OBJ_52_OBJ_52.setName("OBJ_52_OBJ_52");

      //---- OBJ_55_OBJ_55 ----
      OBJ_55_OBJ_55.setText("Code journal de bque \u00e0 \u00e9diter");
      OBJ_55_OBJ_55.setName("OBJ_55_OBJ_55");

      //---- WTOU ----
      WTOU.setText("S\u00e9lection compl\u00e8te des comptes");
      WTOU.setComponentPopupMenu(BTD);
      WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      WTOU.setName("WTOU");
      WTOU.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          WTOUActionPerformed(e);
        }
      });

      //---- OBJ_44_OBJ_44 ----
      OBJ_44_OBJ_44.setText("@MESCLO@");
      OBJ_44_OBJ_44.setName("OBJ_44_OBJ_44");

      //---- OBJ_46_OBJ_46 ----
      OBJ_46_OBJ_46.setText("Collectif \u00e0 traiter");
      OBJ_46_OBJ_46.setName("OBJ_46_OBJ_46");

      //---- OBJ_43_OBJ_43 ----
      OBJ_43_OBJ_43.setText("@DGPERX@");
      OBJ_43_OBJ_43.setName("OBJ_43_OBJ_43");

      //---- OBJ_42_OBJ_42 ----
      OBJ_42_OBJ_42.setText("En cours");
      OBJ_42_OBJ_42.setName("OBJ_42_OBJ_42");

      //======== panel2 ========
      {
        panel2.setName("panel2");
        panel2.setLayout(null);

        //---- OBJ_27 ----
        OBJ_27.setTitle("Option de traitement d\u00e9sir\u00e9");
        OBJ_27.setName("OBJ_27");
        panel2.add(OBJ_27);
        OBJ_27.setBounds(15, 20, 650, OBJ_27.getPreferredSize().height);

        //---- TRT1 ----
        TRT1.setText("1- Liste de Contr\u00f4le seulement");
        TRT1.setComponentPopupMenu(BTD);
        TRT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        TRT1.setName("TRT1");
        TRT1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            TRT1ActionPerformed(e);
          }
        });
        panel2.add(TRT1);
        TRT1.setBounds(35, 55, 204, 20);

        //---- TRT2 ----
        TRT2.setText("2- Liste et G\u00e9n\u00e9ration CHQ");
        TRT2.setComponentPopupMenu(BTD);
        TRT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        TRT2.setName("TRT2");
        TRT2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            TRT2ActionPerformed(e);
          }
        });
        panel2.add(TRT2);
        TRT2.setBounds(35, 90, 180, 20);

        //---- OBJ_50_OBJ_50 ----
        OBJ_50_OBJ_50.setText("Une seule option de traitement");
        OBJ_50_OBJ_50.setName("OBJ_50_OBJ_50");
        panel2.add(OBJ_50_OBJ_50);
        OBJ_50_OBJ_50.setBounds(35, 91, 184, 18);

        //---- OBJ_53_OBJ_53 ----
        OBJ_53_OBJ_53.setText("Une seule option de traitement");
        OBJ_53_OBJ_53.setName("OBJ_53_OBJ_53");
        panel2.add(OBJ_53_OBJ_53);
        OBJ_53_OBJ_53.setBounds(35, 56, 184, 18);
      }

      //======== panel1 ========
      {
        panel1.setName("panel1");
        panel1.setLayout(null);

        //---- OBJ_26 ----
        OBJ_26.setTitle(" ");
        OBJ_26.setName("OBJ_26");
        panel1.add(OBJ_26);
        OBJ_26.setBounds(15, 20, 650, 14);

        //---- OBJ_51_OBJ_51 ----
        OBJ_51_OBJ_51.setText("Num\u00e9ro passage \u00e0 r\u00e9\u00e9diter");
        OBJ_51_OBJ_51.setName("OBJ_51_OBJ_51");
        panel1.add(OBJ_51_OBJ_51);
        OBJ_51_OBJ_51.setBounds(35, 55, 174, 20);

        //---- NUMPAS ----
        NUMPAS.setComponentPopupMenu(BTD);
        NUMPAS.setName("NUMPAS");
        panel1.add(NUMPAS);
        NUMPAS.setBounds(220, 51, 36, NUMPAS.getPreferredSize().height);
      }

      //---- TYPCH ----
      TYPCH.setComponentPopupMenu(BTD);
      TYPCH.setName("TYPCH");

      //---- CODJO ----
      CODJO.setComponentPopupMenu(BTD);
      CODJO.setName("CODJO");

      //---- COLLEC ----
      COLLEC.setComponentPopupMenu(BTD);
      COLLEC.setName("COLLEC");

      //---- NUMCHQ ----
      NUMCHQ.setComponentPopupMenu(BTD);
      NUMCHQ.setName("NUMCHQ");

      //======== panel3 ========
      {
        panel3.setBorder(new BevelBorder(BevelBorder.LOWERED));
        panel3.setName("panel3");
        panel3.setLayout(null);

        //---- OBJ_45_OBJ_45 ----
        OBJ_45_OBJ_45.setText("Compte Auxiliaire D\u00e9but");
        OBJ_45_OBJ_45.setName("OBJ_45_OBJ_45");
        panel3.add(OBJ_45_OBJ_45);
        OBJ_45_OBJ_45.setBounds(15, 15, 165, 20);

        //---- OBJ_47_OBJ_47 ----
        OBJ_47_OBJ_47.setText("Compte Auxiliaire Fin");
        OBJ_47_OBJ_47.setName("OBJ_47_OBJ_47");
        panel3.add(OBJ_47_OBJ_47);
        OBJ_47_OBJ_47.setBounds(15, 45, 147, 20);

        //---- AUXIL1 ----
        AUXIL1.setComponentPopupMenu(BTD);
        AUXIL1.setName("AUXIL1");
        panel3.add(AUXIL1);
        AUXIL1.setBounds(205, 11, 60, AUXIL1.getPreferredSize().height);

        //---- AUXIL2 ----
        AUXIL2.setComponentPopupMenu(BTD);
        AUXIL2.setName("AUXIL2");
        panel3.add(AUXIL2);
        AUXIL2.setBounds(205, 40, 60, AUXIL2.getPreferredSize().height);
      }

      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 650, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(55, 55, 55)
                .addComponent(WSOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                .addGap(48, 48, 48)
                .addGroup(P_CentreLayout.createParallelGroup()
                  .addComponent(OBJ_41_OBJ_41, GroupLayout.PREFERRED_SIZE, 400, GroupLayout.PREFERRED_SIZE)
                  .addGroup(P_CentreLayout.createSequentialGroup()
                    .addComponent(OBJ_42_OBJ_42, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
                    .addGap(7, 7, 7)
                    .addComponent(OBJ_43_OBJ_43, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(OBJ_44_OBJ_44, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE))))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(P_CentreLayout.createParallelGroup()
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 685, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 685, GroupLayout.PREFERRED_SIZE)))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 650, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(55, 55, 55)
                .addComponent(OBJ_54_OBJ_54, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(TYPCH, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(60, 60, 60)
                .addComponent(OBJ_55_OBJ_55, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(CODJO, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(55, 55, 55)
                .addComponent(OBJ_46_OBJ_46, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
                .addGap(80, 80, 80)
                .addComponent(COLLEC, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addGap(50, 50, 50)
                .addComponent(OBJ_48_OBJ_48, GroupLayout.PREFERRED_SIZE, 247, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(55, 55, 55)
                .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                .addGap(60, 60, 60)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(55, 55, 55)
                .addComponent(OBJ_52_OBJ_52, GroupLayout.PREFERRED_SIZE, 187, GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(NUMCHQ, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
                .addGap(52, 52, 52)
                .addComponent(EDTOBS, GroupLayout.PREFERRED_SIZE, 198, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(P_CentreLayout.createParallelGroup()
                  .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, 650, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 650, GroupLayout.PREFERRED_SIZE))))
            .addGap(75, 75, 75))
      );
      P_CentreLayout.setVerticalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(30, 30, 30)
            .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(16, 16, 16)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(WSOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addComponent(OBJ_41_OBJ_41, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(P_CentreLayout.createParallelGroup()
                  .addComponent(OBJ_42_OBJ_42, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_43_OBJ_43, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_44_OBJ_44, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
            .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(17, 17, 17)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_54_OBJ_54, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(TYPCH, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_55_OBJ_55, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(CODJO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGap(2, 2, 2)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_46_OBJ_46, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(COLLEC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_48_OBJ_48, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
            .addGap(21, 21, 21)
            .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(6, 6, 6)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE))
            .addGap(12, 12, 12)
            .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(18, 18, 18)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_52_OBJ_52, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(NUMCHQ, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(EDTOBS, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
      );
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(139)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Choix du Papier");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      CMD.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setText("Annuler");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      CMD.add(OBJ_9);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Invite");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private JLabel l_LOCTP;
  private JButton bt_Fonctions;
  private JPanel P_Centre;
  private XRiTextField WSOC;
  private JButton BT_ChgSoc;
  private JXTitledSeparator OBJ_22;
  private JXTitledSeparator OBJ_39;
  private JXTitledSeparator OBJ_25;
  private JXTitledSeparator OBJ_24;
  private JLabel OBJ_41_OBJ_41;
  private JLabel OBJ_48_OBJ_48;
  private XRiCheckBox EDTOBS;
  private JLabel OBJ_54_OBJ_54;
  private JLabel OBJ_52_OBJ_52;
  private JLabel OBJ_55_OBJ_55;
  private XRiCheckBox WTOU;
  private JLabel OBJ_44_OBJ_44;
  private JLabel OBJ_46_OBJ_46;
  private JLabel OBJ_43_OBJ_43;
  private JLabel OBJ_42_OBJ_42;
  private JPanel panel2;
  private JXTitledSeparator OBJ_27;
  private XRiCheckBox TRT1;
  private XRiCheckBox TRT2;
  private JLabel OBJ_50_OBJ_50;
  private JLabel OBJ_53_OBJ_53;
  private JPanel panel1;
  private JXTitledSeparator OBJ_26;
  private JLabel OBJ_51_OBJ_51;
  private XRiTextField NUMPAS;
  private XRiTextField TYPCH;
  private XRiTextField CODJO;
  private XRiTextField COLLEC;
  private XRiTextField NUMCHQ;
  private JPanel panel3;
  private JLabel OBJ_45_OBJ_45;
  private JLabel OBJ_47_OBJ_47;
  private XRiTextField AUXIL1;
  private XRiTextField AUXIL2;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
