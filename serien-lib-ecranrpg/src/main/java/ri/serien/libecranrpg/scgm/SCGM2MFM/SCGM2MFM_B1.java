
package ri.serien.libecranrpg.scgm.SCGM2MFM;
// Nom Fichier: b_SCGM2MFM_FMTB1_FMTF1_972.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SCGM2MFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public SCGM2MFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    TOP07.setValeursSelection("1", " ");
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
    OBJ_39.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    OBJ_43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MESCLO@")).trim());
    OBJ_42.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGPERX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    WSOC.setVisible(lexique.isPresent("WSOC"));
    OBJ_50.setVisible(lexique.isPresent("NCAFIN"));
    NCADEB.setVisible(lexique.isPresent("NCADEB"));
    DATFIN.setVisible(lexique.isPresent("DATFIN"));
    DATDEB.setVisible(lexique.isPresent("DATDEB"));
    COLEC3.setVisible(lexique.isPresent("COLEC3"));
    COLEC2.setVisible(lexique.isPresent("COLEC2"));
    COLEC1.setVisible(lexique.isPresent("COLEC1"));
    OBJ_42.setVisible(lexique.isPresent("DGPERX"));
    OBJ_59.setVisible(lexique.isPresent("TOP05"));
    OBJ_59.setSelected(lexique.HostFieldGetData("TOP05").equalsIgnoreCase("1"));
    OBJ_62.setVisible(lexique.isPresent("TOP04"));
    OBJ_62.setSelected(lexique.HostFieldGetData("TOP04").equalsIgnoreCase("1"));
    // TOP07.setVisible( lexique.isPresent("TOP07"));
    // TOP07.setSelected(lexique.HostFieldGetData("TOP07").equalsIgnoreCase("1"));
    OBJ_61.setVisible(lexique.isPresent("TOP06"));
    OBJ_61.setSelected(lexique.HostFieldGetData("TOP06").equalsIgnoreCase("1"));
    OBJ_43.setVisible(lexique.isPresent("MESCLO"));
    OBJ_60.setVisible(lexique.isPresent("TOP03"));
    OBJ_60.setSelected(lexique.HostFieldGetData("TOP03").equalsIgnoreCase("1"));
    OBJ_58.setVisible(lexique.isPresent("TOP02"));
    OBJ_58.setSelected(lexique.HostFieldGetData("TOP02").equalsIgnoreCase("1"));
    OBJ_39.setVisible(lexique.isPresent("DGNOM"));
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    // TODO Icones
    BT_ChgSoc.setIcon(lexique.chargerImage("images/changer.png", true));
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    xH_Titre.setIcon(ManagerSessionClient.getInstance().chargerLogoEtablissement(WSOC.getText()));
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ - @TITPG2@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    if (OBJ_59.isSelected()) {
      lexique.HostFieldPutData("TOP05", 0, "1");
    }
    else {
      lexique.HostFieldPutData("TOP05", 0, " ");
    }
    if (OBJ_62.isSelected()) {
      lexique.HostFieldPutData("TOP04", 0, "1");
    }
    else {
      lexique.HostFieldPutData("TOP04", 0, " ");
    }
    // if (TOP07.isSelected())
    // lexique.HostFieldPutData("TOP07", 0, "1");
    // else
    // lexique.HostFieldPutData("TOP07", 0, " ");
    if (OBJ_61.isSelected()) {
      lexique.HostFieldPutData("TOP06", 0, "1");
    }
    else {
      lexique.HostFieldPutData("TOP06", 0, " ");
    }
    if (OBJ_60.isSelected()) {
      lexique.HostFieldPutData("TOP03", 0, "1");
    }
    else {
      lexique.HostFieldPutData("TOP03", 0, " ");
    }
    if (OBJ_58.isSelected()) {
      lexique.HostFieldPutData("TOP02", 0, "1");
    }
    else {
      lexique.HostFieldPutData("TOP02", 0, " ");
    }
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "Attention");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void BT_ChgSocActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1");
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@");
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@");
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@");
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@");
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@");
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@");
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_FonctionsActionPerformed(ActionEvent e) {
    CMD.show(bt_Fonctions, 0, bt_Fonctions.getHeight());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    bt_Fonctions = new JButton();
    label2 = new JLabel();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    P_Centre = new JPanel();
    OBJ_38 = new JXTitledSeparator();
    OBJ_56 = new JXTitledSeparator();
    OBJ_52 = new JXTitledSeparator();
    OBJ_48 = new JXTitledSeparator();
    OBJ_39 = new JLabel();
    OBJ_58 = new JCheckBox();
    OBJ_60 = new JCheckBox();
    OBJ_43 = new JLabel();
    OBJ_62 = new JCheckBox();
    OBJ_42 = new JLabel();
    OBJ_41 = new JLabel();
    COLEC1 = new XRiTextField();
    COLEC2 = new XRiTextField();
    COLEC3 = new XRiTextField();
    DATDEB = new XRiTextField();
    DATFIN = new XRiTextField();
    NCADEB = new XRiTextField();
    OBJ_50 = new XRiTextField();
    WSOC = new XRiTextField();
    OBJ_55 = new JLabel();
    OBJ_51 = new JLabel();
    BT_ChgSoc = new JButton();
    OBJ_44 = new JXTitledSeparator();
    OBJ_57 = new JXTitledSeparator();
    OBJ_59 = new JCheckBox();
    OBJ_61 = new JCheckBox();
    TOP07 = new XRiCheckBox();
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    CellConstraints cc = new CellConstraints();

    //======== this ========
    setPreferredSize(new Dimension(740, 645));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("@TITPG1@ @TITPG2@");
      xH_Titre.setDescription("FM@LOCGRP/+1/@");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 22));
        P_Infos.setPreferredSize(new Dimension(66, 35));
        P_Infos.setName("P_Infos");

        //---- bt_Fonctions ----
        bt_Fonctions.setText("Fonctions");
        bt_Fonctions.setName("bt_Fonctions");
        bt_Fonctions.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_FonctionsActionPerformed(e);
          }
        });

        //---- label2 ----
        label2.setText("@V01F@");
        label2.setFont(new Font("sansserif", Font.BOLD, 12));
        label2.setName("label2");

        GroupLayout P_InfosLayout = new GroupLayout(P_Infos);
        P_Infos.setLayout(P_InfosLayout);
        P_InfosLayout.setHorizontalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, P_InfosLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(label2, GroupLayout.PREFERRED_SIZE, 172, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 685, Short.MAX_VALUE)
              .addComponent(bt_Fonctions, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
              .addContainerGap())
        );
        P_InfosLayout.setVerticalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGroup(P_InfosLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(bt_Fonctions)
                .addComponent(label2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(140)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");

      //---- OBJ_38 ----
      OBJ_38.setTitle("Soci\u00e9t\u00e9 s\u00e9lectionn\u00e9e");
      OBJ_38.setName("OBJ_38");

      //---- OBJ_56 ----
      OBJ_56.setTitle("S\u00e9lection(s)");
      OBJ_56.setName("OBJ_56");

      //---- OBJ_52 ----
      OBJ_52.setTitle("Plage dates \u00e9ch\u00e9ances");
      OBJ_52.setName("OBJ_52");

      //---- OBJ_48 ----
      OBJ_48.setTitle("Auxiliaires \u00e0 s\u00e9lectionner");
      OBJ_48.setName("OBJ_48");

      //---- OBJ_39 ----
      OBJ_39.setText("@DGNOM@");
      OBJ_39.setForeground(Color.gray);
      OBJ_39.setName("OBJ_39");

      //---- OBJ_58 ----
      OBJ_58.setText("Ech\u00e9ances non d\u00e9clar\u00e9es seulement");
      OBJ_58.setComponentPopupMenu(BTD);
      OBJ_58.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_58.setName("OBJ_58");

      //---- OBJ_60 ----
      OBJ_60.setText("Prorogations accept\u00e9es seulement");
      OBJ_60.setComponentPopupMenu(BTD);
      OBJ_60.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_60.setName("OBJ_60");

      //---- OBJ_43 ----
      OBJ_43.setText("@MESCLO@");
      OBJ_43.setForeground(Color.darkGray);
      OBJ_43.setName("OBJ_43");

      //---- OBJ_62 ----
      OBJ_62.setText("Contentieux seulement");
      OBJ_62.setComponentPopupMenu(BTD);
      OBJ_62.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_62.setName("OBJ_62");

      //---- OBJ_42 ----
      OBJ_42.setText("@DGPERX@");
      OBJ_42.setForeground(Color.darkGray);
      OBJ_42.setName("OBJ_42");

      //---- OBJ_41 ----
      OBJ_41.setText("En cours");
      OBJ_41.setName("OBJ_41");

      //---- COLEC1 ----
      COLEC1.setComponentPopupMenu(BTD);
      COLEC1.setName("COLEC1");

      //---- COLEC2 ----
      COLEC2.setComponentPopupMenu(BTD);
      COLEC2.setName("COLEC2");

      //---- COLEC3 ----
      COLEC3.setComponentPopupMenu(BTD);
      COLEC3.setName("COLEC3");

      //---- DATDEB ----
      DATDEB.setComponentPopupMenu(BTD);
      DATDEB.setName("DATDEB");

      //---- DATFIN ----
      DATFIN.setComponentPopupMenu(BTD);
      DATFIN.setName("DATFIN");

      //---- NCADEB ----
      NCADEB.setComponentPopupMenu(BTD);
      NCADEB.setHorizontalAlignment(SwingConstants.CENTER);
      NCADEB.setName("NCADEB");

      //---- OBJ_50 ----
      OBJ_50.setComponentPopupMenu(BTD);
      OBJ_50.setName("OBJ_50");

      //---- WSOC ----
      WSOC.setComponentPopupMenu(BTD);
      WSOC.setName("WSOC");

      //---- OBJ_55 ----
      OBJ_55.setText("au");
      OBJ_55.setName("OBJ_55");

      //---- OBJ_51 ----
      OBJ_51.setText("/");
      OBJ_51.setName("OBJ_51");

      //---- BT_ChgSoc ----
      BT_ChgSoc.setText("");
      BT_ChgSoc.setToolTipText("Changement d'\u00e9tablissement");
      BT_ChgSoc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ChgSoc.setName("BT_ChgSoc");
      BT_ChgSoc.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ChgSocActionPerformed(e);
        }
      });

      //---- OBJ_44 ----
      OBJ_44.setTitle("Collectifs \u00e0 traiter");
      OBJ_44.setName("OBJ_44");

      //---- OBJ_57 ----
      OBJ_57.setTitle("Option(s) de traitement");
      OBJ_57.setName("OBJ_57");

      //---- OBJ_59 ----
      OBJ_59.setText("Edition de la liste");
      OBJ_59.setComponentPopupMenu(BTD);
      OBJ_59.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_59.setName("OBJ_59");

      //---- OBJ_61 ----
      OBJ_61.setText("G\u00e9n\u00e9ration demande prorogation");
      OBJ_61.setComponentPopupMenu(BTD);
      OBJ_61.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_61.setName("OBJ_61");

      //---- TOP07 ----
      TOP07.setText("avec option \"\u00e0 ne pas d\u00e9clarer\"");
      TOP07.setComponentPopupMenu(BTD);
      TOP07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      TOP07.setName("TOP07");

      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 950, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addComponent(WSOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                .addGap(49, 49, 49)
                .addGroup(P_CentreLayout.createParallelGroup()
                  .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 400, GroupLayout.PREFERRED_SIZE)
                  .addGroup(P_CentreLayout.createSequentialGroup()
                    .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
                    .addGap(8, 8, 8)
                    .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                    .addGap(8, 8, 8)
                    .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE))))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 650, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addComponent(COLEC1, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(COLEC2, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(COLEC3, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 650, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addComponent(NCADEB, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addGroup(P_CentreLayout.createParallelGroup()
                  .addGroup(P_CentreLayout.createSequentialGroup()
                    .addGap(7, 7, 7)
                    .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 650, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addComponent(DATDEB, GroupLayout.PREFERRED_SIZE, 62, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(DATFIN, GroupLayout.PREFERRED_SIZE, 62, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 650, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 247, GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 234, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 169, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 650, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addGroup(P_CentreLayout.createParallelGroup()
                  .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 224, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 126, GroupLayout.PREFERRED_SIZE)
                  .addComponent(TOP07, GroupLayout.PREFERRED_SIZE, 213, GroupLayout.PREFERRED_SIZE))))
            .addGap(10, 10, 10))
      );
      P_CentreLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] {OBJ_38, OBJ_44, OBJ_48, OBJ_52, OBJ_56, OBJ_57});
      P_CentreLayout.setVerticalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(30, 30, 30)
            .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(6, 6, 6)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(WSOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(P_CentreLayout.createParallelGroup()
                  .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
            .addGap(10, 10, 10)
            .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(11, 11, 11)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(COLEC1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(COLEC2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(COLEC3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGap(20, 20, 20)
            .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(6, 6, 6)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(NCADEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_51))
            .addGap(10, 10, 10)
            .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(11, 11, 11)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(DATDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(DATFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(11, 11, 11)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(11, 11, 11)
            .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(TOP07, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
            .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
      );
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Choix du Papier");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      CMD.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setText("Annuler");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      CMD.add(OBJ_9);
      CMD.addSeparator();

      //---- OBJ_10 ----
      OBJ_10.setText("Exploitation");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      CMD.add(OBJ_10);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Invite");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private JButton bt_Fonctions;
  private JLabel label2;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  private JPanel P_Centre;
  private JXTitledSeparator OBJ_38;
  private JXTitledSeparator OBJ_56;
  private JXTitledSeparator OBJ_52;
  private JXTitledSeparator OBJ_48;
  private JLabel OBJ_39;
  private JCheckBox OBJ_58;
  private JCheckBox OBJ_60;
  private JLabel OBJ_43;
  private JCheckBox OBJ_62;
  private JLabel OBJ_42;
  private JLabel OBJ_41;
  private XRiTextField COLEC1;
  private XRiTextField COLEC2;
  private XRiTextField COLEC3;
  private XRiTextField DATDEB;
  private XRiTextField DATFIN;
  private XRiTextField NCADEB;
  private XRiTextField OBJ_50;
  private XRiTextField WSOC;
  private JLabel OBJ_55;
  private JLabel OBJ_51;
  private JButton BT_ChgSoc;
  private JXTitledSeparator OBJ_44;
  private JXTitledSeparator OBJ_57;
  private JCheckBox OBJ_59;
  private JCheckBox OBJ_61;
  private XRiCheckBox TOP07;
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
