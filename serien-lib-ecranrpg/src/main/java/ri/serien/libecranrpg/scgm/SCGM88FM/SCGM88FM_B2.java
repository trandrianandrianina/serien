
package ri.serien.libecranrpg.scgm.SCGM88FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;

/**
 * @author Stéphane Vénéri
 */
public class SCGM88FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] WOPT5_Value = { "1", "2", "3", };
  private String[] WOPT5_Title = { "Débit global de la remise", "Débit unitaire par opération", "Débit global par devise de transfert", };
  private String[] WOPT4_Value = { "", "1", "2", "3", };
  private String[] WOPT4_Title = { "", "Mono date et monodevise", "Mono date et multi devise", "Multi dates et mono devise", };
  
  public SCGM88FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    WOPT1.setValeurs("1");
    WOPT2.setValeurs("1");
    WOPT3.setValeurs("1");
    WOPT5.setValeurs(WOPT5_Value, WOPT5_Title);
    WOPT4.setValeurs(WOPT4_Value, WOPT4_Title);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // lexique.HostFieldPutData("WOPT5", 0, WOPT5_Value[WOPT5.getSelectedIndex()]);
    // lexique.HostFieldPutData("WOPT4", 0, WOPT4_Value[WOPT4.getSelectedIndex()]);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    xTitledSeparator1 = new JXTitledSeparator();
    WOPT1 = new XRiRadioButton();
    WOPT2 = new XRiRadioButton();
    WOPT3 = new XRiRadioButton();
    xTitledSeparator2 = new JXTitledSeparator();
    WOPT4 = new XRiComboBox();
    OBJ_28 = new JLabel();
    WOPTD = new XRiCalendrier();
    xTitledSeparator3 = new JXTitledSeparator();
    OBJ_33 = new JLabel();
    WOPT5 = new XRiComboBox();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(605, 305));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(e -> bouton_validerActionPerformed(e));
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(e -> bouton_retourActionPerformed(e));
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
          
          // ======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");
            
            // ---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);
          
          // ======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");
            
            // ---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Type de sortie");
            riSousMenu_bt6.setToolTipText("Type de sortie");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(e -> riSousMenu_bt6ActionPerformed(e));
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        
        // ======== p_recup ========
        {
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);
          
          // ---- xTitledSeparator1 ----
          xTitledSeparator1.setTitle("Imputation des frais");
          xTitledSeparator1.setName("xTitledSeparator1");
          p_recup.add(xTitledSeparator1);
          xTitledSeparator1.setBounds(15, 10, 390, xTitledSeparator1.getPreferredSize().height);
          
          // ---- WOPT1 ----
          WOPT1.setText("B\u00e9n\u00e9ficiaire");
          WOPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WOPT1.setName("WOPT1");
          p_recup.add(WOPT1);
          WOPT1.setBounds(20, 35, 106, 20);
          
          // ---- WOPT2 ----
          WOPT2.setText("Emetteur B\u00e9n\u00e9ficiaire");
          WOPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WOPT2.setName("WOPT2");
          p_recup.add(WOPT2);
          WOPT2.setBounds(20, 55, 153, 20);
          
          // ---- WOPT3 ----
          WOPT3.setText("Emetteur");
          WOPT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WOPT3.setName("WOPT3");
          p_recup.add(WOPT3);
          WOPT3.setBounds(20, 75, 82, 20);
          
          // ---- xTitledSeparator2 ----
          xTitledSeparator2.setTitle("Indice de remise");
          xTitledSeparator2.setName("xTitledSeparator2");
          p_recup.add(xTitledSeparator2);
          xTitledSeparator2.setBounds(15, 110, 390, xTitledSeparator2.getPreferredSize().height);
          
          // ---- WOPT4 ----
          WOPT4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WOPT4.setName("WOPT4");
          p_recup.add(WOPT4);
          WOPT4.setBounds(20, 135, 205, WOPT4.getPreferredSize().height);
          
          // ---- OBJ_28 ----
          OBJ_28.setText("Date");
          OBJ_28.setName("OBJ_28");
          p_recup.add(OBJ_28);
          OBJ_28.setBounds(245, 138, 45, 20);
          
          // ---- WOPTD ----
          WOPTD.setName("WOPTD");
          p_recup.add(WOPTD);
          WOPTD.setBounds(285, 134, 105, WOPTD.getPreferredSize().height);
          
          // ---- xTitledSeparator3 ----
          xTitledSeparator3.setTitle("");
          xTitledSeparator3.setName("xTitledSeparator3");
          p_recup.add(xTitledSeparator3);
          xTitledSeparator3.setBounds(15, 178, 390, xTitledSeparator3.getPreferredSize().height);
          
          // ---- OBJ_33 ----
          OBJ_33.setText("Type de d\u00e9bit");
          OBJ_33.setName("OBJ_33");
          p_recup.add(OBJ_33);
          OBJ_33.setBounds(20, 193, 303, 20);
          
          // ---- WOPT5 ----
          WOPT5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WOPT5.setName("WOPT5");
          p_recup.add(WOPT5);
          WOPT5.setBounds(20, 218, 240, WOPT5.getPreferredSize().height);
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < p_recup.getComponentCount(); i++) {
              Rectangle bounds = p_recup.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_recup.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_recup.setMinimumSize(preferredSize);
            p_recup.setPreferredSize(preferredSize);
          }
        }
        
        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
            .addGap(20, 20, 20).addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 420, GroupLayout.PREFERRED_SIZE)));
        p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
            .addGap(20, 20, 20).addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)));
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(WOPT1);
    RB_GRP.add(WOPT2);
    RB_GRP.add(WOPT3);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JXTitledSeparator xTitledSeparator1;
  private XRiRadioButton WOPT1;
  private XRiRadioButton WOPT2;
  private XRiRadioButton WOPT3;
  private JXTitledSeparator xTitledSeparator2;
  private XRiComboBox WOPT4;
  private JLabel OBJ_28;
  private XRiCalendrier WOPTD;
  private JXTitledSeparator xTitledSeparator3;
  private JLabel OBJ_33;
  private XRiComboBox WOPT5;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
