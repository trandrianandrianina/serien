
package ri.serien.libecranrpg.scgm.SCGM12FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;

/**
 * @author Stéphane Vénéri
 */
public class SCGM12FM_B4 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SCGM12FM_B4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    RNAS.setValeursSelection("1", " ");
    RNAF.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    RNAF.setEnabled(!lexique.HostFieldGetData("RNAS").trim().equalsIgnoreCase("1"));
    // RNAF.setSelected(lexique.HostFieldGetData("RNAF").equalsIgnoreCase("1"));
    RNAS.setEnabled(!lexique.HostFieldGetData("RNAF").trim().equalsIgnoreCase("1"));
    // RNAS.setSelected(lexique.HostFieldGetData("RNAS").equalsIgnoreCase("1"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (RNAF.isSelected())
    // lexique.HostFieldPutData("RNAF", 0, "1");
    // else
    // lexique.HostFieldPutData("RNAF", 0, " ");
    // if (RNAS.isSelected())
    // lexique.HostFieldPutData("RNAS", 0, "1");
    // else
    // lexique.HostFieldPutData("RNAS", 0, " ");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    RNAF = new XRiCheckBox();
    RNAS = new XRiCheckBox();
    OBJ_23_OBJ_23 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(535, 160));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder("Options d'\u00e9dition"));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");

          //---- RNAF ----
          RNAF.setText("Nature analytique + Affaire");
          RNAF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RNAF.setName("RNAF");

          //---- RNAS ----
          RNAS.setText("Nature analytique + Secteur");
          RNAS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RNAS.setName("RNAS");

          //---- OBJ_23_OBJ_23 ----
          OBJ_23_OBJ_23.setText("OU");
          OBJ_23_OBJ_23.setFont(OBJ_23_OBJ_23.getFont().deriveFont(OBJ_23_OBJ_23.getFont().getStyle() | Font.BOLD, OBJ_23_OBJ_23.getFont().getSize() - 1f));
          OBJ_23_OBJ_23.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_23_OBJ_23.setName("OBJ_23_OBJ_23");

          GroupLayout p_recupLayout = new GroupLayout(p_recup);
          p_recup.setLayout(p_recupLayout);
          p_recupLayout.setHorizontalGroup(
            p_recupLayout.createParallelGroup()
              .addGroup(p_recupLayout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addGroup(p_recupLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                  .addComponent(RNAF, GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
                  .addComponent(RNAS, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(OBJ_23_OBJ_23, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(48, Short.MAX_VALUE))
          );
          p_recupLayout.setVerticalGroup(
            p_recupLayout.createParallelGroup()
              .addGroup(p_recupLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(RNAF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(OBJ_23_OBJ_23, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(RNAS, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(22, Short.MAX_VALUE))
          );
        }
        p_contenu.add(p_recup);
        p_recup.setBounds(10, 10, 345, 140);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private JPanel p_recup;
  private XRiCheckBox RNAF;
  private XRiCheckBox RNAS;
  private JLabel OBJ_23_OBJ_23;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
