
package ri.serien.libecranrpg.scgm.SCGM8MFM;
// Nom Fichier: b_SCGM8MFM_FMTB1_FMTF1_619.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.LayoutStyle;
import javax.swing.border.BevelBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SCGM8MFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SCGM8MFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    l_LOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    OBJ_29_OBJ_29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SON001@")).trim());
    OBJ_30_OBJ_30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGAD1X@")).trim());
    OBJ_27_OBJ_27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SOS001@")).trim());
    OBJ_31_OBJ_31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGDE1X@")).trim());
    OBJ_32_OBJ_32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGDP1X@")).trim());
    OBJ_35_OBJ_35.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGDE2X@")).trim());
    OBJ_36_OBJ_36.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGFE2X@")).trim());
    OBJ_37_OBJ_37.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGDP2X@")).trim());
    OBJ_43_OBJ_43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGFE1X@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    OBJ_43_OBJ_43.setVisible(lexique.isPresent("DGFE1X"));
    OBJ_37_OBJ_37.setVisible(lexique.isPresent("DGDP2X"));
    OBJ_36_OBJ_36.setVisible(lexique.isPresent("DGFE2X"));
    OBJ_35_OBJ_35.setVisible(lexique.isPresent("DGDE2X"));
    OBJ_32_OBJ_32.setVisible(lexique.isPresent("DGDP1X"));
    OBJ_31_OBJ_31.setVisible(lexique.isPresent("DGDE1X"));
    SOM001.setEnabled(lexique.isPresent("SOM001"));
    OBJ_27_OBJ_27.setVisible(lexique.isPresent("SOS001"));
    OBJ_30_OBJ_30.setVisible(lexique.isPresent("DGAD1X"));
    OBJ_29_OBJ_29.setVisible(lexique.isPresent("SON001"));
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    // TODO Icones
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    xH_Titre.setIcon(lexique.chargerImage("images/logo32.png", true));
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F10"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "scgm8m"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bt_FonctionsActionPerformed(ActionEvent e) {
    CMD.show(bt_Fonctions, 0, bt_Fonctions.getHeight());
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1");
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@");
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@");
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@");
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@");
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@");
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@");
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    l_LOCTP = new JLabel();
    bt_Fonctions = new JButton();
    P_Centre = new JPanel();
    SOM001 = new XRiTextField();
    OBJ_29_OBJ_29 = new JLabel();
    OBJ_30_OBJ_30 = new JLabel();
    OBJ_28_OBJ_28 = new JLabel();
    OBJ_33_OBJ_33 = new JLabel();
    OBJ_26_OBJ_26 = new JLabel();
    OBJ_27_OBJ_27 = new JLabel();
    OBJ_45_OBJ_45 = new JLabel();
    OBJ_46_OBJ_46 = new JLabel();
    OBJ_47_OBJ_47 = new JLabel();
    OBJ_31_OBJ_31 = new JLabel();
    OBJ_32_OBJ_32 = new JLabel();
    OBJ_35_OBJ_35 = new JLabel();
    OBJ_36_OBJ_36 = new JLabel();
    OBJ_37_OBJ_37 = new JLabel();
    OBJ_43_OBJ_43 = new JLabel();
    xTitledSeparator1 = new JXTitledSeparator();
    xTitledSeparator2 = new JXTitledSeparator();
    xTitledSeparator3 = new JXTitledSeparator();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    CellConstraints cc = new CellConstraints();

    //======== this ========
    setPreferredSize(new Dimension(740, 645));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("@TITPG1@ @TITPG2@");
      xH_Titre.setDescription("FM@LOCGRP/+1/@");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 22));
        P_Infos.setPreferredSize(new Dimension(66, 35));
        P_Infos.setName("P_Infos");

        //---- l_LOCTP ----
        l_LOCTP.setText("@LOCTP@");
        l_LOCTP.setFont(new Font("sansserif", Font.BOLD, 12));
        l_LOCTP.setName("l_LOCTP");

        //---- bt_Fonctions ----
        bt_Fonctions.setText("Fonctions");
        bt_Fonctions.setName("bt_Fonctions");
        bt_Fonctions.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_FonctionsActionPerformed(e);
          }
        });

        GroupLayout P_InfosLayout = new GroupLayout(P_Infos);
        P_Infos.setLayout(P_InfosLayout);
        P_InfosLayout.setHorizontalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(19, 19, 19)
              .addComponent(l_LOCTP, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 463, Short.MAX_VALUE)
              .addComponent(bt_Fonctions, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
        );
        P_InfosLayout.setVerticalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGroup(P_InfosLayout.createParallelGroup()
                .addGroup(P_InfosLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addComponent(l_LOCTP, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addComponent(bt_Fonctions))
              .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");

      //---- SOM001 ----
      SOM001.setComponentPopupMenu(BTD);
      SOM001.setName("SOM001");

      //---- OBJ_29_OBJ_29 ----
      OBJ_29_OBJ_29.setText("@SON001@");
      OBJ_29_OBJ_29.setName("OBJ_29_OBJ_29");

      //---- OBJ_30_OBJ_30 ----
      OBJ_30_OBJ_30.setText("@DGAD1X@");
      OBJ_30_OBJ_30.setName("OBJ_30_OBJ_30");

      //---- OBJ_28_OBJ_28 ----
      OBJ_28_OBJ_28.setText("Nom ou Raison Sociale");
      OBJ_28_OBJ_28.setName("OBJ_28_OBJ_28");

      //---- OBJ_33_OBJ_33 ----
      OBJ_33_OBJ_33.setText("Mois \u00e0 traiter");
      OBJ_33_OBJ_33.setName("OBJ_33_OBJ_33");

      //---- OBJ_26_OBJ_26 ----
      OBJ_26_OBJ_26.setText("Soci\u00e9t\u00e9");
      OBJ_26_OBJ_26.setName("OBJ_26_OBJ_26");

      //---- OBJ_27_OBJ_27 ----
      OBJ_27_OBJ_27.setText("@SOS001@");
      OBJ_27_OBJ_27.setName("OBJ_27_OBJ_27");

      //---- OBJ_45_OBJ_45 ----
      OBJ_45_OBJ_45.setText("D\u00e9but");
      OBJ_45_OBJ_45.setName("OBJ_45_OBJ_45");

      //---- OBJ_46_OBJ_46 ----
      OBJ_46_OBJ_46.setText("Fin");
      OBJ_46_OBJ_46.setName("OBJ_46_OBJ_46");

      //---- OBJ_47_OBJ_47 ----
      OBJ_47_OBJ_47.setText("Mois");
      OBJ_47_OBJ_47.setName("OBJ_47_OBJ_47");

      //---- OBJ_31_OBJ_31 ----
      OBJ_31_OBJ_31.setText("@DGDE1X@");
      OBJ_31_OBJ_31.setName("OBJ_31_OBJ_31");

      //---- OBJ_32_OBJ_32 ----
      OBJ_32_OBJ_32.setText("@DGDP1X@");
      OBJ_32_OBJ_32.setName("OBJ_32_OBJ_32");

      //---- OBJ_35_OBJ_35 ----
      OBJ_35_OBJ_35.setText("@DGDE2X@");
      OBJ_35_OBJ_35.setName("OBJ_35_OBJ_35");

      //---- OBJ_36_OBJ_36 ----
      OBJ_36_OBJ_36.setText("@DGFE2X@");
      OBJ_36_OBJ_36.setName("OBJ_36_OBJ_36");

      //---- OBJ_37_OBJ_37 ----
      OBJ_37_OBJ_37.setText("@DGDP2X@");
      OBJ_37_OBJ_37.setName("OBJ_37_OBJ_37");

      //---- OBJ_43_OBJ_43 ----
      OBJ_43_OBJ_43.setText("@DGFE1X@");
      OBJ_43_OBJ_43.setName("OBJ_43_OBJ_43");

      //---- xTitledSeparator1 ----
      xTitledSeparator1.setTitle("Soci\u00e9t\u00e9");
      xTitledSeparator1.setName("xTitledSeparator1");

      //---- xTitledSeparator2 ----
      xTitledSeparator2.setTitle("Exercice en cours");
      xTitledSeparator2.setName("xTitledSeparator2");

      //---- xTitledSeparator3 ----
      xTitledSeparator3.setTitle("Exercice Suivant");
      xTitledSeparator3.setName("xTitledSeparator3");

      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(70, 70, 70)
            .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, 455, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(80, 80, 80)
            .addComponent(OBJ_26_OBJ_26, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
            .addGap(55, 55, 55)
            .addComponent(OBJ_27_OBJ_27, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(80, 80, 80)
            .addComponent(OBJ_28_OBJ_28, GroupLayout.PREFERRED_SIZE, 163, GroupLayout.PREFERRED_SIZE)
            .addGap(7, 7, 7)
            .addComponent(OBJ_29_OBJ_29, GroupLayout.PREFERRED_SIZE, 345, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(250, 250, 250)
            .addComponent(OBJ_30_OBJ_30, GroupLayout.PREFERRED_SIZE, 345, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(80, 80, 80)
            .addComponent(OBJ_33_OBJ_33, GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
            .addGap(52, 52, 52)
            .addComponent(SOM001, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(70, 70, 70)
            .addComponent(xTitledSeparator2, GroupLayout.PREFERRED_SIZE, 195, GroupLayout.PREFERRED_SIZE)
            .addGap(90, 90, 90)
            .addComponent(xTitledSeparator3, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(80, 80, 80)
            .addComponent(OBJ_45_OBJ_45, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
            .addGap(63, 63, 63)
            .addComponent(OBJ_31_OBJ_31, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
            .addGap(130, 130, 130)
            .addComponent(OBJ_35_OBJ_35, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(80, 80, 80)
            .addComponent(OBJ_46_OBJ_46, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
            .addGap(63, 63, 63)
            .addComponent(OBJ_43_OBJ_43, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
            .addGap(130, 130, 130)
            .addComponent(OBJ_36_OBJ_36, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(80, 80, 80)
            .addComponent(OBJ_47_OBJ_47, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
            .addGap(63, 63, 63)
            .addComponent(OBJ_32_OBJ_32, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
            .addGap(130, 130, 130)
            .addComponent(OBJ_37_OBJ_37, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
      );
      P_CentreLayout.setVerticalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(40, 40, 40)
            .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(21, 21, 21)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_26_OBJ_26, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_27_OBJ_27, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addGap(5, 5, 5)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_28_OBJ_28, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_29_OBJ_29, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addGap(5, 5, 5)
            .addComponent(OBJ_30_OBJ_30, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
            .addGap(10, 10, 10)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_33_OBJ_33, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(SOM001, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGap(27, 27, 27)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(xTitledSeparator2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(xTitledSeparator3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGap(16, 16, 16)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_45_OBJ_45, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_31_OBJ_31, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_35_OBJ_35, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addGap(5, 5, 5)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_46_OBJ_46, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_43_OBJ_43, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_36_OBJ_36, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addGap(5, 5, 5)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_47_OBJ_47, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_32_OBJ_32, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_37_OBJ_37, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
      );
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(140)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("R\u00e9afficher");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      CMD.add(OBJ_6);

      //---- OBJ_7 ----
      OBJ_7.setText("Choix du Papier");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Annuler");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      CMD.add(OBJ_8);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private JLabel l_LOCTP;
  private JButton bt_Fonctions;
  private JPanel P_Centre;
  private XRiTextField SOM001;
  private JLabel OBJ_29_OBJ_29;
  private JLabel OBJ_30_OBJ_30;
  private JLabel OBJ_28_OBJ_28;
  private JLabel OBJ_33_OBJ_33;
  private JLabel OBJ_26_OBJ_26;
  private JLabel OBJ_27_OBJ_27;
  private JLabel OBJ_45_OBJ_45;
  private JLabel OBJ_46_OBJ_46;
  private JLabel OBJ_47_OBJ_47;
  private JLabel OBJ_31_OBJ_31;
  private JLabel OBJ_32_OBJ_32;
  private JLabel OBJ_35_OBJ_35;
  private JLabel OBJ_36_OBJ_36;
  private JLabel OBJ_37_OBJ_37;
  private JLabel OBJ_43_OBJ_43;
  private JXTitledSeparator xTitledSeparator1;
  private JXTitledSeparator xTitledSeparator2;
  private JXTitledSeparator xTitledSeparator3;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
