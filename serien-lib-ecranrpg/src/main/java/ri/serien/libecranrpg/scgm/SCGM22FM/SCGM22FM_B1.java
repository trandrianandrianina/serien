
package ri.serien.libecranrpg.scgm.SCGM22FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SCGM22FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private boolean modePlanning = false;
  
  public SCGM22FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    REPON2.setValeursSelection("OUI", "NON");
    LIGNE1.setValeursSelection("OUI", "NON");
    REPON1.setValeursSelection("OUI", "NON");
    REP.setValeursSelection("OUI", "NON");
    DAT2.setValeursSelection("X", " ");
    DAT1.setValeursSelection("X", " ");
    DETEXC.setValeursSelection("OUI", "NON");
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    OBJ_55.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBCOL@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MESCLO@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSOC@")).trim());
    z_wencx_2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGPERX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    modePlanning = lexique.isTrue("81");
    // Message planning
    lbPlanning.setVisible(modePlanning);
    if (modePlanning) {
      Message message = Message.getMessageImportant("Vous êtes en paramétrage de la mise au planning pour cette demande.");
      lbPlanning.setMessage(message);
    }
    
    OBJ_55.setVisible(lexique.isPresent("LIBCOL"));
    CODREP_CHK.setSelected(lexique.HostFieldGetData("CODREP").equalsIgnoreCase("**"));
    P_SEL0.setVisible(!CODREP_CHK.isSelected());
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@"));
    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    if (CODREP_CHK.isSelected()) {
      lexique.HostFieldPutData("CODREP", 0, "**");
    }
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void CODREP1ActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
    if (!CODREP_CHK.isSelected()) {
      CODREP.setText("");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledSeparator2 = new JXTitledSeparator();
    OBJ_47 = new JLabel();
    PERIO1 = new XRiTextField();
    OBJ_49 = new JLabel();
    WNBM = new XRiTextField();
    DAT1 = new XRiCheckBox();
    DAT2 = new XRiCheckBox();
    xTitledSeparator3 = new JXTitledSeparator();
    OBJ_54 = new JLabel();
    COLLEC = new XRiTextField();
    OBJ_55 = new RiZoneSortie();
    xTitledSeparator4 = new JXTitledSeparator();
    OBJ_58 = new JLabel();
    AUXIL1 = new XRiTextField();
    OBJ_59 = new JLabel();
    AUXIL2 = new XRiTextField();
    xTitledSeparator5 = new JXTitledSeparator();
    CODREP_CHK = new JCheckBox();
    P_SEL0 = new JPanel();
    OBJ_66 = new JLabel();
    CODREP = new XRiTextField();
    REP = new XRiCheckBox();
    REPON1 = new XRiCheckBox();
    LIGNE1 = new XRiCheckBox();
    DETEXC = new XRiCheckBox();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    z_wencx_2 = new RiZoneSortie();
    label2 = new JLabel();
    REPON2 = new XRiCheckBox();
    lbPlanning = new SNMessage();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
          
          // ---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(e -> bouton_validerActionPerformed(e));
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(e -> bouton_retourActionPerformed(e));
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 200));
          menus_haut.setPreferredSize(new Dimension(160, 200));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
          
          // ======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");
            
            // ---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);
          
          // ======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");
            
            // ---- riSousMenu_bt_export ----
            riSousMenu_bt_export.setText("Type de sortie");
            riSousMenu_bt_export.setToolTipText("Type de sortie");
            riSousMenu_bt_export.setName("riSousMenu_bt_export");
            riSousMenu_bt_export.addActionListener(e -> riSousMenu_bt_exportActionPerformed(e));
            riSousMenu6.add(riSousMenu_bt_export);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(700, 550));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          
          // ---- xTitledSeparator2 ----
          xTitledSeparator2.setTitle("");
          xTitledSeparator2.setName("xTitledSeparator2");
          
          // ---- OBJ_47 ----
          OBJ_47.setText("P\u00e9riode r\u00e9f\u00e9rence");
          OBJ_47.setName("OBJ_47");
          
          // ---- PERIO1 ----
          PERIO1.setComponentPopupMenu(BTD);
          PERIO1.setToolTipText("sous la forme MMAA ou MM.AA");
          PERIO1.setName("PERIO1");
          
          // ---- OBJ_49 ----
          OBJ_49.setText("Nombre de mois par p\u00e9riode");
          OBJ_49.setName("OBJ_49");
          
          // ---- WNBM ----
          WNBM.setComponentPopupMenu(BTD);
          WNBM.setName("WNBM");
          
          // ---- DAT1 ----
          DAT1.setText("Date \u00e9ch\u00e9ance");
          DAT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          DAT1.setName("DAT1");
          
          // ---- DAT2 ----
          DAT2.setText("Date de facture");
          DAT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          DAT2.setName("DAT2");
          
          // ---- xTitledSeparator3 ----
          xTitledSeparator3.setTitle("");
          xTitledSeparator3.setName("xTitledSeparator3");
          
          // ---- OBJ_54 ----
          OBJ_54.setText("Collectif \u00e0 traiter");
          OBJ_54.setName("OBJ_54");
          
          // ---- COLLEC ----
          COLLEC.setComponentPopupMenu(BTD);
          COLLEC.setName("COLLEC");
          
          // ---- OBJ_55 ----
          OBJ_55.setText("@LIBCOL@");
          OBJ_55.setName("OBJ_55");
          
          // ---- xTitledSeparator4 ----
          xTitledSeparator4.setTitle("Plage de comptes auxiliaires \u00e0 \u00e9diter");
          xTitledSeparator4.setName("xTitledSeparator4");
          
          // ---- OBJ_58 ----
          OBJ_58.setText("D\u00e9but");
          OBJ_58.setName("OBJ_58");
          
          // ---- AUXIL1 ----
          AUXIL1.setComponentPopupMenu(BTD);
          AUXIL1.setName("AUXIL1");
          
          // ---- OBJ_59 ----
          OBJ_59.setText("Fin");
          OBJ_59.setName("OBJ_59");
          
          // ---- AUXIL2 ----
          AUXIL2.setComponentPopupMenu(BTD);
          AUXIL2.setName("AUXIL2");
          
          // ---- xTitledSeparator5 ----
          xTitledSeparator5.setTitle("Options d'\u00e9dition");
          xTitledSeparator5.setName("xTitledSeparator5");
          
          // ---- CODREP_CHK ----
          CODREP_CHK.setText("S\u00e9lection de tous les repr\u00e9sentants");
          CODREP_CHK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CODREP_CHK.setName("CODREP_CHK");
          CODREP_CHK.addActionListener(e -> CODREP1ActionPerformed(e));
          
          // ======== P_SEL0 ========
          {
            P_SEL0.setOpaque(false);
            P_SEL0.setName("P_SEL0");
            P_SEL0.setLayout(null);
            
            // ---- OBJ_66 ----
            OBJ_66.setText("Code repr\u00e9sentant");
            OBJ_66.setName("OBJ_66");
            P_SEL0.add(OBJ_66);
            OBJ_66.setBounds(0, 5, 155, 20);
            
            // ---- CODREP ----
            CODREP.setComponentPopupMenu(BTD);
            CODREP.setName("CODREP");
            P_SEL0.add(CODREP);
            CODREP.setBounds(155, 0, 30, CODREP.getPreferredSize().height);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < P_SEL0.getComponentCount(); i++) {
                Rectangle bounds = P_SEL0.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = P_SEL0.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              P_SEL0.setMinimumSize(preferredSize);
              P_SEL0.setPreferredSize(preferredSize);
            }
          }
          
          // ---- REP ----
          REP.setText("Edition du d\u00e9tail de l'ant\u00e9rieur");
          REP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REP.setName("REP");
          
          // ---- REPON1 ----
          REPON1.setText("Remplacement des z\u00e9ros par des tirets si le montant est nul");
          REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON1.setName("REPON1");
          
          // ---- LIGNE1 ----
          LIGNE1.setText("Edition d'une ligne par client");
          LIGNE1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          LIGNE1.setName("LIGNE1");
          
          // ---- DETEXC ----
          DETEXC.setText("D\u00e9tail des factures");
          DETEXC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          DETEXC.setName("DETEXC");
          
          // ---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");
          
          // ---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");
          
          // ---- z_wencx_ ----
          z_wencx_.setText("@MESCLO@");
          z_wencx_.setName("z_wencx_");
          
          // ---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WSOC@");
          z_etablissement_.setName("z_etablissement_");
          
          // ---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(e -> bouton_etablissementActionPerformed(e));
          
          // ---- z_wencx_2 ----
          z_wencx_2.setText("@DGPERX@");
          z_wencx_2.setFont(z_wencx_2.getFont().deriveFont(z_wencx_2.getFont().getStyle() | Font.BOLD));
          z_wencx_2.setName("z_wencx_2");
          
          // ---- label2 ----
          label2.setText("En cours");
          label2.setName("label2");
          
          // ---- REPON2 ----
          REPON2.setText("R\u00e8glement partiel");
          REPON2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON2.setName("REPON2");
          
          // ---- lbPlanning ----
          lbPlanning.setText("Label Planning");
          lbPlanning.setName("lbPlanning");
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout
              .setHorizontalGroup(
                  p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup().addGroup(p_contenuLayout.createParallelGroup()
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(34, 34, 34).addComponent(sep_etablissement,
                              GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(49, 49, 49)
                              .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                              .addComponent(
                                  bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                              .addGap(82, 82, 82)
                              .addGroup(p_contenuLayout.createParallelGroup()
                                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                                  .addGroup(p_contenuLayout.createSequentialGroup()
                                      .addComponent(label2, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                                      .addComponent(z_wencx_2, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                                      .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE))))
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(34, 34, 34).addComponent(xTitledSeparator2,
                              GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(49, 49, 49)
                              .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                              .addComponent(PERIO1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE).addGap(65, 65, 65)
                              .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                              .addComponent(WNBM, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(49, 49, 49).addComponent(DAT1,
                              GroupLayout.PREFERRED_SIZE, 205, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(49, 49, 49).addComponent(DAT2,
                              GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(34, 34, 34).addComponent(xTitledSeparator3,
                              GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(49, 49, 49)
                              .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                              .addComponent(COLLEC, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(15, 15, 15)
                              .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 217, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(34, 34, 34).addComponent(xTitledSeparator4,
                              GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(49, 49, 49)
                              .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                              .addComponent(AUXIL1, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(160, 160, 160)
                              .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                              .addComponent(AUXIL2, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(34, 34, 34).addComponent(xTitledSeparator5,
                              GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(49, 49, 49)
                              .addGroup(p_contenuLayout.createParallelGroup()
                                  .addComponent(CODREP_CHK, GroupLayout.PREFERRED_SIZE, 340, GroupLayout.PREFERRED_SIZE)
                                  .addComponent(REP, GroupLayout.PREFERRED_SIZE, 340, GroupLayout.PREFERRED_SIZE))
                              .addGap(5, 5, 5).addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 205, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(49, 49, 49).addComponent(REPON1,
                              GroupLayout.PREFERRED_SIZE, 385, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(49, 49, 49).addComponent(LIGNE1,
                              GroupLayout.PREFERRED_SIZE, 340, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(49, 49, 49).addComponent(DETEXC,
                              GroupLayout.PREFERRED_SIZE, 340, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(49, 49, 49).addComponent(REPON2,
                              GroupLayout.PREFERRED_SIZE, 340, GroupLayout.PREFERRED_SIZE)))
                          .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                      .addGroup(GroupLayout.Alignment.TRAILING,
                          p_contenuLayout.createSequentialGroup().addContainerGap()
                              .addComponent(lbPlanning, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                              .addContainerGap()));
          p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                  .addComponent(lbPlanning, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(14, 14, 14)
                  .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(11, 11, 11)
                  .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(17, 17, 17).addComponent(z_etablissement_,
                          GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(15, 15, 15).addComponent(bouton_etablissement,
                          GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup()
                          .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addGap(6, 6, 6)
                          .addGroup(p_contenuLayout.createParallelGroup()
                              .addGroup(p_contenuLayout.createSequentialGroup().addGap(5, 5, 5).addComponent(label2))
                              .addComponent(z_wencx_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                              .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGap(16, 16, 16)
                  .addComponent(xTitledSeparator2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(8, 8, 8)
                  .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(PERIO1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WNBM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(4, 4, 4)
                          .addGroup(p_contenuLayout.createParallelGroup()
                              .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                              .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
                  .addGap(12, 12, 12).addComponent(DAT1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(DAT2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(10, 10, 10)
                  .addComponent(xTitledSeparator3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(11, 11, 11)
                  .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE,
                          20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(COLLEC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE,
                          GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(14, 14, 14)
                  .addComponent(xTitledSeparator4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(16, 16, 16)
                  .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(AUXIL1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(AUXIL2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(4, 4, 4)
                          .addGroup(p_contenuLayout.createParallelGroup()
                              .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                              .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
                  .addGap(12, 12, 12)
                  .addComponent(xTitledSeparator5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(6, 6, 6)
                  .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(4, 4, 4)
                          .addComponent(CODREP_CHK, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(3, 3, 3)
                          .addComponent(REP, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                  .addGap(3, 3, 3).addComponent(REPON1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(3, 3, 3)
                  .addComponent(LIGNE1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(3, 3, 3)
                  .addComponent(DETEXC, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(3, 3, 3)
                  .addComponent(REPON2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(e -> OBJ_13ActionPerformed(e));
      BTD.add(OBJ_13);
      
      // ---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(e -> OBJ_12ActionPerformed(e));
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator xTitledSeparator2;
  private JLabel OBJ_47;
  private XRiTextField PERIO1;
  private JLabel OBJ_49;
  private XRiTextField WNBM;
  private XRiCheckBox DAT1;
  private XRiCheckBox DAT2;
  private JXTitledSeparator xTitledSeparator3;
  private JLabel OBJ_54;
  private XRiTextField COLLEC;
  private RiZoneSortie OBJ_55;
  private JXTitledSeparator xTitledSeparator4;
  private JLabel OBJ_58;
  private XRiTextField AUXIL1;
  private JLabel OBJ_59;
  private XRiTextField AUXIL2;
  private JXTitledSeparator xTitledSeparator5;
  private JCheckBox CODREP_CHK;
  private JPanel P_SEL0;
  private JLabel OBJ_66;
  private XRiTextField CODREP;
  private XRiCheckBox REP;
  private XRiCheckBox REPON1;
  private XRiCheckBox LIGNE1;
  private XRiCheckBox DETEXC;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private RiZoneSortie z_wencx_2;
  private JLabel label2;
  private XRiCheckBox REPON2;
  private SNMessage lbPlanning;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
