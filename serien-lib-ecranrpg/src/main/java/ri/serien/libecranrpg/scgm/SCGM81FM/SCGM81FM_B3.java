
package ri.serien.libecranrpg.scgm.SCGM81FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SCGM81FM_B3 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] _PAT001_Top = { "PAT001", "PAT002", "PAT003", "PAT004", "PAT005", "PAT006", "PAT007", "PAT008", "PAT009", "PAT010",
      "PAT011", "PAT012", "PAT013", "PAT014", "PAT015", "PAT016", };
  private String[] _PAT001_Title = { "Code Soc.", "Identification", "Exercice en cours", "Exercice suivant", };
  private String[][] _PAT001_Data = { { "PAS001", "PAN001", "PA1001", "PA2001", }, { "PAS002", "PAN002", "PA1002", "PA2002", },
      { "PAS003", "PAN003", "PA1003", "PA2003", }, { "PAS004", "PAN004", "PA1004", "PA2004", },
      { "PAS005", "PAN005", "PA1005", "PA2005", }, { "PAS006", "PAN006", "PA1006", "PA2006", },
      { "PAS007", "PAN007", "PA1007", "PA2007", }, { "PAS008", "PAN008", "PA1008", "PA2008", },
      { "PAS009", "PAN009", "PA1009", "PA2009", }, { "PAS010", "PAN010", "PA1010", "PA2010", },
      { "PAS011", "PAN011", "PA1011", "PA2011", }, { "PAS012", "PAN012", "PA1012", "PA2012", },
      { "PAS013", "PAN013", "PA1013", "PA2013", }, { "PAS014", "PAN014", "PA1014", "PA2014", },
      { "PAS015", "PAN015", "PA1015", "PA2015", }, { "PAS016", "PAN016", "PA1016", "PA2016", }, };
  private int[] _PAT001_Width = { 33, 204, 141, 141, };
  // private String[][] _LIST_Title_Data_Brut=null;
  
  public SCGM81FM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // PAT001.getTableHeader().setFont(new Font("Courier New", Font.BOLD, 12));
    // ((DefaultTableCellRenderer)PAT001.getTableHeader().getDefaultRenderer()).setHorizontalAlignment( JLabel.LEFT );
    // _LIST_Title_Data_Brut = initTable(LIST);
    
    // Ajout
    initDiverses();
    PAT001.setAspectTable(_PAT001_Top, _PAT001_Title, _PAT001_Data, _PAT001_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    OBJ_51.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUMPAS@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, _LIST_Title_Data_Brut, _PAT001_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    OBJ_51.setVisible(lexique.isPresent("NUMPAS"));
    OBJ_49.setEnabled(lexique.isPresent("WTOU"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void PAT001MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _PAT001_Top, "1", "F6", e);
    if (PAT001.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "F6");
    }
  }
  
  private void OBJ_49ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTOU", 0, "**");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _PAT001_Top, "1", "F6");
    PAT001.setValeurTop("1");
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    OBJ_50 = new JLabel();
    OBJ_51 = new RiZoneSortie();
    xTitledSeparator1 = new JXTitledSeparator();
    SCROLLPANE_LIST = new JScrollPane();
    PAT001 = new XRiTable();
    OBJ_49 = new SNBouton();
    popupMenu1 = new JPopupMenu();
    menuItem1 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Type de sortie");
              riSousMenu_bt_export.setToolTipText("Type de sortie");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(760, 450));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- OBJ_50 ----
          OBJ_50.setText("Dernier num\u00e9ro de passage en compte");
          OBJ_50.setName("OBJ_50");

          //---- OBJ_51 ----
          OBJ_51.setText("@NUMPAS@");
          OBJ_51.setForeground(Color.black);
          OBJ_51.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_51.setName("OBJ_51");

          //---- xTitledSeparator1 ----
          xTitledSeparator1.setTitle("Choix de la soci\u00e9t\u00e9");
          xTitledSeparator1.setName("xTitledSeparator1");

          //======== SCROLLPANE_LIST ========
          {
            SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

            //---- PAT001 ----
            PAT001.setPreferredSize(new Dimension(520, 240));
            PAT001.setComponentPopupMenu(popupMenu1);
            PAT001.setName("PAT001");
            PAT001.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                PAT001MouseClicked(e);
              }
            });
            SCROLLPANE_LIST.setViewportView(PAT001);
          }

          //---- OBJ_49 ----
          OBJ_49.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_49.setToolTipText("S\u00e9lection de toutes les soci\u00e9t\u00e9s");
          OBJ_49.setText("Toutes les soci\u00e9t\u00e9s");
          OBJ_49.setName("OBJ_49");
          OBJ_49.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_49ActionPerformed(e);
            }
          });

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(34, 34, 34)
                    .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(24, 24, 24)
                    .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, 705, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(36, 36, 36)
                    .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                      .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 680, GroupLayout.PREFERRED_SIZE))))
                .addGap(53, 53, 53))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 269, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== popupMenu1 ========
    {
      popupMenu1.setName("popupMenu1");

      //---- menuItem1 ----
      menuItem1.setText("Choisir");
      menuItem1.setName("menuItem1");
      menuItem1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem1ActionPerformed(e);
        }
      });
      popupMenu1.add(menuItem1);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JLabel OBJ_50;
  private RiZoneSortie OBJ_51;
  private JXTitledSeparator xTitledSeparator1;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable PAT001;
  private SNBouton OBJ_49;
  private JPopupMenu popupMenu1;
  private JMenuItem menuItem1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
