
package ri.serien.libecranrpg.scgm.SCGM55FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SpinnerListModel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiSpinner;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SCGM55FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SCGM55FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    RELCRE.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    // RELCRE.setSelected(lexique.HostFieldGetData("RELCRE").equalsIgnoreCase("OUI"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@ @TITPG2@"));
    
    

    
    p_bpresentation.setCodeEtablissement(WSOC.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WSOC.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (RELCRE.isSelected())
    // lexique.HostFieldPutData("RELCRE", 0, "OUI");
    // else
    // lexique.HostFieldPutData("RELCRE", 0, "NON");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void BT_ChgSocActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_41_OBJ_41 = new JLabel();
    WSOC = new XRiTextField();
    BT_ChgSoc = new SNBoutonRecherche();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_55 = new JXTitledSeparator();
    OBJ_39 = new JXTitledSeparator();
    OBJ_40 = new JXTitledSeparator();
    OBJ_47_OBJ_47 = new JLabel();
    OBJ_45_OBJ_45 = new JLabel();
    OBJ_49_OBJ_49 = new JLabel();
    TIT1 = new XRiTextField();
    TIT3 = new XRiTextField();
    TIT5 = new XRiTextField();
    TIT7 = new XRiTextField();
    TIT9 = new XRiTextField();
    TIT11 = new XRiTextField();
    TIT13 = new XRiTextField();
    TIT15 = new XRiTextField();
    TIT2 = new XRiTextField();
    TIT4 = new XRiTextField();
    TIT6 = new XRiTextField();
    TIT8 = new XRiTextField();
    TIT10 = new XRiTextField();
    TIT12 = new XRiTextField();
    TIT14 = new XRiTextField();
    TIT16 = new XRiTextField();
    OBJ_43_OBJ_43 = new JLabel();
    RELCRE = new XRiCheckBox();
    OBJ_52_OBJ_52 = new JLabel();
    MTALE = new XRiTextField();
    MTLIV = new XRiTextField();
    MTCOM = new XRiTextField();
    NBJREF = new XRiTextField();
    DT01 = new XRiSpinner();
    DT03 = new XRiSpinner();
    DT05 = new XRiSpinner();
    DT07 = new XRiSpinner();
    DT09 = new XRiSpinner();
    DT11 = new XRiSpinner();
    DT13 = new XRiSpinner();
    DT15 = new XRiSpinner();
    SG01 = new XRiSpinner();
    SG03 = new XRiSpinner();
    SG05 = new XRiSpinner();
    SG07 = new XRiSpinner();
    SG09 = new XRiSpinner();
    SG11 = new XRiSpinner();
    SG13 = new XRiSpinner();
    SG15 = new XRiSpinner();
    DT02 = new XRiSpinner();
    DT04 = new XRiSpinner();
    DT06 = new XRiSpinner();
    DT08 = new XRiSpinner();
    DT10 = new XRiSpinner();
    DT12 = new XRiSpinner();
    DT14 = new XRiSpinner();
    DT16 = new XRiSpinner();
    SG02 = new XRiSpinner();
    SG04 = new XRiSpinner();
    SG06 = new XRiSpinner();
    SG08 = new XRiSpinner();
    SG10 = new XRiSpinner();
    SG12 = new XRiSpinner();
    SG14 = new XRiSpinner();
    SG16 = new XRiSpinner();
    NBJSGN = new XRiTextField();
    RG01 = new XRiTextField();
    RG03 = new XRiTextField();
    RG05 = new XRiTextField();
    RG07 = new XRiTextField();
    RG09 = new XRiTextField();
    RG11 = new XRiTextField();
    RG13 = new XRiTextField();
    RG15 = new XRiTextField();
    NJ01 = new XRiTextField();
    NJ03 = new XRiTextField();
    NJ05 = new XRiTextField();
    NJ07 = new XRiTextField();
    NJ09 = new XRiTextField();
    NJ11 = new XRiTextField();
    NJ13 = new XRiTextField();
    NJ15 = new XRiTextField();
    RG02 = new XRiTextField();
    RG04 = new XRiTextField();
    RG06 = new XRiTextField();
    RG08 = new XRiTextField();
    RG10 = new XRiTextField();
    RG12 = new XRiTextField();
    RG14 = new XRiTextField();
    RG16 = new XRiTextField();
    NJ02 = new XRiTextField();
    NJ04 = new XRiTextField();
    NJ06 = new XRiTextField();
    NJ08 = new XRiTextField();
    NJ10 = new XRiTextField();
    NJ12 = new XRiTextField();
    NJ14 = new XRiTextField();
    NJ16 = new XRiTextField();
    RELRGL = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Contr\u00f4le des \u00e9ch\u00e9ances d\u00e9pass\u00e9es");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_41_OBJ_41 ----
          OBJ_41_OBJ_41.setText("Soci\u00e9t\u00e9 ");
          OBJ_41_OBJ_41.setName("OBJ_41_OBJ_41");

          //---- WSOC ----
          WSOC.setName("WSOC");

          //---- BT_ChgSoc ----
          BT_ChgSoc.setText("");
          BT_ChgSoc.setToolTipText("Changement d'\u00e9tablissement");
          BT_ChgSoc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_ChgSoc.setName("BT_ChgSoc");
          BT_ChgSoc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              BT_ChgSocActionPerformed(e);
            }
          });

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_41_OBJ_41, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(WSOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_41_OBJ_41))
              .addComponent(WSOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(845, 540));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");

            //---- OBJ_55 ----
            OBJ_55.setTitle("Crit\u00e8res de s\u00e9lection sur mode de r\u00e8glement");
            OBJ_55.setName("OBJ_55");

            //---- OBJ_39 ----
            OBJ_39.setTitle("Montant minimum  pour alerte pour alerte ou blocage");
            OBJ_39.setName("OBJ_39");

            //---- OBJ_40 ----
            OBJ_40.setTitle("Options de s\u00e9lection \u00e9ch\u00e9ance");
            OBJ_40.setName("OBJ_40");

            //---- OBJ_47_OBJ_47 ----
            OBJ_47_OBJ_47.setText("Montant minimum pour blocage des commandes");
            OBJ_47_OBJ_47.setName("OBJ_47_OBJ_47");

            //---- OBJ_45_OBJ_45 ----
            OBJ_45_OBJ_45.setText("Montant minimum pour blocage de la livraison");
            OBJ_45_OBJ_45.setHorizontalAlignment(SwingConstants.LEFT);
            OBJ_45_OBJ_45.setName("OBJ_45_OBJ_45");

            //---- OBJ_49_OBJ_49 ----
            OBJ_49_OBJ_49.setText("Nombre de jours de r\u00e9f\u00e9rence / date du jour");
            OBJ_49_OBJ_49.setName("OBJ_49_OBJ_49");

            //---- TIT1 ----
            TIT1.setEditable(false);
            TIT1.setName("TIT1");

            //---- TIT3 ----
            TIT3.setEditable(false);
            TIT3.setName("TIT3");

            //---- TIT5 ----
            TIT5.setEditable(false);
            TIT5.setName("TIT5");

            //---- TIT7 ----
            TIT7.setEditable(false);
            TIT7.setName("TIT7");

            //---- TIT9 ----
            TIT9.setEditable(false);
            TIT9.setName("TIT9");

            //---- TIT11 ----
            TIT11.setEditable(false);
            TIT11.setName("TIT11");

            //---- TIT13 ----
            TIT13.setEditable(false);
            TIT13.setName("TIT13");

            //---- TIT15 ----
            TIT15.setEditable(false);
            TIT15.setName("TIT15");

            //---- TIT2 ----
            TIT2.setEditable(false);
            TIT2.setName("TIT2");

            //---- TIT4 ----
            TIT4.setEditable(false);
            TIT4.setName("TIT4");

            //---- TIT6 ----
            TIT6.setEditable(false);
            TIT6.setName("TIT6");

            //---- TIT8 ----
            TIT8.setEditable(false);
            TIT8.setName("TIT8");

            //---- TIT10 ----
            TIT10.setEditable(false);
            TIT10.setName("TIT10");

            //---- TIT12 ----
            TIT12.setEditable(false);
            TIT12.setName("TIT12");

            //---- TIT14 ----
            TIT14.setEditable(false);
            TIT14.setName("TIT14");

            //---- TIT16 ----
            TIT16.setEditable(false);
            TIT16.setName("TIT16");

            //---- OBJ_43_OBJ_43 ----
            OBJ_43_OBJ_43.setText("Montant minimum pour d\u00e9clencher une alerte");
            OBJ_43_OBJ_43.setName("OBJ_43_OBJ_43");

            //---- RELCRE ----
            RELCRE.setText("Cr\u00e9dits non point\u00e9s");
            RELCRE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RELCRE.setName("RELCRE");

            //---- OBJ_52_OBJ_52 ----
            OBJ_52_OBJ_52.setText("R\u00e8glement \u00e0 traiter");
            OBJ_52_OBJ_52.setHorizontalAlignment(SwingConstants.LEFT);
            OBJ_52_OBJ_52.setName("OBJ_52_OBJ_52");

            //---- MTALE ----
            MTALE.setComponentPopupMenu(BTD);
            MTALE.setName("MTALE");

            //---- MTLIV ----
            MTLIV.setComponentPopupMenu(BTD);
            MTLIV.setName("MTLIV");

            //---- MTCOM ----
            MTCOM.setComponentPopupMenu(BTD);
            MTCOM.setName("MTCOM");

            //---- NBJREF ----
            NBJREF.setComponentPopupMenu(BTD);
            NBJREF.setName("NBJREF");

            //---- DT01 ----
            DT01.setComponentPopupMenu(BTD);
            DT01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DT01.setModel(new SpinnerListModel(new String[] {"", "E", "F"}));
            DT01.setName("DT01");

            //---- DT03 ----
            DT03.setComponentPopupMenu(BTD);
            DT03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DT03.setModel(new SpinnerListModel(new String[] {"", "E", "F"}));
            DT03.setName("DT03");

            //---- DT05 ----
            DT05.setComponentPopupMenu(BTD);
            DT05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DT05.setModel(new SpinnerListModel(new String[] {"", "E", "F"}));
            DT05.setName("DT05");

            //---- DT07 ----
            DT07.setComponentPopupMenu(BTD);
            DT07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DT07.setModel(new SpinnerListModel(new String[] {"", "E", "F"}));
            DT07.setName("DT07");

            //---- DT09 ----
            DT09.setComponentPopupMenu(BTD);
            DT09.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DT09.setModel(new SpinnerListModel(new String[] {"", "E", "F"}));
            DT09.setName("DT09");

            //---- DT11 ----
            DT11.setComponentPopupMenu(BTD);
            DT11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DT11.setModel(new SpinnerListModel(new String[] {"", "E", "F"}));
            DT11.setName("DT11");

            //---- DT13 ----
            DT13.setComponentPopupMenu(BTD);
            DT13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DT13.setModel(new SpinnerListModel(new String[] {"", "E", "F"}));
            DT13.setName("DT13");

            //---- DT15 ----
            DT15.setComponentPopupMenu(BTD);
            DT15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DT15.setModel(new SpinnerListModel(new String[] {"", "E", "F"}));
            DT15.setName("DT15");

            //---- SG01 ----
            SG01.setComponentPopupMenu(BTD);
            SG01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SG01.setModel(new SpinnerListModel(new String[] {"", "+", "-"}));
            SG01.setName("SG01");

            //---- SG03 ----
            SG03.setComponentPopupMenu(BTD);
            SG03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SG03.setModel(new SpinnerListModel(new String[] {"", "+", "-"}));
            SG03.setName("SG03");

            //---- SG05 ----
            SG05.setComponentPopupMenu(BTD);
            SG05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SG05.setModel(new SpinnerListModel(new String[] {"", "+", "-"}));
            SG05.setName("SG05");

            //---- SG07 ----
            SG07.setComponentPopupMenu(BTD);
            SG07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SG07.setModel(new SpinnerListModel(new String[] {"", "+", "-"}));
            SG07.setName("SG07");

            //---- SG09 ----
            SG09.setComponentPopupMenu(BTD);
            SG09.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SG09.setModel(new SpinnerListModel(new String[] {"", "+", "-"}));
            SG09.setName("SG09");

            //---- SG11 ----
            SG11.setComponentPopupMenu(BTD);
            SG11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SG11.setModel(new SpinnerListModel(new String[] {"", "+", "-"}));
            SG11.setName("SG11");

            //---- SG13 ----
            SG13.setComponentPopupMenu(BTD);
            SG13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SG13.setModel(new SpinnerListModel(new String[] {"", "+", "-"}));
            SG13.setName("SG13");

            //---- SG15 ----
            SG15.setComponentPopupMenu(BTD);
            SG15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SG15.setModel(new SpinnerListModel(new String[] {"", "+", "-"}));
            SG15.setName("SG15");

            //---- DT02 ----
            DT02.setComponentPopupMenu(BTD);
            DT02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DT02.setModel(new SpinnerListModel(new String[] {"", "E", "F"}));
            DT02.setName("DT02");

            //---- DT04 ----
            DT04.setComponentPopupMenu(BTD);
            DT04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DT04.setModel(new SpinnerListModel(new String[] {"", "E", "F"}));
            DT04.setName("DT04");

            //---- DT06 ----
            DT06.setComponentPopupMenu(BTD);
            DT06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DT06.setModel(new SpinnerListModel(new String[] {"", "E", "F"}));
            DT06.setName("DT06");

            //---- DT08 ----
            DT08.setComponentPopupMenu(BTD);
            DT08.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DT08.setModel(new SpinnerListModel(new String[] {"", "E", "F"}));
            DT08.setName("DT08");

            //---- DT10 ----
            DT10.setComponentPopupMenu(BTD);
            DT10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DT10.setModel(new SpinnerListModel(new String[] {"", "E", "F"}));
            DT10.setName("DT10");

            //---- DT12 ----
            DT12.setComponentPopupMenu(BTD);
            DT12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DT12.setModel(new SpinnerListModel(new String[] {"", "E", "F"}));
            DT12.setName("DT12");

            //---- DT14 ----
            DT14.setComponentPopupMenu(BTD);
            DT14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DT14.setModel(new SpinnerListModel(new String[] {"", "E", "F"}));
            DT14.setName("DT14");

            //---- DT16 ----
            DT16.setComponentPopupMenu(BTD);
            DT16.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DT16.setModel(new SpinnerListModel(new String[] {"", "E", "F"}));
            DT16.setName("DT16");

            //---- SG02 ----
            SG02.setComponentPopupMenu(BTD);
            SG02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SG02.setModel(new SpinnerListModel(new String[] {"", "+", "-"}));
            SG02.setName("SG02");

            //---- SG04 ----
            SG04.setComponentPopupMenu(BTD);
            SG04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SG04.setModel(new SpinnerListModel(new String[] {"", "+", "-"}));
            SG04.setName("SG04");

            //---- SG06 ----
            SG06.setComponentPopupMenu(BTD);
            SG06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SG06.setModel(new SpinnerListModel(new String[] {"", "+", "-"}));
            SG06.setName("SG06");

            //---- SG08 ----
            SG08.setComponentPopupMenu(BTD);
            SG08.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SG08.setModel(new SpinnerListModel(new String[] {"", "+", "-"}));
            SG08.setName("SG08");

            //---- SG10 ----
            SG10.setComponentPopupMenu(BTD);
            SG10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SG10.setModel(new SpinnerListModel(new String[] {"", "+", "-"}));
            SG10.setName("SG10");

            //---- SG12 ----
            SG12.setComponentPopupMenu(BTD);
            SG12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SG12.setModel(new SpinnerListModel(new String[] {"", "+", "-"}));
            SG12.setName("SG12");

            //---- SG14 ----
            SG14.setComponentPopupMenu(BTD);
            SG14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SG14.setModel(new SpinnerListModel(new String[] {"", "+", "-"}));
            SG14.setName("SG14");

            //---- SG16 ----
            SG16.setComponentPopupMenu(BTD);
            SG16.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SG16.setModel(new SpinnerListModel(new String[] {"", "+", "-"}));
            SG16.setName("SG16");

            //---- NBJSGN ----
            NBJSGN.setComponentPopupMenu(BTD);
            NBJSGN.setName("NBJSGN");

            //---- RG01 ----
            RG01.setComponentPopupMenu(BTD);
            RG01.setName("RG01");

            //---- RG03 ----
            RG03.setComponentPopupMenu(BTD);
            RG03.setName("RG03");

            //---- RG05 ----
            RG05.setComponentPopupMenu(BTD);
            RG05.setName("RG05");

            //---- RG07 ----
            RG07.setComponentPopupMenu(BTD);
            RG07.setName("RG07");

            //---- RG09 ----
            RG09.setComponentPopupMenu(BTD);
            RG09.setName("RG09");

            //---- RG11 ----
            RG11.setComponentPopupMenu(BTD);
            RG11.setName("RG11");

            //---- RG13 ----
            RG13.setComponentPopupMenu(BTD);
            RG13.setName("RG13");

            //---- RG15 ----
            RG15.setComponentPopupMenu(BTD);
            RG15.setName("RG15");

            //---- NJ01 ----
            NJ01.setComponentPopupMenu(BTD);
            NJ01.setName("NJ01");

            //---- NJ03 ----
            NJ03.setComponentPopupMenu(BTD);
            NJ03.setName("NJ03");

            //---- NJ05 ----
            NJ05.setComponentPopupMenu(BTD);
            NJ05.setName("NJ05");

            //---- NJ07 ----
            NJ07.setComponentPopupMenu(BTD);
            NJ07.setName("NJ07");

            //---- NJ09 ----
            NJ09.setComponentPopupMenu(BTD);
            NJ09.setName("NJ09");

            //---- NJ11 ----
            NJ11.setComponentPopupMenu(BTD);
            NJ11.setName("NJ11");

            //---- NJ13 ----
            NJ13.setComponentPopupMenu(BTD);
            NJ13.setName("NJ13");

            //---- NJ15 ----
            NJ15.setComponentPopupMenu(BTD);
            NJ15.setName("NJ15");

            //---- RG02 ----
            RG02.setComponentPopupMenu(BTD);
            RG02.setName("RG02");

            //---- RG04 ----
            RG04.setComponentPopupMenu(BTD);
            RG04.setName("RG04");

            //---- RG06 ----
            RG06.setComponentPopupMenu(BTD);
            RG06.setName("RG06");

            //---- RG08 ----
            RG08.setComponentPopupMenu(BTD);
            RG08.setName("RG08");

            //---- RG10 ----
            RG10.setComponentPopupMenu(BTD);
            RG10.setName("RG10");

            //---- RG12 ----
            RG12.setComponentPopupMenu(BTD);
            RG12.setName("RG12");

            //---- RG14 ----
            RG14.setComponentPopupMenu(BTD);
            RG14.setName("RG14");

            //---- RG16 ----
            RG16.setComponentPopupMenu(BTD);
            RG16.setName("RG16");

            //---- NJ02 ----
            NJ02.setComponentPopupMenu(BTD);
            NJ02.setName("NJ02");

            //---- NJ04 ----
            NJ04.setComponentPopupMenu(BTD);
            NJ04.setName("NJ04");

            //---- NJ06 ----
            NJ06.setComponentPopupMenu(BTD);
            NJ06.setName("NJ06");

            //---- NJ08 ----
            NJ08.setComponentPopupMenu(BTD);
            NJ08.setName("NJ08");

            //---- NJ10 ----
            NJ10.setComponentPopupMenu(BTD);
            NJ10.setName("NJ10");

            //---- NJ12 ----
            NJ12.setComponentPopupMenu(BTD);
            NJ12.setName("NJ12");

            //---- NJ14 ----
            NJ14.setComponentPopupMenu(BTD);
            NJ14.setName("NJ14");

            //---- NJ16 ----
            NJ16.setComponentPopupMenu(BTD);
            NJ16.setName("NJ16");

            //---- RELRGL ----
            RELRGL.setComponentPopupMenu(BTD);
            RELRGL.setName("RELRGL");

            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 810, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addComponent(OBJ_43_OBJ_43, GroupLayout.PREFERRED_SIZE, 305, GroupLayout.PREFERRED_SIZE)
                  .addGap(2, 2, 2)
                  .addComponent(MTALE, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
                  .addGap(65, 65, 65)
                  .addComponent(OBJ_45_OBJ_45, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                  .addGap(10, 10, 10)
                  .addComponent(MTLIV, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addComponent(OBJ_47_OBJ_47, GroupLayout.PREFERRED_SIZE, 305, GroupLayout.PREFERRED_SIZE)
                  .addGap(2, 2, 2)
                  .addComponent(MTCOM, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 810, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addComponent(OBJ_49_OBJ_49, GroupLayout.PREFERRED_SIZE, 305, GroupLayout.PREFERRED_SIZE)
                  .addGap(2, 2, 2)
                  .addComponent(NBJSGN, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addGap(18, 18, 18)
                  .addComponent(NBJREF, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addGap(145, 145, 145)
                  .addComponent(OBJ_52_OBJ_52, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
                  .addGap(10, 10, 10)
                  .addComponent(RELRGL, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addComponent(RELCRE, GroupLayout.PREFERRED_SIZE, 325, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 810, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(RG01, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(DT01, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NJ01, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(SG01, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(TIT1, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                  .addGap(20, 20, 20)
                  .addComponent(RG02, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(DT02, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NJ02, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(SG02, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(TIT2, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(RG03, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(DT03, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NJ03, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(SG03, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(TIT3, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                  .addGap(20, 20, 20)
                  .addComponent(RG04, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(DT04, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NJ04, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(SG04, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(TIT4, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(RG05, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(DT05, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NJ05, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(SG05, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(TIT5, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                  .addGap(20, 20, 20)
                  .addComponent(RG06, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(DT06, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NJ06, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(SG06, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(TIT6, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(RG07, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(DT07, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NJ07, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(SG07, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(TIT7, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                  .addGap(20, 20, 20)
                  .addComponent(RG08, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(DT08, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NJ08, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(SG08, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(TIT8, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(RG09, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(DT09, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NJ09, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(SG09, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(TIT9, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                  .addGap(20, 20, 20)
                  .addComponent(RG10, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(DT10, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NJ10, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(SG10, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(TIT10, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(RG11, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(DT11, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NJ11, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(SG11, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(TIT11, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                  .addGap(20, 20, 20)
                  .addComponent(RG12, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(DT12, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NJ12, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(SG12, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(TIT12, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(RG13, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(DT13, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NJ13, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(SG13, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(TIT13, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                  .addGap(20, 20, 20)
                  .addComponent(RG14, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(DT14, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NJ14, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(SG14, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(TIT14, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(RG15, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(DT15, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NJ15, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(SG15, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(TIT15, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                  .addGap(20, 20, 20)
                  .addComponent(RG16, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(DT16, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NJ16, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addComponent(SG16, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(TIT16, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE))
            );
            panel1Layout.setVerticalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(21, 21, 21)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_43_OBJ_43, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(MTALE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_45_OBJ_45, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(MTLIV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_47_OBJ_47, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(MTCOM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(16, 16, 16)
                  .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(17, 17, 17)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_49_OBJ_49, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(NBJSGN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NBJREF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_52_OBJ_52, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(RELRGL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(6, 6, 6)
                  .addComponent(RELCRE, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addGap(25, 25, 25)
                  .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(21, 21, 21)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(RG01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DT01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NJ01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SG01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIT1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(RG02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DT02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NJ02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SG02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIT2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(RG03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DT03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NJ03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SG03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIT3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(RG04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DT04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NJ04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SG04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIT4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(RG05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DT05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NJ05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SG05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIT5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(RG06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DT06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NJ06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SG06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIT6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(RG07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DT07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NJ07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SG07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIT7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(RG08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DT08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NJ08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SG08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIT8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(RG09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DT09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NJ09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SG09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIT9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(RG10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DT10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NJ10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SG10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIT10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(RG11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DT11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NJ11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SG11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIT11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(RG12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DT12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NJ12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SG12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIT12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(RG13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DT13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NJ13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SG13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIT13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(RG14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DT14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NJ14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SG14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIT14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(RG15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DT15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NJ15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SG15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIT15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(RG16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DT16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NJ16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SG16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TIT16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }
          p_contenu.add(panel1);
          panel1.setBounds(10, 10, 825, 525);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_41_OBJ_41;
  private XRiTextField WSOC;
  private SNBoutonRecherche BT_ChgSoc;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JXTitledSeparator OBJ_55;
  private JXTitledSeparator OBJ_39;
  private JXTitledSeparator OBJ_40;
  private JLabel OBJ_47_OBJ_47;
  private JLabel OBJ_45_OBJ_45;
  private JLabel OBJ_49_OBJ_49;
  private XRiTextField TIT1;
  private XRiTextField TIT3;
  private XRiTextField TIT5;
  private XRiTextField TIT7;
  private XRiTextField TIT9;
  private XRiTextField TIT11;
  private XRiTextField TIT13;
  private XRiTextField TIT15;
  private XRiTextField TIT2;
  private XRiTextField TIT4;
  private XRiTextField TIT6;
  private XRiTextField TIT8;
  private XRiTextField TIT10;
  private XRiTextField TIT12;
  private XRiTextField TIT14;
  private XRiTextField TIT16;
  private JLabel OBJ_43_OBJ_43;
  private XRiCheckBox RELCRE;
  private JLabel OBJ_52_OBJ_52;
  private XRiTextField MTALE;
  private XRiTextField MTLIV;
  private XRiTextField MTCOM;
  private XRiTextField NBJREF;
  private XRiSpinner DT01;
  private XRiSpinner DT03;
  private XRiSpinner DT05;
  private XRiSpinner DT07;
  private XRiSpinner DT09;
  private XRiSpinner DT11;
  private XRiSpinner DT13;
  private XRiSpinner DT15;
  private XRiSpinner SG01;
  private XRiSpinner SG03;
  private XRiSpinner SG05;
  private XRiSpinner SG07;
  private XRiSpinner SG09;
  private XRiSpinner SG11;
  private XRiSpinner SG13;
  private XRiSpinner SG15;
  private XRiSpinner DT02;
  private XRiSpinner DT04;
  private XRiSpinner DT06;
  private XRiSpinner DT08;
  private XRiSpinner DT10;
  private XRiSpinner DT12;
  private XRiSpinner DT14;
  private XRiSpinner DT16;
  private XRiSpinner SG02;
  private XRiSpinner SG04;
  private XRiSpinner SG06;
  private XRiSpinner SG08;
  private XRiSpinner SG10;
  private XRiSpinner SG12;
  private XRiSpinner SG14;
  private XRiSpinner SG16;
  private XRiTextField NBJSGN;
  private XRiTextField RG01;
  private XRiTextField RG03;
  private XRiTextField RG05;
  private XRiTextField RG07;
  private XRiTextField RG09;
  private XRiTextField RG11;
  private XRiTextField RG13;
  private XRiTextField RG15;
  private XRiTextField NJ01;
  private XRiTextField NJ03;
  private XRiTextField NJ05;
  private XRiTextField NJ07;
  private XRiTextField NJ09;
  private XRiTextField NJ11;
  private XRiTextField NJ13;
  private XRiTextField NJ15;
  private XRiTextField RG02;
  private XRiTextField RG04;
  private XRiTextField RG06;
  private XRiTextField RG08;
  private XRiTextField RG10;
  private XRiTextField RG12;
  private XRiTextField RG14;
  private XRiTextField RG16;
  private XRiTextField NJ02;
  private XRiTextField NJ04;
  private XRiTextField NJ06;
  private XRiTextField NJ08;
  private XRiTextField NJ10;
  private XRiTextField NJ12;
  private XRiTextField NJ14;
  private XRiTextField NJ16;
  private XRiTextField RELRGL;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
