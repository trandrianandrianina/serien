
package ri.serien.libecranrpg.scgm.SCGM1AFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;

/**
 * @author Stéphane Vénéri
 */
public class SCGM1AFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SCGM1AFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    A1LIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    A1CPL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1CPL@")).trim());
    A1RUE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1RUE@")).trim());
    A1LOC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LOC@")).trim());
    A1PAY.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1PAY@")).trim());
    A1VIL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1VIL@")).trim());
    A1CDP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1CDP@")).trim());
    A1COP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1COP@")).trim());
    A1TEL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1TEL@")).trim());
    A1FAX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1FAX@")).trim());
    A1PAC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1PAC@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    riSousMenu6.setVisible(lexique.HostFieldGetData("TYPMOD").trim().equalsIgnoreCase("FRS"));
    riSousMenu9.setVisible(lexique.HostFieldGetData("TYPMOD").trim().equalsIgnoreCase("FRS"));
    riSousMenu7.setVisible(lexique.HostFieldGetData("TYPMOD").trim().equalsIgnoreCase("CLT"));
    riSousMenu8.setVisible(lexique.HostFieldGetData("TYPMOD").trim().equalsIgnoreCase("CLT"));
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Informations complémentaires"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7", true);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", true);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7", true);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", true);
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    A1LIB = new RiZoneSortie();
    A1CPL = new RiZoneSortie();
    A1RUE = new RiZoneSortie();
    A1LOC = new RiZoneSortie();
    A1PAY = new RiZoneSortie();
    A1VIL = new RiZoneSortie();
    A1CDP = new RiZoneSortie();
    A1COP = new RiZoneSortie();
    xTitledSeparator1 = new JXTitledSeparator();
    panel2 = new JPanel();
    OBJ_14 = new JLabel();
    A1TEL = new RiZoneSortie();
    A1FAX = new RiZoneSortie();
    OBJ_17 = new JLabel();
    OBJ_21 = new JLabel();
    A1PAC = new RiZoneSortie();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1000, 290));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Fiche fournisseur");
            riSousMenu_bt6.setToolTipText("Acc\u00e8s \u00e0 la fiche fournisseur");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);

          //======== riSousMenu9 ========
          {
            riSousMenu9.setName("riSousMenu9");

            //---- riSousMenu_bt9 ----
            riSousMenu_bt9.setText("Bloc-notes fournisseur");
            riSousMenu_bt9.setToolTipText("Acc\u00e8s au bloc-notes de la fiche fournisseur");
            riSousMenu_bt9.setName("riSousMenu_bt9");
            riSousMenu_bt9.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt9ActionPerformed(e);
              }
            });
            riSousMenu9.add(riSousMenu_bt9);
          }
          menus_haut.add(riSousMenu9);

          //======== riSousMenu8 ========
          {
            riSousMenu8.setName("riSousMenu8");

            //---- riSousMenu_bt8 ----
            riSousMenu_bt8.setText("Acc\u00e8s \u00e0 la fiche client");
            riSousMenu_bt8.setToolTipText("Acc\u00e8s \u00e0 la fiche client");
            riSousMenu_bt8.setName("riSousMenu_bt8");
            riSousMenu_bt8.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt8ActionPerformed(e);
              }
            });
            riSousMenu8.add(riSousMenu_bt8);
          }
          menus_haut.add(riSousMenu8);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Bloc-notes fiche client");
            riSousMenu_bt7.setToolTipText("Acc\u00e8s au bloc-notes de la fiche client");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);

          //======== riSousMenu10 ========
          {
            riSousMenu10.setName("riSousMenu10");

            //---- riSousMenu_bt10 ----
            riSousMenu_bt10.setText("Bloc-notes PCG");
            riSousMenu_bt10.setToolTipText("Acc\u00e8s au bloc-notes du plan comptable");
            riSousMenu_bt10.setName("riSousMenu_bt10");
            riSousMenu_bt10.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt10ActionPerformed(e);
              }
            });
            riSousMenu10.add(riSousMenu_bt10);
          }
          menus_haut.add(riSousMenu10);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- A1LIB ----
          A1LIB.setFont(A1LIB.getFont().deriveFont(A1LIB.getFont().getStyle() | Font.BOLD));
          A1LIB.setText("@A1LIB@");
          A1LIB.setName("A1LIB");
          panel1.add(A1LIB);
          A1LIB.setBounds(50, 25, 310, A1LIB.getPreferredSize().height);

          //---- A1CPL ----
          A1CPL.setText("@A1CPL@");
          A1CPL.setName("A1CPL");
          panel1.add(A1CPL);
          A1CPL.setBounds(50, 57, 310, A1CPL.getPreferredSize().height);

          //---- A1RUE ----
          A1RUE.setText("@A1RUE@");
          A1RUE.setName("A1RUE");
          panel1.add(A1RUE);
          A1RUE.setBounds(50, 89, 310, A1RUE.getPreferredSize().height);

          //---- A1LOC ----
          A1LOC.setText("@A1LOC@");
          A1LOC.setName("A1LOC");
          panel1.add(A1LOC);
          A1LOC.setBounds(50, 121, 310, A1LOC.getPreferredSize().height);

          //---- A1PAY ----
          A1PAY.setText("@A1PAY@");
          A1PAY.setName("A1PAY");
          panel1.add(A1PAY);
          A1PAY.setBounds(50, 185, 250, A1PAY.getPreferredSize().height);

          //---- A1VIL ----
          A1VIL.setText("@A1VIL@");
          A1VIL.setName("A1VIL");
          panel1.add(A1VIL);
          A1VIL.setBounds(110, 153, 250, A1VIL.getPreferredSize().height);

          //---- A1CDP ----
          A1CDP.setText("@A1CDP@");
          A1CDP.setName("A1CDP");
          panel1.add(A1CDP);
          A1CDP.setBounds(50, 153, 55, A1CDP.getPreferredSize().height);

          //---- A1COP ----
          A1COP.setText("@A1COP@");
          A1COP.setName("A1COP");
          panel1.add(A1COP);
          A1COP.setBounds(320, 185, 40, A1COP.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 35, 405, 235);

        //---- xTitledSeparator1 ----
        xTitledSeparator1.setTitle("Identification");
        xTitledSeparator1.setName("xTitledSeparator1");
        p_contenu.add(xTitledSeparator1);
        xTitledSeparator1.setBounds(10, 10, 810, xTitledSeparator1.getPreferredSize().height);

        //======== panel2 ========
        {
          panel2.setOpaque(false);
          panel2.setBorder(new TitledBorder(""));
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- OBJ_14 ----
          OBJ_14.setText("T\u00e9l\u00e9phone");
          OBJ_14.setName("OBJ_14");
          panel2.add(OBJ_14);
          OBJ_14.setBounds(35, 15, 100, 25);

          //---- A1TEL ----
          A1TEL.setText("@A1TEL@");
          A1TEL.setName("A1TEL");
          panel2.add(A1TEL);
          A1TEL.setBounds(35, 40, 125, A1TEL.getPreferredSize().height);

          //---- A1FAX ----
          A1FAX.setText("@A1FAX@");
          A1FAX.setName("A1FAX");
          panel2.add(A1FAX);
          A1FAX.setBounds(35, 100, 125, A1FAX.getPreferredSize().height);

          //---- OBJ_17 ----
          OBJ_17.setText("Fax");
          OBJ_17.setName("OBJ_17");
          panel2.add(OBJ_17);
          OBJ_17.setBounds(35, 75, 100, 25);

          //---- OBJ_21 ----
          OBJ_21.setText("Contact");
          OBJ_21.setName("OBJ_21");
          panel2.add(OBJ_21);
          OBJ_21.setBounds(35, 135, 100, 25);

          //---- A1PAC ----
          A1PAC.setText("@A1PAC@");
          A1PAC.setName("A1PAC");
          panel2.add(A1PAC);
          A1PAC.setBounds(35, 160, 325, A1PAC.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel2);
        panel2.setBounds(420, 35, 400, 235);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie A1LIB;
  private RiZoneSortie A1CPL;
  private RiZoneSortie A1RUE;
  private RiZoneSortie A1LOC;
  private RiZoneSortie A1PAY;
  private RiZoneSortie A1VIL;
  private RiZoneSortie A1CDP;
  private RiZoneSortie A1COP;
  private JXTitledSeparator xTitledSeparator1;
  private JPanel panel2;
  private JLabel OBJ_14;
  private RiZoneSortie A1TEL;
  private RiZoneSortie A1FAX;
  private JLabel OBJ_17;
  private JLabel OBJ_21;
  private RiZoneSortie A1PAC;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
