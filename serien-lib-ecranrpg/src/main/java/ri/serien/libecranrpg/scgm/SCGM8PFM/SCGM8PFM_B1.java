
package ri.serien.libecranrpg.scgm.SCGM8PFM;
// Nom Fichier: pop_SCGM8PFM_FMTB1_FMTF1_941.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SCGM8PFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SCGM8PFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_24_OBJ_24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MTTORI@")).trim());
    OBJ_33_OBJ_33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MTTDIF@")).trim());
    OBJ_27_OBJ_27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PCEORI@")).trim());
    OBJ_25_OBJ_25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ECHORI@")).trim());
    OBJ_26_OBJ_26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RGLORI@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    OBJ_26_OBJ_26.setVisible(lexique.isPresent("RGLORI"));
    ECH2.setVisible(lexique.isPresent("ECH2"));
    ECH1.setVisible(lexique.isPresent("ECH1"));
    OBJ_25_OBJ_25.setVisible(lexique.isPresent("ECHORI"));
    OBJ_27_OBJ_27.setVisible(lexique.isPresent("PCEORI"));
    OBJ_33_OBJ_33.setVisible(lexique.isPresent("MTTDIF"));
    MTT2.setVisible(lexique.isPresent("MTT2"));
    MTT1.setVisible(lexique.isPresent("MTT1"));
    OBJ_24_OBJ_24.setVisible(lexique.isPresent("MTTORI"));
    
    // TODO Icones
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel3 = new JPanel();
    OBJ_28_OBJ_28 = new JLabel();
    OBJ_23_OBJ_23 = new JLabel();
    OBJ_24_OBJ_24 = new RiZoneSortie();
    MTT1 = new XRiTextField();
    ECH1 = new XRiTextField();
    MTT2 = new XRiTextField();
    ECH2 = new XRiTextField();
    OBJ_33_OBJ_33 = new RiZoneSortie();
    OBJ_20_OBJ_20 = new JLabel();
    OBJ_22_OBJ_22 = new JLabel();
    OBJ_19_OBJ_19 = new JLabel();
    OBJ_27_OBJ_27 = new RiZoneSortie();
    OBJ_25_OBJ_25 = new RiZoneSortie();
    OBJ_26_OBJ_26 = new RiZoneSortie();
    OBJ_21_OBJ_21 = new JLabel();
    xTitledSeparator1 = new JXTitledSeparator();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(690, 230));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel3 ========
        {
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);

          //---- OBJ_28_OBJ_28 ----
          OBJ_28_OBJ_28.setText("Apr\u00e8s d\u00e9coupage");
          OBJ_28_OBJ_28.setFont(OBJ_28_OBJ_28.getFont().deriveFont(OBJ_28_OBJ_28.getFont().getStyle() | Font.BOLD));
          OBJ_28_OBJ_28.setName("OBJ_28_OBJ_28");
          panel3.add(OBJ_28_OBJ_28);
          OBJ_28_OBJ_28.setBounds(30, 114, 130, 20);

          //---- OBJ_23_OBJ_23 ----
          OBJ_23_OBJ_23.setText("Avant d\u00e9coupage");
          OBJ_23_OBJ_23.setFont(OBJ_23_OBJ_23.getFont().deriveFont(OBJ_23_OBJ_23.getFont().getStyle() | Font.BOLD));
          OBJ_23_OBJ_23.setName("OBJ_23_OBJ_23");
          panel3.add(OBJ_23_OBJ_23);
          OBJ_23_OBJ_23.setBounds(30, 79, 128, 20);

          //---- OBJ_24_OBJ_24 ----
          OBJ_24_OBJ_24.setText("@MTTORI@");
          OBJ_24_OBJ_24.setName("OBJ_24_OBJ_24");
          panel3.add(OBJ_24_OBJ_24);
          OBJ_24_OBJ_24.setBounds(170, 75, 100, 28);

          //---- MTT1 ----
          MTT1.setComponentPopupMenu(BTD);
          MTT1.setName("MTT1");
          panel3.add(MTT1);
          MTT1.setBounds(170, 110, 100, 28);

          //---- ECH1 ----
          ECH1.setComponentPopupMenu(BTD);
          ECH1.setName("ECH1");
          panel3.add(ECH1);
          ECH1.setBounds(280, 110, 76, ECH1.getPreferredSize().height);

          //---- MTT2 ----
          MTT2.setComponentPopupMenu(BTD);
          MTT2.setName("MTT2");
          panel3.add(MTT2);
          MTT2.setBounds(170, 140, 100, 28);

          //---- ECH2 ----
          ECH2.setComponentPopupMenu(BTD);
          ECH2.setName("ECH2");
          panel3.add(ECH2);
          ECH2.setBounds(280, 140, 76, ECH2.getPreferredSize().height);

          //---- OBJ_33_OBJ_33 ----
          OBJ_33_OBJ_33.setText("@MTTDIF@");
          OBJ_33_OBJ_33.setName("OBJ_33_OBJ_33");
          panel3.add(OBJ_33_OBJ_33);
          OBJ_33_OBJ_33.setBounds(170, 175, 100, 28);

          //---- OBJ_20_OBJ_20 ----
          OBJ_20_OBJ_20.setText("\u00e9ch\u00e9ance");
          OBJ_20_OBJ_20.setFont(OBJ_20_OBJ_20.getFont().deriveFont(OBJ_20_OBJ_20.getFont().getStyle() | Font.BOLD));
          OBJ_20_OBJ_20.setName("OBJ_20_OBJ_20");
          panel3.add(OBJ_20_OBJ_20);
          OBJ_20_OBJ_20.setBounds(280, 55, 70, 17);

          //---- OBJ_22_OBJ_22 ----
          OBJ_22_OBJ_22.setText("N\u00b0pi\u00e8ce");
          OBJ_22_OBJ_22.setFont(OBJ_22_OBJ_22.getFont().deriveFont(OBJ_22_OBJ_22.getFont().getStyle() | Font.BOLD));
          OBJ_22_OBJ_22.setName("OBJ_22_OBJ_22");
          panel3.add(OBJ_22_OBJ_22);
          OBJ_22_OBJ_22.setBounds(405, 55, 69, 17);

          //---- OBJ_19_OBJ_19 ----
          OBJ_19_OBJ_19.setText("Montant");
          OBJ_19_OBJ_19.setFont(OBJ_19_OBJ_19.getFont().deriveFont(OBJ_19_OBJ_19.getFont().getStyle() | Font.BOLD));
          OBJ_19_OBJ_19.setName("OBJ_19_OBJ_19");
          panel3.add(OBJ_19_OBJ_19);
          OBJ_19_OBJ_19.setBounds(170, 55, 68, 17);

          //---- OBJ_27_OBJ_27 ----
          OBJ_27_OBJ_27.setText("@PCEORI@");
          OBJ_27_OBJ_27.setName("OBJ_27_OBJ_27");
          panel3.add(OBJ_27_OBJ_27);
          OBJ_27_OBJ_27.setBounds(405, 75, 58, 28);

          //---- OBJ_25_OBJ_25 ----
          OBJ_25_OBJ_25.setText("@ECHORI@");
          OBJ_25_OBJ_25.setName("OBJ_25_OBJ_25");
          panel3.add(OBJ_25_OBJ_25);
          OBJ_25_OBJ_25.setBounds(280, 75, 76, 28);

          //---- OBJ_26_OBJ_26 ----
          OBJ_26_OBJ_26.setText("@RGLORI@");
          OBJ_26_OBJ_26.setName("OBJ_26_OBJ_26");
          panel3.add(OBJ_26_OBJ_26);
          OBJ_26_OBJ_26.setBounds(365, 75, 28, 28);

          //---- OBJ_21_OBJ_21 ----
          OBJ_21_OBJ_21.setText("Rg");
          OBJ_21_OBJ_21.setFont(OBJ_21_OBJ_21.getFont().deriveFont(OBJ_21_OBJ_21.getFont().getStyle() | Font.BOLD));
          OBJ_21_OBJ_21.setName("OBJ_21_OBJ_21");
          panel3.add(OBJ_21_OBJ_21);
          OBJ_21_OBJ_21.setBounds(365, 55, 28, 17);

          //---- xTitledSeparator1 ----
          xTitledSeparator1.setTitle("D\u00e9coupage d'une \u00e9criture comptable");
          xTitledSeparator1.setName("xTitledSeparator1");
          panel3.add(xTitledSeparator1);
          xTitledSeparator1.setBounds(20, 20, 470, xTitledSeparator1.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel3.getComponentCount(); i++) {
              Rectangle bounds = panel3.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel3.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel3.setMinimumSize(preferredSize);
            panel3.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel3);
        panel3.setBounds(10, 10, 505, 210);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel3;
  private JLabel OBJ_28_OBJ_28;
  private JLabel OBJ_23_OBJ_23;
  private RiZoneSortie OBJ_24_OBJ_24;
  private XRiTextField MTT1;
  private XRiTextField ECH1;
  private XRiTextField MTT2;
  private XRiTextField ECH2;
  private RiZoneSortie OBJ_33_OBJ_33;
  private JLabel OBJ_20_OBJ_20;
  private JLabel OBJ_22_OBJ_22;
  private JLabel OBJ_19_OBJ_19;
  private RiZoneSortie OBJ_27_OBJ_27;
  private RiZoneSortie OBJ_25_OBJ_25;
  private RiZoneSortie OBJ_26_OBJ_26;
  private JLabel OBJ_21_OBJ_21;
  private JXTitledSeparator xTitledSeparator1;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
