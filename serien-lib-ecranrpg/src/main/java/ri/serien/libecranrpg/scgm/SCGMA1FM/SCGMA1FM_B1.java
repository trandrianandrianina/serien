
package ri.serien.libecranrpg.scgm.SCGMA1FM;
// Nom Fichier: b_SCGMA1FM_FMTB1_FMTF1_254.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.LayoutStyle;
import javax.swing.border.BevelBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SCGMA1FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SCGMA1FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    l_LOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    OBJ_59_OBJ_59.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SO2001@")).trim());
    OBJ_45_OBJ_45.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@BASC@")).trim());
    OBJ_50_OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MSGCLO@")).trim());
    OBJ_44_OBJ_44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOIS@")).trim());
    OBJ_51_OBJ_51.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOIS2@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    SOS001.setVisible(lexique.isPresent("SOS001"));
    SOM001.setVisible(lexique.isPresent("SOM001"));
    DGDP2X.setVisible(lexique.isPresent("DGDP2X"));
    DGFE2X.setVisible(lexique.isPresent("DGFE2X"));
    DGDE2X.setVisible(lexique.isPresent("DGDE2X"));
    DGDP1X.setVisible(lexique.isPresent("DGDP1X"));
    DGFE1X.setVisible(lexique.isPresent("DGFE1X"));
    DGDE1X.setVisible(lexique.isPresent("DGDE1X"));
    OBJ_50_OBJ_50.setVisible(lexique.isPresent("MSGCLO"));
    DGAD1X.setVisible(lexique.isPresent("DGAD1X"));
    SON001.setVisible(lexique.isPresent("SON001"));
    OBJ_59_OBJ_59.setVisible(lexique.isPresent("SO2001"));
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    // TODO Icones
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    xH_Titre.setIcon(lexique.chargerImage("images/logo32.png", true));
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F10"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "scgma1"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_FonctionsActionPerformed(ActionEvent e) {
    CMD.show(bt_Fonctions, 0, bt_Fonctions.getHeight());
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1", false);
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@", false);
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@", false);
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@", false);
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@", false);
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@", false);
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@", false);
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    l_LOCTP = new JLabel();
    bt_Fonctions = new JButton();
    P_Centre = new JPanel();
    SOS001 = new XRiTextField();
    OBJ_59_OBJ_59 = new JLabel();
    SON001 = new XRiTextField();
    DGAD1X = new XRiTextField();
    OBJ_41_OBJ_41 = new JLabel();
    OBJ_45_OBJ_45 = new JLabel();
    OBJ_52_OBJ_52 = new JLabel();
    OBJ_57_OBJ_57 = new JLabel();
    OBJ_50_OBJ_50 = new JLabel();
    OBJ_38_OBJ_38 = new JLabel();
    OBJ_44_OBJ_44 = new JLabel();
    OBJ_51_OBJ_51 = new JLabel();
    DGDE1X = new XRiTextField();
    DGFE1X = new XRiTextField();
    DGDP1X = new XRiTextField();
    DGDE2X = new XRiTextField();
    DGFE2X = new XRiTextField();
    DGDP2X = new XRiTextField();
    SOM001 = new XRiTextField();
    OBJ_47_OBJ_47 = new JLabel();
    OBJ_54_OBJ_54 = new JLabel();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    CellConstraints cc = new CellConstraints();

    //======== this ========
    setPreferredSize(new Dimension(740, 645));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("@TITPG1@ @TITPG2@");
      xH_Titre.setDescription("FM@LOCGRP/+1/@");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 22));
        P_Infos.setPreferredSize(new Dimension(66, 35));
        P_Infos.setName("P_Infos");

        //---- l_LOCTP ----
        l_LOCTP.setText("@LOCTP@");
        l_LOCTP.setFont(new Font("sansserif", Font.BOLD, 12));
        l_LOCTP.setName("l_LOCTP");

        //---- bt_Fonctions ----
        bt_Fonctions.setText("Fonctions");
        bt_Fonctions.setName("bt_Fonctions");
        bt_Fonctions.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_FonctionsActionPerformed(e);
          }
        });

        GroupLayout P_InfosLayout = new GroupLayout(P_Infos);
        P_Infos.setLayout(P_InfosLayout);
        P_InfosLayout.setHorizontalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(19, 19, 19)
              .addComponent(l_LOCTP, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 502, Short.MAX_VALUE)
              .addComponent(bt_Fonctions, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
        );
        P_InfosLayout.setVerticalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGroup(P_InfosLayout.createParallelGroup()
                .addGroup(P_InfosLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addComponent(l_LOCTP, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addComponent(bt_Fonctions))
              .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");

      //---- SOS001 ----
      SOS001.setName("SOS001");

      //---- OBJ_59_OBJ_59 ----
      OBJ_59_OBJ_59.setText("@SO2001@");
      OBJ_59_OBJ_59.setName("OBJ_59_OBJ_59");

      //---- SON001 ----
      SON001.setName("SON001");

      //---- DGAD1X ----
      DGAD1X.setName("DGAD1X");

      //---- OBJ_41_OBJ_41 ----
      OBJ_41_OBJ_41.setText("Nom ou Raison Sociale");
      OBJ_41_OBJ_41.setName("OBJ_41_OBJ_41");

      //---- OBJ_45_OBJ_45 ----
      OBJ_45_OBJ_45.setText("@BASC@");
      OBJ_45_OBJ_45.setName("OBJ_45_OBJ_45");

      //---- OBJ_52_OBJ_52 ----
      OBJ_52_OBJ_52.setText("Exercice Suivant");
      OBJ_52_OBJ_52.setName("OBJ_52_OBJ_52");

      //---- OBJ_57_OBJ_57 ----
      OBJ_57_OBJ_57.setText("P\u00e9riode \u00e0 traiter");
      OBJ_57_OBJ_57.setName("OBJ_57_OBJ_57");

      //---- OBJ_50_OBJ_50 ----
      OBJ_50_OBJ_50.setText("@MSGCLO@");
      OBJ_50_OBJ_50.setName("OBJ_50_OBJ_50");

      //---- OBJ_38_OBJ_38 ----
      OBJ_38_OBJ_38.setText("Soci\u00e9t\u00e9");
      OBJ_38_OBJ_38.setName("OBJ_38_OBJ_38");

      //---- OBJ_44_OBJ_44 ----
      OBJ_44_OBJ_44.setText("@MOIS@");
      OBJ_44_OBJ_44.setName("OBJ_44_OBJ_44");

      //---- OBJ_51_OBJ_51 ----
      OBJ_51_OBJ_51.setText("@MOIS2@");
      OBJ_51_OBJ_51.setName("OBJ_51_OBJ_51");

      //---- DGDE1X ----
      DGDE1X.setName("DGDE1X");

      //---- DGFE1X ----
      DGFE1X.setName("DGFE1X");

      //---- DGDP1X ----
      DGDP1X.setName("DGDP1X");

      //---- DGDE2X ----
      DGDE2X.setName("DGDE2X");

      //---- DGFE2X ----
      DGFE2X.setName("DGFE2X");

      //---- DGDP2X ----
      DGDP2X.setName("DGDP2X");

      //---- SOM001 ----
      SOM001.setComponentPopupMenu(BTD);
      SOM001.setName("SOM001");

      //---- OBJ_47_OBJ_47 ----
      OBJ_47_OBJ_47.setText("\u00e0");
      OBJ_47_OBJ_47.setName("OBJ_47_OBJ_47");

      //---- OBJ_54_OBJ_54 ----
      OBJ_54_OBJ_54.setText("\u00e0");
      OBJ_54_OBJ_54.setName("OBJ_54_OBJ_54");

      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(70, 70, 70)
            .addComponent(OBJ_38_OBJ_38, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
            .addGap(88, 88, 88)
            .addComponent(SOS001, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(70, 70, 70)
            .addComponent(OBJ_41_OBJ_41, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
            .addGap(10, 10, 10)
            .addComponent(SON001, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(225, 225, 225)
            .addComponent(DGAD1X, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(370, 370, 370)
            .addComponent(OBJ_44_OBJ_44, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(70, 70, 70)
            .addComponent(OBJ_45_OBJ_45, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE)
            .addGap(27, 27, 27)
            .addComponent(DGDE1X, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
            .addGap(13, 13, 13)
            .addComponent(OBJ_47_OBJ_47, GroupLayout.PREFERRED_SIZE, 11, GroupLayout.PREFERRED_SIZE)
            .addGap(14, 14, 14)
            .addComponent(DGFE1X, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
            .addGap(3, 3, 3)
            .addComponent(DGDP1X, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
            .addGap(8, 8, 8)
            .addComponent(OBJ_50_OBJ_50, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(370, 370, 370)
            .addComponent(OBJ_51_OBJ_51, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(70, 70, 70)
            .addComponent(OBJ_52_OBJ_52, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
            .addGap(35, 35, 35)
            .addComponent(DGDE2X, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
            .addGap(13, 13, 13)
            .addComponent(OBJ_54_OBJ_54, GroupLayout.PREFERRED_SIZE, 11, GroupLayout.PREFERRED_SIZE)
            .addGap(14, 14, 14)
            .addComponent(DGFE2X, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
            .addGap(3, 3, 3)
            .addComponent(DGDP2X, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(70, 70, 70)
            .addComponent(OBJ_57_OBJ_57, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
            .addGap(39, 39, 39)
            .addComponent(SOM001, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
            .addGap(50, 50, 50)
            .addComponent(OBJ_59_OBJ_59, GroupLayout.PREFERRED_SIZE, 203, GroupLayout.PREFERRED_SIZE))
      );
      P_CentreLayout.setVerticalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(51, 51, 51)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_38_OBJ_38, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(SOS001, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGap(2, 2, 2)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_41_OBJ_41, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(SON001, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGap(1, 1, 1)
            .addComponent(DGAD1X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(7, 7, 7)
            .addComponent(OBJ_44_OBJ_44, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
            .addGap(1, 1, 1)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_45_OBJ_45, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(DGDE1X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_47_OBJ_47, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(DGFE1X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(DGDP1X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_50_OBJ_50, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
            .addGap(11, 11, 11)
            .addComponent(OBJ_51_OBJ_51, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
            .addGap(1, 1, 1)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_52_OBJ_52, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(DGDE2X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_54_OBJ_54, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(DGFE2X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(DGDP2X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGap(2, 2, 2)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_57_OBJ_57, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(SOM001, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_59_OBJ_59, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
      );
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(140)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Choix du Papier");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      CMD.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setText("Annuler");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      CMD.add(OBJ_9);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Invite");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private JLabel l_LOCTP;
  private JButton bt_Fonctions;
  private JPanel P_Centre;
  private XRiTextField SOS001;
  private JLabel OBJ_59_OBJ_59;
  private XRiTextField SON001;
  private XRiTextField DGAD1X;
  private JLabel OBJ_41_OBJ_41;
  private JLabel OBJ_45_OBJ_45;
  private JLabel OBJ_52_OBJ_52;
  private JLabel OBJ_57_OBJ_57;
  private JLabel OBJ_50_OBJ_50;
  private JLabel OBJ_38_OBJ_38;
  private JLabel OBJ_44_OBJ_44;
  private JLabel OBJ_51_OBJ_51;
  private XRiTextField DGDE1X;
  private XRiTextField DGFE1X;
  private XRiTextField DGDP1X;
  private XRiTextField DGDE2X;
  private XRiTextField DGFE2X;
  private XRiTextField DGDP2X;
  private XRiTextField SOM001;
  private JLabel OBJ_47_OBJ_47;
  private JLabel OBJ_54_OBJ_54;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
