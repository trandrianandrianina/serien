
package ri.serien.libecranrpg.scgm.SCGM72FM;
// Nom Fichier: p_SCGM72FM_FMTB0_FMTF1_418.java

import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jdesktop.swingx.JXTitledSeparator;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class SCGM72FM_B0 extends SNPanelEcranRPG implements ioFrame {
  
  
  public SCGM72FM_B0(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    OBJ_24.setIcon(lexique.chargerImage("images/msgbox04.gif", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    OBJ_23 = new JPanel();
    OBJ_26 = new JLabel();
    OBJ_24 = new JLabel();
    OBJ_27 = new JLabel();
    OBJ_21 = new JLabel();
    xTitledSeparator1 = new JXTitledSeparator();

    //======== this ========
    setName("this");
    setLayout(null);

    //======== OBJ_23 ========
    {
      OBJ_23.setName("OBJ_23");
      OBJ_23.setLayout(null);

      //---- OBJ_26 ----
      OBJ_26.setText("Injection des \u00e9critures");
      OBJ_26.setName("OBJ_26");
      OBJ_23.add(OBJ_26);
      OBJ_26.setBounds(210, 12, 190, 28);

      //---- OBJ_24 ----
      OBJ_24.setIcon(new ImageIcon("images/msgbox04.gif"));
      OBJ_24.setName("OBJ_24");
      OBJ_23.add(OBJ_24);
      OBJ_24.setBounds(5, 2, 51, 42);

      //---- OBJ_27 ----
      OBJ_27.setText("Phase 1");
      OBJ_27.setName("OBJ_27");
      OBJ_23.add(OBJ_27);
      OBJ_27.setBounds(549, 24, 51, 20);
    }
    add(OBJ_23);
    OBJ_23.setBounds(10, 95, 607, 48);

    //---- OBJ_21 ----
    OBJ_21.setText("TRAITEMENT EN COURS");
    OBJ_21.setName("OBJ_21");
    add(OBJ_21);
    OBJ_21.setBounds(100, 50, 238, 24);

    //---- xTitledSeparator1 ----
    xTitledSeparator1.setTitle(" ");
    xTitledSeparator1.setName("xTitledSeparator1");
    add(xTitledSeparator1);
    xTitledSeparator1.setBounds(5, 10, 645, xTitledSeparator1.getPreferredSize().height);

    setPreferredSize(new Dimension(660, 160));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel OBJ_23;
  private JLabel OBJ_26;
  private JLabel OBJ_24;
  private JLabel OBJ_27;
  private JLabel OBJ_21;
  private JXTitledSeparator xTitledSeparator1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
