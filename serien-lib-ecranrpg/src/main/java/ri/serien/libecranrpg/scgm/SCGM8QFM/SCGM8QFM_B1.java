
package ri.serien.libecranrpg.scgm.SCGM8QFM;
// Nom Fichier: b_SCGM8QFM_FMTB1_FMTF1_980.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.LayoutStyle;
import javax.swing.border.BevelBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SCGM8QFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SCGM8QFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
    // setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    l_LOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    OBJ_33_OBJ_33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    OBJ_45_OBJ_45.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MESCLO@")).trim());
    OBJ_44_OBJ_44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGPERX@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    REPFIN.setVisible(lexique.isPresent("REPFIN"));
    REPDEB.setVisible(lexique.isPresent("REPDEB"));
    WSOC.setVisible(lexique.isPresent("WSOC"));
    AUXFIN.setVisible(lexique.isPresent("AUXFIN"));
    AUXDEB.setVisible(lexique.isPresent("AUXDEB"));
    // DATFIN.setVisible( lexique.isPresent("DATFIN"));
    // DATDEB.setVisible( lexique.isPresent("DATDEB"));
    GENFIN.setVisible(lexique.isPresent("GENFIN"));
    GENDEB.setVisible(lexique.isPresent("GENDEB"));
    OBJ_44_OBJ_44.setVisible(lexique.isPresent("DGPERX"));
    OBJ_45_OBJ_45.setVisible(lexique.isPresent("MESCLO"));
    OBJ_33_OBJ_33.setVisible(lexique.isPresent("DGNOM"));
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    // TODO Icones
    BT_ChgSoc.setIcon(lexique.chargerImage("images/changer.png", true));
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    xH_Titre.setIcon(ManagerSessionClient.getInstance().chargerLogoEtablissement(WSOC.getText()));
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F10"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "scgm"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_FonctionsActionPerformed(ActionEvent e) {
    CMD.show(bt_Fonctions, 0, bt_Fonctions.getHeight());
  }
  
  private void BT_ChgSocActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1", false);
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@", false);
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@", false);
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@", false);
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@", false);
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@", false);
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@", false);
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    l_LOCTP = new JLabel();
    bt_Fonctions = new JButton();
    P_Centre = new JPanel();
    OBJ_28 = new JXTitledSeparator();
    OBJ_29 = new JXTitledSeparator();
    OBJ_30 = new JXTitledSeparator();
    OBJ_31 = new JXTitledSeparator();
    OBJ_32 = new JXTitledSeparator();
    OBJ_33_OBJ_33 = new JLabel();
    OBJ_45_OBJ_45 = new JLabel();
    OBJ_44_OBJ_44 = new JLabel();
    OBJ_48_OBJ_48 = new JLabel();
    GENDEB = new XRiTextField();
    GENFIN = new XRiTextField();
    OBJ_49_OBJ_49 = new JLabel();
    OBJ_52_OBJ_52 = new JLabel();
    OBJ_50_OBJ_50 = new JLabel();
    DATDEB = new XRiCalendrier();
    DATFIN = new XRiCalendrier();
    OBJ_43_OBJ_43 = new JLabel();
    AUXDEB = new XRiTextField();
    AUXFIN = new XRiTextField();
    WSOC = new XRiTextField();
    OBJ_53_OBJ_53 = new JLabel();
    OBJ_51_OBJ_51 = new JLabel();
    REPDEB = new XRiTextField();
    REPFIN = new XRiTextField();
    OBJ_46_OBJ_46 = new JLabel();
    OBJ_47_OBJ_47 = new JLabel();
    BT_ChgSoc = new JButton();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    CellConstraints cc = new CellConstraints();

    //======== this ========
    setPreferredSize(new Dimension(950, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("@TITPG1@ @TITPG2@");
      xH_Titre.setDescription("FM@LOCGRP/+1/@");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 22));
        P_Infos.setPreferredSize(new Dimension(66, 35));
        P_Infos.setName("P_Infos");

        //---- l_LOCTP ----
        l_LOCTP.setText("@LOCTP@");
        l_LOCTP.setFont(new Font("sansserif", Font.BOLD, 12));
        l_LOCTP.setName("l_LOCTP");

        //---- bt_Fonctions ----
        bt_Fonctions.setText("Fonctions");
        bt_Fonctions.setName("bt_Fonctions");
        bt_Fonctions.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_FonctionsActionPerformed(e);
          }
        });

        GroupLayout P_InfosLayout = new GroupLayout(P_Infos);
        P_Infos.setLayout(P_InfosLayout);
        P_InfosLayout.setHorizontalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(19, 19, 19)
              .addComponent(l_LOCTP, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 782, Short.MAX_VALUE)
              .addComponent(bt_Fonctions, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
        );
        P_InfosLayout.setVerticalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGroup(P_InfosLayout.createParallelGroup()
                .addGroup(P_InfosLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addComponent(l_LOCTP, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addComponent(bt_Fonctions))
              .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");

      //---- OBJ_28 ----
      OBJ_28.setTitle("Plage repr\u00e9sentants");
      OBJ_28.setName("OBJ_28");

      //---- OBJ_29 ----
      OBJ_29.setTitle("Plage de comptes auxiliaires");
      OBJ_29.setName("OBJ_29");

      //---- OBJ_30 ----
      OBJ_30.setTitle("Plage de comptes g\u00e9n\u00e9raux");
      OBJ_30.setName("OBJ_30");

      //---- OBJ_31 ----
      OBJ_31.setTitle("Plage date");
      OBJ_31.setName("OBJ_31");

      //---- OBJ_32 ----
      OBJ_32.setTitle("Soci\u00e9t\u00e9 s\u00e9lectionn\u00e9e");
      OBJ_32.setName("OBJ_32");

      //---- OBJ_33_OBJ_33 ----
      OBJ_33_OBJ_33.setText("@DGNOM@");
      OBJ_33_OBJ_33.setName("OBJ_33_OBJ_33");

      //---- OBJ_45_OBJ_45 ----
      OBJ_45_OBJ_45.setText("@MESCLO@");
      OBJ_45_OBJ_45.setName("OBJ_45_OBJ_45");

      //---- OBJ_44_OBJ_44 ----
      OBJ_44_OBJ_44.setText("@DGPERX@");
      OBJ_44_OBJ_44.setName("OBJ_44_OBJ_44");

      //---- OBJ_48_OBJ_48 ----
      OBJ_48_OBJ_48.setText("N\u00b0Cpt D\u00e9but");
      OBJ_48_OBJ_48.setName("OBJ_48_OBJ_48");

      //---- GENDEB ----
      GENDEB.setComponentPopupMenu(BTD);
      GENDEB.setName("GENDEB");

      //---- GENFIN ----
      GENFIN.setComponentPopupMenu(BTD);
      GENFIN.setName("GENFIN");

      //---- OBJ_49_OBJ_49 ----
      OBJ_49_OBJ_49.setText("N\u00b0Cpt Fin");
      OBJ_49_OBJ_49.setName("OBJ_49_OBJ_49");

      //---- OBJ_52_OBJ_52 ----
      OBJ_52_OBJ_52.setText("D\u00e9but");
      OBJ_52_OBJ_52.setName("OBJ_52_OBJ_52");

      //---- OBJ_50_OBJ_50 ----
      OBJ_50_OBJ_50.setText("N\u00b0 D\u00e9but");
      OBJ_50_OBJ_50.setName("OBJ_50_OBJ_50");

      //---- DATDEB ----
      DATDEB.setComponentPopupMenu(BTD);
      DATDEB.setName("DATDEB");

      //---- DATFIN ----
      DATFIN.setComponentPopupMenu(BTD);
      DATFIN.setName("DATFIN");

      //---- OBJ_43_OBJ_43 ----
      OBJ_43_OBJ_43.setText("En cours");
      OBJ_43_OBJ_43.setName("OBJ_43_OBJ_43");

      //---- AUXDEB ----
      AUXDEB.setComponentPopupMenu(BTD);
      AUXDEB.setName("AUXDEB");

      //---- AUXFIN ----
      AUXFIN.setComponentPopupMenu(BTD);
      AUXFIN.setName("AUXFIN");

      //---- WSOC ----
      WSOC.setComponentPopupMenu(BTD);
      WSOC.setName("WSOC");

      //---- OBJ_53_OBJ_53 ----
      OBJ_53_OBJ_53.setText("Fin");
      OBJ_53_OBJ_53.setName("OBJ_53_OBJ_53");

      //---- OBJ_51_OBJ_51 ----
      OBJ_51_OBJ_51.setText("N\u00b0 Fin");
      OBJ_51_OBJ_51.setName("OBJ_51_OBJ_51");

      //---- REPDEB ----
      REPDEB.setComponentPopupMenu(BTD);
      REPDEB.setName("REPDEB");

      //---- REPFIN ----
      REPFIN.setComponentPopupMenu(BTD);
      REPFIN.setName("REPFIN");

      //---- OBJ_46_OBJ_46 ----
      OBJ_46_OBJ_46.setText("du");
      OBJ_46_OBJ_46.setName("OBJ_46_OBJ_46");

      //---- OBJ_47_OBJ_47 ----
      OBJ_47_OBJ_47.setText("au");
      OBJ_47_OBJ_47.setName("OBJ_47_OBJ_47");

      //---- BT_ChgSoc ----
      BT_ChgSoc.setText("");
      BT_ChgSoc.setToolTipText("Changement d'\u00e9tablissement");
      BT_ChgSoc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ChgSoc.setName("BT_ChgSoc");
      BT_ChgSoc.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ChgSocActionPerformed(e);
        }
      });

      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, 635, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(85, 85, 85)
            .addComponent(WSOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
            .addComponent(BT_ChgSoc)
            .addGap(57, 57, 57)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_33_OBJ_33, GroupLayout.PREFERRED_SIZE, 397, GroupLayout.PREFERRED_SIZE)
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addComponent(OBJ_43_OBJ_43, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_44_OBJ_44, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(OBJ_45_OBJ_45, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE))))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, 635, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(85, 85, 85)
            .addComponent(OBJ_46_OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
            .addGap(12, 12, 12)
            .addComponent(DATDEB, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
            .addGap(10, 10, 10)
            .addComponent(OBJ_47_OBJ_47, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
            .addGap(12, 12, 12)
            .addComponent(DATFIN, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, 635, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(85, 85, 85)
            .addComponent(OBJ_48_OBJ_48, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
            .addGap(14, 14, 14)
            .addComponent(GENDEB, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
            .addGap(20, 20, 20)
            .addComponent(OBJ_49_OBJ_49, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
            .addGap(17, 17, 17)
            .addComponent(GENFIN, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 635, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(85, 85, 85)
            .addComponent(OBJ_50_OBJ_50, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)
            .addGap(34, 34, 34)
            .addComponent(AUXDEB, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
            .addGap(30, 30, 30)
            .addComponent(OBJ_51_OBJ_51, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
            .addGap(27, 27, 27)
            .addComponent(AUXFIN, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 635, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(85, 85, 85)
            .addComponent(OBJ_52_OBJ_52, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE)
            .addGap(33, 33, 33)
            .addComponent(REPDEB, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
            .addGap(10, 10, 10)
            .addComponent(OBJ_53_OBJ_53, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
            .addGap(5, 5, 5)
            .addComponent(REPFIN, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
      );
      P_CentreLayout.setVerticalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(6, 6, 6)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(WSOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addComponent(OBJ_33_OBJ_33, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addGroup(P_CentreLayout.createParallelGroup()
                  .addComponent(OBJ_43_OBJ_43, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_44_OBJ_44, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_45_OBJ_45, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
            .addGap(10, 10, 10)
            .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(17, 17, 17)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_46_OBJ_46, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(DATDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_47_OBJ_47, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(DATFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGap(16, 16, 16)
            .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(17, 17, 17)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_48_OBJ_48, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(GENDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_49_OBJ_49, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(GENFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGap(16, 16, 16)
            .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(22, 22, 22)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_50_OBJ_50, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(AUXDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_51_OBJ_51, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(AUXFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGap(21, 21, 21)
            .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(22, 22, 22)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_52_OBJ_52, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(REPDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_53_OBJ_53, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(REPFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
      );
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(200)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Choix du Papier");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      CMD.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setText("Annuler");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      CMD.add(OBJ_9);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Invite");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private JLabel l_LOCTP;
  private JButton bt_Fonctions;
  private JPanel P_Centre;
  private JXTitledSeparator OBJ_28;
  private JXTitledSeparator OBJ_29;
  private JXTitledSeparator OBJ_30;
  private JXTitledSeparator OBJ_31;
  private JXTitledSeparator OBJ_32;
  private JLabel OBJ_33_OBJ_33;
  private JLabel OBJ_45_OBJ_45;
  private JLabel OBJ_44_OBJ_44;
  private JLabel OBJ_48_OBJ_48;
  private XRiTextField GENDEB;
  private XRiTextField GENFIN;
  private JLabel OBJ_49_OBJ_49;
  private JLabel OBJ_52_OBJ_52;
  private JLabel OBJ_50_OBJ_50;
  private XRiCalendrier DATDEB;
  private XRiCalendrier DATFIN;
  private JLabel OBJ_43_OBJ_43;
  private XRiTextField AUXDEB;
  private XRiTextField AUXFIN;
  private XRiTextField WSOC;
  private JLabel OBJ_53_OBJ_53;
  private JLabel OBJ_51_OBJ_51;
  private XRiTextField REPDEB;
  private XRiTextField REPFIN;
  private JLabel OBJ_46_OBJ_46;
  private JLabel OBJ_47_OBJ_47;
  private JButton BT_ChgSoc;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
