
package ri.serien.libecranrpg.scgm.SCGM5BFM;
// Nom Fichier: pop_SCGM52FM_FMTB1_FMTF1_55.java

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.jdesktop.swingx.JXTitledSeparator;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SCGM5BFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SCGM5BFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBORI@")).trim());
    OBJ_19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBDES@")).trim());
    OBJ_24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@JOLIB@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    JODES.setEnabled(lexique.isPresent("JODES"));
    AUXORI.setEnabled(lexique.isPresent("AUXORI"));
    COLORI.setEnabled(lexique.isPresent("COLORI"));
    FOLDES.setEnabled(lexique.isPresent("FOLDES"));
    AUXDES.setEnabled(lexique.isPresent("AUXDES"));
    NCGDES.setEnabled(lexique.isPresent("NCGDES"));
    DATDES.setEnabled(lexique.isPresent("DATDES"));
    
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    // TODO Icones
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    OBJ_18.setIcon(lexique.chargerImage("images/retour.png", true));
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITRE@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_11 = new JLabel();
    OBJ_19 = new JLabel();
    OBJ_24 = new JLabel();
    OBJ_12 = new JLabel();
    OBJ_8 = new JLabel();
    BT_ENTER = new JButton();
    OBJ_18 = new JButton();
    OBJ_21 = new JLabel();
    DATDES = new XRiTextField();
    NCGDES = new XRiTextField();
    AUXDES = new XRiTextField();
    FOLDES = new XRiTextField();
    COLORI = new XRiTextField();
    AUXORI = new XRiTextField();
    OBJ_23 = new JLabel();
    JODES = new XRiTextField();
    OBJ_20 = new JLabel();
    xTitledSeparator1 = new JXTitledSeparator();
    BT_ERR = new JButton();
    V03F = new JLabel();
    BTD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();

    //======== this ========
    setName("this");
    setLayout(null);

    //---- OBJ_11 ----
    OBJ_11.setText("@LIBORI@");
    OBJ_11.setForeground(Color.gray);
    OBJ_11.setName("OBJ_11");
    add(OBJ_11);
    OBJ_11.setBounds(359, 45, 305, 20);

    //---- OBJ_19 ----
    OBJ_19.setText("@LIBDES@");
    OBJ_19.setForeground(Color.gray);
    OBJ_19.setName("OBJ_19");
    add(OBJ_19);
    OBJ_19.setBounds(359, 90, 305, 20);

    //---- OBJ_24 ----
    OBJ_24.setText("@JOLIB@");
    OBJ_24.setForeground(Color.gray);
    OBJ_24.setName("OBJ_24");
    add(OBJ_24);
    OBJ_24.setBounds(359, 135, 305, 20);

    //---- OBJ_12 ----
    OBJ_12.setText("Compte destination");
    OBJ_12.setName("OBJ_12");
    add(OBJ_12);
    OBJ_12.setBounds(30, 90, 123, 20);

    //---- OBJ_8 ----
    OBJ_8.setText("Compte origine");
    OBJ_8.setName("OBJ_8");
    add(OBJ_8);
    OBJ_8.setBounds(30, 45, 102, 20);

    //---- BT_ENTER ----
    BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_ENTER.setName("BT_ENTER");
    BT_ENTER.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_17ActionPerformed(e);
      }
    });
    add(BT_ENTER);
    BT_ENTER.setBounds(552, 197, 56, 45);

    //---- OBJ_18 ----
    OBJ_18.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_18.setName("OBJ_18");
    OBJ_18.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_18ActionPerformed(e);
      }
    });
    add(OBJ_18);
    OBJ_18.setBounds(615, 197, 56, 45);

    //---- OBJ_21 ----
    OBJ_21.setText("Code folio");
    OBJ_21.setName("OBJ_21");
    add(OBJ_21);
    OBJ_21.setBounds(94, 135, 70, 20);

    //---- DATDES ----
    DATDES.setComponentPopupMenu(BTD);
    DATDES.setName("DATDES");
    add(DATDES);
    DATDES.setBounds(274, 131, 76, DATDES.getPreferredSize().height);

    //---- NCGDES ----
    NCGDES.setComponentPopupMenu(BTD);
    NCGDES.setName("NCGDES");
    add(NCGDES);
    NCGDES.setBounds(160, 86, 70, NCGDES.getPreferredSize().height);

    //---- AUXDES ----
    AUXDES.setComponentPopupMenu(BTD);
    AUXDES.setName("AUXDES");
    add(AUXDES);
    AUXDES.setBounds(275, 86, 60, AUXDES.getPreferredSize().height);

    //---- FOLDES ----
    FOLDES.setComponentPopupMenu(BTD);
    FOLDES.setName("FOLDES");
    add(FOLDES);
    FOLDES.setBounds(160, 131, 50, FOLDES.getPreferredSize().height);

    //---- COLORI ----
    COLORI.setName("COLORI");
    add(COLORI);
    COLORI.setBounds(160, 41, 60, COLORI.getPreferredSize().height);

    //---- AUXORI ----
    AUXORI.setName("AUXORI");
    add(AUXORI);
    AUXORI.setBounds(274, 41, 60, AUXORI.getPreferredSize().height);

    //---- OBJ_23 ----
    OBJ_23.setText("Date");
    OBJ_23.setName("OBJ_23");
    add(OBJ_23);
    OBJ_23.setBounds(238, 135, 35, 20);

    //---- JODES ----
    JODES.setComponentPopupMenu(BTD);
    JODES.setName("JODES");
    add(JODES);
    JODES.setBounds(60, 131, 30, JODES.getPreferredSize().height);

    //---- OBJ_20 ----
    OBJ_20.setText("Jo");
    OBJ_20.setName("OBJ_20");
    add(OBJ_20);
    OBJ_20.setBounds(30, 135, 22, 20);

    //---- xTitledSeparator1 ----
    xTitledSeparator1.setTitle("G\u00e9n\u00e9ration de l'OD");
    xTitledSeparator1.setName("xTitledSeparator1");
    add(xTitledSeparator1);
    xTitledSeparator1.setBounds(20, 10, 644, xTitledSeparator1.getPreferredSize().height);

    //---- BT_ERR ----
    BT_ERR.setPreferredSize(new Dimension(32, 32));
    BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_ERR.setName("BT_ERR");
    BT_ERR.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        BT_ERRActionPerformed(e);
      }
    });
    add(BT_ERR);
    BT_ERR.setBounds(new Rectangle(new Point(15, 210), BT_ERR.getPreferredSize()));

    //---- V03F ----
    V03F.setText("@V03F@");
    V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
    V03F.setName("V03F");
    add(V03F);
    V03F.setBounds(52, 210, 248, 32);

    {
      // compute preferred size
      Dimension preferredSize = new Dimension();
      for(int i = 0; i < getComponentCount(); i++) {
        Rectangle bounds = getComponent(i).getBounds();
        preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
        preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
      }
      Insets insets = getInsets();
      preferredSize.width += insets.right;
      preferredSize.height += insets.bottom;
      setMinimumSize(preferredSize);
      setPreferredSize(preferredSize);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTD.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("Invite");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      BTD.add(OBJ_6);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel OBJ_11;
  private JLabel OBJ_19;
  private JLabel OBJ_24;
  private JLabel OBJ_12;
  private JLabel OBJ_8;
  private JButton BT_ENTER;
  private JButton OBJ_18;
  private JLabel OBJ_21;
  private XRiTextField DATDES;
  private XRiTextField NCGDES;
  private XRiTextField AUXDES;
  private XRiTextField FOLDES;
  private XRiTextField COLORI;
  private XRiTextField AUXORI;
  private JLabel OBJ_23;
  private XRiTextField JODES;
  private JLabel OBJ_20;
  private JXTitledSeparator xTitledSeparator1;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPopupMenu BTD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
