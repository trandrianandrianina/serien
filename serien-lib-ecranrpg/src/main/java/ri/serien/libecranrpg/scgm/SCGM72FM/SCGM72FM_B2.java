
package ri.serien.libecranrpg.scgm.SCGM72FM;
// Nom Fichier: pop_SCGM72FM_FMTB2_FMTF1_419.java

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;

import org.jdesktop.swingx.JXTitledSeparator;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class SCGM72FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  
  public SCGM72FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(OBJ_8);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_5.setText(interpreteurD.analyseExpression("@ANO1@").trim());
    OBJ_6.setText(interpreteurD.analyseExpression("@ANO2@").trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    OBJ_7.setIcon(lexique.chargerImage("images/w95mbx03.gif", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@LOCMNU@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "enter");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    OBJ_5 = new JLabel();
    OBJ_6 = new JLabel();
    OBJ_7 = new JLabel();
    OBJ_8 = new JButton();
    xTitledSeparator1 = new JXTitledSeparator();

    //======== this ========
    setName("this");
    setLayout(null);

    //---- OBJ_5 ----
    OBJ_5.setText("@ANO1@");
    OBJ_5.setForeground(Color.red);
    OBJ_5.setName("OBJ_5");
    add(OBJ_5);
    OBJ_5.setBounds(79, 30, 492, 28);

    //---- OBJ_6 ----
    OBJ_6.setText("@ANO2@");
    OBJ_6.setForeground(Color.red);
    OBJ_6.setName("OBJ_6");
    add(OBJ_6);
    OBJ_6.setBounds(79, 62, 492, 28);

    //---- OBJ_7 ----
    OBJ_7.setIcon(new ImageIcon("images/w95mbx03.gif"));
    OBJ_7.setName("OBJ_7");
    add(OBJ_7);
    OBJ_7.setBounds(25, 30, 54, 38);

    //---- OBJ_8 ----
    OBJ_8.setText("Ok");
    OBJ_8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_8.setName("OBJ_8");
    OBJ_8.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_8ActionPerformed(e);
      }
    });
    add(OBJ_8);
    OBJ_8.setBounds(490, 115, 82, 24);

    //---- xTitledSeparator1 ----
    xTitledSeparator1.setTitle(" ");
    xTitledSeparator1.setName("xTitledSeparator1");
    add(xTitledSeparator1);
    xTitledSeparator1.setBounds(15, 15, 560, xTitledSeparator1.getPreferredSize().height);

    setPreferredSize(new Dimension(592, 148));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel OBJ_5;
  private JLabel OBJ_6;
  private JLabel OBJ_7;
  private JButton OBJ_8;
  private JXTitledSeparator xTitledSeparator1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
