
package ri.serien.libecranrpg.scgm.SCGM53FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SCGM53FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SCGM53FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WDIER.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDIER@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    // DATDEB.setVisible( lexique.isPresent("DATDEB"));
    WDIER.setVisible(lexique.isTrue("19"));
    OBJ_18.setVisible(lexique.isTrue("19"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Différence de pointage"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    OBJ_18 = new JLabel();
    OBJ_20 = new JLabel();
    WDIER = new RiZoneSortie();
    DATDEB = new XRiCalendrier();
    CGE01 = new XRiTextField();
    MTE01 = new XRiTextField();
    LIE01 = new XRiTextField();
    label1 = new JLabel();
    CGE02 = new XRiTextField();
    MTE02 = new XRiTextField();
    LIE02 = new XRiTextField();
    CGE03 = new XRiTextField();
    MTE03 = new XRiTextField();
    LIE03 = new XRiTextField();
    CGE04 = new XRiTextField();
    MTE04 = new XRiTextField();
    LIE04 = new XRiTextField();
    label2 = new JLabel();
    C6CGT = new XRiTextField();
    C6MTT = new XRiTextField();
    C6LIT = new XRiTextField();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    CGE05 = new XRiTextField();
    MTE05 = new XRiTextField();
    LIE05 = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(705, 270));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- OBJ_18 ----
          OBJ_18.setText("Diff\u00e9rence");
          OBJ_18.setFont(OBJ_18.getFont().deriveFont(OBJ_18.getFont().getStyle() | Font.BOLD));
          OBJ_18.setName("OBJ_18");
          p_recup.add(OBJ_18);
          OBJ_18.setBounds(85, 224, 70, 20);

          //---- OBJ_20 ----
          OBJ_20.setText("G\u00e9n\u00e9ration au");
          OBJ_20.setName("OBJ_20");
          p_recup.add(OBJ_20);
          OBJ_20.setBounds(300, 224, 95, 20);

          //---- WDIER ----
          WDIER.setText("@WDIER@");
          WDIER.setHorizontalAlignment(SwingConstants.RIGHT);
          WDIER.setName("WDIER");
          p_recup.add(WDIER);
          WDIER.setBounds(155, 222, 78, WDIER.getPreferredSize().height);

          //---- DATDEB ----
          DATDEB.setName("DATDEB");
          p_recup.add(DATDEB);
          DATDEB.setBounds(400, 220, 105, DATDEB.getPreferredSize().height);

          //---- CGE01 ----
          CGE01.setName("CGE01");
          p_recup.add(CGE01);
          CGE01.setBounds(85, 40, 70, CGE01.getPreferredSize().height);

          //---- MTE01 ----
          MTE01.setName("MTE01");
          p_recup.add(MTE01);
          MTE01.setBounds(155, 40, 80, MTE01.getPreferredSize().height);

          //---- LIE01 ----
          LIE01.setName("LIE01");
          p_recup.add(LIE01);
          LIE01.setBounds(235, 40, 270, LIE01.getPreferredSize().height);

          //---- label1 ----
          label1.setText("Charge ");
          label1.setName("label1");
          p_recup.add(label1);
          label1.setBounds(30, 44, 60, 20);

          //---- CGE02 ----
          CGE02.setName("CGE02");
          p_recup.add(CGE02);
          CGE02.setBounds(85, 70, 70, 28);

          //---- MTE02 ----
          MTE02.setName("MTE02");
          p_recup.add(MTE02);
          MTE02.setBounds(155, 70, 80, 28);

          //---- LIE02 ----
          LIE02.setName("LIE02");
          p_recup.add(LIE02);
          LIE02.setBounds(235, 70, 270, 28);

          //---- CGE03 ----
          CGE03.setName("CGE03");
          p_recup.add(CGE03);
          CGE03.setBounds(85, 100, 70, 28);

          //---- MTE03 ----
          MTE03.setName("MTE03");
          p_recup.add(MTE03);
          MTE03.setBounds(155, 100, 80, 28);

          //---- LIE03 ----
          LIE03.setName("LIE03");
          p_recup.add(LIE03);
          LIE03.setBounds(235, 100, 270, 28);

          //---- CGE04 ----
          CGE04.setName("CGE04");
          p_recup.add(CGE04);
          CGE04.setBounds(85, 130, 70, 28);

          //---- MTE04 ----
          MTE04.setName("MTE04");
          p_recup.add(MTE04);
          MTE04.setBounds(155, 130, 80, 28);

          //---- LIE04 ----
          LIE04.setName("LIE04");
          p_recup.add(LIE04);
          LIE04.setBounds(235, 130, 270, 28);

          //---- label2 ----
          label2.setText("TVA ");
          label2.setName("label2");
          p_recup.add(label2);
          label2.setBounds(30, 194, 40, 20);

          //---- C6CGT ----
          C6CGT.setName("C6CGT");
          p_recup.add(C6CGT);
          C6CGT.setBounds(85, 190, 70, C6CGT.getPreferredSize().height);

          //---- C6MTT ----
          C6MTT.setName("C6MTT");
          p_recup.add(C6MTT);
          C6MTT.setBounds(155, 190, 80, C6MTT.getPreferredSize().height);

          //---- C6LIT ----
          C6LIT.setName("C6LIT");
          p_recup.add(C6LIT);
          C6LIT.setBounds(235, 190, 270, C6LIT.getPreferredSize().height);

          //---- label3 ----
          label3.setText("N\u00b0 compte");
          label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
          label3.setName("label3");
          p_recup.add(label3);
          label3.setBounds(85, 15, 70, 21);

          //---- label4 ----
          label4.setText("Montant");
          label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
          label4.setName("label4");
          p_recup.add(label4);
          label4.setBounds(160, 15, 75, 21);

          //---- label5 ----
          label5.setText("Libell\u00e9 \u00e9criture");
          label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
          label5.setName("label5");
          p_recup.add(label5);
          label5.setBounds(240, 15, 195, 21);

          //---- CGE05 ----
          CGE05.setName("CGE05");
          p_recup.add(CGE05);
          CGE05.setBounds(85, 160, 70, 28);

          //---- MTE05 ----
          MTE05.setName("MTE05");
          p_recup.add(MTE05);
          MTE05.setBounds(155, 160, 80, 28);

          //---- LIE05 ----
          LIE05.setName("LIE05");
          p_recup.add(LIE05);
          LIE05.setBounds(235, 160, 270, 28);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 520, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(15, Short.MAX_VALUE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addComponent(p_recup, GroupLayout.DEFAULT_SIZE, 257, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JLabel OBJ_18;
  private JLabel OBJ_20;
  private RiZoneSortie WDIER;
  private XRiCalendrier DATDEB;
  private XRiTextField CGE01;
  private XRiTextField MTE01;
  private XRiTextField LIE01;
  private JLabel label1;
  private XRiTextField CGE02;
  private XRiTextField MTE02;
  private XRiTextField LIE02;
  private XRiTextField CGE03;
  private XRiTextField MTE03;
  private XRiTextField LIE03;
  private XRiTextField CGE04;
  private XRiTextField MTE04;
  private XRiTextField LIE04;
  private JLabel label2;
  private XRiTextField C6CGT;
  private XRiTextField C6MTT;
  private XRiTextField C6LIT;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private XRiTextField CGE05;
  private XRiTextField MTE05;
  private XRiTextField LIE05;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
