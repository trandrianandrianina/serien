
package ri.serien.libecranrpg.scgm.SCGM5TFM;
// Nom Fichier: pop_SCGM5TFM_FMTB2_1.java

import java.awt.Cursor;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class SCGM5TFM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SCGM5TFM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_5_OBJ_5 = new JLabel();
    OBJ_4 = new JButton();

    //======== this ========
    setName("this");

    //---- OBJ_5_OBJ_5 ----
    OBJ_5_OBJ_5.setText("G\u00e9n\u00e9ration en cours...");
    OBJ_5_OBJ_5.setName("OBJ_5_OBJ_5");

    //---- OBJ_4 ----
    OBJ_4.setText("");
    OBJ_4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_4.setName("OBJ_4");

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(20, 20, 20)
          .addComponent(OBJ_4, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)
          .addGap(28, 28, 28)
          .addComponent(OBJ_5_OBJ_5, GroupLayout.PREFERRED_SIZE, 213, GroupLayout.PREFERRED_SIZE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(25, 25, 25)
          .addComponent(OBJ_4, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(34, 34, 34)
          .addComponent(OBJ_5_OBJ_5, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
    );
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel OBJ_5_OBJ_5;
  private JButton OBJ_4;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
