
package ri.serien.libecranrpg.scgm.SCGM88FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SCGM88FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] RELF_Value = { "", "OUI", "FAX", "MAI" };
  private String[] RELF_Title = { "Pas d'édition", "Edition papier", "Envoi par fax", "Envoi automatique par mail" };
  
  public SCGM88FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    RELF.setValeurs(RELF_Value, RELF_Title);
    TRT2.setValeursSelection("1", " ");
    TRT1.setValeursSelection("1", " ");
    WTOU.setValeursSelection("**", "  ");
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGPERX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSOC@")).trim());
    OBJ_31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MESCLO@")).trim());
    xTitledSeparator2.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@OPTI@")).trim());
    OBJ_35.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBCOL@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    P_SEL0.setVisible(!WTOU.isSelected());
    OBJ_52.setVisible(NUMPAS.isVisible());
    OBJ_50.setVisible(DATREG.isVisible());
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    try {
      lexique.HostScreenSendKey(this, "F10");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_31 = new RiZoneSortie();
    xTitledSeparator2 = new JXTitledSeparator();
    xTitledSeparator3 = new JXTitledSeparator();
    OBJ_38 = new JLabel();
    CODJO = new XRiTextField();
    OBJ_33 = new JLabel();
    COLLEC = new XRiTextField();
    OBJ_35 = new RiZoneSortie();
    xTitledSeparator4 = new JXTitledSeparator();
    WTOU = new XRiCheckBox();
    xTitledSeparator5 = new JXTitledSeparator();
    OBJ_51 = new JLabel();
    RELF = new XRiComboBox();
    OBJ_50 = new JLabel();
    DATREG = new XRiCalendrier();
    P_SEL0 = new JPanel();
    OBJ_32 = new JLabel();
    OBJ_34 = new JLabel();
    AUXIL1 = new XRiTextField();
    AUXIL2 = new XRiTextField();
    panel2 = new JPanel();
    TRT1 = new XRiCheckBox();
    TRT2 = new XRiCheckBox();
    OBJ_52 = new JLabel();
    NUMPAS = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
          
          // ---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(e -> bouton_validerActionPerformed(e));
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(e -> bouton_retourActionPerformed(e));
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Type de sortie");
              riSousMenu_bt_export.setToolTipText("Type de sortie");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(e -> riSousMenu_bt_exportActionPerformed(e));
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(650, 550));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          
          // ---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");
          
          // ---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");
          
          // ---- z_wencx_ ----
          z_wencx_.setText("@DGPERX@");
          z_wencx_.setName("z_wencx_");
          
          // ---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WSOC@");
          z_etablissement_.setName("z_etablissement_");
          
          // ---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(e -> bouton_etablissementActionPerformed(e));
          
          // ---- OBJ_31 ----
          OBJ_31.setText("@MESCLO@");
          OBJ_31.setName("OBJ_31");
          
          // ---- xTitledSeparator2 ----
          xTitledSeparator2.setTitle("@OPTI@");
          xTitledSeparator2.setName("xTitledSeparator2");
          
          // ---- xTitledSeparator3 ----
          xTitledSeparator3.setTitle("");
          xTitledSeparator3.setName("xTitledSeparator3");
          
          // ---- OBJ_38 ----
          OBJ_38.setText("Code journal de banque \u00e0 traiter");
          OBJ_38.setName("OBJ_38");
          
          // ---- CODJO ----
          CODJO.setComponentPopupMenu(BTD);
          CODJO.setName("CODJO");
          
          // ---- OBJ_33 ----
          OBJ_33.setText("Collectif \u00e0 traiter");
          OBJ_33.setName("OBJ_33");
          
          // ---- COLLEC ----
          COLLEC.setComponentPopupMenu(BTD);
          COLLEC.setName("COLLEC");
          
          // ---- OBJ_35 ----
          OBJ_35.setText("@LIBCOL@");
          OBJ_35.setName("OBJ_35");
          
          // ---- xTitledSeparator4 ----
          xTitledSeparator4.setTitle("Plage de comptes \u00e0 \u00e9diter");
          xTitledSeparator4.setName("xTitledSeparator4");
          
          // ---- WTOU ----
          WTOU.setText("S\u00e9lection compl\u00e8te des comptes");
          WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU.setName("WTOU");
          WTOU.addActionListener(e -> WTOUActionPerformed(e));
          
          // ---- xTitledSeparator5 ----
          xTitledSeparator5.setTitle("");
          xTitledSeparator5.setName("xTitledSeparator5");
          
          // ---- OBJ_51 ----
          OBJ_51.setText("Edition d'un relev\u00e9 fournisseur");
          OBJ_51.setName("OBJ_51");
          
          // ---- RELF ----
          RELF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RELF.setName("RELF");
          
          // ---- OBJ_50 ----
          OBJ_50.setText("Date effective du r\u00e8glement");
          OBJ_50.setName("OBJ_50");
          
          // ---- DATREG ----
          DATREG.setComponentPopupMenu(BTD);
          DATREG.setName("DATREG");
          
          // ======== P_SEL0 ========
          {
            P_SEL0.setOpaque(false);
            P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL0.setName("P_SEL0");
            P_SEL0.setLayout(null);
            
            // ---- OBJ_32 ----
            OBJ_32.setText("Compte auxiliaire d\u00e9but");
            OBJ_32.setName("OBJ_32");
            P_SEL0.add(OBJ_32);
            OBJ_32.setBounds(30, 14, 165, 20);
            
            // ---- OBJ_34 ----
            OBJ_34.setText("Compte auxiliaire fin");
            OBJ_34.setName("OBJ_34");
            P_SEL0.add(OBJ_34);
            OBJ_34.setBounds(30, 44, 147, 20);
            
            // ---- AUXIL1 ----
            AUXIL1.setComponentPopupMenu(BTD);
            AUXIL1.setName("AUXIL1");
            P_SEL0.add(AUXIL1);
            AUXIL1.setBounds(190, 10, 60, AUXIL1.getPreferredSize().height);
            
            // ---- AUXIL2 ----
            AUXIL2.setComponentPopupMenu(BTD);
            AUXIL2.setName("AUXIL2");
            P_SEL0.add(AUXIL2);
            AUXIL2.setBounds(190, 40, 60, AUXIL2.getPreferredSize().height);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < P_SEL0.getComponentCount(); i++) {
                Rectangle bounds = P_SEL0.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = P_SEL0.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              P_SEL0.setMinimumSize(preferredSize);
              P_SEL0.setPreferredSize(preferredSize);
            }
          }
          
          // ======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);
            
            // ---- TRT1 ----
            TRT1.setText("1- Liste de contr\u00f4le seulement");
            TRT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TRT1.setName("TRT1");
            panel2.add(TRT1);
            TRT1.setBounds(15, 15, 204, 20);
            
            // ---- TRT2 ----
            TRT2.setText("2- Liste et g\u00e9n\u00e9ration virements");
            TRT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TRT2.setName("TRT2");
            panel2.add(TRT2);
            TRT2.setBounds(15, 40, 220, 20);
            
            // ---- OBJ_52 ----
            OBJ_52.setText("Num\u00e9ro de passage \u00e0 r\u00e9\u00e9diter");
            OBJ_52.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_52.setName("OBJ_52");
            panel2.add(OBJ_52);
            OBJ_52.setBounds(265, 15, 190, 20);
            
            // ---- NUMPAS ----
            NUMPAS.setComponentPopupMenu(BTD);
            NUMPAS.setName("NUMPAS");
            panel2.add(NUMPAS);
            NUMPAS.setBounds(470, 11, 36, NUMPAS.getPreferredSize().height);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(34, 34, 34).addComponent(sep_etablissement,
                  GroupLayout.PREFERRED_SIZE, 570, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(49, 49, 49)
                  .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(122, 122, 122)
                  .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 295, GroupLayout.PREFERRED_SIZE)
                      .addGroup(p_contenuLayout.createSequentialGroup()
                          .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE).addGap(7, 7, 7)
                          .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, 178, GroupLayout.PREFERRED_SIZE))))
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(34, 34, 34).addComponent(xTitledSeparator2,
                  GroupLayout.PREFERRED_SIZE, 570, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(34, 34, 34).addComponent(panel2, GroupLayout.PREFERRED_SIZE, 520,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(34, 34, 34).addComponent(xTitledSeparator3,
                  GroupLayout.PREFERRED_SIZE, 570, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(49, 49, 49).addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup().addGap(195, 195, 195).addComponent(CODJO, GroupLayout.PREFERRED_SIZE,
                      34, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(49, 49, 49)
                  .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 205, GroupLayout.PREFERRED_SIZE)
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(195, 195, 195).addComponent(COLLEC,
                          GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)))
                  .addGap(10, 10, 10).addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(34, 34, 34).addComponent(xTitledSeparator4,
                  GroupLayout.PREFERRED_SIZE, 570, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(49, 49, 49)
                  .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE).addGap(15, 15, 15)
                  .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(34, 34, 34).addComponent(xTitledSeparator5,
                  GroupLayout.PREFERRED_SIZE, 570, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(54, 54, 54)
                  .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE)
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(190, 190, 190).addComponent(RELF,
                          GroupLayout.PREFERRED_SIZE, 220, GroupLayout.PREFERRED_SIZE))))
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(54, 54, 54)
                  .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 184, GroupLayout.PREFERRED_SIZE).addGap(6, 6, 6)
                  .addComponent(DATREG, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)));
          p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(29, 29, 29)
                  .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(16, 16, 16)
                  .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(17, 17, 17).addComponent(z_etablissement_,
                          GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(15, 15, 15).addComponent(bouton_etablissement,
                          GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup()
                          .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addGap(6, 6, 6)
                          .addGroup(p_contenuLayout.createParallelGroup()
                              .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                              .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGap(16, 16, 16)
                  .addComponent(xTitledSeparator2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1).addComponent(panel2, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(xTitledSeparator3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(23, 23, 23)
                  .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(CODJO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE,
                          20, GroupLayout.PREFERRED_SIZE)))
                  .addGap(2, 2, 2)
                  .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE,
                          20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(COLLEC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE,
                          GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(17, 17, 17)
                  .addComponent(xTitledSeparator4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(6, 6, 6)
                  .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(15, 15, 15).addComponent(WTOU, GroupLayout.PREFERRED_SIZE,
                          20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE))
                  .addGap(10, 10, 10)
                  .addComponent(xTitledSeparator5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(23, 23, 23)
                  .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(3, 3, 3).addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE,
                          20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(RELF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(4, 4, 4)
                  .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE,
                          20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(DATREG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(e -> OBJ_13ActionPerformed(e));
      BTD.add(OBJ_13);
      
      // ---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(e -> OBJ_12ActionPerformed(e));
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private RiZoneSortie OBJ_31;
  private JXTitledSeparator xTitledSeparator2;
  private JXTitledSeparator xTitledSeparator3;
  private JLabel OBJ_38;
  private XRiTextField CODJO;
  private JLabel OBJ_33;
  private XRiTextField COLLEC;
  private RiZoneSortie OBJ_35;
  private JXTitledSeparator xTitledSeparator4;
  private XRiCheckBox WTOU;
  private JXTitledSeparator xTitledSeparator5;
  private JLabel OBJ_51;
  private XRiComboBox RELF;
  private JLabel OBJ_50;
  private XRiCalendrier DATREG;
  private JPanel P_SEL0;
  private JLabel OBJ_32;
  private JLabel OBJ_34;
  private XRiTextField AUXIL1;
  private XRiTextField AUXIL2;
  private JPanel panel2;
  private XRiCheckBox TRT1;
  private XRiCheckBox TRT2;
  private JLabel OBJ_52;
  private XRiTextField NUMPAS;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
