
package ri.serien.libecranrpg.scgm.SCGM9GFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SCGM9GFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SCGM9GFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTOU.setValeursSelection("**", "  ");
    BDGREA.setValeursSelection("OUI", "NON");
    REPON1.setValeursSelection("OUI", "NON");
    DETAIL.setValeursSelection("OUI", "NON");
    EDTRGR.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MESCLO@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSOC@")).trim());
    z_wencx_2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGPERX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    NIVH.setVisible(lexique.isPresent("NIVH"));
    HIE.setVisible(lexique.isPresent("HIE"));
    PERIOD.setVisible(lexique.isPresent("PERIOD"));
    SECT2.setVisible(!interpreteurD.analyseExpression("@WTOU@").trim().equalsIgnoreCase("**") & lexique.isPresent("SECT2"));
    SECT1.setVisible(!interpreteurD.analyseExpression("@WTOU@").trim().equalsIgnoreCase("**") & lexique.isPresent("SECT1"));
    GAN.setVisible(lexique.isPresent("GAN"));
    OBJ_43.setVisible(!interpreteurD.analyseExpression("@WTOU@").trim().equalsIgnoreCase("**"));
    OBJ_42.setVisible(!interpreteurD.analyseExpression("@WTOU@").trim().equalsIgnoreCase("**"));
    // DETAIL.setSelected(lexique.HostFieldGetData("DETAIL").equalsIgnoreCase("OUI"));
    // EDTRGR.setSelected(lexique.HostFieldGetData("EDTRGR").equalsIgnoreCase("OUI"));
    // WTOU.setSelected(lexique.HostFieldGetData("WTOU").equalsIgnoreCase("**"));
    // BDGREA.setVisible( lexique.isPresent("BDGREA"));
    // BDGREA.setSelected(lexique.HostFieldGetData("BDGREA").equalsIgnoreCase("OUI"));
    // REPON1.setVisible( lexique.isPresent("REPON1"));
    // REPON1.setSelected(lexique.HostFieldGetData("REPON1").equalsIgnoreCase("OUI"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@"));
    
    

    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (DETAIL.isSelected())
    // lexique.HostFieldPutData("DETAIL", 0, "OUI");
    // else
    // lexique.HostFieldPutData("DETAIL", 0, "NON");
    // if (EDTRGR.isSelected())
    // lexique.HostFieldPutData("EDTRGR", 0, "OUI");
    // else
    // lexique.HostFieldPutData("EDTRGR", 0, "NON");
    // if (WTOU.isSelected())
    // lexique.HostFieldPutData("WTOU", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU", 0, " ");
    // if (BDGREA.isSelected())
    // lexique.HostFieldPutData("BDGREA", 0, "OUI");
    // else
    // lexique.HostFieldPutData("BDGREA", 0, "NON");
    // if (REPON1.isSelected())
    // lexique.HostFieldPutData("REPON1", 0, "OUI");
    // else
    // lexique.HostFieldPutData("REPON1", 0, "NON");
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    OBJ_42.setVisible(!WTOU.isSelected());
    OBJ_43.setVisible(!WTOU.isSelected());
    SECT1.setVisible(!WTOU.isSelected() & lexique.isPresent("SECT1"));
    SECT2.setVisible(!WTOU.isSelected() & lexique.isPresent("SECT2"));
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    OBJ_44 = new JLabel();
    PERIOD = new XRiTextField();
    EDTRGR = new XRiCheckBox();
    DETAIL = new XRiCheckBox();
    REPON1 = new XRiCheckBox();
    BDGREA = new XRiCheckBox();
    xTitledSeparator2 = new JXTitledSeparator();
    xTitledSeparator5 = new JXTitledSeparator();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    z_wencx_2 = new RiZoneSortie();
    label2 = new JLabel();
    xTitledSeparator6 = new JXTitledSeparator();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_42 = new JLabel();
    OBJ_43 = new JLabel();
    SECT1 = new XRiTextField();
    SECT2 = new XRiTextField();
    WTOU = new XRiCheckBox();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_45 = new JLabel();
    HIE = new XRiTextField();
    NIVH = new XRiTextField();
    OBJ_60 = new JLabel();
    OBJ_46 = new JLabel();
    GAN = new XRiTextField();
    OBJ_80 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Type de sortie");
              riSousMenu_bt_export.setToolTipText("Type de sortie");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(780, 520));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- OBJ_44 ----
          OBJ_44.setText("P\u00e9riode \u00e0 \u00e9diter");
          OBJ_44.setName("OBJ_44");

          //---- PERIOD ----
          PERIOD.setComponentPopupMenu(BTD);
          PERIOD.setToolTipText("sous la forme MMAA ou MM.AA");
          PERIOD.setName("PERIOD");

          //---- EDTRGR ----
          EDTRGR.setText("Edition regroup\u00e9e");
          EDTRGR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EDTRGR.setName("EDTRGR");

          //---- DETAIL ----
          DETAIL.setText("Edition d\u00e9taill\u00e9e");
          DETAIL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          DETAIL.setName("DETAIL");

          //---- REPON1 ----
          REPON1.setText("Comparatif avec p\u00e9riode pr\u00e9c\u00e9dente");
          REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON1.setName("REPON1");

          //---- BDGREA ----
          BDGREA.setText("Budget R\u00e9actualis\u00e9");
          BDGREA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BDGREA.setName("BDGREA");

          //---- xTitledSeparator2 ----
          xTitledSeparator2.setTitle("");
          xTitledSeparator2.setName("xTitledSeparator2");

          //---- xTitledSeparator5 ----
          xTitledSeparator5.setTitle("");
          xTitledSeparator5.setName("xTitledSeparator5");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@MESCLO@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WSOC@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- z_wencx_2 ----
          z_wencx_2.setText("@DGPERX@");
          z_wencx_2.setFont(z_wencx_2.getFont().deriveFont(z_wencx_2.getFont().getStyle() | Font.BOLD));
          z_wencx_2.setName("z_wencx_2");

          //---- label2 ----
          label2.setText("En cours");
          label2.setName("label2");

          //---- xTitledSeparator6 ----
          xTitledSeparator6.setTitle("");
          xTitledSeparator6.setName("xTitledSeparator6");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Plage de sections analytiques");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

            //---- OBJ_42 ----
            OBJ_42.setText("Section de d\u00e9but");
            OBJ_42.setName("OBJ_42");

            //---- OBJ_43 ----
            OBJ_43.setText("Section de fin");
            OBJ_43.setName("OBJ_43");

            //---- SECT1 ----
            SECT1.setComponentPopupMenu(BTD);
            SECT1.setName("SECT1");

            //---- SECT2 ----
            SECT2.setComponentPopupMenu(BTD);
            SECT2.setName("SECT2");

            //---- WTOU ----
            WTOU.setText("S\u00e9lection compl\u00e8te");
            WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU.setName("WTOU");
            WTOU.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUActionPerformed(e);
              }
            });

            GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
            xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
            xTitledPanel1ContentContainerLayout.setHorizontalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(12, 12, 12)
                  .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(45, 45, 45)
                  .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addComponent(SECT1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(45, 45, 45)
                  .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addComponent(SECT2, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
            );
            xTitledPanel1ContentContainerLayout.setVerticalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(13, 13, 13)
                  .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addGap(7, 7, 7)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(SECT1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(7, 7, 7)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(SECT2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setTitle("Groupes analytiques");
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setPreferredSize(new Dimension(325, 148));
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();

            //---- OBJ_45 ----
            OBJ_45.setText("Hi\u00e9rarchie");
            OBJ_45.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_45.setName("OBJ_45");

            //---- HIE ----
            HIE.setComponentPopupMenu(BTD);
            HIE.setName("HIE");

            //---- NIVH ----
            NIVH.setComponentPopupMenu(BTD);
            NIVH.setName("NIVH");

            //---- OBJ_60 ----
            OBJ_60.setText("Niveau");
            OBJ_60.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_60.setName("OBJ_60");

            //---- OBJ_46 ----
            OBJ_46.setText("Groupe");
            OBJ_46.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_46.setName("OBJ_46");

            //---- GAN ----
            GAN.setComponentPopupMenu(BTD);
            GAN.setName("GAN");

            GroupLayout xTitledPanel2ContentContainerLayout = new GroupLayout(xTitledPanel2ContentContainer);
            xTitledPanel2ContentContainer.setLayout(xTitledPanel2ContentContainerLayout);
            xTitledPanel2ContentContainerLayout.setHorizontalGroup(
              xTitledPanel2ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(50, 50, 50)
                  .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE)
                  .addGap(2, 2, 2)
                  .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                  .addGap(17, 17, 17)
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(70, 70, 70)
                  .addComponent(HIE, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addGap(40, 40, 40)
                  .addComponent(NIVH, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addGap(35, 35, 35)
                  .addComponent(GAN, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
            );
            xTitledPanel2ContentContainerLayout.setVerticalGroup(
              xTitledPanel2ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(35, 35, 35)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(HIE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NIVH, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GAN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }

          //---- OBJ_80 ----
          OBJ_80.setText("OU");
          OBJ_80.setFont(new Font("Tahoma", Font.BOLD, 11));
          OBJ_80.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_80.setName("OBJ_80");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 715, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(label2, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(z_wencx_2, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE))))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(xTitledSeparator2, GroupLayout.PREFERRED_SIZE, 715, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 215, GroupLayout.PREFERRED_SIZE)
                .addComponent(PERIOD, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(xTitledSeparator6, GroupLayout.PREFERRED_SIZE, 715, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE)
                .addComponent(OBJ_80, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(xTitledSeparator5, GroupLayout.PREFERRED_SIZE, 715, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(EDTRGR, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(DETAIL, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(REPON1, GroupLayout.PREFERRED_SIZE, 241, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(BDGREA, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(label2))
                      .addComponent(z_wencx_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGap(16, 16, 16)
                .addComponent(xTitledSeparator2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(PERIOD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17)
                .addComponent(xTitledSeparator6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(65, 65, 65)
                    .addComponent(OBJ_80, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(xTitledSeparator5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addComponent(EDTRGR, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(DETAIL, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(REPON1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(BDGREA, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JLabel OBJ_44;
  private XRiTextField PERIOD;
  private XRiCheckBox EDTRGR;
  private XRiCheckBox DETAIL;
  private XRiCheckBox REPON1;
  private XRiCheckBox BDGREA;
  private JXTitledSeparator xTitledSeparator2;
  private JXTitledSeparator xTitledSeparator5;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private RiZoneSortie z_wencx_2;
  private JLabel label2;
  private JXTitledSeparator xTitledSeparator6;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_42;
  private JLabel OBJ_43;
  private XRiTextField SECT1;
  private XRiTextField SECT2;
  private XRiCheckBox WTOU;
  private JXTitledPanel xTitledPanel2;
  private JLabel OBJ_45;
  private XRiTextField HIE;
  private XRiTextField NIVH;
  private JLabel OBJ_60;
  private JLabel OBJ_46;
  private XRiTextField GAN;
  private JLabel OBJ_80;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
