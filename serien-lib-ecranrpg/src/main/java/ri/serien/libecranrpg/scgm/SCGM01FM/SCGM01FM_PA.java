
package ri.serien.libecranrpg.scgm.SCGM01FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class SCGM01FM_PA extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _T01_Top = { "T01", "T02", "T03", "T04", "T05", "T06", "T07", "T08", };
  private String[] _T01_Title = { "", };
  private String[][] _T01_Data = { { "L01", }, { "L02", }, { "L03", }, { "L04", }, { "L05", }, { "L06", }, { "L07", }, { "L08", }, };
  private int[] _T01_Width = { 291, };
  
  public SCGM01FM_PA(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    T01.setAspectTable(_T01_Top, _T01_Title, _T01_Data, _T01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _T01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    RCHA.setVisible(lexique.isPresent("RCHA"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@LIBPAR@"));
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _T01_Top, "X", "Enter");
    T01.setValeurTop("X");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void T01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _T01_Top, "X", "Enter",e);
    if (T01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER", true);
    }
  }
  
  private void riBoutonRecherche1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F16");
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _T01_Top, "X", "Enter");
    T01.setValeurTop("X");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    T01 = new XRiTable();
    RCHA = new XRiTextField();
    xTitledSeparator1 = new JXTitledSeparator();
    xTitledSeparator2 = new JXTitledSeparator();
    BT_PGDOWN = new JButton();
    BT_PGUP = new JButton();
    riBoutonRecherche1 = new SNBoutonRecherche();
    popupMenu1 = new JPopupMenu();
    menuItem1 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(615, 290));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 120));
            menus_haut.setPreferredSize(new Dimension(160, 120));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Gestion des param\u00e8tres");
              riSousMenu_bt6.setToolTipText("Gestion des param\u00e8tres");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== SCROLLPANE_LIST ========
        {
          SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

          //---- T01 ----
          T01.setComponentPopupMenu(popupMenu1);
          T01.setName("T01");
          T01.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              T01MouseClicked(e);
            }
          });
          SCROLLPANE_LIST.setViewportView(T01);
        }

        //---- RCHA ----
        RCHA.setName("RCHA");

        //---- xTitledSeparator1 ----
        xTitledSeparator1.setTitle("Recherche alphab\u00e9tique");
        xTitledSeparator1.setName("xTitledSeparator1");

        //---- xTitledSeparator2 ----
        xTitledSeparator2.setTitle("Choix possibles");
        xTitledSeparator2.setName("xTitledSeparator2");

        //---- BT_PGDOWN ----
        BT_PGDOWN.setText("");
        BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGDOWN.setName("BT_PGDOWN");

        //---- BT_PGUP ----
        BT_PGUP.setText("");
        BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGUP.setName("BT_PGUP");

        //---- riBoutonRecherche1 ----
        riBoutonRecherche1.setName("riBoutonRecherche1");
        riBoutonRecherche1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            riBoutonRecherche1ActionPerformed(e);
          }
        });

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(45, 45, 45)
              .addComponent(RCHA, GroupLayout.PREFERRED_SIZE, 186, GroupLayout.PREFERRED_SIZE)
              .addGap(9, 9, 9)
              .addComponent(riBoutonRecherche1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(xTitledSeparator2, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE))
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(45, 45, 45)
              .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 315, GroupLayout.PREFERRED_SIZE)
              .addGap(5, 5, 5)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGap(11, 11, 11)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(RCHA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(riBoutonRecherche1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGap(17, 17, 17)
              .addComponent(xTitledSeparator2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGap(11, 11, 11)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addGap(19, 19, 19)
                  .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== popupMenu1 ========
    {
      popupMenu1.setName("popupMenu1");

      //---- menuItem1 ----
      menuItem1.setText("Choisir");
      menuItem1.setName("menuItem1");
      menuItem1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem1ActionPerformed(e);
        }
      });
      popupMenu1.add(menuItem1);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable T01;
  private XRiTextField RCHA;
  private JXTitledSeparator xTitledSeparator1;
  private JXTitledSeparator xTitledSeparator2;
  private JButton BT_PGDOWN;
  private JButton BT_PGUP;
  private SNBoutonRecherche riBoutonRecherche1;
  private JPopupMenu popupMenu1;
  private JMenuItem menuItem1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
