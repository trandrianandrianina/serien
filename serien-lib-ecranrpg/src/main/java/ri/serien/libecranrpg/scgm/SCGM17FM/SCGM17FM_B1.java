
package ri.serien.libecranrpg.scgm.SCGM17FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

public class SCGM17FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private boolean modePlanning = false;
  
  public SCGM17FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // TODO constructeur spécifiques...
    
    // Ajout
    initDiverses();
    WTOU.setValeursSelection("**", "  ");
    REPON2.setValeursSelection("OUI", "NON");
    REPON1.setValeursSelection("OUI", "NON");
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MESCLO@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSOC@")).trim());
    z_wencx_2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGPERX@")).trim());
    OBJ_28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBCOL@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    modePlanning = lexique.isTrue("85");
    // Message planning
    lbPlanning.setVisible(modePlanning);
    if (modePlanning) {
      Message message = Message.getMessageImportant("Vous êtes en paramétrage de la mise au planning pour cette demande.");
      lbPlanning.setMessage(message);
    }
    
    panel1.setVisible(!WTOU.isSelected());
    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // TODO getDATA spécifiques
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    panel1.setVisible(!WTOU.isSelected());
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTDA.getInvoker().getName());
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    z_wencx_2 = new RiZoneSortie();
    label1 = new JLabel();
    REPON1 = new XRiCheckBox();
    REPON2 = new XRiCheckBox();
    OBJ_34 = new JLabel();
    DREP = new XRiTextField();
    OBJ_27 = new JLabel();
    COLLEC = new XRiTextField();
    OBJ_28 = new RiZoneSortie();
    OBJ_35 = new JLabel();
    PERIO1 = new XRiTextField();
    xTitledSeparator2 = new JXTitledSeparator();
    xTitledSeparator3 = new JXTitledSeparator();
    xTitledSeparator4 = new JXTitledSeparator();
    xTitledSeparator5 = new JXTitledSeparator();
    xTitledSeparator6 = new JXTitledSeparator();
    panel1 = new JPanel();
    AUXIL1 = new XRiTextField();
    AUXIL2 = new XRiTextField();
    OBJ_43 = new JLabel();
    OBJ_45 = new JLabel();
    WTOU = new XRiCheckBox();
    lbPlanning = new SNMessage();
    BTDA = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_15 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Edition de l'historique des comptes auxiliaires");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
          
          // ---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(e -> bouton_validerActionPerformed(e));
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(e -> bouton_retourActionPerformed(e));
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Type de sortie");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(e -> riSousMenu_bt_exportActionPerformed(e));
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(700, 480));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          
          // ---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");
          
          // ---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");
          
          // ---- z_wencx_ ----
          z_wencx_.setText("@MESCLO@");
          z_wencx_.setName("z_wencx_");
          
          // ---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WSOC@");
          z_etablissement_.setName("z_etablissement_");
          
          // ---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(e -> bouton_etablissementActionPerformed(e));
          
          // ---- z_wencx_2 ----
          z_wencx_2.setText("@DGPERX@");
          z_wencx_2.setFont(z_wencx_2.getFont().deriveFont(z_wencx_2.getFont().getStyle() | Font.BOLD));
          z_wencx_2.setName("z_wencx_2");
          
          // ---- label1 ----
          label1.setText("En cours");
          label1.setName("label1");
          
          // ---- REPON1 ----
          REPON1.setText("Saut de page par compte");
          REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON1.setName("REPON1");
          
          // ---- REPON2 ----
          REPON2.setText("Edition bloc note \u00e9critures");
          REPON2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON2.setName("REPON2");
          
          // ---- OBJ_34 ----
          OBJ_34.setText("Date de reprise");
          OBJ_34.setFont(OBJ_34.getFont().deriveFont(OBJ_34.getFont().getStyle() | Font.BOLD));
          OBJ_34.setName("OBJ_34");
          
          // ---- DREP ----
          DREP.setToolTipText("Date \u00e0 partir de laquelle vous obtiendrez le d\u00e9tail");
          DREP.setComponentPopupMenu(BTDA);
          DREP.setName("DREP");
          
          // ---- OBJ_27 ----
          OBJ_27.setText("Collectif \u00e0 traiter");
          OBJ_27.setName("OBJ_27");
          
          // ---- COLLEC ----
          COLLEC.setComponentPopupMenu(BTD);
          COLLEC.setName("COLLEC");
          
          // ---- OBJ_28 ----
          OBJ_28.setText("@LIBCOL@");
          OBJ_28.setForeground(Color.black);
          OBJ_28.setName("OBJ_28");
          
          // ---- OBJ_35 ----
          OBJ_35.setText("P\u00e9riode \u00e0 \u00e9diter");
          OBJ_35.setFont(OBJ_35.getFont().deriveFont(OBJ_35.getFont().getStyle() | Font.BOLD));
          OBJ_35.setName("OBJ_35");
          
          // ---- PERIO1 ----
          PERIO1.setToolTipText("Edition depuis le d\u00e9but d'exercice jusqu'\u00e0 la p\u00e9riode saisie");
          PERIO1.setComponentPopupMenu(BTDA);
          PERIO1.setName("PERIO1");
          
          // ---- xTitledSeparator2 ----
          xTitledSeparator2.setTitle("");
          xTitledSeparator2.setName("xTitledSeparator2");
          
          // ---- xTitledSeparator3 ----
          xTitledSeparator3.setTitle("");
          xTitledSeparator3.setName("xTitledSeparator3");
          
          // ---- xTitledSeparator4 ----
          xTitledSeparator4.setTitle("Plage de comptes \u00e0 \u00e9diter");
          xTitledSeparator4.setName("xTitledSeparator4");
          
          // ---- xTitledSeparator5 ----
          xTitledSeparator5.setTitle("");
          xTitledSeparator5.setName("xTitledSeparator5");
          
          // ---- xTitledSeparator6 ----
          xTitledSeparator6.setTitle("");
          xTitledSeparator6.setName("xTitledSeparator6");
          
          // ======== panel1 ========
          {
            panel1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);
            
            // ---- AUXIL1 ----
            AUXIL1.setComponentPopupMenu(BTD);
            AUXIL1.setName("AUXIL1");
            panel1.add(AUXIL1);
            AUXIL1.setBounds(215, 10, 60, AUXIL1.getPreferredSize().height);
            
            // ---- AUXIL2 ----
            AUXIL2.setComponentPopupMenu(BTD);
            AUXIL2.setName("AUXIL2");
            panel1.add(AUXIL2);
            AUXIL2.setBounds(215, 45, 60, AUXIL2.getPreferredSize().height);
            
            // ---- OBJ_43 ----
            OBJ_43.setText("Compte auxiliaire de d\u00e9but");
            OBJ_43.setName("OBJ_43");
            panel1.add(OBJ_43);
            OBJ_43.setBounds(10, 14, 205, 20);
            
            // ---- OBJ_45 ----
            OBJ_45.setText("Compte auxiliaire de fin");
            OBJ_45.setName("OBJ_45");
            panel1.add(OBJ_45);
            OBJ_45.setBounds(10, 49, 205, 20);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          
          // ---- WTOU ----
          WTOU.setText("S\u00e9lection compl\u00e8te");
          WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU.setName("WTOU");
          WTOU.addActionListener(e -> WTOUActionPerformed(e));
          
          // ---- lbPlanning ----
          lbPlanning.setText("Label Planning");
          lbPlanning.setName("lbPlanning");
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout
              .setHorizontalGroup(
                  p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup().addGroup(p_contenuLayout.createParallelGroup()
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(34, 34, 34).addComponent(sep_etablissement,
                              GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(49, 49, 49)
                              .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                              .addComponent(
                                  bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                              .addGap(82, 82, 82)
                              .addGroup(p_contenuLayout.createParallelGroup()
                                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                                  .addGroup(p_contenuLayout.createSequentialGroup()
                                      .addComponent(label1, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                                      .addComponent(z_wencx_2, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                                      .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE))))
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(34, 34, 34).addComponent(xTitledSeparator2,
                              GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(49, 49, 49)
                              .addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 111, GroupLayout.PREFERRED_SIZE).addGap(44, 44, 44)
                              .addComponent(PERIO1, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(34, 34, 34).addComponent(xTitledSeparator3,
                              GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(49, 49, 49)
                              .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE).addGap(40, 40, 40)
                              .addComponent(COLLEC, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                              .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 271, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(34, 34, 34).addComponent(xTitledSeparator4,
                              GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(49, 49, 49)
                              .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 315, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(34, 34, 34).addComponent(xTitledSeparator5,
                              GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(49, 49, 49).addComponent(REPON1,
                              GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(49, 49, 49).addComponent(REPON2,
                              GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(34, 34, 34).addComponent(xTitledSeparator6,
                              GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(49, 49, 49)
                              .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE).addGap(47, 47, 47)
                              .addComponent(DREP, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)))
                          .addGap(39, 39, 39))
                      .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                          .addComponent(lbPlanning, GroupLayout.PREFERRED_SIZE, 674, GroupLayout.PREFERRED_SIZE).addContainerGap()));
          p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
              .addComponent(lbPlanning, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addGap(4, 4, 4)
              .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGap(11, 11, 11)
              .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup().addGap(17, 17, 17).addComponent(z_etablissement_,
                      GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup().addGap(15, 15, 15).addComponent(bouton_etablissement,
                      GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                      .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(6, 6, 6)
                      .addGroup(p_contenuLayout.createParallelGroup()
                          .addGroup(p_contenuLayout.createSequentialGroup().addGap(5, 5, 5).addComponent(label1))
                          .addComponent(z_wencx_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
              .addGap(16, 16, 16)
              .addComponent(xTitledSeparator2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGap(8, 8, 8)
              .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 20,
                      GroupLayout.PREFERRED_SIZE))
                  .addComponent(PERIO1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGap(7, 7, 7)
              .addComponent(xTitledSeparator3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGap(13, 13, 13)
              .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 20,
                      GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                      .addComponent(COLLEC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
              .addGap(17, 17, 17)
              .addComponent(xTitledSeparator4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGap(6, 6, 6)
              .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup().addGap(15, 15, 15).addComponent(WTOU, GroupLayout.PREFERRED_SIZE, 20,
                      GroupLayout.PREFERRED_SIZE))
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE))
              .addGap(10, 10, 10)
              .addComponent(xTitledSeparator5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGap(8, 8, 8).addComponent(REPON1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(10, 10, 10)
              .addComponent(REPON2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(10, 10, 10)
              .addComponent(xTitledSeparator6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGap(8, 8, 8)
              .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 20,
                      GroupLayout.PREFERRED_SIZE))
                  .addComponent(DREP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTDA ========
    {
      BTDA.setName("BTDA");
      
      // ---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(e -> OBJ_5ActionPerformed(e));
      BTDA.add(OBJ_5);
    }
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_15 ----
      OBJ_15.setText("Choix possibles");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(e -> OBJ_15ActionPerformed(e));
      BTD.add(OBJ_15);
      
      // ---- OBJ_14 ----
      OBJ_14.setText("Aide en ligne");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(e -> OBJ_14ActionPerformed(e));
      BTD.add(OBJ_14);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private RiZoneSortie z_wencx_2;
  private JLabel label1;
  private XRiCheckBox REPON1;
  private XRiCheckBox REPON2;
  private JLabel OBJ_34;
  private XRiTextField DREP;
  private JLabel OBJ_27;
  private XRiTextField COLLEC;
  private RiZoneSortie OBJ_28;
  private JLabel OBJ_35;
  private XRiTextField PERIO1;
  private JXTitledSeparator xTitledSeparator2;
  private JXTitledSeparator xTitledSeparator3;
  private JXTitledSeparator xTitledSeparator4;
  private JXTitledSeparator xTitledSeparator5;
  private JXTitledSeparator xTitledSeparator6;
  private JPanel panel1;
  private XRiTextField AUXIL1;
  private XRiTextField AUXIL2;
  private JLabel OBJ_43;
  private JLabel OBJ_45;
  private XRiCheckBox WTOU;
  private SNMessage lbPlanning;
  private JPopupMenu BTDA;
  private JMenuItem OBJ_5;
  private JPopupMenu BTD;
  private JMenuItem OBJ_15;
  private JMenuItem OBJ_14;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
