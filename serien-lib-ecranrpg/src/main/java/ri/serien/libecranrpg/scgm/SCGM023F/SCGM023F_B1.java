
package ri.serien.libecranrpg.scgm.SCGM023F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SCGM023F_B1 extends SNPanelEcranRPG implements ioFrame {
  
  public SCGM023F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    WTP.setValeurs("5", WTP_GRP);
    WTP_B.setValeurs("6");
    WTP_C.setValeurs("5");
    WTP_D.setValeurs("5");
    WTP_E.setValeurs("2");
    WTP_F.setValeurs("3");
    WTP_G.setValeurs("4");
    SUFFI1.setListeCaracteresNonAutorises(Constantes.caracteresNonAutorisesPourFichier);
    SUFFI2.setListeCaracteresNonAutorises(Constantes.caracteresNonAutorisesPourFichier);
    SUFFI3.setListeCaracteresNonAutorises(Constantes.caracteresNonAutorisesPourFichier);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    panel2.setVisible(WTP_C.isSelected());
    panel3.setVisible(WTP_B.isSelected());
    panel4.setVisible(WTP_F.isSelected());
    OBJ_20.setVisible(lexique.isTrue("43"));
    WTP_B.setVisible(lexique.isTrue("20"));
    
    /*
    if (!lexique.HostFieldGetData("WTP").equalsIgnoreCase("3"))
    {
      OBJ_18.setVisible(false);
      OBJ_19.setVisible(false);
      OBJ_20.setVisible(false);
    }*/
    
    // Titre
    
    setTitle(interpreteurD.analyseExpression("Choix sortie"));
    
    // setSize();
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  // modification de la taille de la popup suivant les cas
  private void setSize() {
    Dimension dim = null;
    
    if (panel2.isVisible() || panel3.isVisible()) {
      dim = new Dimension(800, 350);
    }
    else {
      dim = new Dimension(800, 195);
    }
    
    javax.swing.SwingUtilities.getWindowAncestor(this).setSize(dim);
    // this.setPreferredSize(dim);
    this.repaint();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_34ActionPerformed(ActionEvent e) {
    panel2.setVisible(WTP_C.isSelected());
    // if (panel2.isVisible())
    panel3.setVisible(!panel2.isVisible());
    setSize();
  }
  
  private void OBJ_33ActionPerformed(ActionEvent e) {
    panel3.setVisible(WTP.isSelected() || WTP_B.isSelected());
    // if (panel3.isVisible())
    panel2.setVisible(!panel3.isVisible());
    setSize();
  }
  
  private void OBJ_32ActionPerformed(ActionEvent e) {
    panel2.setVisible(false);
    panel3.setVisible(false);
    setSize();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    WTP = new XRiRadioButton();
    WTP_B = new XRiRadioButton();
    WTP_C = new XRiRadioButton();
    panel2 = new JPanel();
    WTP_E = new XRiRadioButton();
    WTP_F = new XRiRadioButton();
    WTP_G = new XRiRadioButton();
    panel4 = new JPanel();
    OBJ_18 = new JLabel();
    OBJ_19 = new XRiTextField();
    OBJ_20 = new JLabel();
    WTP_D = new XRiRadioButton();
    panel3 = new JPanel();
    SUFFI1 = new XRiTextField();
    SUFFI2 = new XRiTextField();
    SUFFI3 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    WTP_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 170));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(e -> bouton_validerActionPerformed(e));
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(e -> bouton_retourActionPerformed(e));
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Type de sortie"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(new GridBagLayout());
          ((GridBagLayout)panel1.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)panel1.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
          ((GridBagLayout)panel1.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
          ((GridBagLayout)panel1.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

          //---- WTP ----
          WTP.setText("Fichier PDF");
          WTP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTP.setPreferredSize(new Dimension(200, 30));
          WTP.setMinimumSize(new Dimension(200, 30));
          WTP.setName("WTP");
          WTP.addActionListener(e -> OBJ_32ActionPerformed(e));
          panel1.add(WTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- WTP_B ----
          WTP_B.setText("Fichier tableur");
          WTP_B.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTP_B.setPreferredSize(new Dimension(200, 30));
          WTP_B.setMinimumSize(new Dimension(200, 30));
          WTP_B.setName("WTP_B");
          WTP_B.addActionListener(e -> OBJ_33ActionPerformed(e));
          panel1.add(WTP_B, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- WTP_C ----
          WTP_C.setText("Edition sur papier sp\u00e9cial");
          WTP_C.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTP_C.setPreferredSize(new Dimension(200, 30));
          WTP_C.setMinimumSize(new Dimension(200, 30));
          WTP_C.setName("WTP_C");
          WTP_C.addActionListener(e -> OBJ_34ActionPerformed(e));
          panel1.add(WTP_C, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 605, 145);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Choix du papier"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- WTP_E ----
          WTP_E.setText("Papier comptable");
          WTP_E.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTP_E.setName("WTP_E");
          panel2.add(WTP_E);
          WTP_E.setBounds(30, 53, 151, 20);

          //---- WTP_F ----
          WTP_F.setText("Papier standard zon\u00e9");
          WTP_F.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTP_F.setName("WTP_F");
          panel2.add(WTP_F);
          WTP_F.setBounds(30, 76, 151, 20);

          //---- WTP_G ----
          WTP_G.setText("Papier 12''");
          WTP_G.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTP_G.setName("WTP_G");
          panel2.add(WTP_G);
          WTP_G.setBounds(30, 99, 151, 20);

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- OBJ_18 ----
            OBJ_18.setText("Nombre de lignes");
            OBJ_18.setName("OBJ_18");
            panel4.add(OBJ_18);
            OBJ_18.setBounds(0, 5, 110, 20);

            //---- OBJ_19 ----
            OBJ_19.setComponentPopupMenu(BTD);
            OBJ_19.setName("OBJ_19");
            panel4.add(OBJ_19);
            OBJ_19.setBounds(110, 0, 26, OBJ_19.getPreferredSize().height);

            //---- OBJ_20 ----
            OBJ_20.setText("< obligatoire");
            OBJ_20.setForeground(Color.red);
            OBJ_20.setName("OBJ_20");
            panel4.add(OBJ_20);
            OBJ_20.setBounds(145, 5, 79, 20);

            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }
          panel2.add(panel4);
          panel4.setBounds(200, 70, panel4.getPreferredSize().width, 30);

          //---- WTP_D ----
          WTP_D.setText("Papier A4");
          WTP_D.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTP_D.setName("WTP_D");
          panel2.add(WTP_D);
          WTP_D.setBounds(30, 30, 151, 20);

          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel2);
        panel2.setBounds(10, 170, 605, 135);

        //======== panel3 ========
        {
          panel3.setBorder(new TitledBorder("Nom du fichier (obligatoire)"));
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);

          //---- SUFFI1 ----
          SUFFI1.setComponentPopupMenu(BTD);
          SUFFI1.setName("SUFFI1");
          panel3.add(SUFFI1);
          SUFFI1.setBounds(30, 35, 510, SUFFI1.getPreferredSize().height);

          //---- SUFFI2 ----
          SUFFI2.setComponentPopupMenu(BTD);
          SUFFI2.setName("SUFFI2");
          panel3.add(SUFFI2);
          SUFFI2.setBounds(30, 60, 510, SUFFI2.getPreferredSize().height);

          //---- SUFFI3 ----
          SUFFI3.setComponentPopupMenu(BTD);
          SUFFI3.setName("SUFFI3");
          panel3.add(SUFFI3);
          SUFFI3.setBounds(30, 85, 510, SUFFI3.getPreferredSize().height);

          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel3.getComponentCount(); i++) {
              Rectangle bounds = panel3.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel3.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel3.setMinimumSize(preferredSize);
            panel3.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel3);
        panel3.setBounds(10, 170, 605, 135);

        { // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(e -> OBJ_13ActionPerformed(e));
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(e -> OBJ_12ActionPerformed(e));
      BTD.add(OBJ_12);
    }

    //---- WTP_GRP ----
    WTP_GRP.add(WTP);
    WTP_GRP.add(WTP_B);
    WTP_GRP.add(WTP_C);
    WTP_GRP.add(WTP_E);
    WTP_GRP.add(WTP_F);
    WTP_GRP.add(WTP_G);
    WTP_GRP.add(WTP_D);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiRadioButton WTP;
  private XRiRadioButton WTP_B;
  private XRiRadioButton WTP_C;
  private JPanel panel2;
  private XRiRadioButton WTP_E;
  private XRiRadioButton WTP_F;
  private XRiRadioButton WTP_G;
  private JPanel panel4;
  private JLabel OBJ_18;
  private XRiTextField OBJ_19;
  private JLabel OBJ_20;
  private XRiRadioButton WTP_D;
  private JPanel panel3;
  private XRiTextField SUFFI1;
  private XRiTextField SUFFI2;
  private XRiTextField SUFFI3;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  private ButtonGroup WTP_GRP;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
