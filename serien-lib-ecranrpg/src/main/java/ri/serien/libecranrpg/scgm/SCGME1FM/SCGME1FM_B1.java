
package ri.serien.libecranrpg.scgm.SCGME1FM;
// Nom Fichier: b_SCGME1FM_FMTB1_FMTF1_272.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class SCGME1FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public SCGME1FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
    setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@")).trim());
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG2@")).trim());
    LOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
    OBJ_43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SON001@")).trim());
    OBJ_44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGAD1X@")).trim());
    OBJ_60.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MSGCLO@")).trim());
    OBJ_41.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SOS001@")).trim());
    SOM001.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SOM001@")).trim());
    OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGDE1X@")).trim());
    OBJ_51.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGDE2X@")).trim());
    OBJ_53.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGFE1X@")).trim());
    OBJ_54.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGFE2X@")).trim());
    OBJ_56.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGDP1X@")).trim());
    OBJ_57.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGDP2X@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    OBJ_57.setVisible(lexique.isPresent("DGDP2X"));
    OBJ_56.setVisible(lexique.isPresent("DGDP1X"));
    OBJ_54.setVisible(lexique.isPresent("DGFE2X"));
    OBJ_53.setVisible(lexique.isPresent("DGFE1X"));
    OBJ_51.setVisible(lexique.isPresent("DGDE2X"));
    OBJ_50.setVisible(lexique.isPresent("DGDE1X"));
    SOM001.setVisible(lexique.isPresent("SOM001"));
    SOM001.setEnabled(lexique.isPresent("SOM001"));
    OBJ_59.setVisible(!interpreteurD.analyseExpression("@MSGCLO@").trim().equalsIgnoreCase(""));
    OBJ_41.setVisible(lexique.isPresent("SOS001"));
    OBJ_60.setVisible(lexique.isPresent("MSGCLO"));
    OBJ_44.setVisible(lexique.isPresent("DGAD1X"));
    OBJ_43.setVisible(lexique.isPresent("SON001"));
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    // TODO Icones
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    xH_Titre.setIcon(lexique.chargerImage("images/logo32.png", true));
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "Attention");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1");
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@");
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@");
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@");
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@");
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@");
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@");
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    LOCTP = new JLabel();
    label1 = new JLabel();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    P_Centre = new JPanel();
    OBJ_43 = new JLabel();
    OBJ_44 = new JLabel();
    OBJ_42 = new JLabel();
    OBJ_45 = new JLabel();
    OBJ_60 = new JLabel();
    OBJ_40 = new JLabel();
    OBJ_41 = new JLabel();
    OBJ_55 = new JLabel();
    OBJ_59 = new JLabel();
    OBJ_49 = new JLabel();
    OBJ_52 = new JLabel();
    SOM001 = new JLabel();
    OBJ_50 = new JLabel();
    OBJ_51 = new JLabel();
    OBJ_53 = new JLabel();
    OBJ_54 = new JLabel();
    OBJ_56 = new JLabel();
    OBJ_57 = new JLabel();
    xTitledSeparator1 = new JXTitledSeparator();
    xTitledSeparator2 = new JXTitledSeparator();
    xTitledSeparator3 = new JXTitledSeparator();
    OBJ_4 = new JMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    CellConstraints cc = new CellConstraints();

    //======== this ========
    setPreferredSize(new Dimension(740, 645));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("@TITPG1@");
      xH_Titre.setDescription("@TITPG2@");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 22));
        P_Infos.setPreferredSize(new Dimension(66, 22));
        P_Infos.setName("P_Infos");

        //---- LOCTP ----
        LOCTP.setText("@LOCTP@");
        LOCTP.setHorizontalAlignment(SwingConstants.RIGHT);
        LOCTP.setName("LOCTP");

        //---- label1 ----
        label1.setText("FM@LOCGRP/+1/@");
        label1.setName("label1");

        GroupLayout P_InfosLayout = new GroupLayout(P_Infos);
        P_Infos.setLayout(P_InfosLayout);
        P_InfosLayout.setHorizontalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(label1, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
              .addGap(687, 687, 687)
              .addComponent(LOCTP, GroupLayout.PREFERRED_SIZE, 174, GroupLayout.PREFERRED_SIZE)
              .addGap(5, 5, 5))
        );
        P_InfosLayout.setVerticalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(1, 1, 1)
              .addGroup(P_InfosLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(LOCTP)
                .addComponent(label1)))
        );
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(89)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");

      //---- OBJ_43 ----
      OBJ_43.setText("@SON001@");
      OBJ_43.setName("OBJ_43");

      //---- OBJ_44 ----
      OBJ_44.setText("@DGAD1X@");
      OBJ_44.setName("OBJ_44");

      //---- OBJ_42 ----
      OBJ_42.setText("Nom ou Raison Sociale");
      OBJ_42.setName("OBJ_42");

      //---- OBJ_45 ----
      OBJ_45.setText("P\u00e9riode \u00e0 traiter");
      OBJ_45.setName("OBJ_45");

      //---- OBJ_60 ----
      OBJ_60.setText("@MSGCLO@");
      OBJ_60.setForeground(Color.gray);
      OBJ_60.setName("OBJ_60");

      //---- OBJ_40 ----
      OBJ_40.setText("Soci\u00e9t\u00e9");
      OBJ_40.setName("OBJ_40");

      //---- OBJ_41 ----
      OBJ_41.setText("@SOS001@");
      OBJ_41.setName("OBJ_41");

      //---- OBJ_55 ----
      OBJ_55.setText("Encours");
      OBJ_55.setName("OBJ_55");

      //---- OBJ_59 ----
      OBJ_59.setText("Exercice");
      OBJ_59.setName("OBJ_59");

      //---- OBJ_49 ----
      OBJ_49.setText("D\u00e9but");
      OBJ_49.setName("OBJ_49");

      //---- OBJ_52 ----
      OBJ_52.setText("Fin");
      OBJ_52.setName("OBJ_52");

      //---- SOM001 ----
      SOM001.setText("@SOM001@");
      SOM001.setComponentPopupMenu(BTD);
      SOM001.setName("SOM001");

      //---- OBJ_50 ----
      OBJ_50.setText("@DGDE1X@");
      OBJ_50.setName("OBJ_50");

      //---- OBJ_51 ----
      OBJ_51.setText("@DGDE2X@");
      OBJ_51.setName("OBJ_51");

      //---- OBJ_53 ----
      OBJ_53.setText("@DGFE1X@");
      OBJ_53.setName("OBJ_53");

      //---- OBJ_54 ----
      OBJ_54.setText("@DGFE2X@");
      OBJ_54.setName("OBJ_54");

      //---- OBJ_56 ----
      OBJ_56.setText("@DGDP1X@");
      OBJ_56.setName("OBJ_56");

      //---- OBJ_57 ----
      OBJ_57.setText("@DGDP2X@");
      OBJ_57.setName("OBJ_57");

      //---- xTitledSeparator1 ----
      xTitledSeparator1.setTitle("Soci\u00e9t\u00e9");
      xTitledSeparator1.setName("xTitledSeparator1");

      //---- xTitledSeparator2 ----
      xTitledSeparator2.setTitle("Exercice en cours");
      xTitledSeparator2.setName("xTitledSeparator2");

      //---- xTitledSeparator3 ----
      xTitledSeparator3.setTitle("Soci\u00e9t\u00e9");
      xTitledSeparator3.setName("xTitledSeparator3");

      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, 950, GroupLayout.PREFERRED_SIZE)
              .addComponent(xTitledSeparator3, GroupLayout.PREFERRED_SIZE, 650, GroupLayout.PREFERRED_SIZE)))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(40, 40, 40)
            .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
            .addGap(103, 103, 103)
            .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 360, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(40, 40, 40)
            .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 163, GroupLayout.PREFERRED_SIZE)
            .addGap(7, 7, 7)
            .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 345, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(210, 210, 210)
            .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 345, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(40, 40, 40)
            .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
            .addGap(52, 52, 52)
            .addComponent(SOM001, GroupLayout.PREFERRED_SIZE, 430, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(xTitledSeparator2, GroupLayout.PREFERRED_SIZE, 950, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(40, 40, 40)
            .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
            .addGap(128, 128, 128)
            .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE)
            .addGap(30, 30, 30)
            .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 195, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(40, 40, 40)
            .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
            .addGap(128, 128, 128)
            .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE)
            .addGap(30, 30, 30)
            .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 195, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(40, 40, 40)
            .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
            .addGap(118, 118, 118)
            .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE)
            .addGap(30, 30, 30)
            .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 195, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(40, 40, 40)
            .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
            .addGap(115, 115, 115)
            .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE))
      );
      P_CentreLayout.setVerticalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(30, 30, 30)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(xTitledSeparator3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGap(6, 6, 6)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addGap(5, 5, 5)
            .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(SOM001, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addGap(15, 15, 15)
            .addComponent(xTitledSeparator2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(6, 6, 6)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addGap(5, 5, 5)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addGap(5, 5, 5)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_59)
              .addComponent(OBJ_60)))
      );
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== OBJ_4 ========
    {
      OBJ_4.setText("Commandes");
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Choix du Papier");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setText("Annuler");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_9);
      OBJ_4.addSeparator();

      //---- OBJ_10 ----
      OBJ_10.setText("Exploitation");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_10);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Invite");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private JLabel LOCTP;
  private JLabel label1;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  private JPanel P_Centre;
  private JLabel OBJ_43;
  private JLabel OBJ_44;
  private JLabel OBJ_42;
  private JLabel OBJ_45;
  private JLabel OBJ_60;
  private JLabel OBJ_40;
  private JLabel OBJ_41;
  private JLabel OBJ_55;
  private JLabel OBJ_59;
  private JLabel OBJ_49;
  private JLabel OBJ_52;
  private JLabel SOM001;
  private JLabel OBJ_50;
  private JLabel OBJ_51;
  private JLabel OBJ_53;
  private JLabel OBJ_54;
  private JLabel OBJ_56;
  private JLabel OBJ_57;
  private JXTitledSeparator xTitledSeparator1;
  private JXTitledSeparator xTitledSeparator2;
  private JXTitledSeparator xTitledSeparator3;
  private JMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
