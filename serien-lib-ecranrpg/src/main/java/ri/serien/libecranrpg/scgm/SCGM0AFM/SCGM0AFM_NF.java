
package ri.serien.libecranrpg.scgm.SCGM0AFM;
// Nom Fichier: pop_SCGM0AFM_FMTNF_192.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.JXTitledSeparator;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SCGM0AFM_NF extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SCGM0AFM_NF(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    SUFFIC.setVisible(lexique.isPresent("SUFFIC"));
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@(TITLE)@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_10 = new JLabel();
    BT_ENTER = new JButton();
    OBJ_11 = new JButton();
    OBJ_5 = new JLabel();
    SUFFIC = new XRiTextField();
    xTitledSeparator1 = new JXTitledSeparator();

    //======== this ========
    setName("this");
    setLayout(null);

    //---- OBJ_10 ----
    OBJ_10.setText("( TCGM + Suffixe saisi )");
    OBJ_10.setHorizontalAlignment(SwingConstants.CENTER);
    OBJ_10.setName("OBJ_10");
    add(OBJ_10);
    OBJ_10.setBounds(25, 80, 145, 16);

    //---- BT_ENTER ----
    BT_ENTER.setText("Ok");
    BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_ENTER.setName("BT_ENTER");
    BT_ENTER.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_7ActionPerformed(e);
      }
    });
    add(BT_ENTER);
    BT_ENTER.setBounds(35, 130, 82, 24);

    //---- OBJ_11 ----
    OBJ_11.setText("Annuler");
    OBJ_11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_11.setName("OBJ_11");
    OBJ_11.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_11ActionPerformed(e);
      }
    });
    add(OBJ_11);
    OBJ_11.setBounds(125, 130, 82, 24);

    //---- OBJ_5 ----
    OBJ_5.setText("Suffixe");
    OBJ_5.setName("OBJ_5");
    add(OBJ_5);
    OBJ_5.setBounds(46, 54, 59, 20);

    //---- SUFFIC ----
    SUFFIC.setName("SUFFIC");
    add(SUFFIC);
    SUFFIC.setBounds(110, 55, 40, 20);

    //---- xTitledSeparator1 ----
    xTitledSeparator1.setTitle("Fichier Disque");
    xTitledSeparator1.setName("xTitledSeparator1");
    add(xTitledSeparator1);
    xTitledSeparator1.setBounds(20, 10, 180, xTitledSeparator1.getPreferredSize().height);

    setPreferredSize(new Dimension(213, 160));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel OBJ_10;
  private JButton BT_ENTER;
  private JButton OBJ_11;
  private JLabel OBJ_5;
  private XRiTextField SUFFIC;
  private JXTitledSeparator xTitledSeparator1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
