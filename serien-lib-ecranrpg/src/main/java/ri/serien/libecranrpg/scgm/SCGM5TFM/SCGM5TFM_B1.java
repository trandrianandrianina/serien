
package ri.serien.libecranrpg.scgm.SCGM5TFM;
// Nom Fichier: p_SCGM5TFM_FMTB1_FMTF1_2.java

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;

/**
 * @author Stéphane Vénéri
 */
public class SCGM5TFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SCGM5TFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    REP.setValeurs("OUI", REP_GRP);
    REP_NON.setValeurs("NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_39_OBJ_39.setText(lexique.TranslationTable(interpreteurD.analyseExpression("(Votre dernière génération date du @DATTRT@)")).trim());
    OBJ_29_OBJ_29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    // OBJ_42.setEnabled( lexique.isPresent("REP"));
    // OBJ_42.setSelected(lexique.HostFieldGetData("REP").equalsIgnoreCase("NON"));
    // OBJ_41.setEnabled( lexique.isPresent("REP"));
    // OBJ_41.setSelected(lexique.HostFieldGetData("REP").equalsIgnoreCase("OUI"));
    
    // TODO Icones
    OBJ_43.setIcon(lexique.chargerImage("images/msgbox04.gif", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // if (OBJ_42.isSelected())
    // lexique.HostFieldPutData("REP", 0, "NON");
    // if (OBJ_41.isSelected())
    // lexique.HostFieldPutData("REP", 0, "OUI");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "scgm5t"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_32_OBJ_32 = new JLabel();
    OBJ_40 = new JPanel();
    OBJ_44_OBJ_44 = new JLabel();
    REP = new XRiRadioButton();
    REP_NON = new XRiRadioButton();
    OBJ_39_OBJ_39 = new JLabel();
    OBJ_45_OBJ_45 = new JLabel();
    OBJ_4 = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();
    OBJ_43 = new JLabel();
    OBJ_29_OBJ_29 = new JLabel();
    REP_GRP = new ButtonGroup();

    //======== this ========
    setName("this");

    //---- OBJ_32_OBJ_32 ----
    OBJ_32_OBJ_32.setText("Ce traitement doit \u00eatre effectu\u00e9 quotidiennement pour optimiser le contr\u00f4le en gestion commerciale");
    OBJ_32_OBJ_32.setName("OBJ_32_OBJ_32");

    //======== OBJ_40 ========
    {
      OBJ_40.setName("OBJ_40");
      OBJ_40.setLayout(null);

      //---- OBJ_44_OBJ_44 ----
      OBJ_44_OBJ_44.setText("Confirmez-vous votre demande ?");
      OBJ_44_OBJ_44.setName("OBJ_44_OBJ_44");
      OBJ_40.add(OBJ_44_OBJ_44);
      OBJ_44_OBJ_44.setBounds(146, 14, 241, 26);

      //---- REP ----
      REP.setText("OUI");
      REP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      REP.setName("REP");
      OBJ_40.add(REP);
      REP.setBounds(410, 6, 66, 20);

      //---- REP_NON ----
      REP_NON.setText("NON");
      REP_NON.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      REP_NON.setName("REP_NON");
      OBJ_40.add(REP_NON);
      REP_NON.setBounds(410, 26, 66, 20);
    }

    //---- OBJ_39_OBJ_39 ----
    OBJ_39_OBJ_39.setText("(Votre derni\u00e8re g\u00e9n\u00e9ration date du @DATTRT@)");
    OBJ_39_OBJ_39.setName("OBJ_39_OBJ_39");

    //---- OBJ_45_OBJ_45 ----
    OBJ_45_OBJ_45.setText("ATTENTION");
    OBJ_45_OBJ_45.setName("OBJ_45_OBJ_45");

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(20, 20, 20)
          .addGroup(layout.createParallelGroup()
            .addComponent(OBJ_32_OBJ_32, GroupLayout.PREFERRED_SIZE, 550, GroupLayout.PREFERRED_SIZE)
            .addComponent(OBJ_45_OBJ_45, GroupLayout.PREFERRED_SIZE, 465, GroupLayout.PREFERRED_SIZE)))
        .addGroup(layout.createSequentialGroup()
          .addGap(20, 20, 20)
          .addComponent(OBJ_39_OBJ_39, GroupLayout.PREFERRED_SIZE, 530, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(10, 10, 10)
          .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 555, GroupLayout.PREFERRED_SIZE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(15, 15, 15)
          .addGroup(layout.createParallelGroup()
            .addGroup(layout.createSequentialGroup()
              .addGap(25, 25, 25)
              .addComponent(OBJ_32_OBJ_32, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
            .addComponent(OBJ_45_OBJ_45, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
          .addComponent(OBJ_39_OBJ_39, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
          .addGap(16, 16, 16)
          .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE))
    );

    //======== OBJ_4 ========
    {
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("R\u00e9afficher");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_6);

      //---- OBJ_7 ----
      OBJ_7.setText("Annuler");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);
      OBJ_4.addSeparator();

      //---- OBJ_8 ----
      OBJ_8.setText("Exploitation");
      OBJ_8.setName("OBJ_8");
      OBJ_4.add(OBJ_8);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }

    //---- OBJ_43 ----
    OBJ_43.setIcon(new ImageIcon("images/msgbox04.gif"));
    OBJ_43.setName("OBJ_43");

    //---- OBJ_29_OBJ_29 ----
    OBJ_29_OBJ_29.setText("@LOCTP@");
    OBJ_29_OBJ_29.setName("OBJ_29_OBJ_29");

    //---- REP_GRP ----
    REP_GRP.add(REP);
    REP_GRP.add(REP_NON);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel OBJ_32_OBJ_32;
  private JPanel OBJ_40;
  private JLabel OBJ_44_OBJ_44;
  private XRiRadioButton REP;
  private XRiRadioButton REP_NON;
  private JLabel OBJ_39_OBJ_39;
  private JLabel OBJ_45_OBJ_45;
  private JPopupMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  private JLabel OBJ_43;
  private JLabel OBJ_29_OBJ_29;
  private ButtonGroup REP_GRP;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
