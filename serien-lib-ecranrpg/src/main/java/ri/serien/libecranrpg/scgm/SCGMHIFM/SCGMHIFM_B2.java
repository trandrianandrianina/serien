
package ri.serien.libecranrpg.scgm.SCGMHIFM;
// Nom Fichier: b_SCGMHIFM_FMTB2_FMTF1_800.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SCGMHIFM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  
  public SCGMHIFM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
    setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@")).trim());
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG2@")).trim());
    LOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
    OBJ_33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    WR2.setVisible(lexique.isPresent("WR2"));
    WR1.setVisible(lexique.isPresent("WR1"));
    WSOC.setVisible(lexique.isPresent("WSOC"));
    WBIB.setVisible(lexique.isPresent("WBIB"));
    OBJ_33.setVisible(lexique.isPresent("DGNOM"));
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    // TODO Icones
    BT_ChgSoc.setIcon(lexique.chargerImage("images/changer.png", true));
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    xH_Titre.setIcon(ManagerSessionClient.getInstance().chargerLogoEtablissement(WSOC.getText()));
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "Attention");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void BT_ChgSocActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1");
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@");
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@");
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@");
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@");
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@");
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@");
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    LOCTP = new JLabel();
    label2 = new JLabel();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    P_Centre = new JPanel();
    OBJ_24 = new JXTitledSeparator();
    OBJ_23 = new JXTitledSeparator();
    OBJ_22 = new JXTitledSeparator();
    OBJ_33 = new JLabel();
    OBJ_30 = new JLabel();
    OBJ_32 = new JLabel();
    OBJ_39 = new JLabel();
    OBJ_40 = new JLabel();
    WBIB = new XRiTextField();
    OBJ_37 = new JLabel();
    OBJ_38 = new JLabel();
    DE1XB = new XRiTextField();
    FE1XB = new XRiTextField();
    DE2XB = new XRiTextField();
    FE2XB = new XRiTextField();
    DRE1X = new XRiTextField();
    FRE1X = new XRiTextField();
    DRE2X = new XRiTextField();
    FRE2X = new XRiTextField();
    DE1X = new XRiTextField();
    FE1X = new XRiTextField();
    DE2X = new XRiTextField();
    FE2X = new XRiTextField();
    WSOC = new XRiTextField();
    OBJ_41 = new JLabel();
    OBJ_46 = new JLabel();
    WR1 = new XRiTextField();
    WR2 = new XRiTextField();
    OBJ_25 = new JLabel();
    OBJ_47 = new JLabel();
    BT_ChgSoc = new JButton();
    label1 = new JLabel();
    OBJ_48 = new JLabel();
    OBJ_42 = new JLabel();
    OBJ_26 = new JLabel();
    OBJ_49 = new JLabel();
    OBJ_4 = new JMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    CellConstraints cc = new CellConstraints();

    //======== this ========
    setPreferredSize(new Dimension(740, 645));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("@TITPG1@");
      xH_Titre.setDescription("@TITPG2@");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 22));
        P_Infos.setPreferredSize(new Dimension(66, 22));
        P_Infos.setName("P_Infos");

        //---- LOCTP ----
        LOCTP.setText("@LOCTP@");
        LOCTP.setHorizontalAlignment(SwingConstants.RIGHT);
        LOCTP.setName("LOCTP");

        //---- label2 ----
        label2.setText("FM@LOCGRP/+1/@");
        label2.setName("label2");

        GroupLayout P_InfosLayout = new GroupLayout(P_Infos);
        P_Infos.setLayout(P_InfosLayout);
        P_InfosLayout.setHorizontalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(label2, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
              .addGap(599, 599, 599)
              .addComponent(LOCTP, GroupLayout.PREFERRED_SIZE, 262, GroupLayout.PREFERRED_SIZE)
              .addGap(5, 5, 5))
        );
        P_InfosLayout.setVerticalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(1, 1, 1)
              .addGroup(P_InfosLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(LOCTP)
                .addComponent(label2)))
        );
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(89)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");

      //---- OBJ_24 ----
      OBJ_24.setTitle(" ");
      OBJ_24.setName("OBJ_24");

      //---- OBJ_23 ----
      OBJ_23.setTitle("Soci\u00e9t\u00e9 s\u00e9lectionn\u00e9e");
      OBJ_23.setName("OBJ_23");

      //---- OBJ_22 ----
      OBJ_22.setTitle("Rang de l'exercice \u00e0 historiser");
      OBJ_22.setName("OBJ_22");

      //---- OBJ_33 ----
      OBJ_33.setText("@DGNOM@");
      OBJ_33.setForeground(Color.gray);
      OBJ_33.setName("OBJ_33");

      //---- OBJ_30 ----
      OBJ_30.setText("Exercice 1");
      OBJ_30.setName("OBJ_30");

      //---- OBJ_32 ----
      OBJ_32.setText("Exercice 2");
      OBJ_32.setName("OBJ_32");

      //---- OBJ_39 ----
      OBJ_39.setText("Exercice 1");
      OBJ_39.setName("OBJ_39");

      //---- OBJ_40 ----
      OBJ_40.setText("Exercice 2");
      OBJ_40.setName("OBJ_40");

      //---- WBIB ----
      WBIB.setComponentPopupMenu(BTD);
      WBIB.setName("WBIB");

      //---- OBJ_37 ----
      OBJ_37.setText("Exercice 1");
      OBJ_37.setName("OBJ_37");

      //---- OBJ_38 ----
      OBJ_38.setText("Exercice 2");
      OBJ_38.setName("OBJ_38");

      //---- DE1XB ----
      DE1XB.setName("DE1XB");

      //---- FE1XB ----
      FE1XB.setName("FE1XB");

      //---- DE2XB ----
      DE2XB.setName("DE2XB");

      //---- FE2XB ----
      FE2XB.setName("FE2XB");

      //---- DRE1X ----
      DRE1X.setName("DRE1X");

      //---- FRE1X ----
      FRE1X.setName("FRE1X");

      //---- DRE2X ----
      DRE2X.setName("DRE2X");

      //---- FRE2X ----
      FRE2X.setName("FRE2X");

      //---- DE1X ----
      DE1X.setName("DE1X");

      //---- FE1X ----
      FE1X.setName("FE1X");

      //---- DE2X ----
      DE2X.setName("DE2X");

      //---- FE2X ----
      FE2X.setName("FE2X");

      //---- WSOC ----
      WSOC.setComponentPopupMenu(BTD);
      WSOC.setName("WSOC");

      //---- OBJ_41 ----
      OBJ_41.setText("d\u00e9but");
      OBJ_41.setName("OBJ_41");

      //---- OBJ_46 ----
      OBJ_46.setText("d\u00e9but");
      OBJ_46.setName("OBJ_46");

      //---- WR1 ----
      WR1.setComponentPopupMenu(BTD);
      WR1.setName("WR1");

      //---- WR2 ----
      WR2.setComponentPopupMenu(BTD);
      WR2.setName("WR2");

      //---- OBJ_25 ----
      OBJ_25.setText("fin");
      OBJ_25.setName("OBJ_25");

      //---- OBJ_47 ----
      OBJ_47.setText("fin");
      OBJ_47.setName("OBJ_47");

      //---- BT_ChgSoc ----
      BT_ChgSoc.setText("");
      BT_ChgSoc.setToolTipText("Changement d'\u00e9tablissement");
      BT_ChgSoc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ChgSoc.setName("BT_ChgSoc");
      BT_ChgSoc.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ChgSocActionPerformed(e);
        }
      });

      //---- label1 ----
      label1.setText("Biblioth\u00e8que r\u00e9elle \u00e0 mettre \u00e0 jour");
      label1.setName("label1");

      //---- OBJ_48 ----
      OBJ_48.setText("d\u00e9but");
      OBJ_48.setName("OBJ_48");

      //---- OBJ_42 ----
      OBJ_42.setText("d\u00e9but");
      OBJ_42.setName("OBJ_42");

      //---- OBJ_26 ----
      OBJ_26.setText("fin");
      OBJ_26.setName("OBJ_26");

      //---- OBJ_49 ----
      OBJ_49.setText("fin");
      OBJ_49.setName("OBJ_49");

      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(OBJ_23, GroupLayout.PREFERRED_SIZE, 950, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(45, 45, 45)
            .addComponent(WSOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
            .addGap(5, 5, 5)
            .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
            .addGap(43, 43, 43)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 400, GroupLayout.PREFERRED_SIZE)
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(DE1XB, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(FE1XB, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE))))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(165, 165, 165)
            .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
            .addGap(12, 12, 12)
            .addComponent(DE2XB, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
            .addGap(12, 12, 12)
            .addComponent(FE2XB, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, 950, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(45, 45, 45)
            .addComponent(label1)
            .addGap(54, 54, 54)
            .addComponent(WBIB, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(165, 165, 165)
            .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
            .addGap(30, 30, 30)
            .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
            .addGap(17, 17, 17)
            .addComponent(DRE1X, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
            .addGap(18, 18, 18)
            .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE)
            .addGap(14, 14, 14)
            .addComponent(FRE1X, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(165, 165, 165)
            .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
            .addGap(30, 30, 30)
            .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
            .addGap(17, 17, 17)
            .addComponent(DRE2X, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
            .addGap(18, 18, 18)
            .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE)
            .addGap(14, 14, 14)
            .addComponent(FRE2X, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 950, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(45, 45, 45)
            .addComponent(WR1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
            .addGap(100, 100, 100)
            .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
            .addGap(12, 12, 12)
            .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
            .addGap(17, 17, 17)
            .addComponent(DE1X, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
            .addGap(18, 18, 18)
            .addComponent(OBJ_26, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE)
            .addGap(14, 14, 14)
            .addComponent(FE1X, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(45, 45, 45)
            .addComponent(WR2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
            .addGap(100, 100, 100)
            .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
            .addGap(12, 12, 12)
            .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
            .addGap(17, 17, 17)
            .addComponent(DE2X, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
            .addGap(18, 18, 18)
            .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE)
            .addGap(14, 14, 14)
            .addComponent(FE2X, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE))
      );
      P_CentreLayout.setVerticalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(30, 30, 30)
            .addComponent(OBJ_23, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(6, 6, 6)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(WSOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(P_CentreLayout.createParallelGroup()
                  .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(DE1XB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(FE1XB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
            .addGap(4, 4, 4)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(DE2XB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addComponent(FE2XB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGap(9, 9, 9)
            .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(7, 7, 7)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(label1)
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(WBIB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            .addGap(14, 14, 14)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
              .addComponent(DRE1X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
              .addComponent(FRE1X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGap(5, 5, 5)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
              .addComponent(DRE2X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(FRE2X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGap(8, 8, 8)
            .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(16, 16, 16)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(WR1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
              .addComponent(DE1X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_26, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
              .addComponent(FE1X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGap(4, 4, 4)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(WR2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
              .addComponent(DE2X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
              .addComponent(FE2X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
      );
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== OBJ_4 ========
    {
      OBJ_4.setText("Commandes");
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Choix du Papier");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setText("Annuler");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_9);
      OBJ_4.addSeparator();

      //---- OBJ_10 ----
      OBJ_10.setText("Exploitation");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_10);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Invite");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private JLabel LOCTP;
  private JLabel label2;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  private JPanel P_Centre;
  private JXTitledSeparator OBJ_24;
  private JXTitledSeparator OBJ_23;
  private JXTitledSeparator OBJ_22;
  private JLabel OBJ_33;
  private JLabel OBJ_30;
  private JLabel OBJ_32;
  private JLabel OBJ_39;
  private JLabel OBJ_40;
  private XRiTextField WBIB;
  private JLabel OBJ_37;
  private JLabel OBJ_38;
  private XRiTextField DE1XB;
  private XRiTextField FE1XB;
  private XRiTextField DE2XB;
  private XRiTextField FE2XB;
  private XRiTextField DRE1X;
  private XRiTextField FRE1X;
  private XRiTextField DRE2X;
  private XRiTextField FRE2X;
  private XRiTextField DE1X;
  private XRiTextField FE1X;
  private XRiTextField DE2X;
  private XRiTextField FE2X;
  private XRiTextField WSOC;
  private JLabel OBJ_41;
  private JLabel OBJ_46;
  private XRiTextField WR1;
  private XRiTextField WR2;
  private JLabel OBJ_25;
  private JLabel OBJ_47;
  private JButton BT_ChgSoc;
  private JLabel label1;
  private JLabel OBJ_48;
  private JLabel OBJ_42;
  private JLabel OBJ_26;
  private JLabel OBJ_49;
  private JMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
