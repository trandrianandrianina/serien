
package ri.serien.libecranrpg.scgm.SCGM22FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;

/**
 * @author Stéphane Vénéri
 */
public class SCGM22FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  public SCGM22FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    TRI1.setValeurs("1", "RB");
    TRI2.setValeurs("2", "RB");
    TRI3.setValeurs("3", "RB");
    TRI4.setValeurs("4", "RB");
    TRI5.setValeurs("5", "RB");
    TRI6.setValeurs("6", "RB");
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Choix ordre de classement"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    TRI6 = new XRiRadioButton();
    TRI1 = new XRiRadioButton();
    TRI2 = new XRiRadioButton();
    TRI3 = new XRiRadioButton();
    TRI4 = new XRiRadioButton();
    TRI5 = new XRiRadioButton();
    RB_GRP = new ButtonGroup();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(430, 290));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);
        
        // ======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder("Choix ordre de classement"));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          
          // ---- TRI6 ----
          TRI6.setText("Rep + Solde d\u00e9croissant");
          TRI6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TRI6.setName("TRI6");
          
          // ---- TRI1 ----
          TRI1.setText("Num\u00e9ro de tiers");
          TRI1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TRI1.setName("TRI1");
          
          // ---- TRI2 ----
          TRI2.setText("Nom de tiers");
          TRI2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TRI2.setName("TRI2");
          
          // ---- TRI3 ----
          TRI3.setText("Solde d\u00e9croissant");
          TRI3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TRI3.setName("TRI3");
          
          // ---- TRI4 ----
          TRI4.setText("Rep + N\u00b0 de Tiers");
          TRI4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TRI4.setName("TRI4");
          
          // ---- TRI5 ----
          TRI5.setText("Rep + Nom de Tiers");
          TRI5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TRI5.setName("TRI5");
          
          GroupLayout p_recupLayout = new GroupLayout(p_recup);
          p_recup.setLayout(p_recupLayout);
          p_recupLayout.setHorizontalGroup(p_recupLayout.createParallelGroup()
              .addGroup(p_recupLayout.createSequentialGroup().addGap(19, 19, 19).addGroup(
                  p_recupLayout.createParallelGroup().addComponent(TRI1, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
                      .addComponent(TRI2, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
                      .addComponent(TRI3, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
                      .addComponent(TRI4, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
                      .addComponent(TRI5, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
                      .addComponent(TRI6, GroupLayout.PREFERRED_SIZE, 168, GroupLayout.PREFERRED_SIZE))));
          p_recupLayout.setVerticalGroup(p_recupLayout.createParallelGroup()
              .addGroup(p_recupLayout.createSequentialGroup().addGap(30, 30, 30)
                  .addComponent(TRI1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(7, 7, 7)
                  .addComponent(TRI2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(7, 7, 7)
                  .addComponent(TRI3, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(7, 7, 7)
                  .addComponent(TRI4, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(7, 7, 7)
                  .addComponent(TRI5, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(7, 7, 7)
                  .addComponent(TRI6, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)));
        }
        p_contenu.add(p_recup);
        p_recup.setBounds(15, 15, 230, 260);
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ---- RB_GRP ----
    RB_GRP.add(TRI6);
    RB_GRP.add(TRI1);
    RB_GRP.add(TRI2);
    RB_GRP.add(TRI3);
    RB_GRP.add(TRI4);
    RB_GRP.add(TRI5);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_recup;
  private XRiRadioButton TRI6;
  private XRiRadioButton TRI1;
  private XRiRadioButton TRI2;
  private XRiRadioButton TRI3;
  private XRiRadioButton TRI4;
  private XRiRadioButton TRI5;
  private ButtonGroup RB_GRP;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
