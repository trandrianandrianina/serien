
package ri.serien.libecranrpg.scgm.SCGM9UFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SCGM9UFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SCGM9UFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    DAT1.setValeurs("1", "RB");
    AFF1.setValeurs("2", "RB");
    AFF2.setValeurs("3", "RB");
    REF1.setValeurs("4", "RB");
    REF2.setValeurs("5", "RB");
    PCE1.setValeurs("6", "RB");
    PCE2.setValeurs("7", "RB");
    LIB1.setValeurs("8", "RB");
    LIB2.setValeurs("9", "RB");
    DEMTRS.setValeurs("10", "RB");
    REL1.setValeurs("11", "RB");
    REL2.setValeurs("12", "RB");
    ECHL.setValeurs("13", "RB");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // POSD.setEnabled( lexique.isPresent("POSD"));
    // RB13.setVisible( lexique.isPresent("RB"));
    // RB13.setEnabled( lexique.isPresent("RB"));
    // RB13.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("13"));
    // OBJ_12.setVisible( lexique.isPresent("RB"));
    // OBJ_12.setEnabled( lexique.isPresent("RB"));
    // DAT1.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("01"));
    // RB11.setVisible( lexique.isPresent("RB"));
    // RB11.setEnabled( lexique.isPresent("RB"));
    // RB11.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("11"));
    // DEMTRS.setVisible( lexique.isPresent("RB"));
    // DEMTRS.setEnabled( lexique.isPresent("RB"));
    // DEMTRS.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("10"));
    // LIB2.setVisible( lexique.isPresent("RB"));
    // LIB2.setEnabled( lexique.isPresent("RB"));
    // LIB2.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("09"));
    // LIB1.setVisible( lexique.isPresent("RB"));
    // LIB1.setEnabled( lexique.isPresent("RB"));
    // LIB1.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("08"));
    // PCE2.setVisible( lexique.isPresent("RB"));
    // PCE2.setEnabled( lexique.isPresent("RB"));
    // PCE2.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("07"));
    // PCE1.setVisible( lexique.isPresent("RB"));
    // PCE1.setEnabled( lexique.isPresent("RB"));
    // PCE1.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("06"));
    // REF2.setVisible( lexique.isPresent("RB"));
    // REF2.setEnabled( lexique.isPresent("RB"));
    // REF2.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("05"));
    // REF1.setVisible( lexique.isPresent("RB"));
    // REF1.setEnabled( lexique.isPresent("RB"));
    // REF1.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("04"));
    // AFF2.setVisible( lexique.isPresent("RB"));
    // AFF2.setEnabled( lexique.isPresent("RB"));
    // AFF2.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("03"));
    // AFF1.setVisible( lexique.isPresent("RB"));
    // AFF1.setEnabled( lexique.isPresent("RB"));
    // AFF1.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("02"));
    // RB12.setVisible( lexique.isPresent("RB"));
    // RB12.setEnabled( lexique.isPresent("RB"));
    // RB12.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("12"));
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Visualisation et lettrages"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (RB13.isSelected())
    // lexique.HostFieldPutData("RB", 0, "13");
    // if (DAT1.isSelected())
    // lexique.HostFieldPutData("RB", 0, "01");
    // if (RB11.isSelected())
    // lexique.HostFieldPutData("RB", 0, "11");
    // if (DEMTRS.isSelected())
    // lexique.HostFieldPutData("RB", 0, "10");
    // if (LIB2.isSelected())
    // lexique.HostFieldPutData("RB", 0, "09");
    // if (LIB1.isSelected())
    // lexique.HostFieldPutData("RB", 0, "08");
    // if (PCE2.isSelected())
    // lexique.HostFieldPutData("RB", 0, "07");
    // if (PCE1.isSelected())
    // lexique.HostFieldPutData("RB", 0, "06");
    // if (REF2.isSelected())
    // lexique.HostFieldPutData("RB", 0, "05");
    // if (REF1.isSelected())
    // lexique.HostFieldPutData("RB", 0, "04");
    // if (AFF2.isSelected())
    // lexique.HostFieldPutData("RB", 0, "03");
    // if (AFF1.isSelected())
    // lexique.HostFieldPutData("RB", 0, "02");
    // if (RB12.isSelected())
    // lexique.HostFieldPutData("RB", 0, "12");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    DAT1 = new XRiRadioButton();
    AFF1 = new XRiRadioButton();
    AFF2 = new XRiRadioButton();
    REF1 = new XRiRadioButton();
    REF2 = new XRiRadioButton();
    PCE1 = new XRiRadioButton();
    PCE2 = new XRiRadioButton();
    LIB1 = new XRiRadioButton();
    LIB2 = new XRiRadioButton();
    DEMTRS = new XRiRadioButton();
    REL1 = new XRiRadioButton();
    REL2 = new XRiRadioButton();
    ECHL = new XRiRadioButton();
    POSD = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(685, 540));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Options de visualisation"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- DAT1 ----
          DAT1.setText("Afficher la position du compte (dont exigible) en date du");
          DAT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          DAT1.setName("DAT1");
          panel1.add(DAT1);
          DAT1.setBounds(20, 50, 385, 20);

          //---- AFF1 ----
          AFF1.setText("Visualiser les \u00e9critures non lettr\u00e9es par num\u00e9ro d'affectation");
          AFF1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          AFF1.setName("AFF1");
          panel1.add(AFF1);
          AFF1.setBounds(20, 84, 460, 20);

          //---- AFF2 ----
          AFF2.setText("Visualiser les \u00e9critures par num\u00e9ro d'affectation (avec \u00e9critures lettr\u00e9es)");
          AFF2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          AFF2.setName("AFF2");
          panel1.add(AFF2);
          AFF2.setBounds(20, 118, 460, 20);

          //---- REF1 ----
          REF1.setText("Visualiser les \u00e9critures non lettr\u00e9es par r\u00e9f\u00e9rence de classement");
          REF1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REF1.setName("REF1");
          panel1.add(REF1);
          REF1.setBounds(20, 152, 460, 20);

          //---- REF2 ----
          REF2.setText("Visualiser les \u00e9critures / r\u00e9f\u00e9rence de classement (avec \u00e9critures lettr\u00e9es)");
          REF2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REF2.setName("REF2");
          panel1.add(REF2);
          REF2.setBounds(20, 186, 460, 20);

          //---- PCE1 ----
          PCE1.setText("Visualiser les \u00e9critures non lettr\u00e9es par num\u00e9ro de pi\u00e8ce");
          PCE1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PCE1.setName("PCE1");
          panel1.add(PCE1);
          PCE1.setBounds(20, 220, 460, 20);

          //---- PCE2 ----
          PCE2.setText("Visualiser les \u00e9critures par num\u00e9ro de pi\u00e8ce (avec \u00e9critures lettr\u00e9es)");
          PCE2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PCE2.setName("PCE2");
          panel1.add(PCE2);
          PCE2.setBounds(20, 254, 460, 20);

          //---- LIB1 ----
          LIB1.setText("Visualiser les \u00e9critures non lettr\u00e9es par libell\u00e9");
          LIB1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          LIB1.setName("LIB1");
          panel1.add(LIB1);
          LIB1.setBounds(20, 288, 460, 20);

          //---- LIB2 ----
          LIB2.setText("Visualiser les \u00e9critures par libell\u00e9 (avec \u00e9critures lettr\u00e9es)");
          LIB2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          LIB2.setName("LIB2");
          panel1.add(LIB2);
          LIB2.setBounds(20, 322, 460, 20);

          //---- DEMTRS ----
          DEMTRS.setText("Transfert compte \u00e0 compte (solde et lettrage compte origine)");
          DEMTRS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          DEMTRS.setName("DEMTRS");
          panel1.add(DEMTRS);
          DEMTRS.setBounds(20, 356, 460, 20);

          //---- REL1 ----
          REL1.setText("Demande \u00e9dition relance client (liste de contr\u00f4le seulement)");
          REL1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REL1.setName("REL1");
          panel1.add(REL1);
          REL1.setBounds(20, 390, 460, 20);

          //---- REL2 ----
          REL2.setText("Demande \u00e9dition relance client (avec mise \u00e0 jour des niveaux de relance)");
          REL2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REL2.setName("REL2");
          panel1.add(REL2);
          REL2.setBounds(20, 424, 460, 20);

          //---- ECHL ----
          ECHL.setText("Demande d'affichage par date d'\u00e9ch\u00e9ance");
          ECHL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ECHL.setName("ECHL");
          panel1.add(ECHL);
          ECHL.setBounds(20, 458, 460, 20);

          //---- POSD ----
          POSD.setName("POSD");
          panel1.add(POSD);
          POSD.setBounds(410, 46, 60, POSD.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 495, 520);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(DAT1);
    RB_GRP.add(AFF1);
    RB_GRP.add(AFF2);
    RB_GRP.add(REF1);
    RB_GRP.add(REF2);
    RB_GRP.add(PCE1);
    RB_GRP.add(PCE2);
    RB_GRP.add(LIB1);
    RB_GRP.add(LIB2);
    RB_GRP.add(DEMTRS);
    RB_GRP.add(REL1);
    RB_GRP.add(REL2);
    RB_GRP.add(ECHL);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiRadioButton DAT1;
  private XRiRadioButton AFF1;
  private XRiRadioButton AFF2;
  private XRiRadioButton REF1;
  private XRiRadioButton REF2;
  private XRiRadioButton PCE1;
  private XRiRadioButton PCE2;
  private XRiRadioButton LIB1;
  private XRiRadioButton LIB2;
  private XRiRadioButton DEMTRS;
  private XRiRadioButton REL1;
  private XRiRadioButton REL2;
  private XRiRadioButton ECHL;
  private XRiTextField POSD;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
