
package ri.serien.libecranrpg.scgm.SCGM04FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class SCGM04FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _T01_Top = { "T01", "T02", "T03", "T04", "T05", "T06", "T07", "T08", "T09", "T10", "T11", "T12", "T13", "T14", "T15", };
  private String[] _T01_Title = { "Période", "Jo", "Fol.", "Nb Ec", "Débit", "Crédit", "Création", "Homolog.", "", };
  private String[][] _T01_Data = { { "DP01", "JO01", "FO01", "EC01", "DB01", "CR01", "DC01", "DH01", "ET01", },
      { "DP02", "JO02", "FO02", "EC02", "DB02", "CR02", "DC02", "DH02", "ET02", },
      { "DP03", "JO03", "FO03", "EC03", "DB03", "CR03", "DC03", "DH03", "ET03", },
      { "DP04", "JO04", "FO04", "EC04", "DB04", "CR04", "DC04", "DH04", "ET04", },
      { "DP05", "JO05", "FO05", "EC05", "DB05", "CR05", "DC05", "DH05", "ET05", },
      { "DP06", "JO06", "FO06", "EC06", "DB06", "CR06", "DC06", "DH06", "ET06", },
      { "DP07", "JO07", "FO07", "EC07", "DB07", "CR07", "DC07", "DH07", "ET07", },
      { "DP08", "JO08", "FO08", "EC08", "DB08", "CR08", "DC08", "DH08", "ET08", },
      { "DP09", "JO09", "FO09", "EC09", "DB09", "CR09", "DC09", "DH09", "ET09", },
      { "DP10", "JO10", "FO10", "EC10", "DB10", "CR10", "DC10", "DH10", "ET10", },
      { "DP11", "JO11", "FO11", "EC11", "DB11", "CR11", "DC11", "DH11", "ET11", },
      { "DP12", "JO12", "FO12", "EC12", "DB12", "CR12", "DC12", "DH12", "ET12", },
      { "DP13", "JO13", "FO13", "EC13", "DB13", "CR13", "DC13", "DH13", "ET13", },
      { "DP14", "JO14", "FO14", "EC14", "DB14", "CR14", "DC14", "DH14", "ET14", },
      { "DP15", "JO15", "FO15", "EC15", "DB15", "CR15", "DC15", "DH15", "ET15", }, };
  private int[] _T01_Width = { 57, 22, 37, 46, 105, 105, 72, 72, 33, };
  private int[] justif = { SwingConstants.LEFT, SwingConstants.LEFT, SwingConstants.LEFT, SwingConstants.LEFT, SwingConstants.RIGHT,
      SwingConstants.RIGHT, SwingConstants.LEFT, SwingConstants.LEFT, SwingConstants.LEFT };
  
  public SCGM04FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    T01.setAspectTable(_T01_Top, _T01_Title, _T01_Data, _T01_Width, false, justif, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(),_T01_Top,justif);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Titre
    setTitle("Recherche des folios");
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _T01_Top, "1", "enter");
    T01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "enter");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void T01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _T01_Top, "1", "enter", e);
    if (T01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "enter");
    }
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _T01_Top, "1", "enter");
    T01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "enter");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    xTitledSeparator1 = new JXTitledSeparator();
    SCROLLPANE_LIST = new JScrollPane();
    T01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    popupMenu1 = new JPopupMenu();
    menuItem1 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(870, 370));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //---- xTitledSeparator1 ----
        xTitledSeparator1.setTitle("Recherche des folios");
        xTitledSeparator1.setName("xTitledSeparator1");

        //======== SCROLLPANE_LIST ========
        {
          SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

          //---- T01 ----
          T01.setComponentPopupMenu(popupMenu1);
          T01.setName("T01");
          T01.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              T01MouseClicked(e);
            }
          });
          SCROLLPANE_LIST.setViewportView(T01);
        }

        //---- BT_PGUP ----
        BT_PGUP.setText("");
        BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGUP.setName("BT_PGUP");

        //---- BT_PGDOWN ----
        BT_PGDOWN.setText("");
        BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGDOWN.setName("BT_PGDOWN");

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, 655, GroupLayout.PREFERRED_SIZE))
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(25, 25, 25)
              .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 605, GroupLayout.PREFERRED_SIZE)
              .addGap(5, 5, 5)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(30, 30, 30)
              .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGap(21, 21, 21)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 122, GroupLayout.PREFERRED_SIZE)
                  .addGap(18, 18, 18)
                  .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 122, GroupLayout.PREFERRED_SIZE))))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== popupMenu1 ========
    {
      popupMenu1.setName("popupMenu1");

      //---- menuItem1 ----
      menuItem1.setText("Choisir");
      menuItem1.setName("menuItem1");
      menuItem1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem1ActionPerformed(e);
        }
      });
      popupMenu1.add(menuItem1);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JXTitledSeparator xTitledSeparator1;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable T01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu popupMenu1;
  private JMenuItem menuItem1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
