
package ri.serien.libecranrpg.scgm.SCGM02FM;
// Nom Fichier: pop_SCGM02FM_FMTB1_195.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SCGM02FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SCGM02FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(OBJ_5);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    ACT.setVisible(lexique.isPresent("ACT"));
    OBJ_11.setVisible(interpreteurD.analyseExpression("@TEST@").equalsIgnoreCase("Zoné..."));
    OBJ_16.setVisible(interpreteurD.analyseExpression("@NUM_COUR@").equalsIgnoreCase("1"));
    OBJ_15.setVisible(interpreteurD.analyseExpression("@NUM_COUR@").equalsIgnoreCase("2"));
    OBJ_14.setVisible(interpreteurD.analyseExpression("@NUM_COUR@").equalsIgnoreCase("4"));
    OBJ_13.setVisible(interpreteurD.analyseExpression("@NUM_COUR@").equalsIgnoreCase("5"));
    OBJ_12.setVisible(interpreteurD.analyseExpression("@NUM_COUR@").equalsIgnoreCase("3"));
    OBJ_10.setVisible(interpreteurD.analyseExpression("@EXCEL@").equalsIgnoreCase("Excel"));
    OBJ_9.setVisible(!interpreteurD.analyseExpression("@NUM_COUR@").trim().equalsIgnoreCase("5"));
    OBJ_8.setVisible(!interpreteurD.analyseExpression("@NUM_COUR@").trim().equalsIgnoreCase("4"));
    OBJ_7.setVisible(!interpreteurD.analyseExpression("@NUM_COUR@").trim().equalsIgnoreCase("3"));
    OBJ_6.setVisible(!interpreteurD.analyseExpression("@NUM_COUR@").trim().equalsIgnoreCase("2"));
    OBJ_5.setVisible(!interpreteurD.analyseExpression("@NUM_COUR@").trim().equalsIgnoreCase("1"));
    
    // TODO Icones
    OBJ_11.setIcon(lexique.chargerImage("images/point02.gif", true));
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@(TITLE)@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_5 = new JButton();
    OBJ_6 = new JButton();
    OBJ_7 = new JButton();
    OBJ_8 = new JButton();
    OBJ_9 = new JButton();
    OBJ_10 = new JButton();
    OBJ_12 = new JButton();
    OBJ_13 = new JButton();
    OBJ_14 = new JButton();
    OBJ_15 = new JButton();
    OBJ_16 = new JButton();
    OBJ_11 = new JButton();
    ACT = new XRiTextField();

    //======== this ========
    setName("this");
    setLayout(null);

    //---- OBJ_5 ----
    OBJ_5.setText("Retour");
    OBJ_5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_5.setName("OBJ_5");
    add(OBJ_5);
    OBJ_5.setBounds(42, 24, 136, 24);

    //---- OBJ_6 ----
    OBJ_6.setText("Comptable");
    OBJ_6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_6.setName("OBJ_6");
    add(OBJ_6);
    OBJ_6.setBounds(42, 56, 136, 24);

    //---- OBJ_7 ----
    OBJ_7.setText("Standard zon\u00e9");
    OBJ_7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_7.setName("OBJ_7");
    add(OBJ_7);
    OBJ_7.setBounds(42, 88, 136, 24);

    //---- OBJ_8 ----
    OBJ_8.setText("Papier 12''");
    OBJ_8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_8.setName("OBJ_8");
    add(OBJ_8);
    OBJ_8.setBounds(42, 120, 136, 24);

    //---- OBJ_9 ----
    OBJ_9.setText("Feuille A4");
    OBJ_9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_9.setName("OBJ_9");
    add(OBJ_9);
    OBJ_9.setBounds(42, 152, 136, 24);

    //---- OBJ_10 ----
    OBJ_10.setText("Disque Excel");
    OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_10.setName("OBJ_10");
    add(OBJ_10);
    OBJ_10.setBounds(42, 186, 136, 24);

    //---- OBJ_12 ----
    OBJ_12.setText("Standard zon\u00e9");
    OBJ_12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_12.setName("OBJ_12");
    add(OBJ_12);
    OBJ_12.setBounds(42, 88, 136, 24);

    //---- OBJ_13 ----
    OBJ_13.setText("Feuille A4");
    OBJ_13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_13.setName("OBJ_13");
    add(OBJ_13);
    OBJ_13.setBounds(42, 152, 136, 24);

    //---- OBJ_14 ----
    OBJ_14.setText("Papier 12''");
    OBJ_14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_14.setName("OBJ_14");
    add(OBJ_14);
    OBJ_14.setBounds(42, 120, 136, 24);

    //---- OBJ_15 ----
    OBJ_15.setText("Comptable");
    OBJ_15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_15.setName("OBJ_15");
    add(OBJ_15);
    OBJ_15.setBounds(42, 56, 136, 24);

    //---- OBJ_16 ----
    OBJ_16.setText("Retour");
    OBJ_16.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_16.setName("OBJ_16");
    add(OBJ_16);
    OBJ_16.setBounds(42, 24, 136, 24);

    //---- OBJ_11 ----
    OBJ_11.setText("");
    OBJ_11.setToolTipText("Modification de la hauteur");
    OBJ_11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_11.setName("OBJ_11");
    OBJ_11.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_11ActionPerformed(e);
      }
    });
    add(OBJ_11);
    OBJ_11.setBounds(180, 88, 42, 24);

    //---- ACT ----
    ACT.setName("ACT");
    add(ACT);
    ACT.setBounds(7, 6, 18, 20);

    setPreferredSize(new Dimension(231, 220));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JButton OBJ_5;
  private JButton OBJ_6;
  private JButton OBJ_7;
  private JButton OBJ_8;
  private JButton OBJ_9;
  private JButton OBJ_10;
  private JButton OBJ_12;
  private JButton OBJ_13;
  private JButton OBJ_14;
  private JButton OBJ_15;
  private JButton OBJ_16;
  private JButton OBJ_11;
  private XRiTextField ACT;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
