
package ri.serien.libecranrpg.scgm.SCGM18FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerListModel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiSpinner;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

public class SCGM18FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  // TODO declarations classe spécifiques...
  
  public SCGM18FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    FRAIS.setValeursSelection("OUI", "NON");
    REPON1.setValeursSelection("OUI", "NON");
    DETCOL.setValeursSelection("X", "");
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSOC@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGPERX@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MESCLO@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // TODO setData spécifiques...
    OBJ_63.setVisible(lexique.isTrue("81"));
    if (interpreteurD.analyseExpression("@MESCLO@").trim().equals("*Multi SOCIETES*")) {
      p_simplesoc.setVisible(false);
      p_multisoc.setVisible(true);
    }
    else {
      p_simplesoc.setVisible(true);
      p_multisoc.setVisible(false);
    }
    
    

    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_63 = new JLabel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    OBJ_41 = new JLabel();
    PERIO1 = new XRiTextField();
    OBJ_35 = new JLabel();
    OBJ_36 = new JLabel();
    CLADEB = new XRiTextField();
    CLAFIN = new XRiTextField();
    OBJ_38 = new JLabel();
    OBJ_39 = new JLabel();
    CPTDEB = new XRiTextField();
    CPTFIN = new XRiTextField();
    OBJ_33 = new JLabel();
    NIVEAU = new XRiSpinner();
    REPON1 = new XRiCheckBox();
    FRAIS = new XRiCheckBox();
    xTitledSeparator3 = new JXTitledSeparator();
    xTitledSeparator4 = new JXTitledSeparator();
    xTitledSeparator5 = new JXTitledSeparator();
    p_simplesoc = new JPanel();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    z_dgnom_ = new RiZoneSortie();
    label2 = new JLabel();
    z_wencx_2 = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    p_multisoc = new JPanel();
    bouton_etablissement2 = new SNBoutonRecherche();
    l_libelle = new JLabel();
    DETCOL = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Edition de la balance g\u00e9n\u00e9rale");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- OBJ_63 ----
          OBJ_63.setText(
              "Vous param\u00e9trez une demande de traitement qui sera ex\u00e9cut\u00e9e automatiquement par le planning des travaux S\u00e9rie N");
          OBJ_63.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_63.setForeground(Color.red);
          OBJ_63.setName("OBJ_63");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout
              .setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup().addGroup(p_tete_gaucheLayout.createSequentialGroup()
                  .addGap(5, 5, 5).addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 689, GroupLayout.PREFERRED_SIZE)));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup().addComponent(OBJ_63));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
          
          // ---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Type de sortie");
              riSousMenu_bt_export.setToolTipText("Type de sortie");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.CENTER);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(700, 420));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          
          // ---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");
          
          // ---- OBJ_41 ----
          OBJ_41.setText("P\u00e9riode \u00e0 \u00e9diter");
          OBJ_41.setName("OBJ_41");
          
          // ---- PERIO1 ----
          PERIO1.setComponentPopupMenu(BTD);
          PERIO1.setToolTipText("sous la forme MMAA ou MM.AA");
          PERIO1.setName("PERIO1");
          
          // ---- OBJ_35 ----
          OBJ_35.setText("Classe de d\u00e9but");
          OBJ_35.setName("OBJ_35");
          
          // ---- OBJ_36 ----
          OBJ_36.setText("Classe de fin");
          OBJ_36.setName("OBJ_36");
          
          // ---- CLADEB ----
          CLADEB.setComponentPopupMenu(BTD);
          CLADEB.setName("CLADEB");
          
          // ---- CLAFIN ----
          CLAFIN.setComponentPopupMenu(BTD);
          CLAFIN.setName("CLAFIN");
          
          // ---- OBJ_38 ----
          OBJ_38.setText("Num\u00e9ro de compte de d\u00e9but");
          OBJ_38.setName("OBJ_38");
          
          // ---- OBJ_39 ----
          OBJ_39.setText("Num\u00e9ro de compte de fin");
          OBJ_39.setName("OBJ_39");
          
          // ---- CPTDEB ----
          CPTDEB.setComponentPopupMenu(BTD);
          CPTDEB.setName("CPTDEB");
          
          // ---- CPTFIN ----
          CPTFIN.setComponentPopupMenu(BTD);
          CPTFIN.setName("CPTFIN");
          
          // ---- OBJ_33 ----
          OBJ_33.setText("Niveau d'\u00e9dition");
          OBJ_33.setName("OBJ_33");
          
          // ---- NIVEAU ----
          NIVEAU.setComponentPopupMenu(BTD);
          NIVEAU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          NIVEAU.setModel(new SpinnerListModel(new String[] { " ", "0", "1", "2", "3", "4", "5" }) {
            {
              setValue("0");
            }
          });
          NIVEAU.setName("NIVEAU");
          
          // ---- REPON1 ----
          REPON1.setText("Saut de page entre les classes");
          REPON1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON1.setName("REPON1");
          
          // ---- FRAIS ----
          FRAIS.setText("Ventilation des frais");
          FRAIS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          FRAIS.setName("FRAIS");
          
          // ---- xTitledSeparator3 ----
          xTitledSeparator3.setTitle("");
          xTitledSeparator3.setName("xTitledSeparator3");
          
          // ---- xTitledSeparator4 ----
          xTitledSeparator4.setTitle("Classes de comptes ou plage de comptes");
          xTitledSeparator4.setName("xTitledSeparator4");
          
          // ---- xTitledSeparator5 ----
          xTitledSeparator5.setTitle("");
          xTitledSeparator5.setName("xTitledSeparator5");
          
          // ======== p_simplesoc ========
          {
            p_simplesoc.setOpaque(false);
            p_simplesoc.setFont(
                p_simplesoc.getFont().deriveFont(p_simplesoc.getFont().getStyle() | Font.BOLD, p_simplesoc.getFont().getSize() + 3f));
            p_simplesoc.setName("p_simplesoc");
            
            // ---- z_etablissement_ ----
            z_etablissement_.setComponentPopupMenu(null);
            z_etablissement_.setText("@WSOC@");
            z_etablissement_.setName("z_etablissement_");
            
            // ---- bouton_etablissement ----
            bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
            bouton_etablissement.setName("bouton_etablissement");
            bouton_etablissement.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_etablissementActionPerformed(e);
              }
            });
            
            // ---- z_dgnom_ ----
            z_dgnom_.setText("@DGNOM@");
            z_dgnom_.setName("z_dgnom_");
            
            // ---- label2 ----
            label2.setText("En cours");
            label2.setName("label2");
            
            // ---- z_wencx_2 ----
            z_wencx_2.setText("@DGPERX@");
            z_wencx_2.setFont(z_wencx_2.getFont().deriveFont(z_wencx_2.getFont().getStyle() | Font.BOLD));
            z_wencx_2.setName("z_wencx_2");
            
            // ---- z_wencx_ ----
            z_wencx_.setText("@MESCLO@");
            z_wencx_.setName("z_wencx_");
            
            GroupLayout p_simplesocLayout = new GroupLayout(p_simplesoc);
            p_simplesoc.setLayout(p_simplesocLayout);
            p_simplesocLayout.setHorizontalGroup(p_simplesocLayout.createParallelGroup()
                .addGroup(p_simplesocLayout.createSequentialGroup().addGap(10, 10, 10)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(82, 82, 82)
                    .addGroup(p_simplesocLayout.createParallelGroup()
                        .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                        .addGroup(p_simplesocLayout.createSequentialGroup()
                            .addComponent(label2, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                            .addComponent(z_wencx_2, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                            .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)))));
            p_simplesocLayout.setVerticalGroup(p_simplesocLayout.createParallelGroup()
                .addGroup(p_simplesocLayout.createSequentialGroup().addGap(25, 25, 25).addComponent(z_etablissement_,
                    GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_simplesocLayout.createSequentialGroup().addGap(25, 25, 25).addComponent(bouton_etablissement,
                    GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_simplesocLayout.createSequentialGroup().addGap(10, 10, 10)
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addGroup(p_simplesocLayout.createParallelGroup()
                        .addGroup(p_simplesocLayout.createSequentialGroup().addGap(5, 5, 5).addComponent(label2))
                        .addComponent(z_wencx_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))));
          }
          
          // ======== p_multisoc ========
          {
            p_multisoc.setOpaque(false);
            p_multisoc.setName("p_multisoc");
            
            // ---- bouton_etablissement2 ----
            bouton_etablissement2.setToolTipText("Changement d'\u00e9tablissement");
            bouton_etablissement2.setName("bouton_etablissement2");
            bouton_etablissement2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_etablissementActionPerformed(e);
              }
            });
            
            // ---- l_libelle ----
            l_libelle.setText("S\u00e9lection multi-\u00e9tablissement");
            l_libelle
                .setFont(l_libelle.getFont().deriveFont(l_libelle.getFont().getStyle() | Font.BOLD, l_libelle.getFont().getSize() + 4f));
            l_libelle.setName("l_libelle");
            
            GroupLayout p_multisocLayout = new GroupLayout(p_multisoc);
            p_multisoc.setLayout(p_multisocLayout);
            p_multisocLayout.setHorizontalGroup(p_multisocLayout.createParallelGroup()
                .addGroup(p_multisocLayout.createSequentialGroup().addGap(5, 5, 5)
                    .addComponent(bouton_etablissement2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(22, 22, 22).addComponent(l_libelle, GroupLayout.PREFERRED_SIZE, 375, GroupLayout.PREFERRED_SIZE)));
            p_multisocLayout.setVerticalGroup(p_multisocLayout.createParallelGroup()
                .addGroup(p_multisocLayout.createSequentialGroup().addGap(20, 20, 20).addComponent(bouton_etablissement2,
                    GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_multisocLayout.createSequentialGroup().addGap(24, 24, 24).addComponent(l_libelle)));
          }
          
          // ---- DETCOL ----
          DETCOL.setText("Edition en d\u00e9tail de certains comptes collectifs");
          DETCOL.setName("DETCOL");
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(34, 34, 34).addComponent(sep_etablissement,
                  GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(39, 39, 39)
                  .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(p_simplesoc, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(p_multisoc, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(34, 34, 34).addComponent(xTitledSeparator3,
                  GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(49, 49, 49)
                  .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 215, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(PERIO1, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(34, 34, 34).addComponent(xTitledSeparator4,
                  GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(49, 49, 49)
                  .addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 215, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(CLADEB, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(75, 75, 75)
                  .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(CPTDEB, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(49, 49, 49)
                  .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 215, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(CLAFIN, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(75, 75, 75)
                  .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(CPTFIN, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(34, 34, 34).addComponent(xTitledSeparator5,
                  GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(49, 49, 49)
                  .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 215, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(NIVEAU, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(49, 49, 49)
                  .addComponent(REPON1, GroupLayout.PREFERRED_SIZE, 295, GroupLayout.PREFERRED_SIZE).addGap(15, 15, 15)
                  .addComponent(DETCOL, GroupLayout.PREFERRED_SIZE, 290, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(49, 49, 49).addComponent(FRAIS, GroupLayout.PREFERRED_SIZE, 435,
                  GroupLayout.PREFERRED_SIZE)));
          p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(34, 34, 34)
                  .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(4, 4, 4)
                  .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(p_simplesoc, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(p_multisoc, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addComponent(xTitledSeparator3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(19, 19, 19)
                  .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE,
                          20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(PERIO1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(21, 21, 21)
                  .addComponent(xTitledSeparator4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(11, 11, 11)
                  .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(CLADEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(CPTDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(4, 4, 4)
                          .addGroup(p_contenuLayout.createParallelGroup()
                              .addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                              .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
                  .addGap(7, 7, 7)
                  .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(CLAFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(CPTFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(4, 4, 4)
                          .addGroup(p_contenuLayout.createParallelGroup()
                              .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                              .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
                  .addGap(12, 12, 12)
                  .addComponent(xTitledSeparator5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(8, 8, 8)
                  .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(5, 5, 5).addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE,
                          20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(NIVEAU, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(7, 7, 7)
                  .addGroup(p_contenuLayout
                      .createParallelGroup().addComponent(REPON1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(DETCOL, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGap(10, 10, 10).addComponent(FRAIS, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
      
      // ---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_63;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private JLabel OBJ_41;
  private XRiTextField PERIO1;
  private JLabel OBJ_35;
  private JLabel OBJ_36;
  private XRiTextField CLADEB;
  private XRiTextField CLAFIN;
  private JLabel OBJ_38;
  private JLabel OBJ_39;
  private XRiTextField CPTDEB;
  private XRiTextField CPTFIN;
  private JLabel OBJ_33;
  private XRiSpinner NIVEAU;
  private XRiCheckBox REPON1;
  private XRiCheckBox FRAIS;
  private JXTitledSeparator xTitledSeparator3;
  private JXTitledSeparator xTitledSeparator4;
  private JXTitledSeparator xTitledSeparator5;
  private JPanel p_simplesoc;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private RiZoneSortie z_dgnom_;
  private JLabel label2;
  private RiZoneSortie z_wencx_2;
  private RiZoneSortie z_wencx_;
  private JPanel p_multisoc;
  private SNBoutonRecherche bouton_etablissement2;
  private JLabel l_libelle;
  private XRiCheckBox DETCOL;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
