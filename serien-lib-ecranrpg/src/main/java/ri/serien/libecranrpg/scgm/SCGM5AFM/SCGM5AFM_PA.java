
package ri.serien.libecranrpg.scgm.SCGM5AFM;
// Nom Fichier: pop_SCGM5AFM_FMTPA_640.java

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class SCGM5AFM_PA extends SNPanelEcranRPG implements ioFrame {
  
   
  // private String[] _LIST_Top = null;
  private String[] _L01_Title = { "HLD48", };
  private String[][] _L01_Data =
      { { "L01", }, { "L02", }, { "L03", }, { "L04", }, { "L05", }, { "L06", }, { "L07", }, { "L08", }, { "L09", }, { "L10", }, };
  private int[] _L01_Width = { 340, };
  // private String[][] _LIST_Title_Data_Brut = null;
  
  public SCGM5AFM_PA(ArrayList<?> param) {
    super(param);
    initComponents();
    // _LIST_Title_Data_Brut = initTable(LIST, "Courier New", Font.BOLD, 12, "Courier New", Font.PLAIN, 12);
    setDialog(true);
    
    // Ajout
    initDiverses();
    L01.setAspectTable(null, _L01_Title, _L01_Data, _L01_Width, false, null, null, null, null);
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, _LIST_Title_Data_Brut, _LIST_Top);
    
    
    TOTMTT.setEnabled(lexique.isPresent("TOTMTT"));
    
    // TODO Icones
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    OBJ_62.setIcon(lexique.chargerImage("images/pgup20.png", true));
    OBJ_68.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Affichage échéances dépassées"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // Touche="F3"
    // Scriptcall("G_Touche")
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "scgm5a"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_62ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGUP");
  }
  
  private void OBJ_68ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGDOWN");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    SCROLLPANE_LIST2 = new JScrollPane();
    L01 = new XRiTable();
    OBJ_18_OBJ_18 = new JLabel();
    BT_ENTER = new JButton();
    TOTMTT = new XRiTextField();
    OBJ_62 = new JButton();
    OBJ_68 = new JButton();
    OBJ_4 = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_9 = new JMenuItem();

    //======== this ========
    setName("this");

    //======== SCROLLPANE_LIST2 ========
    {
      SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

      //---- L01 ----
      L01.setName("L01");
      SCROLLPANE_LIST2.setViewportView(L01);
    }

    //---- OBJ_18_OBJ_18 ----
    OBJ_18_OBJ_18.setText("Montant total \u00e9ch\u00e9ances d\u00e9pass\u00e9es");
    OBJ_18_OBJ_18.setName("OBJ_18_OBJ_18");

    //---- BT_ENTER ----
    BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_ENTER.setName("BT_ENTER");
    BT_ENTER.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_17ActionPerformed(e);
      }
    });

    //---- TOTMTT ----
    TOTMTT.setName("TOTMTT");

    //---- OBJ_62 ----
    OBJ_62.setText("");
    OBJ_62.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_62.setName("OBJ_62");
    OBJ_62.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_62ActionPerformed(e);
      }
    });

    //---- OBJ_68 ----
    OBJ_68.setText("");
    OBJ_68.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_68.setName("OBJ_68");
    OBJ_68.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_68ActionPerformed(e);
      }
    });

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(25, 25, 25)
          .addComponent(SCROLLPANE_LIST2, GroupLayout.PREFERRED_SIZE, 370, GroupLayout.PREFERRED_SIZE)
          .addGap(5, 5, 5)
          .addGroup(layout.createParallelGroup()
            .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
            .addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)))
        .addGroup(layout.createSequentialGroup()
          .addGap(25, 25, 25)
          .addComponent(OBJ_18_OBJ_18, GroupLayout.PREFERRED_SIZE, 226, GroupLayout.PREFERRED_SIZE)
          .addGap(44, 44, 44)
          .addComponent(TOTMTT, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(339, 339, 339)
          .addComponent(BT_ENTER, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(20, 20, 20)
          .addGroup(layout.createParallelGroup()
            .addComponent(SCROLLPANE_LIST2, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
            .addGroup(layout.createSequentialGroup()
              .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
              .addGap(10, 10, 10)
              .addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)))
          .addGap(10, 10, 10)
          .addGroup(layout.createParallelGroup()
            .addGroup(layout.createSequentialGroup()
              .addGap(4, 4, 4)
              .addComponent(OBJ_18_OBJ_18, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addComponent(TOTMTT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          .addGap(12, 12, 12)
          .addComponent(BT_ENTER, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
    );

    //======== OBJ_4 ========
    {
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("Annuler");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_6);

      //---- OBJ_7 ----
      OBJ_7.setText("Exploitation");
      OBJ_7.setName("OBJ_7");
      OBJ_4.add(OBJ_7);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_9 ----
      OBJ_9.setText("Aide en ligne");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      BTD.add(OBJ_9);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable L01;
  private JLabel OBJ_18_OBJ_18;
  private JButton BT_ENTER;
  private XRiTextField TOTMTT;
  private JButton OBJ_62;
  private JButton OBJ_68;
  private JPopupMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_7;
  private JPopupMenu BTD;
  private JMenuItem OBJ_9;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
