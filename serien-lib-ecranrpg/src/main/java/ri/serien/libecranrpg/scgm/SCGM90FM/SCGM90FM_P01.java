
package ri.serien.libecranrpg.scgm.SCGM90FM;
// Nom Fichier: p_SCGM90P1_P01_244.java

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SCGM90FM_P01 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SCGM90FM_P01(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    // setDefaultButton(BT_ENTER);
    
    // Menu Command
    setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    FCH.setEnabled(lexique.isPresent("FCH"));
    
    // TODO Icones
    OBJ_29.setIcon(lexique.chargerImage("images/msgbox04.gif", true));
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@ENVP/+1/@ - @TITPG1@ @TITPG2@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "Attention");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_28 = new JPanel();
    OBJ_30 = new JLabel();
    OBJ_23 = new JLabel();
    OBJ_21 = new JLabel();
    FCH = new XRiTextField();
    OBJ_29 = new JLabel();
    OBJ_4 = new JMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();

    //======== this ========
    setName("this");

    //======== OBJ_28 ========
    {
      OBJ_28.setName("OBJ_28");
      OBJ_28.setLayout(null);

      //---- OBJ_30 ----
      OBJ_30.setText("Veuillez Valider");
      OBJ_30.setForeground(Color.darkGray);
      OBJ_30.setFont(OBJ_30.getFont().deriveFont(OBJ_30.getFont().getStyle() | Font.BOLD));
      OBJ_30.setName("OBJ_30");
      OBJ_28.add(OBJ_30);
      OBJ_30.setBounds(235, 10, 154, 24);
    }

    //---- OBJ_23 ----
    OBJ_23.setText("(Appuyez sur OK si le nom propos\u00e9 est correct, ou effacez-le pour sortir du programme sans rien \u00e9diter.)");
    OBJ_23.setHorizontalAlignment(SwingConstants.CENTER);
    OBJ_23.setForeground(Color.gray);
    OBJ_23.setName("OBJ_23");

    //---- OBJ_21 ----
    OBJ_21.setText("Donnez ici le nom du fichier \u00e0 \u00e9diter");
    OBJ_21.setForeground(Color.darkGray);
    OBJ_21.setFont(OBJ_21.getFont().deriveFont(OBJ_21.getFont().getStyle() | Font.BOLD));
    OBJ_21.setName("OBJ_21");

    //---- FCH ----
    FCH.setName("FCH");

    //---- OBJ_29 ----
    OBJ_29.setIcon(new ImageIcon("images/msgbox04.gif"));
    OBJ_29.setName("OBJ_29");

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addComponent(OBJ_23, GroupLayout.PREFERRED_SIZE, 1000, GroupLayout.PREFERRED_SIZE)
        .addGroup(layout.createSequentialGroup()
          .addGap(35, 35, 35)
          .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
          .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 607, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(326, 326, 326)
          .addGroup(layout.createParallelGroup()
            .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
            .addGroup(layout.createSequentialGroup()
              .addGap(220, 220, 220)
              .addComponent(FCH, GroupLayout.PREFERRED_SIZE, 103, GroupLayout.PREFERRED_SIZE))))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(137, 137, 137)
          .addGroup(layout.createParallelGroup()
            .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
            .addGroup(layout.createSequentialGroup()
              .addGap(5, 5, 5)
              .addComponent(FCH, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          .addGap(18, 18, 18)
          .addComponent(OBJ_23, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
          .addGap(343, 343, 343)
          .addGroup(layout.createParallelGroup()
            .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
            .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE))
          .addContainerGap())
    );

    //======== OBJ_4 ========
    {
      OBJ_4.setText("Commandes");
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("Exploitation");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_6);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel OBJ_28;
  private JLabel OBJ_30;
  private JLabel OBJ_23;
  private JLabel OBJ_21;
  private XRiTextField FCH;
  private JLabel OBJ_29;
  private JMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
