
package ri.serien.libecranrpg.scgm.SCGMRFFM;
// Nom Fichier: pop_SCGMRFFM_FMTB1_818.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.jdesktop.swingx.JXTitledSeparator;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SCGMRFFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public SCGMRFFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(OBJ_28);
    
    // Menu Command
    setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    L1SNS.setVisible(lexique.isPresent("L1SNS"));
    L1CJO.setVisible(lexique.isPresent("L1CJO"));
    L1CFO.setVisible(lexique.isPresent("L1CFO"));
    DTECR.setVisible(lexique.isPresent("DTECR"));
    L1PCE.setVisible(lexique.isPresent("L1PCE"));
    L1MTT.setVisible(lexique.isPresent("L1MTT"));
    L1LIB.setVisible(lexique.isPresent("L1LIB"));
    
    // TODO Icones
    
    // Titre
    // setTitle(???);
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_28ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "Attention");
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(OBJ_7.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    L1LIB = new XRiTextField();
    L1MTT = new XRiTextField();
    OBJ_28 = new JButton();
    L1PCE = new XRiTextField();
    OBJ_15 = new JLabel();
    DTECR = new XRiTextField();
    OBJ_21 = new JLabel();
    OBJ_23 = new JLabel();
    OBJ_26 = new JLabel();
    OBJ_17 = new JLabel();
    L1CFO = new XRiTextField();
    L1CJO = new XRiTextField();
    OBJ_19 = new JLabel();
    L1SNS = new XRiTextField();
    xTitledSeparator1 = new JXTitledSeparator();
    OBJ_4 = new JMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    OBJ_7 = new JPopupMenu();
    OBJ_8 = new JMenuItem();

    //======== this ========
    setName("this");
    setLayout(null);

    //---- L1LIB ----
    L1LIB.setName("L1LIB");
    add(L1LIB);
    L1LIB.setBounds(85, 105, 270, 20);

    //---- L1MTT ----
    L1MTT.setName("L1MTT");
    add(L1MTT);
    L1MTT.setBounds(85, 80, 86, 20);

    //---- OBJ_28 ----
    OBJ_28.setText("Retour");
    OBJ_28.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_28.setName("OBJ_28");
    OBJ_28.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_28ActionPerformed(e);
      }
    });
    add(OBJ_28);
    OBJ_28.setBounds(295, 170, 82, 24);

    //---- L1PCE ----
    L1PCE.setName("L1PCE");
    add(L1PCE);
    L1PCE.setBounds(85, 56, 56, 20);

    //---- OBJ_15 ----
    OBJ_15.setText("Journal");
    OBJ_15.setName("OBJ_15");
    add(OBJ_15);
    OBJ_15.setBounds(15, 30, 65, 20);

    //---- DTECR ----
    DTECR.setName("DTECR");
    add(DTECR);
    DTECR.setBounds(280, 30, 62, 20);

    //---- OBJ_21 ----
    OBJ_21.setText("Pi\u00e8ce");
    OBJ_21.setName("OBJ_21");
    add(OBJ_21);
    OBJ_21.setBounds(15, 55, 65, 20);

    //---- OBJ_23 ----
    OBJ_23.setText("Montant");
    OBJ_23.setName("OBJ_23");
    add(OBJ_23);
    OBJ_23.setBounds(15, 80, 65, 20);

    //---- OBJ_26 ----
    OBJ_26.setText("Libell\u00e9");
    OBJ_26.setName("OBJ_26");
    add(OBJ_26);
    OBJ_26.setBounds(15, 105, 65, 20);

    //---- OBJ_17 ----
    OBJ_17.setText("N\u00b0de folio");
    OBJ_17.setName("OBJ_17");
    add(OBJ_17);
    OBJ_17.setBounds(130, 30, 61, 20);

    //---- L1CFO ----
    L1CFO.setName("L1CFO");
    add(L1CFO);
    L1CFO.setBounds(195, 30, 50, 20);

    //---- L1CJO ----
    L1CJO.setName("L1CJO");
    add(L1CJO);
    L1CJO.setBounds(85, 30, 30, 20);

    //---- OBJ_19 ----
    OBJ_19.setText("du");
    OBJ_19.setName("OBJ_19");
    add(OBJ_19);
    OBJ_19.setBounds(255, 30, 18, 20);

    //---- L1SNS ----
    L1SNS.setName("L1SNS");
    add(L1SNS);
    L1SNS.setBounds(195, 80, 20, 20);

    //---- xTitledSeparator1 ----
    xTitledSeparator1.setTitle("R\u00e9f\u00e9rence d'\u00e9criture d\u00e9j\u00e0 existante");
    xTitledSeparator1.setName("xTitledSeparator1");
    add(xTitledSeparator1);
    xTitledSeparator1.setBounds(10, 10, 360, xTitledSeparator1.getPreferredSize().height);

    setPreferredSize(new Dimension(385, 200));

    //======== OBJ_4 ========
    {
      OBJ_4.setText("Commandes");
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Annuler");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("Exploitation");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_6);
    }

    //======== OBJ_7 ========
    {
      OBJ_7.setName("OBJ_7");

      //---- OBJ_8 ----
      OBJ_8.setText("Aide en ligne");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      OBJ_7.add(OBJ_8);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private XRiTextField L1LIB;
  private XRiTextField L1MTT;
  private JButton OBJ_28;
  private XRiTextField L1PCE;
  private JLabel OBJ_15;
  private XRiTextField DTECR;
  private JLabel OBJ_21;
  private JLabel OBJ_23;
  private JLabel OBJ_26;
  private JLabel OBJ_17;
  private XRiTextField L1CFO;
  private XRiTextField L1CJO;
  private JLabel OBJ_19;
  private XRiTextField L1SNS;
  private JXTitledSeparator xTitledSeparator1;
  private JMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JPopupMenu OBJ_7;
  private JMenuItem OBJ_8;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
