
package ri.serien.libecranrpg.scgm.SCGM01FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class SCGM01FM_DG extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _T01_Top = { "T01", "T02", "T03", "T04", "T05", "T06", "T07", "T08", };
  private String[] _T01_Title =
      { "Etb Raison Sociale", "Exercice en cours #CRLF#Début   Fin   Enc.", "", "Exercice Suivant #CRLF#Début   Fin   Enc.", };
  private String[][] _T01_Data = { { "LE01", "E101", "C01", "E201", }, { "LE02", "E102", "C02", "E202", },
      { "LE03", "E103", "C03", "E203", }, { "LE04", "E104", "C04", "E204", }, { "LE05", "E105", "C05", "E205", },
      { "LE06", "E106", "C06", "E206", }, { "LE07", "E107", "C07", "E207", }, { "LE08", "E108", "C08", "E208", }, };
  private int[] _T01_Width = { 210, 147, 30, 145, };
  
  public SCGM01FM_DG(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    T01.setAspectTable(_T01_Top, _T01_Title, _T01_Data, _T01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _T01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITRE@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void T01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _T01_Top, "1", "ENTER", e);
    if (T01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER", true);
    }
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _T01_Top, "1", "Enter");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    T01 = new XRiTable();
    xTitledSeparator1 = new JXTitledSeparator();
    BT_PGDOWN = new JButton();
    BT_PGUP = new JButton();
    popupMenu1 = new JPopupMenu();
    menuItem1 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 500));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== SCROLLPANE_LIST ========
        {
          SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

          //---- T01 ----
          T01.setComponentPopupMenu(popupMenu1);
          T01.setName("T01");
          T01.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              T01MouseClicked(e);
            }
          });
          SCROLLPANE_LIST.setViewportView(T01);
        }

        //---- xTitledSeparator1 ----
        xTitledSeparator1.setTitle("S\u00e9lection");
        xTitledSeparator1.setName("xTitledSeparator1");

        //---- BT_PGDOWN ----
        BT_PGDOWN.setText("");
        BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGDOWN.setName("BT_PGDOWN");

        //---- BT_PGUP ----
        BT_PGUP.setText("");
        BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGUP.setName("BT_PGUP");

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, 680, GroupLayout.PREFERRED_SIZE))
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(30, 30, 30)
              .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 615, GroupLayout.PREFERRED_SIZE)
              .addGap(5, 5, 5)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(25, 25, 25)
              .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGap(16, 16, 16)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                  .addGap(20, 20, 20)
                  .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== popupMenu1 ========
    {
      popupMenu1.setName("popupMenu1");

      //---- menuItem1 ----
      menuItem1.setText("Choisir");
      menuItem1.setName("menuItem1");
      menuItem1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem1ActionPerformed(e);
        }
      });
      popupMenu1.add(menuItem1);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable T01;
  private JXTitledSeparator xTitledSeparator1;
  private JButton BT_PGDOWN;
  private JButton BT_PGUP;
  private JPopupMenu popupMenu1;
  private JMenuItem menuItem1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
