
package ri.serien.libecranrpg.scgm.SCGM98FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SpinnerListModel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiSpinner;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SCGM98FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SCGM98FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    OBJ_56.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA1@")).trim());
    OBJ_57.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA2@")).trim());
    OBJ_58.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA3@")).trim());
    OBJ_59.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA4@")).trim());
    OBJ_60.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA5@")).trim());
    OBJ_61.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBAA6@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MESCLO@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSOC@")).trim());
    z_wencx_2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGPERX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledSeparator2 = new JXTitledSeparator();
    OBJ_47 = new JLabel();
    PERIOD = new XRiTextField();
    xTitledSeparator3 = new JXTitledSeparator();
    OBJ_56 = new JLabel();
    OBJ_57 = new JLabel();
    OBJ_58 = new JLabel();
    OBJ_59 = new JLabel();
    OBJ_60 = new JLabel();
    OBJ_61 = new JLabel();
    AA1 = new XRiTextField();
    AA2 = new XRiTextField();
    AA3 = new XRiTextField();
    AA4 = new XRiTextField();
    AA5 = new XRiTextField();
    AA6 = new XRiTextField();
    xTitledSeparator4 = new JXTitledSeparator();
    OBJ_62 = new JLabel();
    WOPE = new XRiSpinner();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    z_wencx_2 = new RiZoneSortie();
    label2 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(700, 350));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- xTitledSeparator2 ----
          xTitledSeparator2.setTitle("");
          xTitledSeparator2.setName("xTitledSeparator2");

          //---- OBJ_47 ----
          OBJ_47.setText("Date d'analyse");
          OBJ_47.setName("OBJ_47");

          //---- PERIOD ----
          PERIOD.setComponentPopupMenu(BTD);
          PERIOD.setName("PERIOD");

          //---- xTitledSeparator3 ----
          xTitledSeparator3.setTitle("Axes d'analyses \u00e0 s\u00e9lectionner");
          xTitledSeparator3.setName("xTitledSeparator3");

          //---- OBJ_56 ----
          OBJ_56.setText("@LIBAA1@");
          OBJ_56.setForeground(Color.gray);
          OBJ_56.setName("OBJ_56");

          //---- OBJ_57 ----
          OBJ_57.setText("@LIBAA2@");
          OBJ_57.setForeground(Color.gray);
          OBJ_57.setName("OBJ_57");

          //---- OBJ_58 ----
          OBJ_58.setText("@LIBAA3@");
          OBJ_58.setForeground(Color.gray);
          OBJ_58.setName("OBJ_58");

          //---- OBJ_59 ----
          OBJ_59.setText("@LIBAA4@");
          OBJ_59.setForeground(Color.gray);
          OBJ_59.setName("OBJ_59");

          //---- OBJ_60 ----
          OBJ_60.setText("@LIBAA5@");
          OBJ_60.setForeground(Color.gray);
          OBJ_60.setName("OBJ_60");

          //---- OBJ_61 ----
          OBJ_61.setText("@LIBAA6@");
          OBJ_61.setForeground(Color.gray);
          OBJ_61.setName("OBJ_61");

          //---- AA1 ----
          AA1.setComponentPopupMenu(BTD);
          AA1.setName("AA1");

          //---- AA2 ----
          AA2.setComponentPopupMenu(BTD);
          AA2.setName("AA2");

          //---- AA3 ----
          AA3.setComponentPopupMenu(BTD);
          AA3.setName("AA3");

          //---- AA4 ----
          AA4.setComponentPopupMenu(BTD);
          AA4.setName("AA4");

          //---- AA5 ----
          AA5.setComponentPopupMenu(BTD);
          AA5.setName("AA5");

          //---- AA6 ----
          AA6.setComponentPopupMenu(BTD);
          AA6.setName("AA6");

          //---- xTitledSeparator4 ----
          xTitledSeparator4.setTitle("");
          xTitledSeparator4.setName("xTitledSeparator4");

          //---- OBJ_62 ----
          OBJ_62.setText("Relation entre axes");
          OBJ_62.setName("OBJ_62");

          //---- WOPE ----
          WOPE.setComponentPopupMenu(BTD);
          WOPE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WOPE.setModel(new SpinnerListModel(new String[] {" ", "OU", "ET"}) {
            { setValue("OU"); }
          });
          WOPE.setName("WOPE");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@MESCLO@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WSOC@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- z_wencx_2 ----
          z_wencx_2.setText("@DGPERX@");
          z_wencx_2.setFont(z_wencx_2.getFont().deriveFont(z_wencx_2.getFont().getStyle() | Font.BOLD));
          z_wencx_2.setName("z_wencx_2");

          //---- label2 ----
          label2.setText("En cours");
          label2.setName("label2");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(label2, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addComponent(z_wencx_2, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE))))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(xTitledSeparator2, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 96, GroupLayout.PREFERRED_SIZE)
                .addGap(59, 59, 59)
                .addComponent(PERIOD, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(xTitledSeparator3, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(74, 74, 74)
                        .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE))
                      .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE))
                    .addGap(73, 73, 73)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(74, 74, 74)
                        .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(148, 148, 148)
                        .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE))))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(148, 148, 148)
                    .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE))))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(AA1, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(AA2, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(AA3, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(AA4, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(AA5, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(AA6, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(xTitledSeparator4, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(155, 155, 155)
                    .addComponent(WOPE, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 171, GroupLayout.PREFERRED_SIZE)))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(label2))
                      .addComponent(z_wencx_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGap(16, 16, 16)
                .addComponent(xTitledSeparator2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(PERIOD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addComponent(xTitledSeparator3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(AA1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(AA2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(AA3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(AA4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(AA5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(AA6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addComponent(xTitledSeparator4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(WOPE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator xTitledSeparator2;
  private JLabel OBJ_47;
  private XRiTextField PERIOD;
  private JXTitledSeparator xTitledSeparator3;
  private JLabel OBJ_56;
  private JLabel OBJ_57;
  private JLabel OBJ_58;
  private JLabel OBJ_59;
  private JLabel OBJ_60;
  private JLabel OBJ_61;
  private XRiTextField AA1;
  private XRiTextField AA2;
  private XRiTextField AA3;
  private XRiTextField AA4;
  private XRiTextField AA5;
  private XRiTextField AA6;
  private JXTitledSeparator xTitledSeparator4;
  private JLabel OBJ_62;
  private XRiSpinner WOPE;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private RiZoneSortie z_wencx_2;
  private JLabel label2;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
