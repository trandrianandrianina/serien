
package ri.serien.libecranrpg.scgm.SCGM44FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SCGM44FM_B6 extends SNPanelEcranRPG implements ioFrame {
  
   
  // private String[] _LIST_Top=null;
  private String[] _CTS001_Title = { "Code Soc", "Mois", "", "Code Soc", "Mois", "", "Code Soc", "Mois", "", "Code Soc", "Mois", "",
      "Code Soc", "Mois", "", "Code Soc", "Mois", "R", "Code Soc", "Mois", };
  private String[][] _CTS001_Data = {
      { "CTS001", "CTM001", "null", "CTS002", "CTM002", "null", "CTS003", "CTM003", "null", "CTS004", "CTM004", "null", "CTS005", "CTM005",
          "null", "CTS006", "CTM006", "null", "CTS007", "CTM007", },
      { "CTS008", "CTM008", "null", "CTS009", "CTM009", "null", "CTS010", "CTM010", "null", "CTS011", "CTM011", "null", "CTS012", "CTM012",
          "null", "CTS013", "CTM013", "null", "CTS014", "CTM014", },
      { "CTS015", "CTM015", "null", "CTS016", "CTM016", "null", "CTS017", "CTM017", "null", "CTS018", "CTM018", "null", "CTS019", "CTM019",
          "null", "CTS020", "CTM020", "null", "CTS021", "CTM021", },
      { "CTS022", "CTM022", "null", "CTS023", "CTM023", "null", "CTS024", "CTM024", "null", "CTS025", "CTM025", "null", "CTS026", "CTM026",
          "null", "CTS027", "CTM027", "null", "CTS028", "CTM028", },
      { "CTS029", "CTM029", "null", "CTS030", "CTM030", "null", "CTS031", "CTM031", "null", "CTS032", "CTM032", "null", "CTS033", "CTM033",
          "null", "CTS034", "CTM034", "null", "CTS035", "CTM035", },
      { "CTS036", "CTM036", "null", "CTS037", "CTM037", "null", "CTS038", "CTM038", "null", "CTS039", "CTM039", "null", "CTS040", "CTM040",
          "null", "CTS041", "CTM041", "null", "CTS042", "CTM042", },
      { "CTS043", "CTM043", "null", "CTS044", "CTM044", "null", "CTS045", "CTM045", "null", "CTS046", "CTM046", "null", "CTS047", "CTM047",
          "null", "CTS048", "CTM048", "null", "CTS049", "CTM049", },
      { "CTS050", "CTM050", "null", "CTS051", "CTM051", "null", "CTS052", "CTM052", "null", "CTS053", "CTM053", "null", "CTS054", "CTM054",
          "null", "CTS055", "CTM055", "null", "CTS056", "CTM056", },
      { "CTS057", "CTM057", "null", "CTS058", "CTM058", "null", "CTS059", "CTM059", "null", "CTS060", "CTM060", "null", "CTS061", "CTM061",
          "null", "CTS062", "CTM062", "null", "CTS063", "CTM063", },
      { "CTS064", "CTM064", "null", "CTS065", "CTM065", "null", "CTS066", "CTM066", "null", "CTS067", "CTM067", "null", "CTS068", "CTM068",
          "null", "CTS069", "CTM069", "null", "CTS070", "CTM070", },
      { "CTS071", "CTM071", "null", "CTS072", "CTM072", "null", "CTS073", "CTM731", "null", "CTS074", "CTM074", "null", "CTS075", "CTM075",
          "null", "CTS076", "CTM076", "null", "CTS077", "CTM077", },
      { "CTS078", "CTM078", "null", "CTS079", "CTM079", "null", "CTS080", "CTM080", "null", "CTS081", "CTM081", "null", "CTS082", "CTM082",
          "null", "CTS083", "CTM083", "null", "CTS084", "CTM084", },
      { "CTS085", "CTM085", "null", "CTS086", "CTM086", "null", "CTS087", "CTM087", "null", "CTS088", "CTM088", "null", "CTS089", "CTM089",
          "null", "CTS090", "CTM903", "null", "CTS091", "CTM091", },
      { "CTS092", "CTM092", "null", "CTS093", "CTM093", "null", "CTS094", "CTM094", "null", "CTS095", "CTM095", "null", "CTS096", "CTM096",
          "null", "CTS097", "CTM097", "null", "CTS098", "CTM098", },
      { "CTS099", "CTM099", "null", "CTS100", "CTM100", "null", "CTS101", "CTM101", "null", "CTS102", "CTM102", "null", "CTS103", "CTM103",
          "null", "CTS104", "CTM104", "null", "CTS105", "CTM105", },
      { "CTS106", "CTM106", null, "CTS107", "CTM107", null, "CTS108", "CTM108", null, "CTS109", "CTM109", null, "CTS110", "CTM10", null,
          "CTS111", "CTM111", null, "CTS112", "CTM112", }, };
  private int[] _CTS001_Width = { 33, 42, 15, 33, 42, 15, 33, 42, 15, 33, 42, 15, 33, 42, 15, 33, 42, 15, 33, 42, };
  
  public SCGM44FM_B6(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    CTS001.setAspectTable(null, _CTS001_Title, _CTS001_Data, _CTS001_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // Ajoute à la liste des oData les variables non liées directement à un composant graphique
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _LIST_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@  @TITPG2@"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, "OUI");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, "NON");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledSeparator1 = new JXTitledSeparator();
    SCROLLPANE_LIST = new JScrollPane();
    CTS001 = new XRiTable();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(820, 400));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- xTitledSeparator1 ----
          xTitledSeparator1.setTitle("Soci\u00e9t\u00e9 (s) S\u00e9lectionn\u00e9e (s)");
          xTitledSeparator1.setName("xTitledSeparator1");

          //======== SCROLLPANE_LIST ========
          {
            SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

            //---- CTS001 ----
            CTS001.setName("CTS001");
            SCROLLPANE_LIST.setViewportView(CTS001);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, 745, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 732, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21)
                .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 284, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator xTitledSeparator1;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable CTS001;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
