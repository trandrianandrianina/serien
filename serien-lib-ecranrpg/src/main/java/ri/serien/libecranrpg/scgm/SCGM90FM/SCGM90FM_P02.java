
package ri.serien.libecranrpg.scgm.SCGM90FM;
// Nom Fichier: p_SCGM90P1_P02_245.java

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SCGM90FM_P02 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SCGM90FM_P02(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    // setDefaultButton(BT_ENTER);
    
    // Menu Command
    setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_23.setText(interpreteurD.analyseExpression("@PARM6@").trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    FCH.setEnabled(lexique.isPresent("FCH"));
    OBJ_23.setVisible(lexique.isPresent("PARM6"));
    
    // TODO Icones
    OBJ_38.setIcon(lexique.chargerImage("images/msgbox04.gif", true));
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@ENVP/+1/@ - @TITPG1@ @TITPG2@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "Attention");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_31 = new JPanel();
    OBJ_38 = new JLabel();
    OBJ_33 = new JLabel();
    OBJ_37 = new JPanel();
    OBJ_22 = new JLabel();
    OBJ_23 = new JLabel();
    OBJ_21 = new JLabel();
    FCH = new XRiTextField();
    OBJ_24 = new JLabel();
    OBJ_25 = new JLabel();
    OBJ_4 = new JMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();

    //======== this ========
    setName("this");
    setLayout(null);

    //======== OBJ_31 ========
    {
      OBJ_31.setName("OBJ_31");
      OBJ_31.setLayout(null);

      //---- OBJ_38 ----
      OBJ_38.setIcon(new ImageIcon("images/msgbox04.gif"));
      OBJ_38.setName("OBJ_38");
      OBJ_31.add(OBJ_38);
      OBJ_38.setBounds(15, 5, 51, 42);

      //---- OBJ_33 ----
      OBJ_33.setText("Veuillez Valider");
      OBJ_33.setName("OBJ_33");
      OBJ_31.add(OBJ_33);
      OBJ_33.setBounds(250, 15, 154, 24);
    }
    add(OBJ_31);
    OBJ_31.setBounds(155, 545, 607, 48);

    //======== OBJ_37 ========
    {
      OBJ_37.setName("OBJ_37");
      OBJ_37.setLayout(null);
    }
    add(OBJ_37);
    OBJ_37.setBounds(151, 464, 607, 48);

    //---- OBJ_22 ----
    OBJ_22.setText("Ce fichier n'existe pas sur le disque");
    OBJ_22.setName("OBJ_22");
    add(OBJ_22);
    OBJ_22.setBounds(265, 235, 301, 26);

    //---- OBJ_23 ----
    OBJ_23.setText("@PARM6@");
    OBJ_23.setForeground(Color.gray);
    OBJ_23.setName("OBJ_23");
    add(OBJ_23);
    OBJ_23.setBounds(585, 235, 207, 26);

    //---- OBJ_21 ----
    OBJ_21.setText("ERREUR");
    OBJ_21.setHorizontalAlignment(SwingConstants.CENTER);
    OBJ_21.setFont(OBJ_21.getFont().deriveFont(OBJ_21.getFont().getStyle() | Font.BOLD));
    OBJ_21.setForeground(Color.red);
    OBJ_21.setName("OBJ_21");
    add(OBJ_21);
    OBJ_21.setBounds(120, 185, 740, 24);

    //---- FCH ----
    FCH.setName("FCH");
    add(FCH);
    FCH.setBounds(585, 290, 103, 20);

    //---- OBJ_24 ----
    OBJ_24.setText("Donnez ici le nom du fichier \u00e0 \u00e9diter");
    OBJ_24.setForeground(Color.darkGray);
    OBJ_24.setFont(OBJ_24.getFont().deriveFont(OBJ_24.getFont().getStyle() | Font.BOLD));
    OBJ_24.setName("OBJ_24");
    add(OBJ_24);
    OBJ_24.setBounds(265, 290, 316, 24);

    //---- OBJ_25 ----
    OBJ_25.setText("(Appuyez sur OK si le nom propos\u00e9 est correct, ou effacez-le pour sortir du programme sans rien \u00e9diter.)");
    OBJ_25.setHorizontalAlignment(SwingConstants.CENTER);
    OBJ_25.setForeground(Color.gray);
    OBJ_25.setName("OBJ_25");
    add(OBJ_25);
    OBJ_25.setBounds(120, 360, 740, 24);

    setPreferredSize(new Dimension(1000, 600));

    //======== OBJ_4 ========
    {
      OBJ_4.setText("Commandes");
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("Exploitation");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_6);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel OBJ_31;
  private JLabel OBJ_38;
  private JLabel OBJ_33;
  private JPanel OBJ_37;
  private JLabel OBJ_22;
  private JLabel OBJ_23;
  private JLabel OBJ_21;
  private XRiTextField FCH;
  private JLabel OBJ_24;
  private JLabel OBJ_25;
  private JMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
