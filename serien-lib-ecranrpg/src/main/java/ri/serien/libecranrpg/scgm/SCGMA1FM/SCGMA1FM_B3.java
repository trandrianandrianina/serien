
package ri.serien.libecranrpg.scgm.SCGMA1FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SCGMA1FM_B3 extends SNPanelEcranRPG implements ioFrame {
  
  
  public SCGMA1FM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    PAT001.setValeursSelection("1", "");
    PAT002.setValeursSelection("1", "");
    PAT003.setValeursSelection("1", "");
    PAT004.setValeursSelection("1", "");
    PAT005.setValeursSelection("1", "");
    PAT006.setValeursSelection("1", "");
    PAT007.setValeursSelection("1", "");
    PAT008.setValeursSelection("1", "");
    PAT009.setValeursSelection("1", "");
    PAT010.setValeursSelection("1", "");
    PAT011.setValeursSelection("1", "");
    PAT012.setValeursSelection("1", "");
    PAT013.setValeursSelection("1", "");
    PAT014.setValeursSelection("1", "");
    PAT015.setValeursSelection("1", "");
    PAT016.setValeursSelection("1", "");
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    PAS001.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS001@")).trim());
    PAN001.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN001@")).trim());
    PA1001.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA1001@")).trim());
    PA2001.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA2001@")).trim());
    PAS2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS002@")).trim());
    PAN2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN002@")).trim());
    PA1002.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA1002@")).trim());
    PA2002.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA2002@")).trim());
    PAS3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS003@")).trim());
    PAN3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN003@")).trim());
    PA1003.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA1003@")).trim());
    PA2003.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA2003@")).trim());
    PAS4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS004@")).trim());
    PAN4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN004@")).trim());
    PA1004.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA1004@")).trim());
    PA2004.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA2004@")).trim());
    PAS5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS005@")).trim());
    PAN5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN005@")).trim());
    PA1005.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA1005@")).trim());
    PA2005.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA2005@")).trim());
    PAS6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS006@")).trim());
    PAN6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN006@")).trim());
    PA1006.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA1006@")).trim());
    PA2006.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA2006@")).trim());
    PAS7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS007@")).trim());
    PAN7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN007@")).trim());
    PA1007.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA1007@")).trim());
    PA2007.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA2007@")).trim());
    PAS8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS008@")).trim());
    PAN8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN008@")).trim());
    PA1008.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA1008@")).trim());
    PA2008.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA2008@")).trim());
    PAS9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS009@")).trim());
    PAN9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN009@")).trim());
    PA1009.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA1009@")).trim());
    PA2009.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA2009@")).trim());
    PAS10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS010@")).trim());
    PAN10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN010@")).trim());
    PA1010.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA1010@")).trim());
    PA2010.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA2010@")).trim());
    PAS11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS011@")).trim());
    PAN11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN011@")).trim());
    PA1011.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA1011@")).trim());
    PA2011.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA2011@")).trim());
    PAS12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS012@")).trim());
    PAN12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN012@")).trim());
    PA1012.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA1012@")).trim());
    PA2012.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA2012@")).trim());
    PAS13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS013@")).trim());
    PAN13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN013@")).trim());
    PA1013.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA1013@")).trim());
    PA2013.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA2013@")).trim());
    PAS14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS014@")).trim());
    PAN14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN014@")).trim());
    PA1014.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA1014@")).trim());
    PA2014.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA2014@")).trim());
    PAS15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS015@")).trim());
    PAN15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN015@")).trim());
    PA1015.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA1015@")).trim());
    PA2015.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA2015@")).trim());
    PAS16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAS016@")).trim());
    PAN16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PAN016@")).trim());
    PA1016.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA1016@")).trim());
    PA2016.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA2016@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    OBJ_48.setEnabled(lexique.isPresent("WTOU"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_48ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTOU", 0, "**");
    lexique.HostScreenSendKey(this, "F6", false);
  }
  
  private void PAT001ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    PAS001 = new RiZoneSortie();
    PAT001 = new XRiCheckBox();
    PAN001 = new RiZoneSortie();
    PAM001 = new XRiCalendrier();
    PA1001 = new RiZoneSortie();
    PA2001 = new RiZoneSortie();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    PAS2 = new RiZoneSortie();
    PAT002 = new XRiCheckBox();
    PAN2 = new RiZoneSortie();
    PAM002 = new XRiCalendrier();
    PA1002 = new RiZoneSortie();
    PA2002 = new RiZoneSortie();
    PAS3 = new RiZoneSortie();
    PAT003 = new XRiCheckBox();
    PAN3 = new RiZoneSortie();
    PAM003 = new XRiCalendrier();
    PA1003 = new RiZoneSortie();
    PA2003 = new RiZoneSortie();
    PAS4 = new RiZoneSortie();
    PAT004 = new XRiCheckBox();
    PAN4 = new RiZoneSortie();
    PAM004 = new XRiCalendrier();
    PA1004 = new RiZoneSortie();
    PA2004 = new RiZoneSortie();
    PAS5 = new RiZoneSortie();
    PAT005 = new XRiCheckBox();
    PAN5 = new RiZoneSortie();
    PAM005 = new XRiCalendrier();
    PA1005 = new RiZoneSortie();
    PA2005 = new RiZoneSortie();
    PAS6 = new RiZoneSortie();
    PAT006 = new XRiCheckBox();
    PAN6 = new RiZoneSortie();
    PAM006 = new XRiCalendrier();
    PA1006 = new RiZoneSortie();
    PA2006 = new RiZoneSortie();
    PAS7 = new RiZoneSortie();
    PAT007 = new XRiCheckBox();
    PAN7 = new RiZoneSortie();
    PAM007 = new XRiCalendrier();
    PA1007 = new RiZoneSortie();
    PA2007 = new RiZoneSortie();
    PAS8 = new RiZoneSortie();
    PAT008 = new XRiCheckBox();
    PAN8 = new RiZoneSortie();
    PAM008 = new XRiCalendrier();
    PA1008 = new RiZoneSortie();
    PA2008 = new RiZoneSortie();
    PAS9 = new RiZoneSortie();
    PAT009 = new XRiCheckBox();
    PAN9 = new RiZoneSortie();
    PAM009 = new XRiCalendrier();
    PA1009 = new RiZoneSortie();
    PA2009 = new RiZoneSortie();
    PAS10 = new RiZoneSortie();
    PAT010 = new XRiCheckBox();
    PAN10 = new RiZoneSortie();
    PAM010 = new XRiCalendrier();
    PA1010 = new RiZoneSortie();
    PA2010 = new RiZoneSortie();
    PAS11 = new RiZoneSortie();
    PAT011 = new XRiCheckBox();
    PAN11 = new RiZoneSortie();
    PAM011 = new XRiCalendrier();
    PA1011 = new RiZoneSortie();
    PA2011 = new RiZoneSortie();
    PAS12 = new RiZoneSortie();
    PAT012 = new XRiCheckBox();
    PAN12 = new RiZoneSortie();
    PAM012 = new XRiCalendrier();
    PA1012 = new RiZoneSortie();
    PA2012 = new RiZoneSortie();
    PAS13 = new RiZoneSortie();
    PAT013 = new XRiCheckBox();
    PAN13 = new RiZoneSortie();
    PAM013 = new XRiCalendrier();
    PA1013 = new RiZoneSortie();
    PA2013 = new RiZoneSortie();
    PAS14 = new RiZoneSortie();
    PAT014 = new XRiCheckBox();
    PAN14 = new RiZoneSortie();
    PAM014 = new XRiCalendrier();
    PA1014 = new RiZoneSortie();
    PA2014 = new RiZoneSortie();
    PAS15 = new RiZoneSortie();
    PAT015 = new XRiCheckBox();
    PAN15 = new RiZoneSortie();
    PAM015 = new XRiCalendrier();
    PA1015 = new RiZoneSortie();
    PA2015 = new RiZoneSortie();
    PAS16 = new RiZoneSortie();
    PAT016 = new XRiCheckBox();
    PAN16 = new RiZoneSortie();
    PAM016 = new XRiCalendrier();
    PA1016 = new RiZoneSortie();
    PA2016 = new RiZoneSortie();
    OBJ_48 = new SNBouton();
    label6 = new JLabel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Choix du papier");
              riSousMenu_bt_export.setToolTipText("Choix du papier");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 570));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(900, 519));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Choix de la soci\u00e9t\u00e9"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- PAS001 ----
            PAS001.setText("@PAS001@");
            PAS001.setName("PAS001");
            panel1.add(PAS001);
            PAS001.setBounds(55, 65, 44, PAS001.getPreferredSize().height);

            //---- PAT001 ----
            PAT001.setName("PAT001");
            PAT001.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                PAT001ActionPerformed(e);
              }
            });
            panel1.add(PAT001);
            PAT001.setBounds(30, 65, PAT001.getPreferredSize().width, 24);

            //---- PAN001 ----
            PAN001.setText("@PAN001@");
            PAN001.setName("PAN001");
            panel1.add(PAN001);
            PAN001.setBounds(105, 65, 264, PAN001.getPreferredSize().height);

            //---- PAM001 ----
            PAM001.setTypeSaisie(6);
            PAM001.setName("PAM001");
            panel1.add(PAM001);
            PAM001.setBounds(375, 63, 90, PAM001.getPreferredSize().height);

            //---- PA1001 ----
            PA1001.setText("@PA1001@");
            PA1001.setName("PA1001");
            panel1.add(PA1001);
            PA1001.setBounds(455, 65, 184, PA1001.getPreferredSize().height);

            //---- PA2001 ----
            PA2001.setText("@PA2001@");
            PA2001.setName("PA2001");
            panel1.add(PA2001);
            PA2001.setBounds(645, 65, 184, PA2001.getPreferredSize().height);

            //---- label1 ----
            label1.setText("Code");
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(55, 30, 45, 25);

            //---- label2 ----
            label2.setText("Mois");
            label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
            label2.setName("label2");
            panel1.add(label2);
            label2.setBounds(375, 30, 75, 25);

            //---- label3 ----
            label3.setText("Libell\u00e9 soci\u00e9t\u00e9");
            label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
            label3.setName("label3");
            panel1.add(label3);
            label3.setBounds(105, 30, 265, 25);

            //---- label4 ----
            label4.setText("Exercice en cours");
            label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
            label4.setName("label4");
            panel1.add(label4);
            label4.setBounds(455, 30, 184, 25);

            //---- label5 ----
            label5.setText("Arr\u00eat\u00e9 abonnement");
            label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
            label5.setName("label5");
            panel1.add(label5);
            label5.setBounds(645, 30, 184, 25);

            //---- PAS2 ----
            PAS2.setText("@PAS002@");
            PAS2.setName("PAS2");
            panel1.add(PAS2);
            PAS2.setBounds(55, 90, 44, PAS2.getPreferredSize().height);

            //---- PAT002 ----
            PAT002.setName("PAT002");
            PAT002.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                PAT001ActionPerformed(e);
              }
            });
            panel1.add(PAT002);
            PAT002.setBounds(30, 90, PAT002.getPreferredSize().width, 24);

            //---- PAN2 ----
            PAN2.setText("@PAN002@");
            PAN2.setName("PAN2");
            panel1.add(PAN2);
            PAN2.setBounds(105, 90, 264, PAN2.getPreferredSize().height);

            //---- PAM002 ----
            PAM002.setTypeSaisie(6);
            PAM002.setName("PAM002");
            panel1.add(PAM002);
            PAM002.setBounds(375, 88, 90, PAM002.getPreferredSize().height);

            //---- PA1002 ----
            PA1002.setText("@PA1002@");
            PA1002.setName("PA1002");
            panel1.add(PA1002);
            PA1002.setBounds(455, 90, 184, PA1002.getPreferredSize().height);

            //---- PA2002 ----
            PA2002.setText("@PA2002@");
            PA2002.setName("PA2002");
            panel1.add(PA2002);
            PA2002.setBounds(645, 90, 184, PA2002.getPreferredSize().height);

            //---- PAS3 ----
            PAS3.setText("@PAS003@");
            PAS3.setName("PAS3");
            panel1.add(PAS3);
            PAS3.setBounds(55, 115, 44, PAS3.getPreferredSize().height);

            //---- PAT003 ----
            PAT003.setName("PAT003");
            PAT003.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                PAT001ActionPerformed(e);
              }
            });
            panel1.add(PAT003);
            PAT003.setBounds(30, 115, PAT003.getPreferredSize().width, 24);

            //---- PAN3 ----
            PAN3.setText("@PAN003@");
            PAN3.setName("PAN3");
            panel1.add(PAN3);
            PAN3.setBounds(105, 115, 264, PAN3.getPreferredSize().height);

            //---- PAM003 ----
            PAM003.setTypeSaisie(6);
            PAM003.setName("PAM003");
            panel1.add(PAM003);
            PAM003.setBounds(375, 113, 90, PAM003.getPreferredSize().height);

            //---- PA1003 ----
            PA1003.setText("@PA1003@");
            PA1003.setName("PA1003");
            panel1.add(PA1003);
            PA1003.setBounds(455, 115, 184, PA1003.getPreferredSize().height);

            //---- PA2003 ----
            PA2003.setText("@PA2003@");
            PA2003.setName("PA2003");
            panel1.add(PA2003);
            PA2003.setBounds(645, 115, 184, PA2003.getPreferredSize().height);

            //---- PAS4 ----
            PAS4.setText("@PAS004@");
            PAS4.setName("PAS4");
            panel1.add(PAS4);
            PAS4.setBounds(55, 140, 44, PAS4.getPreferredSize().height);

            //---- PAT004 ----
            PAT004.setName("PAT004");
            PAT004.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                PAT001ActionPerformed(e);
              }
            });
            panel1.add(PAT004);
            PAT004.setBounds(30, 140, PAT004.getPreferredSize().width, 24);

            //---- PAN4 ----
            PAN4.setText("@PAN004@");
            PAN4.setName("PAN4");
            panel1.add(PAN4);
            PAN4.setBounds(105, 140, 264, PAN4.getPreferredSize().height);

            //---- PAM004 ----
            PAM004.setTypeSaisie(6);
            PAM004.setName("PAM004");
            panel1.add(PAM004);
            PAM004.setBounds(375, 138, 90, PAM004.getPreferredSize().height);

            //---- PA1004 ----
            PA1004.setText("@PA1004@");
            PA1004.setName("PA1004");
            panel1.add(PA1004);
            PA1004.setBounds(455, 140, 184, PA1004.getPreferredSize().height);

            //---- PA2004 ----
            PA2004.setText("@PA2004@");
            PA2004.setName("PA2004");
            panel1.add(PA2004);
            PA2004.setBounds(645, 140, 184, PA2004.getPreferredSize().height);

            //---- PAS5 ----
            PAS5.setText("@PAS005@");
            PAS5.setName("PAS5");
            panel1.add(PAS5);
            PAS5.setBounds(55, 165, 44, PAS5.getPreferredSize().height);

            //---- PAT005 ----
            PAT005.setName("PAT005");
            PAT005.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                PAT001ActionPerformed(e);
              }
            });
            panel1.add(PAT005);
            PAT005.setBounds(30, 165, PAT005.getPreferredSize().width, 24);

            //---- PAN5 ----
            PAN5.setText("@PAN005@");
            PAN5.setName("PAN5");
            panel1.add(PAN5);
            PAN5.setBounds(105, 165, 264, PAN5.getPreferredSize().height);

            //---- PAM005 ----
            PAM005.setTypeSaisie(6);
            PAM005.setName("PAM005");
            panel1.add(PAM005);
            PAM005.setBounds(375, 163, 90, PAM005.getPreferredSize().height);

            //---- PA1005 ----
            PA1005.setText("@PA1005@");
            PA1005.setName("PA1005");
            panel1.add(PA1005);
            PA1005.setBounds(455, 165, 184, PA1005.getPreferredSize().height);

            //---- PA2005 ----
            PA2005.setText("@PA2005@");
            PA2005.setName("PA2005");
            panel1.add(PA2005);
            PA2005.setBounds(645, 165, 184, PA2005.getPreferredSize().height);

            //---- PAS6 ----
            PAS6.setText("@PAS006@");
            PAS6.setName("PAS6");
            panel1.add(PAS6);
            PAS6.setBounds(55, 190, 44, PAS6.getPreferredSize().height);

            //---- PAT006 ----
            PAT006.setName("PAT006");
            PAT006.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                PAT001ActionPerformed(e);
              }
            });
            panel1.add(PAT006);
            PAT006.setBounds(30, 190, PAT006.getPreferredSize().width, 24);

            //---- PAN6 ----
            PAN6.setText("@PAN006@");
            PAN6.setName("PAN6");
            panel1.add(PAN6);
            PAN6.setBounds(105, 190, 264, PAN6.getPreferredSize().height);

            //---- PAM006 ----
            PAM006.setTypeSaisie(6);
            PAM006.setName("PAM006");
            panel1.add(PAM006);
            PAM006.setBounds(375, 188, 90, PAM006.getPreferredSize().height);

            //---- PA1006 ----
            PA1006.setText("@PA1006@");
            PA1006.setName("PA1006");
            panel1.add(PA1006);
            PA1006.setBounds(455, 190, 184, PA1006.getPreferredSize().height);

            //---- PA2006 ----
            PA2006.setText("@PA2006@");
            PA2006.setName("PA2006");
            panel1.add(PA2006);
            PA2006.setBounds(645, 190, 184, PA2006.getPreferredSize().height);

            //---- PAS7 ----
            PAS7.setText("@PAS007@");
            PAS7.setName("PAS7");
            panel1.add(PAS7);
            PAS7.setBounds(55, 215, 44, PAS7.getPreferredSize().height);

            //---- PAT007 ----
            PAT007.setName("PAT007");
            PAT007.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                PAT001ActionPerformed(e);
              }
            });
            panel1.add(PAT007);
            PAT007.setBounds(30, 215, PAT007.getPreferredSize().width, 24);

            //---- PAN7 ----
            PAN7.setText("@PAN007@");
            PAN7.setName("PAN7");
            panel1.add(PAN7);
            PAN7.setBounds(105, 215, 264, PAN7.getPreferredSize().height);

            //---- PAM007 ----
            PAM007.setTypeSaisie(6);
            PAM007.setName("PAM007");
            panel1.add(PAM007);
            PAM007.setBounds(375, 213, 90, PAM007.getPreferredSize().height);

            //---- PA1007 ----
            PA1007.setText("@PA1007@");
            PA1007.setName("PA1007");
            panel1.add(PA1007);
            PA1007.setBounds(455, 215, 184, PA1007.getPreferredSize().height);

            //---- PA2007 ----
            PA2007.setText("@PA2007@");
            PA2007.setName("PA2007");
            panel1.add(PA2007);
            PA2007.setBounds(645, 215, 184, PA2007.getPreferredSize().height);

            //---- PAS8 ----
            PAS8.setText("@PAS008@");
            PAS8.setName("PAS8");
            panel1.add(PAS8);
            PAS8.setBounds(55, 240, 44, PAS8.getPreferredSize().height);

            //---- PAT008 ----
            PAT008.setName("PAT008");
            PAT008.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                PAT001ActionPerformed(e);
              }
            });
            panel1.add(PAT008);
            PAT008.setBounds(30, 240, PAT008.getPreferredSize().width, 24);

            //---- PAN8 ----
            PAN8.setText("@PAN008@");
            PAN8.setName("PAN8");
            panel1.add(PAN8);
            PAN8.setBounds(105, 240, 264, PAN8.getPreferredSize().height);

            //---- PAM008 ----
            PAM008.setTypeSaisie(6);
            PAM008.setName("PAM008");
            panel1.add(PAM008);
            PAM008.setBounds(375, 238, 90, PAM008.getPreferredSize().height);

            //---- PA1008 ----
            PA1008.setText("@PA1008@");
            PA1008.setName("PA1008");
            panel1.add(PA1008);
            PA1008.setBounds(455, 240, 184, PA1008.getPreferredSize().height);

            //---- PA2008 ----
            PA2008.setText("@PA2008@");
            PA2008.setName("PA2008");
            panel1.add(PA2008);
            PA2008.setBounds(645, 240, 184, PA2008.getPreferredSize().height);

            //---- PAS9 ----
            PAS9.setText("@PAS009@");
            PAS9.setName("PAS9");
            panel1.add(PAS9);
            PAS9.setBounds(55, 265, 44, PAS9.getPreferredSize().height);

            //---- PAT009 ----
            PAT009.setName("PAT009");
            PAT009.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                PAT001ActionPerformed(e);
              }
            });
            panel1.add(PAT009);
            PAT009.setBounds(30, 265, PAT009.getPreferredSize().width, 24);

            //---- PAN9 ----
            PAN9.setText("@PAN009@");
            PAN9.setName("PAN9");
            panel1.add(PAN9);
            PAN9.setBounds(105, 265, 264, PAN9.getPreferredSize().height);

            //---- PAM009 ----
            PAM009.setTypeSaisie(6);
            PAM009.setName("PAM009");
            panel1.add(PAM009);
            PAM009.setBounds(375, 263, 90, PAM009.getPreferredSize().height);

            //---- PA1009 ----
            PA1009.setText("@PA1009@");
            PA1009.setName("PA1009");
            panel1.add(PA1009);
            PA1009.setBounds(455, 265, 184, PA1009.getPreferredSize().height);

            //---- PA2009 ----
            PA2009.setText("@PA2009@");
            PA2009.setName("PA2009");
            panel1.add(PA2009);
            PA2009.setBounds(645, 265, 184, PA2009.getPreferredSize().height);

            //---- PAS10 ----
            PAS10.setText("@PAS010@");
            PAS10.setName("PAS10");
            panel1.add(PAS10);
            PAS10.setBounds(55, 290, 44, PAS10.getPreferredSize().height);

            //---- PAT010 ----
            PAT010.setName("PAT010");
            PAT010.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                PAT001ActionPerformed(e);
              }
            });
            panel1.add(PAT010);
            PAT010.setBounds(30, 290, PAT010.getPreferredSize().width, 24);

            //---- PAN10 ----
            PAN10.setText("@PAN010@");
            PAN10.setName("PAN10");
            panel1.add(PAN10);
            PAN10.setBounds(105, 290, 264, PAN10.getPreferredSize().height);

            //---- PAM010 ----
            PAM010.setTypeSaisie(6);
            PAM010.setName("PAM010");
            panel1.add(PAM010);
            PAM010.setBounds(375, 288, 90, PAM010.getPreferredSize().height);

            //---- PA1010 ----
            PA1010.setText("@PA1010@");
            PA1010.setName("PA1010");
            panel1.add(PA1010);
            PA1010.setBounds(455, 290, 184, PA1010.getPreferredSize().height);

            //---- PA2010 ----
            PA2010.setText("@PA2010@");
            PA2010.setName("PA2010");
            panel1.add(PA2010);
            PA2010.setBounds(645, 290, 184, PA2010.getPreferredSize().height);

            //---- PAS11 ----
            PAS11.setText("@PAS011@");
            PAS11.setName("PAS11");
            panel1.add(PAS11);
            PAS11.setBounds(55, 315, 44, PAS11.getPreferredSize().height);

            //---- PAT011 ----
            PAT011.setName("PAT011");
            PAT011.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                PAT001ActionPerformed(e);
              }
            });
            panel1.add(PAT011);
            PAT011.setBounds(30, 315, PAT011.getPreferredSize().width, 24);

            //---- PAN11 ----
            PAN11.setText("@PAN011@");
            PAN11.setName("PAN11");
            panel1.add(PAN11);
            PAN11.setBounds(105, 315, 264, PAN11.getPreferredSize().height);

            //---- PAM011 ----
            PAM011.setTypeSaisie(6);
            PAM011.setName("PAM011");
            panel1.add(PAM011);
            PAM011.setBounds(375, 313, 90, PAM011.getPreferredSize().height);

            //---- PA1011 ----
            PA1011.setText("@PA1011@");
            PA1011.setName("PA1011");
            panel1.add(PA1011);
            PA1011.setBounds(455, 315, 184, PA1011.getPreferredSize().height);

            //---- PA2011 ----
            PA2011.setText("@PA2011@");
            PA2011.setName("PA2011");
            panel1.add(PA2011);
            PA2011.setBounds(645, 315, 184, PA2011.getPreferredSize().height);

            //---- PAS12 ----
            PAS12.setText("@PAS012@");
            PAS12.setName("PAS12");
            panel1.add(PAS12);
            PAS12.setBounds(55, 340, 44, PAS12.getPreferredSize().height);

            //---- PAT012 ----
            PAT012.setName("PAT012");
            PAT012.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                PAT001ActionPerformed(e);
              }
            });
            panel1.add(PAT012);
            PAT012.setBounds(30, 340, PAT012.getPreferredSize().width, 24);

            //---- PAN12 ----
            PAN12.setText("@PAN012@");
            PAN12.setName("PAN12");
            panel1.add(PAN12);
            PAN12.setBounds(105, 340, 264, PAN12.getPreferredSize().height);

            //---- PAM012 ----
            PAM012.setTypeSaisie(6);
            PAM012.setName("PAM012");
            panel1.add(PAM012);
            PAM012.setBounds(375, 338, 90, PAM012.getPreferredSize().height);

            //---- PA1012 ----
            PA1012.setText("@PA1012@");
            PA1012.setName("PA1012");
            panel1.add(PA1012);
            PA1012.setBounds(455, 340, 184, PA1012.getPreferredSize().height);

            //---- PA2012 ----
            PA2012.setText("@PA2012@");
            PA2012.setName("PA2012");
            panel1.add(PA2012);
            PA2012.setBounds(645, 340, 184, PA2012.getPreferredSize().height);

            //---- PAS13 ----
            PAS13.setText("@PAS013@");
            PAS13.setName("PAS13");
            panel1.add(PAS13);
            PAS13.setBounds(55, 365, 44, PAS13.getPreferredSize().height);

            //---- PAT013 ----
            PAT013.setName("PAT013");
            PAT013.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                PAT001ActionPerformed(e);
              }
            });
            panel1.add(PAT013);
            PAT013.setBounds(30, 365, PAT013.getPreferredSize().width, 24);

            //---- PAN13 ----
            PAN13.setText("@PAN013@");
            PAN13.setName("PAN13");
            panel1.add(PAN13);
            PAN13.setBounds(105, 365, 264, PAN13.getPreferredSize().height);

            //---- PAM013 ----
            PAM013.setTypeSaisie(6);
            PAM013.setName("PAM013");
            panel1.add(PAM013);
            PAM013.setBounds(375, 363, 90, PAM013.getPreferredSize().height);

            //---- PA1013 ----
            PA1013.setText("@PA1013@");
            PA1013.setName("PA1013");
            panel1.add(PA1013);
            PA1013.setBounds(455, 365, 184, PA1013.getPreferredSize().height);

            //---- PA2013 ----
            PA2013.setText("@PA2013@");
            PA2013.setName("PA2013");
            panel1.add(PA2013);
            PA2013.setBounds(645, 365, 184, PA2013.getPreferredSize().height);

            //---- PAS14 ----
            PAS14.setText("@PAS014@");
            PAS14.setName("PAS14");
            panel1.add(PAS14);
            PAS14.setBounds(55, 390, 44, PAS14.getPreferredSize().height);

            //---- PAT014 ----
            PAT014.setName("PAT014");
            PAT014.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                PAT001ActionPerformed(e);
              }
            });
            panel1.add(PAT014);
            PAT014.setBounds(30, 390, PAT014.getPreferredSize().width, 24);

            //---- PAN14 ----
            PAN14.setText("@PAN014@");
            PAN14.setName("PAN14");
            panel1.add(PAN14);
            PAN14.setBounds(105, 390, 264, PAN14.getPreferredSize().height);

            //---- PAM014 ----
            PAM014.setTypeSaisie(6);
            PAM014.setName("PAM014");
            panel1.add(PAM014);
            PAM014.setBounds(375, 388, 90, PAM014.getPreferredSize().height);

            //---- PA1014 ----
            PA1014.setText("@PA1014@");
            PA1014.setName("PA1014");
            panel1.add(PA1014);
            PA1014.setBounds(455, 390, 184, PA1014.getPreferredSize().height);

            //---- PA2014 ----
            PA2014.setText("@PA2014@");
            PA2014.setName("PA2014");
            panel1.add(PA2014);
            PA2014.setBounds(645, 390, 184, PA2014.getPreferredSize().height);

            //---- PAS15 ----
            PAS15.setText("@PAS015@");
            PAS15.setName("PAS15");
            panel1.add(PAS15);
            PAS15.setBounds(55, 415, 44, PAS15.getPreferredSize().height);

            //---- PAT015 ----
            PAT015.setName("PAT015");
            PAT015.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                PAT001ActionPerformed(e);
              }
            });
            panel1.add(PAT015);
            PAT015.setBounds(30, 415, PAT015.getPreferredSize().width, 24);

            //---- PAN15 ----
            PAN15.setText("@PAN015@");
            PAN15.setName("PAN15");
            panel1.add(PAN15);
            PAN15.setBounds(105, 415, 264, PAN15.getPreferredSize().height);

            //---- PAM015 ----
            PAM015.setTypeSaisie(6);
            PAM015.setName("PAM015");
            panel1.add(PAM015);
            PAM015.setBounds(375, 413, 90, PAM015.getPreferredSize().height);

            //---- PA1015 ----
            PA1015.setText("@PA1015@");
            PA1015.setName("PA1015");
            panel1.add(PA1015);
            PA1015.setBounds(455, 415, 184, PA1015.getPreferredSize().height);

            //---- PA2015 ----
            PA2015.setText("@PA2015@");
            PA2015.setName("PA2015");
            panel1.add(PA2015);
            PA2015.setBounds(645, 415, 184, PA2015.getPreferredSize().height);

            //---- PAS16 ----
            PAS16.setText("@PAS016@");
            PAS16.setName("PAS16");
            panel1.add(PAS16);
            PAS16.setBounds(55, 440, 44, PAS16.getPreferredSize().height);

            //---- PAT016 ----
            PAT016.setName("PAT016");
            PAT016.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                PAT001ActionPerformed(e);
              }
            });
            panel1.add(PAT016);
            PAT016.setBounds(30, 440, PAT016.getPreferredSize().width, 24);

            //---- PAN16 ----
            PAN16.setText("@PAN016@");
            PAN16.setName("PAN16");
            panel1.add(PAN16);
            PAN16.setBounds(105, 440, 264, PAN16.getPreferredSize().height);

            //---- PAM016 ----
            PAM016.setTypeSaisie(6);
            PAM016.setName("PAM016");
            panel1.add(PAM016);
            PAM016.setBounds(375, 438, 90, PAM016.getPreferredSize().height);

            //---- PA1016 ----
            PA1016.setText("@PA1016@");
            PA1016.setName("PA1016");
            panel1.add(PA1016);
            PA1016.setBounds(455, 440, 184, PA1016.getPreferredSize().height);

            //---- PA2016 ----
            PA2016.setText("@PA2016@");
            PA2016.setName("PA2016");
            panel1.add(PA2016);
            PA2016.setBounds(645, 440, 184, PA2016.getPreferredSize().height);

            //---- OBJ_48 ----
            OBJ_48.setText("Toutes les soci\u00e9t\u00e9s");
            OBJ_48.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_48.setName("OBJ_48");
            OBJ_48.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_48ActionPerformed(e);
              }
            });
            panel1.add(OBJ_48);
            OBJ_48.setBounds(new Rectangle(new Point(690, 475), OBJ_48.getPreferredSize()));

            //---- label6 ----
            label6.setText("D\u00e9but      Fin       Encours");
            label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
            label6.setName("label6");
            panel1.add(label6);
            label6.setBounds(455, 45, 184, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 870, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 540, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //---- BT_PGUP ----
    BT_PGUP.setText("");
    BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_PGUP.setName("BT_PGUP");

    //---- BT_PGDOWN ----
    BT_PGDOWN.setText("");
    BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_PGDOWN.setName("BT_PGDOWN");
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie PAS001;
  private XRiCheckBox PAT001;
  private RiZoneSortie PAN001;
  private XRiCalendrier PAM001;
  private RiZoneSortie PA1001;
  private RiZoneSortie PA2001;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private RiZoneSortie PAS2;
  private XRiCheckBox PAT002;
  private RiZoneSortie PAN2;
  private XRiCalendrier PAM002;
  private RiZoneSortie PA1002;
  private RiZoneSortie PA2002;
  private RiZoneSortie PAS3;
  private XRiCheckBox PAT003;
  private RiZoneSortie PAN3;
  private XRiCalendrier PAM003;
  private RiZoneSortie PA1003;
  private RiZoneSortie PA2003;
  private RiZoneSortie PAS4;
  private XRiCheckBox PAT004;
  private RiZoneSortie PAN4;
  private XRiCalendrier PAM004;
  private RiZoneSortie PA1004;
  private RiZoneSortie PA2004;
  private RiZoneSortie PAS5;
  private XRiCheckBox PAT005;
  private RiZoneSortie PAN5;
  private XRiCalendrier PAM005;
  private RiZoneSortie PA1005;
  private RiZoneSortie PA2005;
  private RiZoneSortie PAS6;
  private XRiCheckBox PAT006;
  private RiZoneSortie PAN6;
  private XRiCalendrier PAM006;
  private RiZoneSortie PA1006;
  private RiZoneSortie PA2006;
  private RiZoneSortie PAS7;
  private XRiCheckBox PAT007;
  private RiZoneSortie PAN7;
  private XRiCalendrier PAM007;
  private RiZoneSortie PA1007;
  private RiZoneSortie PA2007;
  private RiZoneSortie PAS8;
  private XRiCheckBox PAT008;
  private RiZoneSortie PAN8;
  private XRiCalendrier PAM008;
  private RiZoneSortie PA1008;
  private RiZoneSortie PA2008;
  private RiZoneSortie PAS9;
  private XRiCheckBox PAT009;
  private RiZoneSortie PAN9;
  private XRiCalendrier PAM009;
  private RiZoneSortie PA1009;
  private RiZoneSortie PA2009;
  private RiZoneSortie PAS10;
  private XRiCheckBox PAT010;
  private RiZoneSortie PAN10;
  private XRiCalendrier PAM010;
  private RiZoneSortie PA1010;
  private RiZoneSortie PA2010;
  private RiZoneSortie PAS11;
  private XRiCheckBox PAT011;
  private RiZoneSortie PAN11;
  private XRiCalendrier PAM011;
  private RiZoneSortie PA1011;
  private RiZoneSortie PA2011;
  private RiZoneSortie PAS12;
  private XRiCheckBox PAT012;
  private RiZoneSortie PAN12;
  private XRiCalendrier PAM012;
  private RiZoneSortie PA1012;
  private RiZoneSortie PA2012;
  private RiZoneSortie PAS13;
  private XRiCheckBox PAT013;
  private RiZoneSortie PAN13;
  private XRiCalendrier PAM013;
  private RiZoneSortie PA1013;
  private RiZoneSortie PA2013;
  private RiZoneSortie PAS14;
  private XRiCheckBox PAT014;
  private RiZoneSortie PAN14;
  private XRiCalendrier PAM014;
  private RiZoneSortie PA1014;
  private RiZoneSortie PA2014;
  private RiZoneSortie PAS15;
  private XRiCheckBox PAT015;
  private RiZoneSortie PAN15;
  private XRiCalendrier PAM015;
  private RiZoneSortie PA1015;
  private RiZoneSortie PA2015;
  private RiZoneSortie PAS16;
  private XRiCheckBox PAT016;
  private RiZoneSortie PAN16;
  private XRiCalendrier PAM016;
  private RiZoneSortie PA1016;
  private RiZoneSortie PA2016;
  private SNBouton OBJ_48;
  private JLabel label6;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
