
package ri.serien.libecranrpg.scgm.SCGM13FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.ODialog;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.autonome.recherchejournaux.GfxRechercheJournaux;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SCGM13FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  public ODialog dialog_JO = null;
  
  public SCGM13FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TRICPT.setValeursSelection("OUI", "NON");
    TRIPCE.setValeursSelection("OUI", "NON");
    RECH.setValeursSelection("OUI", "NON");
    OPT4.setValeursSelection("1", " ");
    OPT2.setValeursSelection("1", " ");
    OPT3.setValeursSelection("1", " ");
    OPT1.setValeursSelection("1", " ");
    OPT0.setValeursSelection("**", "  ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    OBJ_32_OBJ_32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@JOLIB@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MESCLO@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSOC@")).trim());
    z_wencx_2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGPERX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    CODEJO.setEnabled(lexique.isPresent("CODEJO"));
    DATE.setVisible(lexique.isPresent("DATE"));
    
    OBJ_32_OBJ_32.setVisible(lexique.isPresent("JOLIB"));
    
    // Titre
    // setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@"));
    setTitle(p_bpresentation.getText());
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OPT0MouseClicked(MouseEvent e) {
    panel1.setVisible(!panel1.isVisible());
  }
  
  private void BTN_JOActionPerformed(ActionEvent e) {
    lexique.addVariableGlobale("CODE_SOC", z_etablissement_.getText());
    lexique.addVariableGlobale("ZONE_JO", "CODEJO");
    if (dialog_JO == null) {
      dialog_JO = new ODialog((Window) getTopLevelAncestor(), new GfxRechercheJournaux(this));
    }
    dialog_JO.affichePopupPerso();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    DATE = new XRiTextField();
    DATDEB = new XRiCalendrier();
    DATFIN = new XRiCalendrier();
    CODEJO = new XRiTextField();
    OBJ_22 = new JXTitledSeparator();
    OBJ_21 = new JXTitledSeparator();
    OBJ_25 = new JXTitledSeparator();
    OBJ_20 = new JXTitledSeparator();
    OBJ_32_OBJ_32 = new RiZoneSortie();
    OPT0 = new XRiCheckBox();
    panel1 = new JPanel();
    OPT1 = new XRiCheckBox();
    OPT3 = new XRiCheckBox();
    OPT2 = new XRiCheckBox();
    OPT4 = new XRiCheckBox();
    RECH = new XRiCheckBox();
    TRICPT = new XRiCheckBox();
    OBJ_49_OBJ_49 = new JLabel();
    OBJ_26_OBJ_26 = new JLabel();
    OBJ_47_OBJ_47 = new JLabel();
    OBJ_48_OBJ_48 = new JLabel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    z_wencx_2 = new RiZoneSortie();
    label2 = new JLabel();
    BTN_JO = new JButton();
    TRIPCE = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Type de sortie");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(700, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- DATE ----
          DATE.setComponentPopupMenu(BTD);
          DATE.setName("DATE");

          //---- DATDEB ----
          DATDEB.setComponentPopupMenu(BTD);
          DATDEB.setName("DATDEB");

          //---- DATFIN ----
          DATFIN.setComponentPopupMenu(BTD);
          DATFIN.setName("DATFIN");

          //---- CODEJO ----
          CODEJO.setComponentPopupMenu(BTD);
          CODEJO.setName("CODEJO");

          //---- OBJ_22 ----
          OBJ_22.setTitle("Options pour l'\u00e9dition");
          OBJ_22.setName("OBJ_22");

          //---- OBJ_21 ----
          OBJ_21.setTitle("");
          OBJ_21.setName("OBJ_21");

          //---- OBJ_25 ----
          OBJ_25.setTitle("");
          OBJ_25.setName("OBJ_25");

          //---- OBJ_20 ----
          OBJ_20.setTitle("");
          OBJ_20.setName("OBJ_20");

          //---- OBJ_32_OBJ_32 ----
          OBJ_32_OBJ_32.setText("@JOLIB@");
          OBJ_32_OBJ_32.setName("OBJ_32_OBJ_32");

          //---- OPT0 ----
          OPT0.setText("S\u00e9lection compl\u00e8te des folios");
          OPT0.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPT0.setName("OPT0");
          OPT0.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              OPT0MouseClicked(e);
            }
          });

          //======== panel1 ========
          {
            panel1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OPT1 ----
            OPT1.setText("Homologu\u00e9s");
            OPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT1.setName("OPT1");
            panel1.add(OPT1);
            OPT1.setBounds(20, 10, 141, 20);

            //---- OPT3 ----
            OPT3.setText("Erron\u00e9s");
            OPT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT3.setName("OPT3");
            panel1.add(OPT3);
            OPT3.setBounds(165, 10, 112, 20);

            //---- OPT2 ----
            OPT2.setText("En attente");
            OPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT2.setName("OPT2");
            panel1.add(OPT2);
            OPT2.setBounds(20, 35, 123, 20);

            //---- OPT4 ----
            OPT4.setText("Verrouill\u00e9s");
            OPT4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPT4.setName("OPT4");
            panel1.add(OPT4);
            OPT4.setBounds(165, 35, 124, 20);
          }

          //---- RECH ----
          RECH.setText("Edition Ech\u00e9ance");
          RECH.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RECH.setName("RECH");

          //---- TRICPT ----
          TRICPT.setText("Edition tri\u00e9e par compte");
          TRICPT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TRICPT.setName("TRICPT");

          //---- OBJ_49_OBJ_49 ----
          OBJ_49_OBJ_49.setText("Plage de dates");
          OBJ_49_OBJ_49.setName("OBJ_49_OBJ_49");

          //---- OBJ_26_OBJ_26 ----
          OBJ_26_OBJ_26.setText("Date \u00e0 traiter");
          OBJ_26_OBJ_26.setName("OBJ_26_OBJ_26");

          //---- OBJ_47_OBJ_47 ----
          OBJ_47_OBJ_47.setText("OU");
          OBJ_47_OBJ_47.setFont(OBJ_47_OBJ_47.getFont().deriveFont(OBJ_47_OBJ_47.getFont().getStyle() | Font.BOLD));
          OBJ_47_OBJ_47.setName("OBJ_47_OBJ_47");

          //---- OBJ_48_OBJ_48 ----
          OBJ_48_OBJ_48.setText("au");
          OBJ_48_OBJ_48.setName("OBJ_48_OBJ_48");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@MESCLO@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WSOC@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- z_wencx_2 ----
          z_wencx_2.setText("@DGPERX@");
          z_wencx_2.setFont(z_wencx_2.getFont().deriveFont(z_wencx_2.getFont().getStyle() | Font.BOLD));
          z_wencx_2.setName("z_wencx_2");

          //---- label2 ----
          label2.setText("En cours");
          label2.setName("label2");

          //---- BTN_JO ----
          BTN_JO.setText("Journal");
          BTN_JO.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BTN_JO.setToolTipText("Recherche d'un journal");
          BTN_JO.setName("BTN_JO");
          BTN_JO.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              BTN_JOActionPerformed(e);
            }
          });

          //---- TRIPCE ----
          TRIPCE.setText("Edition tri\u00e9e par num\u00e9ro de pi\u00e8ce");
          TRIPCE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TRIPCE.setName("TRIPCE");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(label2, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, 0)
                    .addComponent(z_wencx_2, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE))))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(85, 85, 85)
                    .addComponent(DATE, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_26_OBJ_26, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE))
                .addGap(43, 43, 43)
                .addComponent(OBJ_47_OBJ_47, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41)
                .addComponent(OBJ_49_OBJ_49, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(DATDEB, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(OBJ_48_OBJ_48, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(DATFIN, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(BTN_JO, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(CODEJO, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_32_OBJ_32, GroupLayout.PREFERRED_SIZE, 282, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(OPT0, GroupLayout.PREFERRED_SIZE, 195, GroupLayout.PREFERRED_SIZE)
                .addGap(50, 50, 50)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 315, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(RECH, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
                .addGap(135, 135, 135)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(TRICPT, GroupLayout.PREFERRED_SIZE, 166, GroupLayout.PREFERRED_SIZE)
                  .addComponent(TRIPCE, GroupLayout.PREFERRED_SIZE, 309, GroupLayout.PREFERRED_SIZE)))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(label2))
                      .addComponent(z_wencx_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGap(56, 56, 56)
                .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(DATE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_26_OBJ_26, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_47_OBJ_47, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_49_OBJ_49, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(DATDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_48_OBJ_48, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(DATFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(BTN_JO)
                  .addComponent(CODEJO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_32_OBJ_32, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(19, 19, 19)
                .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(OPT0, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(RECH, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(TRICPT, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(TRIPCE, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private XRiTextField DATE;
  private XRiCalendrier DATDEB;
  private XRiCalendrier DATFIN;
  private XRiTextField CODEJO;
  private JXTitledSeparator OBJ_22;
  private JXTitledSeparator OBJ_21;
  private JXTitledSeparator OBJ_25;
  private JXTitledSeparator OBJ_20;
  private RiZoneSortie OBJ_32_OBJ_32;
  private XRiCheckBox OPT0;
  private JPanel panel1;
  private XRiCheckBox OPT1;
  private XRiCheckBox OPT3;
  private XRiCheckBox OPT2;
  private XRiCheckBox OPT4;
  private XRiCheckBox RECH;
  private XRiCheckBox TRICPT;
  private JLabel OBJ_49_OBJ_49;
  private JLabel OBJ_26_OBJ_26;
  private JLabel OBJ_47_OBJ_47;
  private JLabel OBJ_48_OBJ_48;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private RiZoneSortie z_wencx_2;
  private JLabel label2;
  private JButton BTN_JO;
  private XRiCheckBox TRIPCE;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
