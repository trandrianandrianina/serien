
package ri.serien.libecranrpg.scgm.SCGM27FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;

/**
 * @author Stéphane Vénéri
 */
public class SCGM27FM_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SCGM27FM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    ECH6.setValeursSelection("X", " ");
    ECH5.setValeursSelection("X", " ");
    ECH4.setValeursSelection("X", " ");
    ECH3.setValeursSelection("X", " ");
    ECH2.setValeursSelection("X", " ");
    ECH1.setValeursSelection("X", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    ECH1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DET1@")).trim());
    ECH2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DET2@")).trim());
    ECH3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DET3@")).trim());
    ECH4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DET4@")).trim());
    ECH5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DET5@")).trim());
    ECH6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DET6@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    ECH6.setEnabled(lexique.isTrue("36"));
    // ECH6.setSelected(lexique.HostFieldGetData("ECH6").equalsIgnoreCase("X"));
    ECH5.setEnabled(lexique.isTrue("35"));
    // ECH5.setSelected(lexique.HostFieldGetData("ECH5").equalsIgnoreCase("X"));
    ECH4.setEnabled(lexique.isTrue("34"));
    // ECH4.setSelected(lexique.HostFieldGetData("ECH4").equalsIgnoreCase("X"));
    ECH3.setEnabled(lexique.isTrue("33"));
    // ECH3.setSelected(lexique.HostFieldGetData("ECH3").equalsIgnoreCase("X"));
    ECH2.setEnabled(lexique.isTrue("32"));
    // ECH2.setSelected(lexique.HostFieldGetData("ECH2").equalsIgnoreCase("X"));
    ECH1.setEnabled(lexique.isTrue("31"));
    // ECH1.setSelected(lexique.HostFieldGetData("ECH1").equalsIgnoreCase("X"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Sélection"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (ECH6.isSelected())
    // lexique.HostFieldPutData("ECH6", 0, "X");
    // else
    // lexique.HostFieldPutData("ECH6", 0, " ");
    // if (ECH5.isSelected())
    // lexique.HostFieldPutData("ECH5", 0, "X");
    // else
    // lexique.HostFieldPutData("ECH5", 0, " ");
    // if (ECH4.isSelected())
    // lexique.HostFieldPutData("ECH4", 0, "X");
    // else
    // lexique.HostFieldPutData("ECH4", 0, " ");
    // if (ECH3.isSelected())
    // lexique.HostFieldPutData("ECH3", 0, "X");
    // else
    // lexique.HostFieldPutData("ECH3", 0, " ");
    // if (ECH2.isSelected())
    // lexique.HostFieldPutData("ECH2", 0, "X");
    // else
    // lexique.HostFieldPutData("ECH2", 0, " ");
    // if (ECH1.isSelected())
    // lexique.HostFieldPutData("ECH1", 0, "X");
    // else
    // lexique.HostFieldPutData("ECH1", 0, " ");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    ECH1 = new XRiCheckBox();
    ECH2 = new XRiCheckBox();
    ECH3 = new XRiCheckBox();
    ECH4 = new XRiCheckBox();
    ECH5 = new XRiCheckBox();
    ECH6 = new XRiCheckBox();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(415, 225));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder("D\u00e9tail des \u00e9ch\u00e9ances"));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- ECH1 ----
          ECH1.setText("@DET1@");
          ECH1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ECH1.setName("ECH1");
          p_recup.add(ECH1);
          ECH1.setBounds(20, 35, 190, 20);

          //---- ECH2 ----
          ECH2.setText("@DET2@");
          ECH2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ECH2.setName("ECH2");
          p_recup.add(ECH2);
          ECH2.setBounds(20, 60, 190, 20);

          //---- ECH3 ----
          ECH3.setText("@DET3@");
          ECH3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ECH3.setName("ECH3");
          p_recup.add(ECH3);
          ECH3.setBounds(20, 85, 190, 20);

          //---- ECH4 ----
          ECH4.setText("@DET4@");
          ECH4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ECH4.setName("ECH4");
          p_recup.add(ECH4);
          ECH4.setBounds(20, 110, 190, 20);

          //---- ECH5 ----
          ECH5.setText("@DET5@");
          ECH5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ECH5.setName("ECH5");
          p_recup.add(ECH5);
          ECH5.setBounds(20, 135, 190, 20);

          //---- ECH6 ----
          ECH6.setText("@DET6@");
          ECH6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ECH6.setName("ECH6");
          p_recup.add(ECH6);
          ECH6.setBounds(20, 160, 190, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_recup.getComponentCount(); i++) {
              Rectangle bounds = p_recup.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_recup.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_recup.setMinimumSize(preferredSize);
            p_recup.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(p_recup);
        p_recup.setBounds(10, 10, 225, 205);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_recup;
  private XRiCheckBox ECH1;
  private XRiCheckBox ECH2;
  private XRiCheckBox ECH3;
  private XRiCheckBox ECH4;
  private XRiCheckBox ECH5;
  private XRiCheckBox ECH6;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
