
package ri.serien.libecranrpg.scgm.SCGM9ZFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SCGM9ZFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SCGM9ZFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    DEMHOM.setValeursSelection("OUI", "NON");
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSOC@")).trim());
    OBJ_45_OBJ_45.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MESCLO@")).trim());
    OBJ_44_OBJ_44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGPERX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    OBJ_44_OBJ_44.setVisible(lexique.isPresent("DGPERX"));
    OBJ_45_OBJ_45.setVisible(lexique.isPresent("MESCLO"));
    // DEMHOM.setVisible( lexique.isPresent("DEMHOM"));
    // DEMHOM.setSelected(lexique.HostFieldGetData("DEMHOM").equalsIgnoreCase("OUI"));
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_45_OBJ_45 = new RiZoneSortie();
    OBJ_44_OBJ_44 = new RiZoneSortie();
    OBJ_43_OBJ_43 = new JLabel();
    EPNCGD = new XRiTextField();
    EPLIBD = new XRiTextField();
    NCTVA = new XRiTextField();
    EPNCGC = new XRiTextField();
    EPLIBC = new XRiTextField();
    PERD = new XRiCalendrier();
    PERF = new XRiCalendrier();
    EPJO = new XRiTextField();
    EPMTT = new XRiTextField();
    PERIO1 = new XRiCalendrier();
    EPSAN = new XRiTextField();
    DEMHOM = new XRiCheckBox();
    COLEC = new XRiTextField();
    OBJ_38 = new JXTitledSeparator();
    OBJ_67_OBJ_67 = new JLabel();
    OBJ_71_OBJ_71 = new JLabel();
    OBJ_66_OBJ_66 = new JLabel();
    OBJ_48_OBJ_48 = new JLabel();
    OBJ_69_OBJ_69 = new JLabel();
    OBJ_68_OBJ_68 = new JLabel();
    OBJ_65_OBJ_65 = new JLabel();
    OBJ_47_OBJ_47 = new JLabel();
    OBJ_64_OBJ_64 = new JLabel();
    OBJ_49_OBJ_49 = new JLabel();
    OBJ_72_OBJ_72 = new JLabel();
    OBJ_70_OBJ_70 = new JLabel();
    separator1 = new JSeparator();
    TTVA = new XRiTextField();
    OBJ_66_OBJ_67 = new JLabel();
    EPNAT = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setToolTipText("Type de sortie");
              riSousMenu_bt_export.setText("Type de sortie");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(720, 430));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WSOC@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_45_OBJ_45 ----
          OBJ_45_OBJ_45.setText("@MESCLO@");
          OBJ_45_OBJ_45.setName("OBJ_45_OBJ_45");

          //---- OBJ_44_OBJ_44 ----
          OBJ_44_OBJ_44.setText("@DGPERX@");
          OBJ_44_OBJ_44.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_44_OBJ_44.setName("OBJ_44_OBJ_44");

          //---- OBJ_43_OBJ_43 ----
          OBJ_43_OBJ_43.setText("En cours");
          OBJ_43_OBJ_43.setName("OBJ_43_OBJ_43");

          //---- EPNCGD ----
          EPNCGD.setComponentPopupMenu(BTD);
          EPNCGD.setName("EPNCGD");

          //---- EPLIBD ----
          EPLIBD.setComponentPopupMenu(null);
          EPLIBD.setName("EPLIBD");

          //---- NCTVA ----
          NCTVA.setComponentPopupMenu(null);
          NCTVA.setName("NCTVA");

          //---- EPNCGC ----
          EPNCGC.setComponentPopupMenu(BTD);
          EPNCGC.setName("EPNCGC");

          //---- EPLIBC ----
          EPLIBC.setComponentPopupMenu(null);
          EPLIBC.setName("EPLIBC");

          //---- PERD ----
          PERD.setComponentPopupMenu(null);
          PERD.setName("PERD");

          //---- PERF ----
          PERF.setComponentPopupMenu(null);
          PERF.setName("PERF");

          //---- EPJO ----
          EPJO.setComponentPopupMenu(BTD);
          EPJO.setName("EPJO");

          //---- EPMTT ----
          EPMTT.setComponentPopupMenu(null);
          EPMTT.setName("EPMTT");

          //---- PERIO1 ----
          PERIO1.setComponentPopupMenu(null);
          PERIO1.setTypeSaisie(6);
          PERIO1.setName("PERIO1");

          //---- EPSAN ----
          EPSAN.setComponentPopupMenu(null);
          EPSAN.setName("EPSAN");

          //---- DEMHOM ----
          DEMHOM.setText("Homologation folio");
          DEMHOM.setComponentPopupMenu(BTD);
          DEMHOM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          DEMHOM.setName("DEMHOM");

          //---- COLEC ----
          COLEC.setComponentPopupMenu(BTD);
          COLEC.setName("COLEC");

          //---- OBJ_38 ----
          OBJ_38.setTitle("Diff\u00e9rence de r\u00e9glement");
          OBJ_38.setName("OBJ_38");

          //---- OBJ_67_OBJ_67 ----
          OBJ_67_OBJ_67.setText("Date comptable du folio g\u00e9n\u00e9r\u00e9");
          OBJ_67_OBJ_67.setName("OBJ_67_OBJ_67");

          //---- OBJ_71_OBJ_71 ----
          OBJ_71_OBJ_71.setText("Limite maximum");
          OBJ_71_OBJ_71.setName("OBJ_71_OBJ_71");

          //---- OBJ_66_OBJ_66 ----
          OBJ_66_OBJ_66.setText("Section analytique");
          OBJ_66_OBJ_66.setName("OBJ_66_OBJ_66");

          //---- OBJ_48_OBJ_48 ----
          OBJ_48_OBJ_48.setText("Libell\u00e9 \u00e9criture");
          OBJ_48_OBJ_48.setName("OBJ_48_OBJ_48");

          //---- OBJ_69_OBJ_69 ----
          OBJ_69_OBJ_69.setText("P\u00e9riode \u00e0 solder");
          OBJ_69_OBJ_69.setName("OBJ_69_OBJ_69");

          //---- OBJ_68_OBJ_68 ----
          OBJ_68_OBJ_68.setText("Collectif \u00e0 traiter");
          OBJ_68_OBJ_68.setName("OBJ_68_OBJ_68");

          //---- OBJ_65_OBJ_65 ----
          OBJ_65_OBJ_65.setText("sur journal");
          OBJ_65_OBJ_65.setName("OBJ_65_OBJ_65");

          //---- OBJ_47_OBJ_47 ----
          OBJ_47_OBJ_47.setText("N\u00b0Compte");
          OBJ_47_OBJ_47.setName("OBJ_47_OBJ_47");

          //---- OBJ_64_OBJ_64 ----
          OBJ_64_OBJ_64.setText("Cr\u00e9dit");
          OBJ_64_OBJ_64.setName("OBJ_64_OBJ_64");

          //---- OBJ_49_OBJ_49 ----
          OBJ_49_OBJ_49.setText("D\u00e9bit");
          OBJ_49_OBJ_49.setName("OBJ_49_OBJ_49");

          //---- OBJ_72_OBJ_72 ----
          OBJ_72_OBJ_72.setText("au");
          OBJ_72_OBJ_72.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_72_OBJ_72.setName("OBJ_72_OBJ_72");

          //---- OBJ_70_OBJ_70 ----
          OBJ_70_OBJ_70.setText("du");
          OBJ_70_OBJ_70.setName("OBJ_70_OBJ_70");

          //---- separator1 ----
          separator1.setName("separator1");

          //---- TTVA ----
          TTVA.setComponentPopupMenu(null);
          TTVA.setName("TTVA");

          //---- OBJ_66_OBJ_67 ----
          OBJ_66_OBJ_67.setText("Nature");
          OBJ_66_OBJ_67.setName("OBJ_66_OBJ_67");

          //---- EPNAT ----
          EPNAT.setComponentPopupMenu(null);
          EPNAT.setToolTipText("nature (analytique)");
          EPNAT.setName("EPNAT");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 645, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(59, 59, 59)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(52, 52, 52)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(OBJ_43_OBJ_43, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addComponent(OBJ_44_OBJ_44, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)))
                    .addGap(10, 10, 10)
                    .addComponent(OBJ_45_OBJ_45, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE))))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 650, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(189, 189, 189)
                .addComponent(OBJ_47_OBJ_47, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(OBJ_48_OBJ_48, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(59, 59, 59)
                .addComponent(OBJ_49_OBJ_49, GroupLayout.PREFERRED_SIZE, 53, GroupLayout.PREFERRED_SIZE)
                .addGap(77, 77, 77)
                .addComponent(EPNCGD, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(EPLIBD, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(NCTVA, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(TTVA, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(59, 59, 59)
                .addComponent(OBJ_64_OBJ_64, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)
                .addGap(74, 74, 74)
                .addComponent(EPNCGC, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(EPLIBC, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(separator1, GroupLayout.PREFERRED_SIZE, 650, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(59, 59, 59)
                .addComponent(OBJ_69_OBJ_69, GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE)
                .addGap(48, 48, 48)
                .addComponent(OBJ_70_OBJ_70, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addComponent(PERD, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_72_OBJ_72, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(PERF, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(OBJ_65_OBJ_65, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(EPJO, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(59, 59, 59)
                .addComponent(OBJ_71_OBJ_71, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
                .addGap(74, 74, 74)
                .addComponent(EPMTT, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                .addGap(51, 51, 51)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_67_OBJ_67, GroupLayout.PREFERRED_SIZE, 186, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(185, 185, 185)
                    .addComponent(PERIO1, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE))))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(59, 59, 59)
                .addComponent(OBJ_66_OBJ_66, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                .addGap(70, 70, 70)
                .addComponent(EPSAN, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                .addGap(49, 49, 49)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_66_OBJ_67, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(47, 47, 47)
                    .addComponent(EPNAT, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE))))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(59, 59, 59)
                .addComponent(OBJ_68_OBJ_68, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
                .addGap(75, 75, 75)
                .addComponent(COLEC, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addComponent(DEMHOM, GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(OBJ_43_OBJ_43, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(OBJ_44_OBJ_44, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_45_OBJ_45, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGap(16, 16, 16)
                .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_47_OBJ_47, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_48_OBJ_48, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_49_OBJ_49, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(EPNCGD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(EPLIBD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NCTVA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(TTVA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_64_OBJ_64, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(EPNCGC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(EPLIBC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17)
                .addComponent(separator1, GroupLayout.PREFERRED_SIZE, 5, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_69_OBJ_69, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_70_OBJ_70, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(PERD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_72_OBJ_72, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(PERF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_65_OBJ_65, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(EPJO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_71_OBJ_71, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(EPMTT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_67_OBJ_67, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(PERIO1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(EPSAN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(EPNAT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(OBJ_66_OBJ_66, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_66_OBJ_67, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_68_OBJ_68, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                  .addComponent(COLEC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(DEMHOM, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private RiZoneSortie OBJ_45_OBJ_45;
  private RiZoneSortie OBJ_44_OBJ_44;
  private JLabel OBJ_43_OBJ_43;
  private XRiTextField EPNCGD;
  private XRiTextField EPLIBD;
  private XRiTextField NCTVA;
  private XRiTextField EPNCGC;
  private XRiTextField EPLIBC;
  private XRiCalendrier PERD;
  private XRiCalendrier PERF;
  private XRiTextField EPJO;
  private XRiTextField EPMTT;
  private XRiCalendrier PERIO1;
  private XRiTextField EPSAN;
  private XRiCheckBox DEMHOM;
  private XRiTextField COLEC;
  private JXTitledSeparator OBJ_38;
  private JLabel OBJ_67_OBJ_67;
  private JLabel OBJ_71_OBJ_71;
  private JLabel OBJ_66_OBJ_66;
  private JLabel OBJ_48_OBJ_48;
  private JLabel OBJ_69_OBJ_69;
  private JLabel OBJ_68_OBJ_68;
  private JLabel OBJ_65_OBJ_65;
  private JLabel OBJ_47_OBJ_47;
  private JLabel OBJ_64_OBJ_64;
  private JLabel OBJ_49_OBJ_49;
  private JLabel OBJ_72_OBJ_72;
  private JLabel OBJ_70_OBJ_70;
  private JSeparator separator1;
  private XRiTextField TTVA;
  private JLabel OBJ_66_OBJ_67;
  private XRiTextField EPNAT;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
