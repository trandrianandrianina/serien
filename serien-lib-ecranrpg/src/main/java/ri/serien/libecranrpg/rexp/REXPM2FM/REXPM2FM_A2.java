
package ri.serien.libecranrpg.rexp.REXPM2FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class REXPM2FM_A2 extends SNPanelEcranRPG implements ioFrame {
  // Constantes
  private static final String BOUTON_CONSULTATION = "Consulter type";
  private static final String BOUTON_MODIFICATION = "Modifier type";
  
  // Variables
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "TIT1", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 1050, };
  
  /**
   * Constructeur.
   */
  public REXPM2FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    initDiverses();
    
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
    
    // Initialiser les boutons
    snBarreBouton.ajouterBouton(BOUTON_CONSULTATION, 'i', false);
    snBarreBouton.ajouterBouton(BOUTON_MODIFICATION, 'm', false);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  // -- Méthodes publiques

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Mettre à jour l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // rafraichir l'état des boutons
    rafraichirBoutonModifier();
    rafraichirBoutonConsulter();
    
    // Activation du mode consultation si ce n'est pas le cas
    if (lexique.getMode() != Lexical.MODE_CONSULTATION) {
      final SNPanelEcranRPG panel = this;
      SwingUtilities.invokeLater(new Runnable() {
        @Override
        public void run() {
          lexique.HostScreenSendKey(panel, "F15");
        }
      });
    }
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  // -- Méthodes privées
  
  /**
   * Permet d'afficher la fiche de la ligne sélectionée dans le mode choisi.
   */
  private void afficherFiche(String pValeur) {
    int ligne = WTP01.getSelectedRow();
    if (ligne > -1) {
      WTP01.setValeurTop(pValeur);
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  /**
   * Rafraichir le bouton modifier.
   */
  private void rafraichirBoutonModifier() {
    snBarreBouton.activerBouton(BOUTON_MODIFICATION, isLigneSelectionnee());
  }
  
  /**
   * Rafraichir le bouton consulter.
   */
  private void rafraichirBoutonConsulter() {
    snBarreBouton.activerBouton(BOUTON_CONSULTATION, isLigneSelectionnee());
  }
  
  /**
   * Contrôle qu'une ligne de la liste soit sélectionnée et que la ligne ne soit pas vide.
   */
  private boolean isLigneSelectionnee() {
    boolean selectionnee = false;
    int ligne = WTP01.getSelectedRow();
    if (ligne != -1) {
      selectionnee = true;
    }
    
    String nomVariable = String.format("LD%02d", ligne + 1);
    String valeur = Constantes.normerTexte(lexique.HostFieldGetData(nomVariable));
    if (valeur.isEmpty()) {
      selectionnee = false;
    }
    return selectionnee;
  }
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(BOUTON_MODIFICATION)) {
        afficherFiche("2");
      }
      else if (pSNBouton.isBouton(BOUTON_CONSULTATION)) {
        afficherFiche("1");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    // Affichage des boutons
    rafraichirBoutonModifier();
    rafraichirBoutonConsulter();
    
    // Double sur la liste
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      lexique.HostScreenSendKey(this, "Enter");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlBpresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbNumeroType = new SNLabelChamp();
    INDID = new XRiTextField();
    pnlListeType = new SNPanelTitre();
    scrpListeType = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //---- pnlBpresentation ----
    pnlBpresentation.setText("Gestion des types de mail");
    pnlBpresentation.setName("pnlBpresentation");
    add(pnlBpresentation, BorderLayout.NORTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
      ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

      //---- lbEtablissement ----
      lbEtablissement.setText("Etablissement");
      lbEtablissement.setName("lbEtablissement");
      pnlContenu.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 5), 0, 0));

      //---- snEtablissement ----
      snEtablissement.setMinimumSize(new Dimension(320, 30));
      snEtablissement.setPreferredSize(new Dimension(320, 30));
      snEtablissement.setName("snEtablissement");
      snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
        @Override
        public void valueChanged(SNComposantEvent e) {
          snEtablissementValueChanged(e);
        }
      });
      pnlContenu.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 5, 0), 0, 0));

      //---- lbNumeroType ----
      lbNumeroType.setText("Num\u00e9ro du type");
      lbNumeroType.setName("lbNumeroType");
      pnlContenu.add(lbNumeroType, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 5), 0, 0));

      //---- INDID ----
      INDID.setComponentPopupMenu(null);
      INDID.setMaximumSize(new Dimension(100, 30));
      INDID.setMinimumSize(new Dimension(100, 30));
      INDID.setPreferredSize(new Dimension(100, 30));
      INDID.setName("INDID");
      pnlContenu.add(INDID, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== pnlListeType ========
      {
        pnlListeType.setOpaque(false);
        pnlListeType.setTitre("Liste des types de mail");
        pnlListeType.setPreferredSize(new Dimension(792, 340));
        pnlListeType.setName("pnlListeType");

        //======== scrpListeType ========
        {
          scrpListeType.setMinimumSize(new Dimension(900, 0));
          scrpListeType.setPreferredSize(new Dimension(900, 324));
          scrpListeType.setAutoscrolls(true);
          scrpListeType.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
          scrpListeType.setName("scrpListeType");

          //---- WTP01 ----
          WTP01.setPreferredScrollableViewportSize(new Dimension(900, 300));
          WTP01.setMinimumSize(new Dimension(900, 0));
          WTP01.setPreferredSize(new Dimension(1050, 240));
          WTP01.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
          WTP01.setName("WTP01");
          WTP01.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              WTP01MouseClicked(e);
            }
          });
          scrpListeType.setViewportView(WTP01);
        }

        //---- BT_PGUP ----
        BT_PGUP.setText("");
        BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
        BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGUP.setName("BT_PGUP");

        //---- BT_PGDOWN ----
        BT_PGDOWN.setText("");
        BT_PGDOWN.setToolTipText("Page suivante");
        BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGDOWN.setName("BT_PGDOWN");

        GroupLayout pnlListeTypeLayout = new GroupLayout(pnlListeType);
        pnlListeType.setLayout(pnlListeTypeLayout);
        pnlListeTypeLayout.setHorizontalGroup(
          pnlListeTypeLayout.createParallelGroup()
            .addGroup(pnlListeTypeLayout.createSequentialGroup()
              .addGap(14, 14, 14)
              .addComponent(scrpListeType, GroupLayout.PREFERRED_SIZE, 710, GroupLayout.PREFERRED_SIZE)
              .addGap(5, 5, 5)
              .addGroup(pnlListeTypeLayout.createParallelGroup()
                .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)))
        );
        pnlListeTypeLayout.setVerticalGroup(
          pnlListeTypeLayout.createParallelGroup()
            .addGroup(pnlListeTypeLayout.createSequentialGroup()
              .addGap(12, 12, 12)
              .addGroup(pnlListeTypeLayout.createParallelGroup()
                .addComponent(scrpListeType, GroupLayout.PREFERRED_SIZE, 267, GroupLayout.PREFERRED_SIZE)
                .addGroup(pnlListeTypeLayout.createSequentialGroup()
                  .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                  .addGap(47, 47, 47)
                  .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))))
        );
      }
      pnlContenu.add(pnlListeType, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre pnlBpresentation;
  private SNPanelContenu pnlContenu;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbNumeroType;
  private XRiTextField INDID;
  private SNPanelTitre pnlListeType;
  private JScrollPane scrpListeType;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
