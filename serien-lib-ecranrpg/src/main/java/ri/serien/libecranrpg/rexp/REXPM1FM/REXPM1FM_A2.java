
package ri.serien.libecranrpg.rexp.REXPM1FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiBarreBouton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class REXPM1FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "TIT1", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 1050, };
  
  public REXPM1FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
    
    XRiBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    XRiBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    XRiBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Renseigner l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Active les bouton liée au V01F
    XRiBarreBouton.rafraichir(lexique);
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    XRiBarreBouton.isTraiterClickBouton(pSNBouton);
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void bt_choisirActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlNord = new JPanel();
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbIdentifiant = new SNLabelChamp();
    INDID = new XRiTextField();
    pnlListeServeur = new SNPanelTitre();
    scrListeServeur = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    XRiBarreBouton = new XRiBarreBouton();
    BTD = new JPopupMenu();
    miChoisir = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlNord ========
    {
      pnlNord.setName("pnlNord");
      pnlNord.setLayout(new VerticalLayout());

      //---- bpPresentation ----
      bpPresentation.setText("Gestion des serveurs de mail");
      bpPresentation.setName("bpPresentation");
      pnlNord.add(bpPresentation);
    }
    add(pnlNord, BorderLayout.NORTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {765, 0};
      ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 1.0, 1.0E-4};

      //======== pnlEtablissement ========
      {
        pnlEtablissement.setName("pnlEtablissement");
        pnlEtablissement.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlEtablissement.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlEtablissement.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlEtablissement.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
        ((GridBagLayout)pnlEtablissement.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //---- lbEtablissement ----
        lbEtablissement.setText("Etablissement");
        lbEtablissement.setName("lbEtablissement");
        pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snEtablissement ----
        snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
        snEtablissement.setName("snEtablissement");
        pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbIdentifiant ----
        lbIdentifiant.setText("Identifiant");
        lbIdentifiant.setName("lbIdentifiant");
        pnlEtablissement.add(lbIdentifiant, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- INDID ----
        INDID.setComponentPopupMenu(null);
        INDID.setPreferredSize(new Dimension(100, 30));
        INDID.setName("INDID");
        pnlEtablissement.add(INDID, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));

      //======== pnlListeServeur ========
      {
        pnlListeServeur.setOpaque(false);
        pnlListeServeur.setTitre("Liste des serveurs");
        pnlListeServeur.setMinimumSize(new Dimension(766, 400));
        pnlListeServeur.setPreferredSize(new Dimension(774, 330));
        pnlListeServeur.setName("pnlListeServeur");
        pnlListeServeur.setLayout(null);

        //======== scrListeServeur ========
        {
          scrListeServeur.setMinimumSize(new Dimension(900, 0));
          scrListeServeur.setPreferredSize(new Dimension(900, 324));
          scrListeServeur.setAutoscrolls(true);
          scrListeServeur.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
          scrListeServeur.setName("scrListeServeur");

          //---- WTP01 ----
          WTP01.setPreferredScrollableViewportSize(new Dimension(900, 300));
          WTP01.setComponentPopupMenu(BTD);
          WTP01.setMinimumSize(new Dimension(900, 0));
          WTP01.setPreferredSize(new Dimension(1050, 240));
          WTP01.setName("WTP01");
          WTP01.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              WTP01MouseClicked(e);
            }
          });
          scrListeServeur.setViewportView(WTP01);
        }
        pnlListeServeur.add(scrListeServeur);
        scrListeServeur.setBounds(20, 40, 710, 267);

        //---- BT_PGUP ----
        BT_PGUP.setText("");
        BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
        BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGUP.setName("BT_PGUP");
        pnlListeServeur.add(BT_PGUP);
        BT_PGUP.setBounds(735, 40, 25, 110);

        //---- BT_PGDOWN ----
        BT_PGDOWN.setText("");
        BT_PGDOWN.setToolTipText("Page suivante");
        BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGDOWN.setName("BT_PGDOWN");
        pnlListeServeur.add(BT_PGDOWN);
        BT_PGDOWN.setBounds(735, 195, 25, 110);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < pnlListeServeur.getComponentCount(); i++) {
            Rectangle bounds = pnlListeServeur.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = pnlListeServeur.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          pnlListeServeur.setMinimumSize(preferredSize);
          pnlListeServeur.setPreferredSize(preferredSize);
        }
      }
      pnlContenu.add(pnlListeServeur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- XRiBarreBouton ----
    XRiBarreBouton.setName("XRiBarreBouton");
    add(XRiBarreBouton, BorderLayout.SOUTH);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- miChoisir ----
      miChoisir.setText("Choisir");
      miChoisir.setName("miChoisir");
      miChoisir.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_choisirActionPerformed(e);
        }
      });
      BTD.add(miChoisir);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlNord;
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbIdentifiant;
  private XRiTextField INDID;
  private SNPanelTitre pnlListeServeur;
  private JScrollPane scrListeServeur;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private XRiBarreBouton XRiBarreBouton;
  private JPopupMenu BTD;
  private JMenuItem miChoisir;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
