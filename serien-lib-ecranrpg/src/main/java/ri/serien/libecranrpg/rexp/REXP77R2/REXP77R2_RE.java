
package ri.serien.libecranrpg.rexp.REXP77R2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class REXP77R2_RE extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private boolean isModifiable = true;
  
  public REXP77R2_RE(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
    String requete = lexique.HostFieldGetData("REQUETE512");
    requete = requete.replaceAll("<", "&#8249;");
    requete = requete.replaceAll(">", "&#8250;");
    visu_requete.setText("<html>" + requete + "</html>");
    
    gererleMode();
    
    // Titre
    setTitle("Affichage de la requête");
    
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    gererleMode();
  }
  
  private void gererleMode() {
    isModifiable = !isModifiable;
    COND1.setEditable(isModifiable);
    COND2.setEditable(isModifiable);
    COND3.setEditable(isModifiable);
    COND4.setEditable(isModifiable);
    COND5.setEditable(isModifiable);
    COND6.setEditable(isModifiable);
    COND7.setEditable(isModifiable);
    COND8.setEditable(isModifiable);
    COND9.setEditable(isModifiable);
    COND10.setEditable(isModifiable);
    
    if (isModifiable) {
      riSousMenu_bt6.setText("Consulter la requête");
      COND1.requestFocusInWindow();
      COND1.selectAll();
    }
    else {
      riSousMenu_bt6.setText("Modifier la requête");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    visu_requete = new JLabel();
    panel2 = new JPanel();
    COND1 = new XRiTextField();
    COND2 = new XRiTextField();
    COND3 = new XRiTextField();
    COND4 = new XRiTextField();
    COND5 = new XRiTextField();
    COND6 = new XRiTextField();
    COND7 = new XRiTextField();
    COND8 = new XRiTextField();
    COND9 = new XRiTextField();
    COND10 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_5 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(755, 435));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Modifier la requ\u00eate");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Affichage de la requ\u00eate"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- visu_requete ----
          visu_requete.setForeground(Color.blue);
          visu_requete.setFont(visu_requete.getFont().deriveFont(visu_requete.getFont().getStyle() | Font.BOLD));
          visu_requete.setName("visu_requete");
          panel1.add(visu_requete);
          visu_requete.setBounds(15, 30, 510, 115);
        }
        p_contenu.add(panel1);
        panel1.setBounds(20, 9, 540, 161);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Conditions 'WHERE'"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- COND1 ----
          COND1.setName("COND1");
          panel2.add(COND1);
          COND1.setBounds(20, 30, 500, 24);

          //---- COND2 ----
          COND2.setName("COND2");
          panel2.add(COND2);
          COND2.setBounds(20, 50, 500, 24);

          //---- COND3 ----
          COND3.setName("COND3");
          panel2.add(COND3);
          COND3.setBounds(20, 70, 500, 24);

          //---- COND4 ----
          COND4.setName("COND4");
          panel2.add(COND4);
          COND4.setBounds(20, 90, 500, 24);

          //---- COND5 ----
          COND5.setName("COND5");
          panel2.add(COND5);
          COND5.setBounds(20, 110, 500, 24);

          //---- COND6 ----
          COND6.setName("COND6");
          panel2.add(COND6);
          COND6.setBounds(20, 130, 500, 24);

          //---- COND7 ----
          COND7.setName("COND7");
          panel2.add(COND7);
          COND7.setBounds(20, 150, 500, 24);

          //---- COND8 ----
          COND8.setName("COND8");
          panel2.add(COND8);
          COND8.setBounds(20, 170, 500, 24);

          //---- COND9 ----
          COND9.setName("COND9");
          panel2.add(COND9);
          COND9.setBounds(20, 190, 500, 24);

          //---- COND10 ----
          COND10.setName("COND10");
          panel2.add(COND10);
          COND10.setBounds(20, 210, 500, 24);
        }
        p_contenu.add(panel2);
        panel2.setBounds(20, 175, 540, 251);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTD.add(OBJ_5);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel visu_requete;
  private JPanel panel2;
  private XRiTextField COND1;
  private XRiTextField COND2;
  private XRiTextField COND3;
  private XRiTextField COND4;
  private XRiTextField COND5;
  private XRiTextField COND6;
  private XRiTextField COND7;
  private XRiTextField COND8;
  private XRiTextField COND9;
  private XRiTextField COND10;
  private JPopupMenu BTD;
  private JMenuItem OBJ_5;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
