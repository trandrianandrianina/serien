
package ri.serien.libecranrpg.rexp.REXP45FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class REXP45FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "TIT1", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 1050, };
  
  public REXP45FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    BT_FIN.setIcon(lexique.chargerImage("images/pfin20.png", true));
    BT_DEB.setIcon(lexique.chargerImage("images/pdeb20.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void BT_DEBActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "D");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void BT_FINActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "F");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER", false);
    }
  }
  
  private void bt_choisirActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void bt_modifierActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void bt_dupliquerActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("3");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void bt_annulerActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void bt_afficherActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void bt_envoyerActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("6");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_32_OBJ_32 = new JLabel();
    INDPRF = new XRiTextField();
    OBJ_33_OBJ_33 = new JLabel();
    INDETA = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    SCROLLPANE__LIST_ = new JScrollPane();
    WTP01 = new XRiTable();
    BT_DEB = new JButton();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BT_FIN = new JButton();
    BTD = new JPopupMenu();
    bt_choisir = new JMenuItem();
    bt_modifier = new JMenuItem();
    bt_dupliquer = new JMenuItem();
    bt_annuler = new JMenuItem();
    bt_afficher = new JMenuItem();
    bt_envoyer = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion de la messagerie");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_32_OBJ_32 ----
          OBJ_32_OBJ_32.setText("Alias");
          OBJ_32_OBJ_32.setName("OBJ_32_OBJ_32");
          p_tete_gauche.add(OBJ_32_OBJ_32);
          OBJ_32_OBJ_32.setBounds(5, 5, 85, 18);

          //---- INDPRF ----
          INDPRF.setComponentPopupMenu(null);
          INDPRF.setName("INDPRF");
          p_tete_gauche.add(INDPRF);
          INDPRF.setBounds(120, 0, 164, INDPRF.getPreferredSize().height);

          //---- OBJ_33_OBJ_33 ----
          OBJ_33_OBJ_33.setText("Etat");
          OBJ_33_OBJ_33.setName("OBJ_33_OBJ_33");
          p_tete_gauche.add(OBJ_33_OBJ_33);
          OBJ_33_OBJ_33.setBounds(330, 5, 95, 18);

          //---- INDETA ----
          INDETA.setComponentPopupMenu(null);
          INDETA.setName("INDETA");
          p_tete_gauche.add(INDETA);
          INDETA.setBounds(425, 0, 54, INDETA.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1000, 340));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Emails envoy\u00e9s depuis S\u00e9rie N"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== SCROLLPANE__LIST_ ========
            {
              SCROLLPANE__LIST_.setMinimumSize(new Dimension(900, 0));
              SCROLLPANE__LIST_.setPreferredSize(new Dimension(900, 324));
              SCROLLPANE__LIST_.setAutoscrolls(true);
              SCROLLPANE__LIST_.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
              SCROLLPANE__LIST_.setName("SCROLLPANE__LIST_");

              //---- WTP01 ----
              WTP01.setPreferredScrollableViewportSize(new Dimension(900, 300));
              WTP01.setComponentPopupMenu(BTD);
              WTP01.setMinimumSize(new Dimension(900, 0));
              WTP01.setPreferredSize(new Dimension(1050, 240));
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE__LIST_.setViewportView(WTP01);
            }
            panel1.add(SCROLLPANE__LIST_);
            SCROLLPANE__LIST_.setBounds(10, 30, 925, 267);

            //---- BT_DEB ----
            BT_DEB.setText("");
            BT_DEB.setToolTipText("D\u00e9but de la liste");
            BT_DEB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_DEB.setName("BT_DEB");
            BT_DEB.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                BT_DEBActionPerformed(e);
              }
            });
            panel1.add(BT_DEB);
            BT_DEB.setBounds(940, 30, 25, 30);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(940, 65, 25, 95);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(940, 165, 25, 95);

            //---- BT_FIN ----
            BT_FIN.setText("");
            BT_FIN.setToolTipText("Fin de la liste");
            BT_FIN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_FIN.setName("BT_FIN");
            BT_FIN.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                BT_FINActionPerformed(e);
              }
            });
            panel1.add(BT_FIN);
            BT_FIN.setBounds(940, 267, 25, 30);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 974, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 312, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- bt_choisir ----
      bt_choisir.setText("Choisir");
      bt_choisir.setName("bt_choisir");
      bt_choisir.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_choisirActionPerformed(e);
        }
      });
      BTD.add(bt_choisir);

      //---- bt_modifier ----
      bt_modifier.setText("Modifier");
      bt_modifier.setName("bt_modifier");
      bt_modifier.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_modifierActionPerformed(e);
        }
      });
      BTD.add(bt_modifier);

      //---- bt_dupliquer ----
      bt_dupliquer.setText("Dupliquer");
      bt_dupliquer.setName("bt_dupliquer");
      bt_dupliquer.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_dupliquerActionPerformed(e);
        }
      });
      BTD.add(bt_dupliquer);

      //---- bt_annuler ----
      bt_annuler.setText("Annuler");
      bt_annuler.setName("bt_annuler");
      bt_annuler.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_annulerActionPerformed(e);
        }
      });
      BTD.add(bt_annuler);

      //---- bt_afficher ----
      bt_afficher.setText("Afficher");
      bt_afficher.setName("bt_afficher");
      bt_afficher.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_afficherActionPerformed(e);
        }
      });
      BTD.add(bt_afficher);

      //---- bt_envoyer ----
      bt_envoyer.setText("Envoyer");
      bt_envoyer.setToolTipText("Sous total");
      bt_envoyer.setName("bt_envoyer");
      bt_envoyer.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_envoyerActionPerformed(e);
        }
      });
      BTD.add(bt_envoyer);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_32_OBJ_32;
  private XRiTextField INDPRF;
  private JLabel OBJ_33_OBJ_33;
  private XRiTextField INDETA;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JScrollPane SCROLLPANE__LIST_;
  private XRiTable WTP01;
  private JButton BT_DEB;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JButton BT_FIN;
  private JPopupMenu BTD;
  private JMenuItem bt_choisir;
  private JMenuItem bt_modifier;
  private JMenuItem bt_dupliquer;
  private JMenuItem bt_annuler;
  private JMenuItem bt_afficher;
  private JMenuItem bt_envoyer;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
