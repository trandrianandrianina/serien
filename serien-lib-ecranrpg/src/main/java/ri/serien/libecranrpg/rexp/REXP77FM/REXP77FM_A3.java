
package ri.serien.libecranrpg.rexp.REXP77FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class REXP77FM_A3 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "TIT1L", };
  private String[][] _WTP01_Data = { { "LDL01", }, { "LDL02", }, { "LDL03", }, { "LDL04", }, { "LDL05", }, { "LDL06", }, { "LDL07", },
      { "LDL08", }, { "LDL09", }, { "LDL10", }, { "LDL11", }, { "LDL12", }, { "LDL13", }, { "LDL14", }, { "LDL15", }, };
  private int[] _WTP01_Width = { 1050, };
  
  public REXP77FM_A3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPGM@")).trim());
    lbLIBREF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBREF@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    BT_FIN.setIcon(lexique.chargerImage("images/pfin20.png", true));
    BT_DEB.setIcon(lexique.chargerImage("images/pdeb20.png", true));
    
    REFRECH.setVisible(!lbLIBREF.getText().trim().isEmpty());
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void BT_DEBActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "D");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void BT_FINActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "F");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void bt_choisirActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void bt_modifierActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("S");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new SNPanelContenu();
    pnlListe = new SNPanelTitre();
    lbLIBREF = new JLabel();
    REFRECH = new XRiTextField();
    SCROLLPANE__LIST_ = new JScrollPane();
    WTP01 = new XRiTable();
    BT_DEB = new JButton();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BT_FIN = new JButton();
    BTD = new JPopupMenu();
    bt_choisir = new JMenuItem();
    bt_modifier = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPGM@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setPreferredSize(new Dimension(1000, 340));
        p_contenu.setBorder(new LineBorder(Color.darkGray));
        p_contenu.setBackground(new Color(239, 239, 222));
        p_contenu.setOpaque(true);
        p_contenu.setName("p_contenu");

        //======== pnlListe ========
        {
          pnlListe.setOpaque(false);
          pnlListe.setName("pnlListe");
          pnlListe.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlListe.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
          ((GridBagLayout)pnlListe.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0};
          ((GridBagLayout)pnlListe.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlListe.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- lbLIBREF ----
          lbLIBREF.setText("@LIBREF@");
          lbLIBREF.setPreferredSize(new Dimension(250, 28));
          lbLIBREF.setMinimumSize(new Dimension(250, 28));
          lbLIBREF.setMaximumSize(new Dimension(210, 24));
          lbLIBREF.setBorder(null);
          lbLIBREF.setBackground(new Color(239, 239, 222));
          lbLIBREF.setName("lbLIBREF");
          pnlListe.add(lbLIBREF, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- REFRECH ----
          REFRECH.setComponentPopupMenu(BTD);
          REFRECH.setMinimumSize(new Dimension(210, 28));
          REFRECH.setMaximumSize(new Dimension(210, 28));
          REFRECH.setPreferredSize(new Dimension(210, 28));
          REFRECH.setName("REFRECH");
          pnlListe.add(REFRECH, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //======== SCROLLPANE__LIST_ ========
          {
            SCROLLPANE__LIST_.setMinimumSize(new Dimension(900, 270));
            SCROLLPANE__LIST_.setPreferredSize(new Dimension(900, 270));
            SCROLLPANE__LIST_.setAutoscrolls(true);
            SCROLLPANE__LIST_.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
            SCROLLPANE__LIST_.setName("SCROLLPANE__LIST_");

            //---- WTP01 ----
            WTP01.setPreferredScrollableViewportSize(new Dimension(900, 270));
            WTP01.setMinimumSize(new Dimension(900, 0));
            WTP01.setPreferredSize(new Dimension(1050, 240));
            WTP01.setComponentPopupMenu(BTD);
            WTP01.setName("WTP01");
            WTP01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                WTP01MouseClicked(e);
              }
            });
            SCROLLPANE__LIST_.setViewportView(WTP01);
          }
          pnlListe.add(SCROLLPANE__LIST_, new GridBagConstraints(0, 1, 2, 4, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- BT_DEB ----
          BT_DEB.setText("");
          BT_DEB.setToolTipText("D\u00e9but de la liste");
          BT_DEB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_DEB.setMaximumSize(new Dimension(25, 30));
          BT_DEB.setMinimumSize(new Dimension(25, 30));
          BT_DEB.setPreferredSize(new Dimension(25, 30));
          BT_DEB.setName("BT_DEB");
          BT_DEB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              BT_DEBActionPerformed(e);
            }
          });
          pnlListe.add(BT_DEB, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setMaximumSize(new Dimension(25, 95));
          BT_PGUP.setMinimumSize(new Dimension(25, 95));
          BT_PGUP.setPreferredSize(new Dimension(25, 95));
          BT_PGUP.setName("BT_PGUP");
          pnlListe.add(BT_PGUP, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setToolTipText("Page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setMaximumSize(new Dimension(25, 95));
          BT_PGDOWN.setMinimumSize(new Dimension(25, 95));
          BT_PGDOWN.setPreferredSize(new Dimension(25, 95));
          BT_PGDOWN.setName("BT_PGDOWN");
          pnlListe.add(BT_PGDOWN, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- BT_FIN ----
          BT_FIN.setText("");
          BT_FIN.setToolTipText("Fin de la liste");
          BT_FIN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_FIN.setMaximumSize(new Dimension(25, 30));
          BT_FIN.setMinimumSize(new Dimension(25, 30));
          BT_FIN.setPreferredSize(new Dimension(25, 30));
          BT_FIN.setName("BT_FIN");
          BT_FIN.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              BT_FINActionPerformed(e);
            }
          });
          pnlListe.add(BT_FIN, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(pnlListe, GroupLayout.DEFAULT_SIZE, 994, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(pnlListe, GroupLayout.DEFAULT_SIZE, 583, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_sud.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- bt_choisir ----
      bt_choisir.setText("Choisir");
      bt_choisir.setName("bt_choisir");
      bt_choisir.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_choisirActionPerformed(e);
        }
      });
      BTD.add(bt_choisir);

      //---- bt_modifier ----
      bt_modifier.setText("Services");
      bt_modifier.setName("bt_modifier");
      bt_modifier.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_modifierActionPerformed(e);
        }
      });
      BTD.add(bt_modifier);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelContenu p_contenu;
  private SNPanelTitre pnlListe;
  private JLabel lbLIBREF;
  private XRiTextField REFRECH;
  private JScrollPane SCROLLPANE__LIST_;
  private XRiTable WTP01;
  private JButton BT_DEB;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JButton BT_FIN;
  private JPopupMenu BTD;
  private JMenuItem bt_choisir;
  private JMenuItem bt_modifier;
  // JFormDesigner - End of variables declaration  //GEN-END:variables


}
