
package ri.serien.libecranrpg.rexp.REXP77RF;
// Nom Fichier: pop_REXP77REF_FMTRE0_313.java

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class REXP77RF_R0 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public REXP77RF_R0(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    OBJ_10.setIcon(lexique.chargerImage("images/OK.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITRE@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_10 = new JButton();
    panel1 = new JPanel();
    REQUETE256 = new XRiTextField();

    //======== this ========
    setName("this");

    //---- OBJ_10 ----
    OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_10.setName("OBJ_10");
    OBJ_10.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_10ActionPerformed(e);
      }
    });

    //======== panel1 ========
    {
      panel1.setBorder(new TitledBorder("Affichage de la requ\u00eate"));
      panel1.setName("panel1");
      panel1.setLayout(null);

      //---- REQUETE256 ----
      REQUETE256.setName("REQUETE256");
      panel1.add(REQUETE256);
      REQUETE256.setBounds(20, 40, 800, REQUETE256.getPreferredSize().height);
    }

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(15, 15, 15)
          .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 840, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(795, 795, 795)
          .addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(20, 20, 20)
          .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
          .addGap(5, 5, 5)
          .addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
    );
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JButton OBJ_10;
  private JPanel panel1;
  private XRiTextField REQUETE256;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
