
package ri.serien.libecranrpg.rexp.REXP45FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class REXP45FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public REXP45FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    
    TIDX3.setValeurs("3");
    TIDX4.setValeurs("4");
    TIDX5.setValeurs("5");
    TIDX6.setValeurs("6");
    TIDX07.setValeurs("7");
    TIDX08.setValeurs("8");
    TIDX09.setValeurs("9");
    TIDX10.setValeurs("10");
    TIDX12.setValeurs("12");
    TIDX13.setValeurs("13");
    TIDX16.setValeurs("16");
    SCAN3.setValeursSelection("1", " ");
    SCAN16.setValeursSelection("1", " ");
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    barre_dup.setVisible(lexique.getMode() == Lexical.MODE_DUPLICATION);
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_32_OBJ_32 = new JLabel();
    INDPRF = new XRiTextField();
    OBJ_33_OBJ_33 = new JLabel();
    INDETA = new XRiTextField();
    barre_dup = new JMenuBar();
    p_tete_gauche2 = new JPanel();
    OBJ_32_OBJ_33 = new JLabel();
    IN3PRF = new XRiTextField();
    OBJ_33_OBJ_34 = new JLabel();
    IN3ETA = new XRiTextField();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    TIDX3 = new XRiRadioButton();
    TIDX4 = new XRiRadioButton();
    TIDX5 = new XRiRadioButton();
    TIDX10 = new XRiRadioButton();
    TIDX07 = new XRiRadioButton();
    TIDX08 = new XRiRadioButton();
    TIDX09 = new XRiRadioButton();
    TIDX12 = new XRiRadioButton();
    TIDX13 = new XRiRadioButton();
    TIDX6 = new XRiRadioButton();
    TIDX16 = new XRiRadioButton();
    ARG3A = new XRiTextField();
    ARG4D = new XRiCalendrier();
    ARF4D = new XRiCalendrier();
    ARG5H = new XRiTextField();
    ARF5H = new XRiTextField();
    ARG10A = new XRiTextField();
    ARG7A1 = new XRiTextField();
    ARG8A2 = new XRiTextField();
    ARG7A2 = new XRiTextField();
    WCLIA = new XRiTextField();
    ARG8A1 = new XRiTextField();
    WFRSA = new XRiTextField();
    WPROA = new XRiTextField();
    WREPA = new XRiTextField();
    WRTEA = new XRiTextField();
    ARG9A1 = new XRiTextField();
    ARG9A2 = new XRiTextField();
    ARG12A = new XRiTextField();
    ARG13A = new XRiTextField();
    ARG6N = new XRiTextField();
    ARG16A = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    SCAN3 = new XRiCheckBox();
    SCAN16 = new XRiCheckBox();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion de la messagerie");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_32_OBJ_32 ----
          OBJ_32_OBJ_32.setText("Alias");
          OBJ_32_OBJ_32.setName("OBJ_32_OBJ_32");
          p_tete_gauche.add(OBJ_32_OBJ_32);
          OBJ_32_OBJ_32.setBounds(5, 5, 85, 18);

          //---- INDPRF ----
          INDPRF.setComponentPopupMenu(null);
          INDPRF.setName("INDPRF");
          p_tete_gauche.add(INDPRF);
          INDPRF.setBounds(120, 0, 164, INDPRF.getPreferredSize().height);

          //---- OBJ_33_OBJ_33 ----
          OBJ_33_OBJ_33.setText("Etat");
          OBJ_33_OBJ_33.setName("OBJ_33_OBJ_33");
          p_tete_gauche.add(OBJ_33_OBJ_33);
          OBJ_33_OBJ_33.setBounds(330, 5, 95, 18);

          //---- INDETA ----
          INDETA.setComponentPopupMenu(null);
          INDETA.setName("INDETA");
          p_tete_gauche.add(INDETA);
          INDETA.setBounds(425, 0, 54, INDETA.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);
      }
      p_nord.add(barre_tete);

      //======== barre_dup ========
      {
        barre_dup.setMinimumSize(new Dimension(111, 34));
        barre_dup.setPreferredSize(new Dimension(111, 34));
        barre_dup.setName("barre_dup");

        //======== p_tete_gauche2 ========
        {
          p_tete_gauche2.setOpaque(false);
          p_tete_gauche2.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche2.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche2.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche2.setName("p_tete_gauche2");
          p_tete_gauche2.setLayout(null);

          //---- OBJ_32_OBJ_33 ----
          OBJ_32_OBJ_33.setText("Par duplication de ");
          OBJ_32_OBJ_33.setName("OBJ_32_OBJ_33");
          p_tete_gauche2.add(OBJ_32_OBJ_33);
          OBJ_32_OBJ_33.setBounds(5, 5, 110, 18);

          //---- IN3PRF ----
          IN3PRF.setComponentPopupMenu(null);
          IN3PRF.setName("IN3PRF");
          p_tete_gauche2.add(IN3PRF);
          IN3PRF.setBounds(120, 0, 164, IN3PRF.getPreferredSize().height);

          //---- OBJ_33_OBJ_34 ----
          OBJ_33_OBJ_34.setText("Etat");
          OBJ_33_OBJ_34.setName("OBJ_33_OBJ_34");
          p_tete_gauche2.add(OBJ_33_OBJ_34);
          OBJ_33_OBJ_34.setBounds(330, 5, 95, 18);

          //---- IN3ETA ----
          IN3ETA.setComponentPopupMenu(null);
          IN3ETA.setName("IN3ETA");
          p_tete_gauche2.add(IN3ETA);
          IN3ETA.setBounds(425, 0, 54, IN3ETA.getPreferredSize().height);
        }
        barre_dup.add(p_tete_gauche2);
      }
      p_nord.add(barre_dup);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(800, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Recherche de mails"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- TIDX3 ----
            TIDX3.setText("Sujet");
            TIDX3.setName("TIDX3");
            panel1.add(TIDX3);
            TIDX3.setBounds(85, 50, 190, TIDX3.getPreferredSize().height);

            //---- TIDX4 ----
            TIDX4.setText("Date");
            TIDX4.setName("TIDX4");
            panel1.add(TIDX4);
            TIDX4.setBounds(85, 86, 190, TIDX4.getPreferredSize().height);

            //---- TIDX5 ----
            TIDX5.setText("Heure");
            TIDX5.setName("TIDX5");
            panel1.add(TIDX5);
            TIDX5.setBounds(85, 122, 190, TIDX5.getPreferredSize().height);

            //---- TIDX10 ----
            TIDX10.setText("Tiers");
            TIDX10.setName("TIDX10");
            panel1.add(TIDX10);
            TIDX10.setBounds(85, 158, 190, TIDX10.getPreferredSize().height);

            //---- TIDX07 ----
            TIDX07.setText("Num\u00e9ro de client");
            TIDX07.setName("TIDX07");
            panel1.add(TIDX07);
            TIDX07.setBounds(85, 194, 190, TIDX07.getPreferredSize().height);

            //---- TIDX08 ----
            TIDX08.setText("Num\u00e9ro de fournisseur");
            TIDX08.setName("TIDX08");
            panel1.add(TIDX08);
            TIDX08.setBounds(85, 230, 190, TIDX08.getPreferredSize().height);

            //---- TIDX09 ----
            TIDX09.setText("Num\u00e9ro de prospect");
            TIDX09.setName("TIDX09");
            panel1.add(TIDX09);
            TIDX09.setBounds(85, 266, 190, TIDX09.getPreferredSize().height);

            //---- TIDX12 ----
            TIDX12.setText("Num\u00e9ro de repr\u00e9sentant");
            TIDX12.setName("TIDX12");
            panel1.add(TIDX12);
            TIDX12.setBounds(85, 302, 190, TIDX12.getPreferredSize().height);

            //---- TIDX13 ----
            TIDX13.setText("Num\u00e9ro de contact");
            TIDX13.setName("TIDX13");
            panel1.add(TIDX13);
            TIDX13.setBounds(85, 338, 190, TIDX13.getPreferredSize().height);

            //---- TIDX6 ----
            TIDX6.setText("Num\u00e9ro du mail");
            TIDX6.setName("TIDX6");
            panel1.add(TIDX6);
            TIDX6.setBounds(85, 374, 190, TIDX6.getPreferredSize().height);

            //---- TIDX16 ----
            TIDX16.setText("Destinataire");
            TIDX16.setName("TIDX16");
            panel1.add(TIDX16);
            TIDX16.setBounds(85, 410, 190, TIDX16.getPreferredSize().height);

            //---- ARG3A ----
            ARG3A.setName("ARG3A");
            panel1.add(ARG3A);
            ARG3A.setBounds(310, 45, 314, ARG3A.getPreferredSize().height);

            //---- ARG4D ----
            ARG4D.setName("ARG4D");
            panel1.add(ARG4D);
            ARG4D.setBounds(310, 81, 105, ARG4D.getPreferredSize().height);

            //---- ARF4D ----
            ARF4D.setName("ARF4D");
            panel1.add(ARF4D);
            ARF4D.setBounds(465, 81, 105, ARF4D.getPreferredSize().height);

            //---- ARG5H ----
            ARG5H.setName("ARG5H");
            panel1.add(ARG5H);
            ARG5H.setBounds(310, 117, 76, ARG5H.getPreferredSize().height);

            //---- ARF5H ----
            ARF5H.setName("ARF5H");
            panel1.add(ARF5H);
            ARF5H.setBounds(465, 117, 76, ARF5H.getPreferredSize().height);

            //---- ARG10A ----
            ARG10A.setName("ARG10A");
            panel1.add(ARG10A);
            ARG10A.setBounds(310, 153, 44, ARG10A.getPreferredSize().height);

            //---- ARG7A1 ----
            ARG7A1.setName("ARG7A1");
            panel1.add(ARG7A1);
            ARG7A1.setBounds(310, 189, 76, ARG7A1.getPreferredSize().height);

            //---- ARG8A2 ----
            ARG8A2.setName("ARG8A2");
            panel1.add(ARG8A2);
            ARG8A2.setBounds(335, 225, 76, ARG8A2.getPreferredSize().height);

            //---- ARG7A2 ----
            ARG7A2.setName("ARG7A2");
            panel1.add(ARG7A2);
            ARG7A2.setBounds(390, 189, 44, ARG7A2.getPreferredSize().height);

            //---- WCLIA ----
            WCLIA.setName("WCLIA");
            panel1.add(WCLIA);
            WCLIA.setBounds(465, 189, 214, WCLIA.getPreferredSize().height);

            //---- ARG8A1 ----
            ARG8A1.setName("ARG8A1");
            panel1.add(ARG8A1);
            ARG8A1.setBounds(310, 225, 20, ARG8A1.getPreferredSize().height);

            //---- WFRSA ----
            WFRSA.setName("WFRSA");
            panel1.add(WFRSA);
            WFRSA.setBounds(465, 225, 214, WFRSA.getPreferredSize().height);

            //---- WPROA ----
            WPROA.setName("WPROA");
            panel1.add(WPROA);
            WPROA.setBounds(465, 261, 214, WPROA.getPreferredSize().height);

            //---- WREPA ----
            WREPA.setName("WREPA");
            panel1.add(WREPA);
            WREPA.setBounds(465, 297, 214, WREPA.getPreferredSize().height);

            //---- WRTEA ----
            WRTEA.setName("WRTEA");
            panel1.add(WRTEA);
            WRTEA.setBounds(465, 333, 214, WRTEA.getPreferredSize().height);

            //---- ARG9A1 ----
            ARG9A1.setName("ARG9A1");
            panel1.add(ARG9A1);
            ARG9A1.setBounds(310, 261, 76, ARG9A1.getPreferredSize().height);

            //---- ARG9A2 ----
            ARG9A2.setName("ARG9A2");
            panel1.add(ARG9A2);
            ARG9A2.setBounds(390, 261, 44, ARG9A2.getPreferredSize().height);

            //---- ARG12A ----
            ARG12A.setName("ARG12A");
            panel1.add(ARG12A);
            ARG12A.setBounds(310, 297, 34, ARG12A.getPreferredSize().height);

            //---- ARG13A ----
            ARG13A.setName("ARG13A");
            panel1.add(ARG13A);
            ARG13A.setBounds(310, 333, 76, ARG13A.getPreferredSize().height);

            //---- ARG6N ----
            ARG6N.setName("ARG6N");
            panel1.add(ARG6N);
            ARG6N.setBounds(310, 369, 76, ARG6N.getPreferredSize().height);

            //---- ARG16A ----
            ARG16A.setName("ARG16A");
            panel1.add(ARG16A);
            ARG16A.setBounds(310, 405, 314, ARG16A.getPreferredSize().height);

            //---- label1 ----
            label1.setText("/");
            label1.setHorizontalAlignment(SwingConstants.CENTER);
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(410, 87, 40, label1.getPreferredSize().height);

            //---- label2 ----
            label2.setText("/");
            label2.setHorizontalAlignment(SwingConstants.CENTER);
            label2.setName("label2");
            panel1.add(label2);
            label2.setBounds(410, 123, 40, label2.getPreferredSize().height);

            //---- SCAN3 ----
            SCAN3.setToolTipText("Scan");
            SCAN3.setName("SCAN3");
            panel1.add(SCAN3);
            SCAN3.setBounds(635, 50, 25, SCAN3.getPreferredSize().height);

            //---- SCAN16 ----
            SCAN16.setToolTipText("Scan");
            SCAN16.setName("SCAN16");
            panel1.add(SCAN16);
            SCAN16.setBounds(635, 410, 25, SCAN16.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 774, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 472, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_32_OBJ_32;
  private XRiTextField INDPRF;
  private JLabel OBJ_33_OBJ_33;
  private XRiTextField INDETA;
  private JMenuBar barre_dup;
  private JPanel p_tete_gauche2;
  private JLabel OBJ_32_OBJ_33;
  private XRiTextField IN3PRF;
  private JLabel OBJ_33_OBJ_34;
  private XRiTextField IN3ETA;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiRadioButton TIDX3;
  private XRiRadioButton TIDX4;
  private XRiRadioButton TIDX5;
  private XRiRadioButton TIDX10;
  private XRiRadioButton TIDX07;
  private XRiRadioButton TIDX08;
  private XRiRadioButton TIDX09;
  private XRiRadioButton TIDX12;
  private XRiRadioButton TIDX13;
  private XRiRadioButton TIDX6;
  private XRiRadioButton TIDX16;
  private XRiTextField ARG3A;
  private XRiCalendrier ARG4D;
  private XRiCalendrier ARF4D;
  private XRiTextField ARG5H;
  private XRiTextField ARF5H;
  private XRiTextField ARG10A;
  private XRiTextField ARG7A1;
  private XRiTextField ARG8A2;
  private XRiTextField ARG7A2;
  private XRiTextField WCLIA;
  private XRiTextField ARG8A1;
  private XRiTextField WFRSA;
  private XRiTextField WPROA;
  private XRiTextField WREPA;
  private XRiTextField WRTEA;
  private XRiTextField ARG9A1;
  private XRiTextField ARG9A2;
  private XRiTextField ARG12A;
  private XRiTextField ARG13A;
  private XRiTextField ARG6N;
  private XRiTextField ARG16A;
  private JLabel label1;
  private JLabel label2;
  private XRiCheckBox SCAN3;
  private XRiCheckBox SCAN16;
  // JFormDesigner - End of variables declaration  //GEN-END:variables


}
