
package ri.serien.libecranrpg.rexp.REXP77RF;
// Nom Fichier: pop_REXP77REF_FMTRE_270.java

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiTextArea;

/**
 * @author Stéphane Vénéri
 */
public class REXP77RF_RE extends SNPanelEcranRPG implements ioFrame {
  
   
  private LinkedHashMap<String, String> donnees = null;
  
  public REXP77RF_RE(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_20_OBJ_20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("<html>@REQUETE256@</html>")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    donnees.put("COND1", lexique.HostFieldGetData("COND1"));
    donnees.put("COND2", lexique.HostFieldGetData("COND2"));
    donnees.put("COND3", lexique.HostFieldGetData("COND3"));
    donnees.put("COND4", lexique.HostFieldGetData("COND4"));
    donnees.put("COND5", lexique.HostFieldGetData("COND5"));
    donnees.put("COND6", lexique.HostFieldGetData("COND6"));
    donnees.put("COND7", lexique.HostFieldGetData("COND7"));
    donnees.put("COND8", lexique.HostFieldGetData("COND8"));
    donnees.put("COND9", lexique.HostFieldGetData("COND9"));
    donnees.put("COND10", lexique.HostFieldGetData("COND10"));
    riTA_Conditions.setData(donnees, true, true);
    
    // TODO Icones
    OBJ_34.setIcon(lexique.chargerImage("images/retour.png", true));
    OBJ_33.setIcon(lexique.chargerImage("images/OK.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Affichage de la requête"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    riTA_Conditions.getData(50);
    for (Map.Entry<String, String> entry : donnees.entrySet()) {
      lexique.HostFieldPutData(entry.getKey(), 0, entry.getValue());
    }
  }
  
  private void OBJ_33ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_34ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    panel2 = new JPanel();
    OBJ_20_OBJ_20 = new JLabel();
    panel3 = new JPanel();
    scrollPane1 = new JScrollPane();
    riTA_Conditions = new RiTextArea();
    panel1 = new JPanel();
    OBJ_33 = new JButton();
    OBJ_34 = new JButton();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setName("this");

    //======== panel2 ========
    {
      panel2.setBorder(new TitledBorder("Affichage de la requ\u00eate"));
      panel2.setName("panel2");
      panel2.setLayout(null);

      //---- OBJ_20_OBJ_20 ----
      OBJ_20_OBJ_20.setText("<html>@REQUETE256@</html>");
      OBJ_20_OBJ_20.setForeground(Color.blue);
      OBJ_20_OBJ_20.setFont(OBJ_20_OBJ_20.getFont().deriveFont(OBJ_20_OBJ_20.getFont().getStyle() | Font.BOLD));
      OBJ_20_OBJ_20.setName("OBJ_20_OBJ_20");
      panel2.add(OBJ_20_OBJ_20);
      OBJ_20_OBJ_20.setBounds(20, 30, 510, 72);
    }

    //======== panel3 ========
    {
      panel3.setBorder(new TitledBorder("Conditions 'WHERE'"));
      panel3.setName("panel3");
      panel3.setLayout(null);

      //======== scrollPane1 ========
      {
        scrollPane1.setName("scrollPane1");

        //---- riTA_Conditions ----
        riTA_Conditions.setName("riTA_Conditions");
        scrollPane1.setViewportView(riTA_Conditions);
      }
      panel3.add(scrollPane1);
      scrollPane1.setBounds(20, 35, 515, 90);
    }

    //======== panel1 ========
    {
      panel1.setName("panel1");
      panel1.setLayout(null);

      //---- OBJ_33 ----
      OBJ_33.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_33.setName("OBJ_33");
      OBJ_33.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_33ActionPerformed(e);
        }
      });
      panel1.add(OBJ_33);
      OBJ_33.setBounds(10, 5, 56, 40);

      //---- OBJ_34 ----
      OBJ_34.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_34.setName("OBJ_34");
      OBJ_34.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_34ActionPerformed(e);
        }
      });
      panel1.add(OBJ_34);
      OBJ_34.setBounds(65, 5, 56, 40);
    }

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addContainerGap()
          .addGroup(layout.createParallelGroup()
            .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 555, GroupLayout.PREFERRED_SIZE)
            .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 555, GroupLayout.PREFERRED_SIZE))
          .addContainerGap(13, Short.MAX_VALUE))
        .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
          .addContainerGap(438, Short.MAX_VALUE)
          .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
          .addContainerGap())
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addContainerGap()
          .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
          .addGap(5, 5, 5)
          .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
          .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
          .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Invite");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel panel2;
  private JLabel OBJ_20_OBJ_20;
  private JPanel panel3;
  private JScrollPane scrollPane1;
  private RiTextArea riTA_Conditions;
  private JPanel panel1;
  private JButton OBJ_33;
  private JButton OBJ_34;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
