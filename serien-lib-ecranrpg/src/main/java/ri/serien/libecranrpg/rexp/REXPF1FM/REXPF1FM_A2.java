
package ri.serien.libecranrpg.rexp.REXPF1FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class REXPF1FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "TIT1", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 1050, };
  
  private final static String BOUTON_CHOISIR = "Choisir le filtre";
  private final static String BOUTON_MODIFIER = "Modifier le nom du filtre";
  private final static String BOUTON_METTRE_A_JOUR = "Modifier les arguments";
  private final static String BOUTON_SUPPRIMER = "Supprimer le filtre";
  
  public REXPF1FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    
    // Ajout
    initDiverses();
    
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_CHOISIR, 'c', true);
    snBarreBouton.ajouterBouton(BOUTON_MODIFIER, 'm', true);
    snBarreBouton.ajouterBouton(BOUTON_METTRE_A_JOUR, 'J', true);
    snBarreBouton.ajouterBouton(BOUTON_SUPPRIMER, 's', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    


    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_CHOISIR)) {
        WTP01.setValeurTop("1");
        lexique.HostScreenSendKey(this, "Enter");
      }
      else if (pSNBouton.isBouton(BOUTON_MODIFIER)) {
        WTP01.setValeurTop("2");
        lexique.HostScreenSendKey(this, "Enter");
      }
      else if (pSNBouton.isBouton(BOUTON_METTRE_A_JOUR)) {
        WTP01.setValeurTop("3");
        lexique.HostScreenSendKey(this, "Enter");
      }
      else if (pSNBouton.isBouton(BOUTON_SUPPRIMER)) {
        WTP01.setValeurTop("4");
        lexique.HostScreenSendKey(this, "Enter");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void bt_choisirActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void bt_ModifierActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void bt_MajActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("3");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void bt_SupprimerActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_presentation = new JPanel();
    p_logo = new JPanel();
    logoEtb = new JLabel();
    p_titre = new JPanel();
    titre = new JLabel();
    fond_titre = new JLabel();
    snBarreBouton = new SNBarreBouton();
    sNPanelPrincipal1 = new SNPanelContenu();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    pnlListe = new SNPanelTitre();
    SCROLLPANE__LIST_ = new JScrollPane();
    WTP01 = new XRiTable();
    pnlBoutonliste = new SNPanel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BTD = new JPopupMenu();
    bt_choisir = new JMenuItem();
    bt_Modifier = new JMenuItem();
    bt_Maj = new JMenuItem();
    bt_Supprimer = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //======== p_presentation ========
      {
        p_presentation.setPreferredSize(new Dimension(1020, 55));
        p_presentation.setMinimumSize(new Dimension(950, 55));
        p_presentation.setMaximumSize(new Dimension(32767, 55));
        p_presentation.setBackground(Color.white);
        p_presentation.setAutoscrolls(true);
        p_presentation.setFocusable(false);
        p_presentation.setName("p_presentation");
        p_presentation.setLayout(new BorderLayout());

        //======== p_logo ========
        {
          p_logo.setMinimumSize(new Dimension(120, 55));
          p_logo.setPreferredSize(new Dimension(120, 55));
          p_logo.setBackground(new Color(238, 239, 241));
          p_logo.setName("p_logo");
          p_logo.setLayout(null);

          //---- logoEtb ----
          logoEtb.setBorder(null);
          logoEtb.setName("logoEtb");
          p_logo.add(logoEtb);
          logoEtb.setBounds(70, 5, 45, 45);
        }
        p_presentation.add(p_logo, BorderLayout.EAST);

        //======== p_titre ========
        {
          p_titre.setBackground(new Color(238, 239, 241));
          p_titre.setPreferredSize(new Dimension(930, 0));
          p_titre.setMinimumSize(new Dimension(930, 0));
          p_titre.setName("p_titre");
          p_titre.setLayout(null);

          //---- titre ----
          titre.setBackground(new Color(198, 198, 200));
          titre.setText("Gestion des filtres de recherche");
          titre.setHorizontalAlignment(SwingConstants.LEFT);
          titre.setFont(titre.getFont().deriveFont(titre.getFont().getStyle() | Font.BOLD, titre.getFont().getSize() + 6f));
          titre.setForeground(Color.darkGray);
          titre.setName("titre");
          p_titre.add(titre);
          titre.setBounds(10, 0, 600, 55);

          //---- fond_titre ----
          fond_titre.setFont(new Font("Arial", Font.BOLD, 18));
          fond_titre.setForeground(Color.white);
          fond_titre.setName("fond_titre");
          p_titre.add(fond_titre);
          fond_titre.setBounds(0, 0, 900, 55);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_titre.getComponentCount(); i++) {
              Rectangle bounds = p_titre.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_titre.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_titre.setMinimumSize(preferredSize);
            p_titre.setPreferredSize(preferredSize);
          }
        }
        p_presentation.add(p_titre, BorderLayout.CENTER);
      }
      p_nord.add(p_presentation);
    }
    add(p_nord, BorderLayout.NORTH);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //======== sNPanelPrincipal1 ========
    {
      sNPanelPrincipal1.setPreferredSize(new Dimension(945, 350));
      sNPanelPrincipal1.setMinimumSize(new Dimension(945, 350));
      sNPanelPrincipal1.setName("sNPanelPrincipal1");
      sNPanelPrincipal1.setLayout(new GridBagLayout());
      ((GridBagLayout)sNPanelPrincipal1.getLayout()).columnWidths = new int[] {0, 0, 0};
      ((GridBagLayout)sNPanelPrincipal1.getLayout()).rowHeights = new int[] {0, 0, 0};
      ((GridBagLayout)sNPanelPrincipal1.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
      ((GridBagLayout)sNPanelPrincipal1.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

      //---- lbEtablissement ----
      lbEtablissement.setText("Etablissement");
      lbEtablissement.setName("lbEtablissement");
      sNPanelPrincipal1.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 5), 0, 0));

      //---- snEtablissement ----
      snEtablissement.setEnabled(false);
      snEtablissement.setName("snEtablissement");
      sNPanelPrincipal1.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== pnlListe ========
      {
        pnlListe.setTitre("Filtres de recherche existant");
        pnlListe.setName("pnlListe");
        pnlListe.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlListe.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlListe.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlListe.getLayout()).columnWeights = new double[] {1.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnlListe.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //======== SCROLLPANE__LIST_ ========
        {
          SCROLLPANE__LIST_.setMinimumSize(new Dimension(900, 240));
          SCROLLPANE__LIST_.setPreferredSize(new Dimension(900, 240));
          SCROLLPANE__LIST_.setAutoscrolls(true);
          SCROLLPANE__LIST_.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
          SCROLLPANE__LIST_.setMaximumSize(new Dimension(900, 240));
          SCROLLPANE__LIST_.setName("SCROLLPANE__LIST_");

          //---- WTP01 ----
          WTP01.setModel(new DefaultTableModel(
            new Object[][] {
              {"@LD01@"},
              {"@LD02@"},
              {"@LD03@"},
              {"@LD04@"},
              {"@LD05@"},
              {"@LD06@"},
              {"@LD07@"},
              {"@LD08@"},
              {"@LD09@"},
              {"@LD10@"},
              {"@LD11@"},
              {"@LD12@"},
              {"@LD13@"},
              {"@LD14@"},
              {"@LD15@"},
            },
            new String[] {
              "@TITG@"
            }
          ));
          WTP01.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 12));
          WTP01.setComponentPopupMenu(BTD);
          WTP01.setMinimumSize(new Dimension(15, 250));
          WTP01.setPreferredSize(new Dimension(75, 250));
          WTP01.setPreferredScrollableViewportSize(new Dimension(450, 250));
          WTP01.setMaximumSize(new Dimension(2147483647, 250));
          WTP01.setName("WTP01");
          WTP01.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              WTP01MouseClicked(e);
            }
          });
          SCROLLPANE__LIST_.setViewportView(WTP01);
        }
        pnlListe.add(SCROLLPANE__LIST_, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //======== pnlBoutonliste ========
        {
          pnlBoutonliste.setMinimumSize(new Dimension(28, 275));
          pnlBoutonliste.setPreferredSize(new Dimension(28, 275));
          pnlBoutonliste.setName("pnlBoutonliste");
          pnlBoutonliste.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlBoutonliste.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlBoutonliste.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
          ((GridBagLayout)pnlBoutonliste.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
          ((GridBagLayout)pnlBoutonliste.getLayout()).rowWeights = new double[] {0.0, 1.0, 1.0, 0.0, 1.0E-4};

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setMinimumSize(new Dimension(28, 90));
          BT_PGUP.setMaximumSize(new Dimension(28, 90));
          BT_PGUP.setPreferredSize(new Dimension(28, 70));
          BT_PGUP.setName("BT_PGUP");
          pnlBoutonliste.add(BT_PGUP, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setMinimumSize(new Dimension(28, 90));
          BT_PGDOWN.setMaximumSize(new Dimension(28, 90));
          BT_PGDOWN.setPreferredSize(new Dimension(28, 70));
          BT_PGDOWN.setName("BT_PGDOWN");
          pnlBoutonliste.add(BT_PGDOWN, new GridBagConstraints(0, 2, 1, 2, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlListe.add(pnlBoutonliste, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      sNPanelPrincipal1.add(pnlListe, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(sNPanelPrincipal1, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- bt_choisir ----
      bt_choisir.setText("Choisir");
      bt_choisir.setName("bt_choisir");
      bt_choisir.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_choisirActionPerformed(e);
        }
      });
      BTD.add(bt_choisir);

      //---- bt_Modifier ----
      bt_Modifier.setText("Modifier");
      bt_Modifier.setName("bt_Modifier");
      bt_Modifier.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_ModifierActionPerformed(e);
        }
      });
      BTD.add(bt_Modifier);

      //---- bt_Maj ----
      bt_Maj.setText("Mettre \u00e0 jour le filtre");
      bt_Maj.setName("bt_Maj");
      bt_Maj.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_MajActionPerformed(e);
        }
      });
      BTD.add(bt_Maj);

      //---- bt_Supprimer ----
      bt_Supprimer.setText("Supprimer le filtre");
      bt_Supprimer.setName("bt_Supprimer");
      bt_Supprimer.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_SupprimerActionPerformed(e);
        }
      });
      BTD.add(bt_Supprimer);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private JPanel p_presentation;
  private JPanel p_logo;
  private JLabel logoEtb;
  private JPanel p_titre;
  private JLabel titre;
  private JLabel fond_titre;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu sNPanelPrincipal1;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNPanelTitre pnlListe;
  private JScrollPane SCROLLPANE__LIST_;
  private XRiTable WTP01;
  private SNPanel pnlBoutonliste;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu BTD;
  private JMenuItem bt_choisir;
  private JMenuItem bt_Modifier;
  private JMenuItem bt_Maj;
  private JMenuItem bt_Supprimer;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
