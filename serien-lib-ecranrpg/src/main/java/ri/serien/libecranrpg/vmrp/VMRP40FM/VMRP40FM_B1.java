
package ri.serien.libecranrpg.vmrp.VMRP40FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VMRP40FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VMRP40FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    VVETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VVETB@")).trim());
    VVART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VVART@")).trim());
    VVMAG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VVMAG@")).trim());
    VVAN.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VVAN@")).trim());
    VVCAT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VVCAT@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Date de mise à jour @VVMAJX@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    MALIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MALIB@")).trim());
    VVLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VVLIB@")).trim());
    CCLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CCLIB@")).trim());
    VVCL1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VVCL1@")).trim());
    FRNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRNOM@")).trim());
    FALIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FALIB@")).trim());
    GRLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@GRLIB@")).trim());
    VVGRP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VVGRP@")).trim());
    VVFAM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VVFAM@")).trim());
    VVABC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VVABC@")).trim());
    VVCOF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VVCOF@")).trim());
    VVFRS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VVFRS@")).trim());
    PQT01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PQT01@")).trim());
    PQT02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PQT02@")).trim());
    PQT03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PQT03@")).trim());
    PQT04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PQT04@")).trim());
    PQT05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PQT05@")).trim());
    PQT06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PQT06@")).trim());
    PQT07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PQT07@")).trim());
    PQT08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PQT08@")).trim());
    PQT09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PQT09@")).trim());
    PQT10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PQT10@")).trim());
    PQT11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PQT11@")).trim());
    PQT12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PQT12@")).trim());
    PQTTT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PQTTT@")).trim());
    VQT01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VQT01@")).trim());
    VQT02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VQT02@")).trim());
    VQT03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VQT03@")).trim());
    VQT04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VQT04@")).trim());
    VQT05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VQT05@")).trim());
    VQT06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VQT06@")).trim());
    VQT07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VQT07@")).trim());
    VQT08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VQT08@")).trim());
    VQT09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VQT09@")).trim());
    VQT10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VQT10@")).trim());
    VQT11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VQT11@")).trim());
    VQT12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VQT12@")).trim());
    VQTTT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VQTTT@")).trim());
    RQT01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RQT01@")).trim());
    RQT02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RQT02@")).trim());
    RQT03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RQT03@")).trim());
    RQT04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RQT04@")).trim());
    RQT05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RQT05@")).trim());
    RQT06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RQT06@")).trim());
    RQT07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RQT07@")).trim());
    RQT08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RQT08@")).trim());
    RQT09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RQT09@")).trim());
    RQT10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RQT10@")).trim());
    v11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RQT11@")).trim());
    PQT28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RQT12@")).trim());
    RQTTT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RQTTT@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Titre
    setTitle("Gestion des prévisions");
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_70 = new JLabel();
    VVETB = new RiZoneSortie();
    OBJ_72 = new JLabel();
    VVART = new RiZoneSortie();
    VVMAG = new RiZoneSortie();
    OBJ_71 = new JLabel();
    OBJ_76 = new JLabel();
    VVAN = new RiZoneSortie();
    OBJ_77 = new JLabel();
    VVCAT = new RiZoneSortie();
    p_tete_droite = new JPanel();
    label1 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_35 = new JLabel();
    MALIB = new RiZoneSortie();
    VVLIB = new RiZoneSortie();
    OBJ_36 = new JLabel();
    CCLIB = new RiZoneSortie();
    OBJ_37 = new JLabel();
    OBJ_38 = new JLabel();
    VVCL1 = new RiZoneSortie();
    OBJ_39 = new JLabel();
    OBJ_40 = new JLabel();
    OBJ_41 = new JLabel();
    OBJ_54 = new JLabel();
    FRNOM = new RiZoneSortie();
    FALIB = new RiZoneSortie();
    GRLIB = new RiZoneSortie();
    VVGRP = new RiZoneSortie();
    VVFAM = new RiZoneSortie();
    VVABC = new RiZoneSortie();
    VVCOF = new RiZoneSortie();
    VVFRS = new RiZoneSortie();
    panel2 = new JPanel();
    PQT01 = new RiZoneSortie();
    PQT02 = new RiZoneSortie();
    PQT03 = new RiZoneSortie();
    PQT04 = new RiZoneSortie();
    PQT05 = new RiZoneSortie();
    PQT06 = new RiZoneSortie();
    PQT07 = new RiZoneSortie();
    PQT08 = new RiZoneSortie();
    PQT09 = new RiZoneSortie();
    PQT10 = new RiZoneSortie();
    PQT11 = new RiZoneSortie();
    PQT12 = new RiZoneSortie();
    PQTTT = new RiZoneSortie();
    label2 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();
    label11 = new JLabel();
    label12 = new JLabel();
    label13 = new JLabel();
    label14 = new JLabel();
    label15 = new JLabel();
    label16 = new JLabel();
    panel3 = new JPanel();
    VQT01 = new RiZoneSortie();
    VQT02 = new RiZoneSortie();
    VQT03 = new RiZoneSortie();
    VQT04 = new RiZoneSortie();
    VQT05 = new RiZoneSortie();
    VQT06 = new RiZoneSortie();
    VQT07 = new RiZoneSortie();
    VQT08 = new RiZoneSortie();
    VQT09 = new RiZoneSortie();
    VQT10 = new RiZoneSortie();
    VQT11 = new RiZoneSortie();
    VQT12 = new RiZoneSortie();
    VQTTT = new RiZoneSortie();
    label3 = new JLabel();
    label17 = new JLabel();
    label18 = new JLabel();
    label19 = new JLabel();
    label20 = new JLabel();
    label21 = new JLabel();
    label22 = new JLabel();
    label23 = new JLabel();
    label24 = new JLabel();
    label25 = new JLabel();
    label26 = new JLabel();
    label27 = new JLabel();
    label28 = new JLabel();
    panel4 = new JPanel();
    RQT01 = new RiZoneSortie();
    RQT02 = new RiZoneSortie();
    RQT03 = new RiZoneSortie();
    RQT04 = new RiZoneSortie();
    RQT05 = new RiZoneSortie();
    RQT06 = new RiZoneSortie();
    RQT07 = new RiZoneSortie();
    RQT08 = new RiZoneSortie();
    RQT09 = new RiZoneSortie();
    RQT10 = new RiZoneSortie();
    v11 = new RiZoneSortie();
    PQT28 = new RiZoneSortie();
    RQTTT = new RiZoneSortie();
    label4 = new JLabel();
    label29 = new JLabel();
    label30 = new JLabel();
    label31 = new JLabel();
    label32 = new JLabel();
    label33 = new JLabel();
    label34 = new JLabel();
    label35 = new JLabel();
    label36 = new JLabel();
    label37 = new JLabel();
    label38 = new JLabel();
    label39 = new JLabel();
    label40 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Analyse des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(750, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_70 ----
          OBJ_70.setText("Etablissement");
          OBJ_70.setName("OBJ_70");
          p_tete_gauche.add(OBJ_70);
          OBJ_70.setBounds(5, 1, 93, 28);

          //---- VVETB ----
          VVETB.setComponentPopupMenu(null);
          VVETB.setText("@VVETB@");
          VVETB.setOpaque(false);
          VVETB.setName("VVETB");
          p_tete_gauche.add(VVETB);
          VVETB.setBounds(100, 3, 40, VVETB.getPreferredSize().height);

          //---- OBJ_72 ----
          OBJ_72.setText("Article");
          OBJ_72.setName("OBJ_72");
          p_tete_gauche.add(OBJ_72);
          OBJ_72.setBounds(290, 1, 55, 28);

          //---- VVART ----
          VVART.setComponentPopupMenu(null);
          VVART.setText("@VVART@");
          VVART.setOpaque(false);
          VVART.setName("VVART");
          p_tete_gauche.add(VVART);
          VVART.setBounds(345, 3, 214, VVART.getPreferredSize().height);

          //---- VVMAG ----
          VVMAG.setComponentPopupMenu(BTD);
          VVMAG.setText("@VVMAG@");
          VVMAG.setOpaque(false);
          VVMAG.setName("VVMAG");
          p_tete_gauche.add(VVMAG);
          VVMAG.setBounds(230, 3, 34, VVMAG.getPreferredSize().height);

          //---- OBJ_71 ----
          OBJ_71.setText("Magasin");
          OBJ_71.setName("OBJ_71");
          p_tete_gauche.add(OBJ_71);
          OBJ_71.setBounds(165, 1, 65, 28);

          //---- OBJ_76 ----
          OBJ_76.setText("Ann\u00e9e");
          OBJ_76.setName("OBJ_76");
          p_tete_gauche.add(OBJ_76);
          OBJ_76.setBounds(580, 1, 45, 28);

          //---- VVAN ----
          VVAN.setComponentPopupMenu(BTD);
          VVAN.setOpaque(false);
          VVAN.setText("@VVAN@");
          VVAN.setName("VVAN");
          p_tete_gauche.add(VVAN);
          VVAN.setBounds(635, 3, 44, VVAN.getPreferredSize().height);

          //---- OBJ_77 ----
          OBJ_77.setText("Cat\u00e9gorie");
          OBJ_77.setName("OBJ_77");
          p_tete_gauche.add(OBJ_77);
          OBJ_77.setBounds(700, 1, 70, 28);

          //---- VVCAT ----
          VVCAT.setComponentPopupMenu(BTD);
          VVCAT.setOpaque(false);
          VVCAT.setText("@VVCAT@");
          VVCAT.setName("VVCAT");
          p_tete_gauche.add(VVCAT);
          VVCAT.setBounds(770, 3, 40, VVCAT.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- label1 ----
          label1.setText("Date de mise \u00e0 jour @VVMAJX@");
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD, label1.getFont().getSize() + 1f));
          label1.setName("label1");
          p_tete_droite.add(label1);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1000, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setPreferredSize(new Dimension(516, 570));
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_35 ----
            OBJ_35.setText("Magasin");
            OBJ_35.setName("OBJ_35");
            panel1.add(OBJ_35);
            OBJ_35.setBounds(30, 23, 75, 28);

            //---- MALIB ----
            MALIB.setText("@MALIB@");
            MALIB.setName("MALIB");
            panel1.add(MALIB);
            MALIB.setBounds(105, 25, 314, MALIB.getPreferredSize().height);

            //---- VVLIB ----
            VVLIB.setText("@VVLIB@");
            VVLIB.setName("VVLIB");
            panel1.add(VVLIB);
            VVLIB.setBounds(105, 55, 314, VVLIB.getPreferredSize().height);

            //---- OBJ_36 ----
            OBJ_36.setText("Article");
            OBJ_36.setName("OBJ_36");
            panel1.add(OBJ_36);
            OBJ_36.setBounds(30, 53, 75, 28);

            //---- CCLIB ----
            CCLIB.setText("@CCLIB@");
            CCLIB.setName("CCLIB");
            panel1.add(CCLIB);
            CCLIB.setBounds(105, 115, 314, CCLIB.getPreferredSize().height);

            //---- OBJ_37 ----
            OBJ_37.setText("Cat\u00e9gorie");
            OBJ_37.setName("OBJ_37");
            panel1.add(OBJ_37);
            OBJ_37.setBounds(30, 113, 75, 28);

            //---- OBJ_38 ----
            OBJ_38.setText("S\u00e9rie");
            OBJ_38.setName("OBJ_38");
            panel1.add(OBJ_38);
            OBJ_38.setBounds(30, 83, 75, 28);

            //---- VVCL1 ----
            VVCL1.setText("@VVCL1@");
            VVCL1.setName("VVCL1");
            panel1.add(VVCL1);
            VVCL1.setBounds(105, 85, 214, VVCL1.getPreferredSize().height);

            //---- OBJ_39 ----
            OBJ_39.setText("Groupe");
            OBJ_39.setName("OBJ_39");
            panel1.add(OBJ_39);
            OBJ_39.setBounds(450, 25, 75, 28);

            //---- OBJ_40 ----
            OBJ_40.setText("Famille");
            OBJ_40.setName("OBJ_40");
            panel1.add(OBJ_40);
            OBJ_40.setBounds(450, 55, 75, 28);

            //---- OBJ_41 ----
            OBJ_41.setText("Code ABC");
            OBJ_41.setName("OBJ_41");
            panel1.add(OBJ_41);
            OBJ_41.setBounds(450, 85, 75, 28);

            //---- OBJ_54 ----
            OBJ_54.setText("Fournisseur");
            OBJ_54.setName("OBJ_54");
            panel1.add(OBJ_54);
            OBJ_54.setBounds(450, 115, 75, 28);

            //---- FRNOM ----
            FRNOM.setText("@FRNOM@");
            FRNOM.setName("FRNOM");
            panel1.add(FRNOM);
            FRNOM.setBounds(630, 115, 239, FRNOM.getPreferredSize().height);

            //---- FALIB ----
            FALIB.setText("@FALIB@");
            FALIB.setName("FALIB");
            panel1.add(FALIB);
            FALIB.setBounds(575, 55, 294, FALIB.getPreferredSize().height);

            //---- GRLIB ----
            GRLIB.setText("@GRLIB@");
            GRLIB.setName("GRLIB");
            panel1.add(GRLIB);
            GRLIB.setBounds(555, 25, 314, GRLIB.getPreferredSize().height);

            //---- VVGRP ----
            VVGRP.setText("@VVGRP@");
            VVGRP.setName("VVGRP");
            panel1.add(VVGRP);
            VVGRP.setBounds(525, 25, 24, VVGRP.getPreferredSize().height);

            //---- VVFAM ----
            VVFAM.setText("@VVFAM@");
            VVFAM.setName("VVFAM");
            panel1.add(VVFAM);
            VVFAM.setBounds(525, 55, 44, VVFAM.getPreferredSize().height);

            //---- VVABC ----
            VVABC.setText("@VVABC@");
            VVABC.setName("VVABC");
            panel1.add(VVABC);
            VVABC.setBounds(525, 85, 24, VVABC.getPreferredSize().height);

            //---- VVCOF ----
            VVCOF.setText("@VVCOF@");
            VVCOF.setName("VVCOF");
            panel1.add(VVCOF);
            VVCOF.setBounds(525, 115, 24, VVCOF.getPreferredSize().height);

            //---- VVFRS ----
            VVFRS.setText("@VVFRS@");
            VVFRS.setName("VVFRS");
            panel1.add(VVFRS);
            VVFRS.setBounds(555, 115, 68, VVFRS.getPreferredSize().height);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("Pr\u00e9visions"));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- PQT01 ----
              PQT01.setText("@PQT01@");
              PQT01.setHorizontalAlignment(SwingConstants.RIGHT);
              PQT01.setName("PQT01");
              panel2.add(PQT01);
              PQT01.setBounds(10, 60, 69, PQT01.getPreferredSize().height);

              //---- PQT02 ----
              PQT02.setText("@PQT02@");
              PQT02.setHorizontalAlignment(SwingConstants.RIGHT);
              PQT02.setName("PQT02");
              panel2.add(PQT02);
              PQT02.setBounds(80, 60, 69, PQT02.getPreferredSize().height);

              //---- PQT03 ----
              PQT03.setText("@PQT03@");
              PQT03.setHorizontalAlignment(SwingConstants.RIGHT);
              PQT03.setName("PQT03");
              panel2.add(PQT03);
              PQT03.setBounds(150, 60, 69, PQT03.getPreferredSize().height);

              //---- PQT04 ----
              PQT04.setText("@PQT04@");
              PQT04.setHorizontalAlignment(SwingConstants.RIGHT);
              PQT04.setName("PQT04");
              panel2.add(PQT04);
              PQT04.setBounds(220, 60, 69, PQT04.getPreferredSize().height);

              //---- PQT05 ----
              PQT05.setText("@PQT05@");
              PQT05.setHorizontalAlignment(SwingConstants.RIGHT);
              PQT05.setName("PQT05");
              panel2.add(PQT05);
              PQT05.setBounds(290, 60, 69, PQT05.getPreferredSize().height);

              //---- PQT06 ----
              PQT06.setText("@PQT06@");
              PQT06.setHorizontalAlignment(SwingConstants.RIGHT);
              PQT06.setName("PQT06");
              panel2.add(PQT06);
              PQT06.setBounds(360, 60, 69, PQT06.getPreferredSize().height);

              //---- PQT07 ----
              PQT07.setText("@PQT07@");
              PQT07.setHorizontalAlignment(SwingConstants.RIGHT);
              PQT07.setName("PQT07");
              panel2.add(PQT07);
              PQT07.setBounds(430, 60, 69, PQT07.getPreferredSize().height);

              //---- PQT08 ----
              PQT08.setText("@PQT08@");
              PQT08.setHorizontalAlignment(SwingConstants.RIGHT);
              PQT08.setName("PQT08");
              panel2.add(PQT08);
              PQT08.setBounds(500, 60, 69, PQT08.getPreferredSize().height);

              //---- PQT09 ----
              PQT09.setText("@PQT09@");
              PQT09.setHorizontalAlignment(SwingConstants.RIGHT);
              PQT09.setName("PQT09");
              panel2.add(PQT09);
              PQT09.setBounds(570, 60, 69, PQT09.getPreferredSize().height);

              //---- PQT10 ----
              PQT10.setText("@PQT10@");
              PQT10.setHorizontalAlignment(SwingConstants.RIGHT);
              PQT10.setName("PQT10");
              panel2.add(PQT10);
              PQT10.setBounds(640, 60, 69, PQT10.getPreferredSize().height);

              //---- PQT11 ----
              PQT11.setText("@PQT11@");
              PQT11.setHorizontalAlignment(SwingConstants.RIGHT);
              PQT11.setName("PQT11");
              panel2.add(PQT11);
              PQT11.setBounds(710, 60, 69, PQT11.getPreferredSize().height);

              //---- PQT12 ----
              PQT12.setText("@PQT12@");
              PQT12.setHorizontalAlignment(SwingConstants.RIGHT);
              PQT12.setName("PQT12");
              panel2.add(PQT12);
              PQT12.setBounds(780, 60, 69, PQT12.getPreferredSize().height);

              //---- PQTTT ----
              PQTTT.setText("@PQTTT@");
              PQTTT.setHorizontalAlignment(SwingConstants.RIGHT);
              PQTTT.setName("PQTTT");
              panel2.add(PQTTT);
              PQTTT.setBounds(885, 60, 69, PQTTT.getPreferredSize().height);

              //---- label2 ----
              label2.setText("Total");
              label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
              label2.setHorizontalAlignment(SwingConstants.LEFT);
              label2.setName("label2");
              panel2.add(label2);
              label2.setBounds(885, 35, 70, 25);

              //---- label5 ----
              label5.setText("janvier");
              label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
              label5.setHorizontalAlignment(SwingConstants.LEFT);
              label5.setName("label5");
              panel2.add(label5);
              label5.setBounds(10, 35, 70, 25);

              //---- label6 ----
              label6.setText("f\u00e9vrier");
              label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
              label6.setHorizontalAlignment(SwingConstants.LEFT);
              label6.setName("label6");
              panel2.add(label6);
              label6.setBounds(80, 35, 70, 25);

              //---- label7 ----
              label7.setText("mars");
              label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD));
              label7.setHorizontalAlignment(SwingConstants.LEFT);
              label7.setName("label7");
              panel2.add(label7);
              label7.setBounds(150, 35, 70, 25);

              //---- label8 ----
              label8.setText("avril");
              label8.setFont(label8.getFont().deriveFont(label8.getFont().getStyle() | Font.BOLD));
              label8.setHorizontalAlignment(SwingConstants.LEFT);
              label8.setName("label8");
              panel2.add(label8);
              label8.setBounds(220, 35, 70, 25);

              //---- label9 ----
              label9.setText("mai");
              label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD));
              label9.setHorizontalAlignment(SwingConstants.LEFT);
              label9.setName("label9");
              panel2.add(label9);
              label9.setBounds(290, 35, 70, 25);

              //---- label10 ----
              label10.setText("juin");
              label10.setFont(label10.getFont().deriveFont(label10.getFont().getStyle() | Font.BOLD));
              label10.setHorizontalAlignment(SwingConstants.LEFT);
              label10.setName("label10");
              panel2.add(label10);
              label10.setBounds(360, 35, 70, 25);

              //---- label11 ----
              label11.setText("juillet");
              label11.setFont(label11.getFont().deriveFont(label11.getFont().getStyle() | Font.BOLD));
              label11.setHorizontalAlignment(SwingConstants.LEFT);
              label11.setName("label11");
              panel2.add(label11);
              label11.setBounds(430, 35, 70, 25);

              //---- label12 ----
              label12.setText("ao\u00fbt");
              label12.setFont(label12.getFont().deriveFont(label12.getFont().getStyle() | Font.BOLD));
              label12.setHorizontalAlignment(SwingConstants.LEFT);
              label12.setName("label12");
              panel2.add(label12);
              label12.setBounds(500, 35, 70, 25);

              //---- label13 ----
              label13.setText("septembre");
              label13.setFont(label13.getFont().deriveFont(label13.getFont().getStyle() | Font.BOLD));
              label13.setHorizontalAlignment(SwingConstants.LEFT);
              label13.setName("label13");
              panel2.add(label13);
              label13.setBounds(570, 35, 70, 25);

              //---- label14 ----
              label14.setText("octobre");
              label14.setFont(label14.getFont().deriveFont(label14.getFont().getStyle() | Font.BOLD));
              label14.setHorizontalAlignment(SwingConstants.LEFT);
              label14.setName("label14");
              panel2.add(label14);
              label14.setBounds(640, 35, 70, 25);

              //---- label15 ----
              label15.setText("novembre");
              label15.setFont(label15.getFont().deriveFont(label15.getFont().getStyle() | Font.BOLD));
              label15.setHorizontalAlignment(SwingConstants.LEFT);
              label15.setName("label15");
              panel2.add(label15);
              label15.setBounds(710, 35, 70, 25);

              //---- label16 ----
              label16.setText("d\u00e9cembre");
              label16.setFont(label16.getFont().deriveFont(label16.getFont().getStyle() | Font.BOLD));
              label16.setHorizontalAlignment(SwingConstants.LEFT);
              label16.setName("label16");
              panel2.add(label16);
              label16.setBounds(780, 35, 70, 25);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel2);
            panel2.setBounds(5, 170, 970, 110);

            //======== panel3 ========
            {
              panel3.setBorder(new TitledBorder("Ventes"));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- VQT01 ----
              VQT01.setText("@VQT01@");
              VQT01.setHorizontalAlignment(SwingConstants.RIGHT);
              VQT01.setName("VQT01");
              panel3.add(VQT01);
              VQT01.setBounds(10, 60, 69, VQT01.getPreferredSize().height);

              //---- VQT02 ----
              VQT02.setText("@VQT02@");
              VQT02.setHorizontalAlignment(SwingConstants.RIGHT);
              VQT02.setName("VQT02");
              panel3.add(VQT02);
              VQT02.setBounds(80, 60, 69, VQT02.getPreferredSize().height);

              //---- VQT03 ----
              VQT03.setText("@VQT03@");
              VQT03.setHorizontalAlignment(SwingConstants.RIGHT);
              VQT03.setName("VQT03");
              panel3.add(VQT03);
              VQT03.setBounds(150, 60, 69, VQT03.getPreferredSize().height);

              //---- VQT04 ----
              VQT04.setText("@VQT04@");
              VQT04.setHorizontalAlignment(SwingConstants.RIGHT);
              VQT04.setName("VQT04");
              panel3.add(VQT04);
              VQT04.setBounds(220, 60, 69, VQT04.getPreferredSize().height);

              //---- VQT05 ----
              VQT05.setText("@VQT05@");
              VQT05.setHorizontalAlignment(SwingConstants.RIGHT);
              VQT05.setName("VQT05");
              panel3.add(VQT05);
              VQT05.setBounds(290, 60, 69, VQT05.getPreferredSize().height);

              //---- VQT06 ----
              VQT06.setText("@VQT06@");
              VQT06.setHorizontalAlignment(SwingConstants.RIGHT);
              VQT06.setName("VQT06");
              panel3.add(VQT06);
              VQT06.setBounds(360, 60, 69, VQT06.getPreferredSize().height);

              //---- VQT07 ----
              VQT07.setText("@VQT07@");
              VQT07.setHorizontalAlignment(SwingConstants.RIGHT);
              VQT07.setName("VQT07");
              panel3.add(VQT07);
              VQT07.setBounds(430, 60, 69, VQT07.getPreferredSize().height);

              //---- VQT08 ----
              VQT08.setText("@VQT08@");
              VQT08.setHorizontalAlignment(SwingConstants.RIGHT);
              VQT08.setName("VQT08");
              panel3.add(VQT08);
              VQT08.setBounds(500, 60, 69, VQT08.getPreferredSize().height);

              //---- VQT09 ----
              VQT09.setText("@VQT09@");
              VQT09.setHorizontalAlignment(SwingConstants.RIGHT);
              VQT09.setName("VQT09");
              panel3.add(VQT09);
              VQT09.setBounds(570, 60, 69, VQT09.getPreferredSize().height);

              //---- VQT10 ----
              VQT10.setText("@VQT10@");
              VQT10.setHorizontalAlignment(SwingConstants.RIGHT);
              VQT10.setName("VQT10");
              panel3.add(VQT10);
              VQT10.setBounds(640, 60, 69, VQT10.getPreferredSize().height);

              //---- VQT11 ----
              VQT11.setText("@VQT11@");
              VQT11.setHorizontalAlignment(SwingConstants.RIGHT);
              VQT11.setName("VQT11");
              panel3.add(VQT11);
              VQT11.setBounds(710, 60, 69, VQT11.getPreferredSize().height);

              //---- VQT12 ----
              VQT12.setText("@VQT12@");
              VQT12.setHorizontalAlignment(SwingConstants.RIGHT);
              VQT12.setName("VQT12");
              panel3.add(VQT12);
              VQT12.setBounds(780, 60, 69, VQT12.getPreferredSize().height);

              //---- VQTTT ----
              VQTTT.setText("@VQTTT@");
              VQTTT.setHorizontalAlignment(SwingConstants.RIGHT);
              VQTTT.setName("VQTTT");
              panel3.add(VQTTT);
              VQTTT.setBounds(885, 60, 69, VQTTT.getPreferredSize().height);

              //---- label3 ----
              label3.setText("Total");
              label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
              label3.setHorizontalAlignment(SwingConstants.LEFT);
              label3.setName("label3");
              panel3.add(label3);
              label3.setBounds(885, 35, 70, 25);

              //---- label17 ----
              label17.setText("janvier");
              label17.setFont(label17.getFont().deriveFont(label17.getFont().getStyle() | Font.BOLD));
              label17.setHorizontalAlignment(SwingConstants.LEFT);
              label17.setName("label17");
              panel3.add(label17);
              label17.setBounds(10, 35, 70, 25);

              //---- label18 ----
              label18.setText("f\u00e9vrier");
              label18.setFont(label18.getFont().deriveFont(label18.getFont().getStyle() | Font.BOLD));
              label18.setHorizontalAlignment(SwingConstants.LEFT);
              label18.setName("label18");
              panel3.add(label18);
              label18.setBounds(80, 35, 70, 25);

              //---- label19 ----
              label19.setText("mars");
              label19.setFont(label19.getFont().deriveFont(label19.getFont().getStyle() | Font.BOLD));
              label19.setHorizontalAlignment(SwingConstants.LEFT);
              label19.setName("label19");
              panel3.add(label19);
              label19.setBounds(150, 35, 70, 25);

              //---- label20 ----
              label20.setText("avril");
              label20.setFont(label20.getFont().deriveFont(label20.getFont().getStyle() | Font.BOLD));
              label20.setHorizontalAlignment(SwingConstants.LEFT);
              label20.setName("label20");
              panel3.add(label20);
              label20.setBounds(220, 35, 70, 25);

              //---- label21 ----
              label21.setText("mai");
              label21.setFont(label21.getFont().deriveFont(label21.getFont().getStyle() | Font.BOLD));
              label21.setHorizontalAlignment(SwingConstants.LEFT);
              label21.setName("label21");
              panel3.add(label21);
              label21.setBounds(290, 35, 70, 25);

              //---- label22 ----
              label22.setText("juin");
              label22.setFont(label22.getFont().deriveFont(label22.getFont().getStyle() | Font.BOLD));
              label22.setHorizontalAlignment(SwingConstants.LEFT);
              label22.setName("label22");
              panel3.add(label22);
              label22.setBounds(360, 35, 70, 25);

              //---- label23 ----
              label23.setText("juillet");
              label23.setFont(label23.getFont().deriveFont(label23.getFont().getStyle() | Font.BOLD));
              label23.setHorizontalAlignment(SwingConstants.LEFT);
              label23.setName("label23");
              panel3.add(label23);
              label23.setBounds(430, 35, 70, 25);

              //---- label24 ----
              label24.setText("ao\u00fbt");
              label24.setFont(label24.getFont().deriveFont(label24.getFont().getStyle() | Font.BOLD));
              label24.setHorizontalAlignment(SwingConstants.LEFT);
              label24.setName("label24");
              panel3.add(label24);
              label24.setBounds(500, 35, 70, 25);

              //---- label25 ----
              label25.setText("septembre");
              label25.setFont(label25.getFont().deriveFont(label25.getFont().getStyle() | Font.BOLD));
              label25.setHorizontalAlignment(SwingConstants.LEFT);
              label25.setName("label25");
              panel3.add(label25);
              label25.setBounds(570, 35, 70, 25);

              //---- label26 ----
              label26.setText("octobre");
              label26.setFont(label26.getFont().deriveFont(label26.getFont().getStyle() | Font.BOLD));
              label26.setHorizontalAlignment(SwingConstants.LEFT);
              label26.setName("label26");
              panel3.add(label26);
              label26.setBounds(640, 35, 70, 25);

              //---- label27 ----
              label27.setText("novembre");
              label27.setFont(label27.getFont().deriveFont(label27.getFont().getStyle() | Font.BOLD));
              label27.setHorizontalAlignment(SwingConstants.LEFT);
              label27.setName("label27");
              panel3.add(label27);
              label27.setBounds(710, 35, 70, 25);

              //---- label28 ----
              label28.setText("d\u00e9cembre");
              label28.setFont(label28.getFont().deriveFont(label28.getFont().getStyle() | Font.BOLD));
              label28.setHorizontalAlignment(SwingConstants.LEFT);
              label28.setName("label28");
              panel3.add(label28);
              label28.setBounds(780, 35, 70, 25);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel3);
            panel3.setBounds(5, 290, 970, 110);

            //======== panel4 ========
            {
              panel4.setBorder(new TitledBorder("Ruptures"));
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);

              //---- RQT01 ----
              RQT01.setText("@RQT01@");
              RQT01.setHorizontalAlignment(SwingConstants.RIGHT);
              RQT01.setName("RQT01");
              panel4.add(RQT01);
              RQT01.setBounds(10, 60, 69, RQT01.getPreferredSize().height);

              //---- RQT02 ----
              RQT02.setText("@RQT02@");
              RQT02.setHorizontalAlignment(SwingConstants.RIGHT);
              RQT02.setName("RQT02");
              panel4.add(RQT02);
              RQT02.setBounds(80, 60, 69, RQT02.getPreferredSize().height);

              //---- RQT03 ----
              RQT03.setText("@RQT03@");
              RQT03.setHorizontalAlignment(SwingConstants.RIGHT);
              RQT03.setName("RQT03");
              panel4.add(RQT03);
              RQT03.setBounds(150, 60, 69, RQT03.getPreferredSize().height);

              //---- RQT04 ----
              RQT04.setText("@RQT04@");
              RQT04.setHorizontalAlignment(SwingConstants.RIGHT);
              RQT04.setName("RQT04");
              panel4.add(RQT04);
              RQT04.setBounds(220, 60, 69, RQT04.getPreferredSize().height);

              //---- RQT05 ----
              RQT05.setText("@RQT05@");
              RQT05.setHorizontalAlignment(SwingConstants.RIGHT);
              RQT05.setName("RQT05");
              panel4.add(RQT05);
              RQT05.setBounds(290, 60, 69, RQT05.getPreferredSize().height);

              //---- RQT06 ----
              RQT06.setText("@RQT06@");
              RQT06.setHorizontalAlignment(SwingConstants.RIGHT);
              RQT06.setName("RQT06");
              panel4.add(RQT06);
              RQT06.setBounds(360, 60, 69, RQT06.getPreferredSize().height);

              //---- RQT07 ----
              RQT07.setText("@RQT07@");
              RQT07.setHorizontalAlignment(SwingConstants.RIGHT);
              RQT07.setName("RQT07");
              panel4.add(RQT07);
              RQT07.setBounds(430, 60, 69, RQT07.getPreferredSize().height);

              //---- RQT08 ----
              RQT08.setText("@RQT08@");
              RQT08.setHorizontalAlignment(SwingConstants.RIGHT);
              RQT08.setName("RQT08");
              panel4.add(RQT08);
              RQT08.setBounds(500, 60, 69, RQT08.getPreferredSize().height);

              //---- RQT09 ----
              RQT09.setText("@RQT09@");
              RQT09.setHorizontalAlignment(SwingConstants.RIGHT);
              RQT09.setName("RQT09");
              panel4.add(RQT09);
              RQT09.setBounds(570, 60, 69, RQT09.getPreferredSize().height);

              //---- RQT10 ----
              RQT10.setText("@RQT10@");
              RQT10.setHorizontalAlignment(SwingConstants.RIGHT);
              RQT10.setName("RQT10");
              panel4.add(RQT10);
              RQT10.setBounds(640, 60, 69, RQT10.getPreferredSize().height);

              //---- v11 ----
              v11.setText("@RQT11@");
              v11.setHorizontalAlignment(SwingConstants.RIGHT);
              v11.setName("v11");
              panel4.add(v11);
              v11.setBounds(710, 60, 69, v11.getPreferredSize().height);

              //---- PQT28 ----
              PQT28.setText("@RQT12@");
              PQT28.setHorizontalAlignment(SwingConstants.RIGHT);
              PQT28.setName("PQT28");
              panel4.add(PQT28);
              PQT28.setBounds(780, 60, 69, PQT28.getPreferredSize().height);

              //---- RQTTT ----
              RQTTT.setText("@RQTTT@");
              RQTTT.setHorizontalAlignment(SwingConstants.RIGHT);
              RQTTT.setName("RQTTT");
              panel4.add(RQTTT);
              RQTTT.setBounds(885, 60, 69, RQTTT.getPreferredSize().height);

              //---- label4 ----
              label4.setText("Total");
              label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
              label4.setHorizontalAlignment(SwingConstants.LEFT);
              label4.setName("label4");
              panel4.add(label4);
              label4.setBounds(885, 35, 70, 25);

              //---- label29 ----
              label29.setText("janvier");
              label29.setFont(label29.getFont().deriveFont(label29.getFont().getStyle() | Font.BOLD));
              label29.setHorizontalAlignment(SwingConstants.LEFT);
              label29.setName("label29");
              panel4.add(label29);
              label29.setBounds(10, 35, 70, 25);

              //---- label30 ----
              label30.setText("f\u00e9vrier");
              label30.setFont(label30.getFont().deriveFont(label30.getFont().getStyle() | Font.BOLD));
              label30.setHorizontalAlignment(SwingConstants.LEFT);
              label30.setName("label30");
              panel4.add(label30);
              label30.setBounds(80, 35, 70, 25);

              //---- label31 ----
              label31.setText("mars");
              label31.setFont(label31.getFont().deriveFont(label31.getFont().getStyle() | Font.BOLD));
              label31.setHorizontalAlignment(SwingConstants.LEFT);
              label31.setName("label31");
              panel4.add(label31);
              label31.setBounds(150, 35, 70, 25);

              //---- label32 ----
              label32.setText("avril");
              label32.setFont(label32.getFont().deriveFont(label32.getFont().getStyle() | Font.BOLD));
              label32.setHorizontalAlignment(SwingConstants.LEFT);
              label32.setName("label32");
              panel4.add(label32);
              label32.setBounds(220, 35, 70, 25);

              //---- label33 ----
              label33.setText("mai");
              label33.setFont(label33.getFont().deriveFont(label33.getFont().getStyle() | Font.BOLD));
              label33.setHorizontalAlignment(SwingConstants.LEFT);
              label33.setName("label33");
              panel4.add(label33);
              label33.setBounds(290, 35, 70, 25);

              //---- label34 ----
              label34.setText("juin");
              label34.setFont(label34.getFont().deriveFont(label34.getFont().getStyle() | Font.BOLD));
              label34.setHorizontalAlignment(SwingConstants.LEFT);
              label34.setName("label34");
              panel4.add(label34);
              label34.setBounds(360, 35, 70, 25);

              //---- label35 ----
              label35.setText("juillet");
              label35.setFont(label35.getFont().deriveFont(label35.getFont().getStyle() | Font.BOLD));
              label35.setHorizontalAlignment(SwingConstants.LEFT);
              label35.setName("label35");
              panel4.add(label35);
              label35.setBounds(430, 35, 70, 25);

              //---- label36 ----
              label36.setText("ao\u00fbt");
              label36.setFont(label36.getFont().deriveFont(label36.getFont().getStyle() | Font.BOLD));
              label36.setHorizontalAlignment(SwingConstants.LEFT);
              label36.setName("label36");
              panel4.add(label36);
              label36.setBounds(500, 35, 70, 25);

              //---- label37 ----
              label37.setText("septembre");
              label37.setFont(label37.getFont().deriveFont(label37.getFont().getStyle() | Font.BOLD));
              label37.setHorizontalAlignment(SwingConstants.LEFT);
              label37.setName("label37");
              panel4.add(label37);
              label37.setBounds(570, 35, 70, 25);

              //---- label38 ----
              label38.setText("octobre");
              label38.setFont(label38.getFont().deriveFont(label38.getFont().getStyle() | Font.BOLD));
              label38.setHorizontalAlignment(SwingConstants.LEFT);
              label38.setName("label38");
              panel4.add(label38);
              label38.setBounds(640, 35, 70, 25);

              //---- label39 ----
              label39.setText("novembre");
              label39.setFont(label39.getFont().deriveFont(label39.getFont().getStyle() | Font.BOLD));
              label39.setHorizontalAlignment(SwingConstants.LEFT);
              label39.setName("label39");
              panel4.add(label39);
              label39.setBounds(710, 35, 70, 25);

              //---- label40 ----
              label40.setText("d\u00e9cembre");
              label40.setFont(label40.getFont().deriveFont(label40.getFont().getStyle() | Font.BOLD));
              label40.setHorizontalAlignment(SwingConstants.LEFT);
              label40.setName("label40");
              panel4.add(label40);
              label40.setBounds(780, 35, 70, 25);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel4.getComponentCount(); i++) {
                  Rectangle bounds = panel4.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel4.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel4.setMinimumSize(preferredSize);
                panel4.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel4);
            panel4.setBounds(0, 410, 970, 110);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 978, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 552, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_70;
  private RiZoneSortie VVETB;
  private JLabel OBJ_72;
  private RiZoneSortie VVART;
  private RiZoneSortie VVMAG;
  private JLabel OBJ_71;
  private JLabel OBJ_76;
  private RiZoneSortie VVAN;
  private JLabel OBJ_77;
  private RiZoneSortie VVCAT;
  private JPanel p_tete_droite;
  private JLabel label1;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_35;
  private RiZoneSortie MALIB;
  private RiZoneSortie VVLIB;
  private JLabel OBJ_36;
  private RiZoneSortie CCLIB;
  private JLabel OBJ_37;
  private JLabel OBJ_38;
  private RiZoneSortie VVCL1;
  private JLabel OBJ_39;
  private JLabel OBJ_40;
  private JLabel OBJ_41;
  private JLabel OBJ_54;
  private RiZoneSortie FRNOM;
  private RiZoneSortie FALIB;
  private RiZoneSortie GRLIB;
  private RiZoneSortie VVGRP;
  private RiZoneSortie VVFAM;
  private RiZoneSortie VVABC;
  private RiZoneSortie VVCOF;
  private RiZoneSortie VVFRS;
  private JPanel panel2;
  private RiZoneSortie PQT01;
  private RiZoneSortie PQT02;
  private RiZoneSortie PQT03;
  private RiZoneSortie PQT04;
  private RiZoneSortie PQT05;
  private RiZoneSortie PQT06;
  private RiZoneSortie PQT07;
  private RiZoneSortie PQT08;
  private RiZoneSortie PQT09;
  private RiZoneSortie PQT10;
  private RiZoneSortie PQT11;
  private RiZoneSortie PQT12;
  private RiZoneSortie PQTTT;
  private JLabel label2;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private JLabel label10;
  private JLabel label11;
  private JLabel label12;
  private JLabel label13;
  private JLabel label14;
  private JLabel label15;
  private JLabel label16;
  private JPanel panel3;
  private RiZoneSortie VQT01;
  private RiZoneSortie VQT02;
  private RiZoneSortie VQT03;
  private RiZoneSortie VQT04;
  private RiZoneSortie VQT05;
  private RiZoneSortie VQT06;
  private RiZoneSortie VQT07;
  private RiZoneSortie VQT08;
  private RiZoneSortie VQT09;
  private RiZoneSortie VQT10;
  private RiZoneSortie VQT11;
  private RiZoneSortie VQT12;
  private RiZoneSortie VQTTT;
  private JLabel label3;
  private JLabel label17;
  private JLabel label18;
  private JLabel label19;
  private JLabel label20;
  private JLabel label21;
  private JLabel label22;
  private JLabel label23;
  private JLabel label24;
  private JLabel label25;
  private JLabel label26;
  private JLabel label27;
  private JLabel label28;
  private JPanel panel4;
  private RiZoneSortie RQT01;
  private RiZoneSortie RQT02;
  private RiZoneSortie RQT03;
  private RiZoneSortie RQT04;
  private RiZoneSortie RQT05;
  private RiZoneSortie RQT06;
  private RiZoneSortie RQT07;
  private RiZoneSortie RQT08;
  private RiZoneSortie RQT09;
  private RiZoneSortie RQT10;
  private RiZoneSortie v11;
  private RiZoneSortie PQT28;
  private RiZoneSortie RQTTT;
  private JLabel label4;
  private JLabel label29;
  private JLabel label30;
  private JLabel label31;
  private JLabel label32;
  private JLabel label33;
  private JLabel label34;
  private JLabel label35;
  private JLabel label36;
  private JLabel label37;
  private JLabel label38;
  private JLabel label39;
  private JLabel label40;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
