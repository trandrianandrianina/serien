
package ri.serien.libecranrpg.vmrp.VMRP35FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VMRP35FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VMRP35FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    MVETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MVETB@")).trim());
    MVART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MVART@")).trim());
    MVMAG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MVMAG@")).trim());
    MVAN.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MVAN@")).trim());
    MVCAT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MVCAT@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    MALIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MALIB@")).trim());
    WLOTCD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLOTCD@")).trim());
    FRNOM2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    CCLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CCLIB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Titre
    setTitle("Gestion des prévisions");
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_70 = new JLabel();
    MVETB = new RiZoneSortie();
    OBJ_72 = new JLabel();
    MVART = new RiZoneSortie();
    MVMAG = new RiZoneSortie();
    OBJ_71 = new JLabel();
    OBJ_76 = new JLabel();
    MVAN = new RiZoneSortie();
    OBJ_77 = new JLabel();
    MVCAT = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_35 = new JLabel();
    OBJ_38 = new JLabel();
    MALIB = new RiZoneSortie();
    WLOTCD = new RiZoneSortie();
    FRNOM2 = new RiZoneSortie();
    OBJ_36 = new JLabel();
    CCLIB = new RiZoneSortie();
    OBJ_37 = new JLabel();
    OBJ_40 = new JLabel();
    WQTE = new XRiTextField();
    OBJ_41 = new JLabel();
    WVKR = new XRiTextField();
    MVQT01 = new XRiTextField();
    OBJ_42 = new JLabel();
    MVQT02 = new XRiTextField();
    OBJ_43 = new JLabel();
    MVQT03 = new XRiTextField();
    OBJ_44 = new JLabel();
    MVQT04 = new XRiTextField();
    MVQT05 = new XRiTextField();
    MVQT06 = new XRiTextField();
    MVQT07 = new XRiTextField();
    MVQT08 = new XRiTextField();
    MVQT09 = new XRiTextField();
    MVQT10 = new XRiTextField();
    MVQT11 = new XRiTextField();
    MVQT12 = new XRiTextField();
    OBJ_45 = new JLabel();
    OBJ_46 = new JLabel();
    OBJ_48 = new JLabel();
    OBJ_49 = new JLabel();
    OBJ_50 = new JLabel();
    OBJ_51 = new JLabel();
    OBJ_52 = new JLabel();
    OBJ_53 = new JLabel();
    OBJ_47 = new JLabel();
    label1 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion pr\u00e9visions MRP");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(750, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_70 ----
          OBJ_70.setText("Etablissement");
          OBJ_70.setName("OBJ_70");
          p_tete_gauche.add(OBJ_70);
          OBJ_70.setBounds(5, 1, 93, 28);

          //---- MVETB ----
          MVETB.setComponentPopupMenu(null);
          MVETB.setText("@MVETB@");
          MVETB.setOpaque(false);
          MVETB.setName("MVETB");
          p_tete_gauche.add(MVETB);
          MVETB.setBounds(100, 3, 40, MVETB.getPreferredSize().height);

          //---- OBJ_72 ----
          OBJ_72.setText("Article");
          OBJ_72.setName("OBJ_72");
          p_tete_gauche.add(OBJ_72);
          OBJ_72.setBounds(290, 1, 55, 28);

          //---- MVART ----
          MVART.setComponentPopupMenu(null);
          MVART.setText("@MVART@");
          MVART.setOpaque(false);
          MVART.setName("MVART");
          p_tete_gauche.add(MVART);
          MVART.setBounds(345, 3, 214, MVART.getPreferredSize().height);

          //---- MVMAG ----
          MVMAG.setComponentPopupMenu(BTD);
          MVMAG.setText("@MVMAG@");
          MVMAG.setOpaque(false);
          MVMAG.setName("MVMAG");
          p_tete_gauche.add(MVMAG);
          MVMAG.setBounds(230, 3, 34, MVMAG.getPreferredSize().height);

          //---- OBJ_71 ----
          OBJ_71.setText("Magasin");
          OBJ_71.setName("OBJ_71");
          p_tete_gauche.add(OBJ_71);
          OBJ_71.setBounds(165, 1, 65, 28);

          //---- OBJ_76 ----
          OBJ_76.setText("Ann\u00e9e");
          OBJ_76.setName("OBJ_76");
          p_tete_gauche.add(OBJ_76);
          OBJ_76.setBounds(580, 1, 45, 28);

          //---- MVAN ----
          MVAN.setComponentPopupMenu(BTD);
          MVAN.setOpaque(false);
          MVAN.setText("@MVAN@");
          MVAN.setName("MVAN");
          p_tete_gauche.add(MVAN);
          MVAN.setBounds(635, 3, 44, MVAN.getPreferredSize().height);

          //---- OBJ_77 ----
          OBJ_77.setText("Cat\u00e9gorie");
          OBJ_77.setName("OBJ_77");
          p_tete_gauche.add(OBJ_77);
          OBJ_77.setBounds(700, 1, 70, 28);

          //---- MVCAT ----
          MVCAT.setComponentPopupMenu(BTD);
          MVCAT.setOpaque(false);
          MVCAT.setText("@MVCAT@");
          MVCAT.setName("MVCAT");
          p_tete_gauche.add(MVCAT);
          MVCAT.setBounds(770, 3, 40, MVCAT.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("R\u00e9partitions");
              riSousMenu_bt14.setToolTipText("R\u00e9partitions");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(580, 570));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setPreferredSize(new Dimension(516, 570));
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_35 ----
            OBJ_35.setText("Magasin");
            OBJ_35.setName("OBJ_35");
            panel1.add(OBJ_35);
            OBJ_35.setBounds(30, 23, 150, 28);

            //---- OBJ_38 ----
            OBJ_38.setText("Lot de commande");
            OBJ_38.setName("OBJ_38");
            panel1.add(OBJ_38);
            OBJ_38.setBounds(30, 123, 150, 28);

            //---- MALIB ----
            MALIB.setText("@MALIB@");
            MALIB.setName("MALIB");
            panel1.add(MALIB);
            MALIB.setBounds(195, 25, 314, MALIB.getPreferredSize().height);

            //---- WLOTCD ----
            WLOTCD.setText("@WLOTCD@");
            WLOTCD.setHorizontalTextPosition(SwingConstants.RIGHT);
            WLOTCD.setName("WLOTCD");
            panel1.add(WLOTCD);
            WLOTCD.setBounds(195, 125, 100, WLOTCD.getPreferredSize().height);

            //---- FRNOM2 ----
            FRNOM2.setText("@A1LIB@");
            FRNOM2.setName("FRNOM2");
            panel1.add(FRNOM2);
            FRNOM2.setBounds(195, 55, 314, FRNOM2.getPreferredSize().height);

            //---- OBJ_36 ----
            OBJ_36.setText("Article");
            OBJ_36.setName("OBJ_36");
            panel1.add(OBJ_36);
            OBJ_36.setBounds(30, 53, 150, 28);

            //---- CCLIB ----
            CCLIB.setText("@CCLIB@");
            CCLIB.setName("CCLIB");
            panel1.add(CCLIB);
            CCLIB.setBounds(195, 85, 314, CCLIB.getPreferredSize().height);

            //---- OBJ_37 ----
            OBJ_37.setText("Cat\u00e9gorie");
            OBJ_37.setName("OBJ_37");
            panel1.add(OBJ_37);
            OBJ_37.setBounds(30, 83, 150, 28);

            //---- OBJ_40 ----
            OBJ_40.setText("Quantit\u00e9 globale");
            OBJ_40.setName("OBJ_40");
            panel1.add(OBJ_40);
            OBJ_40.setBounds(30, 155, 150, 28);

            //---- WQTE ----
            WQTE.setName("WQTE");
            panel1.add(WQTE);
            WQTE.setBounds(195, 155, 100, WQTE.getPreferredSize().height);

            //---- OBJ_41 ----
            OBJ_41.setText("Param\u00e8tre coefficient");
            OBJ_41.setName("OBJ_41");
            panel1.add(OBJ_41);
            OBJ_41.setBounds(330, 155, 140, 28);

            //---- WVKR ----
            WVKR.setComponentPopupMenu(BTD);
            WVKR.setName("WVKR");
            panel1.add(WVKR);
            WVKR.setBounds(470, 155, 40, WVKR.getPreferredSize().height);

            //---- MVQT01 ----
            MVQT01.setName("MVQT01");
            panel1.add(MVQT01);
            MVQT01.setBounds(195, 200, 100, MVQT01.getPreferredSize().height);

            //---- OBJ_42 ----
            OBJ_42.setText("Quantit\u00e9 mois 01");
            OBJ_42.setName("OBJ_42");
            panel1.add(OBJ_42);
            OBJ_42.setBounds(30, 202, 150, 24);

            //---- MVQT02 ----
            MVQT02.setName("MVQT02");
            panel1.add(MVQT02);
            MVQT02.setBounds(195, 226, 100, MVQT02.getPreferredSize().height);

            //---- OBJ_43 ----
            OBJ_43.setText("Quantit\u00e9 mois 02");
            OBJ_43.setName("OBJ_43");
            panel1.add(OBJ_43);
            OBJ_43.setBounds(30, 224, 150, 28);

            //---- MVQT03 ----
            MVQT03.setName("MVQT03");
            panel1.add(MVQT03);
            MVQT03.setBounds(195, 252, 100, MVQT03.getPreferredSize().height);

            //---- OBJ_44 ----
            OBJ_44.setText("Quantit\u00e9 mois 03");
            OBJ_44.setName("OBJ_44");
            panel1.add(OBJ_44);
            OBJ_44.setBounds(30, 250, 150, 28);

            //---- MVQT04 ----
            MVQT04.setName("MVQT04");
            panel1.add(MVQT04);
            MVQT04.setBounds(195, 278, 100, MVQT04.getPreferredSize().height);

            //---- MVQT05 ----
            MVQT05.setName("MVQT05");
            panel1.add(MVQT05);
            MVQT05.setBounds(195, 304, 100, MVQT05.getPreferredSize().height);

            //---- MVQT06 ----
            MVQT06.setName("MVQT06");
            panel1.add(MVQT06);
            MVQT06.setBounds(195, 330, 100, MVQT06.getPreferredSize().height);

            //---- MVQT07 ----
            MVQT07.setName("MVQT07");
            panel1.add(MVQT07);
            MVQT07.setBounds(195, 356, 100, MVQT07.getPreferredSize().height);

            //---- MVQT08 ----
            MVQT08.setName("MVQT08");
            panel1.add(MVQT08);
            MVQT08.setBounds(195, 382, 100, MVQT08.getPreferredSize().height);

            //---- MVQT09 ----
            MVQT09.setName("MVQT09");
            panel1.add(MVQT09);
            MVQT09.setBounds(195, 408, 100, MVQT09.getPreferredSize().height);

            //---- MVQT10 ----
            MVQT10.setName("MVQT10");
            panel1.add(MVQT10);
            MVQT10.setBounds(195, 434, 100, MVQT10.getPreferredSize().height);

            //---- MVQT11 ----
            MVQT11.setName("MVQT11");
            panel1.add(MVQT11);
            MVQT11.setBounds(195, 460, 100, MVQT11.getPreferredSize().height);

            //---- MVQT12 ----
            MVQT12.setName("MVQT12");
            panel1.add(MVQT12);
            MVQT12.setBounds(195, 486, 100, MVQT12.getPreferredSize().height);

            //---- OBJ_45 ----
            OBJ_45.setText("Quantit\u00e9 mois 04");
            OBJ_45.setName("OBJ_45");
            panel1.add(OBJ_45);
            OBJ_45.setBounds(30, 276, 150, 28);

            //---- OBJ_46 ----
            OBJ_46.setText("Quantit\u00e9 mois 12");
            OBJ_46.setName("OBJ_46");
            panel1.add(OBJ_46);
            OBJ_46.setBounds(30, 484, 150, 28);

            //---- OBJ_48 ----
            OBJ_48.setText("Quantit\u00e9 mois 06");
            OBJ_48.setName("OBJ_48");
            panel1.add(OBJ_48);
            OBJ_48.setBounds(30, 328, 150, 28);

            //---- OBJ_49 ----
            OBJ_49.setText("Quantit\u00e9 mois 07");
            OBJ_49.setName("OBJ_49");
            panel1.add(OBJ_49);
            OBJ_49.setBounds(30, 354, 150, 28);

            //---- OBJ_50 ----
            OBJ_50.setText("Quantit\u00e9 mois 08");
            OBJ_50.setName("OBJ_50");
            panel1.add(OBJ_50);
            OBJ_50.setBounds(30, 380, 150, 28);

            //---- OBJ_51 ----
            OBJ_51.setText("Quantit\u00e9 mois 09");
            OBJ_51.setName("OBJ_51");
            panel1.add(OBJ_51);
            OBJ_51.setBounds(30, 406, 150, 28);

            //---- OBJ_52 ----
            OBJ_52.setText("Quantit\u00e9 mois 11");
            OBJ_52.setName("OBJ_52");
            panel1.add(OBJ_52);
            OBJ_52.setBounds(30, 458, 150, 28);

            //---- OBJ_53 ----
            OBJ_53.setText("Quantit\u00e9 mois 10");
            OBJ_53.setName("OBJ_53");
            panel1.add(OBJ_53);
            OBJ_53.setBounds(30, 432, 150, 28);

            //---- OBJ_47 ----
            OBJ_47.setText("Quantit\u00e9 mois 05");
            OBJ_47.setName("OBJ_47");
            panel1.add(OBJ_47);
            OBJ_47.setBounds(30, 302, 150, 28);

            //---- label1 ----
            label1.setText("(quantit\u00e9 plan)");
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(305, 125, 105, 24);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 554, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 542, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_70;
  private RiZoneSortie MVETB;
  private JLabel OBJ_72;
  private RiZoneSortie MVART;
  private RiZoneSortie MVMAG;
  private JLabel OBJ_71;
  private JLabel OBJ_76;
  private RiZoneSortie MVAN;
  private JLabel OBJ_77;
  private RiZoneSortie MVCAT;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_35;
  private JLabel OBJ_38;
  private RiZoneSortie MALIB;
  private RiZoneSortie WLOTCD;
  private RiZoneSortie FRNOM2;
  private JLabel OBJ_36;
  private RiZoneSortie CCLIB;
  private JLabel OBJ_37;
  private JLabel OBJ_40;
  private XRiTextField WQTE;
  private JLabel OBJ_41;
  private XRiTextField WVKR;
  private XRiTextField MVQT01;
  private JLabel OBJ_42;
  private XRiTextField MVQT02;
  private JLabel OBJ_43;
  private XRiTextField MVQT03;
  private JLabel OBJ_44;
  private XRiTextField MVQT04;
  private XRiTextField MVQT05;
  private XRiTextField MVQT06;
  private XRiTextField MVQT07;
  private XRiTextField MVQT08;
  private XRiTextField MVQT09;
  private XRiTextField MVQT10;
  private XRiTextField MVQT11;
  private XRiTextField MVQT12;
  private JLabel OBJ_45;
  private JLabel OBJ_46;
  private JLabel OBJ_48;
  private JLabel OBJ_49;
  private JLabel OBJ_50;
  private JLabel OBJ_51;
  private JLabel OBJ_52;
  private JLabel OBJ_53;
  private JLabel OBJ_47;
  private JLabel label1;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
