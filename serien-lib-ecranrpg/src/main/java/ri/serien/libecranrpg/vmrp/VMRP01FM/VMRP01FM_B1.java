
package ri.serien.libecranrpg.vmrp.VMRP01FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VMRP01FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VMRP01FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDialog(true);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    per1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR01@")).trim());
    per2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR02@")).trim());
    per3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR03@")).trim());
    per4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR04@")).trim());
    per5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR05@")).trim());
    per6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR06@")).trim());
    per7.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR07@")).trim());
    per8.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR08@")).trim());
    per9.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR09@")).trim());
    per10.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR10@")).trim());
    per11.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR11@")).trim());
    per12.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR12@")).trim());
    per13.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR13@")).trim());
    per14.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR14@")).trim());
    per15.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR15@")).trim());
    per16.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR16@")).trim());
    per17.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR17@")).trim());
    per18.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR18@")).trim());
    per19.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR19@")).trim());
    per20.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR20@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGETB@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    per1.setVisible(!lexique.HostFieldGetData("ERR01").trim().equalsIgnoreCase(""));
    per2.setVisible(!lexique.HostFieldGetData("ERR02").trim().equalsIgnoreCase(""));
    per3.setVisible(!lexique.HostFieldGetData("ERR03").trim().equalsIgnoreCase(""));
    per4.setVisible(!lexique.HostFieldGetData("ERR04").trim().equalsIgnoreCase(""));
    per5.setVisible(!lexique.HostFieldGetData("ERR05").trim().equalsIgnoreCase(""));
    per6.setVisible(!lexique.HostFieldGetData("ERR06").trim().equalsIgnoreCase(""));
    per7.setVisible(!lexique.HostFieldGetData("ERR07").trim().equalsIgnoreCase(""));
    per8.setVisible(!lexique.HostFieldGetData("ERR08").trim().equalsIgnoreCase(""));
    per9.setVisible(!lexique.HostFieldGetData("ERR09").trim().equalsIgnoreCase(""));
    per10.setVisible(!lexique.HostFieldGetData("ERR10").trim().equalsIgnoreCase(""));
    per11.setVisible(!lexique.HostFieldGetData("ERR11").trim().equalsIgnoreCase(""));
    per12.setVisible(!lexique.HostFieldGetData("ERR12").trim().equalsIgnoreCase(""));
    per13.setVisible(!lexique.HostFieldGetData("ERR13").trim().equalsIgnoreCase(""));
    per14.setVisible(!lexique.HostFieldGetData("ERR14").trim().equalsIgnoreCase(""));
    per15.setVisible(!lexique.HostFieldGetData("ERR15").trim().equalsIgnoreCase(""));
    per16.setVisible(!lexique.HostFieldGetData("ERR16").trim().equalsIgnoreCase(""));
    per17.setVisible(!lexique.HostFieldGetData("ERR17").trim().equalsIgnoreCase(""));
    per18.setVisible(!lexique.HostFieldGetData("ERR18").trim().equalsIgnoreCase(""));
    per19.setVisible(!lexique.HostFieldGetData("ERR19").trim().equalsIgnoreCase(""));
    per20.setVisible(!lexique.HostFieldGetData("ERR20").trim().equalsIgnoreCase(""));
    
    // TODO Icones
    per1.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    per2.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    per3.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    per4.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    per5.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    per6.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    per7.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    per8.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    per9.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    per10.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    per11.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    per12.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    per13.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    per14.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    per15.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    per16.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    per17.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    per18.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    per19.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    per20.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    
    // Titre
    // setTitle(???);
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    D01 = new XRiTextField();
    P01 = new XRiTextField();
    D02 = new XRiTextField();
    P02 = new XRiTextField();
    D03 = new XRiTextField();
    P03 = new XRiTextField();
    D04 = new XRiTextField();
    P04 = new XRiTextField();
    D05 = new XRiTextField();
    P05 = new XRiTextField();
    D06 = new XRiTextField();
    P06 = new XRiTextField();
    D07 = new XRiTextField();
    P07 = new XRiTextField();
    D08 = new XRiTextField();
    P08 = new XRiTextField();
    D09 = new XRiTextField();
    P09 = new XRiTextField();
    D10 = new XRiTextField();
    P10 = new XRiTextField();
    D11 = new XRiTextField();
    P11 = new XRiTextField();
    D12 = new XRiTextField();
    P12 = new XRiTextField();
    D13 = new XRiTextField();
    P13 = new XRiTextField();
    D14 = new XRiTextField();
    P14 = new XRiTextField();
    D15 = new XRiTextField();
    P15 = new XRiTextField();
    D16 = new XRiTextField();
    P16 = new XRiTextField();
    D17 = new XRiTextField();
    P17 = new XRiTextField();
    D18 = new XRiTextField();
    P18 = new XRiTextField();
    D19 = new XRiTextField();
    P19 = new XRiTextField();
    D20 = new XRiTextField();
    P20 = new XRiTextField();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    label2 = new JLabel();
    label3 = new JLabel();
    per1 = new JLabel();
    per2 = new JLabel();
    per3 = new JLabel();
    per4 = new JLabel();
    per5 = new JLabel();
    per6 = new JLabel();
    per7 = new JLabel();
    per8 = new JLabel();
    per9 = new JLabel();
    per10 = new JLabel();
    per11 = new JLabel();
    per12 = new JLabel();
    per13 = new JLabel();
    per14 = new JLabel();
    per15 = new JLabel();
    per16 = new JLabel();
    per17 = new JLabel();
    per18 = new JLabel();
    per19 = new JLabel();
    per20 = new JLabel();
    barre_tete = new JMenuBar();
    panel1 = new JPanel();
    label1 = new JLabel();
    z_etablissement_ = new RiZoneSortie();
    z_dgnom_ = new RiZoneSortie();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(605, 630));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Co\u00e9fficients mensuels"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- D01 ----
          D01.setName("D01");
          panel2.add(D01);
          D01.setBounds(90, 50, 80, D01.getPreferredSize().height);

          //---- P01 ----
          P01.setName("P01");
          panel2.add(P01);
          P01.setBounds(185, 50, 62, P01.getPreferredSize().height);

          //---- D02 ----
          D02.setName("D02");
          panel2.add(D02);
          D02.setBounds(90, 75, 80, D02.getPreferredSize().height);

          //---- P02 ----
          P02.setName("P02");
          panel2.add(P02);
          P02.setBounds(185, 75, 62, P02.getPreferredSize().height);

          //---- D03 ----
          D03.setName("D03");
          panel2.add(D03);
          D03.setBounds(90, 100, 80, D03.getPreferredSize().height);

          //---- P03 ----
          P03.setName("P03");
          panel2.add(P03);
          P03.setBounds(185, 100, 62, P03.getPreferredSize().height);

          //---- D04 ----
          D04.setName("D04");
          panel2.add(D04);
          D04.setBounds(90, 125, 80, D04.getPreferredSize().height);

          //---- P04 ----
          P04.setName("P04");
          panel2.add(P04);
          P04.setBounds(185, 125, 62, P04.getPreferredSize().height);

          //---- D05 ----
          D05.setName("D05");
          panel2.add(D05);
          D05.setBounds(90, 150, 80, D05.getPreferredSize().height);

          //---- P05 ----
          P05.setName("P05");
          panel2.add(P05);
          P05.setBounds(185, 150, 62, P05.getPreferredSize().height);

          //---- D06 ----
          D06.setName("D06");
          panel2.add(D06);
          D06.setBounds(90, 175, 80, D06.getPreferredSize().height);

          //---- P06 ----
          P06.setName("P06");
          panel2.add(P06);
          P06.setBounds(185, 175, 62, P06.getPreferredSize().height);

          //---- D07 ----
          D07.setName("D07");
          panel2.add(D07);
          D07.setBounds(90, 200, 80, D07.getPreferredSize().height);

          //---- P07 ----
          P07.setName("P07");
          panel2.add(P07);
          P07.setBounds(185, 200, 62, P07.getPreferredSize().height);

          //---- D08 ----
          D08.setName("D08");
          panel2.add(D08);
          D08.setBounds(90, 225, 80, D08.getPreferredSize().height);

          //---- P08 ----
          P08.setName("P08");
          panel2.add(P08);
          P08.setBounds(185, 225, 62, P08.getPreferredSize().height);

          //---- D09 ----
          D09.setName("D09");
          panel2.add(D09);
          D09.setBounds(90, 250, 80, D09.getPreferredSize().height);

          //---- P09 ----
          P09.setName("P09");
          panel2.add(P09);
          P09.setBounds(185, 250, 62, P09.getPreferredSize().height);

          //---- D10 ----
          D10.setName("D10");
          panel2.add(D10);
          D10.setBounds(90, 275, 80, D10.getPreferredSize().height);

          //---- P10 ----
          P10.setName("P10");
          panel2.add(P10);
          P10.setBounds(185, 275, 62, P10.getPreferredSize().height);

          //---- D11 ----
          D11.setName("D11");
          panel2.add(D11);
          D11.setBounds(90, 300, 80, D11.getPreferredSize().height);

          //---- P11 ----
          P11.setName("P11");
          panel2.add(P11);
          P11.setBounds(185, 300, 62, P11.getPreferredSize().height);

          //---- D12 ----
          D12.setName("D12");
          panel2.add(D12);
          D12.setBounds(90, 325, 80, D12.getPreferredSize().height);

          //---- P12 ----
          P12.setName("P12");
          panel2.add(P12);
          P12.setBounds(185, 325, 62, P12.getPreferredSize().height);

          //---- D13 ----
          D13.setName("D13");
          panel2.add(D13);
          D13.setBounds(90, 350, 80, D13.getPreferredSize().height);

          //---- P13 ----
          P13.setName("P13");
          panel2.add(P13);
          P13.setBounds(185, 350, 62, P13.getPreferredSize().height);

          //---- D14 ----
          D14.setName("D14");
          panel2.add(D14);
          D14.setBounds(90, 375, 80, D14.getPreferredSize().height);

          //---- P14 ----
          P14.setName("P14");
          panel2.add(P14);
          P14.setBounds(185, 375, 62, P14.getPreferredSize().height);

          //---- D15 ----
          D15.setName("D15");
          panel2.add(D15);
          D15.setBounds(90, 400, 80, D15.getPreferredSize().height);

          //---- P15 ----
          P15.setName("P15");
          panel2.add(P15);
          P15.setBounds(185, 400, 62, P15.getPreferredSize().height);

          //---- D16 ----
          D16.setName("D16");
          panel2.add(D16);
          D16.setBounds(90, 425, 80, D16.getPreferredSize().height);

          //---- P16 ----
          P16.setName("P16");
          panel2.add(P16);
          P16.setBounds(185, 425, 62, P16.getPreferredSize().height);

          //---- D17 ----
          D17.setName("D17");
          panel2.add(D17);
          D17.setBounds(90, 450, 80, D17.getPreferredSize().height);

          //---- P17 ----
          P17.setName("P17");
          panel2.add(P17);
          P17.setBounds(185, 450, 62, P17.getPreferredSize().height);

          //---- D18 ----
          D18.setName("D18");
          panel2.add(D18);
          D18.setBounds(90, 475, 80, D18.getPreferredSize().height);

          //---- P18 ----
          P18.setName("P18");
          panel2.add(P18);
          P18.setBounds(185, 475, 62, P18.getPreferredSize().height);

          //---- D19 ----
          D19.setName("D19");
          panel2.add(D19);
          D19.setBounds(90, 500, 80, D19.getPreferredSize().height);

          //---- P19 ----
          P19.setName("P19");
          panel2.add(P19);
          P19.setBounds(185, 500, 62, P19.getPreferredSize().height);

          //---- D20 ----
          D20.setName("D20");
          panel2.add(D20);
          D20.setBounds(90, 525, 80, D20.getPreferredSize().height);

          //---- P20 ----
          P20.setName("P20");
          panel2.add(P20);
          P20.setBounds(185, 525, 62, P20.getPreferredSize().height);

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          panel2.add(BT_PGUP);
          BT_PGUP.setBounds(260, 50, 25, 228);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          panel2.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(260, 328, 25, 225);

          //---- label2 ----
          label2.setText("Date");
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
          label2.setName("label2");
          panel2.add(label2);
          label2.setBounds(90, 30, 80, 20);

          //---- label3 ----
          label3.setText("Coefficient");
          label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
          label3.setName("label3");
          panel2.add(label3);
          label3.setBounds(185, 30, 65, 20);

          //---- per1 ----
          per1.setToolTipText("@ERR01@");
          per1.setName("per1");
          panel2.add(per1);
          per1.setBounds(50, 50, 35, 28);

          //---- per2 ----
          per2.setToolTipText("@ERR02@");
          per2.setName("per2");
          panel2.add(per2);
          per2.setBounds(50, 75, 35, 28);

          //---- per3 ----
          per3.setToolTipText("@ERR03@");
          per3.setName("per3");
          panel2.add(per3);
          per3.setBounds(50, 100, 35, 28);

          //---- per4 ----
          per4.setToolTipText("@ERR04@");
          per4.setName("per4");
          panel2.add(per4);
          per4.setBounds(50, 125, 35, 28);

          //---- per5 ----
          per5.setToolTipText("@ERR05@");
          per5.setName("per5");
          panel2.add(per5);
          per5.setBounds(50, 150, 35, 28);

          //---- per6 ----
          per6.setToolTipText("@ERR06@");
          per6.setName("per6");
          panel2.add(per6);
          per6.setBounds(50, 175, 35, 28);

          //---- per7 ----
          per7.setToolTipText("@ERR07@");
          per7.setName("per7");
          panel2.add(per7);
          per7.setBounds(50, 200, 35, 28);

          //---- per8 ----
          per8.setToolTipText("@ERR08@");
          per8.setName("per8");
          panel2.add(per8);
          per8.setBounds(50, 225, 35, 28);

          //---- per9 ----
          per9.setToolTipText("@ERR09@");
          per9.setName("per9");
          panel2.add(per9);
          per9.setBounds(50, 250, 35, 28);

          //---- per10 ----
          per10.setToolTipText("@ERR10@");
          per10.setName("per10");
          panel2.add(per10);
          per10.setBounds(50, 275, 35, 28);

          //---- per11 ----
          per11.setToolTipText("@ERR11@");
          per11.setName("per11");
          panel2.add(per11);
          per11.setBounds(50, 300, 35, 28);

          //---- per12 ----
          per12.setToolTipText("@ERR12@");
          per12.setName("per12");
          panel2.add(per12);
          per12.setBounds(50, 325, 35, 28);

          //---- per13 ----
          per13.setToolTipText("@ERR13@");
          per13.setName("per13");
          panel2.add(per13);
          per13.setBounds(50, 350, 35, 28);

          //---- per14 ----
          per14.setToolTipText("@ERR14@");
          per14.setName("per14");
          panel2.add(per14);
          per14.setBounds(50, 375, 35, 28);

          //---- per15 ----
          per15.setToolTipText("@ERR15@");
          per15.setName("per15");
          panel2.add(per15);
          per15.setBounds(50, 400, 35, 28);

          //---- per16 ----
          per16.setToolTipText("@ERR16@");
          per16.setName("per16");
          panel2.add(per16);
          per16.setBounds(50, 425, 35, 28);

          //---- per17 ----
          per17.setToolTipText("@ERR17@");
          per17.setName("per17");
          panel2.add(per17);
          per17.setBounds(50, 450, 35, 28);

          //---- per18 ----
          per18.setToolTipText("@ERR18@");
          per18.setName("per18");
          panel2.add(per18);
          per18.setBounds(50, 475, 35, 28);

          //---- per19 ----
          per19.setToolTipText("@ERR19@");
          per19.setName("per19");
          panel2.add(per19);
          per19.setBounds(50, 500, 35, 28);

          //---- per20 ----
          per20.setToolTipText("@ERR20@");
          per20.setName("per20");
          panel2.add(per20);
          per20.setBounds(50, 525, 35, 28);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel2);
        panel2.setBounds(10, 10, 415, 580);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== barre_tete ========
    {
      barre_tete.setPreferredSize(new Dimension(111, 30));
      barre_tete.setName("barre_tete");

      //======== panel1 ========
      {
        panel1.setOpaque(false);
        panel1.setName("panel1");
        panel1.setLayout(null);

        //---- label1 ----
        label1.setText("Etablissement");
        label1.setName("label1");
        panel1.add(label1);
        label1.setBounds(5, 1, 100, 25);

        //---- z_etablissement_ ----
        z_etablissement_.setComponentPopupMenu(null);
        z_etablissement_.setText("@DGETB@");
        z_etablissement_.setOpaque(false);
        z_etablissement_.setName("z_etablissement_");
        panel1.add(z_etablissement_);
        z_etablissement_.setBounds(115, 1, 40, z_etablissement_.getPreferredSize().height);

        //---- z_dgnom_ ----
        z_dgnom_.setText("@DGNOM@");
        z_dgnom_.setOpaque(false);
        z_dgnom_.setName("z_dgnom_");
        panel1.add(z_dgnom_);
        z_dgnom_.setBounds(160, 1, 260, z_dgnom_.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel1.getComponentCount(); i++) {
            Rectangle bounds = panel1.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel1.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel1.setMinimumSize(preferredSize);
          panel1.setPreferredSize(preferredSize);
        }
      }
      barre_tete.add(panel1);
    }
    add(barre_tete, BorderLayout.NORTH);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiTextField D01;
  private XRiTextField P01;
  private XRiTextField D02;
  private XRiTextField P02;
  private XRiTextField D03;
  private XRiTextField P03;
  private XRiTextField D04;
  private XRiTextField P04;
  private XRiTextField D05;
  private XRiTextField P05;
  private XRiTextField D06;
  private XRiTextField P06;
  private XRiTextField D07;
  private XRiTextField P07;
  private XRiTextField D08;
  private XRiTextField P08;
  private XRiTextField D09;
  private XRiTextField P09;
  private XRiTextField D10;
  private XRiTextField P10;
  private XRiTextField D11;
  private XRiTextField P11;
  private XRiTextField D12;
  private XRiTextField P12;
  private XRiTextField D13;
  private XRiTextField P13;
  private XRiTextField D14;
  private XRiTextField P14;
  private XRiTextField D15;
  private XRiTextField P15;
  private XRiTextField D16;
  private XRiTextField P16;
  private XRiTextField D17;
  private XRiTextField P17;
  private XRiTextField D18;
  private XRiTextField P18;
  private XRiTextField D19;
  private XRiTextField P19;
  private XRiTextField D20;
  private XRiTextField P20;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JLabel label2;
  private JLabel label3;
  private JLabel per1;
  private JLabel per2;
  private JLabel per3;
  private JLabel per4;
  private JLabel per5;
  private JLabel per6;
  private JLabel per7;
  private JLabel per8;
  private JLabel per9;
  private JLabel per10;
  private JLabel per11;
  private JLabel per12;
  private JLabel per13;
  private JLabel per14;
  private JLabel per15;
  private JLabel per16;
  private JLabel per17;
  private JLabel per18;
  private JLabel per19;
  private JLabel per20;
  private JMenuBar barre_tete;
  private JPanel panel1;
  private JLabel label1;
  private RiZoneSortie z_etablissement_;
  private RiZoneSortie z_dgnom_;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
