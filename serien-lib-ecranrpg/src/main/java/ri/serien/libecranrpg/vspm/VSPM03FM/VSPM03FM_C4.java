
package ri.serien.libecranrpg.vspm.VSPM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;

/**
 * @author Stéphane Vénéri
 */
public class VSPM03FM_C4 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VSPM03FM_C4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    SCS11.setValeursSelection("1", " ");
    SCWEB.setValeursSelection("1", " ");
    SCSD1.setValeursSelection("1", " ");
    SCGNM.setValeursSelection("1", " ");
    SCS61.setValeursSelection("1", " ");
    SCS41.setValeursSelection("1", " ");
    SCS33.setValeursSelection("1", " ");
    SCS32.setValeursSelection("1", " ");
    SCS31.setValeursSelection("1", " ");
    SCGMM.setValeursSelection("1", " ");
    SCGPM.setValeursSelection("1", " ");
    SCGAM.setValeursSelection("1", " ");
    SCLTM.setValeursSelection("1", " ");
    SCPRM.setValeursSelection("1", " ");
    SCTRM.setValeursSelection("1", " ");
    SCGVM.setValeursSelection("1", " ");
    SCTDS.setValeursSelection("1", " ");
    SCGSM.setValeursSelection("1", " ");
    SCPAM.setValeursSelection("1", " ");
    SCGIM.setValeursSelection("1", " ");
    SCTIM.setValeursSelection("1", " ");
    SCGEM.setValeursSelection("1", " ");
    SCSD2.setValeursSelection("1", " ");
    SCGLF.setValeursSelection("1", " ");
    SCGTM.setValeursSelection("1", " ");
    SCTBM.setValeursSelection("1", " ");
    SCCGM.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    // SCSD1.setVisible( lexique.isPresent("SCSD1"));
    // SCSD1.setSelected(lexique.HostFieldGetData("SCSD1").equalsIgnoreCase("1"));
    // SCS11.setEnabled( lexique.isPresent("SCS11"));
    // SCS11.setSelected(lexique.HostFieldGetData("SCS11").equalsIgnoreCase("1"));
    // SCWEB.setEnabled( lexique.isPresent("SCWEB"));
    // SCWEB.setSelected(lexique.HostFieldGetData("SCWEB").equalsIgnoreCase("1"));
    // SCGNM.setEnabled( lexique.isPresent("SCGNM"));
    // SCGNM.setSelected(lexique.HostFieldGetData("SCGNM").equalsIgnoreCase("1"));
    // SCS61.setEnabled( lexique.isPresent("SCS61"));
    // SCS61.setSelected(lexique.HostFieldGetData("SCS61").equalsIgnoreCase("1"));
    // SCS41.setEnabled( lexique.isPresent("SCS41"));
    // SCS41.setSelected(lexique.HostFieldGetData("SCS41").equalsIgnoreCase("1"));
    // SCS33.setEnabled( lexique.isPresent("SCS33"));
    // SCS33.setSelected(lexique.HostFieldGetData("SCS33").equalsIgnoreCase("1"));
    // SCS32.setEnabled( lexique.isPresent("SCS32"));
    // SCS32.setSelected(lexique.HostFieldGetData("SCS32").equalsIgnoreCase("1"));
    // SCS31.setEnabled( lexique.isPresent("SCS31"));
    // SCS31.setSelected(lexique.HostFieldGetData("SCS31").equalsIgnoreCase("1"));
    // SCGMM.setEnabled( lexique.isPresent("SCGMM"));
    // SCGMM.setSelected(lexique.HostFieldGetData("SCGMM").equalsIgnoreCase("1"));
    // SCGPM.setEnabled( lexique.isPresent("SCGPM"));
    // SCGPM.setSelected(lexique.HostFieldGetData("SCGPM").equalsIgnoreCase("1"));
    // SCGAM.setEnabled( lexique.isPresent("SCGAM"));
    // SCGAM.setSelected(lexique.HostFieldGetData("SCGAM").equalsIgnoreCase("1"));
    // SCLTM.setEnabled( lexique.isPresent("SCLTM"));
    // SCLTM.setSelected(lexique.HostFieldGetData("SCLTM").equalsIgnoreCase("1"));
    // SCPRM.setEnabled( lexique.isPresent("SCPRM"));
    // SCPRM.setSelected(lexique.HostFieldGetData("SCPRM").equalsIgnoreCase("1"));
    // SCTRM.setEnabled( lexique.isPresent("SCTRM"));
    // SCTRM.setSelected(lexique.HostFieldGetData("SCTRM").equalsIgnoreCase("1"));
    // SCGVM.setEnabled( lexique.isPresent("SCGVM"));
    // SCGVM.setSelected(lexique.HostFieldGetData("SCGVM").equalsIgnoreCase("1"));
    // SCTDS.setEnabled( lexique.isPresent("SCTDS"));
    // SCTDS.setSelected(lexique.HostFieldGetData("SCTDS").equalsIgnoreCase("1"));
    // SCGSM.setEnabled( lexique.isPresent("SCGSM"));
    // SCGSM.setSelected(lexique.HostFieldGetData("SCGSM").equalsIgnoreCase("1"));
    // SCPAM.setEnabled( lexique.isPresent("SCPAM"));
    // SCPAM.setSelected(lexique.HostFieldGetData("SCPAM").equalsIgnoreCase("1"));
    // SCGIM.setEnabled( lexique.isPresent("SCGIM"));
    // SCGIM.setSelected(lexique.HostFieldGetData("SCGIM").equalsIgnoreCase("1"));
    // SCTIM.setEnabled( lexique.isPresent("SCTIM"));
    // SCTIM.setSelected(lexique.HostFieldGetData("SCTIM").equalsIgnoreCase("1"));
    // SCGEM.setEnabled( lexique.isPresent("SCGEM"));
    // SCGEM.setSelected(lexique.HostFieldGetData("SCGEM").equalsIgnoreCase("1"));
    // SCSD2.setEnabled( lexique.isPresent("SCSD2"));
    // SCSD2.setSelected(lexique.HostFieldGetData("SCSD2").equalsIgnoreCase("1"));
    // SCGLF.setEnabled( lexique.isPresent("SCGLF"));
    // SCGLF.setSelected(lexique.HostFieldGetData("SCGLF").equalsIgnoreCase("1"));
    // SCGTM.setEnabled( lexique.isPresent("SCGTM"));
    // SCGTM.setSelected(lexique.HostFieldGetData("SCGTM").equalsIgnoreCase("1"));
    // SCTBM.setEnabled( lexique.isPresent("SCTBM"));
    // SCTBM.setSelected(lexique.HostFieldGetData("SCTBM").equalsIgnoreCase("1"));
    // SCCGM.setEnabled( lexique.isPresent("SCCGM"));
    // SCCGM.setSelected(lexique.HostFieldGetData("SCCGM").equalsIgnoreCase("1"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Modules"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (SCSD1.isSelected())
    // lexique.HostFieldPutData("SCSD1", 0, "1");
    // else
    // lexique.HostFieldPutData("SCSD1", 0, " ");
    // if (SCS11.isSelected())
    // lexique.HostFieldPutData("SCS11", 0, "1");
    // else
    // lexique.HostFieldPutData("SCS11", 0, " ");
    // if (SCWEB.isSelected())
    // lexique.HostFieldPutData("SCWEB", 0, "1");
    // else
    // lexique.HostFieldPutData("SCWEB", 0, " ");
    // if (SCGNM.isSelected())
    // lexique.HostFieldPutData("SCGNM", 0, "1");
    // else
    // lexique.HostFieldPutData("SCGNM", 0, " ");
    // if (SCS61.isSelected())
    // lexique.HostFieldPutData("SCS61", 0, "1");
    // else
    // lexique.HostFieldPutData("SCS61", 0, " ");
    // if (SCS41.isSelected())
    // lexique.HostFieldPutData("SCS41", 0, "1");
    // else
    // lexique.HostFieldPutData("SCS41", 0, " ");
    // if (SCS33.isSelected())
    // lexique.HostFieldPutData("SCS33", 0, "1");
    // else
    // lexique.HostFieldPutData("SCS33", 0, " ");
    // if (SCS32.isSelected())
    // lexique.HostFieldPutData("SCS32", 0, "1");
    // else
    // lexique.HostFieldPutData("SCS32", 0, " ");
    // if (SCS31.isSelected())
    // lexique.HostFieldPutData("SCS31", 0, "1");
    // else
    // lexique.HostFieldPutData("SCS31", 0, " ");
    // if (SCGMM.isSelected())
    // lexique.HostFieldPutData("SCGMM", 0, "1");
    // else
    // lexique.HostFieldPutData("SCGMM", 0, " ");
    // if (SCGPM.isSelected())
    // lexique.HostFieldPutData("SCGPM", 0, "1");
    // else
    // lexique.HostFieldPutData("SCGPM", 0, " ");
    // if (SCGAM.isSelected())
    // lexique.HostFieldPutData("SCGAM", 0, "1");
    // else
    // lexique.HostFieldPutData("SCGAM", 0, " ");
    // if (SCLTM.isSelected())
    // lexique.HostFieldPutData("SCLTM", 0, "1");
    // else
    // lexique.HostFieldPutData("SCLTM", 0, " ");
    // if (SCPRM.isSelected())
    // lexique.HostFieldPutData("SCPRM", 0, "1");
    // else
    // lexique.HostFieldPutData("SCPRM", 0, " ");
    // if (SCTRM.isSelected())
    // lexique.HostFieldPutData("SCTRM", 0, "1");
    // else
    // lexique.HostFieldPutData("SCTRM", 0, " ");
    // if (SCGVM.isSelected())
    // lexique.HostFieldPutData("SCGVM", 0, "1");
    // else
    // lexique.HostFieldPutData("SCGVM", 0, " ");
    // if (SCTDS.isSelected())
    // lexique.HostFieldPutData("SCTDS", 0, "1");
    // else
    // lexique.HostFieldPutData("SCTDS", 0, " ");
    // if (SCGSM.isSelected())
    // lexique.HostFieldPutData("SCGSM", 0, "1");
    // else
    // lexique.HostFieldPutData("SCGSM", 0, " ");
    // if (SCPAM.isSelected())
    // lexique.HostFieldPutData("SCPAM", 0, "1");
    // else
    // lexique.HostFieldPutData("SCPAM", 0, " ");
    // if (SCGIM.isSelected())
    // lexique.HostFieldPutData("SCGIM", 0, "1");
    // else
    // lexique.HostFieldPutData("SCGIM", 0, " ");
    // if (SCTIM.isSelected())
    // lexique.HostFieldPutData("SCTIM", 0, "1");
    // else
    // lexique.HostFieldPutData("SCTIM", 0, " ");
    // if (SCGEM.isSelected())
    // lexique.HostFieldPutData("SCGEM", 0, "1");
    // else
    // lexique.HostFieldPutData("SCGEM", 0, " ");
    // if (SCSD2.isSelected())
    // lexique.HostFieldPutData("SCSD2", 0, "1");
    // else
    // lexique.HostFieldPutData("SCSD2", 0, " ");
    // if (SCGLF.isSelected())
    // lexique.HostFieldPutData("SCGLF", 0, "1");
    // else
    // lexique.HostFieldPutData("SCGLF", 0, " ");
    // if (SCGTM.isSelected())
    // lexique.HostFieldPutData("SCGTM", 0, "1");
    // else
    // lexique.HostFieldPutData("SCGTM", 0, " ");
    // if (SCTBM.isSelected())
    // lexique.HostFieldPutData("SCTBM", 0, "1");
    // else
    // lexique.HostFieldPutData("SCTBM", 0, " ");
    // if (SCCGM.isSelected())
    // lexique.HostFieldPutData("SCCGM", 0, "1");
    // else
    // lexique.HostFieldPutData("SCCGM", 0, " ");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    SCCGM = new XRiCheckBox();
    SCTBM = new XRiCheckBox();
    SCGTM = new XRiCheckBox();
    SCGLF = new XRiCheckBox();
    SCSD2 = new XRiCheckBox();
    SCGEM = new XRiCheckBox();
    SCTIM = new XRiCheckBox();
    SCGIM = new XRiCheckBox();
    panel2 = new JPanel();
    SCPAM = new XRiCheckBox();
    SCGSM = new XRiCheckBox();
    SCTDS = new XRiCheckBox();
    panel3 = new JPanel();
    SCGVM = new XRiCheckBox();
    SCTRM = new XRiCheckBox();
    SCPRM = new XRiCheckBox();
    SCLTM = new XRiCheckBox();
    SCGAM = new XRiCheckBox();
    SCGPM = new XRiCheckBox();
    SCGMM = new XRiCheckBox();
    SCS31 = new XRiCheckBox();
    SCS32 = new XRiCheckBox();
    SCS33 = new XRiCheckBox();
    SCS41 = new XRiCheckBox();
    SCS61 = new XRiCheckBox();
    SCGNM = new XRiCheckBox();
    SCSD1 = new XRiCheckBox();
    panel4 = new JPanel();
    SCWEB = new XRiCheckBox();
    panel5 = new JPanel();
    SCS11 = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 450));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Comptabilit\u00e9/finance"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- SCCGM ----
          SCCGM.setText("CGM");
          SCCGM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCCGM.setName("SCCGM");
          panel1.add(SCCGM);
          SCCGM.setBounds(20, 35, 66, 20);

          //---- SCTBM ----
          SCTBM.setText("TBM");
          SCTBM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCTBM.setName("SCTBM");
          panel1.add(SCTBM);
          SCTBM.setBounds(100, 35, 66, 20);

          //---- SCGTM ----
          SCGTM.setText("GTM");
          SCGTM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCGTM.setName("SCGTM");
          panel1.add(SCGTM);
          SCGTM.setBounds(185, 35, 66, 20);

          //---- SCGLF ----
          SCGLF.setText("GLF");
          SCGLF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCGLF.setName("SCGLF");
          panel1.add(SCGLF);
          SCGLF.setBounds(265, 35, 66, 20);

          //---- SCSD2 ----
          SCSD2.setText("SD2");
          SCSD2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCSD2.setName("SCSD2");
          panel1.add(SCSD2);
          SCSD2.setBounds(430, 35, 66, 20);

          //---- SCGEM ----
          SCGEM.setText("GEM");
          SCGEM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCGEM.setName("SCGEM");
          panel1.add(SCGEM);
          SCGEM.setBounds(20, 65, 66, 20);

          //---- SCTIM ----
          SCTIM.setText("TIM");
          SCTIM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCTIM.setName("SCTIM");
          panel1.add(SCTIM);
          SCTIM.setBounds(100, 65, 66, 20);

          //---- SCGIM ----
          SCGIM.setText("GIM");
          SCGIM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCGIM.setName("SCGIM");
          panel1.add(SCGIM);
          SCGIM.setBounds(20, 95, 66, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 610, 135);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Paie et ressources humaines"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- SCPAM ----
          SCPAM.setText("PAM");
          SCPAM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCPAM.setName("SCPAM");
          panel2.add(SCPAM);
          SCPAM.setBounds(20, 35, 66, 20);

          //---- SCGSM ----
          SCGSM.setText("GSM");
          SCGSM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCGSM.setName("SCGSM");
          panel2.add(SCGSM);
          SCGSM.setBounds(100, 35, 66, 20);

          //---- SCTDS ----
          SCTDS.setText("TDS");
          SCTDS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCTDS.setName("SCTDS");
          panel2.add(SCTDS);
          SCTDS.setBounds(185, 35, 66, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel2);
        panel2.setBounds(10, 150, 610, 85);

        //======== panel3 ========
        {
          panel3.setBorder(new TitledBorder("Gestion commerciale"));
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);

          //---- SCGVM ----
          SCGVM.setText("GVM");
          SCGVM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCGVM.setName("SCGVM");
          panel3.add(SCGVM);
          SCGVM.setBounds(20, 30, 66, 20);

          //---- SCTRM ----
          SCTRM.setText("TRM");
          SCTRM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCTRM.setName("SCTRM");
          panel3.add(SCTRM);
          SCTRM.setBounds(100, 30, 66, 20);

          //---- SCPRM ----
          SCPRM.setText("PRM");
          SCPRM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCPRM.setName("SCPRM");
          panel3.add(SCPRM);
          SCPRM.setBounds(185, 30, 66, 20);

          //---- SCLTM ----
          SCLTM.setText("LTM");
          SCLTM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCLTM.setName("SCLTM");
          panel3.add(SCLTM);
          SCLTM.setBounds(265, 30, 66, 20);

          //---- SCGAM ----
          SCGAM.setText("GAM");
          SCGAM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCGAM.setName("SCGAM");
          panel3.add(SCGAM);
          SCGAM.setBounds(350, 30, 66, 20);

          //---- SCGPM ----
          SCGPM.setText("GPM");
          SCGPM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCGPM.setName("SCGPM");
          panel3.add(SCGPM);
          SCGPM.setBounds(430, 30, 66, 20);

          //---- SCGMM ----
          SCGMM.setText("GMM");
          SCGMM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCGMM.setName("SCGMM");
          panel3.add(SCGMM);
          SCGMM.setBounds(515, 30, 66, 20);

          //---- SCS31 ----
          SCS31.setText("S31");
          SCS31.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCS31.setName("SCS31");
          panel3.add(SCS31);
          SCS31.setBounds(20, 65, 66, 20);

          //---- SCS32 ----
          SCS32.setText("S32");
          SCS32.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCS32.setName("SCS32");
          panel3.add(SCS32);
          SCS32.setBounds(100, 65, 66, 20);

          //---- SCS33 ----
          SCS33.setText("S33");
          SCS33.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCS33.setName("SCS33");
          panel3.add(SCS33);
          SCS33.setBounds(185, 65, 66, 20);

          //---- SCS41 ----
          SCS41.setText("S41");
          SCS41.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCS41.setName("SCS41");
          panel3.add(SCS41);
          SCS41.setBounds(265, 65, 66, 20);

          //---- SCS61 ----
          SCS61.setText("S61");
          SCS61.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCS61.setName("SCS61");
          panel3.add(SCS61);
          SCS61.setBounds(350, 65, 66, 20);

          //---- SCGNM ----
          SCGNM.setText("GNM");
          SCGNM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCGNM.setName("SCGNM");
          panel3.add(SCGNM);
          SCGNM.setBounds(430, 65, 66, 20);

          //---- SCSD1 ----
          SCSD1.setText("DAM");
          SCSD1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCSD1.setName("SCSD1");
          panel3.add(SCSD1);
          SCSD1.setBounds(515, 65, 66, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel3.getComponentCount(); i++) {
              Rectangle bounds = panel3.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel3.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel3.setMinimumSize(preferredSize);
            panel3.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel3);
        panel3.setBounds(10, 240, 610, 115);

        //======== panel4 ========
        {
          panel4.setBorder(new TitledBorder("Pack internet"));
          panel4.setOpaque(false);
          panel4.setName("panel4");
          panel4.setLayout(null);

          //---- SCWEB ----
          SCWEB.setText("WEB");
          SCWEB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCWEB.setName("SCWEB");
          panel4.add(SCWEB);
          SCWEB.setBounds(20, 35, 66, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel4.getComponentCount(); i++) {
              Rectangle bounds = panel4.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel4.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel4.setMinimumSize(preferredSize);
            panel4.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel4);
        panel4.setBounds(10, 360, 295, 80);

        //======== panel5 ========
        {
          panel5.setBorder(new TitledBorder("Impression"));
          panel5.setOpaque(false);
          panel5.setName("panel5");
          panel5.setLayout(null);

          //---- SCS11 ----
          SCS11.setText("SIM");
          SCS11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCS11.setName("SCS11");
          panel5.add(SCS11);
          SCS11.setBounds(20, 35, 66, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel5.getComponentCount(); i++) {
              Rectangle bounds = panel5.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel5.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel5.setMinimumSize(preferredSize);
            panel5.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel5);
        panel5.setBounds(320, 360, 300, 80);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiCheckBox SCCGM;
  private XRiCheckBox SCTBM;
  private XRiCheckBox SCGTM;
  private XRiCheckBox SCGLF;
  private XRiCheckBox SCSD2;
  private XRiCheckBox SCGEM;
  private XRiCheckBox SCTIM;
  private XRiCheckBox SCGIM;
  private JPanel panel2;
  private XRiCheckBox SCPAM;
  private XRiCheckBox SCGSM;
  private XRiCheckBox SCTDS;
  private JPanel panel3;
  private XRiCheckBox SCGVM;
  private XRiCheckBox SCTRM;
  private XRiCheckBox SCPRM;
  private XRiCheckBox SCLTM;
  private XRiCheckBox SCGAM;
  private XRiCheckBox SCGPM;
  private XRiCheckBox SCGMM;
  private XRiCheckBox SCS31;
  private XRiCheckBox SCS32;
  private XRiCheckBox SCS33;
  private XRiCheckBox SCS41;
  private XRiCheckBox SCS61;
  private XRiCheckBox SCGNM;
  private XRiCheckBox SCSD1;
  private JPanel panel4;
  private XRiCheckBox SCWEB;
  private JPanel panel5;
  private XRiCheckBox SCS11;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
