
package ri.serien.libecranrpg.vspm.VSPM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

public class VSPM03FM_C6 extends SNPanelEcranRPG implements ioFrame {
  
   
  // TODO declarations classe spécifiques...
  
  public VSPM03FM_C6(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // TODO constructeur spécifiques...
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // TODO setData spécifiques...
    
    

    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // TODO getDATA spécifiques
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    SCTCP = new XRiTextField();
    OBJ_21 = new JLabel();
    P_PnlOpts = new JPanel();
    OBJ_50 = new JLabel();
    OBJ_19 = new JLabel();
    SCSYST = new XRiTextField();
    OBJ_42 = new JLabel();
    OBJ_17 = new JLabel();
    SCRSC = new XRiTextField();
    OBJ_38 = new JLabel();
    OBJ_44 = new JLabel();
    panel3 = new JPanel();
    OB4 = new XRiTextField();
    OB5 = new XRiTextField();
    panel2 = new JPanel();
    OB1 = new XRiTextField();
    OB2 = new XRiTextField();
    OB3 = new XRiTextField();
    panel4 = new JPanel();
    SCNCNX = new XRiTextField();
    SCNLMO = new XRiTextField();
    OBJ_35 = new JLabel();
    SCIDLO = new XRiTextField();
    SCLIEU = new XRiTextField();
    SCMODE = new XRiTextField();
    OBJ_25 = new JLabel();
    OBJ_27 = new JLabel();
    OBJ_31 = new JLabel();
    OBJ_33 = new JLabel();
    OBJ_29 = new JLabel();
    SCTMOD = new XRiTextField();
    SCUSER = new XRiTextField();
    SCPASS = new XRiTextField();
    SCTELN = new XRiTextField();
    SCFTP = new XRiTextField();
    SCPORF = new XRiTextField();
    OBJ_46 = new JLabel();
    OBJ_40 = new JLabel();
    SCPORT = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(875, 555));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Configuration modem");
            riSousMenu_bt6.setToolTipText("Configuration du modem interne");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Connexions"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- SCTCP ----
          SCTCP.setComponentPopupMenu(BTD);
          SCTCP.setFont(SCTCP.getFont().deriveFont(SCTCP.getFont().getStyle() | Font.BOLD));
          SCTCP.setName("SCTCP");
          panel1.add(SCTCP);
          SCTCP.setBounds(505, 25, 160, SCTCP.getPreferredSize().height);

          //---- OBJ_21 ----
          OBJ_21.setText("Adresse TCP/IP");
          OBJ_21.setFont(OBJ_21.getFont().deriveFont(OBJ_21.getFont().getStyle() | Font.BOLD));
          OBJ_21.setName("OBJ_21");
          panel1.add(OBJ_21);
          OBJ_21.setBounds(400, 29, 105, 20);

          //======== P_PnlOpts ========
          {
            P_PnlOpts.setName("P_PnlOpts");
            P_PnlOpts.setLayout(null);
          }
          panel1.add(P_PnlOpts);
          P_PnlOpts.setBounds(1036, 15, 55, 516);

          //---- OBJ_50 ----
          OBJ_50.setText("Mot Passe");
          OBJ_50.setFont(OBJ_50.getFont().deriveFont(OBJ_50.getFont().getStyle() | Font.BOLD));
          OBJ_50.setName("OBJ_50");
          panel1.add(OBJ_50);
          OBJ_50.setBounds(225, 234, 85, 20);

          //---- OBJ_19 ----
          OBJ_19.setText("Ressource");
          OBJ_19.setName("OBJ_19");
          panel1.add(OBJ_19);
          OBJ_19.setBounds(210, 29, 78, 20);

          //---- SCSYST ----
          SCSYST.setComponentPopupMenu(BTD);
          SCSYST.setName("SCSYST");
          panel1.add(SCSYST);
          SCSYST.setBounds(85, 25, 90, SCSYST.getPreferredSize().height);

          //---- OBJ_42 ----
          OBJ_42.setText("Utilisateur");
          OBJ_42.setFont(OBJ_42.getFont().deriveFont(OBJ_42.getFont().getStyle() | Font.BOLD));
          OBJ_42.setName("OBJ_42");
          panel1.add(OBJ_42);
          OBJ_42.setBounds(20, 234, 62, 20);

          //---- OBJ_17 ----
          OBJ_17.setText("Syst\u00e8me");
          OBJ_17.setName("OBJ_17");
          panel1.add(OBJ_17);
          OBJ_17.setBounds(15, 29, 57, 20);

          //---- SCRSC ----
          SCRSC.setComponentPopupMenu(BTD);
          SCRSC.setName("SCRSC");
          panel1.add(SCRSC);
          SCRSC.setBounds(290, 25, 90, SCRSC.getPreferredSize().height);

          //---- OBJ_38 ----
          OBJ_38.setText("TELNET");
          OBJ_38.setName("OBJ_38");
          panel1.add(OBJ_38);
          OBJ_38.setBounds(20, 174, 55, 20);

          //---- OBJ_44 ----
          OBJ_44.setText("FTP");
          OBJ_44.setName("OBJ_44");
          panel1.add(OBJ_44);
          OBJ_44.setBounds(20, 204, 47, 20);

          //======== panel3 ========
          {
            panel3.setBackground(new Color(214, 217, 223));
            panel3.setBorder(new TitledBorder("Observations R\u00e9solution Informatique"));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- OB4 ----
            OB4.setName("OB4");
            panel3.add(OB4);
            OB4.setBounds(15, 35, 610, OB4.getPreferredSize().height);

            //---- OB5 ----
            OB5.setName("OB5");
            panel3.add(OB5);
            OB5.setBounds(15, 60, 610, OB5.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel3);
          panel3.setBounds(20, 410, 645, 105);

          //======== panel2 ========
          {
            panel2.setBackground(new Color(214, 217, 223));
            panel2.setBorder(new TitledBorder("Observations client"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OB1 ----
            OB1.setName("OB1");
            panel2.add(OB1);
            OB1.setBounds(20, 35, 610, OB1.getPreferredSize().height);

            //---- OB2 ----
            OB2.setName("OB2");
            panel2.add(OB2);
            OB2.setBounds(20, 60, 610, OB2.getPreferredSize().height);

            //---- OB3 ----
            OB3.setName("OB3");
            panel2.add(OB3);
            OB3.setBounds(20, 85, 610, OB3.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel2);
          panel2.setBounds(20, 270, 645, 135);

          //======== panel4 ========
          {
            panel4.setBorder(new TitledBorder("Attributs"));
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- SCNCNX ----
            SCNCNX.setComponentPopupMenu(BTD);
            SCNCNX.setName("SCNCNX");
            panel4.add(SCNCNX);
            SCNCNX.setBounds(485, 60, 150, SCNCNX.getPreferredSize().height);

            //---- SCNLMO ----
            SCNLMO.setComponentPopupMenu(BTD);
            SCNLMO.setName("SCNLMO");
            panel4.add(SCNLMO);
            SCNLMO.setBounds(245, 60, 145, SCNLMO.getPreferredSize().height);

            //---- OBJ_35 ----
            OBJ_35.setText("Connexion");
            OBJ_35.setName("OBJ_35");
            panel4.add(OBJ_35);
            OBJ_35.setBounds(405, 64, 66, 20);

            //---- SCIDLO ----
            SCIDLO.setComponentPopupMenu(BTD);
            SCIDLO.setName("SCIDLO");
            panel4.add(SCIDLO);
            SCIDLO.setBounds(70, 30, 74, SCIDLO.getPreferredSize().height);

            //---- SCLIEU ----
            SCLIEU.setComponentPopupMenu(BTD);
            SCLIEU.setName("SCLIEU");
            panel4.add(SCLIEU);
            SCLIEU.setBounds(245, 30, 74, SCLIEU.getPreferredSize().height);

            //---- SCMODE ----
            SCMODE.setComponentPopupMenu(BTD);
            SCMODE.setName("SCMODE");
            panel4.add(SCMODE);
            SCMODE.setBounds(485, 30, 74, SCMODE.getPreferredSize().height);

            //---- OBJ_25 ----
            OBJ_25.setText("ID local");
            OBJ_25.setName("OBJ_25");
            panel4.add(OBJ_25);
            OBJ_25.setBounds(10, 34, 60, 20);

            //---- OBJ_27 ----
            OBJ_27.setText("Lieu");
            OBJ_27.setName("OBJ_27");
            panel4.add(OBJ_27);
            OBJ_27.setBounds(175, 34, 65, 20);

            //---- OBJ_31 ----
            OBJ_31.setText("Modem");
            OBJ_31.setName("OBJ_31");
            panel4.add(OBJ_31);
            OBJ_31.setBounds(10, 64, 60, 20);

            //---- OBJ_33 ----
            OBJ_33.setText("N\u00b0 ligne");
            OBJ_33.setName("OBJ_33");
            panel4.add(OBJ_33);
            OBJ_33.setBounds(175, 64, 60, 20);

            //---- OBJ_29 ----
            OBJ_29.setText("Mode");
            OBJ_29.setName("OBJ_29");
            panel4.add(OBJ_29);
            OBJ_29.setBounds(405, 34, 39, 20);

            //---- SCTMOD ----
            SCTMOD.setComponentPopupMenu(BTD);
            SCTMOD.setName("SCTMOD");
            panel4.add(SCTMOD);
            SCTMOD.setBounds(70, 60, 50, SCTMOD.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel4);
          panel4.setBounds(20, 55, 645, 110);

          //---- SCUSER ----
          SCUSER.setComponentPopupMenu(BTD);
          SCUSER.setFont(SCUSER.getFont().deriveFont(SCUSER.getFont().getStyle() | Font.BOLD));
          SCUSER.setName("SCUSER");
          panel1.add(SCUSER);
          SCUSER.setBounds(95, 230, 110, SCUSER.getPreferredSize().height);

          //---- SCPASS ----
          SCPASS.setComponentPopupMenu(BTD);
          SCPASS.setFont(SCPASS.getFont().deriveFont(SCPASS.getFont().getStyle() | Font.BOLD));
          SCPASS.setName("SCPASS");
          panel1.add(SCPASS);
          SCPASS.setBounds(310, 230, 110, SCPASS.getPreferredSize().height);

          //---- SCTELN ----
          SCTELN.setComponentPopupMenu(BTD);
          SCTELN.setName("SCTELN");
          panel1.add(SCTELN);
          SCTELN.setBounds(95, 170, 440, SCTELN.getPreferredSize().height);

          //---- SCFTP ----
          SCFTP.setComponentPopupMenu(BTD);
          SCFTP.setName("SCFTP");
          panel1.add(SCFTP);
          SCFTP.setBounds(95, 200, 440, SCFTP.getPreferredSize().height);

          //---- SCPORF ----
          SCPORF.setComponentPopupMenu(BTD);
          SCPORF.setName("SCPORF");
          panel1.add(SCPORF);
          SCPORF.setBounds(590, 200, 60, SCPORF.getPreferredSize().height);

          //---- OBJ_46 ----
          OBJ_46.setText("Port");
          OBJ_46.setName("OBJ_46");
          panel1.add(OBJ_46);
          OBJ_46.setBounds(555, 204, 30, 20);

          //---- OBJ_40 ----
          OBJ_40.setText("Port");
          OBJ_40.setName("OBJ_40");
          panel1.add(OBJ_40);
          OBJ_40.setBounds(555, 174, 30, 20);

          //---- SCPORT ----
          SCPORT.setComponentPopupMenu(BTD);
          SCPORT.setName("SCPORT");
          panel1.add(SCPORT);
          SCPORT.setBounds(590, 170, 60, SCPORT.getPreferredSize().height);
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 685, 535);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField SCTCP;
  private JLabel OBJ_21;
  private JPanel P_PnlOpts;
  private JLabel OBJ_50;
  private JLabel OBJ_19;
  private XRiTextField SCSYST;
  private JLabel OBJ_42;
  private JLabel OBJ_17;
  private XRiTextField SCRSC;
  private JLabel OBJ_38;
  private JLabel OBJ_44;
  private JPanel panel3;
  private XRiTextField OB4;
  private XRiTextField OB5;
  private JPanel panel2;
  private XRiTextField OB1;
  private XRiTextField OB2;
  private XRiTextField OB3;
  private JPanel panel4;
  private XRiTextField SCNCNX;
  private XRiTextField SCNLMO;
  private JLabel OBJ_35;
  private XRiTextField SCIDLO;
  private XRiTextField SCLIEU;
  private XRiTextField SCMODE;
  private JLabel OBJ_25;
  private JLabel OBJ_27;
  private JLabel OBJ_31;
  private JLabel OBJ_33;
  private JLabel OBJ_29;
  private XRiTextField SCTMOD;
  private XRiTextField SCUSER;
  private XRiTextField SCPASS;
  private XRiTextField SCTELN;
  private XRiTextField SCFTP;
  private XRiTextField SCPORF;
  private JLabel OBJ_46;
  private JLabel OBJ_40;
  private XRiTextField SCPORT;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
