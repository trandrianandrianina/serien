
package ri.serien.libecranrpg.vspm.VSPM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

public class VSPM03FM_C3 extends SNPanelEcranRPG implements ioFrame {
  
   
  // TODO declarations classe spécifiques...
  private String[] SCTLCO_Value = { "", "T", "C", "E" };
  
  public VSPM03FM_C3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // TODO constructeur spécifiques...
    
    // Ajout
    initDiverses();
    SCTLCO.setValeurs(SCTLCO_Value, null);
    SCSQL.setValeursSelection("1", " ");
    SCQRY.setValeursSelection("1", " ");
    SCFORM.setValeursSelection("1", " ");
    SCSPA.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // TODO setData spécifiques...
    // SCTLCO.setSelectedIndex(getIndice("SCTLCO", SCTLCO_Value));
    // SCSPA.setSelected(lexique.HostFieldGetData("SCSPA").equalsIgnoreCase("1"));
    // SCSQL.setSelected(lexique.HostFieldGetData("SCSQL").equalsIgnoreCase("1"));
    // SCQRY.setSelected(lexique.HostFieldGetData("SCQRY").equalsIgnoreCase("1"));
    // SCFORM.setSelected(lexique.HostFieldGetData("SCFORM").equalsIgnoreCase("1"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // TODO getDATA spécifiques
    // lexique.HostFieldPutData("SCTLCO", 0, SCTLCO_Value[SCTLCO.getSelectedIndex()]);
    
    // if (SCSPA.isSelected())
    // lexique.HostFieldPutData("SCSPA", 0, "1");
    // else
    // lexique.HostFieldPutData("SCSPA", 0, " ");
    
    // if (SCSQL.isSelected())
    // lexique.HostFieldPutData("SCSQL", 0, "1");
    // else
    // lexique.HostFieldPutData("SCSQL", 0, " ");
    
    // if (SCQRY.isSelected())
    // lexique.HostFieldPutData("SCQRY", 0, "1");
    // else
    // lexique.HostFieldPutData("SCQRY", 0, " ");
    
    // if (SCFORM.isSelected())
    // lexique.HostFieldPutData("SCFORM", 0, "1");
    // else
    // lexique.HostFieldPutData("SCFORM", 0, " ");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_17 = new JLabel();
    SCSEN = new XRiTextField();
    OBJ_19 = new JLabel();
    SCTMA = new XRiTextField();
    OBJ_21 = new JLabel();
    SCVOS = new XRiTextField();
    SCNFA = new XRiTextField();
    OBJ_25 = new JLabel();
    SCDAFX = new XRiCalendrier();
    OBJ_27 = new JLabel();
    SCCIBM = new XRiTextField();
    SCOBSM = new XRiTextField();
    SCFMTX = new XRiCalendrier();
    SCFMLX = new XRiCalendrier();
    xTitledSeparator1 = new JXTitledSeparator();
    OBJ_18 = new JLabel();
    OBJ_20 = new JLabel();
    label1 = new JLabel();
    panel2 = new JPanel();
    SCTLCO = new XRiComboBox();
    SCSPA = new XRiCheckBox();
    OBJ_35 = new JLabel();
    SCFORM = new XRiCheckBox();
    SCQRY = new XRiCheckBox();
    SCSQL = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(860, 415));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Mat\u00e9riel"));
          panel1.setOpaque(false);
          panel1.setName("panel1");

          //---- OBJ_17 ----
          OBJ_17.setText("Num\u00e9ro de s\u00e9rie");
          OBJ_17.setName("OBJ_17");

          //---- SCSEN ----
          SCSEN.setComponentPopupMenu(BTD);
          SCSEN.setName("SCSEN");

          //---- OBJ_19 ----
          OBJ_19.setText("Type");
          OBJ_19.setName("OBJ_19");

          //---- SCTMA ----
          SCTMA.setComponentPopupMenu(BTD);
          SCTMA.setName("SCTMA");

          //---- OBJ_21 ----
          OBJ_21.setText("Version OS400");
          OBJ_21.setName("OBJ_21");

          //---- SCVOS ----
          SCVOS.setComponentPopupMenu(BTD);
          SCVOS.setName("SCVOS");

          //---- SCNFA ----
          SCNFA.setComponentPopupMenu(BTD);
          SCNFA.setName("SCNFA");

          //---- OBJ_25 ----
          OBJ_25.setText("Date de cette facture");
          OBJ_25.setName("OBJ_25");

          //---- SCDAFX ----
          SCDAFX.setComponentPopupMenu(BTD);
          SCDAFX.setName("SCDAFX");

          //---- OBJ_27 ----
          OBJ_27.setText("Num\u00e9ro client IBM");
          OBJ_27.setName("OBJ_27");

          //---- SCCIBM ----
          SCCIBM.setComponentPopupMenu(BTD);
          SCCIBM.setName("SCCIBM");

          //---- SCOBSM ----
          SCOBSM.setComponentPopupMenu(BTD);
          SCOBSM.setName("SCOBSM");

          //---- SCFMTX ----
          SCFMTX.setComponentPopupMenu(BTD);
          SCFMTX.setName("SCFMTX");

          //---- SCFMLX ----
          SCFMLX.setComponentPopupMenu(BTD);
          SCFMLX.setName("SCFMLX");

          //---- xTitledSeparator1 ----
          xTitledSeparator1.setTitle("Observations");
          xTitledSeparator1.setName("xTitledSeparator1");

          //---- OBJ_18 ----
          OBJ_18.setText("Fin de maintenance mat\u00e9rielle");
          OBJ_18.setName("OBJ_18");

          //---- OBJ_20 ----
          OBJ_20.setText("Num\u00e9ro de facture");
          OBJ_20.setName("OBJ_20");

          //---- label1 ----
          label1.setText("Fin de maintenance logicielle");
          label1.setName("label1");

          GroupLayout panel1Layout = new GroupLayout(panel1);
          panel1.setLayout(panel1Layout);
          panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(OBJ_17, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(SCSEN, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(SCTMA, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(SCVOS, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(SCNFA, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
                .addGap(99, 99, 99)
                .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
                .addComponent(SCDAFX, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(SCCIBM, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(OBJ_18, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
                .addComponent(SCFMTX, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(label1, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
                .addComponent(SCFMLX, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, 635, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(SCOBSM, GroupLayout.PREFERRED_SIZE, 605, GroupLayout.PREFERRED_SIZE))
          );
          panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_17, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(SCSEN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(SCTMA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(SCVOS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(SCNFA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(SCDAFX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(SCCIBM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_18, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(SCFMTX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(label1))
                  .addComponent(SCFMLX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(SCOBSM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 670, 290);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Logiciels et utilitaires"));
          panel2.setOpaque(false);
          panel2.setName("panel2");

          //---- SCTLCO ----
          SCTLCO.setModel(new DefaultComboBoxModel(new String[] {
            " ",
            "Telbank",
            "Combac",
            "eBanks"
          }));
          SCTLCO.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCTLCO.setName("SCTLCO");

          //---- SCSPA ----
          SCSPA.setText("Programmes sp\u00e9cifiques agent");
          SCSPA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCSPA.setName("SCSPA");

          //---- OBJ_35 ----
          OBJ_35.setText("Transmission bancaire");
          OBJ_35.setName("OBJ_35");

          //---- SCFORM ----
          SCFORM.setText("Formulary");
          SCFORM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCFORM.setName("SCFORM");

          //---- SCQRY ----
          SCQRY.setText("Query");
          SCQRY.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCQRY.setName("SCQRY");

          //---- SCSQL ----
          SCSQL.setText("SQL");
          SCSQL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCSQL.setName("SCSQL");

          GroupLayout panel2Layout = new GroupLayout(panel2);
          panel2.setLayout(panel2Layout);
          panel2Layout.setHorizontalGroup(
            panel2Layout.createParallelGroup()
              .addGroup(panel2Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(SCTLCO, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(SCSPA, GroupLayout.PREFERRED_SIZE, 212, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel2Layout.createSequentialGroup()
                .addGap(344, 344, 344)
                .addComponent(SCSQL, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(SCQRY, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addComponent(SCFORM, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE))
          );
          panel2Layout.setVerticalGroup(
            panel2Layout.createParallelGroup()
              .addGroup(panel2Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(panel2Layout.createParallelGroup()
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(SCTLCO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(SCSPA, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
                .addGap(2, 2, 2)
                .addGroup(panel2Layout.createParallelGroup()
                  .addComponent(SCSQL, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(SCQRY, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(SCFORM, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_contenu.add(panel2);
        panel2.setBounds(10, 305, 670, 100);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_17;
  private XRiTextField SCSEN;
  private JLabel OBJ_19;
  private XRiTextField SCTMA;
  private JLabel OBJ_21;
  private XRiTextField SCVOS;
  private XRiTextField SCNFA;
  private JLabel OBJ_25;
  private XRiCalendrier SCDAFX;
  private JLabel OBJ_27;
  private XRiTextField SCCIBM;
  private XRiTextField SCOBSM;
  private XRiCalendrier SCFMTX;
  private XRiCalendrier SCFMLX;
  private JXTitledSeparator xTitledSeparator1;
  private JLabel OBJ_18;
  private JLabel OBJ_20;
  private JLabel label1;
  private JPanel panel2;
  private XRiComboBox SCTLCO;
  private XRiCheckBox SCSPA;
  private JLabel OBJ_35;
  private XRiCheckBox SCFORM;
  private XRiCheckBox SCQRY;
  private XRiCheckBox SCSQL;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
