
package ri.serien.libecranrpg.vspm.VSPM05FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;

/**
 * @author Stéphane Vénéri
 */
public class VSPM05FM_NL extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VSPM05FM_NL(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    WNCTD.setValeursSelection("OUI", "NON");
    WCHX1.setValeurs("1");
    WCHX2.setValeurs("2");
    WCHX3.setValeurs("3");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_32.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("@LTITNL@")).trim()));
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LERR@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    // WNCTD.setSelected(lexique.HostFieldGetData("WNCTD").trim().equalsIgnoreCase("OUI"));
    // WCHX1.setSelected(!lexique.HostFieldGetData("WCHX1").trim().equalsIgnoreCase(""));
    // WCHX2.setSelected(!lexique.HostFieldGetData("WCHX2").trim().equalsIgnoreCase(""));
    // WCHX3.setSelected(!lexique.HostFieldGetData("WCHX3").trim().equalsIgnoreCase(""));
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Licences"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WNCTD.isSelected())
    // lexique.HostFieldPutData("WNCTD", 0, "OUI");
    // else
    // lexique.HostFieldPutData("WNCTD", 0, "NON");
    
    // if (WCHX1.isSelected())
    // lexique.HostFieldPutData("WCHX1", 0, "1");
    // else
    // lexique.HostFieldPutData("WCHX1", 0, " ");
    // if (WCHX2.isSelected())
    // lexique.HostFieldPutData("WCHX2", 0, "1");
    // else
    // lexique.HostFieldPutData("WCHX2", 0, " ");
    // if (WCHX3.isSelected())
    // lexique.HostFieldPutData("WCHX3", 0, "1");
    // else
    // lexique.HostFieldPutData("WCHX3", 0, " ");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void inviteActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void aideActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  // EVENEMENTIEL
  // +++++++++++++++++++++++++++++++++++++++++++
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_contenu = new JPanel();
    OBJ_32 = new JPanel();
    WNCTD = new XRiCheckBox();
    separator1 = compFactory.createSeparator("Type de sortie");
    label1 = new JLabel();
    WCHX1 = new XRiRadioButton();
    WCHX2 = new XRiRadioButton();
    WCHX3 = new XRiRadioButton();
    BTD = new JPopupMenu();
    invite = new JMenuItem();
    aide = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(565, 270));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Suppression");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== OBJ_32 ========
        {
          OBJ_32.setBorder(new TitledBorder("@LTITNL@"));
          OBJ_32.setOpaque(false);
          OBJ_32.setName("OBJ_32");
          OBJ_32.setLayout(null);

          //---- WNCTD ----
          WNCTD.setText("Les num\u00e9ros des licences sont d\u00e9finitifs");
          WNCTD.setFont(WNCTD.getFont().deriveFont(WNCTD.getFont().getSize() + 3f));
          WNCTD.setName("WNCTD");
          OBJ_32.add(WNCTD);
          WNCTD.setBounds(35, 40, 315, WNCTD.getPreferredSize().height);

          //---- separator1 ----
          separator1.setName("separator1");
          OBJ_32.add(separator1);
          separator1.setBounds(25, 90, 315, separator1.getPreferredSize().height);

          //---- label1 ----
          label1.setText("@LERR@");
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getSize() + 3f));
          label1.setForeground(Color.red);
          label1.setName("label1");
          OBJ_32.add(label1);
          label1.setBounds(15, 210, 345, label1.getPreferredSize().height);

          //---- WCHX1 ----
          WCHX1.setText("Affichage");
          WCHX1.setFont(WCHX1.getFont().deriveFont(WCHX1.getFont().getSize() + 3f));
          WCHX1.setName("WCHX1");
          OBJ_32.add(WCHX1);
          WCHX1.setBounds(35, 115, 315, WCHX1.getPreferredSize().height);

          //---- WCHX2 ----
          WCHX2.setText("Edition");
          WCHX2.setFont(WCHX2.getFont().deriveFont(WCHX2.getFont().getSize() + 3f));
          WCHX2.setName("WCHX2");
          OBJ_32.add(WCHX2);
          WCHX2.setBounds(35, 145, 315, WCHX2.getPreferredSize().height);

          //---- WCHX3 ----
          WCHX3.setText("Envoi par email");
          WCHX3.setFont(WCHX3.getFont().deriveFont(WCHX3.getFont().getSize() + 3f));
          WCHX3.setName("WCHX3");
          OBJ_32.add(WCHX3);
          WCHX3.setBounds(35, 175, 315, WCHX3.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < OBJ_32.getComponentCount(); i++) {
              Rectangle bounds = OBJ_32.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = OBJ_32.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            OBJ_32.setMinimumSize(preferredSize);
            OBJ_32.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(OBJ_32);
        OBJ_32.setBounds(10, 10, 375, 250);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- invite ----
      invite.setText("Choix possibles");
      invite.setName("invite");
      invite.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          inviteActionPerformed(e);
        }
      });
      BTD.add(invite);

      //---- aide ----
      aide.setText("Aide en ligne");
      aide.setName("aide");
      aide.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          aideActionPerformed(e);
        }
      });
      BTD.add(aide);
    }

    //---- WCHX_GRP ----
    ButtonGroup WCHX_GRP = new ButtonGroup();
    WCHX_GRP.add(WCHX1);
    WCHX_GRP.add(WCHX2);
    WCHX_GRP.add(WCHX3);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private JPanel p_contenu;
  private JPanel OBJ_32;
  private XRiCheckBox WNCTD;
  private JComponent separator1;
  private JLabel label1;
  private XRiRadioButton WCHX1;
  private XRiRadioButton WCHX2;
  private XRiRadioButton WCHX3;
  private JPopupMenu BTD;
  private JMenuItem invite;
  private JMenuItem aide;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
