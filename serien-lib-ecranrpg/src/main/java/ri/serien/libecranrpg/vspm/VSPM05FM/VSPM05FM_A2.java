
package ri.serien.libecranrpg.vspm.VSPM05FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Menu;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.exploitation.licence.Licence;
import ri.serien.libcommun.exploitation.licence.ParametreMenuLicence;
import ri.serien.libcommun.exploitation.menu.EnumPointMenu;
import ri.serien.libcommun.exploitation.menu.IdEnregistrementMneMnp;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.ODialog;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VSPM05FM_A2 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  private String[] SCTLCO_Value = { " ", "T", "C", "E" };
  public ODialog dialog_CNX = null;
  public ODialog dialog_SAV = null;
  public ODialog dialog_MOD = null;
  public ODialog dialog_MAT = null;
  
  /**
   * Constructeur.
   */
  public VSPM05FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    panel2.setRightDecoration(TCI1);
    panel6.setRightDecoration(TCI2);
    panel8.setRightDecoration(TCI3);
    panel11.setRightDecoration(TCI4);
    
    CLNOM.activerModeFantome("nom ou raison sociale");
    
    // Ajout
    initDiverses();
    SCTLCO.setValeurs(SCTLCO_Value, null);
    SCGPM.setValeursSelection("1", " ");
    SCGVM.setValeursSelection("1", " ");
    SCS11.setValeursSelection("1", " ");
    SCPAM.setValeursSelection("1", " ");
    SCGAM.setValeursSelection("1", " ");
    SCCGM.setValeursSelection("1", " ");
    SCSQL.setValeursSelection("O", "N");
    SCSPA.setValeursSelection("O", "N");
    SCFORM.setValeursSelection("O", "N");
    SCRONE.setValeursSelection("O", "N");
    SCQRY.setValeursSelection("O", "N");
    
    // Chargement des icones
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    CLINOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLINOM@")).trim());
    CAPNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAPNOM@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  // -- Méthodes privées
  
  /**
   * Lance le programme des licences avec les paramèters qui vont bien.
   */
  private void lancerPointMenuLicence() {
    // Création de l'identifiant du client en cours
    IdEtablissement idEtablissement = IdEtablissement.getInstance(Licence.CODE_ETABLISSEMENT_RI);
    Integer numero = Constantes.convertirTexteEnInteger(CLCLI.getText());
    Integer suffixe = Constantes.convertirTexteEnInteger(CLLIV.getText());
    IdClient idClient = IdClient.getInstance(idEtablissement, numero, suffixe);
    
    // Initialisation des paramètres pour le programme des licences Série N
    final ParametreMenuLicence parametreMenuLicence = new ParametreMenuLicence();
    parametreMenuLicence.setModeExecution(Integer.valueOf(1));
    parametreMenuLicence.setIdClient(idClient);
    // Lancement du point de menu
    ManagerSessionClient.getInstance().lancerPointMenu(getSession(), IdEnregistrementMneMnp.getInstance(EnumPointMenu.LICENCES),
        parametreMenuLicence);
  }
  
  // -- Méthodes évènementielles
  
  private void TCI1ActionPerformed(ActionEvent e) {
    if (dialog_CNX == null) {
      dialog_CNX = new ODialog((Window) getTopLevelAncestor(), new VSPM05FM_CNX(this));
    }
    dialog_CNX.affichePopupPerso();
  }
  
  private void TCI2ActionPerformed(ActionEvent e) {
    if (dialog_SAV == null) {
      dialog_SAV = new ODialog((Window) getTopLevelAncestor(), new VSPM05FM_SAV(this));
    }
    dialog_SAV.affichePopupPerso();
  }
  
  private void TCI3ActionPerformed(ActionEvent e) {
    if (dialog_MOD == null) {
      dialog_MOD = new ODialog((Window) getTopLevelAncestor(), new VSPM05FM_MOD(this));
    }
    dialog_MOD.affichePopupPerso();
  }
  
  private void TCI4ActionPerformed(ActionEvent e) {
    if (dialog_MAT == null) {
      dialog_MAT = new ODialog((Window) getTopLevelAncestor(), new VSPM05FM_MAT(this));
    }
    dialog_MAT.affichePopupPerso();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", false);
  }
  
  private void riSousMenu_bt13ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lancerPointMenuLicence();
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23", false);
  }
  
  private void inviteActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void aideActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    Desktop desktop = null;
    java.net.URI url;
    
    try {
      url = new java.net.URI(
          "http://documentation.resolution-informatique.com/documentation/?page_id=6403226#" + lexique.HostFieldGetData("CLCLI"));
      if (Desktop.isDesktopSupported()) {
        desktop = Desktop.getDesktop();
        desktop.browse(url);
      }
    }
    catch (Exception ex) {
      Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
  
  // -- Méthodes évènementielles
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    panel3 = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu13 = new RiSousMenu();
    riSousMenu_bt13 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    layeredPane1 = new JLayeredPane();
    panel10 = new JPanel();
    panel2 = new JXTitledPanel();
    OBJ_139 = new JLabel();
    SCSYST = new XRiTextField();
    OBJ_142 = new JLabel();
    SCRSC = new XRiTextField();
    OBJ_144 = new JLabel();
    SCTCP = new XRiTextField();
    xTitledSeparator6 = new JXTitledSeparator();
    xTitledSeparator7 = new JXTitledSeparator();
    SCTELN = new XRiTextField();
    SCFTP = new XRiTextField();
    SCPORT = new XRiTextField();
    OBJ_157 = new JLabel();
    OBJ_158 = new JLabel();
    SCPORF = new XRiTextField();
    panel1 = new JPanel();
    OBJ_159 = new JLabel();
    SCUSER = new XRiTextField();
    OBJ_160 = new JLabel();
    SCPASS = new XRiTextField();
    panel6 = new JXTitledPanel();
    OBJ_182 = new JLabel();
    SCCLS = new XRiTextField();
    SCCAP = new XRiTextField();
    label4 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    SCNFS = new XRiTextField();
    SCDRSX = new XRiCalendrier();
    CLINOM = new RiZoneSortie();
    CAPNOM = new RiZoneSortie();
    panel7 = new JXTitledPanel();
    label8 = new JLabel();
    SCTLCO = new XRiComboBox();
    SCQRY = new XRiCheckBox();
    SCRONE = new XRiCheckBox();
    SCFORM = new XRiCheckBox();
    SCSPA = new XRiCheckBox();
    SCSQL = new XRiCheckBox();
    panel11 = new JXTitledPanel();
    OBJ_183 = new JLabel();
    SCSEN = new XRiTextField();
    OBJ_187 = new JLabel();
    SCTMA = new XRiTextField();
    OBJ_184 = new JLabel();
    SCVOS = new XRiTextField();
    OBJ_188 = new JLabel();
    SCDAFX = new XRiCalendrier();
    panel8 = new JXTitledPanel();
    SCCGM = new XRiCheckBox();
    SCGAM = new XRiCheckBox();
    SCPAM = new XRiCheckBox();
    SCS11 = new XRiCheckBox();
    SCGVM = new XRiCheckBox();
    SCGPM = new XRiCheckBox();
    panel9 = new JXTitledPanel();
    OBJ_185 = new JLabel();
    SCNBU = new XRiTextField();
    SCVGM = new XRiTextField();
    OBJ_189 = new JLabel();
    OBJ_190 = new JLabel();
    SCBUL = new XRiTextField();
    OBJ_191 = new JLabel();
    SCEFF = new XRiTextField();
    OBJ_192 = new JLabel();
    SCNMC = new XRiTextField();
    OBJ_65 = new JPanel();
    CLNOM = new XRiTextField();
    CLCLI = new XRiTextField();
    CLLIV = new XRiTextField();
    OBJ_140 = new JLabel();
    SCTSM = new XRiTextField();
    OBJ_141 = new JLabel();
    SCOBS = new XRiTextField();
    OBJ_143 = new JLabel();
    SCACT = new XRiTextField();
    OBJ_148 = new JLabel();
    SCCAF = new XRiTextField();
    OBJ_149 = new JLabel();
    SCBASP = new XRiTextField();
    label1 = new JLabel();
    p_desac = new JPanel();
    lab_desac = new JLabel();
    TCI1 = new SNBoutonDetail();
    TCI2 = new SNBoutonDetail();
    TCI3 = new SNBoutonDetail();
    TCI4 = new SNBoutonDetail();
    BTD = new JPopupMenu();
    invite = new JMenuItem();
    aide = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(1200, 710));
    setPreferredSize(new Dimension(1200, 710));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Fiche sp\u00e9cifique des clients de R\u00e9solution Informatique");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 34));
          p_tete_gauche.setMinimumSize(new Dimension(700, 34));
          p_tete_gauche.setName("p_tete_gauche");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGap(0, 862, Short.MAX_VALUE)
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGap(0, 30, Short.MAX_VALUE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //======== panel3 ========
          {
            panel3.setName("panel3");
            panel3.setLayout(null);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }
          p_tete_droite.add(panel3);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Licences S\u00e9rie M");
              riSousMenu_bt6.setToolTipText("Retour fiche courante");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu13 ========
            {
              riSousMenu13.setName("riSousMenu13");

              //---- riSousMenu_bt13 ----
              riSousMenu_bt13.setText("Licences S\u00e9rie N");
              riSousMenu_bt13.setToolTipText("Derniers \u00e9v\u00e8nements");
              riSousMenu_bt13.setName("riSousMenu_bt13");
              riSousMenu_bt13.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt13ActionPerformed(e);
                }
              });
              riSousMenu13.add(riSousMenu_bt13);
            }
            menus_haut.add(riSousMenu13);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Nvelles licences S\u00e9rie N");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Calcul de clef V9");
              riSousMenu_bt8.setToolTipText("Calcul de clef version 9");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Dossier client extranet");
              riSousMenu_bt10.setToolTipText("Dossier client sur notre extranet d'assistance");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1000, 610));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setForeground(Color.black);
          p_contenu.setMinimumSize(new Dimension(1000, 610));
          p_contenu.setName("p_contenu");

          //======== layeredPane1 ========
          {
            layeredPane1.setPreferredSize(new Dimension(1000, 610));
            layeredPane1.setMinimumSize(new Dimension(1000, 610));
            layeredPane1.setName("layeredPane1");

            //======== panel10 ========
            {
              panel10.setOpaque(false);
              panel10.setName("panel10");
              panel10.setLayout(null);

              //======== panel2 ========
              {
                panel2.setBorder(new DropShadowBorder());
                panel2.setTitle("Connexions");
                panel2.setName("panel2");
                Container panel2ContentContainer = panel2.getContentContainer();
                panel2ContentContainer.setLayout(null);

                //---- OBJ_139 ----
                OBJ_139.setText("Syst\u00e8me");
                OBJ_139.setName("OBJ_139");
                panel2ContentContainer.add(OBJ_139);
                OBJ_139.setBounds(15, 9, 70, 20);

                //---- SCSYST ----
                SCSYST.setComponentPopupMenu(BTD);
                SCSYST.setName("SCSYST");
                panel2ContentContainer.add(SCSYST);
                SCSYST.setBounds(85, 5, 94, SCSYST.getPreferredSize().height);

                //---- OBJ_142 ----
                OBJ_142.setText("Ressource communication");
                OBJ_142.setName("OBJ_142");
                panel2ContentContainer.add(OBJ_142);
                OBJ_142.setBounds(260, 9, 160, 20);

                //---- SCRSC ----
                SCRSC.setComponentPopupMenu(BTD);
                SCRSC.setName("SCRSC");
                panel2ContentContainer.add(SCRSC);
                SCRSC.setBounds(430, 5, 94, SCRSC.getPreferredSize().height);

                //---- OBJ_144 ----
                OBJ_144.setText("Adresse IP");
                OBJ_144.setName("OBJ_144");
                panel2ContentContainer.add(OBJ_144);
                OBJ_144.setBounds(620, 9, 75, 20);

                //---- SCTCP ----
                SCTCP.setComponentPopupMenu(BTD);
                SCTCP.setName("SCTCP");
                panel2ContentContainer.add(SCTCP);
                SCTCP.setBounds(715, 5, 164, SCTCP.getPreferredSize().height);

                //---- xTitledSeparator6 ----
                xTitledSeparator6.setTitle("TELNET");
                xTitledSeparator6.setName("xTitledSeparator6");
                panel2ContentContainer.add(xTitledSeparator6);
                xTitledSeparator6.setBounds(15, 35, 575, xTitledSeparator6.getPreferredSize().height);

                //---- xTitledSeparator7 ----
                xTitledSeparator7.setTitle("FTP");
                xTitledSeparator7.setName("xTitledSeparator7");
                panel2ContentContainer.add(xTitledSeparator7);
                xTitledSeparator7.setBounds(15, 85, 575, xTitledSeparator7.getPreferredSize().height);

                //---- SCTELN ----
                SCTELN.setComponentPopupMenu(BTD);
                SCTELN.setName("SCTELN");
                panel2ContentContainer.add(SCTELN);
                SCTELN.setBounds(25, 55, 437, SCTELN.getPreferredSize().height);

                //---- SCFTP ----
                SCFTP.setComponentPopupMenu(BTD);
                SCFTP.setName("SCFTP");
                panel2ContentContainer.add(SCFTP);
                SCFTP.setBounds(25, 105, 437, SCFTP.getPreferredSize().height);

                //---- SCPORT ----
                SCPORT.setComponentPopupMenu(BTD);
                SCPORT.setName("SCPORT");
                panel2ContentContainer.add(SCPORT);
                SCPORT.setBounds(515, 50, 64, SCPORT.getPreferredSize().height);

                //---- OBJ_157 ----
                OBJ_157.setText("Port");
                OBJ_157.setName("OBJ_157");
                panel2ContentContainer.add(OBJ_157);
                OBJ_157.setBounds(470, 55, 45, 20);

                //---- OBJ_158 ----
                OBJ_158.setText("Port");
                OBJ_158.setName("OBJ_158");
                panel2ContentContainer.add(OBJ_158);
                OBJ_158.setBounds(470, 105, 45, 20);

                //---- SCPORF ----
                SCPORF.setComponentPopupMenu(BTD);
                SCPORF.setName("SCPORF");
                panel2ContentContainer.add(SCPORF);
                SCPORF.setBounds(515, 100, 64, SCPORF.getPreferredSize().height);

                //======== panel1 ========
                {
                  panel1.setBorder(new TitledBorder(""));
                  panel1.setOpaque(false);
                  panel1.setName("panel1");
                  panel1.setLayout(null);

                  //---- OBJ_159 ----
                  OBJ_159.setText("Utilisateur");
                  OBJ_159.setName("OBJ_159");
                  panel1.add(OBJ_159);
                  OBJ_159.setBounds(15, 19, 85, 20);

                  //---- SCUSER ----
                  SCUSER.setComponentPopupMenu(BTD);
                  SCUSER.setName("SCUSER");
                  panel1.add(SCUSER);
                  SCUSER.setBounds(105, 15, 210, SCUSER.getPreferredSize().height);

                  //---- OBJ_160 ----
                  OBJ_160.setText("Mot de passe");
                  OBJ_160.setName("OBJ_160");
                  panel1.add(OBJ_160);
                  OBJ_160.setBounds(15, 64, 85, 20);

                  //---- SCPASS ----
                  SCPASS.setComponentPopupMenu(BTD);
                  SCPASS.setName("SCPASS");
                  panel1.add(SCPASS);
                  SCPASS.setBounds(105, 60, 210, SCPASS.getPreferredSize().height);

                  {
                    // compute preferred size
                    Dimension preferredSize = new Dimension();
                    for(int i = 0; i < panel1.getComponentCount(); i++) {
                      Rectangle bounds = panel1.getComponent(i).getBounds();
                      preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                      preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                    }
                    Insets insets = panel1.getInsets();
                    preferredSize.width += insets.right;
                    preferredSize.height += insets.bottom;
                    panel1.setMinimumSize(preferredSize);
                    panel1.setPreferredSize(preferredSize);
                  }
                }
                panel2ContentContainer.add(panel1);
                panel1.setBounds(610, 40, 330, 100);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < panel2ContentContainer.getComponentCount(); i++) {
                    Rectangle bounds = panel2ContentContainer.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = panel2ContentContainer.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  panel2ContentContainer.setMinimumSize(preferredSize);
                  panel2ContentContainer.setPreferredSize(preferredSize);
                }
              }
              panel10.add(panel2);
              panel2.setBounds(10, 124, 965, 170);

              //======== panel6 ========
              {
                panel6.setTitle("SAV");
                panel6.setBorder(new DropShadowBorder());
                panel6.setName("panel6");
                Container panel6ContentContainer = panel6.getContentContainer();
                panel6ContentContainer.setLayout(null);

                //---- OBJ_182 ----
                OBJ_182.setText("Num\u00e9ro");
                OBJ_182.setName("OBJ_182");
                panel6ContentContainer.add(OBJ_182);
                OBJ_182.setBounds(15, 14, 60, 20);

                //---- SCCLS ----
                SCCLS.setComponentPopupMenu(BTD);
                SCCLS.setName("SCCLS");
                panel6ContentContainer.add(SCCLS);
                SCCLS.setBounds(125, 10, 68, SCCLS.getPreferredSize().height);

                //---- SCCAP ----
                SCCAP.setComponentPopupMenu(BTD);
                SCCAP.setName("SCCAP");
                panel6ContentContainer.add(SCCAP);
                SCCAP.setBounds(125, 42, 44, SCCAP.getPreferredSize().height);

                //---- label4 ----
                label4.setText("Chef de projet");
                label4.setName("label4");
                panel6ContentContainer.add(label4);
                label4.setBounds(15, 43, 100, 26);

                //---- label6 ----
                label6.setText("Derni\u00e8re facture");
                label6.setName("label6");
                panel6ContentContainer.add(label6);
                label6.setBounds(15, 75, 100, 26);

                //---- label7 ----
                label7.setText("Renouvellement");
                label7.setName("label7");
                panel6ContentContainer.add(label7);
                label7.setBounds(245, 75, 100, 26);

                //---- SCNFS ----
                SCNFS.setName("SCNFS");
                panel6ContentContainer.add(SCNFS);
                SCNFS.setBounds(125, 74, 68, SCNFS.getPreferredSize().height);

                //---- SCDRSX ----
                SCDRSX.setName("SCDRSX");
                panel6ContentContainer.add(SCDRSX);
                SCDRSX.setBounds(350, 74, 110, SCDRSX.getPreferredSize().height);

                //---- CLINOM ----
                CLINOM.setText("@CLINOM@");
                CLINOM.setName("CLINOM");
                panel6ContentContainer.add(CLINOM);
                CLINOM.setBounds(195, 12, 270, CLINOM.getPreferredSize().height);

                //---- CAPNOM ----
                CAPNOM.setText("@CAPNOM@");
                CAPNOM.setName("CAPNOM");
                panel6ContentContainer.add(CAPNOM);
                CAPNOM.setBounds(195, 44, 270, CAPNOM.getPreferredSize().height);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < panel6ContentContainer.getComponentCount(); i++) {
                    Rectangle bounds = panel6ContentContainer.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = panel6ContentContainer.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  panel6ContentContainer.setMinimumSize(preferredSize);
                  panel6ContentContainer.setPreferredSize(preferredSize);
                }
              }
              panel10.add(panel6);
              panel6.setBounds(10, 294, 475, 146);

              //======== panel7 ========
              {
                panel7.setTitle("Logiciels tiers");
                panel7.setBorder(new DropShadowBorder());
                panel7.setName("panel7");
                Container panel7ContentContainer = panel7.getContentContainer();
                panel7ContentContainer.setLayout(null);

                //---- label8 ----
                label8.setText("Transmissions bancaire");
                label8.setName("label8");
                panel7ContentContainer.add(label8);
                label8.setBounds(10, 13, 155, 20);

                //---- SCTLCO ----
                SCTLCO.setModel(new DefaultComboBoxModel(new String[] {
                  " ",
                  "Telbank",
                  "Combac",
                  "EBanks"
                }));
                SCTLCO.setName("SCTLCO");
                panel7ContentContainer.add(SCTLCO);
                SCTLCO.setBounds(175, 10, 265, SCTLCO.getPreferredSize().height);

                //---- SCQRY ----
                SCQRY.setText("Query");
                SCQRY.setName("SCQRY");
                panel7ContentContainer.add(SCQRY);
                SCQRY.setBounds(20, 45, 125, SCQRY.getPreferredSize().height);

                //---- SCRONE ----
                SCRONE.setText("Report One");
                SCRONE.setName("SCRONE");
                panel7ContentContainer.add(SCRONE);
                SCRONE.setBounds(157, 75, 125, SCRONE.getPreferredSize().height);

                //---- SCFORM ----
                SCFORM.setText("Formulary");
                SCFORM.setName("SCFORM");
                panel7ContentContainer.add(SCFORM);
                SCFORM.setBounds(20, 75, 125, SCFORM.getPreferredSize().height);

                //---- SCSPA ----
                SCSPA.setText("Sp\u00e9cifique agent");
                SCSPA.setName("SCSPA");
                panel7ContentContainer.add(SCSPA);
                SCSPA.setBounds(294, 45, 125, SCSPA.getPreferredSize().height);

                //---- SCSQL ----
                SCSQL.setText("SQL");
                SCSQL.setName("SCSQL");
                panel7ContentContainer.add(SCSQL);
                SCSQL.setBounds(157, 45, 125, SCSQL.getPreferredSize().height);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < panel7ContentContainer.getComponentCount(); i++) {
                    Rectangle bounds = panel7ContentContainer.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = panel7ContentContainer.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  panel7ContentContainer.setMinimumSize(preferredSize);
                  panel7ContentContainer.setPreferredSize(preferredSize);
                }
              }
              panel10.add(panel7);
              panel7.setBounds(10, 444, 475, 146);

              //======== panel11 ========
              {
                panel11.setTitle("Mat\u00e9riel");
                panel11.setBorder(new DropShadowBorder());
                panel11.setName("panel11");
                Container panel11ContentContainer = panel11.getContentContainer();
                panel11ContentContainer.setLayout(null);

                //---- OBJ_183 ----
                OBJ_183.setText("Num\u00e9ro de s\u00e9rie");
                OBJ_183.setName("OBJ_183");
                panel11ContentContainer.add(OBJ_183);
                OBJ_183.setBounds(10, 9, 110, 20);

                //---- SCSEN ----
                SCSEN.setComponentPopupMenu(BTD);
                SCSEN.setName("SCSEN");
                panel11ContentContainer.add(SCSEN);
                SCSEN.setBounds(130, 5, 94, SCSEN.getPreferredSize().height);

                //---- OBJ_187 ----
                OBJ_187.setText("Type");
                OBJ_187.setName("OBJ_187");
                panel11ContentContainer.add(OBJ_187);
                OBJ_187.setBounds(245, 10, 60, 20);

                //---- SCTMA ----
                SCTMA.setComponentPopupMenu(BTD);
                SCTMA.setName("SCTMA");
                panel11ContentContainer.add(SCTMA);
                SCTMA.setBounds(315, 5, 144, SCTMA.getPreferredSize().height);

                //---- OBJ_184 ----
                OBJ_184.setText("Version de l'OS");
                OBJ_184.setName("OBJ_184");
                panel11ContentContainer.add(OBJ_184);
                OBJ_184.setBounds(10, 39, 110, 20);

                //---- SCVOS ----
                SCVOS.setComponentPopupMenu(BTD);
                SCVOS.setName("SCVOS");
                panel11ContentContainer.add(SCVOS);
                SCVOS.setBounds(130, 35, 68, SCVOS.getPreferredSize().height);

                //---- OBJ_188 ----
                OBJ_188.setText("Achat serveur");
                OBJ_188.setName("OBJ_188");
                panel11ContentContainer.add(OBJ_188);
                OBJ_188.setBounds(245, 40, 85, 20);

                //---- SCDAFX ----
                SCDAFX.setName("SCDAFX");
                panel11ContentContainer.add(SCDAFX);
                SCDAFX.setBounds(349, 35, 110, SCDAFX.getPreferredSize().height);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < panel11ContentContainer.getComponentCount(); i++) {
                    Rectangle bounds = panel11ContentContainer.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = panel11ContentContainer.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  panel11ContentContainer.setMinimumSize(preferredSize);
                  panel11ContentContainer.setPreferredSize(preferredSize);
                }
              }
              panel10.add(panel11);
              panel11.setBounds(500, 294, 475, 96);

              //======== panel8 ========
              {
                panel8.setTitle("Modules");
                panel8.setBorder(new DropShadowBorder());
                panel8.setName("panel8");
                Container panel8ContentContainer = panel8.getContentContainer();
                panel8ContentContainer.setLayout(null);

                //---- SCCGM ----
                SCCGM.setText("Comptabilit\u00e9 (CGM)");
                SCCGM.setName("SCCGM");
                panel8ContentContainer.add(SCCGM);
                SCCGM.setBounds(20, 35, 145, SCCGM.getPreferredSize().height);

                //---- SCGAM ----
                SCGAM.setText("Achats (GAM)");
                SCGAM.setName("SCGAM");
                panel8ContentContainer.add(SCGAM);
                SCGAM.setBounds(170, 10, 145, SCGAM.getPreferredSize().height);

                //---- SCPAM ----
                SCPAM.setText("Paye (PAM)");
                SCPAM.setName("SCPAM");
                panel8ContentContainer.add(SCPAM);
                SCPAM.setBounds(170, 35, 145, SCPAM.getPreferredSize().height);

                //---- SCS11 ----
                SCS11.setText("Editions (SIM)");
                SCS11.setName("SCS11");
                panel8ContentContainer.add(SCS11);
                SCS11.setBounds(320, 35, 145, SCS11.getPreferredSize().height);

                //---- SCGVM ----
                SCGVM.setText("Ventes (GVM)");
                SCGVM.setName("SCGVM");
                panel8ContentContainer.add(SCGVM);
                SCGVM.setBounds(20, 10, 145, SCGVM.getPreferredSize().height);

                //---- SCGPM ----
                SCGPM.setText("Production (GPM)");
                SCGPM.setName("SCGPM");
                panel8ContentContainer.add(SCGPM);
                SCGPM.setBounds(320, 10, 145, SCGPM.getPreferredSize().height);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < panel8ContentContainer.getComponentCount(); i++) {
                    Rectangle bounds = panel8ContentContainer.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = panel8ContentContainer.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  panel8ContentContainer.setMinimumSize(preferredSize);
                  panel8ContentContainer.setPreferredSize(preferredSize);
                }
              }
              panel10.add(panel8);
              panel8.setBounds(500, 394, 475, 96);

              //======== panel9 ========
              {
                panel9.setTitle("Divers");
                panel9.setBorder(new DropShadowBorder());
                panel9.setName("panel9");
                Container panel9ContentContainer = panel9.getContentContainer();
                panel9ContentContainer.setLayout(null);

                //---- OBJ_185 ----
                OBJ_185.setText("Nombre de sessions");
                OBJ_185.setName("OBJ_185");
                panel9ContentContainer.add(OBJ_185);
                OBJ_185.setBounds(10, 9, 125, 20);

                //---- SCNBU ----
                SCNBU.setComponentPopupMenu(BTD);
                SCNBU.setName("SCNBU");
                panel9ContentContainer.add(SCNBU);
                SCNBU.setBounds(149, 5, 44, SCNBU.getPreferredSize().height);

                //---- SCVGM ----
                SCVGM.setComponentPopupMenu(BTD);
                SCVGM.setName("SCVGM");
                panel9ContentContainer.add(SCVGM);
                SCVGM.setBounds(421, 5, 44, SCVGM.getPreferredSize().height);

                //---- OBJ_189 ----
                OBJ_189.setText("Nombre de sessions graphiques");
                OBJ_189.setName("OBJ_189");
                panel9ContentContainer.add(OBJ_189);
                OBJ_189.setBounds(225, 9, 195, 20);

                //---- OBJ_190 ----
                OBJ_190.setText("Nombre de bulletins");
                OBJ_190.setName("OBJ_190");
                panel9ContentContainer.add(OBJ_190);
                OBJ_190.setBounds(10, 39, 125, 20);

                //---- SCBUL ----
                SCBUL.setComponentPopupMenu(BTD);
                SCBUL.setName("SCBUL");
                panel9ContentContainer.add(SCBUL);
                SCBUL.setBounds(125, 35, 68, SCBUL.getPreferredSize().height);

                //---- OBJ_191 ----
                OBJ_191.setText("Effectif");
                OBJ_191.setName("OBJ_191");
                panel9ContentContainer.add(OBJ_191);
                OBJ_191.setBounds(200, 39, 45, 20);

                //---- SCEFF ----
                SCEFF.setComponentPopupMenu(BTD);
                SCEFF.setName("SCEFF");
                panel9ContentContainer.add(SCEFF);
                SCEFF.setBounds(245, 35, 48, SCEFF.getPreferredSize().height);

                //---- OBJ_192 ----
                OBJ_192.setText("Module compl\u00e9mentaire");
                OBJ_192.setName("OBJ_192");
                panel9ContentContainer.add(OBJ_192);
                OBJ_192.setBounds(305, 39, 140, 20);

                //---- SCNMC ----
                SCNMC.setComponentPopupMenu(BTD);
                SCNMC.setName("SCNMC");
                panel9ContentContainer.add(SCNMC);
                SCNMC.setBounds(445, 35, 20, SCNMC.getPreferredSize().height);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < panel9ContentContainer.getComponentCount(); i++) {
                    Rectangle bounds = panel9ContentContainer.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = panel9ContentContainer.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  panel9ContentContainer.setMinimumSize(preferredSize);
                  panel9ContentContainer.setPreferredSize(preferredSize);
                }
              }
              panel10.add(panel9);
              panel9.setBounds(500, 494, 475, 96);

              //======== OBJ_65 ========
              {
                OBJ_65.setPreferredSize(new Dimension(310, 180));
                OBJ_65.setOpaque(false);
                OBJ_65.setBorder(new TitledBorder(""));
                OBJ_65.setName("OBJ_65");
                OBJ_65.setLayout(null);

                //---- CLNOM ----
                CLNOM.setBackground(Color.white);
                CLNOM.setFont(CLNOM.getFont().deriveFont(CLNOM.getFont().getStyle() | Font.BOLD, CLNOM.getFont().getSize() + 3f));
                CLNOM.setName("CLNOM");
                OBJ_65.add(CLNOM);
                CLNOM.setBounds(250, 14, 685, CLNOM.getPreferredSize().height);

                //---- CLCLI ----
                CLCLI.setFont(CLCLI.getFont().deriveFont(CLCLI.getFont().getStyle() | Font.BOLD, CLCLI.getFont().getSize() + 3f));
                CLCLI.setName("CLCLI");
                OBJ_65.add(CLCLI);
                CLCLI.setBounds(95, 14, 80, CLCLI.getPreferredSize().height);

                //---- CLLIV ----
                CLLIV.setFont(CLLIV.getFont().deriveFont(CLLIV.getFont().getStyle() | Font.BOLD, CLLIV.getFont().getSize() + 3f));
                CLLIV.setName("CLLIV");
                OBJ_65.add(CLLIV);
                CLLIV.setBounds(185, 14, 45, CLLIV.getPreferredSize().height);

                //---- OBJ_140 ----
                OBJ_140.setText("Version du logiciel");
                OBJ_140.setName("OBJ_140");
                OBJ_65.add(OBJ_140);
                OBJ_140.setBounds(20, 54, 115, 20);

                //---- SCTSM ----
                SCTSM.setComponentPopupMenu(BTD);
                SCTSM.setName("SCTSM");
                OBJ_65.add(SCTSM);
                SCTSM.setBounds(185, 50, 44, SCTSM.getPreferredSize().height);

                //---- OBJ_141 ----
                OBJ_141.setText("Observations");
                OBJ_141.setName("OBJ_141");
                OBJ_65.add(OBJ_141);
                OBJ_141.setBounds(250, 54, 85, 20);

                //---- SCOBS ----
                SCOBS.setComponentPopupMenu(BTD);
                SCOBS.setName("SCOBS");
                OBJ_65.add(SCOBS);
                SCOBS.setBounds(335, 50, 214, SCOBS.getPreferredSize().height);

                //---- OBJ_143 ----
                OBJ_143.setText("Secteur d'activit\u00e9");
                OBJ_143.setName("OBJ_143");
                OBJ_65.add(OBJ_143);
                OBJ_143.setBounds(20, 84, 150, 20);

                //---- SCACT ----
                SCACT.setComponentPopupMenu(BTD);
                SCACT.setName("SCACT");
                OBJ_65.add(SCACT);
                SCACT.setBounds(185, 80, 365, SCACT.getPreferredSize().height);

                //---- OBJ_148 ----
                OBJ_148.setText("CAF (en M d'\u20ac)");
                OBJ_148.setName("OBJ_148");
                OBJ_65.add(OBJ_148);
                OBJ_148.setBounds(580, 54, 95, 20);

                //---- SCCAF ----
                SCCAF.setComponentPopupMenu(BTD);
                SCCAF.setName("SCCAF");
                OBJ_65.add(SCCAF);
                SCCAF.setBounds(678, 50, 64, SCCAF.getPreferredSize().height);

                //---- OBJ_149 ----
                OBJ_149.setText("ASP");
                OBJ_149.setName("OBJ_149");
                OBJ_65.add(OBJ_149);
                OBJ_149.setBounds(810, 54, 55, 20);

                //---- SCBASP ----
                SCBASP.setComponentPopupMenu(BTD);
                SCBASP.setName("SCBASP");
                OBJ_65.add(SCBASP);
                SCBASP.setBounds(871, 50, 64, SCBASP.getPreferredSize().height);

                //---- label1 ----
                label1.setText("Client");
                label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD, label1.getFont().getSize() + 3f));
                label1.setName("label1");
                OBJ_65.add(label1);
                label1.setBounds(20, 15, 75, 32);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < OBJ_65.getComponentCount(); i++) {
                    Rectangle bounds = OBJ_65.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = OBJ_65.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  OBJ_65.setMinimumSize(preferredSize);
                  OBJ_65.setPreferredSize(preferredSize);
                }
              }
              panel10.add(OBJ_65);
              OBJ_65.setBounds(10, 5, 965, 119);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel10.getComponentCount(); i++) {
                  Rectangle bounds = panel10.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel10.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel10.setMinimumSize(preferredSize);
                panel10.setPreferredSize(preferredSize);
              }
            }
            layeredPane1.add(panel10, JLayeredPane.DEFAULT_LAYER);
            panel10.setBounds(10, 10, 980, 600);

            //======== p_desac ========
            {
              p_desac.setOpaque(false);
              p_desac.setPreferredSize(new Dimension(950, 650));
              p_desac.setName("p_desac");
              p_desac.setLayout(null);

              //---- lab_desac ----
              lab_desac.setName("lab_desac");
              p_desac.add(lab_desac);
              lab_desac.setBounds(200, 100, 670, 435);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < p_desac.getComponentCount(); i++) {
                  Rectangle bounds = p_desac.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = p_desac.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                p_desac.setMinimumSize(preferredSize);
                p_desac.setPreferredSize(preferredSize);
              }
            }
            layeredPane1.add(p_desac, JLayeredPane.DEFAULT_LAYER);
            p_desac.setBounds(0, 0, 965, 595);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addComponent(layeredPane1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addComponent(layeredPane1, GroupLayout.PREFERRED_SIZE, 612, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //---- TCI1 ----
    TCI1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI1.setBorder(null);
    TCI1.setName("TCI1");
    TCI1.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI1ActionPerformed(e);
      }
    });

    //---- TCI2 ----
    TCI2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI2.setBorder(null);
    TCI2.setName("TCI2");
    TCI2.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI2ActionPerformed(e);
      }
    });

    //---- TCI3 ----
    TCI3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI3.setBorder(null);
    TCI3.setName("TCI3");
    TCI3.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI3ActionPerformed(e);
      }
    });

    //---- TCI4 ----
    TCI4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI4.setBorder(null);
    TCI4.setName("TCI4");
    TCI4.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI4ActionPerformed(e);
      }
    });

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- invite ----
      invite.setText("Choix possibles");
      invite.setName("invite");
      invite.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          inviteActionPerformed(e);
        }
      });
      BTD.add(invite);

      //---- aide ----
      aide.setText("Aide en ligne");
      aide.setName("aide");
      aide.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          aideActionPerformed(e);
        }
      });
      BTD.add(aide);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JPanel panel3;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu13;
  private RiSousMenu_bt riSousMenu_bt13;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JLayeredPane layeredPane1;
  private JPanel panel10;
  private JXTitledPanel panel2;
  private JLabel OBJ_139;
  private XRiTextField SCSYST;
  private JLabel OBJ_142;
  private XRiTextField SCRSC;
  private JLabel OBJ_144;
  private XRiTextField SCTCP;
  private JXTitledSeparator xTitledSeparator6;
  private JXTitledSeparator xTitledSeparator7;
  private XRiTextField SCTELN;
  private XRiTextField SCFTP;
  private XRiTextField SCPORT;
  private JLabel OBJ_157;
  private JLabel OBJ_158;
  private XRiTextField SCPORF;
  private JPanel panel1;
  private JLabel OBJ_159;
  private XRiTextField SCUSER;
  private JLabel OBJ_160;
  private XRiTextField SCPASS;
  private JXTitledPanel panel6;
  private JLabel OBJ_182;
  private XRiTextField SCCLS;
  private XRiTextField SCCAP;
  private JLabel label4;
  private JLabel label6;
  private JLabel label7;
  private XRiTextField SCNFS;
  private XRiCalendrier SCDRSX;
  private RiZoneSortie CLINOM;
  private RiZoneSortie CAPNOM;
  private JXTitledPanel panel7;
  private JLabel label8;
  private XRiComboBox SCTLCO;
  private XRiCheckBox SCQRY;
  private XRiCheckBox SCRONE;
  private XRiCheckBox SCFORM;
  private XRiCheckBox SCSPA;
  private XRiCheckBox SCSQL;
  private JXTitledPanel panel11;
  private JLabel OBJ_183;
  private XRiTextField SCSEN;
  private JLabel OBJ_187;
  private XRiTextField SCTMA;
  private JLabel OBJ_184;
  private XRiTextField SCVOS;
  private JLabel OBJ_188;
  private XRiCalendrier SCDAFX;
  private JXTitledPanel panel8;
  private XRiCheckBox SCCGM;
  private XRiCheckBox SCGAM;
  private XRiCheckBox SCPAM;
  private XRiCheckBox SCS11;
  private XRiCheckBox SCGVM;
  private XRiCheckBox SCGPM;
  private JXTitledPanel panel9;
  private JLabel OBJ_185;
  private XRiTextField SCNBU;
  private XRiTextField SCVGM;
  private JLabel OBJ_189;
  private JLabel OBJ_190;
  private XRiTextField SCBUL;
  private JLabel OBJ_191;
  private XRiTextField SCEFF;
  private JLabel OBJ_192;
  private XRiTextField SCNMC;
  private JPanel OBJ_65;
  private XRiTextField CLNOM;
  private XRiTextField CLCLI;
  private XRiTextField CLLIV;
  private JLabel OBJ_140;
  private XRiTextField SCTSM;
  private JLabel OBJ_141;
  private XRiTextField SCOBS;
  private JLabel OBJ_143;
  private XRiTextField SCACT;
  private JLabel OBJ_148;
  private XRiTextField SCCAF;
  private JLabel OBJ_149;
  private XRiTextField SCBASP;
  private JLabel label1;
  private JPanel p_desac;
  private JLabel lab_desac;
  private SNBoutonDetail TCI1;
  private SNBoutonDetail TCI2;
  private SNBoutonDetail TCI3;
  private SNBoutonDetail TCI4;
  private JPopupMenu BTD;
  private JMenuItem invite;
  private JMenuItem aide;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
