
package ri.serien.libecranrpg.vspm.VSPM05FM;
// MON PACKAGE

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VSPM05FM_SAV extends SNPanelEcranRPG implements ioFrame {
   
  
  // private oFrame master=null;
  
  public VSPM05FM_SAV(SNPanelEcranRPG parent) {
    super(parent);
    // master = parent;
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    CAPNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAPNOM@")).trim());
    CLINOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLINOM@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // icones
    bouton_valider.setIcon(lexique.chargerImage("images/OK_p.png", true));
    bouton_retour.setIcon(lexique.chargerImage("images/retour_p.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Client RI - Service Après Vente"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  @Override
  public void dispose() {
    getJDialog().dispose();
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    closePopupLinkWithBuffer(true);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    getData();
    closePopupLinkWithBuffer(true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel8 = new JPanel();
    CAPNOM = new RiZoneSortie();
    SCCLS = new XRiTextField();
    SCNFS = new XRiTextField();
    label6 = new JLabel();
    SCDRSX = new XRiCalendrier();
    label7 = new JLabel();
    OBJ_182 = new JLabel();
    CLINOM = new RiZoneSortie();
    label4 = new JLabel();
    SCCAP = new XRiTextField();
    OBJ_183 = new JLabel();
    SCMTS = new XRiTextField();
    label8 = new JLabel();
    SCDBSX = new XRiCalendrier();
    label9 = new JLabel();
    SCDDEX = new XRiCalendrier();

    //======== this ========
    setMinimumSize(new Dimension(795, 355));
    setPreferredSize(new Dimension(795, 325));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setPreferredSize(new Dimension(955, 215));
      p_principal.setMinimumSize(new Dimension(955, 215));
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel8 ========
        {
          panel8.setBorder(new TitledBorder("Service Apr\u00e8s Vente"));
          panel8.setOpaque(false);
          panel8.setName("panel8");
          panel8.setLayout(null);

          //---- CAPNOM ----
          CAPNOM.setText("@CAPNOM@");
          CAPNOM.setName("CAPNOM");
          panel8.add(CAPNOM);
          CAPNOM.setBounds(240, 242, 314, CAPNOM.getPreferredSize().height);

          //---- SCCLS ----
          SCCLS.setComponentPopupMenu(null);
          SCCLS.setName("SCCLS");
          panel8.add(SCCLS);
          SCCLS.setBounds(165, 30, 68, SCCLS.getPreferredSize().height);

          //---- SCNFS ----
          SCNFS.setName("SCNFS");
          panel8.add(SCNFS);
          SCNFS.setBounds(165, 100, 68, SCNFS.getPreferredSize().height);

          //---- label6 ----
          label6.setText("Derni\u00e8re facture");
          label6.setName("label6");
          panel8.add(label6);
          label6.setBounds(20, 101, 145, 26);

          //---- SCDRSX ----
          SCDRSX.setName("SCDRSX");
          panel8.add(SCDRSX);
          SCDRSX.setBounds(165, 205, 110, SCDRSX.getPreferredSize().height);

          //---- label7 ----
          label7.setText("Renouvellement");
          label7.setName("label7");
          panel8.add(label7);
          label7.setBounds(20, 206, 145, 26);

          //---- OBJ_182 ----
          OBJ_182.setText("Num\u00e9ro de client");
          OBJ_182.setName("OBJ_182");
          panel8.add(OBJ_182);
          OBJ_182.setBounds(20, 34, 145, 20);

          //---- CLINOM ----
          CLINOM.setText("@CLINOM@");
          CLINOM.setName("CLINOM");
          panel8.add(CLINOM);
          CLINOM.setBounds(240, 32, 314, CLINOM.getPreferredSize().height);

          //---- label4 ----
          label4.setText("Chef de projet CAP");
          label4.setName("label4");
          panel8.add(label4);
          label4.setBounds(20, 241, 145, 26);

          //---- SCCAP ----
          SCCAP.setComponentPopupMenu(null);
          SCCAP.setName("SCCAP");
          panel8.add(SCCAP);
          SCCAP.setBounds(165, 240, 44, SCCAP.getPreferredSize().height);

          //---- OBJ_183 ----
          OBJ_183.setText("Montant du contrat");
          OBJ_183.setName("OBJ_183");
          panel8.add(OBJ_183);
          OBJ_183.setBounds(20, 69, 145, 20);

          //---- SCMTS ----
          SCMTS.setComponentPopupMenu(null);
          SCMTS.setName("SCMTS");
          panel8.add(SCMTS);
          SCMTS.setBounds(165, 65, 84, SCMTS.getPreferredSize().height);

          //---- label8 ----
          label8.setText("Date de d\u00e9but");
          label8.setName("label8");
          panel8.add(label8);
          label8.setBounds(20, 136, 145, 26);

          //---- SCDBSX ----
          SCDBSX.setName("SCDBSX");
          panel8.add(SCDBSX);
          SCDBSX.setBounds(165, 135, 110, SCDBSX.getPreferredSize().height);

          //---- label9 ----
          label9.setText("Date de d\u00e9marrage");
          label9.setName("label9");
          panel8.add(label9);
          label9.setBounds(20, 171, 145, 26);

          //---- SCDDEX ----
          SCDDEX.setName("SCDDEX");
          panel8.add(SCDDEX);
          SCDDEX.setBounds(165, 170, 110, SCDDEX.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel8.getComponentCount(); i++) {
              Rectangle bounds = panel8.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel8.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel8.setMinimumSize(preferredSize);
            panel8.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel8, GroupLayout.DEFAULT_SIZE, 601, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel8, GroupLayout.DEFAULT_SIZE, 299, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel8;
  private RiZoneSortie CAPNOM;
  private XRiTextField SCCLS;
  private XRiTextField SCNFS;
  private JLabel label6;
  private XRiCalendrier SCDRSX;
  private JLabel label7;
  private JLabel OBJ_182;
  private RiZoneSortie CLINOM;
  private JLabel label4;
  private XRiTextField SCCAP;
  private JLabel OBJ_183;
  private XRiTextField SCMTS;
  private JLabel label8;
  private XRiCalendrier SCDBSX;
  private JLabel label9;
  private XRiCalendrier SCDDEX;
  // JFormDesigner - End of variables declaration  //GEN-END:variables



}
