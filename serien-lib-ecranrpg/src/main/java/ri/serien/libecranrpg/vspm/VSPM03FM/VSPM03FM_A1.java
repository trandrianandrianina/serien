
package ri.serien.libecranrpg.vspm.VSPM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VSPM03FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VSPM03FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TIDX6.setValeurs("6", "RB");
    TIDX5.setValeurs("5", "RB");
    TIDX1.setValeurs("1", "RB");
    TIDX7.setValeurs("7", "RB");
    TIDX8.setValeurs("8", "RB");
    TIDX9.setValeurs("9", "RB");
    TIDX4.setValeurs("4", "RB");
    TIDX3.setValeurs("3", "RB");
    TIDX2.setValeurs("2", "RB");
    ARG70.setValeursSelection("X", " ");
    ARG79.setValeursSelection("X", " ");
    ARG78.setValeursSelection("X", " ");
    ARG77.setValeursSelection("X", " ");
    ARG76.setValeursSelection("X", " ");
    ARG75.setValeursSelection("X", " ");
    ARG74.setValeursSelection("X", " ");
    ARG73.setValeursSelection("X", " ");
    ARG72.setValeursSelection("X", " ");
    ARG71.setValeursSelection("X", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    barre_dup.setVisible(lexique.isTrue("56"));
    // TIDX1.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("1"));
    // TIDX2.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("2"));
    // TIDX3.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("3"));
    // TIDX4.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("4"));
    // TIDX5.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("5"));
    // TIDX6.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("6"));
    // TIDX7.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("7"));
    // TIDX8.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("8"));
    // TIDX9.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("9"));
    
    // ARG71.setSelected(lexique.HostFieldGetData("ARG71").equalsIgnoreCase("X"));
    // ARG72.setSelected(lexique.HostFieldGetData("ARG72").equalsIgnoreCase("X"));
    // ARG73.setSelected(lexique.HostFieldGetData("ARG73").equalsIgnoreCase("X"));
    // ARG74.setSelected(lexique.HostFieldGetData("ARG74").equalsIgnoreCase("X"));
    // ARG75.setSelected(lexique.HostFieldGetData("ARG75").equalsIgnoreCase("X"));
    // ARG76.setSelected(lexique.HostFieldGetData("ARG76").equalsIgnoreCase("X"));
    // ARG77.setSelected(lexique.HostFieldGetData("ARG77").equalsIgnoreCase("X"));
    // ARG78.setSelected(lexique.HostFieldGetData("ARG78").equalsIgnoreCase("X"));
    // ARG79.setSelected(lexique.HostFieldGetData("ARG79").equalsIgnoreCase("X"));
    // ARG70.setSelected(lexique.HostFieldGetData("ARG70").equalsIgnoreCase("X"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  FICHE REPRESENTANT"));
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (TIDX1.isSelected())
    // lexique.HostFieldPutData("RB", 0, "1");
    // if (TIDX2.isSelected())
    // lexique.HostFieldPutData("RB", 0, "2");
    // if (TIDX3.isSelected())
    // lexique.HostFieldPutData("RB", 0, "3");
    // if (TIDX4.isSelected())
    // lexique.HostFieldPutData("RB", 0, "4");
    // if (TIDX5.isSelected())
    // lexique.HostFieldPutData("RB", 0, "5");
    // if (TIDX6.isSelected())
    // lexique.HostFieldPutData("RB", 0, "6");
    // if (TIDX7.isSelected())
    // lexique.HostFieldPutData("RB", 0, "7");
    // if (TIDX8.isSelected())
    // lexique.HostFieldPutData("RB", 0, "8");
    // if (TIDX9.isSelected())
    // lexique.HostFieldPutData("RB", 0, "9");
    
    // if (ARG71.isSelected())
    // lexique.HostFieldPutData("ARG71", 0, "X");
    // else
    // lexique.HostFieldPutData("ARG71", 0, " ");
    
    // if (ARG72.isSelected())
    // lexique.HostFieldPutData("ARG72", 0, "X");
    // else
    // lexique.HostFieldPutData("ARG72", 0, " ");
    
    // if (ARG73.isSelected())
    // lexique.HostFieldPutData("ARG73", 0, "X");
    // else
    // lexique.HostFieldPutData("ARG73", 0, " ");
    
    // if (ARG74.isSelected())
    // lexique.HostFieldPutData("ARG74", 0, "X");
    // else
    // lexique.HostFieldPutData("ARG74", 0, " ");
    
    // if (ARG75.isSelected())
    // lexique.HostFieldPutData("ARG75", 0, "X");
    // else
    // lexique.HostFieldPutData("ARG75", 0, " ");
    
    // if (ARG76.isSelected())
    // lexique.HostFieldPutData("ARG76", 0, "X");
    // else
    // lexique.HostFieldPutData("ARG76", 0, " ");
    
    // if (ARG77.isSelected())
    // lexique.HostFieldPutData("ARG77", 0, "X");
    // else
    // lexique.HostFieldPutData("ARG77", 0, " ");
    
    // if (ARG78.isSelected())
    // lexique.HostFieldPutData("ARG78", 0, "X");
    // else
    // lexique.HostFieldPutData("ARG78", 0, " ");
    
    // if (ARG79.isSelected())
    // lexique.HostFieldPutData("ARG79", 0, "X");
    // else
    // lexique.HostFieldPutData("ARG79", 0, " ");
    
    // if (ARG70.isSelected())
    // lexique.HostFieldPutData("ARG70", 0, "X");
    // else
    // lexique.HostFieldPutData("ARG70", 0, " ");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD2.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_84_OBJ_84 = new JLabel();
    INDETB = new XRiTextField();
    OBJECT_4 = new JLabel();
    INDCLI = new XRiTextField();
    INDLIV = new XRiTextField();
    p_tete_droite = new JPanel();
    barre_dup = new JMenuBar();
    p_dup_gauche = new JPanel();
    OBJ_84_OBJ_85 = new JLabel();
    IN3ETB = new XRiTextField();
    OBJECT_5 = new JLabel();
    IN3CLI = new XRiTextField();
    IN3LIV = new XRiTextField();
    p_dup_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    TIDX2 = new XRiRadioButton();
    ARG2 = new XRiTextField();
    TIDX3 = new XRiRadioButton();
    ARG3 = new XRiTextField();
    TIDX4 = new XRiRadioButton();
    ARG4 = new XRiTextField();
    TIDX9 = new XRiRadioButton();
    TIDX8 = new XRiRadioButton();
    ARG5 = new XRiTextField();
    TIDX7 = new XRiRadioButton();
    ARG8 = new XRiTextField();
    TIDX1 = new XRiRadioButton();
    TIDX5 = new XRiRadioButton();
    ARG1 = new XRiTextField();
    TIDX6 = new XRiRadioButton();
    ARG6 = new XRiTextField();
    ARG41 = new XRiTextField();
    ARG9 = new XRiTextField();
    ARG91 = new XRiTextField();
    ARG81 = new XRiTextField();
    ARG71 = new XRiCheckBox();
    ARG72 = new XRiCheckBox();
    ARG73 = new XRiCheckBox();
    ARG74 = new XRiCheckBox();
    ARG75 = new XRiCheckBox();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    ARG76 = new XRiCheckBox();
    ARG77 = new XRiCheckBox();
    ARG78 = new XRiCheckBox();
    ARG79 = new XRiCheckBox();
    ARG70 = new XRiCheckBox();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();
    label11 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    BTD2 = new JPopupMenu();
    OBJ_20 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Clients S\u00e9rie N");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 30));
          p_tete_gauche.setMinimumSize(new Dimension(700, 30));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_84_OBJ_84 ----
          OBJ_84_OBJ_84.setText("Etablissement");
          OBJ_84_OBJ_84.setName("OBJ_84_OBJ_84");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD2);
          INDETB.setName("INDETB");

          //---- OBJECT_4 ----
          OBJECT_4.setText("Client");
          OBJECT_4.setName("OBJECT_4");

          //---- INDCLI ----
          INDCLI.setName("INDCLI");

          //---- INDLIV ----
          INDLIV.setName("INDLIV");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_84_OBJ_84, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(60, 60, 60)
                .addComponent(OBJECT_4)
                .addGap(13, 13, 13)
                .addComponent(INDCLI, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addComponent(INDLIV, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_84_OBJ_84, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJECT_4))
              .addComponent(INDCLI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDLIV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);

      //======== barre_dup ========
      {
        barre_dup.setMinimumSize(new Dimension(111, 34));
        barre_dup.setPreferredSize(new Dimension(111, 34));
        barre_dup.setName("barre_dup");

        //======== p_dup_gauche ========
        {
          p_dup_gauche.setOpaque(false);
          p_dup_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_dup_gauche.setPreferredSize(new Dimension(700, 30));
          p_dup_gauche.setMinimumSize(new Dimension(700, 30));
          p_dup_gauche.setName("p_dup_gauche");

          //---- OBJ_84_OBJ_85 ----
          OBJ_84_OBJ_85.setText("Par duplication de");
          OBJ_84_OBJ_85.setName("OBJ_84_OBJ_85");

          //---- IN3ETB ----
          IN3ETB.setComponentPopupMenu(BTD2);
          IN3ETB.setName("IN3ETB");

          //---- OBJECT_5 ----
          OBJECT_5.setText("Client");
          OBJECT_5.setName("OBJECT_5");

          //---- IN3CLI ----
          IN3CLI.setName("IN3CLI");

          //---- IN3LIV ----
          IN3LIV.setName("IN3LIV");

          GroupLayout p_dup_gaucheLayout = new GroupLayout(p_dup_gauche);
          p_dup_gauche.setLayout(p_dup_gaucheLayout);
          p_dup_gaucheLayout.setHorizontalGroup(
            p_dup_gaucheLayout.createParallelGroup()
              .addGroup(p_dup_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_84_OBJ_85, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                .addComponent(IN3ETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(60, 60, 60)
                .addComponent(OBJECT_5)
                .addGap(13, 13, 13)
                .addComponent(IN3CLI, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addComponent(IN3LIV, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE))
          );
          p_dup_gaucheLayout.setVerticalGroup(
            p_dup_gaucheLayout.createParallelGroup()
              .addGroup(p_dup_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_84_OBJ_85, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(IN3ETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_dup_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJECT_5))
              .addComponent(IN3CLI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(IN3LIV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_dup.add(p_dup_gauche);

        //======== p_dup_droite ========
        {
          p_dup_droite.setOpaque(false);
          p_dup_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_dup_droite.setPreferredSize(new Dimension(150, 0));
          p_dup_droite.setMinimumSize(new Dimension(150, 0));
          p_dup_droite.setName("p_dup_droite");
          p_dup_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_dup.add(p_dup_droite);
      }
      p_nord.add(barre_dup);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(800, 460));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Recherche multi-crit\u00e8res"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- TIDX2 ----
            TIDX2.setText("Code alphab\u00e9tique");
            TIDX2.setComponentPopupMenu(BTD2);
            TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX2.setToolTipText("Tri sur cet argument");
            TIDX2.setName("TIDX2");
            panel1.add(TIDX2);
            TIDX2.setBounds(36, 44, 180, 20);

            //---- ARG2 ----
            ARG2.setComponentPopupMenu(BTD2);
            ARG2.setName("ARG2");
            panel1.add(ARG2);
            ARG2.setBounds(265, 40, 185, ARG2.getPreferredSize().height);

            //---- TIDX3 ----
            TIDX3.setText("D\u00e9partement ou code postal");
            TIDX3.setComponentPopupMenu(BTD2);
            TIDX3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX3.setToolTipText("Tri sur cet argument");
            TIDX3.setName("TIDX3");
            panel1.add(TIDX3);
            TIDX3.setBounds(36, 84, 200, 20);

            //---- ARG3 ----
            ARG3.setComponentPopupMenu(BTD2);
            ARG3.setName("ARG3");
            panel1.add(ARG3);
            ARG3.setBounds(265, 80, 55, ARG3.getPreferredSize().height);

            //---- TIDX4 ----
            TIDX4.setText("Repr\u00e9sentants 1 ET 2");
            TIDX4.setComponentPopupMenu(BTD2);
            TIDX4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX4.setToolTipText("Tri sur cet argument");
            TIDX4.setName("TIDX4");
            panel1.add(TIDX4);
            TIDX4.setBounds(36, 124, 200, 20);

            //---- ARG4 ----
            ARG4.setComponentPopupMenu(BTD2);
            ARG4.setName("ARG4");
            panel1.add(ARG4);
            ARG4.setBounds(265, 120, 30, ARG4.getPreferredSize().height);

            //---- TIDX9 ----
            TIDX9.setText("Repr\u00e9sentants 1 OU 2");
            TIDX9.setComponentPopupMenu(BTD2);
            TIDX9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX9.setToolTipText("Tri sur cet argument");
            TIDX9.setName("TIDX9");
            panel1.add(TIDX9);
            TIDX9.setBounds(35, 164, 195, 20);

            //---- TIDX8 ----
            TIDX8.setText("Version de S\u00e9rie N");
            TIDX8.setComponentPopupMenu(BTD2);
            TIDX8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX8.setToolTipText("Tri sur cet argument");
            TIDX8.setName("TIDX8");
            panel1.add(TIDX8);
            TIDX8.setBounds(35, 244, 190, 20);

            //---- ARG5 ----
            ARG5.setComponentPopupMenu(BTD);
            ARG5.setName("ARG5");
            panel1.add(ARG5);
            ARG5.setBounds(265, 200, 40, ARG5.getPreferredSize().height);

            //---- TIDX7 ----
            TIDX7.setText("Modules");
            TIDX7.setComponentPopupMenu(BTD2);
            TIDX7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX7.setToolTipText("Tri sur cet argument");
            TIDX7.setName("TIDX7");
            panel1.add(TIDX7);
            TIDX7.setBounds(35, 279, 180, 20);

            //---- ARG8 ----
            ARG8.setComponentPopupMenu(BTD2);
            ARG8.setName("ARG8");
            panel1.add(ARG8);
            ARG8.setBounds(265, 240, 40, ARG8.getPreferredSize().height);

            //---- TIDX1 ----
            TIDX1.setText("Num\u00e9ro client");
            TIDX1.setComponentPopupMenu(BTD2);
            TIDX1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX1.setToolTipText("Tri sur cet argument");
            TIDX1.setName("TIDX1");
            panel1.add(TIDX1);
            TIDX1.setBounds(35, 384, 180, 20);

            //---- TIDX5 ----
            TIDX5.setText("Cat\u00e9gorie");
            TIDX5.setComponentPopupMenu(BTD2);
            TIDX5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX5.setToolTipText("Tri sur cet argument");
            TIDX5.setName("TIDX5");
            panel1.add(TIDX5);
            TIDX5.setBounds(35, 204, 180, 20);

            //---- ARG1 ----
            ARG1.setComponentPopupMenu(BTD);
            ARG1.setName("ARG1");
            panel1.add(ARG1);
            ARG1.setBounds(265, 380, 60, ARG1.getPreferredSize().height);

            //---- TIDX6 ----
            TIDX6.setText("Date de vente du serveur");
            TIDX6.setComponentPopupMenu(BTD2);
            TIDX6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX6.setToolTipText("Tri sur cet argument");
            TIDX6.setName("TIDX6");
            panel1.add(TIDX6);
            TIDX6.setBounds(35, 344, 180, 20);

            //---- ARG6 ----
            ARG6.setComponentPopupMenu(BTD);
            ARG6.setName("ARG6");
            panel1.add(ARG6);
            ARG6.setBounds(265, 340, 65, ARG6.getPreferredSize().height);

            //---- ARG41 ----
            ARG41.setComponentPopupMenu(BTD2);
            ARG41.setName("ARG41");
            panel1.add(ARG41);
            ARG41.setBounds(344, 120, 30, ARG41.getPreferredSize().height);

            //---- ARG9 ----
            ARG9.setComponentPopupMenu(BTD2);
            ARG9.setName("ARG9");
            panel1.add(ARG9);
            ARG9.setBounds(265, 160, 30, ARG9.getPreferredSize().height);

            //---- ARG91 ----
            ARG91.setComponentPopupMenu(BTD2);
            ARG91.setName("ARG91");
            panel1.add(ARG91);
            ARG91.setBounds(344, 160, 30, ARG91.getPreferredSize().height);

            //---- ARG81 ----
            ARG81.setComponentPopupMenu(BTD2);
            ARG81.setName("ARG81");
            panel1.add(ARG81);
            ARG81.setBounds(344, 240, 40, ARG81.getPreferredSize().height);

            //---- ARG71 ----
            ARG71.setText("Compta");
            ARG71.setToolTipText("module CGM");
            ARG71.setName("ARG71");
            panel1.add(ARG71);
            ARG71.setBounds(265, 280, 75, ARG71.getPreferredSize().height);

            //---- ARG72 ----
            ARG72.setText("Ventes");
            ARG72.setToolTipText("Modules GVM et GAM");
            ARG72.setName("ARG72");
            panel1.add(ARG72);
            ARG72.setBounds(363, 280, 75, ARG72.getPreferredSize().height);

            //---- ARG73 ----
            ARG73.setText("Paie");
            ARG73.setToolTipText("Module PAM");
            ARG73.setName("ARG73");
            panel1.add(ARG73);
            ARG73.setBounds(461, 280, 75, ARG73.getPreferredSize().height);

            //---- ARG74 ----
            ARG74.setText("Product.");
            ARG74.setToolTipText("Module GPM");
            ARG74.setName("ARG74");
            panel1.add(ARG74);
            ARG74.setBounds(559, 280, 75, ARG74.getPreferredSize().height);

            //---- ARG75 ----
            ARG75.setText("Editions");
            ARG75.setToolTipText("Module SIM");
            ARG75.setName("ARG75");
            panel1.add(ARG75);
            ARG75.setBounds(657, 280, 75, ARG75.getPreferredSize().height);

            //---- label1 ----
            label1.setText("et");
            label1.setHorizontalAlignment(SwingConstants.CENTER);
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(344, 281, 15, label1.getPreferredSize().height);

            //---- label2 ----
            label2.setText("et");
            label2.setHorizontalAlignment(SwingConstants.CENTER);
            label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
            label2.setName("label2");
            panel1.add(label2);
            label2.setBounds(442, 281, 15, label2.getPreferredSize().height);

            //---- label3 ----
            label3.setText("et");
            label3.setHorizontalAlignment(SwingConstants.CENTER);
            label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
            label3.setName("label3");
            panel1.add(label3);
            label3.setBounds(540, 281, 15, label3.getPreferredSize().height);

            //---- label4 ----
            label4.setText("et");
            label4.setHorizontalAlignment(SwingConstants.CENTER);
            label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
            label4.setName("label4");
            panel1.add(label4);
            label4.setBounds(638, 281, 15, label4.getPreferredSize().height);

            //---- ARG76 ----
            ARG76.setText("Compta");
            ARG76.setToolTipText("module CGM");
            ARG76.setName("ARG76");
            panel1.add(ARG76);
            ARG76.setBounds(265, 310, 75, ARG76.getPreferredSize().height);

            //---- ARG77 ----
            ARG77.setText("Ventes");
            ARG77.setToolTipText("Modules GVM et GAM");
            ARG77.setName("ARG77");
            panel1.add(ARG77);
            ARG77.setBounds(363, 310, 75, ARG77.getPreferredSize().height);

            //---- ARG78 ----
            ARG78.setText("Paie");
            ARG78.setToolTipText("Module PAM");
            ARG78.setName("ARG78");
            panel1.add(ARG78);
            ARG78.setBounds(461, 310, 75, ARG78.getPreferredSize().height);

            //---- ARG79 ----
            ARG79.setText("Product.");
            ARG79.setToolTipText("Module GPM");
            ARG79.setName("ARG79");
            panel1.add(ARG79);
            ARG79.setBounds(559, 310, 75, ARG79.getPreferredSize().height);

            //---- ARG70 ----
            ARG70.setText("Editions");
            ARG70.setToolTipText("Module SIM");
            ARG70.setName("ARG70");
            panel1.add(ARG70);
            ARG70.setBounds(657, 310, 75, ARG70.getPreferredSize().height);

            //---- label5 ----
            label5.setText("ou");
            label5.setHorizontalAlignment(SwingConstants.CENTER);
            label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
            label5.setName("label5");
            panel1.add(label5);
            label5.setBounds(344, 311, 15, label5.getPreferredSize().height);

            //---- label6 ----
            label6.setText("ou");
            label6.setHorizontalAlignment(SwingConstants.CENTER);
            label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
            label6.setName("label6");
            panel1.add(label6);
            label6.setBounds(442, 311, 15, label6.getPreferredSize().height);

            //---- label7 ----
            label7.setText("ou");
            label7.setHorizontalAlignment(SwingConstants.CENTER);
            label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD));
            label7.setName("label7");
            panel1.add(label7);
            label7.setBounds(540, 311, 15, label7.getPreferredSize().height);

            //---- label8 ----
            label8.setText("ou");
            label8.setHorizontalAlignment(SwingConstants.CENTER);
            label8.setFont(label8.getFont().deriveFont(label8.getFont().getStyle() | Font.BOLD));
            label8.setName("label8");
            panel1.add(label8);
            label8.setBounds(638, 311, 15, label8.getPreferredSize().height);

            //---- label9 ----
            label9.setText("et");
            label9.setHorizontalAlignment(SwingConstants.CENTER);
            label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD));
            label9.setName("label9");
            panel1.add(label9);
            label9.setBounds(300, 126, 40, 16);

            //---- label10 ----
            label10.setText("ou");
            label10.setHorizontalAlignment(SwingConstants.CENTER);
            label10.setFont(label10.getFont().deriveFont(label10.getFont().getStyle() | Font.BOLD));
            label10.setName("label10");
            panel1.add(label10);
            label10.setBounds(300, 166, 40, label10.getPreferredSize().height);

            //---- label11 ----
            label11.setText("\u00e0");
            label11.setHorizontalAlignment(SwingConstants.CENTER);
            label11.setName("label11");
            panel1.add(label11);
            label11.setBounds(305, 246, 40, label11.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 774, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 432, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- OBJ_20 ----
      OBJ_20.setText("Aide en ligne");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_20);
    }

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(TIDX2);
    RB_GRP.add(TIDX3);
    RB_GRP.add(TIDX4);
    RB_GRP.add(TIDX9);
    RB_GRP.add(TIDX8);
    RB_GRP.add(TIDX7);
    RB_GRP.add(TIDX1);
    RB_GRP.add(TIDX5);
    RB_GRP.add(TIDX6);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_84_OBJ_84;
  private XRiTextField INDETB;
  private JLabel OBJECT_4;
  private XRiTextField INDCLI;
  private XRiTextField INDLIV;
  private JPanel p_tete_droite;
  private JMenuBar barre_dup;
  private JPanel p_dup_gauche;
  private JLabel OBJ_84_OBJ_85;
  private XRiTextField IN3ETB;
  private JLabel OBJECT_5;
  private XRiTextField IN3CLI;
  private XRiTextField IN3LIV;
  private JPanel p_dup_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiRadioButton TIDX2;
  private XRiTextField ARG2;
  private XRiRadioButton TIDX3;
  private XRiTextField ARG3;
  private XRiRadioButton TIDX4;
  private XRiTextField ARG4;
  private XRiRadioButton TIDX9;
  private XRiRadioButton TIDX8;
  private XRiTextField ARG5;
  private XRiRadioButton TIDX7;
  private XRiTextField ARG8;
  private XRiRadioButton TIDX1;
  private XRiRadioButton TIDX5;
  private XRiTextField ARG1;
  private XRiRadioButton TIDX6;
  private XRiTextField ARG6;
  private XRiTextField ARG41;
  private XRiTextField ARG9;
  private XRiTextField ARG91;
  private XRiTextField ARG81;
  private XRiCheckBox ARG71;
  private XRiCheckBox ARG72;
  private XRiCheckBox ARG73;
  private XRiCheckBox ARG74;
  private XRiCheckBox ARG75;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private XRiCheckBox ARG76;
  private XRiCheckBox ARG77;
  private XRiCheckBox ARG78;
  private XRiCheckBox ARG79;
  private XRiCheckBox ARG70;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private JLabel label10;
  private JLabel label11;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  private JPopupMenu BTD2;
  private JMenuItem OBJ_20;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
