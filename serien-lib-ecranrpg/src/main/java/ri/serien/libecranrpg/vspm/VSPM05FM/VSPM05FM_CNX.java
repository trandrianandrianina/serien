
package ri.serien.libecranrpg.vspm.VSPM05FM;
// MON PACKAGE

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VSPM05FM_CNX extends SNPanelEcranRPG implements ioFrame {
   
  
  // private oFrame master=null;
  
  public VSPM05FM_CNX(SNPanelEcranRPG parent) {
    super(parent);
    initComponents();
    setVersionLook(2);
    
    // master = parent;
    setDialog(true);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // icones
    bouton_valider.setIcon(lexique.chargerImage("images/OK_p.png", true));
    bouton_retour.setIcon(lexique.chargerImage("images/retour_p.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Client RI - Service Après Vente"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  @Override
  public void dispose() {
    getJDialog().dispose();
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    closePopupLinkWithBuffer(true);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    getData();
    closePopupLinkWithBuffer(true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    OBJ_139 = new JLabel();
    SCSYST = new XRiTextField();
    OBJ_142 = new JLabel();
    SCRSC = new XRiTextField();
    OBJ_144 = new JLabel();
    SCTCP = new XRiTextField();
    panel3 = new JPanel();
    OBJ_145 = new JLabel();
    SCIDLO = new XRiTextField();
    OBJ_146 = new JLabel();
    SCLIEU = new XRiTextField();
    OBJ_147 = new JLabel();
    SCMODE = new XRiTextField();
    panel4 = new JPanel();
    OBJ_150 = new JLabel();
    SCTMOD = new XRiTextField();
    OBJ_153 = new JLabel();
    SCNLMO = new XRiTextField();
    OBJ_155 = new JLabel();
    SCNCNX = new XRiTextField();
    panel5 = new JPanel();
    SCTELN = new XRiTextField();
    OBJ_157 = new JLabel();
    SCPORT = new XRiTextField();
    panel6 = new JPanel();
    SCFTP = new XRiTextField();
    OBJ_161 = new JLabel();
    SCPORF = new XRiTextField();
    panel1 = new JPanel();
    OBJ_159 = new JLabel();
    SCUSER = new XRiTextField();
    OBJ_160 = new JLabel();
    SCPASS = new XRiTextField();
    panel7 = new JPanel();
    SCOBS1 = new XRiTextField();
    SCOBS2 = new XRiTextField();
    SCOBS3 = new XRiTextField();
    panel8 = new JPanel();
    SCOBS4 = new XRiTextField();
    SCOBS5 = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(970, 695));
    setPreferredSize(new Dimension(970, 680));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setPreferredSize(new Dimension(955, 215));
      p_principal.setMinimumSize(new Dimension(955, 215));
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel2 ========
        {
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- OBJ_139 ----
          OBJ_139.setText("Syst\u00e8me");
          OBJ_139.setName("OBJ_139");
          panel2.add(OBJ_139);
          OBJ_139.setBounds(15, 19, 70, 20);

          //---- SCSYST ----
          SCSYST.setComponentPopupMenu(null);
          SCSYST.setName("SCSYST");
          panel2.add(SCSYST);
          SCSYST.setBounds(85, 15, 94, SCSYST.getPreferredSize().height);

          //---- OBJ_142 ----
          OBJ_142.setText("Ressource communication");
          OBJ_142.setName("OBJ_142");
          panel2.add(OBJ_142);
          OBJ_142.setBounds(200, 19, 160, 20);

          //---- SCRSC ----
          SCRSC.setComponentPopupMenu(null);
          SCRSC.setName("SCRSC");
          panel2.add(SCRSC);
          SCRSC.setBounds(365, 15, 94, SCRSC.getPreferredSize().height);

          //---- OBJ_144 ----
          OBJ_144.setText("Adresse IP");
          OBJ_144.setName("OBJ_144");
          panel2.add(OBJ_144);
          OBJ_144.setBounds(485, 19, 75, 20);

          //---- SCTCP ----
          SCTCP.setComponentPopupMenu(null);
          SCTCP.setName("SCTCP");
          panel2.add(SCTCP);
          SCTCP.setBounds(575, 15, 164, SCTCP.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }

        //======== panel3 ========
        {
          panel3.setBorder(new TitledBorder("Attributs"));
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);

          //---- OBJ_145 ----
          OBJ_145.setText("ID local");
          OBJ_145.setName("OBJ_145");
          panel3.add(OBJ_145);
          OBJ_145.setBounds(15, 30, 70, 20);

          //---- SCIDLO ----
          SCIDLO.setComponentPopupMenu(null);
          SCIDLO.setName("SCIDLO");
          panel3.add(SCIDLO);
          SCIDLO.setBounds(15, 50, 94, SCIDLO.getPreferredSize().height);

          //---- OBJ_146 ----
          OBJ_146.setText("Lieu loc.");
          OBJ_146.setName("OBJ_146");
          panel3.add(OBJ_146);
          OBJ_146.setBounds(115, 30, 70, 20);

          //---- SCLIEU ----
          SCLIEU.setComponentPopupMenu(null);
          SCLIEU.setName("SCLIEU");
          panel3.add(SCLIEU);
          SCLIEU.setBounds(115, 50, 94, SCLIEU.getPreferredSize().height);

          //---- OBJ_147 ----
          OBJ_147.setText("Mode");
          OBJ_147.setName("OBJ_147");
          panel3.add(OBJ_147);
          OBJ_147.setBounds(215, 30, 70, 20);

          //---- SCMODE ----
          SCMODE.setComponentPopupMenu(null);
          SCMODE.setName("SCMODE");
          panel3.add(SCMODE);
          SCMODE.setBounds(215, 50, 94, SCMODE.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel3.getComponentCount(); i++) {
              Rectangle bounds = panel3.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel3.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel3.setMinimumSize(preferredSize);
            panel3.setPreferredSize(preferredSize);
          }
        }

        //======== panel4 ========
        {
          panel4.setBorder(new TitledBorder("Modem interne"));
          panel4.setOpaque(false);
          panel4.setName("panel4");
          panel4.setLayout(null);

          //---- OBJ_150 ----
          OBJ_150.setText("Modem");
          OBJ_150.setName("OBJ_150");
          panel4.add(OBJ_150);
          OBJ_150.setBounds(15, 30, 54, 20);

          //---- SCTMOD ----
          SCTMOD.setComponentPopupMenu(null);
          SCTMOD.setName("SCTMOD");
          panel4.add(SCTMOD);
          SCTMOD.setBounds(15, 50, 54, SCTMOD.getPreferredSize().height);

          //---- OBJ_153 ----
          OBJ_153.setText("Num\u00e9ro de ligne");
          OBJ_153.setName("OBJ_153");
          panel4.add(OBJ_153);
          OBJ_153.setBounds(75, 30, 164, 20);

          //---- SCNLMO ----
          SCNLMO.setComponentPopupMenu(null);
          SCNLMO.setName("SCNLMO");
          panel4.add(SCNLMO);
          SCNLMO.setBounds(75, 50, 164, SCNLMO.getPreferredSize().height);

          //---- OBJ_155 ----
          OBJ_155.setText("Connexion");
          OBJ_155.setName("OBJ_155");
          panel4.add(OBJ_155);
          OBJ_155.setBounds(245, 30, 70, 20);

          //---- SCNCNX ----
          SCNCNX.setComponentPopupMenu(null);
          SCNCNX.setName("SCNCNX");
          panel4.add(SCNCNX);
          SCNCNX.setBounds(245, 50, 164, SCNCNX.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel4.getComponentCount(); i++) {
              Rectangle bounds = panel4.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel4.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel4.setMinimumSize(preferredSize);
            panel4.setPreferredSize(preferredSize);
          }
        }

        //======== panel5 ========
        {
          panel5.setBorder(new TitledBorder("TELNET"));
          panel5.setOpaque(false);
          panel5.setName("panel5");
          panel5.setLayout(null);

          //---- SCTELN ----
          SCTELN.setComponentPopupMenu(null);
          SCTELN.setName("SCTELN");
          panel5.add(SCTELN);
          SCTELN.setBounds(30, 30, 437, SCTELN.getPreferredSize().height);

          //---- OBJ_157 ----
          OBJ_157.setText("Port");
          OBJ_157.setName("OBJ_157");
          panel5.add(OBJ_157);
          OBJ_157.setBounds(530, 34, 45, 20);

          //---- SCPORT ----
          SCPORT.setComponentPopupMenu(null);
          SCPORT.setName("SCPORT");
          panel5.add(SCPORT);
          SCPORT.setBounds(585, 30, 64, SCPORT.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel5.getComponentCount(); i++) {
              Rectangle bounds = panel5.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel5.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel5.setMinimumSize(preferredSize);
            panel5.setPreferredSize(preferredSize);
          }
        }

        //======== panel6 ========
        {
          panel6.setBorder(new TitledBorder("FTP"));
          panel6.setOpaque(false);
          panel6.setName("panel6");
          panel6.setLayout(null);

          //---- SCFTP ----
          SCFTP.setComponentPopupMenu(null);
          SCFTP.setName("SCFTP");
          panel6.add(SCFTP);
          SCFTP.setBounds(30, 30, 437, SCFTP.getPreferredSize().height);

          //---- OBJ_161 ----
          OBJ_161.setText("Port");
          OBJ_161.setName("OBJ_161");
          panel6.add(OBJ_161);
          OBJ_161.setBounds(525, 34, 45, 20);

          //---- SCPORF ----
          SCPORF.setComponentPopupMenu(null);
          SCPORF.setName("SCPORF");
          panel6.add(SCPORF);
          SCPORF.setBounds(585, 30, 64, SCPORF.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel6.getComponentCount(); i++) {
              Rectangle bounds = panel6.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel6.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel6.setMinimumSize(preferredSize);
            panel6.setPreferredSize(preferredSize);
          }
        }

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_159 ----
          OBJ_159.setText("Utilisateur");
          OBJ_159.setName("OBJ_159");
          panel1.add(OBJ_159);
          OBJ_159.setBounds(15, 15, 85, 20);

          //---- SCUSER ----
          SCUSER.setComponentPopupMenu(null);
          SCUSER.setName("SCUSER");
          panel1.add(SCUSER);
          SCUSER.setBounds(105, 11, 210, SCUSER.getPreferredSize().height);

          //---- OBJ_160 ----
          OBJ_160.setText("Mot de passe");
          OBJ_160.setName("OBJ_160");
          panel1.add(OBJ_160);
          OBJ_160.setBounds(355, 15, 85, 20);

          //---- SCPASS ----
          SCPASS.setComponentPopupMenu(null);
          SCPASS.setName("SCPASS");
          panel1.add(SCPASS);
          SCPASS.setBounds(445, 11, 210, SCPASS.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        //======== panel7 ========
        {
          panel7.setBorder(new TitledBorder("Observations client"));
          panel7.setOpaque(false);
          panel7.setName("panel7");
          panel7.setLayout(null);

          //---- SCOBS1 ----
          SCOBS1.setComponentPopupMenu(null);
          SCOBS1.setName("SCOBS1");
          panel7.add(SCOBS1);
          SCOBS1.setBounds(30, 35, 624, SCOBS1.getPreferredSize().height);

          //---- SCOBS2 ----
          SCOBS2.setComponentPopupMenu(null);
          SCOBS2.setName("SCOBS2");
          panel7.add(SCOBS2);
          SCOBS2.setBounds(30, 60, 624, SCOBS2.getPreferredSize().height);

          //---- SCOBS3 ----
          SCOBS3.setComponentPopupMenu(null);
          SCOBS3.setName("SCOBS3");
          panel7.add(SCOBS3);
          SCOBS3.setBounds(30, 85, 624, SCOBS3.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel7.getComponentCount(); i++) {
              Rectangle bounds = panel7.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel7.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel7.setMinimumSize(preferredSize);
            panel7.setPreferredSize(preferredSize);
          }
        }

        //======== panel8 ========
        {
          panel8.setBorder(new TitledBorder("Observations RI"));
          panel8.setOpaque(false);
          panel8.setName("panel8");
          panel8.setLayout(null);

          //---- SCOBS4 ----
          SCOBS4.setComponentPopupMenu(null);
          SCOBS4.setName("SCOBS4");
          panel8.add(SCOBS4);
          SCOBS4.setBounds(30, 30, 624, SCOBS4.getPreferredSize().height);

          //---- SCOBS5 ----
          SCOBS5.setComponentPopupMenu(null);
          SCOBS5.setName("SCOBS5");
          panel8.add(SCOBS5);
          SCOBS5.setBounds(30, 55, 624, SCOBS5.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel8.getComponentCount(); i++) {
              Rectangle bounds = panel8.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel8.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel8.setMinimumSize(preferredSize);
            panel8.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                .addComponent(panel1, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 768, Short.MAX_VALUE)
                .addComponent(panel8, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 768, Short.MAX_VALUE)
                .addComponent(panel7, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 768, Short.MAX_VALUE)
                .addGroup(GroupLayout.Alignment.LEADING, p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                  .addComponent(panel2, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addGroup(GroupLayout.Alignment.LEADING, p_contenuLayout.createSequentialGroup()
                    .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 328, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 433, GroupLayout.PREFERRED_SIZE)))
                .addComponent(panel5, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 768, Short.MAX_VALUE)
                .addComponent(panel6, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 768, Short.MAX_VALUE))
              .addGap(42, 42, 42))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                .addComponent(panel4, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(panel3, GroupLayout.DEFAULT_SIZE, 101, Short.MAX_VALUE))
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(panel7, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(panel8, GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(32, Short.MAX_VALUE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel2;
  private JLabel OBJ_139;
  private XRiTextField SCSYST;
  private JLabel OBJ_142;
  private XRiTextField SCRSC;
  private JLabel OBJ_144;
  private XRiTextField SCTCP;
  private JPanel panel3;
  private JLabel OBJ_145;
  private XRiTextField SCIDLO;
  private JLabel OBJ_146;
  private XRiTextField SCLIEU;
  private JLabel OBJ_147;
  private XRiTextField SCMODE;
  private JPanel panel4;
  private JLabel OBJ_150;
  private XRiTextField SCTMOD;
  private JLabel OBJ_153;
  private XRiTextField SCNLMO;
  private JLabel OBJ_155;
  private XRiTextField SCNCNX;
  private JPanel panel5;
  private XRiTextField SCTELN;
  private JLabel OBJ_157;
  private XRiTextField SCPORT;
  private JPanel panel6;
  private XRiTextField SCFTP;
  private JLabel OBJ_161;
  private XRiTextField SCPORF;
  private JPanel panel1;
  private JLabel OBJ_159;
  private XRiTextField SCUSER;
  private JLabel OBJ_160;
  private XRiTextField SCPASS;
  private JPanel panel7;
  private XRiTextField SCOBS1;
  private XRiTextField SCOBS2;
  private XRiTextField SCOBS3;
  private JPanel panel8;
  private XRiTextField SCOBS4;
  private XRiTextField SCOBS5;
  // JFormDesigner - End of variables declaration  //GEN-END:variables



}
