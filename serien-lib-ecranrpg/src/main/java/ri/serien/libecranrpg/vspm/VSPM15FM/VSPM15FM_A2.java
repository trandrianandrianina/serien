
package ri.serien.libecranrpg.vspm.VSPM15FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VSPM15FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VSPM15FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WNCTD.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    riZoneSortie1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@*USER@")).trim());
    WNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNOM@")).trim());
    MOD06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD06@")).trim());
    MDP06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP06@")).trim());
    MOD07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD07@")).trim());
    MDP07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP07@")).trim());
    MOD08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD08@")).trim());
    MDP08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP08@")).trim());
    MOD09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD09@")).trim());
    MDP09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP09@")).trim());
    MOD10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD10@")).trim());
    MDP10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP10@")).trim());
    MOD11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD11@")).trim());
    MDP11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP11@")).trim());
    MOD12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD12@")).trim());
    MDP12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP12@")).trim());
    MOD13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD13@")).trim());
    MDP13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP13@")).trim());
    MOD14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD14@")).trim());
    MDP14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP14@")).trim());
    MOD15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD15@")).trim());
    MDP15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP15@")).trim());
    MOD16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD16@")).trim());
    MDP16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP16@")).trim());
    MOD17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD17@")).trim());
    MDP17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP17@")).trim());
    MOD18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD18@")).trim());
    MDP18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP18@")).trim());
    MOD19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD19@")).trim());
    MDP19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP19@")).trim());
    MOD20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD20@")).trim());
    MDP20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP20@")).trim());
    MOD21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD21@")).trim());
    MDP21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP21@")).trim());
    MOD22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD22@")).trim());
    MDP22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP22@")).trim());
    MOD23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD23@")).trim());
    MDP23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP23@")).trim());
    MOD24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD24@")).trim());
    MDP24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP24@")).trim());
    MOD25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD25@")).trim());
    MDP25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP25@")).trim());
    MOD26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD26@")).trim());
    MDP26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP26@")).trim());
    MOD27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD27@")).trim());
    MDP27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP27@")).trim());
    MOD28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD28@")).trim());
    MDP28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP28@")).trim());
    MOD29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD29@")).trim());
    MDP29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP029@")).trim());
    MOD30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD30@")).trim());
    MDP30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP30@")).trim());
    MOD31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD31@")).trim());
    MDP31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP31@")).trim());
    MOD32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD32@")).trim());
    MDP32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP32@")).trim());
    MOD33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD33@")).trim());
    MDP33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP33@")).trim());
    MOD34.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD34@")).trim());
    MDP34.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP34@")).trim());
    MOD35.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD35@")).trim());
    MDP35.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP35@")).trim());
    MOD36.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD36@")).trim());
    MDP36.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP36@")).trim());
    MOD37.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD37@")).trim());
    MDP37.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP37@")).trim());
    MOD38.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD38@")).trim());
    MDP38.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP38@")).trim());
    MOD39.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD39@")).trim());
    MDP39.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP39@")).trim());
    MOD40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD40@")).trim());
    MDP40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP40@")).trim());
    MOD41.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD41@")).trim());
    MDP41.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP41@")).trim());
    MOD42.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD42@")).trim());
    MDP42.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP42@")).trim());
    MOD43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD43@")).trim());
    MDP43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP43@")).trim());
    MOD44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD44@")).trim());
    MDP44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP44@")).trim());
    MOD45.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD45@")).trim());
    MDP45.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP45@")).trim());
    MOD46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD46@")).trim());
    MDP46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP46@")).trim());
    MOD47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD47@")).trim());
    MDP47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP47@")).trim());
    MOD48.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD48@")).trim());
    MDP48.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP48@")).trim());
    MOD49.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD49@")).trim());
    MDP49.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP49@")).trim());
    MOD50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD50@")).trim());
    MDP50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP50@")).trim());
    MOD01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD01@")).trim());
    MDP01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP01@")).trim());
    MOD02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD02@")).trim());
    MDP02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP02@")).trim());
    MOD03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD03@")).trim());
    MDP03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP03@")).trim());
    MOD04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD04@")).trim());
    MDP04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP04@")).trim());
    MOD05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOD05@")).trim());
    MDP05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MDP05@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    NBUTI.setEnabled(lexique.isPresent("NBUTI"));
    NUMLIV.setEnabled(lexique.isPresent("NUMLIV"));
    NBBUL.setEnabled(lexique.isPresent("NBBUL"));
    NUMCLI.setEnabled(lexique.isPresent("NUMCLI"));
    SN8.setEnabled(lexique.isPresent("SN8"));
    // WNCTD.setSelected(lexique.HostFieldGetData("WNCTD").equalsIgnoreCase("OUI"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Licences"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WNCTD.isSelected())
    // lexique.HostFieldPutData("WNCTD", 0, "OUI");
    // else
    // lexique.HostFieldPutData("WNCTD", 0, "NON");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    riZoneSortie1 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    WNOM = new RiZoneSortie();
    WNCTD = new XRiCheckBox();
    OBJ_57 = new JLabel();
    OBJ_60 = new JLabel();
    OBJ_61 = new JLabel();
    OBJ_47 = new JLabel();
    SN8 = new XRiTextField();
    NUMCLI = new XRiTextField();
    NBBUL = new XRiTextField();
    NUMLIV = new XRiTextField();
    NBUTI = new XRiTextField();
    MOD06 = new RiZoneSortie();
    MDP06 = new RiZoneSortie();
    MOD07 = new RiZoneSortie();
    MDP07 = new RiZoneSortie();
    MOD08 = new RiZoneSortie();
    MDP08 = new RiZoneSortie();
    MOD09 = new RiZoneSortie();
    MDP09 = new RiZoneSortie();
    MOD10 = new RiZoneSortie();
    MDP10 = new RiZoneSortie();
    MOD11 = new RiZoneSortie();
    MDP11 = new RiZoneSortie();
    MOD12 = new RiZoneSortie();
    MDP12 = new RiZoneSortie();
    MOD13 = new RiZoneSortie();
    MDP13 = new RiZoneSortie();
    MOD14 = new RiZoneSortie();
    MDP14 = new RiZoneSortie();
    MOD15 = new RiZoneSortie();
    MDP15 = new RiZoneSortie();
    MOD16 = new RiZoneSortie();
    MDP16 = new RiZoneSortie();
    MOD17 = new RiZoneSortie();
    MDP17 = new RiZoneSortie();
    MOD18 = new RiZoneSortie();
    MDP18 = new RiZoneSortie();
    MOD19 = new RiZoneSortie();
    MDP19 = new RiZoneSortie();
    MOD20 = new RiZoneSortie();
    MDP20 = new RiZoneSortie();
    MOD21 = new RiZoneSortie();
    MDP21 = new RiZoneSortie();
    MOD22 = new RiZoneSortie();
    MDP22 = new RiZoneSortie();
    MOD23 = new RiZoneSortie();
    MDP23 = new RiZoneSortie();
    MOD24 = new RiZoneSortie();
    MDP24 = new RiZoneSortie();
    MOD25 = new RiZoneSortie();
    MDP25 = new RiZoneSortie();
    MOD26 = new RiZoneSortie();
    MDP26 = new RiZoneSortie();
    MOD27 = new RiZoneSortie();
    MDP27 = new RiZoneSortie();
    MOD28 = new RiZoneSortie();
    MDP28 = new RiZoneSortie();
    MOD29 = new RiZoneSortie();
    MDP29 = new RiZoneSortie();
    MOD30 = new RiZoneSortie();
    MDP30 = new RiZoneSortie();
    MOD31 = new RiZoneSortie();
    MDP31 = new RiZoneSortie();
    MOD32 = new RiZoneSortie();
    MDP32 = new RiZoneSortie();
    MOD33 = new RiZoneSortie();
    MDP33 = new RiZoneSortie();
    MOD34 = new RiZoneSortie();
    MDP34 = new RiZoneSortie();
    MOD35 = new RiZoneSortie();
    MDP35 = new RiZoneSortie();
    MOD36 = new RiZoneSortie();
    MDP36 = new RiZoneSortie();
    MOD37 = new RiZoneSortie();
    MDP37 = new RiZoneSortie();
    MOD38 = new RiZoneSortie();
    MDP38 = new RiZoneSortie();
    MOD39 = new RiZoneSortie();
    MDP39 = new RiZoneSortie();
    MOD40 = new RiZoneSortie();
    MDP40 = new RiZoneSortie();
    MOD41 = new RiZoneSortie();
    MDP41 = new RiZoneSortie();
    MOD42 = new RiZoneSortie();
    MDP42 = new RiZoneSortie();
    MOD43 = new RiZoneSortie();
    MDP43 = new RiZoneSortie();
    MOD44 = new RiZoneSortie();
    MDP44 = new RiZoneSortie();
    MOD45 = new RiZoneSortie();
    MDP45 = new RiZoneSortie();
    MOD46 = new RiZoneSortie();
    MDP46 = new RiZoneSortie();
    MOD47 = new RiZoneSortie();
    MDP47 = new RiZoneSortie();
    MOD48 = new RiZoneSortie();
    MDP48 = new RiZoneSortie();
    MOD49 = new RiZoneSortie();
    MDP49 = new RiZoneSortie();
    MOD50 = new RiZoneSortie();
    MDP50 = new RiZoneSortie();
    MOD01 = new RiZoneSortie();
    MDP01 = new RiZoneSortie();
    MOD02 = new RiZoneSortie();
    MDP02 = new RiZoneSortie();
    MOD03 = new RiZoneSortie();
    MDP03 = new RiZoneSortie();
    MOD04 = new RiZoneSortie();
    MDP04 = new RiZoneSortie();
    MOD05 = new RiZoneSortie();
    MDP05 = new RiZoneSortie();
    separator1 = compFactory.createSeparator("Licences par modules");
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- riZoneSortie1 ----
          riZoneSortie1.setText("@*USER@");
          riZoneSortie1.setOpaque(false);
          riZoneSortie1.setName("riZoneSortie1");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(riZoneSortie1, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(riZoneSortie1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 740));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Param\u00e8tres pour le calcul des licences"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- WNOM ----
            WNOM.setComponentPopupMenu(BTD);
            WNOM.setText("@WNOM@");
            WNOM.setName("WNOM");
            panel1.add(WNOM);
            WNOM.setBounds(330, 65, 310, WNOM.getPreferredSize().height);

            //---- WNCTD ----
            WNCTD.setText("Num\u00e9ros de licences d\u00e9finitifs");
            WNCTD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WNCTD.setName("WNCTD");
            panel1.add(WNCTD);
            WNCTD.setBounds(30, 162, 350, 20);

            //---- OBJ_57 ----
            OBJ_57.setText("Num\u00e9ro de client chez R.I");
            OBJ_57.setName("OBJ_57");
            panel1.add(OBJ_57);
            OBJ_57.setBounds(30, 67, 190, 20);

            //---- OBJ_60 ----
            OBJ_60.setText("Nombre d'utilisateurs");
            OBJ_60.setName("OBJ_60");
            panel1.add(OBJ_60);
            OBJ_60.setBounds(30, 100, 190, 20);

            //---- OBJ_61 ----
            OBJ_61.setText("Nombre de bulletins de paie");
            OBJ_61.setName("OBJ_61");
            panel1.add(OBJ_61);
            OBJ_61.setBounds(30, 133, 190, 20);

            //---- OBJ_47 ----
            OBJ_47.setText("Num\u00e9ro de s\u00e9rie");
            OBJ_47.setName("OBJ_47");
            panel1.add(OBJ_47);
            OBJ_47.setBounds(30, 34, 190, 20);

            //---- SN8 ----
            SN8.setComponentPopupMenu(BTD);
            SN8.setName("SN8");
            panel1.add(SN8);
            SN8.setBounds(225, 30, 90, SN8.getPreferredSize().height);

            //---- NUMCLI ----
            NUMCLI.setComponentPopupMenu(BTD);
            NUMCLI.setName("NUMCLI");
            panel1.add(NUMCLI);
            NUMCLI.setBounds(225, 63, 60, NUMCLI.getPreferredSize().height);

            //---- NBBUL ----
            NBBUL.setComponentPopupMenu(BTD);
            NBBUL.setName("NBBUL");
            panel1.add(NBBUL);
            NBBUL.setBounds(225, 129, 58, NBBUL.getPreferredSize().height);

            //---- NUMLIV ----
            NUMLIV.setComponentPopupMenu(BTD);
            NUMLIV.setName("NUMLIV");
            panel1.add(NUMLIV);
            NUMLIV.setBounds(290, 63, 34, NUMLIV.getPreferredSize().height);

            //---- NBUTI ----
            NBUTI.setComponentPopupMenu(BTD);
            NBUTI.setName("NBUTI");
            panel1.add(NBUTI);
            NBUTI.setBounds(225, 96, 34, NBUTI.getPreferredSize().height);

            //---- MOD06 ----
            MOD06.setText("@MOD06@");
            MOD06.setName("MOD06");
            panel1.add(MOD06);
            MOD06.setBounds(25, 260, 44, MOD06.getPreferredSize().height);

            //---- MDP06 ----
            MDP06.setText("@MDP06@");
            MDP06.setName("MDP06");
            panel1.add(MDP06);
            MDP06.setBounds(75, 260, 94, MDP06.getPreferredSize().height);

            //---- MOD07 ----
            MOD07.setText("@MOD07@");
            MOD07.setName("MOD07");
            panel1.add(MOD07);
            MOD07.setBounds(200, 260, 44, MOD07.getPreferredSize().height);

            //---- MDP07 ----
            MDP07.setText("@MDP07@");
            MDP07.setName("MDP07");
            panel1.add(MDP07);
            MDP07.setBounds(250, 260, 94, MDP07.getPreferredSize().height);

            //---- MOD08 ----
            MOD08.setText("@MOD08@");
            MOD08.setName("MOD08");
            panel1.add(MOD08);
            MOD08.setBounds(380, 260, 44, MOD08.getPreferredSize().height);

            //---- MDP08 ----
            MDP08.setText("@MDP08@");
            MDP08.setName("MDP08");
            panel1.add(MDP08);
            MDP08.setBounds(430, 260, 94, MDP08.getPreferredSize().height);

            //---- MOD09 ----
            MOD09.setText("@MOD09@");
            MOD09.setName("MOD09");
            panel1.add(MOD09);
            MOD09.setBounds(555, 260, 44, MOD09.getPreferredSize().height);

            //---- MDP09 ----
            MDP09.setText("@MDP09@");
            MDP09.setName("MDP09");
            panel1.add(MDP09);
            MDP09.setBounds(605, 260, 94, MDP09.getPreferredSize().height);

            //---- MOD10 ----
            MOD10.setText("@MOD10@");
            MOD10.setName("MOD10");
            panel1.add(MOD10);
            MOD10.setBounds(735, 260, 44, MOD10.getPreferredSize().height);

            //---- MDP10 ----
            MDP10.setText("@MDP10@");
            MDP10.setName("MDP10");
            panel1.add(MDP10);
            MDP10.setBounds(785, 260, 94, MDP10.getPreferredSize().height);

            //---- MOD11 ----
            MOD11.setText("@MOD11@");
            MOD11.setName("MOD11");
            panel1.add(MOD11);
            MOD11.setBounds(25, 290, 44, MOD11.getPreferredSize().height);

            //---- MDP11 ----
            MDP11.setText("@MDP11@");
            MDP11.setName("MDP11");
            panel1.add(MDP11);
            MDP11.setBounds(75, 290, 94, MDP11.getPreferredSize().height);

            //---- MOD12 ----
            MOD12.setText("@MOD12@");
            MOD12.setName("MOD12");
            panel1.add(MOD12);
            MOD12.setBounds(200, 290, 44, MOD12.getPreferredSize().height);

            //---- MDP12 ----
            MDP12.setText("@MDP12@");
            MDP12.setName("MDP12");
            panel1.add(MDP12);
            MDP12.setBounds(250, 290, 94, MDP12.getPreferredSize().height);

            //---- MOD13 ----
            MOD13.setText("@MOD13@");
            MOD13.setName("MOD13");
            panel1.add(MOD13);
            MOD13.setBounds(380, 290, 44, MOD13.getPreferredSize().height);

            //---- MDP13 ----
            MDP13.setText("@MDP13@");
            MDP13.setName("MDP13");
            panel1.add(MDP13);
            MDP13.setBounds(430, 290, 94, MDP13.getPreferredSize().height);

            //---- MOD14 ----
            MOD14.setText("@MOD14@");
            MOD14.setName("MOD14");
            panel1.add(MOD14);
            MOD14.setBounds(555, 290, 44, MOD14.getPreferredSize().height);

            //---- MDP14 ----
            MDP14.setText("@MDP14@");
            MDP14.setName("MDP14");
            panel1.add(MDP14);
            MDP14.setBounds(605, 290, 94, MDP14.getPreferredSize().height);

            //---- MOD15 ----
            MOD15.setText("@MOD15@");
            MOD15.setName("MOD15");
            panel1.add(MOD15);
            MOD15.setBounds(735, 290, 44, MOD15.getPreferredSize().height);

            //---- MDP15 ----
            MDP15.setText("@MDP15@");
            MDP15.setName("MDP15");
            panel1.add(MDP15);
            MDP15.setBounds(785, 290, 94, MDP15.getPreferredSize().height);

            //---- MOD16 ----
            MOD16.setText("@MOD16@");
            MOD16.setName("MOD16");
            panel1.add(MOD16);
            MOD16.setBounds(25, 320, 44, MOD16.getPreferredSize().height);

            //---- MDP16 ----
            MDP16.setText("@MDP16@");
            MDP16.setName("MDP16");
            panel1.add(MDP16);
            MDP16.setBounds(75, 320, 94, MDP16.getPreferredSize().height);

            //---- MOD17 ----
            MOD17.setText("@MOD17@");
            MOD17.setName("MOD17");
            panel1.add(MOD17);
            MOD17.setBounds(200, 320, 44, MOD17.getPreferredSize().height);

            //---- MDP17 ----
            MDP17.setText("@MDP17@");
            MDP17.setName("MDP17");
            panel1.add(MDP17);
            MDP17.setBounds(250, 320, 94, MDP17.getPreferredSize().height);

            //---- MOD18 ----
            MOD18.setText("@MOD18@");
            MOD18.setName("MOD18");
            panel1.add(MOD18);
            MOD18.setBounds(380, 320, 44, MOD18.getPreferredSize().height);

            //---- MDP18 ----
            MDP18.setText("@MDP18@");
            MDP18.setName("MDP18");
            panel1.add(MDP18);
            MDP18.setBounds(430, 320, 94, MDP18.getPreferredSize().height);

            //---- MOD19 ----
            MOD19.setText("@MOD19@");
            MOD19.setName("MOD19");
            panel1.add(MOD19);
            MOD19.setBounds(555, 320, 44, MOD19.getPreferredSize().height);

            //---- MDP19 ----
            MDP19.setText("@MDP19@");
            MDP19.setName("MDP19");
            panel1.add(MDP19);
            MDP19.setBounds(605, 320, 94, MDP19.getPreferredSize().height);

            //---- MOD20 ----
            MOD20.setText("@MOD20@");
            MOD20.setName("MOD20");
            panel1.add(MOD20);
            MOD20.setBounds(735, 320, 44, MOD20.getPreferredSize().height);

            //---- MDP20 ----
            MDP20.setText("@MDP20@");
            MDP20.setName("MDP20");
            panel1.add(MDP20);
            MDP20.setBounds(785, 320, 94, MDP20.getPreferredSize().height);

            //---- MOD21 ----
            MOD21.setText("@MOD21@");
            MOD21.setName("MOD21");
            panel1.add(MOD21);
            MOD21.setBounds(25, 350, 44, MOD21.getPreferredSize().height);

            //---- MDP21 ----
            MDP21.setText("@MDP21@");
            MDP21.setName("MDP21");
            panel1.add(MDP21);
            MDP21.setBounds(75, 350, 94, MDP21.getPreferredSize().height);

            //---- MOD22 ----
            MOD22.setText("@MOD22@");
            MOD22.setName("MOD22");
            panel1.add(MOD22);
            MOD22.setBounds(200, 350, 44, MOD22.getPreferredSize().height);

            //---- MDP22 ----
            MDP22.setText("@MDP22@");
            MDP22.setName("MDP22");
            panel1.add(MDP22);
            MDP22.setBounds(250, 350, 94, MDP22.getPreferredSize().height);

            //---- MOD23 ----
            MOD23.setText("@MOD23@");
            MOD23.setName("MOD23");
            panel1.add(MOD23);
            MOD23.setBounds(380, 350, 44, MOD23.getPreferredSize().height);

            //---- MDP23 ----
            MDP23.setText("@MDP23@");
            MDP23.setName("MDP23");
            panel1.add(MDP23);
            MDP23.setBounds(430, 350, 94, MDP23.getPreferredSize().height);

            //---- MOD24 ----
            MOD24.setText("@MOD24@");
            MOD24.setName("MOD24");
            panel1.add(MOD24);
            MOD24.setBounds(555, 350, 44, MOD24.getPreferredSize().height);

            //---- MDP24 ----
            MDP24.setText("@MDP24@");
            MDP24.setName("MDP24");
            panel1.add(MDP24);
            MDP24.setBounds(605, 350, 94, MDP24.getPreferredSize().height);

            //---- MOD25 ----
            MOD25.setText("@MOD25@");
            MOD25.setName("MOD25");
            panel1.add(MOD25);
            MOD25.setBounds(735, 350, 44, MOD25.getPreferredSize().height);

            //---- MDP25 ----
            MDP25.setText("@MDP25@");
            MDP25.setName("MDP25");
            panel1.add(MDP25);
            MDP25.setBounds(785, 350, 94, MDP25.getPreferredSize().height);

            //---- MOD26 ----
            MOD26.setText("@MOD26@");
            MOD26.setName("MOD26");
            panel1.add(MOD26);
            MOD26.setBounds(25, 380, 44, MOD26.getPreferredSize().height);

            //---- MDP26 ----
            MDP26.setText("@MDP26@");
            MDP26.setName("MDP26");
            panel1.add(MDP26);
            MDP26.setBounds(75, 380, 94, MDP26.getPreferredSize().height);

            //---- MOD27 ----
            MOD27.setText("@MOD27@");
            MOD27.setName("MOD27");
            panel1.add(MOD27);
            MOD27.setBounds(200, 380, 44, MOD27.getPreferredSize().height);

            //---- MDP27 ----
            MDP27.setText("@MDP27@");
            MDP27.setName("MDP27");
            panel1.add(MDP27);
            MDP27.setBounds(250, 380, 94, MDP27.getPreferredSize().height);

            //---- MOD28 ----
            MOD28.setText("@MOD28@");
            MOD28.setName("MOD28");
            panel1.add(MOD28);
            MOD28.setBounds(380, 380, 44, MOD28.getPreferredSize().height);

            //---- MDP28 ----
            MDP28.setText("@MDP28@");
            MDP28.setName("MDP28");
            panel1.add(MDP28);
            MDP28.setBounds(430, 380, 94, MDP28.getPreferredSize().height);

            //---- MOD29 ----
            MOD29.setText("@MOD29@");
            MOD29.setName("MOD29");
            panel1.add(MOD29);
            MOD29.setBounds(555, 380, 44, MOD29.getPreferredSize().height);

            //---- MDP29 ----
            MDP29.setText("@MDP029@");
            MDP29.setName("MDP29");
            panel1.add(MDP29);
            MDP29.setBounds(605, 380, 94, MDP29.getPreferredSize().height);

            //---- MOD30 ----
            MOD30.setText("@MOD30@");
            MOD30.setName("MOD30");
            panel1.add(MOD30);
            MOD30.setBounds(735, 380, 44, MOD30.getPreferredSize().height);

            //---- MDP30 ----
            MDP30.setText("@MDP30@");
            MDP30.setName("MDP30");
            panel1.add(MDP30);
            MDP30.setBounds(785, 380, 94, MDP30.getPreferredSize().height);

            //---- MOD31 ----
            MOD31.setText("@MOD31@");
            MOD31.setName("MOD31");
            panel1.add(MOD31);
            MOD31.setBounds(25, 410, 44, MOD31.getPreferredSize().height);

            //---- MDP31 ----
            MDP31.setText("@MDP31@");
            MDP31.setName("MDP31");
            panel1.add(MDP31);
            MDP31.setBounds(75, 410, 94, MDP31.getPreferredSize().height);

            //---- MOD32 ----
            MOD32.setText("@MOD32@");
            MOD32.setName("MOD32");
            panel1.add(MOD32);
            MOD32.setBounds(200, 410, 44, MOD32.getPreferredSize().height);

            //---- MDP32 ----
            MDP32.setText("@MDP32@");
            MDP32.setName("MDP32");
            panel1.add(MDP32);
            MDP32.setBounds(250, 410, 94, MDP32.getPreferredSize().height);

            //---- MOD33 ----
            MOD33.setText("@MOD33@");
            MOD33.setName("MOD33");
            panel1.add(MOD33);
            MOD33.setBounds(380, 410, 44, MOD33.getPreferredSize().height);

            //---- MDP33 ----
            MDP33.setText("@MDP33@");
            MDP33.setName("MDP33");
            panel1.add(MDP33);
            MDP33.setBounds(430, 410, 94, MDP33.getPreferredSize().height);

            //---- MOD34 ----
            MOD34.setText("@MOD34@");
            MOD34.setName("MOD34");
            panel1.add(MOD34);
            MOD34.setBounds(555, 410, 44, MOD34.getPreferredSize().height);

            //---- MDP34 ----
            MDP34.setText("@MDP34@");
            MDP34.setName("MDP34");
            panel1.add(MDP34);
            MDP34.setBounds(605, 410, 94, MDP34.getPreferredSize().height);

            //---- MOD35 ----
            MOD35.setText("@MOD35@");
            MOD35.setName("MOD35");
            panel1.add(MOD35);
            MOD35.setBounds(735, 410, 44, MOD35.getPreferredSize().height);

            //---- MDP35 ----
            MDP35.setText("@MDP35@");
            MDP35.setName("MDP35");
            panel1.add(MDP35);
            MDP35.setBounds(785, 410, 94, MDP35.getPreferredSize().height);

            //---- MOD36 ----
            MOD36.setText("@MOD36@");
            MOD36.setName("MOD36");
            panel1.add(MOD36);
            MOD36.setBounds(25, 440, 44, MOD36.getPreferredSize().height);

            //---- MDP36 ----
            MDP36.setText("@MDP36@");
            MDP36.setName("MDP36");
            panel1.add(MDP36);
            MDP36.setBounds(75, 440, 94, MDP36.getPreferredSize().height);

            //---- MOD37 ----
            MOD37.setText("@MOD37@");
            MOD37.setName("MOD37");
            panel1.add(MOD37);
            MOD37.setBounds(200, 440, 44, MOD37.getPreferredSize().height);

            //---- MDP37 ----
            MDP37.setText("@MDP37@");
            MDP37.setName("MDP37");
            panel1.add(MDP37);
            MDP37.setBounds(250, 440, 94, MDP37.getPreferredSize().height);

            //---- MOD38 ----
            MOD38.setText("@MOD38@");
            MOD38.setName("MOD38");
            panel1.add(MOD38);
            MOD38.setBounds(380, 440, 44, MOD38.getPreferredSize().height);

            //---- MDP38 ----
            MDP38.setText("@MDP38@");
            MDP38.setName("MDP38");
            panel1.add(MDP38);
            MDP38.setBounds(430, 440, 94, MDP38.getPreferredSize().height);

            //---- MOD39 ----
            MOD39.setText("@MOD39@");
            MOD39.setName("MOD39");
            panel1.add(MOD39);
            MOD39.setBounds(555, 440, 44, MOD39.getPreferredSize().height);

            //---- MDP39 ----
            MDP39.setText("@MDP39@");
            MDP39.setName("MDP39");
            panel1.add(MDP39);
            MDP39.setBounds(605, 440, 94, MDP39.getPreferredSize().height);

            //---- MOD40 ----
            MOD40.setText("@MOD40@");
            MOD40.setName("MOD40");
            panel1.add(MOD40);
            MOD40.setBounds(735, 440, 44, MOD40.getPreferredSize().height);

            //---- MDP40 ----
            MDP40.setText("@MDP40@");
            MDP40.setName("MDP40");
            panel1.add(MDP40);
            MDP40.setBounds(785, 440, 94, MDP40.getPreferredSize().height);

            //---- MOD41 ----
            MOD41.setText("@MOD41@");
            MOD41.setName("MOD41");
            panel1.add(MOD41);
            MOD41.setBounds(25, 470, 44, 25);

            //---- MDP41 ----
            MDP41.setText("@MDP41@");
            MDP41.setName("MDP41");
            panel1.add(MDP41);
            MDP41.setBounds(75, 470, 94, 25);

            //---- MOD42 ----
            MOD42.setText("@MOD42@");
            MOD42.setName("MOD42");
            panel1.add(MOD42);
            MOD42.setBounds(200, 470, 44, 25);

            //---- MDP42 ----
            MDP42.setText("@MDP42@");
            MDP42.setName("MDP42");
            panel1.add(MDP42);
            MDP42.setBounds(250, 470, 94, 25);

            //---- MOD43 ----
            MOD43.setText("@MOD43@");
            MOD43.setName("MOD43");
            panel1.add(MOD43);
            MOD43.setBounds(380, 470, 44, 25);

            //---- MDP43 ----
            MDP43.setText("@MDP43@");
            MDP43.setName("MDP43");
            panel1.add(MDP43);
            MDP43.setBounds(430, 470, 94, 25);

            //---- MOD44 ----
            MOD44.setText("@MOD44@");
            MOD44.setName("MOD44");
            panel1.add(MOD44);
            MOD44.setBounds(555, 470, 44, 25);

            //---- MDP44 ----
            MDP44.setText("@MDP44@");
            MDP44.setName("MDP44");
            panel1.add(MDP44);
            MDP44.setBounds(605, 470, 94, 25);

            //---- MOD45 ----
            MOD45.setText("@MOD45@");
            MOD45.setName("MOD45");
            panel1.add(MOD45);
            MOD45.setBounds(735, 470, 44, 25);

            //---- MDP45 ----
            MDP45.setText("@MDP45@");
            MDP45.setName("MDP45");
            panel1.add(MDP45);
            MDP45.setBounds(785, 470, 94, 25);

            //---- MOD46 ----
            MOD46.setText("@MOD46@");
            MOD46.setName("MOD46");
            panel1.add(MOD46);
            MOD46.setBounds(25, 500, 44, MOD46.getPreferredSize().height);

            //---- MDP46 ----
            MDP46.setText("@MDP46@");
            MDP46.setName("MDP46");
            panel1.add(MDP46);
            MDP46.setBounds(75, 500, 94, MDP46.getPreferredSize().height);

            //---- MOD47 ----
            MOD47.setText("@MOD47@");
            MOD47.setName("MOD47");
            panel1.add(MOD47);
            MOD47.setBounds(200, 500, 44, MOD47.getPreferredSize().height);

            //---- MDP47 ----
            MDP47.setText("@MDP47@");
            MDP47.setName("MDP47");
            panel1.add(MDP47);
            MDP47.setBounds(250, 500, 94, MDP47.getPreferredSize().height);

            //---- MOD48 ----
            MOD48.setText("@MOD48@");
            MOD48.setName("MOD48");
            panel1.add(MOD48);
            MOD48.setBounds(380, 500, 44, MOD48.getPreferredSize().height);

            //---- MDP48 ----
            MDP48.setText("@MDP48@");
            MDP48.setName("MDP48");
            panel1.add(MDP48);
            MDP48.setBounds(430, 500, 94, MDP48.getPreferredSize().height);

            //---- MOD49 ----
            MOD49.setText("@MOD49@");
            MOD49.setName("MOD49");
            panel1.add(MOD49);
            MOD49.setBounds(555, 500, 44, MOD49.getPreferredSize().height);

            //---- MDP49 ----
            MDP49.setText("@MDP49@");
            MDP49.setName("MDP49");
            panel1.add(MDP49);
            MDP49.setBounds(605, 500, 94, MDP49.getPreferredSize().height);

            //---- MOD50 ----
            MOD50.setText("@MOD50@");
            MOD50.setName("MOD50");
            panel1.add(MOD50);
            MOD50.setBounds(735, 500, 44, MOD50.getPreferredSize().height);

            //---- MDP50 ----
            MDP50.setText("@MDP50@");
            MDP50.setName("MDP50");
            panel1.add(MDP50);
            MDP50.setBounds(785, 500, 94, MDP50.getPreferredSize().height);

            //---- MOD01 ----
            MOD01.setText("@MOD01@");
            MOD01.setName("MOD01");
            panel1.add(MOD01);
            MOD01.setBounds(25, 230, 44, MOD01.getPreferredSize().height);

            //---- MDP01 ----
            MDP01.setText("@MDP01@");
            MDP01.setName("MDP01");
            panel1.add(MDP01);
            MDP01.setBounds(75, 230, 94, MDP01.getPreferredSize().height);

            //---- MOD02 ----
            MOD02.setText("@MOD02@");
            MOD02.setName("MOD02");
            panel1.add(MOD02);
            MOD02.setBounds(200, 230, 44, MOD02.getPreferredSize().height);

            //---- MDP02 ----
            MDP02.setText("@MDP02@");
            MDP02.setName("MDP02");
            panel1.add(MDP02);
            MDP02.setBounds(250, 230, 94, MDP02.getPreferredSize().height);

            //---- MOD03 ----
            MOD03.setText("@MOD03@");
            MOD03.setName("MOD03");
            panel1.add(MOD03);
            MOD03.setBounds(380, 230, 44, MOD03.getPreferredSize().height);

            //---- MDP03 ----
            MDP03.setText("@MDP03@");
            MDP03.setName("MDP03");
            panel1.add(MDP03);
            MDP03.setBounds(430, 230, 94, MDP03.getPreferredSize().height);

            //---- MOD04 ----
            MOD04.setText("@MOD04@");
            MOD04.setName("MOD04");
            panel1.add(MOD04);
            MOD04.setBounds(555, 230, 44, MOD04.getPreferredSize().height);

            //---- MDP04 ----
            MDP04.setText("@MDP04@");
            MDP04.setName("MDP04");
            panel1.add(MDP04);
            MDP04.setBounds(605, 230, 94, MDP04.getPreferredSize().height);

            //---- MOD05 ----
            MOD05.setText("@MOD05@");
            MOD05.setName("MOD05");
            panel1.add(MOD05);
            MOD05.setBounds(735, 230, 44, MOD05.getPreferredSize().height);

            //---- MDP05 ----
            MDP05.setText("@MDP05@");
            MDP05.setName("MDP05");
            panel1.add(MDP05);
            MDP05.setBounds(785, 230, 94, MDP05.getPreferredSize().height);

            //---- separator1 ----
            separator1.setName("separator1");
            panel1.add(separator1);
            separator1.setBounds(15, 205, 870, separator1.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 901, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 541, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private RiZoneSortie riZoneSortie1;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie WNOM;
  private XRiCheckBox WNCTD;
  private JLabel OBJ_57;
  private JLabel OBJ_60;
  private JLabel OBJ_61;
  private JLabel OBJ_47;
  private XRiTextField SN8;
  private XRiTextField NUMCLI;
  private XRiTextField NBBUL;
  private XRiTextField NUMLIV;
  private XRiTextField NBUTI;
  private RiZoneSortie MOD06;
  private RiZoneSortie MDP06;
  private RiZoneSortie MOD07;
  private RiZoneSortie MDP07;
  private RiZoneSortie MOD08;
  private RiZoneSortie MDP08;
  private RiZoneSortie MOD09;
  private RiZoneSortie MDP09;
  private RiZoneSortie MOD10;
  private RiZoneSortie MDP10;
  private RiZoneSortie MOD11;
  private RiZoneSortie MDP11;
  private RiZoneSortie MOD12;
  private RiZoneSortie MDP12;
  private RiZoneSortie MOD13;
  private RiZoneSortie MDP13;
  private RiZoneSortie MOD14;
  private RiZoneSortie MDP14;
  private RiZoneSortie MOD15;
  private RiZoneSortie MDP15;
  private RiZoneSortie MOD16;
  private RiZoneSortie MDP16;
  private RiZoneSortie MOD17;
  private RiZoneSortie MDP17;
  private RiZoneSortie MOD18;
  private RiZoneSortie MDP18;
  private RiZoneSortie MOD19;
  private RiZoneSortie MDP19;
  private RiZoneSortie MOD20;
  private RiZoneSortie MDP20;
  private RiZoneSortie MOD21;
  private RiZoneSortie MDP21;
  private RiZoneSortie MOD22;
  private RiZoneSortie MDP22;
  private RiZoneSortie MOD23;
  private RiZoneSortie MDP23;
  private RiZoneSortie MOD24;
  private RiZoneSortie MDP24;
  private RiZoneSortie MOD25;
  private RiZoneSortie MDP25;
  private RiZoneSortie MOD26;
  private RiZoneSortie MDP26;
  private RiZoneSortie MOD27;
  private RiZoneSortie MDP27;
  private RiZoneSortie MOD28;
  private RiZoneSortie MDP28;
  private RiZoneSortie MOD29;
  private RiZoneSortie MDP29;
  private RiZoneSortie MOD30;
  private RiZoneSortie MDP30;
  private RiZoneSortie MOD31;
  private RiZoneSortie MDP31;
  private RiZoneSortie MOD32;
  private RiZoneSortie MDP32;
  private RiZoneSortie MOD33;
  private RiZoneSortie MDP33;
  private RiZoneSortie MOD34;
  private RiZoneSortie MDP34;
  private RiZoneSortie MOD35;
  private RiZoneSortie MDP35;
  private RiZoneSortie MOD36;
  private RiZoneSortie MDP36;
  private RiZoneSortie MOD37;
  private RiZoneSortie MDP37;
  private RiZoneSortie MOD38;
  private RiZoneSortie MDP38;
  private RiZoneSortie MOD39;
  private RiZoneSortie MDP39;
  private RiZoneSortie MOD40;
  private RiZoneSortie MDP40;
  private RiZoneSortie MOD41;
  private RiZoneSortie MDP41;
  private RiZoneSortie MOD42;
  private RiZoneSortie MDP42;
  private RiZoneSortie MOD43;
  private RiZoneSortie MDP43;
  private RiZoneSortie MOD44;
  private RiZoneSortie MDP44;
  private RiZoneSortie MOD45;
  private RiZoneSortie MDP45;
  private RiZoneSortie MOD46;
  private RiZoneSortie MDP46;
  private RiZoneSortie MOD47;
  private RiZoneSortie MDP47;
  private RiZoneSortie MOD48;
  private RiZoneSortie MDP48;
  private RiZoneSortie MOD49;
  private RiZoneSortie MDP49;
  private RiZoneSortie MOD50;
  private RiZoneSortie MDP50;
  private RiZoneSortie MOD01;
  private RiZoneSortie MDP01;
  private RiZoneSortie MOD02;
  private RiZoneSortie MDP02;
  private RiZoneSortie MOD03;
  private RiZoneSortie MDP03;
  private RiZoneSortie MOD04;
  private RiZoneSortie MDP04;
  private RiZoneSortie MOD05;
  private RiZoneSortie MDP05;
  private JComponent separator1;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
