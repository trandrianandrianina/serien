
package ri.serien.libecranrpg.vspm.VSPM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VSPM03FM_C2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VSPM03FM_C2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    CLINOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLINOM@")).trim());
    CLINOM2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAPNOM@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("S.A.V."));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void inviteActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void aideActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  // EVENEMENTIEL
  // +++++++++++++++++++++++++++++++++++++++++++
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_contenu = new JPanel();
    OBJ_32 = new JPanel();
    CLINOM = new RiZoneSortie();
    OBJ_33 = new JLabel();
    OBJ_36 = new JLabel();
    OBJ_39 = new JLabel();
    SCCLS = new XRiTextField();
    SCMTS = new XRiTextField();
    SCCAP = new XRiTextField();
    OBJ_37 = new JLabel();
    OBJ_40 = new JLabel();
    OBJ_41 = new JLabel();
    OBJ_42 = new JLabel();
    SCNFS = new XRiTextField();
    SCDBSX = new XRiCalendrier();
    SCDDEX = new XRiCalendrier();
    SCDRSX = new XRiCalendrier();
    CLINOM2 = new RiZoneSortie();
    BTD = new JPopupMenu();
    invite = new JMenuItem();
    aide = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(865, 370));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Suppression");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== OBJ_32 ========
        {
          OBJ_32.setBorder(new TitledBorder("Service Apr\u00e8s Vente"));
          OBJ_32.setOpaque(false);
          OBJ_32.setName("OBJ_32");

          //---- CLINOM ----
          CLINOM.setText("@CLINOM@");
          CLINOM.setName("CLINOM");

          //---- OBJ_33 ----
          OBJ_33.setText("Num\u00e9ro de client");
          OBJ_33.setName("OBJ_33");

          //---- OBJ_36 ----
          OBJ_36.setText("Montant du contrat");
          OBJ_36.setName("OBJ_36");

          //---- OBJ_39 ----
          OBJ_39.setText("Date de d\u00e9but");
          OBJ_39.setName("OBJ_39");

          //---- SCCLS ----
          SCCLS.setComponentPopupMenu(BTD);
          SCCLS.setName("SCCLS");

          //---- SCMTS ----
          SCMTS.setComponentPopupMenu(BTD);
          SCMTS.setName("SCMTS");

          //---- SCCAP ----
          SCCAP.setComponentPopupMenu(BTD);
          SCCAP.setName("SCCAP");

          //---- OBJ_37 ----
          OBJ_37.setText("Num\u00e9ro de la derni\u00e8re facture");
          OBJ_37.setName("OBJ_37");

          //---- OBJ_40 ----
          OBJ_40.setText("Date de d\u00e9marrage");
          OBJ_40.setName("OBJ_40");

          //---- OBJ_41 ----
          OBJ_41.setText("Renouvellement");
          OBJ_41.setName("OBJ_41");

          //---- OBJ_42 ----
          OBJ_42.setText("Chef de projet CAP");
          OBJ_42.setName("OBJ_42");

          //---- SCNFS ----
          SCNFS.setComponentPopupMenu(BTD);
          SCNFS.setName("SCNFS");

          //---- SCDBSX ----
          SCDBSX.setName("SCDBSX");

          //---- SCDDEX ----
          SCDDEX.setName("SCDDEX");

          //---- SCDRSX ----
          SCDRSX.setName("SCDRSX");

          //---- CLINOM2 ----
          CLINOM2.setText("@CAPNOM@");
          CLINOM2.setName("CLINOM2");

          GroupLayout OBJ_32Layout = new GroupLayout(OBJ_32);
          OBJ_32.setLayout(OBJ_32Layout);
          OBJ_32Layout.setHorizontalGroup(
            OBJ_32Layout.createParallelGroup()
              .addGroup(OBJ_32Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(OBJ_32Layout.createParallelGroup()
                  .addGroup(OBJ_32Layout.createSequentialGroup()
                    .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(SCCLS, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(CLINOM, GroupLayout.PREFERRED_SIZE, 340, GroupLayout.PREFERRED_SIZE))
                  .addGroup(OBJ_32Layout.createSequentialGroup()
                    .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(SCMTS, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE))
                  .addGroup(OBJ_32Layout.createSequentialGroup()
                    .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(SCNFS, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addGroup(OBJ_32Layout.createSequentialGroup()
                    .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(SCDBSX, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                  .addGroup(OBJ_32Layout.createSequentialGroup()
                    .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(SCDDEX, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                  .addGroup(OBJ_32Layout.createSequentialGroup()
                    .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(SCDRSX, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                  .addGroup(OBJ_32Layout.createSequentialGroup()
                    .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(SCCAP, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addGap(30, 30, 30)
                    .addComponent(CLINOM2, GroupLayout.PREFERRED_SIZE, 340, GroupLayout.PREFERRED_SIZE))))
          );
          OBJ_32Layout.setVerticalGroup(
            OBJ_32Layout.createParallelGroup()
              .addGroup(OBJ_32Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(OBJ_32Layout.createParallelGroup()
                  .addGroup(OBJ_32Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(SCCLS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(OBJ_32Layout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(CLINOM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(11, 11, 11)
                .addGroup(OBJ_32Layout.createParallelGroup()
                  .addGroup(OBJ_32Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(SCMTS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(OBJ_32Layout.createParallelGroup()
                  .addGroup(OBJ_32Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(SCNFS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(OBJ_32Layout.createParallelGroup()
                  .addGroup(OBJ_32Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(SCDBSX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(OBJ_32Layout.createParallelGroup()
                  .addGroup(OBJ_32Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(SCDDEX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(OBJ_32Layout.createParallelGroup()
                  .addGroup(OBJ_32Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(SCDRSX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(OBJ_32Layout.createParallelGroup()
                  .addGroup(OBJ_32Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(SCCAP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(OBJ_32Layout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(CLINOM2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
          );
        }
        p_contenu.add(OBJ_32);
        OBJ_32.setBounds(10, 10, 675, 350);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- invite ----
      invite.setText("Choix possibles");
      invite.setName("invite");
      invite.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          inviteActionPerformed(e);
        }
      });
      BTD.add(invite);

      //---- aide ----
      aide.setText("Aide en ligne");
      aide.setName("aide");
      aide.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          aideActionPerformed(e);
        }
      });
      BTD.add(aide);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private JPanel p_contenu;
  private JPanel OBJ_32;
  private RiZoneSortie CLINOM;
  private JLabel OBJ_33;
  private JLabel OBJ_36;
  private JLabel OBJ_39;
  private XRiTextField SCCLS;
  private XRiTextField SCMTS;
  private XRiTextField SCCAP;
  private JLabel OBJ_37;
  private JLabel OBJ_40;
  private JLabel OBJ_41;
  private JLabel OBJ_42;
  private XRiTextField SCNFS;
  private XRiCalendrier SCDBSX;
  private XRiCalendrier SCDDEX;
  private XRiCalendrier SCDRSX;
  private RiZoneSortie CLINOM2;
  private JPopupMenu BTD;
  private JMenuItem invite;
  private JMenuItem aide;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
