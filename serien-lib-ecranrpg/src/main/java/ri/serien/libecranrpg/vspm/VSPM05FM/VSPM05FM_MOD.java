
package ri.serien.libecranrpg.vspm.VSPM05FM;
// MON PACKAGE

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;

/**
 * @author Stéphane Vénéri
 */
public class VSPM05FM_MOD extends SNPanelEcranRPG implements ioFrame {
   
  
  // private oFrame master=null;
  
  public VSPM05FM_MOD(SNPanelEcranRPG parent) {
    super(parent);
    // master = parent;
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    // Ajout
    initDiverses();
    SCWEB.setValeursSelection("1", " ");
    SCS61.setValeursSelection("1", " ");
    SCSIM.setValeursSelection("1", " ");
    SCSK1.setValeursSelection("1", " ");
    SCSD2.setValeursSelection("1", " ");
    SCS33.setValeursSelection("1", " ");
    SCSA1.setValeursSelection("1", " ");
    SCS41.setValeursSelection("1", " ");
    SCS32.setValeursSelection("1", " ");
    SCS31.setValeursSelection("1", " ");
    SCDAM.setValeursSelection("1", " ");
    SCTLM.setValeursSelection("1", " ");
    SCTDS.setValeursSelection("1", " ");
    SCGSM.setValeursSelection("1", " ");
    SCPAM.setValeursSelection("1", " ");
    SCGMM.setValeursSelection("1", " ");
    SCGPM.setValeursSelection("1", " ");
    SCGAM.setValeursSelection("1", " ");
    SCLTM.setValeursSelection("1", " ");
    SCPRM.setValeursSelection("1", " ");
    SCTRM.setValeursSelection("1", " ");
    SCGVM.setValeursSelection("1", " ");
    SCGIM.setValeursSelection("1", " ");
    SCTIM.setValeursSelection("1", " ");
    SCGEM.setValeursSelection("1", " ");
    SCGLF.setValeursSelection("1", " ");
    SCGTM.setValeursSelection("1", " ");
    SCTBM.setValeursSelection("1", " ");
    SCCGM.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // icones
    bouton_valider.setIcon(lexique.chargerImage("images/OK_p.png", true));
    bouton_retour.setIcon(lexique.chargerImage("images/retour_p.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Client RI - Modules actifs"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  @Override
  public void dispose() {
    getJDialog().dispose();
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    closePopupLinkWithBuffer(true);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    getData();
    closePopupLinkWithBuffer(true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel8 = new JPanel();
    SCCGM = new XRiCheckBox();
    separator1 = compFactory.createSeparator("Finance");
    separator2 = compFactory.createSeparator("Gestion commerciale");
    SCTBM = new XRiCheckBox();
    SCGTM = new XRiCheckBox();
    SCGLF = new XRiCheckBox();
    SCGEM = new XRiCheckBox();
    SCTIM = new XRiCheckBox();
    SCGIM = new XRiCheckBox();
    SCGVM = new XRiCheckBox();
    SCTRM = new XRiCheckBox();
    SCPRM = new XRiCheckBox();
    SCLTM = new XRiCheckBox();
    SCGAM = new XRiCheckBox();
    SCGPM = new XRiCheckBox();
    SCGMM = new XRiCheckBox();
    separator3 = compFactory.createSeparator("Gestion du personnel");
    separator4 = compFactory.createSeparator("Sp\u00e9ciaux");
    SCPAM = new XRiCheckBox();
    SCGSM = new XRiCheckBox();
    SCTDS = new XRiCheckBox();
    SCTLM = new XRiCheckBox();
    SCDAM = new XRiCheckBox();
    SCS31 = new XRiCheckBox();
    SCS32 = new XRiCheckBox();
    SCS41 = new XRiCheckBox();
    SCSA1 = new XRiCheckBox();
    SCS33 = new XRiCheckBox();
    SCSD2 = new XRiCheckBox();
    SCSK1 = new XRiCheckBox();
    SCSIM = new XRiCheckBox();
    SCS61 = new XRiCheckBox();
    SCWEB = new XRiCheckBox();

    //======== this ========
    setMinimumSize(new Dimension(900, 360));
    setPreferredSize(new Dimension(900, 350));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setPreferredSize(new Dimension(955, 215));
      p_principal.setMinimumSize(new Dimension(955, 215));
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel8 ========
        {
          panel8.setBorder(new TitledBorder("Modules"));
          panel8.setOpaque(false);
          panel8.setName("panel8");
          panel8.setLayout(null);

          //---- SCCGM ----
          SCCGM.setText("CGM");
          SCCGM.setName("SCCGM");
          panel8.add(SCCGM);
          SCCGM.setBounds(35, 55, 70, SCCGM.getPreferredSize().height);

          //---- separator1 ----
          separator1.setName("separator1");
          panel8.add(separator1);
          separator1.setBounds(25, 30, 640, separator1.getPreferredSize().height);

          //---- separator2 ----
          separator2.setName("separator2");
          panel8.add(separator2);
          separator2.setBounds(25, 95, 640, separator2.getPreferredSize().height);

          //---- SCTBM ----
          SCTBM.setText("TBM");
          SCTBM.setName("SCTBM");
          panel8.add(SCTBM);
          SCTBM.setBounds(127, 55, 70, SCTBM.getPreferredSize().height);

          //---- SCGTM ----
          SCGTM.setText("GTM");
          SCGTM.setName("SCGTM");
          panel8.add(SCGTM);
          SCGTM.setBounds(219, 55, 70, SCGTM.getPreferredSize().height);

          //---- SCGLF ----
          SCGLF.setText("GLF");
          SCGLF.setName("SCGLF");
          panel8.add(SCGLF);
          SCGLF.setBounds(311, 55, 70, SCGLF.getPreferredSize().height);

          //---- SCGEM ----
          SCGEM.setText("GEM");
          SCGEM.setName("SCGEM");
          panel8.add(SCGEM);
          SCGEM.setBounds(403, 55, 70, SCGEM.getPreferredSize().height);

          //---- SCTIM ----
          SCTIM.setText("TIM");
          SCTIM.setName("SCTIM");
          panel8.add(SCTIM);
          SCTIM.setBounds(495, 55, 70, SCTIM.getPreferredSize().height);

          //---- SCGIM ----
          SCGIM.setText("GIM");
          SCGIM.setName("SCGIM");
          panel8.add(SCGIM);
          SCGIM.setBounds(587, 55, 70, SCGIM.getPreferredSize().height);

          //---- SCGVM ----
          SCGVM.setText("GVM");
          SCGVM.setName("SCGVM");
          panel8.add(SCGVM);
          SCGVM.setBounds(35, 120, 70, SCGVM.getPreferredSize().height);

          //---- SCTRM ----
          SCTRM.setText("TRM");
          SCTRM.setName("SCTRM");
          panel8.add(SCTRM);
          SCTRM.setBounds(125, 120, 70, SCTRM.getPreferredSize().height);

          //---- SCPRM ----
          SCPRM.setText("PRM");
          SCPRM.setName("SCPRM");
          panel8.add(SCPRM);
          SCPRM.setBounds(220, 120, 70, SCPRM.getPreferredSize().height);

          //---- SCLTM ----
          SCLTM.setText("LTM");
          SCLTM.setName("SCLTM");
          panel8.add(SCLTM);
          SCLTM.setBounds(310, 120, 70, SCLTM.getPreferredSize().height);

          //---- SCGAM ----
          SCGAM.setText("GAM");
          SCGAM.setName("SCGAM");
          panel8.add(SCGAM);
          SCGAM.setBounds(400, 120, 70, SCGAM.getPreferredSize().height);

          //---- SCGPM ----
          SCGPM.setText("GPM");
          SCGPM.setName("SCGPM");
          panel8.add(SCGPM);
          SCGPM.setBounds(495, 120, 70, SCGPM.getPreferredSize().height);

          //---- SCGMM ----
          SCGMM.setText("GMM");
          SCGMM.setName("SCGMM");
          panel8.add(SCGMM);
          SCGMM.setBounds(587, 120, 70, SCGMM.getPreferredSize().height);

          //---- separator3 ----
          separator3.setName("separator3");
          panel8.add(separator3);
          separator3.setBounds(25, 155, 640, separator3.getPreferredSize().height);

          //---- separator4 ----
          separator4.setName("separator4");
          panel8.add(separator4);
          separator4.setBounds(25, 210, 640, separator4.getPreferredSize().height);

          //---- SCPAM ----
          SCPAM.setText("PAM");
          SCPAM.setName("SCPAM");
          panel8.add(SCPAM);
          SCPAM.setBounds(35, 180, 70, SCPAM.getPreferredSize().height);

          //---- SCGSM ----
          SCGSM.setText("GSM");
          SCGSM.setName("SCGSM");
          panel8.add(SCGSM);
          SCGSM.setBounds(125, 180, 70, SCGSM.getPreferredSize().height);

          //---- SCTDS ----
          SCTDS.setText("TDS");
          SCTDS.setName("SCTDS");
          panel8.add(SCTDS);
          SCTDS.setBounds(220, 180, 70, SCTDS.getPreferredSize().height);

          //---- SCTLM ----
          SCTLM.setText("TLM");
          SCTLM.setName("SCTLM");
          panel8.add(SCTLM);
          SCTLM.setBounds(35, 235, 70, SCTLM.getPreferredSize().height);

          //---- SCDAM ----
          SCDAM.setText("DAM");
          SCDAM.setName("SCDAM");
          panel8.add(SCDAM);
          SCDAM.setBounds(125, 235, 70, SCDAM.getPreferredSize().height);

          //---- SCS31 ----
          SCS31.setText("S31");
          SCS31.setName("SCS31");
          panel8.add(SCS31);
          SCS31.setBounds(215, 235, 70, SCS31.getPreferredSize().height);

          //---- SCS32 ----
          SCS32.setText("S32");
          SCS32.setName("SCS32");
          panel8.add(SCS32);
          SCS32.setBounds(310, 235, 70, SCS32.getPreferredSize().height);

          //---- SCS41 ----
          SCS41.setText("S41");
          SCS41.setName("SCS41");
          panel8.add(SCS41);
          SCS41.setBounds(400, 235, 70, SCS41.getPreferredSize().height);

          //---- SCSA1 ----
          SCSA1.setText("SA1");
          SCSA1.setName("SCSA1");
          panel8.add(SCSA1);
          SCSA1.setBounds(495, 235, 70, SCSA1.getPreferredSize().height);

          //---- SCS33 ----
          SCS33.setText("S33");
          SCS33.setName("SCS33");
          panel8.add(SCS33);
          SCS33.setBounds(587, 235, 70, SCS33.getPreferredSize().height);

          //---- SCSD2 ----
          SCSD2.setText("SD2");
          SCSD2.setName("SCSD2");
          panel8.add(SCSD2);
          SCSD2.setBounds(35, 260, 70, SCSD2.getPreferredSize().height);

          //---- SCSK1 ----
          SCSK1.setText("SK1");
          SCSK1.setName("SCSK1");
          panel8.add(SCSK1);
          SCSK1.setBounds(125, 260, 70, SCSK1.getPreferredSize().height);

          //---- SCSIM ----
          SCSIM.setText("SIM");
          SCSIM.setName("SCSIM");
          panel8.add(SCSIM);
          SCSIM.setBounds(215, 260, 70, SCSIM.getPreferredSize().height);

          //---- SCS61 ----
          SCS61.setText("S61");
          SCS61.setName("SCS61");
          panel8.add(SCS61);
          SCS61.setBounds(310, 260, 70, SCS61.getPreferredSize().height);

          //---- SCWEB ----
          SCWEB.setText("WEB");
          SCWEB.setName("SCWEB");
          panel8.add(SCWEB);
          SCWEB.setBounds(400, 260, 70, SCWEB.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel8.getComponentCount(); i++) {
              Rectangle bounds = panel8.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel8.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel8.setMinimumSize(preferredSize);
            panel8.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel8, GroupLayout.DEFAULT_SIZE, 706, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel8, GroupLayout.DEFAULT_SIZE, 324, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel8;
  private XRiCheckBox SCCGM;
  private JComponent separator1;
  private JComponent separator2;
  private XRiCheckBox SCTBM;
  private XRiCheckBox SCGTM;
  private XRiCheckBox SCGLF;
  private XRiCheckBox SCGEM;
  private XRiCheckBox SCTIM;
  private XRiCheckBox SCGIM;
  private XRiCheckBox SCGVM;
  private XRiCheckBox SCTRM;
  private XRiCheckBox SCPRM;
  private XRiCheckBox SCLTM;
  private XRiCheckBox SCGAM;
  private XRiCheckBox SCGPM;
  private XRiCheckBox SCGMM;
  private JComponent separator3;
  private JComponent separator4;
  private XRiCheckBox SCPAM;
  private XRiCheckBox SCGSM;
  private XRiCheckBox SCTDS;
  private XRiCheckBox SCTLM;
  private XRiCheckBox SCDAM;
  private XRiCheckBox SCS31;
  private XRiCheckBox SCS32;
  private XRiCheckBox SCS41;
  private XRiCheckBox SCSA1;
  private XRiCheckBox SCS33;
  private XRiCheckBox SCSD2;
  private XRiCheckBox SCSK1;
  private XRiCheckBox SCSIM;
  private XRiCheckBox SCS61;
  private XRiCheckBox SCWEB;
  // JFormDesigner - End of variables declaration  //GEN-END:variables



}
