
package ri.serien.libecranrpg.vspm.VSPM12FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VSPM12FM_A2 extends SNPanelEcranRPG implements ioFrame {
   
  
  public VSPM12FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    MAIL.setValeursSelection("X", " ");
    SCNMC.setValeursSelection("X", " ");
    WNCTD.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    riZoneSortie1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@*USER@")).trim());
    WNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNOM@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    NBUTI.setEnabled(lexique.isPresent("NBUTI"));
    NUMLIV.setEnabled(lexique.isPresent("NUMLIV"));
    OBJ_130.setVisible(lexique.isPresent("MDPSK1"));
    OBJ_128.setVisible(lexique.isPresent("MDPTDS"));
    OBJ_126.setVisible(lexique.isPresent("MDPGSM"));
    OBJ_104.setVisible(lexique.isPresent("MDPWEB"));
    OBJ_102.setVisible(lexique.isPresent("MDPS61"));
    OBJ_100.setVisible(lexique.isPresent("MDPS11"));
    OBJ_98.setVisible(lexique.isPresent("MDPSD2"));
    OBJ_96.setVisible(lexique.isPresent("MDPS33"));
    OBJ_94.setVisible(lexique.isPresent("MDPSA1"));
    OBJ_92.setVisible(lexique.isPresent("MDPS41"));
    OBJ_90.setVisible(lexique.isPresent("MDPS32"));
    OBJ_88.setVisible(lexique.isPresent("MDPS31"));
    OBJ_86.setVisible(lexique.isPresent("MDPSD1"));
    OBJ_83.setVisible(lexique.isPresent("MDPGMM"));
    OBJ_81.setVisible(lexique.isPresent("MDPGNM"));
    OBJ_79.setVisible(lexique.isPresent("MDPGPM"));
    OBJ_77.setVisible(lexique.isPresent("MDPGAM"));
    OBJ_74.setVisible(lexique.isPresent("MDPLTM"));
    OBJ_72.setVisible(lexique.isPresent("MDPPRM"));
    OBJ_70.setVisible(lexique.isPresent("MDPTRM"));
    OBJ_68.setVisible(lexique.isPresent("MDPGVM"));
    OBJ_65.setVisible(lexique.isPresent("MDPPAM"));
    OBJ_63.setVisible(lexique.isPresent("MDPGIM"));
    OBJ_60.setVisible(lexique.isPresent("MDPTIM"));
    OBJ_59.setVisible(lexique.isPresent("MDPGEM"));
    OBJ_56.setVisible(lexique.isPresent("MDPGLF"));
    OBJ_54.setVisible(lexique.isPresent("MDPGTM"));
    OBJ_52.setVisible(lexique.isPresent("MDPTBM"));
    OBJ_51.setVisible(lexique.isPresent("MDPCGM"));
    OBJ_49.setVisible(lexique.isPresent("MDPTEL"));
    OBJ_47.setVisible(lexique.isPresent("MDPEXP"));
    OBJ_106.setVisible(lexique.isPresent("MDPSP1"));
    NBBUL.setEnabled(lexique.isPresent("NBBUL"));
    NUMCLI.setEnabled(lexique.isPresent("NUMCLI"));
    MDPSK1.setVisible(lexique.isPresent("MDPSK1"));
    MDPTDS.setVisible(lexique.isPresent("MDPTDS"));
    MDPGSM.setVisible(lexique.isPresent("MDPGSM"));
    MDPGEM.setVisible(lexique.isPresent("MDPGEM"));
    MDPCGM.setVisible(lexique.isPresent("MDPCGM"));
    MDPSP1.setVisible(lexique.isPresent("MDPSP1"));
    MDPWEB.setVisible(lexique.isPresent("MDPWEB"));
    MDPS61.setVisible(lexique.isPresent("MDPS61"));
    MDPS11.setVisible(lexique.isPresent("MDPS11"));
    MDPSD2.setVisible(lexique.isPresent("MDPSD2"));
    MDPS33.setVisible(lexique.isPresent("MDPS33"));
    MDPSA1.setVisible(lexique.isPresent("MDPSA1"));
    MDPS41.setVisible(lexique.isPresent("MDPS41"));
    MDPS32.setVisible(lexique.isPresent("MDPS32"));
    MDPS31.setVisible(lexique.isPresent("MDPS31"));
    MDPSD1.setVisible(lexique.isPresent("MDPSD1"));
    MDPGMM.setVisible(lexique.isPresent("MDPGMM"));
    MDPGNM.setVisible(lexique.isPresent("MDPGNM"));
    MDPGPM.setVisible(lexique.isPresent("MDPGPM"));
    MDPGAM.setVisible(lexique.isPresent("MDPGAM"));
    MDPLTM.setVisible(lexique.isPresent("MDPLTM"));
    MDPPRM.setVisible(lexique.isPresent("MDPPRM"));
    MDPTRM.setVisible(lexique.isPresent("MDPTRM"));
    MDPGVM.setVisible(lexique.isPresent("MDPGVM"));
    MDPPAM.setVisible(lexique.isPresent("MDPPAM"));
    MDPGIM.setVisible(lexique.isPresent("MDPGIM"));
    MDPTIM.setVisible(lexique.isPresent("MDPTIM"));
    MDPGLF.setVisible(lexique.isPresent("MDPGLF"));
    MDPGTM.setVisible(lexique.isPresent("MDPGTM"));
    MDPTBM.setVisible(lexique.isPresent("MDPTBM"));
    MDPTEL.setVisible(lexique.isPresent("MDPTEL"));
    MDPEXP.setVisible(lexique.isPresent("MDPEXP"));
    SN8.setEnabled(lexique.isPresent("SN8"));
    // MAIL.setSelected(lexique.HostFieldGetData("MAIL").equalsIgnoreCase("X"));
    // SCNMC.setVisible( lexique.isPresent("SCNMC"));
    // SCNMC.setSelected(lexique.HostFieldGetData("SCNMC").equalsIgnoreCase("X"));
    // WNCTD.setSelected(lexique.HostFieldGetData("WNCTD").equalsIgnoreCase("OUI"));
    WNOM.setEnabled(lexique.isPresent("WNOM"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Licences"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (MAIL.isSelected())
    // lexique.HostFieldPutData("MAIL", 0, "X");
    // else
    // lexique.HostFieldPutData("MAIL", 0, " ");
    // if (SCNMC.isSelected())
    // lexique.HostFieldPutData("SCNMC", 0, "X");
    // else
    // lexique.HostFieldPutData("SCNMC", 0, " ");
    // if (WNCTD.isSelected())
    // lexique.HostFieldPutData("WNCTD", 0, "OUI");
    // else
    // lexique.HostFieldPutData("WNCTD", 0, "NON");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    riZoneSortie1 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    MDPEXP = new XRiTextField();
    MDPTEL = new XRiTextField();
    MDPTBM = new XRiTextField();
    MDPGTM = new XRiTextField();
    MDPGLF = new XRiTextField();
    MDPTIM = new XRiTextField();
    MDPGIM = new XRiTextField();
    MDPPAM = new XRiTextField();
    MDPGVM = new XRiTextField();
    MDPTRM = new XRiTextField();
    MDPPRM = new XRiTextField();
    MDPLTM = new XRiTextField();
    MDPGAM = new XRiTextField();
    MDPGPM = new XRiTextField();
    MDPGNM = new XRiTextField();
    MDPGMM = new XRiTextField();
    MDPSD1 = new XRiTextField();
    MDPS31 = new XRiTextField();
    MDPS32 = new XRiTextField();
    MDPS41 = new XRiTextField();
    MDPSA1 = new XRiTextField();
    MDPS33 = new XRiTextField();
    MDPSD2 = new XRiTextField();
    MDPS11 = new XRiTextField();
    MDPS61 = new XRiTextField();
    MDPWEB = new XRiTextField();
    MDPSP1 = new XRiTextField();
    MDPCGM = new XRiTextField();
    MDPGEM = new XRiTextField();
    MDPGSM = new XRiTextField();
    MDPTDS = new XRiTextField();
    MDPSK1 = new XRiTextField();
    OBJ_106 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_49 = new JLabel();
    OBJ_51 = new JLabel();
    OBJ_52 = new JLabel();
    OBJ_54 = new JLabel();
    OBJ_56 = new JLabel();
    OBJ_59 = new JLabel();
    OBJ_60 = new JLabel();
    OBJ_63 = new JLabel();
    OBJ_65 = new JLabel();
    OBJ_68 = new JLabel();
    OBJ_70 = new JLabel();
    OBJ_72 = new JLabel();
    OBJ_74 = new JLabel();
    OBJ_77 = new JLabel();
    OBJ_79 = new JLabel();
    OBJ_81 = new JLabel();
    OBJ_83 = new JLabel();
    OBJ_86 = new JLabel();
    OBJ_88 = new JLabel();
    OBJ_90 = new JLabel();
    OBJ_92 = new JLabel();
    OBJ_94 = new JLabel();
    OBJ_96 = new JLabel();
    OBJ_98 = new JLabel();
    OBJ_100 = new JLabel();
    OBJ_102 = new JLabel();
    OBJ_104 = new JLabel();
    OBJ_126 = new JLabel();
    OBJ_128 = new JLabel();
    OBJ_130 = new JLabel();
    panel2 = new JPanel();
    WNOM = new RiZoneSortie();
    WNCTD = new XRiCheckBox();
    SCNMC = new XRiCheckBox();
    OBJ_57 = new JLabel();
    MAIL = new XRiCheckBox();
    OBJ_61 = new JLabel();
    OBJ_62 = new JLabel();
    OBJ_48 = new JLabel();
    SN8 = new XRiTextField();
    NUMCLI = new XRiTextField();
    NBBUL = new XRiTextField();
    NUMLIV = new XRiTextField();
    NBUTI = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- riZoneSortie1 ----
          riZoneSortie1.setText("@*USER@");
          riZoneSortie1.setOpaque(false);
          riZoneSortie1.setName("riZoneSortie1");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(riZoneSortie1, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(riZoneSortie1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(700, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Modules"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- MDPEXP ----
            MDPEXP.setName("MDPEXP");
            panel1.add(MDPEXP);
            MDPEXP.setBounds(92, 35, 74, MDPEXP.getPreferredSize().height);

            //---- MDPTEL ----
            MDPTEL.setName("MDPTEL");
            panel1.add(MDPTEL);
            MDPTEL.setBounds(232, 35, 74, MDPTEL.getPreferredSize().height);

            //---- MDPTBM ----
            MDPTBM.setName("MDPTBM");
            panel1.add(MDPTBM);
            MDPTBM.setBounds(232, 59, 74, MDPTBM.getPreferredSize().height);

            //---- MDPGTM ----
            MDPGTM.setName("MDPGTM");
            panel1.add(MDPGTM);
            MDPGTM.setBounds(377, 59, 74, MDPGTM.getPreferredSize().height);

            //---- MDPGLF ----
            MDPGLF.setName("MDPGLF");
            panel1.add(MDPGLF);
            MDPGLF.setBounds(527, 59, 74, MDPGLF.getPreferredSize().height);

            //---- MDPTIM ----
            MDPTIM.setName("MDPTIM");
            panel1.add(MDPTIM);
            MDPTIM.setBounds(232, 83, 74, MDPTIM.getPreferredSize().height);

            //---- MDPGIM ----
            MDPGIM.setName("MDPGIM");
            panel1.add(MDPGIM);
            MDPGIM.setBounds(92, 107, 74, MDPGIM.getPreferredSize().height);

            //---- MDPPAM ----
            MDPPAM.setName("MDPPAM");
            panel1.add(MDPPAM);
            MDPPAM.setBounds(92, 131, 74, MDPPAM.getPreferredSize().height);

            //---- MDPGVM ----
            MDPGVM.setName("MDPGVM");
            panel1.add(MDPGVM);
            MDPGVM.setBounds(92, 155, 74, MDPGVM.getPreferredSize().height);

            //---- MDPTRM ----
            MDPTRM.setName("MDPTRM");
            panel1.add(MDPTRM);
            MDPTRM.setBounds(232, 155, 74, MDPTRM.getPreferredSize().height);

            //---- MDPPRM ----
            MDPPRM.setName("MDPPRM");
            panel1.add(MDPPRM);
            MDPPRM.setBounds(377, 155, 74, MDPPRM.getPreferredSize().height);

            //---- MDPLTM ----
            MDPLTM.setName("MDPLTM");
            panel1.add(MDPLTM);
            MDPLTM.setBounds(527, 155, 74, MDPLTM.getPreferredSize().height);

            //---- MDPGAM ----
            MDPGAM.setName("MDPGAM");
            panel1.add(MDPGAM);
            MDPGAM.setBounds(92, 179, 74, MDPGAM.getPreferredSize().height);

            //---- MDPGPM ----
            MDPGPM.setName("MDPGPM");
            panel1.add(MDPGPM);
            MDPGPM.setBounds(232, 179, 74, MDPGPM.getPreferredSize().height);

            //---- MDPGNM ----
            MDPGNM.setName("MDPGNM");
            panel1.add(MDPGNM);
            MDPGNM.setBounds(377, 179, 74, MDPGNM.getPreferredSize().height);

            //---- MDPGMM ----
            MDPGMM.setName("MDPGMM");
            panel1.add(MDPGMM);
            MDPGMM.setBounds(527, 179, 74, MDPGMM.getPreferredSize().height);

            //---- MDPSD1 ----
            MDPSD1.setName("MDPSD1");
            panel1.add(MDPSD1);
            MDPSD1.setBounds(92, 203, 74, MDPSD1.getPreferredSize().height);

            //---- MDPS31 ----
            MDPS31.setName("MDPS31");
            panel1.add(MDPS31);
            MDPS31.setBounds(232, 203, 74, MDPS31.getPreferredSize().height);

            //---- MDPS32 ----
            MDPS32.setName("MDPS32");
            panel1.add(MDPS32);
            MDPS32.setBounds(377, 203, 74, MDPS32.getPreferredSize().height);

            //---- MDPS41 ----
            MDPS41.setName("MDPS41");
            panel1.add(MDPS41);
            MDPS41.setBounds(527, 203, 74, MDPS41.getPreferredSize().height);

            //---- MDPSA1 ----
            MDPSA1.setName("MDPSA1");
            panel1.add(MDPSA1);
            MDPSA1.setBounds(92, 227, 74, MDPSA1.getPreferredSize().height);

            //---- MDPS33 ----
            MDPS33.setName("MDPS33");
            panel1.add(MDPS33);
            MDPS33.setBounds(232, 227, 74, MDPS33.getPreferredSize().height);

            //---- MDPSD2 ----
            MDPSD2.setName("MDPSD2");
            panel1.add(MDPSD2);
            MDPSD2.setBounds(377, 227, 74, MDPSD2.getPreferredSize().height);

            //---- MDPS11 ----
            MDPS11.setName("MDPS11");
            panel1.add(MDPS11);
            MDPS11.setBounds(92, 251, 74, MDPS11.getPreferredSize().height);

            //---- MDPS61 ----
            MDPS61.setName("MDPS61");
            panel1.add(MDPS61);
            MDPS61.setBounds(232, 251, 74, MDPS61.getPreferredSize().height);

            //---- MDPWEB ----
            MDPWEB.setName("MDPWEB");
            panel1.add(MDPWEB);
            MDPWEB.setBounds(377, 251, 74, MDPWEB.getPreferredSize().height);

            //---- MDPSP1 ----
            MDPSP1.setName("MDPSP1");
            panel1.add(MDPSP1);
            MDPSP1.setBounds(527, 251, 74, MDPSP1.getPreferredSize().height);

            //---- MDPCGM ----
            MDPCGM.setName("MDPCGM");
            panel1.add(MDPCGM);
            MDPCGM.setBounds(92, 59, 74, MDPCGM.getPreferredSize().height);

            //---- MDPGEM ----
            MDPGEM.setName("MDPGEM");
            panel1.add(MDPGEM);
            MDPGEM.setBounds(92, 83, 74, MDPGEM.getPreferredSize().height);

            //---- MDPGSM ----
            MDPGSM.setName("MDPGSM");
            panel1.add(MDPGSM);
            MDPGSM.setBounds(232, 131, 74, MDPGSM.getPreferredSize().height);

            //---- MDPTDS ----
            MDPTDS.setName("MDPTDS");
            panel1.add(MDPTDS);
            MDPTDS.setBounds(377, 131, 74, MDPTDS.getPreferredSize().height);

            //---- MDPSK1 ----
            MDPSK1.setName("MDPSK1");
            panel1.add(MDPSK1);
            MDPSK1.setBounds(527, 131, 74, MDPSK1.getPreferredSize().height);

            //---- OBJ_106 ----
            OBJ_106.setText("Sp\u00e9 N\u00b0");
            OBJ_106.setName("OBJ_106");
            panel1.add(OBJ_106);
            OBJ_106.setBounds(478, 255, 45, 20);

            //---- OBJ_47 ----
            OBJ_47.setText("EXPL");
            OBJ_47.setName("OBJ_47");
            panel1.add(OBJ_47);
            OBJ_47.setBounds(38, 39, 39, 20);

            //---- OBJ_49 ----
            OBJ_49.setText("T.L.M");
            OBJ_49.setName("OBJ_49");
            panel1.add(OBJ_49);
            OBJ_49.setBounds(183, 39, 39, 20);

            //---- OBJ_51 ----
            OBJ_51.setText("C.G.M");
            OBJ_51.setName("OBJ_51");
            panel1.add(OBJ_51);
            OBJ_51.setBounds(38, 63, 39, 20);

            //---- OBJ_52 ----
            OBJ_52.setText("T.B.M");
            OBJ_52.setName("OBJ_52");
            panel1.add(OBJ_52);
            OBJ_52.setBounds(183, 63, 39, 20);

            //---- OBJ_54 ----
            OBJ_54.setText("G.T.M");
            OBJ_54.setName("OBJ_54");
            panel1.add(OBJ_54);
            OBJ_54.setBounds(328, 63, 39, 20);

            //---- OBJ_56 ----
            OBJ_56.setText("G.L.F");
            OBJ_56.setName("OBJ_56");
            panel1.add(OBJ_56);
            OBJ_56.setBounds(478, 63, 39, 20);

            //---- OBJ_59 ----
            OBJ_59.setText("G.E.M");
            OBJ_59.setName("OBJ_59");
            panel1.add(OBJ_59);
            OBJ_59.setBounds(38, 87, 39, 20);

            //---- OBJ_60 ----
            OBJ_60.setText("T.I.M");
            OBJ_60.setName("OBJ_60");
            panel1.add(OBJ_60);
            OBJ_60.setBounds(183, 87, 39, 20);

            //---- OBJ_63 ----
            OBJ_63.setText("G.I.M");
            OBJ_63.setName("OBJ_63");
            panel1.add(OBJ_63);
            OBJ_63.setBounds(38, 111, 39, 20);

            //---- OBJ_65 ----
            OBJ_65.setText("P.A.M");
            OBJ_65.setName("OBJ_65");
            panel1.add(OBJ_65);
            OBJ_65.setBounds(38, 135, 39, 20);

            //---- OBJ_68 ----
            OBJ_68.setText("G.V.M");
            OBJ_68.setName("OBJ_68");
            panel1.add(OBJ_68);
            OBJ_68.setBounds(38, 159, 39, 20);

            //---- OBJ_70 ----
            OBJ_70.setText("T.R.M");
            OBJ_70.setName("OBJ_70");
            panel1.add(OBJ_70);
            OBJ_70.setBounds(183, 159, 39, 20);

            //---- OBJ_72 ----
            OBJ_72.setText("P.R.M");
            OBJ_72.setName("OBJ_72");
            panel1.add(OBJ_72);
            OBJ_72.setBounds(328, 159, 39, 20);

            //---- OBJ_74 ----
            OBJ_74.setText("L.T.M");
            OBJ_74.setName("OBJ_74");
            panel1.add(OBJ_74);
            OBJ_74.setBounds(478, 160, 39, 20);

            //---- OBJ_77 ----
            OBJ_77.setText("G.A.M");
            OBJ_77.setName("OBJ_77");
            panel1.add(OBJ_77);
            OBJ_77.setBounds(38, 183, 39, 20);

            //---- OBJ_79 ----
            OBJ_79.setText("G.P.M");
            OBJ_79.setName("OBJ_79");
            panel1.add(OBJ_79);
            OBJ_79.setBounds(183, 183, 39, 20);

            //---- OBJ_81 ----
            OBJ_81.setText("G.N.M");
            OBJ_81.setName("OBJ_81");
            panel1.add(OBJ_81);
            OBJ_81.setBounds(328, 183, 39, 20);

            //---- OBJ_83 ----
            OBJ_83.setText("G.M.M");
            OBJ_83.setName("OBJ_83");
            panel1.add(OBJ_83);
            OBJ_83.setBounds(478, 185, 39, 20);

            //---- OBJ_86 ----
            OBJ_86.setText("D.A.M");
            OBJ_86.setName("OBJ_86");
            panel1.add(OBJ_86);
            OBJ_86.setBounds(38, 207, 39, 20);

            //---- OBJ_88 ----
            OBJ_88.setText("S.3.1");
            OBJ_88.setName("OBJ_88");
            panel1.add(OBJ_88);
            OBJ_88.setBounds(183, 207, 39, 20);

            //---- OBJ_90 ----
            OBJ_90.setText("S.3.2");
            OBJ_90.setName("OBJ_90");
            panel1.add(OBJ_90);
            OBJ_90.setBounds(328, 207, 39, 20);

            //---- OBJ_92 ----
            OBJ_92.setText("S.4.1");
            OBJ_92.setName("OBJ_92");
            panel1.add(OBJ_92);
            OBJ_92.setBounds(478, 207, 39, 20);

            //---- OBJ_94 ----
            OBJ_94.setText("S.A.1");
            OBJ_94.setName("OBJ_94");
            panel1.add(OBJ_94);
            OBJ_94.setBounds(38, 231, 39, 20);

            //---- OBJ_96 ----
            OBJ_96.setText("S.3.3");
            OBJ_96.setName("OBJ_96");
            panel1.add(OBJ_96);
            OBJ_96.setBounds(183, 231, 39, 20);

            //---- OBJ_98 ----
            OBJ_98.setText("S.D.2");
            OBJ_98.setName("OBJ_98");
            panel1.add(OBJ_98);
            OBJ_98.setBounds(328, 231, 39, 20);

            //---- OBJ_100 ----
            OBJ_100.setText("S.I.M");
            OBJ_100.setName("OBJ_100");
            panel1.add(OBJ_100);
            OBJ_100.setBounds(38, 255, 39, 20);

            //---- OBJ_102 ----
            OBJ_102.setText("S.6.1");
            OBJ_102.setName("OBJ_102");
            panel1.add(OBJ_102);
            OBJ_102.setBounds(183, 255, 39, 20);

            //---- OBJ_104 ----
            OBJ_104.setText("W.E.B");
            OBJ_104.setName("OBJ_104");
            panel1.add(OBJ_104);
            OBJ_104.setBounds(328, 255, 39, 20);

            //---- OBJ_126 ----
            OBJ_126.setText("G.S.M");
            OBJ_126.setName("OBJ_126");
            panel1.add(OBJ_126);
            OBJ_126.setBounds(183, 135, 39, 20);

            //---- OBJ_128 ----
            OBJ_128.setText("N4DS");
            OBJ_128.setName("OBJ_128");
            panel1.add(OBJ_128);
            OBJ_128.setBounds(328, 135, 39, 20);

            //---- OBJ_130 ----
            OBJ_130.setText("G.H.M");
            OBJ_130.setName("OBJ_130");
            panel1.add(OBJ_130);
            OBJ_130.setBounds(478, 135, 39, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Param\u00e8tres pour le calcul des licences"));
            panel2.setOpaque(false);
            panel2.setName("panel2");

            //---- WNOM ----
            WNOM.setComponentPopupMenu(BTD);
            WNOM.setText("@WNOM@");
            WNOM.setName("WNOM");

            //---- WNCTD ----
            WNCTD.setText("Num\u00e9ros de licences d\u00e9finitifs");
            WNCTD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WNCTD.setName("WNCTD");

            //---- SCNMC ----
            SCNMC.setText("Num\u00e9ro sp\u00e9cifique S\u00e9rie M");
            SCNMC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SCNMC.setName("SCNMC");

            //---- OBJ_57 ----
            OBJ_57.setText("Num\u00e9ro de client chez R.I");
            OBJ_57.setName("OBJ_57");

            //---- MAIL ----
            MAIL.setText("Envoi du fichier par e-mail");
            MAIL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            MAIL.setName("MAIL");

            //---- OBJ_61 ----
            OBJ_61.setText("Nombre d'utilisateurs");
            OBJ_61.setName("OBJ_61");

            //---- OBJ_62 ----
            OBJ_62.setText("Nombre de bulletins de paie");
            OBJ_62.setName("OBJ_62");

            //---- OBJ_48 ----
            OBJ_48.setText("Num\u00e9ro de s\u00e9rie");
            OBJ_48.setName("OBJ_48");

            //---- SN8 ----
            SN8.setComponentPopupMenu(BTD);
            SN8.setName("SN8");

            //---- NUMCLI ----
            NUMCLI.setComponentPopupMenu(BTD);
            NUMCLI.setName("NUMCLI");

            //---- NBBUL ----
            NBBUL.setComponentPopupMenu(BTD);
            NBBUL.setName("NBBUL");

            //---- NUMLIV ----
            NUMLIV.setComponentPopupMenu(BTD);
            NUMLIV.setName("NUMLIV");

            //---- NBUTI ----
            NBUTI.setComponentPopupMenu(BTD);
            NBUTI.setName("NBUTI");

            GroupLayout panel2Layout = new GroupLayout(panel2);
            panel2.setLayout(panel2Layout);
            panel2Layout.setHorizontalGroup(
              panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(19, 19, 19)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(SN8, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(MAIL, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(NUMCLI, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(NUMLIV, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                      .addGap(6, 6, 6)
                      .addComponent(WNOM, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(NBUTI, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(NBBUL, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE))
                    .addComponent(WNCTD, GroupLayout.PREFERRED_SIZE, 350, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SCNMC, GroupLayout.PREFERRED_SIZE, 350, GroupLayout.PREFERRED_SIZE)))
            );
            panel2Layout.setVerticalGroup(
              panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addComponent(SN8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addGroup(panel2Layout.createParallelGroup()
                        .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(MAIL, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
                  .addGap(32, 32, 32)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(NUMCLI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NUMLIV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(WNOM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(7, 7, 7)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(NBUTI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(12, 12, 12)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(NBBUL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(12, 12, 12)
                  .addComponent(WNCTD, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addGap(10, 10, 10)
                  .addComponent(SCNMC, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                  .addComponent(panel1, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(panel2, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 299, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private RiZoneSortie riZoneSortie1;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField MDPEXP;
  private XRiTextField MDPTEL;
  private XRiTextField MDPTBM;
  private XRiTextField MDPGTM;
  private XRiTextField MDPGLF;
  private XRiTextField MDPTIM;
  private XRiTextField MDPGIM;
  private XRiTextField MDPPAM;
  private XRiTextField MDPGVM;
  private XRiTextField MDPTRM;
  private XRiTextField MDPPRM;
  private XRiTextField MDPLTM;
  private XRiTextField MDPGAM;
  private XRiTextField MDPGPM;
  private XRiTextField MDPGNM;
  private XRiTextField MDPGMM;
  private XRiTextField MDPSD1;
  private XRiTextField MDPS31;
  private XRiTextField MDPS32;
  private XRiTextField MDPS41;
  private XRiTextField MDPSA1;
  private XRiTextField MDPS33;
  private XRiTextField MDPSD2;
  private XRiTextField MDPS11;
  private XRiTextField MDPS61;
  private XRiTextField MDPWEB;
  private XRiTextField MDPSP1;
  private XRiTextField MDPCGM;
  private XRiTextField MDPGEM;
  private XRiTextField MDPGSM;
  private XRiTextField MDPTDS;
  private XRiTextField MDPSK1;
  private JLabel OBJ_106;
  private JLabel OBJ_47;
  private JLabel OBJ_49;
  private JLabel OBJ_51;
  private JLabel OBJ_52;
  private JLabel OBJ_54;
  private JLabel OBJ_56;
  private JLabel OBJ_59;
  private JLabel OBJ_60;
  private JLabel OBJ_63;
  private JLabel OBJ_65;
  private JLabel OBJ_68;
  private JLabel OBJ_70;
  private JLabel OBJ_72;
  private JLabel OBJ_74;
  private JLabel OBJ_77;
  private JLabel OBJ_79;
  private JLabel OBJ_81;
  private JLabel OBJ_83;
  private JLabel OBJ_86;
  private JLabel OBJ_88;
  private JLabel OBJ_90;
  private JLabel OBJ_92;
  private JLabel OBJ_94;
  private JLabel OBJ_96;
  private JLabel OBJ_98;
  private JLabel OBJ_100;
  private JLabel OBJ_102;
  private JLabel OBJ_104;
  private JLabel OBJ_126;
  private JLabel OBJ_128;
  private JLabel OBJ_130;
  private JPanel panel2;
  private RiZoneSortie WNOM;
  private XRiCheckBox WNCTD;
  private XRiCheckBox SCNMC;
  private JLabel OBJ_57;
  private XRiCheckBox MAIL;
  private JLabel OBJ_61;
  private JLabel OBJ_62;
  private JLabel OBJ_48;
  private XRiTextField SN8;
  private XRiTextField NUMCLI;
  private XRiTextField NBBUL;
  private XRiTextField NUMLIV;
  private XRiTextField NBUTI;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
