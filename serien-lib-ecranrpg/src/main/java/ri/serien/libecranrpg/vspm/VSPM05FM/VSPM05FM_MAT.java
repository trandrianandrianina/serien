
package ri.serien.libecranrpg.vspm.VSPM05FM;
// MON PACKAGE

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VSPM05FM_MAT extends SNPanelEcranRPG implements ioFrame {
   
  
  // private oFrame master=null;
  
  public VSPM05FM_MAT(SNPanelEcranRPG parent) {
    super(parent);
    // master = parent;
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    initDiverses();
    SCIN1.setValeursSelection("X", "");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    // master.setData();
    
    
    
    // icones
    bouton_valider.setIcon(lexique.chargerImage("images/OK_p.png", true));
    bouton_retour.setIcon(lexique.chargerImage("images/retour_p.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Client RI - Service Après Vente"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  @Override
  public void dispose() {
    getJDialog().dispose();
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    closePopupLinkWithBuffer(true);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    getData();
    closePopupLinkWithBuffer(true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel8 = new JPanel();
    OBJ_183 = new JLabel();
    OBJ_184 = new JLabel();
    SCVOS = new XRiTextField();
    SCSEN = new XRiTextField();
    OBJ_187 = new JLabel();
    OBJ_188 = new JLabel();
    SCTMA = new XRiTextField();
    SCDAFX = new XRiCalendrier();
    OBJ_185 = new JLabel();
    SCCIBM = new XRiTextField();
    OBJ_190 = new JLabel();
    SCFMLX = new XRiCalendrier();
    OBJ_186 = new JLabel();
    SCOBSM = new XRiTextField();
    SCFMTX = new XRiCalendrier();
    OBJ_191 = new JLabel();
    OBJ_189 = new JLabel();
    SCNFA = new XRiTextField();
    SCIN1 = new XRiCheckBox();

    //======== this ========
    setMinimumSize(new Dimension(900, 365));
    setPreferredSize(new Dimension(900, 360));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setPreferredSize(new Dimension(955, 215));
      p_principal.setMinimumSize(new Dimension(955, 215));
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel8 ========
        {
          panel8.setBorder(new TitledBorder("Mat\u00e9riel"));
          panel8.setOpaque(false);
          panel8.setName("panel8");
          panel8.setLayout(null);

          //---- OBJ_183 ----
          OBJ_183.setText("Num\u00e9ro de s\u00e9rie du serveur");
          OBJ_183.setName("OBJ_183");
          panel8.add(OBJ_183);
          OBJ_183.setBounds(35, 49, 195, 20);

          //---- OBJ_184 ----
          OBJ_184.setText("Version de l'OS");
          OBJ_184.setName("OBJ_184");
          panel8.add(OBJ_184);
          OBJ_184.setBounds(35, 84, 195, 20);

          //---- SCVOS ----
          SCVOS.setComponentPopupMenu(null);
          SCVOS.setName("SCVOS");
          panel8.add(SCVOS);
          SCVOS.setBounds(235, 80, 44, SCVOS.getPreferredSize().height);

          //---- SCSEN ----
          SCSEN.setComponentPopupMenu(null);
          SCSEN.setName("SCSEN");
          panel8.add(SCSEN);
          SCSEN.setBounds(235, 45, 94, SCSEN.getPreferredSize().height);

          //---- OBJ_187 ----
          OBJ_187.setText("Type de serveur");
          OBJ_187.setName("OBJ_187");
          panel8.add(OBJ_187);
          OBJ_187.setBounds(385, 49, 110, 20);

          //---- OBJ_188 ----
          OBJ_188.setText("Date d'achat du serveur");
          OBJ_188.setName("OBJ_188");
          panel8.add(OBJ_188);
          OBJ_188.setBounds(35, 154, 195, 20);

          //---- SCTMA ----
          SCTMA.setComponentPopupMenu(null);
          SCTMA.setName("SCTMA");
          panel8.add(SCTMA);
          SCTMA.setBounds(515, 45, 144, SCTMA.getPreferredSize().height);

          //---- SCDAFX ----
          SCDAFX.setComponentPopupMenu(null);
          SCDAFX.setName("SCDAFX");
          panel8.add(SCDAFX);
          SCDAFX.setBounds(235, 150, 110, SCDAFX.getPreferredSize().height);

          //---- OBJ_185 ----
          OBJ_185.setText("Num\u00e9ro de client IBM");
          OBJ_185.setName("OBJ_185");
          panel8.add(OBJ_185);
          OBJ_185.setBounds(35, 119, 195, 20);

          //---- SCCIBM ----
          SCCIBM.setComponentPopupMenu(null);
          SCCIBM.setName("SCCIBM");
          panel8.add(SCCIBM);
          SCCIBM.setBounds(235, 115, 68, 28);

          //---- OBJ_190 ----
          OBJ_190.setText("Fin de la maintenance logicielle");
          OBJ_190.setName("OBJ_190");
          panel8.add(OBJ_190);
          OBJ_190.setBounds(35, 259, 195, 20);

          //---- SCFMLX ----
          SCFMLX.setComponentPopupMenu(null);
          SCFMLX.setName("SCFMLX");
          panel8.add(SCFMLX);
          SCFMLX.setBounds(235, 255, 110, SCFMLX.getPreferredSize().height);

          //---- OBJ_186 ----
          OBJ_186.setText("Observations");
          OBJ_186.setName("OBJ_186");
          panel8.add(OBJ_186);
          OBJ_186.setBounds(35, 189, 195, 20);

          //---- SCOBSM ----
          SCOBSM.setComponentPopupMenu(null);
          SCOBSM.setName("SCOBSM");
          panel8.add(SCOBSM);
          SCOBSM.setBounds(235, 185, 424, SCOBSM.getPreferredSize().height);

          //---- SCFMTX ----
          SCFMTX.setComponentPopupMenu(null);
          SCFMTX.setName("SCFMTX");
          panel8.add(SCFMTX);
          SCFMTX.setBounds(235, 220, 110, SCFMTX.getPreferredSize().height);

          //---- OBJ_191 ----
          OBJ_191.setText("Fin de la maintenance mat\u00e9rielle");
          OBJ_191.setName("OBJ_191");
          panel8.add(OBJ_191);
          OBJ_191.setBounds(35, 224, 195, 20);

          //---- OBJ_189 ----
          OBJ_189.setText("Num\u00e9ro de facture I5");
          OBJ_189.setName("OBJ_189");
          panel8.add(OBJ_189);
          OBJ_189.setBounds(385, 154, 130, 20);

          //---- SCNFA ----
          SCNFA.setComponentPopupMenu(null);
          SCNFA.setName("SCNFA");
          panel8.add(SCNFA);
          SCNFA.setBounds(515, 150, 68, SCNFA.getPreferredSize().height);

          //---- SCIN1 ----
          SCIN1.setText("Mise \u00e0 jour par internet");
          SCIN1.setName("SCIN1");
          panel8.add(SCIN1);
          SCIN1.setBounds(35, 295, 345, SCIN1.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel8.getComponentCount(); i++) {
              Rectangle bounds = panel8.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel8.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel8.setMinimumSize(preferredSize);
            panel8.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel8, GroupLayout.DEFAULT_SIZE, 706, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel8, GroupLayout.DEFAULT_SIZE, 334, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel8;
  private JLabel OBJ_183;
  private JLabel OBJ_184;
  private XRiTextField SCVOS;
  private XRiTextField SCSEN;
  private JLabel OBJ_187;
  private JLabel OBJ_188;
  private XRiTextField SCTMA;
  private XRiCalendrier SCDAFX;
  private JLabel OBJ_185;
  private XRiTextField SCCIBM;
  private JLabel OBJ_190;
  private XRiCalendrier SCFMLX;
  private JLabel OBJ_186;
  private XRiTextField SCOBSM;
  private XRiCalendrier SCFMTX;
  private JLabel OBJ_191;
  private JLabel OBJ_189;
  private XRiTextField SCNFA;
  private XRiCheckBox SCIN1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables



}
