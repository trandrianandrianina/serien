
package ri.serien.libecranrpg.vspm.VSPM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VSPM03FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VSPM03FM_B1(ArrayList<?> param) {
    
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    panel2.setRightDecoration(TCI1);
    panel4.setRightDecoration(TCI2);
    panel5.setRightDecoration(TCI3);
    panel7.setRightDecoration(TCI5);
    panel6.setRightDecoration(TCI4);
    panel8.setRightDecoration(TCI6);
    
    CLNOM.activerModeFantome("nom ou raison sociale");
    CLCPL.activerModeFantome("complément de nom");
    CLRUE.activerModeFantome("rue");
    CLLOC.activerModeFantome("localité");
    CLCDP.activerModeFantome("00000");
    CLVILR.activerModeFantome("ville");
    CLPAY.activerModeFantome("pays");
    
    // Ajout
    initDiverses();
    SCGAM.setValeursSelection("1", " ");
    SCWEB.setValeursSelection("1", " ");
    SCGMM.setValeursSelection("1", " ");
    SCGPM.setValeursSelection("1", " ");
    SCPAM.setValeursSelection("1", " ");
    SCGLF.setValeursSelection("1", " ");
    SCGVM.setValeursSelection("1", " ");
    SCGIM.setValeursSelection("1", " ");
    SCGEM.setValeursSelection("1", " ");
    SCCGM.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_83.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CCLIBR@")).trim());
    OBJ_110.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RPLIBR@")).trim());
    OBJ_119.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RPLIB2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    OBJ_193.setVisible(lexique.isTrue("(57) AND (N40) AND (53)"));
    
    // SCCGM.setSelected(lexique.HostFieldGetData("SCCGM").equalsIgnoreCase("1"));
    // SCGLF.setSelected(lexique.HostFieldGetData("SCGLF").equalsIgnoreCase("1"));
    // SCGEM.setSelected(lexique.HostFieldGetData("SCGEM").equalsIgnoreCase("1"));
    // SCGIM.setSelected(lexique.HostFieldGetData("SCGIM").equalsIgnoreCase("1"));
    // SCPAM.setSelected(lexique.HostFieldGetData("SCPAM").equalsIgnoreCase("1"));
    // SCGVM.setSelected(lexique.HostFieldGetData("SCGVM").equalsIgnoreCase("1"));
    // SCGAM.setSelected(lexique.HostFieldGetData("SCGAM").equalsIgnoreCase("1"));
    // SCGPM.setSelected(lexique.HostFieldGetData("SCGPM").equalsIgnoreCase("1"));
    // SCGMM.setSelected(lexique.HostFieldGetData("SCGMM").equalsIgnoreCase("1"));
    // SCWEB.setSelected(lexique.HostFieldGetData("SCWEB").equalsIgnoreCase("1"));
    
    OBJ_115.setVisible(CLOBS.isVisible());
    
    // Groupe représentant
    xTitledSeparator3.setVisible(CLREP.isVisible());
    OBJ_110.setVisible(CLREP.isVisible());
    OBJ_111.setVisible(CLREP.isVisible());
    OBJ_119.setVisible(CLREP2.isVisible());
    OBJ_120.setVisible(CLREP2.isVisible());
    
    // Gestion des foncitions disponibles en création
    if (lexique.isTrue("53")) {
      
      // panels détails
      if (lexique.HostFieldGetData("TCI1").trim().equals("")) {
        TCI1.setEnabled(false);
      }
      else {
        TCI1.setEnabled(true);
      }
      if (lexique.HostFieldGetData("TCI2").trim().equals("")) {
        TCI2.setEnabled(false);
      }
      else {
        TCI2.setEnabled(true);
      }
      if (lexique.HostFieldGetData("TCI3").trim().equals("")) {
        TCI3.setEnabled(false);
      }
      else {
        TCI3.setEnabled(true);
      }
      if (lexique.HostFieldGetData("TCI4").trim().equals("")) {
        TCI4.setEnabled(false);
      }
      else {
        TCI4.setEnabled(true);
      }
      if (lexique.HostFieldGetData("TCI5").trim().equals("")) {
        TCI5.setEnabled(false);
      }
      else {
        TCI5.setEnabled(true);
      }
      if (lexique.HostFieldGetData("TCI6").trim().equals("")) {
        TCI6.setEnabled(false);
      }
      else {
        TCI6.setEnabled(true);
      }
    }
    else {
      TCI1.setEnabled(true);
      TCI2.setEnabled(true);
      TCI3.setEnabled(true);
      TCI4.setEnabled(true);
      TCI5.setEnabled(true);
      TCI6.setEnabled(true);
      
    }
    
    // LayeredPane
    // ++++++++++++++++++++++++++++++++++++++++++
    
    if (lexique.HostFieldGetData("CLTNS").trim().equalsIgnoreCase("9")) {
      lab_desac.setIcon(lexique.chargerImage("images/desact.png", true));
      p_desac.setVisible(true);
      layeredPane1.setComponentZOrder(p_desac, 0);
      layeredPane1.setComponentZOrder(panel10, 2);
    }
    else if (lexique.HostFieldGetData("CLTNS").trim().equalsIgnoreCase("2")) {
      lab_desac.setIcon(lexique.chargerImage("images/interdit.png", true));
      p_desac.setVisible(true);
      layeredPane1.setComponentZOrder(p_desac, 0);
      layeredPane1.setComponentZOrder(panel10, 1);
    }
    else {
      p_desac.setVisible(false);
    }
    
    if (lexique.isTrue("60")) {
      lab_desac.setIcon(lexique.chargerImage("images/blacklist.png", true));
      p_desac.setVisible(true);
      layeredPane1.setComponentZOrder(p_desac, 0);
      layeredPane1.setComponentZOrder(panel10, 2);
    }
    else {
      p_desac.setVisible(false);
    }
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (SCCGM.isSelected())
    // lexique.HostFieldPutData("SCCGM", 0, "1");
    // else
    // lexique.HostFieldPutData("SCCGM", 0, " ");
    
    // if (SCGLF.isSelected())
    // lexique.HostFieldPutData("SCGLF", 0, "1");
    // else
    // lexique.HostFieldPutData("SCGLF", 0, " ");
    
    // if (SCGEM.isSelected())
    // lexique.HostFieldPutData("SCGEM", 0, "1");
    // else
    // lexique.HostFieldPutData("SCGEM", 0, " ");
    
    // if (SCGIM.isSelected())
    // lexique.HostFieldPutData("SCGIM", 0, "1");
    // else
    // lexique.HostFieldPutData("SCGIM", 0, " ");
    
    // if (SCPAM.isSelected())
    // lexique.HostFieldPutData("SCPAM", 0, "1");
    // else
    // lexique.HostFieldPutData("SCPAM", 0, " ");
    
    // if (SCGVM.isSelected())
    // lexique.HostFieldPutData("SCGVM", 0, "1");
    // else
    // lexique.HostFieldPutData("SCGVM", 0, " ");
    
    // if (SCGAM.isSelected())
    // lexique.HostFieldPutData("SCGAM", 0, "1");
    // else
    // lexique.HostFieldPutData("SCGAM", 0, " ");
    
    // if (SCGPM.isSelected())
    // lexique.HostFieldPutData("SCGPM", 0, "1");
    // else
    // lexique.HostFieldPutData("SCGPM", 0, " ");
    
    // if (SCGMM.isSelected())
    // lexique.HostFieldPutData("SCGMM", 0, "1");
    // else
    // lexique.HostFieldPutData("SCGMM", 0, " ");
    
    // if (SCWEB.isSelected())
    // lexique.HostFieldPutData("SCWEB", 0, "1");
    // else
    // lexique.HostFieldPutData("SCWEB", 0, " ");
    
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD2.getInvoker().getName());
  }
  
  private void TCI1ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI1").trim().equals("")) {
      lexique.HostFieldPutData("TCI1", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI1"));
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void TCI2ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI2").trim().equals("")) {
      lexique.HostFieldPutData("TCI2", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI2"));
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void TCI3ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI3").trim().equals("")) {
      lexique.HostFieldPutData("TCI3", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI3"));
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void TCI5ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI5").trim().equals("")) {
      lexique.HostFieldPutData("TCI5", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI5"));
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void TCI4ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI4").trim().equals("")) {
      lexique.HostFieldPutData("TCI4", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI4"));
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void TCI6ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI6").trim().equals("")) {
      lexique.HostFieldPutData("TCI6", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI6"));
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  /*
    public static void sendTextToClipBoard(String text)
    {
      if (text == null) return;
      StringSelection data = new StringSelection(text);
      Toolkit.getDefaultToolkit().getSystemClipboard().setContents(data, data);
    }
  */
  
  // Gestion icones sur panels suivant focus
  
  private void OBJ_111MouseClicked() {
    lexique.HostCursorPut(10, 50);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_120MouseClicked() {
    lexique.HostCursorPut(11, 50);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  /* APPELS TELEPHONIQUES : on fait la version SGM plus tard (VEXP11FM)
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI12").trim().equals(""))
      lexique.HostFieldPutData("TCI12", 0, "T");
    else
    {
      lexique.HostFieldPutData("V06FO", 0, "T");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  */
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI12").trim().equals("")) {
      lexique.HostFieldPutData("TCI12", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, "X");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt13ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06FO", 0, "²");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void inviteActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void aideActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_117ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  // EVENEMENTIEL
  // +++++++++++++++++++++++++++++++++++++++++++
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_84_OBJ_84 = new JLabel();
    INDETB = new XRiTextField();
    OBJECT_4 = new JLabel();
    INDCLI = new XRiTextField();
    INDLIV = new XRiTextField();
    p_tete_droite = new JPanel();
    panel3 = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu13 = new RiSousMenu();
    riSousMenu_bt13 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    layeredPane1 = new JLayeredPane();
    panel10 = new JPanel();
    OBJ_65 = new JPanel();
    CLNOM = new XRiTextField();
    CLCPL = new XRiTextField();
    CLRUE = new XRiTextField();
    CLLOC = new XRiTextField();
    CLPAY = new XRiTextField();
    CLVILR = new XRiTextField();
    CLOBS = new XRiTextField();
    OBJ_115 = new JLabel();
    CLCDP = new XRiTextField();
    CLCOP = new XRiTextField();
    OBJ_121 = new JPanel();
    OBJ_117 = new SNBoutonDetail();
    panel1 = new JPanel();
    panel9 = new JPanel();
    xTitledSeparator1 = new JXTitledSeparator();
    CLCAT = new XRiTextField();
    OBJ_83 = new RiZoneSortie();
    panel11 = new JPanel();
    xTitledSeparator3 = new JXTitledSeparator();
    CLREP = new XRiTextField();
    OBJ_111 = new SNBoutonDetail();
    OBJ_110 = new RiZoneSortie();
    CLREP2 = new XRiTextField();
    OBJ_120 = new SNBoutonDetail();
    OBJ_119 = new RiZoneSortie();
    panel12 = new JPanel();
    xTitledSeparator2 = new JXTitledSeparator();
    CLCLK = new XRiTextField();
    panel13 = new JPanel();
    SCTSM = new XRiTextField();
    SCEFF = new XRiTextField();
    CLCLA = new XRiTextField();
    label7 = new JLabel();
    SCCAF = new XRiTextField();
    OBJ_72 = new JLabel();
    SCAPE = new XRiTextField();
    OBJ_73 = new JLabel();
    WVERS = new XRiTextField();
    OBJ_74 = new JLabel();
    OBJ_75 = new JLabel();
    OBJ_76 = new JLabel();
    label8 = new JLabel();
    SCBASP = new XRiTextField();
    panel14 = new JPanel();
    xTitledSeparator4 = new JXTitledSeparator();
    CLTEL = new XRiTextField();
    CLFAX = new XRiTextField();
    OBJ_128 = new SNBoutonDetail();
    panel2 = new JXTitledPanel();
    CLPAC = new XRiTextField();
    PE3PLU = new XRiTextField();
    panel5 = new JXTitledPanel();
    OBJ_164 = new JLabel();
    SCTMA = new XRiTextField();
    OBJ_170 = new JLabel();
    SCVOS = new XRiTextField();
    OBJ_172 = new JLabel();
    SCSEN = new XRiTextField();
    OBJ_176 = new JLabel();
    SCNFA = new XRiTextField();
    panel4 = new JXTitledPanel();
    OBJ_138 = new JLabel();
    SCCSL = new XRiTextField();
    OBJ_141 = new JLabel();
    SCDDEX = new XRiCalendrier();
    OBJ_149 = new JLabel();
    SCDRSX = new XRiCalendrier();
    OBJ_152 = new JLabel();
    SCNFS = new XRiTextField();
    panel7 = new JXTitledPanel();
    SCNBU = new XRiTextField();
    OBJ_192 = new JLabel();
    SCVGM = new XRiTextField();
    OBJ_193 = new JLabel();
    SCBUL = new XRiTextField();
    label3 = new JLabel();
    panel8 = new JXTitledPanel();
    OBJ_209 = new JLabel();
    SCSYST = new XRiTextField();
    OBJ_203 = new JLabel();
    SCRSC = new XRiTextField();
    OBJ_205 = new JLabel();
    SCTCP = new XRiTextField();
    panel6 = new JXTitledPanel();
    SCCGM = new XRiCheckBox();
    SCGEM = new XRiCheckBox();
    SCGIM = new XRiCheckBox();
    SCGVM = new XRiCheckBox();
    SCGLF = new XRiCheckBox();
    SCPAM = new XRiCheckBox();
    SCGPM = new XRiCheckBox();
    SCGMM = new XRiCheckBox();
    SCWEB = new XRiCheckBox();
    SCGAM = new XRiCheckBox();
    label1 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();
    label11 = new JLabel();
    label12 = new JLabel();
    label13 = new JLabel();
    label14 = new JLabel();
    p_desac = new JPanel();
    lab_desac = new JLabel();
    BTD2 = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    TCI1 = new SNBoutonDetail();
    TCI2 = new SNBoutonDetail();
    TCI3 = new SNBoutonDetail();
    TCI5 = new SNBoutonDetail();
    TCI4 = new SNBoutonDetail();
    TCI6 = new SNBoutonDetail();
    BTD = new JPopupMenu();
    invite = new JMenuItem();
    aide = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(1170, 690));
    setPreferredSize(new Dimension(1170, 690));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Clients S\u00e9rie N");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 30));
          p_tete_gauche.setMinimumSize(new Dimension(700, 30));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_84_OBJ_84 ----
          OBJ_84_OBJ_84.setText("Etablissement");
          OBJ_84_OBJ_84.setName("OBJ_84_OBJ_84");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD2);
          INDETB.setName("INDETB");

          //---- OBJECT_4 ----
          OBJECT_4.setText("Client");
          OBJECT_4.setName("OBJECT_4");

          //---- INDCLI ----
          INDCLI.setName("INDCLI");

          //---- INDLIV ----
          INDLIV.setName("INDLIV");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_84_OBJ_84, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(60, 60, 60)
                .addComponent(OBJECT_4)
                .addGap(13, 13, 13)
                .addComponent(INDCLI, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDLIV, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_84_OBJ_84, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJECT_4))
              .addComponent(INDCLI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDLIV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //======== panel3 ========
          {
            panel3.setName("panel3");
            panel3.setLayout(null);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }
          p_tete_droite.add(panel3);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu13 ========
            {
              riSousMenu13.setName("riSousMenu13");

              //---- riSousMenu_bt13 ----
              riSousMenu_bt13.setText("Envoi d'un e-mail");
              riSousMenu_bt13.setToolTipText("Derniers \u00e9v\u00e8nements");
              riSousMenu_bt13.setEnabled(false);
              riSousMenu_bt13.setName("riSousMenu_bt13");
              riSousMenu_bt13.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt13ActionPerformed(e);
                }
              });
              riSousMenu13.add(riSousMenu_bt13);
            }
            menus_haut.add(riSousMenu13);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Etiquettes");
              riSousMenu_bt7.setEnabled(false);
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Licences 2 utilisateurs");
              riSousMenu_bt8.setToolTipText("Zones personnalis\u00e9es");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Licences 3 utilisateurs");
              riSousMenu_bt9.setToolTipText("Affichage personnalis\u00e9");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Configuration modem");
              riSousMenu_bt10.setToolTipText("Derniers \u00e9v\u00e8nements");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("M\u00e9mo");
              riSousMenu_bt14.setToolTipText("M\u00e9mo");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);

            //======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");

              //---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Bloc-notes");
              riSousMenu_bt15.setToolTipText("Documents li\u00e9s");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed(e);
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1000, 630));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setForeground(Color.black);
          p_contenu.setMinimumSize(new Dimension(1000, 630));
          p_contenu.setName("p_contenu");

          //======== layeredPane1 ========
          {
            layeredPane1.setPreferredSize(new Dimension(1000, 650));
            layeredPane1.setMinimumSize(new Dimension(950, 650));
            layeredPane1.setName("layeredPane1");

            //======== panel10 ========
            {
              panel10.setOpaque(false);
              panel10.setName("panel10");
              panel10.setLayout(null);

              //======== OBJ_65 ========
              {
                OBJ_65.setPreferredSize(new Dimension(310, 180));
                OBJ_65.setOpaque(false);
                OBJ_65.setBorder(new TitledBorder(""));
                OBJ_65.setName("OBJ_65");
                OBJ_65.setLayout(null);

                //---- CLNOM ----
                CLNOM.setBackground(Color.white);
                CLNOM.setFont(CLNOM.getFont().deriveFont(CLNOM.getFont().getStyle() | Font.BOLD, CLNOM.getFont().getSize() + 1f));
                CLNOM.setName("CLNOM");
                OBJ_65.add(CLNOM);
                CLNOM.setBounds(20, 15, 310, CLNOM.getPreferredSize().height);

                //---- CLCPL ----
                CLCPL.setBackground(Color.white);
                CLCPL.setName("CLCPL");
                OBJ_65.add(CLCPL);
                CLCPL.setBounds(20, 45, 310, CLCPL.getPreferredSize().height);

                //---- CLRUE ----
                CLRUE.setBackground(Color.white);
                CLRUE.setName("CLRUE");
                OBJ_65.add(CLRUE);
                CLRUE.setBounds(20, 73, 310, CLRUE.getPreferredSize().height);

                //---- CLLOC ----
                CLLOC.setBackground(Color.white);
                CLLOC.setName("CLLOC");
                OBJ_65.add(CLLOC);
                CLLOC.setBounds(20, 101, 310, CLLOC.getPreferredSize().height);

                //---- CLPAY ----
                CLPAY.setBackground(Color.white);
                CLPAY.setName("CLPAY");
                OBJ_65.add(CLPAY);
                CLPAY.setBounds(20, 159, 270, CLPAY.getPreferredSize().height);

                //---- CLVILR ----
                CLVILR.setBackground(Color.white);
                CLVILR.setFont(CLVILR.getFont().deriveFont(CLVILR.getFont().getStyle() | Font.BOLD, CLVILR.getFont().getSize() + 1f));
                CLVILR.setComponentPopupMenu(BTD);
                CLVILR.setName("CLVILR");
                OBJ_65.add(CLVILR);
                CLVILR.setBounds(80, 129, 250, CLVILR.getPreferredSize().height);

                //---- CLOBS ----
                CLOBS.setBackground(Color.white);
                CLOBS.setName("CLOBS");
                OBJ_65.add(CLOBS);
                CLOBS.setBounds(100, 195, 190, CLOBS.getPreferredSize().height);

                //---- OBJ_115 ----
                OBJ_115.setText("Observation");
                OBJ_115.setBackground(Color.white);
                OBJ_115.setName("OBJ_115");
                OBJ_65.add(OBJ_115);
                OBJ_115.setBounds(24, 195, 76, 28);

                //---- CLCDP ----
                CLCDP.setBackground(Color.white);
                CLCDP.setFont(CLCDP.getFont().deriveFont(CLCDP.getFont().getStyle() | Font.BOLD, CLCDP.getFont().getSize() + 1f));
                CLCDP.setComponentPopupMenu(BTD);
                CLCDP.setName("CLCDP");
                OBJ_65.add(CLCDP);
                CLCDP.setBounds(19, 129, 52, CLCDP.getPreferredSize().height);

                //---- CLCOP ----
                CLCOP.setBackground(Color.white);
                CLCOP.setComponentPopupMenu(BTD);
                CLCOP.setName("CLCOP");
                OBJ_65.add(CLCOP);
                CLCOP.setBounds(289, 159, 40, CLCOP.getPreferredSize().height);

                //======== OBJ_121 ========
                {
                  OBJ_121.setName("OBJ_121");
                  OBJ_121.setLayout(null);
                }
                OBJ_65.add(OBJ_121);
                OBJ_121.setBounds(264, 189, 1, 1);

                //---- OBJ_117 ----
                OBJ_117.setText("");
                OBJ_117.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                OBJ_117.setBorder(null);
                OBJ_117.setToolTipText("informations disponibles dans le m\u00e9mo de cette fiche");
                OBJ_117.setName("OBJ_117");
                OBJ_117.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    OBJ_117ActionPerformed(e);
                  }
                });
                OBJ_65.add(OBJ_117);
                OBJ_117.setBounds(309, 195, 20, 28);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < OBJ_65.getComponentCount(); i++) {
                    Rectangle bounds = OBJ_65.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = OBJ_65.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  OBJ_65.setMinimumSize(preferredSize);
                  OBJ_65.setPreferredSize(preferredSize);
                }
              }
              panel10.add(OBJ_65);
              OBJ_65.setBounds(12, 1, 350, 235);

              //======== panel1 ========
              {
                panel1.setOpaque(false);
                panel1.setName("panel1");
                panel1.setLayout(null);

                //======== panel9 ========
                {
                  panel9.setOpaque(false);
                  panel9.setName("panel9");
                  panel9.setLayout(null);

                  //---- xTitledSeparator1 ----
                  xTitledSeparator1.setTitle("Cat\u00e9gorie");
                  xTitledSeparator1.setName("xTitledSeparator1");
                  panel9.add(xTitledSeparator1);
                  xTitledSeparator1.setBounds(0, 0, 275, xTitledSeparator1.getPreferredSize().height);

                  //---- CLCAT ----
                  CLCAT.setComponentPopupMenu(BTD);
                  CLCAT.setName("CLCAT");
                  panel9.add(CLCAT);
                  CLCAT.setBounds(15, 20, 40, CLCAT.getPreferredSize().height);

                  //---- OBJ_83 ----
                  OBJ_83.setText("@CCLIBR@");
                  OBJ_83.setName("OBJ_83");
                  panel9.add(OBJ_83);
                  OBJ_83.setBounds(92, 22, 180, OBJ_83.getPreferredSize().height);

                  {
                    // compute preferred size
                    Dimension preferredSize = new Dimension();
                    for(int i = 0; i < panel9.getComponentCount(); i++) {
                      Rectangle bounds = panel9.getComponent(i).getBounds();
                      preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                      preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                    }
                    Insets insets = panel9.getInsets();
                    preferredSize.width += insets.right;
                    preferredSize.height += insets.bottom;
                    panel9.setMinimumSize(preferredSize);
                    panel9.setPreferredSize(preferredSize);
                  }
                }
                panel1.add(panel9);
                panel9.setBounds(0, 95, 290, 55);

                //======== panel11 ========
                {
                  panel11.setOpaque(false);
                  panel11.setName("panel11");
                  panel11.setLayout(null);

                  //---- xTitledSeparator3 ----
                  xTitledSeparator3.setTitle("Repr\u00e9sentant ou agent");
                  xTitledSeparator3.setName("xTitledSeparator3");
                  panel11.add(xTitledSeparator3);
                  xTitledSeparator3.setBounds(0, 0, 275, xTitledSeparator3.getPreferredSize().height);

                  //---- CLREP ----
                  CLREP.setComponentPopupMenu(BTD);
                  CLREP.setName("CLREP");
                  panel11.add(CLREP);
                  CLREP.setBounds(15, 20, 34, CLREP.getPreferredSize().height);

                  //---- OBJ_111 ----
                  OBJ_111.setText("");
                  OBJ_111.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                  OBJ_111.setBorder(null);
                  OBJ_111.setName("OBJ_111");
                  OBJ_111.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                      OBJ_111MouseClicked();
                    }
                  });
                  panel11.add(OBJ_111);
                  OBJ_111.setBounds(65, 20, 20, 28);

                  //---- OBJ_110 ----
                  OBJ_110.setText("@RPLIBR@");
                  OBJ_110.setName("OBJ_110");
                  panel11.add(OBJ_110);
                  OBJ_110.setBounds(90, 22, 180, OBJ_110.getPreferredSize().height);

                  //---- CLREP2 ----
                  CLREP2.setComponentPopupMenu(BTD);
                  CLREP2.setName("CLREP2");
                  panel11.add(CLREP2);
                  CLREP2.setBounds(15, 50, 34, CLREP2.getPreferredSize().height);

                  //---- OBJ_120 ----
                  OBJ_120.setText("");
                  OBJ_120.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                  OBJ_120.setBorder(null);
                  OBJ_120.setName("OBJ_120");
                  OBJ_120.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                      OBJ_120MouseClicked();
                    }
                  });
                  panel11.add(OBJ_120);
                  OBJ_120.setBounds(65, 50, 20, 28);

                  //---- OBJ_119 ----
                  OBJ_119.setText("@RPLIB2@");
                  OBJ_119.setName("OBJ_119");
                  panel11.add(OBJ_119);
                  OBJ_119.setBounds(90, 52, 180, OBJ_119.getPreferredSize().height);

                  {
                    // compute preferred size
                    Dimension preferredSize = new Dimension();
                    for(int i = 0; i < panel11.getComponentCount(); i++) {
                      Rectangle bounds = panel11.getComponent(i).getBounds();
                      preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                      preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                    }
                    Insets insets = panel11.getInsets();
                    preferredSize.width += insets.right;
                    preferredSize.height += insets.bottom;
                    panel11.setMinimumSize(preferredSize);
                    panel11.setPreferredSize(preferredSize);
                  }
                }
                panel1.add(panel11);
                panel11.setBounds(0, 155, 290, 90);

                //======== panel12 ========
                {
                  panel12.setOpaque(false);
                  panel12.setName("panel12");
                  panel12.setLayout(null);

                  //---- xTitledSeparator2 ----
                  xTitledSeparator2.setTitle("Classement");
                  xTitledSeparator2.setName("xTitledSeparator2");
                  panel12.add(xTitledSeparator2);
                  xTitledSeparator2.setBounds(0, 0, 285, xTitledSeparator2.getPreferredSize().height);

                  //---- CLCLK ----
                  CLCLK.setName("CLCLK");
                  panel12.add(CLCLK);
                  CLCLK.setBounds(15, 20, 210, CLCLK.getPreferredSize().height);

                  {
                    // compute preferred size
                    Dimension preferredSize = new Dimension();
                    for(int i = 0; i < panel12.getComponentCount(); i++) {
                      Rectangle bounds = panel12.getComponent(i).getBounds();
                      preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                      preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                    }
                    Insets insets = panel12.getInsets();
                    preferredSize.width += insets.right;
                    preferredSize.height += insets.bottom;
                    panel12.setMinimumSize(preferredSize);
                    panel12.setPreferredSize(preferredSize);
                  }
                }
                panel1.add(panel12);
                panel12.setBounds(290, 95, 295, 55);

                //======== panel13 ========
                {
                  panel13.setOpaque(false);
                  panel13.setName("panel13");
                  panel13.setLayout(null);

                  //---- SCTSM ----
                  SCTSM.setComponentPopupMenu(BTD);
                  SCTSM.setName("SCTSM");
                  panel13.add(SCTSM);
                  SCTSM.setBounds(90, 0, 40, SCTSM.getPreferredSize().height);

                  //---- SCEFF ----
                  SCEFF.setComponentPopupMenu(BTD);
                  SCEFF.setName("SCEFF");
                  panel13.add(SCEFF);
                  SCEFF.setBounds(295, 60, 50, SCEFF.getPreferredSize().height);

                  //---- CLCLA ----
                  CLCLA.setName("CLCLA");
                  panel13.add(CLCLA);
                  CLCLA.setBounds(90, 30, 475, CLCLA.getPreferredSize().height);

                  //---- label7 ----
                  label7.setText("Chiffre d'affaires");
                  label7.setName("label7");
                  panel13.add(label7);
                  label7.setBounds(385, 66, 120, label7.getPreferredSize().height);

                  //---- SCCAF ----
                  SCCAF.setToolTipText("Exprim\u00e9 en milions d'euros");
                  SCCAF.setName("SCCAF");
                  panel13.add(SCCAF);
                  SCCAF.setBounds(510, 60, 55, SCCAF.getPreferredSize().height);

                  //---- OBJ_72 ----
                  OBJ_72.setText("num\u00e9ro APE");
                  OBJ_72.setName("OBJ_72");
                  panel13.add(OBJ_72);
                  OBJ_72.setBounds(15, 63, 75, 22);

                  //---- SCAPE ----
                  SCAPE.setComponentPopupMenu(BTD);
                  SCAPE.setName("SCAPE");
                  panel13.add(SCAPE);
                  SCAPE.setBounds(90, 60, 80, SCAPE.getPreferredSize().height);

                  //---- OBJ_73 ----
                  OBJ_73.setText("Effectif");
                  OBJ_73.setName("OBJ_73");
                  panel13.add(OBJ_73);
                  OBJ_73.setBounds(220, 63, 70, 22);

                  //---- WVERS ----
                  WVERS.setName("WVERS");
                  panel13.add(WVERS);
                  WVERS.setBounds(295, 0, 20, WVERS.getPreferredSize().height);

                  //---- OBJ_74 ----
                  OBJ_74.setText("S\u00e9rie N");
                  OBJ_74.setName("OBJ_74");
                  panel13.add(OBJ_74);
                  OBJ_74.setBounds(15, 3, 55, 22);

                  //---- OBJ_75 ----
                  OBJ_75.setText("Version");
                  OBJ_75.setName("OBJ_75");
                  panel13.add(OBJ_75);
                  OBJ_75.setBounds(220, 3, 75, 22);

                  //---- OBJ_76 ----
                  OBJ_76.setText("Activit\u00e9");
                  OBJ_76.setName("OBJ_76");
                  panel13.add(OBJ_76);
                  OBJ_76.setBounds(15, 33, 75, 22);

                  //---- label8 ----
                  label8.setText("Biblioth\u00e8que ASP");
                  label8.setName("label8");
                  panel13.add(label8);
                  label8.setBounds(385, 6, 120, label8.getPreferredSize().height);

                  //---- SCBASP ----
                  SCBASP.setToolTipText("Exprim\u00e9 en milions d'euros");
                  SCBASP.setName("SCBASP");
                  panel13.add(SCBASP);
                  SCBASP.setBounds(510, 0, 55, SCBASP.getPreferredSize().height);

                  {
                    // compute preferred size
                    Dimension preferredSize = new Dimension();
                    for(int i = 0; i < panel13.getComponentCount(); i++) {
                      Rectangle bounds = panel13.getComponent(i).getBounds();
                      preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                      preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                    }
                    Insets insets = panel13.getInsets();
                    preferredSize.width += insets.right;
                    preferredSize.height += insets.bottom;
                    panel13.setMinimumSize(preferredSize);
                    panel13.setPreferredSize(preferredSize);
                  }
                }
                panel1.add(panel13);
                panel13.setBounds(0, 0, 570, 90);

                //======== panel14 ========
                {
                  panel14.setOpaque(false);
                  panel14.setName("panel14");
                  panel14.setLayout(null);

                  //---- xTitledSeparator4 ----
                  xTitledSeparator4.setTitle("T\u00e9l\u00e9phone et fax");
                  xTitledSeparator4.setName("xTitledSeparator4");
                  panel14.add(xTitledSeparator4);
                  xTitledSeparator4.setBounds(0, 0, 285, xTitledSeparator4.getPreferredSize().height);

                  //---- CLTEL ----
                  CLTEL.setName("CLTEL");
                  panel14.add(CLTEL);
                  CLTEL.setBounds(15, 20, 210, CLTEL.getPreferredSize().height);

                  //---- CLFAX ----
                  CLFAX.setName("CLFAX");
                  panel14.add(CLFAX);
                  CLFAX.setBounds(15, 50, 210, CLFAX.getPreferredSize().height);

                  //---- OBJ_128 ----
                  OBJ_128.setText("");
                  OBJ_128.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                  OBJ_128.setName("OBJ_128");
                  panel14.add(OBJ_128);
                  OBJ_128.setBounds(230, 20, 27, 28);

                  {
                    // compute preferred size
                    Dimension preferredSize = new Dimension();
                    for(int i = 0; i < panel14.getComponentCount(); i++) {
                      Rectangle bounds = panel14.getComponent(i).getBounds();
                      preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                      preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                    }
                    Insets insets = panel14.getInsets();
                    preferredSize.width += insets.right;
                    preferredSize.height += insets.bottom;
                    panel14.setMinimumSize(preferredSize);
                    panel14.setPreferredSize(preferredSize);
                  }
                }
                panel1.add(panel14);
                panel14.setBounds(290, 155, 295, 85);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < panel1.getComponentCount(); i++) {
                    Rectangle bounds = panel1.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = panel1.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  panel1.setMinimumSize(preferredSize);
                  panel1.setPreferredSize(preferredSize);
                }
              }
              panel10.add(panel1);
              panel1.setBounds(380, 0, 578, 245);

              //======== panel2 ========
              {
                panel2.setBorder(new DropShadowBorder());
                panel2.setTitle("Contact");
                panel2.setName("panel2");
                Container panel2ContentContainer = panel2.getContentContainer();
                panel2ContentContainer.setLayout(null);

                //---- CLPAC ----
                CLPAC.setFont(CLPAC.getFont().deriveFont(CLPAC.getFont().getStyle() | Font.BOLD));
                CLPAC.setName("CLPAC");
                panel2ContentContainer.add(CLPAC);
                CLPAC.setBounds(20, 20, 280, CLPAC.getPreferredSize().height);

                //---- PE3PLU ----
                PE3PLU.setFont(PE3PLU.getFont().deriveFont(PE3PLU.getFont().getStyle() | Font.BOLD));
                PE3PLU.setName("PE3PLU");
                panel2ContentContainer.add(PE3PLU);
                PE3PLU.setBounds(305, 20, 20, PE3PLU.getPreferredSize().height);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < panel2ContentContainer.getComponentCount(); i++) {
                    Rectangle bounds = panel2ContentContainer.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = panel2ContentContainer.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  panel2ContentContainer.setMinimumSize(preferredSize);
                  panel2ContentContainer.setPreferredSize(preferredSize);
                }
              }
              panel10.add(panel2);
              panel2.setBounds(12, 243, 343, 92);

              //======== panel5 ========
              {
                panel5.setTitle("Mat\u00e9riel");
                panel5.setBorder(new DropShadowBorder());
                panel5.setName("panel5");
                Container panel5ContentContainer = panel5.getContentContainer();
                panel5ContentContainer.setLayout(null);

                //---- OBJ_164 ----
                OBJ_164.setText("Type");
                OBJ_164.setName("OBJ_164");
                panel5ContentContainer.add(OBJ_164);
                OBJ_164.setBounds(15, 54, 56, 20);

                //---- SCTMA ----
                SCTMA.setComponentPopupMenu(null);
                SCTMA.setName("SCTMA");
                panel5ContentContainer.add(SCTMA);
                SCTMA.setBounds(95, 50, 170, SCTMA.getPreferredSize().height);

                //---- OBJ_170 ----
                OBJ_170.setText("Version d'OS");
                OBJ_170.setName("OBJ_170");
                panel5ContentContainer.add(OBJ_170);
                OBJ_170.setBounds(15, 84, 110, 20);

                //---- SCVOS ----
                SCVOS.setComponentPopupMenu(BTD);
                SCVOS.setName("SCVOS");
                panel5ContentContainer.add(SCVOS);
                SCVOS.setBounds(225, 80, 40, SCVOS.getPreferredSize().height);

                //---- OBJ_172 ----
                OBJ_172.setText("Num\u00e9ro de s\u00e9rie");
                OBJ_172.setName("OBJ_172");
                panel5ContentContainer.add(OBJ_172);
                OBJ_172.setBounds(15, 24, 110, 20);

                //---- SCSEN ----
                SCSEN.setComponentPopupMenu(BTD);
                SCSEN.setName("SCSEN");
                panel5ContentContainer.add(SCSEN);
                SCSEN.setBounds(175, 20, 90, SCSEN.getPreferredSize().height);

                //---- OBJ_176 ----
                OBJ_176.setText("Num\u00e9ro de facture du serveur");
                OBJ_176.setName("OBJ_176");
                panel5ContentContainer.add(OBJ_176);
                OBJ_176.setBounds(15, 114, 175, 20);

                //---- SCNFA ----
                SCNFA.setName("SCNFA");
                panel5ContentContainer.add(SCNFA);
                SCNFA.setBounds(195, 110, 70, SCNFA.getPreferredSize().height);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < panel5ContentContainer.getComponentCount(); i++) {
                    Rectangle bounds = panel5ContentContainer.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = panel5ContentContainer.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  panel5ContentContainer.setMinimumSize(preferredSize);
                  panel5ContentContainer.setPreferredSize(preferredSize);
                }
              }
              panel10.add(panel5);
              panel5.setBounds(670, 243, 295, 190);

              //======== panel4 ========
              {
                panel4.setTitle("Service apr\u00e8s vente");
                panel4.setBorder(new DropShadowBorder());
                panel4.setName("panel4");
                Container panel4ContentContainer = panel4.getContentContainer();
                panel4ContentContainer.setLayout(null);

                //---- OBJ_138 ----
                OBJ_138.setText("num\u00e9ro client");
                OBJ_138.setName("OBJ_138");
                panel4ContentContainer.add(OBJ_138);
                OBJ_138.setBounds(10, 24, 140, 20);

                //---- SCCSL ----
                SCCSL.setComponentPopupMenu(BTD);
                SCCSL.setName("SCCSL");
                panel4ContentContainer.add(SCCSL);
                SCCSL.setBounds(205, 20, 60, SCCSL.getPreferredSize().height);

                //---- OBJ_141 ----
                OBJ_141.setText("D\u00e9marrage");
                OBJ_141.setName("OBJ_141");
                panel4ContentContainer.add(OBJ_141);
                OBJ_141.setBounds(10, 54, 140, 20);

                //---- SCDDEX ----
                SCDDEX.setComponentPopupMenu(BTD);
                SCDDEX.setName("SCDDEX");
                panel4ContentContainer.add(SCDDEX);
                SCDDEX.setBounds(155, 50, 110, SCDDEX.getPreferredSize().height);

                //---- OBJ_149 ----
                OBJ_149.setText("Renouvellement");
                OBJ_149.setName("OBJ_149");
                panel4ContentContainer.add(OBJ_149);
                OBJ_149.setBounds(10, 84, 140, 20);

                //---- SCDRSX ----
                SCDRSX.setComponentPopupMenu(BTD);
                SCDRSX.setName("SCDRSX");
                panel4ContentContainer.add(SCDRSX);
                SCDRSX.setBounds(155, 80, 110, SCDRSX.getPreferredSize().height);

                //---- OBJ_152 ----
                OBJ_152.setText("Derni\u00e8re facture");
                OBJ_152.setName("OBJ_152");
                panel4ContentContainer.add(OBJ_152);
                OBJ_152.setBounds(10, 114, 140, 20);

                //---- SCNFS ----
                SCNFS.setComponentPopupMenu(BTD);
                SCNFS.setName("SCNFS");
                panel4ContentContainer.add(SCNFS);
                SCNFS.setBounds(205, 110, 60, SCNFS.getPreferredSize().height);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < panel4ContentContainer.getComponentCount(); i++) {
                    Rectangle bounds = panel4ContentContainer.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = panel4ContentContainer.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  panel4ContentContainer.setMinimumSize(preferredSize);
                  panel4ContentContainer.setPreferredSize(preferredSize);
                }
              }
              panel10.add(panel4);
              panel4.setBounds(367, 243, 290, 190);

              //======== panel7 ========
              {
                panel7.setTitle("Divers");
                panel7.setBorder(new DropShadowBorder());
                panel7.setName("panel7");
                Container panel7ContentContainer = panel7.getContentContainer();
                panel7ContentContainer.setLayout(null);

                //---- SCNBU ----
                SCNBU.setName("SCNBU");
                panel7ContentContainer.add(SCNBU);
                SCNBU.setBounds(220, 20, 40, SCNBU.getPreferredSize().height);

                //---- OBJ_192 ----
                OBJ_192.setText("Sessions graphiques");
                OBJ_192.setName("OBJ_192");
                panel7ContentContainer.add(OBJ_192);
                OBJ_192.setBounds(10, 53, 175, 22);

                //---- SCVGM ----
                SCVGM.setName("SCVGM");
                panel7ContentContainer.add(SCVGM);
                SCVGM.setBounds(220, 50, 40, SCVGM.getPreferredSize().height);

                //---- OBJ_193 ----
                OBJ_193.setText("Nombre de bulletins");
                OBJ_193.setName("OBJ_193");
                panel7ContentContainer.add(OBJ_193);
                OBJ_193.setBounds(10, 83, 175, 22);

                //---- SCBUL ----
                SCBUL.setForeground(Color.black);
                SCBUL.setName("SCBUL");
                panel7ContentContainer.add(SCBUL);
                SCBUL.setBounds(200, 80, 60, SCBUL.getPreferredSize().height);

                //---- label3 ----
                label3.setText("Nombre de sessions");
                label3.setName("label3");
                panel7ContentContainer.add(label3);
                label3.setBounds(10, 23, 175, 22);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < panel7ContentContainer.getComponentCount(); i++) {
                    Rectangle bounds = panel7ContentContainer.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = panel7ContentContainer.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  panel7ContentContainer.setMinimumSize(preferredSize);
                  panel7ContentContainer.setPreferredSize(preferredSize);
                }
              }
              panel10.add(panel7);
              panel7.setBounds(367, 440, 290, 160);

              //======== panel8 ========
              {
                panel8.setTitle("Connexions");
                panel8.setBorder(new DropShadowBorder());
                panel8.setName("panel8");
                Container panel8ContentContainer = panel8.getContentContainer();
                panel8ContentContainer.setLayout(null);

                //---- OBJ_209 ----
                OBJ_209.setText("Syst\u00e8me");
                OBJ_209.setName("OBJ_209");
                panel8ContentContainer.add(OBJ_209);
                OBJ_209.setBounds(10, 25, 125, 18);

                //---- SCSYST ----
                SCSYST.setName("SCSYST");
                panel8ContentContainer.add(SCSYST);
                SCSYST.setBounds(180, 20, 80, SCSYST.getPreferredSize().height);

                //---- OBJ_203 ----
                OBJ_203.setText("Ressource communication");
                OBJ_203.setName("OBJ_203");
                panel8ContentContainer.add(OBJ_203);
                OBJ_203.setBounds(10, 84, 170, 20);

                //---- SCRSC ----
                SCRSC.setName("SCRSC");
                panel8ContentContainer.add(SCRSC);
                SCRSC.setBounds(180, 80, 80, SCRSC.getPreferredSize().height);

                //---- OBJ_205 ----
                OBJ_205.setText("Adresse IP");
                OBJ_205.setName("OBJ_205");
                panel8ContentContainer.add(OBJ_205);
                OBJ_205.setBounds(10, 54, 125, 20);

                //---- SCTCP ----
                SCTCP.setName("SCTCP");
                panel8ContentContainer.add(SCTCP);
                SCTCP.setBounds(140, 50, 120, SCTCP.getPreferredSize().height);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < panel8ContentContainer.getComponentCount(); i++) {
                    Rectangle bounds = panel8ContentContainer.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = panel8ContentContainer.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  panel8ContentContainer.setMinimumSize(preferredSize);
                  panel8ContentContainer.setPreferredSize(preferredSize);
                }
              }
              panel10.add(panel8);
              panel8.setBounds(670, 440, 295, 160);

              //======== panel6 ========
              {
                panel6.setTitle("Modules");
                panel6.setBorder(new DropShadowBorder());
                panel6.setName("panel6");
                Container panel6ContentContainer = panel6.getContentContainer();
                panel6ContentContainer.setLayout(null);

                //---- SCCGM ----
                SCCGM.setText("CGM");
                SCCGM.setName("SCCGM");
                panel6ContentContainer.add(SCCGM);
                SCCGM.setBounds(20, 10, 60, SCCGM.getPreferredSize().height);

                //---- SCGEM ----
                SCGEM.setText("GEM");
                SCGEM.setName("SCGEM");
                panel6ContentContainer.add(SCGEM);
                SCGEM.setBounds(20, 52, 60, SCGEM.getPreferredSize().height);

                //---- SCGIM ----
                SCGIM.setText("GIM");
                SCGIM.setName("SCGIM");
                panel6ContentContainer.add(SCGIM);
                SCGIM.setBounds(20, 73, 60, SCGIM.getPreferredSize().height);

                //---- SCGVM ----
                SCGVM.setText("GVM");
                SCGVM.setName("SCGVM");
                panel6ContentContainer.add(SCGVM);
                SCGVM.setBounds(20, 115, 60, SCGVM.getPreferredSize().height);

                //---- SCGLF ----
                SCGLF.setText("GLF");
                SCGLF.setName("SCGLF");
                panel6ContentContainer.add(SCGLF);
                SCGLF.setBounds(20, 31, 60, SCGLF.getPreferredSize().height);

                //---- SCPAM ----
                SCPAM.setText("PAM");
                SCPAM.setName("SCPAM");
                panel6ContentContainer.add(SCPAM);
                SCPAM.setBounds(20, 94, 60, SCPAM.getPreferredSize().height);

                //---- SCGPM ----
                SCGPM.setText("GPM");
                SCGPM.setName("SCGPM");
                panel6ContentContainer.add(SCGPM);
                SCGPM.setBounds(20, 157, 60, SCGPM.getPreferredSize().height);

                //---- SCGMM ----
                SCGMM.setText("GMM");
                SCGMM.setName("SCGMM");
                panel6ContentContainer.add(SCGMM);
                SCGMM.setBounds(20, 178, 60, SCGMM.getPreferredSize().height);

                //---- SCWEB ----
                SCWEB.setText("WEB");
                SCWEB.setName("SCWEB");
                panel6ContentContainer.add(SCWEB);
                SCWEB.setBounds(20, 199, 60, SCWEB.getPreferredSize().height);

                //---- SCGAM ----
                SCGAM.setText("GAM");
                SCGAM.setName("SCGAM");
                panel6ContentContainer.add(SCGAM);
                SCGAM.setBounds(20, 136, 60, SCGAM.getPreferredSize().height);

                //---- label1 ----
                label1.setText(" - Comptabilit\u00e9");
                label1.setName("label1");
                panel6ContentContainer.add(label1);
                label1.setBounds(85, 10, 230, 18);

                //---- label4 ----
                label4.setText(" - Liasse fiscale");
                label4.setName("label4");
                panel6ContentContainer.add(label4);
                label4.setBounds(85, 31, 230, 18);

                //---- label5 ----
                label5.setText(" - R\u00e9glements \u00e9volu\u00e9s");
                label5.setName("label5");
                panel6ContentContainer.add(label5);
                label5.setBounds(85, 52, 230, 18);

                //---- label6 ----
                label6.setText(" - Immobilisations");
                label6.setName("label6");
                panel6ContentContainer.add(label6);
                label6.setBounds(85, 73, 230, 18);

                //---- label9 ----
                label9.setText(" - Paie / Ressources humaines");
                label9.setName("label9");
                panel6ContentContainer.add(label9);
                label9.setBounds(85, 94, 230, 18);

                //---- label10 ----
                label10.setText(" - Ventes");
                label10.setName("label10");
                panel6ContentContainer.add(label10);
                label10.setBounds(85, 115, 230, 18);

                //---- label11 ----
                label11.setText(" - Achats");
                label11.setName("label11");
                panel6ContentContainer.add(label11);
                label11.setBounds(85, 136, 230, 18);

                //---- label12 ----
                label12.setText(" - Production");
                label12.setName("label12");
                panel6ContentContainer.add(label12);
                label12.setBounds(85, 157, 230, 18);

                //---- label13 ----
                label13.setText(" - Maintenance");
                label13.setName("label13");
                panel6ContentContainer.add(label13);
                label13.setBounds(85, 178, 230, 18);

                //---- label14 ----
                label14.setText(" - Pack internet");
                label14.setName("label14");
                panel6ContentContainer.add(label14);
                label14.setBounds(85, 199, 230, 18);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < panel6ContentContainer.getComponentCount(); i++) {
                    Rectangle bounds = panel6ContentContainer.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = panel6ContentContainer.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  panel6ContentContainer.setMinimumSize(preferredSize);
                  panel6ContentContainer.setPreferredSize(preferredSize);
                }
              }
              panel10.add(panel6);
              panel6.setBounds(12, 340, 343, 260);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel10.getComponentCount(); i++) {
                  Rectangle bounds = panel10.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel10.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel10.setMinimumSize(preferredSize);
                panel10.setPreferredSize(preferredSize);
              }
            }
            layeredPane1.add(panel10, JLayeredPane.DEFAULT_LAYER);
            panel10.setBounds(10, 5, 980, 605);

            //======== p_desac ========
            {
              p_desac.setOpaque(false);
              p_desac.setPreferredSize(new Dimension(950, 650));
              p_desac.setName("p_desac");
              p_desac.setLayout(null);

              //---- lab_desac ----
              lab_desac.setName("lab_desac");
              p_desac.add(lab_desac);
              lab_desac.setBounds(200, 100, 670, 435);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < p_desac.getComponentCount(); i++) {
                  Rectangle bounds = p_desac.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = p_desac.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                p_desac.setMinimumSize(preferredSize);
                p_desac.setPreferredSize(preferredSize);
              }
            }
            layeredPane1.add(p_desac, JLayeredPane.DEFAULT_LAYER);
            p_desac.setBounds(0, 0, 965, 595);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addComponent(layeredPane1, GroupLayout.DEFAULT_SIZE, 998, Short.MAX_VALUE)
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addComponent(layeredPane1, GroupLayout.PREFERRED_SIZE, 612, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(7, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_5);
    }

    //---- TCI1 ----
    TCI1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI1.setBorder(null);
    TCI1.setName("TCI1");
    TCI1.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI1ActionPerformed(e);
      }
    });

    //---- TCI2 ----
    TCI2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI2.setBorder(null);
    TCI2.setName("TCI2");
    TCI2.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI2ActionPerformed(e);
      }
    });

    //---- TCI3 ----
    TCI3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI3.setBorder(null);
    TCI3.setName("TCI3");
    TCI3.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI3ActionPerformed(e);
      }
    });

    //---- TCI5 ----
    TCI5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI5.setBorder(null);
    TCI5.setName("TCI5");
    TCI5.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI5ActionPerformed(e);
      }
    });

    //---- TCI4 ----
    TCI4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI4.setBorder(null);
    TCI4.setName("TCI4");
    TCI4.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI4ActionPerformed(e);
      }
    });

    //---- TCI6 ----
    TCI6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI6.setBorder(null);
    TCI6.setName("TCI6");
    TCI6.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI6ActionPerformed(e);
      }
    });

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- invite ----
      invite.setText("Choix possibles");
      invite.setName("invite");
      invite.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          inviteActionPerformed(e);
        }
      });
      BTD.add(invite);

      //---- aide ----
      aide.setText("Aide en ligne");
      aide.setName("aide");
      aide.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          aideActionPerformed(e);
        }
      });
      BTD.add(aide);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_84_OBJ_84;
  private XRiTextField INDETB;
  private JLabel OBJECT_4;
  private XRiTextField INDCLI;
  private XRiTextField INDLIV;
  private JPanel p_tete_droite;
  private JPanel panel3;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu13;
  private RiSousMenu_bt riSousMenu_bt13;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JLayeredPane layeredPane1;
  private JPanel panel10;
  private JPanel OBJ_65;
  private XRiTextField CLNOM;
  private XRiTextField CLCPL;
  private XRiTextField CLRUE;
  private XRiTextField CLLOC;
  private XRiTextField CLPAY;
  private XRiTextField CLVILR;
  private XRiTextField CLOBS;
  private JLabel OBJ_115;
  private XRiTextField CLCDP;
  private XRiTextField CLCOP;
  private JPanel OBJ_121;
  private SNBoutonDetail OBJ_117;
  private JPanel panel1;
  private JPanel panel9;
  private JXTitledSeparator xTitledSeparator1;
  private XRiTextField CLCAT;
  private RiZoneSortie OBJ_83;
  private JPanel panel11;
  private JXTitledSeparator xTitledSeparator3;
  private XRiTextField CLREP;
  private SNBoutonDetail OBJ_111;
  private RiZoneSortie OBJ_110;
  private XRiTextField CLREP2;
  private SNBoutonDetail OBJ_120;
  private RiZoneSortie OBJ_119;
  private JPanel panel12;
  private JXTitledSeparator xTitledSeparator2;
  private XRiTextField CLCLK;
  private JPanel panel13;
  private XRiTextField SCTSM;
  private XRiTextField SCEFF;
  private XRiTextField CLCLA;
  private JLabel label7;
  private XRiTextField SCCAF;
  private JLabel OBJ_72;
  private XRiTextField SCAPE;
  private JLabel OBJ_73;
  private XRiTextField WVERS;
  private JLabel OBJ_74;
  private JLabel OBJ_75;
  private JLabel OBJ_76;
  private JLabel label8;
  private XRiTextField SCBASP;
  private JPanel panel14;
  private JXTitledSeparator xTitledSeparator4;
  private XRiTextField CLTEL;
  private XRiTextField CLFAX;
  private SNBoutonDetail OBJ_128;
  private JXTitledPanel panel2;
  private XRiTextField CLPAC;
  private XRiTextField PE3PLU;
  private JXTitledPanel panel5;
  private JLabel OBJ_164;
  private XRiTextField SCTMA;
  private JLabel OBJ_170;
  private XRiTextField SCVOS;
  private JLabel OBJ_172;
  private XRiTextField SCSEN;
  private JLabel OBJ_176;
  private XRiTextField SCNFA;
  private JXTitledPanel panel4;
  private JLabel OBJ_138;
  private XRiTextField SCCSL;
  private JLabel OBJ_141;
  private XRiCalendrier SCDDEX;
  private JLabel OBJ_149;
  private XRiCalendrier SCDRSX;
  private JLabel OBJ_152;
  private XRiTextField SCNFS;
  private JXTitledPanel panel7;
  private XRiTextField SCNBU;
  private JLabel OBJ_192;
  private XRiTextField SCVGM;
  private JLabel OBJ_193;
  private XRiTextField SCBUL;
  private JLabel label3;
  private JXTitledPanel panel8;
  private JLabel OBJ_209;
  private XRiTextField SCSYST;
  private JLabel OBJ_203;
  private XRiTextField SCRSC;
  private JLabel OBJ_205;
  private XRiTextField SCTCP;
  private JXTitledPanel panel6;
  private XRiCheckBox SCCGM;
  private XRiCheckBox SCGEM;
  private XRiCheckBox SCGIM;
  private XRiCheckBox SCGVM;
  private XRiCheckBox SCGLF;
  private XRiCheckBox SCPAM;
  private XRiCheckBox SCGPM;
  private XRiCheckBox SCGMM;
  private XRiCheckBox SCWEB;
  private XRiCheckBox SCGAM;
  private JLabel label1;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label9;
  private JLabel label10;
  private JLabel label11;
  private JLabel label12;
  private JLabel label13;
  private JLabel label14;
  private JPanel p_desac;
  private JLabel lab_desac;
  private JPopupMenu BTD2;
  private JMenuItem OBJ_5;
  private SNBoutonDetail TCI1;
  private SNBoutonDetail TCI2;
  private SNBoutonDetail TCI3;
  private SNBoutonDetail TCI5;
  private SNBoutonDetail TCI4;
  private SNBoutonDetail TCI6;
  private JPopupMenu BTD;
  private JMenuItem invite;
  private JMenuItem aide;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
