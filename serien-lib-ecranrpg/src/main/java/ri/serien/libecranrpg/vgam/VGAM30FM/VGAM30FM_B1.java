/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libecranrpg.vgam.VGAM30FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.dialoguestandard.information.DialogueInformation;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * Import des catalogue fournisseurs.
 * [GAM6726] Gestion des achats -> Editions diverses -> Importations et exportations -> Importations articles -> Imports catalogues.
 */
public class VGAM30FM_B1 extends SNPanelEcranRPG implements ioFrame {
  private static final String CREER = "Créer";
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", "WTP16", "WTP17", "WTP18", "WTP19", "WTP20", };
  private String[] _WTP01_Title = { "TITLD1", };
  private String[][] _WTP01_Data = { { "LD101", }, { "LD102", }, { "LD103", }, { "LD104", }, { "LD105", }, { "LD106", }, { "LD107", },
      { "LD108", }, { "LD109", }, { "LD110", }, { "LD111", }, { "LD112", }, { "LD113", }, { "LD114", }, { "LD115", }, { "LD116", },
      { "LD117", }, { "LD118", }, { "LD119", }, { "LD120", }, };
  private int[] _WTP01_Width = { 559, };
  
  
  public VGAM30FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
    
    snBarreBouton.ajouterBouton(CREER, 'c', true);
    snBarreBouton.ajouterBouton(EnumBouton.CONSULTER, true);
    snBarreBouton.ajouterBouton(EnumBouton.RECHERCHER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    LIBNUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBNUM@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Afficher le message d'erreur s'il y en a un
    if (lexique.isTrue("19")) {
      String message = lexique.HostFieldGetData("V03F");
      if (message != null && !message.isEmpty()) {
        DialogueInformation.afficher(message);
      }
    }
    
    // Charger les données du composant établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Renseigner le fournisseur
    snFournisseur.setSession(getSession());
    snFournisseur.setIdEtablissement(snEtablissement.getIdSelection());
    snFournisseur.charger(false);
    snFournisseur.setSelectionParChampRPG(lexique, "WCOL", "WFRS");
    
    // Rafraîchir les boutons
    rafraichirBoutons();
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snFournisseur.renseignerChampRPG(lexique, "WCOL", "WFRS");
    snEtablissement.renseignerChampRPG(lexique, "WETB");
  }
  
  // Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(EnumBouton.CONSULTER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.RECHERCHER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(CREER)) {
        lexique.HostScreenSendKey(this, "F13");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * On rafraichit les boutons consulter et rechercher
   */
  private void rafraichirBoutons() {
    snBarreBouton.activerBouton(EnumBouton.CONSULTER, WTP01.getSelectedRowCount() > 0);
    snBarreBouton.activerBouton(EnumBouton.RECHERCHER, WTP01.getSelectedRowCount() < 1);
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    rafraichirBoutons();
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD2.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      snFournisseur.setIdEtablissement(snEtablissement.getIdSelection());
      snFournisseur.charger(false);
      lexique.HostScreenSendKey(this, "Enter");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpTitre = new SNBandeauTitre();
    pnlContenu = new JPanel();
    pnlFiltre = new JPanel();
    pnlFiltre1 = new JPanel();
    lbFournisseur = new SNLabelChamp();
    snFournisseur = new SNFournisseur();
    lbConfigurationImport = new SNLabelChamp();
    WNUM = new XRiTextField();
    LIBNUM = new RiZoneSortie();
    pnlFiltre2 = new JPanel();
    sNLabel1 = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbTitre = new SNLabelTitre();
    pnlResultat = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    snBarreBouton = new SNBarreBouton();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    BTD2 = new JPopupMenu();
    OBJ_20 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setBackground(new Color(239, 239, 222));
    setName("this");
    setLayout(new BorderLayout());

    //---- bpTitre ----
    bpTitre.setText("Imports de catalogue fournisseur");
    bpTitre.setName("bpTitre");
    add(bpTitre, BorderLayout.NORTH);

    //======== pnlContenu ========
    {
      pnlContenu.setPreferredSize(new Dimension(1020, 610));
      pnlContenu.setBorder(new EmptyBorder(10, 10, 10, 10));
      pnlContenu.setBackground(new Color(239, 239, 222));
      pnlContenu.setMinimumSize(new Dimension(1020, 610));
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {1000, 0};
      ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 430, 0};
      ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

      //======== pnlFiltre ========
      {
        pnlFiltre.setBorder(new TitledBorder(""));
        pnlFiltre.setOpaque(false);
        pnlFiltre.setMinimumSize(new Dimension(20, 100));
        pnlFiltre.setRequestFocusEnabled(false);
        pnlFiltre.setPreferredSize(new Dimension(20, 120));
        pnlFiltre.setName("pnlFiltre");
        pnlFiltre.setLayout(new GridLayout(1, 2, 5, 5));

        //======== pnlFiltre1 ========
        {
          pnlFiltre1.setBorder(null);
          pnlFiltre1.setOpaque(false);
          pnlFiltre1.setPreferredSize(new Dimension(20, 20));
          pnlFiltre1.setMaximumSize(new Dimension(877, 100));
          pnlFiltre1.setMinimumSize(new Dimension(20, 20));
          pnlFiltre1.setName("pnlFiltre1");
          pnlFiltre1.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlFiltre1.getLayout()).columnWidths = new int[] {152, 0, 309, 0};
          ((GridBagLayout)pnlFiltre1.getLayout()).rowHeights = new int[] {40, 35, 0};
          ((GridBagLayout)pnlFiltre1.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlFiltre1.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbFournisseur ----
          lbFournisseur.setText("Fournisseur");
          lbFournisseur.setName("lbFournisseur");
          pnlFiltre1.add(lbFournisseur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.NONE,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snFournisseur ----
          snFournisseur.setName("snFournisseur");
          pnlFiltre1.add(snFournisseur, new GridBagConstraints(1, 0, 2, 1, 1.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbConfigurationImport ----
          lbConfigurationImport.setText("Configuration d'import");
          lbConfigurationImport.setName("lbConfigurationImport");
          pnlFiltre1.add(lbConfigurationImport, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.NONE,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- WNUM ----
          WNUM.setComponentPopupMenu(BTD2);
          WNUM.setMinimumSize(new Dimension(100, 30));
          WNUM.setPreferredSize(new Dimension(100, 30));
          WNUM.setMaximumSize(new Dimension(100, 30));
          WNUM.setName("WNUM");
          pnlFiltre1.add(WNUM, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.NONE,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- LIBNUM ----
          LIBNUM.setText("@LIBNUM@");
          LIBNUM.setMaximumSize(new Dimension(510, 25));
          LIBNUM.setMinimumSize(new Dimension(300, 30));
          LIBNUM.setPreferredSize(new Dimension(300, 30));
          LIBNUM.setName("LIBNUM");
          pnlFiltre1.add(LIBNUM, new GridBagConstraints(2, 1, 1, 1, 1.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlFiltre.add(pnlFiltre1);

        //======== pnlFiltre2 ========
        {
          pnlFiltre2.setOpaque(false);
          pnlFiltre2.setName("pnlFiltre2");
          pnlFiltre2.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlFiltre2.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlFiltre2.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlFiltre2.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlFiltre2.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- sNLabel1 ----
          sNLabel1.setText("Etablissement");
          sNLabel1.setName("sNLabel1");
          pnlFiltre2.add(sNLabel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snEtablissement ----
          snEtablissement.setMinimumSize(new Dimension(400, 30));
          snEtablissement.setPreferredSize(new Dimension(400, 30));
          snEtablissement.setName("snEtablissement");
          pnlFiltre2.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        }
        pnlFiltre.add(pnlFiltre2);
      }
      pnlContenu.add(pnlFiltre, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //---- lbTitre ----
      lbTitre.setText("Historique des importations");
      lbTitre.setName("lbTitre");
      pnlContenu.add(lbTitre, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== pnlResultat ========
      {
        pnlResultat.setBorder(null);
        pnlResultat.setOpaque(false);
        pnlResultat.setPreferredSize(new Dimension(877, 410));
        pnlResultat.setMinimumSize(new Dimension(877, 410));
        pnlResultat.setMaximumSize(new Dimension(877, 410));
        pnlResultat.setName("pnlResultat");
        pnlResultat.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlResultat.getLayout()).columnWidths = new int[] {944, 0, 0};
        ((GridBagLayout)pnlResultat.getLayout()).rowHeights = new int[] {177, 172, 0};
        ((GridBagLayout)pnlResultat.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnlResultat.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //======== SCROLLPANE_LIST ========
        {
          SCROLLPANE_LIST.setComponentPopupMenu(BTD);
          SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

          //---- WTP01 ----
          WTP01.setComponentPopupMenu(BTD);
          WTP01.setName("WTP01");
          WTP01.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              WTP01MouseClicked(e);
            }
          });
          SCROLLPANE_LIST.setViewportView(WTP01);
        }
        pnlResultat.add(SCROLLPANE_LIST, new GridBagConstraints(0, 0, 1, 2, 1.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- BT_PGUP ----
        BT_PGUP.setText("");
        BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
        BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGUP.setMaximumSize(new Dimension(28, 132));
        BT_PGUP.setMinimumSize(new Dimension(28, 132));
        BT_PGUP.setPreferredSize(new Dimension(28, 132));
        BT_PGUP.setName("BT_PGUP");
        pnlResultat.add(BT_PGUP, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- BT_PGDOWN ----
        BT_PGDOWN.setText("");
        BT_PGDOWN.setToolTipText("Page suivante");
        BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGDOWN.setMaximumSize(new Dimension(28, 132));
        BT_PGDOWN.setMinimumSize(new Dimension(28, 132));
        BT_PGDOWN.setPreferredSize(new Dimension(28, 132));
        BT_PGDOWN.setName("BT_PGDOWN");
        pnlResultat.add(BT_PGDOWN, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlResultat, new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
        GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);
    }

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- OBJ_20 ----
      OBJ_20.setText("Choix possibles");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_20);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpTitre;
  private JPanel pnlContenu;
  private JPanel pnlFiltre;
  private JPanel pnlFiltre1;
  private SNLabelChamp lbFournisseur;
  private SNFournisseur snFournisseur;
  private SNLabelChamp lbConfigurationImport;
  private XRiTextField WNUM;
  private RiZoneSortie LIBNUM;
  private JPanel pnlFiltre2;
  private SNLabelChamp sNLabel1;
  private SNEtablissement snEtablissement;
  private SNLabelTitre lbTitre;
  private JPanel pnlResultat;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private SNBarreBouton snBarreBouton;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JPopupMenu BTD2;
  private JMenuItem OBJ_20;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
