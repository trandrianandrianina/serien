
package ri.serien.libecranrpg.vgam.VGAM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGAM01FX_BF extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] BFT06_Value = { "", "A", "C", "N", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", };
  private String[] BFT05_Value = { "", "A", "C", "N", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", };
  private String[] BFT04_Value = { "", "A", "C", "N", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", };
  private String[] BFT03_Value = { "", "A", "C", "N", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", };
  private String[] BFT02_Value = { "", "A", "C", "N", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", };
  private String[] BFT01_Value = { "", "A", "C", "N", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", };
  
  public VGAM01FX_BF(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    BFT06.setValeurs(BFT06_Value, null);
    BFT05.setValeurs(BFT05_Value, null);
    BFT04.setValeurs(BFT04_Value, null);
    BFT03.setValeurs(BFT03_Value, null);
    BFT02.setValeurs(BFT02_Value, null);
    BFT01.setValeurs(BFT01_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    // BFT01.setSelectedIndex(getIndice("BFT01", BFT01_Value));
    // BFT02.setSelectedIndex(getIndice("BFT02", BFT02_Value));
    // BFT03.setSelectedIndex(getIndice("BFT03", BFT03_Value));
    // BFT04.setSelectedIndex(getIndice("BFT04", BFT04_Value));
    // BFT05.setSelectedIndex(getIndice("BFT05", BFT05_Value));
    // BFT06.setSelectedIndex(getIndice("BFT06", BFT06_Value));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@ de @LOCGRP/-1/@"));
    
    

    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("BFT06", 0, BFT06_Value[BFT06.getSelectedIndex()]);
    // lexique.HostFieldPutData("BFT05", 0, BFT05_Value[BFT05.getSelectedIndex()]);
    // lexique.HostFieldPutData("BFT04", 0, BFT04_Value[BFT04.getSelectedIndex()]);
    // lexique.HostFieldPutData("BFT03", 0, BFT03_Value[BFT03.getSelectedIndex()]);
    // lexique.HostFieldPutData("BFT02", 0, BFT02_Value[BFT02.getSelectedIndex()]);
    // lexique.HostFieldPutData("BFT01", 0, BFT01_Value[BFT01.getSelectedIndex()]);
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_48 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    BFT01 = new XRiComboBox();
    BFT02 = new XRiComboBox();
    BFT03 = new XRiComboBox();
    BFT04 = new XRiComboBox();
    BFT05 = new XRiComboBox();
    BFT06 = new XRiComboBox();
    BFL01 = new XRiTextField();
    BFL02 = new XRiTextField();
    BFL03 = new XRiTextField();
    BFL04 = new XRiTextField();
    BFL05 = new XRiTextField();
    BFL06 = new XRiTextField();
    OBJ_73 = new JLabel();
    OBJ_72 = new JLabel();
    OBJ_53 = new JLabel();
    OBJ_55 = new JLabel();
    OBJ_59 = new JLabel();
    OBJ_61 = new JLabel();
    OBJ_63 = new JLabel();
    OBJ_65 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des achats");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44 ----
          OBJ_44.setText("Etablissement");
          OBJ_44.setName("OBJ_44");

          //---- INDETB ----
          INDETB.setName("INDETB");

          //---- OBJ_46 ----
          OBJ_46.setText("Code");
          OBJ_46.setName("OBJ_46");

          //---- INDTYP ----
          INDTYP.setName("INDTYP");

          //---- OBJ_48 ----
          OBJ_48.setText("Ordre");
          OBJ_48.setName("OBJ_48");

          //---- INDIND ----
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(513, Short.MAX_VALUE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(650, 350));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Bases de calcul des frais sur achats");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- BFT01 ----
            BFT01.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Prix d'achat",
              "CAF",
              "Achat - bonif. p\u00e9riodique",
              "Prix tarif 1",
              "Prix tarif 2",
              "Prix tarif 3",
              "Prix tarif 4",
              "Prix tarif 5",
              "Prix tarif 6",
              "Prix tarif 7",
              "Prix tarif 8",
              "Prix tarif 9",
              "Prix tarif 10"
            }));
            BFT01.setComponentPopupMenu(BTD);
            BFT01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BFT01.setName("BFT01");
            xTitledPanel1ContentContainer.add(BFT01);
            BFT01.setBounds(240, 41, 205, BFT01.getPreferredSize().height);

            //---- BFT02 ----
            BFT02.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Prix d'achat",
              "CAF",
              "Achat - bonif. p\u00e9riodique",
              "Prix tarif 1",
              "Prix tarif 2",
              "Prix tarif 3",
              "Prix tarif 4",
              "Prix tarif 5",
              "Prix tarif 6",
              "Prix tarif 7",
              "Prix tarif 8",
              "Prix tarif 9",
              "Prix tarif 10"
            }));
            BFT02.setComponentPopupMenu(BTD);
            BFT02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BFT02.setName("BFT02");
            xTitledPanel1ContentContainer.add(BFT02);
            BFT02.setBounds(240, 74, 205, BFT02.getPreferredSize().height);

            //---- BFT03 ----
            BFT03.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Prix d'achat",
              "CAF",
              "Achat - bonif. p\u00e9riodique",
              "Prix tarif 1",
              "Prix tarif 2",
              "Prix tarif 3",
              "Prix tarif 4",
              "Prix tarif 5",
              "Prix tarif 6",
              "Prix tarif 7",
              "Prix tarif 8",
              "Prix tarif 9",
              "Prix tarif 10"
            }));
            BFT03.setComponentPopupMenu(BTD);
            BFT03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BFT03.setName("BFT03");
            xTitledPanel1ContentContainer.add(BFT03);
            BFT03.setBounds(240, 107, 205, BFT03.getPreferredSize().height);

            //---- BFT04 ----
            BFT04.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Prix d'achat",
              "CAF",
              "Achat - bonif. p\u00e9riodique",
              "Prix tarif 1",
              "Prix tarif 2",
              "Prix tarif 3",
              "Prix tarif 4",
              "Prix tarif 5",
              "Prix tarif 6",
              "Prix tarif 7",
              "Prix tarif 8",
              "Prix tarif 9",
              "Prix tarif 10"
            }));
            BFT04.setComponentPopupMenu(BTD);
            BFT04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BFT04.setName("BFT04");
            xTitledPanel1ContentContainer.add(BFT04);
            BFT04.setBounds(240, 140, 205, BFT04.getPreferredSize().height);

            //---- BFT05 ----
            BFT05.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Prix d'achat",
              "CAF",
              "Achat - bonif. p\u00e9riodique",
              "Prix tarif 1",
              "Prix tarif 2",
              "Prix tarif 3",
              "Prix tarif 4",
              "Prix tarif 5",
              "Prix tarif 6",
              "Prix tarif 7",
              "Prix tarif 8",
              "Prix tarif 9",
              "Prix tarif 10"
            }));
            BFT05.setComponentPopupMenu(BTD);
            BFT05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BFT05.setName("BFT05");
            xTitledPanel1ContentContainer.add(BFT05);
            BFT05.setBounds(240, 173, 205, BFT05.getPreferredSize().height);

            //---- BFT06 ----
            BFT06.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Prix d'achat",
              "CAF",
              "Achat - bonif. p\u00e9riodique",
              "Prix tarif 1",
              "Prix tarif 2",
              "Prix tarif 3",
              "Prix tarif 4",
              "Prix tarif 5",
              "Prix tarif 6",
              "Prix tarif 7",
              "Prix tarif 8",
              "Prix tarif 9",
              "Prix tarif 10"
            }));
            BFT06.setComponentPopupMenu(BTD);
            BFT06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BFT06.setName("BFT06");
            xTitledPanel1ContentContainer.add(BFT06);
            BFT06.setBounds(240, 206, 205, BFT06.getPreferredSize().height);

            //---- BFL01 ----
            BFL01.setComponentPopupMenu(BTD);
            BFL01.setName("BFL01");
            xTitledPanel1ContentContainer.add(BFL01);
            BFL01.setBounds(55, 40, 160, BFL01.getPreferredSize().height);

            //---- BFL02 ----
            BFL02.setComponentPopupMenu(BTD);
            BFL02.setName("BFL02");
            xTitledPanel1ContentContainer.add(BFL02);
            BFL02.setBounds(55, 73, 160, BFL02.getPreferredSize().height);

            //---- BFL03 ----
            BFL03.setComponentPopupMenu(BTD);
            BFL03.setName("BFL03");
            xTitledPanel1ContentContainer.add(BFL03);
            BFL03.setBounds(55, 106, 160, BFL03.getPreferredSize().height);

            //---- BFL04 ----
            BFL04.setComponentPopupMenu(BTD);
            BFL04.setName("BFL04");
            xTitledPanel1ContentContainer.add(BFL04);
            BFL04.setBounds(55, 139, 160, BFL04.getPreferredSize().height);

            //---- BFL05 ----
            BFL05.setComponentPopupMenu(BTD);
            BFL05.setName("BFL05");
            xTitledPanel1ContentContainer.add(BFL05);
            BFL05.setBounds(55, 172, 160, BFL05.getPreferredSize().height);

            //---- BFL06 ----
            BFL06.setComponentPopupMenu(BTD);
            BFL06.setName("BFL06");
            xTitledPanel1ContentContainer.add(BFL06);
            BFL06.setBounds(55, 205, 160, BFL06.getPreferredSize().height);

            //---- OBJ_73 ----
            OBJ_73.setText("Initialisation");
            OBJ_73.setFont(OBJ_73.getFont().deriveFont(OBJ_73.getFont().getStyle() | Font.BOLD));
            OBJ_73.setName("OBJ_73");
            xTitledPanel1ContentContainer.add(OBJ_73);
            OBJ_73.setBounds(240, 15, 72, 16);

            //---- OBJ_72 ----
            OBJ_72.setText("Libell\u00e9");
            OBJ_72.setFont(OBJ_72.getFont().deriveFont(OBJ_72.getFont().getStyle() | Font.BOLD));
            OBJ_72.setName("OBJ_72");
            xTitledPanel1ContentContainer.add(OBJ_72);
            OBJ_72.setBounds(55, 15, 62, 16);

            //---- OBJ_53 ----
            OBJ_53.setText("1");
            OBJ_53.setName("OBJ_53");
            xTitledPanel1ContentContainer.add(OBJ_53);
            OBJ_53.setBounds(30, 44, 28, 20);

            //---- OBJ_55 ----
            OBJ_55.setText("2");
            OBJ_55.setName("OBJ_55");
            xTitledPanel1ContentContainer.add(OBJ_55);
            OBJ_55.setBounds(30, 77, 28, 20);

            //---- OBJ_59 ----
            OBJ_59.setText("3");
            OBJ_59.setName("OBJ_59");
            xTitledPanel1ContentContainer.add(OBJ_59);
            OBJ_59.setBounds(30, 110, 28, 20);

            //---- OBJ_61 ----
            OBJ_61.setText("4");
            OBJ_61.setName("OBJ_61");
            xTitledPanel1ContentContainer.add(OBJ_61);
            OBJ_61.setBounds(30, 143, 28, 20);

            //---- OBJ_63 ----
            OBJ_63.setText("5");
            OBJ_63.setName("OBJ_63");
            xTitledPanel1ContentContainer.add(OBJ_63);
            OBJ_63.setBounds(30, 176, 28, 20);

            //---- OBJ_65 ----
            OBJ_65.setText("6");
            OBJ_65.setName("OBJ_65");
            xTitledPanel1ContentContainer.add(OBJ_65);
            OBJ_65.setBounds(30, 209, 28, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(77, 77, 77)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 488, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(83, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(34, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44;
  private XRiTextField INDETB;
  private JLabel OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_48;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private XRiComboBox BFT01;
  private XRiComboBox BFT02;
  private XRiComboBox BFT03;
  private XRiComboBox BFT04;
  private XRiComboBox BFT05;
  private XRiComboBox BFT06;
  private XRiTextField BFL01;
  private XRiTextField BFL02;
  private XRiTextField BFL03;
  private XRiTextField BFL04;
  private XRiTextField BFL05;
  private XRiTextField BFL06;
  private JLabel OBJ_73;
  private JLabel OBJ_72;
  private JLabel OBJ_53;
  private JLabel OBJ_55;
  private JLabel OBJ_59;
  private JLabel OBJ_61;
  private JLabel OBJ_63;
  private JLabel OBJ_65;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
