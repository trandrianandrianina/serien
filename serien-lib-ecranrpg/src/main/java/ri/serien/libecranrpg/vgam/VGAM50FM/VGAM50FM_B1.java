/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libecranrpg.vgam.VGAM50FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.commun.message.EnumImportanceMessage;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.ODialog;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGAM50FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private ODialog dialog_PANEL = null;
  private ODialog dialog_PANEL2 = null;
  private boolean prixMini = false;
  private String[] CAIN2_Value = { " ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", };
  private String[] CAIN7_Value = { "", "J" };
  private String[] CATRL_Value = { " ", "A", };
  
  public VGAM50FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    CAIN1.setValeursSelection("P", " ");
    CAIN2.setValeurs(CAIN2_Value, null);
    CAIN4.setValeursSelection("1", " ");
    CAIN5.setValeursSelection("1", " ");
    CAIN7.setValeurs(CAIN7_Value, null);
    CATRL.setValeurs(CATRL_Value, null);
    WATTN1.setValeursSelection("1", " ");
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    WECAM1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WECAM1@")).trim());
    OPALIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OPALIB@")).trim());
    ULBUNA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ULBUNA@")).trim());
    DVLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DVLIB@")).trim());
    CAUNA1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAUNA@")).trim());
    UPNETX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UPNETX@")).trim());
    CAUNA2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAUNA@")).trim());
    CAPRVX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAPRVX@")).trim());
    CAUNA3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAUNA@")).trim());
    WKAP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WKAP@")).trim());
    CAUNA4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAUNA@")).trim());
    FRCNR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRCNR@")).trim());
    ULBUNC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ULBUNC@")).trim());
    lbULBUNC1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("1 @ULBUNC@  =")).trim());
    lbUNITESTOCKS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("unités de stocks (@WUNS4@)")).trim());
    ULBUNA_COPIE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ULBUNA@")).trim());
    lbULBUNC2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("1 @ULBUNC@  =")).trim());
    lbUNITEACHAT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("unités d'achats (@WCAUNA@)")).trim());
    WUNS1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUNS1@")).trim());
    WUNS2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUNS2@")).trim());
    WUNS3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUNS3@")).trim());
    lbTITQEC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITQEC@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    navig_valid.setVisible(!lexique.isTrue("53"));
    boolean isModification = lexique.getMode() != Lexical.MODE_CONSULTATION;
    
    // RAZ de la mémorisation des onglets
    if (lexique.HostFieldGetData("TOPRAZ").trim().equals("1")) {
      lexique.addVariableGlobale("ongletEnCours_VGAM50FM_B1", null);
    }
    // mémorisation des bavettes
    if (lexique.getValeurVariableGlobale("ongletEnCours_VGAM50FM_B1") != null) {
      btpCONDITIONACHAT.setSelectedIndex((Integer) lexique.getValeurVariableGlobale("ongletEnCours_VGAM50FM_B1"));
    }
    
    CAIN3.setVisible(lexique.HostFieldGetData("CAIN3").equalsIgnoreCase("G"));
    if (CAIN4.isSelected()) {
      prixMini = true;
    }
    lbCAGCD.setVisible(lexique.isTrue("N63"));
    
    lbCARGA.setVisible(lexique.isTrue("N68"));
    lbCAREF.setVisible(lexique.isTrue("N60"));
    lbCARFC.setVisible(lexique.isTrue("N60"));
    
    prixMini = CAIN4.isSelected();
    
    CAKPR.setVisible(!prixMini);
    lbCAKPR.setVisible(!prixMini);
    
    DVLIB.setVisible(lexique.isTrue("N86"));
    lbCADEV.setVisible(lexique.isTrue("N86"));
    pnlDevise.setVisible(lexique.isTrue("N86"));
    lbPrixen.setVisible(lexique.isTrue("(N86) AND (N87)"));
    DGDEV.setVisible(lexique.isTrue("(N86) AND (N87)"));
    UPNDVX.setVisible(lexique.isTrue("(N86) AND (N87)"));
    lbTaux.setVisible(lexique.isTrue("(N86) AND (N87)"));
    WCHGX.setVisible(lexique.isTrue("(N86) AND (N87)"));
    CAUNA1.setEnabled(false);
    CAUNA2.setEnabled(false);
    CAUNA3.setEnabled(false);
    CAUNA4.setEnabled(false);
    CAUNA_COPIE.setEnabled(false);
    ULBUNA_COPIE.setEnabled(false);
    snFournisseurRegroupement.setEnabled(false);
    snFournisseurPrincipal.setEnabled(lexique.getMode() == Lexical.MODE_CREATION);
    
    // Panel regroupant GENERAL et FOURNISSEUR +++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if (lexique.HostFieldGetData("WFRFRR").trim().equals("")) {
      lbCARFC.setText(lexique.HostFieldGetData("MERFAB"));
      CAIN3.setText("Mise à jour du prix de vente à partir de");
      CAIN3.setForeground(Color.BLACK);
    }
    else {
      lbCARFC.setText(lexique.HostFieldGetData("PAR"));
      CAIN3.setVisible(lexique.HostFieldGetData("CAIN3").trim().equalsIgnoreCase("G"));
    }
    
    if (lexique.HostFieldGetData("MEPNET").trim().equals("")) {
      lbUPNETX.setText("Prix net");
    }
    
    FRCNR.setVisible(true);
    
    // PANEL UNITES ET QUANTITES +++++++
    
    CAUNA_COPIE.setText(lexique.HostFieldGetData("CAUNA"));
    
    // pas visible en consultation
    riSousMenu7.setEnabled(lexique.isTrue("N53"));
    riSousMenu12.setEnabled(lexique.isTrue("N53"));
    
    riSousMenu9.setEnabled(lexique.isTrue("N41"));
    riSousMenu10.setEnabled(lexique.isTrue("41"));
    riSousMenu11.setEnabled(lexique.isTrue("52"));
    
    lbCADAFX.setVisible(CADAFX.isVisible());
    lbCADEL.setVisible(CADEL.isVisible());
    lbCADELS.setVisible(CADELS.isVisible());
    lbCAIN7.setVisible(CAIN7.isVisible());
    lbCAQMI.setVisible(CAQMI.isVisible());
    WUNS1.setVisible(CAQMI.isVisible());
    lbTITQEC.setVisible(CAQEC.isVisible());
    WUNS2.setVisible(CAQMI.isVisible());
    OBJ_188.setVisible(CANUA.isVisible());
    WUNS3.setVisible(CAQMI.isVisible());
    
    // Gérer le champ "A partir de" et le bouton associé
    lib_CACPV.setText(gererCACPV(lexique.HostFieldGetData("CACPV")));
    lib_CACPV.setVisible(lexique.isTrue("N59"));
    lib_CACPV.setEnabled(isModification);
    btCACPV.setVisible(lib_CACPV.isVisible());
    btCACPV.setEnabled(lib_CACPV.isEnabled());
    
    btCAIN4.setEnabled(CAIN4.isEnabled());
    btCAIN4.setVisible(lexique.isTrue("N59") && (isModification));
    btCAIN4.setVisible(CAIN4.isSelected());
    
    if (!lexique.HostFieldGetData("CAFRS").trim().equals("")) {
      btpCONDITIONACHAT.setSelectedIndex(1);
    }
    
    lbWATTN2.setVisible(WATTN2.isVisible());
    
    // Composant Fournisseur
    snFournisseurPrincipal.setSession(getSession());
    snFournisseurPrincipal.setIdEtablissement(IdEtablissement.getInstance(lexique.HostFieldGetData("CAETB")));
    snFournisseurPrincipal.charger(false);
    snFournisseurPrincipal.setSelectionParChampRPG(lexique, "CACOL", "CAFRS");
    // Composant Fournisseur Regroupement
    snFournisseurRegroupement.setSession(getSession());
    snFournisseurRegroupement.setIdEtablissement(IdEtablissement.getInstance(lexique.HostFieldGetData("CAETB")));
    snFournisseurRegroupement.charger(false);
    snFournisseurRegroupement.setSelectionParChampRPG(lexique, "WFRCOR", "WFRFRR");
    
    // Titre
    setTitle(interpreteurD.analyseExpression(
        "Conditions normales d'achat : " + lexique.HostFieldGetData("A1ART").trim() + " - " + lexique.HostFieldGetData("A1LIB")));
    
    rbCAFV1.setSelected(!CAFV1.getText().trim().isEmpty());
    rbCAFP1.setSelected(!rbCAFV1.isSelected());
    rbCAFV1.setEnabled(isModification);
    rbCAFP1.setEnabled(isModification);
    CAFV1.setEnabled(rbCAFV1.isSelected() && isModification);
    CAFK1.setEnabled(rbCAFV1.isSelected() && isModification);
    CAFP1.setEnabled(rbCAFP1.isSelected() && isModification);
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
    riMenu_bt4.setIcon(lexique.chargerImage("images/fonctions.png", true));
    bouton_art.setIcon(lexique.chargerImage("images/fin_p.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    lexique.addVariableGlobale("ongletEnCours_VGAM50FM_B1", btpCONDITIONACHAT.getSelectedIndex());
    snFournisseurPrincipal.renseignerChampRPG(lexique, "CACOL", "CAFRS");
  }
  
  protected String gererCACPV(String valeur) {
    if (valeur != null) {
      valeur = valeur.trim();
      if (valeur.equals("")) {
        return "";
      }
      else if (valeur.equals("V")) {
        return "Prix catalogue x Coeff.";
      }
      else if (valeur.equals("C")) {
        return "Prix catalogue seul";
      }
      else if (valeur.equals("W")) {
        return "Prix catalogue x coeff. d'approche x coeff.";
      }
      else if (valeur.equals("N")) {
        return "Prix net d'achat x Coeff.";
      }
      else if (valeur.equals("M")) {
        return "Prix net x Coeff. d'approche moyen x coeff.";
      }
      else if (valeur.equals("A")) {
        return "Prix net x Coeff. d'approche saisi x coeff.";
      }
      else if (valeur.equals("1")) {
        return "1er prix quantitatif";
      }
      else if (valeur.equals("2")) {
        return "2eme prix quantitatif";
      }
      else if (valeur.equals("3")) {
        return "3eme prix quantitatif";
      }
      else if (valeur.equals("4")) {
        return "4eme prix quantitatif";
      }
      else if (valeur.equals("P")) {
        return "PUMP dès la réception d'achat x coeff.";
      }
      else if (valeur.equals("Q")) {
        return "PUMP à la demande (GAM 561) x coeff.";
      }
      else if (valeur.equals("R")) {
        return "Prix de revient x coeff.";
      }
      else if (valeur.equals("D")) {
        return "Prix de revient réel du dernier achat x coeff.";
      }
      else if (valeur.equals("S")) {
        return "Variation du prix de revient standard sur prix de vente";
      }
      else {
        return "";
      }
    }
    else {
      return null;
    }
  }
  
  private void riSousMenu_bt6ActionPerformed() {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void riSousMenu_bt7ActionPerformed() {
    lexique.HostCursorPut(12, 50);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt8ActionPerformed() {
    lexique.HostCursorPut("CAFRS");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt9ActionPerformed() {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void riSousMenu_bt10ActionPerformed() {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void riSousMenu_bt11ActionPerformed() {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void riSousMenu_bt12ActionPerformed() {
    lexique.HostCursorPut(15, 20);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt14ActionPerformed() {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt18ActionPerformed() {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void riSousMenu_bt19ActionPerformed() {
    lexique.HostScreenSendKey(this, "F7");
  }
  
  private void CAUNAFocusLost(FocusEvent e) {
    // TODO add your code here
  }
  
  private void CAUNA_FocusLost(FocusEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
    lexique.ClosePanel();
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void bouton_artActionPerformed() {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void riBoutonDetail2ActionPerformed(ActionEvent e) {
    if (dialog_PANEL == null) {
      dialog_PANEL = new ODialog((Window) getTopLevelAncestor(), new OPTION_PANEL(this));
    }
    dialog_PANEL.affichePopupPerso();
  }
  
  private void riBoutonDetail1ActionPerformed(ActionEvent e) {
    if (dialog_PANEL2 == null) {
      dialog_PANEL2 = new ODialog((Window) getTopLevelAncestor(), new OPTION_PANEL2(this));
    }
    dialog_PANEL2.affichePopupPerso();
  }
  
  private void CAKPRFocusLost(FocusEvent e) {
    if (CAKPR.getText().trim().equals("")) {
      CAIN4.setEnabled(false);
      btCAIN4.setEnabled(false);
    }
    else {
      CAIN4.setEnabled(true);
      btCAIN4.setEnabled(true);
    }
  }
  
  private void CAIN4ActionPerformed(ActionEvent e) {
    
    if (CAIN4.isSelected()) {
      prixMini = true;
    }
    else {
      prixMini = false;
    }
    
    CAREX3.setVisible(!prixMini);
    CAREX4.setVisible(!prixMini);
    CAREX5.setVisible(!prixMini);
    CAREX6.setVisible(!prixMini);
    CAKPR.setVisible(!prixMini);
    lbCAKPR.setVisible(!prixMini);
    btCAIN4.setVisible(CAIN4.isSelected());
  }
  
  private void bouton_erreursActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1");
  }
  
  private void rbMontantPortActionPerformed(ActionEvent e) {
    try {
      CAFV1.setEnabled(rbCAFV1.isSelected());
      CAFK1.setEnabled(rbCAFV1.isSelected());
      CAFP1.setEnabled(rbCAFP1.isSelected());
      if (rbCAFV1.isSelected()) {
        CAFP1.setText("");
      }
      if (rbCAFP1.isSelected()) {
        CAFV1.setText("");
        CAFK1.setText("");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbCoeffPortActionPerformed(ActionEvent e) {
    try {
      CAFV1.setEnabled(rbCAFV1.isSelected());
      CAFK1.setEnabled(rbCAFV1.isSelected());
      CAFP1.setEnabled(rbCAFP1.isSelected());
      if (rbCAFV1.isSelected()) {
        CAFP1.setText("");
      }
      if (rbCAFP1.isSelected()) {
        CAFV1.setText("");
        CAFK1.setText("");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    navig_art = new RiMenu();
    bouton_art = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt1 = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riMenu4 = new RiMenu();
    riMenu_bt4 = new RiMenu_bt();
    riSousMenu18 = new RiSousMenu();
    riSousMenu_bt18 = new RiSousMenu_bt();
    riSousMenu19 = new RiSousMenu();
    riSousMenu_bt19 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    pnlContenu = new SNPanelContenu();
    pnlEntete = new JPanel();
    CALIB = new XRiTextField();
    CADAPX = new XRiCalendrier();
    CADAFX = new XRiCalendrier();
    lbCADAPX = new SNLabelChamp();
    lbCADAFX = new SNLabelChamp();
    lbCALIB = new SNLabelChamp();
    CAIN1 = new XRiCheckBox();
    btpCONDITIONACHAT = new JTabbedPane();
    pnlGeneral = new JPanel();
    sNLabelTitre2 = new SNLabelTitre();
    pnlConditiondufournisseur = new JPanel();
    pnlCodeFournisseur = new JPanel();
    lbCAFRS = new SNLabelChamp();
    snFournisseurPrincipal = new SNFournisseur();
    lbCAFRS2 = new SNLabelChamp();
    snFournisseurRegroupement = new SNFournisseur();
    CAIN3 = new JLabel();
    pnlDELAIFOURNISSEUR = new JPanel();
    lbCADEL = new SNLabelChamp();
    CADEL = new XRiTextField();
    lbCADELS = new SNLabelChamp();
    CADELS = new XRiTextField();
    lbWECAM1 = new SNLabelChamp();
    WECAM1 = new XRiTextField();
    lbWECAM1jour = new SNLabelUnite();
    CAIN7 = new XRiComboBox();
    lbCAIN7 = new SNLabelChamp();
    pnlReferenceFournisseur = new JPanel();
    lbCAREF = new SNLabelChamp();
    CAREF = new XRiTextField();
    lbCARFC = new SNLabelChamp();
    CARFC = new XRiTextField();
    pnlGencodRegroupement = new JPanel();
    lbCAGCD = new SNLabelChamp();
    CAGCD = new XRiTextField();
    lbCARGA = new SNLabelChamp();
    CARGA = new XRiTextField();
    pnlPays = new JPanel();
    lbCAOPA = new SNLabelChamp();
    CAOPA = new XRiTextField();
    OPALIB = new XRiTextField();
    lbDelai = new SNLabelTitre();
    pnlConditions = new SNPanelContenu();
    pnlCNAGauche = new SNPanelTitre();
    lbCAUNA = new SNLabelChamp();
    pnlUniteAchatHT = new SNPanel();
    CAUNA = new XRiTextField();
    ULBUNA = new XRiTextField();
    lbCADEV = new SNLabelChamp();
    pnlDevise = new SNPanel();
    CADEV = new XRiTextField();
    DVLIB = new XRiTextField();
    lbCAKPR = new SNLabelChamp();
    CAKPR = new XRiTextField();
    lbCAPRAX = new SNLabelChamp();
    pnlPrixAchatBrut = new SNPanel();
    CAPRAX = new XRiTextField();
    lbSLASH1 = new SNLabelUnite();
    CAUNA1 = new XRiTextField();
    CATRL = new XRiComboBox();
    pnlRemise = new SNPanel();
    CAREX1 = new XRiTextField();
    CAREX7 = new XRiTextField();
    lbPourcentage = new SNLabelUnite();
    CAREX2 = new XRiTextField();
    CAREX8 = new XRiTextField();
    lbPourcentage2 = new SNLabelUnite();
    CAREX3 = new XRiTextField();
    CAREX9 = new XRiTextField();
    lbPourcentage6 = new SNLabelUnite();
    CAREX4 = new XRiTextField();
    lbPourcentage4 = new SNLabelUnite();
    CAREX5 = new XRiTextField();
    lbPourcentage5 = new SNLabelUnite();
    CAREX6 = new XRiTextField();
    CAREX10 = new XRiTextField();
    lbPourcentage3 = new SNLabelUnite();
    lbCAFK3 = new SNLabelChamp();
    pnlTaxeOuMajoration = new SNPanel();
    CAFK3 = new XRiTextField();
    lbPourcentage7 = new SNLabelUnite();
    lbCAFV3 = new SNLabelChamp();
    CAFV3 = new XRiTextField();
    lbUPNETX = new SNLabelChamp();
    pnlPrixAchatNetHT = new SNPanel();
    UPNETX = new XRiTextField();
    lbSLASH2 = new SNLabelUnite();
    CAUNA2 = new XRiTextField();
    lbPrixen = new SNLabelChamp();
    pnlPrixEnDevise = new SNPanel();
    UPNDVX = new XRiTextField();
    DGDEV = new XRiTextField();
    lbTaux = new SNLabelChamp();
    WCHGX = new XRiTextField();
    pnlCNADroite = new SNPanelTitre();
    rbCAFV1 = new JRadioButton();
    pnlMontantPortHT = new SNPanel();
    CAFV1 = new XRiTextField();
    lbxpoids = new SNLabelUnite();
    CAFK1 = new XRiTextField();
    rbCAFP1 = new JRadioButton();
    pnlPourcentagePort = new SNPanel();
    CAFP1 = new XRiTextField();
    lbPourcentage8 = new SNLabelUnite();
    lbCAPRVX = new SNLabelChamp();
    pnlPrixRevientFournisseur = new SNPanel();
    CAPRVX = new XRiTextField();
    lbSLASH3 = new SNLabelUnite();
    CAUNA3 = new XRiTextField();
    lbCAFP2 = new SNLabelChamp();
    pnlPourcentageMajoration = new SNPanel();
    CAFP2 = new XRiTextField();
    lbPourcentage9 = new SNLabelUnite();
    lbCAKAP = new SNLabelChamp();
    CAKAP = new XRiTextField();
    lbWKAP = new SNLabelChamp();
    WKAP = new XRiTextField();
    lbWPRSA = new SNLabelChamp();
    pnlPrixRevientStandardHT = new SNPanel();
    WPRSA = new XRiTextField();
    lbSLASH4 = new SNLabelUnite();
    CAUNA4 = new XRiTextField();
    lbCAPRS0 = new SNLabelChamp();
    pnlPrixRevientStandardHTenUS = new SNPanel();
    CAPRS0 = new XRiTextField();
    CAPRS = new XRiTextField();
    lbSLASH5 = new SNLabelUnite();
    A1UNS = new XRiTextField();
    lbFRCNR = new SNLabelChamp();
    FRCNR = new XRiTextField();
    pnlMiseAJourPrixDeVente = new SNPanelTitre();
    lbCAKPV = new SNLabelChamp();
    CAKPV = new XRiTextField();
    pnlCalculPrixVenteMinimum = new SNPanel();
    CAIN4 = new XRiCheckBox();
    btCAIN4 = new SNBoutonDetail();
    lbCACPV = new SNLabelChamp();
    pnlAPartirDe = new SNPanel();
    lib_CACPV = new XRiTextField();
    btCACPV = new SNBoutonDetail();
    pnlUnitesEtQuantites = new JPanel();
    sNLabelTitre1 = new SNLabelTitre();
    pnlUnitesQuantites1 = new JPanel();
    pnlUnites = new JPanel();
    lbCAUNC = new SNLabelChamp();
    CAUNC = new XRiTextField();
    ULBUNC = new XRiTextField();
    lbULBUNC1 = new SNLabelChamp();
    CAKSC = new XRiTextField();
    lbUNITESTOCKS = new SNLabelUnite();
    OBJ_179 = new SNLabelChamp();
    CAUNA_COPIE = new XRiTextField();
    ULBUNA_COPIE = new XRiTextField();
    lbULBUNC2 = new SNLabelChamp();
    CAKACX = new XRiTextField();
    lbUNITEACHAT = new SNLabelUnite();
    pnlAvertissementQualité = new JPanel();
    lbWATTN2 = new SNLabelChamp();
    WATTN2 = new XRiTextField();
    WATTN1 = new XRiCheckBox();
    lbCAIN2 = new SNLabelChamp();
    CAIN2 = new XRiComboBox();
    pnlQuantites = new JPanel();
    lbCAQMI = new SNLabelChamp();
    CAQMI = new XRiTextField();
    WUNS1 = new XRiTextField();
    CAQEC = new XRiTextField();
    WUNS2 = new XRiTextField();
    OBJ_188 = new SNLabelChamp();
    CANUA = new XRiTextField();
    WUNS3 = new XRiTextField();
    CAIN5 = new XRiCheckBox();
    lbTITQEC = new SNLabelChamp();
    BTD = new JPopupMenu();
    OBJ_20 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    buttonGroup1 = new ButtonGroup();
    
    // ======== this ========
    setMinimumSize(new Dimension(1200, 710));
    setPreferredSize(new Dimension(1200, 710));
    setMaximumSize(new Dimension(1200, 710));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            bouton_erreurs.addActionListener(e -> bouton_erreursActionPerformed(e));
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(e -> bouton_validerActionPerformed(e));
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Historique CNA");
            bouton_retour.setToolTipText("Historique conditions");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(e -> bouton_retourActionPerformed(e));
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
          
          // ======== navig_art ========
          {
            navig_art.setName("navig_art");
            
            // ---- bouton_art ----
            bouton_art.setText("Fiche article");
            bouton_art.setToolTipText("Fiche article");
            bouton_art.setName("bouton_art");
            bouton_art.addActionListener(e -> bouton_artActionPerformed());
            navig_art.add(bouton_art);
          }
          menus_bas.add(navig_art);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt1 ----
              riMenu_bt1.setText("@V01F@");
              riMenu_bt1.setPreferredSize(new Dimension(167, 50));
              riMenu_bt1.setMinimumSize(new Dimension(167, 50));
              riMenu_bt1.setMaximumSize(new Dimension(170, 50));
              riMenu_bt1.setFont(riMenu_bt1.getFont().deriveFont(riMenu_bt1.getFont().getSize() + 2f));
              riMenu_bt1.setName("riMenu_bt1");
              riMenu_V01F.add(riMenu_bt1);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Historique d'achats");
              riSousMenu_bt6.setToolTipText("Historique d'achats");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(e -> riSousMenu_bt6ActionPerformed());
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Code bonification");
              riSousMenu_bt7.setToolTipText("Code bonification");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(e -> riSousMenu_bt7ActionPerformed());
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
            
            // ======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");
              
              // ---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Acc\u00e9s fournisseur");
              riSousMenu_bt8.setToolTipText("Acc\u00e9s fournisseur");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(e -> riSousMenu_bt8ActionPerformed());
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
            
            // ======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");
              
              // ---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Conditions quantitatives");
              riSousMenu_bt9.setToolTipText("Conditions quantitatives");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(e -> riSousMenu_bt9ActionPerformed());
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
            
            // ======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");
              
              // ---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Tarifs en colonne");
              riSousMenu_bt10.setToolTipText("Tarifs en colonne");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(e -> riSousMenu_bt10ActionPerformed());
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);
            
            // ======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");
              
              // ---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Prix n\u00e9goci\u00e9");
              riSousMenu_bt11.setToolTipText("Prix n\u00e9goci\u00e9");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(e -> riSousMenu_bt11ActionPerformed());
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);
            
            // ======== riSousMenu12 ========
            {
              riSousMenu12.setName("riSousMenu12");
              
              // ---- riSousMenu_bt12 ----
              riSousMenu_bt12.setText("Histo. coeff. approche");
              riSousMenu_bt12.setToolTipText("Historique du coefficient d'approche");
              riSousMenu_bt12.setName("riSousMenu_bt12");
              riSousMenu_bt12.addActionListener(e -> riSousMenu_bt12ActionPerformed());
              riSousMenu12.add(riSousMenu_bt12);
            }
            menus_haut.add(riSousMenu12);
            
            // ======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");
              
              // ---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);
            
            // ======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");
              
              // ---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Bloc-notes");
              riSousMenu_bt14.setToolTipText("Bloc-notes");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(e -> riSousMenu_bt14ActionPerformed());
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
            
            // ======== riMenu4 ========
            {
              riMenu4.setName("riMenu4");
              
              // ---- riMenu_bt4 ----
              riMenu_bt4.setText("Fonctions");
              riMenu_bt4.setName("riMenu_bt4");
              riMenu4.add(riMenu_bt4);
            }
            menus_haut.add(riMenu4);
            
            // ======== riSousMenu18 ========
            {
              riSousMenu18.setName("riSousMenu18");
              
              // ---- riSousMenu_bt18 ----
              riSousMenu_bt18.setText("Date d'applic. pr\u00e9c\u00e9dente");
              riSousMenu_bt18.setToolTipText("Date d'application pr\u00e9c\u00e9dente");
              riSousMenu_bt18.setName("riSousMenu_bt18");
              riSousMenu_bt18.addActionListener(e -> riSousMenu_bt18ActionPerformed());
              riSousMenu18.add(riSousMenu_bt18);
            }
            menus_haut.add(riSousMenu18);
            
            // ======== riSousMenu19 ========
            {
              riSousMenu19.setName("riSousMenu19");
              
              // ---- riSousMenu_bt19 ----
              riSousMenu_bt19.setText("Date d'applic. suivante");
              riSousMenu_bt19.setToolTipText("Date d'application suivante");
              riSousMenu_bt19.setName("riSousMenu_bt19");
              riSousMenu_bt19.addActionListener(e -> riSousMenu_bt19ActionPerformed());
              riSousMenu19.add(riSousMenu_bt19);
            }
            menus_haut.add(riSousMenu19);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(new BorderLayout());
        
        // ======== pnlContenu ========
        {
          pnlContenu.setName("pnlContenu");
          pnlContenu.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          
          // ======== pnlEntete ========
          {
            pnlEntete.setOpaque(false);
            pnlEntete.setName("pnlEntete");
            pnlEntete.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEntete.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlEntete.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEntete.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlEntete.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- CALIB ----
            CALIB.setMaximumSize(new Dimension(310, 28));
            CALIB.setMinimumSize(new Dimension(310, 28));
            CALIB.setPreferredSize(new Dimension(310, 28));
            CALIB.setName("CALIB");
            pnlEntete.add(CALIB, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- CADAPX ----
            CADAPX.setMaximumSize(new Dimension(110, 28));
            CADAPX.setMinimumSize(new Dimension(110, 28));
            CADAPX.setPreferredSize(new Dimension(110, 28));
            CADAPX.setName("CADAPX");
            pnlEntete.add(CADAPX, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- CADAFX ----
            CADAFX.setPreferredSize(new Dimension(90, 28));
            CADAFX.setMaximumSize(new Dimension(90, 28));
            CADAFX.setMinimumSize(new Dimension(90, 28));
            CADAFX.setName("CADAFX");
            pnlEntete.add(CADAFX, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbCADAPX ----
            lbCADAPX.setText("Date d'application");
            lbCADAPX.setName("lbCADAPX");
            pnlEntete.add(lbCADAPX, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbCADAFX ----
            lbCADAFX.setText("Date de fin de validit\u00e9");
            lbCADAFX.setName("lbCADAFX");
            pnlEntete.add(lbCADAFX, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbCALIB ----
            lbCALIB.setText("Libell\u00e9");
            lbCALIB.setName("lbCALIB");
            pnlEntete.add(lbCALIB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- CAIN1 ----
            CAIN1.setText("Promotion");
            CAIN1.setComponentPopupMenu(null);
            CAIN1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CAIN1.setFont(new Font("sansserif", Font.PLAIN, 14));
            CAIN1.setMinimumSize(new Dimension(100, 30));
            CAIN1.setMaximumSize(new Dimension(100, 30));
            CAIN1.setPreferredSize(new Dimension(100, 30));
            CAIN1.setName("CAIN1");
            pnlEntete.add(CAIN1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlContenu.add(pnlEntete, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== btpCONDITIONACHAT ========
          {
            btpCONDITIONACHAT.setPreferredSize(new Dimension(990, 500));
            btpCONDITIONACHAT.setMinimumSize(new Dimension(990, 500));
            btpCONDITIONACHAT.setMaximumSize(new Dimension(990, 500));
            btpCONDITIONACHAT.setFont(new Font("sansserif", Font.PLAIN, 14));
            btpCONDITIONACHAT.setName("btpCONDITIONACHAT");
            
            // ======== pnlGeneral ========
            {
              pnlGeneral.setPreferredSize(new Dimension(950, 600));
              pnlGeneral.setOpaque(false);
              pnlGeneral.setMinimumSize(new Dimension(950, 600));
              pnlGeneral.setMaximumSize(new Dimension(950, 600));
              pnlGeneral.setName("pnlGeneral");
              pnlGeneral.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlGeneral.getLayout()).columnWidths = new int[] { 0, 0 };
              ((GridBagLayout) pnlGeneral.getLayout()).rowHeights = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlGeneral.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
              ((GridBagLayout) pnlGeneral.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              
              // ---- sNLabelTitre2 ----
              sNLabelTitre2.setText("Condition du fournisseur");
              sNLabelTitre2.setName("sNLabelTitre2");
              pnlGeneral.add(sNLabelTitre2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ======== pnlConditiondufournisseur ========
              {
                pnlConditiondufournisseur.setBorder(new TitledBorder(""));
                pnlConditiondufournisseur.setOpaque(false);
                pnlConditiondufournisseur.setPreferredSize(new Dimension(800, 500));
                pnlConditiondufournisseur.setMaximumSize(new Dimension(800, 500));
                pnlConditiondufournisseur.setMinimumSize(new Dimension(800, 500));
                pnlConditiondufournisseur.setName("pnlConditiondufournisseur");
                pnlConditiondufournisseur.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlConditiondufournisseur.getLayout()).columnWidths = new int[] { 0, 0 };
                ((GridBagLayout) pnlConditiondufournisseur.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
                ((GridBagLayout) pnlConditiondufournisseur.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
                ((GridBagLayout) pnlConditiondufournisseur.getLayout()).rowWeights =
                    new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                
                // ======== pnlCodeFournisseur ========
                {
                  pnlCodeFournisseur.setMaximumSize(new Dimension(600, 100));
                  pnlCodeFournisseur.setMinimumSize(new Dimension(600, 100));
                  pnlCodeFournisseur.setPreferredSize(new Dimension(600, 100));
                  pnlCodeFournisseur.setOpaque(false);
                  pnlCodeFournisseur.setRequestFocusEnabled(false);
                  pnlCodeFournisseur.setName("pnlCodeFournisseur");
                  pnlCodeFournisseur.setLayout(new GridBagLayout());
                  ((GridBagLayout) pnlCodeFournisseur.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                  ((GridBagLayout) pnlCodeFournisseur.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
                  ((GridBagLayout) pnlCodeFournisseur.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                  ((GridBagLayout) pnlCodeFournisseur.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                  
                  // ---- lbCAFRS ----
                  lbCAFRS.setText("Fournisseur");
                  lbCAFRS.setMaximumSize(new Dimension(250, 30));
                  lbCAFRS.setMinimumSize(new Dimension(250, 30));
                  lbCAFRS.setPreferredSize(new Dimension(250, 30));
                  lbCAFRS.setName("lbCAFRS");
                  pnlCodeFournisseur.add(lbCAFRS, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                  
                  // ---- snFournisseurPrincipal ----
                  snFournisseurPrincipal.setMaximumSize(new Dimension(410, 30));
                  snFournisseurPrincipal.setMinimumSize(new Dimension(410, 30));
                  snFournisseurPrincipal.setPreferredSize(new Dimension(410, 30));
                  snFournisseurPrincipal.setName("snFournisseurPrincipal");
                  pnlCodeFournisseur.add(snFournisseurPrincipal, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                  
                  // ---- lbCAFRS2 ----
                  lbCAFRS2.setText("Fournisseur de regroupement");
                  lbCAFRS2.setMaximumSize(new Dimension(250, 30));
                  lbCAFRS2.setMinimumSize(new Dimension(250, 30));
                  lbCAFRS2.setPreferredSize(new Dimension(250, 30));
                  lbCAFRS2.setName("lbCAFRS2");
                  pnlCodeFournisseur.add(lbCAFRS2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                  
                  // ---- snFournisseurRegroupement ----
                  snFournisseurRegroupement.setName("snFournisseurRegroupement");
                  pnlCodeFournisseur.add(snFournisseurRegroupement, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                      GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                  
                  // ---- CAIN3 ----
                  CAIN3.setText("Fournisseurs pour GBA");
                  CAIN3.setFont(new Font("sansserif", Font.BOLD, 14));
                  CAIN3.setForeground(new Color(255, 51, 51));
                  CAIN3.setName("CAIN3");
                  pnlCodeFournisseur.add(CAIN3, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                }
                pnlConditiondufournisseur.add(pnlCodeFournisseur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ======== pnlDELAIFOURNISSEUR ========
                {
                  pnlDELAIFOURNISSEUR.setBorder(new TitledBorder(""));
                  pnlDELAIFOURNISSEUR.setOpaque(false);
                  pnlDELAIFOURNISSEUR.setName("pnlDELAIFOURNISSEUR");
                  pnlDELAIFOURNISSEUR.setLayout(new GridBagLayout());
                  ((GridBagLayout) pnlDELAIFOURNISSEUR.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
                  ((GridBagLayout) pnlDELAIFOURNISSEUR.getLayout()).rowHeights = new int[] { 0, 0, 0 };
                  ((GridBagLayout) pnlDELAIFOURNISSEUR.getLayout()).columnWeights =
                      new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                  ((GridBagLayout) pnlDELAIFOURNISSEUR.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                  
                  // ---- lbCADEL ----
                  lbCADEL.setText("Premier d\u00e9lai");
                  lbCADEL.setName("lbCADEL");
                  pnlDELAIFOURNISSEUR.add(lbCADEL, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                  
                  // ---- CADEL ----
                  CADEL.setComponentPopupMenu(null);
                  CADEL.setMaximumSize(new Dimension(28, 28));
                  CADEL.setMinimumSize(new Dimension(28, 28));
                  CADEL.setPreferredSize(new Dimension(28, 28));
                  CADEL.setName("CADEL");
                  pnlDELAIFOURNISSEUR.add(CADEL, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                  
                  // ---- lbCADELS ----
                  lbCADELS.setText("D\u00e9lai suppl\u00e9mentaire");
                  lbCADELS.setName("lbCADELS");
                  pnlDELAIFOURNISSEUR.add(lbCADELS, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                  
                  // ---- CADELS ----
                  CADELS.setComponentPopupMenu(null);
                  CADELS.setMaximumSize(new Dimension(28, 28));
                  CADELS.setMinimumSize(new Dimension(28, 28));
                  CADELS.setPreferredSize(new Dimension(28, 28));
                  CADELS.setName("CADELS");
                  pnlDELAIFOURNISSEUR.add(CADELS, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                  
                  // ---- lbWECAM1 ----
                  lbWECAM1.setText("Ecart moyen r\u00e9ception/livraison pr\u00e9vue");
                  lbWECAM1.setMaximumSize(new Dimension(350, 30));
                  lbWECAM1.setMinimumSize(new Dimension(350, 30));
                  lbWECAM1.setPreferredSize(new Dimension(350, 30));
                  lbWECAM1.setName("lbWECAM1");
                  pnlDELAIFOURNISSEUR.add(lbWECAM1, new GridBagConstraints(0, 1, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- WECAM1 ----
                  WECAM1.setText("@WECAM1@");
                  WECAM1.setHorizontalAlignment(SwingConstants.RIGHT);
                  WECAM1.setMaximumSize(new Dimension(36, 28));
                  WECAM1.setMinimumSize(new Dimension(36, 28));
                  WECAM1.setPreferredSize(new Dimension(36, 28));
                  WECAM1.setName("WECAM1");
                  pnlDELAIFOURNISSEUR.add(WECAM1, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- lbWECAM1jour ----
                  lbWECAM1jour.setText("jours");
                  lbWECAM1jour.setMaximumSize(new Dimension(64, 30));
                  lbWECAM1jour.setMinimumSize(new Dimension(64, 30));
                  lbWECAM1jour.setPreferredSize(new Dimension(64, 30));
                  lbWECAM1jour.setName("lbWECAM1jour");
                  pnlDELAIFOURNISSEUR.add(lbWECAM1jour, new GridBagConstraints(6, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                  
                  // ---- CAIN7 ----
                  CAIN7.setModel(new DefaultComboBoxModel<>(new String[] { "semaines", "jours" }));
                  CAIN7.setComponentPopupMenu(null);
                  CAIN7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                  CAIN7.setName("CAIN7");
                  pnlDELAIFOURNISSEUR.add(CAIN7, new GridBagConstraints(5, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                  
                  // ---- lbCAIN7 ----
                  lbCAIN7.setText("d\u00e9lais en");
                  lbCAIN7.setMaximumSize(new Dimension(100, 30));
                  lbCAIN7.setMinimumSize(new Dimension(100, 30));
                  lbCAIN7.setPreferredSize(new Dimension(100, 30));
                  lbCAIN7.setName("lbCAIN7");
                  pnlDELAIFOURNISSEUR.add(lbCAIN7, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                }
                pnlConditiondufournisseur.add(pnlDELAIFOURNISSEUR, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                
                // ======== pnlReferenceFournisseur ========
                {
                  pnlReferenceFournisseur.setMaximumSize(new Dimension(750, 80));
                  pnlReferenceFournisseur.setMinimumSize(new Dimension(750, 80));
                  pnlReferenceFournisseur.setPreferredSize(new Dimension(750, 80));
                  pnlReferenceFournisseur.setOpaque(false);
                  pnlReferenceFournisseur.setName("pnlReferenceFournisseur");
                  pnlReferenceFournisseur.setLayout(new GridBagLayout());
                  ((GridBagLayout) pnlReferenceFournisseur.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                  ((GridBagLayout) pnlReferenceFournisseur.getLayout()).rowHeights = new int[] { 0, 0, 0 };
                  ((GridBagLayout) pnlReferenceFournisseur.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                  ((GridBagLayout) pnlReferenceFournisseur.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                  
                  // ---- lbCAREF ----
                  lbCAREF.setText("R\u00e9f\u00e9rence article fournisseur");
                  lbCAREF.setMaximumSize(new Dimension(250, 30));
                  lbCAREF.setMinimumSize(new Dimension(250, 30));
                  lbCAREF.setPreferredSize(new Dimension(250, 30));
                  lbCAREF.setName("lbCAREF");
                  pnlReferenceFournisseur.add(lbCAREF, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                  
                  // ---- CAREF ----
                  CAREF.setComponentPopupMenu(null);
                  CAREF.setPreferredSize(new Dimension(210, 28));
                  CAREF.setMaximumSize(new Dimension(210, 28));
                  CAREF.setMinimumSize(new Dimension(210, 28));
                  CAREF.setName("CAREF");
                  pnlReferenceFournisseur.add(CAREF, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                  
                  // ---- lbCARFC ----
                  lbCARFC.setText("lib de CARFC");
                  lbCARFC.setMaximumSize(new Dimension(250, 30));
                  lbCARFC.setMinimumSize(new Dimension(250, 30));
                  lbCARFC.setPreferredSize(new Dimension(250, 30));
                  lbCARFC.setName("lbCARFC");
                  pnlReferenceFournisseur.add(lbCARFC, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- CARFC ----
                  CARFC.setComponentPopupMenu(null);
                  CARFC.setMaximumSize(new Dimension(210, 28));
                  CARFC.setMinimumSize(new Dimension(210, 28));
                  CARFC.setPreferredSize(new Dimension(210, 28));
                  CARFC.setName("CARFC");
                  pnlReferenceFournisseur.add(CARFC, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                }
                pnlConditiondufournisseur.add(pnlReferenceFournisseur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ======== pnlGencodRegroupement ========
                {
                  pnlGencodRegroupement.setOpaque(false);
                  pnlGencodRegroupement.setMaximumSize(new Dimension(700, 40));
                  pnlGencodRegroupement.setMinimumSize(new Dimension(700, 40));
                  pnlGencodRegroupement.setPreferredSize(new Dimension(700, 40));
                  pnlGencodRegroupement.setName("pnlGencodRegroupement");
                  pnlGencodRegroupement.setLayout(new GridBagLayout());
                  ((GridBagLayout) pnlGencodRegroupement.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
                  ((GridBagLayout) pnlGencodRegroupement.getLayout()).rowHeights = new int[] { 0, 0 };
                  ((GridBagLayout) pnlGencodRegroupement.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                  ((GridBagLayout) pnlGencodRegroupement.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                  
                  // ---- lbCAGCD ----
                  lbCAGCD.setText("Gencode article fournisseur");
                  lbCAGCD.setMaximumSize(new Dimension(250, 30));
                  lbCAGCD.setMinimumSize(new Dimension(250, 30));
                  lbCAGCD.setPreferredSize(new Dimension(250, 30));
                  lbCAGCD.setInheritsPopupMenu(false);
                  lbCAGCD.setName("lbCAGCD");
                  pnlGencodRegroupement.add(lbCAGCD, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- CAGCD ----
                  CAGCD.setToolTipText("Num\u00e9ro de code barres");
                  CAGCD.setComponentPopupMenu(null);
                  CAGCD.setMaximumSize(new Dimension(116, 28));
                  CAGCD.setMinimumSize(new Dimension(116, 28));
                  CAGCD.setPreferredSize(new Dimension(116, 28));
                  CAGCD.setName("CAGCD");
                  pnlGencodRegroupement.add(CAGCD, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- lbCARGA ----
                  lbCARGA.setText("Regroupement");
                  lbCARGA.setName("lbCARGA");
                  pnlGencodRegroupement.add(lbCARGA, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- CARGA ----
                  CARGA.setComponentPopupMenu(BTD);
                  CARGA.setToolTipText("Regroupement d'achat");
                  CARGA.setMaximumSize(new Dimension(110, 28));
                  CARGA.setMinimumSize(new Dimension(110, 28));
                  CARGA.setPreferredSize(new Dimension(110, 28));
                  CARGA.setName("CARGA");
                  pnlGencodRegroupement.add(CARGA, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                }
                pnlConditiondufournisseur.add(pnlGencodRegroupement, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ======== pnlPays ========
                {
                  pnlPays.setMaximumSize(new Dimension(700, 40));
                  pnlPays.setMinimumSize(new Dimension(700, 40));
                  pnlPays.setPreferredSize(new Dimension(700, 40));
                  pnlPays.setOpaque(false);
                  pnlPays.setName("pnlPays");
                  pnlPays.setLayout(new GridBagLayout());
                  ((GridBagLayout) pnlPays.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
                  ((GridBagLayout) pnlPays.getLayout()).rowHeights = new int[] { 0, 0 };
                  ((GridBagLayout) pnlPays.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                  ((GridBagLayout) pnlPays.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                  
                  // ---- lbCAOPA ----
                  lbCAOPA.setText("Code pays");
                  lbCAOPA.setMaximumSize(new Dimension(250, 30));
                  lbCAOPA.setMinimumSize(new Dimension(250, 30));
                  lbCAOPA.setPreferredSize(new Dimension(250, 30));
                  lbCAOPA.setName("lbCAOPA");
                  pnlPays.add(lbCAOPA, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                      new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- CAOPA ----
                  CAOPA.setComponentPopupMenu(BTD);
                  CAOPA.setMaximumSize(new Dimension(44, 28));
                  CAOPA.setMinimumSize(new Dimension(44, 28));
                  CAOPA.setPreferredSize(new Dimension(44, 28));
                  CAOPA.setName("CAOPA");
                  pnlPays.add(CAOPA, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                      new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- OPALIB ----
                  OPALIB.setText("@OPALIB@");
                  OPALIB.setMaximumSize(new Dimension(200, 28));
                  OPALIB.setMinimumSize(new Dimension(200, 28));
                  OPALIB.setPreferredSize(new Dimension(200, 28));
                  OPALIB.setName("OPALIB");
                  pnlPays.add(OPALIB, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                      new Insets(0, 0, 0, 0), 0, 0));
                }
                pnlConditiondufournisseur.add(pnlPays, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbDelai ----
                lbDelai.setText("D\u00e9lais");
                lbDelai.setName("lbDelai");
                pnlConditiondufournisseur.add(lbDelai, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              }
              pnlGeneral.add(pnlConditiondufournisseur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            btpCONDITIONACHAT.addTab("G\u00e9n\u00e9ral", pnlGeneral);
            
            // ======== pnlConditions ========
            {
              pnlConditions.setName("pnlConditions");
              pnlConditions.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlConditions.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlConditions.getLayout()).rowHeights = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlConditions.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlConditions.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              
              // ======== pnlCNAGauche ========
              {
                pnlCNAGauche.setTitre("Calcul de la condition d'achats");
                pnlCNAGauche.setName("pnlCNAGauche");
                pnlCNAGauche.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlCNAGauche.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlCNAGauche.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                ((GridBagLayout) pnlCNAGauche.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
                ((GridBagLayout) pnlCNAGauche.getLayout()).rowWeights =
                    new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                
                // ---- lbCAUNA ----
                lbCAUNA.setText("Unit\u00e9 d'achat (UA)");
                lbCAUNA.setHorizontalAlignment(SwingConstants.RIGHT);
                lbCAUNA.setMaximumSize(new Dimension(200, 30));
                lbCAUNA.setMinimumSize(new Dimension(200, 30));
                lbCAUNA.setPreferredSize(new Dimension(200, 30));
                lbCAUNA.setName("lbCAUNA");
                pnlCNAGauche.add(lbCAUNA, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                
                // ======== pnlUniteAchatHT ========
                {
                  pnlUniteAchatHT.setName("pnlUniteAchatHT");
                  pnlUniteAchatHT.setLayout(new GridBagLayout());
                  ((GridBagLayout) pnlUniteAchatHT.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                  ((GridBagLayout) pnlUniteAchatHT.getLayout()).rowHeights = new int[] { 0, 0 };
                  ((GridBagLayout) pnlUniteAchatHT.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
                  ((GridBagLayout) pnlUniteAchatHT.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                  
                  // ---- CAUNA ----
                  CAUNA.setComponentPopupMenu(BTD);
                  CAUNA.setToolTipText("unit\u00e9 d'achat");
                  CAUNA.setMaximumSize(new Dimension(60, 30));
                  CAUNA.setMinimumSize(new Dimension(60, 30));
                  CAUNA.setPreferredSize(new Dimension(60, 30));
                  CAUNA.setFont(new Font("sansserif", Font.PLAIN, 14));
                  CAUNA.setName("CAUNA");
                  CAUNA.addFocusListener(new FocusAdapter() {
                    @Override
                    public void focusLost(FocusEvent e) {
                      CAUNAFocusLost(e);
                    }
                  });
                  pnlUniteAchatHT.add(CAUNA, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- ULBUNA ----
                  ULBUNA.setText("@ULBUNA@");
                  ULBUNA.setMaximumSize(new Dimension(100, 30));
                  ULBUNA.setMinimumSize(new Dimension(100, 30));
                  ULBUNA.setPreferredSize(new Dimension(100, 30));
                  ULBUNA.setFont(new Font("sansserif", Font.PLAIN, 14));
                  ULBUNA.setEnabled(false);
                  ULBUNA.setName("ULBUNA");
                  pnlUniteAchatHT.add(ULBUNA, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                }
                pnlCNAGauche.add(pnlUniteAchatHT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbCADEV ----
                lbCADEV.setText("Devise");
                lbCADEV.setHorizontalAlignment(SwingConstants.RIGHT);
                lbCADEV.setMaximumSize(new Dimension(200, 30));
                lbCADEV.setMinimumSize(new Dimension(200, 30));
                lbCADEV.setPreferredSize(new Dimension(200, 30));
                lbCADEV.setName("lbCADEV");
                pnlCNAGauche.add(lbCADEV, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                
                // ======== pnlDevise ========
                {
                  pnlDevise.setName("pnlDevise");
                  pnlDevise.setLayout(new GridBagLayout());
                  ((GridBagLayout) pnlDevise.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                  ((GridBagLayout) pnlDevise.getLayout()).rowHeights = new int[] { 0, 0 };
                  ((GridBagLayout) pnlDevise.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
                  ((GridBagLayout) pnlDevise.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                  
                  // ---- CADEV ----
                  CADEV.setComponentPopupMenu(BTD);
                  CADEV.setMaximumSize(new Dimension(60, 30));
                  CADEV.setMinimumSize(new Dimension(60, 30));
                  CADEV.setPreferredSize(new Dimension(60, 30));
                  CADEV.setFont(new Font("sansserif", Font.PLAIN, 14));
                  CADEV.setName("CADEV");
                  pnlDevise.add(CADEV, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                      new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- DVLIB ----
                  DVLIB.setText("@DVLIB@");
                  DVLIB.setMaximumSize(new Dimension(100, 30));
                  DVLIB.setMinimumSize(new Dimension(100, 30));
                  DVLIB.setPreferredSize(new Dimension(100, 30));
                  DVLIB.setFont(new Font("sansserif", Font.PLAIN, 14));
                  DVLIB.setEnabled(false);
                  DVLIB.setName("DVLIB");
                  pnlDevise.add(DVLIB, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                      new Insets(0, 0, 0, 0), 0, 0));
                }
                pnlCNAGauche.add(pnlDevise, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbCAKPR ----
                lbCAKPR.setText("Coefficient de remise");
                lbCAKPR.setHorizontalAlignment(SwingConstants.RIGHT);
                lbCAKPR.setMaximumSize(new Dimension(200, 30));
                lbCAKPR.setMinimumSize(new Dimension(200, 30));
                lbCAKPR.setPreferredSize(new Dimension(200, 30));
                lbCAKPR.setName("lbCAKPR");
                pnlCNAGauche.add(lbCAKPR, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- CAKPR ----
                CAKPR.setComponentPopupMenu(null);
                CAKPR.setMaximumSize(new Dimension(60, 30));
                CAKPR.setMinimumSize(new Dimension(60, 30));
                CAKPR.setPreferredSize(new Dimension(60, 30));
                CAKPR.setFont(new Font("sansserif", Font.PLAIN, 14));
                CAKPR.setName("CAKPR");
                CAKPR.addFocusListener(new FocusAdapter() {
                  @Override
                  public void focusLost(FocusEvent e) {
                    CAKPRFocusLost(e);
                  }
                });
                pnlCNAGauche.add(CAKPR, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbCAPRAX ----
                lbCAPRAX.setText("Prix d'achat brut HT (UA)");
                lbCAPRAX.setImportanceMessage(EnumImportanceMessage.MOYEN);
                lbCAPRAX.setName("lbCAPRAX");
                pnlCNAGauche.add(lbCAPRAX, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ======== pnlPrixAchatBrut ========
                {
                  pnlPrixAchatBrut.setName("pnlPrixAchatBrut");
                  pnlPrixAchatBrut.setLayout(new GridBagLayout());
                  ((GridBagLayout) pnlPrixAchatBrut.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
                  ((GridBagLayout) pnlPrixAchatBrut.getLayout()).rowHeights = new int[] { 0, 0 };
                  ((GridBagLayout) pnlPrixAchatBrut.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                  ((GridBagLayout) pnlPrixAchatBrut.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                  
                  // ---- CAPRAX ----
                  CAPRAX.setComponentPopupMenu(null);
                  CAPRAX.setHorizontalAlignment(SwingConstants.RIGHT);
                  CAPRAX.setFont(new Font("sansserif", Font.PLAIN, 14));
                  CAPRAX.setMaximumSize(new Dimension(100, 30));
                  CAPRAX.setPreferredSize(new Dimension(100, 30));
                  CAPRAX.setMinimumSize(new Dimension(100, 30));
                  CAPRAX.setName("CAPRAX");
                  pnlPrixAchatBrut.add(CAPRAX, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- lbSLASH1 ----
                  lbSLASH1.setText("/");
                  lbSLASH1.setHorizontalAlignment(SwingConstants.CENTER);
                  lbSLASH1.setMaximumSize(new Dimension(10, 30));
                  lbSLASH1.setMinimumSize(new Dimension(10, 30));
                  lbSLASH1.setPreferredSize(new Dimension(10, 30));
                  lbSLASH1.setName("lbSLASH1");
                  pnlPrixAchatBrut.add(lbSLASH1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- CAUNA1 ----
                  CAUNA1.setComponentPopupMenu(BTD);
                  CAUNA1.setToolTipText("unit\u00e9 d'achat");
                  CAUNA1.setText("@CAUNA@");
                  CAUNA1.setMaximumSize(new Dimension(50, 30));
                  CAUNA1.setMinimumSize(new Dimension(50, 30));
                  CAUNA1.setPreferredSize(new Dimension(50, 30));
                  CAUNA1.setFont(new Font("sansserif", Font.BOLD, 14));
                  CAUNA1.setEnabled(false);
                  CAUNA1.setName("CAUNA1");
                  CAUNA1.addFocusListener(new FocusAdapter() {
                    @Override
                    public void focusLost(FocusEvent e) {
                      CAUNAFocusLost(e);
                    }
                  });
                  pnlPrixAchatBrut.add(CAUNA1, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                }
                pnlCNAGauche.add(pnlPrixAchatBrut, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- CATRL ----
                CATRL.setModel(new DefaultComboBoxModel<>(new String[] { "Remises en cascade", "Remises en ajout" }));
                CATRL.setMaximumSize(new Dimension(200, 30));
                CATRL.setMinimumSize(new Dimension(200, 30));
                CATRL.setPreferredSize(new Dimension(200, 30));
                CATRL.setFont(new Font("sansserif", Font.PLAIN, 14));
                CATRL.setName("CATRL");
                pnlCNAGauche.add(CATRL, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                    GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
                
                // ======== pnlRemise ========
                {
                  pnlRemise.setName("pnlRemise");
                  pnlRemise.setLayout(new GridBagLayout());
                  ((GridBagLayout) pnlRemise.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
                  ((GridBagLayout) pnlRemise.getLayout()).rowHeights = new int[] { 0, 0, 0 };
                  ((GridBagLayout) pnlRemise.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                  ((GridBagLayout) pnlRemise.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                  
                  // ---- CAREX1 ----
                  CAREX1.setComponentPopupMenu(null);
                  CAREX1.setHorizontalAlignment(SwingConstants.RIGHT);
                  CAREX1.setMaximumSize(new Dimension(60, 30));
                  CAREX1.setMinimumSize(new Dimension(60, 30));
                  CAREX1.setPreferredSize(new Dimension(60, 30));
                  CAREX1.setFont(new Font("sansserif", Font.PLAIN, 14));
                  CAREX1.setName("CAREX1");
                  pnlRemise.add(CAREX1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                      new Insets(0, 0, 5, 5), 0, 0));
                  
                  // ---- CAREX7 ----
                  CAREX7.setComponentPopupMenu(null);
                  CAREX7.setHorizontalAlignment(SwingConstants.RIGHT);
                  CAREX7.setMaximumSize(new Dimension(50, 30));
                  CAREX7.setMinimumSize(new Dimension(50, 30));
                  CAREX7.setPreferredSize(new Dimension(50, 30));
                  CAREX7.setName("CAREX7");
                  pnlRemise.add(CAREX7, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                      new Insets(0, 0, 5, 5), 0, 0));
                  
                  // ---- lbPourcentage ----
                  lbPourcentage.setText("%");
                  lbPourcentage.setMaximumSize(new Dimension(20, 30));
                  lbPourcentage.setMinimumSize(new Dimension(20, 30));
                  lbPourcentage.setPreferredSize(new Dimension(20, 30));
                  lbPourcentage.setName("lbPourcentage");
                  pnlRemise.add(lbPourcentage, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                  
                  // ---- CAREX2 ----
                  CAREX2.setComponentPopupMenu(null);
                  CAREX2.setHorizontalAlignment(SwingConstants.RIGHT);
                  CAREX2.setMaximumSize(new Dimension(60, 30));
                  CAREX2.setMinimumSize(new Dimension(60, 30));
                  CAREX2.setPreferredSize(new Dimension(60, 30));
                  CAREX2.setFont(new Font("sansserif", Font.PLAIN, 14));
                  CAREX2.setName("CAREX2");
                  pnlRemise.add(CAREX2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                      new Insets(0, 0, 5, 5), 0, 0));
                  
                  // ---- CAREX8 ----
                  CAREX8.setComponentPopupMenu(null);
                  CAREX8.setHorizontalAlignment(SwingConstants.RIGHT);
                  CAREX8.setMaximumSize(new Dimension(50, 30));
                  CAREX8.setMinimumSize(new Dimension(50, 30));
                  CAREX8.setPreferredSize(new Dimension(50, 30));
                  CAREX8.setName("CAREX8");
                  pnlRemise.add(CAREX8, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                      new Insets(0, 0, 5, 5), 0, 0));
                  
                  // ---- lbPourcentage2 ----
                  lbPourcentage2.setText("%");
                  lbPourcentage2.setMaximumSize(new Dimension(20, 30));
                  lbPourcentage2.setMinimumSize(new Dimension(20, 30));
                  lbPourcentage2.setPreferredSize(new Dimension(20, 30));
                  lbPourcentage2.setName("lbPourcentage2");
                  pnlRemise.add(lbPourcentage2, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                  
                  // ---- CAREX3 ----
                  CAREX3.setComponentPopupMenu(null);
                  CAREX3.setHorizontalAlignment(SwingConstants.RIGHT);
                  CAREX3.setMaximumSize(new Dimension(60, 30));
                  CAREX3.setMinimumSize(new Dimension(60, 30));
                  CAREX3.setPreferredSize(new Dimension(60, 30));
                  CAREX3.setFont(new Font("sansserif", Font.PLAIN, 14));
                  CAREX3.setName("CAREX3");
                  pnlRemise.add(CAREX3, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                      new Insets(0, 0, 5, 5), 0, 0));
                  
                  // ---- CAREX9 ----
                  CAREX9.setComponentPopupMenu(null);
                  CAREX9.setHorizontalAlignment(SwingConstants.RIGHT);
                  CAREX9.setMaximumSize(new Dimension(50, 30));
                  CAREX9.setMinimumSize(new Dimension(50, 30));
                  CAREX9.setPreferredSize(new Dimension(50, 30));
                  CAREX9.setName("CAREX9");
                  pnlRemise.add(CAREX9, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                      new Insets(0, 0, 5, 5), 0, 0));
                  
                  // ---- lbPourcentage6 ----
                  lbPourcentage6.setText("%");
                  lbPourcentage6.setMaximumSize(new Dimension(20, 30));
                  lbPourcentage6.setMinimumSize(new Dimension(20, 30));
                  lbPourcentage6.setPreferredSize(new Dimension(20, 30));
                  lbPourcentage6.setName("lbPourcentage6");
                  pnlRemise.add(lbPourcentage6, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                  
                  // ---- CAREX4 ----
                  CAREX4.setComponentPopupMenu(null);
                  CAREX4.setHorizontalAlignment(SwingConstants.RIGHT);
                  CAREX4.setMaximumSize(new Dimension(60, 30));
                  CAREX4.setMinimumSize(new Dimension(60, 30));
                  CAREX4.setPreferredSize(new Dimension(60, 30));
                  CAREX4.setFont(new Font("sansserif", Font.PLAIN, 14));
                  CAREX4.setName("CAREX4");
                  pnlRemise.add(CAREX4, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                      new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- lbPourcentage4 ----
                  lbPourcentage4.setText("%");
                  lbPourcentage4.setMaximumSize(new Dimension(20, 30));
                  lbPourcentage4.setMinimumSize(new Dimension(20, 30));
                  lbPourcentage4.setPreferredSize(new Dimension(20, 30));
                  lbPourcentage4.setName("lbPourcentage4");
                  pnlRemise.add(lbPourcentage4, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- CAREX5 ----
                  CAREX5.setComponentPopupMenu(null);
                  CAREX5.setHorizontalAlignment(SwingConstants.RIGHT);
                  CAREX5.setMaximumSize(new Dimension(60, 30));
                  CAREX5.setMinimumSize(new Dimension(60, 30));
                  CAREX5.setPreferredSize(new Dimension(60, 30));
                  CAREX5.setFont(new Font("sansserif", Font.PLAIN, 14));
                  CAREX5.setName("CAREX5");
                  pnlRemise.add(CAREX5, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                      new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- lbPourcentage5 ----
                  lbPourcentage5.setText("%");
                  lbPourcentage5.setMaximumSize(new Dimension(20, 30));
                  lbPourcentage5.setMinimumSize(new Dimension(20, 30));
                  lbPourcentage5.setPreferredSize(new Dimension(20, 30));
                  lbPourcentage5.setName("lbPourcentage5");
                  pnlRemise.add(lbPourcentage5, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- CAREX6 ----
                  CAREX6.setComponentPopupMenu(null);
                  CAREX6.setHorizontalAlignment(SwingConstants.RIGHT);
                  CAREX6.setMaximumSize(new Dimension(60, 30));
                  CAREX6.setMinimumSize(new Dimension(60, 30));
                  CAREX6.setPreferredSize(new Dimension(60, 30));
                  CAREX6.setFont(new Font("sansserif", Font.PLAIN, 14));
                  CAREX6.setName("CAREX6");
                  pnlRemise.add(CAREX6, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                      new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- CAREX10 ----
                  CAREX10.setComponentPopupMenu(null);
                  CAREX10.setHorizontalAlignment(SwingConstants.RIGHT);
                  CAREX10.setMaximumSize(new Dimension(50, 30));
                  CAREX10.setMinimumSize(new Dimension(50, 30));
                  CAREX10.setPreferredSize(new Dimension(50, 30));
                  CAREX10.setName("CAREX10");
                  pnlRemise.add(CAREX10, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                      new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- lbPourcentage3 ----
                  lbPourcentage3.setText("%");
                  lbPourcentage3.setMaximumSize(new Dimension(20, 30));
                  lbPourcentage3.setMinimumSize(new Dimension(20, 30));
                  lbPourcentage3.setPreferredSize(new Dimension(20, 30));
                  lbPourcentage3.setName("lbPourcentage3");
                  pnlRemise.add(lbPourcentage3, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                }
                pnlCNAGauche.add(pnlRemise, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbCAFK3 ----
                lbCAFK3.setText("Taxe ou majoration");
                lbCAFK3.setHorizontalAlignment(SwingConstants.RIGHT);
                lbCAFK3.setMaximumSize(new Dimension(200, 30));
                lbCAFK3.setMinimumSize(new Dimension(200, 30));
                lbCAFK3.setPreferredSize(new Dimension(200, 30));
                lbCAFK3.setName("lbCAFK3");
                pnlCNAGauche.add(lbCAFK3, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                
                // ======== pnlTaxeOuMajoration ========
                {
                  pnlTaxeOuMajoration.setName("pnlTaxeOuMajoration");
                  pnlTaxeOuMajoration.setLayout(new GridBagLayout());
                  ((GridBagLayout) pnlTaxeOuMajoration.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                  ((GridBagLayout) pnlTaxeOuMajoration.getLayout()).rowHeights = new int[] { 0, 0 };
                  ((GridBagLayout) pnlTaxeOuMajoration.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                  ((GridBagLayout) pnlTaxeOuMajoration.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                  
                  // ---- CAFK3 ----
                  CAFK3.setComponentPopupMenu(null);
                  CAFK3.setHorizontalAlignment(SwingConstants.RIGHT);
                  CAFK3.setMaximumSize(new Dimension(60, 30));
                  CAFK3.setMinimumSize(new Dimension(60, 30));
                  CAFK3.setPreferredSize(new Dimension(60, 30));
                  CAFK3.setFont(new Font("sansserif", Font.PLAIN, 14));
                  CAFK3.setName("CAFK3");
                  pnlTaxeOuMajoration.add(CAFK3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- lbPourcentage7 ----
                  lbPourcentage7.setText("%");
                  lbPourcentage7.setMaximumSize(new Dimension(20, 30));
                  lbPourcentage7.setMinimumSize(new Dimension(20, 30));
                  lbPourcentage7.setPreferredSize(new Dimension(20, 30));
                  lbPourcentage7.setName("lbPourcentage7");
                  pnlTaxeOuMajoration.add(lbPourcentage7, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                }
                pnlCNAGauche.add(pnlTaxeOuMajoration, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbCAFV3 ----
                lbCAFV3.setText("Montant conditionnement");
                lbCAFV3.setHorizontalAlignment(SwingConstants.RIGHT);
                lbCAFV3.setMaximumSize(new Dimension(200, 30));
                lbCAFV3.setMinimumSize(new Dimension(200, 30));
                lbCAFV3.setPreferredSize(new Dimension(200, 30));
                lbCAFV3.setName("lbCAFV3");
                pnlCNAGauche.add(lbCAFV3, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- CAFV3 ----
                CAFV3.setComponentPopupMenu(null);
                CAFV3.setHorizontalAlignment(SwingConstants.RIGHT);
                CAFV3.setMaximumSize(new Dimension(100, 30));
                CAFV3.setMinimumSize(new Dimension(100, 30));
                CAFV3.setPreferredSize(new Dimension(100, 30));
                CAFV3.setFont(new Font("sansserif", Font.PLAIN, 14));
                CAFV3.setName("CAFV3");
                pnlCNAGauche.add(CAFV3, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbUPNETX ----
                lbUPNETX.setText("Prix d'achat net HT (UA)");
                lbUPNETX.setImportanceMessage(EnumImportanceMessage.MOYEN);
                lbUPNETX.setName("lbUPNETX");
                pnlCNAGauche.add(lbUPNETX, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ======== pnlPrixAchatNetHT ========
                {
                  pnlPrixAchatNetHT.setName("pnlPrixAchatNetHT");
                  pnlPrixAchatNetHT.setLayout(new GridBagLayout());
                  ((GridBagLayout) pnlPrixAchatNetHT.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
                  ((GridBagLayout) pnlPrixAchatNetHT.getLayout()).rowHeights = new int[] { 0, 0 };
                  ((GridBagLayout) pnlPrixAchatNetHT.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                  ((GridBagLayout) pnlPrixAchatNetHT.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                  
                  // ---- UPNETX ----
                  UPNETX.setHorizontalAlignment(SwingConstants.RIGHT);
                  UPNETX.setFont(new Font("sansserif", Font.BOLD, 14));
                  UPNETX.setComponentPopupMenu(null);
                  UPNETX.setText("@UPNETX@");
                  UPNETX.setMaximumSize(new Dimension(100, 30));
                  UPNETX.setMinimumSize(new Dimension(100, 30));
                  UPNETX.setPreferredSize(new Dimension(100, 30));
                  UPNETX.setEnabled(false);
                  UPNETX.setName("UPNETX");
                  pnlPrixAchatNetHT.add(UPNETX, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- lbSLASH2 ----
                  lbSLASH2.setText("/");
                  lbSLASH2.setHorizontalAlignment(SwingConstants.CENTER);
                  lbSLASH2.setMaximumSize(new Dimension(10, 30));
                  lbSLASH2.setMinimumSize(new Dimension(10, 30));
                  lbSLASH2.setPreferredSize(new Dimension(10, 30));
                  lbSLASH2.setName("lbSLASH2");
                  pnlPrixAchatNetHT.add(lbSLASH2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- CAUNA2 ----
                  CAUNA2.setComponentPopupMenu(BTD);
                  CAUNA2.setToolTipText("unit\u00e9 d'achat");
                  CAUNA2.setText("@CAUNA@");
                  CAUNA2.setMaximumSize(new Dimension(50, 30));
                  CAUNA2.setMinimumSize(new Dimension(50, 30));
                  CAUNA2.setPreferredSize(new Dimension(50, 30));
                  CAUNA2.setFont(new Font("sansserif", Font.BOLD, 14));
                  CAUNA2.setEnabled(false);
                  CAUNA2.setName("CAUNA2");
                  CAUNA2.addFocusListener(new FocusAdapter() {
                    @Override
                    public void focusLost(FocusEvent e) {
                      CAUNAFocusLost(e);
                    }
                  });
                  pnlPrixAchatNetHT.add(CAUNA2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                }
                pnlCNAGauche.add(pnlPrixAchatNetHT, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbPrixen ----
                lbPrixen.setText("Prix d'achat net HT en devises");
                lbPrixen.setMinimumSize(new Dimension(200, 30));
                lbPrixen.setPreferredSize(new Dimension(200, 30));
                lbPrixen.setMaximumSize(new Dimension(200, 30));
                lbPrixen.setName("lbPrixen");
                pnlCNAGauche.add(lbPrixen, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ======== pnlPrixEnDevise ========
                {
                  pnlPrixEnDevise.setName("pnlPrixEnDevise");
                  pnlPrixEnDevise.setLayout(new GridBagLayout());
                  ((GridBagLayout) pnlPrixEnDevise.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                  ((GridBagLayout) pnlPrixEnDevise.getLayout()).rowHeights = new int[] { 0, 0 };
                  ((GridBagLayout) pnlPrixEnDevise.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                  ((GridBagLayout) pnlPrixEnDevise.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                  
                  // ---- UPNDVX ----
                  UPNDVX.setMaximumSize(new Dimension(100, 30));
                  UPNDVX.setMinimumSize(new Dimension(100, 30));
                  UPNDVX.setPreferredSize(new Dimension(100, 30));
                  UPNDVX.setHorizontalAlignment(SwingConstants.TRAILING);
                  UPNDVX.setFont(new Font("sansserif", Font.PLAIN, 14));
                  UPNDVX.setEnabled(false);
                  UPNDVX.setName("UPNDVX");
                  pnlPrixEnDevise.add(UPNDVX, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- DGDEV ----
                  DGDEV.setMaximumSize(new Dimension(44, 28));
                  DGDEV.setMinimumSize(new Dimension(44, 28));
                  DGDEV.setPreferredSize(new Dimension(44, 28));
                  DGDEV.setFont(new Font("sansserif", Font.PLAIN, 14));
                  DGDEV.setEnabled(false);
                  DGDEV.setName("DGDEV");
                  pnlPrixEnDevise.add(DGDEV, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                }
                pnlCNAGauche.add(pnlPrixEnDevise, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbTaux ----
                lbTaux.setText("Taux");
                lbTaux.setMaximumSize(new Dimension(200, 30));
                lbTaux.setMinimumSize(new Dimension(200, 30));
                lbTaux.setPreferredSize(new Dimension(200, 30));
                lbTaux.setName("lbTaux");
                pnlCNAGauche.add(lbTaux, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- WCHGX ----
                WCHGX.setMaximumSize(new Dimension(60, 30));
                WCHGX.setMinimumSize(new Dimension(60, 30));
                WCHGX.setPreferredSize(new Dimension(60, 30));
                WCHGX.setFont(new Font("sansserif", Font.PLAIN, 14));
                WCHGX.setName("WCHGX");
                pnlCNAGauche.add(WCHGX, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlConditions.add(pnlCNAGauche, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              
              // ======== pnlCNADroite ========
              {
                pnlCNADroite.setTitre("Suite du calcul de la condition d'achats");
                pnlCNADroite.setName("pnlCNADroite");
                pnlCNADroite.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlCNADroite.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlCNADroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                ((GridBagLayout) pnlCNADroite.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
                ((GridBagLayout) pnlCNADroite.getLayout()).rowWeights =
                    new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                
                // ---- rbCAFV1 ----
                rbCAFV1.setText("Montant port HT");
                rbCAFV1.setFont(new Font("sansserif", Font.PLAIN, 14));
                rbCAFV1.setMaximumSize(new Dimension(135, 30));
                rbCAFV1.setMinimumSize(new Dimension(135, 30));
                rbCAFV1.setPreferredSize(new Dimension(135, 30));
                rbCAFV1.setName("rbCAFV1");
                rbCAFV1.addActionListener(e -> rbMontantPortActionPerformed(e));
                pnlCNADroite.add(rbCAFV1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
                
                // ======== pnlMontantPortHT ========
                {
                  pnlMontantPortHT.setName("pnlMontantPortHT");
                  pnlMontantPortHT.setLayout(new GridBagLayout());
                  ((GridBagLayout) pnlMontantPortHT.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
                  ((GridBagLayout) pnlMontantPortHT.getLayout()).rowHeights = new int[] { 0, 0 };
                  ((GridBagLayout) pnlMontantPortHT.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                  ((GridBagLayout) pnlMontantPortHT.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                  
                  // ---- CAFV1 ----
                  CAFV1.setComponentPopupMenu(null);
                  CAFV1.setMaximumSize(new Dimension(80, 30));
                  CAFV1.setMinimumSize(new Dimension(80, 30));
                  CAFV1.setPreferredSize(new Dimension(80, 30));
                  CAFV1.setFont(new Font("sansserif", Font.PLAIN, 14));
                  CAFV1.setName("CAFV1");
                  pnlMontantPortHT.add(CAFV1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- lbxpoids ----
                  lbxpoids.setText("X poids");
                  lbxpoids.setName("lbxpoids");
                  pnlMontantPortHT.add(lbxpoids, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- CAFK1 ----
                  CAFK1.setComponentPopupMenu(null);
                  CAFK1.setMaximumSize(new Dimension(80, 30));
                  CAFK1.setMinimumSize(new Dimension(80, 30));
                  CAFK1.setPreferredSize(new Dimension(80, 30));
                  CAFK1.setFont(new Font("sansserif", Font.PLAIN, 14));
                  CAFK1.setName("CAFK1");
                  pnlMontantPortHT.add(CAFK1, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                }
                pnlCNADroite.add(pnlMontantPortHT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- rbCAFP1 ----
                rbCAFP1.setText("Pourcentage port");
                rbCAFP1.setFont(new Font("sansserif", Font.PLAIN, 14));
                rbCAFP1.setMaximumSize(new Dimension(135, 30));
                rbCAFP1.setPreferredSize(new Dimension(135, 30));
                rbCAFP1.setMinimumSize(new Dimension(135, 30));
                rbCAFP1.setName("rbCAFP1");
                rbCAFP1.addActionListener(e -> rbCoeffPortActionPerformed(e));
                pnlCNADroite.add(rbCAFP1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
                
                // ======== pnlPourcentagePort ========
                {
                  pnlPourcentagePort.setName("pnlPourcentagePort");
                  pnlPourcentagePort.setLayout(new GridBagLayout());
                  ((GridBagLayout) pnlPourcentagePort.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                  ((GridBagLayout) pnlPourcentagePort.getLayout()).rowHeights = new int[] { 0, 0 };
                  ((GridBagLayout) pnlPourcentagePort.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                  ((GridBagLayout) pnlPourcentagePort.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                  
                  // ---- CAFP1 ----
                  CAFP1.setComponentPopupMenu(null);
                  CAFP1.setMaximumSize(new Dimension(60, 30));
                  CAFP1.setMinimumSize(new Dimension(60, 30));
                  CAFP1.setPreferredSize(new Dimension(60, 30));
                  CAFP1.setFont(new Font("sansserif", Font.PLAIN, 14));
                  CAFP1.setName("CAFP1");
                  pnlPourcentagePort.add(CAFP1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- lbPourcentage8 ----
                  lbPourcentage8.setText("%");
                  lbPourcentage8.setMaximumSize(new Dimension(20, 30));
                  lbPourcentage8.setMinimumSize(new Dimension(20, 30));
                  lbPourcentage8.setPreferredSize(new Dimension(20, 30));
                  lbPourcentage8.setName("lbPourcentage8");
                  pnlPourcentagePort.add(lbPourcentage8, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                }
                pnlCNADroite.add(pnlPourcentagePort, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbCAPRVX ----
                lbCAPRVX.setText("Prix de revient fournisseur HT (UA)");
                lbCAPRVX.setImportanceMessage(EnumImportanceMessage.MOYEN);
                lbCAPRVX.setHorizontalAlignment(SwingConstants.TRAILING);
                lbCAPRVX.setName("lbCAPRVX");
                pnlCNADroite.add(lbCAPRVX, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ======== pnlPrixRevientFournisseur ========
                {
                  pnlPrixRevientFournisseur.setName("pnlPrixRevientFournisseur");
                  pnlPrixRevientFournisseur.setLayout(new GridBagLayout());
                  ((GridBagLayout) pnlPrixRevientFournisseur.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
                  ((GridBagLayout) pnlPrixRevientFournisseur.getLayout()).rowHeights = new int[] { 0, 0 };
                  ((GridBagLayout) pnlPrixRevientFournisseur.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                  ((GridBagLayout) pnlPrixRevientFournisseur.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                  
                  // ---- CAPRVX ----
                  CAPRVX.setHorizontalAlignment(SwingConstants.RIGHT);
                  CAPRVX.setFont(new Font("sansserif", Font.BOLD, 14));
                  CAPRVX.setComponentPopupMenu(null);
                  CAPRVX.setText("@CAPRVX@");
                  CAPRVX.setInheritsPopupMenu(false);
                  CAPRVX.setMaximumSize(new Dimension(100, 30));
                  CAPRVX.setMinimumSize(new Dimension(100, 30));
                  CAPRVX.setPreferredSize(new Dimension(100, 30));
                  CAPRVX.setEnabled(false);
                  CAPRVX.setName("CAPRVX");
                  pnlPrixRevientFournisseur.add(CAPRVX, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- lbSLASH3 ----
                  lbSLASH3.setText("/");
                  lbSLASH3.setHorizontalAlignment(SwingConstants.CENTER);
                  lbSLASH3.setMaximumSize(new Dimension(10, 30));
                  lbSLASH3.setMinimumSize(new Dimension(10, 30));
                  lbSLASH3.setPreferredSize(new Dimension(10, 30));
                  lbSLASH3.setName("lbSLASH3");
                  pnlPrixRevientFournisseur.add(lbSLASH3, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- CAUNA3 ----
                  CAUNA3.setComponentPopupMenu(BTD);
                  CAUNA3.setToolTipText("unit\u00e9 d'achat");
                  CAUNA3.setText("@CAUNA@");
                  CAUNA3.setMaximumSize(new Dimension(50, 30));
                  CAUNA3.setMinimumSize(new Dimension(50, 30));
                  CAUNA3.setPreferredSize(new Dimension(50, 30));
                  CAUNA3.setFont(new Font("sansserif", Font.BOLD, 14));
                  CAUNA3.setEnabled(false);
                  CAUNA3.setName("CAUNA3");
                  CAUNA3.addFocusListener(new FocusAdapter() {
                    @Override
                    public void focusLost(FocusEvent e) {
                      CAUNAFocusLost(e);
                    }
                  });
                  pnlPrixRevientFournisseur.add(CAUNA3, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                }
                pnlCNADroite.add(pnlPrixRevientFournisseur, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbCAFP2 ----
                lbCAFP2.setText("Frais d'exploitation");
                lbCAFP2.setHorizontalAlignment(SwingConstants.RIGHT);
                lbCAFP2.setMaximumSize(new Dimension(250, 30));
                lbCAFP2.setMinimumSize(new Dimension(250, 30));
                lbCAFP2.setPreferredSize(new Dimension(250, 30));
                lbCAFP2.setName("lbCAFP2");
                pnlCNADroite.add(lbCAFP2, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                
                // ======== pnlPourcentageMajoration ========
                {
                  pnlPourcentageMajoration.setName("pnlPourcentageMajoration");
                  pnlPourcentageMajoration.setLayout(new GridBagLayout());
                  ((GridBagLayout) pnlPourcentageMajoration.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                  ((GridBagLayout) pnlPourcentageMajoration.getLayout()).rowHeights = new int[] { 0, 0 };
                  ((GridBagLayout) pnlPourcentageMajoration.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                  ((GridBagLayout) pnlPourcentageMajoration.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                  
                  // ---- CAFP2 ----
                  CAFP2.setComponentPopupMenu(null);
                  CAFP2.setMaximumSize(new Dimension(60, 30));
                  CAFP2.setMinimumSize(new Dimension(60, 30));
                  CAFP2.setPreferredSize(new Dimension(60, 30));
                  CAFP2.setFont(new Font("sansserif", Font.PLAIN, 14));
                  CAFP2.setName("CAFP2");
                  pnlPourcentageMajoration.add(CAFP2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- lbPourcentage9 ----
                  lbPourcentage9.setText("%");
                  lbPourcentage9.setMaximumSize(new Dimension(20, 30));
                  lbPourcentage9.setMinimumSize(new Dimension(20, 30));
                  lbPourcentage9.setPreferredSize(new Dimension(20, 30));
                  lbPourcentage9.setName("lbPourcentage9");
                  pnlPourcentageMajoration.add(lbPourcentage9, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                }
                pnlCNADroite.add(pnlPourcentageMajoration, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbCAKAP ----
                lbCAKAP.setText("Coefficient d'approche fixe");
                lbCAKAP.setHorizontalAlignment(SwingConstants.RIGHT);
                lbCAKAP.setMaximumSize(new Dimension(250, 30));
                lbCAKAP.setMinimumSize(new Dimension(250, 30));
                lbCAKAP.setPreferredSize(new Dimension(250, 30));
                lbCAKAP.setName("lbCAKAP");
                pnlCNADroite.add(lbCAKAP, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- CAKAP ----
                CAKAP.setComponentPopupMenu(null);
                CAKAP.setMaximumSize(new Dimension(60, 30));
                CAKAP.setMinimumSize(new Dimension(60, 30));
                CAKAP.setPreferredSize(new Dimension(60, 30));
                CAKAP.setFont(new Font("sansserif", Font.PLAIN, 14));
                CAKAP.setName("CAKAP");
                pnlCNADroite.add(CAKAP, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbWKAP ----
                lbWKAP.setText("Coefficient d'approche moyen");
                lbWKAP.setName("lbWKAP");
                pnlCNADroite.add(lbWKAP, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- WKAP ----
                WKAP.setComponentPopupMenu(null);
                WKAP.setText("@WKAP@");
                WKAP.setHorizontalAlignment(SwingConstants.RIGHT);
                WKAP.setForeground(Color.darkGray);
                WKAP.setMaximumSize(new Dimension(60, 30));
                WKAP.setMinimumSize(new Dimension(60, 30));
                WKAP.setName("WKAP");
                WKAP.setPreferredSize(new Dimension(60, 30));
                WKAP.setFont(new Font("sansserif", Font.PLAIN, 14));
                WKAP.setEnabled(false);
                pnlCNADroite.add(WKAP, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbWPRSA ----
                lbWPRSA.setText("Prix de revient standard HT (UA)");
                lbWPRSA.setImportanceMessage(EnumImportanceMessage.MOYEN);
                lbWPRSA.setName("lbWPRSA");
                pnlCNADroite.add(lbWPRSA, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                
                // ======== pnlPrixRevientStandardHT ========
                {
                  pnlPrixRevientStandardHT.setName("pnlPrixRevientStandardHT");
                  pnlPrixRevientStandardHT.setLayout(new GridBagLayout());
                  ((GridBagLayout) pnlPrixRevientStandardHT.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
                  ((GridBagLayout) pnlPrixRevientStandardHT.getLayout()).rowHeights = new int[] { 0, 0 };
                  ((GridBagLayout) pnlPrixRevientStandardHT.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                  ((GridBagLayout) pnlPrixRevientStandardHT.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                  
                  // ---- WPRSA ----
                  WPRSA.setComponentPopupMenu(null);
                  WPRSA.setHorizontalAlignment(SwingConstants.RIGHT);
                  WPRSA.setFont(new Font("sansserif", Font.BOLD, 14));
                  WPRSA.setMaximumSize(new Dimension(100, 30));
                  WPRSA.setMinimumSize(new Dimension(100, 30));
                  WPRSA.setPreferredSize(new Dimension(100, 30));
                  WPRSA.setName("WPRSA");
                  pnlPrixRevientStandardHT.add(WPRSA, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- lbSLASH4 ----
                  lbSLASH4.setText("/");
                  lbSLASH4.setHorizontalAlignment(SwingConstants.CENTER);
                  lbSLASH4.setMaximumSize(new Dimension(10, 30));
                  lbSLASH4.setMinimumSize(new Dimension(10, 30));
                  lbSLASH4.setPreferredSize(new Dimension(10, 30));
                  lbSLASH4.setName("lbSLASH4");
                  pnlPrixRevientStandardHT.add(lbSLASH4, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- CAUNA4 ----
                  CAUNA4.setComponentPopupMenu(BTD);
                  CAUNA4.setToolTipText("unit\u00e9 d'achat");
                  CAUNA4.setText("@CAUNA@");
                  CAUNA4.setMaximumSize(new Dimension(50, 30));
                  CAUNA4.setMinimumSize(new Dimension(50, 30));
                  CAUNA4.setPreferredSize(new Dimension(50, 30));
                  CAUNA4.setFont(new Font("sansserif", Font.BOLD, 14));
                  CAUNA4.setEnabled(false);
                  CAUNA4.setName("CAUNA4");
                  CAUNA4.addFocusListener(new FocusAdapter() {
                    @Override
                    public void focusLost(FocusEvent e) {
                      CAUNAFocusLost(e);
                    }
                  });
                  pnlPrixRevientStandardHT.add(CAUNA4, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                }
                pnlCNADroite.add(pnlPrixRevientStandardHT, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbCAPRS0 ----
                lbCAPRS0.setText("Prix de revient standard HT (US)");
                lbCAPRS0.setFont(lbCAPRS0.getFont().deriveFont(lbCAPRS0.getFont().getStyle() & ~Font.BOLD));
                lbCAPRS0.setHorizontalAlignment(SwingConstants.RIGHT);
                lbCAPRS0.setMaximumSize(new Dimension(250, 30));
                lbCAPRS0.setMinimumSize(new Dimension(250, 30));
                lbCAPRS0.setPreferredSize(new Dimension(250, 30));
                lbCAPRS0.setName("lbCAPRS0");
                pnlCNADroite.add(lbCAPRS0, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ======== pnlPrixRevientStandardHTenUS ========
                {
                  pnlPrixRevientStandardHTenUS.setName("pnlPrixRevientStandardHTenUS");
                  pnlPrixRevientStandardHTenUS.setLayout(new GridBagLayout());
                  ((GridBagLayout) pnlPrixRevientStandardHTenUS.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
                  ((GridBagLayout) pnlPrixRevientStandardHTenUS.getLayout()).rowHeights = new int[] { 0, 0 };
                  ((GridBagLayout) pnlPrixRevientStandardHTenUS.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                  ((GridBagLayout) pnlPrixRevientStandardHTenUS.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                  
                  // ---- CAPRS0 ----
                  CAPRS0.setComponentPopupMenu(null);
                  CAPRS0.setHorizontalAlignment(SwingConstants.RIGHT);
                  CAPRS0.setMaximumSize(new Dimension(100, 30));
                  CAPRS0.setMinimumSize(new Dimension(100, 30));
                  CAPRS0.setPreferredSize(new Dimension(100, 30));
                  CAPRS0.setFont(new Font("sansserif", Font.PLAIN, 14));
                  CAPRS0.setName("CAPRS0");
                  pnlPrixRevientStandardHTenUS.add(CAPRS0, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- CAPRS ----
                  CAPRS.setComponentPopupMenu(null);
                  CAPRS.setHorizontalAlignment(SwingConstants.RIGHT);
                  CAPRS.setMaximumSize(new Dimension(100, 30));
                  CAPRS.setMinimumSize(new Dimension(100, 30));
                  CAPRS.setPreferredSize(new Dimension(100, 30));
                  CAPRS.setFont(new Font("sansserif", Font.PLAIN, 14));
                  CAPRS.setName("CAPRS");
                  pnlPrixRevientStandardHTenUS.add(CAPRS, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- lbSLASH5 ----
                  lbSLASH5.setText("/");
                  lbSLASH5.setHorizontalAlignment(SwingConstants.CENTER);
                  lbSLASH5.setMaximumSize(new Dimension(10, 30));
                  lbSLASH5.setMinimumSize(new Dimension(10, 30));
                  lbSLASH5.setPreferredSize(new Dimension(10, 30));
                  lbSLASH5.setName("lbSLASH5");
                  pnlPrixRevientStandardHTenUS.add(lbSLASH5, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- A1UNS ----
                  A1UNS.setComponentPopupMenu(BTD);
                  A1UNS.setToolTipText("unit\u00e9 d'achat");
                  A1UNS.setMaximumSize(new Dimension(50, 30));
                  A1UNS.setMinimumSize(new Dimension(50, 30));
                  A1UNS.setPreferredSize(new Dimension(50, 30));
                  A1UNS.setFont(new Font("sansserif", Font.PLAIN, 14));
                  A1UNS.setName("A1UNS");
                  A1UNS.addFocusListener(new FocusAdapter() {
                    @Override
                    public void focusLost(FocusEvent e) {
                      CAUNAFocusLost(e);
                    }
                  });
                  pnlPrixRevientStandardHTenUS.add(A1UNS, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                }
                pnlCNADroite.add(pnlPrixRevientStandardHTenUS, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbFRCNR ----
                lbFRCNR.setText("Code bonification");
                lbFRCNR.setHorizontalAlignment(SwingConstants.RIGHT);
                lbFRCNR.setMaximumSize(new Dimension(250, 30));
                lbFRCNR.setMinimumSize(new Dimension(250, 30));
                lbFRCNR.setPreferredSize(new Dimension(250, 30));
                lbFRCNR.setName("lbFRCNR");
                pnlCNADroite.add(lbFRCNR, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- FRCNR ----
                FRCNR.setComponentPopupMenu(null);
                FRCNR.setToolTipText("code bonification");
                FRCNR.setText("@FRCNR@");
                FRCNR.setMaximumSize(new Dimension(60, 30));
                FRCNR.setMinimumSize(new Dimension(60, 30));
                FRCNR.setPreferredSize(new Dimension(60, 30));
                FRCNR.setFont(new Font("sansserif", Font.PLAIN, 14));
                FRCNR.setEditable(false);
                FRCNR.setEnabled(false);
                FRCNR.setName("FRCNR");
                pnlCNADroite.add(FRCNR, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlConditions.add(pnlCNADroite, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              
              // ======== pnlMiseAJourPrixDeVente ========
              {
                pnlMiseAJourPrixDeVente.setTitre("Mise \u00e0 jour du prix de vente");
                pnlMiseAJourPrixDeVente.setName("pnlMiseAJourPrixDeVente");
                pnlMiseAJourPrixDeVente.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlMiseAJourPrixDeVente.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
                ((GridBagLayout) pnlMiseAJourPrixDeVente.getLayout()).rowHeights = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlMiseAJourPrixDeVente.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlMiseAJourPrixDeVente.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                
                // ---- lbCAKPV ----
                lbCAKPV.setText("Coefficient calcul prix de vente");
                lbCAKPV.setMaximumSize(new Dimension(200, 30));
                lbCAKPV.setMinimumSize(new Dimension(200, 30));
                lbCAKPV.setPreferredSize(new Dimension(200, 30));
                lbCAKPV.setName("lbCAKPV");
                pnlMiseAJourPrixDeVente.add(lbCAKPV, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- CAKPV ----
                CAKPV.setMinimumSize(new Dimension(50, 30));
                CAKPV.setPreferredSize(new Dimension(50, 30));
                CAKPV.setMaximumSize(new Dimension(50, 30));
                CAKPV.setFont(new Font("sansserif", Font.PLAIN, 14));
                CAKPV.setName("CAKPV");
                pnlMiseAJourPrixDeVente.add(CAKPV, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
                
                // ======== pnlCalculPrixVenteMinimum ========
                {
                  pnlCalculPrixVenteMinimum.setName("pnlCalculPrixVenteMinimum");
                  pnlCalculPrixVenteMinimum.setLayout(new GridBagLayout());
                  ((GridBagLayout) pnlCalculPrixVenteMinimum.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                  ((GridBagLayout) pnlCalculPrixVenteMinimum.getLayout()).rowHeights = new int[] { 0, 0 };
                  ((GridBagLayout) pnlCalculPrixVenteMinimum.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                  ((GridBagLayout) pnlCalculPrixVenteMinimum.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                  
                  // ---- CAIN4 ----
                  CAIN4.setText("Calcul prix de vente minimum");
                  CAIN4.setComponentPopupMenu(null);
                  CAIN4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                  CAIN4.setFont(new Font("sansserif", Font.PLAIN, 14));
                  CAIN4.setMinimumSize(new Dimension(209, 30));
                  CAIN4.setMaximumSize(new Dimension(209, 30));
                  CAIN4.setPreferredSize(new Dimension(209, 30));
                  CAIN4.setName("CAIN4");
                  CAIN4.addActionListener(e -> CAIN4ActionPerformed(e));
                  pnlCalculPrixVenteMinimum.add(CAIN4, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- btCAIN4 ----
                  btCAIN4.setName("btCAIN4");
                  btCAIN4.addActionListener(e -> riBoutonDetail1ActionPerformed(e));
                  pnlCalculPrixVenteMinimum.add(btCAIN4, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                }
                pnlMiseAJourPrixDeVente.add(pnlCalculPrixVenteMinimum, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbCACPV ----
                lbCACPV.setText("A partir de");
                lbCACPV.setName("lbCACPV");
                pnlMiseAJourPrixDeVente.add(lbCACPV, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ======== pnlAPartirDe ========
                {
                  pnlAPartirDe.setName("pnlAPartirDe");
                  pnlAPartirDe.setLayout(new GridBagLayout());
                  ((GridBagLayout) pnlAPartirDe.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                  ((GridBagLayout) pnlAPartirDe.getLayout()).rowHeights = new int[] { 0, 0 };
                  ((GridBagLayout) pnlAPartirDe.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                  ((GridBagLayout) pnlAPartirDe.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                  
                  // ---- lib_CACPV ----
                  lib_CACPV.setComponentPopupMenu(null);
                  lib_CACPV.setMaximumSize(new Dimension(400, 30));
                  lib_CACPV.setPreferredSize(new Dimension(400, 30));
                  lib_CACPV.setMinimumSize(new Dimension(400, 30));
                  lib_CACPV.setFont(new Font("sansserif", Font.PLAIN, 14));
                  lib_CACPV.setName("lib_CACPV");
                  pnlAPartirDe.add(lib_CACPV, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- btCACPV ----
                  btCACPV.setMaximumSize(new Dimension(20, 30));
                  btCACPV.setMinimumSize(new Dimension(20, 30));
                  btCACPV.setPreferredSize(new Dimension(20, 30));
                  btCACPV.setName("btCACPV");
                  btCACPV.addActionListener(e -> riBoutonDetail2ActionPerformed(e));
                  pnlAPartirDe.add(btCACPV, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                }
                pnlMiseAJourPrixDeVente.add(pnlAPartirDe, new GridBagConstraints(1, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlConditions.add(pnlMiseAJourPrixDeVente, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            btpCONDITIONACHAT.addTab("Conditions", pnlConditions);
            
            // ======== pnlUnitesEtQuantites ========
            {
              pnlUnitesEtQuantites.setOpaque(false);
              pnlUnitesEtQuantites.setName("pnlUnitesEtQuantites");
              pnlUnitesEtQuantites.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlUnitesEtQuantites.getLayout()).columnWidths = new int[] { 0, 0 };
              ((GridBagLayout) pnlUnitesEtQuantites.getLayout()).rowHeights = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlUnitesEtQuantites.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
              ((GridBagLayout) pnlUnitesEtQuantites.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              
              // ---- sNLabelTitre1 ----
              sNLabelTitre1.setText("Unit\u00e9s et quantit\u00e9s");
              sNLabelTitre1.setName("sNLabelTitre1");
              pnlUnitesEtQuantites.add(sNLabelTitre1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ======== pnlUnitesQuantites1 ========
              {
                pnlUnitesQuantites1.setBorder(new TitledBorder(""));
                pnlUnitesQuantites1.setOpaque(false);
                pnlUnitesQuantites1.setMaximumSize(new Dimension(950, 360));
                pnlUnitesQuantites1.setMinimumSize(new Dimension(950, 360));
                pnlUnitesQuantites1.setPreferredSize(new Dimension(950, 360));
                pnlUnitesQuantites1.setInheritsPopupMenu(true);
                pnlUnitesQuantites1.setRequestFocusEnabled(false);
                pnlUnitesQuantites1.setName("pnlUnitesQuantites1");
                pnlUnitesQuantites1.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlUnitesQuantites1.getLayout()).columnWidths = new int[] { 0, 0 };
                ((GridBagLayout) pnlUnitesQuantites1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
                ((GridBagLayout) pnlUnitesQuantites1.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
                ((GridBagLayout) pnlUnitesQuantites1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                
                // ======== pnlUnites ========
                {
                  pnlUnites.setMaximumSize(new Dimension(650, 80));
                  pnlUnites.setMinimumSize(new Dimension(650, 80));
                  pnlUnites.setPreferredSize(new Dimension(650, 80));
                  pnlUnites.setOpaque(false);
                  pnlUnites.setName("pnlUnites");
                  pnlUnites.setLayout(new GridBagLayout());
                  ((GridBagLayout) pnlUnites.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
                  ((GridBagLayout) pnlUnites.getLayout()).rowHeights = new int[] { 0, 0, 0 };
                  ((GridBagLayout) pnlUnites.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                  ((GridBagLayout) pnlUnites.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                  
                  // ---- lbCAUNC ----
                  lbCAUNC.setText("Unit\u00e9 de commande (UCA)");
                  lbCAUNC.setMaximumSize(new Dimension(200, 30));
                  lbCAUNC.setMinimumSize(new Dimension(200, 30));
                  lbCAUNC.setPreferredSize(new Dimension(200, 30));
                  lbCAUNC.setName("lbCAUNC");
                  pnlUnites.add(lbCAUNC, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                      new Insets(0, 0, 5, 5), 0, 0));
                  
                  // ---- CAUNC ----
                  CAUNC.setComponentPopupMenu(BTD);
                  CAUNC.setMaximumSize(new Dimension(34, 28));
                  CAUNC.setMinimumSize(new Dimension(34, 28));
                  CAUNC.setPreferredSize(new Dimension(34, 28));
                  CAUNC.setName("CAUNC");
                  pnlUnites.add(CAUNC, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                      new Insets(0, 0, 5, 5), 0, 0));
                  
                  // ---- ULBUNC ----
                  ULBUNC.setText("@ULBUNC@");
                  ULBUNC.setMaximumSize(new Dimension(100, 24));
                  ULBUNC.setMinimumSize(new Dimension(100, 24));
                  ULBUNC.setName("ULBUNC");
                  pnlUnites.add(ULBUNC, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                      new Insets(0, 0, 5, 5), 0, 0));
                  
                  // ---- lbULBUNC1 ----
                  lbULBUNC1.setText("1 @ULBUNC@  =");
                  lbULBUNC1.setHorizontalAlignment(SwingConstants.RIGHT);
                  lbULBUNC1.setName("lbULBUNC1");
                  pnlUnites.add(lbULBUNC1, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                  
                  // ---- CAKSC ----
                  CAKSC.setComponentPopupMenu(null);
                  CAKSC.setMaximumSize(new Dimension(76, 28));
                  CAKSC.setMinimumSize(new Dimension(76, 28));
                  CAKSC.setPreferredSize(new Dimension(76, 28));
                  CAKSC.setName("CAKSC");
                  pnlUnites.add(CAKSC, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                      new Insets(0, 0, 5, 5), 0, 0));
                  
                  // ---- lbUNITESTOCKS ----
                  lbUNITESTOCKS.setText("unit\u00e9s de stocks (@WUNS4@)");
                  lbUNITESTOCKS.setMaximumSize(new Dimension(200, 30));
                  lbUNITESTOCKS.setMinimumSize(new Dimension(200, 30));
                  lbUNITESTOCKS.setPreferredSize(new Dimension(200, 30));
                  lbUNITESTOCKS.setName("lbUNITESTOCKS");
                  pnlUnites.add(lbUNITESTOCKS, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                  
                  // ---- OBJ_179 ----
                  OBJ_179.setText("Unit\u00e9 d'achat (UA)");
                  OBJ_179.setMaximumSize(new Dimension(200, 30));
                  OBJ_179.setMinimumSize(new Dimension(200, 30));
                  OBJ_179.setPreferredSize(new Dimension(200, 30));
                  OBJ_179.setName("OBJ_179");
                  pnlUnites.add(OBJ_179, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                      new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- CAUNA_COPIE ----
                  CAUNA_COPIE.setComponentPopupMenu(BTD);
                  CAUNA_COPIE.setMaximumSize(new Dimension(34, 28));
                  CAUNA_COPIE.setMinimumSize(new Dimension(34, 28));
                  CAUNA_COPIE.setPreferredSize(new Dimension(34, 28));
                  CAUNA_COPIE.setName("CAUNA_COPIE");
                  CAUNA_COPIE.addFocusListener(new FocusAdapter() {
                    @Override
                    public void focusLost(FocusEvent e) {
                      CAUNA_FocusLost(e);
                    }
                  });
                  pnlUnites.add(CAUNA_COPIE, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- ULBUNA_COPIE ----
                  ULBUNA_COPIE.setText("@ULBUNA@");
                  ULBUNA_COPIE.setMaximumSize(new Dimension(100, 24));
                  ULBUNA_COPIE.setMinimumSize(new Dimension(100, 24));
                  ULBUNA_COPIE.setName("ULBUNA_COPIE");
                  pnlUnites.add(ULBUNA_COPIE, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- lbULBUNC2 ----
                  lbULBUNC2.setText("1 @ULBUNC@  =");
                  lbULBUNC2.setHorizontalAlignment(SwingConstants.RIGHT);
                  lbULBUNC2.setName("lbULBUNC2");
                  pnlUnites.add(lbULBUNC2, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- CAKACX ----
                  CAKACX.setComponentPopupMenu(null);
                  CAKACX.setHorizontalAlignment(SwingConstants.RIGHT);
                  CAKACX.setMaximumSize(new Dimension(76, 28));
                  CAKACX.setMinimumSize(new Dimension(76, 28));
                  CAKACX.setPreferredSize(new Dimension(76, 28));
                  CAKACX.setName("CAKACX");
                  pnlUnites.add(CAKACX, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                      new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- lbUNITEACHAT ----
                  lbUNITEACHAT.setText("unit\u00e9s d'achats (@WCAUNA@)");
                  lbUNITEACHAT.setMaximumSize(new Dimension(150, 30));
                  lbUNITEACHAT.setMinimumSize(new Dimension(150, 30));
                  lbUNITEACHAT.setPreferredSize(new Dimension(150, 30));
                  lbUNITEACHAT.setName("lbUNITEACHAT");
                  pnlUnites.add(lbUNITEACHAT, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                }
                pnlUnitesQuantites1.add(pnlUnites, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ======== pnlAvertissementQualité ========
                {
                  pnlAvertissementQualité.setMaximumSize(new Dimension(800, 110));
                  pnlAvertissementQualité.setMinimumSize(new Dimension(800, 110));
                  pnlAvertissementQualité.setPreferredSize(new Dimension(800, 110));
                  pnlAvertissementQualité.setOpaque(false);
                  pnlAvertissementQualité.setName("pnlAvertissementQualit\u00e9");
                  pnlAvertissementQualité.setLayout(new GridBagLayout());
                  ((GridBagLayout) pnlAvertissementQualité.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
                  ((GridBagLayout) pnlAvertissementQualité.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
                  ((GridBagLayout) pnlAvertissementQualité.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                  ((GridBagLayout) pnlAvertissementQualité.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                  
                  // ---- lbWATTN2 ----
                  lbWATTN2.setText("Avertissement");
                  lbWATTN2.setMaximumSize(new Dimension(200, 30));
                  lbWATTN2.setMinimumSize(new Dimension(200, 30));
                  lbWATTN2.setPreferredSize(new Dimension(200, 30));
                  lbWATTN2.setName("lbWATTN2");
                  pnlAvertissementQualité.add(lbWATTN2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                  
                  // ---- WATTN2 ----
                  WATTN2.setComponentPopupMenu(null);
                  WATTN2.setMaximumSize(new Dimension(600, 28));
                  WATTN2.setMinimumSize(new Dimension(600, 28));
                  WATTN2.setPreferredSize(new Dimension(600, 28));
                  WATTN2.setName("WATTN2");
                  pnlAvertissementQualité.add(WATTN2, new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                  
                  // ---- WATTN1 ----
                  WATTN1.setText("Afficher l'avertissement dans la saisie des bons d'achat");
                  WATTN1.setComponentPopupMenu(null);
                  WATTN1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                  WATTN1.setFont(new Font("sansserif", Font.PLAIN, 14));
                  WATTN1.setName("WATTN1");
                  pnlAvertissementQualité.add(WATTN1, new GridBagConstraints(1, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                  
                  // ---- lbCAIN2 ----
                  lbCAIN2.setText("Qualit\u00e9");
                  lbCAIN2.setMaximumSize(new Dimension(200, 30));
                  lbCAIN2.setMinimumSize(new Dimension(200, 30));
                  lbCAIN2.setPreferredSize(new Dimension(200, 30));
                  lbCAIN2.setName("lbCAIN2");
                  pnlAvertissementQualité.add(lbCAIN2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- CAIN2 ----
                  CAIN2.setComponentPopupMenu(null);
                  CAIN2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                  CAIN2.setModel(new DefaultComboBoxModel<>(new String[] { " ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" }));
                  CAIN2.setMaximumSize(new Dimension(44, 30));
                  CAIN2.setMinimumSize(new Dimension(44, 30));
                  CAIN2.setPreferredSize(new Dimension(44, 30));
                  CAIN2.setLightWeightPopupEnabled(false);
                  CAIN2.setName("CAIN2");
                  pnlAvertissementQualité.add(CAIN2, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                      GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
                }
                pnlUnitesQuantites1.add(pnlAvertissementQualité, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ======== pnlQuantites ========
                {
                  pnlQuantites.setMaximumSize(new Dimension(800, 100));
                  pnlQuantites.setMinimumSize(new Dimension(800, 100));
                  pnlQuantites.setPreferredSize(new Dimension(800, 100));
                  pnlQuantites.setOpaque(false);
                  pnlQuantites.setName("pnlQuantites");
                  pnlQuantites.setLayout(new GridBagLayout());
                  ((GridBagLayout) pnlQuantites.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
                  ((GridBagLayout) pnlQuantites.getLayout()).rowHeights = new int[] { 0, 0, 0 };
                  ((GridBagLayout) pnlQuantites.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                  ((GridBagLayout) pnlQuantites.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                  
                  // ---- lbCAQMI ----
                  lbCAQMI.setText("Quantit\u00e9 minimale");
                  lbCAQMI.setToolTipText("unit\u00e9 de stock");
                  lbCAQMI.setMaximumSize(new Dimension(200, 30));
                  lbCAQMI.setMinimumSize(new Dimension(200, 30));
                  lbCAQMI.setPreferredSize(new Dimension(200, 30));
                  lbCAQMI.setName("lbCAQMI");
                  pnlQuantites.add(lbCAQMI, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                  
                  // ---- CAQMI ----
                  CAQMI.setComponentPopupMenu(null);
                  CAQMI.setToolTipText("unit\u00e9 de stock");
                  CAQMI.setMaximumSize(new Dimension(92, 28));
                  CAQMI.setMinimumSize(new Dimension(92, 28));
                  CAQMI.setPreferredSize(new Dimension(92, 28));
                  CAQMI.setName("CAQMI");
                  pnlQuantites.add(CAQMI, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                      new Insets(0, 0, 5, 5), 0, 0));
                  
                  // ---- WUNS1 ----
                  WUNS1.setComponentPopupMenu(BTD);
                  WUNS1.setToolTipText("unit\u00e9 de stock");
                  WUNS1.setText("@WUNS1@");
                  WUNS1.setMaximumSize(new Dimension(34, 28));
                  WUNS1.setMinimumSize(new Dimension(34, 28));
                  WUNS1.setPreferredSize(new Dimension(34, 28));
                  WUNS1.setName("WUNS1");
                  WUNS1.addFocusListener(new FocusAdapter() {
                    @Override
                    public void focusLost(FocusEvent e) {
                      CAUNAFocusLost(e);
                    }
                  });
                  pnlQuantites.add(WUNS1, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                      new Insets(0, 0, 5, 5), 0, 0));
                  
                  // ---- CAQEC ----
                  CAQEC.setComponentPopupMenu(null);
                  CAQEC.setToolTipText("unit\u00e9 de stock");
                  CAQEC.setMaximumSize(new Dimension(92, 28));
                  CAQEC.setMinimumSize(new Dimension(92, 28));
                  CAQEC.setPreferredSize(new Dimension(92, 28));
                  CAQEC.setName("CAQEC");
                  pnlQuantites.add(CAQEC, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                      new Insets(0, 0, 5, 5), 0, 0));
                  
                  // ---- WUNS2 ----
                  WUNS2.setComponentPopupMenu(BTD);
                  WUNS2.setToolTipText("unit\u00e9 de stock");
                  WUNS2.setText("@WUNS2@");
                  WUNS2.setMaximumSize(new Dimension(34, 28));
                  WUNS2.setMinimumSize(new Dimension(34, 28));
                  WUNS2.setPreferredSize(new Dimension(34, 28));
                  WUNS2.setName("WUNS2");
                  WUNS2.addFocusListener(new FocusAdapter() {
                    @Override
                    public void focusLost(FocusEvent e) {
                      CAUNAFocusLost(e);
                    }
                  });
                  pnlQuantites.add(WUNS2, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                      GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
                  
                  // ---- OBJ_188 ----
                  OBJ_188.setText("Conditionnement");
                  OBJ_188.setHorizontalAlignment(SwingConstants.RIGHT);
                  OBJ_188.setToolTipText("unit\u00e9 de stock");
                  OBJ_188.setMaximumSize(new Dimension(200, 30));
                  OBJ_188.setMinimumSize(new Dimension(200, 30));
                  OBJ_188.setPreferredSize(new Dimension(200, 30));
                  OBJ_188.setName("OBJ_188");
                  pnlQuantites.add(OBJ_188, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- CANUA ----
                  CANUA.setComponentPopupMenu(BTD);
                  CANUA.setToolTipText("unit\u00e9 de stock");
                  CANUA.setMaximumSize(new Dimension(92, 28));
                  CANUA.setMinimumSize(new Dimension(92, 28));
                  CANUA.setPreferredSize(new Dimension(92, 28));
                  CANUA.setName("CANUA");
                  pnlQuantites.add(CANUA, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                      new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- WUNS3 ----
                  WUNS3.setComponentPopupMenu(BTD);
                  WUNS3.setToolTipText("unit\u00e9 de stock");
                  WUNS3.setText("@WUNS3@");
                  WUNS3.setMaximumSize(new Dimension(34, 28));
                  WUNS3.setMinimumSize(new Dimension(34, 28));
                  WUNS3.setPreferredSize(new Dimension(34, 28));
                  WUNS3.setName("WUNS3");
                  WUNS3.addFocusListener(new FocusAdapter() {
                    @Override
                    public void focusLost(FocusEvent e) {
                      CAUNAFocusLost(e);
                    }
                  });
                  pnlQuantites.add(WUNS3, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                      new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ---- CAIN5 ----
                  CAIN5.setText("Alerte si quantit\u00e9 achet\u00e9e non multiple du conditionnement");
                  CAIN5.setComponentPopupMenu(null);
                  CAIN5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                  CAIN5.setMaximumSize(new Dimension(400, 28));
                  CAIN5.setMinimumSize(new Dimension(400, 28));
                  CAIN5.setPreferredSize(new Dimension(400, 28));
                  CAIN5.setFont(new Font("sansserif", Font.PLAIN, 14));
                  CAIN5.setName("CAIN5");
                  pnlQuantites.add(CAIN5, new GridBagConstraints(3, 1, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                      new Insets(0, 0, 0, 0), 0, 0));
                  
                  // ---- lbTITQEC ----
                  lbTITQEC.setText("@TITQEC@");
                  lbTITQEC.setToolTipText("unit\u00e9 de stock");
                  lbTITQEC.setName("lbCAQMI");
                  pnlQuantites.add(lbTITQEC, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                }
                pnlUnitesQuantites1.add(pnlQuantites, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlUnitesEtQuantites.add(pnlUnitesQuantites1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            btpCONDITIONACHAT.addTab("Unit\u00e9s et quantit\u00e9s", pnlUnitesEtQuantites);
          }
          pnlContenu.add(btpCONDITIONACHAT, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        p_contenu.add(pnlContenu, BorderLayout.CENTER);
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_20 ----
      OBJ_20.setText("Choix possibles");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(e -> OBJ_20ActionPerformed(e));
      BTD.add(OBJ_20);
      
      // ---- OBJ_19 ----
      OBJ_19.setText("Aide en ligne");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(e -> OBJ_19ActionPerformed(e));
      BTD.add(OBJ_19);
    }
    
    // ---- buttonGroup1 ----
    buttonGroup1.add(rbCAFV1);
    buttonGroup1.add(rbCAFP1);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private RiMenu navig_art;
  private RiMenu_bt bouton_art;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt1;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiMenu riMenu4;
  private RiMenu_bt riMenu_bt4;
  private RiSousMenu riSousMenu18;
  private RiSousMenu_bt riSousMenu_bt18;
  private RiSousMenu riSousMenu19;
  private RiSousMenu_bt riSousMenu_bt19;
  private JPanel p_contenu;
  private SNPanelContenu pnlContenu;
  private JPanel pnlEntete;
  private XRiTextField CALIB;
  private XRiCalendrier CADAPX;
  private XRiCalendrier CADAFX;
  private SNLabelChamp lbCADAPX;
  private SNLabelChamp lbCADAFX;
  private SNLabelChamp lbCALIB;
  private XRiCheckBox CAIN1;
  private JTabbedPane btpCONDITIONACHAT;
  private JPanel pnlGeneral;
  private SNLabelTitre sNLabelTitre2;
  private JPanel pnlConditiondufournisseur;
  private JPanel pnlCodeFournisseur;
  private SNLabelChamp lbCAFRS;
  private SNFournisseur snFournisseurPrincipal;
  private SNLabelChamp lbCAFRS2;
  private SNFournisseur snFournisseurRegroupement;
  private JLabel CAIN3;
  private JPanel pnlDELAIFOURNISSEUR;
  private SNLabelChamp lbCADEL;
  private XRiTextField CADEL;
  private SNLabelChamp lbCADELS;
  private XRiTextField CADELS;
  private SNLabelChamp lbWECAM1;
  private XRiTextField WECAM1;
  private SNLabelUnite lbWECAM1jour;
  private XRiComboBox CAIN7;
  private SNLabelChamp lbCAIN7;
  private JPanel pnlReferenceFournisseur;
  private SNLabelChamp lbCAREF;
  private XRiTextField CAREF;
  private SNLabelChamp lbCARFC;
  private XRiTextField CARFC;
  private JPanel pnlGencodRegroupement;
  private SNLabelChamp lbCAGCD;
  private XRiTextField CAGCD;
  private SNLabelChamp lbCARGA;
  private XRiTextField CARGA;
  private JPanel pnlPays;
  private SNLabelChamp lbCAOPA;
  private XRiTextField CAOPA;
  private XRiTextField OPALIB;
  private SNLabelTitre lbDelai;
  private SNPanelContenu pnlConditions;
  private SNPanelTitre pnlCNAGauche;
  private SNLabelChamp lbCAUNA;
  private SNPanel pnlUniteAchatHT;
  private XRiTextField CAUNA;
  private XRiTextField ULBUNA;
  private SNLabelChamp lbCADEV;
  private SNPanel pnlDevise;
  private XRiTextField CADEV;
  private XRiTextField DVLIB;
  private SNLabelChamp lbCAKPR;
  private XRiTextField CAKPR;
  private SNLabelChamp lbCAPRAX;
  private SNPanel pnlPrixAchatBrut;
  private XRiTextField CAPRAX;
  private SNLabelUnite lbSLASH1;
  private XRiTextField CAUNA1;
  private XRiComboBox CATRL;
  private SNPanel pnlRemise;
  private XRiTextField CAREX1;
  private XRiTextField CAREX7;
  private SNLabelUnite lbPourcentage;
  private XRiTextField CAREX2;
  private XRiTextField CAREX8;
  private SNLabelUnite lbPourcentage2;
  private XRiTextField CAREX3;
  private XRiTextField CAREX9;
  private SNLabelUnite lbPourcentage6;
  private XRiTextField CAREX4;
  private SNLabelUnite lbPourcentage4;
  private XRiTextField CAREX5;
  private SNLabelUnite lbPourcentage5;
  private XRiTextField CAREX6;
  private XRiTextField CAREX10;
  private SNLabelUnite lbPourcentage3;
  private SNLabelChamp lbCAFK3;
  private SNPanel pnlTaxeOuMajoration;
  private XRiTextField CAFK3;
  private SNLabelUnite lbPourcentage7;
  private SNLabelChamp lbCAFV3;
  private XRiTextField CAFV3;
  private SNLabelChamp lbUPNETX;
  private SNPanel pnlPrixAchatNetHT;
  private XRiTextField UPNETX;
  private SNLabelUnite lbSLASH2;
  private XRiTextField CAUNA2;
  private SNLabelChamp lbPrixen;
  private SNPanel pnlPrixEnDevise;
  private XRiTextField UPNDVX;
  private XRiTextField DGDEV;
  private SNLabelChamp lbTaux;
  private XRiTextField WCHGX;
  private SNPanelTitre pnlCNADroite;
  private JRadioButton rbCAFV1;
  private SNPanel pnlMontantPortHT;
  private XRiTextField CAFV1;
  private SNLabelUnite lbxpoids;
  private XRiTextField CAFK1;
  private JRadioButton rbCAFP1;
  private SNPanel pnlPourcentagePort;
  private XRiTextField CAFP1;
  private SNLabelUnite lbPourcentage8;
  private SNLabelChamp lbCAPRVX;
  private SNPanel pnlPrixRevientFournisseur;
  private XRiTextField CAPRVX;
  private SNLabelUnite lbSLASH3;
  private XRiTextField CAUNA3;
  private SNLabelChamp lbCAFP2;
  private SNPanel pnlPourcentageMajoration;
  private XRiTextField CAFP2;
  private SNLabelUnite lbPourcentage9;
  private SNLabelChamp lbCAKAP;
  private XRiTextField CAKAP;
  private SNLabelChamp lbWKAP;
  private XRiTextField WKAP;
  private SNLabelChamp lbWPRSA;
  private SNPanel pnlPrixRevientStandardHT;
  private XRiTextField WPRSA;
  private SNLabelUnite lbSLASH4;
  private XRiTextField CAUNA4;
  private SNLabelChamp lbCAPRS0;
  private SNPanel pnlPrixRevientStandardHTenUS;
  private XRiTextField CAPRS0;
  private XRiTextField CAPRS;
  private SNLabelUnite lbSLASH5;
  private XRiTextField A1UNS;
  private SNLabelChamp lbFRCNR;
  private XRiTextField FRCNR;
  private SNPanelTitre pnlMiseAJourPrixDeVente;
  private SNLabelChamp lbCAKPV;
  private XRiTextField CAKPV;
  private SNPanel pnlCalculPrixVenteMinimum;
  private XRiCheckBox CAIN4;
  private SNBoutonDetail btCAIN4;
  private SNLabelChamp lbCACPV;
  private SNPanel pnlAPartirDe;
  private XRiTextField lib_CACPV;
  private SNBoutonDetail btCACPV;
  private JPanel pnlUnitesEtQuantites;
  private SNLabelTitre sNLabelTitre1;
  private JPanel pnlUnitesQuantites1;
  private JPanel pnlUnites;
  private SNLabelChamp lbCAUNC;
  private XRiTextField CAUNC;
  private XRiTextField ULBUNC;
  private SNLabelChamp lbULBUNC1;
  private XRiTextField CAKSC;
  private SNLabelUnite lbUNITESTOCKS;
  private SNLabelChamp OBJ_179;
  private XRiTextField CAUNA_COPIE;
  private XRiTextField ULBUNA_COPIE;
  private SNLabelChamp lbULBUNC2;
  private XRiTextField CAKACX;
  private SNLabelUnite lbUNITEACHAT;
  private JPanel pnlAvertissementQualité;
  private SNLabelChamp lbWATTN2;
  private XRiTextField WATTN2;
  private XRiCheckBox WATTN1;
  private SNLabelChamp lbCAIN2;
  private XRiComboBox CAIN2;
  private JPanel pnlQuantites;
  private SNLabelChamp lbCAQMI;
  private XRiTextField CAQMI;
  private XRiTextField WUNS1;
  private XRiTextField CAQEC;
  private XRiTextField WUNS2;
  private SNLabelChamp OBJ_188;
  private XRiTextField CANUA;
  private XRiTextField WUNS3;
  private XRiCheckBox CAIN5;
  private SNLabelChamp lbTITQEC;
  private JPopupMenu BTD;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_19;
  private ButtonGroup buttonGroup1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
