
package ri.serien.libecranrpg.vgam.VGAM3EFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;

/**
 * @author Stéphane Vénéri
 */
public class VGAM3EFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGAM3EFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    CIMES1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CIMES1@")).trim());
    CIMES2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CIMES2@")).trim());
    CIMES3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CIMES3@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    CIMES1 = new RiZoneSortie();
    CIMES2 = new RiZoneSortie();
    CIMES3 = new RiZoneSortie();

    //======== this ========
    setMinimumSize(new Dimension(840, 175));
    setPreferredSize(new Dimension(840, 175));
    setMaximumSize(new Dimension(840, 175));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder("D\u00e9tail de l'erreur"));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(new GridBagLayout());
          ((GridBagLayout)p_recup.getLayout()).columnWidths = new int[] {616, 0};
          ((GridBagLayout)p_recup.getLayout()).rowHeights = new int[] {35, 35, 30, 0};
          ((GridBagLayout)p_recup.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
          ((GridBagLayout)p_recup.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

          //---- CIMES1 ----
          CIMES1.setText("@CIMES1@");
          CIMES1.setMaximumSize(new Dimension(610, 28));
          CIMES1.setMinimumSize(new Dimension(610, 28));
          CIMES1.setPreferredSize(new Dimension(610, 28));
          CIMES1.setName("CIMES1");
          p_recup.add(CIMES1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.NONE,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- CIMES2 ----
          CIMES2.setText("@CIMES2@");
          CIMES2.setMaximumSize(new Dimension(610, 28));
          CIMES2.setMinimumSize(new Dimension(610, 28));
          CIMES2.setPreferredSize(new Dimension(610, 28));
          CIMES2.setName("CIMES2");
          p_recup.add(CIMES2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.NONE,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- CIMES3 ----
          CIMES3.setText("@CIMES3@");
          CIMES3.setMaximumSize(new Dimension(610, 28));
          CIMES3.setMinimumSize(new Dimension(610, 28));
          CIMES3.setPreferredSize(new Dimension(610, 28));
          CIMES3.setName("CIMES3");
          p_recup.add(CIMES3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.NONE,
            new Insets(0, 0, 0, 0), 0, 0));
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(p_recup, GroupLayout.DEFAULT_SIZE, 646, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(p_recup, GroupLayout.DEFAULT_SIZE, 149, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_recup;
  private RiZoneSortie CIMES1;
  private RiZoneSortie CIMES2;
  private RiZoneSortie CIMES3;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
