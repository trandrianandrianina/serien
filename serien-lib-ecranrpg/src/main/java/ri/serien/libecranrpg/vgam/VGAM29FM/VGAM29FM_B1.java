
package ri.serien.libecranrpg.vgam.VGAM29FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGAM29FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VGAM29FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_AR01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AR01@")).trim());
    OBJ_AL01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AL01@")).trim());
    OBJ_AR02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AR02@")).trim());
    OBJ_AL02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AL02@")).trim());
    OBJ_AR03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AR03@")).trim());
    OBJ_AL03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AL03@")).trim());
    OBJ_AR04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AR04@")).trim());
    OBJ_AL04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AL04@")).trim());
    OBJ_AR05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AR05@")).trim());
    OBJ_AL05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AL05@")).trim());
    OBJ_AR06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AR06@")).trim());
    OBJ_AL06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AL06@")).trim());
    OBJ_AR07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AR07@")).trim());
    OBJ_AL07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AL07@")).trim());
    OBJ_AR08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AR08@")).trim());
    OBJ_AL08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AL08@")).trim());
    OBJ_AR09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AR09@")).trim());
    OBJ_AL09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AL09@")).trim());
    OBJ_AR10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AR10@")).trim());
    OBJ_AL10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AL10@")).trim());
    OBJ_AR11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AR11@")).trim());
    OBJ_AL11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AL11@")).trim());
    OBJ_AR12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AR12@")).trim());
    OBJ_AL12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AL12@")).trim());
    OBJ_AR13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AR13@")).trim());
    OBJ_AL13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AL13@")).trim());
    OBJ_AR14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AR14@")).trim());
    OBJ_AL14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AL14@")).trim());
    OBJ_AR15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AR15@")).trim());
    OBJ_AL15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AL15@")).trim());
    OBJ_AR2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOTAL@")).trim());
    OBJ_ARART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ARART@")).trim());
    OBJ_ARARTL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ALARTL@")).trim());
    OBJ_ARMAG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ARMAG@")).trim());
    OBJ_ARART2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ARMAGL@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("29");
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Grille de répartition"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    try {
      lexique.HostScreenSendKey(this, "F7");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    try {
      lexique.HostScreenSendKey(this, "F8");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    panel3 = new JPanel();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_57 = new JLabel();
    OBJ_58 = new JLabel();
    OBJ_59 = new JLabel();
    OBJ_AR01 = new JLabel();
    OBJ_AL01 = new JLabel();
    PR01 = new XRiTextField();
    OBJ_AR02 = new JLabel();
    OBJ_AL02 = new JLabel();
    PR02 = new XRiTextField();
    OBJ_AR03 = new JLabel();
    OBJ_AL03 = new JLabel();
    PR03 = new XRiTextField();
    OBJ_AR04 = new JLabel();
    OBJ_AL04 = new JLabel();
    PR04 = new XRiTextField();
    OBJ_AR05 = new JLabel();
    OBJ_AL05 = new JLabel();
    PR05 = new XRiTextField();
    OBJ_AR06 = new JLabel();
    OBJ_AL06 = new JLabel();
    PR06 = new XRiTextField();
    OBJ_AR07 = new JLabel();
    OBJ_AL07 = new JLabel();
    PR07 = new XRiTextField();
    OBJ_AR08 = new JLabel();
    OBJ_AL08 = new JLabel();
    PR08 = new XRiTextField();
    OBJ_AR09 = new JLabel();
    OBJ_AL09 = new JLabel();
    PR09 = new XRiTextField();
    OBJ_AR10 = new JLabel();
    OBJ_AL10 = new JLabel();
    PR10 = new XRiTextField();
    OBJ_AR11 = new JLabel();
    OBJ_AL11 = new JLabel();
    PR11 = new XRiTextField();
    OBJ_AR12 = new JLabel();
    OBJ_AL12 = new JLabel();
    PR12 = new XRiTextField();
    OBJ_AR13 = new JLabel();
    OBJ_AL13 = new JLabel();
    PR13 = new XRiTextField();
    OBJ_AR14 = new JLabel();
    OBJ_AL14 = new JLabel();
    PR14 = new XRiTextField();
    OBJ_AR15 = new JLabel();
    OBJ_AL15 = new JLabel();
    PR15 = new XRiTextField();
    OBJ_AR2 = new RiZoneSortie();
    OBJ_LibTotal = new JLabel();
    OBJ_Referent = new JLabel();
    OBJ_LibMagasin = new JLabel();
    OBJ_ARART = new RiZoneSortie();
    OBJ_ARARTL = new RiZoneSortie();
    OBJ_ARMAG = new RiZoneSortie();
    OBJ_ARART2 = new RiZoneSortie();

    //======== this ========
    setMinimumSize(new Dimension(890, 580));
    setPreferredSize(new Dimension(890, 580));
    setMaximumSize(new Dimension(890, 580));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 260));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Exportation tableur");
              riSousMenu_bt6.setToolTipText("Exportation tableur");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Importation tableur");
              riSousMenu_bt7.setToolTipText("Importation tableur");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== panel3 ========
      {
        panel3.setBackground(new Color(238, 238, 210));
        panel3.setName("panel3");
        panel3.setLayout(new GridBagLayout());
        ((GridBagLayout)panel3.getLayout()).columnWidths = new int[] {700, 0};
        ((GridBagLayout)panel3.getLayout()).rowHeights = new int[] {573, 0};
        ((GridBagLayout)panel3.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
        ((GridBagLayout)panel3.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //======== p_contenu ========
        {
          p_contenu.setBackground(new Color(238, 238, 210));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(new GridBagLayout());
          ((GridBagLayout)p_contenu.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 277, 0, 0};
          ((GridBagLayout)p_contenu.getLayout()).rowHeights = new int[] {37, 37, 0, 0};
          ((GridBagLayout)p_contenu.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)p_contenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(new GridBagLayout());
            ((GridBagLayout)panel1.getLayout()).columnWidths = new int[] {0, 361, 97, 0};
            ((GridBagLayout)panel1.getLayout()).rowHeights = new int[] {0, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 25, 0};
            ((GridBagLayout)panel1.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel1.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

            //---- OBJ_57 ----
            OBJ_57.setText("Code article fournisseur");
            OBJ_57.setFont(OBJ_57.getFont().deriveFont(OBJ_57.getFont().getStyle() | Font.BOLD));
            OBJ_57.setName("OBJ_57");
            panel1.add(OBJ_57, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- OBJ_58 ----
            OBJ_58.setText("Libell\u00e9 article");
            OBJ_58.setFont(OBJ_58.getFont().deriveFont(OBJ_58.getFont().getStyle() | Font.BOLD));
            OBJ_58.setName("OBJ_58");
            panel1.add(OBJ_58, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- OBJ_59 ----
            OBJ_59.setText("% R\u00e9partition");
            OBJ_59.setFont(OBJ_59.getFont().deriveFont(OBJ_59.getFont().getStyle() | Font.BOLD));
            OBJ_59.setName("OBJ_59");
            panel1.add(OBJ_59, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 2, 0), 0, 0));

            //---- OBJ_AR01 ----
            OBJ_AR01.setText("@AR01@");
            OBJ_AR01.setMaximumSize(new Dimension(210, 20));
            OBJ_AR01.setInheritsPopupMenu(false);
            OBJ_AR01.setMinimumSize(new Dimension(210, 20));
            OBJ_AR01.setPreferredSize(new Dimension(210, 20));
            OBJ_AR01.setName("OBJ_AR01");
            panel1.add(OBJ_AR01, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- OBJ_AL01 ----
            OBJ_AL01.setText("@AL01@");
            OBJ_AL01.setMaximumSize(new Dimension(310, 20));
            OBJ_AL01.setInheritsPopupMenu(false);
            OBJ_AL01.setMinimumSize(new Dimension(310, 20));
            OBJ_AL01.setPreferredSize(new Dimension(310, 20));
            OBJ_AL01.setName("OBJ_AL01");
            panel1.add(OBJ_AL01, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- PR01 ----
            PR01.setMaximumSize(new Dimension(52, 28));
            PR01.setMinimumSize(new Dimension(52, 28));
            PR01.setPreferredSize(new Dimension(52, 28));
            PR01.setName("PR01");
            panel1.add(PR01, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 0), 0, 0));

            //---- OBJ_AR02 ----
            OBJ_AR02.setText("@AR02@");
            OBJ_AR02.setMaximumSize(new Dimension(210, 20));
            OBJ_AR02.setInheritsPopupMenu(false);
            OBJ_AR02.setMinimumSize(new Dimension(210, 20));
            OBJ_AR02.setPreferredSize(new Dimension(210, 20));
            OBJ_AR02.setName("OBJ_AR02");
            panel1.add(OBJ_AR02, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- OBJ_AL02 ----
            OBJ_AL02.setText("@AL02@");
            OBJ_AL02.setMaximumSize(new Dimension(310, 20));
            OBJ_AL02.setInheritsPopupMenu(false);
            OBJ_AL02.setMinimumSize(new Dimension(310, 20));
            OBJ_AL02.setPreferredSize(new Dimension(310, 20));
            OBJ_AL02.setName("OBJ_AL02");
            panel1.add(OBJ_AL02, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- PR02 ----
            PR02.setMaximumSize(new Dimension(52, 28));
            PR02.setMinimumSize(new Dimension(52, 28));
            PR02.setPreferredSize(new Dimension(52, 28));
            PR02.setName("PR02");
            panel1.add(PR02, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 0), 0, 0));

            //---- OBJ_AR03 ----
            OBJ_AR03.setText("@AR03@");
            OBJ_AR03.setMaximumSize(new Dimension(210, 20));
            OBJ_AR03.setInheritsPopupMenu(false);
            OBJ_AR03.setMinimumSize(new Dimension(210, 20));
            OBJ_AR03.setPreferredSize(new Dimension(210, 20));
            OBJ_AR03.setName("OBJ_AR03");
            panel1.add(OBJ_AR03, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- OBJ_AL03 ----
            OBJ_AL03.setText("@AL03@");
            OBJ_AL03.setMaximumSize(new Dimension(310, 20));
            OBJ_AL03.setInheritsPopupMenu(false);
            OBJ_AL03.setMinimumSize(new Dimension(310, 20));
            OBJ_AL03.setPreferredSize(new Dimension(310, 20));
            OBJ_AL03.setName("OBJ_AL03");
            panel1.add(OBJ_AL03, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- PR03 ----
            PR03.setMaximumSize(new Dimension(52, 28));
            PR03.setMinimumSize(new Dimension(52, 28));
            PR03.setPreferredSize(new Dimension(52, 28));
            PR03.setName("PR03");
            panel1.add(PR03, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 0), 0, 0));

            //---- OBJ_AR04 ----
            OBJ_AR04.setText("@AR04@");
            OBJ_AR04.setMaximumSize(new Dimension(210, 20));
            OBJ_AR04.setInheritsPopupMenu(false);
            OBJ_AR04.setMinimumSize(new Dimension(210, 20));
            OBJ_AR04.setPreferredSize(new Dimension(210, 20));
            OBJ_AR04.setName("OBJ_AR04");
            panel1.add(OBJ_AR04, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- OBJ_AL04 ----
            OBJ_AL04.setText("@AL04@");
            OBJ_AL04.setMaximumSize(new Dimension(310, 20));
            OBJ_AL04.setInheritsPopupMenu(false);
            OBJ_AL04.setMinimumSize(new Dimension(310, 20));
            OBJ_AL04.setPreferredSize(new Dimension(310, 20));
            OBJ_AL04.setName("OBJ_AL04");
            panel1.add(OBJ_AL04, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- PR04 ----
            PR04.setMaximumSize(new Dimension(52, 28));
            PR04.setMinimumSize(new Dimension(52, 28));
            PR04.setPreferredSize(new Dimension(52, 28));
            PR04.setName("PR04");
            panel1.add(PR04, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 0), 0, 0));

            //---- OBJ_AR05 ----
            OBJ_AR05.setText("@AR05@");
            OBJ_AR05.setMaximumSize(new Dimension(210, 20));
            OBJ_AR05.setInheritsPopupMenu(false);
            OBJ_AR05.setMinimumSize(new Dimension(210, 20));
            OBJ_AR05.setPreferredSize(new Dimension(210, 20));
            OBJ_AR05.setName("OBJ_AR05");
            panel1.add(OBJ_AR05, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- OBJ_AL05 ----
            OBJ_AL05.setText("@AL05@");
            OBJ_AL05.setMaximumSize(new Dimension(310, 20));
            OBJ_AL05.setInheritsPopupMenu(false);
            OBJ_AL05.setMinimumSize(new Dimension(310, 20));
            OBJ_AL05.setPreferredSize(new Dimension(310, 20));
            OBJ_AL05.setName("OBJ_AL05");
            panel1.add(OBJ_AL05, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- PR05 ----
            PR05.setMaximumSize(new Dimension(52, 28));
            PR05.setMinimumSize(new Dimension(52, 28));
            PR05.setPreferredSize(new Dimension(52, 28));
            PR05.setName("PR05");
            panel1.add(PR05, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 0), 0, 0));

            //---- OBJ_AR06 ----
            OBJ_AR06.setText("@AR06@");
            OBJ_AR06.setMaximumSize(new Dimension(210, 20));
            OBJ_AR06.setInheritsPopupMenu(false);
            OBJ_AR06.setMinimumSize(new Dimension(210, 20));
            OBJ_AR06.setPreferredSize(new Dimension(210, 20));
            OBJ_AR06.setName("OBJ_AR06");
            panel1.add(OBJ_AR06, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- OBJ_AL06 ----
            OBJ_AL06.setText("@AL06@");
            OBJ_AL06.setMaximumSize(new Dimension(310, 20));
            OBJ_AL06.setInheritsPopupMenu(false);
            OBJ_AL06.setMinimumSize(new Dimension(310, 20));
            OBJ_AL06.setPreferredSize(new Dimension(310, 20));
            OBJ_AL06.setName("OBJ_AL06");
            panel1.add(OBJ_AL06, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- PR06 ----
            PR06.setMaximumSize(new Dimension(52, 28));
            PR06.setMinimumSize(new Dimension(52, 28));
            PR06.setPreferredSize(new Dimension(52, 28));
            PR06.setName("PR06");
            panel1.add(PR06, new GridBagConstraints(2, 6, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 0), 0, 0));

            //---- OBJ_AR07 ----
            OBJ_AR07.setText("@AR07@");
            OBJ_AR07.setMaximumSize(new Dimension(210, 20));
            OBJ_AR07.setInheritsPopupMenu(false);
            OBJ_AR07.setMinimumSize(new Dimension(210, 20));
            OBJ_AR07.setPreferredSize(new Dimension(210, 20));
            OBJ_AR07.setName("OBJ_AR07");
            panel1.add(OBJ_AR07, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- OBJ_AL07 ----
            OBJ_AL07.setText("@AL07@");
            OBJ_AL07.setMaximumSize(new Dimension(310, 20));
            OBJ_AL07.setInheritsPopupMenu(false);
            OBJ_AL07.setMinimumSize(new Dimension(310, 20));
            OBJ_AL07.setPreferredSize(new Dimension(310, 20));
            OBJ_AL07.setName("OBJ_AL07");
            panel1.add(OBJ_AL07, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- PR07 ----
            PR07.setMaximumSize(new Dimension(52, 28));
            PR07.setMinimumSize(new Dimension(52, 28));
            PR07.setPreferredSize(new Dimension(52, 28));
            PR07.setName("PR07");
            panel1.add(PR07, new GridBagConstraints(2, 7, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 0), 0, 0));

            //---- OBJ_AR08 ----
            OBJ_AR08.setText("@AR08@");
            OBJ_AR08.setMaximumSize(new Dimension(210, 20));
            OBJ_AR08.setInheritsPopupMenu(false);
            OBJ_AR08.setMinimumSize(new Dimension(210, 20));
            OBJ_AR08.setPreferredSize(new Dimension(210, 20));
            OBJ_AR08.setName("OBJ_AR08");
            panel1.add(OBJ_AR08, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- OBJ_AL08 ----
            OBJ_AL08.setText("@AL08@");
            OBJ_AL08.setMaximumSize(new Dimension(310, 20));
            OBJ_AL08.setInheritsPopupMenu(false);
            OBJ_AL08.setMinimumSize(new Dimension(310, 20));
            OBJ_AL08.setPreferredSize(new Dimension(310, 20));
            OBJ_AL08.setName("OBJ_AL08");
            panel1.add(OBJ_AL08, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- PR08 ----
            PR08.setMaximumSize(new Dimension(52, 28));
            PR08.setMinimumSize(new Dimension(52, 28));
            PR08.setPreferredSize(new Dimension(52, 28));
            PR08.setName("PR08");
            panel1.add(PR08, new GridBagConstraints(2, 8, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 0), 0, 0));

            //---- OBJ_AR09 ----
            OBJ_AR09.setText("@AR09@");
            OBJ_AR09.setMaximumSize(new Dimension(210, 20));
            OBJ_AR09.setInheritsPopupMenu(false);
            OBJ_AR09.setMinimumSize(new Dimension(210, 20));
            OBJ_AR09.setPreferredSize(new Dimension(210, 20));
            OBJ_AR09.setName("OBJ_AR09");
            panel1.add(OBJ_AR09, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- OBJ_AL09 ----
            OBJ_AL09.setText("@AL09@");
            OBJ_AL09.setMaximumSize(new Dimension(310, 20));
            OBJ_AL09.setInheritsPopupMenu(false);
            OBJ_AL09.setMinimumSize(new Dimension(310, 20));
            OBJ_AL09.setPreferredSize(new Dimension(310, 20));
            OBJ_AL09.setName("OBJ_AL09");
            panel1.add(OBJ_AL09, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- PR09 ----
            PR09.setMaximumSize(new Dimension(52, 28));
            PR09.setMinimumSize(new Dimension(52, 28));
            PR09.setPreferredSize(new Dimension(52, 28));
            PR09.setName("PR09");
            panel1.add(PR09, new GridBagConstraints(2, 9, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 0), 0, 0));

            //---- OBJ_AR10 ----
            OBJ_AR10.setText("@AR10@");
            OBJ_AR10.setMaximumSize(new Dimension(210, 20));
            OBJ_AR10.setInheritsPopupMenu(false);
            OBJ_AR10.setMinimumSize(new Dimension(210, 20));
            OBJ_AR10.setPreferredSize(new Dimension(210, 20));
            OBJ_AR10.setName("OBJ_AR10");
            panel1.add(OBJ_AR10, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- OBJ_AL10 ----
            OBJ_AL10.setText("@AL10@");
            OBJ_AL10.setMaximumSize(new Dimension(310, 20));
            OBJ_AL10.setInheritsPopupMenu(false);
            OBJ_AL10.setMinimumSize(new Dimension(310, 20));
            OBJ_AL10.setPreferredSize(new Dimension(310, 20));
            OBJ_AL10.setName("OBJ_AL10");
            panel1.add(OBJ_AL10, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- PR10 ----
            PR10.setMaximumSize(new Dimension(52, 28));
            PR10.setMinimumSize(new Dimension(52, 28));
            PR10.setPreferredSize(new Dimension(52, 28));
            PR10.setName("PR10");
            panel1.add(PR10, new GridBagConstraints(2, 10, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 0), 0, 0));

            //---- OBJ_AR11 ----
            OBJ_AR11.setText("@AR11@");
            OBJ_AR11.setMaximumSize(new Dimension(210, 20));
            OBJ_AR11.setInheritsPopupMenu(false);
            OBJ_AR11.setMinimumSize(new Dimension(210, 20));
            OBJ_AR11.setPreferredSize(new Dimension(210, 20));
            OBJ_AR11.setName("OBJ_AR11");
            panel1.add(OBJ_AR11, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- OBJ_AL11 ----
            OBJ_AL11.setText("@AL11@");
            OBJ_AL11.setMaximumSize(new Dimension(310, 20));
            OBJ_AL11.setInheritsPopupMenu(false);
            OBJ_AL11.setMinimumSize(new Dimension(310, 20));
            OBJ_AL11.setPreferredSize(new Dimension(310, 20));
            OBJ_AL11.setName("OBJ_AL11");
            panel1.add(OBJ_AL11, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- PR11 ----
            PR11.setMaximumSize(new Dimension(52, 28));
            PR11.setMinimumSize(new Dimension(52, 28));
            PR11.setPreferredSize(new Dimension(52, 28));
            PR11.setName("PR11");
            panel1.add(PR11, new GridBagConstraints(2, 11, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 0), 0, 0));

            //---- OBJ_AR12 ----
            OBJ_AR12.setText("@AR12@");
            OBJ_AR12.setMaximumSize(new Dimension(210, 20));
            OBJ_AR12.setInheritsPopupMenu(false);
            OBJ_AR12.setMinimumSize(new Dimension(210, 20));
            OBJ_AR12.setPreferredSize(new Dimension(210, 20));
            OBJ_AR12.setName("OBJ_AR12");
            panel1.add(OBJ_AR12, new GridBagConstraints(0, 12, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- OBJ_AL12 ----
            OBJ_AL12.setText("@AL12@");
            OBJ_AL12.setMaximumSize(new Dimension(310, 20));
            OBJ_AL12.setInheritsPopupMenu(false);
            OBJ_AL12.setMinimumSize(new Dimension(310, 20));
            OBJ_AL12.setPreferredSize(new Dimension(310, 20));
            OBJ_AL12.setName("OBJ_AL12");
            panel1.add(OBJ_AL12, new GridBagConstraints(1, 12, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- PR12 ----
            PR12.setMaximumSize(new Dimension(52, 28));
            PR12.setMinimumSize(new Dimension(52, 28));
            PR12.setPreferredSize(new Dimension(52, 28));
            PR12.setName("PR12");
            panel1.add(PR12, new GridBagConstraints(2, 12, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 0), 0, 0));

            //---- OBJ_AR13 ----
            OBJ_AR13.setText("@AR13@");
            OBJ_AR13.setMaximumSize(new Dimension(210, 20));
            OBJ_AR13.setInheritsPopupMenu(false);
            OBJ_AR13.setMinimumSize(new Dimension(210, 20));
            OBJ_AR13.setPreferredSize(new Dimension(210, 20));
            OBJ_AR13.setName("OBJ_AR13");
            panel1.add(OBJ_AR13, new GridBagConstraints(0, 13, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- OBJ_AL13 ----
            OBJ_AL13.setText("@AL13@");
            OBJ_AL13.setMaximumSize(new Dimension(310, 20));
            OBJ_AL13.setInheritsPopupMenu(false);
            OBJ_AL13.setMinimumSize(new Dimension(310, 20));
            OBJ_AL13.setPreferredSize(new Dimension(310, 20));
            OBJ_AL13.setName("OBJ_AL13");
            panel1.add(OBJ_AL13, new GridBagConstraints(1, 13, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- PR13 ----
            PR13.setMaximumSize(new Dimension(52, 28));
            PR13.setMinimumSize(new Dimension(52, 28));
            PR13.setPreferredSize(new Dimension(52, 28));
            PR13.setName("PR13");
            panel1.add(PR13, new GridBagConstraints(2, 13, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 0), 0, 0));

            //---- OBJ_AR14 ----
            OBJ_AR14.setText("@AR14@");
            OBJ_AR14.setMaximumSize(new Dimension(210, 20));
            OBJ_AR14.setInheritsPopupMenu(false);
            OBJ_AR14.setMinimumSize(new Dimension(210, 20));
            OBJ_AR14.setPreferredSize(new Dimension(210, 20));
            OBJ_AR14.setName("OBJ_AR14");
            panel1.add(OBJ_AR14, new GridBagConstraints(0, 14, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- OBJ_AL14 ----
            OBJ_AL14.setText("@AL14@");
            OBJ_AL14.setMaximumSize(new Dimension(310, 20));
            OBJ_AL14.setInheritsPopupMenu(false);
            OBJ_AL14.setMinimumSize(new Dimension(310, 20));
            OBJ_AL14.setPreferredSize(new Dimension(310, 20));
            OBJ_AL14.setName("OBJ_AL14");
            panel1.add(OBJ_AL14, new GridBagConstraints(1, 14, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 5), 0, 0));

            //---- PR14 ----
            PR14.setMaximumSize(new Dimension(52, 28));
            PR14.setMinimumSize(new Dimension(52, 28));
            PR14.setPreferredSize(new Dimension(52, 28));
            PR14.setName("PR14");
            panel1.add(PR14, new GridBagConstraints(2, 14, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 2, 0), 0, 0));

            //---- OBJ_AR15 ----
            OBJ_AR15.setText("@AR15@");
            OBJ_AR15.setMaximumSize(new Dimension(210, 20));
            OBJ_AR15.setInheritsPopupMenu(false);
            OBJ_AR15.setMinimumSize(new Dimension(210, 20));
            OBJ_AR15.setPreferredSize(new Dimension(210, 20));
            OBJ_AR15.setName("OBJ_AR15");
            panel1.add(OBJ_AR15, new GridBagConstraints(0, 15, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- OBJ_AL15 ----
            OBJ_AL15.setText("@AL15@");
            OBJ_AL15.setMaximumSize(new Dimension(310, 20));
            OBJ_AL15.setInheritsPopupMenu(false);
            OBJ_AL15.setMinimumSize(new Dimension(310, 20));
            OBJ_AL15.setPreferredSize(new Dimension(310, 20));
            OBJ_AL15.setName("OBJ_AL15");
            panel1.add(OBJ_AL15, new GridBagConstraints(1, 15, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- PR15 ----
            PR15.setMaximumSize(new Dimension(52, 28));
            PR15.setMinimumSize(new Dimension(52, 28));
            PR15.setPreferredSize(new Dimension(52, 28));
            PR15.setName("PR15");
            panel1.add(PR15, new GridBagConstraints(2, 15, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          p_contenu.add(panel1, new GridBagConstraints(0, 2, 6, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));

          //---- OBJ_AR2 ----
          OBJ_AR2.setText("@TOTAL@");
          OBJ_AR2.setMaximumSize(new Dimension(68, 20));
          OBJ_AR2.setInheritsPopupMenu(false);
          OBJ_AR2.setMinimumSize(new Dimension(68, 20));
          OBJ_AR2.setPreferredSize(new Dimension(68, 20));
          OBJ_AR2.setFont(OBJ_AR2.getFont().deriveFont(OBJ_AR2.getFont().getStyle() | Font.BOLD));
          OBJ_AR2.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_AR2.setName("OBJ_AR2");
          p_contenu.add(OBJ_AR2, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 5, 20), 0, 0));

          //---- OBJ_LibTotal ----
          OBJ_LibTotal.setText("Total :");
          OBJ_LibTotal.setFont(OBJ_LibTotal.getFont().deriveFont(OBJ_LibTotal.getFont().getStyle() | Font.BOLD));
          OBJ_LibTotal.setName("OBJ_LibTotal");
          p_contenu.add(OBJ_LibTotal, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- OBJ_Referent ----
          OBJ_Referent.setText("R\u00e9f\u00e9rent :");
          OBJ_Referent.setName("OBJ_Referent");
          p_contenu.add(OBJ_Referent, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- OBJ_LibMagasin ----
          OBJ_LibMagasin.setText("Magasin :");
          OBJ_LibMagasin.setName("OBJ_LibMagasin");
          p_contenu.add(OBJ_LibMagasin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- OBJ_ARART ----
          OBJ_ARART.setText("@ARART@");
          OBJ_ARART.setMaximumSize(new Dimension(210, 20));
          OBJ_ARART.setInheritsPopupMenu(false);
          OBJ_ARART.setMinimumSize(new Dimension(210, 20));
          OBJ_ARART.setPreferredSize(new Dimension(210, 20));
          OBJ_ARART.setName("OBJ_ARART");
          p_contenu.add(OBJ_ARART, new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- OBJ_ARARTL ----
          OBJ_ARARTL.setText("@ALARTL@");
          OBJ_ARARTL.setMaximumSize(new Dimension(310, 20));
          OBJ_ARARTL.setInheritsPopupMenu(false);
          OBJ_ARARTL.setMinimumSize(new Dimension(310, 20));
          OBJ_ARARTL.setPreferredSize(new Dimension(310, 20));
          OBJ_ARARTL.setName("OBJ_ARARTL");
          p_contenu.add(OBJ_ARARTL, new GridBagConstraints(3, 0, 3, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- OBJ_ARMAG ----
          OBJ_ARMAG.setText("@ARMAG@");
          OBJ_ARMAG.setMaximumSize(new Dimension(34, 20));
          OBJ_ARMAG.setInheritsPopupMenu(false);
          OBJ_ARMAG.setMinimumSize(new Dimension(34, 20));
          OBJ_ARMAG.setPreferredSize(new Dimension(34, 20));
          OBJ_ARMAG.setName("OBJ_ARMAG");
          p_contenu.add(OBJ_ARMAG, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- OBJ_ARART2 ----
          OBJ_ARART2.setText("@ARMAGL@");
          OBJ_ARART2.setMaximumSize(new Dimension(210, 20));
          OBJ_ARART2.setInheritsPopupMenu(false);
          OBJ_ARART2.setMinimumSize(new Dimension(210, 20));
          OBJ_ARART2.setPreferredSize(new Dimension(210, 20));
          OBJ_ARART2.setName("OBJ_ARART2");
          p_contenu.add(OBJ_ARART2, new GridBagConstraints(2, 1, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 5, 5), 0, 0));
        }
        panel3.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(10, 10, 10, 10), 0, 0));
      }
      p_principal.add(panel3, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private JPanel panel3;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_57;
  private JLabel OBJ_58;
  private JLabel OBJ_59;
  private JLabel OBJ_AR01;
  private JLabel OBJ_AL01;
  private XRiTextField PR01;
  private JLabel OBJ_AR02;
  private JLabel OBJ_AL02;
  private XRiTextField PR02;
  private JLabel OBJ_AR03;
  private JLabel OBJ_AL03;
  private XRiTextField PR03;
  private JLabel OBJ_AR04;
  private JLabel OBJ_AL04;
  private XRiTextField PR04;
  private JLabel OBJ_AR05;
  private JLabel OBJ_AL05;
  private XRiTextField PR05;
  private JLabel OBJ_AR06;
  private JLabel OBJ_AL06;
  private XRiTextField PR06;
  private JLabel OBJ_AR07;
  private JLabel OBJ_AL07;
  private XRiTextField PR07;
  private JLabel OBJ_AR08;
  private JLabel OBJ_AL08;
  private XRiTextField PR08;
  private JLabel OBJ_AR09;
  private JLabel OBJ_AL09;
  private XRiTextField PR09;
  private JLabel OBJ_AR10;
  private JLabel OBJ_AL10;
  private XRiTextField PR10;
  private JLabel OBJ_AR11;
  private JLabel OBJ_AL11;
  private XRiTextField PR11;
  private JLabel OBJ_AR12;
  private JLabel OBJ_AL12;
  private XRiTextField PR12;
  private JLabel OBJ_AR13;
  private JLabel OBJ_AL13;
  private XRiTextField PR13;
  private JLabel OBJ_AR14;
  private JLabel OBJ_AL14;
  private XRiTextField PR14;
  private JLabel OBJ_AR15;
  private JLabel OBJ_AL15;
  private XRiTextField PR15;
  private RiZoneSortie OBJ_AR2;
  private JLabel OBJ_LibTotal;
  private JLabel OBJ_Referent;
  private JLabel OBJ_LibMagasin;
  private RiZoneSortie OBJ_ARART;
  private RiZoneSortie OBJ_ARARTL;
  private RiZoneSortie OBJ_ARMAG;
  private RiZoneSortie OBJ_ARART2;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
