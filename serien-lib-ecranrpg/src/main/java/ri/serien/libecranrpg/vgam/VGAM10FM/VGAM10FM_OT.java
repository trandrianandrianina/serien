
package ri.serien.libecranrpg.vgam.VGAM10FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGAM10FM_OT extends SNPanelEcranRPG implements ioFrame {
  
  public VGAM10FM_OT(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_retour);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT1@")).trim());
    OBJ_11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT3@")).trim());
    OBJ_15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOTNB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    FLD001.setVisible(lexique.isPresent("FLD001"));
    OBJ_15.setVisible(lexique.isPresent("TOTNB"));
    OBJ_11.setVisible(lexique.isPresent("TOT3"));
    OBJ_8.setVisible(lexique.isPresent("TOT1"));
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Totalisation liée à la demande"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    OBJ_8 = new RiZoneSortie();
    OBJ_11 = new RiZoneSortie();
    OBJ_14 = new JLabel();
    OBJ_15 = new RiZoneSortie();
    OBJ_10 = new JLabel();
    OBJ_7 = new JLabel();
    FLD001 = new XRiTextField();
    
    // ======== this ========
    setMinimumSize(new Dimension(515, 155));
    setPreferredSize(new Dimension(515, 155));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        
        // ---- OBJ_8 ----
        OBJ_8.setText("@TOT1@");
        OBJ_8.setName("OBJ_8");
        
        // ---- OBJ_11 ----
        OBJ_11.setText("@TOT3@");
        OBJ_11.setName("OBJ_11");
        
        // ---- OBJ_14 ----
        OBJ_14.setText("Nombre de bons ou factures");
        OBJ_14.setName("OBJ_14");
        
        // ---- OBJ_15 ----
        OBJ_15.setText("@TOTNB@");
        OBJ_15.setName("OBJ_15");
        
        // ---- OBJ_10 ----
        OBJ_10.setText("Montant TTC");
        OBJ_10.setName("OBJ_10");
        
        // ---- OBJ_7 ----
        OBJ_7.setText("Montant H.T");
        OBJ_7.setName("OBJ_7");
        
        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup().addGap(25, 25, 25)
                .addGroup(p_contenuLayout.createParallelGroup()
                    .addGroup(p_contenuLayout.createSequentialGroup()
                        .addComponent(OBJ_7, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE).addGap(25, 25, 25)
                        .addComponent(OBJ_8, GroupLayout.PREFERRED_SIZE, 171, GroupLayout.PREFERRED_SIZE))
                    .addGroup(p_contenuLayout.createSequentialGroup()
                        .addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE).addGap(21, 21, 21)
                        .addComponent(OBJ_11, GroupLayout.PREFERRED_SIZE, 171, GroupLayout.PREFERRED_SIZE))
                    .addGroup(p_contenuLayout.createSequentialGroup()
                        .addComponent(OBJ_14, GroupLayout.PREFERRED_SIZE, 164, GroupLayout.PREFERRED_SIZE).addGap(10, 10, 10)
                        .addComponent(OBJ_15, GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE)))));
        p_contenuLayout
            .setVerticalGroup(
                p_contenuLayout.createParallelGroup()
                    .addGroup(p_contenuLayout.createSequentialGroup().addGap(35, 35, 35)
                        .addGroup(p_contenuLayout.createParallelGroup()
                            .addGroup(p_contenuLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(OBJ_7,
                                GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                            .addComponent(OBJ_8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGap(6, 6, 6)
                        .addGroup(p_contenuLayout.createParallelGroup()
                            .addGroup(p_contenuLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(OBJ_10,
                                GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                            .addComponent(OBJ_11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGap(6, 6, 6)
                        .addGroup(p_contenuLayout.createParallelGroup()
                            .addGroup(p_contenuLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(OBJ_14,
                                GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                            .addComponent(OBJ_15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))));
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ---- FLD001 ----
    FLD001.setName("FLD001");
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private RiZoneSortie OBJ_8;
  private RiZoneSortie OBJ_11;
  private JLabel OBJ_14;
  private RiZoneSortie OBJ_15;
  private JLabel OBJ_10;
  private JLabel OBJ_7;
  private XRiTextField FLD001;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
