//$$david$$ ££17/12/10££ -> nouveau de A à Z

package ri.serien.libecranrpg.vgam.VGAM35FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.ODialog;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGAM35FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private ODialog dialog_OPTION = null;
  
  public VGAM35FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    R0TYP.setVisible(false);
    R0TYP_lib.setText(gererR0TYP(lexique.HostFieldGetData("R0TYP")));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    lexique.HostFieldPutData("R0TYP", 0, R0TYP.getText());
  }
  
  private String gererR0TYP(String valeur) {
    if (valeur != null) {
      valeur = valeur.trim();
      if (valeur.equals("")) {
        return "";
      }
      else if (valeur.equals("V")) {
        return "Prix catalogue x Coeff.";
      }
      else if (valeur.equals("C")) {
        return "Prix catalogue seul";
      }
      else if (valeur.equals("W")) {
        return "Prix catalogue x coeff. d'approche x coeff.";
      }
      else if (valeur.equals("N")) {
        return "Prix net d'achat x Coeff.";
      }
      else if (valeur.equals("M")) {
        return "Prix net x Coeff. d'approche moyen x coeff.";
      }
      else if (valeur.equals("A")) {
        return "Prix net x Coeff. d'approche saisi x coeff.";
      }
      else if (valeur.equals("1")) {
        return "1er prix quantitatif";
      }
      else if (valeur.equals("2")) {
        return "2eme prix quantitatif";
      }
      else if (valeur.equals("3")) {
        return "3eme prix quantitatif";
      }
      else if (valeur.equals("4")) {
        return "4eme prix quantitatif";
      }
      else if (valeur.equals("5")) {
        return "5eme prix quantitatif";
      }
      else if (valeur.equals("6")) {
        return "6eme prix quantitatif";
      }
      else if (valeur.equals("7")) {
        return "7eme prix quantitatif";
      }
      else if (valeur.equals("8")) {
        return "8eme prix quantitatif";
      }
      else if (valeur.equals("9")) {
        return "9eme prix quantitatif";
      }
      else if (valeur.equals("P")) {
        return "PUMP dès la réception d'achat x coeff.";
      }
      else if (valeur.equals("Q")) {
        return "PUMP à la demande (GAM 561) x coeff.";
      }
      else if (valeur.equals("R")) {
        return "Prix de revient x coeff.";
      }
      else if (valeur.equals("D")) {
        return "Prix de revient réel du dernier achat x coeff.";
      }
      else {
        return "";
      }
    }
    else {
      return null;
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riBoutonDetail1ActionPerformed(ActionEvent e) {
    if (dialog_OPTION == null) {
      dialog_OPTION = new ODialog((Window) getTopLevelAncestor(), new OPTION_PANEL(this));
    }
    dialog_OPTION.affichePopupPerso();
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_66 = new JLabel();
    OBJ_65 = new JLabel();
    OBJ_68 = new JLabel();
    INDRGA = new XRiTextField();
    INDFRS = new XRiTextField();
    INDETB = new XRiTextField();
    INDCOL = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_50 = new JLabel();
    R0LIB = new XRiTextField();
    OBJ_40 = new JLabel();
    OBJ_42 = new JLabel();
    R0REM1 = new XRiTextField();
    R0REM2 = new XRiTextField();
    R0REM3 = new XRiTextField();
    R0REM4 = new XRiTextField();
    R0REM5 = new XRiTextField();
    R0REM6 = new XRiTextField();
    R0IN1 = new XRiTextField();
    panel2 = new JPanel();
    OBJ_59 = new JLabel();
    OBJ_57 = new JLabel();
    OBJ_55 = new JLabel();
    R0COE = new XRiTextField();
    R0COA = new XRiTextField();
    R0CPVM = new XRiTextField();
    OBJ_53 = new JLabel();
    riBoutonDetail1 = new SNBoutonDetail();
    R0TYP_lib = new RiZoneSortie();
    R0TYP = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Regroupement achat");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
          
          // ---- OBJ_66 ----
          OBJ_66.setText("Etablissement");
          OBJ_66.setName("OBJ_66");
          p_tete_gauche.add(OBJ_66);
          OBJ_66.setBounds(5, 5, 95, 20);
          
          // ---- OBJ_65 ----
          OBJ_65.setText("Code RA.");
          OBJ_65.setName("OBJ_65");
          p_tete_gauche.add(OBJ_65);
          OBJ_65.setBounds(345, 5, 65, 20);
          
          // ---- OBJ_68 ----
          OBJ_68.setText("Fournisseur");
          OBJ_68.setName("OBJ_68");
          p_tete_gauche.add(OBJ_68);
          OBJ_68.setBounds(165, 5, 78, 20);
          
          // ---- INDRGA ----
          INDRGA.setComponentPopupMenu(BTD);
          INDRGA.setName("INDRGA");
          p_tete_gauche.add(INDRGA);
          INDRGA.setBounds(410, 1, 110, INDRGA.getPreferredSize().height);
          
          // ---- INDFRS ----
          INDFRS.setComponentPopupMenu(BTD);
          INDFRS.setName("INDFRS");
          p_tete_gauche.add(INDFRS);
          INDFRS.setBounds(265, 1, 58, INDFRS.getPreferredSize().height);
          
          // ---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(105, 1, 40, INDETB.getPreferredSize().height);
          
          // ---- INDCOL ----
          INDCOL.setComponentPopupMenu(BTD);
          INDCOL.setName("INDCOL");
          p_tete_gauche.add(INDCOL);
          INDCOL.setBounds(245, 1, 18, INDCOL.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(e -> bouton_validerActionPerformed(e));
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(e -> bouton_retourActionPerformed(e));
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 180));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Crit\u00e8res de r\u00e9appro.");
              riSousMenu_bt6.setToolTipText("Crit\u00e8res de r\u00e9approvisionnement");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(e -> riSousMenu_bt6ActionPerformed(e));
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        ((GridBagLayout) p_centrage.getLayout()).columnWidths = new int[] { 600 };
        ((GridBagLayout) p_centrage.getLayout()).rowHeights = new int[] { 465, 0 };
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(600, 460));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(600, 460));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);
          
          // ======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Regroupement achat"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            
            // ---- OBJ_50 ----
            OBJ_50.setText("Nombre de caract\u00e8res pour condition de d\u00e9rogation");
            OBJ_50.setName("OBJ_50");
            
            // ---- R0LIB ----
            R0LIB.setComponentPopupMenu(BTD);
            R0LIB.setName("R0LIB");
            
            // ---- OBJ_40 ----
            OBJ_40.setText("Libell\u00e9");
            OBJ_40.setName("OBJ_40");
            
            // ---- OBJ_42 ----
            OBJ_42.setText("Remises");
            OBJ_42.setName("OBJ_42");
            
            // ---- R0REM1 ----
            R0REM1.setComponentPopupMenu(BTD);
            R0REM1.setName("R0REM1");
            
            // ---- R0REM2 ----
            R0REM2.setComponentPopupMenu(BTD);
            R0REM2.setName("R0REM2");
            
            // ---- R0REM3 ----
            R0REM3.setComponentPopupMenu(BTD);
            R0REM3.setName("R0REM3");
            
            // ---- R0REM4 ----
            R0REM4.setComponentPopupMenu(BTD);
            R0REM4.setName("R0REM4");
            
            // ---- R0REM5 ----
            R0REM5.setComponentPopupMenu(BTD);
            R0REM5.setName("R0REM5");
            
            // ---- R0REM6 ----
            R0REM6.setComponentPopupMenu(BTD);
            R0REM6.setName("R0REM6");
            
            // ---- R0IN1 ----
            R0IN1.setComponentPopupMenu(BTD);
            R0IN1.setName("R0IN1");
            
            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup().addGap(24, 24, 24)
                    .addGroup(panel1Layout.createParallelGroup()
                        .addGroup(panel1Layout.createSequentialGroup()
                            .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE).addGap(13, 13, 13)
                            .addComponent(R0LIB, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                            .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE).addGap(13, 13, 13)
                            .addComponent(R0REM1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                            .addComponent(R0REM2, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE).addGap(10, 10, 10)
                            .addComponent(R0REM3, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                            .addComponent(R0REM4, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                            .addComponent(R0REM5, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                            .addComponent(R0REM6, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                            .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 290, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                            .addComponent(R0IN1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))));
            panel1Layout.setVerticalGroup(panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup().addGap(15, 15, 15)
                    .addGroup(panel1Layout.createParallelGroup()
                        .addGroup(panel1Layout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE,
                            20, GroupLayout.PREFERRED_SIZE))
                        .addComponent(R0LIB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(7, 7, 7)
                    .addGroup(panel1Layout.createParallelGroup()
                        .addGroup(panel1Layout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE,
                            20, GroupLayout.PREFERRED_SIZE))
                        .addComponent(R0REM1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0REM2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0REM3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0REM4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0REM5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(R0REM6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(7, 7, 7)
                    .addGroup(panel1Layout.createParallelGroup()
                        .addGroup(panel1Layout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE,
                            20, GroupLayout.PREFERRED_SIZE))
                        .addComponent(R0IN1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))));
          }
          p_contenu.add(panel1);
          panel1.setBounds(40, 40, 515, 150);
          
          // ======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Mise \u00e0 jour du prix de vente "));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            
            // ---- OBJ_59 ----
            OBJ_59.setText("Coefficient de calcul du prix de vente mini");
            OBJ_59.setName("OBJ_59");
            
            // ---- OBJ_57 ----
            OBJ_57.setText("Coefficient de calcul du prix catalogue");
            OBJ_57.setName("OBJ_57");
            
            // ---- OBJ_55 ----
            OBJ_55.setText("Coefficient");
            OBJ_55.setName("OBJ_55");
            
            // ---- R0COE ----
            R0COE.setComponentPopupMenu(BTD);
            R0COE.setName("R0COE");
            
            // ---- R0COA ----
            R0COA.setComponentPopupMenu(BTD);
            R0COA.setName("R0COA");
            
            // ---- R0CPVM ----
            R0CPVM.setComponentPopupMenu(BTD);
            R0CPVM.setName("R0CPVM");
            
            // ---- OBJ_53 ----
            OBJ_53.setText("Type");
            OBJ_53.setName("OBJ_53");
            
            // ---- riBoutonDetail1 ----
            riBoutonDetail1.setName("riBoutonDetail1");
            riBoutonDetail1.addActionListener(e -> riBoutonDetail1ActionPerformed(e));
            
            // ---- R0TYP_lib ----
            R0TYP_lib.setText("text");
            R0TYP_lib.setName("R0TYP_lib");
            
            // ---- R0TYP ----
            R0TYP.setName("R0TYP");
            
            GroupLayout panel2Layout = new GroupLayout(panel2);
            panel2.setLayout(panel2Layout);
            panel2Layout.setHorizontalGroup(panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup().addGap(24, 24, 24)
                    .addGroup(panel2Layout.createParallelGroup().addGroup(panel2Layout.createSequentialGroup()
                        .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE).addGap(4, 4, 4)
                        .addComponent(R0TYP, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(10, 10, 10)
                        .addComponent(R0TYP_lib, GroupLayout.PREFERRED_SIZE, 325, GroupLayout.PREFERRED_SIZE).addGap(10, 10, 10)
                        .addComponent(riBoutonDetail1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel2Layout.createSequentialGroup()
                            .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                            .addComponent(R0COE, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel2Layout.createSequentialGroup()
                            .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 235, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                            .addComponent(R0COA, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel2Layout.createSequentialGroup()
                            .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                            .addComponent(R0CPVM, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)))));
            panel2Layout.setVerticalGroup(panel2Layout.createParallelGroup().addGroup(panel2Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 20,
                        GroupLayout.PREFERRED_SIZE))
                    .addComponent(R0TYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel2Layout.createSequentialGroup().addGap(2, 2, 2).addComponent(R0TYP_lib, GroupLayout.PREFERRED_SIZE,
                        GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createSequentialGroup().addGap(5, 5, 5)
                        .addComponent(riBoutonDetail1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(8, 8, 8)
                .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup().addGap(5, 5, 5).addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 19,
                        GroupLayout.PREFERRED_SIZE))
                    .addComponent(R0COE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 20,
                        GroupLayout.PREFERRED_SIZE))
                    .addComponent(R0COA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 20,
                        GroupLayout.PREFERRED_SIZE))
                    .addComponent(R0CPVM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))));
          }
          p_contenu.add(panel2);
          panel2.setBounds(40, 220, 515, 200);
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(e -> OBJ_11ActionPerformed(e));
      BTD.add(OBJ_11);
      
      // ---- OBJ_12 ----
      OBJ_12.setText("Invite");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(e -> OBJ_12ActionPerformed(e));
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_66;
  private JLabel OBJ_65;
  private JLabel OBJ_68;
  private XRiTextField INDRGA;
  private XRiTextField INDFRS;
  private XRiTextField INDETB;
  private XRiTextField INDCOL;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_50;
  private XRiTextField R0LIB;
  private JLabel OBJ_40;
  private JLabel OBJ_42;
  private XRiTextField R0REM1;
  private XRiTextField R0REM2;
  private XRiTextField R0REM3;
  private XRiTextField R0REM4;
  private XRiTextField R0REM5;
  private XRiTextField R0REM6;
  private XRiTextField R0IN1;
  private JPanel panel2;
  private JLabel OBJ_59;
  private JLabel OBJ_57;
  private JLabel OBJ_55;
  private XRiTextField R0COE;
  private XRiTextField R0COA;
  private XRiTextField R0CPVM;
  private JLabel OBJ_53;
  private SNBoutonDetail riBoutonDetail1;
  private RiZoneSortie R0TYP_lib;
  private XRiTextField R0TYP;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
