/*
 * Created by JFormDesigner on Wed Dec 04 12:08:02 CET 2019
 */

package ri.serien.libecranrpg.vgam.VGAM20FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JScrollPane;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiBarreBouton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GAM284] Gestion des achats -> Documents d'achats -> Editions factures fournisseurs -> Analyse des frais
 * Indicateur : 00000000
 * Titre :Analyse des imputations de frais
 * 
 */
public class VGAM20FM_A3 extends SNPanelEcranRPG implements ioFrame {
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", "WTP16", "WTP17", "WTP18", "WTP19", "WTP20", };
  private String[] _WTP01_Title =
      { "Facture Date Fac  N°Frs  Ach    Montant     Coef PRV", "Facture Lig                           Date Fac    Montant       " };
  private String[][] _WTP01_Data =
      { { "LD011", "LD012", }, { "LD021", "LD022", }, { "LD031", "LD032", }, { "LD041", "LD042", }, { "LD051", "LD052", },
          { "LD061", "LD062", }, { "LD071", "LD072", }, { "LD081", "LD082", }, { "LD091", "LD092", }, { "LD101", "LD102", },
          { "LD111", "LD112", }, { "LD121", "LD122", }, { "LD131", "LD132", }, { "LD141", "LD142", }, { "LD151", "LD152", },
          { "LD161", "LD162", }, { "LD171", "LD172", }, { "LD181", "LD182", }, { "LD191", "LD192", }, { "LD201", "LD202", }, };
  private int[] _WTP01_Width = { 160, 80, 81, 81, 83, };
  
  public VGAM20FM_A3(ArrayList param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
    
    // Ajout
    initDiverses();
    
    xriBarreBouton.ajouterBouton(EnumBouton.CONTINUER, false);
    xriBarreBouton.ajouterBouton(EnumBouton.ALLER_ECRAN_PRECEDENT, true);
    xriBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
        xriBarreBouton.isTraiterClickBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Charge de l'etablissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Activation du bouton valider si une ligne est sélectionnée
    if (WTP01.getSelectedRowCount() != 0) {
      xriBarreBouton.activerBouton(EnumBouton.CONTINUER, true);
    }
    else {
      xriBarreBouton.activerBouton(EnumBouton.CONTINUER, false);
    }
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
  }
  
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.CONTINUER)) {
        WTP01.setValeurTop("1");
        lexique.HostScreenSendKey(this, "Enter");
      }
      else if (pSNBouton.isBouton(EnumBouton.ALLER_ECRAN_PRECEDENT)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.getSelectedRowCount() != 0) {
      xriBarreBouton.activerBouton(EnumBouton.CONTINUER, true);
    }
    else {
      xriBarreBouton.activerBouton(EnumBouton.CONTINUER, false);
    }
    if (WTP01.doubleClicSelection(e)) {
      WTP01.setValeurTop("1");
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlObjetAnalyse = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbDossier = new SNLabelChamp();
    WDOS = new XRiTextField();
    lbContainer = new SNLabelChamp();
    WCNT = new XRiTextField();
    pnlAnalyseImputation = new SNPanelTitre();
    lbMarchandise = new SNLabelTitre();
    lbFrais = new SNLabelTitre();
    scpScrollTableau = new JScrollPane();
    WTP01 = new XRiTable();
    lbVeuillezSelectionner = new SNMessage();
    xriBarreBouton = new XRiBarreBouton();
    
    // ======== this ========
    setPreferredSize(new Dimension(900, 600));
    setMinimumSize(new Dimension(0, 0));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Analyse des imputations de frais");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
      
      // ======== pnlObjetAnalyse ========
      {
        pnlObjetAnalyse.setTitre("Objet de l'analyse");
        pnlObjetAnalyse.setPreferredSize(new Dimension(580, 150));
        pnlObjetAnalyse.setMinimumSize(new Dimension(580, 150));
        pnlObjetAnalyse.setName("pnlObjetAnalyse");
        pnlObjetAnalyse.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlObjetAnalyse.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlObjetAnalyse.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlObjetAnalyse.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlObjetAnalyse.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbEtablissement ----
        lbEtablissement.setText("Etablissement");
        lbEtablissement.setName("lbEtablissement");
        pnlObjetAnalyse.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snEtablissement ----
        snEtablissement.setEnabled(false);
        snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
        snEtablissement.setName("snEtablissement");
        pnlObjetAnalyse.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbDossier ----
        lbDossier.setText("Dossier");
        lbDossier.setName("lbDossier");
        pnlObjetAnalyse.add(lbDossier, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WDOS ----
        WDOS.setPreferredSize(new Dimension(92, 30));
        WDOS.setMinimumSize(new Dimension(92, 30));
        WDOS.setMaximumSize(new Dimension(92, 30));
        WDOS.setFont(new Font("sansserif", Font.PLAIN, 14));
        WDOS.setName("WDOS");
        pnlObjetAnalyse.add(WDOS, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbContainer ----
        lbContainer.setText("Container");
        lbContainer.setName("lbContainer");
        pnlObjetAnalyse.add(lbContainer, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- WCNT ----
        WCNT.setPreferredSize(new Dimension(164, 30));
        WCNT.setMinimumSize(new Dimension(164, 30));
        WCNT.setMaximumSize(new Dimension(164, 30));
        WCNT.setFont(new Font("sansserif", Font.PLAIN, 14));
        WCNT.setName("WCNT");
        pnlObjetAnalyse.add(WCNT, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlObjetAnalyse, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 0), 0, 0));
      
      // ======== pnlAnalyseImputation ========
      {
        pnlAnalyseImputation.setTitre("Analyse des imputation des frais");
        pnlAnalyseImputation.setName("pnlAnalyseImputation");
        pnlAnalyseImputation.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlAnalyseImputation.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlAnalyseImputation.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlAnalyseImputation.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlAnalyseImputation.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbMarchandise ----
        lbMarchandise.setText("Marchandises");
        lbMarchandise.setName("lbMarchandise");
        pnlAnalyseImputation.add(lbMarchandise, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 2, 5), 0, 0));
        
        // ---- lbFrais ----
        lbFrais.setText("Frais");
        lbFrais.setName("lbFrais");
        pnlAnalyseImputation.add(lbFrais, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 2, 0), 0, 0));
        
        // ======== scpScrollTableau ========
        {
          scpScrollTableau.setMinimumSize(new Dimension(25, 348));
          scpScrollTableau.setPreferredSize(new Dimension(456, 348));
          scpScrollTableau.setName("scpScrollTableau");
          
          // ---- WTP01 ----
          WTP01.setPreferredSize(new Dimension(600, 320));
          WTP01.setMinimumSize(new Dimension(15, 320));
          WTP01.setName("WTP01");
          WTP01.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              WTP01MouseClicked(e);
            }
          });
          scpScrollTableau.setViewportView(WTP01);
        }
        pnlAnalyseImputation.add(scpScrollTableau, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.NORTH,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 2, 0), 0, 0));
        
        // ---- lbVeuillezSelectionner ----
        lbVeuillezSelectionner.setText("Veuillez s\u00e9lectionner une ligne pour pouvoir poursuivre");
        lbVeuillezSelectionner.setName("lbVeuillezSelectionner");
        pnlAnalyseImputation.add(lbVeuillezSelectionner, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlAnalyseImputation, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
          GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- xriBarreBouton ----
    xriBarreBouton.setName("xriBarreBouton");
    add(xriBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlObjetAnalyse;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbDossier;
  private XRiTextField WDOS;
  private SNLabelChamp lbContainer;
  private XRiTextField WCNT;
  private SNPanelTitre pnlAnalyseImputation;
  private SNLabelTitre lbMarchandise;
  private SNLabelTitre lbFrais;
  private JScrollPane scpScrollTableau;
  private XRiTable WTP01;
  private SNMessage lbVeuillezSelectionner;
  private XRiBarreBouton xriBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
