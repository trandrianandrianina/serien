
package ri.serien.libecranrpg.vgam.VGAM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGAM01FX_EF extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] EFREP_Value = { "", "0", "1", "2", "3", "4", "5", "6", };
  
  public VGAM01FX_EF(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    EFREP.setValeurs(EFREP_Value, null);
    EFDDI5.setValeursSelection("1", " ");
    EFPDI5.setValeursSelection("1", " ");
    EFDVTE.setValeursSelection("1", " ");
    EFPVTE.setValeursSelection("1", " ");
    EFDHTI.setValeursSelection("1", " ");
    EFPHTI.setValeursSelection("1", " ");
    EFDDIS.setValeursSelection("1", " ");
    EFPDIS.setValeursSelection("1", " ");
    EFDMSD.setValeursSelection("1", " ");
    EFPMSD.setValeursSelection("1", " ");
    EFDMAG.setValeursSelection("1", " ");
    EFPMAG.setValeursSelection("1", " ");
    EFBA6.setValeursSelection("1", " ");
    EFBA5.setValeursSelection("1", " ");
    EFBA4.setValeursSelection("1", " ");
    EFBA3.setValeursSelection("1", " ");
    EFBA2.setValeursSelection("1", " ");
    EFBA1.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    // EFDDI5.setSelected(lexique.HostFieldGetData("EFDDI5").equalsIgnoreCase("1"));
    // EFPDI5.setSelected(lexique.HostFieldGetData("EFPDI5").equalsIgnoreCase("1"));
    // EFDVTE.setSelected(lexique.HostFieldGetData("EFDVTE").equalsIgnoreCase("1"));
    // EFPVTE.setSelected(lexique.HostFieldGetData("EFPVTE").equalsIgnoreCase("1"));
    // EFDHTI.setSelected(lexique.HostFieldGetData("EFDHTI").equalsIgnoreCase("1"));
    // EFPHTI.setSelected(lexique.HostFieldGetData("EFPHTI").equalsIgnoreCase("1"));
    // EFDDIS.setSelected(lexique.HostFieldGetData("EFDDIS").equalsIgnoreCase("1"));
    // EFPDIS.setSelected(lexique.HostFieldGetData("EFPDIS").equalsIgnoreCase("1"));
    // EFDMSD.setSelected(lexique.HostFieldGetData("EFDMSD").equalsIgnoreCase("1"));
    EFPDIS.setSelected(lexique.HostFieldGetData("EFPDIS").equalsIgnoreCase("1"));
    // EFPMAG.setSelected(lexique.HostFieldGetData("EFDMAG").equalsIgnoreCase("1"));
    // EFDMAG.setSelected(lexique.HostFieldGetData("EFPMAG").equalsIgnoreCase("1"));
    // EFPMSD.setSelected(lexique.HostFieldGetData("EFPMSD").equalsIgnoreCase("1"));
    // EFBA6.setSelected(lexique.HostFieldGetData("EFBA6").equalsIgnoreCase("1"));
    // EFBA5.setSelected(lexique.HostFieldGetData("EFBA5").equalsIgnoreCase("1"));
    // EFBA4.setSelected(lexique.HostFieldGetData("EFBA4").equalsIgnoreCase("1"));
    // EFBA3.setSelected(lexique.HostFieldGetData("EFBA3").equalsIgnoreCase("1"));
    // EFBA2.setSelected(lexique.HostFieldGetData("EFBA2").equalsIgnoreCase("1"));
    // EFBA1.setSelected(lexique.HostFieldGetData("EFBA1").equalsIgnoreCase("1"));
    // EFREP.setSelectedIndex(getIndice("EFREP", EFREP_Value));
    EFPVTE.setVisible(false);
    EFDHTI.setVisible(false);
    EFDMSD.setVisible(false);
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@ de @LOCGRP/-1/@"));
    
    

    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (EFDDI5.isSelected())
    // lexique.HostFieldPutData("EFDDI5", 0, "1");
    // else
    // lexique.HostFieldPutData("EFDDI5", 0, " ");
    // if (EFPDI5.isSelected())
    // lexique.HostFieldPutData("EFPDI5", 0, "1");
    // else
    // lexique.HostFieldPutData("EFPDI5", 0, " ");
    // if (EFDVTE.isSelected())
    // lexique.HostFieldPutData("EFDVTE", 0, "1");
    // else
    // lexique.HostFieldPutData("EFDVTE", 0, " ");
    // if (EFPVTE.isSelected())
    // lexique.HostFieldPutData("EFPVTE", 0, "1");
    // else
    // lexique.HostFieldPutData("EFPVTE", 0, " ");
    // if (EFDHTI.isSelected())
    // lexique.HostFieldPutData("EFDHTI", 0, "1");
    // else
    // lexique.HostFieldPutData("EFDHTI", 0, " ");
    // if (EFPHTI.isSelected())
    // lexique.HostFieldPutData("EFPHTI", 0, "1");
    // else
    // lexique.HostFieldPutData("EFPHTI", 0, " ");
    // if (EFDDIS.isSelected())
    // lexique.HostFieldPutData("EFDDIS", 0, "1");
    // else
    // lexique.HostFieldPutData("EFDDIS", 0, " ");
    // if (EFPDIS.isSelected())
    // lexique.HostFieldPutData("EFPDIS", 0, "1");
    // else
    // lexique.HostFieldPutData("EFPDIS", 0, " ");
    // if (EFDMSD.isSelected())
    // lexique.HostFieldPutData("EFDMSD", 0, "1");
    // else
    // lexique.HostFieldPutData("EFDMSD", 0, " ");
    if (EFPDIS.isSelected()) {
      lexique.HostFieldPutData("EFPDIS", 0, "1");
    }
    else {
      lexique.HostFieldPutData("EFPDIS", 0, " ");
      // if (EFPMAG.isSelected())
      // lexique.HostFieldPutData("EFPMAG", 0, "1");
      // else
      // lexique.HostFieldPutData("EFPMAG", 0, " ");
      // if (EFDMAG.isSelected())
      // lexique.HostFieldPutData("EFDMAG", 0, "1");
      // else
      // lexique.HostFieldPutData("EFDMAG", 0, " ");
      // if (EFPMSD.isSelected())
      // lexique.HostFieldPutData("EFPMSD", 0, "1");
      // else
      // lexique.HostFieldPutData("EFPMSD", 0, " ");
      // if (EFBA6.isSelected())
      // lexique.HostFieldPutData("EFBA6", 0, "1");
      // else
      // lexique.HostFieldPutData("EFBA6", 0, " ");
      // if (EFBA5.isSelected())
      // lexique.HostFieldPutData("EFBA5", 0, "1");
      // else
      // lexique.HostFieldPutData("EFBA5", 0, " ");
      // if (EFBA4.isSelected())
      // lexique.HostFieldPutData("EFBA4", 0, "1");
      // else
      // lexique.HostFieldPutData("EFBA4", 0, " ");
      // if (EFBA3.isSelected())
      // lexique.HostFieldPutData("EFBA3", 0, "1");
      // else
      // lexique.HostFieldPutData("EFBA3", 0, " ");
      // if (EFBA2.isSelected())
      // lexique.HostFieldPutData("EFBA2", 0, "1");
      // else
      // lexique.HostFieldPutData("EFBA2", 0, " ");
      // if (EFBA1.isSelected())
      // lexique.HostFieldPutData("EFBA1", 0, "1");
      // else
      // lexique.HostFieldPutData("EFBA1", 0, " ");
      // lexique.HostFieldPutData("EFREP", 0, EFREP_Value[EFREP.getSelectedIndex()]);
    }
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_48 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    panel1 = new JPanel();
    OBJ_52 = new JLabel();
    EFLIB = new XRiTextField();
    OBJ_55 = new JLabel();
    EFBA1 = new XRiCheckBox();
    EFBA2 = new XRiCheckBox();
    EFBA3 = new XRiCheckBox();
    EFBA4 = new XRiCheckBox();
    EFBA5 = new XRiCheckBox();
    EFBA6 = new XRiCheckBox();
    EFREP = new XRiComboBox();
    OBJ_63 = new JLabel();
    panel2 = new JPanel();
    OBJ_72 = new JLabel();
    OBJ_75 = new JLabel();
    OBJ_79 = new JLabel();
    OBJ_82 = new JLabel();
    OBJ_85 = new JLabel();
    OBJ_101 = new JLabel();
    OBJ_91 = new JLabel();
    OBJ_69 = new JLabel();
    OBJ_68 = new JLabel();
    OBJ_94 = new JLabel();
    OBJ_92 = new JLabel();
    EFCOD = new XRiTextField();
    EFCOC = new XRiTextField();
    OBJ_71 = new JPanel();
    EFPMAG = new XRiCheckBox();
    EFDMAG = new XRiCheckBox();
    EFPMSD = new XRiCheckBox();
    EFDMSD = new XRiCheckBox();
    EFPDIS = new XRiCheckBox();
    EFDDIS = new XRiCheckBox();
    EFPHTI = new XRiCheckBox();
    EFDHTI = new XRiCheckBox();
    EFPVTE = new XRiCheckBox();
    EFDVTE = new XRiCheckBox();
    EFPDI5 = new XRiCheckBox();
    EFDDI5 = new XRiCheckBox();
    OBJ_70 = new JPanel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des achats");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44 ----
          OBJ_44.setText("Etablissement");
          OBJ_44.setName("OBJ_44");

          //---- INDETB ----
          INDETB.setName("INDETB");

          //---- OBJ_46 ----
          OBJ_46.setText("Code");
          OBJ_46.setName("OBJ_46");

          //---- INDTYP ----
          INDTYP.setName("INDTYP");

          //---- OBJ_48 ----
          OBJ_48.setText("Ordre");
          OBJ_48.setName("OBJ_48");

          //---- INDIND ----
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(513, Short.MAX_VALUE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(800, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("El\u00e9ment de frais");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //======== panel1 ========
            {
              panel1.setBorder(new TitledBorder(""));
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- OBJ_52 ----
              OBJ_52.setText("Libell\u00e9");
              OBJ_52.setName("OBJ_52");
              panel1.add(OBJ_52);
              OBJ_52.setBounds(40, 13, 61, 22);

              //---- EFLIB ----
              EFLIB.setComponentPopupMenu(BTD);
              EFLIB.setName("EFLIB");
              panel1.add(EFLIB);
              EFLIB.setBounds(145, 10, 310, EFLIB.getPreferredSize().height);

              //---- OBJ_55 ----
              OBJ_55.setText("Cumul dans base(s)");
              OBJ_55.setName("OBJ_55");
              panel1.add(OBJ_55);
              OBJ_55.setBounds(40, 50, 123, 20);

              //---- EFBA1 ----
              EFBA1.setText("1");
              EFBA1.setComponentPopupMenu(BTD);
              EFBA1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              EFBA1.setName("EFBA1");
              panel1.add(EFBA1);
              EFBA1.setBounds(195, 50, 32, 20);

              //---- EFBA2 ----
              EFBA2.setText("2");
              EFBA2.setComponentPopupMenu(BTD);
              EFBA2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              EFBA2.setName("EFBA2");
              panel1.add(EFBA2);
              EFBA2.setBounds(240, 50, 32, 20);

              //---- EFBA3 ----
              EFBA3.setText("3");
              EFBA3.setComponentPopupMenu(BTD);
              EFBA3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              EFBA3.setName("EFBA3");
              panel1.add(EFBA3);
              EFBA3.setBounds(285, 50, 32, 20);

              //---- EFBA4 ----
              EFBA4.setText("4");
              EFBA4.setComponentPopupMenu(BTD);
              EFBA4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              EFBA4.setName("EFBA4");
              panel1.add(EFBA4);
              EFBA4.setBounds(330, 50, 32, 20);

              //---- EFBA5 ----
              EFBA5.setText("5");
              EFBA5.setComponentPopupMenu(BTD);
              EFBA5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              EFBA5.setName("EFBA5");
              panel1.add(EFBA5);
              EFBA5.setBounds(375, 50, 32, 20);

              //---- EFBA6 ----
              EFBA6.setText("6");
              EFBA6.setComponentPopupMenu(BTD);
              EFBA6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              EFBA6.setName("EFBA6");
              panel1.add(EFBA6);
              EFBA6.setBounds(420, 50, 32, 20);

              //---- EFREP ----
              EFREP.setModel(new DefaultComboBoxModel(new String[] {
                "",
                "Imput\u00e9, non r\u00e9parti",
                "Imput\u00e9, r\u00e9parti au prorata des montants",
                "Imput\u00e9, r\u00e9parti au prorata des quantit\u00e9s",
                "Imput\u00e9, r\u00e9parti au prorata des poids",
                "Imput\u00e9, r\u00e9parti au prorata des volumes",
                "Double r\u00e9partition : au poids et au montant",
                "Double r\u00e9partition : au volume et au montant"
              }));
              EFREP.setComponentPopupMenu(BTD);
              EFREP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              EFREP.setName("EFREP");
              panel1.add(EFREP);
              EFREP.setBounds(285, 85, 350, EFREP.getPreferredSize().height);

              //---- OBJ_63 ----
              OBJ_63.setText("R\u00e9partition frais imput\u00e9s par facture");
              OBJ_63.setName("OBJ_63");
              panel1.add(OBJ_63);
              OBJ_63.setBounds(40, 90, 240, 16);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel1);
            panel1.setBounds(15, 10, 735, 140);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("A calculer pour"));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- OBJ_72 ----
              OBJ_72.setText("Facture / magasin ordinaire");
              OBJ_72.setName("OBJ_72");
              panel2.add(OBJ_72);
              OBJ_72.setBounds(40, 65, 308, 16);

              //---- OBJ_75 ----
              OBJ_75.setText("Facture / magasin sous douane");
              OBJ_75.setName("OBJ_75");
              panel2.add(OBJ_75);
              OBJ_75.setBounds(40, 95, 308, 16);

              //---- OBJ_79 ----
              OBJ_79.setText("Transfert magasin sous douane vers d\u00e9p\u00f4t");
              OBJ_79.setName("OBJ_79");
              panel2.add(OBJ_79);
              OBJ_79.setBounds(40, 130, 308, 16);

              //---- OBJ_82 ----
              OBJ_82.setText("Facture/magasin HTGI");
              OBJ_82.setName("OBJ_82");
              panel2.add(OBJ_82);
              OBJ_82.setBounds(40, 190, 308, 16);

              //---- OBJ_85 ----
              OBJ_85.setText("Vente export");
              OBJ_85.setName("OBJ_85");
              panel2.add(OBJ_85);
              OBJ_85.setBounds(40, 220, 308, 16);

              //---- OBJ_101 ----
              OBJ_101.setText("Transfert magasin sous douane vers d\u00e9p\u00f4t HTGI");
              OBJ_101.setName("OBJ_101");
              panel2.add(OBJ_101);
              OBJ_101.setBounds(40, 160, 308, 16);

              //---- OBJ_91 ----
              OBJ_91.setText("Postes de comptabilisation");
              OBJ_91.setName("OBJ_91");
              panel2.add(OBJ_91);
              OBJ_91.setBounds(40, 290, 176, 20);

              //---- OBJ_69 ----
              OBJ_69.setText("Imputation douane");
              OBJ_69.setName("OBJ_69");
              panel2.add(OBJ_69);
              OBJ_69.setBounds(530, 30, 113, 20);

              //---- OBJ_68 ----
              OBJ_68.setText("Imputation PRV");
              OBJ_68.setName("OBJ_68");
              panel2.add(OBJ_68);
              OBJ_68.setBounds(390, 30, 95, 20);

              //---- OBJ_94 ----
              OBJ_94.setText("Cr\u00e9dit");
              OBJ_94.setName("OBJ_94");
              panel2.add(OBJ_94);
              OBJ_94.setBounds(430, 290, 38, 20);

              //---- OBJ_92 ----
              OBJ_92.setText("D\u00e9bit");
              OBJ_92.setName("OBJ_92");
              panel2.add(OBJ_92);
              OBJ_92.setBounds(280, 290, 35, 20);

              //---- EFCOD ----
              EFCOD.setComponentPopupMenu(BTD);
              EFCOD.setName("EFCOD");
              panel2.add(EFCOD);
              EFCOD.setBounds(330, 286, 40, EFCOD.getPreferredSize().height);

              //---- EFCOC ----
              EFCOC.setComponentPopupMenu(BTD);
              EFCOC.setName("EFCOC");
              panel2.add(EFCOC);
              EFCOC.setBounds(480, 286, 40, EFCOC.getPreferredSize().height);

              //======== OBJ_71 ========
              {
                OBJ_71.setBackground(new Color(90, 90, 90));
                OBJ_71.setName("OBJ_71");
                OBJ_71.setLayout(null);
              }
              panel2.add(OBJ_71);
              OBJ_71.setBounds(370, 55, 283, 1);

              //---- EFPMAG ----
              EFPMAG.setText("");
              EFPMAG.setComponentPopupMenu(BTD);
              EFPMAG.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              EFPMAG.setName("EFPMAG");
              panel2.add(EFPMAG);
              EFPMAG.setBounds(425, 64, 20, 18);

              //---- EFDMAG ----
              EFDMAG.setText("");
              EFDMAG.setComponentPopupMenu(BTD);
              EFDMAG.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              EFDMAG.setName("EFDMAG");
              panel2.add(EFDMAG);
              EFDMAG.setBounds(580, 64, 20, 18);

              //---- EFPMSD ----
              EFPMSD.setText("");
              EFPMSD.setComponentPopupMenu(BTD);
              EFPMSD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              EFPMSD.setName("EFPMSD");
              panel2.add(EFPMSD);
              EFPMSD.setBounds(425, 94, 20, 18);

              //---- EFDMSD ----
              EFDMSD.setText("");
              EFDMSD.setComponentPopupMenu(BTD);
              EFDMSD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              EFDMSD.setName("EFDMSD");
              panel2.add(EFDMSD);
              EFDMSD.setBounds(580, 94, 20, 18);

              //---- EFPDIS ----
              EFPDIS.setText("");
              EFPDIS.setComponentPopupMenu(BTD);
              EFPDIS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              EFPDIS.setName("EFPDIS");
              panel2.add(EFPDIS);
              EFPDIS.setBounds(425, 129, 20, 18);

              //---- EFDDIS ----
              EFDDIS.setText("");
              EFDDIS.setComponentPopupMenu(BTD);
              EFDDIS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              EFDDIS.setName("EFDDIS");
              panel2.add(EFDDIS);
              EFDDIS.setBounds(580, 129, 20, 18);

              //---- EFPHTI ----
              EFPHTI.setText("");
              EFPHTI.setComponentPopupMenu(BTD);
              EFPHTI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              EFPHTI.setName("EFPHTI");
              panel2.add(EFPHTI);
              EFPHTI.setBounds(425, 189, 20, 18);

              //---- EFDHTI ----
              EFDHTI.setText("");
              EFDHTI.setComponentPopupMenu(BTD);
              EFDHTI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              EFDHTI.setName("EFDHTI");
              panel2.add(EFDHTI);
              EFDHTI.setBounds(580, 189, 20, 18);

              //---- EFPVTE ----
              EFPVTE.setText("");
              EFPVTE.setComponentPopupMenu(BTD);
              EFPVTE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              EFPVTE.setName("EFPVTE");
              panel2.add(EFPVTE);
              EFPVTE.setBounds(425, 219, 20, 18);

              //---- EFDVTE ----
              EFDVTE.setText("");
              EFDVTE.setComponentPopupMenu(BTD);
              EFDVTE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              EFDVTE.setName("EFDVTE");
              panel2.add(EFDVTE);
              EFDVTE.setBounds(580, 219, 20, 18);

              //---- EFPDI5 ----
              EFPDI5.setText("x");
              EFPDI5.setComponentPopupMenu(BTD);
              EFPDI5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              EFPDI5.setName("EFPDI5");
              panel2.add(EFPDI5);
              EFPDI5.setBounds(425, 159, 20, 18);

              //---- EFDDI5 ----
              EFDDI5.setText("x");
              EFDDI5.setComponentPopupMenu(BTD);
              EFDDI5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              EFDDI5.setName("EFDDI5");
              panel2.add(EFDDI5);
              EFDDI5.setBounds(580, 159, 20, 18);

              //======== OBJ_70 ========
              {
                OBJ_70.setBackground(new Color(90, 90, 90));
                OBJ_70.setName("OBJ_70");
                OBJ_70.setLayout(null);
              }
              panel2.add(OBJ_70);
              OBJ_70.setBounds(509, 35, 1, 215);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel2);
            panel2.setBounds(15, 160, 735, 335);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap(17, Short.MAX_VALUE)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 769, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 541, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(24, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44;
  private XRiTextField INDETB;
  private JLabel OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_48;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JPanel panel1;
  private JLabel OBJ_52;
  private XRiTextField EFLIB;
  private JLabel OBJ_55;
  private XRiCheckBox EFBA1;
  private XRiCheckBox EFBA2;
  private XRiCheckBox EFBA3;
  private XRiCheckBox EFBA4;
  private XRiCheckBox EFBA5;
  private XRiCheckBox EFBA6;
  private XRiComboBox EFREP;
  private JLabel OBJ_63;
  private JPanel panel2;
  private JLabel OBJ_72;
  private JLabel OBJ_75;
  private JLabel OBJ_79;
  private JLabel OBJ_82;
  private JLabel OBJ_85;
  private JLabel OBJ_101;
  private JLabel OBJ_91;
  private JLabel OBJ_69;
  private JLabel OBJ_68;
  private JLabel OBJ_94;
  private JLabel OBJ_92;
  private XRiTextField EFCOD;
  private XRiTextField EFCOC;
  private JPanel OBJ_71;
  private XRiCheckBox EFPMAG;
  private XRiCheckBox EFDMAG;
  private XRiCheckBox EFPMSD;
  private XRiCheckBox EFDMSD;
  private XRiCheckBox EFPDIS;
  private XRiCheckBox EFDDIS;
  private XRiCheckBox EFPHTI;
  private XRiCheckBox EFDHTI;
  private XRiCheckBox EFPVTE;
  private XRiCheckBox EFDVTE;
  private XRiCheckBox EFPDI5;
  private XRiCheckBox EFDDI5;
  private JPanel OBJ_70;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
