
package ri.serien.libecranrpg.vgam.VGAM50FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VGAM50FM_A4 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] _WTP01_Top = { "WTP01", };
  private String[] _WTP01_Title = { "TITLD", };
  private String[][] _WTP01_Data = { { "LR01", }, };
  private int[] _WTP01_Width = { 576, };
  private String[] _WTP02_Top = { "WTP02", };
  private String[] _WTP02_Title = { "TITLD", };
  private String[][] _WTP02_Data = { { "LR02", }, };
  private int[] _WTP02_Width = { 576, };
  private String[] _WTP03_Top = { "WTP03", };
  private String[] _WTP03_Title = { "TITLD", };
  private String[][] _WTP03_Data = { { "LR03", }, };
  private int[] _WTP03_Width = { 576, };
  
  public VGAM50FM_A4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
    WTP03.setAspectTable(_WTP03_Top, _WTP03_Title, _WTP03_Data, _WTP03_Width, false, null, null, null, null);
    WTP02.setAspectTable(_WTP02_Top, _WTP02_Title, _WTP02_Data, _WTP02_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    A1UNS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNS@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST, LIST.get_LIST_Title_Data_Brut(), _WTP01_Top);
    // majTable( WTP02, WTP02.get_LIST_Title_Data_Brut(), _WTP02_Top);
    // majTable( WTP03, WTP03.get_LIST_Title_Data_Brut(), _WTP03_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    A1UNS.setVisible(lexique.isPresent("A1UNS"));
    WQTE.setEnabled(lexique.isPresent("WQTE"));
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Meilleure(s) condition(s) en prix, délai et qualité : "
        + lexique.HostFieldGetData("A1ART").trim() + " - " + lexique.HostFieldGetData("A1LIB")));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void CHOIXActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "1", "Enter");
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "2", "Enter");
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "4", "Enter");
    WTP01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "5", "Enter");
    WTP01.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "6", "Enter");
    WTP01.setValeurTop("6");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "7", "Enter");
    WTP01.setValeurTop("7");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "8", "Enter");
    WTP01.setValeurTop("8");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "P", "Enter");
    WTP01.setValeurTop("P");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void CHOIX2ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST1, _WTP02_Top, "1", "Enter");
    WTP02.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST1, _WTP02_Top, "2", "Enter");
    WTP02.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST1, _WTP02_Top, "4", "Enter");
    WTP02.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_27ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST1, _WTP02_Top, "5", "Enter");
    WTP02.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_28ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST1, _WTP02_Top, "6", "Enter");
    WTP02.setValeurTop("6");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_29ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST1, _WTP02_Top, "7", "Enter");
    WTP02.setValeurTop("7");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_30ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST1, _WTP02_Top, "8", "Enter");
    WTP02.setValeurTop("8");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_31ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST1, _WTP02_Top, "P", "Enter");
    WTP02.setValeurTop("P");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void CHOIX3ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST2, _WTP03_Top, "1", "Enter");
    WTP03.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_35ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST2, _WTP03_Top, "2", "Enter");
    WTP03.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_36ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST2, _WTP03_Top, "4", "Enter");
    WTP03.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_37ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST2, _WTP03_Top, "5", "Enter");
    WTP03.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_38ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST2, _WTP03_Top, "6", "Enter");
    WTP03.setValeurTop("6");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_39ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST2, _WTP03_Top, "7", "Enter");
    WTP03.setValeurTop("7");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_40ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST2, _WTP03_Top, "8", "Enter");
    WTP03.setValeurTop("8");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_41ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST2, _WTP03_Top, "P", "Enter");
    WTP03.setValeurTop("P");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _WTP01_Top, "1", "ENTER", e);
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void WTP02MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST1, _WTP02_Top, "1", "ENTER", e);
    if (WTP02.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void WTP03MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST2, _WTP03_Top, "1", "ENTER", e);
    if (WTP03.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    OBJ_33 = new JLabel();
    WQTE = new XRiTextField();
    A1UNS = new RiZoneSortie();
    panel1 = new JPanel();
    SCROLLPANE_WTP01 = new JScrollPane();
    WTP01 = new XRiTable();
    panel2 = new JPanel();
    SCROLLPANE_WTP02 = new JScrollPane();
    WTP02 = new XRiTable();
    panel3 = new JPanel();
    SCROLLPANE_WTP03 = new JScrollPane();
    WTP03 = new XRiTable();
    BTD = new JPopupMenu();
    CHOIX = new JMenuItem();
    OBJ_17 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    OBJ_20 = new JMenuItem();
    OBJ_21 = new JMenuItem();
    OBJ_22 = new JMenuItem();
    OBJ_23 = new JMenuItem();
    OBJ_24 = new JMenuItem();
    BTD2 = new JPopupMenu();
    CHOIX2 = new JMenuItem();
    OBJ_25 = new JMenuItem();
    OBJ_26 = new JMenuItem();
    OBJ_27 = new JMenuItem();
    OBJ_28 = new JMenuItem();
    OBJ_29 = new JMenuItem();
    OBJ_30 = new JMenuItem();
    OBJ_31 = new JMenuItem();
    OBJ_34 = new JMenuItem();
    BTD3 = new JPopupMenu();
    CHOIX3 = new JMenuItem();
    OBJ_35 = new JMenuItem();
    OBJ_36 = new JMenuItem();
    OBJ_37 = new JMenuItem();
    OBJ_38 = new JMenuItem();
    OBJ_39 = new JMenuItem();
    OBJ_40 = new JMenuItem();
    OBJ_41 = new JMenuItem();
    OBJ_43 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(810, 335));
    setPreferredSize(new Dimension(810, 335));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //---- OBJ_33 ----
        OBJ_33.setText("Quantit\u00e9");
        OBJ_33.setName("OBJ_33");
        p_contenu.add(OBJ_33);
        OBJ_33.setBounds(25, 15, 70, 18);

        //---- WQTE ----
        WQTE.setName("WQTE");
        p_contenu.add(WQTE);
        WQTE.setBounds(95, 10, 66, WQTE.getPreferredSize().height);

        //---- A1UNS ----
        A1UNS.setText("@A1UNS@");
        A1UNS.setName("A1UNS");
        p_contenu.add(A1UNS);
        A1UNS.setBounds(165, 12, 34, A1UNS.getPreferredSize().height);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Meilleur prix"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //======== SCROLLPANE_WTP01 ========
          {
            SCROLLPANE_WTP01.setComponentPopupMenu(BTD);
            SCROLLPANE_WTP01.setName("SCROLLPANE_WTP01");

            //---- WTP01 ----
            WTP01.setComponentPopupMenu(BTD);
            WTP01.setName("WTP01");
            WTP01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                WTP01MouseClicked(e);
              }
            });
            SCROLLPANE_WTP01.setViewportView(WTP01);
          }
          panel1.add(SCROLLPANE_WTP01);
          SCROLLPANE_WTP01.setBounds(15, 25, 590, 50);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(5, 45, 620, 90);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Meilleur d\u00e9lai"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //======== SCROLLPANE_WTP02 ========
          {
            SCROLLPANE_WTP02.setComponentPopupMenu(BTD);
            SCROLLPANE_WTP02.setName("SCROLLPANE_WTP02");

            //---- WTP02 ----
            WTP02.setComponentPopupMenu(BTD2);
            WTP02.setName("WTP02");
            WTP02.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                WTP02MouseClicked(e);
              }
            });
            SCROLLPANE_WTP02.setViewportView(WTP02);
          }
          panel2.add(SCROLLPANE_WTP02);
          SCROLLPANE_WTP02.setBounds(15, 25, 590, 50);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel2);
        panel2.setBounds(5, 135, 620, 90);

        //======== panel3 ========
        {
          panel3.setBorder(new TitledBorder("Meilleure qualit\u00e9"));
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);

          //======== SCROLLPANE_WTP03 ========
          {
            SCROLLPANE_WTP03.setComponentPopupMenu(BTD3);
            SCROLLPANE_WTP03.setName("SCROLLPANE_WTP03");

            //---- WTP03 ----
            WTP03.setName("WTP03");
            WTP03.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                WTP03MouseClicked(e);
              }
            });
            SCROLLPANE_WTP03.setViewportView(WTP03);
          }
          panel3.add(SCROLLPANE_WTP03);
          SCROLLPANE_WTP03.setBounds(15, 25, 590, 50);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel3.getComponentCount(); i++) {
              Rectangle bounds = panel3.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel3.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel3.setMinimumSize(preferredSize);
            panel3.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel3);
        panel3.setBounds(5, 225, 620, 90);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOIX ----
      CHOIX.setText("Choisir");
      CHOIX.setName("CHOIX");
      CHOIX.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOIXActionPerformed(e);
        }
      });
      BTD.add(CHOIX);

      //---- OBJ_17 ----
      OBJ_17.setText("Modifier");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_18 ----
      OBJ_18.setText("Annuler");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_19 ----
      OBJ_19.setText("Consulter");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_20 ----
      OBJ_20.setText("D\u00e9tail");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);

      //---- OBJ_21 ----
      OBJ_21.setText("Historique");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);

      //---- OBJ_22 ----
      OBJ_22.setText("Bloc-notes");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);

      //---- OBJ_23 ----
      OBJ_23.setText("Fournisseur principal");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD.add(OBJ_23);
      BTD.addSeparator();

      //---- OBJ_24 ----
      OBJ_24.setText("Aide en ligne");
      OBJ_24.setName("OBJ_24");
      OBJ_24.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_24ActionPerformed(e);
        }
      });
      BTD.add(OBJ_24);
    }

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- CHOIX2 ----
      CHOIX2.setText("Choisir");
      CHOIX2.setName("CHOIX2");
      CHOIX2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOIX2ActionPerformed(e);
        }
      });
      BTD2.add(CHOIX2);

      //---- OBJ_25 ----
      OBJ_25.setText("Modifier");
      OBJ_25.setName("OBJ_25");
      OBJ_25.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_25ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_25);

      //---- OBJ_26 ----
      OBJ_26.setText("Annuler");
      OBJ_26.setName("OBJ_26");
      OBJ_26.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_26ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_26);

      //---- OBJ_27 ----
      OBJ_27.setText("Consulter");
      OBJ_27.setName("OBJ_27");
      OBJ_27.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_27ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_27);

      //---- OBJ_28 ----
      OBJ_28.setText("D\u00e9tail");
      OBJ_28.setName("OBJ_28");
      OBJ_28.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_28ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_28);

      //---- OBJ_29 ----
      OBJ_29.setText("Historique");
      OBJ_29.setName("OBJ_29");
      OBJ_29.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_29ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_29);

      //---- OBJ_30 ----
      OBJ_30.setText("Bloc-notes");
      OBJ_30.setName("OBJ_30");
      OBJ_30.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_30ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_30);

      //---- OBJ_31 ----
      OBJ_31.setText("Fournisseur principal");
      OBJ_31.setName("OBJ_31");
      OBJ_31.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_31ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_31);
      BTD2.addSeparator();

      //---- OBJ_34 ----
      OBJ_34.setText("Aide en ligne");
      OBJ_34.setName("OBJ_34");
      OBJ_34.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_24ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_34);
    }

    //======== BTD3 ========
    {
      BTD3.setName("BTD3");

      //---- CHOIX3 ----
      CHOIX3.setText("Choisir");
      CHOIX3.setName("CHOIX3");
      CHOIX3.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOIX3ActionPerformed(e);
        }
      });
      BTD3.add(CHOIX3);

      //---- OBJ_35 ----
      OBJ_35.setText("Modifier");
      OBJ_35.setName("OBJ_35");
      OBJ_35.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_35ActionPerformed(e);
        }
      });
      BTD3.add(OBJ_35);

      //---- OBJ_36 ----
      OBJ_36.setText("Annuler");
      OBJ_36.setName("OBJ_36");
      OBJ_36.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_36ActionPerformed(e);
        }
      });
      BTD3.add(OBJ_36);

      //---- OBJ_37 ----
      OBJ_37.setText("Consulter");
      OBJ_37.setName("OBJ_37");
      OBJ_37.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_37ActionPerformed(e);
        }
      });
      BTD3.add(OBJ_37);

      //---- OBJ_38 ----
      OBJ_38.setText("D\u00e9tail");
      OBJ_38.setName("OBJ_38");
      OBJ_38.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_38ActionPerformed(e);
        }
      });
      BTD3.add(OBJ_38);

      //---- OBJ_39 ----
      OBJ_39.setText("Historique");
      OBJ_39.setName("OBJ_39");
      OBJ_39.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_39ActionPerformed(e);
        }
      });
      BTD3.add(OBJ_39);

      //---- OBJ_40 ----
      OBJ_40.setText("Bloc-notes");
      OBJ_40.setName("OBJ_40");
      OBJ_40.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_40ActionPerformed(e);
        }
      });
      BTD3.add(OBJ_40);

      //---- OBJ_41 ----
      OBJ_41.setText("Fournisseur principal");
      OBJ_41.setName("OBJ_41");
      OBJ_41.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_41ActionPerformed(e);
        }
      });
      BTD3.add(OBJ_41);
      BTD3.addSeparator();

      //---- OBJ_43 ----
      OBJ_43.setText("Aide en ligne");
      OBJ_43.setName("OBJ_43");
      OBJ_43.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_24ActionPerformed(e);
        }
      });
      BTD3.add(OBJ_43);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private JLabel OBJ_33;
  private XRiTextField WQTE;
  private RiZoneSortie A1UNS;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_WTP01;
  private XRiTable WTP01;
  private JPanel panel2;
  private JScrollPane SCROLLPANE_WTP02;
  private XRiTable WTP02;
  private JPanel panel3;
  private JScrollPane SCROLLPANE_WTP03;
  private XRiTable WTP03;
  private JPopupMenu BTD;
  private JMenuItem CHOIX;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_24;
  private JPopupMenu BTD2;
  private JMenuItem CHOIX2;
  private JMenuItem OBJ_25;
  private JMenuItem OBJ_26;
  private JMenuItem OBJ_27;
  private JMenuItem OBJ_28;
  private JMenuItem OBJ_29;
  private JMenuItem OBJ_30;
  private JMenuItem OBJ_31;
  private JMenuItem OBJ_34;
  private JPopupMenu BTD3;
  private JMenuItem CHOIX3;
  private JMenuItem OBJ_35;
  private JMenuItem OBJ_36;
  private JMenuItem OBJ_37;
  private JMenuItem OBJ_38;
  private JMenuItem OBJ_39;
  private JMenuItem OBJ_40;
  private JMenuItem OBJ_41;
  private JMenuItem OBJ_43;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
