
package ri.serien.libecranrpg.vgam.VGAM15FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGAM15FM_C4 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  /**
   * Constructeur.
   */
  public VGAM15FM_C4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Titre
    setTitle("Ligne commentaire");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    LAIN3.setEnabled(lexique.isPresent("LAIN3"));
    WCOD.setVisible(lexique.isPresent("WCOD"));
    WNLI.setVisible(lexique.isPresent("WNLI"));
    WARTT.setVisible(lexique.isPresent("WARTT"));
    LALIB1.setEnabled(lexique.isPresent("LALIB1"));
    LALIB2.setEnabled(lexique.isPresent("LALIB2"));
    LALIB3.setEnabled(lexique.isPresent("LALIB3"));
    LALIB4.setEnabled(lexique.isPresent("LALIB4"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    LALIB1 = new XRiTextField();
    LALIB2 = new XRiTextField();
    LALIB3 = new XRiTextField();
    LALIB4 = new XRiTextField();
    WARTT = new XRiTextField();
    OBJ_24 = new JLabel();
    OBJ_18 = new JLabel();
    OBJ_19 = new JLabel();
    WNLI = new XRiTextField();
    WCOD = new XRiTextField();
    LAIN3 = new XRiTextField();
    OBJ_25 = new JLabel();
    OBJ_27 = new JLabel();
    OBJ_29 = new JLabel();
    OBJ_31 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(950, 210));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder(""));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- LALIB1 ----
          LALIB1.setComponentPopupMenu(BTD);
          LALIB1.setName("LALIB1");
          panel2.add(LALIB1);
          LALIB1.setBounds(420, 45, 310, LALIB1.getPreferredSize().height);

          //---- LALIB2 ----
          LALIB2.setComponentPopupMenu(BTD);
          LALIB2.setName("LALIB2");
          panel2.add(LALIB2);
          LALIB2.setBounds(420, 75, 310, LALIB2.getPreferredSize().height);

          //---- LALIB3 ----
          LALIB3.setComponentPopupMenu(BTD);
          LALIB3.setName("LALIB3");
          panel2.add(LALIB3);
          LALIB3.setBounds(420, 105, 310, LALIB3.getPreferredSize().height);

          //---- LALIB4 ----
          LALIB4.setComponentPopupMenu(BTD);
          LALIB4.setName("LALIB4");
          panel2.add(LALIB4);
          LALIB4.setBounds(420, 135, 310, LALIB4.getPreferredSize().height);

          //---- WARTT ----
          WARTT.setComponentPopupMenu(BTD);
          WARTT.setName("WARTT");
          panel2.add(WARTT);
          WARTT.setBounds(85, 45, 186, WARTT.getPreferredSize().height);

          //---- OBJ_24 ----
          OBJ_24.setText("Commentaire");
          OBJ_24.setFont(OBJ_24.getFont().deriveFont(OBJ_24.getFont().getStyle() | Font.BOLD));
          OBJ_24.setName("OBJ_24");
          panel2.add(OBJ_24);
          OBJ_24.setBounds(420, 24, 100, 20);

          //---- OBJ_18 ----
          OBJ_18.setText("C     N\u00b0Li");
          OBJ_18.setFont(OBJ_18.getFont().deriveFont(OBJ_18.getFont().getStyle() | Font.BOLD));
          OBJ_18.setName("OBJ_18");
          panel2.add(OBJ_18);
          OBJ_18.setBounds(25, 24, 60, 20);

          //---- OBJ_19 ----
          OBJ_19.setText("Article");
          OBJ_19.setFont(OBJ_19.getFont().deriveFont(OBJ_19.getFont().getStyle() | Font.BOLD));
          OBJ_19.setName("OBJ_19");
          panel2.add(OBJ_19);
          OBJ_19.setBounds(90, 24, 60, 20);

          //---- WNLI ----
          WNLI.setComponentPopupMenu(BTD);
          WNLI.setName("WNLI");
          panel2.add(WNLI);
          WNLI.setBounds(40, 45, 42, WNLI.getPreferredSize().height);

          //---- WCOD ----
          WCOD.setComponentPopupMenu(BTD);
          WCOD.setName("WCOD");
          panel2.add(WCOD);
          WCOD.setBounds(20, 45, 20, WCOD.getPreferredSize().height);

          //---- LAIN3 ----
          LAIN3.setComponentPopupMenu(BTD);
          LAIN3.setName("LAIN3");
          panel2.add(LAIN3);
          LAIN3.setBounds(275, 45, 20, LAIN3.getPreferredSize().height);

          //---- OBJ_25 ----
          OBJ_25.setText("1");
          OBJ_25.setFont(OBJ_25.getFont().deriveFont(OBJ_25.getFont().getStyle() | Font.BOLD));
          OBJ_25.setName("OBJ_25");
          panel2.add(OBJ_25);
          OBJ_25.setBounds(405, 50, 12, 20);

          //---- OBJ_27 ----
          OBJ_27.setText("2");
          OBJ_27.setFont(OBJ_27.getFont().deriveFont(OBJ_27.getFont().getStyle() | Font.BOLD));
          OBJ_27.setName("OBJ_27");
          panel2.add(OBJ_27);
          OBJ_27.setBounds(405, 80, 12, 20);

          //---- OBJ_29 ----
          OBJ_29.setText("3");
          OBJ_29.setFont(OBJ_29.getFont().deriveFont(OBJ_29.getFont().getStyle() | Font.BOLD));
          OBJ_29.setName("OBJ_29");
          panel2.add(OBJ_29);
          OBJ_29.setBounds(405, 110, 12, 20);

          //---- OBJ_31 ----
          OBJ_31.setText("4");
          OBJ_31.setFont(OBJ_31.getFont().deriveFont(OBJ_31.getFont().getStyle() | Font.BOLD));
          OBJ_31.setName("OBJ_31");
          panel2.add(OBJ_31);
          OBJ_31.setBounds(405, 140, 12, 20);
        }
        p_contenu.add(panel2);
        panel2.setBounds(10, 10, 755, 185);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiTextField LALIB1;
  private XRiTextField LALIB2;
  private XRiTextField LALIB3;
  private XRiTextField LALIB4;
  private XRiTextField WARTT;
  private JLabel OBJ_24;
  private JLabel OBJ_18;
  private JLabel OBJ_19;
  private XRiTextField WNLI;
  private XRiTextField WCOD;
  private XRiTextField LAIN3;
  private JLabel OBJ_25;
  private JLabel OBJ_27;
  private JLabel OBJ_29;
  private JLabel OBJ_31;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
