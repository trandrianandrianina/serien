
package ri.serien.libecranrpg.vgam.VGAM52FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGAM52FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGAM52FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    FRNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRNOM@")).trim());
    OBJ_40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TRLIB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_70 = new JLabel();
    APCREX = new XRiTextField();
    OBJ_68 = new JLabel();
    OBJ_69 = new JLabel();
    APNUM = new XRiTextField();
    APCAM = new XRiTextField();
    APETB = new XRiTextField();
    OBJ_67 = new JLabel();
    APSUF = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    APLIB1 = new XRiTextField();
    APLIB2 = new XRiTextField();
    APLIB3 = new XRiTextField();
    FRNOM = new RiZoneSortie();
    OBJ_41 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_40 = new RiZoneSortie();
    OBJ_51 = new JLabel();
    OBJ_45 = new JLabel();
    OBJ_38 = new JLabel();
    OBJ_34 = new JLabel();
    APDT1X = new XRiCalendrier();
    APDT2X = new XRiCalendrier();
    APDT3X = new XRiCalendrier();
    APDT4X = new XRiCalendrier();
    APFRS = new XRiTextField();
    APHRE1 = new XRiTextField();
    APHRE3 = new XRiTextField();
    APHRE4 = new XRiTextField();
    OBJ_43 = new JLabel();
    OBJ_49 = new JLabel();
    OBJ_53 = new JLabel();
    APCDTR = new XRiTextField();
    APCOL = new XRiTextField();
    separator1 = compFactory.createSeparator("Commentaires");
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion du planning des arrivages");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 40));
          p_tete_gauche.setMinimumSize(new Dimension(900, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_70 ----
          OBJ_70.setText("Date de cr\u00e9ation");
          OBJ_70.setName("OBJ_70");

          //---- APCREX ----
          APCREX.setComponentPopupMenu(BTD);
          APCREX.setName("APCREX");

          //---- OBJ_68 ----
          OBJ_68.setText("Num\u00e9ro de bon");
          OBJ_68.setName("OBJ_68");

          //---- OBJ_69 ----
          OBJ_69.setText("Camion");
          OBJ_69.setName("OBJ_69");

          //---- APNUM ----
          APNUM.setComponentPopupMenu(BTD);
          APNUM.setName("APNUM");

          //---- APCAM ----
          APCAM.setComponentPopupMenu(BTD);
          APCAM.setName("APCAM");

          //---- APETB ----
          APETB.setComponentPopupMenu(BTD);
          APETB.setName("APETB");

          //---- OBJ_67 ----
          OBJ_67.setText("Etablissement");
          OBJ_67.setName("OBJ_67");

          //---- APSUF ----
          APSUF.setComponentPopupMenu(BTD);
          APSUF.setName("APSUF");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(APETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(APNUM, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(APSUF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(47, 47, 47)
                .addComponent(OBJ_69, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(APCAM, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38)
                .addComponent(OBJ_70, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(APCREX, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(APETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(APNUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(APSUF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(APCAM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(APCREX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_69, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_70, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(700, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(707, 480));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- APLIB1 ----
            APLIB1.setComponentPopupMenu(BTD);
            APLIB1.setName("APLIB1");
            panel1.add(APLIB1);
            APLIB1.setBounds(25, 348, 610, APLIB1.getPreferredSize().height);

            //---- APLIB2 ----
            APLIB2.setComponentPopupMenu(BTD);
            APLIB2.setName("APLIB2");
            panel1.add(APLIB2);
            APLIB2.setBounds(25, 379, 610, APLIB2.getPreferredSize().height);

            //---- APLIB3 ----
            APLIB3.setComponentPopupMenu(BTD);
            APLIB3.setName("APLIB3");
            panel1.add(APLIB3);
            APLIB3.setBounds(25, 410, 610, APLIB3.getPreferredSize().height);

            //---- FRNOM ----
            FRNOM.setText("@FRNOM@");
            FRNOM.setName("FRNOM");
            panel1.add(FRNOM);
            FRNOM.setBounds(345, 28, 231, FRNOM.getPreferredSize().height);

            //---- OBJ_41 ----
            OBJ_41.setText("Date arrivage pr\u00e9visionnelle");
            OBJ_41.setName("OBJ_41");
            panel1.add(OBJ_41);
            OBJ_41.setBounds(25, 116, 180, 20);

            //---- OBJ_47 ----
            OBJ_47.setText("Date confirm\u00e9e arrivage");
            OBJ_47.setName("OBJ_47");
            panel1.add(OBJ_47);
            OBJ_47.setBounds(25, 202, 180, 20);

            //---- OBJ_40 ----
            OBJ_40.setText("@TRLIB@");
            OBJ_40.setName("OBJ_40");
            panel1.add(OBJ_40);
            OBJ_40.setBounds(345, 71, 161, OBJ_40.getPreferredSize().height);

            //---- OBJ_51 ----
            OBJ_51.setText("Date r\u00e9elle r\u00e9ception");
            OBJ_51.setName("OBJ_51");
            panel1.add(OBJ_51);
            OBJ_51.setBounds(25, 245, 180, 20);

            //---- OBJ_45 ----
            OBJ_45.setText("Date de confirmation");
            OBJ_45.setName("OBJ_45");
            panel1.add(OBJ_45);
            OBJ_45.setBounds(25, 159, 180, 20);

            //---- OBJ_38 ----
            OBJ_38.setText("Transporteur");
            OBJ_38.setName("OBJ_38");
            panel1.add(OBJ_38);
            OBJ_38.setBounds(25, 73, 98, 20);

            //---- OBJ_34 ----
            OBJ_34.setText("Fournisseur");
            OBJ_34.setName("OBJ_34");
            panel1.add(OBJ_34);
            OBJ_34.setBounds(25, 30, 91, 20);

            //---- APDT1X ----
            APDT1X.setComponentPopupMenu(BTD);
            APDT1X.setName("APDT1X");
            panel1.add(APDT1X);
            APDT1X.setBounds(215, 112, 105, APDT1X.getPreferredSize().height);

            //---- APDT2X ----
            APDT2X.setComponentPopupMenu(BTD);
            APDT2X.setName("APDT2X");
            panel1.add(APDT2X);
            APDT2X.setBounds(215, 155, 105, APDT2X.getPreferredSize().height);

            //---- APDT3X ----
            APDT3X.setComponentPopupMenu(BTD);
            APDT3X.setName("APDT3X");
            panel1.add(APDT3X);
            APDT3X.setBounds(215, 198, 105, APDT3X.getPreferredSize().height);

            //---- APDT4X ----
            APDT4X.setComponentPopupMenu(BTD);
            APDT4X.setName("APDT4X");
            panel1.add(APDT4X);
            APDT4X.setBounds(215, 241, 105, APDT4X.getPreferredSize().height);

            //---- APFRS ----
            APFRS.setComponentPopupMenu(BTD);
            APFRS.setName("APFRS");
            panel1.add(APFRS);
            APFRS.setBounds(240, 26, 60, APFRS.getPreferredSize().height);

            //---- APHRE1 ----
            APHRE1.setComponentPopupMenu(BTD);
            APHRE1.setName("APHRE1");
            panel1.add(APHRE1);
            APHRE1.setBounds(400, 112, 50, APHRE1.getPreferredSize().height);

            //---- APHRE3 ----
            APHRE3.setComponentPopupMenu(BTD);
            APHRE3.setName("APHRE3");
            panel1.add(APHRE3);
            APHRE3.setBounds(400, 198, 50, APHRE3.getPreferredSize().height);

            //---- APHRE4 ----
            APHRE4.setComponentPopupMenu(BTD);
            APHRE4.setName("APHRE4");
            panel1.add(APHRE4);
            APHRE4.setBounds(400, 241, 50, APHRE4.getPreferredSize().height);

            //---- OBJ_43 ----
            OBJ_43.setText("Heure");
            OBJ_43.setName("OBJ_43");
            panel1.add(OBJ_43);
            OBJ_43.setBounds(345, 116, 50, 20);

            //---- OBJ_49 ----
            OBJ_49.setText("Heure");
            OBJ_49.setName("OBJ_49");
            panel1.add(OBJ_49);
            OBJ_49.setBounds(345, 202, 50, 20);

            //---- OBJ_53 ----
            OBJ_53.setText("Heure");
            OBJ_53.setName("OBJ_53");
            panel1.add(OBJ_53);
            OBJ_53.setBounds(345, 245, 50, 20);

            //---- APCDTR ----
            APCDTR.setComponentPopupMenu(BTD);
            APCDTR.setName("APCDTR");
            panel1.add(APCDTR);
            APCDTR.setBounds(215, 69, 34, APCDTR.getPreferredSize().height);

            //---- APCOL ----
            APCOL.setComponentPopupMenu(BTD);
            APCOL.setName("APCOL");
            panel1.add(APCOL);
            APCOL.setBounds(215, 26, 20, APCOL.getPreferredSize().height);

            //---- separator1 ----
            separator1.setName("separator1");
            panel1.add(separator1);
            separator1.setBounds(20, 325, 620, separator1.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 674, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 472, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_70;
  private XRiTextField APCREX;
  private JLabel OBJ_68;
  private JLabel OBJ_69;
  private XRiTextField APNUM;
  private XRiTextField APCAM;
  private XRiTextField APETB;
  private JLabel OBJ_67;
  private XRiTextField APSUF;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField APLIB1;
  private XRiTextField APLIB2;
  private XRiTextField APLIB3;
  private RiZoneSortie FRNOM;
  private JLabel OBJ_41;
  private JLabel OBJ_47;
  private RiZoneSortie OBJ_40;
  private JLabel OBJ_51;
  private JLabel OBJ_45;
  private JLabel OBJ_38;
  private JLabel OBJ_34;
  private XRiCalendrier APDT1X;
  private XRiCalendrier APDT2X;
  private XRiCalendrier APDT3X;
  private XRiCalendrier APDT4X;
  private XRiTextField APFRS;
  private XRiTextField APHRE1;
  private XRiTextField APHRE3;
  private XRiTextField APHRE4;
  private JLabel OBJ_43;
  private JLabel OBJ_49;
  private JLabel OBJ_53;
  private XRiTextField APCDTR;
  private XRiTextField APCOL;
  private JComponent separator1;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
