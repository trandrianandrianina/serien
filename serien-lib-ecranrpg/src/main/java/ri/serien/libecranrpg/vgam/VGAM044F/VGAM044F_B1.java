
package ri.serien.libecranrpg.vgam.VGAM044F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGAM044F_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  // private String[] _LIST_Top={"OPT01", "OPT02", "OPT03", "WTP04", "OPT05"};
  
  public VGAM044F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Titre
    setTitle("Codes barres fournisseurs");
    
    bt_Supprime01.setIcon(lexique.chargerImage("images/poubelle_p.gif", true));
    bt_Supprime02.setIcon(lexique.chargerImage("images/poubelle_p.gif", true));
    bt_Supprime03.setIcon(lexique.chargerImage("images/poubelle_p.gif", true));
    bt_Supprime04.setIcon(lexique.chargerImage("images/poubelle_p.gif", true));
    bt_Supprime05.setIcon(lexique.chargerImage("images/poubelle_p.gif", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    String zone = BTD.getInvoker().getName();
    if (zone.equals("GCD01") || zone.equals("LIB01")) {
      lexique.HostFieldPutData("OPT01", 0, "P");
    }
    else if (zone.equals("GCD02") || zone.equals("LIB02")) {
      lexique.HostFieldPutData("OPT02", 0, "P");
    }
    else if (zone.equals("GCD03") || zone.equals("LIB03")) {
      lexique.HostFieldPutData("OPT03", 0, "P");
    }
    else if (zone.equals("GCD04") || zone.equals("LIB04")) {
      lexique.HostFieldPutData("OPT04", 0, "P");
    }
    else if (zone.equals("GCD05") || zone.equals("LIB05")) {
      lexique.HostFieldPutData("OPT05", 0, "P");
    }
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void bt_Supprime01ActionPerformed(ActionEvent e) {
    GCD01.setText("");
    LIB01.setText("");
    lexique.HostFieldPutData("OPT01", 0, "S");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void bt_Supprime02ActionPerformed(ActionEvent e) {
    GCD02.setText("");
    LIB02.setText("");
    lexique.HostFieldPutData("OPT02", 0, "S");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void bt_Supprime03ActionPerformed(ActionEvent e) {
    GCD03.setText("");
    LIB03.setText("");
    lexique.HostFieldPutData("OPT03", 0, "S");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void bt_Supprime04ActionPerformed(ActionEvent e) {
    GCD04.setText("");
    LIB04.setText("");
    lexique.HostFieldPutData("OPT04", 0, "S");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void bt_Supprime05ActionPerformed(ActionEvent e) {
    GCD05.setText("");
    LIB05.setText("");
    lexique.HostFieldPutData("OPT05", 0, "S");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  // EVENEMENTIEL
  // +++++++++++++++++++++++++++++++++++++++++++
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    label1 = new JLabel();
    label2 = new JLabel();
    bt_Supprime01 = new JButton();
    GCD01 = new XRiTextField();
    LIB01 = new XRiTextField();
    bt_Supprime02 = new JButton();
    GCD02 = new XRiTextField();
    LIB02 = new XRiTextField();
    bt_Supprime03 = new JButton();
    GCD03 = new XRiTextField();
    LIB03 = new XRiTextField();
    bt_Supprime04 = new JButton();
    GCD04 = new XRiTextField();
    LIB04 = new XRiTextField();
    bt_Supprime05 = new JButton();
    GCD05 = new XRiTextField();
    LIB05 = new XRiTextField();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_16 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(650, 200));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //---- label1 ----
        label1.setText("Code Barres");
        label1.setName("label1");
        p_contenu.add(label1);
        label1.setBounds(45, 10, 100, 20);

        //---- label2 ----
        label2.setText("Libell\u00e9");
        label2.setName("label2");
        p_contenu.add(label2);
        label2.setBounds(165, 10, 130, 20);

        //---- bt_Supprime01 ----
        bt_Supprime01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_Supprime01.setName("bt_Supprime01");
        bt_Supprime01.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_Supprime01ActionPerformed(e);
          }
        });
        p_contenu.add(bt_Supprime01);
        bt_Supprime01.setBounds(10, 28, 32, 32);

        //---- GCD01 ----
        GCD01.setComponentPopupMenu(BTD);
        GCD01.setName("GCD01");
        p_contenu.add(GCD01);
        GCD01.setBounds(45, 30, 116, GCD01.getPreferredSize().height);

        //---- LIB01 ----
        LIB01.setComponentPopupMenu(BTD);
        LIB01.setName("LIB01");
        p_contenu.add(LIB01);
        LIB01.setBounds(165, 30, 305, LIB01.getPreferredSize().height);

        //---- bt_Supprime02 ----
        bt_Supprime02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_Supprime02.setName("bt_Supprime02");
        bt_Supprime02.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_Supprime02ActionPerformed(e);
          }
        });
        p_contenu.add(bt_Supprime02);
        bt_Supprime02.setBounds(10, 59, 32, 32);

        //---- GCD02 ----
        GCD02.setComponentPopupMenu(BTD);
        GCD02.setName("GCD02");
        p_contenu.add(GCD02);
        GCD02.setBounds(45, 61, 116, GCD02.getPreferredSize().height);

        //---- LIB02 ----
        LIB02.setComponentPopupMenu(BTD);
        LIB02.setName("LIB02");
        p_contenu.add(LIB02);
        LIB02.setBounds(165, 61, 305, LIB02.getPreferredSize().height);

        //---- bt_Supprime03 ----
        bt_Supprime03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_Supprime03.setName("bt_Supprime03");
        bt_Supprime03.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_Supprime03ActionPerformed(e);
          }
        });
        p_contenu.add(bt_Supprime03);
        bt_Supprime03.setBounds(10, 90, 32, 32);

        //---- GCD03 ----
        GCD03.setComponentPopupMenu(BTD);
        GCD03.setName("GCD03");
        p_contenu.add(GCD03);
        GCD03.setBounds(45, 92, 116, GCD03.getPreferredSize().height);

        //---- LIB03 ----
        LIB03.setComponentPopupMenu(BTD);
        LIB03.setName("LIB03");
        p_contenu.add(LIB03);
        LIB03.setBounds(165, 92, 305, LIB03.getPreferredSize().height);

        //---- bt_Supprime04 ----
        bt_Supprime04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_Supprime04.setName("bt_Supprime04");
        bt_Supprime04.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_Supprime04ActionPerformed(e);
          }
        });
        p_contenu.add(bt_Supprime04);
        bt_Supprime04.setBounds(10, 121, 32, 32);

        //---- GCD04 ----
        GCD04.setComponentPopupMenu(BTD);
        GCD04.setName("GCD04");
        p_contenu.add(GCD04);
        GCD04.setBounds(45, 123, 116, GCD04.getPreferredSize().height);

        //---- LIB04 ----
        LIB04.setComponentPopupMenu(BTD);
        LIB04.setName("LIB04");
        p_contenu.add(LIB04);
        LIB04.setBounds(165, 123, 305, LIB04.getPreferredSize().height);

        //---- bt_Supprime05 ----
        bt_Supprime05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_Supprime05.setName("bt_Supprime05");
        bt_Supprime05.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_Supprime05ActionPerformed(e);
          }
        });
        p_contenu.add(bt_Supprime05);
        bt_Supprime05.setBounds(10, 152, 32, 32);

        //---- GCD05 ----
        GCD05.setComponentPopupMenu(BTD);
        GCD05.setName("GCD05");
        p_contenu.add(GCD05);
        GCD05.setBounds(45, 154, 116, GCD05.getPreferredSize().height);

        //---- LIB05 ----
        LIB05.setComponentPopupMenu(BTD);
        LIB05.setName("LIB05");
        p_contenu.add(LIB05);
        LIB05.setBounds(165, 154, 305, LIB05.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Code barre principal");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);
      BTD.addSeparator();

      //---- OBJ_16 ----
      OBJ_16.setText("Aide en ligne");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JLabel label1;
  private JLabel label2;
  private JButton bt_Supprime01;
  private XRiTextField GCD01;
  private XRiTextField LIB01;
  private JButton bt_Supprime02;
  private XRiTextField GCD02;
  private XRiTextField LIB02;
  private JButton bt_Supprime03;
  private XRiTextField GCD03;
  private XRiTextField LIB03;
  private JButton bt_Supprime04;
  private XRiTextField GCD04;
  private XRiTextField LIB04;
  private JButton bt_Supprime05;
  private XRiTextField GCD05;
  private XRiTextField LIB05;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_16;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
