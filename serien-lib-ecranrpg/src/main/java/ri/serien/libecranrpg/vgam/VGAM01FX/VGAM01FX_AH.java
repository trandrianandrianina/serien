
package ri.serien.libecranrpg.vgam.VGAM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGAM01FX_AH extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGAM01FX_AH(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    AHO10.setEnabled(lexique.isPresent("AHO10"));
    AHO09.setEnabled(lexique.isPresent("AHO09"));
    AHO08.setEnabled(lexique.isPresent("AHO08"));
    AHO07.setEnabled(lexique.isPresent("AHO07"));
    AHO06.setEnabled(lexique.isPresent("AHO06"));
    AHO05.setEnabled(lexique.isPresent("AHO05"));
    AHO04.setEnabled(lexique.isPresent("AHO04"));
    AHO03.setEnabled(lexique.isPresent("AHO03"));
    AHO02.setEnabled(lexique.isPresent("AHO02"));
    AHO01.setEnabled(lexique.isPresent("AHO01"));
    AHMS5.setEnabled(lexique.isPresent("AHMS5"));
    AHMS4.setEnabled(lexique.isPresent("AHMS4"));
    AHMS3.setEnabled(lexique.isPresent("AHMS3"));
    AHMS2.setEnabled(lexique.isPresent("AHMS2"));
    AHMS1.setEnabled(lexique.isPresent("AHMS1"));
    AHFAC.setEnabled(lexique.isPresent("AHFAC"));
    AHREC.setEnabled(lexique.isPresent("AHREC"));
    AHHOM.setEnabled(lexique.isPresent("AHHOM"));
    AHMMX.setEnabled(lexique.isPresent("AHMMX"));
    AHBUDR.setEnabled(lexique.isPresent("AHBUDR"));
    AHTEL.setEnabled(lexique.isPresent("AHTEL"));
    AHMAIL.setEnabled(lexique.isPresent("AHMAIL"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@ de @LOCGRP/-1/@"));
    
    

    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_44 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_47 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    panel1 = new JPanel();
    OBJ_80 = new JLabel();
    OBJ_78 = new JLabel();
    OBJ_82 = new JLabel();
    AHHOM = new XRiTextField();
    AHREC = new XRiTextField();
    AHFAC = new XRiTextField();
    panel2 = new JPanel();
    AHLIB = new XRiTextField();
    AHMAIL = new XRiTextField();
    OBJ_73 = new JLabel();
    AHTEL = new XRiTextField();
    OBJ_62 = new JLabel();
    OBJ_71 = new JLabel();
    AHBUDR = new XRiTextField();
    OBJ_60 = new JLabel();
    AHMMX = new XRiTextField();
    OBJ_69 = new JLabel();
    panel3 = new JPanel();
    AHO01 = new XRiTextField();
    AHO02 = new XRiTextField();
    AHO03 = new XRiTextField();
    AHO04 = new XRiTextField();
    AHO05 = new XRiTextField();
    AHO06 = new XRiTextField();
    AHO07 = new XRiTextField();
    AHO08 = new XRiTextField();
    AHO09 = new XRiTextField();
    AHO10 = new XRiTextField();
    panel4 = new JPanel();
    AHMS1 = new XRiTextField();
    AHMS2 = new XRiTextField();
    AHMS3 = new XRiTextField();
    AHMS4 = new XRiTextField();
    AHMS5 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des achats");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");

          //---- INDETB ----
          INDETB.setName("INDETB");

          //---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");

          //---- INDTYP ----
          INDTYP.setName("INDTYP");

          //---- OBJ_47 ----
          OBJ_47.setText("Ordre");
          OBJ_47.setName("OBJ_47");

          //---- INDIND ----
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(513, Short.MAX_VALUE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(860, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Code acheteur");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //======== panel1 ========
            {
              panel1.setBorder(new TitledBorder("Option propos\u00e9e en fin de"));
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- OBJ_80 ----
              OBJ_80.setText("Bon de r\u00e9ception");
              OBJ_80.setName("OBJ_80");
              panel1.add(OBJ_80);
              OBJ_80.setBounds(40, 65, 140, 20);

              //---- OBJ_78 ----
              OBJ_78.setText("Bon de commande");
              OBJ_78.setName("OBJ_78");
              panel1.add(OBJ_78);
              OBJ_78.setBounds(40, 35, 140, 20);

              //---- OBJ_82 ----
              OBJ_82.setText("Facture");
              OBJ_82.setName("OBJ_82");
              panel1.add(OBJ_82);
              OBJ_82.setBounds(40, 90, 140, 20);

              //---- AHHOM ----
              AHHOM.setComponentPopupMenu(null);
              AHHOM.setName("AHHOM");
              panel1.add(AHHOM);
              AHHOM.setBounds(180, 30, 50, AHHOM.getPreferredSize().height);

              //---- AHREC ----
              AHREC.setComponentPopupMenu(null);
              AHREC.setName("AHREC");
              panel1.add(AHREC);
              AHREC.setBounds(180, 60, 50, AHREC.getPreferredSize().height);

              //---- AHFAC ----
              AHFAC.setComponentPopupMenu(null);
              AHFAC.setName("AHFAC");
              panel1.add(AHFAC);
              AHFAC.setBounds(180, 90, 50, AHFAC.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel1);
            panel1.setBounds(20, 195, 765, 135);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder(""));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- AHLIB ----
              AHLIB.setComponentPopupMenu(null);
              AHLIB.setName("AHLIB");
              panel2.add(AHLIB);
              AHLIB.setBounds(180, 15, 310, AHLIB.getPreferredSize().height);

              //---- AHMAIL ----
              AHMAIL.setComponentPopupMenu(null);
              AHMAIL.setName("AHMAIL");
              panel2.add(AHMAIL);
              AHMAIL.setBounds(180, 80, 410, AHMAIL.getPreferredSize().height);

              //---- OBJ_73 ----
              OBJ_73.setText("Montant maxi de commande autoris\u00e9");
              OBJ_73.setName("OBJ_73");
              panel2.add(OBJ_73);
              OBJ_73.setBounds(40, 145, 275, 20);

              //---- AHTEL ----
              AHTEL.setComponentPopupMenu(null);
              AHTEL.setName("AHTEL");
              panel2.add(AHTEL);
              AHTEL.setBounds(180, 50, 210, AHTEL.getPreferredSize().height);

              //---- OBJ_62 ----
              OBJ_62.setText("T\u00e9l\u00e9phone");
              OBJ_62.setName("OBJ_62");
              panel2.add(OBJ_62);
              OBJ_62.setBounds(40, 50, 135, 20);

              //---- OBJ_71 ----
              OBJ_71.setText("Budget");
              OBJ_71.setName("OBJ_71");
              panel2.add(OBJ_71);
              OBJ_71.setBounds(40, 115, 135, 20);

              //---- AHBUDR ----
              AHBUDR.setComponentPopupMenu(null);
              AHBUDR.setName("AHBUDR");
              panel2.add(AHBUDR);
              AHBUDR.setBounds(180, 110, 82, AHBUDR.getPreferredSize().height);

              //---- OBJ_60 ----
              OBJ_60.setText("Nom");
              OBJ_60.setName("OBJ_60");
              panel2.add(OBJ_60);
              OBJ_60.setBounds(40, 20, 135, 20);

              //---- AHMMX ----
              AHMMX.setComponentPopupMenu(null);
              AHMMX.setName("AHMMX");
              panel2.add(AHMMX);
              AHMMX.setBounds(324, 140, 66, AHMMX.getPreferredSize().height);

              //---- OBJ_69 ----
              OBJ_69.setText("Mail");
              OBJ_69.setName("OBJ_69");
              panel2.add(OBJ_69);
              OBJ_69.setBounds(40, 85, 135, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel2);
            panel2.setBounds(20, 10, 765, 180);

            //======== panel3 ========
            {
              panel3.setBorder(new TitledBorder("Options autoris\u00e9es en fin de bon"));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- AHO01 ----
              AHO01.setComponentPopupMenu(null);
              AHO01.setName("AHO01");
              panel3.add(AHO01);
              AHO01.setBounds(40, 35, 50, AHO01.getPreferredSize().height);

              //---- AHO02 ----
              AHO02.setComponentPopupMenu(null);
              AHO02.setName("AHO02");
              panel3.add(AHO02);
              AHO02.setBounds(95, 35, 50, AHO02.getPreferredSize().height);

              //---- AHO03 ----
              AHO03.setComponentPopupMenu(null);
              AHO03.setName("AHO03");
              panel3.add(AHO03);
              AHO03.setBounds(150, 35, 50, AHO03.getPreferredSize().height);

              //---- AHO04 ----
              AHO04.setComponentPopupMenu(null);
              AHO04.setName("AHO04");
              panel3.add(AHO04);
              AHO04.setBounds(205, 35, 50, AHO04.getPreferredSize().height);

              //---- AHO05 ----
              AHO05.setComponentPopupMenu(null);
              AHO05.setName("AHO05");
              panel3.add(AHO05);
              AHO05.setBounds(260, 35, 50, AHO05.getPreferredSize().height);

              //---- AHO06 ----
              AHO06.setComponentPopupMenu(null);
              AHO06.setName("AHO06");
              panel3.add(AHO06);
              AHO06.setBounds(315, 35, 50, AHO06.getPreferredSize().height);

              //---- AHO07 ----
              AHO07.setComponentPopupMenu(null);
              AHO07.setName("AHO07");
              panel3.add(AHO07);
              AHO07.setBounds(370, 35, 50, AHO07.getPreferredSize().height);

              //---- AHO08 ----
              AHO08.setComponentPopupMenu(null);
              AHO08.setName("AHO08");
              panel3.add(AHO08);
              AHO08.setBounds(425, 35, 50, AHO08.getPreferredSize().height);

              //---- AHO09 ----
              AHO09.setComponentPopupMenu(null);
              AHO09.setName("AHO09");
              panel3.add(AHO09);
              AHO09.setBounds(480, 35, 50, AHO09.getPreferredSize().height);

              //---- AHO10 ----
              AHO10.setComponentPopupMenu(null);
              AHO10.setName("AHO10");
              panel3.add(AHO10);
              AHO10.setBounds(535, 35, 50, AHO10.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel3);
            panel3.setBounds(20, 335, 765, 85);

            //======== panel4 ========
            {
              panel4.setBorder(new TitledBorder("Types de stockage magasin autoris\u00e9s"));
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);

              //---- AHMS1 ----
              AHMS1.setComponentPopupMenu(BTD);
              AHMS1.setName("AHMS1");
              panel4.add(AHMS1);
              AHMS1.setBounds(40, 35, 60, AHMS1.getPreferredSize().height);

              //---- AHMS2 ----
              AHMS2.setComponentPopupMenu(BTD);
              AHMS2.setName("AHMS2");
              panel4.add(AHMS2);
              AHMS2.setBounds(150, 35, 60, AHMS2.getPreferredSize().height);

              //---- AHMS3 ----
              AHMS3.setComponentPopupMenu(BTD);
              AHMS3.setName("AHMS3");
              panel4.add(AHMS3);
              AHMS3.setBounds(260, 35, 60, AHMS3.getPreferredSize().height);

              //---- AHMS4 ----
              AHMS4.setComponentPopupMenu(BTD);
              AHMS4.setName("AHMS4");
              panel4.add(AHMS4);
              AHMS4.setBounds(370, 35, 60, AHMS4.getPreferredSize().height);

              //---- AHMS5 ----
              AHMS5.setComponentPopupMenu(BTD);
              AHMS5.setName("AHMS5");
              panel4.add(AHMS5);
              AHMS5.setBounds(480, 35, 60, AHMS5.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel4.getComponentCount(); i++) {
                  Rectangle bounds = panel4.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel4.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel4.setMinimumSize(preferredSize);
                panel4.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel4);
            panel4.setBounds(20, 425, 765, 85);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap(25, Short.MAX_VALUE)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 813, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 552, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private XRiTextField INDETB;
  private JLabel OBJ_44;
  private XRiTextField INDTYP;
  private JLabel OBJ_47;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JPanel panel1;
  private JLabel OBJ_80;
  private JLabel OBJ_78;
  private JLabel OBJ_82;
  private XRiTextField AHHOM;
  private XRiTextField AHREC;
  private XRiTextField AHFAC;
  private JPanel panel2;
  private XRiTextField AHLIB;
  private XRiTextField AHMAIL;
  private JLabel OBJ_73;
  private XRiTextField AHTEL;
  private JLabel OBJ_62;
  private JLabel OBJ_71;
  private XRiTextField AHBUDR;
  private JLabel OBJ_60;
  private XRiTextField AHMMX;
  private JLabel OBJ_69;
  private JPanel panel3;
  private XRiTextField AHO01;
  private XRiTextField AHO02;
  private XRiTextField AHO03;
  private XRiTextField AHO04;
  private XRiTextField AHO05;
  private XRiTextField AHO06;
  private XRiTextField AHO07;
  private XRiTextField AHO08;
  private XRiTextField AHO09;
  private XRiTextField AHO10;
  private JPanel panel4;
  private XRiTextField AHMS1;
  private XRiTextField AHMS2;
  private XRiTextField AHMS3;
  private XRiTextField AHMS4;
  private XRiTextField AHMS5;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
