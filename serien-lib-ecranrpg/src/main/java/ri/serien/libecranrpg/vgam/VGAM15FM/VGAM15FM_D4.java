
package ri.serien.libecranrpg.vgam.VGAM15FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGAM15FM_D4 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  /**
   * Constructeur.
   */
  public VGAM15FM_D4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    LRG1R.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRG1R@")).trim());
    LRG1R2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRG1R@")).trim());
    LRG1R3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRG1R@")).trim());
    LRG1R4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRG1R@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // ScriptCall("G_CONVER")
    
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(FCT1.getInvoker().getName());
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(OBJ_7.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel3 = new JPanel();
    WTTCX = new XRiTextField();
    OBJ_22 = new JLabel();
    panel2 = new JPanel();
    EARG1 = new XRiTextField();
    BQE1 = new XRiTextField();
    CHQ1 = new XRiTextField();
    HPL1 = new XRiTextField();
    DTE1 = new XRiCalendrier();
    MRG1 = new XRiTextField();
    EAEC1 = new XRiTextField();
    EAPC1 = new XRiTextField();
    LRG1R = new RiZoneSortie();
    EARG2 = new XRiTextField();
    BQE2 = new XRiTextField();
    CHQ2 = new XRiTextField();
    HPL2 = new XRiTextField();
    DTE2 = new XRiCalendrier();
    MRG2 = new XRiTextField();
    EAEC2 = new XRiTextField();
    EAPC2 = new XRiTextField();
    LRG1R2 = new RiZoneSortie();
    EARG3 = new XRiTextField();
    BQE3 = new XRiTextField();
    CHQ3 = new XRiTextField();
    HPL3 = new XRiTextField();
    DTE3 = new XRiCalendrier();
    MRG3 = new XRiTextField();
    EAEC3 = new XRiTextField();
    EAPC3 = new XRiTextField();
    LRG1R3 = new RiZoneSortie();
    EARG4 = new XRiTextField();
    BQE4 = new XRiTextField();
    CHQ4 = new XRiTextField();
    HPL4 = new XRiTextField();
    DTE4 = new XRiCalendrier();
    MRG4 = new XRiTextField();
    EAEC4 = new XRiTextField();
    EAPC4 = new XRiTextField();
    LRG1R4 = new RiZoneSortie();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    FCT1 = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    OBJ_7 = new JPopupMenu();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1115, 305));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel3 ========
        {
          panel3.setBorder(new TitledBorder("R\u00e9glement"));
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);

          //---- WTTCX ----
          WTTCX.setComponentPopupMenu(FCT1);
          WTTCX.setForeground(Color.black);
          WTTCX.setHorizontalAlignment(SwingConstants.RIGHT);
          WTTCX.setFont(WTTCX.getFont().deriveFont(WTTCX.getFont().getStyle() | Font.BOLD));
          WTTCX.setName("WTTCX");
          panel3.add(WTTCX);
          WTTCX.setBounds(205, 35, 155, WTTCX.getPreferredSize().height);

          //---- OBJ_22 ----
          OBJ_22.setText("Total \u00e0 r\u00e8gler");
          OBJ_22.setFont(OBJ_22.getFont().deriveFont(OBJ_22.getFont().getStyle() | Font.BOLD));
          OBJ_22.setName("OBJ_22");
          panel3.add(OBJ_22);
          OBJ_22.setBounds(25, 38, 103, 22);
        }
        p_contenu.add(panel3);
        panel3.setBounds(15, 10, 905, 85);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Ventilation r\u00e9glements"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- EARG1 ----
          EARG1.setName("EARG1");
          panel2.add(EARG1);
          EARG1.setBounds(15, 55, 34, EARG1.getPreferredSize().height);

          //---- BQE1 ----
          BQE1.setName("BQE1");
          panel2.add(BQE1);
          BQE1.setBounds(50, 55, 64, BQE1.getPreferredSize().height);

          //---- CHQ1 ----
          CHQ1.setName("CHQ1");
          panel2.add(CHQ1);
          CHQ1.setBounds(115, 55, 84, CHQ1.getPreferredSize().height);

          //---- HPL1 ----
          HPL1.setName("HPL1");
          panel2.add(HPL1);
          HPL1.setBounds(205, 55, 24, HPL1.getPreferredSize().height);

          //---- DTE1 ----
          DTE1.setName("DTE1");
          panel2.add(DTE1);
          DTE1.setBounds(230, 55, 105, DTE1.getPreferredSize().height);

          //---- MRG1 ----
          MRG1.setName("MRG1");
          panel2.add(MRG1);
          MRG1.setBounds(340, 55, 144, MRG1.getPreferredSize().height);

          //---- EAEC1 ----
          EAEC1.setName("EAEC1");
          panel2.add(EAEC1);
          EAEC1.setBounds(485, 55, 34, EAEC1.getPreferredSize().height);

          //---- EAPC1 ----
          EAPC1.setName("EAPC1");
          panel2.add(EAPC1);
          EAPC1.setBounds(525, 55, 34, EAPC1.getPreferredSize().height);

          //---- LRG1R ----
          LRG1R.setText("@LRG1R@");
          LRG1R.setName("LRG1R");
          panel2.add(LRG1R);
          LRG1R.setBounds(565, 57, 324, LRG1R.getPreferredSize().height);

          //---- EARG2 ----
          EARG2.setName("EARG2");
          panel2.add(EARG2);
          EARG2.setBounds(15, 85, 34, EARG2.getPreferredSize().height);

          //---- BQE2 ----
          BQE2.setName("BQE2");
          panel2.add(BQE2);
          BQE2.setBounds(50, 85, 64, BQE2.getPreferredSize().height);

          //---- CHQ2 ----
          CHQ2.setName("CHQ2");
          panel2.add(CHQ2);
          CHQ2.setBounds(115, 85, 84, CHQ2.getPreferredSize().height);

          //---- HPL2 ----
          HPL2.setName("HPL2");
          panel2.add(HPL2);
          HPL2.setBounds(205, 85, 24, HPL2.getPreferredSize().height);

          //---- DTE2 ----
          DTE2.setName("DTE2");
          panel2.add(DTE2);
          DTE2.setBounds(230, 85, 105, DTE2.getPreferredSize().height);

          //---- MRG2 ----
          MRG2.setName("MRG2");
          panel2.add(MRG2);
          MRG2.setBounds(340, 85, 144, MRG2.getPreferredSize().height);

          //---- EAEC2 ----
          EAEC2.setName("EAEC2");
          panel2.add(EAEC2);
          EAEC2.setBounds(485, 85, 34, EAEC2.getPreferredSize().height);

          //---- EAPC2 ----
          EAPC2.setName("EAPC2");
          panel2.add(EAPC2);
          EAPC2.setBounds(525, 85, 34, EAPC2.getPreferredSize().height);

          //---- LRG1R2 ----
          LRG1R2.setText("@LRG1R@");
          LRG1R2.setName("LRG1R2");
          panel2.add(LRG1R2);
          LRG1R2.setBounds(565, 87, 324, LRG1R2.getPreferredSize().height);

          //---- EARG3 ----
          EARG3.setName("EARG3");
          panel2.add(EARG3);
          EARG3.setBounds(15, 115, 34, EARG3.getPreferredSize().height);

          //---- BQE3 ----
          BQE3.setName("BQE3");
          panel2.add(BQE3);
          BQE3.setBounds(50, 115, 64, BQE3.getPreferredSize().height);

          //---- CHQ3 ----
          CHQ3.setName("CHQ3");
          panel2.add(CHQ3);
          CHQ3.setBounds(115, 115, 84, CHQ3.getPreferredSize().height);

          //---- HPL3 ----
          HPL3.setName("HPL3");
          panel2.add(HPL3);
          HPL3.setBounds(205, 115, 24, HPL3.getPreferredSize().height);

          //---- DTE3 ----
          DTE3.setName("DTE3");
          panel2.add(DTE3);
          DTE3.setBounds(230, 115, 105, DTE3.getPreferredSize().height);

          //---- MRG3 ----
          MRG3.setName("MRG3");
          panel2.add(MRG3);
          MRG3.setBounds(340, 115, 144, MRG3.getPreferredSize().height);

          //---- EAEC3 ----
          EAEC3.setName("EAEC3");
          panel2.add(EAEC3);
          EAEC3.setBounds(485, 115, 34, EAEC3.getPreferredSize().height);

          //---- EAPC3 ----
          EAPC3.setName("EAPC3");
          panel2.add(EAPC3);
          EAPC3.setBounds(525, 115, 34, EAPC3.getPreferredSize().height);

          //---- LRG1R3 ----
          LRG1R3.setText("@LRG1R@");
          LRG1R3.setName("LRG1R3");
          panel2.add(LRG1R3);
          LRG1R3.setBounds(565, 117, 324, LRG1R3.getPreferredSize().height);

          //---- EARG4 ----
          EARG4.setName("EARG4");
          panel2.add(EARG4);
          EARG4.setBounds(15, 145, 34, EARG4.getPreferredSize().height);

          //---- BQE4 ----
          BQE4.setName("BQE4");
          panel2.add(BQE4);
          BQE4.setBounds(50, 145, 64, BQE4.getPreferredSize().height);

          //---- CHQ4 ----
          CHQ4.setName("CHQ4");
          panel2.add(CHQ4);
          CHQ4.setBounds(115, 145, 84, CHQ4.getPreferredSize().height);

          //---- HPL4 ----
          HPL4.setName("HPL4");
          panel2.add(HPL4);
          HPL4.setBounds(205, 145, 24, HPL4.getPreferredSize().height);

          //---- DTE4 ----
          DTE4.setName("DTE4");
          panel2.add(DTE4);
          DTE4.setBounds(230, 145, 105, DTE4.getPreferredSize().height);

          //---- MRG4 ----
          MRG4.setName("MRG4");
          panel2.add(MRG4);
          MRG4.setBounds(340, 145, 144, MRG4.getPreferredSize().height);

          //---- EAEC4 ----
          EAEC4.setName("EAEC4");
          panel2.add(EAEC4);
          EAEC4.setBounds(485, 145, 34, EAEC4.getPreferredSize().height);

          //---- EAPC4 ----
          EAPC4.setName("EAPC4");
          panel2.add(EAPC4);
          EAPC4.setBounds(525, 145, 34, EAPC4.getPreferredSize().height);

          //---- LRG1R4 ----
          LRG1R4.setText("@LRG1R@");
          LRG1R4.setName("LRG1R4");
          panel2.add(LRG1R4);
          LRG1R4.setBounds(565, 147, 324, LRG1R4.getPreferredSize().height);

          //---- label1 ----
          label1.setText("MR");
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
          label1.setHorizontalAlignment(SwingConstants.CENTER);
          label1.setName("label1");
          panel2.add(label1);
          label1.setBounds(15, 30, 34, 25);

          //---- label2 ----
          label2.setText("Banque");
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
          label2.setHorizontalAlignment(SwingConstants.CENTER);
          label2.setName("label2");
          panel2.add(label2);
          label2.setBounds(50, 30, 64, 25);

          //---- label3 ----
          label3.setText("N\u00b0 ch\u00e8que");
          label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
          label3.setHorizontalAlignment(SwingConstants.CENTER);
          label3.setName("label3");
          panel2.add(label3);
          label3.setBounds(115, 30, 84, 25);

          //---- label4 ----
          label4.setText("H");
          label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
          label4.setHorizontalAlignment(SwingConstants.CENTER);
          label4.setName("label4");
          panel2.add(label4);
          label4.setBounds(205, 30, 24, 25);

          //---- label5 ----
          label5.setText("Date");
          label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
          label5.setHorizontalAlignment(SwingConstants.CENTER);
          label5.setName("label5");
          panel2.add(label5);
          label5.setBounds(230, 30, 65, 25);

          //---- label6 ----
          label6.setText("Montant");
          label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
          label6.setHorizontalAlignment(SwingConstants.CENTER);
          label6.setName("label6");
          panel2.add(label6);
          label6.setBounds(340, 30, 144, 25);

          //---- label7 ----
          label7.setText("CE");
          label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD));
          label7.setHorizontalAlignment(SwingConstants.CENTER);
          label7.setName("label7");
          panel2.add(label7);
          label7.setBounds(485, 30, 34, 25);

          //---- label8 ----
          label8.setText("%");
          label8.setFont(label8.getFont().deriveFont(label8.getFont().getStyle() | Font.BOLD));
          label8.setHorizontalAlignment(SwingConstants.CENTER);
          label8.setName("label8");
          panel2.add(label8);
          label8.setBounds(525, 30, 34, 25);

          //---- label9 ----
          label9.setText("Libell\u00e9 r\u00e8glement");
          label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD));
          label9.setName("label9");
          panel2.add(label9);
          label9.setBounds(565, 30, 325, 25);
        }
        p_contenu.add(panel2);
        panel2.setBounds(15, 105, 905, 190);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== FCT1 ========
    {
      FCT1.setName("FCT1");

      //---- OBJ_5 ----
      OBJ_5.setText("Convertisseur mon\u00e9taire");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      FCT1.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("Aide en ligne");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      FCT1.add(OBJ_6);
    }

    //======== OBJ_7 ========
    {
      OBJ_7.setName("OBJ_7");

      //---- OBJ_8 ----
      OBJ_8.setText("Demande Fin de Travail ou Fin de Bon");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      OBJ_7.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setText("Choix possibles (Recherche, Liste ou Saisie pleine page)");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      OBJ_7.add(OBJ_9);

      //---- OBJ_10 ----
      OBJ_10.setText("Interruption Ligne ou Bon");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      OBJ_7.add(OBJ_10);
      OBJ_7.addSeparator();

      //---- OBJ_11 ----
      OBJ_11.setText("Exploitation");
      OBJ_11.setName("OBJ_11");
      OBJ_7.add(OBJ_11);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Aide en ligne");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("Choix possibles");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private JPanel p_contenu;
  private JPanel panel3;
  private XRiTextField WTTCX;
  private JLabel OBJ_22;
  private JPanel panel2;
  private XRiTextField EARG1;
  private XRiTextField BQE1;
  private XRiTextField CHQ1;
  private XRiTextField HPL1;
  private XRiCalendrier DTE1;
  private XRiTextField MRG1;
  private XRiTextField EAEC1;
  private XRiTextField EAPC1;
  private RiZoneSortie LRG1R;
  private XRiTextField EARG2;
  private XRiTextField BQE2;
  private XRiTextField CHQ2;
  private XRiTextField HPL2;
  private XRiCalendrier DTE2;
  private XRiTextField MRG2;
  private XRiTextField EAEC2;
  private XRiTextField EAPC2;
  private RiZoneSortie LRG1R2;
  private XRiTextField EARG3;
  private XRiTextField BQE3;
  private XRiTextField CHQ3;
  private XRiTextField HPL3;
  private XRiCalendrier DTE3;
  private XRiTextField MRG3;
  private XRiTextField EAEC3;
  private XRiTextField EAPC3;
  private RiZoneSortie LRG1R3;
  private XRiTextField EARG4;
  private XRiTextField BQE4;
  private XRiTextField CHQ4;
  private XRiTextField HPL4;
  private XRiCalendrier DTE4;
  private XRiTextField MRG4;
  private XRiTextField EAEC4;
  private XRiTextField EAPC4;
  private RiZoneSortie LRG1R4;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private JPopupMenu FCT1;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JPopupMenu OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_11;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
