
package ri.serien.libecranrpg.vgam.VGAM91FM;
// Nom Fichier: pop_null_CGM05DOM.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import ri.serien.libswing.composantrpg.lexical.RiGraphe;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * @author Stéphane Vénéri
 */
public class VGAM91FM_G04 extends JDialog {
  
  private Lexical lexique = null;
  private iData interpreteurD = null;
  private JPanel master = null;
  private RiGraphe graphe = new RiGraphe(RiGraphe.GRAPHE_LINE3D);
  
  public VGAM91FM_G04(JPanel panel, Lexical lex, iData iD) {
    master = panel;
    lexique = lex;
    interpreteurD = iD;
    initComponents();
    setData();
    setVisible(true);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  public void mettreAJourVariableLibelle() {
  }
  
  public void setData() {
    mettreAJourVariableLibelle();
    // Valeur
    // label2.setText(lexique.HostFieldGetData("MG01"));
    
    // GRAPHE
    
    String[] libelle = new String[12];
    String[] donnee = new String[12];
    String[] donnee2 = new String[12];
    String[] ligneDonnee = new String[12];
    
    // Chargement des libellés
    for (int i = 0; i < libelle.length; i++) {
      libelle[i] = lexique.HostFieldGetData("M" + (i + 1));
    }
    
    // Chargement des données
    for (int i = 0; i < 12; i++) {
      
      // on charge toute la ligne de la LD
      ligneDonnee[i] = lexique.HostFieldGetData("L8" + ((i + 1)));
      // On converti le substring qui nous intéresse depuis l'EBCDIC
      donnee[i] = lexique.HostFieldGetNumericString(ligneDonnee[i].substring(0, 9), 0);
      donnee2[i] = lexique.HostFieldGetNumericString(ligneDonnee[i].substring(26, 35).trim(), 0);
    }
    
    // Préparation des données
    Object[][] data = new Object[libelle.length][3];
    
    for (int i = 0; i < libelle.length; i++) {
      data[i][0] = libelle[i];
      donnee[i] = donnee[i].replaceAll("\\s", "0");
      data[i][1] = Double.parseDouble(donnee[i]);
      donnee2[i] = donnee2[i].replaceAll("\\s", "0");
      data[i][2] = Double.parseDouble(donnee2[i]);
    }
    
    graphe.setDonnee(data, "", false);
    graphe.getGraphe("Evolution du chiffre d'affaire", false);
    
    l_graphe.setIcon(graphe.getPicture(l_graphe.getWidth(), l_graphe.getHeight()));
    
    
    
    // TODO Icones
    OBJ_10.setIcon(lexique.chargerImage("images/retour_p.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Détail des statistiques " + lexique.HostFieldGetData("L1I1")));
  }
  
  public void getData() {
    
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    getData();
    dispose();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    P_Centre = new JPanel();
    l_graphe = new JLabel();
    OBJ_10 = new JButton();
    
    // ======== this ========
    setMinimumSize(new Dimension(335, 220));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== P_Centre ========
    {
      P_Centre.setBackground(new Color(238, 238, 210));
      P_Centre.setName("P_Centre");
      P_Centre.setLayout(null);
      
      // ---- l_graphe ----
      l_graphe.setHorizontalAlignment(SwingConstants.CENTER);
      l_graphe.setComponentPopupMenu(null);
      l_graphe.setBackground(new Color(214, 217, 223));
      l_graphe.setName("l_graphe");
      P_Centre.add(l_graphe);
      l_graphe.setBounds(12, 15, 1068, 580);
      
      // ---- OBJ_10 ----
      OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_10.setToolTipText("Retour");
      OBJ_10.setText("Retour");
      OBJ_10.setFont(OBJ_10.getFont().deriveFont(OBJ_10.getFont().getStyle() | Font.BOLD, OBJ_10.getFont().getSize() + 3f));
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      P_Centre.add(OBJ_10);
      OBJ_10.setBounds(945, 615, 140, 40);
      
      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for (int i = 0; i < P_Centre.getComponentCount(); i++) {
          Rectangle bounds = P_Centre.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = P_Centre.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        P_Centre.setMinimumSize(preferredSize);
        P_Centre.setPreferredSize(preferredSize);
      }
    }
    contentPane.add(P_Centre, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel P_Centre;
  private JLabel l_graphe;
  private JButton OBJ_10;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
