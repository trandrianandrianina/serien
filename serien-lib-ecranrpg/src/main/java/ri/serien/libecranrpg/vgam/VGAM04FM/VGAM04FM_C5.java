//$$david$$ ££04/01/11££ -> tests et modifs

package ri.serien.libecranrpg.vgam.VGAM04FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGAM04FM_C5 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] FRLIT_Value = { "", "1", "9", };
  
  public VGAM04FM_C5(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    FRLIT.setValeurs(FRLIT_Value, null);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI1@")).trim());
    OBJ_34.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI2@")).trim());
    OBJ_36.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI3@")).trim());
    OBJ_38.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI4@")).trim());
    OBJ_40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI5@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // FRLIT.setSelectedIndex(getIndice("FRLIT", FRLIT_Value));
    boolean isConsultation = (lexique.getMode() == Lexical.MODE_CONSULTATION);
    
    OBJ_40.setVisible(lexique.isPresent("WTI5"));
    OBJ_38.setVisible(lexique.isPresent("WTI4"));
    OBJ_36.setVisible(lexique.isPresent("WTI3"));
    OBJ_34.setVisible(lexique.isPresent("WTI2"));
    OBJ_32.setVisible(lexique.isPresent("WTI1"));
    
    FRTOP1.setVisible(!lexique.HostFieldGetData("WTI1").trim().equals(""));
    FRTOP2.setVisible(!lexique.HostFieldGetData("WTI2").trim().equals(""));
    FRTOP3.setVisible(!lexique.HostFieldGetData("WTI3").trim().equals(""));
    FRTOP4.setVisible(!lexique.HostFieldGetData("WTI4").trim().equals(""));
    FRTOP5.setVisible(!lexique.HostFieldGetData("WTI5").trim().equals(""));
    
    FRAWS1.setSelected(lexique.HostFieldGetData("FRAWS").trim().equals("1"));
    FRAWS2.setSelected(lexique.HostFieldGetData("FRAWS").trim().equals("2"));
    FRAWS1.setEnabled(!isConsultation);
    FRAWS2.setEnabled(!isConsultation);
    FRNWS.setEnabled(FRAWS2.isEnabled() && FRAWS2.isSelected());
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Divers"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // lexique.HostFieldPutData("FRLIT", 0, FRLIT_Value[FRLIT.getSelectedIndex()]);
    String valeurFRAWS = "0";
    if (FRAWS1.isSelected()) {
      valeurFRAWS = "1";
    }
    else if (FRAWS2.isSelected()) {
      valeurFRAWS = "2";
    }
    
    lexique.HostFieldPutData("FRAWS", 0, valeurFRAWS);
    
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void FRAWS1ActionPerformed(ActionEvent e) {
    FRAWS2.setSelected(false);
    FRNWS.setEnabled(FRAWS2.isSelected());
  }
  
  private void FRAWS2ActionPerformed(ActionEvent e) {
    FRAWS1.setSelected(false);
    FRNWS.setEnabled(FRAWS2.isSelected());
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    try {
      lexique.HostScreenSendKey(this, "F2");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // EVENEMENTIEL
  // +++++++++++++++++++++++++++++++++++++++++++
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    FRLIT = new XRiComboBox();
    FROBS = new XRiTextField();
    OBJ_28 = new JLabel();
    OBJ_44 = new JLabel();
    OBJ_30 = new JLabel();
    FRMRQ = new XRiTextField();
    OBJ_42 = new JLabel();
    FRAPEN = new XRiTextField();
    OBJ_27 = new JLabel();
    OBJ_26 = new JLabel();
    FRRAR = new XRiTextField();
    OBJ_32 = new JLabel();
    FRTOP1 = new XRiTextField();
    OBJ_34 = new JLabel();
    FRTOP2 = new XRiTextField();
    OBJ_36 = new JLabel();
    FRTOP3 = new XRiTextField();
    OBJ_38 = new JLabel();
    FRTOP4 = new XRiTextField();
    OBJ_40 = new JLabel();
    FRTOP5 = new XRiTextField();
    FRNOT = new XRiTextField();
    FRMAG = new XRiTextField();
    OBJ_29 = new JLabel();
    label1 = new JLabel();
    FRAWS2 = new JCheckBox();
    FRNWS = new XRiTextField();
    FRAWS1 = new JCheckBox();
    label2 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(760, 360));
    setPreferredSize(new Dimension(760, 360));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setPreferredSize(new Dimension(700, 320));
      p_principal.setMinimumSize(new Dimension(700, 320));
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(e -> bouton_validerActionPerformed(e));
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(e -> bouton_retourActionPerformed(e));
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 180));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
          
          // ======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");
            
            // ---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);
          
          // ======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");
            
            // ---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);
          
          // ======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");
            
            // ---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);
          
          // ======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");
            
            // ---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);
          
          // ======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");
            
            // ---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Suppression");
            riSousMenu_bt_suppr.setToolTipText("Suppression");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);
          
          // ======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");
            
            // ---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);
          
          // ======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");
            
            // ---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);
          
          // ======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");
            
            // ---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Crit\u00e8res de r\u00e9appro.");
            riSousMenu_bt6.setToolTipText("Crit\u00e8res de r\u00e9approvisionnement");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(e -> riSousMenu_bt6ActionPerformed(e));
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.CENTER);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        
        // ======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Divers"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);
          
          // ---- FRLIT ----
          FRLIT.setModel(new DefaultComboBoxModel<>(new String[] { "Non", "Litige", "D\u00e9sactiv\u00e9" }));
          FRLIT.setToolTipText("Code litige");
          FRLIT.setComponentPopupMenu(null);
          FRLIT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          FRLIT.setName("FRLIT");
          panel2.add(FRLIT);
          FRLIT.setBounds(150, 35, 110, FRLIT.getPreferredSize().height);
          
          // ---- FROBS ----
          FROBS.setComponentPopupMenu(null);
          FROBS.setName("FROBS");
          panel2.add(FROBS);
          FROBS.setBounds(150, 125, 290, FROBS.getPreferredSize().height);
          
          // ---- OBJ_28 ----
          OBJ_28.setText("Note fournisseur");
          OBJ_28.setName("OBJ_28");
          panel2.add(OBJ_28);
          OBJ_28.setBounds(25, 99, 120, 20);
          
          // ---- OBJ_44 ----
          OBJ_44.setText("Pr\u00e9fixe r\u00e9f\u00e9rence");
          OBJ_44.setName("OBJ_44");
          panel2.add(OBJ_44);
          OBJ_44.setBounds(290, 189, 104, 20);
          
          // ---- OBJ_30 ----
          OBJ_30.setText("Observations");
          OBJ_30.setName("OBJ_30");
          panel2.add(OBJ_30);
          OBJ_30.setBounds(25, 129, 120, 20);
          
          // ---- FRMRQ ----
          FRMRQ.setComponentPopupMenu(null);
          FRMRQ.setName("FRMRQ");
          panel2.add(FRMRQ);
          FRMRQ.setBounds(150, 185, 110, FRMRQ.getPreferredSize().height);
          
          // ---- OBJ_42 ----
          OBJ_42.setText("Marque");
          OBJ_42.setName("OBJ_42");
          panel2.add(OBJ_42);
          OBJ_42.setBounds(25, 189, 120, 20);
          
          // ---- FRAPEN ----
          FRAPEN.setComponentPopupMenu(null);
          FRAPEN.setName("FRAPEN");
          panel2.add(FRAPEN);
          FRAPEN.setBounds(150, 65, 60, FRAPEN.getPreferredSize().height);
          
          // ---- OBJ_27 ----
          OBJ_27.setText("APE");
          OBJ_27.setName("OBJ_27");
          panel2.add(OBJ_27);
          OBJ_27.setBounds(25, 69, 120, 20);
          
          // ---- OBJ_26 ----
          OBJ_26.setText("Litige/d\u00e9sactiv\u00e9");
          OBJ_26.setName("OBJ_26");
          panel2.add(OBJ_26);
          OBJ_26.setBounds(25, 38, 120, 20);
          
          // ---- FRRAR ----
          FRRAR.setComponentPopupMenu(null);
          FRRAR.setName("FRRAR");
          panel2.add(FRRAR);
          FRRAR.setBounds(400, 185, 40, FRRAR.getPreferredSize().height);
          
          // ---- OBJ_32 ----
          OBJ_32.setText("@WTI1@");
          OBJ_32.setName("OBJ_32");
          panel2.add(OBJ_32);
          OBJ_32.setBounds(25, 219, 25, 20);
          
          // ---- FRTOP1 ----
          FRTOP1.setComponentPopupMenu(BTD);
          FRTOP1.setName("FRTOP1");
          panel2.add(FRTOP1);
          FRTOP1.setBounds(55, 215, 34, FRTOP1.getPreferredSize().height);
          
          // ---- OBJ_34 ----
          OBJ_34.setText("@WTI2@");
          OBJ_34.setName("OBJ_34");
          panel2.add(OBJ_34);
          OBJ_34.setBounds(120, 219, 25, 20);
          
          // ---- FRTOP2 ----
          FRTOP2.setComponentPopupMenu(BTD);
          FRTOP2.setName("FRTOP2");
          panel2.add(FRTOP2);
          FRTOP2.setBounds(150, 215, 34, FRTOP2.getPreferredSize().height);
          
          // ---- OBJ_36 ----
          OBJ_36.setText("@WTI3@");
          OBJ_36.setName("OBJ_36");
          panel2.add(OBJ_36);
          OBJ_36.setBounds(205, 219, 25, 20);
          
          // ---- FRTOP3 ----
          FRTOP3.setComponentPopupMenu(BTD);
          FRTOP3.setName("FRTOP3");
          panel2.add(FRTOP3);
          FRTOP3.setBounds(235, 215, 34, FRTOP3.getPreferredSize().height);
          
          // ---- OBJ_38 ----
          OBJ_38.setText("@WTI4@");
          OBJ_38.setName("OBJ_38");
          panel2.add(OBJ_38);
          OBJ_38.setBounds(283, 219, 25, 20);
          
          // ---- FRTOP4 ----
          FRTOP4.setComponentPopupMenu(BTD);
          FRTOP4.setName("FRTOP4");
          panel2.add(FRTOP4);
          FRTOP4.setBounds(310, 215, 34, FRTOP4.getPreferredSize().height);
          
          // ---- OBJ_40 ----
          OBJ_40.setText("@WTI5@");
          OBJ_40.setName("OBJ_40");
          panel2.add(OBJ_40);
          OBJ_40.setBounds(369, 219, 25, 20);
          
          // ---- FRTOP5 ----
          FRTOP5.setComponentPopupMenu(BTD);
          FRTOP5.setName("FRTOP5");
          panel2.add(FRTOP5);
          FRTOP5.setBounds(400, 215, 34, FRTOP5.getPreferredSize().height);
          
          // ---- FRNOT ----
          FRNOT.setComponentPopupMenu(null);
          FRNOT.setName("FRNOT");
          panel2.add(FRNOT);
          FRNOT.setBounds(150, 95, 20, FRNOT.getPreferredSize().height);
          
          // ---- FRMAG ----
          FRMAG.setComponentPopupMenu(BTD);
          FRMAG.setName("FRMAG");
          panel2.add(FRMAG);
          FRMAG.setBounds(150, 155, 30, FRMAG.getPreferredSize().height);
          
          // ---- OBJ_29 ----
          OBJ_29.setText("Magasin de r\u00e9appro");
          OBJ_29.setName("OBJ_29");
          panel2.add(OBJ_29);
          OBJ_29.setBounds(25, 159, 120, 20);
          
          // ---- label1 ----
          label1.setText("Raison sociale Webshop");
          label1.setName("label1");
          panel2.add(label1);
          label1.setBounds(25, 279, 180, 20);
          
          // ---- FRAWS2 ----
          FRAWS2.setName("FRAWS2");
          FRAWS2.addActionListener(e -> FRAWS2ActionPerformed(e));
          panel2.add(FRAWS2);
          FRAWS2.setBounds(new Rectangle(new Point(205, 280), FRAWS2.getPreferredSize()));
          
          // ---- FRNWS ----
          FRNWS.setEnabled(false);
          FRNWS.setName("FRNWS");
          panel2.add(FRNWS);
          FRNWS.setBounds(235, 275, 275, FRNWS.getPreferredSize().height);
          
          // ---- FRAWS1 ----
          FRAWS1.setName("FRAWS1");
          FRAWS1.addActionListener(e -> FRAWS1ActionPerformed(e));
          panel2.add(FRAWS1);
          FRAWS1.setBounds(new Rectangle(new Point(205, 255), FRAWS1.getPreferredSize()));
          
          // ---- label2 ----
          label2.setText("Masqu\u00e9 dans le Webshop");
          label2.setName("label2");
          panel2.add(label2);
          label2.setBounds(25, 254, 180, 20);
        }
        
        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
            .addContainerGap().addComponent(panel2, GroupLayout.DEFAULT_SIZE, 586, Short.MAX_VALUE).addContainerGap()));
        p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
            .addContainerGap().addComponent(panel2, GroupLayout.DEFAULT_SIZE, 334, Short.MAX_VALUE).addContainerGap()));
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(e -> OBJ_18ActionPerformed(e));
      BTD.add(OBJ_18);
      
      // ---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(e -> OBJ_17ActionPerformed(e));
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiComboBox FRLIT;
  private XRiTextField FROBS;
  private JLabel OBJ_28;
  private JLabel OBJ_44;
  private JLabel OBJ_30;
  private XRiTextField FRMRQ;
  private JLabel OBJ_42;
  private XRiTextField FRAPEN;
  private JLabel OBJ_27;
  private JLabel OBJ_26;
  private XRiTextField FRRAR;
  private JLabel OBJ_32;
  private XRiTextField FRTOP1;
  private JLabel OBJ_34;
  private XRiTextField FRTOP2;
  private JLabel OBJ_36;
  private XRiTextField FRTOP3;
  private JLabel OBJ_38;
  private XRiTextField FRTOP4;
  private JLabel OBJ_40;
  private XRiTextField FRTOP5;
  private XRiTextField FRNOT;
  private XRiTextField FRMAG;
  private JLabel OBJ_29;
  private JLabel label1;
  private JCheckBox FRAWS2;
  private XRiTextField FRNWS;
  private JCheckBox FRAWS1;
  private JLabel label2;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
