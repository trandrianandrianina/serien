
package ri.serien.libecranrpg.vgam.VGAM50FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VGAM50FM_A3 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] _WT301_Top = { "WT301", "WT302", "WT303", "WT304", "WT305", "WT306", "WT307", "WT308", "WT309", "WT310", };
  private String[] _WT301_Title = { "TITLD", };
  private String[][] _WT301_Data = { { "L301", }, { "L302", }, { "L303", }, { "L304", }, { "L305", }, { "L306", }, { "L307", },
      { "L308", }, { "L309", }, { "L310", }, };
  private int[] _WT301_Width = { 501, };
  // private String[][] _LIST_Title_Data_Brut=null;
  
  public VGAM50FM_A3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // _LIST_Title_Data_Brut = initTable(LIST, "Courier New", Font.BOLD, 12, "Courier New", Font.PLAIN, 12);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    WT301.setAspectTable(_WT301_Top, _WT301_Title, _WT301_Data, _WT301_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_39.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCOL@")).trim());
    OBJ_28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WFRS@")).trim());
    OBJ_29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNOM@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST, _LIST_Title_Data_Brut, _WT301_Top);
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    OBJ_32.setVisible(lexique.isTrue("92"));
    OBJ_33.setVisible(lexique.isTrue("93"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression(
        "Conditions normales d'achat : " + lexique.HostFieldGetData("A1ART").trim() + " - " + lexique.HostFieldGetData("A1LIB")));
    
    bouton_fin.setIcon(lexique.chargerImage("images/fin_p.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void WT301MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _WT301_Top, "1", "ENTER", e);
    if (WT301.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WT301_Top, "1", "Enter");
    WT301.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WT301_Top, "2", "Enter");
    WT301.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WT301_Top, "4", "Enter");
    WT301.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WT301_Top, "5", "Enter");
    WT301.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WT301_Top, "7", "Enter");
    WT301.setValeurTop("7");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void bouton_finActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  // EVENEMENTIEL
  // +++++++++++++++++++++++++++++++++++++++++++
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    navig_fin = new RiMenu();
    bouton_fin = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt1 = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    SCROLLPANE_LIST2 = new JScrollPane();
    WT301 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    OBJ_39 = new JLabel();
    OBJ_28 = new JLabel();
    OBJ_29 = new JLabel();
    OBJ_32 = new JLabel();
    OBJ_33 = new JLabel();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_15 = new JMenuItem();
    OBJ_16 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    OBJ_22 = new JMenuItem();
    OBJ_23 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(785, 280));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("CNA fournisseurs");
            bouton_retour.setToolTipText("CNA fournisseurs");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);

          //======== navig_fin ========
          {
            navig_fin.setName("navig_fin");

            //---- bouton_fin ----
            bouton_fin.setText("Fiche article");
            bouton_fin.setName("bouton_fin");
            bouton_fin.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_finActionPerformed(e);
              }
            });
            navig_fin.add(bouton_fin);
          }
          menus_bas.add(navig_fin);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 300));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt1 ----
              riMenu_bt1.setText("@V01F@");
              riMenu_bt1.setPreferredSize(new Dimension(167, 50));
              riMenu_bt1.setMinimumSize(new Dimension(167, 50));
              riMenu_bt1.setMaximumSize(new Dimension(170, 50));
              riMenu_bt1.setFont(riMenu_bt1.getFont().deriveFont(riMenu_bt1.getFont().getSize() + 2f));
              riMenu_bt1.setName("riMenu_bt1");
              riMenu_V01F.add(riMenu_bt1);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("CNA du fournisseur"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //======== SCROLLPANE_LIST2 ========
          {
            SCROLLPANE_LIST2.setComponentPopupMenu(BTD);
            SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

            //---- WT301 ----
            WT301.setComponentPopupMenu(BTD);
            WT301.setName("WT301");
            WT301.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                WT301MouseClicked(e);
              }
            });
            SCROLLPANE_LIST2.setViewportView(WT301);
          }
          panel2.add(SCROLLPANE_LIST2);
          SCROLLPANE_LIST2.setBounds(15, 55, 519, 190);

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          panel2.add(BT_PGUP);
          BT_PGUP.setBounds(540, 55, 25, 85);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setToolTipText("Page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          panel2.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(540, 160, 25, 85);

          //---- OBJ_39 ----
          OBJ_39.setText("@WCOL@");
          OBJ_39.setFont(OBJ_39.getFont().deriveFont(OBJ_39.getFont().getStyle() | Font.BOLD, OBJ_39.getFont().getSize() + 2f));
          OBJ_39.setBorder(new BevelBorder(BevelBorder.LOWERED));
          OBJ_39.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_39.setName("OBJ_39");
          panel2.add(OBJ_39);
          OBJ_39.setBounds(15, 25, 20, 25);

          //---- OBJ_28 ----
          OBJ_28.setText("@WFRS@");
          OBJ_28.setFont(OBJ_28.getFont().deriveFont(OBJ_28.getFont().getStyle() | Font.BOLD, OBJ_28.getFont().getSize() + 2f));
          OBJ_28.setBorder(new BevelBorder(BevelBorder.LOWERED));
          OBJ_28.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_28.setName("OBJ_28");
          panel2.add(OBJ_28);
          OBJ_28.setBounds(40, 25, 54, 25);

          //---- OBJ_29 ----
          OBJ_29.setText("@WNOM@");
          OBJ_29.setFont(OBJ_29.getFont().deriveFont(OBJ_29.getFont().getStyle() | Font.BOLD, OBJ_29.getFont().getSize() + 2f));
          OBJ_29.setBorder(new BevelBorder(BevelBorder.LOWERED));
          OBJ_29.setName("OBJ_29");
          panel2.add(OBJ_29);
          OBJ_29.setBounds(100, 25, 283, 25);

          //---- OBJ_32 ----
          OBJ_32.setText("En cours");
          OBJ_32.setName("OBJ_32");
          panel2.add(OBJ_32);
          OBJ_32.setBounds(475, 0, 91, 20);

          //---- OBJ_33 ----
          OBJ_33.setText("En pr\u00e9paration");
          OBJ_33.setName("OBJ_33");
          panel2.add(OBJ_33);
          OBJ_33.setBounds(475, 0, 91, 20);
        }
        p_contenu.add(panel2);
        panel2.setBounds(15, 10, 585, 260);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_15 ----
      OBJ_15.setText("Modifier");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);

      //---- OBJ_16 ----
      OBJ_16.setText("Annuler");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);

      //---- OBJ_17 ----
      OBJ_17.setText("Interroger");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_18 ----
      OBJ_18.setText("Dupliquer");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
      BTD.addSeparator();

      //---- OBJ_22 ----
      OBJ_22.setText("Aide en ligne");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);

      //---- OBJ_23 ----
      OBJ_23.setText("Invite");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD.add(OBJ_23);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private RiMenu navig_fin;
  private RiMenu_bt bouton_fin;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt1;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private JPanel p_contenu;
  private JPanel panel2;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable WT301;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JLabel OBJ_39;
  private JLabel OBJ_28;
  private JLabel OBJ_29;
  private JLabel OBJ_32;
  private JLabel OBJ_33;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_15;
  private JMenuItem OBJ_16;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_23;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
