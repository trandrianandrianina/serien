
package ri.serien.libecranrpg.vgam.VGAM18FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.border.TitledBorder;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.achat.documentachat.snacheteur.SNAcheteur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiBarreBouton;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GAM2192] Gestion des achats -> Documents d'achats -> Commandes et réapprovisionnements -> Gestion des demandes d'achats ->
 * Centralisation de demandes d'achats
 * Indicateur : 00000001
 * Titre : Centralisation de demandes d'achats
 */
public class VGAM18FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private Message V03F = null;
  private static final String BOUTON_DEMANDER_ACHAT = "Demande d'achat";
  private static final String BOUTON_CENTRALISATION = "Centralisation";
  
  public VGAM18FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    initDiverses();
    xriBarreBouton.ajouterBouton(BOUTON_DEMANDER_ACHAT, 'a', true);
    xriBarreBouton.ajouterBouton(BOUTON_CENTRALISATION, 'c', true);
    xriBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    xriBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    xriBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbV03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Active les bouton liée au V01F
    xriBarreBouton.rafraichir(lexique);
    
    // Initialise l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Gestion de V01F
    lbV03F.setVisible(!lexique.HostFieldGetData("V03F").trim().isEmpty());
    V03F = V03F.getMessageNormal(lexique.HostFieldGetData("V03F"));
    lbV03F.setMessage(V03F);
    
    // Initialise les composants
    chargerMagasin();
    chargerAcheteur();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snMagasin.renseignerChampRPG(lexique, "WMAG");
    snAcheteur.renseignerChampRPG(lexique, "WACH");
  }
  
  /**
   * Initialise le composant magasin
   */
  private void chargerMagasin() {
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.setTousAutorise(true);
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "WMAG");
  }
  
  /**
   * Initialise le composant acheteur
   */
  private void chargerAcheteur() {
    snAcheteur.setSession(getSession());
    snAcheteur.setIdEtablissement(snEtablissement.getIdSelection());
    snAcheteur.charger(false);
    snAcheteur.setSelectionParChampRPG(lexique, "WACH");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    xriBarreBouton.isTraiterClickBouton(pSNBouton);
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_CENTRALISATION)) {
        lexique.HostScreenSendKey(this, "F13");
      }
      else if (pSNBouton.isBouton(BOUTON_DEMANDER_ACHAT)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    xriBarreBouton = new XRiBarreBouton();
    pnlCotnenu = new SNPanelContenu();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbDate = new SNLabelChamp();
    WDATX = new XRiCalendrier();
    lbV03F = new SNMessage();
    pnlAcheteurMagasin = new SNPanelTitre();
    lbAcheteur = new SNLabelChamp();
    snAcheteur = new SNAcheteur();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Centralisation de demandes d'achats");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ---- xriBarreBouton ----
    xriBarreBouton.setName("xriBarreBouton");
    add(xriBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlCotnenu ========
    {
      pnlCotnenu.setName("pnlCotnenu");
      pnlCotnenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlCotnenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlCotnenu.getLayout()).rowHeights = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlCotnenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlCotnenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
      
      // ======== pnlEtablissement ========
      {
        pnlEtablissement.setName("pnlEtablissement");
        pnlEtablissement.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbEtablissement ----
        lbEtablissement.setText("Etablissement");
        lbEtablissement.setPreferredSize(new Dimension(200, 30));
        lbEtablissement.setMinimumSize(new Dimension(200, 30));
        lbEtablissement.setMaximumSize(new Dimension(200, 30));
        lbEtablissement.setName("lbEtablissement");
        pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- snEtablissement ----
        snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
        snEtablissement.setName("snEtablissement");
        pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbDate ----
        lbDate.setText("Date");
        lbDate.setName("lbDate");
        pnlEtablissement.add(lbDate, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- WDATX ----
        WDATX.setPreferredSize(new Dimension(110, 30));
        WDATX.setMinimumSize(new Dimension(110, 30));
        WDATX.setMaximumSize(new Dimension(110, 30));
        WDATX.setFont(new Font("sansserif", Font.PLAIN, 14));
        WDATX.setName("WDATX");
        pnlEtablissement.add(WDATX, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbV03F ----
        lbV03F.setText("@V03F@");
        lbV03F.setName("lbV03F");
        pnlEtablissement.add(lbV03F, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlCotnenu.add(pnlEtablissement,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlAcheteurMagasin ========
      {
        pnlAcheteurMagasin.setBorder(new TitledBorder(""));
        pnlAcheteurMagasin.setOpaque(false);
        pnlAcheteurMagasin.setName("pnlAcheteurMagasin");
        pnlAcheteurMagasin.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlAcheteurMagasin.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlAcheteurMagasin.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlAcheteurMagasin.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlAcheteurMagasin.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ---- lbAcheteur ----
        lbAcheteur.setText("Acheteur pour la  centralisation");
        lbAcheteur.setPreferredSize(new Dimension(200, 30));
        lbAcheteur.setMinimumSize(new Dimension(200, 30));
        lbAcheteur.setMaximumSize(new Dimension(200, 30));
        lbAcheteur.setName("lbAcheteur");
        pnlAcheteurMagasin.add(lbAcheteur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snAcheteur ----
        snAcheteur.setFont(new Font("sansserif", Font.PLAIN, 14));
        snAcheteur.setName("snAcheteur");
        pnlAcheteurMagasin.add(snAcheteur, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbMagasin ----
        lbMagasin.setText("Magasin d'achat");
        lbMagasin.setPreferredSize(new Dimension(200, 30));
        lbMagasin.setMinimumSize(new Dimension(200, 30));
        lbMagasin.setMaximumSize(new Dimension(200, 30));
        lbMagasin.setName("lbMagasin");
        pnlAcheteurMagasin.add(lbMagasin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- snMagasin ----
        snMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
        snMagasin.setName("snMagasin");
        pnlAcheteurMagasin.add(snMagasin, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlCotnenu.add(pnlAcheteurMagasin,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlCotnenu, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private XRiBarreBouton xriBarreBouton;
  private SNPanelContenu pnlCotnenu;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbDate;
  private XRiCalendrier WDATX;
  private SNMessage lbV03F;
  private SNPanelTitre pnlAcheteurMagasin;
  private SNLabelChamp lbAcheteur;
  private SNAcheteur snAcheteur;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
