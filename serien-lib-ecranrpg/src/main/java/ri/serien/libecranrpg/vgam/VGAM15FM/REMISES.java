
package ri.serien.libecranrpg.vgam.VGAM15FM;
// Nom Fichier: pop_null_CGM05DOM.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.PopupPerso;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class REMISES extends SNPanelEcranRPG implements ioFrame {
   
  private PopupPerso popupPerso = new PopupPerso(this);  

  private String[] EATRL_Value = { "C", "A" };
  private String[] EATRP_Value = { "C", "A" };
  private String[] EABRL_Value = { "M", "P" };
  
  public REMISES(SNPanelEcranRPG parent) {
    super(parent);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    setCloseKey("ENTER", "F12");
    setPopupPerso(popupPerso);
    popupPerso.ajouterProprieteTouche("ENTER", true, true, false);
    popupPerso.ajouterProprieteTouche("F12", false, true, false);

    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    initDiverses();
    EATRL.setValeurs(EATRL_Value, new String[] { "Cascade", "Ajout" });
    EATRP.setValeurs(EATRP_Value, new String[] { "Cascade", "Ajout" });
    EABRL.setValeurs(EABRL_Value, new String[] { "Montant", "Prix" });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // TODO Icones
    bouton_valider.setIcon(lexique.chargerImage("images/ok_p.png", true));
    bouton_retour.setIcon(lexique.chargerImage("images/retour_p.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  @Override
  public void dispose() {
    getJDialog().dispose();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    popupPerso.traiterTouche("ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    popupPerso.traiterTouche("F12");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    P_Centre = new JPanel();
    panel1 = new JPanel();
    OBJ_38 = new JLabel();
    EAREM1 = new XRiTextField();
    EAREM2 = new XRiTextField();
    EAREM3 = new XRiTextField();
    EAREM4 = new XRiTextField();
    EAREM5 = new XRiTextField();
    EAREM6 = new XRiTextField();
    OBJ_36 = new JLabel();
    OBJ_37 = new JLabel();
    OBJ_39 = new JLabel();
    EARP1 = new XRiTextField();
    EARP2 = new XRiTextField();
    EARP3 = new XRiTextField();
    EARP4 = new XRiTextField();
    EARP5 = new XRiTextField();
    EARP6 = new XRiTextField();
    EATRL = new XRiComboBox();
    EABRL = new XRiComboBox();
    EATRP = new XRiComboBox();

    //======== this ========
    setMinimumSize(new Dimension(840, 150));
    setPreferredSize(new Dimension(840, 150));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_menus ========
    {
      p_menus.setPreferredSize(new Dimension(170, 0));
      p_menus.setMinimumSize(new Dimension(170, 0));
      p_menus.setBackground(new Color(238, 239, 241));
      p_menus.setBorder(LineBorder.createGrayLineBorder());
      p_menus.setName("p_menus");
      p_menus.setLayout(new BorderLayout());

      //======== menus_bas ========
      {
        menus_bas.setOpaque(false);
        menus_bas.setBackground(new Color(238, 239, 241));
        menus_bas.setName("menus_bas");
        menus_bas.setLayout(new VerticalLayout());

        //======== navig_valid ========
        {
          navig_valid.setName("navig_valid");

          //---- bouton_valider ----
          bouton_valider.setText("Valider");
          bouton_valider.setToolTipText("Valider");
          bouton_valider.setName("bouton_valider");
          bouton_valider.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_validerActionPerformed(e);
            }
          });
          navig_valid.add(bouton_valider);
        }
        menus_bas.add(navig_valid);

        //======== navig_retour ========
        {
          navig_retour.setName("navig_retour");

          //---- bouton_retour ----
          bouton_retour.setText("Retour");
          bouton_retour.setToolTipText("Retour");
          bouton_retour.setName("bouton_retour");
          bouton_retour.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_retourActionPerformed(e);
            }
          });
          navig_retour.add(bouton_retour);
        }
        menus_bas.add(navig_retour);
      }
      p_menus.add(menus_bas, BorderLayout.SOUTH);
    }
    add(p_menus, BorderLayout.EAST);

    //======== P_Centre ========
    {
      P_Centre.setBackground(new Color(238, 238, 210));
      P_Centre.setName("P_Centre");
      P_Centre.setLayout(null);

      //======== panel1 ========
      {
        panel1.setBorder(new TitledBorder(""));
        panel1.setOpaque(false);
        panel1.setName("panel1");
        panel1.setLayout(null);

        //---- OBJ_38 ----
        OBJ_38.setText("Remises / ligne");
        OBJ_38.setName("OBJ_38");
        panel1.add(OBJ_38);
        OBJ_38.setBounds(20, 39, 111, 20);

        //---- EAREM1 ----
        EAREM1.setComponentPopupMenu(null);
        EAREM1.setName("EAREM1");
        panel1.add(EAREM1);
        EAREM1.setBounds(133, 35, 52, EAREM1.getPreferredSize().height);

        //---- EAREM2 ----
        EAREM2.setComponentPopupMenu(null);
        EAREM2.setName("EAREM2");
        panel1.add(EAREM2);
        EAREM2.setBounds(187, 35, 52, EAREM2.getPreferredSize().height);

        //---- EAREM3 ----
        EAREM3.setComponentPopupMenu(null);
        EAREM3.setName("EAREM3");
        panel1.add(EAREM3);
        EAREM3.setBounds(241, 35, 52, EAREM3.getPreferredSize().height);

        //---- EAREM4 ----
        EAREM4.setComponentPopupMenu(null);
        EAREM4.setName("EAREM4");
        panel1.add(EAREM4);
        EAREM4.setBounds(295, 35, 52, EAREM4.getPreferredSize().height);

        //---- EAREM5 ----
        EAREM5.setComponentPopupMenu(null);
        EAREM5.setName("EAREM5");
        panel1.add(EAREM5);
        EAREM5.setBounds(349, 35, 52, EAREM5.getPreferredSize().height);

        //---- EAREM6 ----
        EAREM6.setComponentPopupMenu(null);
        EAREM6.setName("EAREM6");
        panel1.add(EAREM6);
        EAREM6.setBounds(403, 35, 52, EAREM6.getPreferredSize().height);

        //---- OBJ_36 ----
        OBJ_36.setText("Type");
        OBJ_36.setName("OBJ_36");
        panel1.add(OBJ_36);
        OBJ_36.setBounds(465, 10, 36, 20);

        //---- OBJ_37 ----
        OBJ_37.setText("Base");
        OBJ_37.setName("OBJ_37");
        panel1.add(OBJ_37);
        OBJ_37.setBounds(560, 10, 36, 20);

        //---- OBJ_39 ----
        OBJ_39.setText("Remises / pied");
        OBJ_39.setName("OBJ_39");
        panel1.add(OBJ_39);
        OBJ_39.setBounds(20, 70, 111, 20);

        //---- EARP1 ----
        EARP1.setComponentPopupMenu(null);
        EARP1.setName("EARP1");
        panel1.add(EARP1);
        EARP1.setBounds(133, 66, 52, EARP1.getPreferredSize().height);

        //---- EARP2 ----
        EARP2.setComponentPopupMenu(null);
        EARP2.setName("EARP2");
        panel1.add(EARP2);
        EARP2.setBounds(187, 66, 52, EARP2.getPreferredSize().height);

        //---- EARP3 ----
        EARP3.setComponentPopupMenu(null);
        EARP3.setName("EARP3");
        panel1.add(EARP3);
        EARP3.setBounds(241, 66, 52, EARP3.getPreferredSize().height);

        //---- EARP4 ----
        EARP4.setComponentPopupMenu(null);
        EARP4.setName("EARP4");
        panel1.add(EARP4);
        EARP4.setBounds(295, 66, 52, EARP4.getPreferredSize().height);

        //---- EARP5 ----
        EARP5.setComponentPopupMenu(null);
        EARP5.setName("EARP5");
        panel1.add(EARP5);
        EARP5.setBounds(349, 66, 52, EARP5.getPreferredSize().height);

        //---- EARP6 ----
        EARP6.setComponentPopupMenu(null);
        EARP6.setName("EARP6");
        panel1.add(EARP6);
        EARP6.setBounds(403, 66, 52, EARP6.getPreferredSize().height);

        //---- EATRL ----
        EATRL.setModel(new DefaultComboBoxModel(new String[] {
          "Cascade",
          "Ajout"
        }));
        EATRL.setName("EATRL");
        panel1.add(EATRL);
        EATRL.setBounds(457, 36, 85, EATRL.getPreferredSize().height);

        //---- EABRL ----
        EABRL.setModel(new DefaultComboBoxModel(new String[] {
          "Montant",
          "Prix"
        }));
        EABRL.setName("EABRL");
        panel1.add(EABRL);
        EABRL.setBounds(544, 36, 80, EABRL.getPreferredSize().height);

        //---- EATRP ----
        EATRP.setModel(new DefaultComboBoxModel(new String[] {
          "Cascade",
          "Ajout"
        }));
        EATRP.setName("EATRP");
        panel1.add(EATRP);
        EATRP.setBounds(457, 67, 85, EATRP.getPreferredSize().height);
      }
      P_Centre.add(panel1);
      panel1.setBounds(5, 5, 650, 140);
    }
    add(P_Centre, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel P_Centre;
  private JPanel panel1;
  private JLabel OBJ_38;
  private XRiTextField EAREM1;
  private XRiTextField EAREM2;
  private XRiTextField EAREM3;
  private XRiTextField EAREM4;
  private XRiTextField EAREM5;
  private XRiTextField EAREM6;
  private JLabel OBJ_36;
  private JLabel OBJ_37;
  private JLabel OBJ_39;
  private XRiTextField EARP1;
  private XRiTextField EARP2;
  private XRiTextField EARP3;
  private XRiTextField EARP4;
  private XRiTextField EARP5;
  private XRiTextField EARP6;
  private XRiComboBox EATRL;
  private XRiComboBox EABRL;
  private XRiComboBox EATRP;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
