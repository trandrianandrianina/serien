
package ri.serien.libecranrpg.vgam.VGAM17FM;

import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.border.TitledBorder;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.achat.documentachat.snacheteur.SNAcheteur;
import ri.serien.libswing.composant.metier.comptabilite.snsectionanalytique.SNSectionAnalytique;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.referentiel.transport.snlieutransport.SNLieuTransport;
import ri.serien.libswing.composant.metier.referentiel.transport.sntransporteur.SNTransporteur;
import ri.serien.libswing.composant.metier.vente.affaire.snaffaire.SNAffaire;
import ri.serien.libswing.composant.metier.vente.documentvente.snmodeexpedition.SNModeExpedition;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiBarreBouton;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GAM2191] Gestion des achats -> Documents d'achats -> Commandes et réapprovisionnements -> Gestion des demandes d'achats -> Demandes
 * d'achats
 * Indicateur : 00000001
 * Titre : Demande d'achats
 */
public class VGAM17FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private Message V03F = null;
  
  public VGAM17FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    
    XRiBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    XRiBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    XRiBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbV03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    tfLibelleNature.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBNAT@")).trim());
    lbLibelleZP1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE1@")).trim());
    lbLibelleZP2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE2@")).trim());
    lbLibelleZP3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE3@")).trim());
    lbLibelleZP4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE4@")).trim());
    lbLibelleZP5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE5@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Visibiltié des zone personnalisé
    pnlZonePersonalise.setVisible(EATP1.isVisible() || EATP2.isVisible() || EATP3.isVisible() || EATP4.isVisible() || EATP5.isVisible());
    
    // Initialise le libelle de la nature
    tfLibelleNature.setText(lexique.HostFieldGetData("LIBNAT"));
    
    // Initialise le texte d'information du lieu de transport
    tfInformation.setText(lexique.HostFieldGetData("LVLB1") + "  " + lexique.HostFieldGetData("LVLB2") + "  "
        + lexique.HostFieldGetData("LVLB3") + "  " + lexique.HostFieldGetData("LVLB4"));
    
    // Initialise les libelle des ZP
    lbLibelleZP1.setText("TIZPE1");
    lbLibelleZP2.setText("TIZPE2");
    lbLibelleZP3.setText("TIZPE3");
    lbLibelleZP4.setText("TIZPE4");
    lbLibelleZP5.setText("TIZPE5");
    
    // Active les bouton liée au V01F
    XRiBarreBouton.rafraichir(lexique);
    
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "EAETB");
    
    // Initialise les composants
    chargerMagasin();
    chargerLieuTransport();
    chargerModeExpedition();
    chargerTransporteur();
    chargerAcheteur();
    chargerAffaire();
    chargerSectionAnalytique();
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("EAETB"));
    
    // Gestion de V01F
    lbV03F.setVisible(!lexique.HostFieldGetData("V03F").trim().isEmpty());
    V03F = V03F.getMessageNormal(lexique.HostFieldGetData("V03F"));
    lbV03F.setMessage(V03F);
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  DEMANDE D'ACHATS"));
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "EAETB");
    snMagasin.renseignerChampRPG(lexique, "EAMAG");
    snLieuTransport.renseignerChampRPG(lexique, "EALLV");
    snModeExpedition.renseignerChampRPG(lexique, "EAMEX");
    snTransporteur.renseignerChampRPG(lexique, "EACTR");
    snAcheteur.renseignerChampRPG(lexique, "EAACH");
    snAffaire.renseignerChampRPG(lexique, "EAACT");
    snSectionAnalytique.renseignerChampRPG(lexique, "EASAN");
  }
  
  /**
   * Initialise le composant magasin
   */
  private void chargerMagasin() {
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.setTousAutorise(true);
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "EAMAG");
  }
  
  /**
   * Initialise le composant lieu transport
   */
  private void chargerLieuTransport() {
    snLieuTransport.setSession(getSession());
    snLieuTransport.setIdEtablissement(snEtablissement.getIdSelection());
    snLieuTransport.setTousAutorise(true);
    snLieuTransport.charger(false);
    snLieuTransport.setSelectionParChampRPG(lexique, "EALLV");
  }
  
  /**
   * Initialise le composant mode d'expedition
   */
  private void chargerModeExpedition() {
    snModeExpedition.setSession(getSession());
    snModeExpedition.setIdEtablissement(snEtablissement.getIdSelection());
    snModeExpedition.setTousAutorise(true);
    snModeExpedition.charger(false);
    snModeExpedition.setSelectionParChampRPG(lexique, "EAMEX");
  }
  
  /**
   * Initialise le composant transporteur
   */
  private void chargerTransporteur() {
    snTransporteur.setSession(getSession());
    snTransporteur.setIdEtablissement(snEtablissement.getIdSelection());
    snTransporteur.setTousAutorise(true);
    snTransporteur.charger(false);
    snTransporteur.setSelectionParChampRPG(lexique, "EACTR");
  }
  
  /**
   * Initialise le composant acheteur
   */
  private void chargerAcheteur() {
    snAcheteur.setSession(getSession());
    snAcheteur.setIdEtablissement(snEtablissement.getIdSelection());
    snAcheteur.setTousAutorise(true);
    snAcheteur.charger(false);
    snAcheteur.setSelectionParChampRPG(lexique, "EAACH");
  }
  
  /**
   * Initialise le composant affaire
   */
  private void chargerAffaire() {
    snAffaire.setSession(getSession());
    snAffaire.setIdEtablissement(snEtablissement.getIdSelection());
    snAffaire.setTousAutorise(true);
    snAffaire.charger(false);
    snAffaire.setSelectionParChampRPG(lexique, "EAACT");
  }
  
  /**
   * Initialise le composant section analytique
   */
  private void chargerSectionAnalytique() {
    snSectionAnalytique.setSession(getSession());
    snSectionAnalytique.setIdEtablissement(snEtablissement.getIdSelection());
    snSectionAnalytique.setTousAutorise(true);
    snSectionAnalytique.charger(false);
    snSectionAnalytique.setSelectionParChampRPG(lexique, "EASAN");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    XRiBarreBouton.isTraiterClickBouton(pSNBouton);
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snLieuTransportValueChanged(SNComposantEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlContenu = new SNPanelContenu();
    pnlInformation = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbNumero = new SNLabelChamp();
    EANUM = new XRiTextField();
    lbDate = new SNLabelChamp();
    WDATX = new XRiCalendrier();
    lbV03F = new SNLabelUnite();
    pnlInformationBon = new SNPanelTitre();
    lbReference = new SNLabelChamp();
    EARBC = new XRiTextField();
    lbDemandeur = new SNLabelChamp();
    snAcheteur = new SNAcheteur();
    lbNature = new SNLabelChamp();
    pnlNature = new SNPanel();
    EANAT = new XRiTextField();
    tfLibelleNature = new SNTexte();
    EAPDS = new XRiTextField();
    lbSection = new SNLabelChamp();
    lbAffaire = new SNLabelChamp();
    snAffaire = new SNAffaire();
    snSectionAnalytique = new SNSectionAnalytique();
    lbPoids = new SNLabelChamp();
    pnlZonePersonalise = new SNPanel();
    lbLibelleZP1 = new SNLabelChamp();
    lbLibelleZP2 = new SNLabelChamp();
    lbLibelleZP3 = new SNLabelChamp();
    lbLibelleZP4 = new SNLabelChamp();
    lbLibelleZP5 = new SNLabelChamp();
    lbZonePersonalise = new SNLabelChamp();
    EATP1 = new XRiTextField();
    EATP2 = new XRiTextField();
    EATP3 = new XRiTextField();
    EATP4 = new XRiTextField();
    EATP5 = new XRiTextField();
    pnlInformationLivraison = new SNPanelTitre();
    EARBL = new XRiTextField();
    lbExpedition = new SNLabelChamp();
    lbMagasin = new SNLabelChamp();
    lbLivraison = new SNLabelChamp();
    snMagasin = new SNMagasin();
    pnlDateLieu = new SNPanel();
    lbPrevueLe = new SNLabelChamp();
    EADLPX = new XRiCalendrier();
    lbLieu = new SNLabelChamp();
    snLieuTransport = new SNLieuTransport();
    lbInformationLieuTransport = new SNLabelChamp();
    tfInformation = new SNTexte();
    snModeExpedition = new SNModeExpedition();
    lbTransporteur = new SNLabelChamp();
    snTransporteur = new SNTransporteur();
    XRiBarreBouton = new XRiBarreBouton();
    bpPresentation = new SNBandeauTitre();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
      
      // ======== pnlInformation ========
      {
        pnlInformation.setOpaque(false);
        pnlInformation.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        pnlInformation.setName("pnlInformation");
        pnlInformation.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlInformation.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlInformation.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlInformation.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlInformation.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbEtablissement ----
        lbEtablissement.setText("Etablissement");
        lbEtablissement.setName("lbEtablissement");
        pnlInformation.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- snEtablissement ----
        snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
        snEtablissement.setEnabled(false);
        snEtablissement.setName("snEtablissement");
        pnlInformation.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbNumero ----
        lbNumero.setText("Num\u00e9ro");
        lbNumero.setPreferredSize(new Dimension(100, 30));
        lbNumero.setMinimumSize(new Dimension(100, 30));
        lbNumero.setMaximumSize(new Dimension(100, 30));
        lbNumero.setName("lbNumero");
        pnlInformation.add(lbNumero, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- EANUM ----
        EANUM.setPreferredSize(new Dimension(100, 30));
        EANUM.setMinimumSize(new Dimension(100, 30));
        EANUM.setMaximumSize(new Dimension(100, 30));
        EANUM.setFont(new Font("sansserif", Font.PLAIN, 14));
        EANUM.setName("EANUM");
        pnlInformation.add(EANUM, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbDate ----
        lbDate.setText("Date");
        lbDate.setMaximumSize(new Dimension(100, 30));
        lbDate.setMinimumSize(new Dimension(100, 30));
        lbDate.setPreferredSize(new Dimension(100, 30));
        lbDate.setName("lbDate");
        pnlInformation.add(lbDate, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- WDATX ----
        WDATX.setMaximumSize(new Dimension(110, 30));
        WDATX.setMinimumSize(new Dimension(110, 30));
        WDATX.setPreferredSize(new Dimension(110, 30));
        WDATX.setFont(new Font("sansserif", Font.PLAIN, 14));
        WDATX.setName("WDATX");
        pnlInformation.add(WDATX, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbV03F ----
        lbV03F.setText("@V03F@");
        lbV03F.setName("lbV03F");
        pnlInformation.add(lbV03F, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlInformation,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlInformationBon ========
      {
        pnlInformationBon.setBorder(new TitledBorder(""));
        pnlInformationBon.setOpaque(false);
        pnlInformationBon.setName("pnlInformationBon");
        pnlInformationBon.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlInformationBon.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlInformationBon.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlInformationBon.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlInformationBon.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbReference ----
        lbReference.setText("R\u00e9f\u00e9rence");
        lbReference.setName("lbReference");
        pnlInformationBon.add(lbReference, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- EARBC ----
        EARBC.setFont(new Font("sansserif", Font.PLAIN, 14));
        EARBC.setPreferredSize(new Dimension(150, 30));
        EARBC.setMinimumSize(new Dimension(150, 30));
        EARBC.setMaximumSize(new Dimension(150, 30));
        EARBC.setName("EARBC");
        pnlInformationBon.add(EARBC, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbDemandeur ----
        lbDemandeur.setText("Demandeur");
        lbDemandeur.setName("lbDemandeur");
        pnlInformationBon.add(lbDemandeur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snAcheteur ----
        snAcheteur.setFont(new Font("sansserif", Font.PLAIN, 14));
        snAcheteur.setName("snAcheteur");
        pnlInformationBon.add(snAcheteur, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbNature ----
        lbNature.setText("Nature");
        lbNature.setName("lbNature");
        pnlInformationBon.add(lbNature, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlNature ========
        {
          pnlNature.setName("pnlNature");
          pnlNature.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlNature.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlNature.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlNature.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlNature.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- EANAT ----
          EANAT.setPreferredSize(new Dimension(25, 30));
          EANAT.setMinimumSize(new Dimension(25, 30));
          EANAT.setMaximumSize(new Dimension(25, 30));
          EANAT.setFont(new Font("sansserif", Font.PLAIN, 14));
          EANAT.setName("EANAT");
          pnlNature.add(EANAT, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfLibelleNature ----
          tfLibelleNature.setText("@LIBNAT@");
          tfLibelleNature.setEnabled(false);
          tfLibelleNature.setPreferredSize(new Dimension(250, 30));
          tfLibelleNature.setMinimumSize(new Dimension(250, 30));
          tfLibelleNature.setMaximumSize(new Dimension(250, 30));
          tfLibelleNature.setName("tfLibelleNature");
          pnlNature.add(tfLibelleNature, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlInformationBon.add(pnlNature, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- EAPDS ----
        EAPDS.setPreferredSize(new Dimension(125, 30));
        EAPDS.setMinimumSize(new Dimension(125, 30));
        EAPDS.setMaximumSize(new Dimension(125, 30));
        EAPDS.setFont(new Font("sansserif", Font.PLAIN, 14));
        EAPDS.setName("EAPDS");
        pnlInformationBon.add(EAPDS, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbSection ----
        lbSection.setText("Section");
        lbSection.setName("lbSection");
        pnlInformationBon.add(lbSection, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbAffaire ----
        lbAffaire.setText("Affaire");
        lbAffaire.setName("lbAffaire");
        pnlInformationBon.add(lbAffaire, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snAffaire ----
        snAffaire.setFont(new Font("sansserif", Font.PLAIN, 14));
        snAffaire.setName("snAffaire");
        pnlInformationBon.add(snAffaire, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- snSectionAnalytique ----
        snSectionAnalytique.setFont(new Font("sansserif", Font.PLAIN, 14));
        snSectionAnalytique.setName("snSectionAnalytique");
        pnlInformationBon.add(snSectionAnalytique, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbPoids ----
        lbPoids.setText("Poids");
        lbPoids.setName("lbPoids");
        pnlInformationBon.add(lbPoids, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlZonePersonalise ========
        {
          pnlZonePersonalise.setName("pnlZonePersonalise");
          pnlZonePersonalise.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlZonePersonalise.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlZonePersonalise.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlZonePersonalise.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlZonePersonalise.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbLibelleZP1 ----
          lbLibelleZP1.setText("@TIZPE1@");
          lbLibelleZP1.setName("lbLibelleZP1");
          pnlZonePersonalise.add(lbLibelleZP1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbLibelleZP2 ----
          lbLibelleZP2.setText("@TIZPE2@");
          lbLibelleZP2.setName("lbLibelleZP2");
          pnlZonePersonalise.add(lbLibelleZP2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbLibelleZP3 ----
          lbLibelleZP3.setText("@TIZPE3@");
          lbLibelleZP3.setName("lbLibelleZP3");
          pnlZonePersonalise.add(lbLibelleZP3, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbLibelleZP4 ----
          lbLibelleZP4.setText("@TIZPE4@");
          lbLibelleZP4.setName("lbLibelleZP4");
          pnlZonePersonalise.add(lbLibelleZP4, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbLibelleZP5 ----
          lbLibelleZP5.setText("@TIZPE5@");
          lbLibelleZP5.setName("lbLibelleZP5");
          pnlZonePersonalise.add(lbLibelleZP5, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbZonePersonalise ----
          lbZonePersonalise.setText("Zone personnalis\u00e9e ");
          lbZonePersonalise.setName("lbZonePersonalise");
          pnlZonePersonalise.add(lbZonePersonalise, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- EATP1 ----
          EATP1.setPreferredSize(new Dimension(50, 30));
          EATP1.setMinimumSize(new Dimension(50, 30));
          EATP1.setMaximumSize(new Dimension(50, 30));
          EATP1.setFont(new Font("sansserif", Font.PLAIN, 14));
          EATP1.setName("EATP1");
          pnlZonePersonalise.add(EATP1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- EATP2 ----
          EATP2.setPreferredSize(new Dimension(50, 30));
          EATP2.setMinimumSize(new Dimension(50, 30));
          EATP2.setMaximumSize(new Dimension(50, 30));
          EATP2.setFont(new Font("sansserif", Font.PLAIN, 14));
          EATP2.setName("EATP2");
          pnlZonePersonalise.add(EATP2, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- EATP3 ----
          EATP3.setPreferredSize(new Dimension(50, 30));
          EATP3.setMinimumSize(new Dimension(50, 30));
          EATP3.setMaximumSize(new Dimension(50, 30));
          EATP3.setFont(new Font("sansserif", Font.PLAIN, 14));
          EATP3.setName("EATP3");
          pnlZonePersonalise.add(EATP3, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- EATP4 ----
          EATP4.setPreferredSize(new Dimension(50, 30));
          EATP4.setMinimumSize(new Dimension(50, 30));
          EATP4.setMaximumSize(new Dimension(50, 30));
          EATP4.setFont(new Font("sansserif", Font.PLAIN, 14));
          EATP4.setName("EATP4");
          pnlZonePersonalise.add(EATP4, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- EATP5 ----
          EATP5.setPreferredSize(new Dimension(50, 30));
          EATP5.setMinimumSize(new Dimension(50, 30));
          EATP5.setMaximumSize(new Dimension(50, 30));
          EATP5.setFont(new Font("sansserif", Font.PLAIN, 14));
          EATP5.setName("EATP5");
          pnlZonePersonalise.add(EATP5, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlInformationBon.add(pnlZonePersonalise, new GridBagConstraints(0, 6, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlInformationBon,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlInformationLivraison ========
      {
        pnlInformationLivraison.setOpaque(false);
        pnlInformationLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
        pnlInformationLivraison.setName("pnlInformationLivraison");
        pnlInformationLivraison.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlInformationLivraison.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlInformationLivraison.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlInformationLivraison.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlInformationLivraison.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- EARBL ----
        EARBL.setFont(new Font("sansserif", Font.PLAIN, 14));
        EARBL.setPreferredSize(new Dimension(150, 30));
        EARBL.setMinimumSize(new Dimension(150, 30));
        EARBL.setName("EARBL");
        pnlInformationLivraison.add(EARBL, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbExpedition ----
        lbExpedition.setText("Exp\u00e9dition");
        lbExpedition.setPreferredSize(new Dimension(200, 30));
        lbExpedition.setMinimumSize(new Dimension(200, 30));
        lbExpedition.setMaximumSize(new Dimension(200, 30));
        lbExpedition.setName("lbExpedition");
        pnlInformationLivraison.add(lbExpedition, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbMagasin ----
        lbMagasin.setText("Magasin");
        lbMagasin.setPreferredSize(new Dimension(200, 30));
        lbMagasin.setMinimumSize(new Dimension(200, 30));
        lbMagasin.setMaximumSize(new Dimension(200, 30));
        lbMagasin.setName("lbMagasin");
        pnlInformationLivraison.add(lbMagasin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbLivraison ----
        lbLivraison.setText("Livraison");
        lbLivraison.setPreferredSize(new Dimension(200, 30));
        lbLivraison.setMinimumSize(new Dimension(200, 30));
        lbLivraison.setMaximumSize(new Dimension(200, 30));
        lbLivraison.setName("lbLivraison");
        pnlInformationLivraison.add(lbLivraison, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snMagasin ----
        snMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
        snMagasin.setName("snMagasin");
        pnlInformationLivraison.add(snMagasin, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlDateLieu ========
        {
          pnlDateLieu.setName("pnlDateLieu");
          pnlDateLieu.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDateLieu.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlDateLieu.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlDateLieu.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDateLieu.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbPrevueLe ----
          lbPrevueLe.setText("Pr\u00e9vue le");
          lbPrevueLe.setPreferredSize(new Dimension(200, 30));
          lbPrevueLe.setMinimumSize(new Dimension(200, 30));
          lbPrevueLe.setMaximumSize(new Dimension(200, 30));
          lbPrevueLe.setName("lbPrevueLe");
          pnlDateLieu.add(lbPrevueLe, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- EADLPX ----
          EADLPX.setPreferredSize(new Dimension(110, 30));
          EADLPX.setMinimumSize(new Dimension(110, 30));
          EADLPX.setMaximumSize(new Dimension(110, 30));
          EADLPX.setFont(new Font("sansserif", Font.PLAIN, 14));
          EADLPX.setName("EADLPX");
          pnlDateLieu.add(EADLPX, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbLieu ----
          lbLieu.setText("Lieu");
          lbLieu.setName("lbLieu");
          pnlDateLieu.add(lbLieu, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snLieuTransport ----
          snLieuTransport.setFont(new Font("sansserif", Font.PLAIN, 14));
          snLieuTransport.setName("snLieuTransport");
          snLieuTransport.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snLieuTransportValueChanged(e);
            }
          });
          pnlDateLieu.add(snLieuTransport, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlInformationLivraison.add(pnlDateLieu, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbInformationLieuTransport ----
        lbInformationLieuTransport.setText("Informations lieu de transport");
        lbInformationLieuTransport.setPreferredSize(new Dimension(200, 30));
        lbInformationLieuTransport.setMaximumSize(new Dimension(200, 30));
        lbInformationLieuTransport.setMinimumSize(new Dimension(200, 30));
        lbInformationLieuTransport.setName("lbInformationLieuTransport");
        pnlInformationLivraison.add(lbInformationLieuTransport, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfInformation ----
        tfInformation.setEditable(false);
        tfInformation.setPreferredSize(new Dimension(675, 30));
        tfInformation.setMinimumSize(new Dimension(675, 30));
        tfInformation.setMaximumSize(new Dimension(675, 30));
        tfInformation.setName("tfInformation");
        pnlInformationLivraison.add(tfInformation, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- snModeExpedition ----
        snModeExpedition.setFont(new Font("sansserif", Font.PLAIN, 14));
        snModeExpedition.setName("snModeExpedition");
        pnlInformationLivraison.add(snModeExpedition, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbTransporteur ----
        lbTransporteur.setText("Transporteur");
        lbTransporteur.setPreferredSize(new Dimension(200, 30));
        lbTransporteur.setMinimumSize(new Dimension(200, 30));
        lbTransporteur.setMaximumSize(new Dimension(200, 30));
        lbTransporteur.setName("lbTransporteur");
        pnlInformationLivraison.add(lbTransporteur, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- snTransporteur ----
        snTransporteur.setFont(new Font("sansserif", Font.PLAIN, 14));
        snTransporteur.setName("snTransporteur");
        pnlInformationLivraison.add(snTransporteur, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlInformationLivraison,
          new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- XRiBarreBouton ----
    XRiBarreBouton.setName("XRiBarreBouton");
    add(XRiBarreBouton, BorderLayout.SOUTH);
    
    // ---- bpPresentation ----
    bpPresentation.setText("Demande d'achats");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlInformation;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbNumero;
  private XRiTextField EANUM;
  private SNLabelChamp lbDate;
  private XRiCalendrier WDATX;
  private SNLabelUnite lbV03F;
  private SNPanelTitre pnlInformationBon;
  private SNLabelChamp lbReference;
  private XRiTextField EARBC;
  private SNLabelChamp lbDemandeur;
  private SNAcheteur snAcheteur;
  private SNLabelChamp lbNature;
  private SNPanel pnlNature;
  private XRiTextField EANAT;
  private SNTexte tfLibelleNature;
  private XRiTextField EAPDS;
  private SNLabelChamp lbSection;
  private SNLabelChamp lbAffaire;
  private SNAffaire snAffaire;
  private SNSectionAnalytique snSectionAnalytique;
  private SNLabelChamp lbPoids;
  private SNPanel pnlZonePersonalise;
  private SNLabelChamp lbLibelleZP1;
  private SNLabelChamp lbLibelleZP2;
  private SNLabelChamp lbLibelleZP3;
  private SNLabelChamp lbLibelleZP4;
  private SNLabelChamp lbLibelleZP5;
  private SNLabelChamp lbZonePersonalise;
  private XRiTextField EATP1;
  private XRiTextField EATP2;
  private XRiTextField EATP3;
  private XRiTextField EATP4;
  private XRiTextField EATP5;
  private SNPanelTitre pnlInformationLivraison;
  private XRiTextField EARBL;
  private SNLabelChamp lbExpedition;
  private SNLabelChamp lbMagasin;
  private SNLabelChamp lbLivraison;
  private SNMagasin snMagasin;
  private SNPanel pnlDateLieu;
  private SNLabelChamp lbPrevueLe;
  private XRiCalendrier EADLPX;
  private SNLabelChamp lbLieu;
  private SNLieuTransport snLieuTransport;
  private SNLabelChamp lbInformationLieuTransport;
  private SNTexte tfInformation;
  private SNModeExpedition snModeExpedition;
  private SNLabelChamp lbTransporteur;
  private SNTransporteur snTransporteur;
  private XRiBarreBouton XRiBarreBouton;
  private SNBandeauTitre bpPresentation;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
