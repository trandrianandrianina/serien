
package ri.serien.libecranrpg.vgam.VGAM02SX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGAM02SX_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] PZ07_Value = { "", "1", "2", "3", "10", "20", "98", "99", };
  private String[] PZ06_Value = { "", "1", "2", "3", "10", "20", "98", "99", };
  private String[] PZ05_Value = { "", "1", "2", "3", "10", "20", "98", "99", };
  private String[] PZ04_Value = { "", "1", "2", "3", "10", "20", "98", "99", };
  private String[] PZ03_Value = { "", "1", "2", "3", "10", "20", "98", "99", };
  private String[] PZ02_Value = { "", "1", "2", "3", };
  private String[] PZ01_Value = { "", "1", "2", "3", };
  
  public VGAM02SX_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    PZ07.setValeurs(PZ07_Value, null);
    PZ06.setValeurs(PZ06_Value, null);
    PZ05.setValeurs(PZ05_Value, null);
    PZ04.setValeurs(PZ04_Value, null);
    PZ03.setValeurs(PZ03_Value, null);
    PZ02.setValeurs(PZ02_Value, null);
    PZ01.setValeurs(PZ01_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    // PZ01.setSelectedIndex(getIndice("PZ01", PZ01_Value));
    // PZ02.setSelectedIndex(getIndice("PZ02", PZ02_Value));
    // PZ03.setSelectedIndex(getIndice("PZ03", PZ03_Value));
    // PZ04.setSelectedIndex(getIndice("PZ04", PZ04_Value));
    // PZ05.setSelectedIndex(getIndice("PZ05", PZ05_Value));
    // PZ06.setSelectedIndex(getIndice("PZ06", PZ06_Value));
    // PZ07.setSelectedIndex(getIndice("PZ07", PZ07_Value));
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@ STATISTIQUES"));
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("PZ07", 0, PZ07_Value[PZ07.getSelectedIndex()]);
    // lexique.HostFieldPutData("PZ06", 0, PZ06_Value[PZ06.getSelectedIndex()]);
    // lexique.HostFieldPutData("PZ05", 0, PZ05_Value[PZ05.getSelectedIndex()]);
    // lexique.HostFieldPutData("PZ04", 0, PZ04_Value[PZ04.getSelectedIndex()]);
    // lexique.HostFieldPutData("PZ03", 0, PZ03_Value[PZ03.getSelectedIndex()]);
    // lexique.HostFieldPutData("PZ02", 0, PZ02_Value[PZ02.getSelectedIndex()]);
    // lexique.HostFieldPutData("PZ01", 0, PZ01_Value[PZ01.getSelectedIndex()]);
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_48 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel2 = new JXTitledPanel();
    PZ01 = new XRiComboBox();
    PZ02 = new XRiComboBox();
    PZ03 = new XRiComboBox();
    PZ04 = new XRiComboBox();
    PZ05 = new XRiComboBox();
    PZ06 = new XRiComboBox();
    PZ07 = new XRiComboBox();
    OBJ_59 = new JLabel();
    OBJ_60 = new JLabel();
    OBJ_61 = new JLabel();
    OBJ_62 = new JLabel();
    OBJ_71 = new JLabel();
    OBJ_57 = new JLabel();
    OBJ_58 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    OBJ_16 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des achats");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44 ----
          OBJ_44.setText("Etablissement");
          OBJ_44.setName("OBJ_44");

          //---- INDETB ----
          INDETB.setName("INDETB");

          //---- OBJ_46 ----
          OBJ_46.setText("Code");
          OBJ_46.setName("OBJ_46");

          //---- INDTYP ----
          INDTYP.setName("INDTYP");

          //---- OBJ_48 ----
          OBJ_48.setText("Ordre");
          OBJ_48.setName("OBJ_48");

          //---- INDIND ----
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(513, Short.MAX_VALUE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(600, 340));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setTitle("Param\u00e9trage des zones statistiques");
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //---- PZ01 ----
            PZ01.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Quantit\u00e9 en US",
              "Quantit\u00e9 en UA",
              "Quantit\u00e9 en UC"
            }));
            PZ01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            PZ01.setName("PZ01");
            xTitledPanel2ContentContainer.add(PZ01);
            PZ01.setBounds(295, 30, 182, PZ01.getPreferredSize().height);

            //---- PZ02 ----
            PZ02.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Quantit\u00e9 en US",
              "Quantit\u00e9 en UA",
              "Quantit\u00e9 en UC"
            }));
            PZ02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            PZ02.setName("PZ02");
            xTitledPanel2ContentContainer.add(PZ02);
            PZ02.setBounds(295, 60, 182, PZ02.getPreferredSize().height);

            //---- PZ03 ----
            PZ03.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Quantit\u00e9 en US",
              "Quantit\u00e9 en UA",
              "Quantit\u00e9 en UC",
              "Montant frais",
              "Nombre de jour d\u00e9lai",
              "Poids",
              "Acc\u00e8s ext\u00e9rieur"
            }));
            PZ03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            PZ03.setName("PZ03");
            xTitledPanel2ContentContainer.add(PZ03);
            PZ03.setBounds(295, 90, 182, PZ03.getPreferredSize().height);

            //---- PZ04 ----
            PZ04.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Quantit\u00e9 en US",
              "Quantit\u00e9 en UA",
              "Quantit\u00e9 en UC",
              "Montant frais",
              "Nombre de jour d\u00e9lai",
              "Poids",
              "Acc\u00e8s ext\u00e9rieur"
            }));
            PZ04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            PZ04.setName("PZ04");
            xTitledPanel2ContentContainer.add(PZ04);
            PZ04.setBounds(295, 120, 182, PZ04.getPreferredSize().height);

            //---- PZ05 ----
            PZ05.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Quantit\u00e9 en US",
              "Quantit\u00e9 en UA",
              "Quantit\u00e9 en UC",
              "Montant frais",
              "Nombre de jour d\u00e9lai",
              "Poids",
              "Acc\u00e8s ext\u00e9rieur"
            }));
            PZ05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            PZ05.setName("PZ05");
            xTitledPanel2ContentContainer.add(PZ05);
            PZ05.setBounds(295, 150, 182, PZ05.getPreferredSize().height);

            //---- PZ06 ----
            PZ06.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Quantit\u00e9 en US",
              "Quantit\u00e9 en UA",
              "Quantit\u00e9 en UC",
              "Montant frais",
              "Nombre de jour d\u00e9lai",
              "Poids",
              "Acc\u00e8s ext\u00e9rieur"
            }));
            PZ06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            PZ06.setName("PZ06");
            xTitledPanel2ContentContainer.add(PZ06);
            PZ06.setBounds(295, 180, 182, PZ06.getPreferredSize().height);

            //---- PZ07 ----
            PZ07.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Quantit\u00e9 en US",
              "Quantit\u00e9 en UA",
              "Quantit\u00e9 en UC",
              "Montant frais",
              "Nombre de jour d\u00e9lai",
              "Poids",
              "Acc\u00e8s ext\u00e9rieur"
            }));
            PZ07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            PZ07.setName("PZ07");
            xTitledPanel2ContentContainer.add(PZ07);
            PZ07.setBounds(295, 210, 182, PZ07.getPreferredSize().height);

            //---- OBJ_59 ----
            OBJ_59.setText("Zone personnalis\u00e9e  1");
            OBJ_59.setName("OBJ_59");
            xTitledPanel2ContentContainer.add(OBJ_59);
            OBJ_59.setBounds(35, 93, 137, 20);

            //---- OBJ_60 ----
            OBJ_60.setText("Zone personnalis\u00e9e  2");
            OBJ_60.setName("OBJ_60");
            xTitledPanel2ContentContainer.add(OBJ_60);
            OBJ_60.setBounds(35, 123, 137, 20);

            //---- OBJ_61 ----
            OBJ_61.setText("Zone personnalis\u00e9e  3");
            OBJ_61.setName("OBJ_61");
            xTitledPanel2ContentContainer.add(OBJ_61);
            OBJ_61.setBounds(35, 153, 137, 20);

            //---- OBJ_62 ----
            OBJ_62.setText("Zone personnalis\u00e9e  4");
            OBJ_62.setName("OBJ_62");
            xTitledPanel2ContentContainer.add(OBJ_62);
            OBJ_62.setBounds(35, 183, 137, 20);

            //---- OBJ_71 ----
            OBJ_71.setText("Zone personnalis\u00e9e  5");
            OBJ_71.setName("OBJ_71");
            xTitledPanel2ContentContainer.add(OBJ_71);
            OBJ_71.setBounds(35, 213, 137, 20);

            //---- OBJ_57 ----
            OBJ_57.setText("Quantit\u00e9  1");
            OBJ_57.setName("OBJ_57");
            xTitledPanel2ContentContainer.add(OBJ_57);
            OBJ_57.setBounds(35, 33, 83, 20);

            //---- OBJ_58 ----
            OBJ_58.setText("Quantit\u00e9  2");
            OBJ_58.setName("OBJ_58");
            xTitledPanel2ContentContainer.add(OBJ_58);
            OBJ_58.setBounds(35, 63, 83, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap(22, Short.MAX_VALUE)
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 556, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 313, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Choix possibles");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_16 ----
      OBJ_16.setText("Aide en ligne");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44;
  private XRiTextField INDETB;
  private JLabel OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_48;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel2;
  private XRiComboBox PZ01;
  private XRiComboBox PZ02;
  private XRiComboBox PZ03;
  private XRiComboBox PZ04;
  private XRiComboBox PZ05;
  private XRiComboBox PZ06;
  private XRiComboBox PZ07;
  private JLabel OBJ_59;
  private JLabel OBJ_60;
  private JLabel OBJ_61;
  private JLabel OBJ_62;
  private JLabel OBJ_71;
  private JLabel OBJ_57;
  private JLabel OBJ_58;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_16;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
