
package ri.serien.libecranrpg.vgam.VGAM91FM;
// Nom Fichier: pop_null_CGM05DOM.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;

import com.jgoodies.forms.factories.DefaultComponentFactory;

import ri.serien.libswing.composantrpg.lexical.RiGraphe;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * @author Stéphane Vénéri
 */
public class VGAM91FM_G01 extends JDialog {
  
  private Lexical lexique = null;
  private iData interpreteurD = null;
  private JPanel master = null;
  private RiGraphe graphe = new RiGraphe(RiGraphe.GRAPHE_PIE3D);
  private RiGraphe graphe2 = new RiGraphe(RiGraphe.GRAPHE_PIE3D);
  
  public VGAM91FM_G01(JPanel panel, Lexical lex, iData iD) {
    master = panel;
    lexique = lex;
    interpreteurD = iD;
    initComponents();
    setData();
    setVisible(true);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  public void mettreAJourVariableLibelle() {
  }
  
  public void setData() {
    mettreAJourVariableLibelle();
    // Valeur
    // label2.setText(lexique.HostFieldGetData("MG01"));
    
    // GRAPHE
    
    String[] libelle = new String[12];
    String[] donnee1 = new String[12];
    String[] donnee2 = new String[12];
    
    // Chargement des libellés
    for (int i = 0; i < libelle.length; i++) {
      libelle[i] = lexique.HostFieldGetData("L4C" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1)));
    }
    
    // Chargement des données
    for (int i = 0; i < donnee1.length; i++) {
      donnee1[i] = lexique.HostFieldGetNumericData("UV" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1)) + "1".trim());
    }
    for (int i = 0; i < donnee2.length; i++) {
      donnee2[i] = lexique.HostFieldGetNumericData("UV" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1)) + "2".trim());
    }
    
    // Préparation des données
    Object[][] data = new Object[libelle.length][2];
    Object[][] data2 = new Object[libelle.length][2];
    for (int i = 0; i < libelle.length; i++) {
      data[i][0] = libelle[i];
      data2[i][0] = libelle[i];
      data[i][1] = Double.parseDouble(donnee1[i]);
      data2[i][1] = Double.parseDouble(donnee2[i]);
    }
    
    graphe.setDonnee(data, "", false);
    graphe.getGraphe("Du " + lexique.HostFieldGetData("XGDE1V") + " au " + lexique.HostFieldGetData("XGFE1V"), false);
    
    graphe2.setDonnee(data2, "", false);
    graphe2.getGraphe("Exercice précédent", false);
    
    l_graphe.setIcon(graphe.getPicture(l_graphe.getWidth(), l_graphe.getHeight()));
    l_graphe2.setIcon(graphe2.getPicture(l_graphe2.getWidth(), l_graphe2.getHeight()));
    
    
    
    // TODO Icones
    OBJ_10.setIcon(lexique.chargerImage("images/retour_p.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Détail des statistiques"));
  }
  
  public void getData() {
    
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    getData();
    dispose();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    P_Centre = new JPanel();
    l_graphe = new JLabel();
    l_graphe2 = new JLabel();
    separator1 = compFactory.createSeparator("");
    OBJ_10 = new JButton();
    
    // ======== this ========
    setMinimumSize(new Dimension(335, 220));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== P_Centre ========
    {
      P_Centre.setBackground(new Color(238, 238, 210));
      P_Centre.setName("P_Centre");
      
      // ---- l_graphe ----
      l_graphe.setHorizontalAlignment(SwingConstants.CENTER);
      l_graphe.setComponentPopupMenu(null);
      l_graphe.setBackground(new Color(214, 217, 223));
      l_graphe.setName("l_graphe");
      
      // ---- l_graphe2 ----
      l_graphe2.setHorizontalAlignment(SwingConstants.CENTER);
      l_graphe2.setComponentPopupMenu(null);
      l_graphe2.setBackground(new Color(214, 217, 223));
      l_graphe2.setName("l_graphe2");
      
      // ---- separator1 ----
      separator1.setName("separator1");
      
      // ---- OBJ_10 ----
      OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_10.setToolTipText("Retour");
      OBJ_10.setText("Retour");
      OBJ_10.setFont(OBJ_10.getFont().deriveFont(OBJ_10.getFont().getStyle() | Font.BOLD, OBJ_10.getFont().getSize() + 3f));
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      
      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup().addContainerGap()
              .addGroup(P_CentreLayout.createParallelGroup().addComponent(separator1, GroupLayout.DEFAULT_SIZE, 1068, Short.MAX_VALUE)
                  .addGroup(P_CentreLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                      .addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                      .addGroup(P_CentreLayout.createSequentialGroup()
                          .addComponent(l_graphe, GroupLayout.PREFERRED_SIZE, 523, GroupLayout.PREFERRED_SIZE)
                          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                          .addComponent(l_graphe2, GroupLayout.PREFERRED_SIZE, 496, GroupLayout.PREFERRED_SIZE))))
              .addContainerGap()));
      P_CentreLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] { l_graphe, l_graphe2 });
      P_CentreLayout.setVerticalGroup(P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup().addGap(11, 11, 11)
              .addComponent(separator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGap(18, 18, 18)
              .addGroup(P_CentreLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                  .addComponent(l_graphe, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(l_graphe2, GroupLayout.DEFAULT_SIZE, 499, Short.MAX_VALUE))
              .addGap(80, 80, 80).addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addContainerGap()));
    }
    contentPane.add(P_Centre, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel P_Centre;
  private JLabel l_graphe;
  private JLabel l_graphe2;
  private JComponent separator1;
  private JButton OBJ_10;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
