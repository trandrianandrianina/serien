
package ri.serien.libecranrpg.vgam.VGAM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGAM01FX_UN extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] UNDEC_Value = { "", "0", "1", "2", "3", };
  private String[] UNNAT_Value = { "", "N", "L", "S", "V", "P", "T", };
  private String[] UNCND_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", };
  
  public VGAM01FX_UN(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    UNCND.setValeurs(UNCND_Value, null);
    UNDEC.setValeurs(UNDEC_Value, null);
    UNNAT.setValeurs(UNNAT_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_66.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUL201@")).trim());
    OBJ_71.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUL202@")).trim());
    OBJ_76.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUL203@")).trim());
    OBJ_81.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUL204@")).trim());
    OBJ_87.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUL205@")).trim());
    OBJ_93.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUL206@")).trim());
    OBJ_98.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUL207@")).trim());
    OBJ_103.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUL208@")).trim());
    OBJ_108.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUL209@")).trim());
    OBJ_116.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUL210@")).trim());
    OBJ_62.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UL01R@")).trim());
    OBJ_67.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UL02R@")).trim());
    OBJ_72.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UL03R@")).trim());
    OBJ_77.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UL04R@")).trim());
    OBJ_83.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UL05R@")).trim());
    OBJ_89.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UL06R@")).trim());
    OBJ_94.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UL07R@")).trim());
    OBJ_99.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UL08R@")).trim());
    OBJ_104.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UL09R@")).trim());
    OBJ_109.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UL10R@")).trim());
    INDUNU.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDUNU@")).trim());
    INDUN2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDUN2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    // UNDEC.setSelectedIndex(getIndice("UNDEC", UNDEC_Value));
    // UNNAT.setSelectedIndex(getIndice("UNNAT", UNNAT_Value));
    // UNCND.setSelectedIndex(getIndice("UNCND", UNCND_Value));
    
    UNU10.setEnabled(lexique.isPresent("UNU10"));
    UNU9.setEnabled(lexique.isPresent("UNU9"));
    UNU8.setEnabled(lexique.isPresent("UNU8"));
    UNU7.setEnabled(lexique.isPresent("UNU7"));
    UNU6.setEnabled(lexique.isPresent("UNU6"));
    UNU5.setEnabled(lexique.isPresent("UNU5"));
    UNU4.setEnabled(lexique.isPresent("UNU4"));
    UNU3.setEnabled(lexique.isPresent("UNU3"));
    UNU2.setEnabled(lexique.isPresent("UNU2"));
    UNU1.setEnabled(lexique.isPresent("UNU1"));
    INDUNU.setVisible(lexique.isPresent("INDUNU"));
    UNREFU.setEnabled(lexique.isPresent("UNREFU"));
    UNTAR.setVisible(lexique.isPresent("UNTAR"));
    UNK10.setEnabled(lexique.isPresent("UNK10"));
    UNK09.setEnabled(lexique.isPresent("UNK09"));
    UNK08.setEnabled(lexique.isPresent("UNK08"));
    UNK07.setEnabled(lexique.isPresent("UNK07"));
    UNK06.setEnabled(lexique.isPresent("UNK06"));
    UNK04.setEnabled(lexique.isPresent("UNK04"));
    UNK03.setEnabled(lexique.isPresent("UNK03"));
    UNK02.setEnabled(lexique.isPresent("UNK02"));
    UNK01.setEnabled(lexique.isPresent("UNK01"));
    UNK05.setEnabled(lexique.isPresent("UNK05"));
    OBJ_109.setVisible(lexique.isPresent("UL10R"));
    OBJ_104.setVisible(lexique.isPresent("UL09R"));
    OBJ_99.setVisible(lexique.isPresent("UL08R"));
    OBJ_94.setVisible(lexique.isPresent("UL07R"));
    OBJ_89.setVisible(lexique.isPresent("UL06R"));
    OBJ_83.setVisible(lexique.isPresent("UL05R"));
    OBJ_77.setVisible(lexique.isPresent("UL04R"));
    OBJ_72.setVisible(lexique.isPresent("UL03R"));
    OBJ_67.setVisible(lexique.isPresent("UL02R"));
    OBJ_62.setVisible(lexique.isPresent("UL01R"));
    OBJ_116.setVisible(lexique.isPresent("WUL210"));
    OBJ_108.setVisible(lexique.isPresent("WUL209"));
    OBJ_103.setVisible(lexique.isPresent("WUL208"));
    OBJ_98.setVisible(lexique.isPresent("WUL207"));
    OBJ_93.setVisible(lexique.isPresent("WUL206"));
    OBJ_87.setVisible(lexique.isPresent("WUL205"));
    OBJ_81.setVisible(lexique.isPresent("WUL204"));
    OBJ_76.setVisible(lexique.isPresent("WUL203"));
    OBJ_71.setVisible(lexique.isPresent("WUL202"));
    OBJ_66.setVisible(lexique.isPresent("WUL201"));
    UNLIB2.setEnabled(lexique.isPresent("UNLIB2"));
    UNLIB.setEnabled(lexique.isPresent("UNLIB"));
    OBJ_59.setVisible(lexique.isPresent("UNREFU"));
    // UNCND.setVisible( lexique.isPresent("UNCND"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - Personnalisation de @LOCGRP/-1/@"));
    
    

    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("UNDEC", 0, UNDEC_Value[UNDEC.getSelectedIndex()]);
    // lexique.HostFieldPutData("UNNAT", 0, UNNAT_Value[UNNAT.getSelectedIndex()]);
    // lexique.HostFieldPutData("UNCND", 0, UNCND_Value[UNCND.getSelectedIndex()]);
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_48 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel2 = new JXTitledPanel();
    panel2 = new JPanel();
    UNNAT = new XRiComboBox();
    UNDEC = new XRiComboBox();
    OBJ_57 = new JLabel();
    OBJ_53 = new JLabel();
    UNLIB = new XRiTextField();
    OBJ_56 = new JLabel();
    UNLIB2 = new XRiTextField();
    OBJ_51 = new JLabel();
    panel3 = new JPanel();
    UNCND = new XRiComboBox();
    OBJ_88 = new JLabel();
    OBJ_59 = new JLabel();
    OBJ_60 = new JLabel();
    OBJ_82 = new JLabel();
    OBJ_66 = new RiZoneSortie();
    OBJ_71 = new RiZoneSortie();
    OBJ_76 = new RiZoneSortie();
    OBJ_81 = new RiZoneSortie();
    OBJ_87 = new RiZoneSortie();
    OBJ_93 = new RiZoneSortie();
    OBJ_98 = new RiZoneSortie();
    OBJ_103 = new RiZoneSortie();
    OBJ_108 = new RiZoneSortie();
    OBJ_116 = new RiZoneSortie();
    OBJ_62 = new JLabel();
    OBJ_67 = new JLabel();
    OBJ_72 = new JLabel();
    OBJ_77 = new JLabel();
    OBJ_83 = new JLabel();
    OBJ_89 = new JLabel();
    OBJ_94 = new JLabel();
    OBJ_99 = new JLabel();
    OBJ_104 = new JLabel();
    OBJ_109 = new JLabel();
    UNK05 = new XRiTextField();
    UNK01 = new XRiTextField();
    UNK02 = new XRiTextField();
    UNK03 = new XRiTextField();
    UNK04 = new XRiTextField();
    UNK06 = new XRiTextField();
    UNK07 = new XRiTextField();
    UNK08 = new XRiTextField();
    UNK09 = new XRiTextField();
    UNK10 = new XRiTextField();
    UNTAR = new XRiTextField();
    UNREFU = new XRiTextField();
    INDUNU = new RiZoneSortie();
    UNU1 = new XRiTextField();
    UNU2 = new XRiTextField();
    UNU3 = new XRiTextField();
    UNU4 = new XRiTextField();
    UNU5 = new XRiTextField();
    UNU6 = new XRiTextField();
    UNU7 = new XRiTextField();
    UNU8 = new XRiTextField();
    UNU9 = new XRiTextField();
    UNU10 = new XRiTextField();
    OBJ_64 = new JLabel();
    OBJ_69 = new JLabel();
    OBJ_74 = new JLabel();
    OBJ_79 = new JLabel();
    OBJ_85 = new JLabel();
    OBJ_91 = new JLabel();
    OBJ_96 = new JLabel();
    OBJ_101 = new JLabel();
    OBJ_106 = new JLabel();
    OBJ_111 = new JLabel();
    OBJ_61 = new JLabel();
    INDUN2 = new RiZoneSortie();
    UNKUNN = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des achats");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44 ----
          OBJ_44.setText("Etablissement");
          OBJ_44.setName("OBJ_44");

          //---- INDETB ----
          INDETB.setName("INDETB");

          //---- OBJ_46 ----
          OBJ_46.setText("Code");
          OBJ_46.setName("OBJ_46");

          //---- INDTYP ----
          INDTYP.setName("INDTYP");

          //---- OBJ_48 ----
          OBJ_48.setText("Ordre");
          OBJ_48.setName("OBJ_48");

          //---- INDIND ----
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(513, Short.MAX_VALUE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(840, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setTitle("Code unit\u00e9 de mesure");
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder(""));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- UNNAT ----
              UNNAT.setModel(new DefaultComboBoxModel(new String[] {
                "",
                "D\u00e9nombrement",
                "Longueur",
                "Surface",
                "Volume",
                "Poids",
                "Temps"
              }));
              UNNAT.setComponentPopupMenu(BTD);
              UNNAT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              UNNAT.setName("UNNAT");
              panel2.add(UNNAT);
              UNNAT.setBounds(590, 21, 136, UNNAT.getPreferredSize().height);

              //---- UNDEC ----
              UNDEC.setModel(new DefaultComboBoxModel(new String[] {
                "",
                "n",
                "n,0",
                "n,00",
                "n,000"
              }));
              UNDEC.setComponentPopupMenu(BTD);
              UNDEC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              UNDEC.setName("UNDEC");
              panel2.add(UNDEC);
              UNDEC.setBounds(655, 61, 71, UNDEC.getPreferredSize().height);

              //---- OBJ_57 ----
              OBJ_57.setText("Nombre de d\u00e9cimales");
              OBJ_57.setName("OBJ_57");
              panel2.add(OBJ_57);
              OBJ_57.setBounds(455, 64, 138, 20);

              //---- OBJ_53 ----
              OBJ_53.setText("Syst\u00e8me de mesure");
              OBJ_53.setName("OBJ_53");
              panel2.add(OBJ_53);
              OBJ_53.setBounds(455, 24, 136, 20);

              //---- UNLIB ----
              UNLIB.setComponentPopupMenu(BTD);
              UNLIB.setName("UNLIB");
              panel2.add(UNLIB);
              UNLIB.setBounds(145, 20, 160, UNLIB.getPreferredSize().height);

              //---- OBJ_56 ----
              OBJ_56.setText("Libell\u00e9 court");
              OBJ_56.setName("OBJ_56");
              panel2.add(OBJ_56);
              OBJ_56.setBounds(40, 64, 83, 20);

              //---- UNLIB2 ----
              UNLIB2.setComponentPopupMenu(BTD);
              UNLIB2.setName("UNLIB2");
              panel2.add(UNLIB2);
              UNLIB2.setBounds(145, 60, 60, UNLIB2.getPreferredSize().height);

              //---- OBJ_51 ----
              OBJ_51.setText("Libell\u00e9 unit\u00e9");
              OBJ_51.setName("OBJ_51");
              panel2.add(OBJ_51);
              OBJ_51.setBounds(40, 24, 75, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel2ContentContainer.add(panel2);
            panel2.setBounds(20, 15, 760, 110);

            //======== panel3 ========
            {
              panel3.setBorder(new TitledBorder(""));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- UNCND ----
              UNCND.setModel(new DefaultComboBoxModel(new String[] {
                "",
                "Bons",
                "Facture",
                "Bons et factures",
                "Bons avec prix en unit\u00e9 de conditionnement",
                "Factures avec prix en unit\u00e9 de conditionnement",
                "Bons et factures avec prix en unit\u00e9 de conditionnement",
                "4 si quantit\u00e9 multiple de conditionnement",
                "5 si quantit\u00e9 multiple de conditionnement",
                "6 si quantit\u00e9 multiple de conditionnement",
                "1 (quantit\u00e9 et unit\u00e9 en unit\u00e9 de conditionnement)",
                "2 (quantit\u00e9 et unit\u00e9 en unit\u00e9 de conditionnement)",
                "3(quantit\u00e9 et unit\u00e9 en unit\u00e9 de conditionnement)"
              }));
              UNCND.setComponentPopupMenu(BTD);
              UNCND.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              UNCND.setName("UNCND");
              panel3.add(UNCND);
              UNCND.setBounds(455, 185, 274, UNCND.getPreferredSize().height);

              //---- OBJ_88 ----
              OBJ_88.setText("Pas de conditionnement sur \u00e9dition");
              OBJ_88.setName("OBJ_88");
              panel3.add(OBJ_88);
              OBJ_88.setBounds(455, 165, 211, 20);

              //---- OBJ_59 ----
              OBJ_59.setText("Unit\u00e9 r\u00e9f\u00e9rence encombrement");
              OBJ_59.setName("OBJ_59");
              panel3.add(OBJ_59);
              OBJ_59.setBounds(455, 230, 187, 20);

              //---- OBJ_60 ----
              OBJ_60.setText("1 Unit\u00e9 --> Nombre de");
              OBJ_60.setName("OBJ_60");
              panel3.add(OBJ_60);
              OBJ_60.setBounds(80, 11, 138, 28);

              //---- OBJ_82 ----
              OBJ_82.setText("Tare (en KG)");
              OBJ_82.setName("OBJ_82");
              panel3.add(OBJ_82);
              OBJ_82.setBounds(455, 130, 79, 20);

              //---- OBJ_66 ----
              OBJ_66.setText("@WUL201@");
              OBJ_66.setName("OBJ_66");
              panel3.add(OBJ_66);
              OBJ_66.setBounds(300, 43, 95, OBJ_66.getPreferredSize().height);

              //---- OBJ_71 ----
              OBJ_71.setText("@WUL202@");
              OBJ_71.setName("OBJ_71");
              panel3.add(OBJ_71);
              OBJ_71.setBounds(300, 74, 95, OBJ_71.getPreferredSize().height);

              //---- OBJ_76 ----
              OBJ_76.setText("@WUL203@");
              OBJ_76.setName("OBJ_76");
              panel3.add(OBJ_76);
              OBJ_76.setBounds(300, 105, 95, OBJ_76.getPreferredSize().height);

              //---- OBJ_81 ----
              OBJ_81.setText("@WUL204@");
              OBJ_81.setName("OBJ_81");
              panel3.add(OBJ_81);
              OBJ_81.setBounds(300, 136, 95, OBJ_81.getPreferredSize().height);

              //---- OBJ_87 ----
              OBJ_87.setText("@WUL205@");
              OBJ_87.setName("OBJ_87");
              panel3.add(OBJ_87);
              OBJ_87.setBounds(300, 167, 95, OBJ_87.getPreferredSize().height);

              //---- OBJ_93 ----
              OBJ_93.setText("@WUL206@");
              OBJ_93.setName("OBJ_93");
              panel3.add(OBJ_93);
              OBJ_93.setBounds(300, 198, 95, OBJ_93.getPreferredSize().height);

              //---- OBJ_98 ----
              OBJ_98.setText("@WUL207@");
              OBJ_98.setName("OBJ_98");
              panel3.add(OBJ_98);
              OBJ_98.setBounds(300, 229, 95, OBJ_98.getPreferredSize().height);

              //---- OBJ_103 ----
              OBJ_103.setText("@WUL208@");
              OBJ_103.setName("OBJ_103");
              panel3.add(OBJ_103);
              OBJ_103.setBounds(300, 260, 95, OBJ_103.getPreferredSize().height);

              //---- OBJ_108 ----
              OBJ_108.setText("@WUL209@");
              OBJ_108.setName("OBJ_108");
              panel3.add(OBJ_108);
              OBJ_108.setBounds(300, 291, 95, OBJ_108.getPreferredSize().height);

              //---- OBJ_116 ----
              OBJ_116.setText("@WUL210@");
              OBJ_116.setName("OBJ_116");
              panel3.add(OBJ_116);
              OBJ_116.setBounds(300, 322, 95, OBJ_116.getPreferredSize().height);

              //---- OBJ_62 ----
              OBJ_62.setText("@UL01R@");
              OBJ_62.setName("OBJ_62");
              panel3.add(OBJ_62);
              OBJ_62.setBounds(40, 45, 66, 20);

              //---- OBJ_67 ----
              OBJ_67.setText("@UL02R@");
              OBJ_67.setName("OBJ_67");
              panel3.add(OBJ_67);
              OBJ_67.setBounds(40, 76, 66, 20);

              //---- OBJ_72 ----
              OBJ_72.setText("@UL03R@");
              OBJ_72.setName("OBJ_72");
              panel3.add(OBJ_72);
              OBJ_72.setBounds(40, 107, 66, 20);

              //---- OBJ_77 ----
              OBJ_77.setText("@UL04R@");
              OBJ_77.setName("OBJ_77");
              panel3.add(OBJ_77);
              OBJ_77.setBounds(40, 138, 66, 20);

              //---- OBJ_83 ----
              OBJ_83.setText("@UL05R@");
              OBJ_83.setName("OBJ_83");
              panel3.add(OBJ_83);
              OBJ_83.setBounds(40, 169, 66, 20);

              //---- OBJ_89 ----
              OBJ_89.setText("@UL06R@");
              OBJ_89.setName("OBJ_89");
              panel3.add(OBJ_89);
              OBJ_89.setBounds(40, 200, 66, 20);

              //---- OBJ_94 ----
              OBJ_94.setText("@UL07R@");
              OBJ_94.setName("OBJ_94");
              panel3.add(OBJ_94);
              OBJ_94.setBounds(40, 231, 66, 20);

              //---- OBJ_99 ----
              OBJ_99.setText("@UL08R@");
              OBJ_99.setName("OBJ_99");
              panel3.add(OBJ_99);
              OBJ_99.setBounds(40, 262, 66, 20);

              //---- OBJ_104 ----
              OBJ_104.setText("@UL09R@");
              OBJ_104.setName("OBJ_104");
              panel3.add(OBJ_104);
              OBJ_104.setBounds(40, 293, 66, 20);

              //---- OBJ_109 ----
              OBJ_109.setText("@UL10R@");
              OBJ_109.setName("OBJ_109");
              panel3.add(OBJ_109);
              OBJ_109.setBounds(40, 324, 66, 20);

              //---- UNK05 ----
              UNK05.setComponentPopupMenu(BTD);
              UNK05.setName("UNK05");
              panel3.add(UNK05);
              UNK05.setBounds(215, 165, 74, UNK05.getPreferredSize().height);

              //---- UNK01 ----
              UNK01.setComponentPopupMenu(BTD);
              UNK01.setName("UNK01");
              panel3.add(UNK01);
              UNK01.setBounds(215, 41, 74, UNK01.getPreferredSize().height);

              //---- UNK02 ----
              UNK02.setComponentPopupMenu(BTD);
              UNK02.setName("UNK02");
              panel3.add(UNK02);
              UNK02.setBounds(215, 72, 74, UNK02.getPreferredSize().height);

              //---- UNK03 ----
              UNK03.setComponentPopupMenu(BTD);
              UNK03.setName("UNK03");
              panel3.add(UNK03);
              UNK03.setBounds(215, 103, 74, UNK03.getPreferredSize().height);

              //---- UNK04 ----
              UNK04.setComponentPopupMenu(BTD);
              UNK04.setName("UNK04");
              panel3.add(UNK04);
              UNK04.setBounds(215, 134, 74, UNK04.getPreferredSize().height);

              //---- UNK06 ----
              UNK06.setComponentPopupMenu(BTD);
              UNK06.setName("UNK06");
              panel3.add(UNK06);
              UNK06.setBounds(215, 196, 74, UNK06.getPreferredSize().height);

              //---- UNK07 ----
              UNK07.setComponentPopupMenu(BTD);
              UNK07.setName("UNK07");
              panel3.add(UNK07);
              UNK07.setBounds(215, 227, 74, UNK07.getPreferredSize().height);

              //---- UNK08 ----
              UNK08.setComponentPopupMenu(BTD);
              UNK08.setName("UNK08");
              panel3.add(UNK08);
              UNK08.setBounds(215, 258, 74, UNK08.getPreferredSize().height);

              //---- UNK09 ----
              UNK09.setComponentPopupMenu(BTD);
              UNK09.setName("UNK09");
              panel3.add(UNK09);
              UNK09.setBounds(215, 289, 74, UNK09.getPreferredSize().height);

              //---- UNK10 ----
              UNK10.setComponentPopupMenu(BTD);
              UNK10.setName("UNK10");
              panel3.add(UNK10);
              UNK10.setBounds(215, 320, 74, UNK10.getPreferredSize().height);

              //---- UNTAR ----
              UNTAR.setComponentPopupMenu(BTD);
              UNTAR.setName("UNTAR");
              panel3.add(UNTAR);
              UNTAR.setBounds(655, 126, 74, UNTAR.getPreferredSize().height);

              //---- UNREFU ----
              UNREFU.setComponentPopupMenu(BTD);
              UNREFU.setName("UNREFU");
              panel3.add(UNREFU);
              UNREFU.setBounds(699, 226, 30, UNREFU.getPreferredSize().height);

              //---- INDUNU ----
              INDUNU.setText("@INDUNU@");
              INDUNU.setName("INDUNU");
              panel3.add(INDUNU);
              INDUNU.setBounds(215, 11, 30, 28);

              //---- UNU1 ----
              UNU1.setComponentPopupMenu(BTD);
              UNU1.setName("UNU1");
              panel3.add(UNU1);
              UNU1.setBounds(145, 41, 30, UNU1.getPreferredSize().height);

              //---- UNU2 ----
              UNU2.setComponentPopupMenu(BTD);
              UNU2.setName("UNU2");
              panel3.add(UNU2);
              UNU2.setBounds(145, 72, 30, UNU2.getPreferredSize().height);

              //---- UNU3 ----
              UNU3.setComponentPopupMenu(BTD);
              UNU3.setName("UNU3");
              panel3.add(UNU3);
              UNU3.setBounds(145, 103, 30, UNU3.getPreferredSize().height);

              //---- UNU4 ----
              UNU4.setComponentPopupMenu(BTD);
              UNU4.setName("UNU4");
              panel3.add(UNU4);
              UNU4.setBounds(145, 134, 30, UNU4.getPreferredSize().height);

              //---- UNU5 ----
              UNU5.setComponentPopupMenu(BTD);
              UNU5.setName("UNU5");
              panel3.add(UNU5);
              UNU5.setBounds(145, 165, 30, UNU5.getPreferredSize().height);

              //---- UNU6 ----
              UNU6.setComponentPopupMenu(BTD);
              UNU6.setName("UNU6");
              panel3.add(UNU6);
              UNU6.setBounds(145, 196, 30, UNU6.getPreferredSize().height);

              //---- UNU7 ----
              UNU7.setComponentPopupMenu(BTD);
              UNU7.setName("UNU7");
              panel3.add(UNU7);
              UNU7.setBounds(145, 227, 30, UNU7.getPreferredSize().height);

              //---- UNU8 ----
              UNU8.setComponentPopupMenu(BTD);
              UNU8.setName("UNU8");
              panel3.add(UNU8);
              UNU8.setBounds(145, 258, 30, UNU8.getPreferredSize().height);

              //---- UNU9 ----
              UNU9.setComponentPopupMenu(BTD);
              UNU9.setName("UNU9");
              panel3.add(UNU9);
              UNU9.setBounds(145, 289, 30, UNU9.getPreferredSize().height);

              //---- UNU10 ----
              UNU10.setComponentPopupMenu(BTD);
              UNU10.setName("UNU10");
              panel3.add(UNU10);
              UNU10.setBounds(145, 320, 30, UNU10.getPreferredSize().height);

              //---- OBJ_64 ----
              OBJ_64.setText("=");
              OBJ_64.setName("OBJ_64");
              panel3.add(OBJ_64);
              OBJ_64.setBounds(190, 45, 10, 20);

              //---- OBJ_69 ----
              OBJ_69.setText("=");
              OBJ_69.setName("OBJ_69");
              panel3.add(OBJ_69);
              OBJ_69.setBounds(190, 76, 10, 20);

              //---- OBJ_74 ----
              OBJ_74.setText("=");
              OBJ_74.setName("OBJ_74");
              panel3.add(OBJ_74);
              OBJ_74.setBounds(190, 107, 10, 20);

              //---- OBJ_79 ----
              OBJ_79.setText("=");
              OBJ_79.setName("OBJ_79");
              panel3.add(OBJ_79);
              OBJ_79.setBounds(190, 138, 10, 20);

              //---- OBJ_85 ----
              OBJ_85.setText("=");
              OBJ_85.setName("OBJ_85");
              panel3.add(OBJ_85);
              OBJ_85.setBounds(190, 169, 10, 20);

              //---- OBJ_91 ----
              OBJ_91.setText("=");
              OBJ_91.setName("OBJ_91");
              panel3.add(OBJ_91);
              OBJ_91.setBounds(190, 200, 10, 20);

              //---- OBJ_96 ----
              OBJ_96.setText("=");
              OBJ_96.setName("OBJ_96");
              panel3.add(OBJ_96);
              OBJ_96.setBounds(190, 231, 10, 20);

              //---- OBJ_101 ----
              OBJ_101.setText("=");
              OBJ_101.setName("OBJ_101");
              panel3.add(OBJ_101);
              OBJ_101.setBounds(190, 262, 10, 20);

              //---- OBJ_106 ----
              OBJ_106.setText("=");
              OBJ_106.setName("OBJ_106");
              panel3.add(OBJ_106);
              OBJ_106.setBounds(190, 293, 10, 20);

              //---- OBJ_111 ----
              OBJ_111.setText("=");
              OBJ_111.setName("OBJ_111");
              panel3.add(OBJ_111);
              OBJ_111.setBounds(190, 325, 10, 20);

              //---- OBJ_61 ----
              OBJ_61.setText("Nombre d'unit\u00e9s dans un ");
              OBJ_61.setName("OBJ_61");
              panel3.add(OBJ_61);
              OBJ_61.setBounds(455, 11, 150, 28);

              //---- INDUN2 ----
              INDUN2.setText("@INDUN2@");
              INDUN2.setName("INDUN2");
              panel3.add(INDUN2);
              INDUN2.setBounds(605, 11, 30, 28);

              //---- UNKUNN ----
              UNKUNN.setComponentPopupMenu(BTD);
              UNKUNN.setName("UNKUNN");
              panel3.add(UNKUNN);
              UNKUNN.setBounds(655, 11, 52, 28);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel2ContentContainer.add(panel3);
            panel3.setBounds(20, 130, 760, 380);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 803, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(23, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 560, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(5, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44;
  private XRiTextField INDETB;
  private JLabel OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_48;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel2;
  private JPanel panel2;
  private XRiComboBox UNNAT;
  private XRiComboBox UNDEC;
  private JLabel OBJ_57;
  private JLabel OBJ_53;
  private XRiTextField UNLIB;
  private JLabel OBJ_56;
  private XRiTextField UNLIB2;
  private JLabel OBJ_51;
  private JPanel panel3;
  private XRiComboBox UNCND;
  private JLabel OBJ_88;
  private JLabel OBJ_59;
  private JLabel OBJ_60;
  private JLabel OBJ_82;
  private RiZoneSortie OBJ_66;
  private RiZoneSortie OBJ_71;
  private RiZoneSortie OBJ_76;
  private RiZoneSortie OBJ_81;
  private RiZoneSortie OBJ_87;
  private RiZoneSortie OBJ_93;
  private RiZoneSortie OBJ_98;
  private RiZoneSortie OBJ_103;
  private RiZoneSortie OBJ_108;
  private RiZoneSortie OBJ_116;
  private JLabel OBJ_62;
  private JLabel OBJ_67;
  private JLabel OBJ_72;
  private JLabel OBJ_77;
  private JLabel OBJ_83;
  private JLabel OBJ_89;
  private JLabel OBJ_94;
  private JLabel OBJ_99;
  private JLabel OBJ_104;
  private JLabel OBJ_109;
  private XRiTextField UNK05;
  private XRiTextField UNK01;
  private XRiTextField UNK02;
  private XRiTextField UNK03;
  private XRiTextField UNK04;
  private XRiTextField UNK06;
  private XRiTextField UNK07;
  private XRiTextField UNK08;
  private XRiTextField UNK09;
  private XRiTextField UNK10;
  private XRiTextField UNTAR;
  private XRiTextField UNREFU;
  private RiZoneSortie INDUNU;
  private XRiTextField UNU1;
  private XRiTextField UNU2;
  private XRiTextField UNU3;
  private XRiTextField UNU4;
  private XRiTextField UNU5;
  private XRiTextField UNU6;
  private XRiTextField UNU7;
  private XRiTextField UNU8;
  private XRiTextField UNU9;
  private XRiTextField UNU10;
  private JLabel OBJ_64;
  private JLabel OBJ_69;
  private JLabel OBJ_74;
  private JLabel OBJ_79;
  private JLabel OBJ_85;
  private JLabel OBJ_91;
  private JLabel OBJ_96;
  private JLabel OBJ_101;
  private JLabel OBJ_106;
  private JLabel OBJ_111;
  private JLabel OBJ_61;
  private RiZoneSortie INDUN2;
  private XRiTextField UNKUNN;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
