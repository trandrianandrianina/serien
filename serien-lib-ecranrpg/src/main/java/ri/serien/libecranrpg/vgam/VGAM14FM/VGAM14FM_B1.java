
package ri.serien.libecranrpg.vgam.VGAM14FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerListModel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.metier.referentiel.article.snphotoarticle.SNPhotoArticle;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiSpinner;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGAM14FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] A1IN16_Value = { "", "1", "2", "3", };
  private String[] A1REA_Value = { "", "0", "1", "2", "3", "4", "5", "6", "7", };
  
  public VGAM14FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    A1IN16.setValeurs(A1IN16_Value, null);
    A1REA.setValeurs(A1REA_Value, null);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDMAG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDMAG@")).trim());
    A1FAM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1FAM@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    xtp1.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("Magasin @S1MAG@ @MALIB@")).trim());
    LRMINX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRMINX@")).trim());
    LRMAXX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRMAXX@")).trim());
    LRQTEX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRQTEX@")).trim());
    WQTA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WQTA@")).trim());
    WDEV1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDEV1@")).trim());
    WUNS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUNS@")).trim());
    A1UNS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNS@")).trim());
    A1UNA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNA@")).trim());
    A1IN20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1IN20@")).trim());
    WPRAX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPRAX@")).trim());
    WPOSX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPOSX@")).trim());
    A1PRDX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1PRDX@")).trim());
    A1PRFX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1PRFX@")).trim());
    A1PERK.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1PERK@")).trim());
    LRPRRX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRPRRX@")).trim());
    LRPRDX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRPRDX@")).trim());
    LRMRRX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRMRRX@")).trim());
    LRMRDX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRMRDX@")).trim());
    xTitledPanel1.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@RAFRS@")).trim());
    WATTN2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WATTN2@")).trim());
    OBJ_183.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRNOMA@")).trim());
    OBJ_132.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CALIB@")).trim());
    UPNDVX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UPNDVX@")).trim());
    UPNETX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UPNETX@")).trim());
    CAPRAX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAPRAX@")).trim());
    CAKSC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAKSC@")).trim());
    CAKACX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAKACX@")).trim());
    WCHGX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCHGX@")).trim());
    CAQMI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAQMI@")).trim());
    CAQEC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAQEC@")).trim());
    CANUA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CANUA@")).trim());
    OBJ_182.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRSGA@")).trim());
    CAREX1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAREX1@")).trim());
    CAREX2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAREX2@")).trim());
    CAREX3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAREX3@")).trim());
    CAREX4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAREX4@")).trim());
    CAREX5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAREX5@")).trim());
    CAREX6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAREX6@")).trim());
    CADEV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CADEV@")).trim());
    CAUNC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAUNC@")).trim());
    CAUNA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAUNA@")).trim());
    OBJ_180.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WECAM1@")).trim());
    CADEL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CADEL@")).trim());
    CADELS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CADELS@")).trim());
    OBJ_172.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SEM@")).trim());
    FRFRSG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRFRSG@")).trim());
    OBJ_54.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ULBFRS@")).trim());
    INDAR0.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDAR0@")).trim());
    A1LIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    A1IN4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1IN4@")).trim());
    A1ASB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1ASB@")).trim());
    OBJ_100.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CONS@")).trim());
    OBJ_101.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CONS2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    xtp1.setTitle("Magasin " + lexique.HostFieldGetData("S1MAG") + " " + lexique.HostFieldGetData("MALIB"));
    xTitledPanel1.setTitle(lexique.HostFieldGetData("RAFRS"));
    
    boolean isConsult = (lexique.getMode() == Lexical.MODE_CONSULTATION);
    riSousMenu6.setEnabled(!isConsult);
    riSousMenu10.setEnabled(!isConsult);
    OBJ_108.setVisible(lexique.isPresent("A1PERK"));
    A1IN4.setVisible(lexique.isPresent("A1IN4"));
    OBJ_80.setVisible(lexique.HostFieldGetData("V01F").equalsIgnoreCase("MODIFIC."));
    OBJ_111.setVisible(lexique.isPresent("A1PRDX"));
    INDMAG.setVisible(lexique.isPresent("INDMAG"));
    CAUNA.setVisible(lexique.isPresent("CAUNA"));
    CAUNC.setVisible(lexique.isPresent("CAUNC"));
    // CAIN2.setVisible( lexique.isPresent("CAIN2"));
    CAIN2.setEnabled(!isConsult);
    A1UNA.setVisible(lexique.isPresent("A1UNA"));
    A1UNS.setVisible(lexique.isPresent("A1UNS"));
    WUNS.setVisible(lexique.isPresent("WUNS"));
    A1PERK.setVisible(lexique.isPresent("A1PERK"));
    A1FAM.setVisible(lexique.isPresent("A1FAM"));
    INDETB.setVisible(lexique.isPresent("INDETB"));
    WDEV1.setVisible(lexique.isPresent("WDEV1"));
    OBJ_170.setEnabled(lexique.isPresent("CADEL"));
    OBJ_156.setEnabled(lexique.isPresent("CAQEC"));
    A1PRFX.setVisible(lexique.isPresent("A1PRFX"));
    A1PRDX.setVisible(lexique.isPresent("A1PRDX"));
    OBJ_155.setEnabled(lexique.isPresent("CAQMI"));
    OBJ_139.setVisible(lexique.isPresent("UPNDVX"));
    OBJ_75.setVisible(lexique.isPresent("WDEV1"));
    LRDTRX.setEditable(!isConsult);
    separator1.setVisible(lexique.isPresent("RAPRRX"));
    separator2.setVisible(lexique.isPresent("RAPRRX"));
    OBJ_182.setVisible(lexique.isPresent("FRSGA"));
    CANUA.setEnabled(lexique.isPresent("CANUA"));
    CAQEC.setEnabled(lexique.isPresent("CAQEC"));
    CAQMI.setEnabled(lexique.isPresent("CAQMI"));
    OBJ_149.setVisible(lexique.isPresent("UPNETX"));
    OBJ_157.setEnabled(lexique.isPresent("CANUA"));
    RACMJX.setVisible(lexique.isPresent("RACMJX"));
    WQTA.setVisible(lexique.isPresent("WQTA"));
    OBJ_106.setVisible(lexique.isPresent("A1PRDX"));
    WPOSX.setVisible(lexique.isPresent("WPOSX"));
    WPRAX.setVisible(lexique.isPresent("WPRAX"));
    RAPASX.setEditable(lexique.isPresent("RAPASX"));
    CAKACX.setVisible(lexique.isPresent("CAKACX"));
    CAKSC.setVisible(lexique.isPresent("CAKSC"));
    OBJ_104.setVisible(lexique.isPresent("A1PERK"));
    CAPRAX.setVisible(lexique.isPresent("CAPRAX"));
    UPNETX.setVisible(lexique.isPresent("UPNETX"));
    UPNDVX.setVisible(lexique.isPresent("UPNDVX"));
    OBJ_181.setVisible(lexique.isPresent("FRSGA"));
    INDAR0.setVisible(lexique.isPresent("INDAR0"));
    A1ASB.setVisible(lexique.isPresent("A1ASB"));
    OBJ_54.setVisible(lexique.isPresent("ULBFRS"));
    OBJ_183.setVisible(lexique.isPresent("FRSGA"));
    A1LIB.setVisible(lexique.isPresent("A1LIB"));
    WATTN2.setVisible(lexique.isPresent("WATTN2"));
    // A1IN16.setSelectedIndex(getIndice("A1IN16", A1IN16_Value));
    A1IN16.setEnabled(!isConsult);
    // A1REA.setSelectedIndex(getIndice("A1REA", A1REA_Value));
    // A1REA.setVisible( lexique.isPresent("A1REA"));
    A1REA.setEnabled(!isConsult);
    LRQTEX.setVisible(lexique.isTrue("27"));
    OBJ_77.setVisible(LRQTEX.isVisible());
    OBJ_151.setVisible(WATTN2.isVisible());
    panel2.setVisible(lexique.isTrue("21"));
    OBJ_71.setVisible(A1IN4.isVisible());
    
    // CAREF.setVisible( lexique.isPresent("CAREF"));
    OBJ_130.setVisible(CAREF.isVisible());
    OBJ_132.setVisible(lexique.isPresent("CALIB") && CAREF.isVisible());
    CADEV.setVisible(lexique.isPresent("CADEV"));
    OBJ_135.setVisible(CADEV.isVisible());
    WCHGX.setVisible(lexique.isPresent("WCHGX"));
    OBJ_137.setVisible(WCHGX.isVisible());
    // CAREX6.setVisible(lexique.isPresent("CAREX6"));
    // CAREX5.setVisible(lexique.isPresent("CAREX5"));
    // CAREX4.setVisible(lexique.isPresent("CAREX4"));
    // CAREX3.setVisible(lexique.isPresent("CAREX3"));
    // CAREX2.setVisible(lexique.isPresent("CAREX2"));
    // CAREX1.setVisible( lexique.isPresent("CAREX1"));
    OBJ_142.setVisible(CAREX1.isVisible());
    OBJ_175.setEnabled(lexique.isPresent("CADELS"));
    OBJ_177.setEnabled(OBJ_175.isEnabled());
    OBJ_178.setEnabled(lexique.isPresent("WECAM1"));
    OBJ_179.setEnabled(OBJ_178.isEnabled());
    
    // TODO Icones
    OBJ_80.setIcon(lexique.chargerImage("images/rouge2.gif", true));
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
    
    // Charger la photographie de l'article
    snPhotoArticle.chargerImageArticle(
        lexique.HostFieldGetData("RACINE").trim() + lexique.HostFieldGetData("CHEM1").trim() + lexique.HostFieldGetData("CHEM2").trim(),
        INDAR0.getText(), lexique.isTrue("N51"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // lexique.HostFieldPutData("A1IN16", 0, A1IN16_Value[A1IN16.getSelectedIndex()]);
    // lexique.HostFieldPutData("A1REA", 0, A1REA_Value[A1REA.getSelectedIndex()]);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(1, 1);
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("TOPSTK");
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19", false);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2", false);
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("TOPCNA");
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", false);
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21", false);
  }
  
  private void riSousMenu_bt13ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23", false);
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", false);
  }
  
  private void riSousMenu_bt16ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_80ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(7, 12);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche2 = new JPanel();
    INDETB = new RiZoneSortie();
    OBJ_38 = new JLabel();
    INDMAG = new RiZoneSortie();
    OBJ_39 = new JLabel();
    OBJ_46 = new JLabel();
    A1FAM = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riSousMenu13 = new RiSousMenu();
    riSousMenu_bt13 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    riSousMenu16 = new RiSousMenu();
    riSousMenu_bt16 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    tabbedPane2 = new JTabbedPane();
    OBJ_58 = new JPanel();
    xtp1 = new JXTitledPanel();
    OBJ_83 = new JLabel();
    OBJ_89 = new JLabel();
    OBJ_77 = new JLabel();
    OBJ_79 = new JLabel();
    OBJ_86 = new JLabel();
    LRMINX = new RiZoneSortie();
    LRMAXX = new RiZoneSortie();
    LRQTEX = new RiZoneSortie();
    LRQTSX = new XRiTextField();
    WQTA = new RiZoneSortie();
    OBJ_75 = new JLabel();
    WDEV1 = new RiZoneSortie();
    WUNS = new RiZoneSortie();
    A1UNS = new RiZoneSortie();
    A1UNA = new RiZoneSortie();
    OBJ_80 = new SNBoutonDetail();
    OBJ_108 = new JLabel();
    A1IN20 = new RiZoneSortie();
    OBJ_45 = new JLabel();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_94 = new JLabel();
    WPRAX = new RiZoneSortie();
    OBJ_92 = new JLabel();
    RAPASX = new XRiTextField();
    xTitledPanel3 = new JXTitledPanel();
    A1REA = new XRiComboBox();
    A1IN16 = new XRiComboBox();
    OBJ_97 = new JLabel();
    OBJ_109 = new JLabel();
    OBJ_113 = new JLabel();
    OBJ_104 = new JLabel();
    WPOSX = new RiZoneSortie();
    OBJ_106 = new JLabel();
    A1PRDX = new RiZoneSortie();
    A1PRFX = new RiZoneSortie();
    A1PERK = new RiZoneSortie();
    OBJ_111 = new JLabel();
    LRDTRX = new XRiCalendrier();
    xTitledSeparator1 = new JXTitledSeparator();
    label1 = new JLabel();
    panel2 = new JPanel();
    OBJ_116 = new JLabel();
    LRPRRX = new RiZoneSortie();
    LRPRDX = new RiZoneSortie();
    LRMRRX = new RiZoneSortie();
    LRMRDX = new RiZoneSortie();
    OBJ_118 = new JLabel();
    OBJ_121 = new JLabel();
    separator1 = compFactory.createSeparator("premi\u00e8re");
    separator2 = compFactory.createSeparator("maximale");
    OBJ_123 = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    WATTN2 = new RiZoneSortie();
    OBJ_183 = new RiZoneSortie();
    OBJ_132 = new RiZoneSortie();
    CAREF = new XRiTextField();
    OBJ_181 = new JLabel();
    UPNDVX = new RiZoneSortie();
    UPNETX = new RiZoneSortie();
    OBJ_154 = new JLabel();
    OBJ_153 = new JLabel();
    OBJ_133 = new JLabel();
    CAPRAX = new RiZoneSortie();
    OBJ_158 = new JLabel();
    CAKSC = new RiZoneSortie();
    CAKACX = new RiZoneSortie();
    OBJ_130 = new JLabel();
    OBJ_142 = new JLabel();
    OBJ_157 = new JLabel();
    WCHGX = new RiZoneSortie();
    OBJ_149 = new JLabel();
    OBJ_151 = new JLabel();
    CAQMI = new RiZoneSortie();
    CAQEC = new RiZoneSortie();
    CANUA = new RiZoneSortie();
    OBJ_182 = new RiZoneSortie();
    OBJ_135 = new JLabel();
    OBJ_139 = new JLabel();
    OBJ_155 = new JLabel();
    CAREX1 = new RiZoneSortie();
    CAREX2 = new RiZoneSortie();
    CAREX3 = new RiZoneSortie();
    CAREX4 = new RiZoneSortie();
    CAREX5 = new RiZoneSortie();
    CAREX6 = new RiZoneSortie();
    OBJ_156 = new JLabel();
    OBJ_161 = new JLabel();
    CADEV = new RiZoneSortie();
    OBJ_137 = new JLabel();
    CAUNC = new RiZoneSortie();
    CAUNA = new RiZoneSortie();
    OBJ_159 = new JLabel();
    OBJ_160 = new JLabel();
    CADAPX = new XRiCalendrier();
    l_CADAPX = new JLabel();
    xTitledPanel4 = new JXTitledPanel();
    OBJ_178 = new JLabel();
    OBJ_168 = new JLabel();
    OBJ_175 = new JLabel();
    OBJ_170 = new JLabel();
    OBJ_179 = new JLabel();
    OBJ_180 = new RiZoneSortie();
    CAIN2 = new XRiSpinner();
    CADEL = new RiZoneSortie();
    CADELS = new RiZoneSortie();
    OBJ_172 = new RiZoneSortie();
    OBJ_177 = new JLabel();
    OBJ_184 = new JPanel();
    snPhotoArticle = new SNPhotoArticle();
    panel5 = new JPanel();
    OBJ_44 = new JLabel();
    FRFRSG = new RiZoneSortie();
    OBJ_54 = new RiZoneSortie();
    OBJ_40 = new JLabel();
    INDAR0 = new RiZoneSortie();
    A1LIB = new RiZoneSortie();
    A1IN4 = new RiZoneSortie();
    A1ASB = new RiZoneSortie();
    OBJ_71 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_23 = new JMenuItem();
    OBJ_22 = new JMenuItem();
    RACMJX = new XRiTextField();
    OBJ_100 = new RiZoneSortie();
    OBJ_101 = new JLabel();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("R\u00e9approvisionnement");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche2 ========
        {
          p_tete_gauche2.setOpaque(false);
          p_tete_gauche2.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche2.setPreferredSize(new Dimension(980, 0));
          p_tete_gauche2.setMinimumSize(new Dimension(980, 40));
          p_tete_gauche2.setName("p_tete_gauche2");
          p_tete_gauche2.setLayout(null);
          
          // ---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");
          p_tete_gauche2.add(INDETB);
          INDETB.setBounds(100, 0, 40, INDETB.getPreferredSize().height);
          
          // ---- OBJ_38 ----
          OBJ_38.setText("Etablissement");
          OBJ_38.setName("OBJ_38");
          p_tete_gauche2.add(OBJ_38);
          OBJ_38.setBounds(5, 2, 95, 21);
          
          // ---- INDMAG ----
          INDMAG.setComponentPopupMenu(null);
          INDMAG.setOpaque(false);
          INDMAG.setText("@INDMAG@");
          INDMAG.setName("INDMAG");
          p_tete_gauche2.add(INDMAG);
          INDMAG.setBounds(225, 0, 34, INDMAG.getPreferredSize().height);
          
          // ---- OBJ_39 ----
          OBJ_39.setText("Magasin");
          OBJ_39.setName("OBJ_39");
          p_tete_gauche2.add(OBJ_39);
          OBJ_39.setBounds(165, 2, 60, 21);
          
          // ---- OBJ_46 ----
          OBJ_46.setText("Famille");
          OBJ_46.setName("OBJ_46");
          p_tete_gauche2.add(OBJ_46);
          OBJ_46.setBounds(285, 2, 55, 21);
          
          // ---- A1FAM ----
          A1FAM.setComponentPopupMenu(null);
          A1FAM.setOpaque(false);
          A1FAM.setText("@A1FAM@");
          A1FAM.setName("A1FAM");
          p_tete_gauche2.add(A1FAM);
          A1FAM.setBounds(340, 0, 40, A1FAM.getPreferredSize().height);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < p_tete_gauche2.getComponentCount(); i++) {
              Rectangle bounds = p_tete_gauche2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_tete_gauche2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_tete_gauche2.setMinimumSize(preferredSize);
            p_tete_gauche2.setPreferredSize(preferredSize);
          }
        }
        barre_tete.add(p_tete_gauche2);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Modification stock mini");
              riSousMenu_bt6.setToolTipText("Modification du  stock minimum");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Affichage stocks");
              riSousMenu_bt7.setToolTipText("Affichage stocks");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
            
            // ======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");
              
              // ---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Liste des CNA");
              riSousMenu_bt8.setToolTipText("Liste des conditions d'achat");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
            
            // ======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");
              
              // ---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Fiche fournisseur");
              riSousMenu_bt9.setToolTipText("Appel fiche fournisseur");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
            
            // ======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");
              
              // ---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Conditions d'achat");
              riSousMenu_bt10.setToolTipText("Conditions d'achat");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);
            
            // ======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");
              
              // ---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Historique de conso");
              riSousMenu_bt11.setToolTipText("Historique de consommation");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);
            
            // ======== riSousMenu12 ========
            {
              riSousMenu12.setName("riSousMenu12");
              
              // ---- riSousMenu_bt12 ----
              riSousMenu_bt12.setText("Attendu/command\u00e9");
              riSousMenu_bt12.setToolTipText("D\u00e9tail attendu / command\u00e9");
              riSousMenu_bt12.setName("riSousMenu_bt12");
              riSousMenu_bt12.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt12ActionPerformed(e);
                }
              });
              riSousMenu12.add(riSousMenu_bt12);
            }
            menus_haut.add(riSousMenu12);
            
            // ======== riSousMenu13 ========
            {
              riSousMenu13.setName("riSousMenu13");
              
              // ---- riSousMenu_bt13 ----
              riSousMenu_bt13.setText("Historique sur 12 mois");
              riSousMenu_bt13.setToolTipText("Historique sur 12 mois");
              riSousMenu_bt13.setName("riSousMenu_bt13");
              riSousMenu_bt13.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt13ActionPerformed(e);
                }
              });
              riSousMenu13.add(riSousMenu_bt13);
            }
            menus_haut.add(riSousMenu13);
            
            // ======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");
              
              // ---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);
            
            // ======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");
              
              // ---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Bloc-notes CNA");
              riSousMenu_bt14.setToolTipText("Bloc-notes condition d'achat");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
            
            // ======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");
              
              // ---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Autres vues");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed(e);
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);
            
            // ======== riSousMenu16 ========
            {
              riSousMenu16.setName("riSousMenu16");
              
              // ---- riSousMenu_bt16 ----
              riSousMenu_bt16.setText("S\u00e9lection");
              riSousMenu_bt16.setName("riSousMenu_bt16");
              riSousMenu_bt16.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt16ActionPerformed(e);
                }
              });
              riSousMenu16.add(riSousMenu_bt16);
            }
            menus_haut.add(riSousMenu16);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(810, 620));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(810, 620));
          p_contenu.setName("p_contenu");
          
          // ======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);
            
            // ======== tabbedPane2 ========
            {
              tabbedPane2.setName("tabbedPane2");
              
              // ======== OBJ_58 ========
              {
                OBJ_58.setOpaque(false);
                OBJ_58.setName("OBJ_58");
                OBJ_58.setLayout(null);
                
                // ======== xtp1 ========
                {
                  xtp1.setTitle("Magasin @S1MAG@ @MALIB@");
                  xtp1.setBorder(new DropShadowBorder());
                  xtp1.setName("xtp1");
                  Container xtp1ContentContainer = xtp1.getContentContainer();
                  xtp1ContentContainer.setLayout(null);
                  
                  // ---- OBJ_83 ----
                  OBJ_83.setText("Stock minimum (US)");
                  OBJ_83.setName("OBJ_83");
                  xtp1ContentContainer.add(OBJ_83);
                  OBJ_83.setBounds(355, 47, 130, 20);
                  
                  // ---- OBJ_89 ----
                  OBJ_89.setText("Stock maximum (US)");
                  OBJ_89.setName("OBJ_89");
                  xtp1ContentContainer.add(OBJ_89);
                  OBJ_89.setBounds(355, 77, 130, 20);
                  
                  // ---- OBJ_77 ----
                  OBJ_77.setText("Quantit\u00e9 initiale");
                  OBJ_77.setName("OBJ_77");
                  xtp1ContentContainer.add(OBJ_77);
                  OBJ_77.setBounds(15, 17, 110, 20);
                  
                  // ---- OBJ_79 ----
                  OBJ_79.setText("Quantit\u00e9 (US)");
                  OBJ_79.setName("OBJ_79");
                  xtp1ContentContainer.add(OBJ_79);
                  OBJ_79.setBounds(15, 47, 110, 20);
                  
                  // ---- OBJ_86 ----
                  OBJ_86.setText("Quantit\u00e9 (UA)");
                  OBJ_86.setName("OBJ_86");
                  xtp1ContentContainer.add(OBJ_86);
                  OBJ_86.setBounds(15, 77, 110, 20);
                  
                  // ---- LRMINX ----
                  LRMINX.setText("@LRMINX@");
                  LRMINX.setHorizontalAlignment(SwingConstants.RIGHT);
                  LRMINX.setName("LRMINX");
                  xtp1ContentContainer.add(LRMINX);
                  LRMINX.setBounds(485, 45, 90, LRMINX.getPreferredSize().height);
                  
                  // ---- LRMAXX ----
                  LRMAXX.setText("@LRMAXX@");
                  LRMAXX.setHorizontalAlignment(SwingConstants.RIGHT);
                  LRMAXX.setName("LRMAXX");
                  xtp1ContentContainer.add(LRMAXX);
                  LRMAXX.setBounds(485, 75, 90, LRMAXX.getPreferredSize().height);
                  
                  // ---- LRQTEX ----
                  LRQTEX.setText("@LRQTEX@");
                  LRQTEX.setHorizontalAlignment(SwingConstants.RIGHT);
                  LRQTEX.setName("LRQTEX");
                  xtp1ContentContainer.add(LRQTEX);
                  LRQTEX.setBounds(150, 15, 90, LRQTEX.getPreferredSize().height);
                  
                  // ---- LRQTSX ----
                  LRQTSX.setComponentPopupMenu(null);
                  LRQTSX.setHorizontalAlignment(SwingConstants.RIGHT);
                  LRQTSX.setName("LRQTSX");
                  xtp1ContentContainer.add(LRQTSX);
                  LRQTSX.setBounds(150, 43, 90, LRQTSX.getPreferredSize().height);
                  
                  // ---- WQTA ----
                  WQTA.setText("@WQTA@");
                  WQTA.setHorizontalAlignment(SwingConstants.RIGHT);
                  WQTA.setName("WQTA");
                  xtp1ContentContainer.add(WQTA);
                  WQTA.setBounds(150, 75, 90, WQTA.getPreferredSize().height);
                  
                  // ---- OBJ_75 ----
                  OBJ_75.setText("Devise");
                  OBJ_75.setName("OBJ_75");
                  xtp1ContentContainer.add(OBJ_75);
                  OBJ_75.setBounds(290, 17, 50, 20);
                  
                  // ---- WDEV1 ----
                  WDEV1.setText("@WDEV1@");
                  WDEV1.setName("WDEV1");
                  xtp1ContentContainer.add(WDEV1);
                  WDEV1.setBounds(345, 15, 40, WDEV1.getPreferredSize().height);
                  
                  // ---- WUNS ----
                  WUNS.setText("@WUNS@");
                  WUNS.setName("WUNS");
                  xtp1ContentContainer.add(WUNS);
                  WUNS.setBounds(245, 45, 34, WUNS.getPreferredSize().height);
                  
                  // ---- A1UNS ----
                  A1UNS.setText("@A1UNS@");
                  A1UNS.setName("A1UNS");
                  xtp1ContentContainer.add(A1UNS);
                  A1UNS.setBounds(580, 45, 34, A1UNS.getPreferredSize().height);
                  
                  // ---- A1UNA ----
                  A1UNA.setText("@A1UNA@");
                  A1UNA.setName("A1UNA");
                  xtp1ContentContainer.add(A1UNA);
                  A1UNA.setBounds(245, 75, 34, A1UNA.getPreferredSize().height);
                  
                  // ---- OBJ_80 ----
                  OBJ_80.setText("");
                  OBJ_80.setToolTipText("Position article \u00e0 venir");
                  OBJ_80.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                  OBJ_80.setName("OBJ_80");
                  OBJ_80.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                      OBJ_80ActionPerformed(e);
                    }
                  });
                  xtp1ContentContainer.add(OBJ_80);
                  OBJ_80.setBounds(285, 47, 17, 20);
                  
                  // ---- OBJ_108 ----
                  OBJ_108.setText("%");
                  OBJ_108.setName("OBJ_108");
                  xtp1ContentContainer.add(OBJ_108);
                  OBJ_108.setBounds(580, 77, 20, 20);
                  
                  // ---- A1IN20 ----
                  A1IN20.setComponentPopupMenu(BTD);
                  A1IN20.setText("@A1IN20@");
                  A1IN20.setName("A1IN20");
                  xtp1ContentContainer.add(A1IN20);
                  A1IN20.setBounds(485, 15, 21, A1IN20.getPreferredSize().height);
                  
                  // ---- OBJ_45 ----
                  OBJ_45.setText("GP");
                  OBJ_45.setName("OBJ_45");
                  xtp1ContentContainer.add(OBJ_45);
                  OBJ_45.setBounds(440, 13, 30, 28);
                  
                  {
                    // compute preferred size
                    Dimension preferredSize = new Dimension();
                    for (int i = 0; i < xtp1ContentContainer.getComponentCount(); i++) {
                      Rectangle bounds = xtp1ContentContainer.getComponent(i).getBounds();
                      preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                      preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                    }
                    Insets insets = xtp1ContentContainer.getInsets();
                    preferredSize.width += insets.right;
                    preferredSize.height += insets.bottom;
                    xtp1ContentContainer.setMinimumSize(preferredSize);
                    xtp1ContentContainer.setPreferredSize(preferredSize);
                  }
                }
                OBJ_58.add(xtp1);
                xtp1.setBounds(25, 10, 705, 140);
                
                // ======== xTitledPanel2 ========
                {
                  xTitledPanel2.setBorder(new DropShadowBorder());
                  xTitledPanel2.setTitle("Prix");
                  xTitledPanel2.setName("xTitledPanel2");
                  Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
                  xTitledPanel2ContentContainer.setLayout(null);
                  
                  // ---- OBJ_94 ----
                  OBJ_94.setText("Prix d'achat(UA)");
                  OBJ_94.setName("OBJ_94");
                  xTitledPanel2ContentContainer.add(OBJ_94);
                  OBJ_94.setBounds(15, 9, 111, 20);
                  
                  // ---- WPRAX ----
                  WPRAX.setText("@WPRAX@");
                  WPRAX.setHorizontalAlignment(SwingConstants.RIGHT);
                  WPRAX.setName("WPRAX");
                  xTitledPanel2ContentContainer.add(WPRAX);
                  WPRAX.setBounds(150, 7, 90, WPRAX.getPreferredSize().height);
                  
                  // ---- OBJ_92 ----
                  OBJ_92.setText("Prix n\u00e9goci\u00e9 (UA)");
                  OBJ_92.setName("OBJ_92");
                  xTitledPanel2ContentContainer.add(OBJ_92);
                  OBJ_92.setBounds(355, 9, 125, 20);
                  
                  // ---- RAPASX ----
                  RAPASX.setComponentPopupMenu(BTD);
                  RAPASX.setHorizontalAlignment(SwingConstants.RIGHT);
                  RAPASX.setName("RAPASX");
                  xTitledPanel2ContentContainer.add(RAPASX);
                  RAPASX.setBounds(485, 5, 90, RAPASX.getPreferredSize().height);
                  
                  {
                    // compute preferred size
                    Dimension preferredSize = new Dimension();
                    for (int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                      Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                      preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                      preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                    }
                    Insets insets = xTitledPanel2ContentContainer.getInsets();
                    preferredSize.width += insets.right;
                    preferredSize.height += insets.bottom;
                    xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
                    xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
                  }
                }
                OBJ_58.add(xTitledPanel2);
                xTitledPanel2.setBounds(25, 155, 705, 70);
                
                // ======== xTitledPanel3 ========
                {
                  xTitledPanel3.setBorder(new DropShadowBorder());
                  xTitledPanel3.setName("xTitledPanel3");
                  Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
                  xTitledPanel3ContentContainer.setLayout(null);
                  
                  // ---- A1REA ----
                  A1REA.setModel(new DefaultComboBoxModel(new String[] { "Rupture sur stock minimum", "Rupture sur stock mini",
                      "Consommation moyenne", "R\u00e9appro. manuel", "Pr\u00e9visions conso", "Conso moyenne plafonn\u00e9e",
                      "Plafond couverture 'global'", "Compl\u00e9ment stock maximum", "Plafond couverture 'Mag'" }));
                  A1REA.setComponentPopupMenu(BTD);
                  A1REA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                  A1REA.setName("A1REA");
                  xTitledPanel3ContentContainer.add(A1REA);
                  A1REA.setBounds(149, 44, 215, A1REA.getPreferredSize().height);
                  
                  // ---- A1IN16 ----
                  A1IN16.setModel(new DefaultComboBoxModel(
                      new String[] { "Principal", "Meilleur prix", "Meilleure qualit\u00e9", "D\u00e9lai le plus court" }));
                  A1IN16.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                  A1IN16.setName("A1IN16");
                  xTitledPanel3ContentContainer.add(A1IN16);
                  A1IN16.setBounds(149, 74, 215, A1IN16.getPreferredSize().height);
                  
                  // ---- OBJ_97 ----
                  OBJ_97.setText("Date livraison pr\u00e9vue");
                  OBJ_97.setName("OBJ_97");
                  xTitledPanel3ContentContainer.add(OBJ_97);
                  OBJ_97.setBounds(15, 17, 130, 20);
                  
                  // ---- OBJ_109 ----
                  OBJ_109.setText("Type de r\u00e9appro.");
                  OBJ_109.setName("OBJ_109");
                  xTitledPanel3ContentContainer.add(OBJ_109);
                  OBJ_109.setBounds(15, 47, 125, 20);
                  
                  // ---- OBJ_113 ----
                  OBJ_113.setText("Recherche fournisseur");
                  OBJ_113.setName("OBJ_113");
                  xTitledPanel3ContentContainer.add(OBJ_113);
                  OBJ_113.setBounds(15, 77, 140, 20);
                  
                  // ---- OBJ_104 ----
                  OBJ_104.setText("Pourcentage sur p\u00e9riode");
                  OBJ_104.setName("OBJ_104");
                  xTitledPanel3ContentContainer.add(OBJ_104);
                  OBJ_104.setBounds(470, 77, 155, 20);
                  
                  // ---- WPOSX ----
                  WPOSX.setText("@WPOSX@");
                  WPOSX.setHorizontalAlignment(SwingConstants.RIGHT);
                  WPOSX.setName("WPOSX");
                  xTitledPanel3ContentContainer.add(WPOSX);
                  WPOSX.setBounds(485, 15, 110, WPOSX.getPreferredSize().height);
                  
                  // ---- OBJ_106 ----
                  OBJ_106.setText("Saison du");
                  OBJ_106.setName("OBJ_106");
                  xTitledPanel3ContentContainer.add(OBJ_106);
                  OBJ_106.setBounds(410, 47, 75, 20);
                  
                  // ---- A1PRDX ----
                  A1PRDX.setText("@A1PRDX@");
                  A1PRDX.setName("A1PRDX");
                  xTitledPanel3ContentContainer.add(A1PRDX);
                  A1PRDX.setBounds(485, 45, 60, A1PRDX.getPreferredSize().height);
                  
                  // ---- A1PRFX ----
                  A1PRFX.setText("@A1PRFX@");
                  A1PRFX.setName("A1PRFX");
                  xTitledPanel3ContentContainer.add(A1PRFX);
                  A1PRFX.setBounds(604, 45, 60, A1PRFX.getPreferredSize().height);
                  
                  // ---- A1PERK ----
                  A1PERK.setText("@A1PERK@");
                  A1PERK.setName("A1PERK");
                  xTitledPanel3ContentContainer.add(A1PERK);
                  A1PERK.setBounds(628, 75, 36, A1PERK.getPreferredSize().height);
                  
                  // ---- OBJ_111 ----
                  OBJ_111.setText("au");
                  OBJ_111.setHorizontalAlignment(SwingConstants.CENTER);
                  OBJ_111.setName("OBJ_111");
                  xTitledPanel3ContentContainer.add(OBJ_111);
                  OBJ_111.setBounds(555, 47, 35, 20);
                  
                  // ---- LRDTRX ----
                  LRDTRX.setName("LRDTRX");
                  xTitledPanel3ContentContainer.add(LRDTRX);
                  LRDTRX.setBounds(149, 13, 105, LRDTRX.getPreferredSize().height);
                  
                  // ---- xTitledSeparator1 ----
                  xTitledSeparator1.setTitle("");
                  xTitledSeparator1.setName("xTitledSeparator1");
                  xTitledPanel3ContentContainer.add(xTitledSeparator1);
                  xTitledSeparator1.setBounds(10, 110, 660, xTitledSeparator1.getPreferredSize().height);
                  
                  // ---- label1 ----
                  label1.setText("Position en stock");
                  label1.setName("label1");
                  xTitledPanel3ContentContainer.add(label1);
                  label1.setBounds(355, 17, 115, 20);
                  
                  // ======== panel2 ========
                  {
                    panel2.setOpaque(false);
                    panel2.setName("panel2");
                    panel2.setLayout(null);
                    
                    // ---- OBJ_116 ----
                    OBJ_116.setText("Ruptures");
                    OBJ_116.setName("OBJ_116");
                    panel2.add(OBJ_116);
                    OBJ_116.setBounds(15, 35, 120, 20);
                    
                    // ---- LRPRRX ----
                    LRPRRX.setText("@LRPRRX@");
                    LRPRRX.setHorizontalAlignment(SwingConstants.RIGHT);
                    LRPRRX.setName("LRPRRX");
                    panel2.add(LRPRRX);
                    LRPRRX.setBounds(150, 33, 76, LRPRRX.getPreferredSize().height);
                    
                    // ---- LRPRDX ----
                    LRPRDX.setText("@LRPRDX@");
                    LRPRDX.setHorizontalAlignment(SwingConstants.CENTER);
                    LRPRDX.setName("LRPRDX");
                    panel2.add(LRPRDX);
                    LRPRDX.setBounds(260, 33, 70, LRPRDX.getPreferredSize().height);
                    
                    // ---- LRMRRX ----
                    LRMRRX.setText("@LRMRRX@");
                    LRMRRX.setHorizontalAlignment(SwingConstants.RIGHT);
                    LRMRRX.setName("LRMRRX");
                    panel2.add(LRMRRX);
                    LRMRRX.setBounds(370, 33, 76, LRMRRX.getPreferredSize().height);
                    
                    // ---- LRMRDX ----
                    LRMRDX.setText("@LRMRDX@");
                    LRMRDX.setHorizontalAlignment(SwingConstants.CENTER);
                    LRMRDX.setName("LRMRDX");
                    panel2.add(LRMRDX);
                    LRMRDX.setBounds(485, 33, 70, LRMRDX.getPreferredSize().height);
                    
                    // ---- OBJ_118 ----
                    OBJ_118.setText("le");
                    OBJ_118.setHorizontalAlignment(SwingConstants.CENTER);
                    OBJ_118.setName("OBJ_118");
                    panel2.add(OBJ_118);
                    OBJ_118.setBounds(230, 35, 30, 20);
                    
                    // ---- OBJ_121 ----
                    OBJ_121.setText("le");
                    OBJ_121.setHorizontalAlignment(SwingConstants.CENTER);
                    OBJ_121.setName("OBJ_121");
                    panel2.add(OBJ_121);
                    OBJ_121.setBounds(455, 35, 30, 20);
                    
                    // ---- separator1 ----
                    separator1.setName("separator1");
                    panel2.add(separator1);
                    separator1.setBounds(150, 10, 190, 20);
                    
                    // ---- separator2 ----
                    separator2.setName("separator2");
                    panel2.add(separator2);
                    separator2.setBounds(370, 10, 195, 20);
                    
                    {
                      // compute preferred size
                      Dimension preferredSize = new Dimension();
                      for (int i = 0; i < panel2.getComponentCount(); i++) {
                        Rectangle bounds = panel2.getComponent(i).getBounds();
                        preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                        preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                      }
                      Insets insets = panel2.getInsets();
                      preferredSize.width += insets.right;
                      preferredSize.height += insets.bottom;
                      panel2.setMinimumSize(preferredSize);
                      panel2.setPreferredSize(preferredSize);
                    }
                  }
                  xTitledPanel3ContentContainer.add(panel2);
                  panel2.setBounds(0, 115, 600, 65);
                  
                  {
                    // compute preferred size
                    Dimension preferredSize = new Dimension();
                    for (int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
                      Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
                      preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                      preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                    }
                    Insets insets = xTitledPanel3ContentContainer.getInsets();
                    preferredSize.width += insets.right;
                    preferredSize.height += insets.bottom;
                    xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
                    xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
                  }
                }
                OBJ_58.add(xTitledPanel3);
                xTitledPanel3.setBounds(25, 230, 705, 215);
                
                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for (int i = 0; i < OBJ_58.getComponentCount(); i++) {
                    Rectangle bounds = OBJ_58.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = OBJ_58.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  OBJ_58.setMinimumSize(preferredSize);
                  OBJ_58.setPreferredSize(preferredSize);
                }
              }
              tabbedPane2.addTab("El\u00e9ments de r\u00e9approvisionnement", OBJ_58);
              
              // ======== OBJ_123 ========
              {
                OBJ_123.setOpaque(false);
                OBJ_123.setName("OBJ_123");
                OBJ_123.setLayout(null);
                
                // ======== xTitledPanel1 ========
                {
                  xTitledPanel1.setTitle("@RAFRS@");
                  xTitledPanel1.setBorder(new DropShadowBorder());
                  xTitledPanel1.setName("xTitledPanel1");
                  Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
                  xTitledPanel1ContentContainer.setLayout(null);
                  
                  // ---- WATTN2 ----
                  WATTN2.setText("@WATTN2@");
                  WATTN2.setComponentPopupMenu(null);
                  WATTN2.setName("WATTN2");
                  xTitledPanel1ContentContainer.add(WATTN2);
                  WATTN2.setBounds(120, 137, 530, WATTN2.getPreferredSize().height);
                  
                  // ---- OBJ_183 ----
                  OBJ_183.setText("@FRNOMA@");
                  OBJ_183.setComponentPopupMenu(null);
                  OBJ_183.setName("OBJ_183");
                  xTitledPanel1ContentContainer.add(OBJ_183);
                  OBJ_183.setBounds(233, 167, 266, OBJ_183.getPreferredSize().height);
                  
                  // ---- OBJ_132 ----
                  OBJ_132.setText("@CALIB@");
                  OBJ_132.setComponentPopupMenu(null);
                  OBJ_132.setName("OBJ_132");
                  xTitledPanel1ContentContainer.add(OBJ_132);
                  OBJ_132.setBounds(335, 17, 315, OBJ_132.getPreferredSize().height);
                  
                  // ---- CAREF ----
                  CAREF.setComponentPopupMenu(null);
                  CAREF.setName("CAREF");
                  xTitledPanel1ContentContainer.add(CAREF);
                  CAREF.setBounds(120, 15, 210, CAREF.getPreferredSize().height);
                  
                  // ---- OBJ_181 ----
                  OBJ_181.setText("Fournisseur principal");
                  OBJ_181.setComponentPopupMenu(null);
                  OBJ_181.setName("OBJ_181");
                  xTitledPanel1ContentContainer.add(OBJ_181);
                  OBJ_181.setBounds(15, 170, 127, 20);
                  
                  // ---- UPNDVX ----
                  UPNDVX.setComponentPopupMenu(null);
                  UPNDVX.setText("@UPNDVX@");
                  UPNDVX.setHorizontalAlignment(SwingConstants.RIGHT);
                  UPNDVX.setName("UPNDVX");
                  xTitledPanel1ContentContainer.add(UPNDVX);
                  UPNDVX.setBounds(385, 107, 120, UPNDVX.getPreferredSize().height);
                  
                  // ---- UPNETX ----
                  UPNETX.setComponentPopupMenu(null);
                  UPNETX.setText("@UPNETX@");
                  UPNETX.setHorizontalAlignment(SwingConstants.RIGHT);
                  UPNETX.setName("UPNETX");
                  xTitledPanel1ContentContainer.add(UPNETX);
                  UPNETX.setBounds(120, 107, 120, UPNETX.getPreferredSize().height);
                  
                  // ---- OBJ_154 ----
                  OBJ_154.setText("UA");
                  OBJ_154.setComponentPopupMenu(null);
                  OBJ_154.setName("OBJ_154");
                  xTitledPanel1ContentContainer.add(OBJ_154);
                  OBJ_154.setBounds(310, 200, 35, 20);
                  
                  // ---- OBJ_153 ----
                  OBJ_153.setText("UC");
                  OBJ_153.setComponentPopupMenu(null);
                  OBJ_153.setName("OBJ_153");
                  xTitledPanel1ContentContainer.add(OBJ_153);
                  OBJ_153.setBounds(120, 200, 30, 20);
                  
                  // ---- OBJ_133 ----
                  OBJ_133.setText("Prix catalogue");
                  OBJ_133.setComponentPopupMenu(null);
                  OBJ_133.setName("OBJ_133");
                  xTitledPanel1ContentContainer.add(OBJ_133);
                  OBJ_133.setBounds(15, 49, 100, 20);
                  
                  // ---- CAPRAX ----
                  CAPRAX.setComponentPopupMenu(null);
                  CAPRAX.setText("@CAPRAX@");
                  CAPRAX.setHorizontalAlignment(SwingConstants.RIGHT);
                  CAPRAX.setName("CAPRAX");
                  xTitledPanel1ContentContainer.add(CAPRAX);
                  CAPRAX.setBounds(120, 47, 87, CAPRAX.getPreferredSize().height);
                  
                  // ---- OBJ_158 ----
                  OBJ_158.setText("Commande");
                  OBJ_158.setComponentPopupMenu(null);
                  OBJ_158.setName("OBJ_158");
                  xTitledPanel1ContentContainer.add(OBJ_158);
                  OBJ_158.setBounds(15, 227, 95, 20);
                  
                  // ---- CAKSC ----
                  CAKSC.setComponentPopupMenu(null);
                  CAKSC.setText("@CAKSC@");
                  CAKSC.setHorizontalAlignment(SwingConstants.RIGHT);
                  CAKSC.setName("CAKSC");
                  xTitledPanel1ContentContainer.add(CAKSC);
                  CAKSC.setBounds(165, 225, 82, CAKSC.getPreferredSize().height);
                  
                  // ---- CAKACX ----
                  CAKACX.setComponentPopupMenu(null);
                  CAKACX.setText("@CAKACX@");
                  CAKACX.setHorizontalAlignment(SwingConstants.RIGHT);
                  CAKACX.setName("CAKACX");
                  xTitledPanel1ContentContainer.add(CAKACX);
                  CAKACX.setBounds(355, 225, 82, CAKACX.getPreferredSize().height);
                  
                  // ---- OBJ_130 ----
                  OBJ_130.setText("R\u00e9f\u00e9rence");
                  OBJ_130.setComponentPopupMenu(null);
                  OBJ_130.setName("OBJ_130");
                  xTitledPanel1ContentContainer.add(OBJ_130);
                  OBJ_130.setBounds(15, 19, 100, 20);
                  
                  // ---- OBJ_142 ----
                  OBJ_142.setText("Remise(s)");
                  OBJ_142.setComponentPopupMenu(null);
                  OBJ_142.setName("OBJ_142");
                  xTitledPanel1ContentContainer.add(OBJ_142);
                  OBJ_142.setBounds(15, 79, 105, 20);
                  
                  // ---- OBJ_157 ----
                  OBJ_157.setText("Conditionnement");
                  OBJ_157.setComponentPopupMenu(null);
                  OBJ_157.setName("OBJ_157");
                  xTitledPanel1ContentContainer.add(OBJ_157);
                  OBJ_157.setBounds(650, 200, 105, 20);
                  
                  // ---- WCHGX ----
                  WCHGX.setComponentPopupMenu(null);
                  WCHGX.setText("@WCHGX@");
                  WCHGX.setName("WCHGX");
                  xTitledPanel1ContentContainer.add(WCHGX);
                  WCHGX.setBounds(380, 47, 58, WCHGX.getPreferredSize().height);
                  
                  // ---- OBJ_149 ----
                  OBJ_149.setText("Prix net");
                  OBJ_149.setComponentPopupMenu(null);
                  OBJ_149.setName("OBJ_149");
                  xTitledPanel1ContentContainer.add(OBJ_149);
                  OBJ_149.setBounds(15, 109, 90, 20);
                  
                  // ---- OBJ_151 ----
                  OBJ_151.setText("Attention");
                  OBJ_151.setComponentPopupMenu(null);
                  OBJ_151.setName("OBJ_151");
                  xTitledPanel1ContentContainer.add(OBJ_151);
                  OBJ_151.setBounds(15, 139, 100, 20);
                  
                  // ---- CAQMI ----
                  CAQMI.setComponentPopupMenu(null);
                  CAQMI.setText("@CAQMI@");
                  CAQMI.setHorizontalAlignment(SwingConstants.RIGHT);
                  CAQMI.setName("CAQMI");
                  xTitledPanel1ContentContainer.add(CAQMI);
                  CAQMI.setBounds(445, 225, 92, CAQMI.getPreferredSize().height);
                  
                  // ---- CAQEC ----
                  CAQEC.setComponentPopupMenu(null);
                  CAQEC.setText("@CAQEC@");
                  CAQEC.setHorizontalAlignment(SwingConstants.RIGHT);
                  CAQEC.setName("CAQEC");
                  xTitledPanel1ContentContainer.add(CAQEC);
                  CAQEC.setBounds(550, 225, 92, CAQEC.getPreferredSize().height);
                  
                  // ---- CANUA ----
                  CANUA.setComponentPopupMenu(null);
                  CANUA.setText("@CANUA@");
                  CANUA.setName("CANUA");
                  xTitledPanel1ContentContainer.add(CANUA);
                  CANUA.setBounds(655, 225, 76, CANUA.getPreferredSize().height);
                  
                  // ---- OBJ_182 ----
                  OBJ_182.setText("@FRSGA@");
                  OBJ_182.setComponentPopupMenu(null);
                  OBJ_182.setName("OBJ_182");
                  xTitledPanel1ContentContainer.add(OBJ_182);
                  OBJ_182.setBounds(160, 167, 63, OBJ_182.getPreferredSize().height);
                  
                  // ---- OBJ_135 ----
                  OBJ_135.setText("Devise");
                  OBJ_135.setComponentPopupMenu(null);
                  OBJ_135.setName("OBJ_135");
                  xTitledPanel1ContentContainer.add(OBJ_135);
                  OBJ_135.setBounds(245, 49, 50, 20);
                  
                  // ---- OBJ_139 ----
                  OBJ_139.setText("Prix FF");
                  OBJ_139.setComponentPopupMenu(null);
                  OBJ_139.setName("OBJ_139");
                  xTitledPanel1ContentContainer.add(OBJ_139);
                  OBJ_139.setBounds(315, 109, 55, 20);
                  
                  // ---- OBJ_155 ----
                  OBJ_155.setText("Qt\u00e9 minimum");
                  OBJ_155.setComponentPopupMenu(null);
                  OBJ_155.setName("OBJ_155");
                  xTitledPanel1ContentContainer.add(OBJ_155);
                  OBJ_155.setBounds(445, 200, 92, 20);
                  
                  // ---- CAREX1 ----
                  CAREX1.setComponentPopupMenu(null);
                  CAREX1.setText("@CAREX1@");
                  CAREX1.setHorizontalAlignment(SwingConstants.RIGHT);
                  CAREX1.setName("CAREX1");
                  xTitledPanel1ContentContainer.add(CAREX1);
                  CAREX1.setBounds(120, 77, 60, CAREX1.getPreferredSize().height);
                  
                  // ---- CAREX2 ----
                  CAREX2.setComponentPopupMenu(null);
                  CAREX2.setText("@CAREX2@");
                  CAREX2.setHorizontalAlignment(SwingConstants.RIGHT);
                  CAREX2.setName("CAREX2");
                  xTitledPanel1ContentContainer.add(CAREX2);
                  CAREX2.setBounds(185, 77, 60, CAREX2.getPreferredSize().height);
                  
                  // ---- CAREX3 ----
                  CAREX3.setComponentPopupMenu(null);
                  CAREX3.setText("@CAREX3@");
                  CAREX3.setHorizontalAlignment(SwingConstants.RIGHT);
                  CAREX3.setName("CAREX3");
                  xTitledPanel1ContentContainer.add(CAREX3);
                  CAREX3.setBounds(250, 77, 60, CAREX3.getPreferredSize().height);
                  
                  // ---- CAREX4 ----
                  CAREX4.setComponentPopupMenu(null);
                  CAREX4.setText("@CAREX4@");
                  CAREX4.setHorizontalAlignment(SwingConstants.RIGHT);
                  CAREX4.setName("CAREX4");
                  xTitledPanel1ContentContainer.add(CAREX4);
                  CAREX4.setBounds(315, 77, 60, CAREX4.getPreferredSize().height);
                  
                  // ---- CAREX5 ----
                  CAREX5.setComponentPopupMenu(null);
                  CAREX5.setText("@CAREX5@");
                  CAREX5.setHorizontalAlignment(SwingConstants.RIGHT);
                  CAREX5.setName("CAREX5");
                  xTitledPanel1ContentContainer.add(CAREX5);
                  CAREX5.setBounds(380, 77, 60, CAREX5.getPreferredSize().height);
                  
                  // ---- CAREX6 ----
                  CAREX6.setComponentPopupMenu(null);
                  CAREX6.setText("@CAREX6@");
                  CAREX6.setHorizontalAlignment(SwingConstants.RIGHT);
                  CAREX6.setName("CAREX6");
                  xTitledPanel1ContentContainer.add(CAREX6);
                  CAREX6.setBounds(445, 77, 60, CAREX6.getPreferredSize().height);
                  
                  // ---- OBJ_156 ----
                  OBJ_156.setText("Qt\u00e9 \u00e9conomique");
                  OBJ_156.setComponentPopupMenu(null);
                  OBJ_156.setName("OBJ_156");
                  xTitledPanel1ContentContainer.add(OBJ_156);
                  OBJ_156.setBounds(546, 200, 100, 20);
                  
                  // ---- OBJ_161 ----
                  OBJ_161.setText("Achat");
                  OBJ_161.setComponentPopupMenu(null);
                  OBJ_161.setName("OBJ_161");
                  xTitledPanel1ContentContainer.add(OBJ_161);
                  OBJ_161.setBounds(260, 227, 50, 20);
                  
                  // ---- CADEV ----
                  CADEV.setText("@CADEV@");
                  CADEV.setComponentPopupMenu(null);
                  CADEV.setName("CADEV");
                  xTitledPanel1ContentContainer.add(CADEV);
                  CADEV.setBounds(296, 47, 34, CADEV.getPreferredSize().height);
                  
                  // ---- OBJ_137 ----
                  OBJ_137.setText("Taux");
                  OBJ_137.setComponentPopupMenu(null);
                  OBJ_137.setName("OBJ_137");
                  xTitledPanel1ContentContainer.add(OBJ_137);
                  OBJ_137.setBounds(340, 49, 40, 20);
                  
                  // ---- CAUNC ----
                  CAUNC.setComponentPopupMenu(null);
                  CAUNC.setText("@CAUNC@");
                  CAUNC.setName("CAUNC");
                  xTitledPanel1ContentContainer.add(CAUNC);
                  CAUNC.setBounds(120, 225, 34, CAUNC.getPreferredSize().height);
                  
                  // ---- CAUNA ----
                  CAUNA.setComponentPopupMenu(null);
                  CAUNA.setText("@CAUNA@");
                  CAUNA.setName("CAUNA");
                  xTitledPanel1ContentContainer.add(CAUNA);
                  CAUNA.setBounds(310, 225, 34, CAUNA.getPreferredSize().height);
                  
                  // ---- OBJ_159 ----
                  OBJ_159.setText("NbUS/1UC");
                  OBJ_159.setComponentPopupMenu(null);
                  OBJ_159.setName("OBJ_159");
                  xTitledPanel1ContentContainer.add(OBJ_159);
                  OBJ_159.setBounds(165, 200, 95, 20);
                  
                  // ---- OBJ_160 ----
                  OBJ_160.setText("NbUA/1UC");
                  OBJ_160.setComponentPopupMenu(null);
                  OBJ_160.setName("OBJ_160");
                  xTitledPanel1ContentContainer.add(OBJ_160);
                  OBJ_160.setBounds(355, 200, 80, 20);
                  
                  // ---- CADAPX ----
                  CADAPX.setComponentPopupMenu(null);
                  CADAPX.setName("CADAPX");
                  xTitledPanel1ContentContainer.add(CADAPX);
                  CADAPX.setBounds(550, 45, 105, CADAPX.getPreferredSize().height);
                  
                  // ---- l_CADAPX ----
                  l_CADAPX.setText("Date application");
                  l_CADAPX.setComponentPopupMenu(null);
                  l_CADAPX.setName("l_CADAPX");
                  xTitledPanel1ContentContainer.add(l_CADAPX);
                  l_CADAPX.setBounds(450, 49, 100, 20);
                  
                  {
                    // compute preferred size
                    Dimension preferredSize = new Dimension();
                    for (int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                      Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                      preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                      preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                    }
                    Insets insets = xTitledPanel1ContentContainer.getInsets();
                    preferredSize.width += insets.right;
                    preferredSize.height += insets.bottom;
                    xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
                    xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
                  }
                }
                OBJ_123.add(xTitledPanel1);
                xTitledPanel1.setBounds(5, 10, 760, 295);
                
                // ======== xTitledPanel4 ========
                {
                  xTitledPanel4.setBorder(new DropShadowBorder());
                  xTitledPanel4.setName("xTitledPanel4");
                  Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
                  xTitledPanel4ContentContainer.setLayout(null);
                  
                  // ---- OBJ_178 ----
                  OBJ_178.setText("D\u00e9lai constat\u00e9");
                  OBJ_178.setName("OBJ_178");
                  xTitledPanel4ContentContainer.add(OBJ_178);
                  OBJ_178.setBounds(195, 74, 95, 20);
                  
                  // ---- OBJ_168 ----
                  OBJ_168.setText("Qualit\u00e9");
                  OBJ_168.setName("OBJ_168");
                  xTitledPanel4ContentContainer.add(OBJ_168);
                  OBJ_168.setBounds(20, 14, 80, 20);
                  
                  // ---- OBJ_175 ----
                  OBJ_175.setText("D\u00e9lai 2");
                  OBJ_175.setName("OBJ_175");
                  xTitledPanel4ContentContainer.add(OBJ_175);
                  OBJ_175.setBounds(20, 74, 80, 20);
                  
                  // ---- OBJ_170 ----
                  OBJ_170.setText("D\u00e9lai");
                  OBJ_170.setName("OBJ_170");
                  xTitledPanel4ContentContainer.add(OBJ_170);
                  OBJ_170.setBounds(20, 44, 80, 20);
                  
                  // ---- OBJ_179 ----
                  OBJ_179.setText("jours");
                  OBJ_179.setName("OBJ_179");
                  xTitledPanel4ContentContainer.add(OBJ_179);
                  OBJ_179.setBounds(335, 74, 50, 20);
                  
                  // ---- OBJ_180 ----
                  OBJ_180.setText("@WECAM1@");
                  OBJ_180.setComponentPopupMenu(null);
                  OBJ_180.setName("OBJ_180");
                  xTitledPanel4ContentContainer.add(OBJ_180);
                  OBJ_180.setBounds(290, 72, 36, OBJ_180.getPreferredSize().height);
                  
                  // ---- CAIN2 ----
                  CAIN2.setComponentPopupMenu(null);
                  CAIN2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                  CAIN2.setModel(new SpinnerListModel(new String[] { " ", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" }));
                  CAIN2.setName("CAIN2");
                  xTitledPanel4ContentContainer.add(CAIN2);
                  CAIN2.setBounds(120, 10, 80, CAIN2.getPreferredSize().height);
                  
                  // ---- CADEL ----
                  CADEL.setComponentPopupMenu(null);
                  CADEL.setText("@CADEL@");
                  CADEL.setName("CADEL");
                  xTitledPanel4ContentContainer.add(CADEL);
                  CADEL.setBounds(120, 42, 28, CADEL.getPreferredSize().height);
                  
                  // ---- CADELS ----
                  CADELS.setComponentPopupMenu(null);
                  CADELS.setText("@CADELS@");
                  CADELS.setName("CADELS");
                  xTitledPanel4ContentContainer.add(CADELS);
                  CADELS.setBounds(120, 72, 28, CADELS.getPreferredSize().height);
                  
                  // ---- OBJ_172 ----
                  OBJ_172.setText("@SEM@");
                  OBJ_172.setComponentPopupMenu(null);
                  OBJ_172.setName("OBJ_172");
                  xTitledPanel4ContentContainer.add(OBJ_172);
                  OBJ_172.setBounds(160, 42, 40, OBJ_172.getPreferredSize().height);
                  
                  // ---- OBJ_177 ----
                  OBJ_177.setText("s");
                  OBJ_177.setName("OBJ_177");
                  xTitledPanel4ContentContainer.add(OBJ_177);
                  OBJ_177.setBounds(160, 74, 20, 20);
                  
                  {
                    // compute preferred size
                    Dimension preferredSize = new Dimension();
                    for (int i = 0; i < xTitledPanel4ContentContainer.getComponentCount(); i++) {
                      Rectangle bounds = xTitledPanel4ContentContainer.getComponent(i).getBounds();
                      preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                      preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                    }
                    Insets insets = xTitledPanel4ContentContainer.getInsets();
                    preferredSize.width += insets.right;
                    preferredSize.height += insets.bottom;
                    xTitledPanel4ContentContainer.setMinimumSize(preferredSize);
                    xTitledPanel4ContentContainer.setPreferredSize(preferredSize);
                  }
                }
                OBJ_123.add(xTitledPanel4);
                xTitledPanel4.setBounds(5, 310, 760, 140);
                
                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for (int i = 0; i < OBJ_123.getComponentCount(); i++) {
                    Rectangle bounds = OBJ_123.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = OBJ_123.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  OBJ_123.setMinimumSize(preferredSize);
                  OBJ_123.setPreferredSize(preferredSize);
                }
              }
              tabbedPane2.addTab("Condition du fournisseur", OBJ_123);
              
              // ======== OBJ_184 ========
              {
                OBJ_184.setOpaque(false);
                OBJ_184.setName("OBJ_184");
                OBJ_184.setLayout(null);
                
                // ---- snPhotoArticle ----
                snPhotoArticle.setOpaque(false);
                snPhotoArticle.setName("snPhotoArticle");
                OBJ_184.add(snPhotoArticle);
                snPhotoArticle.setBounds(180, 10, 425, 400);
                
                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for (int i = 0; i < OBJ_184.getComponentCount(); i++) {
                    Rectangle bounds = OBJ_184.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = OBJ_184.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  OBJ_184.setMinimumSize(preferredSize);
                  OBJ_184.setPreferredSize(preferredSize);
                }
              }
              tabbedPane2.addTab("Photo", OBJ_184);
            }
            panel1.add(tabbedPane2);
            tabbedPane2.setBounds(0, 105, 770, 500);
            
            // ======== panel5 ========
            {
              panel5.setOpaque(false);
              panel5.setName("panel5");
              panel5.setLayout(null);
              
              // ---- OBJ_44 ----
              OBJ_44.setText("N\u00b0Fournisseur");
              OBJ_44.setName("OBJ_44");
              panel5.add(OBJ_44);
              OBJ_44.setBounds(20, 2, 105, 20);
              
              // ---- FRFRSG ----
              FRFRSG.setComponentPopupMenu(null);
              FRFRSG.setText("@FRFRSG@");
              FRFRSG.setName("FRFRSG");
              panel5.add(FRFRSG);
              FRFRSG.setBounds(155, 0, 68, FRFRSG.getPreferredSize().height);
              
              // ---- OBJ_54 ----
              OBJ_54.setText("@ULBFRS@");
              OBJ_54.setComponentPopupMenu(null);
              OBJ_54.setName("OBJ_54");
              panel5.add(OBJ_54);
              OBJ_54.setBounds(230, 0, 310, OBJ_54.getPreferredSize().height);
              
              // ---- OBJ_40 ----
              OBJ_40.setText("Article");
              OBJ_40.setName("OBJ_40");
              panel5.add(OBJ_40);
              OBJ_40.setBounds(20, 32, 50, 21);
              
              // ---- INDAR0 ----
              INDAR0.setComponentPopupMenu(null);
              INDAR0.setText("@INDAR0@");
              INDAR0.setName("INDAR0");
              panel5.add(INDAR0);
              INDAR0.setBounds(155, 30, 210, INDAR0.getPreferredSize().height);
              
              // ---- A1LIB ----
              A1LIB.setText("@A1LIB@");
              A1LIB.setName("A1LIB");
              panel5.add(A1LIB);
              A1LIB.setBounds(370, 30, 300, A1LIB.getPreferredSize().height);
              
              // ---- A1IN4 ----
              A1IN4.setText("@A1IN4@");
              A1IN4.setName("A1IN4");
              panel5.add(A1IN4);
              A1IN4.setBounds(155, 60, 24, A1IN4.getPreferredSize().height);
              
              // ---- A1ASB ----
              A1ASB.setText("@A1ASB@");
              A1ASB.setName("A1ASB");
              panel5.add(A1ASB);
              A1ASB.setBounds(185, 60, 210, A1ASB.getPreferredSize().height);
              
              // ---- OBJ_71 ----
              OBJ_71.setText("Substitution");
              OBJ_71.setName("OBJ_71");
              panel5.add(OBJ_71);
              OBJ_71.setBounds(20, 62, 81, 20);
              
              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel5.getComponentCount(); i++) {
                  Rectangle bounds = panel5.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel5.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel5.setMinimumSize(preferredSize);
                panel5.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel5);
            panel5.setBounds(20, 10, 675, 90);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(19, 19, 19).addComponent(panel1, GroupLayout.PREFERRED_SIZE, 775, GroupLayout.PREFERRED_SIZE)));
          p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(9, 9, 9).addComponent(panel1, GroupLayout.PREFERRED_SIZE, 585, GroupLayout.PREFERRED_SIZE)));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_23 ----
      OBJ_23.setText("Choix possibles");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD.add(OBJ_23);
      
      // ---- OBJ_22 ----
      OBJ_22.setText("Aide en ligne");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);
    }
    
    // ---- RACMJX ----
    RACMJX.setName("RACMJX");
    
    // ---- OBJ_100 ----
    OBJ_100.setText("@CONS@");
    OBJ_100.setName("OBJ_100");
    
    // ---- OBJ_101 ----
    OBJ_101.setText("@CONS2@");
    OBJ_101.setName("OBJ_101");
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche2;
  private RiZoneSortie INDETB;
  private JLabel OBJ_38;
  private RiZoneSortie INDMAG;
  private JLabel OBJ_39;
  private JLabel OBJ_46;
  private RiZoneSortie A1FAM;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiSousMenu riSousMenu13;
  private RiSousMenu_bt riSousMenu_bt13;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private RiSousMenu riSousMenu16;
  private RiSousMenu_bt riSousMenu_bt16;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JTabbedPane tabbedPane2;
  private JPanel OBJ_58;
  private JXTitledPanel xtp1;
  private JLabel OBJ_83;
  private JLabel OBJ_89;
  private JLabel OBJ_77;
  private JLabel OBJ_79;
  private JLabel OBJ_86;
  private RiZoneSortie LRMINX;
  private RiZoneSortie LRMAXX;
  private RiZoneSortie LRQTEX;
  private XRiTextField LRQTSX;
  private RiZoneSortie WQTA;
  private JLabel OBJ_75;
  private RiZoneSortie WDEV1;
  private RiZoneSortie WUNS;
  private RiZoneSortie A1UNS;
  private RiZoneSortie A1UNA;
  private SNBoutonDetail OBJ_80;
  private JLabel OBJ_108;
  private RiZoneSortie A1IN20;
  private JLabel OBJ_45;
  private JXTitledPanel xTitledPanel2;
  private JLabel OBJ_94;
  private RiZoneSortie WPRAX;
  private JLabel OBJ_92;
  private XRiTextField RAPASX;
  private JXTitledPanel xTitledPanel3;
  private XRiComboBox A1REA;
  private XRiComboBox A1IN16;
  private JLabel OBJ_97;
  private JLabel OBJ_109;
  private JLabel OBJ_113;
  private JLabel OBJ_104;
  private RiZoneSortie WPOSX;
  private JLabel OBJ_106;
  private RiZoneSortie A1PRDX;
  private RiZoneSortie A1PRFX;
  private RiZoneSortie A1PERK;
  private JLabel OBJ_111;
  private XRiCalendrier LRDTRX;
  private JXTitledSeparator xTitledSeparator1;
  private JLabel label1;
  private JPanel panel2;
  private JLabel OBJ_116;
  private RiZoneSortie LRPRRX;
  private RiZoneSortie LRPRDX;
  private RiZoneSortie LRMRRX;
  private RiZoneSortie LRMRDX;
  private JLabel OBJ_118;
  private JLabel OBJ_121;
  private JComponent separator1;
  private JComponent separator2;
  private JPanel OBJ_123;
  private JXTitledPanel xTitledPanel1;
  private RiZoneSortie WATTN2;
  private RiZoneSortie OBJ_183;
  private RiZoneSortie OBJ_132;
  private XRiTextField CAREF;
  private JLabel OBJ_181;
  private RiZoneSortie UPNDVX;
  private RiZoneSortie UPNETX;
  private JLabel OBJ_154;
  private JLabel OBJ_153;
  private JLabel OBJ_133;
  private RiZoneSortie CAPRAX;
  private JLabel OBJ_158;
  private RiZoneSortie CAKSC;
  private RiZoneSortie CAKACX;
  private JLabel OBJ_130;
  private JLabel OBJ_142;
  private JLabel OBJ_157;
  private RiZoneSortie WCHGX;
  private JLabel OBJ_149;
  private JLabel OBJ_151;
  private RiZoneSortie CAQMI;
  private RiZoneSortie CAQEC;
  private RiZoneSortie CANUA;
  private RiZoneSortie OBJ_182;
  private JLabel OBJ_135;
  private JLabel OBJ_139;
  private JLabel OBJ_155;
  private RiZoneSortie CAREX1;
  private RiZoneSortie CAREX2;
  private RiZoneSortie CAREX3;
  private RiZoneSortie CAREX4;
  private RiZoneSortie CAREX5;
  private RiZoneSortie CAREX6;
  private JLabel OBJ_156;
  private JLabel OBJ_161;
  private RiZoneSortie CADEV;
  private JLabel OBJ_137;
  private RiZoneSortie CAUNC;
  private RiZoneSortie CAUNA;
  private JLabel OBJ_159;
  private JLabel OBJ_160;
  private XRiCalendrier CADAPX;
  private JLabel l_CADAPX;
  private JXTitledPanel xTitledPanel4;
  private JLabel OBJ_178;
  private JLabel OBJ_168;
  private JLabel OBJ_175;
  private JLabel OBJ_170;
  private JLabel OBJ_179;
  private RiZoneSortie OBJ_180;
  private XRiSpinner CAIN2;
  private RiZoneSortie CADEL;
  private RiZoneSortie CADELS;
  private RiZoneSortie OBJ_172;
  private JLabel OBJ_177;
  private JPanel OBJ_184;
  private SNPhotoArticle snPhotoArticle;
  private JPanel panel5;
  private JLabel OBJ_44;
  private RiZoneSortie FRFRSG;
  private RiZoneSortie OBJ_54;
  private JLabel OBJ_40;
  private RiZoneSortie INDAR0;
  private RiZoneSortie A1LIB;
  private RiZoneSortie A1IN4;
  private RiZoneSortie A1ASB;
  private JLabel OBJ_71;
  private JPopupMenu BTD;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_22;
  private XRiTextField RACMJX;
  private RiZoneSortie OBJ_100;
  private JLabel OBJ_101;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
