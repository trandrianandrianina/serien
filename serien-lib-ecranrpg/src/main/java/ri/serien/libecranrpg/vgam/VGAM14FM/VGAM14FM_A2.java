
package ri.serien.libecranrpg.vgam.VGAM14FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGAM14FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "TITLD", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 559, };
  
  public VGAM14FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDACH.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDACH@")).trim());
    INDMAG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDMAG@")).trim());
    INDDAX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDDAX@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_68.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRNOM@")).trim());
    OBJ_73.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRMIN@")).trim());
    OBJ_74.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRFCO@")).trim());
    OBJ_75.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRPER@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST, LIST.get_LIST_Title_Data_Brut(), _WTP01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    riSousMenu7.setEnabled(lexique.isTrue("87"));
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    label1.setVisible(lexique.isTrue("65"));
    
    V06F.setText("");
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    lexique.HostFieldPutData("V06F", 1, V06F.getText());
  }
  
  public XRiTextField getV06F() {
    return V06F;
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7", false);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11", false);
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F17", false);
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F16", false);
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "1", "Enter");
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "2", "Enter");
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "5", "Enter");
    WTP01.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "6", "Enter");
    WTP01.setValeurTop("6");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "+", "Enter");
    WTP01.setValeurTop("+");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "-", "Enter");
    WTP01.setValeurTop("-");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_27ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "D", "Enter");
    WTP01.setValeurTop("D");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_28ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "M", "Enter");
    WTP01.setValeurTop("M");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_29ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "V", "Enter");
    WTP01.setValeurTop("V");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_30ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "T", "Enter");
    WTP01.setValeurTop("T");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_31ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "O", "Enter");
    WTP01.setValeurTop("O");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _WTP01_Top, "1", "Enter", e);
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void OBJ_34ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD_BIS.getInvoker().getName());
  }
  
  private void OBJ_35ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD_BIS.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche2 = new JPanel();
    INDETB = new RiZoneSortie();
    INDACH = new RiZoneSortie();
    OBJ_41 = new JLabel();
    OBJ_42 = new JLabel();
    OBJ_38 = new JLabel();
    INDMAG = new RiZoneSortie();
    OBJ_39 = new JLabel();
    INDDAX = new RiZoneSortie();
    label1 = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel4 = new JPanel();
    OBJ_57 = new JLabel();
    INDAR0 = new XRiTextField();
    OBJ_68 = new RiZoneSortie();
    OBJ_69 = new JLabel();
    OBJ_70 = new JLabel();
    OBJ_72 = new JLabel();
    OBJ_71 = new JLabel();
    OBJ_73 = new RiZoneSortie();
    OBJ_74 = new RiZoneSortie();
    OBJ_75 = new RiZoneSortie();
    OBJ_58 = new JLabel();
    WFRS = new XRiTextField();
    panel3 = new JPanel();
    OBJ_84 = new JLabel();
    ARG2 = new XRiTextField();
    OBJ_85 = new JLabel();
    ARG3 = new XRiTextField();
    OBJ_86 = new JLabel();
    ARG4 = new XRiTextField();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_20 = new JMenuItem();
    OBJ_21 = new JMenuItem();
    OBJ_22 = new JMenuItem();
    OBJ_23 = new JMenuItem();
    OBJ_24 = new JMenuItem();
    OBJ_27 = new JMenuItem();
    OBJ_28 = new JMenuItem();
    OBJ_29 = new JMenuItem();
    OBJ_30 = new JMenuItem();
    OBJ_31 = new JMenuItem();
    BTD_BIS = new JPopupMenu();
    OBJ_35 = new JMenuItem();
    OBJ_34 = new JMenuItem();
    V06F = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("R\u00e9approvisionnement");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche2 ========
        {
          p_tete_gauche2.setOpaque(false);
          p_tete_gauche2.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche2.setPreferredSize(new Dimension(800, 40));
          p_tete_gauche2.setMinimumSize(new Dimension(800, 40));
          p_tete_gauche2.setName("p_tete_gauche2");
          p_tete_gauche2.setLayout(null);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");
          p_tete_gauche2.add(INDETB);
          INDETB.setBounds(100, 2, 40, INDETB.getPreferredSize().height);

          //---- INDACH ----
          INDACH.setComponentPopupMenu(BTD);
          INDACH.setOpaque(false);
          INDACH.setText("@INDACH@");
          INDACH.setName("INDACH");
          p_tete_gauche2.add(INDACH);
          INDACH.setBounds(485, 0, 40, INDACH.getPreferredSize().height);

          //---- OBJ_41 ----
          OBJ_41.setText("Date");
          OBJ_41.setName("OBJ_41");
          p_tete_gauche2.add(OBJ_41);
          OBJ_41.setBounds(285, 5, 40, 19);

          //---- OBJ_42 ----
          OBJ_42.setText("Acheteur");
          OBJ_42.setName("OBJ_42");
          p_tete_gauche2.add(OBJ_42);
          OBJ_42.setBounds(420, 5, 60, 19);

          //---- OBJ_38 ----
          OBJ_38.setText("Etablissement");
          OBJ_38.setName("OBJ_38");
          p_tete_gauche2.add(OBJ_38);
          OBJ_38.setBounds(5, 5, 100, 19);

          //---- INDMAG ----
          INDMAG.setComponentPopupMenu(BTD);
          INDMAG.setOpaque(false);
          INDMAG.setText("@INDMAG@");
          INDMAG.setName("INDMAG");
          p_tete_gauche2.add(INDMAG);
          INDMAG.setBounds(230, 2, 34, INDMAG.getPreferredSize().height);

          //---- OBJ_39 ----
          OBJ_39.setText("Magasin");
          OBJ_39.setName("OBJ_39");
          p_tete_gauche2.add(OBJ_39);
          OBJ_39.setBounds(170, 5, 60, 19);

          //---- INDDAX ----
          INDDAX.setOpaque(false);
          INDDAX.setText("@INDDAX@");
          INDDAX.setName("INDDAX");
          p_tete_gauche2.add(INDDAX);
          INDDAX.setBounds(325, 0, 68, INDDAX.getPreferredSize().height);

          //---- label1 ----
          label1.setText("Restriction aux commandes clients non affect\u00e9es");
          label1.setForeground(Color.black);
          label1.setName("label1");
          p_tete_gauche2.add(label1);
          label1.setBounds(610, 5, label1.getPreferredSize().width, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_tete_gauche2.getComponentCount(); i++) {
              Rectangle bounds = p_tete_gauche2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_tete_gauche2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_tete_gauche2.setMinimumSize(preferredSize);
            p_tete_gauche2.setPreferredSize(preferredSize);
          }
        }
        barre_tete.add(p_tete_gauche2);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout());
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("G\u00e9n\u00e9ration d'achat");
              riSousMenu_bt6.setToolTipText("G\u00e9n\u00e9ration d'achat");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("G\u00e9n\u00e9ration de transferts");
              riSousMenu_bt7.setToolTipText("G\u00e9n\u00e9ration de transferts");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("S\u00e9lection sur la demande");
              riSousMenu_bt8.setToolTipText("S\u00e9lection compl\u00e8te sur la demande");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Totalisation");
              riSousMenu_bt9.setToolTipText("Totalisation");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Nouveau calcul r\u00e9appro.");
              riSousMenu_bt10.setToolTipText("Nouveau calcul de r\u00e9approvisionnement");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);

            //======== riSousMenu12 ========
            {
              riSousMenu12.setName("riSousMenu12");

              //---- riSousMenu_bt12 ----
              riSousMenu_bt12.setText("Autres vues");
              riSousMenu_bt12.setToolTipText("Autres pr\u00e9sentation des donn\u00e9es dans la liste");
              riSousMenu_bt12.setName("riSousMenu_bt12");
              riSousMenu_bt12.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt12ActionPerformed(e);
                }
              });
              riSousMenu12.add(riSousMenu_bt12);
            }
            menus_haut.add(riSousMenu12);

            //======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");

              //---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Restriction lignes");
              riSousMenu_bt11.setToolTipText("Restriction lignes avec commandes clients non affect\u00e9es");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);

            //======== riSousMenu1 ========
            {
              riSousMenu1.setName("riSousMenu1");

              //---- riSousMenu_bt1 ----
              riSousMenu_bt1.setText("Recherche multi-crit\u00e8res");
              riSousMenu_bt1.setToolTipText("Recherche multi-crit\u00e8res");
              riSousMenu_bt1.setName("riSousMenu_bt1");
              riSousMenu_bt1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt1ActionPerformed(e);
                }
              });
              riSousMenu1.add(riSousMenu_bt1);
            }
            menus_haut.add(riSousMenu1);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(780, 520));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel4 ========
          {
            panel4.setBorder(new TitledBorder(""));
            panel4.setOpaque(false);
            panel4.setPreferredSize(new Dimension(877, 50));
            panel4.setMaximumSize(new Dimension(877, 100));
            panel4.setMinimumSize(new Dimension(877, 50));
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- OBJ_57 ----
            OBJ_57.setText("Article");
            OBJ_57.setName("OBJ_57");
            panel4.add(OBJ_57);
            OBJ_57.setBounds(42, 44, 136, 28);

            //---- INDAR0 ----
            INDAR0.setComponentPopupMenu(BTD_BIS);
            INDAR0.setName("INDAR0");
            panel4.add(INDAR0);
            INDAR0.setBounds(178, 44, 260, INDAR0.getPreferredSize().height);

            //---- OBJ_68 ----
            OBJ_68.setText("@FRNOM@");
            OBJ_68.setName("OBJ_68");
            panel4.add(OBJ_68);
            OBJ_68.setBounds(250, 17, 425, OBJ_68.getPreferredSize().height);

            //---- OBJ_69 ----
            OBJ_69.setText("Minimum commande");
            OBJ_69.setName("OBJ_69");
            panel4.add(OBJ_69);
            OBJ_69.setBounds(42, 72, 136, 28);

            //---- OBJ_70 ----
            OBJ_70.setText("Minimum franco");
            OBJ_70.setName("OBJ_70");
            panel4.add(OBJ_70);
            OBJ_70.setBounds(268, 72, 101, 28);

            //---- OBJ_72 ----
            OBJ_72.setText("semaine(s)");
            OBJ_72.setName("OBJ_72");
            panel4.add(OBJ_72);
            OBJ_72.setBounds(610, 72, 70, 28);

            //---- OBJ_71 ----
            OBJ_71.setText("P\u00e9riodicit\u00e9");
            OBJ_71.setName("OBJ_71");
            panel4.add(OBJ_71);
            OBJ_71.setBounds(500, 72, 67, 28);

            //---- OBJ_73 ----
            OBJ_73.setText("@FRMIN@");
            OBJ_73.setName("OBJ_73");
            panel4.add(OBJ_73);
            OBJ_73.setBounds(178, 74, 60, OBJ_73.getPreferredSize().height);

            //---- OBJ_74 ----
            OBJ_74.setText("@FRFCO@");
            OBJ_74.setName("OBJ_74");
            panel4.add(OBJ_74);
            OBJ_74.setBounds(378, 74, 60, OBJ_74.getPreferredSize().height);

            //---- OBJ_75 ----
            OBJ_75.setText("@FRPER@");
            OBJ_75.setName("OBJ_75");
            panel4.add(OBJ_75);
            OBJ_75.setBounds(575, 74, 30, OBJ_75.getPreferredSize().height);

            //---- OBJ_58 ----
            OBJ_58.setText("Fournisseur");
            OBJ_58.setName("OBJ_58");
            panel4.add(OBJ_58);
            OBJ_58.setBounds(42, 15, 136, 28);

            //---- WFRS ----
            WFRS.setComponentPopupMenu(BTD);
            WFRS.setName("WFRS");
            panel4.add(WFRS);
            WFRS.setBounds(178, 15, 66, WFRS.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder("Suggestions de r\u00e9approvisionnement"));
            panel3.setOpaque(false);
            panel3.setPreferredSize(new Dimension(877, 365));
            panel3.setMinimumSize(new Dimension(877, 365));
            panel3.setMaximumSize(new Dimension(877, 365));
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- OBJ_84 ----
            OBJ_84.setText("Famille");
            OBJ_84.setName("OBJ_84");
            panel3.add(OBJ_84);
            OBJ_84.setBounds(25, 41, 54, 26);

            //---- ARG2 ----
            ARG2.setComponentPopupMenu(null);
            ARG2.setName("ARG2");
            panel3.add(ARG2);
            ARG2.setBounds(85, 40, 40, ARG2.getPreferredSize().height);

            //---- OBJ_85 ----
            OBJ_85.setText("Classement 1");
            OBJ_85.setName("OBJ_85");
            panel3.add(OBJ_85);
            OBJ_85.setBounds(140, 41, 92, 26);

            //---- ARG3 ----
            ARG3.setComponentPopupMenu(null);
            ARG3.setName("ARG3");
            panel3.add(ARG3);
            ARG3.setBounds(235, 40, 160, ARG3.getPreferredSize().height);

            //---- OBJ_86 ----
            OBJ_86.setText("Classement 2");
            OBJ_86.setName("OBJ_86");
            panel3.add(OBJ_86);
            OBJ_86.setBounds(420, 41, 84, 26);

            //---- ARG4 ----
            ARG4.setComponentPopupMenu(null);
            ARG4.setName("ARG4");
            panel3.add(ARG4);
            ARG4.setBounds(515, 40, 160, ARG4.getPreferredSize().height);

            //======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST.setViewportView(WTP01);
            }
            panel3.add(SCROLLPANE_LIST);
            SCROLLPANE_LIST.setBounds(26, 81, 649, 269);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel3.add(BT_PGUP);
            BT_PGUP.setBounds(685, 81, 25, 127);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel3.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(685, 227, 25, 123);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                  .addComponent(panel4, GroupLayout.Alignment.LEADING, 0, 734, Short.MAX_VALUE)
                  .addComponent(panel3, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 734, Short.MAX_VALUE))
                .addGap(22, 22, 22))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 375, GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_20 ----
      OBJ_20.setText("Modifier");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);

      //---- OBJ_21 ----
      OBJ_21.setText("Interroger");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);

      //---- OBJ_22 ----
      OBJ_22.setText("Fiche fournisseur");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);

      //---- OBJ_23 ----
      OBJ_23.setText("S\u00e9lectionner");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD.add(OBJ_23);

      //---- OBJ_24 ----
      OBJ_24.setText("Annulation s\u00e9lection");
      OBJ_24.setName("OBJ_24");
      OBJ_24.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_24ActionPerformed(e);
        }
      });
      BTD.add(OBJ_24);

      //---- OBJ_27 ----
      OBJ_27.setText("D\u00e9tail");
      OBJ_27.setName("OBJ_27");
      OBJ_27.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_27ActionPerformed(e);
        }
      });
      BTD.add(OBJ_27);

      //---- OBJ_28 ----
      OBJ_28.setText("Autres magasins pour transfert");
      OBJ_28.setName("OBJ_28");
      OBJ_28.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_28ActionPerformed(e);
        }
      });
      BTD.add(OBJ_28);

      //---- OBJ_29 ----
      OBJ_29.setText("Saisie vecteur (sp\u00e9cial taille/couleur)");
      OBJ_29.setName("OBJ_29");
      OBJ_29.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_29ActionPerformed(e);
        }
      });
      BTD.add(OBJ_29);

      //---- OBJ_30 ----
      OBJ_30.setText("Saisie tableau (sp\u00e9cial taille/couleur)");
      OBJ_30.setName("OBJ_30");
      OBJ_30.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_30ActionPerformed(e);
        }
      });
      BTD.add(OBJ_30);

      //---- OBJ_31 ----
      OBJ_31.setText("Options article");
      OBJ_31.setName("OBJ_31");
      OBJ_31.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_31ActionPerformed(e);
        }
      });
      BTD.add(OBJ_31);
    }

    //======== BTD_BIS ========
    {
      BTD_BIS.setName("BTD_BIS");

      //---- OBJ_35 ----
      OBJ_35.setText("Choix possibles");
      OBJ_35.setName("OBJ_35");
      OBJ_35.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_35ActionPerformed(e);
        }
      });
      BTD_BIS.add(OBJ_35);

      //---- OBJ_34 ----
      OBJ_34.setText("Aide en ligne");
      OBJ_34.setName("OBJ_34");
      OBJ_34.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_34ActionPerformed(e);
        }
      });
      BTD_BIS.add(OBJ_34);
    }

    //---- V06F ----
    V06F.setName("V06F");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche2;
  private RiZoneSortie INDETB;
  private RiZoneSortie INDACH;
  private JLabel OBJ_41;
  private JLabel OBJ_42;
  private JLabel OBJ_38;
  private RiZoneSortie INDMAG;
  private JLabel OBJ_39;
  private RiZoneSortie INDDAX;
  private JLabel label1;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel4;
  private JLabel OBJ_57;
  private XRiTextField INDAR0;
  private RiZoneSortie OBJ_68;
  private JLabel OBJ_69;
  private JLabel OBJ_70;
  private JLabel OBJ_72;
  private JLabel OBJ_71;
  private RiZoneSortie OBJ_73;
  private RiZoneSortie OBJ_74;
  private RiZoneSortie OBJ_75;
  private JLabel OBJ_58;
  private XRiTextField WFRS;
  private JPanel panel3;
  private JLabel OBJ_84;
  private XRiTextField ARG2;
  private JLabel OBJ_85;
  private XRiTextField ARG3;
  private JLabel OBJ_86;
  private XRiTextField ARG4;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_24;
  private JMenuItem OBJ_27;
  private JMenuItem OBJ_28;
  private JMenuItem OBJ_29;
  private JMenuItem OBJ_30;
  private JMenuItem OBJ_31;
  private JPopupMenu BTD_BIS;
  private JMenuItem OBJ_35;
  private JMenuItem OBJ_34;
  private XRiTextField V06F;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
