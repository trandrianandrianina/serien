//$$david$$ ££11/01/11££ -> tests et modifs

package ri.serien.libecranrpg.vgam.VGAM04FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.gescom.commun.contact.EnumTypeContact;
import ri.serien.libcommun.gescom.commun.contact.IdContact;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.personnalisation.parametresysteme.EnumParametreSysteme;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.contact.listecontacttiers.DialogueListeContactTiers;
import ri.serien.libswing.composant.metier.referentiel.contact.listecontacttiers.ModeleListeContactTiers;
import ri.serien.libswing.composant.metier.referentiel.contact.sncontact.SNContact;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.outil.clipboard.Clipboard;

/**
 * @author Stéphane Vénéri
 */
public class VGAM04FM_B1 extends SNPanelEcranRPG implements ioFrame {
  private String[] FRLIT_Value = { "", "1", "9", };
  private String[] TYPFRS_Value = { "", "I", "C", };
  private IdFournisseur idFournisseur = null;
  private boolean ps149 = false;
  
  public VGAM04FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    p_TCI1.setRightDecoration(btnListeContact);
    p_TCI2.setRightDecoration(TCI2);
    p_TCI3.setRightDecoration(TCI3);
    p_TCI4.setRightDecoration(TCI4);
    p_TCI5.setRightDecoration(TCI5);
    p_TCI6.setRightDecoration(TCI6);
    
    // RiTextfield
    FRNOM.activerModeFantome("nom ou raison sociale");
    FRCPL.activerModeFantome("complément de nom");
    FRRUE.activerModeFantome("rue");
    FRLOC.activerModeFantome("localité");
    FRCDP.activerModeFantome("00000");
    FRVIL.activerModeFantome("ville");
    FRPAY.activerModeFantome("pays");
    
    // Ajout
    initDiverses();
    FRLIT.setValeurs(FRLIT_Value, null);
    FRSTQ.setValeursSelection("1", " ");
    FRIN5.setValeursSelection("1", " ");
    TYPFRS.setValeurs(TYPFRS_Value, null);
    
    // $$travaux$$
    riSousMenu4.setEnabled(false);
    // riSousMenu5.setEnabled(false);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUMAUTO@")).trim());
    OBJ_47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUMSUIV@")).trim());
    INDFRS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDFRS@")).trim());
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDCOL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDCOL@")).trim());
    label5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Historique de modifications: @LIBPG@")).trim());
    label6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Fournisseur de regroupement  @NOMFRR@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    CFLIBR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CFLIBR@")).trim());
    SACRM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SACRM@")).trim());
    BSACRM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@BSACRM@")).trim());
    SACR1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SACR1@")).trim());
    BSACR1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@BSACR1@")).trim());
    SACR0.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SACR0@")).trim());
    BSACR0.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@BSACR0@")).trim());
    NOM1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NOM1@")).trim());
    VIL1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VIL1@")).trim());
    TEL1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TEL1@")).trim());
    NOM2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NOM2@")).trim());
    VIL2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VIL2@")).trim());
    TEL2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TEL2@")).trim());
    NOM3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NOM3@")).trim());
    VIL3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VIL3@")).trim());
    TEL3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TEL3@")).trim());
    ULBRGL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ULBRGL@")).trim());
    DVLIBR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DVLIBR@")).trim());
    TFLIB_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TFLIB2@")).trim());
    riZoneSortie1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WKAP@")).trim());
    OBJ_49.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Historique de modifications: @LIBPG@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    riSousMenu2.setEnabled((lexique.getMode() == 3));
    riSousMenu3.setEnabled((lexique.getMode() == 3));
    riSousMenu5.setEnabled((lexique.getMode() == 3));
    riSousMenu9.setEnabled((lexique.getMode() == 3));
    riSousMenu1.setEnabled(lexique.isTrue("96"));
    
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    ManagerSessionClient.getInstance().setIdEtablissement(getIdSession(),
        IdEtablissement.getInstance(lexique.HostFieldGetData("INDETB")));
    // PS149 : numérotation automatique en création de fournisseur. Le numéro n'est attribué qu'après une première validation de la fiche.
    ps149 = ManagerSessionClient.getInstance().isParametreSystemeActif(getSession().getIdSession(), EnumParametreSysteme.PS149);
    
    
    OBJ_76.setVisible(lexique.HostFieldGetData("POSTIT").equalsIgnoreCase("+"));
    OBJ_145.setVisible(lexique.isPresent("SACR1"));
    OBJ_146.setVisible(lexique.isPresent("SACR0"));
    OBJ_144.setVisible(lexique.isPresent("SACRM"));
    
    if (lexique.isTrue("53") && (lexique.HostFieldGetData("FRE1").trim().equals(""))
        && (lexique.HostFieldGetData("FRE2").trim().equals("")) && (lexique.HostFieldGetData("FRE3").trim().equals(""))
        && (lexique.HostFieldGetData("FRE4").trim().equals("")) && (lexique.HostFieldGetData("FRE5").trim().equals(""))
        && (lexique.HostFieldGetData("FRE6").trim().equals("")) && (lexique.HostFieldGetData("FRE7").trim().equals(""))) {
      TCI2.setEnabled(false);
    }
    else {
      TCI2.setEnabled(true);
    }
    OBJ_47.setVisible(lexique.HostFieldGetData("NUMAUTO").equalsIgnoreCase("D000010L21") & lexique.isPresent("NUMSUIV"));
    OBJ_46.setVisible(lexique.HostFieldGetData("NUMAUTO").equalsIgnoreCase("D000010L21") & lexique.isPresent("NUMAUTO"));
    
    // Adresses
    if (!lexique.HostFieldGetData("WFEEXT").trim().isEmpty() && !lexique.HostFieldGetData("WFEEXT").equals("0")) {
      p_TCI2.setTitle("Adresses (" + lexique.HostFieldGetData("WFEEXT") + ")");
      TCI2.setEnabled(true);
    }
    else {
      p_TCI2.setTitle("Adresses");
    }
    if (getIdFournisseur() == null) {
      TCI2.setEnabled(false);
    }
    
    OBJ_111.setVisible(lexique.isPresent("COEF"));
    
    // FRLIT.setEnabled( lexique.isPresent("FRLIT"));
    OBJ_124.setVisible(FRDEV.isVisible());
    OBJ_111.setVisible(true);
    if (lexique.HostFieldGetData("POSTIT").trim().equals("+")) {
      riSousMenu_bt15.setText("Gérer les mémos");
    }
    else {
      riSousMenu_bt15.setText("Créer un mémo");
    }
    
    // En Interrogation +++++++++++++++++++++++++++++++++++++++++++++++++++
    if (lexique.isTrue("53")) {
      FRLIT.setEnabled(false);
      riBoutonDetail1.setEnabled(false);
    }
    else {
      FRLIT.setEnabled(true);
      riBoutonDetail1.setEnabled(true);
    }
    
    // En création
    p_TCI6.setVisible(!(lexique.isTrue("51") && lexique.isTrue("N56")));
    TCI6.setEnabled(p_TCI6.isVisible() && getIdFournisseur() != null);
    
    // panel d'alerte +++++++++++++++++++++++++++++++++++++++++++++++++++++
    // litige
    if (lexique.HostFieldGetData("FRLIT").trim().equals("1")) {
      layeredPane1.setComponentZOrder(p_Alerte, 0);
      p_Alerte.setVisible(true);
      l_alerte.setIcon(lexique.chargerImage("images/litiges.png", true));
      this.repaint();
    }
    // désactivé
    else if (lexique.HostFieldGetData("FRLIT").trim().equals("9")) {
      layeredPane1.setComponentZOrder(p_Alerte, 0);
      p_Alerte.setVisible(true);
      l_alerte.setIcon(lexique.chargerImage("images/desact.png", true));
      this.repaint();
    }
    else {
      p_Alerte.setVisible(false);
    }
    
    // mode historique de modifications
    label5.setVisible(lexique.isTrue("96"));
    riSousMenu1.setVisible(lexique.isTrue("96"));
    
    navig_valid.setVisible(lexique.getMode() != Lexical.MODE_CONSULTATION);
    
    label6.setVisible(!lexique.HostFieldGetData("NOMFRR").trim().equals(""));
    
    // Contact
    snContact.setSession(getSession());
    snContact.setIdEtablissement(getIdEtablissement());
    snContact.setTypeContact(EnumTypeContact.FOURNISSEUR);
    snContact.setIdFournisseur(getIdFournisseur());
    snContact.setSelectionParChampRPG(lexique, "RENUM");
    
    btnListeContact.setEnabled(getIdFournisseur() != null);
    snContact.setEnabled(false);
    snContact.setEditable(false);
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
    OBJ_76.setIcon(lexique.chargerImage("images/rouge2.gif", true));
    OBJ_79.setIcon(lexique.chargerImage("images/fax.gif", true));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  /**
   * Retourner l'identifiant de l'établissement en cours d'affichage.
   */
  private IdEtablissement getIdEtablissement() {
    // Contrôler la présence du champ RPG
    if (lexique.HostFieldGetData("INDETB") == null || lexique.HostFieldGetData("INDETB").isEmpty()) {
      throw new MessageErreurException("L'identifant de l'établissement n'est pas renseigné.");
    }
    
    // Retourner l'identifiant de l'établissement
    return IdEtablissement.getInstance(lexique.HostFieldGetData("INDETB"));
  }
  
  /**
   * Retourner l'identifiant du fournisseur en cours d'affichage.
   */
  private IdFournisseur getIdFournisseur() {
    // Contrôler la présence des champs RPG
    if (!ps149 && (lexique.HostFieldGetData("INDCOL") == null || lexique.HostFieldGetData("INDCOL").isEmpty())) {
      throw new MessageErreurException(
          "Le collectif du fournisseur n'est pas renseigné.\nMerci d'attribuer un collectif de fournisseur avant de compléter cette fiche.");
    }
    if (!ps149 && (lexique.HostFieldGetData("INDFRS") == null || lexique.HostFieldGetData("INDFRS").isEmpty())) {
      throw new MessageErreurException(
          "Le code du fournisseur n'est pas renseigné.\nMerci d'attribuer un numéro de fournisseur avant de compléter cette fiche.");
    }
    if (ps149 && (lexique.HostFieldGetData("INDFRS") == null || lexique.HostFieldGetData("INDFRS").isEmpty())) {
      return null;
    }
    
    // Récupérer le collectif du fournisseur
    int collectif = Constantes.convertirTexteEnInteger(lexique.HostFieldGetData("INDCOL"));
    
    // Récupérer le numéro du fournisseur
    int numero = Constantes.convertirTexteEnInteger(lexique.HostFieldGetData("INDFRS"));
    
    // Retourner l'identifiant du client
    return IdFournisseur.getInstance(getIdEtablissement(), collectif, numero);
  }
  
  /**
   * Retourner l'identifiant du contact principal en cours d'affichage.
   */
  private IdContact getIdContactPrincipal() {
    // Contrôler la présence du champ RPG
    if (lexique.HostFieldGetData("RENUM") == null || lexique.HostFieldGetData("RENUM").isEmpty()) {
      return null;
    }
    
    // Convertir le numéro
    Integer numeroContact = Constantes.convertirTexteEnInteger(lexique.HostFieldGetData("RENUM"));
    
    // Construire l'identifiant du contact
    return IdContact.getInstance(numeroContact);
  }
  
  private String traduireLaZone(String symbole) {
    if (symbole != null) {
      if (symbole.trim().equals("C")) {
        return "commande";
      }
      else if (symbole.trim().equals("E")) {
        return "expédition";
      }
      else if (symbole.trim().equals("F")) {
        return "facturation";
      }
      else if (symbole.trim().equals("R")) {
        return "représentant";
      }
      else {
        return "";
      }
    }
    else {
      return null;
    }
  }
  
  private void bt_option7ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(1, 1);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    String RC = System.getProperty("line.separator");
    
    Clipboard.envoyerTexte(FRNOM.nettoyerLaSaisie() + RC + FRCPL.nettoyerLaSaisie() + RC + FRRUE.nettoyerLaSaisie() + RC
        + FRLOC.nettoyerLaSaisie() + RC + FRCDP.nettoyerLaSaisie() + " " + FRVIL.nettoyerLaSaisie() + RC + FRPAY.nettoyerLaSaisie());
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI7").trim().equals("")) {
      lexique.HostFieldPutData("TCI7", 0, "X");
      lexique.HostScreenSendKey(this, "ENTER");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI7"));
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void riSousMenu_bt16ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(9, 79);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void TCI2ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI2").trim().equals("")) {
      lexique.HostFieldPutData("TCI2", 0, "X");
      lexique.HostScreenSendKey(this, "ENTER");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI2"));
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void TCI3ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI3").trim().equals("")) {
      lexique.HostFieldPutData("TCI3", 0, "X");
      lexique.HostScreenSendKey(this, "ENTER");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI3"));
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void TCI4ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI4").trim().equals("")) {
      lexique.HostFieldPutData("TCI4", 0, "X");
      lexique.HostScreenSendKey(this, "ENTER");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI4"));
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void TCI5ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI5").trim().equals("")) {
      lexique.HostFieldPutData("TCI5", 0, "X");
      lexique.HostScreenSendKey(this, "ENTER");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI5"));
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void TCI6ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI6").trim().equals("")) {
      lexique.HostFieldPutData("TCI6", 0, "X");
      lexique.HostScreenSendKey(this, "ENTER");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI6"));
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void riBoutonDetail1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("GCDPR");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06FO", 0, "²");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt2ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06FO", 0, "C");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt3ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06FO", 0, "F");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt4ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06FO", 0, "P");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt5ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06FO", 0, "E");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06FO", 0, "A");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void thisFocusGained(FocusEvent e) {
    try {
      setData();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btnListeContactActionPerformed(ActionEvent e) {
    try {
      if (getIdFournisseur() == null) {
        return;
      }
      // Afficher la boîte de dialogue avec la liste des contacts
      ModeleListeContactTiers modele = new ModeleListeContactTiers(getSession(), getIdFournisseur());
      DialogueListeContactTiers vue = new DialogueListeContactTiers(modele);
      vue.afficher();
      
      // Récupérer l'identifiant du contact principal de l'écran RPG
      IdContact idContactPrincipalRPG = getIdContactPrincipal();
      
      // Récupérer l'identifiant du contact principal issu de la boîte de dialogue
      IdContact idContactPrincipalJava = null;
      if (modele.getContactPrincipal() != null) {
        idContactPrincipalJava = modele.getContactPrincipal().getId();
      }
      
      // Comparter les identifiants
      if (!Constantes.equals(idContactPrincipalRPG, idContactPrincipalJava)) {
        // Demander au programme RPG de rafraichir les informations du contact principal
        if (lexique.HostFieldGetData("TCI1").trim().equals("")) {
          lexique.HostFieldPutData("TCI1", 0, "X");
          lexique.HostScreenSendKey(this, "ENTER");
        }
        else {
          lexique.HostFieldPutData("V06FO", 0, "1");
          lexique.HostScreenSendKey(this, "ENTER");
        }
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    try {
      lexique.HostCursorPut(1, 1);
      lexique.HostScreenSendKey(this, "F4");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    P_Infos = new JPanel();
    OBJ_46 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_43 = new JLabel();
    INDFRS = new RiZoneSortie();
    INDETB = new RiZoneSortie();
    INDCOL = new RiZoneSortie();
    OBJ_41 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    p_tete_droite = new JPanel();
    TYPFRS = new XRiComboBox();
    FRIN12 = new XRiTextField();
    OBJ_147 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu2 = new RiSousMenu();
    riSousMenu_bt2 = new RiSousMenu_bt();
    riSousMenu3 = new RiSousMenu();
    riSousMenu_bt3 = new RiSousMenu_bt();
    riSousMenu4 = new RiSousMenu();
    riSousMenu_bt4 = new RiSousMenu_bt();
    riSousMenu5 = new RiSousMenu();
    riSousMenu_bt5 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    riSousMenu16 = new RiSousMenu();
    riSousMenu_bt16 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    layeredPane1 = new JLayeredPane();
    panel1 = new JPanel();
    FRPAY = new XRiTextField();
    FRVIL = new XRiTextField();
    FRCDP = new XRiTextField();
    FRCOP = new XRiTextField();
    FRNOM = new XRiTextField();
    FRCPL = new XRiTextField();
    FRRUE = new XRiTextField();
    FRLOC = new XRiTextField();
    panel2 = new JPanel();
    FRCLA = new XRiTextField();
    FRTEL = new XRiTextField();
    FRFAX = new XRiTextField();
    GCDPR = new XRiTextField();
    CFLIBR = new RiZoneSortie();
    FRCL2 = new XRiTextField();
    OBJ_54 = new JLabel();
    OBJ_62 = new JLabel();
    FRCAT = new XRiTextField();
    OBJ_79 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    OBJ_55 = new JLabel();
    riBoutonDetail1 = new SNBoutonDetail();
    p_TCI6 = new JXTitledPanel();
    OBJ_144 = new JLabel();
    SACRM = new RiZoneSortie();
    BSACRM = new RiZoneSortie();
    OBJ_145 = new JLabel();
    SACR1 = new RiZoneSortie();
    BSACR1 = new RiZoneSortie();
    OBJ_146 = new JLabel();
    SACR0 = new RiZoneSortie();
    BSACR0 = new RiZoneSortie();
    FRSTQ = new XRiCheckBox();
    FRIN5 = new XRiCheckBox();
    p_TCI1 = new JXTitledPanel();
    lbContact = new SNLabelChamp();
    snContact = new SNContact();
    p_TCI2 = new JXTitledPanel();
    NOM1 = new RiZoneSortie();
    VIL1 = new RiZoneSortie();
    TEL1 = new RiZoneSortie();
    NOM2 = new RiZoneSortie();
    VIL2 = new RiZoneSortie();
    TEL2 = new RiZoneSortie();
    NOM3 = new RiZoneSortie();
    VIL3 = new RiZoneSortie();
    TEL3 = new RiZoneSortie();
    p_TCI4 = new JXTitledPanel();
    ULBRGL = new RiZoneSortie();
    OBJ_128 = new JLabel();
    FRRGL = new XRiTextField();
    FRECH = new XRiTextField();
    label1 = new JLabel();
    p_TCI3 = new JXTitledPanel();
    DVLIBR = new RiZoneSortie();
    OBJ_111 = new JLabel();
    OBJ_119 = new JLabel();
    OBJ_113 = new JLabel();
    OBJ_122 = new JLabel();
    OBJ_124 = new JLabel();
    FRFCO = new XRiTextField();
    FRDEV = new XRiTextField();
    FRMIN = new XRiTextField();
    FRTFA = new XRiTextField();
    TFLIB_ = new RiZoneSortie();
    riZoneSortie1 = new RiZoneSortie();
    p_Alerte = new JPanel();
    l_alerte = new JLabel();
    p_TCI5 = new JXTitledPanel();
    FRLIT = new XRiComboBox();
    FROBS = new XRiTextField();
    OBJ_89 = new JLabel();
    OBJ_137 = new JLabel();
    OBJ_138 = new JLabel();
    FRNRO = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    OBJ_49 = new JButton();
    OBJ_76 = new JLabel();
    TCI2 = new SNBoutonDetail();
    TCI3 = new SNBoutonDetail();
    TCI4 = new SNBoutonDetail();
    TCI5 = new SNBoutonDetail();
    TCI6 = new SNBoutonDetail();
    bt_option7 = new JMenuItem();
    btnListeContact = new SNBoutonDetail();
    
    // ======== this ========
    setMinimumSize(new Dimension(1140, 650));
    setPreferredSize(new Dimension(1140, 650));
    setName("this");
    addFocusListener(new FocusAdapter() {
      @Override
      public void focusGained(FocusEvent e) {
        thisFocusGained(e);
      }
    });
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Fiche fournisseur");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 0));
          p_tete_gauche.setMinimumSize(new Dimension(900, 0));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ======== P_Infos ========
          {
            P_Infos.setBorder(null);
            P_Infos.setMinimumSize(new Dimension(66, 35));
            P_Infos.setPreferredSize(new Dimension(1000, 35));
            P_Infos.setOpaque(false);
            P_Infos.setName("P_Infos");
            P_Infos.setLayout(null);
            
            // ---- OBJ_46 ----
            OBJ_46.setText("@NUMAUTO@");
            OBJ_46.setName("OBJ_46");
            P_Infos.add(OBJ_46);
            OBJ_46.setBounds(330, 5, 100, 18);
            
            // ---- OBJ_47 ----
            OBJ_47.setText("@NUMSUIV@");
            OBJ_47.setName("OBJ_47");
            P_Infos.add(OBJ_47);
            OBJ_47.setBounds(430, 5, 100, 18);
            
            // ---- OBJ_43 ----
            OBJ_43.setText("Fournisseur");
            OBJ_43.setName("OBJ_43");
            P_Infos.add(OBJ_43);
            OBJ_43.setBounds(160, 5, 78, 18);
            
            // ---- INDFRS ----
            INDFRS.setToolTipText("Num\u00e9ro fournisseur");
            INDFRS.setComponentPopupMenu(BTD);
            INDFRS.setFocusable(false);
            INDFRS.setOpaque(false);
            INDFRS.setText("@INDFRS@");
            INDFRS.setHorizontalAlignment(SwingConstants.RIGHT);
            INDFRS.setName("INDFRS");
            P_Infos.add(INDFRS);
            INDFRS.setBounds(265, 2, 58, INDFRS.getPreferredSize().height);
            
            // ---- INDETB ----
            INDETB.setToolTipText("Etablissement");
            INDETB.setComponentPopupMenu(BTD);
            INDETB.setFocusable(false);
            INDETB.setOpaque(false);
            INDETB.setText("@INDETB@");
            INDETB.setName("INDETB");
            P_Infos.add(INDETB);
            INDETB.setBounds(105, 2, 40, INDETB.getPreferredSize().height);
            
            // ---- INDCOL ----
            INDCOL.setToolTipText("Collectif");
            INDCOL.setComponentPopupMenu(BTD);
            INDCOL.setFocusable(false);
            INDCOL.setOpaque(false);
            INDCOL.setText("@INDCOL@");
            INDCOL.setHorizontalAlignment(SwingConstants.RIGHT);
            INDCOL.setName("INDCOL");
            P_Infos.add(INDCOL);
            INDCOL.setBounds(240, 2, 22, INDCOL.getPreferredSize().height);
            
            // ---- OBJ_41 ----
            OBJ_41.setText("Etablissement");
            OBJ_41.setName("OBJ_41");
            P_Infos.add(OBJ_41);
            OBJ_41.setBounds(10, 5, 95, 18);
            
            // ---- label5 ----
            label5.setText("Historique de modifications: @LIBPG@");
            label5.setBackground(new Color(44, 74, 116));
            label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
            label5.setForeground(new Color(44, 74, 116));
            label5.setName("label5");
            P_Infos.add(label5);
            label5.setBounds(530, 5, 370, 18);
            
            // ---- label6 ----
            label6.setText("Fournisseur de regroupement  @NOMFRR@");
            label6.setName("label6");
            P_Infos.add(label6);
            label6.setBounds(530, 2, 370, 25);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < P_Infos.getComponentCount(); i++) {
                Rectangle bounds = P_Infos.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = P_Infos.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              P_Infos.setMinimumSize(preferredSize);
              P_Infos.setPreferredSize(preferredSize);
            }
          }
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup().addComponent(P_Infos,
              GroupLayout.PREFERRED_SIZE, 905, GroupLayout.PREFERRED_SIZE));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup().addComponent(P_Infos, GroupLayout.PREFERRED_SIZE,
              30, GroupLayout.PREFERRED_SIZE));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setPreferredSize(new Dimension(80, 26));
          p_tete_droite.setMinimumSize(new Dimension(80, 26));
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
          
          // ---- TYPFRS ----
          TYPFRS.setPreferredSize(new Dimension(190, 26));
          TYPFRS.setModel(
              new DefaultComboBoxModel(new String[] { "Fournisseur classique", "Fournisseur interne", "Fournisseur consignataire" }));
          TYPFRS.setFont(TYPFRS.getFont().deriveFont(TYPFRS.getFont().getStyle() | Font.BOLD));
          TYPFRS.setName("TYPFRS");
          p_tete_droite.add(TYPFRS);
          
          // ---- FRIN12 ----
          FRIN12.setComponentPopupMenu(null);
          FRIN12.setMinimumSize(new Dimension(22, 28));
          FRIN12.setPreferredSize(new Dimension(22, 28));
          FRIN12.setName("FRIN12");
          p_tete_droite.add(FRIN12);
          
          // ---- OBJ_147 ----
          OBJ_147.setText("Type");
          OBJ_147.setName("OBJ_147");
          p_tete_droite.add(OBJ_147);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 400));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu1 ========
            {
              riSousMenu1.setName("riSousMenu1");
              
              // ---- riSousMenu_bt1 ----
              riSousMenu_bt1.setText("Historique modifications");
              riSousMenu_bt1.setName("riSousMenu_bt1");
              riSousMenu_bt1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt1ActionPerformed(e);
                }
              });
              riSousMenu1.add(riSousMenu_bt1);
            }
            menus_haut.add(riSousMenu1);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Documents li\u00e9s");
              riSousMenu_bt6.setToolTipText("Documents li\u00e9s");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Copie le bloc adresse");
              riSousMenu_bt7.setToolTipText("Copie le bloc adresse dans le presse-papiers");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
            
            // ======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");
              
              // ---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Extension fournisseur");
              riSousMenu_bt8.setToolTipText("Extention fournisseur");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
            
            // ======== riSousMenu2 ========
            {
              riSousMenu2.setName("riSousMenu2");
              
              // ---- riSousMenu_bt2 ----
              riSousMenu_bt2.setText("Bons de commande");
              riSousMenu_bt2.setToolTipText("Liste des bons de commande");
              riSousMenu_bt2.setName("riSousMenu_bt2");
              riSousMenu_bt2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt2ActionPerformed(e);
                }
              });
              riSousMenu2.add(riSousMenu_bt2);
            }
            menus_haut.add(riSousMenu2);
            
            // ======== riSousMenu3 ========
            {
              riSousMenu3.setName("riSousMenu3");
              
              // ---- riSousMenu_bt3 ----
              riSousMenu_bt3.setText("Liste des factures");
              riSousMenu_bt3.setToolTipText("Liste des factures");
              riSousMenu_bt3.setName("riSousMenu_bt3");
              riSousMenu_bt3.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt3ActionPerformed(e);
                }
              });
              riSousMenu3.add(riSousMenu_bt3);
            }
            menus_haut.add(riSousMenu3);
            
            // ======== riSousMenu4 ========
            {
              riSousMenu4.setName("riSousMenu4");
              
              // ---- riSousMenu_bt4 ----
              riSousMenu_bt4.setText("Demandes de prix");
              riSousMenu_bt4.setName("riSousMenu_bt4");
              riSousMenu_bt4.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt4ActionPerformed(e);
                }
              });
              riSousMenu4.add(riSousMenu_bt4);
            }
            menus_haut.add(riSousMenu4);
            
            // ======== riSousMenu5 ========
            {
              riSousMenu5.setName("riSousMenu5");
              
              // ---- riSousMenu_bt5 ----
              riSousMenu_bt5.setText("Articles consign\u00e9s");
              riSousMenu_bt5.setName("riSousMenu_bt5");
              riSousMenu_bt5.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt5ActionPerformed(e);
                }
              });
              riSousMenu5.add(riSousMenu_bt5);
            }
            menus_haut.add(riSousMenu5);
            
            // ======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");
              
              // ---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Articles vendus");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
            
            // ======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");
              
              // ---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Lien fournisseur/client");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);
            
            // ======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");
              
              // ---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);
            
            // ======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");
              
              // ---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Bloc-notes");
              riSousMenu_bt14.setToolTipText("Bloc-notes");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
            
            // ======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");
              
              // ---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("M\u00e9mo");
              riSousMenu_bt15.setToolTipText("M\u00e9mo");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed(e);
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);
            
            // ======== riSousMenu16 ========
            {
              riSousMenu16.setName("riSousMenu16");
              
              // ---- riSousMenu_bt16 ----
              riSousMenu_bt16.setText("Adresses sites internet");
              riSousMenu_bt16.setToolTipText("Adresses Sites Internet");
              riSousMenu_bt16.setName("riSousMenu_bt16");
              riSousMenu_bt16.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt16ActionPerformed(e);
                }
              });
              riSousMenu16.add(riSousMenu_bt16);
            }
            menus_haut.add(riSousMenu16);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(940, 550));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          
          // ======== layeredPane1 ========
          {
            layeredPane1.setName("layeredPane1");
            
            // ======== panel1 ========
            {
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);
              
              // ---- FRPAY ----
              FRPAY.setComponentPopupMenu(null);
              FRPAY.setName("FRPAY");
              panel1.add(FRPAY);
              FRPAY.setBounds(5, 155, 270, FRPAY.getPreferredSize().height);
              
              // ---- FRVIL ----
              FRVIL.setComponentPopupMenu(null);
              FRVIL.setName("FRVIL");
              panel1.add(FRVIL);
              FRVIL.setBounds(55, 125, 260, FRVIL.getPreferredSize().height);
              
              // ---- FRCDP ----
              FRCDP.setComponentPopupMenu(null);
              FRCDP.setToolTipText("Code postal");
              FRCDP.setName("FRCDP");
              panel1.add(FRCDP);
              FRCDP.setBounds(5, 125, 50, FRCDP.getPreferredSize().height);
              
              // ---- FRCOP ----
              FRCOP.setComponentPopupMenu(BTD);
              FRCOP.setName("FRCOP");
              panel1.add(FRCOP);
              FRCOP.setBounds(275, 155, 40, FRCOP.getPreferredSize().height);
              
              // ---- FRNOM ----
              FRNOM.setFont(FRNOM.getFont().deriveFont(FRNOM.getFont().getStyle() | Font.BOLD));
              FRNOM.setName("FRNOM");
              panel1.add(FRNOM);
              FRNOM.setBounds(5, 5, 310, FRNOM.getPreferredSize().height);
              
              // ---- FRCPL ----
              FRCPL.setName("FRCPL");
              panel1.add(FRCPL);
              FRCPL.setBounds(5, 35, 310, FRCPL.getPreferredSize().height);
              
              // ---- FRRUE ----
              FRRUE.setName("FRRUE");
              panel1.add(FRRUE);
              FRRUE.setBounds(5, 65, 310, FRRUE.getPreferredSize().height);
              
              // ---- FRLOC ----
              FRLOC.setName("FRLOC");
              panel1.add(FRLOC);
              FRLOC.setBounds(5, 95, 310, FRLOC.getPreferredSize().height);
            }
            layeredPane1.add(panel1, JLayeredPane.DEFAULT_LAYER);
            panel1.setBounds(25, 10, 325, 190);
            
            // ======== panel2 ========
            {
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);
              
              // ---- FRCLA ----
              FRCLA.setComponentPopupMenu(null);
              FRCLA.setName("FRCLA");
              panel2.add(FRCLA);
              FRCLA.setBounds(100, 5, 210, FRCLA.getPreferredSize().height);
              
              // ---- FRTEL ----
              FRTEL.setToolTipText("Num\u00e9ro de t\u00e9l\u00e9phone");
              FRTEL.setComponentPopupMenu(null);
              FRTEL.setName("FRTEL");
              panel2.add(FRTEL);
              FRTEL.setBounds(100, 125, 170, FRTEL.getPreferredSize().height);
              
              // ---- FRFAX ----
              FRFAX.setToolTipText("Num\u00e9ro de fax");
              FRFAX.setComponentPopupMenu(null);
              FRFAX.setName("FRFAX");
              panel2.add(FRFAX);
              FRFAX.setBounds(100, 155, 170, FRFAX.getPreferredSize().height);
              
              // ---- GCDPR ----
              GCDPR.setComponentPopupMenu(BTD);
              GCDPR.setHorizontalAlignment(SwingConstants.RIGHT);
              GCDPR.setName("GCDPR");
              panel2.add(GCDPR);
              GCDPR.setBounds(100, 95, 150, GCDPR.getPreferredSize().height);
              
              // ---- CFLIBR ----
              CFLIBR.setText("@CFLIBR@");
              CFLIBR.setName("CFLIBR");
              panel2.add(CFLIBR);
              CFLIBR.setBounds(143, 67, 165, CFLIBR.getPreferredSize().height);
              
              // ---- FRCL2 ----
              FRCL2.setComponentPopupMenu(null);
              FRCL2.setName("FRCL2");
              panel2.add(FRCL2);
              FRCL2.setBounds(100, 35, 210, FRCL2.getPreferredSize().height);
              
              // ---- OBJ_54 ----
              OBJ_54.setText("Classement");
              OBJ_54.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_54.setName("OBJ_54");
              panel2.add(OBJ_54);
              OBJ_54.setBounds(10, 9, 80, 20);
              
              // ---- OBJ_62 ----
              OBJ_62.setText("Cat\u00e9gorie");
              OBJ_62.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_62.setName("OBJ_62");
              panel2.add(OBJ_62);
              OBJ_62.setBounds(10, 69, 80, 20);
              
              // ---- FRCAT ----
              FRCAT.setComponentPopupMenu(BTD);
              FRCAT.setName("FRCAT");
              panel2.add(FRCAT);
              FRCAT.setBounds(100, 65, 40, FRCAT.getPreferredSize().height);
              
              // ---- OBJ_79 ----
              OBJ_79.setIcon(new ImageIcon("images/fax.gif"));
              OBJ_79.setToolTipText("Num\u00e9ro de fax");
              OBJ_79.setVisible(false);
              OBJ_79.setName("OBJ_79");
              panel2.add(OBJ_79);
              OBJ_79.setBounds(275, 155, 28, 28);
              
              // ---- label2 ----
              label2.setText("Gencod");
              label2.setHorizontalAlignment(SwingConstants.RIGHT);
              label2.setName("label2");
              panel2.add(label2);
              label2.setBounds(10, 99, 80, 20);
              
              // ---- label3 ----
              label3.setText("T\u00e9l\u00e9phone");
              label3.setHorizontalAlignment(SwingConstants.RIGHT);
              label3.setName("label3");
              panel2.add(label3);
              label3.setBounds(10, 129, 80, 20);
              
              // ---- label4 ----
              label4.setText("Fax");
              label4.setHorizontalAlignment(SwingConstants.RIGHT);
              label4.setName("label4");
              panel2.add(label4);
              label4.setBounds(10, 159, 80, 20);
              
              // ---- OBJ_55 ----
              OBJ_55.setText("Classement 2");
              OBJ_55.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_55.setName("OBJ_55");
              panel2.add(OBJ_55);
              OBJ_55.setBounds(10, 39, 80, 20);
              
              // ---- riBoutonDetail1 ----
              riBoutonDetail1.setToolTipText("Gencod");
              riBoutonDetail1.setName("riBoutonDetail1");
              riBoutonDetail1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riBoutonDetail1ActionPerformed(e);
                }
              });
              panel2.add(riBoutonDetail1);
              riBoutonDetail1.setBounds(new Rectangle(new Point(252, 100), riBoutonDetail1.getPreferredSize()));
            }
            layeredPane1.add(panel2, JLayeredPane.DEFAULT_LAYER);
            panel2.setBounds(340, 10, 320, 190);
            
            // ======== p_TCI6 ========
            {
              p_TCI6.setTitle("Statistiques");
              p_TCI6.setBorder(new DropShadowBorder());
              p_TCI6.setTitleFont(p_TCI6.getTitleFont().deriveFont(p_TCI6.getTitleFont().getStyle() | Font.BOLD));
              p_TCI6.setName("p_TCI6");
              Container p_TCI6ContentContainer = p_TCI6.getContentContainer();
              p_TCI6ContentContainer.setLayout(null);
              
              // ---- OBJ_144 ----
              OBJ_144.setText("Chiffre d'affaires mois");
              OBJ_144.setName("OBJ_144");
              p_TCI6ContentContainer.add(OBJ_144);
              OBJ_144.setBounds(15, 14, 123, 20);
              
              // ---- SACRM ----
              SACRM.setComponentPopupMenu(null);
              SACRM.setText("@SACRM@");
              SACRM.setHorizontalAlignment(SwingConstants.RIGHT);
              SACRM.setName("SACRM");
              p_TCI6ContentContainer.add(SACRM);
              SACRM.setBounds(140, 12, 105, SACRM.getPreferredSize().height);
              
              // ---- BSACRM ----
              BSACRM.setComponentPopupMenu(BTD);
              BSACRM.setText("@BSACRM@");
              BSACRM.setHorizontalAlignment(SwingConstants.RIGHT);
              BSACRM.setName("BSACRM");
              p_TCI6ContentContainer.add(BSACRM);
              BSACRM.setBounds(140, 12, 105, BSACRM.getPreferredSize().height);
              
              // ---- OBJ_145 ----
              OBJ_145.setText("Exercice en cours");
              OBJ_145.setName("OBJ_145");
              p_TCI6ContentContainer.add(OBJ_145);
              OBJ_145.setBounds(15, 45, 123, 20);
              
              // ---- SACR1 ----
              SACR1.setComponentPopupMenu(BTD);
              SACR1.setText("@SACR1@");
              SACR1.setHorizontalAlignment(SwingConstants.RIGHT);
              SACR1.setName("SACR1");
              p_TCI6ContentContainer.add(SACR1);
              SACR1.setBounds(140, 43, 105, SACR1.getPreferredSize().height);
              
              // ---- BSACR1 ----
              BSACR1.setComponentPopupMenu(null);
              BSACR1.setText("@BSACR1@");
              BSACR1.setHorizontalAlignment(SwingConstants.RIGHT);
              BSACR1.setName("BSACR1");
              p_TCI6ContentContainer.add(BSACR1);
              BSACR1.setBounds(140, 43, 105, BSACR1.getPreferredSize().height);
              
              // ---- OBJ_146 ----
              OBJ_146.setText("Exercice pr\u00e9c\u00e9dent");
              OBJ_146.setName("OBJ_146");
              p_TCI6ContentContainer.add(OBJ_146);
              OBJ_146.setBounds(15, 75, 123, 20);
              
              // ---- SACR0 ----
              SACR0.setComponentPopupMenu(BTD);
              SACR0.setText("@SACR0@");
              SACR0.setHorizontalAlignment(SwingConstants.RIGHT);
              SACR0.setName("SACR0");
              p_TCI6ContentContainer.add(SACR0);
              SACR0.setBounds(140, 73, 105, SACR0.getPreferredSize().height);
              
              // ---- BSACR0 ----
              BSACR0.setComponentPopupMenu(null);
              BSACR0.setText("@BSACR0@");
              BSACR0.setHorizontalAlignment(SwingConstants.RIGHT);
              BSACR0.setName("BSACR0");
              p_TCI6ContentContainer.add(BSACR0);
              BSACR0.setBounds(140, 73, 105, BSACR0.getPreferredSize().height);
              
              // ---- FRSTQ ----
              FRSTQ.setText("Statistiques quantit\u00e9");
              FRSTQ.setComponentPopupMenu(null);
              FRSTQ.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FRSTQ.setName("FRSTQ");
              p_TCI6ContentContainer.add(FRSTQ);
              FRSTQ.setBounds(13, 102, 139, 20);
              
              // ---- FRIN5 ----
              FRIN5.setText("Exclus e-Commerce");
              FRIN5.setComponentPopupMenu(BTD);
              FRIN5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FRIN5.setName("FRIN5");
              p_TCI6ContentContainer.add(FRIN5);
              FRIN5.setBounds(13, 125, 145, 20);
            }
            layeredPane1.add(p_TCI6, JLayeredPane.DEFAULT_LAYER);
            p_TCI6.setBounds(660, 10, 265, 180);
            
            // ======== p_TCI1 ========
            {
              p_TCI1.setTitle("Contact");
              p_TCI1.setBorder(new DropShadowBorder());
              p_TCI1.setTitleFont(p_TCI1.getTitleFont().deriveFont(p_TCI1.getTitleFont().getStyle() | Font.BOLD));
              p_TCI1.setName("p_TCI1");
              Container p_TCI1ContentContainer = p_TCI1.getContentContainer();
              p_TCI1ContentContainer.setLayout(new GridBagLayout());
              ((GridBagLayout) p_TCI1ContentContainer.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) p_TCI1ContentContainer.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) p_TCI1ContentContainer.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) p_TCI1ContentContainer.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbContact ----
              lbContact.setText("Nom");
              lbContact.setMaximumSize(new Dimension(100, 30));
              lbContact.setMinimumSize(new Dimension(100, 30));
              lbContact.setPreferredSize(new Dimension(100, 30));
              lbContact.setName("lbContact");
              p_TCI1ContentContainer.add(lbContact, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(5, 0, 0, 5), 0, 0));
              
              // ---- snContact ----
              snContact.setName("snContact");
              p_TCI1ContentContainer.add(snContact, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(5, 0, 0, 5), 0, 0));
            }
            layeredPane1.add(p_TCI1, JLayeredPane.DEFAULT_LAYER);
            p_TCI1.setBounds(15, 200, 600, 90);
            
            // ======== p_TCI2 ========
            {
              p_TCI2.setTitle("Adresses");
              p_TCI2.setBorder(new DropShadowBorder());
              p_TCI2.setTitleFont(p_TCI2.getTitleFont().deriveFont(p_TCI2.getTitleFont().getStyle() | Font.BOLD));
              p_TCI2.setName("p_TCI2");
              Container p_TCI2ContentContainer = p_TCI2.getContentContainer();
              p_TCI2ContentContainer.setLayout(null);
              
              // ---- NOM1 ----
              NOM1.setPreferredSize(new Dimension(28, 25));
              NOM1.setText("@NOM1@");
              NOM1.setName("NOM1");
              p_TCI2ContentContainer.add(NOM1);
              NOM1.setBounds(10, 10, 260, 24);
              
              // ---- VIL1 ----
              VIL1.setText("@VIL1@");
              VIL1.setName("VIL1");
              p_TCI2ContentContainer.add(VIL1);
              VIL1.setBounds(275, 10, 195, 24);
              
              // ---- TEL1 ----
              TEL1.setText("@TEL1@");
              TEL1.setName("TEL1");
              p_TCI2ContentContainer.add(TEL1);
              TEL1.setBounds(475, 10, 110, 24);
              
              // ---- NOM2 ----
              NOM2.setPreferredSize(new Dimension(28, 25));
              NOM2.setText("@NOM2@");
              NOM2.setName("NOM2");
              p_TCI2ContentContainer.add(NOM2);
              NOM2.setBounds(10, 37, 260, 24);
              
              // ---- VIL2 ----
              VIL2.setText("@VIL2@");
              VIL2.setName("VIL2");
              p_TCI2ContentContainer.add(VIL2);
              VIL2.setBounds(275, 37, 195, VIL2.getPreferredSize().height);
              
              // ---- TEL2 ----
              TEL2.setText("@TEL2@");
              TEL2.setName("TEL2");
              p_TCI2ContentContainer.add(TEL2);
              TEL2.setBounds(475, 37, 110, TEL2.getPreferredSize().height);
              
              // ---- NOM3 ----
              NOM3.setPreferredSize(new Dimension(28, 25));
              NOM3.setText("@NOM3@");
              NOM3.setName("NOM3");
              p_TCI2ContentContainer.add(NOM3);
              NOM3.setBounds(10, 64, 260, 24);
              
              // ---- VIL3 ----
              VIL3.setText("@VIL3@");
              VIL3.setName("VIL3");
              p_TCI2ContentContainer.add(VIL3);
              VIL3.setBounds(275, 64, 195, VIL3.getPreferredSize().height);
              
              // ---- TEL3 ----
              TEL3.setText("@TEL3@");
              TEL3.setName("TEL3");
              p_TCI2ContentContainer.add(TEL3);
              TEL3.setBounds(475, 64, 110, TEL3.getPreferredSize().height);
            }
            layeredPane1.add(p_TCI2, JLayeredPane.DEFAULT_LAYER);
            p_TCI2.setBounds(15, 295, 600, 125);
            
            // ======== p_TCI4 ========
            {
              p_TCI4.setTitle("R\u00e9glement");
              p_TCI4.setBorder(new DropShadowBorder());
              p_TCI4.setTitleFont(p_TCI4.getTitleFont().deriveFont(p_TCI4.getTitleFont().getStyle() | Font.BOLD));
              p_TCI4.setName("p_TCI4");
              Container p_TCI4ContentContainer = p_TCI4.getContentContainer();
              p_TCI4ContentContainer.setLayout(null);
              
              // ---- ULBRGL ----
              ULBRGL.setText("@ULBRGL@");
              ULBRGL.setName("ULBRGL");
              p_TCI4ContentContainer.add(ULBRGL);
              ULBRGL.setBounds(15, 50, 345, ULBRGL.getPreferredSize().height);
              
              // ---- OBJ_128 ----
              OBJ_128.setText("Mode de r\u00e8glement");
              OBJ_128.setName("OBJ_128");
              p_TCI4ContentContainer.add(OBJ_128);
              OBJ_128.setBounds(15, 19, 120, 20);
              
              // ---- FRRGL ----
              FRRGL.setComponentPopupMenu(BTD);
              FRRGL.setName("FRRGL");
              p_TCI4ContentContainer.add(FRRGL);
              FRRGL.setBounds(132, 15, 34, FRRGL.getPreferredSize().height);
              
              // ---- FRECH ----
              FRECH.setComponentPopupMenu(BTD);
              FRECH.setName("FRECH");
              p_TCI4ContentContainer.add(FRECH);
              FRECH.setBounds(325, 15, 34, FRECH.getPreferredSize().height);
              
              // ---- label1 ----
              label1.setText("Code \u00e9ch\u00e9ance");
              label1.setName("label1");
              p_TCI4ContentContainer.add(label1);
              label1.setBounds(230, 19, 95, 20);
            }
            layeredPane1.add(p_TCI4, JLayeredPane.DEFAULT_LAYER);
            p_TCI4.setBounds(15, 425, 405, 120);
            
            // ======== p_TCI3 ========
            {
              p_TCI3.setTitle("Facturation");
              p_TCI3.setBorder(new DropShadowBorder());
              p_TCI3.setTitleFont(p_TCI3.getTitleFont().deriveFont(p_TCI3.getTitleFont().getStyle() | Font.BOLD));
              p_TCI3.setName("p_TCI3");
              Container p_TCI3ContentContainer = p_TCI3.getContentContainer();
              p_TCI3ContentContainer.setLayout(null);
              
              // ---- DVLIBR ----
              DVLIBR.setText("@DVLIBR@");
              DVLIBR.setName("DVLIBR");
              p_TCI3ContentContainer.add(DVLIBR);
              DVLIBR.setBounds(140, 47, 147, 24);
              
              // ---- OBJ_111 ----
              OBJ_111.setText("Coefficient moyen d'approche");
              OBJ_111.setName("OBJ_111");
              p_TCI3ContentContainer.add(OBJ_111);
              OBJ_111.setBounds(15, 80, 175, 20);
              
              // ---- OBJ_119 ----
              OBJ_119.setText("Minimum de commande");
              OBJ_119.setName("OBJ_119");
              p_TCI3ContentContainer.add(OBJ_119);
              OBJ_119.setBounds(15, 140, 145, 20);
              
              // ---- OBJ_113 ----
              OBJ_113.setText("Facturation");
              OBJ_113.setName("OBJ_113");
              p_TCI3ContentContainer.add(OBJ_113);
              OBJ_113.setBounds(15, 19, 67, 20);
              
              // ---- OBJ_122 ----
              OBJ_122.setText("Franco");
              OBJ_122.setName("OBJ_122");
              p_TCI3ContentContainer.add(OBJ_122);
              OBJ_122.setBounds(15, 109, 45, 20);
              
              // ---- OBJ_124 ----
              OBJ_124.setText("Devise");
              OBJ_124.setName("OBJ_124");
              p_TCI3ContentContainer.add(OBJ_124);
              OBJ_124.setBounds(15, 50, 46, 18);
              
              // ---- FRFCO ----
              FRFCO.setComponentPopupMenu(null);
              FRFCO.setName("FRFCO");
              p_TCI3ContentContainer.add(FRFCO);
              FRFCO.setBounds(85, 105, 50, FRFCO.getPreferredSize().height);
              
              // ---- FRDEV ----
              FRDEV.setComponentPopupMenu(BTD);
              FRDEV.setName("FRDEV");
              p_TCI3ContentContainer.add(FRDEV);
              FRDEV.setBounds(85, 45, 40, FRDEV.getPreferredSize().height);
              
              // ---- FRMIN ----
              FRMIN.setComponentPopupMenu(null);
              FRMIN.setName("FRMIN");
              p_TCI3ContentContainer.add(FRMIN);
              FRMIN.setBounds(237, 135, 50, FRMIN.getPreferredSize().height);
              
              // ---- FRTFA ----
              FRTFA.setComponentPopupMenu(BTD);
              FRTFA.setName("FRTFA");
              p_TCI3ContentContainer.add(FRTFA);
              FRTFA.setBounds(85, 15, 24, FRTFA.getPreferredSize().height);
              
              // ---- TFLIB_ ----
              TFLIB_.setText("@TFLIB2@");
              TFLIB_.setName("TFLIB_");
              p_TCI3ContentContainer.add(TFLIB_);
              TFLIB_.setBounds(140, 17, 147, 24);
              
              // ---- riZoneSortie1 ----
              riZoneSortie1.setText("@WKAP@");
              riZoneSortie1.setName("riZoneSortie1");
              p_TCI3ContentContainer.add(riZoneSortie1);
              riZoneSortie1.setBounds(237, 77, 50, riZoneSortie1.getPreferredSize().height);
            }
            layeredPane1.add(p_TCI3, JLayeredPane.DEFAULT_LAYER);
            p_TCI3.setBounds(620, 200, 305, 220);
            
            // ======== p_Alerte ========
            {
              p_Alerte.setOpaque(false);
              p_Alerte.setName("p_Alerte");
              p_Alerte.setLayout(null);
              
              // ---- l_alerte ----
              l_alerte.setName("l_alerte");
              p_Alerte.add(l_alerte);
              l_alerte.setBounds(5, 5, 640, 450);
            }
            layeredPane1.add(p_Alerte, JLayeredPane.DEFAULT_LAYER);
            p_Alerte.setBounds(150, 30, 650, 460);
            
            // ======== p_TCI5 ========
            {
              p_TCI5.setTitle("Divers");
              p_TCI5.setBorder(new DropShadowBorder());
              p_TCI5.setTitleFont(p_TCI5.getTitleFont().deriveFont(p_TCI5.getTitleFont().getStyle() | Font.BOLD));
              p_TCI5.setName("p_TCI5");
              Container p_TCI5ContentContainer = p_TCI5.getContentContainer();
              p_TCI5ContentContainer.setLayout(null);
              
              // ---- FRLIT ----
              FRLIT.setModel(new DefaultComboBoxModel(new String[] { "Non ", "Litige", "D\u00e9sactiv\u00e9" }));
              FRLIT.setToolTipText("Code litige");
              FRLIT.setComponentPopupMenu(null);
              FRLIT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FRLIT.setName("FRLIT");
              p_TCI5ContentContainer.add(FRLIT);
              FRLIT.setBounds(140, 15, 105, FRLIT.getPreferredSize().height);
              
              // ---- FROBS ----
              FROBS.setComponentPopupMenu(null);
              FROBS.setName("FROBS");
              p_TCI5ContentContainer.add(FROBS);
              FROBS.setBounds(140, 45, 340, FROBS.getPreferredSize().height);
              
              // ---- OBJ_89 ----
              OBJ_89.setText("Notre num\u00e9ro client");
              OBJ_89.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_89.setName("OBJ_89");
              p_TCI5ContentContainer.add(OBJ_89);
              OBJ_89.setBounds(245, 18, 145, 20);
              
              // ---- OBJ_137 ----
              OBJ_137.setText("Observations");
              OBJ_137.setName("OBJ_137");
              p_TCI5ContentContainer.add(OBJ_137);
              OBJ_137.setBounds(20, 49, 84, 20);
              
              // ---- OBJ_138 ----
              OBJ_138.setText("Litige/D\u00e9sactiv\u00e9");
              OBJ_138.setName("OBJ_138");
              p_TCI5ContentContainer.add(OBJ_138);
              OBJ_138.setBounds(20, 18, 100, 20);
              
              // ---- FRNRO ----
              FRNRO.setComponentPopupMenu(BTD);
              FRNRO.setHorizontalAlignment(SwingConstants.LEFT);
              FRNRO.setName("FRNRO");
              p_TCI5ContentContainer.add(FRNRO);
              FRNRO.setBounds(400, 14, 80, FRNRO.getPreferredSize().height);
            }
            layeredPane1.add(p_TCI5, JLayeredPane.DEFAULT_LAYER);
            p_TCI5.setBounds(425, 425, 500, 120);
          }
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                  .addComponent(layeredPane1, GroupLayout.PREFERRED_SIZE, 945, GroupLayout.PREFERRED_SIZE)
                  .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
          p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                  .addComponent(layeredPane1, GroupLayout.PREFERRED_SIZE, 545, GroupLayout.PREFERRED_SIZE)
                  .addContainerGap(3, Short.MAX_VALUE)));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
      
      // ---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    
    // ---- OBJ_49 ----
    OBJ_49.setText("Historique de modifications: @LIBPG@");
    OBJ_49.setToolTipText("Historique/Actuel");
    OBJ_49.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_49.setName("OBJ_49");
    
    // ---- OBJ_76 ----
    OBJ_76.setIcon(new ImageIcon("images/rouge2.gif"));
    OBJ_76.setToolTipText("M\u00e9mo");
    OBJ_76.setName("OBJ_76");
    
    // ---- TCI2 ----
    TCI2.setText("");
    TCI2.setToolTipText("Adresse");
    TCI2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI2.setPreferredSize(new Dimension(18, 18));
    TCI2.setMargin(new Insets(0, 0, 0, 0));
    TCI2.setName("TCI2");
    TCI2.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI2ActionPerformed(e);
      }
    });
    
    // ---- TCI3 ----
    TCI3.setText("");
    TCI3.setToolTipText("Facturation");
    TCI3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI3.setPreferredSize(new Dimension(18, 18));
    TCI3.setName("TCI3");
    TCI3.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI3ActionPerformed(e);
      }
    });
    
    // ---- TCI4 ----
    TCI4.setText("");
    TCI4.setToolTipText("R\u00e9glement");
    TCI4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI4.setPreferredSize(new Dimension(18, 18));
    TCI4.setName("TCI4");
    TCI4.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI4ActionPerformed(e);
      }
    });
    
    // ---- TCI5 ----
    TCI5.setText("");
    TCI5.setToolTipText("Divers");
    TCI5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI5.setPreferredSize(new Dimension(18, 18));
    TCI5.setName("TCI5");
    TCI5.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI5ActionPerformed(e);
      }
    });
    
    // ---- TCI6 ----
    TCI6.setText("");
    TCI6.setToolTipText("Statistiques");
    TCI6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI6.setPreferredSize(new Dimension(18, 18));
    TCI6.setName("TCI6");
    TCI6.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI6ActionPerformed(e);
      }
    });
    
    // ---- bt_option7 ----
    bt_option7.setText("Lien fournisseur client");
    bt_option7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_option7.setMargin(new Insets(0, -20, 0, 0));
    bt_option7.setToolTipText("Lien fournisseur client");
    bt_option7.setFont(bt_option7.getFont().deriveFont(bt_option7.getFont().getSize() - 1f));
    bt_option7.setName("bt_option7");
    bt_option7.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_option7ActionPerformed(e);
      }
    });
    
    // ---- btnListeContact ----
    btnListeContact.setText("");
    btnListeContact.setToolTipText("Liste des contacts du fournisseur");
    btnListeContact.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    btnListeContact.setPreferredSize(new Dimension(18, 18));
    btnListeContact.setMargin(new Insets(0, 0, 0, 0));
    btnListeContact.setName("btnListeContact");
    btnListeContact.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnListeContactActionPerformed(e);
      }
    });
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel P_Infos;
  private JLabel OBJ_46;
  private JLabel OBJ_47;
  private JLabel OBJ_43;
  private RiZoneSortie INDFRS;
  private RiZoneSortie INDETB;
  private RiZoneSortie INDCOL;
  private JLabel OBJ_41;
  private JLabel label5;
  private JLabel label6;
  private JPanel p_tete_droite;
  private XRiComboBox TYPFRS;
  private XRiTextField FRIN12;
  private JLabel OBJ_147;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu2;
  private RiSousMenu_bt riSousMenu_bt2;
  private RiSousMenu riSousMenu3;
  private RiSousMenu_bt riSousMenu_bt3;
  private RiSousMenu riSousMenu4;
  private RiSousMenu_bt riSousMenu_bt4;
  private RiSousMenu riSousMenu5;
  private RiSousMenu_bt riSousMenu_bt5;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private RiSousMenu riSousMenu16;
  private RiSousMenu_bt riSousMenu_bt16;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JLayeredPane layeredPane1;
  private JPanel panel1;
  private XRiTextField FRPAY;
  private XRiTextField FRVIL;
  private XRiTextField FRCDP;
  private XRiTextField FRCOP;
  private XRiTextField FRNOM;
  private XRiTextField FRCPL;
  private XRiTextField FRRUE;
  private XRiTextField FRLOC;
  private JPanel panel2;
  private XRiTextField FRCLA;
  private XRiTextField FRTEL;
  private XRiTextField FRFAX;
  private XRiTextField GCDPR;
  private RiZoneSortie CFLIBR;
  private XRiTextField FRCL2;
  private JLabel OBJ_54;
  private JLabel OBJ_62;
  private XRiTextField FRCAT;
  private JLabel OBJ_79;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel OBJ_55;
  private SNBoutonDetail riBoutonDetail1;
  private JXTitledPanel p_TCI6;
  private JLabel OBJ_144;
  private RiZoneSortie SACRM;
  private RiZoneSortie BSACRM;
  private JLabel OBJ_145;
  private RiZoneSortie SACR1;
  private RiZoneSortie BSACR1;
  private JLabel OBJ_146;
  private RiZoneSortie SACR0;
  private RiZoneSortie BSACR0;
  private XRiCheckBox FRSTQ;
  private XRiCheckBox FRIN5;
  private JXTitledPanel p_TCI1;
  private SNLabelChamp lbContact;
  private SNContact snContact;
  private JXTitledPanel p_TCI2;
  private RiZoneSortie NOM1;
  private RiZoneSortie VIL1;
  private RiZoneSortie TEL1;
  private RiZoneSortie NOM2;
  private RiZoneSortie VIL2;
  private RiZoneSortie TEL2;
  private RiZoneSortie NOM3;
  private RiZoneSortie VIL3;
  private RiZoneSortie TEL3;
  private JXTitledPanel p_TCI4;
  private RiZoneSortie ULBRGL;
  private JLabel OBJ_128;
  private XRiTextField FRRGL;
  private XRiTextField FRECH;
  private JLabel label1;
  private JXTitledPanel p_TCI3;
  private RiZoneSortie DVLIBR;
  private JLabel OBJ_111;
  private JLabel OBJ_119;
  private JLabel OBJ_113;
  private JLabel OBJ_122;
  private JLabel OBJ_124;
  private XRiTextField FRFCO;
  private XRiTextField FRDEV;
  private XRiTextField FRMIN;
  private XRiTextField FRTFA;
  private RiZoneSortie TFLIB_;
  private RiZoneSortie riZoneSortie1;
  private JPanel p_Alerte;
  private JLabel l_alerte;
  private JXTitledPanel p_TCI5;
  private XRiComboBox FRLIT;
  private XRiTextField FROBS;
  private JLabel OBJ_89;
  private JLabel OBJ_137;
  private JLabel OBJ_138;
  private XRiTextField FRNRO;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  private JButton OBJ_49;
  private JLabel OBJ_76;
  private SNBoutonDetail TCI2;
  private SNBoutonDetail TCI3;
  private SNBoutonDetail TCI4;
  private SNBoutonDetail TCI5;
  private SNBoutonDetail TCI6;
  private JMenuItem bt_option7;
  private SNBoutonDetail btnListeContact;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
