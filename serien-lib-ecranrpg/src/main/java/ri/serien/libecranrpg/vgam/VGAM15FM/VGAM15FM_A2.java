
package ri.serien.libecranrpg.vgam.VGAM15FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.Borders;
import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.ODialog;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiPanelNav;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGAM15FM_A2 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  private String[] EAINA_Value = { "", "1", "2", "3", "4", };
  private String[] WIN11_Value = { "", "1", "2", };
  
  private RiPanelNav riPanelNav1 = new RiPanelNav();
  public static final int ETAT_NEUTRE = 0;
  public static final int ETAT_SELECTION = 1;
  public static final int ETAT_ENCOURS = 2;
  
  public ODialog dialog_REGL = null;
  public ODialog dialog_REMISE = null;
  public ODialog dialog_DEVISE = null;
  
  private Icon bloc_couleur = null;
  private Icon bloc_neutre = null;
  private String libelleDirectOuInterne = "";
  private boolean isFacture = false;
  private boolean okstatut = false;
  
  /**
   * Constructeur.
   */
  public VGAM15FM_A2(ArrayList<?> param) throws Throwable {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    E2NOM.activerModeFantome("Nom ou raison sociale");
    E2CPL.activerModeFantome("Complément de nom");
    E2RUE.activerModeFantome("Rue");
    E2LOC.activerModeFantome("Localité");
    E2CDPX.activerModeFantome("00000");
    E2VILR.activerModeFantome("Ville");
    E2PAY.activerModeFantome("Pays");
    E2TEL.activerModeFantome("Téléphone");
    
    xTitledPanel4.setRightDecoration(OBJ_174);
    xTitledPanel5.setRightDecoration(OBJ_191);
    
    // Ajout
    initDiverses();
    EAINA.setValeurs(EAINA_Value, null);
    TOPDLP.setValeursSelection("X", " ");
    TOPAFF.setValeursSelection("1", " ");
    TOPSAN.setValeursSelection("1", " ");
    TOPMAG.setValeursSelection("X", " ");
    WIN11.setValeurs(WIN11_Value, null);
    EAIN10.setValeursSelection("1", " ");
    EADILI.setValeursSelection("O", "N");
    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt4.setIcon(lexique.chargerImage("images/navigation.png", true));
    bloc_couleur = lexique.chargerImage("images/bloc_notes.png", true);
    bloc_neutre = lexique.chargerImage("images/bloc_notes_r.png", true);
    
    // Panel de navigation
    riPanelNav1.setImageEtatAtIndex(ETAT_NEUTRE, (lexique.chargerImage("images/blank.png", true)));
    riPanelNav1.setImageEtatAtIndex(ETAT_SELECTION, (lexique.chargerImage("images/navselec.png", true)));
    riPanelNav1.setImageEtatAtIndex(ETAT_ENCOURS, (lexique.chargerImage("images/navencours.png", true)));
    riPanelNav1.setImageDeFond(lexique.chargerImage("images/vgam15_fac999.jpg", true));
    riPanelNav1.setBoutonNav(10, 5, 140, 50, "btnEntete", "Entête de bon", true, null);
    riPanelNav1.setBoutonNav(10, 80, 140, 110, "btnCorps", "Lignes du bon", false, bouton_valider);
    riPanelNav1.setBoutonNav(10, 200, 140, 50, "btnPied", "Options de fin de bon", false, riSousMenu_bt15);
    riSousMenu18.add(riPanelNav1);
    
    // Les boutons sont cachés car le programme VGAM60 appelé n'est pas sgmisé
    riBoutonDetail10.setVisible(false);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYP15@ @WLBNAT@")).trim());
    LIBSTU.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBSTU@")).trim());
    WDATEX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDATEX@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_109.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WKAP@")).trim());
    DGDEV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGDEV@")).trim());
    OBJ_179.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE1@")).trim());
    OBJ_181.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE2@")).trim());
    OBJ_183.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE3@")).trim());
    OBJ_185.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE4@")).trim());
    OBJ_187.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE5@")).trim());
    LIBDT1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBDT1@")).trim());
    LIBDT2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBDT2@")).trim());
    OBJ_47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EADEV@")).trim());
    OBJ_47.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("Taux de change : @EACHGX@, base @EABAS@")).trim());
    riZoneSortie1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DVLIB@")).trim());
    riZoneSortie1
        .setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("Taux de change : @EACHGX@, base @EABAS@")).trim());
    LFAX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LFAX@")).trim());
    LMAIL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LMAIL@")).trim());
    OBJ_138.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBRGR@")).trim());
    OBJ_141.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBRGR@")).trim());
    MALIBR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MALIBR@")).trim());
    TIT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT@")).trim());
    TIT2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TFLIB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(riMenu_bt2);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    isFacture = lexique.isTrue("44");
    riSousMenu6.setVisible(!isFacture);
    riSousMenu9.setVisible(isFacture);
    riSousMenu6.setVisible(!lexique.HostFieldGetData("EAIN7").trim().isEmpty());
    
    OBJ_201.setVisible(!isFacture);
    EAETAI.setVisible(!isFacture);
    MAG_doublon_147.setVisible(lexique.HostFieldGetData("V01F").equalsIgnoreCase("AFFICH."));
    EAMAG.setVisible(!lexique.HostFieldGetData("V01F").trim().equalsIgnoreCase("AFFICH."));
    OBJ_187.setVisible(lexique.isPresent("TIZPE5"));
    OBJ_185.setVisible(lexique.isPresent("TIZPE4"));
    OBJ_183.setVisible(lexique.isPresent("TIZPE3"));
    OBJ_181.setVisible(lexique.isPresent("TIZPE2"));
    EATP1.setVisible(!lexique.HostFieldGetData("TIZPE1").trim().isEmpty());
    EATP2.setVisible(!lexique.HostFieldGetData("TIZPE2").trim().isEmpty());
    EATP3.setVisible(!lexique.HostFieldGetData("TIZPE3").trim().isEmpty());
    EATP4.setVisible(!lexique.HostFieldGetData("TIZPE4").trim().isEmpty());
    EATP5.setVisible(!lexique.HostFieldGetData("TIZPE5").trim().isEmpty());
    EATP1.setEnabled(lexique.isPresent("EATP1"));
    OBJ_178.setVisible(!lexique.HostFieldGetData("TIZPE1").trim().isEmpty());
    OBJ_179.setVisible(lexique.isPresent("TIZPE1"));
    ACH_doublon_127.setVisible(lexique.HostFieldGetData("V01F").equalsIgnoreCase("AFFICH."));
    EAACH.setVisible(!lexique.HostFieldGetData("V01F").trim().equalsIgnoreCase("AFFICH."));
    DGDEV.setVisible(lexique.isTrue("N86"));
    OBJ_114.setVisible(lexique.isPresent("EACJA"));
    OBJ_109.setVisible(!lexique.HostFieldGetData("WKAP").trim().isEmpty());
    OBJ_203.setEnabled(lexique.isPresent("WCSC"));
    
    // Stock à date
    EADILI.setVisible(lexique.isTrue("70"));
    
    // Traitement du message d'état
    String etatDuBon = lexique.HostFieldGetData("ETABON").trim();
    if (etatDuBon.equals("C")) {
      label_etat.setText("Comptabilisé");
    }
    else if (etatDuBon.equals("H")) {
      label_etat.setText("Validé");
      okstatut = true;
    }
    else if (etatDuBon.equals("A")) {
      label_etat.setText("En attente");
      okstatut = true;
    }
    else if (etatDuBon.equals("L")) {
      label_etat.setText("Livré");
      okstatut = true;
    }
    else if (etatDuBon.equals("R")) {
      label_etat.setText("Réceptionné");
      okstatut = true;
    }
    else if (etatDuBon.equals("P")) {
      label_etat.setText("Payable");
    }
    else if (etatDuBon.equals("f")) {
      label_etat.setText("Lié sur facture");
    }
    else if (etatDuBon.equals("F")) {
      label_etat.setText("Facturé");
    }
    else if (etatDuBon.equals("n")) {
      label_etat.setText("Annulé");
    }
    riSousMenu10.setVisible(!etatDuBon.equals("H"));
    
    // Factures d'achat
    pnlFacture.setVisible(isFacture);
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yy");
    Date date = null;
    try {
      if (!lexique.HostFieldGetData("EADLPX").trim().isEmpty())
        date = simpleDateFormat.parse(lexique.HostFieldGetData("EADLPX"));
    }
    catch (ParseException e) {
      e.printStackTrace();
    }
    EADLPX2.setDate(date);
    try {
      if (!lexique.HostFieldGetData("EADT1X").trim().isEmpty())
        date = simpleDateFormat.parse(lexique.HostFieldGetData("EADT1X"));
    }
    catch (ParseException e) {
      e.printStackTrace();
    }
    EADT1X2.setDate(date);
    TOPDLP2.setSelected(!lexique.HostFieldGetData("TOPDLP").trim().isEmpty());
    TOPDLP2.setVisible(lexique.isPresent("TOPDLP"));
    WCONFU2.setText(lexique.HostFieldGetData("WCONFU"));
    WCONFI2.setText(lexique.HostFieldGetData("WCONFU"));
    WCONFI2.setVisible(lexique.isPresent("WCONFI"));
    pnlCommande.setVisible(!isFacture);
    OBJ_146.setVisible(!etatDuBon.equalsIgnoreCase("F") && !etatDuBon.equalsIgnoreCase("C"));
    EALLV.setVisible(OBJ_146.isVisible());
    LVLB1.setVisible(OBJ_146.isVisible());
    LVLB2.setVisible(OBJ_146.isVisible());
    LVLB3.setVisible(OBJ_146.isVisible());
    
    if (isFacture) {
      lbReference.setText("Référence facture");
    }
    else {
      lbReference.setText("Référence livraison");
    }
    
    okstatut = okstatut && !lexique.HostFieldGetData("LIBSTU").trim().isEmpty();
    
    TitreLIBSTU.setVisible(okstatut);
    LIBSTU.setVisible(okstatut);
    
    OBJ_151.setVisible(WNOMFR.isVisible());
    
    OBJ_141.setVisible(etatDuBon.equalsIgnoreCase("X") & lexique.isPresent("LIBRGR"));
    OBJ_138.setVisible(!etatDuBon.equalsIgnoreCase("X") & lexique.isPresent("LIBRGR"));
    OBJ_118.setVisible(!etatDuBon.equalsIgnoreCase("commande"));
    OBJ_117.setVisible(!isFacture);
    OBJ_127.setVisible(etatDuBon.equalsIgnoreCase("X"));
    OBJ_47.setVisible(lexique.isTrue("N86"));
    OBJ_165.setVisible(lexique.isTrue("N86"));
    label4.setVisible(OBJ_47.isVisible());
    E2PAY.setVisible(!lexique.HostFieldGetData("E2PAY").trim().equalsIgnoreCase("") & lexique.isPresent("E2PAY"));
    FRSDIV_doublon_23.setVisible(!interpreteurD.analyseExpression("@LIBFOUR@").trim().equalsIgnoreCase("Nom du Fournisseur si divers")
        & lexique.isPresent("FRSDIV"));
    LVLB2.setVisible(
        !lexique.HostFieldGetData("LIBFOUR").trim().equalsIgnoreCase("Nom du Fournisseur si divers") & lexique.isPresent("LVLB2"));
    OBJ_170.setVisible(!lexique.HostFieldGetData("WKAP").trim().isEmpty());
    riZoneSortie1.setVisible(OBJ_47.isVisible());
    OBJ_199.setVisible(EAMEX.isVisible());
    OBJ_200.setVisible(EACTR.isVisible());
    
    if (lexique.HostFieldGetData("WLBNAT").trim().equalsIgnoreCase("march.")) {
      p_bpresentation.setText(lexique.HostFieldGetData("TYP15").substring(0, 1).toUpperCase()
          + lexique.HostFieldGetData("TYP15").substring(1).toLowerCase() + " marchandises");
    }
    else {
      p_bpresentation.setText(lexique.HostFieldGetData("TYP15").substring(0, 1).toUpperCase()
          + lexique.HostFieldGetData("TYP15").substring(1).toLowerCase() + lexique.HostFieldGetData("WLBNAT").toLowerCase());
    }
    
    if (lexique.HostFieldGetData("POSTIT").trim().equals("+") || lexique.isTrue("N75")) {
      btnBlocNotes.setIcon(bloc_couleur);
      btnBlocNotes.setToolTipText("Le bloc-notes contient des informations");
    }
    else {
      btnBlocNotes.setIcon(bloc_neutre);
      btnBlocNotes.setToolTipText("Le bloc-notes est vide");
    }
    
    // Bandeau du panel
    if (lexique.HostFieldGetData("EAIN9").trim().equals("D")) {
      libelleDirectOuInterne = " (Direct usine)";
    }
    else if (lexique.HostFieldGetData("EAIN9").trim().equals("I")) {
      libelleDirectOuInterne = " (Commande interne)";
    }
    else {
      libelleDirectOuInterne = "";
    }
    p_bpresentation.setText(p_bpresentation.getText() + libelleDirectOuInterne);
    p_bpresentation.setCodeEtablissement(EAETB.getText());
  }
  
  @Override
  public void getData() {
    super.getData();
    if (isFacture) {
      SimpleDateFormat formater = new SimpleDateFormat("dd.MM.yy");
      if (EADLPX2.getDate() != null) {
        lexique.HostFieldPutData("EADLPX", 0, formater.format(EADLPX2.getDate()));
      }
      if (TOPDLP2.isSelected()) {
        lexique.HostFieldPutData("TOPDLP", 0, "1");
      }
      else {
        lexique.HostFieldPutData("TOPDLP", 0, "");
      }
      if (EADT1X2.getDate() != null) {
        lexique.HostFieldPutData("EADT1X", 0, formater.format(EADT1X2.getDate()));
      }
      lexique.HostFieldPutData("WCONFU", 0, WCONFU2.getText());
      lexique.HostFieldPutData("WCONFI", 0, WCONFI2.getText());
    }
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(6, 61);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("E2NOM");
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F14");
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("E2NOM");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_164ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void OBJ_174ActionPerformed(ActionEvent e) {
    if (dialog_REMISE == null) {
      dialog_REMISE = new ODialog((Window) getTopLevelAncestor(), new REMISES(this));
    }
    dialog_REMISE.affichePopupPerso();
  }
  
  private void OBJ_191ActionPerformed(ActionEvent e) {
    if (dialog_REGL == null) {
      dialog_REGL = new ODialog((Window) getTopLevelAncestor(), new REGLEMENT(this));
    }
    dialog_REGL.affichePopupPerso();
  }
  
  public void set_EAREM1(String valeur) {
    EAREM1.setText(valeur);
  }
  
  public void set_EAREM2(String valeur) {
    EAREM2.setText(valeur);
  }
  
  public void set_EAREM3(String valeur) {
    EAREM3.setText(valeur);
  }
  
  public void set_EARG2(String valeur) {
    EARG2.setText(valeur);
  }
  
  private void OBJ_203ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("EXLIBR");
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WCSC");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_165ActionPerformed(ActionEvent e) {
    if (dialog_DEVISE == null) {
      dialog_DEVISE = new ODialog((Window) getTopLevelAncestor(), new DEVISE(this));
    }
    dialog_DEVISE.affichePopupPerso();
  }
  
  private void OBJ_117ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  /**
   * Désactivée car le programme VGAM60 appelé n'est pas sgmisé.
   */
  private void riBoutonDetail9ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(23, 2);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riMenu_bt2ActionPerformed(ActionEvent e) {
    setData();
  }
  
  private void initComponents() throws Throwable {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_52 = new JLabel();
    WNUM = new XRiTextField();
    EAETB = new XRiTextField();
    OBJ_51 = new JLabel();
    WSUF = new XRiTextField();
    OBJ_114 = new JLabel();
    EACJA = new XRiTextField();
    OBJ_48 = new JLabel();
    EAACH = new XRiTextField();
    TitreLIBSTU = new JLabel();
    LIBSTU = new JLabel();
    p_tete_droite = new JPanel();
    label_etat = new JLabel();
    WDATEX = new JLabel();
    OBJ_50 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riMenu4 = new RiMenu();
    riMenu_bt4 = new RiMenu_bt();
    riSousMenu18 = new RiSousMenu();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel4 = new JPanel();
    OBJ_27 = new JPanel();
    E2NOM = new XRiTextField();
    E2CPL = new XRiTextField();
    E2RUE = new XRiTextField();
    E2LOC = new XRiTextField();
    E2PAY = new XRiTextField();
    E2VILR = new XRiTextField();
    E2TEL = new XRiTextField();
    E2FAX = new XRiTextField();
    EAFRS = new XRiTextField();
    E2CDPX = new XRiTextField();
    EACOL = new XRiTextField();
    WEAEXC = new XRiTextField();
    OBJ_117 = new JLabel();
    label3 = new JLabel();
    btnBlocNotes = new JButton();
    label5 = new JLabel();
    xTitledPanel1 = new JXTitledPanel();
    E20NAT = new XRiTextField();
    OBJ_123 = new JLabel();
    OBJ_140 = new JLabel();
    EASAN = new XRiTextField();
    EAACT = new XRiTextField();
    OBJ_137 = new JLabel();
    TOPSAN = new XRiCheckBox();
    TOPAFF = new XRiCheckBox();
    xTitledPanel2 = new JXTitledPanel();
    EAINA = new XRiComboBox();
    EAMTAX = new XRiTextField();
    OBJ_170 = new JLabel();
    OBJ_116 = new JLabel();
    EACPRX = new XRiTextField();
    OBJ_109 = new RiZoneSortie();
    OBJ_115 = new JLabel();
    DGDEV = new RiZoneSortie();
    OBJ_119 = new JLabel();
    EAIN10 = new XRiCheckBox();
    xTitledPanel4 = new JXTitledPanel();
    OBJ_178 = new JLabel();
    EAREM1 = new XRiTextField();
    EAREM2 = new XRiTextField();
    EAREM3 = new XRiTextField();
    OBJ_179 = new JLabel();
    EATP1 = new XRiTextField();
    OBJ_181 = new JLabel();
    EATP2 = new XRiTextField();
    OBJ_183 = new JLabel();
    EATP3 = new XRiTextField();
    OBJ_185 = new JLabel();
    EATP4 = new XRiTextField();
    OBJ_187 = new JLabel();
    EATP5 = new XRiTextField();
    xTitledPanel5 = new JXTitledPanel();
    LRG2 = new XRiTextField();
    OBJ_195 = new JLabel();
    OBJ_193 = new JLabel();
    EAESCX = new XRiTextField();
    EARG2 = new XRiTextField();
    xTitledPanel3 = new JXTitledPanel();
    TRLIBR = new XRiTextField();
    EXLIBR = new XRiTextField();
    EACNT = new XRiTextField();
    EADOS = new XRiTextField();
    OBJ_166 = new JLabel();
    EAPDS = new XRiTextField();
    EAVOL = new XRiTextField();
    OBJ_199 = new JLabel();
    EAIT1X = new XRiCalendrier();
    OBJ_200 = new JLabel();
    OBJ_169 = new JLabel();
    OBJ_161 = new JLabel();
    EAMEX = new XRiTextField();
    EACTR = new XRiTextField();
    label2 = new JLabel();
    LIBDT1 = new SNLabelTitre();
    EAIT2X = new XRiCalendrier();
    LIBDT2 = new SNLabelTitre();
    panel1 = new JPanel();
    WNOMFR = new XRiTextField();
    LVLB1 = new XRiTextField();
    LVLB2 = new XRiTextField();
    LVLB3 = new XRiTextField();
    OBJ_124 = new JLabel();
    lbReference = new JLabel();
    OBJ_121 = new JLabel();
    OBJ_151 = new JLabel();
    EACCT = new XRiTextField();
    EARBC = new XRiTextField();
    EARBL = new XRiTextField();
    EALLV = new XRiTextField();
    OBJ_146 = new JLabel();
    label4 = new JLabel();
    OBJ_47 = new RiZoneSortie();
    riZoneSortie1 = new RiZoneSortie();
    pnlFacture = new JPanel();
    EADLPX2 = new XRiCalendrier();
    TOPDLP2 = new XRiCheckBox();
    OBJ_213 = new JLabel();
    WCONFI2 = new XRiTextField();
    WCONFU2 = new XRiTextField();
    label6 = new JLabel();
    EADT1X2 = new XRiCalendrier();
    label7 = new JLabel();
    EADILI = new XRiCheckBox();
    pnlCommande = new JPanel();
    EADLPX = new XRiCalendrier();
    TOPDLP = new XRiCheckBox();
    WCONFH = new XRiTextField();
    WCONFM = new XRiTextField();
    OBJ_207 = new JLabel();
    OBJ_206 = new JLabel();
    OBJ_208 = new JLabel();
    OBJ_210 = new JLabel();
    WCONFI = new XRiTextField();
    WCONFU = new XRiTextField();
    label1 = new JLabel();
    EADT1X = new XRiCalendrier();
    OBJ_139 = new JLabel();
    EAIN7 = new XRiTextField();
    OBJ_165 = new SNBoutonDetail();
    label19 = new JLabel();
    LFAX = new RiZoneSortie();
    label20 = new JLabel();
    LMAIL = new RiZoneSortie();
    riBoutonDetail9 = new SNBoutonDetail();
    EARFL = new XRiTextField();
    lbReference2 = new JLabel();
    EAMAG = new XRiTextField();
    TOPMAG = new XRiCheckBox();
    OBJ_147 = new JLabel();
    lbWIN11 = new JLabel();
    WIN11 = new XRiComboBox();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    panel2 = new JPanel();
    OBJ_16 = new JMenuItem();
    MAG_doublon_147 = new XRiTextField();
    OBJ_127 = new JLabel();
    OBJ_138 = new JLabel();
    OBJ_141 = new JLabel();
    OBJ_118 = new JLabel();
    ACH_doublon_127 = new XRiTextField();
    MALIBR = new JLabel();
    TIT = new JLabel();
    OBS2 = new XRiTextField();
    TIT2 = new JLabel();
    FRSDIV_doublon_23 = new XRiTextField();
    OBJ_174 = new SNBoutonDetail();
    OBJ_191 = new SNBoutonDetail();
    OBJ_203 = new JButton();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    OBJ_201 = new JLabel();
    EAETAI = new XRiTextField();
    riBoutonDetail10 = new SNBoutonDetail();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("@TYP15@ @WLBNAT@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 60));
          p_tete_gauche.setMinimumSize(new Dimension(900, 60));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- OBJ_52 ----
          OBJ_52.setText("Num\u00e9ro");
          OBJ_52.setName("OBJ_52");
          
          // ---- WNUM ----
          WNUM.setName("WNUM");
          
          // ---- EAETB ----
          EAETB.setName("EAETB");
          
          // ---- OBJ_51 ----
          OBJ_51.setText("Etablissement");
          OBJ_51.setName("OBJ_51");
          
          // ---- WSUF ----
          WSUF.setName("WSUF");
          
          // ---- OBJ_114 ----
          OBJ_114.setText("Journal");
          OBJ_114.setName("OBJ_114");
          
          // ---- EACJA ----
          EACJA.setComponentPopupMenu(BTD);
          EACJA.setName("EACJA");
          
          // ---- OBJ_48 ----
          OBJ_48.setText("Acheteur");
          OBJ_48.setName("OBJ_48");
          
          // ---- EAACH ----
          EAACH.setComponentPopupMenu(BTD);
          EAACH.setName("EAACH");
          
          // ---- TitreLIBSTU ----
          TitreLIBSTU.setText("Statut de la commande ");
          TitreLIBSTU.setHorizontalTextPosition(SwingConstants.LEFT);
          TitreLIBSTU.setName("TitreLIBSTU");
          
          // ---- LIBSTU ----
          LIBSTU.setText("@LIBSTU@");
          LIBSTU.setBorder(new BevelBorder(BevelBorder.LOWERED));
          LIBSTU.setName("LIBSTU");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5)
                  .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(EAETB, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE).addGap(16, 16, 16)
                  .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE).addGap(4, 4, 4)
                  .addComponent(WNUM, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(WSUF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(18, 18, 18)
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(EAACH, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                  .addComponent(OBJ_114, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(EACJA, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE).addGap(18, 18, 18)
                  .addComponent(TitreLIBSTU, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(LIBSTU, GroupLayout.PREFERRED_SIZE, 235, GroupLayout.PREFERRED_SIZE).addContainerGap()));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(7, 7, 7).addComponent(OBJ_51))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(EAETB, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(7, 7, 7).addComponent(OBJ_52))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(WNUM, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(1, 1, 1)
                  .addGroup(p_tete_gaucheLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                      .addComponent(WSUF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(EAACH, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_114, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(EACJA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(TitreLIBSTU, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addComponent(LIBSTU))));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
          
          // ---- label_etat ----
          label_etat
              .setFont(label_etat.getFont().deriveFont(label_etat.getFont().getStyle() | Font.BOLD, label_etat.getFont().getSize() + 3f));
          label_etat.setName("label_etat");
          p_tete_droite.add(label_etat);
          
          // ---- WDATEX ----
          WDATEX.setComponentPopupMenu(BTD);
          WDATEX.setText("@WDATEX@");
          WDATEX.setHorizontalAlignment(SwingConstants.CENTER);
          WDATEX.setFont(WDATEX.getFont().deriveFont(WDATEX.getFont().getStyle() | Font.BOLD));
          WDATEX.setBorder(new BevelBorder(BevelBorder.LOWERED));
          WDATEX.setName("WDATEX");
          p_tete_droite.add(WDATEX);
          
          // ---- OBJ_50 ----
          OBJ_50.setText("Date de traitement");
          OBJ_50.setName("OBJ_50");
          p_tete_droite.add(OBJ_50);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu_bt2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riMenu_bt2ActionPerformed(e);
                }
              });
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Gestion des dates");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Options fournisseur");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
            
            // ======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");
              
              // ---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Rapprochement bon");
              riSousMenu_bt9.setToolTipText("D\u00e9part rapprochement d'un nouveau bon");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
            
            // ======== riSousMenu1 ========
            {
              riSousMenu1.setName("riSousMenu1");
              
              // ---- riSousMenu_bt1 ----
              riSousMenu_bt1.setText("Emballage");
              riSousMenu_bt1.setToolTipText("Gestion d'emballage");
              riSousMenu_bt1.setName("riSousMenu_bt1");
              riSousMenu_bt1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt1ActionPerformed(e);
                }
              });
              riSousMenu1.add(riSousMenu_bt1);
            }
            menus_haut.add(riSousMenu1);
            
            // ======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");
              
              // ---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Changement fournisseur");
              riSousMenu_bt10.setToolTipText("Changement de fournisseur");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);
            
            // ======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");
              
              // ---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Autre adresse frs");
              riSousMenu_bt11.setToolTipText("Recherche li\u00e9e au fournisseur de regroupement");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);
            
            // ======== riMenu4 ========
            {
              riMenu4.setName("riMenu4");
              
              // ---- riMenu_bt4 ----
              riMenu_bt4.setText("Navigation");
              riMenu_bt4.setName("riMenu_bt4");
              riMenu4.add(riMenu_bt4);
            }
            menus_haut.add(riMenu4);
            
            // ======== riSousMenu18 ========
            {
              riSousMenu18.setPreferredSize(new Dimension(170, 260));
              riSousMenu18.setMinimumSize(new Dimension(170, 260));
              riSousMenu18.setMaximumSize(new Dimension(170, 260));
              riSousMenu18.setMargin(new Insets(0, -2, 0, 0));
              riSousMenu18.setOpaque(true);
              riSousMenu18.setName("riSousMenu18");
            }
            menus_haut.add(riSousMenu18);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(960, 615));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(960, 615));
          p_contenu.setName("p_contenu");
          
          // ======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);
            
            // ======== OBJ_27 ========
            {
              OBJ_27.setOpaque(false);
              OBJ_27.setBorder(new TitledBorder("Fournisseur"));
              OBJ_27.setMinimumSize(new Dimension(354, 301));
              OBJ_27.setPreferredSize(new Dimension(354, 300));
              OBJ_27.setName("OBJ_27");
              OBJ_27.setLayout(null);
              
              // ---- E2NOM ----
              E2NOM.setComponentPopupMenu(null);
              E2NOM.setName("E2NOM");
              OBJ_27.add(E2NOM);
              E2NOM.setBounds(20, 58, 310, E2NOM.getPreferredSize().height);
              
              // ---- E2CPL ----
              E2CPL.setComponentPopupMenu(null);
              E2CPL.setName("E2CPL");
              OBJ_27.add(E2CPL);
              E2CPL.setBounds(20, 86, 310, E2CPL.getPreferredSize().height);
              
              // ---- E2RUE ----
              E2RUE.setComponentPopupMenu(null);
              E2RUE.setName("E2RUE");
              OBJ_27.add(E2RUE);
              E2RUE.setBounds(20, 114, 310, E2RUE.getPreferredSize().height);
              
              // ---- E2LOC ----
              E2LOC.setComponentPopupMenu(null);
              E2LOC.setName("E2LOC");
              OBJ_27.add(E2LOC);
              E2LOC.setBounds(20, 142, 310, E2LOC.getPreferredSize().height);
              
              // ---- E2PAY ----
              E2PAY.setComponentPopupMenu(null);
              E2PAY.setName("E2PAY");
              OBJ_27.add(E2PAY);
              E2PAY.setBounds(20, 198, 310, E2PAY.getPreferredSize().height);
              
              // ---- E2VILR ----
              E2VILR.setComponentPopupMenu(null);
              E2VILR.setName("E2VILR");
              OBJ_27.add(E2VILR);
              E2VILR.setBounds(70, 170, 260, E2VILR.getPreferredSize().height);
              
              // ---- E2TEL ----
              E2TEL.setToolTipText("Num\u00e9ro de t\u00e9l\u00e9phone");
              E2TEL.setComponentPopupMenu(null);
              E2TEL.setName("E2TEL");
              OBJ_27.add(E2TEL);
              E2TEL.setBounds(50, 226, 120, E2TEL.getPreferredSize().height);
              
              // ---- E2FAX ----
              E2FAX.setToolTipText("Num\u00e9ro de fax");
              E2FAX.setComponentPopupMenu(null);
              E2FAX.setName("E2FAX");
              OBJ_27.add(E2FAX);
              E2FAX.setBounds(210, 226, 120, E2FAX.getPreferredSize().height);
              
              // ---- EAFRS ----
              EAFRS.setComponentPopupMenu(BTD);
              EAFRS.setName("EAFRS");
              OBJ_27.add(EAFRS);
              EAFRS.setBounds(40, 30, 69, EAFRS.getPreferredSize().height);
              
              // ---- E2CDPX ----
              E2CDPX.setComponentPopupMenu(null);
              E2CDPX.setName("E2CDPX");
              OBJ_27.add(E2CDPX);
              E2CDPX.setBounds(20, 170, 50, E2CDPX.getPreferredSize().height);
              
              // ---- EACOL ----
              EACOL.setComponentPopupMenu(BTD);
              EACOL.setName("EACOL");
              OBJ_27.add(EACOL);
              EACOL.setBounds(20, 30, 20, EACOL.getPreferredSize().height);
              
              // ---- WEAEXC ----
              WEAEXC.setComponentPopupMenu(BTD);
              WEAEXC.setName("WEAEXC");
              OBJ_27.add(WEAEXC);
              WEAEXC.setBounds(280, 30, 50, WEAEXC.getPreferredSize().height);
              
              // ---- OBJ_117 ----
              OBJ_117.setText("Adresse commande");
              OBJ_117.setName("OBJ_117");
              OBJ_27.add(OBJ_117);
              OBJ_117.setBounds(155, 34, 127, 20);
              
              // ---- label3 ----
              label3.setText("Fax");
              label3.setComponentPopupMenu(null);
              label3.setName("label3");
              OBJ_27.add(label3);
              label3.setBounds(180, 229, 30, 22);
              
              // ---- btnBlocNotes ----
              btnBlocNotes.setIcon(null);
              btnBlocNotes.setToolTipText("Le bloc-notes contient des informations");
              btnBlocNotes.setBorderPainted(false);
              btnBlocNotes.setContentAreaFilled(false);
              btnBlocNotes.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              btnBlocNotes.setName("btnBlocNotes");
              btnBlocNotes.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_117ActionPerformed(e);
                }
              });
              OBJ_27.add(btnBlocNotes);
              btnBlocNotes.setBounds(115, 25, 32, 32);
              
              // ---- label5 ----
              label5.setText("T\u00e9l.");
              label5.setComponentPopupMenu(null);
              label5.setName("label5");
              OBJ_27.add(label5);
              label5.setBounds(20, 229, 30, 22);
              
              { // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < OBJ_27.getComponentCount(); i++) {
                  Rectangle bounds = OBJ_27.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = OBJ_27.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                OBJ_27.setMinimumSize(preferredSize);
                OBJ_27.setPreferredSize(preferredSize);
              }
            }
            panel4.add(OBJ_27);
            OBJ_27.setBounds(5, 5, 354, 265);
            
            // ======== xTitledPanel1 ========
            {
              xTitledPanel1.setTitle("Analytique");
              xTitledPanel1.setName("xTitledPanel1");
              Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
              xTitledPanel1ContentContainer.setLayout(null);
              
              // ---- E20NAT ----
              E20NAT.setComponentPopupMenu(BTD);
              E20NAT.setName("E20NAT");
              xTitledPanel1ContentContainer.add(E20NAT);
              E20NAT.setBounds(95, 61, 60, E20NAT.getPreferredSize().height);
              
              // ---- OBJ_123 ----
              OBJ_123.setText("Section");
              OBJ_123.setName("OBJ_123");
              xTitledPanel1ContentContainer.add(OBJ_123);
              OBJ_123.setBounds(10, 9, 48, 20);
              
              // ---- OBJ_140 ----
              OBJ_140.setText("Nature");
              OBJ_140.setName("OBJ_140");
              xTitledPanel1ContentContainer.add(OBJ_140);
              OBJ_140.setBounds(10, 65, 43, 20);
              
              // ---- EASAN ----
              EASAN.setComponentPopupMenu(BTD);
              EASAN.setName("EASAN");
              xTitledPanel1ContentContainer.add(EASAN);
              EASAN.setBounds(95, 5, 50, EASAN.getPreferredSize().height);
              
              // ---- EAACT ----
              EAACT.setComponentPopupMenu(BTD);
              EAACT.setName("EAACT");
              xTitledPanel1ContentContainer.add(EAACT);
              EAACT.setBounds(95, 33, 50, EAACT.getPreferredSize().height);
              
              // ---- OBJ_137 ----
              OBJ_137.setText("Affaire");
              OBJ_137.setName("OBJ_137");
              xTitledPanel1ContentContainer.add(OBJ_137);
              OBJ_137.setBounds(10, 37, 42, 20);
              
              // ---- TOPSAN ----
              TOPSAN.setText("");
              TOPSAN.setComponentPopupMenu(BTD);
              TOPSAN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TOPSAN.setToolTipText("R\u00e9percussion automatique de la section sur les lignes");
              TOPSAN.setName("TOPSAN");
              xTitledPanel1ContentContainer.add(TOPSAN);
              TOPSAN.setBounds(70, 9, 17, 20);
              
              // ---- TOPAFF ----
              TOPAFF.setText("");
              TOPAFF.setComponentPopupMenu(BTD);
              TOPAFF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TOPAFF.setToolTipText("R\u00e9percussion automatique du code affaire sur les lignes");
              TOPAFF.setName("TOPAFF");
              xTitledPanel1ContentContainer.add(TOPAFF);
              TOPAFF.setBounds(70, 37, 17, 20);
            }
            panel4.add(xTitledPanel1);
            xTitledPanel1.setBounds(10, 305, 175, 120);
            
            // ======== xTitledPanel2 ========
            {
              xTitledPanel2.setTitle("Frais sur achat");
              xTitledPanel2.setName("xTitledPanel2");
              Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
              xTitledPanel2ContentContainer.setLayout(null);
              
              // ---- EAINA ----
              EAINA.setModel(new DefaultComboBoxModel(
                  new String[] { "", "Selon le montant HT", "Selon la quantit\u00e9", "Selon le poids", "Selon le volume" }));
              EAINA.setComponentPopupMenu(null);
              EAINA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              EAINA.setName("EAINA");
              xTitledPanel2ContentContainer.add(EAINA);
              EAINA.setBounds(60, 6, 150, EAINA.getPreferredSize().height);
              
              // ---- EAMTAX ----
              EAMTAX.setComponentPopupMenu(null);
              EAMTAX.setHorizontalAlignment(SwingConstants.RIGHT);
              EAMTAX.setName("EAMTAX");
              xTitledPanel2ContentContainer.add(EAMTAX);
              EAMTAX.setBounds(60, 35, 106, EAMTAX.getPreferredSize().height);
              
              // ---- OBJ_170 ----
              OBJ_170.setText("Coefficient d'approche");
              OBJ_170.setName("OBJ_170");
              xTitledPanel2ContentContainer.add(OBJ_170);
              OBJ_170.setBounds(235, 40, 126, 19);
              
              // ---- OBJ_116 ----
              OBJ_116.setText("Coefficient saisi");
              OBJ_116.setName("OBJ_116");
              xTitledPanel2ContentContainer.add(OBJ_116);
              OBJ_116.setBounds(235, 10, 96, 18);
              
              // ---- EACPRX ----
              EACPRX.setComponentPopupMenu(null);
              EACPRX.setName("EACPRX");
              xTitledPanel2ContentContainer.add(EACPRX);
              EACPRX.setBounds(365, 5, 58, EACPRX.getPreferredSize().height);
              
              // ---- OBJ_109 ----
              OBJ_109.setText("@WKAP@");
              OBJ_109.setName("OBJ_109");
              xTitledPanel2ContentContainer.add(OBJ_109);
              OBJ_109.setBounds(365, 37, 58, OBJ_109.getPreferredSize().height);
              
              // ---- OBJ_115 ----
              OBJ_115.setText("Type");
              OBJ_115.setName("OBJ_115");
              xTitledPanel2ContentContainer.add(OBJ_115);
              OBJ_115.setBounds(10, 9, 35, 20);
              
              // ---- DGDEV ----
              DGDEV.setComponentPopupMenu(BTD);
              DGDEV.setText("@DGDEV@");
              DGDEV.setName("DGDEV");
              xTitledPanel2ContentContainer.add(DGDEV);
              DGDEV.setBounds(170, 37, 40, DGDEV.getPreferredSize().height);
              
              // ---- OBJ_119 ----
              OBJ_119.setText("Valeur");
              OBJ_119.setName("OBJ_119");
              xTitledPanel2ContentContainer.add(OBJ_119);
              OBJ_119.setBounds(10, 42, 42, 15);
              
              // ---- EAIN10 ----
              EAIN10.setText("Franco de port");
              EAIN10.setName("EAIN10");
              xTitledPanel2ContentContainer.add(EAIN10);
              EAIN10.setBounds(60, 65, 135, EAIN10.getPreferredSize().height);
            }
            panel4.add(xTitledPanel2);
            xTitledPanel2.setBounds(190, 305, 440, 120);
            
            // ======== xTitledPanel4 ========
            {
              xTitledPanel4.setTitle("Remises");
              xTitledPanel4.setName("xTitledPanel4");
              Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
              xTitledPanel4ContentContainer.setLayout(null);
              
              // ---- OBJ_178 ----
              OBJ_178.setText("Zone personnalis\u00e9e");
              OBJ_178.setName("OBJ_178");
              xTitledPanel4ContentContainer.add(OBJ_178);
              OBJ_178.setBounds(170, 15, 123, 20);
              
              // ---- EAREM1 ----
              EAREM1.setComponentPopupMenu(null);
              EAREM1.setName("EAREM1");
              xTitledPanel4ContentContainer.add(EAREM1);
              EAREM1.setBounds(10, 10, 50, EAREM1.getPreferredSize().height);
              
              // ---- EAREM2 ----
              EAREM2.setComponentPopupMenu(null);
              EAREM2.setName("EAREM2");
              xTitledPanel4ContentContainer.add(EAREM2);
              EAREM2.setBounds(60, 10, 50, EAREM2.getPreferredSize().height);
              
              // ---- EAREM3 ----
              EAREM3.setComponentPopupMenu(null);
              EAREM3.setName("EAREM3");
              xTitledPanel4ContentContainer.add(EAREM3);
              EAREM3.setBounds(110, 10, 50, EAREM3.getPreferredSize().height);
              
              // ---- OBJ_179 ----
              OBJ_179.setText("@TIZPE1@");
              OBJ_179.setHorizontalAlignment(SwingConstants.LEFT);
              OBJ_179.setName("OBJ_179");
              xTitledPanel4ContentContainer.add(OBJ_179);
              OBJ_179.setBounds(290, 15, 25, 20);
              
              // ---- EATP1 ----
              EATP1.setComponentPopupMenu(BTD);
              EATP1.setName("EATP1");
              xTitledPanel4ContentContainer.add(EATP1);
              EATP1.setBounds(315, 11, 34, EATP1.getPreferredSize().height);
              
              // ---- OBJ_181 ----
              OBJ_181.setText("@TIZPE2@");
              OBJ_181.setHorizontalAlignment(SwingConstants.LEFT);
              OBJ_181.setName("OBJ_181");
              xTitledPanel4ContentContainer.add(OBJ_181);
              OBJ_181.setBounds(351, 15, 25, 20);
              
              // ---- EATP2 ----
              EATP2.setComponentPopupMenu(BTD);
              EATP2.setName("EATP2");
              xTitledPanel4ContentContainer.add(EATP2);
              EATP2.setBounds(375, 11, 34, EATP2.getPreferredSize().height);
              
              // ---- OBJ_183 ----
              OBJ_183.setText("@TIZPE3@");
              OBJ_183.setHorizontalAlignment(SwingConstants.LEFT);
              OBJ_183.setName("OBJ_183");
              xTitledPanel4ContentContainer.add(OBJ_183);
              OBJ_183.setBounds(412, 15, 25, 20);
              
              // ---- EATP3 ----
              EATP3.setComponentPopupMenu(BTD);
              EATP3.setName("EATP3");
              xTitledPanel4ContentContainer.add(EATP3);
              EATP3.setBounds(435, 11, 34, EATP3.getPreferredSize().height);
              
              // ---- OBJ_185 ----
              OBJ_185.setText("@TIZPE4@");
              OBJ_185.setHorizontalAlignment(SwingConstants.LEFT);
              OBJ_185.setName("OBJ_185");
              xTitledPanel4ContentContainer.add(OBJ_185);
              OBJ_185.setBounds(473, 15, 25, 20);
              
              // ---- EATP4 ----
              EATP4.setComponentPopupMenu(BTD);
              EATP4.setName("EATP4");
              xTitledPanel4ContentContainer.add(EATP4);
              EATP4.setBounds(495, 11, 34, EATP4.getPreferredSize().height);
              
              // ---- OBJ_187 ----
              OBJ_187.setText("@TIZPE5@");
              OBJ_187.setHorizontalAlignment(SwingConstants.LEFT);
              OBJ_187.setName("OBJ_187");
              xTitledPanel4ContentContainer.add(OBJ_187);
              OBJ_187.setBounds(534, 15, 25, 20);
              
              // ---- EATP5 ----
              EATP5.setComponentPopupMenu(BTD);
              EATP5.setName("EATP5");
              xTitledPanel4ContentContainer.add(EATP5);
              EATP5.setBounds(560, 11, 34, EATP5.getPreferredSize().height);
            }
            panel4.add(xTitledPanel4);
            xTitledPanel4.setBounds(10, 430, 620, 80);
            
            // ======== xTitledPanel5 ========
            {
              xTitledPanel5.setTitle("R\u00e8glement");
              xTitledPanel5.setName("xTitledPanel5");
              Container xTitledPanel5ContentContainer = xTitledPanel5.getContentContainer();
              xTitledPanel5ContentContainer.setLayout(null);
              
              // ---- LRG2 ----
              LRG2.setName("LRG2");
              xTitledPanel5ContentContainer.add(LRG2);
              LRG2.setBounds(130, 10, 294, LRG2.getPreferredSize().height);
              
              // ---- OBJ_195 ----
              OBJ_195.setText("% Escompte");
              OBJ_195.setName("OBJ_195");
              xTitledPanel5ContentContainer.add(OBJ_195);
              OBJ_195.setBounds(445, 14, 85, 20);
              
              // ---- OBJ_193 ----
              OBJ_193.setText("Ech\u00e9ance");
              OBJ_193.setName("OBJ_193");
              xTitledPanel5ContentContainer.add(OBJ_193);
              OBJ_193.setBounds(65, 14, 63, 20);
              
              // ---- EAESCX ----
              EAESCX.setComponentPopupMenu(null);
              EAESCX.setName("EAESCX");
              xTitledPanel5ContentContainer.add(EAESCX);
              EAESCX.setBounds(530, 10, 42, EAESCX.getPreferredSize().height);
              
              // ---- EARG2 ----
              EARG2.setComponentPopupMenu(BTD);
              EARG2.setName("EARG2");
              xTitledPanel5ContentContainer.add(EARG2);
              EARG2.setBounds(10, 10, 34, EARG2.getPreferredSize().height);
            }
            panel4.add(xTitledPanel5);
            xTitledPanel5.setBounds(10, 515, 620, 80);
            
            // ======== xTitledPanel3 ========
            {
              xTitledPanel3.setTitle("Transport");
              xTitledPanel3.setName("xTitledPanel3");
              Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
              xTitledPanel3ContentContainer.setLayout(null);
              
              // ---- TRLIBR ----
              TRLIBR.setName("TRLIBR");
              xTitledPanel3ContentContainer.add(TRLIBR);
              TRLIBR.setBounds(130, 38, 150, TRLIBR.getPreferredSize().height);
              
              // ---- EXLIBR ----
              EXLIBR.setName("EXLIBR");
              xTitledPanel3ContentContainer.add(EXLIBR);
              EXLIBR.setBounds(130, 8, 150, EXLIBR.getPreferredSize().height);
              
              // ---- EACNT ----
              EACNT.setComponentPopupMenu(null);
              EACNT.setName("EACNT");
              xTitledPanel3ContentContainer.add(EACNT);
              EACNT.setBounds(98, 128, 140, EACNT.getPreferredSize().height);
              
              // ---- EADOS ----
              EADOS.setComponentPopupMenu(BTD);
              EADOS.setName("EADOS");
              xTitledPanel3ContentContainer.add(EADOS);
              EADOS.setBounds(98, 158, 82, EADOS.getPreferredSize().height);
              
              // ---- OBJ_166 ----
              OBJ_166.setText("Container");
              OBJ_166.setName("OBJ_166");
              xTitledPanel3ContentContainer.add(OBJ_166);
              OBJ_166.setBounds(8, 132, 77, 21);
              
              // ---- EAPDS ----
              EAPDS.setComponentPopupMenu(BTD);
              EAPDS.setName("EAPDS");
              xTitledPanel3ContentContainer.add(EAPDS);
              EAPDS.setBounds(98, 188, 82, EAPDS.getPreferredSize().height);
              
              // ---- EAVOL ----
              EAVOL.setComponentPopupMenu(BTD);
              EAVOL.setName("EAVOL");
              xTitledPanel3ContentContainer.add(EAVOL);
              EAVOL.setBounds(98, 218, 82, EAVOL.getPreferredSize().height);
              
              // ---- OBJ_199 ----
              OBJ_199.setText("Exp\u00e9dition");
              OBJ_199.setName("OBJ_199");
              xTitledPanel3ContentContainer.add(OBJ_199);
              OBJ_199.setBounds(8, 12, 87, 21);
              
              // ---- EAIT1X ----
              EAIT1X.setComponentPopupMenu(null);
              EAIT1X.setName("EAIT1X");
              xTitledPanel3ContentContainer.add(EAIT1X);
              EAIT1X.setBounds(188, 68, 105, EAIT1X.getPreferredSize().height);
              
              // ---- OBJ_200 ----
              OBJ_200.setText("Transport");
              OBJ_200.setName("OBJ_200");
              xTitledPanel3ContentContainer.add(OBJ_200);
              OBJ_200.setBounds(8, 42, 72, 21);
              
              // ---- OBJ_169 ----
              OBJ_169.setText("Volume");
              OBJ_169.setName("OBJ_169");
              xTitledPanel3ContentContainer.add(OBJ_169);
              OBJ_169.setBounds(8, 222, 77, 21);
              
              // ---- OBJ_161 ----
              OBJ_161.setText("Poids");
              OBJ_161.setName("OBJ_161");
              xTitledPanel3ContentContainer.add(OBJ_161);
              OBJ_161.setBounds(8, 192, 77, 21);
              
              // ---- EAMEX ----
              EAMEX.setComponentPopupMenu(BTD);
              EAMEX.setName("EAMEX");
              xTitledPanel3ContentContainer.add(EAMEX);
              EAMEX.setBounds(98, 8, 34, EAMEX.getPreferredSize().height);
              
              // ---- EACTR ----
              EACTR.setComponentPopupMenu(BTD);
              EACTR.setName("EACTR");
              xTitledPanel3ContentContainer.add(EACTR);
              EACTR.setBounds(98, 38, 34, EACTR.getPreferredSize().height);
              
              // ---- label2 ----
              label2.setText("Dossier");
              label2.setName("label2");
              xTitledPanel3ContentContainer.add(label2);
              label2.setBounds(8, 162, 77, 21);
              
              // ---- LIBDT1 ----
              LIBDT1.setText("@LIBDT1@");
              LIBDT1.setFont(LIBDT1.getFont().deriveFont(LIBDT1.getFont().getSize() - 3f));
              LIBDT1.setVerticalAlignment(SwingConstants.CENTER);
              LIBDT1.setName("LIBDT1");
              xTitledPanel3ContentContainer.add(LIBDT1);
              LIBDT1.setBounds(8, 67, 180, LIBDT1.getPreferredSize().height);
              
              // ---- EAIT2X ----
              EAIT2X.setComponentPopupMenu(null);
              EAIT2X.setName("EAIT2X");
              xTitledPanel3ContentContainer.add(EAIT2X);
              EAIT2X.setBounds(188, 98, 105, EAIT2X.getPreferredSize().height);
              
              // ---- LIBDT2 ----
              LIBDT2.setText("@LIBDT2@");
              LIBDT2.setFont(LIBDT2.getFont().deriveFont(LIBDT2.getFont().getSize() - 3f));
              LIBDT2.setVerticalAlignment(SwingConstants.CENTER);
              LIBDT2.setName("LIBDT2");
              xTitledPanel3ContentContainer.add(LIBDT2);
              LIBDT2.setBounds(8, 97, 180, 30);
              
              { // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = xTitledPanel3ContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
                xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
              }
            }
            panel4.add(xTitledPanel3);
            xTitledPanel3.setBounds(635, 305, 300, 290);
            
            // ======== panel1 ========
            {
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);
              
              // ---- WNOMFR ----
              WNOMFR.setComponentPopupMenu(null);
              WNOMFR.setName("WNOMFR");
              panel1.add(WNOMFR);
              WNOMFR.setBounds(205, 230, 365, WNOMFR.getPreferredSize().height);
              
              // ---- LVLB1 ----
              LVLB1.setComponentPopupMenu(null);
              LVLB1.setName("LVLB1");
              panel1.add(LVLB1);
              LVLB1.setBounds(205, 155, 365, LVLB1.getPreferredSize().height);
              
              // ---- LVLB2 ----
              LVLB2.setComponentPopupMenu(null);
              LVLB2.setName("LVLB2");
              panel1.add(LVLB2);
              LVLB2.setBounds(205, 180, 365, LVLB2.getPreferredSize().height);
              
              // ---- LVLB3 ----
              LVLB3.setComponentPopupMenu(null);
              LVLB3.setName("LVLB3");
              panel1.add(LVLB3);
              LVLB3.setBounds(205, 205, 365, LVLB3.getPreferredSize().height);
              
              // ---- OBJ_124 ----
              OBJ_124.setText("R\u00e9f\u00e9rence commande");
              OBJ_124.setName("OBJ_124");
              panel1.add(OBJ_124);
              OBJ_124.setBounds(0, 58, 145, 20);
              
              // ---- lbReference ----
              lbReference.setText("R\u00e9f. livraison fournisseur");
              lbReference.setName("lbReference");
              panel1.add(lbReference);
              lbReference.setBounds(0, 89, 145, 20);
              
              // ---- OBJ_121 ----
              OBJ_121.setText("Commande initiale");
              OBJ_121.setName("OBJ_121");
              panel1.add(OBJ_121);
              OBJ_121.setBounds(0, 29, 145, 20);
              
              // ---- OBJ_151 ----
              OBJ_151.setText("Nom du fournisseur si divers");
              OBJ_151.setName("OBJ_151");
              panel1.add(OBJ_151);
              OBJ_151.setBounds(0, 230, 190, 28);
              
              // ---- EACCT ----
              EACCT.setComponentPopupMenu(null);
              EACCT.setName("EACCT");
              panel1.add(EACCT);
              EACCT.setBounds(140, 25, 110, EACCT.getPreferredSize().height);
              
              // ---- EARBC ----
              EARBC.setComponentPopupMenu(null);
              EARBC.setName("EARBC");
              panel1.add(EARBC);
              EARBC.setBounds(140, 55, 110, EARBC.getPreferredSize().height);
              
              // ---- EARBL ----
              EARBL.setComponentPopupMenu(null);
              EARBL.setName("EARBL");
              panel1.add(EARBL);
              EARBL.setBounds(140, 85, 110, EARBL.getPreferredSize().height);
              
              // ---- EALLV ----
              EALLV.setComponentPopupMenu(BTD);
              EALLV.setName("EALLV");
              panel1.add(EALLV);
              EALLV.setBounds(140, 155, 60, EALLV.getPreferredSize().height);
              
              // ---- OBJ_146 ----
              OBJ_146.setText("Lieu de livraison");
              OBJ_146.setName("OBJ_146");
              panel1.add(OBJ_146);
              OBJ_146.setBounds(0, 160, 145, 20);
              
              // ---- label4 ----
              label4.setText("Devise");
              label4.setHorizontalTextPosition(SwingConstants.RIGHT);
              label4.setHorizontalAlignment(SwingConstants.RIGHT);
              label4.setName("label4");
              panel1.add(label4);
              label4.setBounds(105, 287, 50, 20);
              
              // ---- OBJ_47 ----
              OBJ_47.setText("@EADEV@");
              OBJ_47.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_47.setToolTipText("Taux de change : @EACHGX@, base @EABAS@");
              OBJ_47.setName("OBJ_47");
              panel1.add(OBJ_47);
              OBJ_47.setBounds(160, 285, 40, OBJ_47.getPreferredSize().height);
              
              // ---- riZoneSortie1 ----
              riZoneSortie1.setText("@DVLIB@");
              riZoneSortie1.setToolTipText("Taux de change : @EACHGX@, base @EABAS@");
              riZoneSortie1.setName("riZoneSortie1");
              panel1.add(riZoneSortie1);
              riZoneSortie1.setBounds(205, 285, 315, riZoneSortie1.getPreferredSize().height);
              
              // ======== pnlFacture ========
              {
                pnlFacture.setBorder(null);
                pnlFacture.setOpaque(false);
                pnlFacture.setName("pnlFacture");
                pnlFacture.setLayout(null);
                
                // ---- EADLPX2 ----
                EADLPX2.setComponentPopupMenu(BTD);
                EADLPX2.setName("EADLPX2");
                pnlFacture.add(EADLPX2);
                EADLPX2.setBounds(130, 75, 105, EADLPX2.getPreferredSize().height);
                
                // ---- TOPDLP2 ----
                TOPDLP2.setText("");
                TOPDLP2.setToolTipText("R\u00e9percussion automatique de la date de facture sur les lignes");
                TOPDLP2.setComponentPopupMenu(BTD);
                TOPDLP2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                TOPDLP2.setName("TOPDLP2");
                pnlFacture.add(TOPDLP2);
                TOPDLP2.setBounds(240, 79, TOPDLP2.getPreferredSize().width, 20);
                
                // ---- OBJ_213 ----
                OBJ_213.setText("par");
                OBJ_213.setHorizontalAlignment(SwingConstants.CENTER);
                OBJ_213.setName("OBJ_213");
                pnlFacture.add(OBJ_213);
                OBJ_213.setBounds(240, 47, 30, 20);
                
                // ---- WCONFI2 ----
                WCONFI2.setComponentPopupMenu(null);
                WCONFI2.setName("WCONFI2");
                pnlFacture.add(WCONFI2);
                WCONFI2.setBounds(110, 43, 20, WCONFI2.getPreferredSize().height);
                
                // ---- WCONFU2 ----
                WCONFU2.setComponentPopupMenu(null);
                WCONFU2.setName("WCONFU2");
                pnlFacture.add(WCONFU2);
                WCONFU2.setBounds(270, 43, 20, WCONFU2.getPreferredSize().height);
                
                // ---- label6 ----
                label6.setText("Confirmation ");
                label6.setName("label6");
                pnlFacture.add(label6);
                label6.setBounds(15, 45, 95, 25);
                
                // ---- EADT1X2 ----
                EADT1X2.setComponentPopupMenu(null);
                EADT1X2.setToolTipText("Date de confirmation");
                EADT1X2.setName("EADT1X2");
                pnlFacture.add(EADT1X2);
                EADT1X2.setBounds(130, 43, 105, EADT1X2.getPreferredSize().height);
                
                // ---- label7 ----
                label7.setText("Date de facture");
                label7.setName("label7");
                pnlFacture.add(label7);
                label7.setBounds(15, 77, 105, 25);
                
                // ---- EADILI ----
                EADILI.setText("Stock \u00e0 date");
                EADILI.setName("EADILI");
                pnlFacture.add(EADILI);
                EADILI.setBounds(110, 15, 130, EADILI.getPreferredSize().height);
                
                { // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for (int i = 0; i < pnlFacture.getComponentCount(); i++) {
                    Rectangle bounds = pnlFacture.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = pnlFacture.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  pnlFacture.setMinimumSize(preferredSize);
                  pnlFacture.setPreferredSize(preferredSize);
                }
              }
              panel1.add(pnlFacture);
              pnlFacture.setBounds(0, 316, 315, 115);
              
              // ======== pnlCommande ========
              {
                pnlCommande.setBorder(new CompoundBorder(new TitledBorder("R\u00e9ception pr\u00e9vue le"), Borders.DLU2_BORDER));
                pnlCommande.setOpaque(false);
                pnlCommande.setName("pnlCommande");
                pnlCommande.setLayout(null);
                
                // ---- EADLPX ----
                EADLPX.setComponentPopupMenu(BTD);
                EADLPX.setName("EADLPX");
                pnlCommande.add(EADLPX);
                EADLPX.setBounds(20, 25, 105, EADLPX.getPreferredSize().height);
                
                // ---- TOPDLP ----
                TOPDLP.setText("");
                TOPDLP.setToolTipText("R\u00e9percussion automatique de la date de r\u00e9ception pr\u00e9vue sur lignes");
                TOPDLP.setComponentPopupMenu(BTD);
                TOPDLP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                TOPDLP.setName("TOPDLP");
                pnlCommande.add(TOPDLP);
                TOPDLP.setBounds(130, 29, TOPDLP.getPreferredSize().width, 20);
                
                // ---- WCONFH ----
                WCONFH.setComponentPopupMenu(BTD);
                WCONFH.setName("WCONFH");
                pnlCommande.add(WCONFH);
                WCONFH.setBounds(178, 25, 26, WCONFH.getPreferredSize().height);
                
                // ---- WCONFM ----
                WCONFM.setComponentPopupMenu(BTD);
                WCONFM.setName("WCONFM");
                pnlCommande.add(WCONFM);
                WCONFM.setBounds(235, 25, 26, WCONFM.getPreferredSize().height);
                
                // ---- OBJ_207 ----
                OBJ_207.setText("mm");
                OBJ_207.setName("OBJ_207");
                pnlCommande.add(OBJ_207);
                OBJ_207.setBounds(268, 31, 25, 17);
                
                // ---- OBJ_206 ----
                OBJ_206.setText("hh");
                OBJ_206.setName("OBJ_206");
                pnlCommande.add(OBJ_206);
                OBJ_206.setBounds(211, 31, 17, 17);
                
                // ---- OBJ_208 ----
                OBJ_208.setText("\u00e0");
                OBJ_208.setHorizontalAlignment(SwingConstants.CENTER);
                OBJ_208.setName("OBJ_208");
                pnlCommande.add(OBJ_208);
                OBJ_208.setBounds(155, 29, 25, 20);
                
                // ---- OBJ_210 ----
                OBJ_210.setText("par");
                OBJ_210.setHorizontalAlignment(SwingConstants.CENTER);
                OBJ_210.setName("OBJ_210");
                pnlCommande.add(OBJ_210);
                OBJ_210.setBounds(245, 55, 30, 20);
                
                // ---- WCONFI ----
                WCONFI.setComponentPopupMenu(null);
                WCONFI.setName("WCONFI");
                pnlCommande.add(WCONFI);
                WCONFI.setBounds(110, 51, 20, WCONFI.getPreferredSize().height);
                
                // ---- WCONFU ----
                WCONFU.setComponentPopupMenu(null);
                WCONFU.setName("WCONFU");
                pnlCommande.add(WCONFU);
                WCONFU.setBounds(275, 51, 20, WCONFU.getPreferredSize().height);
                
                // ---- label1 ----
                label1.setText("Confirmation ");
                label1.setName("label1");
                pnlCommande.add(label1);
                label1.setBounds(20, 53, 95, 25);
                
                // ---- EADT1X ----
                EADT1X.setComponentPopupMenu(null);
                EADT1X.setToolTipText("Date de confirmation");
                EADT1X.setName("EADT1X");
                pnlCommande.add(EADT1X);
                EADT1X.setBounds(130, 51, 105, EADT1X.getPreferredSize().height);
                
                // ---- OBJ_139 ----
                OBJ_139.setText("Code sc\u00e9nario ");
                OBJ_139.setName("OBJ_139");
                pnlCommande.add(OBJ_139);
                OBJ_139.setBounds(20, 81, 95, 20);
                
                // ---- EAIN7 ----
                EAIN7.setComponentPopupMenu(null);
                EAIN7.setName("EAIN7");
                pnlCommande.add(EAIN7);
                EAIN7.setBounds(110, 77, 20, EAIN7.getPreferredSize().height);
                
                { // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for (int i = 0; i < pnlCommande.getComponentCount(); i++) {
                    Rectangle bounds = pnlCommande.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = pnlCommande.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  pnlCommande.setMinimumSize(preferredSize);
                  pnlCommande.setPreferredSize(preferredSize);
                }
              }
              panel1.add(pnlCommande);
              pnlCommande.setBounds(255, 10, 315, 115);
              
              // ---- OBJ_165 ----
              OBJ_165.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_165.setToolTipText("Modifier le taux");
              OBJ_165.setName("OBJ_165");
              OBJ_165.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_165ActionPerformed(e);
                }
              });
              panel1.add(OBJ_165);
              OBJ_165.setBounds(525, 283, 28, 28);
              
              // ---- label19 ----
              label19.setText("Fax");
              label19.setFont(label19.getFont().deriveFont(label19.getFont().getStyle() | Font.BOLD));
              label19.setName("label19");
              panel1.add(label19);
              label19.setBounds(0, 260, 35, 20);
              
              // ---- LFAX ----
              LFAX.setText("@LFAX@");
              LFAX.setName("LFAX");
              panel1.add(LFAX);
              LFAX.setBounds(70, 258, 130, LFAX.getPreferredSize().height);
              
              // ---- label20 ----
              label20.setText("Mail");
              label20.setFont(label20.getFont().deriveFont(label20.getFont().getStyle() | Font.BOLD));
              label20.setName("label20");
              panel1.add(label20);
              label20.setBounds(245, 260, 35, 20);
              
              // ---- LMAIL ----
              LMAIL.setText("@LMAIL@");
              LMAIL.setName("LMAIL");
              panel1.add(LMAIL);
              LMAIL.setBounds(283, 258, 237, LMAIL.getPreferredSize().height);
              
              // ---- riBoutonDetail9 ----
              riBoutonDetail9.setToolTipText("Choix des coordonn\u00e9es d'envoi");
              riBoutonDetail9.setName("riBoutonDetail9");
              riBoutonDetail9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riBoutonDetail9ActionPerformed(e);
                }
              });
              panel1.add(riBoutonDetail9);
              riBoutonDetail9.setBounds(525, 256, 28, 28);
              
              // ---- EARFL ----
              EARFL.setComponentPopupMenu(null);
              EARFL.setName("EARFL");
              panel1.add(EARFL);
              EARFL.setBounds(140, 125, 430, EARFL.getPreferredSize().height);
              
              // ---- lbReference2 ----
              lbReference2.setText("R\u00e9f\u00e9rence interne");
              lbReference2.setName("lbReference2");
              panel1.add(lbReference2);
              lbReference2.setBounds(0, 129, 145, 20);
              
              // ---- EAMAG ----
              EAMAG.setComponentPopupMenu(BTD);
              EAMAG.setName("EAMAG");
              panel1.add(EAMAG);
              EAMAG.setBounds(60, 189, 34, EAMAG.getPreferredSize().height);
              
              // ---- TOPMAG ----
              TOPMAG.setText("");
              TOPMAG.setToolTipText("Remplacement automatique du magasin sur les lignes existantes");
              TOPMAG.setComponentPopupMenu(BTD);
              TOPMAG.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TOPMAG.setName("TOPMAG");
              panel1.add(TOPMAG);
              TOPMAG.setBounds(100, 193, 25, 20);
              
              // ---- OBJ_147 ----
              OBJ_147.setText("Magasin");
              OBJ_147.setName("OBJ_147");
              panel1.add(OBJ_147);
              OBJ_147.setBounds(0, 193, 60, 20);
            }
            panel4.add(panel1);
            panel1.setBounds(365, -10, 575, 315);
            
            // ---- lbWIN11 ----
            lbWIN11.setText("Type de prix d'achat ");
            lbWIN11.setName("lbWIN11");
            panel4.add(lbWIN11);
            lbWIN11.setBounds(10, 278, 120, 20);
            
            // ---- WIN11 ----
            WIN11.setModel(new DefaultComboBoxModel(new String[] { "Prix d'achat net HT", "Prix achat brut HT (prix catalogue)",
                "Prix revient HT (prix d'achat net HT + port HT)" }));
            WIN11.setComponentPopupMenu(null);
            WIN11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WIN11.setName("WIN11");
            panel4.add(WIN11);
            WIN11.setBounds(125, 275, 340, WIN11.getPreferredSize().height);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout
              .setHorizontalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup().addContainerGap()
                  .addComponent(panel4, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addContainerGap()));
          p_contenuLayout.setVerticalGroup(
              p_contenuLayout.createParallelGroup().addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                  .addContainerGap().addComponent(panel4, GroupLayout.DEFAULT_SIZE, 587, Short.MAX_VALUE).addContainerGap()));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_17 ----
      OBJ_17.setText("Choix possibles");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
      
      // ======== panel2 ========
      {
        panel2.setName("panel2");
        panel2.setLayout(null);
        
        { // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < panel2.getComponentCount(); i++) {
            Rectangle bounds = panel2.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel2.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel2.setMinimumSize(preferredSize);
          panel2.setPreferredSize(preferredSize);
        }
      }
      BTD.add(panel2);
      
      // ---- OBJ_16 ----
      OBJ_16.setText("Aide en ligne");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);
    }
    
    // ---- MAG_doublon_147 ----
    MAG_doublon_147.setComponentPopupMenu(BTD);
    MAG_doublon_147.setName("MAG_doublon_147");
    
    // ---- OBJ_127 ----
    OBJ_127.setText("Cpt. provisoirement");
    OBJ_127.setName("OBJ_127");
    
    // ---- OBJ_138 ----
    OBJ_138.setText("@LIBRGR@");
    OBJ_138.setName("OBJ_138");
    
    // ---- OBJ_141 ----
    OBJ_141.setText("@LIBRGR@");
    OBJ_141.setName("OBJ_141");
    
    // ---- OBJ_118 ----
    OBJ_118.setText("Adresse facturation");
    OBJ_118.setName("OBJ_118");
    
    // ---- ACH_doublon_127 ----
    ACH_doublon_127.setComponentPopupMenu(BTD);
    ACH_doublon_127.setName("ACH_doublon_127");
    
    // ---- MALIBR ----
    MALIBR.setText("@MALIBR@");
    MALIBR.setName("MALIBR");
    
    // ---- TIT ----
    TIT.setText("@TIT@");
    TIT.setName("TIT");
    
    // ---- OBS2 ----
    OBS2.setComponentPopupMenu(BTD);
    OBS2.setName("OBS2");
    
    // ---- TIT2 ----
    TIT2.setText("@TFLIB@");
    TIT2.setName("TIT2");
    
    // ---- FRSDIV_doublon_23 ----
    FRSDIV_doublon_23.setComponentPopupMenu(BTD);
    FRSDIV_doublon_23.setName("FRSDIV_doublon_23");
    
    // ---- OBJ_174 ----
    OBJ_174.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_174.setToolTipText("Remises");
    OBJ_174.setName("OBJ_174");
    OBJ_174.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_174ActionPerformed(e);
      }
    });
    
    // ---- OBJ_191 ----
    OBJ_191.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_191.setToolTipText("R\u00e8glement");
    OBJ_191.setName("OBJ_191");
    OBJ_191.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_191ActionPerformed(e);
      }
    });
    
    // ---- OBJ_203 ----
    OBJ_203.setText("Emballage");
    OBJ_203.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_203.setName("OBJ_203");
    OBJ_203.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_203ActionPerformed(e);
      }
    });
    
    // ======== riSousMenu15 ========
    {
      riSousMenu15.setName("riSousMenu15");
      
      // ---- riSousMenu_bt15 ----
      riSousMenu_bt15.setText("fin de bon");
      riSousMenu_bt15.setName("riSousMenu_bt15");
      riSousMenu_bt15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt15ActionPerformed(e);
        }
      });
      riSousMenu15.add(riSousMenu_bt15);
    }
    
    // ======== riSousMenu8 ========
    {
      riSousMenu8.setName("riSousMenu8");
      
      // ---- riSousMenu_bt8 ----
      riSousMenu_bt8.setText("Saisie codes barres");
      riSousMenu_bt8.setToolTipText("Mise en/hors fonction de la saisie code barre");
      riSousMenu_bt8.setName("riSousMenu_bt8");
      riSousMenu_bt8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt8ActionPerformed(e);
        }
      });
      riSousMenu8.add(riSousMenu_bt8);
    }
    
    // ---- OBJ_201 ----
    OBJ_201.setText("Acheminement");
    OBJ_201.setName("OBJ_201");
    
    // ---- EAETAI ----
    EAETAI.setComponentPopupMenu(BTD);
    EAETAI.setName("EAETAI");
    
    // ---- riBoutonDetail10 ----
    riBoutonDetail10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    riBoutonDetail10.setToolTipText("Dossier");
    riBoutonDetail10.setName("riBoutonDetail10");
    riBoutonDetail10.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_164ActionPerformed(e);
      }
    });
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_52;
  private XRiTextField WNUM;
  private XRiTextField EAETB;
  private JLabel OBJ_51;
  private XRiTextField WSUF;
  private JLabel OBJ_114;
  private XRiTextField EACJA;
  private JLabel OBJ_48;
  private XRiTextField EAACH;
  private JLabel TitreLIBSTU;
  private JLabel LIBSTU;
  private JPanel p_tete_droite;
  private JLabel label_etat;
  private JLabel WDATEX;
  private JLabel OBJ_50;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiMenu riMenu4;
  private RiMenu_bt riMenu_bt4;
  private RiSousMenu riSousMenu18;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel4;
  private JPanel OBJ_27;
  private XRiTextField E2NOM;
  private XRiTextField E2CPL;
  private XRiTextField E2RUE;
  private XRiTextField E2LOC;
  private XRiTextField E2PAY;
  private XRiTextField E2VILR;
  private XRiTextField E2TEL;
  private XRiTextField E2FAX;
  private XRiTextField EAFRS;
  private XRiTextField E2CDPX;
  private XRiTextField EACOL;
  private XRiTextField WEAEXC;
  private JLabel OBJ_117;
  private JLabel label3;
  private JButton btnBlocNotes;
  private JLabel label5;
  private JXTitledPanel xTitledPanel1;
  private XRiTextField E20NAT;
  private JLabel OBJ_123;
  private JLabel OBJ_140;
  private XRiTextField EASAN;
  private XRiTextField EAACT;
  private JLabel OBJ_137;
  private XRiCheckBox TOPSAN;
  private XRiCheckBox TOPAFF;
  private JXTitledPanel xTitledPanel2;
  private XRiComboBox EAINA;
  private XRiTextField EAMTAX;
  private JLabel OBJ_170;
  private JLabel OBJ_116;
  private XRiTextField EACPRX;
  private RiZoneSortie OBJ_109;
  private JLabel OBJ_115;
  private RiZoneSortie DGDEV;
  private JLabel OBJ_119;
  private XRiCheckBox EAIN10;
  private JXTitledPanel xTitledPanel4;
  private JLabel OBJ_178;
  private XRiTextField EAREM1;
  private XRiTextField EAREM2;
  private XRiTextField EAREM3;
  private JLabel OBJ_179;
  private XRiTextField EATP1;
  private JLabel OBJ_181;
  private XRiTextField EATP2;
  private JLabel OBJ_183;
  private XRiTextField EATP3;
  private JLabel OBJ_185;
  private XRiTextField EATP4;
  private JLabel OBJ_187;
  private XRiTextField EATP5;
  private JXTitledPanel xTitledPanel5;
  private XRiTextField LRG2;
  private JLabel OBJ_195;
  private JLabel OBJ_193;
  private XRiTextField EAESCX;
  private XRiTextField EARG2;
  private JXTitledPanel xTitledPanel3;
  private XRiTextField TRLIBR;
  private XRiTextField EXLIBR;
  private XRiTextField EACNT;
  private XRiTextField EADOS;
  private JLabel OBJ_166;
  private XRiTextField EAPDS;
  private XRiTextField EAVOL;
  private JLabel OBJ_199;
  private XRiCalendrier EAIT1X;
  private JLabel OBJ_200;
  private JLabel OBJ_169;
  private JLabel OBJ_161;
  private XRiTextField EAMEX;
  private XRiTextField EACTR;
  private JLabel label2;
  private SNLabelTitre LIBDT1;
  private XRiCalendrier EAIT2X;
  private SNLabelTitre LIBDT2;
  private JPanel panel1;
  private XRiTextField WNOMFR;
  private XRiTextField LVLB1;
  private XRiTextField LVLB2;
  private XRiTextField LVLB3;
  private JLabel OBJ_124;
  private JLabel lbReference;
  private JLabel OBJ_121;
  private JLabel OBJ_151;
  private XRiTextField EACCT;
  private XRiTextField EARBC;
  private XRiTextField EARBL;
  private XRiTextField EALLV;
  private JLabel OBJ_146;
  private JLabel label4;
  private RiZoneSortie OBJ_47;
  private RiZoneSortie riZoneSortie1;
  private JPanel pnlFacture;
  private XRiCalendrier EADLPX2;
  private XRiCheckBox TOPDLP2;
  private JLabel OBJ_213;
  private XRiTextField WCONFI2;
  private XRiTextField WCONFU2;
  private JLabel label6;
  private XRiCalendrier EADT1X2;
  private JLabel label7;
  private XRiCheckBox EADILI;
  private JPanel pnlCommande;
  private XRiCalendrier EADLPX;
  private XRiCheckBox TOPDLP;
  private XRiTextField WCONFH;
  private XRiTextField WCONFM;
  private JLabel OBJ_207;
  private JLabel OBJ_206;
  private JLabel OBJ_208;
  private JLabel OBJ_210;
  private XRiTextField WCONFI;
  private XRiTextField WCONFU;
  private JLabel label1;
  private XRiCalendrier EADT1X;
  private JLabel OBJ_139;
  private XRiTextField EAIN7;
  private SNBoutonDetail OBJ_165;
  private JLabel label19;
  private RiZoneSortie LFAX;
  private JLabel label20;
  private RiZoneSortie LMAIL;
  private SNBoutonDetail riBoutonDetail9;
  private XRiTextField EARFL;
  private JLabel lbReference2;
  private XRiTextField EAMAG;
  private XRiCheckBox TOPMAG;
  private JLabel OBJ_147;
  private JLabel lbWIN11;
  private XRiComboBox WIN11;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JPanel panel2;
  private JMenuItem OBJ_16;
  private XRiTextField MAG_doublon_147;
  private JLabel OBJ_127;
  private JLabel OBJ_138;
  private JLabel OBJ_141;
  private JLabel OBJ_118;
  private XRiTextField ACH_doublon_127;
  private JLabel MALIBR;
  private JLabel TIT;
  private XRiTextField OBS2;
  private JLabel TIT2;
  private XRiTextField FRSDIV_doublon_23;
  private SNBoutonDetail OBJ_174;
  private SNBoutonDetail OBJ_191;
  private JButton OBJ_203;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private JLabel OBJ_201;
  private XRiTextField EAETAI;
  private SNBoutonDetail riBoutonDetail10;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
