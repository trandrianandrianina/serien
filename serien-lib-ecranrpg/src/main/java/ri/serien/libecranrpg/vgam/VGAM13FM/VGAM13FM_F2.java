
package ri.serien.libecranrpg.vgam.VGAM13FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;

/**
 * @author Emmanuel MARCQ
 */
public class VGAM13FM_F2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGAM13FM_F2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    W57BD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W57BD@")).trim());
    PG57BD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PG57BD@")).trim());
    PG57BF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PG57BF@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    panel1.setVisible(lexique.isTrue("(N41) AND (42)"));
    panel2.setVisible(lexique.isTrue("(41) AND (N42)"));
    panel3.setVisible(lexique.isTrue("(41) AND (42)"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Génération bons d'achats"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    label1 = new JLabel();
    panel2 = new JPanel();
    label2 = new JLabel();
    W57BD = new RiZoneSortie();
    panel3 = new JPanel();
    label3 = new JLabel();
    PG57BD = new RiZoneSortie();
    PG57BF = new RiZoneSortie();
    label4 = new JLabel();
    label5 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 500));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- label1 ----
          label1.setText("G\u00e9n\u00e9ration de bons d'achats en cours...");
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD, label1.getFont().getSize() + 3f));
          label1.setHorizontalAlignment(SwingConstants.CENTER);
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(5, 75, 450, 40);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 465, 200);

        //======== panel2 ========
        {
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- label2 ----
          label2.setText("Bon de commande d'achats g\u00e9n\u00e9r\u00e9 num\u00e9ro");
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD, label2.getFont().getSize() + 3f));
          label2.setHorizontalAlignment(SwingConstants.CENTER);
          label2.setName("label2");
          panel2.add(label2);
          label2.setBounds(5, 40, 450, 40);

          //---- W57BD ----
          W57BD.setText("@W57BD@");
          W57BD.setName("W57BD");
          panel2.add(W57BD);
          W57BD.setBounds(180, 110, 90, 30);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel2);
        panel2.setBounds(10, 10, 465, 200);

        //======== panel3 ========
        {
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);

          //---- label3 ----
          label3.setText("Bons de commande d'achats g\u00e9n\u00e9r\u00e9s");
          label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD, label3.getFont().getSize() + 3f));
          label3.setHorizontalAlignment(SwingConstants.CENTER);
          label3.setName("label3");
          panel3.add(label3);
          label3.setBounds(5, 40, 450, 40);

          //---- PG57BD ----
          PG57BD.setText("@PG57BD@");
          PG57BD.setName("PG57BD");
          panel3.add(PG57BD);
          PG57BD.setBounds(140, 110, 90, 30);

          //---- PG57BF ----
          PG57BF.setText("@PG57BF@");
          PG57BF.setName("PG57BF");
          panel3.add(PG57BF);
          PG57BF.setBounds(270, 110, 90, 30);

          //---- label4 ----
          label4.setText("du");
          label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD, label4.getFont().getSize() + 3f));
          label4.setHorizontalAlignment(SwingConstants.CENTER);
          label4.setName("label4");
          panel3.add(label4);
          label4.setBounds(105, 105, 30, 40);

          //---- label5 ----
          label5.setText("au");
          label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD, label5.getFont().getSize() + 3f));
          label5.setHorizontalAlignment(SwingConstants.CENTER);
          label5.setName("label5");
          panel3.add(label5);
          label5.setBounds(235, 105, 30, 40);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel3.getComponentCount(); i++) {
              Rectangle bounds = panel3.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel3.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel3.setMinimumSize(preferredSize);
            panel3.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel3);
        panel3.setBounds(10, 10, 465, 200);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel label1;
  private JPanel panel2;
  private JLabel label2;
  private RiZoneSortie W57BD;
  private JPanel panel3;
  private JLabel label3;
  private RiZoneSortie PG57BD;
  private RiZoneSortie PG57BF;
  private JLabel label4;
  private JLabel label5;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
