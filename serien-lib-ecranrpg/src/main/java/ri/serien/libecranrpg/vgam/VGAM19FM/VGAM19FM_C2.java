/*
 * Created by JFormDesigner on Tue Feb 25 17:23:25 CET 2020
 */

package ri.serien.libecranrpg.vgam.VGAM19FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.achat.documentachat.snacheteur.SNAcheteur;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Ecran d'affichage ou de modification de ligne de demande d'achat
 * [GAM2192] Gestion des achats -> Documents d'achats -> Commandes et réapprovisionnements -> Gestion des demandes d'achats ->
 * Centralisation de demandes d'achats
 * Affichage ou modification
 */
public class VGAM19FM_C2 extends SNPanelEcranRPG implements ioFrame {
  private String[] DCDPR_Value = { "0", "1", "2", "3", };
  private String[] DCIN1_Value = { "0", "1", "2", "3", };
  
  public VGAM19FM_C2(ArrayList param) {
    super(param);
    initComponents();
    setVersionLook(2);
    initDiverses();
    setDialog(true);
    
    DCDPR.setValeurs(DCDPR_Value, null);
    DCIN1.setValeurs(DCIN1_Value, null);
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("P19ETB"));
    
    // Charge l'article
    snArticle.setSession(getSession());
    snArticle.setIdEtablissement(idEtablissement);
    snArticle.charger(false);
    snArticle.setSelectionParChampRPG(lexique, "LAART");
    
    // Charge l'acheteur
    snAcheteur.setSession(getSession());
    snAcheteur.setIdEtablissement(idEtablissement);
    snAcheteur.setTousAutorise(true);
    snAcheteur.charger(false);
    snAcheteur.setSelectionParChampRPG(lexique, "EAACH");
    
    // Charge le fournisseur principal
    snFournisseurPrincipal.setSession(getSession());
    snFournisseurPrincipal.setIdEtablissement(idEtablissement);
    snFournisseurPrincipal.charger(false);
    snFournisseurPrincipal.setSelectionParChampRPG(lexique, "DCCOL", "DCFRS");
    
    // Charge le 1er fournisseur
    snFournisseurUn.setSession(getSession());
    snFournisseurUn.setIdEtablissement(idEtablissement);
    snFournisseurUn.charger(false);
    snFournisseurUn.setSelectionParChampRPG(lexique, "DCCOL2", "DCFRS2");
    
    // Charge le 2ème fournisseur
    snFournisseurDeux.setSession(getSession());
    snFournisseurDeux.setIdEtablissement(idEtablissement);
    snFournisseurDeux.charger(false);
    snFournisseurDeux.setSelectionParChampRPG(lexique, "DCCOL3", "DCFRS3");
    
    // Charge le 3ème fournisseur
    snFournisseurTrois.setSession(getSession());
    snFournisseurTrois.setIdEtablissement(idEtablissement);
    snFournisseurTrois.charger(false);
    snFournisseurTrois.setSelectionParChampRPG(lexique, "DCCOL4", "DCFRS4");
    
    // Charge le 4ème fournisseur
    snFournisseurQuatre.setSession(getSession());
    snFournisseurQuatre.setIdEtablissement(idEtablissement);
    snFournisseurQuatre.charger(false);
    snFournisseurQuatre.setSelectionParChampRPG(lexique, "DCCOL5", "DCFRS5");
    
    // Gére le mode modification et le mode consultation
    boolean isConsultation = lexique.getMode() == Lexical.MODE_CONSULTATION;
    boolean isModification = lexique.getMode() == Lexical.MODE_MODIFICATION;
    if (isModification) {
      pnlGeneral.setTitre("Modification de lignes de demandes d'achats");
    }
    else if (isConsultation) {
      pnlGeneral.setTitre("Affichage de lignes de demandes d'achats");
      snFournisseurUn.setEnabled(false);
      snFournisseurDeux.setEnabled(false);
      snFournisseurTrois.setEnabled(false);
      snFournisseurQuatre.setEnabled(false);
    }
  }
  
  @Override
  public void getData() {
    super.getData();
    snArticle.renseignerChampRPG(lexique, "LAART");
    snAcheteur.renseignerChampRPG(lexique, "EAACH");
    snFournisseurPrincipal.renseignerChampRPG(lexique, "DCCOL", "DCFRS");
    snFournisseurUn.renseignerChampRPG(lexique, "DCCOL2", "DCFRS2");
    snFournisseurDeux.renseignerChampRPG(lexique, "DCCOL3", "DCFRS3");
    snFournisseurTrois.renseignerChampRPG(lexique, "DCCOL4", "DCFRS4");
    snFournisseurQuatre.renseignerChampRPG(lexique, "DCCOL5", "DCFRS5");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlContenu = new SNPanelContenu();
    pnlGeneral = new SNPanelTitre();
    lbArticle = new SNLabelChamp();
    snArticle = new SNArticle();
    NumeroDemande = new SNLabelChamp();
    DCNUM = new XRiTextField();
    lbDemandeur = new SNLabelChamp();
    snAcheteur = new SNAcheteur();
    lbDate = new SNLabelChamp();
    EADATX = new XRiCalendrier();
    lbFournisseurPrincipal = new SNLabelChamp();
    snFournisseurPrincipal = new SNFournisseur();
    lbDateLivraison = new SNLabelChamp();
    DCDLPX = new XRiCalendrier();
    lbQuantiteDemande = new SNLabelChamp();
    DCQTAX = new XRiTextField();
    lbQuantiteCommande = new SNLabelChamp();
    LAQTAX = new XRiTextField();
    lbPrixAchat = new SNLabelChamp();
    CAPRAX = new XRiTextField();
    lbPrixNegocie = new SNLabelChamp();
    LAPANX = new XRiTextField();
    lbDemandePrix = new SNLabelChamp();
    DCDPR = new XRiComboBox();
    lbFournisseur = new SNLabelChamp();
    snFournisseurUn = new SNFournisseur();
    snFournisseurDeux = new SNFournisseur();
    snFournisseurTrois = new SNFournisseur();
    snFournisseurQuatre = new SNFournisseur();
    lbGenerationCommande = new SNLabelChamp();
    DCIN1 = new XRiComboBox();
    lbBonCommande = new SNLabelChamp();
    pnlBonCommande = new SNPanel();
    DCNUM1 = new XRiTextField();
    DCSUF1 = new XRiTextField();
    WHOMX = new XRiTextField();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setPreferredSize(new Dimension(750, 725));
    setMinimumSize(new Dimension(750, 725));
    setMaximumSize(new Dimension(750, 725));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

      //======== pnlGeneral ========
      {
        pnlGeneral.setTitre("Titre dans le code");
        pnlGeneral.setName("pnlGeneral");
        pnlGeneral.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlGeneral.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlGeneral.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        ((GridBagLayout)pnlGeneral.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
        ((GridBagLayout)pnlGeneral.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

        //---- lbArticle ----
        lbArticle.setText("Article");
        lbArticle.setName("lbArticle");
        pnlGeneral.add(lbArticle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snArticle ----
        snArticle.setFont(new Font("sansserif", Font.PLAIN, 14));
        snArticle.setEnabled(false);
        snArticle.setName("snArticle");
        pnlGeneral.add(snArticle, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- NumeroDemande ----
        NumeroDemande.setText("Num\u00e9ro de demande");
        NumeroDemande.setName("NumeroDemande");
        pnlGeneral.add(NumeroDemande, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- DCNUM ----
        DCNUM.setPreferredSize(new Dimension(100, 30));
        DCNUM.setMinimumSize(new Dimension(100, 30));
        DCNUM.setMaximumSize(new Dimension(100, 30));
        DCNUM.setFont(new Font("sansserif", Font.PLAIN, 14));
        DCNUM.setName("DCNUM");
        pnlGeneral.add(DCNUM, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbDemandeur ----
        lbDemandeur.setText("Demandeur");
        lbDemandeur.setName("lbDemandeur");
        pnlGeneral.add(lbDemandeur, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snAcheteur ----
        snAcheteur.setFont(new Font("sansserif", Font.PLAIN, 14));
        snAcheteur.setEnabled(false);
        snAcheteur.setName("snAcheteur");
        pnlGeneral.add(snAcheteur, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbDate ----
        lbDate.setText("Date");
        lbDate.setName("lbDate");
        pnlGeneral.add(lbDate, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- EADATX ----
        EADATX.setPreferredSize(new Dimension(110, 30));
        EADATX.setMinimumSize(new Dimension(110, 30));
        EADATX.setMaximumSize(new Dimension(110, 30));
        EADATX.setFont(new Font("sansserif", Font.PLAIN, 14));
        EADATX.setName("EADATX");
        pnlGeneral.add(EADATX, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbFournisseurPrincipal ----
        lbFournisseurPrincipal.setText("Fournisseur principal");
        lbFournisseurPrincipal.setName("lbFournisseurPrincipal");
        pnlGeneral.add(lbFournisseurPrincipal, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snFournisseurPrincipal ----
        snFournisseurPrincipal.setFont(new Font("sansserif", Font.PLAIN, 14));
        snFournisseurPrincipal.setEnabled(false);
        snFournisseurPrincipal.setName("snFournisseurPrincipal");
        pnlGeneral.add(snFournisseurPrincipal, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbDateLivraison ----
        lbDateLivraison.setText("Date de livraison");
        lbDateLivraison.setName("lbDateLivraison");
        pnlGeneral.add(lbDateLivraison, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- DCDLPX ----
        DCDLPX.setPreferredSize(new Dimension(110, 30));
        DCDLPX.setMinimumSize(new Dimension(110, 30));
        DCDLPX.setMaximumSize(new Dimension(110, 30));
        DCDLPX.setFont(new Font("sansserif", Font.PLAIN, 14));
        DCDLPX.setName("DCDLPX");
        pnlGeneral.add(DCDLPX, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbQuantiteDemande ----
        lbQuantiteDemande.setText("Quantit\u00e9 demand\u00e9e en UA");
        lbQuantiteDemande.setPreferredSize(new Dimension(169, 30));
        lbQuantiteDemande.setMinimumSize(new Dimension(169, 30));
        lbQuantiteDemande.setMaximumSize(new Dimension(169, 30));
        lbQuantiteDemande.setName("lbQuantiteDemande");
        pnlGeneral.add(lbQuantiteDemande, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- DCQTAX ----
        DCQTAX.setFont(new Font("sansserif", Font.PLAIN, 14));
        DCQTAX.setMinimumSize(new Dimension(100, 30));
        DCQTAX.setPreferredSize(new Dimension(100, 30));
        DCQTAX.setMaximumSize(new Dimension(100, 30));
        DCQTAX.setName("DCQTAX");
        pnlGeneral.add(DCQTAX, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbQuantiteCommande ----
        lbQuantiteCommande.setText("Quantit\u00e9 comand\u00e9e en UA");
        lbQuantiteCommande.setName("lbQuantiteCommande");
        pnlGeneral.add(lbQuantiteCommande, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- LAQTAX ----
        LAQTAX.setFont(new Font("sansserif", Font.PLAIN, 14));
        LAQTAX.setMinimumSize(new Dimension(100, 30));
        LAQTAX.setMaximumSize(new Dimension(100, 30));
        LAQTAX.setPreferredSize(new Dimension(100, 30));
        LAQTAX.setName("LAQTAX");
        pnlGeneral.add(LAQTAX, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbPrixAchat ----
        lbPrixAchat.setText("Prix d'achat en UA");
        lbPrixAchat.setName("lbPrixAchat");
        pnlGeneral.add(lbPrixAchat, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- CAPRAX ----
        CAPRAX.setFont(new Font("sansserif", Font.PLAIN, 14));
        CAPRAX.setPreferredSize(new Dimension(125, 30));
        CAPRAX.setMinimumSize(new Dimension(125, 30));
        CAPRAX.setMaximumSize(new Dimension(125, 30));
        CAPRAX.setName("CAPRAX");
        pnlGeneral.add(CAPRAX, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbPrixNegocie ----
        lbPrixNegocie.setText("Prix n\u00e9goci\u00e9 en UA");
        lbPrixNegocie.setName("lbPrixNegocie");
        pnlGeneral.add(lbPrixNegocie, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- LAPANX ----
        LAPANX.setFont(new Font("sansserif", Font.PLAIN, 14));
        LAPANX.setPreferredSize(new Dimension(125, 30));
        LAPANX.setMinimumSize(new Dimension(125, 30));
        LAPANX.setMaximumSize(new Dimension(125, 30));
        LAPANX.setName("LAPANX");
        pnlGeneral.add(LAPANX, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbDemandePrix ----
        lbDemandePrix.setText("Demande de prix");
        lbDemandePrix.setName("lbDemandePrix");
        pnlGeneral.add(lbDemandePrix, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- DCDPR ----
        DCDPR.setModel(new DefaultComboBoxModel(new String[] {
          "Pas de demande",
          "Demande \u00e0 faire",
          "Demande effectu\u00e9e",
          "Condition mise \u00e0 jour"
        }));
        DCDPR.setPreferredSize(new Dimension(275, 30));
        DCDPR.setMinimumSize(new Dimension(275, 30));
        DCDPR.setMaximumSize(new Dimension(275, 30));
        DCDPR.setFont(new Font("sansserif", Font.PLAIN, 14));
        DCDPR.setName("DCDPR");
        pnlGeneral.add(DCDPR, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbFournisseur ----
        lbFournisseur.setText("Fournisseur :");
        lbFournisseur.setName("lbFournisseur");
        pnlGeneral.add(lbFournisseur, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snFournisseurUn ----
        snFournisseurUn.setFont(new Font("sansserif", Font.PLAIN, 14));
        snFournisseurUn.setName("snFournisseurUn");
        pnlGeneral.add(snFournisseurUn, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- snFournisseurDeux ----
        snFournisseurDeux.setFont(new Font("sansserif", Font.PLAIN, 14));
        snFournisseurDeux.setName("snFournisseurDeux");
        pnlGeneral.add(snFournisseurDeux, new GridBagConstraints(1, 12, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- snFournisseurTrois ----
        snFournisseurTrois.setFont(new Font("sansserif", Font.PLAIN, 14));
        snFournisseurTrois.setName("snFournisseurTrois");
        pnlGeneral.add(snFournisseurTrois, new GridBagConstraints(1, 13, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- snFournisseurQuatre ----
        snFournisseurQuatre.setFont(new Font("sansserif", Font.PLAIN, 14));
        snFournisseurQuatre.setName("snFournisseurQuatre");
        pnlGeneral.add(snFournisseurQuatre, new GridBagConstraints(1, 14, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbGenerationCommande ----
        lbGenerationCommande.setText("G\u00e9n\u00e9ration d'une commande");
        lbGenerationCommande.setPreferredSize(new Dimension(185, 30));
        lbGenerationCommande.setMinimumSize(new Dimension(185, 30));
        lbGenerationCommande.setMaximumSize(new Dimension(185, 30));
        lbGenerationCommande.setName("lbGenerationCommande");
        pnlGeneral.add(lbGenerationCommande, new GridBagConstraints(0, 15, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- DCIN1 ----
        DCIN1.setModel(new DefaultComboBoxModel(new String[] {
          "Attente",
          "A g\u00e9n\u00e9rer",
          "G\u00e9n\u00e9r\u00e9",
          "Ne pas g\u00e9n\u00e9rer"
        }));
        DCIN1.setFont(new Font("sansserif", Font.PLAIN, 14));
        DCIN1.setPreferredSize(new Dimension(175, 30));
        DCIN1.setMinimumSize(new Dimension(175, 30));
        DCIN1.setMaximumSize(new Dimension(175, 30));
        DCIN1.setName("DCIN1");
        pnlGeneral.add(DCIN1, new GridBagConstraints(1, 15, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbBonCommande ----
        lbBonCommande.setText("Bon de commande");
        lbBonCommande.setName("lbBonCommande");
        pnlGeneral.add(lbBonCommande, new GridBagConstraints(0, 16, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //======== pnlBonCommande ========
        {
          pnlBonCommande.setName("pnlBonCommande");
          pnlBonCommande.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlBonCommande.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
          ((GridBagLayout)pnlBonCommande.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlBonCommande.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlBonCommande.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- DCNUM1 ----
          DCNUM1.setFont(new Font("sansserif", Font.PLAIN, 14));
          DCNUM1.setPreferredSize(new Dimension(75, 30));
          DCNUM1.setMinimumSize(new Dimension(75, 30));
          DCNUM1.setMaximumSize(new Dimension(75, 30));
          DCNUM1.setName("DCNUM1");
          pnlBonCommande.add(DCNUM1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- DCSUF1 ----
          DCSUF1.setFont(new Font("sansserif", Font.PLAIN, 14));
          DCSUF1.setMaximumSize(new Dimension(25, 30));
          DCSUF1.setMinimumSize(new Dimension(25, 30));
          DCSUF1.setPreferredSize(new Dimension(25, 30));
          DCSUF1.setName("DCSUF1");
          pnlBonCommande.add(DCSUF1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- WHOMX ----
          WHOMX.setEnabled(false);
          WHOMX.setFont(new Font("sansserif", Font.PLAIN, 14));
          WHOMX.setPreferredSize(new Dimension(100, 30));
          WHOMX.setMinimumSize(new Dimension(100, 30));
          WHOMX.setMaximumSize(new Dimension(100, 30));
          WHOMX.setName("WHOMX");
          pnlBonCommande.add(WHOMX, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGeneral.add(pnlBonCommande, new GridBagConstraints(1, 16, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlGeneral, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlGeneral;
  private SNLabelChamp lbArticle;
  private SNArticle snArticle;
  private SNLabelChamp NumeroDemande;
  private XRiTextField DCNUM;
  private SNLabelChamp lbDemandeur;
  private SNAcheteur snAcheteur;
  private SNLabelChamp lbDate;
  private XRiCalendrier EADATX;
  private SNLabelChamp lbFournisseurPrincipal;
  private SNFournisseur snFournisseurPrincipal;
  private SNLabelChamp lbDateLivraison;
  private XRiCalendrier DCDLPX;
  private SNLabelChamp lbQuantiteDemande;
  private XRiTextField DCQTAX;
  private SNLabelChamp lbQuantiteCommande;
  private XRiTextField LAQTAX;
  private SNLabelChamp lbPrixAchat;
  private XRiTextField CAPRAX;
  private SNLabelChamp lbPrixNegocie;
  private XRiTextField LAPANX;
  private SNLabelChamp lbDemandePrix;
  private XRiComboBox DCDPR;
  private SNLabelChamp lbFournisseur;
  private SNFournisseur snFournisseurUn;
  private SNFournisseur snFournisseurDeux;
  private SNFournisseur snFournisseurTrois;
  private SNFournisseur snFournisseurQuatre;
  private SNLabelChamp lbGenerationCommande;
  private XRiComboBox DCIN1;
  private SNLabelChamp lbBonCommande;
  private SNPanel pnlBonCommande;
  private XRiTextField DCNUM1;
  private XRiTextField DCSUF1;
  private XRiTextField WHOMX;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
