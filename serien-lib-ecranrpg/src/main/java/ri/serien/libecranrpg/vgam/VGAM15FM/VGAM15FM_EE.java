
package ri.serien.libecranrpg.vgam.VGAM15FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.sectionanalytique.idSectionAnalytique;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.comptabilite.snsectionanalytique.SNSectionAnalytique;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGAM15FM_EE extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  private String[] _Q01_Title = { "WTIT118", "Quantité extraite", };
  private String[][] _Q01_Data = { { "LE01", }, { "LE02", }, { "LE03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _Q01_Width = { 600, 170 };
  private int[] _Q01_Justification = { SwingConstants.LEFT, SwingConstants.RIGHT };
  private static final String BOUTON_AUTRE_VUE = "Autres vues";
  private static final String BOUTON_VALIDATION_GLOBALE = "Validation globale";
  
  /**
   * Constructeur.
   */
  public VGAM15FM_EE(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    
    // Boutons
    snBarreBouton.ajouterBouton(BOUTON_AUTRE_VUE, 'v', true);
    snBarreBouton.ajouterBouton(BOUTON_VALIDATION_GLOBALE, 'g', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYP15@")).trim());
    lbTitre1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTIT118@")).trim());
    lbLigne1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LE01@")).trim());
    lbLigne2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LE02@")).trim());
    lbLigne3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LE03@")).trim());
    lbLigne4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LE04@")).trim());
    lbLigne5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LE05@")).trim());
    lbLigne6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LE06@")).trim());
    lbLigne7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LE07@")).trim());
    lbLigne8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LE08@")).trim());
    lbLigne9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LE09@")).trim());
    lbLigne10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LE10@")).trim());
    lbLigne11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LE11@")).trim());
    lbLigne12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LE12@")).trim());
    lbLigne13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LE13@")).trim());
    lbLigne14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LE14@")).trim());
    lbLigne15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LE15@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    WDATEX.setVisible(lexique.isPresent("WDATEX"));
    
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    IdEtablissement idEtablissement = IdEtablissement.getInstance(Constantes.normerTexte(lexique.HostFieldGetData("EAETB")));
    snEtablissement.setIdSelection(idEtablissement);
    snEtablissement.setEnabled(false);
    
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(idEtablissement);
    snMagasin.charger(false);
    String codeMagasin = Constantes.normerTexte(lexique.HostFieldGetData("WMARAM"));
    if (!codeMagasin.isEmpty()) {
      snMagasin.setIdSelection(IdMagasin.getInstance(idEtablissement, codeMagasin));
    }
    
    if (!lexique.HostFieldGetData("EASAN").trim().isEmpty()) {
      lbSection.setVisible(true);
      snSectionAnalytique.setVisible(true);
      snSectionAnalytique.setSession(getSession());
      snSectionAnalytique.setIdEtablissement(idEtablissement);
      snSectionAnalytique.charger(false);
      snSectionAnalytique
          .setIdSelection(idSectionAnalytique.getInstance(idEtablissement, Constantes.normerTexte(lexique.HostFieldGetData("EASAN"))));
      snSectionAnalytique.setEnabled(false);
    }
    else {
      lbSection.setVisible(false);
      snSectionAnalytique.setVisible(false);
    }
    
    p_bpresentation.setCodeEtablissement(snEtablissement.getCodeSelection());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snMagasin.renseignerChampRPG(lexique, "WMARAM");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_AUTRE_VUE)) {
        lexique.HostScreenSendKey(this, "F24");
      }
      else if (pSNBouton.isBouton(BOUTON_VALIDATION_GLOBALE)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    snBarreBouton = new SNBarreBouton();
    sNPanelContenu1 = new SNPanelContenu();
    pnlEtablissement = new SNPanelTitre();
    lbNumero = new SNLabelChamp();
    sNPanel4 = new SNPanel();
    WNUM = new XRiTextField();
    WSUF = new XRiTextField();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbFournisseur = new SNLabelChamp();
    E2NOM = new XRiTextField();
    lbDate = new SNLabelChamp();
    WDATEX = new XRiCalendrier();
    lbSection = new SNLabelChamp();
    snSectionAnalytique = new SNSectionAnalytique();
    pnlLignes = new SNPanelTitre();
    pnlListe = new SNPanel();
    pnlPresenttion = new SNPanel();
    lbTitre1 = new JLabel();
    lbTitre2 = new JLabel();
    lbLigne1 = new JLabel();
    Q01 = new XRiTextField();
    lbLigne2 = new JLabel();
    Q02 = new XRiTextField();
    lbLigne3 = new JLabel();
    Q03 = new XRiTextField();
    lbLigne4 = new JLabel();
    Q04 = new XRiTextField();
    lbLigne5 = new JLabel();
    Q05 = new XRiTextField();
    lbLigne6 = new JLabel();
    Q06 = new XRiTextField();
    lbLigne7 = new JLabel();
    Q07 = new XRiTextField();
    lbLigne8 = new JLabel();
    Q08 = new XRiTextField();
    lbLigne9 = new JLabel();
    Q09 = new XRiTextField();
    lbLigne10 = new JLabel();
    Q10 = new XRiTextField();
    lbLigne11 = new JLabel();
    Q11 = new XRiTextField();
    lbLigne12 = new JLabel();
    Q12 = new XRiTextField();
    lbLigne13 = new JLabel();
    lbLigne14 = new JLabel();
    Q14 = new XRiTextField();
    lbLigne15 = new JLabel();
    pnlDown = new SNPanel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    sNLabelChamp5 = new SNLabelChamp();
    Q13 = new XRiTextField();
    Q15 = new XRiTextField();
    sNPanel5 = new SNPanel();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    lbWRBL = new SNLabelChamp();
    WRBL = new XRiTextField();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1000, 650));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("@TYP15@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== sNPanelContenu1 ========
    {
      sNPanelContenu1.setName("sNPanelContenu1");
      sNPanelContenu1.setLayout(new GridBagLayout());
      ((GridBagLayout) sNPanelContenu1.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) sNPanelContenu1.getLayout()).rowHeights = new int[] { 0, 0, 0 };
      ((GridBagLayout) sNPanelContenu1.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) sNPanelContenu1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
      
      // ======== pnlEtablissement ========
      {
        pnlEtablissement.setName("pnlEtablissement");
        pnlEtablissement.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbNumero ----
        lbNumero.setText("Num\u00e9ro");
        lbNumero.setName("lbNumero");
        pnlEtablissement.add(lbNumero, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== sNPanel4 ========
        {
          sNPanel4.setName("sNPanel4");
          sNPanel4.setLayout(new GridBagLayout());
          ((GridBagLayout) sNPanel4.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) sNPanel4.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) sNPanel4.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) sNPanel4.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- WNUM ----
          WNUM.setOpaque(false);
          WNUM.setMaximumSize(new Dimension(80, 30));
          WNUM.setMinimumSize(new Dimension(80, 30));
          WNUM.setPreferredSize(new Dimension(80, 30));
          WNUM.setName("WNUM");
          sNPanel4.add(WNUM, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WSUF ----
          WSUF.setOpaque(false);
          WSUF.setMaximumSize(new Dimension(30, 30));
          WSUF.setMinimumSize(new Dimension(30, 30));
          WSUF.setPreferredSize(new Dimension(30, 30));
          WSUF.setName("WSUF");
          sNPanel4.add(WSUF, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlEtablissement.add(sNPanel4, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbEtablissement ----
        lbEtablissement.setText("Etablissement");
        lbEtablissement.setName("lbEtablissement");
        pnlEtablissement.add(lbEtablissement, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snEtablissement ----
        snEtablissement.setName("snEtablissement");
        pnlEtablissement.add(snEtablissement, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbFournisseur ----
        lbFournisseur.setText("Fournisseur");
        lbFournisseur.setName("lbFournisseur");
        pnlEtablissement.add(lbFournisseur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- E2NOM ----
        E2NOM.setMinimumSize(new Dimension(350, 30));
        E2NOM.setMaximumSize(new Dimension(350, 30));
        E2NOM.setPreferredSize(new Dimension(350, 30));
        E2NOM.setName("E2NOM");
        pnlEtablissement.add(E2NOM, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbDate ----
        lbDate.setText("Date");
        lbDate.setName("lbDate");
        pnlEtablissement.add(lbDate, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WDATEX ----
        WDATEX.setMinimumSize(new Dimension(110, 30));
        WDATEX.setMaximumSize(new Dimension(110, 30));
        WDATEX.setPreferredSize(new Dimension(110, 30));
        WDATEX.setName("WDATEX");
        pnlEtablissement.add(WDATEX, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbSection ----
        lbSection.setText("Section analytique");
        lbSection.setName("lbSection");
        pnlEtablissement.add(lbSection, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- snSectionAnalytique ----
        snSectionAnalytique.setName("snSectionAnalytique");
        pnlEtablissement.add(snSectionAnalytique, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
      }
      sNPanelContenu1.add(pnlEtablissement,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlLignes ========
      {
        pnlLignes.setName("pnlLignes");
        pnlLignes.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlLignes.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlLignes.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlLignes.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlLignes.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlListe ========
        {
          pnlListe.setMinimumSize(new Dimension(470, 380));
          pnlListe.setPreferredSize(new Dimension(500, 430));
          pnlListe.setName("pnlListe");
          pnlListe.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlListe.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlListe.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlListe.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlListe.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlPresenttion ========
          {
            pnlPresenttion.setMinimumSize(new Dimension(205, 400));
            pnlPresenttion.setPreferredSize(new Dimension(205, 400));
            pnlPresenttion.setName("pnlPresenttion");
            pnlPresenttion.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlPresenttion.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlPresenttion.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlPresenttion.getLayout()).columnWeights = new double[] { 1.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlPresenttion.getLayout()).rowWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbTitre1 ----
            lbTitre1.setText("@WTIT118@");
            lbTitre1.setMinimumSize(new Dimension(50, 24));
            lbTitre1.setPreferredSize(new Dimension(50, 24));
            lbTitre1.setMaximumSize(new Dimension(50, 23));
            lbTitre1.setFont(new Font("Courier New", Font.BOLD, 14));
            lbTitre1.setOpaque(true);
            lbTitre1.setName("lbTitre1");
            pnlPresenttion.add(lbTitre1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbTitre2 ----
            lbTitre2.setText("Quantit\u00e9 extraite");
            lbTitre2.setMinimumSize(new Dimension(150, 24));
            lbTitre2.setPreferredSize(new Dimension(150, 24));
            lbTitre2.setMaximumSize(new Dimension(50, 23));
            lbTitre2.setFont(new Font("Courier New", Font.BOLD, 14));
            lbTitre2.setName("lbTitre2");
            pnlPresenttion.add(lbTitre2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbLigne1 ----
            lbLigne1.setText("@LE01@");
            lbLigne1.setMinimumSize(new Dimension(50, 24));
            lbLigne1.setPreferredSize(new Dimension(50, 24));
            lbLigne1.setMaximumSize(new Dimension(50, 23));
            lbLigne1.setFont(new Font("Courier New", Font.PLAIN, 14));
            lbLigne1.setBackground(Color.white);
            lbLigne1.setOpaque(true);
            lbLigne1.setName("lbLigne1");
            pnlPresenttion.add(lbLigne1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- Q01 ----
            Q01.setMinimumSize(new Dimension(100, 25));
            Q01.setPreferredSize(new Dimension(100, 24));
            Q01.setMaximumSize(new Dimension(100, 30));
            Q01.setHorizontalAlignment(SwingConstants.RIGHT);
            Q01.setBorder(null);
            Q01.setNextFocusableComponent(Q02);
            Q01.setName("Q01");
            pnlPresenttion.add(Q01, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbLigne2 ----
            lbLigne2.setText("@LE02@");
            lbLigne2.setMinimumSize(new Dimension(50, 24));
            lbLigne2.setPreferredSize(new Dimension(50, 24));
            lbLigne2.setMaximumSize(new Dimension(50, 23));
            lbLigne2.setFont(new Font("Courier New", Font.PLAIN, 14));
            lbLigne2.setOpaque(true);
            lbLigne2.setName("lbLigne2");
            pnlPresenttion.add(lbLigne2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- Q02 ----
            Q02.setMinimumSize(new Dimension(100, 25));
            Q02.setPreferredSize(new Dimension(100, 24));
            Q02.setMaximumSize(new Dimension(100, 30));
            Q02.setHorizontalAlignment(SwingConstants.RIGHT);
            Q02.setBorder(null);
            Q02.setNextFocusableComponent(Q03);
            Q02.setName("Q02");
            pnlPresenttion.add(Q02, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbLigne3 ----
            lbLigne3.setText("@LE03@");
            lbLigne3.setMinimumSize(new Dimension(50, 24));
            lbLigne3.setPreferredSize(new Dimension(50, 24));
            lbLigne3.setMaximumSize(new Dimension(50, 23));
            lbLigne3.setFont(new Font("Courier New", Font.PLAIN, 14));
            lbLigne3.setBackground(Color.white);
            lbLigne3.setOpaque(true);
            lbLigne3.setName("lbLigne3");
            pnlPresenttion.add(lbLigne3, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- Q03 ----
            Q03.setMinimumSize(new Dimension(100, 25));
            Q03.setPreferredSize(new Dimension(100, 24));
            Q03.setMaximumSize(new Dimension(100, 30));
            Q03.setHorizontalAlignment(SwingConstants.RIGHT);
            Q03.setBorder(null);
            Q03.setNextFocusableComponent(Q04);
            Q03.setName("Q03");
            pnlPresenttion.add(Q03, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbLigne4 ----
            lbLigne4.setText("@LE04@");
            lbLigne4.setMinimumSize(new Dimension(50, 24));
            lbLigne4.setPreferredSize(new Dimension(50, 24));
            lbLigne4.setMaximumSize(new Dimension(50, 23));
            lbLigne4.setFont(new Font("Courier New", Font.PLAIN, 14));
            lbLigne4.setOpaque(true);
            lbLigne4.setName("lbLigne4");
            pnlPresenttion.add(lbLigne4, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- Q04 ----
            Q04.setMinimumSize(new Dimension(100, 25));
            Q04.setPreferredSize(new Dimension(100, 24));
            Q04.setMaximumSize(new Dimension(100, 30));
            Q04.setHorizontalAlignment(SwingConstants.RIGHT);
            Q04.setBorder(null);
            Q04.setNextFocusableComponent(Q05);
            Q04.setName("Q04");
            pnlPresenttion.add(Q04, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbLigne5 ----
            lbLigne5.setText("@LE05@");
            lbLigne5.setMinimumSize(new Dimension(50, 24));
            lbLigne5.setPreferredSize(new Dimension(50, 24));
            lbLigne5.setMaximumSize(new Dimension(50, 23));
            lbLigne5.setFont(new Font("Courier New", Font.PLAIN, 14));
            lbLigne5.setBackground(Color.white);
            lbLigne5.setOpaque(true);
            lbLigne5.setName("lbLigne5");
            pnlPresenttion.add(lbLigne5, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- Q05 ----
            Q05.setMinimumSize(new Dimension(100, 25));
            Q05.setPreferredSize(new Dimension(100, 24));
            Q05.setMaximumSize(new Dimension(100, 30));
            Q05.setHorizontalAlignment(SwingConstants.RIGHT);
            Q05.setBorder(null);
            Q05.setNextFocusableComponent(Q06);
            Q05.setName("Q05");
            pnlPresenttion.add(Q05, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbLigne6 ----
            lbLigne6.setText("@LE06@");
            lbLigne6.setMinimumSize(new Dimension(50, 24));
            lbLigne6.setPreferredSize(new Dimension(50, 24));
            lbLigne6.setMaximumSize(new Dimension(50, 23));
            lbLigne6.setFont(new Font("Courier New", Font.PLAIN, 14));
            lbLigne6.setOpaque(true);
            lbLigne6.setName("lbLigne6");
            pnlPresenttion.add(lbLigne6, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- Q06 ----
            Q06.setMinimumSize(new Dimension(100, 25));
            Q06.setPreferredSize(new Dimension(100, 24));
            Q06.setMaximumSize(new Dimension(100, 30));
            Q06.setHorizontalAlignment(SwingConstants.RIGHT);
            Q06.setBorder(null);
            Q06.setNextFocusableComponent(Q07);
            Q06.setName("Q06");
            pnlPresenttion.add(Q06, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbLigne7 ----
            lbLigne7.setText("@LE07@");
            lbLigne7.setMinimumSize(new Dimension(50, 24));
            lbLigne7.setPreferredSize(new Dimension(50, 24));
            lbLigne7.setMaximumSize(new Dimension(50, 23));
            lbLigne7.setFont(new Font("Courier New", Font.PLAIN, 14));
            lbLigne7.setBackground(Color.white);
            lbLigne7.setOpaque(true);
            lbLigne7.setName("lbLigne7");
            pnlPresenttion.add(lbLigne7, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- Q07 ----
            Q07.setMinimumSize(new Dimension(100, 25));
            Q07.setPreferredSize(new Dimension(100, 24));
            Q07.setMaximumSize(new Dimension(100, 30));
            Q07.setHorizontalAlignment(SwingConstants.RIGHT);
            Q07.setBorder(null);
            Q07.setNextFocusableComponent(Q08);
            Q07.setName("Q07");
            pnlPresenttion.add(Q07, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbLigne8 ----
            lbLigne8.setText("@LE08@");
            lbLigne8.setMinimumSize(new Dimension(50, 24));
            lbLigne8.setPreferredSize(new Dimension(50, 24));
            lbLigne8.setMaximumSize(new Dimension(50, 23));
            lbLigne8.setFont(new Font("Courier New", Font.PLAIN, 14));
            lbLigne8.setOpaque(true);
            lbLigne8.setName("lbLigne8");
            pnlPresenttion.add(lbLigne8, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- Q08 ----
            Q08.setMinimumSize(new Dimension(100, 25));
            Q08.setPreferredSize(new Dimension(100, 24));
            Q08.setMaximumSize(new Dimension(100, 30));
            Q08.setHorizontalAlignment(SwingConstants.RIGHT);
            Q08.setBorder(null);
            Q08.setNextFocusableComponent(Q09);
            Q08.setName("Q08");
            pnlPresenttion.add(Q08, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbLigne9 ----
            lbLigne9.setText("@LE09@");
            lbLigne9.setMinimumSize(new Dimension(50, 24));
            lbLigne9.setPreferredSize(new Dimension(50, 24));
            lbLigne9.setMaximumSize(new Dimension(50, 23));
            lbLigne9.setFont(new Font("Courier New", Font.PLAIN, 14));
            lbLigne9.setBackground(Color.white);
            lbLigne9.setOpaque(true);
            lbLigne9.setName("lbLigne9");
            pnlPresenttion.add(lbLigne9, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- Q09 ----
            Q09.setMinimumSize(new Dimension(100, 25));
            Q09.setPreferredSize(new Dimension(100, 24));
            Q09.setMaximumSize(new Dimension(100, 30));
            Q09.setHorizontalAlignment(SwingConstants.RIGHT);
            Q09.setBorder(null);
            Q09.setNextFocusableComponent(Q10);
            Q09.setName("Q09");
            pnlPresenttion.add(Q09, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbLigne10 ----
            lbLigne10.setText("@LE10@");
            lbLigne10.setMinimumSize(new Dimension(50, 24));
            lbLigne10.setPreferredSize(new Dimension(50, 24));
            lbLigne10.setMaximumSize(new Dimension(50, 23));
            lbLigne10.setFont(new Font("Courier New", Font.PLAIN, 14));
            lbLigne10.setOpaque(true);
            lbLigne10.setName("lbLigne10");
            pnlPresenttion.add(lbLigne10, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- Q10 ----
            Q10.setMinimumSize(new Dimension(100, 25));
            Q10.setPreferredSize(new Dimension(100, 24));
            Q10.setMaximumSize(new Dimension(100, 30));
            Q10.setHorizontalAlignment(SwingConstants.RIGHT);
            Q10.setBorder(null);
            Q10.setNextFocusableComponent(Q11);
            Q10.setName("Q10");
            pnlPresenttion.add(Q10, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbLigne11 ----
            lbLigne11.setText("@LE11@");
            lbLigne11.setMinimumSize(new Dimension(50, 24));
            lbLigne11.setPreferredSize(new Dimension(50, 24));
            lbLigne11.setMaximumSize(new Dimension(50, 23));
            lbLigne11.setFont(new Font("Courier New", Font.PLAIN, 14));
            lbLigne11.setBackground(Color.white);
            lbLigne11.setOpaque(true);
            lbLigne11.setName("lbLigne11");
            pnlPresenttion.add(lbLigne11, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- Q11 ----
            Q11.setMinimumSize(new Dimension(100, 25));
            Q11.setPreferredSize(new Dimension(100, 24));
            Q11.setMaximumSize(new Dimension(100, 30));
            Q11.setHorizontalAlignment(SwingConstants.RIGHT);
            Q11.setBorder(null);
            Q11.setNextFocusableComponent(Q12);
            Q11.setName("Q11");
            pnlPresenttion.add(Q11, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbLigne12 ----
            lbLigne12.setText("@LE12@");
            lbLigne12.setMinimumSize(new Dimension(50, 24));
            lbLigne12.setPreferredSize(new Dimension(50, 24));
            lbLigne12.setMaximumSize(new Dimension(50, 23));
            lbLigne12.setFont(new Font("Courier New", Font.PLAIN, 14));
            lbLigne12.setOpaque(true);
            lbLigne12.setName("lbLigne12");
            pnlPresenttion.add(lbLigne12, new GridBagConstraints(0, 12, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- Q12 ----
            Q12.setMinimumSize(new Dimension(100, 25));
            Q12.setPreferredSize(new Dimension(100, 24));
            Q12.setMaximumSize(new Dimension(100, 30));
            Q12.setHorizontalAlignment(SwingConstants.RIGHT);
            Q12.setBorder(null);
            Q12.setNextFocusableComponent(Q13);
            Q12.setName("Q12");
            pnlPresenttion.add(Q12, new GridBagConstraints(1, 12, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbLigne13 ----
            lbLigne13.setText("@LE13@");
            lbLigne13.setMinimumSize(new Dimension(50, 24));
            lbLigne13.setPreferredSize(new Dimension(50, 24));
            lbLigne13.setMaximumSize(new Dimension(50, 23));
            lbLigne13.setFont(new Font("Courier New", Font.PLAIN, 14));
            lbLigne13.setBackground(Color.white);
            lbLigne13.setOpaque(true);
            lbLigne13.setName("lbLigne13");
            pnlPresenttion.add(lbLigne13, new GridBagConstraints(0, 13, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbLigne14 ----
            lbLigne14.setText("@LE14@");
            lbLigne14.setMinimumSize(new Dimension(50, 24));
            lbLigne14.setPreferredSize(new Dimension(50, 24));
            lbLigne14.setMaximumSize(new Dimension(50, 23));
            lbLigne14.setFont(new Font("Courier New", Font.PLAIN, 14));
            lbLigne14.setOpaque(true);
            lbLigne14.setName("lbLigne14");
            pnlPresenttion.add(lbLigne14, new GridBagConstraints(0, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- Q14 ----
            Q14.setMinimumSize(new Dimension(100, 25));
            Q14.setPreferredSize(new Dimension(100, 24));
            Q14.setMaximumSize(new Dimension(100, 30));
            Q14.setHorizontalAlignment(SwingConstants.RIGHT);
            Q14.setBorder(null);
            Q14.setNextFocusableComponent(Q15);
            Q14.setName("Q14");
            pnlPresenttion.add(Q14, new GridBagConstraints(1, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbLigne15 ----
            lbLigne15.setText("@LE15@");
            lbLigne15.setMinimumSize(new Dimension(50, 24));
            lbLigne15.setPreferredSize(new Dimension(50, 24));
            lbLigne15.setMaximumSize(new Dimension(50, 23));
            lbLigne15.setFont(new Font("Courier New", Font.PLAIN, 14));
            lbLigne15.setBackground(Color.white);
            lbLigne15.setOpaque(true);
            lbLigne15.setName("lbLigne15");
            pnlPresenttion.add(lbLigne15, new GridBagConstraints(0, 15, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ======== pnlDown ========
            {
              pnlDown.setMaximumSize(new Dimension(28, 29));
              pnlDown.setName("pnlDown");
              pnlDown.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlDown.getLayout()).columnWidths = new int[] { 0, 0 };
              ((GridBagLayout) pnlDown.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlDown.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
              ((GridBagLayout) pnlDown.getLayout()).rowWeights = new double[] { 1.0, 1.0, 0.0, 1.0E-4 };
              
              // ---- BT_PGUP ----
              BT_PGUP.setText("");
              BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
              BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_PGUP.setMinimumSize(new Dimension(25, 12));
              BT_PGUP.setPreferredSize(new Dimension(25, 12));
              BT_PGUP.setRequestFocusEnabled(false);
              BT_PGUP.setName("BT_PGUP");
              pnlDown.add(BT_PGUP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- BT_PGDOWN ----
              BT_PGDOWN.setText("");
              BT_PGDOWN.setToolTipText("Page suivante");
              BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_PGDOWN.setMinimumSize(new Dimension(25, 12));
              BT_PGDOWN.setPreferredSize(new Dimension(25, 12));
              BT_PGDOWN.setRequestFocusEnabled(false);
              BT_PGDOWN.setName("BT_PGDOWN");
              pnlDown.add(BT_PGDOWN, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- sNLabelChamp5 ----
              sNLabelChamp5.setMinimumSize(new Dimension(25, 1));
              sNLabelChamp5.setPreferredSize(new Dimension(25, 1));
              sNLabelChamp5.setName("sNLabelChamp5");
              pnlDown.add(sNLabelChamp5, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHWEST,
                  GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlPresenttion.add(pnlDown, new GridBagConstraints(2, 1, 1, 15, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- Q13 ----
            Q13.setMinimumSize(new Dimension(100, 25));
            Q13.setPreferredSize(new Dimension(100, 24));
            Q13.setMaximumSize(new Dimension(100, 30));
            Q13.setHorizontalAlignment(SwingConstants.RIGHT);
            Q13.setBorder(null);
            Q13.setNextFocusableComponent(Q14);
            Q13.setName("Q13");
            pnlPresenttion.add(Q13, new GridBagConstraints(1, 13, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- Q15 ----
            Q15.setMinimumSize(new Dimension(100, 25));
            Q15.setPreferredSize(new Dimension(100, 24));
            Q15.setMaximumSize(new Dimension(100, 30));
            Q15.setHorizontalAlignment(SwingConstants.RIGHT);
            Q15.setBorder(null);
            Q15.setNextFocusableComponent(snBarreBouton);
            Q15.setName("Q15");
            pnlPresenttion.add(Q15, new GridBagConstraints(1, 15, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlListe.add(pnlPresenttion, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlLignes.add(pnlListe, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== sNPanel5 ========
        {
          sNPanel5.setName("sNPanel5");
          sNPanel5.setLayout(new GridBagLayout());
          ((GridBagLayout) sNPanel5.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) sNPanel5.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) sNPanel5.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) sNPanel5.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbMagasin ----
          lbMagasin.setText("Magasin de r\u00e9ception");
          lbMagasin.setName("lbMagasin");
          sNPanel5.add(lbMagasin, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snMagasin ----
          snMagasin.setMaximumSize(new Dimension(350, 30));
          snMagasin.setMinimumSize(new Dimension(350, 30));
          snMagasin.setPreferredSize(new Dimension(350, 30));
          snMagasin.setName("snMagasin");
          sNPanel5.add(snMagasin, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbWRBL ----
          lbWRBL.setText("R\u00e9f\u00e9rence de livraison fournisseur");
          lbWRBL.setMinimumSize(new Dimension(250, 30));
          lbWRBL.setMaximumSize(new Dimension(250, 30));
          lbWRBL.setPreferredSize(new Dimension(250, 30));
          lbWRBL.setName("lbWRBL");
          sNPanel5.add(lbWRBL, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WRBL ----
          WRBL.setMinimumSize(new Dimension(100, 30));
          WRBL.setPreferredSize(new Dimension(100, 30));
          WRBL.setMaximumSize(new Dimension(100, 30));
          WRBL.setName("WRBL");
          sNPanel5.add(WRBL, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlLignes.add(sNPanel5, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      sNPanelContenu1.add(pnlLignes,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(sNPanelContenu1, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu sNPanelContenu1;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbNumero;
  private SNPanel sNPanel4;
  private XRiTextField WNUM;
  private XRiTextField WSUF;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbFournisseur;
  private XRiTextField E2NOM;
  private SNLabelChamp lbDate;
  private XRiCalendrier WDATEX;
  private SNLabelChamp lbSection;
  private SNSectionAnalytique snSectionAnalytique;
  private SNPanelTitre pnlLignes;
  private SNPanel pnlListe;
  private SNPanel pnlPresenttion;
  private JLabel lbTitre1;
  private JLabel lbTitre2;
  private JLabel lbLigne1;
  private XRiTextField Q01;
  private JLabel lbLigne2;
  private XRiTextField Q02;
  private JLabel lbLigne3;
  private XRiTextField Q03;
  private JLabel lbLigne4;
  private XRiTextField Q04;
  private JLabel lbLigne5;
  private XRiTextField Q05;
  private JLabel lbLigne6;
  private XRiTextField Q06;
  private JLabel lbLigne7;
  private XRiTextField Q07;
  private JLabel lbLigne8;
  private XRiTextField Q08;
  private JLabel lbLigne9;
  private XRiTextField Q09;
  private JLabel lbLigne10;
  private XRiTextField Q10;
  private JLabel lbLigne11;
  private XRiTextField Q11;
  private JLabel lbLigne12;
  private XRiTextField Q12;
  private JLabel lbLigne13;
  private JLabel lbLigne14;
  private XRiTextField Q14;
  private JLabel lbLigne15;
  private SNPanel pnlDown;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private SNLabelChamp sNLabelChamp5;
  private XRiTextField Q13;
  private XRiTextField Q15;
  private SNPanel sNPanel5;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNLabelChamp lbWRBL;
  private XRiTextField WRBL;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
