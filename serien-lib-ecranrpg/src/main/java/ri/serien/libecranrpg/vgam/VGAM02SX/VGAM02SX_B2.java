
package ri.serien.libecranrpg.vgam.VGAM02SX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGAM02SX_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGAM02SX_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    SXIN4.setValeursSelection("O", "N");
    SXIN3.setValeursSelection("O", "N");
    SXIN1.setValeursSelection("O", "N");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    SXC32.setEnabled(lexique.isPresent("SXC32"));
    SXC31.setEnabled(lexique.isPresent("SXC31"));
    SXC22.setEnabled(lexique.isPresent("SXC22"));
    SXC21.setEnabled(lexique.isPresent("SXC21"));
    SXC12.setEnabled(lexique.isPresent("SXC12"));
    SXC11.setEnabled(lexique.isPresent("SXC11"));
    SXLG3.setEnabled(lexique.isPresent("SXLG3"));
    SXLG2.setEnabled(lexique.isPresent("SXLG2"));
    SXLG1.setEnabled(lexique.isPresent("SXLG1"));
    SXTC3.setEnabled(lexique.isPresent("SXTC3"));
    SXTC2.setEnabled(lexique.isPresent("SXTC2"));
    SXTC1.setEnabled(lexique.isPresent("SXTC1"));
    // SXIN4.setSelected(lexique.HostFieldGetData("SXIN4").equalsIgnoreCase("O"));
    // SXIN3.setSelected(lexique.HostFieldGetData("SXIN3").equalsIgnoreCase("O"));
    SXF32.setEnabled(lexique.isPresent("SXF32"));
    SXD32.setEnabled(lexique.isPresent("SXD32"));
    SXF31.setEnabled(lexique.isPresent("SXF31"));
    SXD31.setEnabled(lexique.isPresent("SXD31"));
    SXF22.setEnabled(lexique.isPresent("SXF22"));
    SXD22.setEnabled(lexique.isPresent("SXD22"));
    SXF21.setEnabled(lexique.isPresent("SXF21"));
    SXD21.setEnabled(lexique.isPresent("SXD21"));
    SXF12.setEnabled(lexique.isPresent("SXF12"));
    SXD12.setEnabled(lexique.isPresent("SXD12"));
    SXF11.setEnabled(lexique.isPresent("SXF11"));
    SXD11.setEnabled(lexique.isPresent("SXD11"));
    // SXIN1.setSelected(lexique.HostFieldGetData("SXIN1").equalsIgnoreCase("O"));
    SXLC3.setEnabled(lexique.isPresent("SXLC3"));
    SXLC2.setEnabled(lexique.isPresent("SXLC2"));
    SXLC1.setEnabled(lexique.isPresent("SXLC1"));
    SXLIB.setEnabled(lexique.isPresent("SXLIB"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITRE@ STATISTIQUES"));
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (SXIN4.isSelected())
    // lexique.HostFieldPutData("SXIN4", 0, "O");
    // else
    // lexique.HostFieldPutData("SXIN4", 0, "N");
    // if (SXIN3.isSelected())
    // lexique.HostFieldPutData("SXIN3", 0, "O");
    // else
    // lexique.HostFieldPutData("SXIN3", 0, "N");
    // if (SXIN1.isSelected())
    // lexique.HostFieldPutData("SXIN1", 0, "O");
    // else
    // lexique.HostFieldPutData("SXIN1", 0, "N");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_48 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel2 = new JXTitledPanel();
    SXLIB = new XRiTextField();
    SXLC1 = new XRiTextField();
    SXLC2 = new XRiTextField();
    SXLC3 = new XRiTextField();
    SXIN1 = new XRiCheckBox();
    SXD11 = new XRiTextField();
    SXF11 = new XRiTextField();
    SXD12 = new XRiTextField();
    SXF12 = new XRiTextField();
    SXD21 = new XRiTextField();
    SXF21 = new XRiTextField();
    SXD22 = new XRiTextField();
    SXF22 = new XRiTextField();
    SXD31 = new XRiTextField();
    SXF31 = new XRiTextField();
    SXD32 = new XRiTextField();
    SXF32 = new XRiTextField();
    OBJ_53 = new JLabel();
    OBJ_90 = new JLabel();
    SXIN3 = new XRiCheckBox();
    SXIN4 = new XRiCheckBox();
    OBJ_91 = new JLabel();
    OBJ_93 = new JLabel();
    SXTC1 = new XRiTextField();
    SXTC2 = new XRiTextField();
    SXTC3 = new XRiTextField();
    OBJ_94 = new JLabel();
    SXLG1 = new XRiTextField();
    SXLG2 = new XRiTextField();
    SXLG3 = new XRiTextField();
    OBJ_92 = new JLabel();
    SXC11 = new XRiTextField();
    SXC12 = new XRiTextField();
    SXC21 = new XRiTextField();
    SXC22 = new XRiTextField();
    SXC31 = new XRiTextField();
    SXC32 = new XRiTextField();
    separator1 = compFactory.createSeparator("Crit\u00e8res");
    separator2 = compFactory.createSeparator("Option(s)");
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    OBJ_16 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des achats");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44 ----
          OBJ_44.setText("Etablissement");
          OBJ_44.setName("OBJ_44");

          //---- INDETB ----
          INDETB.setName("INDETB");

          //---- OBJ_46 ----
          OBJ_46.setText("Code");
          OBJ_46.setName("OBJ_46");

          //---- INDTYP ----
          INDTYP.setName("INDTYP");

          //---- OBJ_48 ----
          OBJ_48.setText("Ordre");
          OBJ_48.setName("OBJ_48");

          //---- INDIND ----
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(513, Short.MAX_VALUE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 540));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setTitle("Param\u00e9trage des zones statistiques");
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //---- SXLIB ----
            SXLIB.setComponentPopupMenu(BTD);
            SXLIB.setName("SXLIB");
            xTitledPanel2ContentContainer.add(SXLIB);
            SXLIB.setBounds(215, 40, 408, SXLIB.getPreferredSize().height);

            //---- SXLC1 ----
            SXLC1.setComponentPopupMenu(BTD);
            SXLC1.setName("SXLC1");
            xTitledPanel2ContentContainer.add(SXLC1);
            SXLC1.setBounds(148, 165, 310, SXLC1.getPreferredSize().height);

            //---- SXLC2 ----
            SXLC2.setComponentPopupMenu(BTD);
            SXLC2.setName("SXLC2");
            xTitledPanel2ContentContainer.add(SXLC2);
            SXLC2.setBounds(148, 245, 310, SXLC2.getPreferredSize().height);

            //---- SXLC3 ----
            SXLC3.setComponentPopupMenu(BTD);
            SXLC3.setName("SXLC3");
            xTitledPanel2ContentContainer.add(SXLC3);
            SXLC3.setBounds(148, 320, 310, SXLC3.getPreferredSize().height);

            //---- SXIN1 ----
            SXIN1.setText("Statistiques journali\u00e8res");
            SXIN1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SXIN1.setName("SXIN1");
            xTitledPanel2ContentContainer.add(SXIN1);
            SXIN1.setBounds(60, 420, 183, 20);

            //---- SXD11 ----
            SXD11.setComponentPopupMenu(BTD);
            SXD11.setName("SXD11");
            xTitledPanel2ContentContainer.add(SXD11);
            SXD11.setBounds(463, 165, 160, SXD11.getPreferredSize().height);

            //---- SXF11 ----
            SXF11.setComponentPopupMenu(BTD);
            SXF11.setName("SXF11");
            xTitledPanel2ContentContainer.add(SXF11);
            SXF11.setBounds(628, 165, 160, SXF11.getPreferredSize().height);

            //---- SXD12 ----
            SXD12.setComponentPopupMenu(BTD);
            SXD12.setName("SXD12");
            xTitledPanel2ContentContainer.add(SXD12);
            SXD12.setBounds(463, 195, 160, SXD12.getPreferredSize().height);

            //---- SXF12 ----
            SXF12.setComponentPopupMenu(BTD);
            SXF12.setName("SXF12");
            xTitledPanel2ContentContainer.add(SXF12);
            SXF12.setBounds(628, 195, 160, SXF12.getPreferredSize().height);

            //---- SXD21 ----
            SXD21.setComponentPopupMenu(BTD);
            SXD21.setName("SXD21");
            xTitledPanel2ContentContainer.add(SXD21);
            SXD21.setBounds(463, 245, 160, SXD21.getPreferredSize().height);

            //---- SXF21 ----
            SXF21.setComponentPopupMenu(BTD);
            SXF21.setName("SXF21");
            xTitledPanel2ContentContainer.add(SXF21);
            SXF21.setBounds(628, 245, 160, SXF21.getPreferredSize().height);

            //---- SXD22 ----
            SXD22.setComponentPopupMenu(BTD);
            SXD22.setName("SXD22");
            xTitledPanel2ContentContainer.add(SXD22);
            SXD22.setBounds(463, 275, 160, SXD22.getPreferredSize().height);

            //---- SXF22 ----
            SXF22.setComponentPopupMenu(BTD);
            SXF22.setName("SXF22");
            xTitledPanel2ContentContainer.add(SXF22);
            SXF22.setBounds(628, 275, 160, SXF22.getPreferredSize().height);

            //---- SXD31 ----
            SXD31.setComponentPopupMenu(BTD);
            SXD31.setName("SXD31");
            xTitledPanel2ContentContainer.add(SXD31);
            SXD31.setBounds(463, 320, 160, SXD31.getPreferredSize().height);

            //---- SXF31 ----
            SXF31.setComponentPopupMenu(BTD);
            SXF31.setName("SXF31");
            xTitledPanel2ContentContainer.add(SXF31);
            SXF31.setBounds(628, 320, 160, SXF31.getPreferredSize().height);

            //---- SXD32 ----
            SXD32.setComponentPopupMenu(BTD);
            SXD32.setName("SXD32");
            xTitledPanel2ContentContainer.add(SXD32);
            SXD32.setBounds(463, 350, 160, SXD32.getPreferredSize().height);

            //---- SXF32 ----
            SXF32.setComponentPopupMenu(BTD);
            SXF32.setName("SXF32");
            xTitledPanel2ContentContainer.add(SXF32);
            SXF32.setBounds(628, 350, 160, SXF32.getPreferredSize().height);

            //---- OBJ_53 ----
            OBJ_53.setText("Libell\u00e9");
            OBJ_53.setName("OBJ_53");
            xTitledPanel2ContentContainer.add(OBJ_53);
            OBJ_53.setBounds(120, 40, 61, 24);

            //---- OBJ_90 ----
            OBJ_90.setText("D\u00e9but");
            OBJ_90.setFont(OBJ_90.getFont().deriveFont(OBJ_90.getFont().getStyle() | Font.BOLD));
            OBJ_90.setName("OBJ_90");
            xTitledPanel2ContentContainer.add(OBJ_90);
            OBJ_90.setBounds(463, 140, 58, 20);

            //---- SXIN3 ----
            SXIN3.setText("Total");
            SXIN3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SXIN3.setName("SXIN3");
            xTitledPanel2ContentContainer.add(SXIN3);
            SXIN3.setBounds(60, 169, 52, 20);

            //---- SXIN4 ----
            SXIN4.setText("Total");
            SXIN4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SXIN4.setName("SXIN4");
            xTitledPanel2ContentContainer.add(SXIN4);
            SXIN4.setBounds(60, 249, 52, 20);

            //---- OBJ_91 ----
            OBJ_91.setText("Fin");
            OBJ_91.setFont(OBJ_91.getFont().deriveFont(OBJ_91.getFont().getStyle() | Font.BOLD));
            OBJ_91.setName("OBJ_91");
            xTitledPanel2ContentContainer.add(OBJ_91);
            OBJ_91.setBounds(628, 140, 40, 20);

            //---- OBJ_93 ----
            OBJ_93.setText("Code");
            OBJ_93.setFont(OBJ_93.getFont().deriveFont(OBJ_93.getFont().getStyle() | Font.BOLD));
            OBJ_93.setName("OBJ_93");
            xTitledPanel2ContentContainer.add(OBJ_93);
            OBJ_93.setBounds(15, 141, 42, 18);

            //---- SXTC1 ----
            SXTC1.setComponentPopupMenu(BTD);
            SXTC1.setName("SXTC1");
            xTitledPanel2ContentContainer.add(SXTC1);
            SXTC1.setBounds(15, 165, 40, SXTC1.getPreferredSize().height);

            //---- SXTC2 ----
            SXTC2.setComponentPopupMenu(BTD);
            SXTC2.setName("SXTC2");
            xTitledPanel2ContentContainer.add(SXTC2);
            SXTC2.setBounds(15, 245, 40, SXTC2.getPreferredSize().height);

            //---- SXTC3 ----
            SXTC3.setComponentPopupMenu(BTD);
            SXTC3.setName("SXTC3");
            xTitledPanel2ContentContainer.add(SXTC3);
            SXTC3.setBounds(15, 320, 40, SXTC3.getPreferredSize().height);

            //---- OBJ_94 ----
            OBJ_94.setText("Lg");
            OBJ_94.setFont(OBJ_94.getFont().deriveFont(OBJ_94.getFont().getStyle() | Font.BOLD));
            OBJ_94.setName("OBJ_94");
            xTitledPanel2ContentContainer.add(OBJ_94);
            OBJ_94.setBounds(117, 141, 25, 18);

            //---- SXLG1 ----
            SXLG1.setComponentPopupMenu(BTD);
            SXLG1.setName("SXLG1");
            xTitledPanel2ContentContainer.add(SXLG1);
            SXLG1.setBounds(117, 165, 26, SXLG1.getPreferredSize().height);

            //---- SXLG2 ----
            SXLG2.setComponentPopupMenu(BTD);
            SXLG2.setName("SXLG2");
            xTitledPanel2ContentContainer.add(SXLG2);
            SXLG2.setBounds(117, 245, 26, SXLG2.getPreferredSize().height);

            //---- SXLG3 ----
            SXLG3.setComponentPopupMenu(BTD);
            SXLG3.setName("SXLG3");
            xTitledPanel2ContentContainer.add(SXLG3);
            SXLG3.setBounds(117, 320, 26, SXLG3.getPreferredSize().height);

            //---- OBJ_92 ----
            OBJ_92.setText("I/E");
            OBJ_92.setFont(OBJ_92.getFont().deriveFont(OBJ_92.getFont().getStyle() | Font.BOLD));
            OBJ_92.setName("OBJ_92");
            xTitledPanel2ContentContainer.add(OBJ_92);
            OBJ_92.setBounds(793, 140, 19, 20);

            //---- SXC11 ----
            SXC11.setComponentPopupMenu(BTD);
            SXC11.setName("SXC11");
            xTitledPanel2ContentContainer.add(SXC11);
            SXC11.setBounds(793, 165, 20, SXC11.getPreferredSize().height);

            //---- SXC12 ----
            SXC12.setComponentPopupMenu(BTD);
            SXC12.setName("SXC12");
            xTitledPanel2ContentContainer.add(SXC12);
            SXC12.setBounds(793, 195, 20, SXC12.getPreferredSize().height);

            //---- SXC21 ----
            SXC21.setComponentPopupMenu(BTD);
            SXC21.setName("SXC21");
            xTitledPanel2ContentContainer.add(SXC21);
            SXC21.setBounds(793, 245, 20, SXC21.getPreferredSize().height);

            //---- SXC22 ----
            SXC22.setComponentPopupMenu(BTD);
            SXC22.setName("SXC22");
            xTitledPanel2ContentContainer.add(SXC22);
            SXC22.setBounds(793, 270, 20, SXC22.getPreferredSize().height);

            //---- SXC31 ----
            SXC31.setComponentPopupMenu(BTD);
            SXC31.setName("SXC31");
            xTitledPanel2ContentContainer.add(SXC31);
            SXC31.setBounds(793, 320, 20, SXC31.getPreferredSize().height);

            //---- SXC32 ----
            SXC32.setComponentPopupMenu(BTD);
            SXC32.setName("SXC32");
            xTitledPanel2ContentContainer.add(SXC32);
            SXC32.setBounds(793, 350, 20, SXC32.getPreferredSize().height);

            //---- separator1 ----
            separator1.setName("separator1");
            xTitledPanel2ContentContainer.add(separator1);
            separator1.setBounds(10, 110, 820, separator1.getPreferredSize().height);

            //---- separator2 ----
            separator2.setName("separator2");
            xTitledPanel2ContentContainer.add(separator2);
            separator2.setBounds(10, 390, 820, separator2.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel2, GroupLayout.DEFAULT_SIZE, 874, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 515, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Choix possibles");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_16 ----
      OBJ_16.setText("Aide en ligne");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44;
  private XRiTextField INDETB;
  private JLabel OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_48;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel2;
  private XRiTextField SXLIB;
  private XRiTextField SXLC1;
  private XRiTextField SXLC2;
  private XRiTextField SXLC3;
  private XRiCheckBox SXIN1;
  private XRiTextField SXD11;
  private XRiTextField SXF11;
  private XRiTextField SXD12;
  private XRiTextField SXF12;
  private XRiTextField SXD21;
  private XRiTextField SXF21;
  private XRiTextField SXD22;
  private XRiTextField SXF22;
  private XRiTextField SXD31;
  private XRiTextField SXF31;
  private XRiTextField SXD32;
  private XRiTextField SXF32;
  private JLabel OBJ_53;
  private JLabel OBJ_90;
  private XRiCheckBox SXIN3;
  private XRiCheckBox SXIN4;
  private JLabel OBJ_91;
  private JLabel OBJ_93;
  private XRiTextField SXTC1;
  private XRiTextField SXTC2;
  private XRiTextField SXTC3;
  private JLabel OBJ_94;
  private XRiTextField SXLG1;
  private XRiTextField SXLG2;
  private XRiTextField SXLG3;
  private JLabel OBJ_92;
  private XRiTextField SXC11;
  private XRiTextField SXC12;
  private XRiTextField SXC21;
  private XRiTextField SXC22;
  private XRiTextField SXC31;
  private XRiTextField SXC32;
  private JComponent separator1;
  private JComponent separator2;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_16;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
