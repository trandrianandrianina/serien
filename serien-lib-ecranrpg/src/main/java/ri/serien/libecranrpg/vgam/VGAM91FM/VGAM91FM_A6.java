
package ri.serien.libecranrpg.vgam.VGAM91FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGAM91FM_A6 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGAM91FM_A6(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLCR1@")).trim());
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLCR2@")).trim());
    label3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLCR3@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    panel3.setVisible((lexique.isTrue("N79")));
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Tableau de bord"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_22 = new JLabel();
    OBJ_25 = new JLabel();
    UDEB1X = new XRiCalendrier();
    UFIN1X = new XRiCalendrier();
    UDEB2X = new XRiCalendrier();
    UFIN2X = new XRiCalendrier();
    panel2 = new JPanel();
    OBJ_18 = new JLabel();
    TYM01 = new XRiTextField();
    TYM02 = new XRiTextField();
    OBJ_19 = new JLabel();
    NBJ01 = new XRiTextField();
    NBJ02 = new XRiTextField();
    OBJ_20 = new JLabel();
    OBJ_21 = new JLabel();
    panel3 = new JPanel();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    WCRI1 = new XRiTextField();
    WCRI2 = new XRiTextField();
    WCRI3 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(800, 320));
    setPreferredSize(new Dimension(800, 320));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("P\u00e9riodes \u00e0 traiter"));
          panel1.setOpaque(false);
          panel1.setName("panel1");

          //---- OBJ_22 ----
          OBJ_22.setText("P\u00e9riode 1");
          OBJ_22.setName("OBJ_22");

          //---- OBJ_25 ----
          OBJ_25.setText("P\u00e9riode 2");
          OBJ_25.setName("OBJ_25");

          //---- UDEB1X ----
          UDEB1X.setName("UDEB1X");

          //---- UFIN1X ----
          UFIN1X.setName("UFIN1X");

          //---- UDEB2X ----
          UDEB2X.setName("UDEB2X");

          //---- UFIN2X ----
          UFIN2X.setName("UFIN2X");

          GroupLayout panel1Layout = new GroupLayout(panel1);
          panel1.setLayout(panel1Layout);
          panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                .addContainerGap(30, Short.MAX_VALUE)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 61, GroupLayout.PREFERRED_SIZE)
                    .addGap(24, 24, 24)
                    .addComponent(UDEB1X, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                    .addGap(20, 20, 20)
                    .addComponent(UFIN1X, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 61, GroupLayout.PREFERRED_SIZE)
                    .addGap(24, 24, 24)
                    .addComponent(UDEB2X, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                    .addGap(20, 20, 20)
                    .addComponent(UFIN2X, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18))
          );
          panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                .addContainerGap(24, Short.MAX_VALUE)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_22))
                  .addComponent(UDEB1X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(UFIN1X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_25))
                  .addComponent(UDEB2X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(UFIN2X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19))
          );
        }
        p_contenu.add(panel1);
        panel1.setBounds(5, 10, 375, 135);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Moyenne"));
          panel2.setOpaque(false);
          panel2.setName("panel2");

          //---- OBJ_18 ----
          OBJ_18.setText("Type");
          OBJ_18.setName("OBJ_18");

          //---- TYM01 ----
          TYM01.setName("TYM01");

          //---- TYM02 ----
          TYM02.setName("TYM02");

          //---- OBJ_19 ----
          OBJ_19.setText("Nombre");
          OBJ_19.setName("OBJ_19");

          //---- NBJ01 ----
          NBJ01.setName("NBJ01");

          //---- NBJ02 ----
          NBJ02.setName("NBJ02");

          //---- OBJ_20 ----
          OBJ_20.setText("Type");
          OBJ_20.setName("OBJ_20");

          //---- OBJ_21 ----
          OBJ_21.setText("Nombre");
          OBJ_21.setName("OBJ_21");

          GroupLayout panel2Layout = new GroupLayout(panel2);
          panel2.setLayout(panel2Layout);
          panel2Layout.setHorizontalGroup(
            panel2Layout.createParallelGroup()
              .addGroup(panel2Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(panel2Layout.createParallelGroup()
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addComponent(OBJ_18, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                    .addGap(4, 4, 4)
                    .addComponent(TYM01, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(15, 15, 15)
                    .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                    .addGap(13, 13, 13)
                    .addComponent(NBJ01, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                    .addGap(4, 4, 4)
                    .addComponent(TYM02, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(15, 15, 15)
                    .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                    .addGap(13, 13, 13)
                    .addComponent(NBJ02, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))))
          );
          panel2Layout.setVerticalGroup(
            panel2Layout.createParallelGroup()
              .addGroup(panel2Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(panel2Layout.createParallelGroup()
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_18, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(TYM01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(NBJ01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(panel2Layout.createParallelGroup()
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(TYM02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(NBJ02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_contenu.add(panel2);
        panel2.setBounds(395, 10, 225, 135);

        //======== panel3 ========
        {
          panel3.setBorder(new TitledBorder("Crit\u00e8res"));
          panel3.setOpaque(false);
          panel3.setName("panel3");

          //---- label1 ----
          label1.setText("@WLCR1@");
          label1.setName("label1");

          //---- label2 ----
          label2.setText("@WLCR2@");
          label2.setName("label2");

          //---- label3 ----
          label3.setText("@WLCR3@");
          label3.setName("label3");

          //---- WCRI1 ----
          WCRI1.setComponentPopupMenu(BTD);
          WCRI1.setName("WCRI1");

          //---- WCRI2 ----
          WCRI2.setComponentPopupMenu(BTD);
          WCRI2.setName("WCRI2");

          //---- WCRI3 ----
          WCRI3.setComponentPopupMenu(BTD);
          WCRI3.setName("WCRI3");

          GroupLayout panel3Layout = new GroupLayout(panel3);
          panel3.setLayout(panel3Layout);
          panel3Layout.setHorizontalGroup(
            panel3Layout.createParallelGroup()
              .addGroup(panel3Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(panel3Layout.createParallelGroup()
                  .addGroup(panel3Layout.createSequentialGroup()
                    .addComponent(label1, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                    .addGap(35, 35, 35)
                    .addComponent(WCRI1, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel3Layout.createSequentialGroup()
                    .addComponent(label2, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                    .addGap(35, 35, 35)
                    .addComponent(WCRI2, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel3Layout.createSequentialGroup()
                    .addComponent(label3, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                    .addGap(35, 35, 35)
                    .addComponent(WCRI3, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE))))
          );
          panel3Layout.setVerticalGroup(
            panel3Layout.createParallelGroup()
              .addGroup(panel3Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(panel3Layout.createParallelGroup()
                  .addGroup(panel3Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(label1))
                  .addComponent(WCRI1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel3Layout.createParallelGroup()
                  .addGroup(panel3Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(label2))
                  .addComponent(WCRI2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel3Layout.createParallelGroup()
                  .addGroup(panel3Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(label3))
                  .addComponent(WCRI3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_contenu.add(panel3);
        panel3.setBounds(5, 150, 615, 153);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_22;
  private JLabel OBJ_25;
  private XRiCalendrier UDEB1X;
  private XRiCalendrier UFIN1X;
  private XRiCalendrier UDEB2X;
  private XRiCalendrier UFIN2X;
  private JPanel panel2;
  private JLabel OBJ_18;
  private XRiTextField TYM01;
  private XRiTextField TYM02;
  private JLabel OBJ_19;
  private XRiTextField NBJ01;
  private XRiTextField NBJ02;
  private JLabel OBJ_20;
  private JLabel OBJ_21;
  private JPanel panel3;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private XRiTextField WCRI1;
  private XRiTextField WCRI2;
  private XRiTextField WCRI3;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
