
package ri.serien.libecranrpg.vgam.VGAM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGAM01FX_EA extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGAM01FX_EA(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    EAFCSV.setValeursSelection("1", " ");
    EAFTXT.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_76.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRSNOM@")).trim());
    OBJ_79.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ELE001@")).trim());
    OBJ_80.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ELE002@")).trim());
    OBJ_81.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ELE003@")).trim());
    OBJ_84.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ELE004@")).trim());
    OBJ_85.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ELE005@")).trim());
    OBJ_86.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ELE006@")).trim());
    OBJ_91.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ELE007@")).trim());
    OBJ_92.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ELE008@")).trim());
    OBJ_93.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ELE009@")).trim());
    OBJ_98.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ELE010@")).trim());
    OBJ_99.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ELE011@")).trim());
    OBJ_100.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ELE012@")).trim());
    OBJ_19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ELL010@")).trim());
    OBJ_106.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ELL001@")).trim());
    OBJ_107.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ELL002@")).trim());
    OBJ_108.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ELL003@")).trim());
    OBJ_109.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ELL004@")).trim());
    OBJ_110.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ELL005@")).trim());
    OBJ_111.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ELL006@")).trim());
    OBJ_112.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ELL007@")).trim());
    OBJ_113.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ELL008@")).trim());
    OBJ_114.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ELL009@")).trim());
    OBJ_115.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ELL011@")).trim());
    OBJ_116.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ELL012@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    EAL012.setEnabled(lexique.isPresent("EAL012"));
    EAL011.setEnabled(lexique.isPresent("EAL011"));
    EAL010.setEnabled(lexique.isPresent("EAL010"));
    EAL009.setEnabled(lexique.isPresent("EAL009"));
    EAL008.setEnabled(lexique.isPresent("EAL008"));
    EAL007.setEnabled(lexique.isPresent("EAL007"));
    EAL006.setEnabled(lexique.isPresent("EAL006"));
    EAL005.setEnabled(lexique.isPresent("EAL005"));
    EAL004.setEnabled(lexique.isPresent("EAL004"));
    EAL003.setEnabled(lexique.isPresent("EAL003"));
    EAL002.setEnabled(lexique.isPresent("EAL002"));
    EAL001.setEnabled(lexique.isPresent("EAL001"));
    EAE012.setEnabled(lexique.isPresent("EAE012"));
    EAE011.setEnabled(lexique.isPresent("EAE011"));
    EAE010.setEnabled(lexique.isPresent("EAE010"));
    EAE009.setEnabled(lexique.isPresent("EAE009"));
    EAE008.setEnabled(lexique.isPresent("EAE008"));
    EAE007.setEnabled(lexique.isPresent("EAE007"));
    EAE006.setEnabled(lexique.isPresent("EAE006"));
    EAE005.setEnabled(lexique.isPresent("EAE005"));
    EAE004.setEnabled(lexique.isPresent("EAE004"));
    EAE003.setEnabled(lexique.isPresent("EAE003"));
    EAE002.setEnabled(lexique.isPresent("EAE002"));
    EAE001.setEnabled(lexique.isPresent("EAE001"));
    // EAFCSV.setSelected(lexique.HostFieldGetData("EAFCSV").equalsIgnoreCase("1"));
    // EAFTXT.setSelected(lexique.HostFieldGetData("EAFTXT").equalsIgnoreCase("1"));
    EANFRS.setEnabled(lexique.isPresent("EANFRS"));
    OBJ_116.setVisible(lexique.isPresent("ELL012"));
    OBJ_115.setVisible(lexique.isPresent("ELL011"));
    OBJ_114.setVisible(lexique.isPresent("ELL009"));
    OBJ_113.setVisible(lexique.isPresent("ELL008"));
    OBJ_112.setVisible(lexique.isPresent("ELL007"));
    OBJ_111.setVisible(lexique.isPresent("ELL006"));
    OBJ_110.setVisible(lexique.isPresent("ELL005"));
    OBJ_109.setVisible(lexique.isPresent("ELL004"));
    OBJ_108.setVisible(lexique.isPresent("ELL003"));
    OBJ_107.setVisible(lexique.isPresent("ELL002"));
    OBJ_106.setVisible(lexique.isPresent("ELL001"));
    OBJ_100.setVisible(lexique.isPresent("ELE012"));
    OBJ_99.setVisible(lexique.isPresent("ELE011"));
    OBJ_98.setVisible(lexique.isPresent("ELE010"));
    OBJ_93.setVisible(lexique.isPresent("ELE009"));
    OBJ_92.setVisible(lexique.isPresent("ELE008"));
    OBJ_91.setVisible(lexique.isPresent("ELE007"));
    OBJ_86.setVisible(lexique.isPresent("ELE006"));
    OBJ_85.setVisible(lexique.isPresent("ELE005"));
    OBJ_84.setVisible(lexique.isPresent("ELE004"));
    OBJ_81.setVisible(lexique.isPresent("ELE003"));
    OBJ_80.setVisible(lexique.isPresent("ELE002"));
    OBJ_79.setVisible(lexique.isPresent("ELE001"));
    OBJ_19.setVisible(lexique.isPresent("ELL010"));
    EANFCH.setEnabled(lexique.isPresent("EANFCH"));
    OBJ_76.setVisible(lexique.isPresent("FRSNOM"));
    EALIB.setEnabled(lexique.isPresent("EALIB"));
    EAMAIL.setEnabled(lexique.isPresent("EAMAIL"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LIBGRP/+1/@ - @TITRE@"));
    
    

    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (EAFCSV.isSelected())
    // lexique.HostFieldPutData("EAFCSV", 0, "1");
    // else
    // lexique.HostFieldPutData("EAFCSV", 0, " ");
    // if (EAFTXT.isSelected())
    // lexique.HostFieldPutData("EAFTXT", 0, "1");
    // else
    // lexique.HostFieldPutData("EAFTXT", 0, " ");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_48 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    panel1 = new JPanel();
    EALIB = new XRiTextField();
    OBJ_76 = new RiZoneSortie();
    EANFCH = new XRiTextField();
    OBJ_75 = new JLabel();
    OBJ_78 = new JLabel();
    OBJ_77 = new JLabel();
    EANFRS = new XRiTextField();
    EAFTXT = new XRiCheckBox();
    EAFCSV = new XRiCheckBox();
    OBJ_74 = new JLabel();
    panel2 = new JPanel();
    OBJ_79 = new RiZoneSortie();
    OBJ_80 = new RiZoneSortie();
    OBJ_81 = new RiZoneSortie();
    OBJ_84 = new RiZoneSortie();
    OBJ_85 = new RiZoneSortie();
    OBJ_86 = new RiZoneSortie();
    OBJ_91 = new RiZoneSortie();
    OBJ_92 = new RiZoneSortie();
    OBJ_93 = new RiZoneSortie();
    OBJ_98 = new RiZoneSortie();
    OBJ_99 = new RiZoneSortie();
    OBJ_100 = new RiZoneSortie();
    EAE001 = new XRiTextField();
    EAE002 = new XRiTextField();
    EAE003 = new XRiTextField();
    EAE004 = new XRiTextField();
    EAE005 = new XRiTextField();
    EAE006 = new XRiTextField();
    EAE007 = new XRiTextField();
    EAE008 = new XRiTextField();
    EAE009 = new XRiTextField();
    EAE010 = new XRiTextField();
    EAE011 = new XRiTextField();
    EAE012 = new XRiTextField();
    OBJ_102 = new JLabel();
    OBJ_103 = new JLabel();
    OBJ_104 = new JLabel();
    OBJ_87 = new JLabel();
    OBJ_88 = new JLabel();
    OBJ_89 = new JLabel();
    OBJ_90 = new JLabel();
    OBJ_94 = new JLabel();
    OBJ_95 = new JLabel();
    OBJ_96 = new JLabel();
    OBJ_97 = new JLabel();
    OBJ_101 = new JLabel();
    panel3 = new JPanel();
    OBJ_19 = new RiZoneSortie();
    OBJ_106 = new RiZoneSortie();
    OBJ_107 = new RiZoneSortie();
    OBJ_108 = new RiZoneSortie();
    OBJ_109 = new RiZoneSortie();
    OBJ_110 = new RiZoneSortie();
    OBJ_111 = new RiZoneSortie();
    OBJ_112 = new RiZoneSortie();
    OBJ_113 = new RiZoneSortie();
    OBJ_114 = new RiZoneSortie();
    OBJ_115 = new RiZoneSortie();
    OBJ_116 = new RiZoneSortie();
    EAL001 = new XRiTextField();
    EAL002 = new XRiTextField();
    EAL003 = new XRiTextField();
    EAL004 = new XRiTextField();
    EAL005 = new XRiTextField();
    EAL006 = new XRiTextField();
    EAL007 = new XRiTextField();
    EAL008 = new XRiTextField();
    EAL009 = new XRiTextField();
    EAL010 = new XRiTextField();
    EAL011 = new XRiTextField();
    EAL012 = new XRiTextField();
    OBJ_129 = new JLabel();
    OBJ_130 = new JLabel();
    OBJ_131 = new JLabel();
    OBJ_120 = new JLabel();
    OBJ_121 = new JLabel();
    OBJ_122 = new JLabel();
    OBJ_123 = new JLabel();
    OBJ_124 = new JLabel();
    OBJ_125 = new JLabel();
    OBJ_126 = new JLabel();
    OBJ_127 = new JLabel();
    OBJ_128 = new JLabel();
    OBJ_118 = new JLabel();
    EAMAIL = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des achats");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44 ----
          OBJ_44.setText("Etablissement");
          OBJ_44.setName("OBJ_44");

          //---- INDETB ----
          INDETB.setName("INDETB");

          //---- OBJ_46 ----
          OBJ_46.setText("Code");
          OBJ_46.setName("OBJ_46");

          //---- INDTYP ----
          INDTYP.setName("INDTYP");

          //---- OBJ_48 ----
          OBJ_48.setText("Ordre");
          OBJ_48.setName("OBJ_48");

          //---- INDIND ----
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(513, Short.MAX_VALUE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(880, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("El\u00e9ment de frais");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //======== panel1 ========
            {
              panel1.setBorder(new TitledBorder(""));
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- EALIB ----
              EALIB.setComponentPopupMenu(BTD);
              EALIB.setName("EALIB");
              panel1.add(EALIB);
              EALIB.setBounds(145, 15, 310, EALIB.getPreferredSize().height);

              //---- OBJ_76 ----
              OBJ_76.setText("@FRSNOM@");
              OBJ_76.setName("OBJ_76");
              panel1.add(OBJ_76);
              OBJ_76.setBounds(224, 47, 231, OBJ_76.getPreferredSize().height);

              //---- EANFCH ----
              EANFCH.setComponentPopupMenu(BTD);
              EANFCH.setName("EANFCH");
              panel1.add(EANFCH);
              EANFCH.setBounds(145, 75, 210, EANFCH.getPreferredSize().height);

              //---- OBJ_75 ----
              OBJ_75.setText("N\u00b0 fournisseur");
              OBJ_75.setName("OBJ_75");
              panel1.add(OBJ_75);
              OBJ_75.setBounds(40, 49, 105, 20);

              //---- OBJ_78 ----
              OBJ_78.setText("Type de fichier");
              OBJ_78.setName("OBJ_78");
              panel1.add(OBJ_78);
              OBJ_78.setBounds(465, 79, 100, 20);

              //---- OBJ_77 ----
              OBJ_77.setText("Nom du fichier");
              OBJ_77.setName("OBJ_77");
              panel1.add(OBJ_77);
              OBJ_77.setBounds(40, 79, 105, 20);

              //---- EANFRS ----
              EANFRS.setComponentPopupMenu(BTD);
              EANFRS.setName("EANFRS");
              panel1.add(EANFRS);
              EANFRS.setBounds(145, 45, 66, EANFRS.getPreferredSize().height);

              //---- EAFTXT ----
              EAFTXT.setText("TXT");
              EAFTXT.setComponentPopupMenu(BTD);
              EAFTXT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              EAFTXT.setName("EAFTXT");
              panel1.add(EAFTXT);
              EAFTXT.setBounds(630, 79, 45, 20);

              //---- EAFCSV ----
              EAFCSV.setText("CSV");
              EAFCSV.setComponentPopupMenu(BTD);
              EAFCSV.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              EAFCSV.setName("EAFCSV");
              panel1.add(EAFCSV);
              EAFCSV.setBounds(565, 79, 55, 20);

              //---- OBJ_74 ----
              OBJ_74.setText("Libell\u00e9");
              OBJ_74.setName("OBJ_74");
              panel1.add(OBJ_74);
              OBJ_74.setBounds(40, 19, 105, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel1);
            panel1.setBounds(20, 10, 795, 120);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("Zones Ent\u00eate \u00e0 int\u00e9grer dans le fichier d'extraction"));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- OBJ_79 ----
              OBJ_79.setText("@ELE001@");
              OBJ_79.setName("OBJ_79");
              panel2.add(OBJ_79);
              OBJ_79.setBounds(120, 42, 135, OBJ_79.getPreferredSize().height);

              //---- OBJ_80 ----
              OBJ_80.setText("@ELE002@");
              OBJ_80.setName("OBJ_80");
              panel2.add(OBJ_80);
              OBJ_80.setBounds(370, 42, 135, OBJ_80.getPreferredSize().height);

              //---- OBJ_81 ----
              OBJ_81.setText("@ELE003@");
              OBJ_81.setName("OBJ_81");
              panel2.add(OBJ_81);
              OBJ_81.setBounds(620, 42, 135, OBJ_81.getPreferredSize().height);

              //---- OBJ_84 ----
              OBJ_84.setText("@ELE004@");
              OBJ_84.setName("OBJ_84");
              panel2.add(OBJ_84);
              OBJ_84.setBounds(120, 70, 135, OBJ_84.getPreferredSize().height);

              //---- OBJ_85 ----
              OBJ_85.setText("@ELE005@");
              OBJ_85.setName("OBJ_85");
              panel2.add(OBJ_85);
              OBJ_85.setBounds(370, 70, 135, OBJ_85.getPreferredSize().height);

              //---- OBJ_86 ----
              OBJ_86.setText("@ELE006@");
              OBJ_86.setName("OBJ_86");
              panel2.add(OBJ_86);
              OBJ_86.setBounds(620, 70, 135, OBJ_86.getPreferredSize().height);

              //---- OBJ_91 ----
              OBJ_91.setText("@ELE007@");
              OBJ_91.setName("OBJ_91");
              panel2.add(OBJ_91);
              OBJ_91.setBounds(120, 98, 135, OBJ_91.getPreferredSize().height);

              //---- OBJ_92 ----
              OBJ_92.setText("@ELE008@");
              OBJ_92.setName("OBJ_92");
              panel2.add(OBJ_92);
              OBJ_92.setBounds(370, 98, 135, OBJ_92.getPreferredSize().height);

              //---- OBJ_93 ----
              OBJ_93.setText("@ELE009@");
              OBJ_93.setName("OBJ_93");
              panel2.add(OBJ_93);
              OBJ_93.setBounds(620, 98, 135, OBJ_93.getPreferredSize().height);

              //---- OBJ_98 ----
              OBJ_98.setText("@ELE010@");
              OBJ_98.setName("OBJ_98");
              panel2.add(OBJ_98);
              OBJ_98.setBounds(120, 126, 135, OBJ_98.getPreferredSize().height);

              //---- OBJ_99 ----
              OBJ_99.setText("@ELE011@");
              OBJ_99.setName("OBJ_99");
              panel2.add(OBJ_99);
              OBJ_99.setBounds(370, 126, 135, OBJ_99.getPreferredSize().height);

              //---- OBJ_100 ----
              OBJ_100.setText("@ELE012@");
              OBJ_100.setName("OBJ_100");
              panel2.add(OBJ_100);
              OBJ_100.setBounds(620, 126, 135, OBJ_100.getPreferredSize().height);

              //---- EAE001 ----
              EAE001.setComponentPopupMenu(BTD);
              EAE001.setName("EAE001");
              panel2.add(EAE001);
              EAE001.setBounds(65, 40, 50, EAE001.getPreferredSize().height);

              //---- EAE002 ----
              EAE002.setComponentPopupMenu(BTD);
              EAE002.setName("EAE002");
              panel2.add(EAE002);
              EAE002.setBounds(315, 40, 50, EAE002.getPreferredSize().height);

              //---- EAE003 ----
              EAE003.setComponentPopupMenu(BTD);
              EAE003.setName("EAE003");
              panel2.add(EAE003);
              EAE003.setBounds(565, 40, 50, EAE003.getPreferredSize().height);

              //---- EAE004 ----
              EAE004.setComponentPopupMenu(BTD);
              EAE004.setName("EAE004");
              panel2.add(EAE004);
              EAE004.setBounds(65, 68, 50, EAE004.getPreferredSize().height);

              //---- EAE005 ----
              EAE005.setComponentPopupMenu(BTD);
              EAE005.setName("EAE005");
              panel2.add(EAE005);
              EAE005.setBounds(315, 68, 50, EAE005.getPreferredSize().height);

              //---- EAE006 ----
              EAE006.setComponentPopupMenu(BTD);
              EAE006.setName("EAE006");
              panel2.add(EAE006);
              EAE006.setBounds(565, 68, 50, EAE006.getPreferredSize().height);

              //---- EAE007 ----
              EAE007.setComponentPopupMenu(BTD);
              EAE007.setName("EAE007");
              panel2.add(EAE007);
              EAE007.setBounds(65, 96, 50, EAE007.getPreferredSize().height);

              //---- EAE008 ----
              EAE008.setComponentPopupMenu(BTD);
              EAE008.setName("EAE008");
              panel2.add(EAE008);
              EAE008.setBounds(315, 96, 50, EAE008.getPreferredSize().height);

              //---- EAE009 ----
              EAE009.setComponentPopupMenu(BTD);
              EAE009.setName("EAE009");
              panel2.add(EAE009);
              EAE009.setBounds(565, 96, 50, EAE009.getPreferredSize().height);

              //---- EAE010 ----
              EAE010.setComponentPopupMenu(BTD);
              EAE010.setName("EAE010");
              panel2.add(EAE010);
              EAE010.setBounds(65, 124, 50, EAE010.getPreferredSize().height);

              //---- EAE011 ----
              EAE011.setComponentPopupMenu(BTD);
              EAE011.setName("EAE011");
              panel2.add(EAE011);
              EAE011.setBounds(315, 124, 50, EAE011.getPreferredSize().height);

              //---- EAE012 ----
              EAE012.setComponentPopupMenu(BTD);
              EAE012.setName("EAE012");
              panel2.add(EAE012);
              EAE012.setBounds(565, 124, 50, EAE012.getPreferredSize().height);

              //---- OBJ_102 ----
              OBJ_102.setText("10");
              OBJ_102.setName("OBJ_102");
              panel2.add(OBJ_102);
              OBJ_102.setBounds(40, 128, 17, 20);

              //---- OBJ_103 ----
              OBJ_103.setText("11");
              OBJ_103.setName("OBJ_103");
              panel2.add(OBJ_103);
              OBJ_103.setBounds(285, 128, 17, 20);

              //---- OBJ_104 ----
              OBJ_104.setText("12");
              OBJ_104.setName("OBJ_104");
              panel2.add(OBJ_104);
              OBJ_104.setBounds(540, 128, 17, 20);

              //---- OBJ_87 ----
              OBJ_87.setText("1");
              OBJ_87.setName("OBJ_87");
              panel2.add(OBJ_87);
              OBJ_87.setBounds(40, 44, 10, 20);

              //---- OBJ_88 ----
              OBJ_88.setText("2");
              OBJ_88.setName("OBJ_88");
              panel2.add(OBJ_88);
              OBJ_88.setBounds(285, 44, 10, 20);

              //---- OBJ_89 ----
              OBJ_89.setText("3");
              OBJ_89.setName("OBJ_89");
              panel2.add(OBJ_89);
              OBJ_89.setBounds(540, 44, 10, 20);

              //---- OBJ_90 ----
              OBJ_90.setText("4");
              OBJ_90.setName("OBJ_90");
              panel2.add(OBJ_90);
              OBJ_90.setBounds(40, 72, 10, 20);

              //---- OBJ_94 ----
              OBJ_94.setText("5");
              OBJ_94.setName("OBJ_94");
              panel2.add(OBJ_94);
              OBJ_94.setBounds(285, 72, 10, 20);

              //---- OBJ_95 ----
              OBJ_95.setText("6");
              OBJ_95.setName("OBJ_95");
              panel2.add(OBJ_95);
              OBJ_95.setBounds(540, 72, 10, 20);

              //---- OBJ_96 ----
              OBJ_96.setText("7");
              OBJ_96.setName("OBJ_96");
              panel2.add(OBJ_96);
              OBJ_96.setBounds(40, 100, 10, 20);

              //---- OBJ_97 ----
              OBJ_97.setText("8");
              OBJ_97.setName("OBJ_97");
              panel2.add(OBJ_97);
              OBJ_97.setBounds(285, 100, 10, 20);

              //---- OBJ_101 ----
              OBJ_101.setText("9");
              OBJ_101.setName("OBJ_101");
              panel2.add(OBJ_101);
              OBJ_101.setBounds(540, 100, 10, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel2);
            panel2.setBounds(20, 135, 795, 170);

            //======== panel3 ========
            {
              panel3.setBorder(new TitledBorder("Zones Ligne  \u00e0 int\u00e9grer dans le fichier d'extraction"));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- OBJ_19 ----
              OBJ_19.setText("@ELL010@");
              OBJ_19.setName("OBJ_19");
              panel3.add(OBJ_19);
              OBJ_19.setBounds(120, 126, 135, OBJ_19.getPreferredSize().height);

              //---- OBJ_106 ----
              OBJ_106.setText("@ELL001@");
              OBJ_106.setName("OBJ_106");
              panel3.add(OBJ_106);
              OBJ_106.setBounds(120, 42, 135, OBJ_106.getPreferredSize().height);

              //---- OBJ_107 ----
              OBJ_107.setText("@ELL002@");
              OBJ_107.setName("OBJ_107");
              panel3.add(OBJ_107);
              OBJ_107.setBounds(370, 42, 135, OBJ_107.getPreferredSize().height);

              //---- OBJ_108 ----
              OBJ_108.setText("@ELL003@");
              OBJ_108.setName("OBJ_108");
              panel3.add(OBJ_108);
              OBJ_108.setBounds(620, 42, 135, OBJ_108.getPreferredSize().height);

              //---- OBJ_109 ----
              OBJ_109.setText("@ELL004@");
              OBJ_109.setName("OBJ_109");
              panel3.add(OBJ_109);
              OBJ_109.setBounds(120, 70, 135, OBJ_109.getPreferredSize().height);

              //---- OBJ_110 ----
              OBJ_110.setText("@ELL005@");
              OBJ_110.setName("OBJ_110");
              panel3.add(OBJ_110);
              OBJ_110.setBounds(370, 70, 135, OBJ_110.getPreferredSize().height);

              //---- OBJ_111 ----
              OBJ_111.setText("@ELL006@");
              OBJ_111.setName("OBJ_111");
              panel3.add(OBJ_111);
              OBJ_111.setBounds(620, 70, 135, OBJ_111.getPreferredSize().height);

              //---- OBJ_112 ----
              OBJ_112.setText("@ELL007@");
              OBJ_112.setName("OBJ_112");
              panel3.add(OBJ_112);
              OBJ_112.setBounds(120, 98, 135, OBJ_112.getPreferredSize().height);

              //---- OBJ_113 ----
              OBJ_113.setText("@ELL008@");
              OBJ_113.setName("OBJ_113");
              panel3.add(OBJ_113);
              OBJ_113.setBounds(370, 98, 135, OBJ_113.getPreferredSize().height);

              //---- OBJ_114 ----
              OBJ_114.setText("@ELL009@");
              OBJ_114.setName("OBJ_114");
              panel3.add(OBJ_114);
              OBJ_114.setBounds(620, 98, 135, OBJ_114.getPreferredSize().height);

              //---- OBJ_115 ----
              OBJ_115.setText("@ELL011@");
              OBJ_115.setName("OBJ_115");
              panel3.add(OBJ_115);
              OBJ_115.setBounds(370, 126, 135, OBJ_115.getPreferredSize().height);

              //---- OBJ_116 ----
              OBJ_116.setText("@ELL012@");
              OBJ_116.setName("OBJ_116");
              panel3.add(OBJ_116);
              OBJ_116.setBounds(620, 126, 135, OBJ_116.getPreferredSize().height);

              //---- EAL001 ----
              EAL001.setComponentPopupMenu(BTD);
              EAL001.setName("EAL001");
              panel3.add(EAL001);
              EAL001.setBounds(65, 40, 50, EAL001.getPreferredSize().height);

              //---- EAL002 ----
              EAL002.setComponentPopupMenu(BTD);
              EAL002.setName("EAL002");
              panel3.add(EAL002);
              EAL002.setBounds(315, 40, 50, EAL002.getPreferredSize().height);

              //---- EAL003 ----
              EAL003.setComponentPopupMenu(BTD);
              EAL003.setName("EAL003");
              panel3.add(EAL003);
              EAL003.setBounds(565, 40, 50, EAL003.getPreferredSize().height);

              //---- EAL004 ----
              EAL004.setComponentPopupMenu(BTD);
              EAL004.setName("EAL004");
              panel3.add(EAL004);
              EAL004.setBounds(65, 68, 50, EAL004.getPreferredSize().height);

              //---- EAL005 ----
              EAL005.setComponentPopupMenu(BTD);
              EAL005.setName("EAL005");
              panel3.add(EAL005);
              EAL005.setBounds(315, 68, 50, EAL005.getPreferredSize().height);

              //---- EAL006 ----
              EAL006.setComponentPopupMenu(BTD);
              EAL006.setName("EAL006");
              panel3.add(EAL006);
              EAL006.setBounds(565, 68, 50, EAL006.getPreferredSize().height);

              //---- EAL007 ----
              EAL007.setComponentPopupMenu(BTD);
              EAL007.setName("EAL007");
              panel3.add(EAL007);
              EAL007.setBounds(65, 96, 50, EAL007.getPreferredSize().height);

              //---- EAL008 ----
              EAL008.setComponentPopupMenu(BTD);
              EAL008.setName("EAL008");
              panel3.add(EAL008);
              EAL008.setBounds(315, 96, 50, EAL008.getPreferredSize().height);

              //---- EAL009 ----
              EAL009.setComponentPopupMenu(BTD);
              EAL009.setName("EAL009");
              panel3.add(EAL009);
              EAL009.setBounds(565, 96, 50, EAL009.getPreferredSize().height);

              //---- EAL010 ----
              EAL010.setComponentPopupMenu(BTD);
              EAL010.setName("EAL010");
              panel3.add(EAL010);
              EAL010.setBounds(65, 124, 50, EAL010.getPreferredSize().height);

              //---- EAL011 ----
              EAL011.setComponentPopupMenu(BTD);
              EAL011.setName("EAL011");
              panel3.add(EAL011);
              EAL011.setBounds(315, 124, 50, EAL011.getPreferredSize().height);

              //---- EAL012 ----
              EAL012.setComponentPopupMenu(BTD);
              EAL012.setName("EAL012");
              panel3.add(EAL012);
              EAL012.setBounds(565, 124, 50, EAL012.getPreferredSize().height);

              //---- OBJ_129 ----
              OBJ_129.setText("10");
              OBJ_129.setName("OBJ_129");
              panel3.add(OBJ_129);
              OBJ_129.setBounds(40, 128, 17, 20);

              //---- OBJ_130 ----
              OBJ_130.setText("11");
              OBJ_130.setName("OBJ_130");
              panel3.add(OBJ_130);
              OBJ_130.setBounds(285, 128, 17, 20);

              //---- OBJ_131 ----
              OBJ_131.setText("12");
              OBJ_131.setName("OBJ_131");
              panel3.add(OBJ_131);
              OBJ_131.setBounds(540, 128, 17, 20);

              //---- OBJ_120 ----
              OBJ_120.setText("1");
              OBJ_120.setName("OBJ_120");
              panel3.add(OBJ_120);
              OBJ_120.setBounds(40, 44, 10, 20);

              //---- OBJ_121 ----
              OBJ_121.setText("2");
              OBJ_121.setName("OBJ_121");
              panel3.add(OBJ_121);
              OBJ_121.setBounds(285, 44, 10, 20);

              //---- OBJ_122 ----
              OBJ_122.setText("3");
              OBJ_122.setName("OBJ_122");
              panel3.add(OBJ_122);
              OBJ_122.setBounds(540, 44, 10, 20);

              //---- OBJ_123 ----
              OBJ_123.setText("4");
              OBJ_123.setName("OBJ_123");
              panel3.add(OBJ_123);
              OBJ_123.setBounds(40, 72, 10, 20);

              //---- OBJ_124 ----
              OBJ_124.setText("5");
              OBJ_124.setName("OBJ_124");
              panel3.add(OBJ_124);
              OBJ_124.setBounds(285, 72, 10, 20);

              //---- OBJ_125 ----
              OBJ_125.setText("6");
              OBJ_125.setName("OBJ_125");
              panel3.add(OBJ_125);
              OBJ_125.setBounds(540, 72, 10, 20);

              //---- OBJ_126 ----
              OBJ_126.setText("7");
              OBJ_126.setName("OBJ_126");
              panel3.add(OBJ_126);
              OBJ_126.setBounds(40, 100, 10, 20);

              //---- OBJ_127 ----
              OBJ_127.setText("8");
              OBJ_127.setName("OBJ_127");
              panel3.add(OBJ_127);
              OBJ_127.setBounds(285, 100, 10, 20);

              //---- OBJ_128 ----
              OBJ_128.setText("9");
              OBJ_128.setName("OBJ_128");
              panel3.add(OBJ_128);
              OBJ_128.setBounds(540, 100, 10, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel3);
            panel3.setBounds(20, 305, 795, 170);

            //---- OBJ_118 ----
            OBJ_118.setText("Adresse Email du destinataire du fichier (uniquement si messagerie OUTLOOK)");
            OBJ_118.setName("OBJ_118");
            xTitledPanel1ContentContainer.add(OBJ_118);
            OBJ_118.setBounds(25, 475, 575, 20);

            //---- EAMAIL ----
            EAMAIL.setComponentPopupMenu(BTD);
            EAMAIL.setName("EAMAIL");
            xTitledPanel1ContentContainer.add(EAMAIL);
            EAMAIL.setBounds(25, 495, 790, EAMAIL.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 847, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(19, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 570, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(15, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44;
  private XRiTextField INDETB;
  private JLabel OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_48;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JPanel panel1;
  private XRiTextField EALIB;
  private RiZoneSortie OBJ_76;
  private XRiTextField EANFCH;
  private JLabel OBJ_75;
  private JLabel OBJ_78;
  private JLabel OBJ_77;
  private XRiTextField EANFRS;
  private XRiCheckBox EAFTXT;
  private XRiCheckBox EAFCSV;
  private JLabel OBJ_74;
  private JPanel panel2;
  private RiZoneSortie OBJ_79;
  private RiZoneSortie OBJ_80;
  private RiZoneSortie OBJ_81;
  private RiZoneSortie OBJ_84;
  private RiZoneSortie OBJ_85;
  private RiZoneSortie OBJ_86;
  private RiZoneSortie OBJ_91;
  private RiZoneSortie OBJ_92;
  private RiZoneSortie OBJ_93;
  private RiZoneSortie OBJ_98;
  private RiZoneSortie OBJ_99;
  private RiZoneSortie OBJ_100;
  private XRiTextField EAE001;
  private XRiTextField EAE002;
  private XRiTextField EAE003;
  private XRiTextField EAE004;
  private XRiTextField EAE005;
  private XRiTextField EAE006;
  private XRiTextField EAE007;
  private XRiTextField EAE008;
  private XRiTextField EAE009;
  private XRiTextField EAE010;
  private XRiTextField EAE011;
  private XRiTextField EAE012;
  private JLabel OBJ_102;
  private JLabel OBJ_103;
  private JLabel OBJ_104;
  private JLabel OBJ_87;
  private JLabel OBJ_88;
  private JLabel OBJ_89;
  private JLabel OBJ_90;
  private JLabel OBJ_94;
  private JLabel OBJ_95;
  private JLabel OBJ_96;
  private JLabel OBJ_97;
  private JLabel OBJ_101;
  private JPanel panel3;
  private RiZoneSortie OBJ_19;
  private RiZoneSortie OBJ_106;
  private RiZoneSortie OBJ_107;
  private RiZoneSortie OBJ_108;
  private RiZoneSortie OBJ_109;
  private RiZoneSortie OBJ_110;
  private RiZoneSortie OBJ_111;
  private RiZoneSortie OBJ_112;
  private RiZoneSortie OBJ_113;
  private RiZoneSortie OBJ_114;
  private RiZoneSortie OBJ_115;
  private RiZoneSortie OBJ_116;
  private XRiTextField EAL001;
  private XRiTextField EAL002;
  private XRiTextField EAL003;
  private XRiTextField EAL004;
  private XRiTextField EAL005;
  private XRiTextField EAL006;
  private XRiTextField EAL007;
  private XRiTextField EAL008;
  private XRiTextField EAL009;
  private XRiTextField EAL010;
  private XRiTextField EAL011;
  private XRiTextField EAL012;
  private JLabel OBJ_129;
  private JLabel OBJ_130;
  private JLabel OBJ_131;
  private JLabel OBJ_120;
  private JLabel OBJ_121;
  private JLabel OBJ_122;
  private JLabel OBJ_123;
  private JLabel OBJ_124;
  private JLabel OBJ_125;
  private JLabel OBJ_126;
  private JLabel OBJ_127;
  private JLabel OBJ_128;
  private JLabel OBJ_118;
  private XRiTextField EAMAIL;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
