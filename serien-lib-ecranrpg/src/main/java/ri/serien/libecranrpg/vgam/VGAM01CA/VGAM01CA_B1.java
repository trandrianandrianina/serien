
package ri.serien.libecranrpg.vgam.VGAM01CA;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGAM01CA_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGAM01CA_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    article1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCOA1@")).trim());
    article2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCOA2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Titre
    setTitle(interpreteurD.analyseExpression("PERSONNALISATION"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    xTitledPanel3 = new JXTitledPanel();
    panel2 = new JPanel();
    OBJ_21 = new JLabel();
    OBJ_37 = new JLabel();
    OBJ_22 = new JLabel();
    OBJ_24 = new JLabel();
    CACOA1 = new XRiTextField();
    CAJOA1 = new XRiTextField();
    article1 = new RiZoneSortie();
    panel3 = new JPanel();
    OBJ_31 = new JLabel();
    OBJ_27 = new JLabel();
    OBJ_38 = new JLabel();
    OBJ_28 = new JLabel();
    CACOA2 = new XRiTextField();
    CAJOA2 = new XRiTextField();
    article2 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(840, 265));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== xTitledPanel3 ========
        {
          xTitledPanel3.setTitle("Comptabilisation des achats par anticipation");
          xTitledPanel3.setBorder(new DropShadowBorder());
          xTitledPanel3.setTitleFont(new Font("sansserif", Font.BOLD, 12));
          xTitledPanel3.setName("xTitledPanel3");
          Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
          xTitledPanel3ContentContainer.setLayout(null);

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Bons Homologu\u00e9s"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_21 ----
            OBJ_21.setText("Libell\u00e9");
            OBJ_21.setFont(new Font("Courier New", Font.BOLD, 12));
            OBJ_21.setName("OBJ_21");
            panel2.add(OBJ_21);
            OBJ_21.setBounds(175, 30, 61, 18);

            //---- OBJ_37 ----
            OBJ_37.setText("Compte");
            OBJ_37.setFont(new Font("Courier New", Font.BOLD, 12));
            OBJ_37.setName("OBJ_37");
            panel2.add(OBJ_37);
            OBJ_37.setBounds(125, 30, 61, 18);

            //---- OBJ_22 ----
            OBJ_22.setText("N\u00b0CO");
            OBJ_22.setName("OBJ_22");
            panel2.add(OBJ_22);
            OBJ_22.setBounds(15, 55, 36, 20);

            //---- OBJ_24 ----
            OBJ_24.setText("Code Journal");
            OBJ_24.setName("OBJ_24");
            panel2.add(OBJ_24);
            OBJ_24.setBounds(495, 55, 84, 20);

            //---- CACOA1 ----
            CACOA1.setComponentPopupMenu(BTD);
            CACOA1.setName("CACOA1");
            panel2.add(CACOA1);
            CACOA1.setBounds(55, 51, 36, CACOA1.getPreferredSize().height);

            //---- CAJOA1 ----
            CAJOA1.setComponentPopupMenu(BTD);
            CAJOA1.setName("CAJOA1");
            panel2.add(CAJOA1);
            CAJOA1.setBounds(580, 51, 30, CAJOA1.getPreferredSize().height);

            //---- article1 ----
            article1.setText("@LCOA1@");
            article1.setFont(new Font("Courier New", Font.PLAIN, 12));
            article1.setBackground(new Color(235, 230, 235));
            article1.setOpaque(false);
            article1.setVerticalAlignment(SwingConstants.BOTTOM);
            article1.setName("article1");
            panel2.add(article1);
            article1.setBounds(125, 51, 324, article1.getPreferredSize().height);
          }
          xTitledPanel3ContentContainer.add(panel2);
          panel2.setBounds(10, 10, 630, 95);

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder("R\u00e9ceptionn\u00e9s"));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- OBJ_31 ----
            OBJ_31.setText("Code Journal");
            OBJ_31.setName("OBJ_31");
            panel3.add(OBJ_31);
            OBJ_31.setBounds(495, 55, 84, 20);

            //---- OBJ_27 ----
            OBJ_27.setText("Libell\u00e9");
            OBJ_27.setFont(new Font("Courier New", Font.BOLD, 12));
            OBJ_27.setName("OBJ_27");
            panel3.add(OBJ_27);
            OBJ_27.setBounds(180, 30, 61, 18);

            //---- OBJ_38 ----
            OBJ_38.setText("Compte");
            OBJ_38.setFont(new Font("Courier New", Font.BOLD, 12));
            OBJ_38.setName("OBJ_38");
            panel3.add(OBJ_38);
            OBJ_38.setBounds(130, 30, 61, 18);

            //---- OBJ_28 ----
            OBJ_28.setText("N\u00b0CO");
            OBJ_28.setName("OBJ_28");
            panel3.add(OBJ_28);
            OBJ_28.setBounds(15, 55, 36, 20);

            //---- CACOA2 ----
            CACOA2.setComponentPopupMenu(BTD);
            CACOA2.setName("CACOA2");
            panel3.add(CACOA2);
            CACOA2.setBounds(55, 51, 36, CACOA2.getPreferredSize().height);

            //---- CAJOA2 ----
            CAJOA2.setComponentPopupMenu(BTD);
            CAJOA2.setName("CAJOA2");
            panel3.add(CAJOA2);
            CAJOA2.setBounds(580, 51, 30, CAJOA2.getPreferredSize().height);

            //---- article2 ----
            article2.setText("@LCOA2@");
            article2.setFont(new Font("Courier New", Font.PLAIN, 12));
            article2.setBackground(new Color(235, 230, 235));
            article2.setOpaque(false);
            article2.setVerticalAlignment(SwingConstants.BOTTOM);
            article2.setName("article2");
            panel3.add(article2);
            article2.setBounds(130, 51, 324, article2.getPreferredSize().height);
          }
          xTitledPanel3ContentContainer.add(panel3);
          panel3.setBounds(10, 110, 630, 95);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel3ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(xTitledPanel3);
        xTitledPanel3.setBounds(10, 10, 650, 245);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel3;
  private JPanel panel2;
  private JLabel OBJ_21;
  private JLabel OBJ_37;
  private JLabel OBJ_22;
  private JLabel OBJ_24;
  private XRiTextField CACOA1;
  private XRiTextField CAJOA1;
  private RiZoneSortie article1;
  private JPanel panel3;
  private JLabel OBJ_31;
  private JLabel OBJ_27;
  private JLabel OBJ_38;
  private JLabel OBJ_28;
  private XRiTextField CACOA2;
  private XRiTextField CAJOA2;
  private RiZoneSortie article2;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
