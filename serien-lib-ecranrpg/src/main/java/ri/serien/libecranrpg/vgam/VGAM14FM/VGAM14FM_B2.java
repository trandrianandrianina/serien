
package ri.serien.libecranrpg.vgam.VGAM14FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.metier.referentiel.article.snphotoarticle.SNPhotoArticle;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGAM14FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] _LIG41_Title = { "Bon    Date     Qté US  Prix Ach.  Px Rev.Ma ", };
  private String[][] _LIG41_Data = { { "LIG41", }, { "LIG42", }, { "LIG43", }, { "LIG44", }, { "LIG45", }, };
  private int[] _LIG41_Width = { 297, };
  private String[] _LIG31_Title = { "Bon    Dat.Cde  Dat livr    Qté MA", };
  private String[][] _LIG31_Data = { { "LIG31", }, { "LIG32", }, { "LIG33", }, { "LIG34", }, { "LIG35", }, };
  private int[] _LIG31_Width = { 241, };
  private String[] _LIST24_Title = { "Entrées", "", };
  private String[][] _LIST24_Data = { { null, "LIGENT", }, };
  private int[] _LIST24_Width = { 53, 527, };
  private String[] _LIG11_Title = { "Sorties", "TIT1", };
  private String[][] _LIG11_Data =
      { { "@MAG1@ @REA1@", "LIG11", }, { "@MAG2@ @REA2@", "LIG12", }, { "@MAG3@ @REA3@", "LIG13", }, { "Total", "LIGTOT", }, };
  private int[] _LIG11_Width = { 53, 527, };
  private String[] A1REA_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", };
  
  public VGAM14FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    A1REA.setValeurs(A1REA_Value, null);
    LIG41.setAspectTable(null, _LIG41_Title, _LIG41_Data, _LIG41_Width, false, null, null, null, null);
    LIG31.setAspectTable(null, _LIG31_Title, _LIG31_Data, _LIG31_Width, false, null, null, null, null);
    LIST24.setAspectTable(null, _LIST24_Title, _LIST24_Data, _LIST24_Width, true, null, null, null, null);
    LIG11.setAspectTable(null, _LIG11_Title, _LIG11_Data, _LIG11_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDMAG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDMAG@")).trim());
    riZoneSortie1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MALIB@")).trim());
    riZoneSortie2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@S1MAG@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_66.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRNOMA@")).trim());
    OBJ_57.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ULBFRS@")).trim());
    OBJ_59.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAREF@")).trim());
    OBJ_56.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRFRSG@")).trim());
    OBJ_65.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRSGA@")).trim());
    OBJ_148.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ENTEX@")).trim());
    OBJ_143.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOY12@")).trim());
    OBJ_145.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOY6@")).trim());
    OBJ_147.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOY3@")).trim());
    OBJ_149.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@COUV@")).trim());
    OBJ_152.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SORMOI@")).trim());
    OBJ_164.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1ABC@")).trim());
    OBJ_154.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WATTX@")).trim());
    PUMP0.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PUMP0@")).trim());
    PUMP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PUMP@")).trim());
    PRV0.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PRV0@")).trim());
    PRV1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PRV@")).trim());
    RAP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RAP@")).trim());
    MAR1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAR1@")).trim());
    MAR2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAR2@")).trim());
    TAR1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TAR1@")).trim());
    OBJ_92.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TAR10@")).trim());
    TAR2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TAR2@")).trim());
    OBJ_108.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TAR20@")).trim());
    OBJ_77.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSTKX@")).trim());
    OBJ_87.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WRESX1@")).trim());
    OBJ_103.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDISX@")).trim());
    OBJ_80.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOTCMD@")).trim());
    OBJ_81.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PLAFCV@")).trim());
    OBJ_78.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MIN@")).trim());
    OBJ_79.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAX@")).trim());
    CAQMI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAQMI@")).trim());
    OBJ_89.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CANUA@")).trim());
    OBJ_114.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNS@")).trim());
    A1IN20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1IN20@")).trim());
    LRQTEX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRQTEX@")).trim());
    INDAR0.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDAR0@")).trim());
    OBJ_52.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    A1FAM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1FAM@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // majTable( LIST22, LIST22.get_LIST_Title_Data_Brut(), _LIST22_Top);
    // majTable( LIST, LIST.get_LIST_Title_Data_Brut(), _LIST_Top);
    // majTable( LIST2, LIST2.get_LIST_Title_Data_Brut(), _LIST2_Top);
    // majTable( LIST24, LIST24.get_LIST_Title_Data_Brut(), _LIST24_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    boolean isConsult = lexique.isTrue("53");
    riSousMenu26.setEnabled(!isConsult);
    riSousMenu25.setEnabled(!isConsult);
    boolean is24 = lexique.isTrue("24");
    
    
    OBJ_164.setVisible(lexique.isPresent("A1ABC"));
    OBJ_114.setVisible(lexique.isPresent("A1UNS"));
    INDMAG.setEnabled(lexique.isPresent("INDMAG"));
    OBJ_162.setVisible(lexique.isPresent("A1ABC"));
    INDETB.setEnabled(lexique.isPresent("INDETB"));
    OBJ_149.setVisible(lexique.isPresent("COUV"));
    OBJ_79.setVisible(lexique.isPresent("MAX"));
    OBJ_78.setVisible(lexique.isPresent("MIN"));
    OBJ_148.setVisible(lexique.isPresent("ENTEX"));
    OBJ_81.setVisible(lexique.isPresent("PLAFCV"));
    OBJ_80.setVisible(lexique.isPresent("LOTCMD"));
    OBJ_152.setVisible(lexique.isPresent("SORMOI"));
    OBJ_113.setVisible(lexique.isPresent("A1UNS"));
    OBJ_58.setVisible(lexique.isPresent("CAREF"));
    OBJ_65.setVisible(lexique.isPresent("FRSGA"));
    OBJ_108.setVisible(lexique.isPresent("TAR20"));
    OBJ_92.setVisible(lexique.isPresent("TAR10"));
    LRQTSX.setEnabled(lexique.isPresent("LRQTSX"));
    LRQTEX.setVisible(lexique.isPresent("LRQTEX"));
    OBJ_154.setVisible(lexique.isPresent("WATTX"));
    // OBJ_103.setVisible( lexique.isPresent("WDISX"));
    OBJ_87.setVisible(lexique.isPresent("WRESX1"));
    OBJ_77.setVisible(lexique.isPresent("WSTKX"));
    // OBJ_131.setVisible( lexique.isPresent("A1PDP"));
    OBJ_59.setVisible(lexique.isPresent("CAREF"));
    INDAR0.setEnabled(lexique.isPresent("INDAR0"));
    OBJ_57.setVisible(lexique.isPresent("ULBFRS"));
    OBJ_66.setVisible(lexique.isPresent("FRNOMA"));
    OBJ_90.setVisible(OBJ_87.isVisible());
    // A1REA.setSelectedIndex(getIndice("A1REA", A1REA_Value));
    A1REA.setEnabled(!isConsult);
    OBJ_52.setVisible(lexique.isPresent("A1LIB"));
    if (lexique.isTrue("88")) {
      panel3.setBorder(new TitledBorder("Dû Client : " + interpreteurD.analyseExpression("QTDUCL")));
    }
    else {
      panel3.setBorder(new TitledBorder("Commandes attendues :"));
    }
    
    lb_elephant.setVisible(!lexique.HostFieldGetData("ELEPH").trim().equals(""));
    
    // quatre décimales
    if (lexique.isTrue("58")) {
      TAR1.setText(lexique.HostFieldGetData("TAR14"));
      TAR2.setText(lexique.HostFieldGetData("TAR24"));
    }
    else {
      TAR1.setText(lexique.HostFieldGetData("TAR1"));
      TAR2.setText(lexique.HostFieldGetData("TAR2"));
    }
    
    lbPRV.setVisible(!is24);
    PRV0.setVisible(!is24);
    PRV1.setVisible(!is24);
    lbMarge.setVisible(!is24);
    MAR1.setVisible(!is24);
    MAR2.setVisible(!is24);
    lbPump.setVisible(!is24);
    PUMP.setVisible(!is24);
    PUMP0.setVisible(!is24);
    RAP.setVisible(!is24);
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
    riMenu_bt4.setIcon(lexique.chargerImage("images/fonctions.png", true));
    
    // Charger la photographie de l'article
    snPhotoArticle.chargerImageArticle(
        lexique.HostFieldGetData("RACINE").trim() + lexique.HostFieldGetData("CHEM1").trim() + lexique.HostFieldGetData("CHEM2").trim(),
        INDAR0.getText(), lexique.isTrue("N51"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("A1REA", 0, A1REA_Value[A1REA.getSelectedIndex()]);
    // lexique.HostFieldGetData("XXCOL"));
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    // lexique.HostCursorPut("TAR1");
    lexique.HostCursorPut(5, 5);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19");
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18");
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void riSousMenu_bt13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(14, 1);
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void riSousMenu_bt18ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(18, 21);
    lexique.HostScreenSendKey(this, "PGUP");
  }
  
  private void riSousMenu_bt20ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(18, 51);
    lexique.HostScreenSendKey(this, "PGUP");
  }
  
  private void riSousMenu_bt21ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void riSousMenu_bt25ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void riSousMenu_bt26ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(3, 29);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt22ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(2, 29);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt24ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(1, 1);
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche2 = new JPanel();
    INDETB = new RiZoneSortie();
    OBJ_38 = new JLabel();
    INDMAG = new RiZoneSortie();
    OBJ_39 = new JLabel();
    riZoneSortie1 = new RiZoneSortie();
    riZoneSortie2 = new RiZoneSortie();
    label1 = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riSousMenu13 = new RiSousMenu();
    riSousMenu_bt13 = new RiSousMenu_bt();
    riSousMenu22 = new RiSousMenu();
    riSousMenu_bt22 = new RiSousMenu_bt();
    riSousMenu24 = new RiSousMenu();
    riSousMenu_bt24 = new RiSousMenu_bt();
    riSousMenu25 = new RiSousMenu();
    riSousMenu_bt25 = new RiSousMenu_bt();
    riSousMenu26 = new RiSousMenu();
    riSousMenu_bt26 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riMenu4 = new RiMenu();
    riMenu_bt4 = new RiMenu_bt();
    riSousMenu19 = new RiSousMenu();
    riSousMenu_bt19 = new RiSousMenu_bt();
    riSousMenu20 = new RiSousMenu();
    riSousMenu_bt20 = new RiSousMenu_bt();
    riSousMenu27 = new RiSousMenu();
    riSousMenu_bt27 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xtp2 = new JXTitledPanel();
    A1REA = new XRiComboBox();
    OBJ_66 = new RiZoneSortie();
    OBJ_57 = new RiZoneSortie();
    OBJ_59 = new RiZoneSortie();
    OBJ_67 = new JLabel();
    OBJ_56 = new RiZoneSortie();
    OBJ_65 = new RiZoneSortie();
    OBJ_58 = new JLabel();
    xTitledPanel4 = new JXTitledPanel();
    SCROLLPANE_LIST22 = new JScrollPane();
    LIG11 = new XRiTable();
    OBJ_141 = new JLabel();
    OBJ_150 = new JLabel();
    OBJ_148 = new RiZoneSortie();
    OBJ_143 = new RiZoneSortie();
    OBJ_145 = new RiZoneSortie();
    OBJ_147 = new RiZoneSortie();
    OBJ_144 = new JLabel();
    OBJ_146 = new JLabel();
    OBJ_151 = new JLabel();
    OBJ_149 = new RiZoneSortie();
    OBJ_153 = new JLabel();
    OBJ_152 = new RiZoneSortie();
    OBJ_162 = new JLabel();
    OBJ_164 = new RiZoneSortie();
    OBJ_140 = new JLabel();
    panel3 = new JPanel();
    SCROLLPANE_LIST2 = new JScrollPane();
    LIG31 = new XRiTable();
    OBJ_154 = new JLabel();
    panel4 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    LIG41 = new XRiTable();
    scrollPane2 = new JScrollPane();
    LIST24 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    xTitledPanel1 = new JXTitledPanel();
    PUMP0 = new RiZoneSortie();
    PUMP = new RiZoneSortie();
    PRV0 = new RiZoneSortie();
    PRV1 = new RiZoneSortie();
    RAP = new RiZoneSortie();
    lbPump = new JLabel();
    MAR1 = new RiZoneSortie();
    MAR2 = new RiZoneSortie();
    lbTarifs = new JLabel();
    lbPRV = new JLabel();
    lbMarge = new JLabel();
    TAR1 = new RiZoneSortie();
    OBJ_92 = new RiZoneSortie();
    TAR2 = new RiZoneSortie();
    OBJ_108 = new RiZoneSortie();
    xTitledPanel3 = new JXTitledPanel();
    OBJ_77 = new RiZoneSortie();
    OBJ_87 = new RiZoneSortie();
    OBJ_103 = new RiZoneSortie();
    OBJ_99 = new JLabel();
    OBJ_113 = new JLabel();
    OBJ_169 = new JLabel();
    OBJ_80 = new RiZoneSortie();
    OBJ_81 = new RiZoneSortie();
    OBJ_78 = new RiZoneSortie();
    OBJ_79 = new RiZoneSortie();
    CAQMI = new RiZoneSortie();
    OBJ_98 = new JLabel();
    OBJ_89 = new RiZoneSortie();
    OBJ_72 = new JLabel();
    OBJ_70 = new JLabel();
    OBJ_71 = new JLabel();
    OBJ_90 = new JLabel();
    OBJ_105 = new JLabel();
    OBJ_82 = new JLabel();
    OBJ_114 = new RiZoneSortie();
    A1IN20 = new RiZoneSortie();
    OBJ_44 = new JLabel();
    snPhotoArticle = new SNPhotoArticle();
    scrollPane1 = new JScrollPane();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_106 = new JLabel();
    LRQTEX = new RiZoneSortie();
    LRQTSX = new XRiTextField();
    OBJ_115 = new JLabel();
    OBJ_41 = new JLabel();
    INDAR0 = new RiZoneSortie();
    OBJ_52 = new RiZoneSortie();
    OBJ_46 = new JLabel();
    A1FAM = new RiZoneSortie();
    lb_elephant = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    riSousMenu18 = new RiSousMenu();
    riSousMenu_bt18 = new RiSousMenu_bt();
    riSousMenu21 = new RiSousMenu();
    riSousMenu_bt21 = new RiSousMenu_bt();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("R\u00e9approvisionnement");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche2 ========
        {
          p_tete_gauche2.setOpaque(false);
          p_tete_gauche2.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche2.setPreferredSize(new Dimension(1060, 40));
          p_tete_gauche2.setMinimumSize(new Dimension(1060, 40));
          p_tete_gauche2.setName("p_tete_gauche2");
          p_tete_gauche2.setLayout(null);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");
          p_tete_gauche2.add(INDETB);
          INDETB.setBounds(100, 0, 40, INDETB.getPreferredSize().height);

          //---- OBJ_38 ----
          OBJ_38.setText("Etablissement");
          OBJ_38.setName("OBJ_38");
          p_tete_gauche2.add(OBJ_38);
          OBJ_38.setBounds(5, 3, 95, 18);

          //---- INDMAG ----
          INDMAG.setComponentPopupMenu(BTD);
          INDMAG.setOpaque(false);
          INDMAG.setText("@INDMAG@");
          INDMAG.setName("INDMAG");
          p_tete_gauche2.add(INDMAG);
          INDMAG.setBounds(215, 0, 34, INDMAG.getPreferredSize().height);

          //---- OBJ_39 ----
          OBJ_39.setText("Magasin");
          OBJ_39.setName("OBJ_39");
          p_tete_gauche2.add(OBJ_39);
          OBJ_39.setBounds(155, 3, 60, 18);

          //---- riZoneSortie1 ----
          riZoneSortie1.setText("@MALIB@");
          riZoneSortie1.setOpaque(false);
          riZoneSortie1.setName("riZoneSortie1");
          p_tete_gauche2.add(riZoneSortie1);
          riZoneSortie1.setBounds(385, 0, 300, riZoneSortie1.getPreferredSize().height);

          //---- riZoneSortie2 ----
          riZoneSortie2.setText("@S1MAG@");
          riZoneSortie2.setOpaque(false);
          riZoneSortie2.setName("riZoneSortie2");
          p_tete_gauche2.add(riZoneSortie2);
          riZoneSortie2.setBounds(345, 0, 34, riZoneSortie2.getPreferredSize().height);

          //---- label1 ----
          label1.setText("Magasin 2");
          label1.setName("label1");
          p_tete_gauche2.add(label1);
          label1.setBounds(275, 2, 75, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_tete_gauche2.getComponentCount(); i++) {
              Rectangle bounds = p_tete_gauche2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_tete_gauche2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_tete_gauche2.setMinimumSize(preferredSize);
            p_tete_gauche2.setPreferredSize(preferredSize);
          }
        }
        barre_tete.add(p_tete_gauche2);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(90, 0));
          p_tete_droite.setMinimumSize(new Dimension(90, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Liste des tarifs");
              riSousMenu_bt6.setToolTipText("Liste des tarifs");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Affichage stocks");
              riSousMenu_bt7.setToolTipText("Affichage stocks");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Liste des CNA");
              riSousMenu_bt10.setToolTipText("Liste des conditions d'achat");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);

            //======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");

              //---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Pr\u00e9visions de conso.");
              riSousMenu_bt11.setToolTipText("Pr\u00e9visions de consommation");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);

            //======== riSousMenu12 ========
            {
              riSousMenu12.setName("riSousMenu12");

              //---- riSousMenu_bt12 ----
              riSousMenu_bt12.setText("Appel fiche fournisseur");
              riSousMenu_bt12.setToolTipText("Appel fiche fournisseur");
              riSousMenu_bt12.setName("riSousMenu_bt12");
              riSousMenu_bt12.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt12ActionPerformed(e);
                }
              });
              riSousMenu12.add(riSousMenu_bt12);
            }
            menus_haut.add(riSousMenu12);

            //======== riSousMenu13 ========
            {
              riSousMenu13.setName("riSousMenu13");

              //---- riSousMenu_bt13 ----
              riSousMenu_bt13.setText("Condition d'achat");
              riSousMenu_bt13.setToolTipText("Condition d'achat");
              riSousMenu_bt13.setName("riSousMenu_bt13");
              riSousMenu_bt13.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt13ActionPerformed(e);
                }
              });
              riSousMenu13.add(riSousMenu_bt13);
            }
            menus_haut.add(riSousMenu13);

            //======== riSousMenu22 ========
            {
              riSousMenu22.setName("riSousMenu22");

              //---- riSousMenu_bt22 ----
              riSousMenu_bt22.setText("Acces article");
              riSousMenu_bt22.setToolTipText("Acces article");
              riSousMenu_bt22.setName("riSousMenu_bt22");
              riSousMenu_bt22.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt22ActionPerformed(e);
                }
              });
              riSousMenu22.add(riSousMenu_bt22);
            }
            menus_haut.add(riSousMenu22);

            //======== riSousMenu24 ========
            {
              riSousMenu24.setName("riSousMenu24");

              //---- riSousMenu_bt24 ----
              riSousMenu_bt24.setText("Modification stock mini");
              riSousMenu_bt24.setToolTipText("Modification stock minimum");
              riSousMenu_bt24.setName("riSousMenu_bt24");
              riSousMenu_bt24.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt24ActionPerformed(e);
                }
              });
              riSousMenu24.add(riSousMenu_bt24);
            }
            menus_haut.add(riSousMenu24);

            //======== riSousMenu25 ========
            {
              riSousMenu25.setName("riSousMenu25");

              //---- riSousMenu_bt25 ----
              riSousMenu_bt25.setToolTipText("D\u00e9tail attendu / command\u00e9");
              riSousMenu_bt25.setText("Attendu/command\u00e9");
              riSousMenu_bt25.setName("riSousMenu_bt25");
              riSousMenu_bt25.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt25ActionPerformed(e);
                }
              });
              riSousMenu25.add(riSousMenu_bt25);
            }
            menus_haut.add(riSousMenu25);

            //======== riSousMenu26 ========
            {
              riSousMenu26.setName("riSousMenu26");

              //---- riSousMenu_bt26 ----
              riSousMenu_bt26.setToolTipText("Position article");
              riSousMenu_bt26.setText("Position article");
              riSousMenu_bt26.setName("riSousMenu_bt26");
              riSousMenu_bt26.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt26ActionPerformed(e);
                }
              });
              riSousMenu26.add(riSousMenu_bt26);
            }
            menus_haut.add(riSousMenu26);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Bloc-notes CNA");
              riSousMenu_bt9.setToolTipText("Bloc-notes condition d'achat");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Autres vues");
              riSousMenu_bt14.setToolTipText("Autres vues");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);

            //======== riMenu4 ========
            {
              riMenu4.setName("riMenu4");

              //---- riMenu_bt4 ----
              riMenu_bt4.setText("Fonctions");
              riMenu_bt4.setName("riMenu_bt4");
              riMenu4.add(riMenu_bt4);
            }
            menus_haut.add(riMenu4);

            //======== riSousMenu19 ========
            {
              riSousMenu19.setName("riSousMenu19");

              //---- riSousMenu_bt19 ----
              riSousMenu_bt19.setText("Autres attendus");
              riSousMenu_bt19.setToolTipText("Autres attendus");
              riSousMenu_bt19.setName("riSousMenu_bt19");
              riSousMenu_bt19.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt19ActionPerformed(e);
                }
              });
              riSousMenu19.add(riSousMenu_bt19);
            }
            menus_haut.add(riSousMenu19);

            //======== riSousMenu20 ========
            {
              riSousMenu20.setName("riSousMenu20");

              //---- riSousMenu_bt20 ----
              riSousMenu_bt20.setText("Autres achats");
              riSousMenu_bt20.setToolTipText("Autres achats");
              riSousMenu_bt20.setName("riSousMenu_bt20");
              riSousMenu_bt20.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt20ActionPerformed(e);
                }
              });
              riSousMenu20.add(riSousMenu_bt20);
            }
            menus_haut.add(riSousMenu20);

            //======== riSousMenu27 ========
            {
              riSousMenu27.setName("riSousMenu27");

              //---- riSousMenu_bt27 ----
              riSousMenu_bt27.setText("Historique sur 12 mois");
              riSousMenu_bt27.setToolTipText("Historique sur 12 mois");
              riSousMenu_bt27.setName("riSousMenu_bt27");
              riSousMenu_bt27.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt21ActionPerformed(e);
                }
              });
              riSousMenu27.add(riSousMenu_bt27);
            }
            menus_haut.add(riSousMenu27);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1030, 620));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(1030, 620));
          p_contenu.setName("p_contenu");

          //======== xtp2 ========
          {
            xtp2.setBorder(new DropShadowBorder());
            xtp2.setTitle("Fournisseur");
            xtp2.setName("xtp2");
            Container xtp2ContentContainer = xtp2.getContentContainer();
            xtp2ContentContainer.setLayout(null);

            //---- A1REA ----
            A1REA.setModel(new DefaultComboBoxModel(new String[] {
              "Rupture sur stock minimum",
              "Consommation moyenne",
              "R\u00e9approvisionnement manuel",
              "Pr\u00e9visions de consommation",
              "Conso moyenne plafonn\u00e9e",
              "G\u00e9r\u00e9\u00a0(Plafond couverture)",
              "Compl\u00e9ment stock maximum",
              "Plafond pour couverture 'Mag'",
              "Produit non g\u00e9r\u00e9"
            }));
            A1REA.setComponentPopupMenu(BTD);
            A1REA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            A1REA.setName("A1REA");
            xtp2ContentContainer.add(A1REA);
            A1REA.setBounds(420, 52, 205, A1REA.getPreferredSize().height);

            //---- OBJ_66 ----
            OBJ_66.setText("@FRNOMA@");
            OBJ_66.setName("OBJ_66");
            xtp2ContentContainer.add(OBJ_66);
            OBJ_66.setBounds(90, 53, 235, OBJ_66.getPreferredSize().height);

            //---- OBJ_57 ----
            OBJ_57.setText("@ULBFRS@");
            OBJ_57.setFont(OBJ_57.getFont().deriveFont(OBJ_57.getFont().getStyle() | Font.BOLD));
            OBJ_57.setName("OBJ_57");
            xtp2ContentContainer.add(OBJ_57);
            OBJ_57.setBounds(90, 16, 215, OBJ_57.getPreferredSize().height);

            //---- OBJ_59 ----
            OBJ_59.setText("@CAREF@");
            OBJ_59.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_59.setName("OBJ_59");
            xtp2ContentContainer.add(OBJ_59);
            OBJ_59.setBounds(420, 16, 156, OBJ_59.getPreferredSize().height);

            //---- OBJ_67 ----
            OBJ_67.setText("Type r\u00e9appro.");
            OBJ_67.setName("OBJ_67");
            xtp2ContentContainer.add(OBJ_67);
            OBJ_67.setBounds(335, 55, 85, 20);

            //---- OBJ_56 ----
            OBJ_56.setText("@FRFRSG@");
            OBJ_56.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_56.setName("OBJ_56");
            xtp2ContentContainer.add(OBJ_56);
            OBJ_56.setBounds(15, 16, 63, OBJ_56.getPreferredSize().height);

            //---- OBJ_65 ----
            OBJ_65.setText("@FRSGA@");
            OBJ_65.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_65.setName("OBJ_65");
            xtp2ContentContainer.add(OBJ_65);
            OBJ_65.setBounds(15, 53, 63, OBJ_65.getPreferredSize().height);

            //---- OBJ_58 ----
            OBJ_58.setText("R\u00e9f\u00e9rence");
            OBJ_58.setName("OBJ_58");
            xtp2ContentContainer.add(OBJ_58);
            OBJ_58.setBounds(335, 18, 80, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xtp2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xtp2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xtp2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xtp2ContentContainer.setMinimumSize(preferredSize);
              xtp2ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel4 ========
          {
            xTitledPanel4.setBorder(new DropShadowBorder());
            xTitledPanel4.setTitle("12 derniers mois");
            xTitledPanel4.setName("xTitledPanel4");
            Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
            xTitledPanel4ContentContainer.setLayout(null);

            //======== SCROLLPANE_LIST22 ========
            {
              SCROLLPANE_LIST22.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST22.setName("SCROLLPANE_LIST22");

              //---- LIG11 ----
              LIG11.setFont(new Font("Courier New", Font.PLAIN, 12));
              LIG11.setName("LIG11");
              SCROLLPANE_LIST22.setViewportView(LIG11);
            }
            xTitledPanel4ContentContainer.add(SCROLLPANE_LIST22);
            SCROLLPANE_LIST22.setBounds(15, 10, 600, 95);

            //---- OBJ_141 ----
            OBJ_141.setText("Moyenne / 12 mois");
            OBJ_141.setName("OBJ_141");
            xTitledPanel4ContentContainer.add(OBJ_141);
            OBJ_141.setBounds(15, 160, 115, 20);

            //---- OBJ_150 ----
            OBJ_150.setText("Entr\u00e9e exercice");
            OBJ_150.setName("OBJ_150");
            xTitledPanel4ContentContainer.add(OBJ_150);
            OBJ_150.setBounds(15, 190, 110, 20);

            //---- OBJ_148 ----
            OBJ_148.setText("@ENTEX@");
            OBJ_148.setComponentPopupMenu(BTD);
            OBJ_148.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_148.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_148.setName("OBJ_148");
            xTitledPanel4ContentContainer.add(OBJ_148);
            OBJ_148.setBounds(130, 189, 54, 22);

            //---- OBJ_143 ----
            OBJ_143.setText("@MOY12@");
            OBJ_143.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_143.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_143.setName("OBJ_143");
            xTitledPanel4ContentContainer.add(OBJ_143);
            OBJ_143.setBounds(130, 159, 49, 22);

            //---- OBJ_145 ----
            OBJ_145.setText("@MOY6@");
            OBJ_145.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_145.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_145.setName("OBJ_145");
            xTitledPanel4ContentContainer.add(OBJ_145);
            OBJ_145.setBounds(240, 159, 49, 22);

            //---- OBJ_147 ----
            OBJ_147.setText("@MOY3@");
            OBJ_147.setComponentPopupMenu(BTD);
            OBJ_147.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_147.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_147.setName("OBJ_147");
            xTitledPanel4ContentContainer.add(OBJ_147);
            OBJ_147.setBounds(350, 159, 49, 22);

            //---- OBJ_144 ----
            OBJ_144.setText("6 mois");
            OBJ_144.setName("OBJ_144");
            xTitledPanel4ContentContainer.add(OBJ_144);
            OBJ_144.setBounds(191, 160, 49, 20);

            //---- OBJ_146 ----
            OBJ_146.setText("3 mois");
            OBJ_146.setName("OBJ_146");
            xTitledPanel4ContentContainer.add(OBJ_146);
            OBJ_146.setBounds(305, 160, 45, 20);

            //---- OBJ_151 ----
            OBJ_151.setText("Couver");
            OBJ_151.setName("OBJ_151");
            xTitledPanel4ContentContainer.add(OBJ_151);
            OBJ_151.setBounds(191, 190, 46, 20);

            //---- OBJ_149 ----
            OBJ_149.setText("@COUV@");
            OBJ_149.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_149.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_149.setName("OBJ_149");
            xTitledPanel4ContentContainer.add(OBJ_149);
            OBJ_149.setBounds(240, 189, 40, 22);

            //---- OBJ_153 ----
            OBJ_153.setText("Sorties");
            OBJ_153.setName("OBJ_153");
            xTitledPanel4ContentContainer.add(OBJ_153);
            OBJ_153.setBounds(305, 190, 46, 20);

            //---- OBJ_152 ----
            OBJ_152.setText("@SORMOI@");
            OBJ_152.setComponentPopupMenu(BTD);
            OBJ_152.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_152.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_152.setName("OBJ_152");
            xTitledPanel4ContentContainer.add(OBJ_152);
            OBJ_152.setBounds(350, 189, 57, 22);

            //---- OBJ_162 ----
            OBJ_162.setText("ABC");
            OBJ_162.setName("OBJ_162");
            xTitledPanel4ContentContainer.add(OBJ_162);
            OBJ_162.setBounds(475, 190, 30, 20);

            //---- OBJ_164 ----
            OBJ_164.setText("@A1ABC@");
            OBJ_164.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_164.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_164.setName("OBJ_164");
            xTitledPanel4ContentContainer.add(OBJ_164);
            OBJ_164.setBounds(515, 189, 25, 22);

            //---- OBJ_140 ----
            OBJ_140.setText("/mois");
            OBJ_140.setName("OBJ_140");
            xTitledPanel4ContentContainer.add(OBJ_140);
            OBJ_140.setBounds(415, 190, 36, 20);

            //======== panel3 ========
            {
              panel3.setBorder(new TitledBorder("text"));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //======== SCROLLPANE_LIST2 ========
              {
                SCROLLPANE_LIST2.setComponentPopupMenu(BTD);
                SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

                //---- LIG31 ----
                LIG31.setFont(new Font("Courier New", Font.PLAIN, 12));
                LIG31.setName("LIG31");
                SCROLLPANE_LIST2.setViewportView(LIG31);
              }
              panel3.add(SCROLLPANE_LIST2);
              SCROLLPANE_LIST2.setBounds(10, 30, 255, 110);

              //---- OBJ_154 ----
              OBJ_154.setText("@WATTX@");
              OBJ_154.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_154.setName("OBJ_154");
              panel3.add(OBJ_154);
              OBJ_154.setBounds(205, 0, 64, 20);
            }
            xTitledPanel4ContentContainer.add(panel3);
            panel3.setBounds(10, 220, 275, 160);

            //======== panel4 ========
            {
              panel4.setBorder(new TitledBorder("Derniers achats"));
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);

              //======== SCROLLPANE_LIST ========
              {
                SCROLLPANE_LIST.setComponentPopupMenu(BTD);
                SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

                //---- LIG41 ----
                LIG41.setFont(new Font("Courier New", Font.PLAIN, 12));
                LIG41.setName("LIG41");
                SCROLLPANE_LIST.setViewportView(LIG41);
              }
              panel4.add(SCROLLPANE_LIST);
              SCROLLPANE_LIST.setBounds(10, 30, 340, 110);
            }
            xTitledPanel4ContentContainer.add(panel4);
            panel4.setBounds(285, 220, 360, 160);

            //======== scrollPane2 ========
            {
              scrollPane2.setBackground(new Color(239, 239, 222));
              scrollPane2.setName("scrollPane2");

              //---- LIST24 ----
              LIST24.setName("LIST24");
              scrollPane2.setViewportView(LIST24);
            }
            xTitledPanel4ContentContainer.add(scrollPane2);
            scrollPane2.setBounds(15, 105, 600, 45);

            //---- BT_PGUP ----
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            xTitledPanel4ContentContainer.add(BT_PGUP);
            BT_PGUP.setBounds(620, 10, 25, 45);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            xTitledPanel4ContentContainer.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(620, 60, 25, 45);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel4ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel4ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel4ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel4ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel4ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Chiffrage");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- PUMP0 ----
            PUMP0.setText("@PUMP0@");
            PUMP0.setBorder(new BevelBorder(BevelBorder.LOWERED));
            PUMP0.setHorizontalAlignment(SwingConstants.RIGHT);
            PUMP0.setName("PUMP0");
            xTitledPanel1ContentContainer.add(PUMP0);
            PUMP0.setBounds(252, 30, 67, 22);

            //---- PUMP ----
            PUMP.setText("@PUMP@");
            PUMP.setBorder(new BevelBorder(BevelBorder.LOWERED));
            PUMP.setHorizontalAlignment(SwingConstants.RIGHT);
            PUMP.setName("PUMP");
            xTitledPanel1ContentContainer.add(PUMP);
            PUMP.setBounds(252, 30, 67, 22);

            //---- PRV0 ----
            PRV0.setText("@PRV0@");
            PRV0.setBorder(new BevelBorder(BevelBorder.LOWERED));
            PRV0.setHorizontalAlignment(SwingConstants.RIGHT);
            PRV0.setName("PRV0");
            xTitledPanel1ContentContainer.add(PRV0);
            PRV0.setBounds(127, 30, 58, 22);

            //---- PRV1 ----
            PRV1.setText("@PRV@");
            PRV1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            PRV1.setHorizontalAlignment(SwingConstants.RIGHT);
            PRV1.setName("PRV1");
            xTitledPanel1ContentContainer.add(PRV1);
            PRV1.setBounds(127, 30, 58, 22);

            //---- RAP ----
            RAP.setText("@RAP@");
            RAP.setToolTipText("Rapport entre tarif n\u00b01 et prix de revient");
            RAP.setBorder(new BevelBorder(BevelBorder.LOWERED));
            RAP.setHorizontalAlignment(SwingConstants.RIGHT);
            RAP.setName("RAP");
            xTitledPanel1ContentContainer.add(RAP);
            RAP.setBounds(127, 60, 58, 22);

            //---- lbPump ----
            lbPump.setText("PUMP");
            lbPump.setName("lbPump");
            xTitledPanel1ContentContainer.add(lbPump);
            lbPump.setBounds(252, 7, 67, 18);

            //---- MAR1 ----
            MAR1.setText("@MAR1@");
            MAR1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            MAR1.setHorizontalAlignment(SwingConstants.RIGHT);
            MAR1.setName("MAR1");
            xTitledPanel1ContentContainer.add(MAR1);
            MAR1.setBounds(197, 30, 45, 22);

            //---- MAR2 ----
            MAR2.setText("@MAR2@");
            MAR2.setBorder(new BevelBorder(BevelBorder.LOWERED));
            MAR2.setHorizontalAlignment(SwingConstants.RIGHT);
            MAR2.setName("MAR2");
            xTitledPanel1ContentContainer.add(MAR2);
            MAR2.setBounds(197, 60, 45, 22);

            //---- lbTarifs ----
            lbTarifs.setText("Tarif 1 et 2");
            lbTarifs.setName("lbTarifs");
            xTitledPanel1ContentContainer.add(lbTarifs);
            lbTarifs.setBounds(15, 7, 70, 18);

            //---- lbPRV ----
            lbPRV.setText("Revient");
            lbPRV.setName("lbPRV");
            xTitledPanel1ContentContainer.add(lbPRV);
            lbPRV.setBounds(127, 7, 58, 18);

            //---- lbMarge ----
            lbMarge.setText("Marge");
            lbMarge.setName("lbMarge");
            xTitledPanel1ContentContainer.add(lbMarge);
            lbMarge.setBounds(197, 7, 48, 18);

            //---- TAR1 ----
            TAR1.setText("@TAR1@");
            TAR1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            TAR1.setHorizontalAlignment(SwingConstants.RIGHT);
            TAR1.setName("TAR1");
            xTitledPanel1ContentContainer.add(TAR1);
            TAR1.setBounds(15, 30, TAR1.getPreferredSize().width, 22);

            //---- OBJ_92 ----
            OBJ_92.setText("@TAR10@");
            OBJ_92.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_92.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_92.setName("OBJ_92");
            xTitledPanel1ContentContainer.add(OBJ_92);
            OBJ_92.setBounds(15, 30, 58, 22);

            //---- TAR2 ----
            TAR2.setText("@TAR2@");
            TAR2.setBorder(new BevelBorder(BevelBorder.LOWERED));
            TAR2.setHorizontalAlignment(SwingConstants.RIGHT);
            TAR2.setName("TAR2");
            xTitledPanel1ContentContainer.add(TAR2);
            TAR2.setBounds(15, 60, TAR2.getPreferredSize().width, 22);

            //---- OBJ_108 ----
            OBJ_108.setText("@TAR20@");
            OBJ_108.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_108.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_108.setName("OBJ_108");
            xTitledPanel1ContentContainer.add(OBJ_108);
            OBJ_108.setBounds(15, 60, 58, 22);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel3 ========
          {
            xTitledPanel3.setTitle("Stock");
            xTitledPanel3.setBorder(new DropShadowBorder());
            xTitledPanel3.setName("xTitledPanel3");
            Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
            xTitledPanel3ContentContainer.setLayout(null);

            //---- OBJ_77 ----
            OBJ_77.setText("@WSTKX@");
            OBJ_77.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_77.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_77.setName("OBJ_77");
            xTitledPanel3ContentContainer.add(OBJ_77);
            OBJ_77.setBounds(120, 5, 65, 22);

            //---- OBJ_87 ----
            OBJ_87.setText("@WRESX1@");
            OBJ_87.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_87.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_87.setName("OBJ_87");
            xTitledPanel3ContentContainer.add(OBJ_87);
            OBJ_87.setBounds(120, 34, 65, 22);

            //---- OBJ_103 ----
            OBJ_103.setText("@WDISX@");
            OBJ_103.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_103.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_103.setName("OBJ_103");
            xTitledPanel3ContentContainer.add(OBJ_103);
            OBJ_103.setBounds(120, 63, 65, 22);

            //---- OBJ_99 ----
            OBJ_99.setText("Conditionnement");
            OBJ_99.setName("OBJ_99");
            xTitledPanel3ContentContainer.add(OBJ_99);
            OBJ_99.setBounds(135, 151, 105, 20);

            //---- OBJ_113 ----
            OBJ_113.setText("Unit\u00e9 stock");
            OBJ_113.setName("OBJ_113");
            xTitledPanel3ContentContainer.add(OBJ_113);
            OBJ_113.setBounds(200, 6, 85, 20);

            //---- OBJ_169 ----
            OBJ_169.setText("Lot de cmd");
            OBJ_169.setName("OBJ_169");
            xTitledPanel3ContentContainer.add(OBJ_169);
            OBJ_169.setBounds(200, 93, 69, 20);

            //---- OBJ_80 ----
            OBJ_80.setText("@LOTCMD@");
            OBJ_80.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_80.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_80.setName("OBJ_80");
            xTitledPanel3ContentContainer.add(OBJ_80);
            OBJ_80.setBounds(267, 92, 49, 22);

            //---- OBJ_81 ----
            OBJ_81.setText("@PLAFCV@");
            OBJ_81.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_81.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_81.setName("OBJ_81");
            xTitledPanel3ContentContainer.add(OBJ_81);
            OBJ_81.setBounds(135, 121, 49, 22);

            //---- OBJ_78 ----
            OBJ_78.setText("@MIN@");
            OBJ_78.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_78.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_78.setName("OBJ_78");
            xTitledPanel3ContentContainer.add(OBJ_78);
            OBJ_78.setBounds(271, 34, 45, 22);

            //---- OBJ_79 ----
            OBJ_79.setText("@MAX@");
            OBJ_79.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_79.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_79.setName("OBJ_79");
            xTitledPanel3ContentContainer.add(OBJ_79);
            OBJ_79.setBounds(271, 63, 45, 22);

            //---- CAQMI ----
            CAQMI.setText("@CAQMI@");
            CAQMI.setBorder(new BevelBorder(BevelBorder.LOWERED));
            CAQMI.setHorizontalAlignment(SwingConstants.RIGHT);
            CAQMI.setToolTipText("Quantit\u00e9 minimum de commande");
            CAQMI.setName("CAQMI");
            xTitledPanel3ContentContainer.add(CAQMI);
            CAQMI.setBounds(93, 92, 92, 22);

            //---- OBJ_98 ----
            OBJ_98.setText("Minimum");
            OBJ_98.setName("OBJ_98");
            xTitledPanel3ContentContainer.add(OBJ_98);
            OBJ_98.setBounds(15, 93, 105, 20);

            //---- OBJ_89 ----
            OBJ_89.setText("@CANUA@");
            OBJ_89.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_89.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_89.setName("OBJ_89");
            xTitledPanel3ContentContainer.add(OBJ_89);
            OBJ_89.setBounds(240, 150, 76, 22);

            //---- OBJ_72 ----
            OBJ_72.setText("Plafond couverture");
            OBJ_72.setName("OBJ_72");
            xTitledPanel3ContentContainer.add(OBJ_72);
            OBJ_72.setBounds(15, 122, 120, 20);

            //---- OBJ_70 ----
            OBJ_70.setText("Mini");
            OBJ_70.setName("OBJ_70");
            xTitledPanel3ContentContainer.add(OBJ_70);
            OBJ_70.setBounds(200, 35, 60, 20);

            //---- OBJ_71 ----
            OBJ_71.setText("Maxi");
            OBJ_71.setName("OBJ_71");
            xTitledPanel3ContentContainer.add(OBJ_71);
            OBJ_71.setBounds(200, 64, 60, 20);

            //---- OBJ_90 ----
            OBJ_90.setText("Command\u00e9");
            OBJ_90.setName("OBJ_90");
            xTitledPanel3ContentContainer.add(OBJ_90);
            OBJ_90.setBounds(15, 35, 105, 20);

            //---- OBJ_105 ----
            OBJ_105.setText("Disponible");
            OBJ_105.setName("OBJ_105");
            xTitledPanel3ContentContainer.add(OBJ_105);
            OBJ_105.setBounds(15, 64, 105, 20);

            //---- OBJ_82 ----
            OBJ_82.setText("R\u00e9el");
            OBJ_82.setName("OBJ_82");
            xTitledPanel3ContentContainer.add(OBJ_82);
            OBJ_82.setBounds(15, 6, 65, 20);

            //---- OBJ_114 ----
            OBJ_114.setText("@A1UNS@");
            OBJ_114.setBorder(new BevelBorder(BevelBorder.LOWERED));
            OBJ_114.setName("OBJ_114");
            xTitledPanel3ContentContainer.add(OBJ_114);
            OBJ_114.setBounds(295, 5, 21, 22);

            //---- A1IN20 ----
            A1IN20.setComponentPopupMenu(BTD);
            A1IN20.setText("@A1IN20@");
            A1IN20.setName("A1IN20");
            xTitledPanel3ContentContainer.add(A1IN20);
            A1IN20.setBounds(295, 120, 21, A1IN20.getPreferredSize().height);

            //---- OBJ_44 ----
            OBJ_44.setText("GP");
            OBJ_44.setName("OBJ_44");
            xTitledPanel3ContentContainer.add(OBJ_44);
            OBJ_44.setBounds(200, 118, 30, 28);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel3ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //---- snPhotoArticle ----
          snPhotoArticle.setOpaque(false);
          snPhotoArticle.setName("snPhotoArticle");

          //======== scrollPane1 ========
          {
            scrollPane1.setName("scrollPane1");

            //======== xTitledPanel2 ========
            {
              xTitledPanel2.setTitle("Quantit\u00e9 \u00e0 commander");
              xTitledPanel2.setBackground(new Color(239, 239, 222));
              xTitledPanel2.setName("xTitledPanel2");
              Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
              xTitledPanel2ContentContainer.setLayout(null);

              //---- OBJ_106 ----
              OBJ_106.setText("Quantit\u00e9 calcul\u00e9e");
              OBJ_106.setHorizontalAlignment(SwingConstants.LEFT);
              OBJ_106.setHorizontalTextPosition(SwingConstants.LEFT);
              OBJ_106.setName("OBJ_106");
              xTitledPanel2ContentContainer.add(OBJ_106);
              OBJ_106.setBounds(10, 7, 110, 20);

              //---- LRQTEX ----
              LRQTEX.setText("@LRQTEX@");
              LRQTEX.setBorder(new BevelBorder(BevelBorder.LOWERED));
              LRQTEX.setHorizontalAlignment(SwingConstants.RIGHT);
              LRQTEX.setOpaque(false);
              LRQTEX.setName("LRQTEX");
              xTitledPanel2ContentContainer.add(LRQTEX);
              LRQTEX.setBounds(225, 5, 85, LRQTEX.getPreferredSize().height);

              //---- LRQTSX ----
              LRQTSX.setComponentPopupMenu(BTD);
              LRQTSX.setHorizontalAlignment(SwingConstants.RIGHT);
              LRQTSX.setName("LRQTSX");
              xTitledPanel2ContentContainer.add(LRQTSX);
              LRQTSX.setBounds(225, 30, 89, LRQTSX.getPreferredSize().height);

              //---- OBJ_115 ----
              OBJ_115.setText("Quantit\u00e9 \u00e0 commander arrondie");
              OBJ_115.setName("OBJ_115");
              xTitledPanel2ContentContainer.add(OBJ_115);
              OBJ_115.setBounds(10, 34, 205, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = xTitledPanel2ContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
                xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
              }
            }
            scrollPane1.setViewportView(xTitledPanel2);
          }

          //---- OBJ_41 ----
          OBJ_41.setText("Article");
          OBJ_41.setName("OBJ_41");

          //---- INDAR0 ----
          INDAR0.setComponentPopupMenu(BTD);
          INDAR0.setOpaque(false);
          INDAR0.setText("@INDAR0@");
          INDAR0.setName("INDAR0");

          //---- OBJ_52 ----
          OBJ_52.setText("@A1LIB@");
          OBJ_52.setOpaque(false);
          OBJ_52.setName("OBJ_52");

          //---- OBJ_46 ----
          OBJ_46.setText("Famille");
          OBJ_46.setName("OBJ_46");

          //---- A1FAM ----
          A1FAM.setComponentPopupMenu(BTD);
          A1FAM.setOpaque(false);
          A1FAM.setText("@A1FAM@");
          A1FAM.setName("A1FAM");

          //---- lb_elephant ----
          lb_elephant.setText("Commande \u00e9l\u00e9phant");
          lb_elephant.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_elephant.setFont(lb_elephant.getFont().deriveFont(lb_elephant.getFont().getStyle() | Font.BOLD));
          lb_elephant.setName("lb_elephant");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDAR0, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
                .addGap(90, 90, 90)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(A1FAM, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lb_elephant, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(12, 12, 12)
                    .addComponent(xtp2, GroupLayout.PREFERRED_SIZE, 657, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 340, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(12, 12, 12)
                    .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 657, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 340, GroupLayout.PREFERRED_SIZE)
                      .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 340, GroupLayout.PREFERRED_SIZE)))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(784, 784, 784)
                    .addComponent(snPhotoArticle, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)))
                .addGap(9, 9, 9))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(INDAR0, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(A1FAM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(lb_elephant, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))))
                .addGap(9, 9, 9)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(xtp2, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                  .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 415, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 209, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(snPhotoArticle, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }

    //======== riSousMenu18 ========
    {
      riSousMenu18.setName("riSousMenu18");

      //---- riSousMenu_bt18 ----
      riSousMenu_bt18.setText("Graphe");
      riSousMenu_bt18.setToolTipText("Graphe statistique");
      riSousMenu_bt18.setName("riSousMenu_bt18");
      riSousMenu_bt18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt18ActionPerformed(e);
        }
      });
      riSousMenu18.add(riSousMenu_bt18);
    }

    //======== riSousMenu21 ========
    {
      riSousMenu21.setName("riSousMenu21");

      //---- riSousMenu_bt21 ----
      riSousMenu_bt21.setText("Historique de conso.");
      riSousMenu_bt21.setToolTipText("Historique de consommation");
      riSousMenu_bt21.setName("riSousMenu_bt21");
      riSousMenu_bt21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt21ActionPerformed(e);
        }
      });
      riSousMenu21.add(riSousMenu_bt21);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche2;
  private RiZoneSortie INDETB;
  private JLabel OBJ_38;
  private RiZoneSortie INDMAG;
  private JLabel OBJ_39;
  private RiZoneSortie riZoneSortie1;
  private RiZoneSortie riZoneSortie2;
  private JLabel label1;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiSousMenu riSousMenu13;
  private RiSousMenu_bt riSousMenu_bt13;
  private RiSousMenu riSousMenu22;
  private RiSousMenu_bt riSousMenu_bt22;
  private RiSousMenu riSousMenu24;
  private RiSousMenu_bt riSousMenu_bt24;
  private RiSousMenu riSousMenu25;
  private RiSousMenu_bt riSousMenu_bt25;
  private RiSousMenu riSousMenu26;
  private RiSousMenu_bt riSousMenu_bt26;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiMenu riMenu4;
  private RiMenu_bt riMenu_bt4;
  private RiSousMenu riSousMenu19;
  private RiSousMenu_bt riSousMenu_bt19;
  private RiSousMenu riSousMenu20;
  private RiSousMenu_bt riSousMenu_bt20;
  private RiSousMenu riSousMenu27;
  private RiSousMenu_bt riSousMenu_bt27;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xtp2;
  private XRiComboBox A1REA;
  private RiZoneSortie OBJ_66;
  private RiZoneSortie OBJ_57;
  private RiZoneSortie OBJ_59;
  private JLabel OBJ_67;
  private RiZoneSortie OBJ_56;
  private RiZoneSortie OBJ_65;
  private JLabel OBJ_58;
  private JXTitledPanel xTitledPanel4;
  private JScrollPane SCROLLPANE_LIST22;
  private XRiTable LIG11;
  private JLabel OBJ_141;
  private JLabel OBJ_150;
  private RiZoneSortie OBJ_148;
  private RiZoneSortie OBJ_143;
  private RiZoneSortie OBJ_145;
  private RiZoneSortie OBJ_147;
  private JLabel OBJ_144;
  private JLabel OBJ_146;
  private JLabel OBJ_151;
  private RiZoneSortie OBJ_149;
  private JLabel OBJ_153;
  private RiZoneSortie OBJ_152;
  private JLabel OBJ_162;
  private RiZoneSortie OBJ_164;
  private JLabel OBJ_140;
  private JPanel panel3;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable LIG31;
  private JLabel OBJ_154;
  private JPanel panel4;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable LIG41;
  private JScrollPane scrollPane2;
  private XRiTable LIST24;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JXTitledPanel xTitledPanel1;
  private RiZoneSortie PUMP0;
  private RiZoneSortie PUMP;
  private RiZoneSortie PRV0;
  private RiZoneSortie PRV1;
  private RiZoneSortie RAP;
  private JLabel lbPump;
  private RiZoneSortie MAR1;
  private RiZoneSortie MAR2;
  private JLabel lbTarifs;
  private JLabel lbPRV;
  private JLabel lbMarge;
  private RiZoneSortie TAR1;
  private RiZoneSortie OBJ_92;
  private RiZoneSortie TAR2;
  private RiZoneSortie OBJ_108;
  private JXTitledPanel xTitledPanel3;
  private RiZoneSortie OBJ_77;
  private RiZoneSortie OBJ_87;
  private RiZoneSortie OBJ_103;
  private JLabel OBJ_99;
  private JLabel OBJ_113;
  private JLabel OBJ_169;
  private RiZoneSortie OBJ_80;
  private RiZoneSortie OBJ_81;
  private RiZoneSortie OBJ_78;
  private RiZoneSortie OBJ_79;
  private RiZoneSortie CAQMI;
  private JLabel OBJ_98;
  private RiZoneSortie OBJ_89;
  private JLabel OBJ_72;
  private JLabel OBJ_70;
  private JLabel OBJ_71;
  private JLabel OBJ_90;
  private JLabel OBJ_105;
  private JLabel OBJ_82;
  private RiZoneSortie OBJ_114;
  private RiZoneSortie A1IN20;
  private JLabel OBJ_44;
  private SNPhotoArticle snPhotoArticle;
  private JScrollPane scrollPane1;
  private JXTitledPanel xTitledPanel2;
  private JLabel OBJ_106;
  private RiZoneSortie LRQTEX;
  private XRiTextField LRQTSX;
  private JLabel OBJ_115;
  private JLabel OBJ_41;
  private RiZoneSortie INDAR0;
  private RiZoneSortie OBJ_52;
  private JLabel OBJ_46;
  private RiZoneSortie A1FAM;
  private JLabel lb_elephant;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  private RiSousMenu riSousMenu18;
  private RiSousMenu_bt riSousMenu_bt18;
  private RiSousMenu riSousMenu21;
  private RiSousMenu_bt riSousMenu_bt21;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
