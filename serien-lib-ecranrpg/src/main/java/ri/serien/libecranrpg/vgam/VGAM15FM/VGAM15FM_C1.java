
package ri.serien.libecranrpg.vgam.VGAM15FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGAM15FM_C1 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "HLD01", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 609, };
  private String[] listeCellules =
      { "LD01", "LD02", "LD03", "LD04", "LD05", "LD06", "LD07", "LD08", "LD09", "LD10", "LD11", "LD12", "LD13", "LD14", "LD15" };
  private Color[][] couleurLigne = new Color[listeCellules.length][1];
  private String st_argument = null;
  private String st_saisie = null;
  private String[] fonctions = { "", "F16", "F17", "F18", "F5", "F20" };
  private String libelleDirectOuInterne = "";
  
  /**
   * Constructeur.
   */
  public VGAM15FM_C1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, couleurLigne, null, null);
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
    riMenu_bt5.setIcon(lexique.chargerImage("images/navigation.png", true));
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYP15@")).trim());
    OBJ_72.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E2NOM@")).trim());
    OBJ_73.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EASAN@")).trim());
    OBJ_74.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSALBR@")).trim());
    WDATEX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDATEX@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_117.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DVLIBR@")).trim());
    WTHTX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTHTX@")).trim());
    WTVAX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTVAX@")).trim());
    WTTCX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTTCX@")).trim());
    WPDSX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPDSX@")).trim());
    OBJ_81.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EUR@")).trim());
    TIT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITB@")).trim());
    arg_classement.setText(lexique.TranslationTable(interpreteurD.analyseExpression("par @TMC1@")).trim());
    arg_classement2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("par @TMC2@")).trim());
    OBJ_93.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCHPA@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    gererCouleurListe();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(riMenu_bt5);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    OBJ_95.setVisible(lexique.isPresent("WAVR"));
    OBJ_94.setVisible(lexique.isPresent("WNAT"));
    OBJ_114.setVisible(lexique.isPresent("WMAG"));
    OBJ_115.setVisible(lexique.isPresent("WSAN"));
    OBJ_73.setVisible(lexique.isPresent("EASAN"));
    OBJ_116.setVisible(lexique.isPresent("WACT"));
    OBJ_112.setVisible(lexique.isPresent("W20NAT"));
    OBJ_92.setVisible(lexique.isPresent("WNLI"));
    OBJ_107.setVisible(lexique.isPresent("WTVAX"));
    WTHTX.setVisible(lexique.isPresent("WTHTX"));
    WTVAX.setVisible(lexique.isPresent("WTVAX"));
    WTTCX.setVisible(lexique.isPresent("WTTCX"));
    OBJ_81.setVisible(lexique.isPresent("EUR"));
    WDATEX.setVisible(lexique.isPresent("WDATX"));
    WART21.setVisible(lexique.isPresent("WART21"));
    OBJ_74.setVisible(lexique.isPresent("WSALBR"));
    OBJ_117.setVisible(lexique.isPresent("DVLIBR"));
    WARTT.setVisible(lexique.isPresent("WARTT"));
    OBJ_72.setVisible(lexique.isPresent("E2NOM"));
    if (lexique.isTrue("06")) {
      OBJ_110.setText("Poids");
    }
    
    if (!WARTT.isVisible()) {
      WTP01.requestFocus();
    }
    
    label1.setVisible(lexique.isTrue("N86"));
    WPDSX.setVisible(lexique.isTrue("06"));
    
    // Visibilité des factures
    boolean isFacture = lexique.isTrue("44");
    
    riSousMenu6.setEnabled(isFacture);
    riSousMenu13.setEnabled(isFacture);
    riSousMenu8.setEnabled(!isFacture);
    
    // Traitement du message d'état
    String etatDuBon = lexique.HostFieldGetData("ETABON").trim();
    if (etatDuBon.equals("C")) {
      label_etat.setText("Comptabilisé");
    }
    else if (etatDuBon.equals("H")) {
      label_etat.setText("Validé");
    }
    else if (etatDuBon.equals("A")) {
      label_etat.setText("En attente");
    }
    else if (etatDuBon.equals("L")) {
      label_etat.setText("Livré");
    }
    else if (etatDuBon.equals("R")) {
      label_etat.setText("Réceptionné");
    }
    else if (etatDuBon.equals("P")) {
      label_etat.setText("Payable");
    }
    else if (etatDuBon.equals("f")) {
      label_etat.setText("Lié sur facture");
    }
    else if (etatDuBon.equals("F")) {
      label_etat.setText("Facturé");
    }
    else {
      label_etat.setText("");
    }
    
    // Gestion du type d'arguments recherchés
    String WCHPA = lexique.HostFieldGetData("WCHPA");
    String[] tab_WCHPA = WCHPA.split("/");
    if (tab_WCHPA.length > 1) {
      st_saisie = tab_WCHPA[1].trim();
    }
    else {
      st_saisie = null;
    }
    st_argument = tab_WCHPA[0].trim();
    gererArguments(st_argument);
    if (st_argument.equals("GenCod")) {
      ck_saisie.setText("Saisie unitaire");
      fonctions[0] = fonctions[4];
    }
    else if (st_argument.equals("Génériq.")) {
      fonctions[0] = fonctions[5];
    }
    else if (st_argument.equals("Famille")) {
      fonctions[0] = fonctions[1];
    }
    else if (st_argument.equals(lexique.HostFieldGetData("TMC1").trim())) {
      st_argument = lexique.HostFieldGetData("TMC1").trim();
      fonctions[0] = fonctions[2];
    }
    else if (st_argument.equals(lexique.HostFieldGetData("TMC2").trim())) {
      st_argument = lexique.HostFieldGetData("TMC2").trim();
      fonctions[0] = fonctions[3];
    }
    else {
      fonctions[0] = "";
    }
    if (st_argument.equals("Génériq.")) {
      bt_argument.setText("     Code générique");
    }
    else {
      bt_argument.setText(" " + st_argument);
    }
    ck_saisie.setSelected(st_saisie != null);
    
    bt_argument.setVisible(WARTT.isVisible());
    ck_saisie.setVisible(WARTT.isVisible());
    riBoutonDetail1.setVisible(WARTT.isVisible());
    
    labelNav.setIcon(lexique.chargerImage("images/fac999.jpg", true));
    labelNavBt2.setIcon(lexique.chargerImage("images/navencours.png", true));
    
    if (lexique.HostFieldGetData("EAIN9").trim().equals("D")) {
      libelleDirectOuInterne = " (Direct usine)";
    }
    else if (lexique.HostFieldGetData("EAIN9").trim().equals("I")) {
      libelleDirectOuInterne = " (Commande interne)";
    }
    else {
      libelleDirectOuInterne = "";
    }
    
    p_bpresentation.setText(p_bpresentation.getText() + libelleDirectOuInterne);
    p_bpresentation.setCodeEtablissement(EAETB.getText());
    
    sortie();
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  // permet de gérer la couleur des lignes de la liste en fonction du type de ligne (article, commentaire , edition,
  // annulé..)
  private void gererCouleurListe() {
    Color listeCommentaire = Constantes.COULEUR_LISTE_COMMENTAIRE;
    Color listeAnnule = Constantes.COULEUR_LISTE_ANNULATION;
    String chaineTest = null;
    for (int i = 0; i < listeCellules.length; i++) {
      chaineTest = lexique.HostFieldGetData(listeCellules[i]).trim();
      if ((chaineTest != null) && (!chaineTest.equals("") && chaineTest.length() > 7)) {
        if (chaineTest.substring(5, 7).trim().equals("") || (chaineTest.substring(5, 7).trim().equals("*E"))) {
          couleurLigne[i][0] = listeCommentaire;
        }
        else if (chaineTest.substring(5, 7).trim().equals("**")) {
          couleurLigne[i][0] = listeAnnule;
        }
        else {
          couleurLigne[i][0] = Color.BLACK;
        }
      }
      else {
        couleurLigne[i][0] = Color.BLACK;
      }
    }
  }
  
  /* cacher le jmenuItem qui est en cours */
  private void gererArguments(String argEnCours) {
    arg_article.setVisible(!argEnCours.equals("Article"));
    arg_generique.setVisible(!argEnCours.equals("Génériq."));
    arg_famille.setVisible(!argEnCours.equals("Famille"));
    arg_classement.setVisible(!argEnCours.equals(lexique.HostFieldGetData("TMC1").trim()));
    arg_classement2.setVisible(!argEnCours.equals(lexique.HostFieldGetData("TMC2").trim()));
    arg_gencod.setVisible(!argEnCours.equals("GenCod"));
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ARTT", 0, "*MAJCPR");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void riSousMenu_bt17ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void riSousMenu_bt13ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "1", "Enter");
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "2", "Enter");
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_27ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "4", "Enter");
    WTP01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_28ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut((WTP01.getSelectedRow() + 4), 4);
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void OBJ_29ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("B");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_30ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("P");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_31ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "R", "Enter");
    WTP01.setValeurTop("R");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_32ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "V", "Enter");
    WTP01.setValeurTop("V");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_33ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "T", "Enter");
    WTP01.setValeurTop("T");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_34ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "X", "Enter");
    WTP01.setValeurTop("X");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_35ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "M", "Enter");
    WTP01.setValeurTop("M");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_36ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "L", "Enter");
    WTP01.setValeurTop("L");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_37ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_40ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void OBJ_41ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD2.getInvoker().getName());
  }
  
  private void OBJ_42ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD2.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_44ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD3.getInvoker().getName());
  }
  
  private void OBJ_45ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD3.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_47ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // ScriptCall("G_CONVER")
    // ScriptCall("G_CONVER")
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _WTP01_Top, "1", "ENTER", e);
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void labelNavBt3MouseClicked() {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void labelNavBt3MouseEntered(MouseEvent e) {
    labelNavBt3.setIcon(lexique.chargerImage("images/navselec.png", true));
  }
  
  private void labelNavBt3MouseExited(MouseEvent e) {
    labelNavBt3.setIcon(lexique.chargerImage("images/blank.png", true));
  }
  
  private void labelNavBt1MouseClicked(MouseEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void labelNavBt1MouseEntered(MouseEvent e) {
    labelNavBt1.setIcon(lexique.chargerImage("images/navselec.png", true));
  }
  
  private void labelNavBt1MouseExited(MouseEvent e) {
    labelNavBt1.setIcon(lexique.chargerImage("images/blank.png", true));
  }
  
  private void sortie() {
    // navigation graphique sortie directe
    String chaine = (String) lexique.getValeurVariableGlobale("VGAM15FX_NAV");
    if ((chaine != null) && !chaine.equals("")) {
      lexique.HostScreenSendKey(this, "F3");
      lexique.addVariableGlobale("VGAM15FX_NAV", "");
    }
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    BTD4.show(WTP01, 50, 50);
  }
  
  private void bt_argumentActionPerformed(ActionEvent e) {
    arguments.show(bt_argument, 0, -150);
  }
  
  private void arg_articleActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, fonctions[0]);
  }
  
  private void arg_generiqueActionPerformed(ActionEvent e) {
    fonctions[0] = fonctions[5];
    lexique.HostScreenSendKey(this, fonctions[5]);
  }
  
  private void arg_familleActionPerformed(ActionEvent e) {
    fonctions[0] = fonctions[1];
    lexique.HostScreenSendKey(this, fonctions[1]);
  }
  
  private void arg_classementActionPerformed(ActionEvent e) {
    fonctions[0] = fonctions[2];
    lexique.HostScreenSendKey(this, fonctions[2]);
  }
  
  private void arg_classement2ActionPerformed(ActionEvent e) {
    fonctions[0] = fonctions[3];
    lexique.HostScreenSendKey(this, fonctions[3]);
  }
  
  private void arg_gencodActionPerformed(ActionEvent e) {
    fonctions[0] = fonctions[4];
    lexique.HostScreenSendKey(this, fonctions[4]);
  }
  
  private void arg_art_commActionPerformed(ActionEvent e) {
    WARTT.setText(" *" + WARTT.getText());
    // lexique.HostFieldPutData("WARTT", 0, " *" + WARTT.getText());
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void arg_commActionPerformed(ActionEvent e) {
    WARTT.setText("");
    lexique.HostCursorPut("WARTT");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetail1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void ck_saisieActionPerformed(ActionEvent e) {
    if (st_argument.equals("GenCod")) {
      lexique.HostScreenSendKey(this, "F6");
    }
    else {
      lexique.HostScreenSendKey(this, "F7");
    }
  }
  
  private void button_okActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_49ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("8");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    EAETB = new XRiTextField();
    WNUM = new XRiTextField();
    WSUF = new XRiTextField();
    OBJ_72 = new RiZoneSortie();
    OBJ_73 = new RiZoneSortie();
    OBJ_74 = new RiZoneSortie();
    WDATEX = new JLabel();
    OBJ_51 = new JLabel();
    OBJ_52 = new JLabel();
    OBJ_53 = new JLabel();
    p_tete_droite = new JPanel();
    label_etat = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riSousMenu13 = new RiSousMenu();
    riSousMenu_bt13 = new RiSousMenu_bt();
    riSousMenu16 = new RiSousMenu();
    riSousMenu_bt17 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    riMenu5 = new RiMenu();
    riMenu_bt5 = new RiMenu_bt();
    riSousMenu23 = new RiSousMenu();
    panelNav = new JPanel();
    labelNavBt1 = new JLabel();
    labelNavBt2 = new JLabel();
    labelNavBt3 = new JLabel();
    labelNav = new JLabel();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    panel2 = new JPanel();
    WARTT = new XRiTextField();
    OBJ_117 = new RiZoneSortie();
    WART21 = new XRiTextField();
    WTHTX = new RiZoneSortie();
    WTVAX = new RiZoneSortie();
    WTTCX = new RiZoneSortie();
    OBJ_110 = new JLabel();
    OBJ_107 = new JLabel();
    OBJ_96 = new JLabel();
    W20NAT = new XRiTextField();
    OBJ_92 = new JLabel();
    OBJ_112 = new JLabel();
    WART22 = new XRiTextField();
    WART23 = new XRiTextField();
    OBJ_116 = new JLabel();
    WSAN = new XRiTextField();
    WACT = new XRiTextField();
    OBJ_115 = new JLabel();
    WNLI = new XRiTextField();
    OBJ_114 = new JLabel();
    WMAG = new XRiTextField();
    WNAT = new XRiTextField();
    WAVR = new XRiTextField();
    WCOD = new XRiTextField();
    OBJ_94 = new JLabel();
    OBJ_95 = new JLabel();
    label1 = new JLabel();
    bt_argument = new SNBoutonLeger();
    riBoutonDetail1 = new SNBoutonDetail();
    ck_saisie = new JCheckBox();
    WPDSX = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_25 = new JMenuItem();
    OBJ_26 = new JMenuItem();
    OBJ_27 = new JMenuItem();
    OBJ_29 = new JMenuItem();
    OBJ_28 = new JMenuItem();
    menuItem1 = new JMenuItem();
    OBJ_37 = new JMenuItem();
    BTD2 = new JPopupMenu();
    OBJ_40 = new JMenuItem();
    separator1 = new JSeparator();
    OBJ_42 = new JMenuItem();
    OBJ_41 = new JMenuItem();
    BTD3 = new JPopupMenu();
    OBJ_45 = new JMenuItem();
    OBJ_44 = new JMenuItem();
    FCT1 = new JPopupMenu();
    OBJ_47 = new JMenuItem();
    OBJ_81 = new JLabel();
    TIT = new JLabel();
    BTD4 = new JPopupMenu();
    OBJ_49 = new JMenuItem();
    OBJ_50 = new JMenuItem();
    OBJ_57 = new JMenuItem();
    OBJ_58 = new JMenuItem();
    OBJ_55 = new JMenuItem();
    OBJ_56 = new JMenuItem();
    OBJ_54 = new JMenuItem();
    OBJ_59 = new JMenuItem();
    OBJ_61 = new JMenuItem();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    arguments = new JPopupMenu();
    menuItem2 = new JMenuItem();
    arg_article = new JMenuItem();
    arg_generique = new JMenuItem();
    arg_famille = new JMenuItem();
    arg_classement = new JMenuItem();
    arg_classement2 = new JMenuItem();
    arg_gencod = new JMenuItem();
    arg_art_comm = new JMenuItem();
    arg_comm = new JMenuItem();
    OBJ_93 = new JLabel();
    navig_valid3 = new RiMenu();
    button_ok = new RiMenu_bt();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("@TYP15@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 40));
          p_tete_gauche.setMinimumSize(new Dimension(860, 40));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- EAETB ----
          EAETB.setComponentPopupMenu(BTD);
          EAETB.setName("EAETB");
          
          // ---- WNUM ----
          WNUM.setComponentPopupMenu(BTD);
          WNUM.setName("WNUM");
          
          // ---- WSUF ----
          WSUF.setComponentPopupMenu(BTD);
          WSUF.setName("WSUF");
          
          // ---- OBJ_72 ----
          OBJ_72.setText("@E2NOM@");
          OBJ_72.setOpaque(false);
          OBJ_72.setName("OBJ_72");
          
          // ---- OBJ_73 ----
          OBJ_73.setText("@EASAN@");
          OBJ_73.setOpaque(false);
          OBJ_73.setName("OBJ_73");
          
          // ---- OBJ_74 ----
          OBJ_74.setText("@WSALBR@");
          OBJ_74.setOpaque(false);
          OBJ_74.setName("OBJ_74");
          
          // ---- WDATEX ----
          WDATEX.setComponentPopupMenu(BTD);
          WDATEX.setText("@WDATEX@");
          WDATEX.setBorder(new BevelBorder(BevelBorder.LOWERED));
          WDATEX.setHorizontalAlignment(SwingConstants.CENTER);
          WDATEX.setFont(WDATEX.getFont().deriveFont(WDATEX.getFont().getStyle() | Font.BOLD));
          WDATEX.setName("WDATEX");
          
          // ---- OBJ_51 ----
          OBJ_51.setText("Etablissement");
          OBJ_51.setName("OBJ_51");
          
          // ---- OBJ_52 ----
          OBJ_52.setText("Num\u00e9ro");
          OBJ_52.setName("OBJ_52");
          
          // ---- OBJ_53 ----
          OBJ_53.setText("Fournisseur");
          OBJ_53.setName("OBJ_53");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5)
                  .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(EAETB, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE).addGap(11, 11, 11)
                  .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE).addGap(4, 4, 4)
                  .addComponent(WNUM, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(WSUF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(15, 15, 15)
                  .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(OBJ_72, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE).addGap(7, 7, 7)
                  .addComponent(OBJ_74, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE).addGap(4, 4, 4)
                  .addComponent(WDATEX, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(7, 7, 7).addComponent(OBJ_51))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(EAETB, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(7, 7, 7).addComponent(OBJ_52))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(WNUM, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(WSUF, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(7, 7, 7).addComponent(OBJ_53))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(3, 3, 3).addComponent(OBJ_72, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(3, 3, 3).addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(3, 3, 3).addComponent(OBJ_74, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(3, 3, 3).addComponent(WDATEX, GroupLayout.PREFERRED_SIZE, 24,
                  GroupLayout.PREFERRED_SIZE)));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
          
          // ---- label_etat ----
          label_etat.setFont(label_etat.getFont().deriveFont(label_etat.getFont().getStyle() | Font.BOLD));
          label_etat.setText("label_etat");
          label_etat.setPreferredSize(new Dimension(90, 20));
          label_etat.setName("label_etat");
          p_tete_droite.add(label_etat);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(e -> bouton_validerActionPerformed(e));
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour ent\u00eate");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(e -> bouton_retourActionPerformed(e));
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Bons rapprochables");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(e -> riSousMenu_bt6ActionPerformed(e));
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");
              
              // ---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Tri des lignes");
              riSousMenu_bt8.setToolTipText("Tri des lignes par r\u00e9f\u00e9rence fournisseur");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(e -> riSousMenu_bt8ActionPerformed(e));
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
            
            // ======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");
              
              // ---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Formule prix de revient");
              riSousMenu_bt9.setToolTipText("Modification formule de prix de revient");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(e -> riSousMenu_bt9ActionPerformed(e));
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
            
            // ======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");
              
              // ---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Affichage quantit\u00e9s/prix");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(e -> riSousMenu_bt10ActionPerformed(e));
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);
            
            // ======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");
              
              // ---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Saisie code barres");
              riSousMenu_bt11.setToolTipText("Mise en/hors fonction de la saisie code barres");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(e -> riSousMenu_bt11ActionPerformed(e));
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);
            
            // ======== riSousMenu12 ========
            {
              riSousMenu12.setName("riSousMenu12");
              
              // ---- riSousMenu_bt12 ----
              riSousMenu_bt12.setText("Saisie qt\u00e9 code barres");
              riSousMenu_bt12.setToolTipText("Mise en/hors fonction de la saisie quantit\u00e9 avec code barres");
              riSousMenu_bt12.setName("riSousMenu_bt12");
              riSousMenu_bt12.addActionListener(e -> riSousMenu_bt12ActionPerformed(e));
              riSousMenu12.add(riSousMenu_bt12);
            }
            menus_haut.add(riSousMenu12);
            
            // ======== riSousMenu13 ========
            {
              riSousMenu13.setName("riSousMenu13");
              
              // ---- riSousMenu_bt13 ----
              riSousMenu_bt13.setText("Liaisons/lignes");
              riSousMenu_bt13.setToolTipText("Affichage liaisons/lignes");
              riSousMenu_bt13.setName("riSousMenu_bt13");
              riSousMenu_bt13.addActionListener(e -> riSousMenu_bt13ActionPerformed(e));
              riSousMenu13.add(riSousMenu_bt13);
            }
            menus_haut.add(riSousMenu13);
            
            // ======== riSousMenu16 ========
            {
              riSousMenu16.setName("riSousMenu16");
              
              // ---- riSousMenu_bt17 ----
              riSousMenu_bt17.setText("Ajouter commandes");
              riSousMenu_bt17.setToolTipText("Mise en/hors fonction de la saisie code barres");
              riSousMenu_bt17.setName("riSousMenu_bt17");
              riSousMenu_bt17.addActionListener(e -> riSousMenu_bt17ActionPerformed(e));
              riSousMenu16.add(riSousMenu_bt17);
            }
            menus_haut.add(riSousMenu16);
            
            // ======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");
              
              // ---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);
            
            // ======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");
              
              // ---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Autres vues");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(e -> riSousMenu_bt15ActionPerformed(e));
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);
            
            // ======== riMenu5 ========
            {
              riMenu5.setName("riMenu5");
              
              // ---- riMenu_bt5 ----
              riMenu_bt5.setText("Navigation");
              riMenu_bt5.setName("riMenu_bt5");
              riMenu5.add(riMenu_bt5);
            }
            menus_haut.add(riMenu5);
            
            // ======== riSousMenu23 ========
            {
              riSousMenu23.setPreferredSize(new Dimension(170, 260));
              riSousMenu23.setMinimumSize(new Dimension(170, 260));
              riSousMenu23.setMaximumSize(new Dimension(170, 260));
              riSousMenu23.setMargin(new Insets(0, -2, 0, 0));
              riSousMenu23.setOpaque(true);
              riSousMenu23.setName("riSousMenu23");
              
              // ======== panelNav ========
              {
                panelNav.setOpaque(false);
                panelNav.setName("panelNav");
                panelNav.setLayout(null);
                
                // ---- labelNavBt1 ----
                labelNavBt1.setToolTipText("ent\u00eate de bon");
                labelNavBt1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                labelNavBt1.setName("labelNavBt1");
                labelNavBt1.addMouseListener(new MouseAdapter() {
                  @Override
                  public void mouseClicked(MouseEvent e) {
                    labelNavBt1MouseClicked(e);
                  }
                  
                  @Override
                  public void mouseEntered(MouseEvent e) {
                    labelNavBt1MouseEntered(e);
                  }
                  
                  @Override
                  public void mouseExited(MouseEvent e) {
                    labelNavBt1MouseExited(e);
                  }
                });
                panelNav.add(labelNavBt1);
                labelNavBt1.setBounds(0, 0, 160, 65);
                
                // ---- labelNavBt2 ----
                labelNavBt2.setToolTipText("lignes de bon");
                labelNavBt2.setName("labelNavBt2");
                panelNav.add(labelNavBt2);
                labelNavBt2.setBounds(0, 65, 160, 120);
                
                // ---- labelNavBt3 ----
                labelNavBt3.setToolTipText("pied de bon");
                labelNavBt3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                labelNavBt3.setName("labelNavBt3");
                labelNavBt3.addMouseListener(new MouseAdapter() {
                  @Override
                  public void mouseClicked(MouseEvent e) {
                    labelNavBt3MouseClicked();
                  }
                  
                  @Override
                  public void mouseEntered(MouseEvent e) {
                    labelNavBt3MouseEntered(e);
                  }
                  
                  @Override
                  public void mouseExited(MouseEvent e) {
                    labelNavBt3MouseExited(e);
                  }
                });
                panelNav.add(labelNavBt3);
                labelNavBt3.setBounds(0, 185, 160, 65);
                
                // ---- labelNav ----
                labelNav.setPreferredSize(new Dimension(160, 250));
                labelNav.setMinimumSize(new Dimension(160, 250));
                labelNav.setMaximumSize(new Dimension(160, 250));
                labelNav.setName("labelNav");
                panelNav.add(labelNav);
                labelNav.setBounds(new Rectangle(new Point(0, 0), labelNav.getPreferredSize()));
                
                { // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for (int i = 0; i < panelNav.getComponentCount(); i++) {
                    Rectangle bounds = panelNav.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = panelNav.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  panelNav.setMinimumSize(preferredSize);
                  panelNav.setPreferredSize(preferredSize);
                }
              }
              riSousMenu23.add(panelNav);
            }
            menus_haut.add(riSousMenu23);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1000, 540));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(1000, 540));
          p_contenu.setName("p_contenu");
          
          // ======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);
            
            // ======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");
              
              // ---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST.setViewportView(WTP01);
            }
            panel1.add(SCROLLPANE_LIST);
            SCROLLPANE_LIST.setBounds(10, 20, 925, 270);
            
            // ---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(940, 20, 25, 119);
            
            // ---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(940, 170, 25, 119);
          }
          
          // ======== panel2 ========
          {
            panel2.setBorder(new TitledBorder(""));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);
            
            // ---- WARTT ----
            WARTT.setComponentPopupMenu(BTD2);
            WARTT.setName("WARTT");
            panel2.add(WARTT);
            WARTT.setBounds(95, 44, 210, WARTT.getPreferredSize().height);
            
            // ---- OBJ_117 ----
            OBJ_117.setText("@DVLIBR@");
            OBJ_117.setName("OBJ_117");
            panel2.add(OBJ_117);
            OBJ_117.setBounds(780, 108, 140, OBJ_117.getPreferredSize().height);
            
            // ---- WART21 ----
            WART21.setComponentPopupMenu(BTD);
            WART21.setName("WART21");
            panel2.add(WART21);
            WART21.setBounds(95, 44, 130, WART21.getPreferredSize().height);
            
            // ---- WTHTX ----
            WTHTX.setComponentPopupMenu(FCT1);
            WTHTX.setText("@WTHTX@");
            WTHTX.setHorizontalAlignment(SwingConstants.RIGHT);
            WTHTX.setName("WTHTX");
            panel2.add(WTHTX);
            WTHTX.setBounds(810, 15, 110, WTHTX.getPreferredSize().height);
            
            // ---- WTVAX ----
            WTVAX.setComponentPopupMenu(FCT1);
            WTVAX.setText("@WTVAX@");
            WTVAX.setHorizontalAlignment(SwingConstants.RIGHT);
            WTVAX.setName("WTVAX");
            panel2.add(WTVAX);
            WTVAX.setBounds(810, 46, 110, WTVAX.getPreferredSize().height);
            
            // ---- WTTCX ----
            WTTCX.setComponentPopupMenu(FCT1);
            WTTCX.setText("@WTTCX@");
            WTTCX.setHorizontalAlignment(SwingConstants.RIGHT);
            WTTCX.setName("WTTCX");
            panel2.add(WTTCX);
            WTTCX.setBounds(810, 77, 110, WTTCX.getPreferredSize().height);
            
            // ---- OBJ_110 ----
            OBJ_110.setText("Total TTC");
            OBJ_110.setName("OBJ_110");
            panel2.add(OBJ_110);
            OBJ_110.setBounds(710, 77, 95, 24);
            
            // ---- OBJ_107 ----
            OBJ_107.setText("Total TVA");
            OBJ_107.setName("OBJ_107");
            panel2.add(OBJ_107);
            OBJ_107.setBounds(710, 46, 95, 24);
            
            // ---- OBJ_96 ----
            OBJ_96.setText("Total H.T");
            OBJ_96.setName("OBJ_96");
            panel2.add(OBJ_96);
            OBJ_96.setBounds(710, 15, 95, 24);
            
            // ---- W20NAT ----
            W20NAT.setComponentPopupMenu(BTD);
            W20NAT.setName("W20NAT");
            panel2.add(W20NAT);
            W20NAT.setBounds(154, 125, 60, W20NAT.getPreferredSize().height);
            
            // ---- OBJ_92 ----
            OBJ_92.setText("C     N\u00b0Li");
            OBJ_92.setName("OBJ_92");
            panel2.add(OBJ_92);
            OBJ_92.setBounds(25, 25, 60, 20);
            
            // ---- OBJ_112 ----
            OBJ_112.setText("Nature");
            OBJ_112.setName("OBJ_112");
            panel2.add(OBJ_112);
            OBJ_112.setBounds(156, 105, 43, 20);
            
            // ---- WART22 ----
            WART22.setComponentPopupMenu(BTD);
            WART22.setName("WART22");
            panel2.add(WART22);
            WART22.setBounds(225, 44, 50, WART22.getPreferredSize().height);
            
            // ---- WART23 ----
            WART23.setComponentPopupMenu(BTD);
            WART23.setName("WART23");
            panel2.add(WART23);
            WART23.setBounds(280, 44, 50, WART23.getPreferredSize().height);
            
            // ---- OBJ_116 ----
            OBJ_116.setText("Affaire");
            OBJ_116.setName("OBJ_116");
            panel2.add(OBJ_116);
            OBJ_116.setBounds(106, 105, 42, 18);
            
            // ---- WSAN ----
            WSAN.setComponentPopupMenu(BTD3);
            WSAN.setName("WSAN");
            panel2.add(WSAN);
            WSAN.setBounds(54, 125, 50, WSAN.getPreferredSize().height);
            
            // ---- WACT ----
            WACT.setComponentPopupMenu(BTD3);
            WACT.setName("WACT");
            panel2.add(WACT);
            WACT.setBounds(104, 125, 50, WACT.getPreferredSize().height);
            
            // ---- OBJ_115 ----
            OBJ_115.setText("Sect");
            OBJ_115.setName("OBJ_115");
            panel2.add(OBJ_115);
            OBJ_115.setBounds(56, 105, 34, 18);
            
            // ---- WNLI ----
            WNLI.setComponentPopupMenu(BTD3);
            WNLI.setName("WNLI");
            panel2.add(WNLI);
            WNLI.setBounds(45, 44, 50, WNLI.getPreferredSize().height);
            
            // ---- OBJ_114 ----
            OBJ_114.setText("Mag");
            OBJ_114.setName("OBJ_114");
            panel2.add(OBJ_114);
            OBJ_114.setBounds(22, 105, 28, 18);
            
            // ---- WMAG ----
            WMAG.setComponentPopupMenu(BTD3);
            WMAG.setName("WMAG");
            panel2.add(WMAG);
            WMAG.setBounds(20, 125, 34, WMAG.getPreferredSize().height);
            
            // ---- WNAT ----
            WNAT.setComponentPopupMenu(BTD3);
            WNAT.setName("WNAT");
            panel2.add(WNAT);
            WNAT.setBounds(305, 44, 24, WNAT.getPreferredSize().height);
            
            // ---- WAVR ----
            WAVR.setComponentPopupMenu(BTD3);
            WAVR.setName("WAVR");
            panel2.add(WAVR);
            WAVR.setBounds(330, 44, 24, WAVR.getPreferredSize().height);
            
            // ---- WCOD ----
            WCOD.setComponentPopupMenu(BTD3);
            WCOD.setName("WCOD");
            panel2.add(WCOD);
            WCOD.setBounds(20, 44, 24, WCOD.getPreferredSize().height);
            
            // ---- OBJ_94 ----
            OBJ_94.setText("N");
            OBJ_94.setName("OBJ_94");
            panel2.add(OBJ_94);
            OBJ_94.setBounds(312, 25, 13, 20);
            
            // ---- OBJ_95 ----
            OBJ_95.setText("A");
            OBJ_95.setName("OBJ_95");
            panel2.add(OBJ_95);
            OBJ_95.setBounds(335, 25, 12, 20);
            
            // ---- label1 ----
            label1.setText("Devise");
            label1.setName("label1");
            panel2.add(label1);
            label1.setBounds(710, 108, 64, 24);
            
            // ---- bt_argument ----
            bt_argument.setText(" Article");
            bt_argument.setComponentPopupMenu(null);
            bt_argument.setToolTipText("Type d'argument de recherche");
            bt_argument.setHorizontalAlignment(SwingConstants.LEADING);
            bt_argument.setVerifyInputWhenFocusTarget(false);
            bt_argument.setMargin(new Insets(0, 2, 0, 0));
            bt_argument.setName("bt_argument");
            bt_argument.addActionListener(e -> bt_argumentActionPerformed(e));
            panel2.add(bt_argument);
            bt_argument.setBounds(95, 10, 185, bt_argument.getPreferredSize().height);
            
            // ---- riBoutonDetail1 ----
            riBoutonDetail1.setToolTipText("Recherche avanc\u00e9e");
            riBoutonDetail1.setName("riBoutonDetail1");
            riBoutonDetail1.addActionListener(e -> riBoutonDetail1ActionPerformed(e));
            panel2.add(riBoutonDetail1);
            riBoutonDetail1.setBounds(new Rectangle(new Point(285, 15), riBoutonDetail1.getPreferredSize()));
            
            // ---- ck_saisie ----
            ck_saisie.setText("Saisie quantit\u00e9 sur liste");
            ck_saisie.setToolTipText("Saisie quantit\u00e9 sur une liste ou recherche sur argument");
            ck_saisie.setName("ck_saisie");
            ck_saisie.addActionListener(e -> ck_saisieActionPerformed(e));
            panel2.add(ck_saisie);
            ck_saisie.setBounds(97, 73, 205, ck_saisie.getPreferredSize().height);
            
            // ---- WPDSX ----
            WPDSX.setComponentPopupMenu(FCT1);
            WPDSX.setText("@WPDSX@");
            WPDSX.setHorizontalAlignment(SwingConstants.RIGHT);
            WPDSX.setName("WPDSX");
            panel2.add(WPDSX);
            WPDSX.setBounds(810, 77, 110, WPDSX.getPreferredSize().height);
          }
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup().addContainerGap()
                  .addGroup(p_contenuLayout.createParallelGroup().addComponent(panel1, GroupLayout.DEFAULT_SIZE, 978, Short.MAX_VALUE)
                      .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 978, Short.MAX_VALUE))
                  .addContainerGap()));
          p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap().addComponent(panel1, GroupLayout.PREFERRED_SIZE, 320, GroupLayout.PREFERRED_SIZE).addGap(18, 18, 18)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 169, GroupLayout.PREFERRED_SIZE).addContainerGap(20, Short.MAX_VALUE)));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_25 ----
      OBJ_25.setText("Choisir");
      OBJ_25.setName("OBJ_25");
      OBJ_25.addActionListener(e -> OBJ_25ActionPerformed(e));
      BTD.add(OBJ_25);
      
      // ---- OBJ_26 ----
      OBJ_26.setText("Modifier");
      OBJ_26.setName("OBJ_26");
      OBJ_26.addActionListener(e -> OBJ_26ActionPerformed(e));
      BTD.add(OBJ_26);
      
      // ---- OBJ_27 ----
      OBJ_27.setText("Supprimer");
      OBJ_27.setName("OBJ_27");
      OBJ_27.addActionListener(e -> OBJ_27ActionPerformed(e));
      BTD.add(OBJ_27);
      
      // ---- OBJ_29 ----
      OBJ_29.setText("Ins\u00e9rer une ligne");
      OBJ_29.setName("OBJ_29");
      OBJ_29.addActionListener(e -> OBJ_29ActionPerformed(e));
      BTD.add(OBJ_29);
      
      // ---- OBJ_28 ----
      OBJ_28.setText("Options article");
      OBJ_28.setName("OBJ_28");
      OBJ_28.addActionListener(e -> OBJ_28ActionPerformed(e));
      BTD.add(OBJ_28);
      
      // ---- menuItem1 ----
      menuItem1.setText("+ Autres options");
      menuItem1.setName("menuItem1");
      menuItem1.addActionListener(e -> menuItem1ActionPerformed(e));
      BTD.add(menuItem1);
      BTD.addSeparator();
      
      // ---- OBJ_37 ----
      OBJ_37.setText("Aide en ligne");
      OBJ_37.setName("OBJ_37");
      OBJ_37.addActionListener(e -> OBJ_37ActionPerformed(e));
      BTD.add(OBJ_37);
    }
    
    // ======== BTD2 ========
    {
      BTD2.setName("BTD2");
      
      // ---- OBJ_40 ----
      OBJ_40.setText("Options article");
      OBJ_40.setName("OBJ_40");
      OBJ_40.addActionListener(e -> OBJ_40ActionPerformed(e));
      BTD2.add(OBJ_40);
      
      // ---- separator1 ----
      separator1.setName("separator1");
      BTD2.add(separator1);
      
      // ---- OBJ_42 ----
      OBJ_42.setText("Choix possibles");
      OBJ_42.setName("OBJ_42");
      OBJ_42.addActionListener(e -> OBJ_42ActionPerformed(e));
      BTD2.add(OBJ_42);
      
      // ---- OBJ_41 ----
      OBJ_41.setText("Aide en ligne");
      OBJ_41.setName("OBJ_41");
      OBJ_41.addActionListener(e -> OBJ_41ActionPerformed(e));
      BTD2.add(OBJ_41);
    }
    
    // ======== BTD3 ========
    {
      BTD3.setName("BTD3");
      
      // ---- OBJ_45 ----
      OBJ_45.setText("Choix possibles");
      OBJ_45.setName("OBJ_45");
      OBJ_45.addActionListener(e -> OBJ_45ActionPerformed(e));
      BTD3.add(OBJ_45);
      
      // ---- OBJ_44 ----
      OBJ_44.setText("Aide en ligne");
      OBJ_44.setName("OBJ_44");
      OBJ_44.addActionListener(e -> OBJ_44ActionPerformed(e));
      BTD3.add(OBJ_44);
    }
    
    // ======== FCT1 ========
    {
      FCT1.setName("FCT1");
      
      // ---- OBJ_47 ----
      OBJ_47.setText("Convertisseur mon\u00e9taire");
      OBJ_47.setName("OBJ_47");
      OBJ_47.addActionListener(e -> OBJ_47ActionPerformed(e));
      FCT1.add(OBJ_47);
    }
    
    // ---- OBJ_81 ----
    OBJ_81.setText("@EUR@");
    OBJ_81.setName("OBJ_81");
    
    // ---- TIT ----
    TIT.setText("@TITB@");
    TIT.setName("TIT");
    
    // ======== BTD4 ========
    {
      BTD4.setName("BTD4");
      
      // ---- OBJ_49 ----
      OBJ_49.setText("Affectation adresses de stocks");
      OBJ_49.setName("OBJ_49");
      OBJ_49.addActionListener(e -> OBJ_49ActionPerformed(e));
      BTD4.add(OBJ_49);
      
      // ---- OBJ_50 ----
      OBJ_50.setText("Calcul prix de revient et tarif");
      OBJ_50.setName("OBJ_50");
      OBJ_50.addActionListener(e -> OBJ_30ActionPerformed(e));
      BTD4.add(OBJ_50);
      
      // ---- OBJ_57 ----
      OBJ_57.setText("Lien sur ventes");
      OBJ_57.setName("OBJ_57");
      OBJ_57.addActionListener(e -> OBJ_34ActionPerformed(e));
      BTD4.add(OBJ_57);
      
      // ---- OBJ_58 ----
      OBJ_58.setText("Changement de code article");
      OBJ_58.setName("OBJ_58");
      OBJ_58.addActionListener(e -> OBJ_35ActionPerformed(e));
      BTD4.add(OBJ_58);
      
      // ---- OBJ_55 ----
      OBJ_55.setText("Saisie vecteur (sp\u00e9cial taille/couleur)");
      OBJ_55.setName("OBJ_55");
      OBJ_55.addActionListener(e -> OBJ_32ActionPerformed(e));
      BTD4.add(OBJ_55);
      
      // ---- OBJ_56 ----
      OBJ_56.setText("Saisie tableau (sp\u00e9cial taille/couleur)");
      OBJ_56.setName("OBJ_56");
      OBJ_56.addActionListener(e -> OBJ_33ActionPerformed(e));
      BTD4.add(OBJ_56);
      
      // ---- OBJ_54 ----
      OBJ_54.setText("R\u00e9partition lot");
      OBJ_54.setName("OBJ_54");
      OBJ_54.addActionListener(e -> OBJ_31ActionPerformed(e));
      BTD4.add(OBJ_54);
      
      // ---- OBJ_59 ----
      OBJ_59.setText("Modification informations lot");
      OBJ_59.setName("OBJ_59");
      OBJ_59.addActionListener(e -> OBJ_36ActionPerformed(e));
      BTD4.add(OBJ_59);
      BTD4.addSeparator();
      
      // ---- OBJ_61 ----
      OBJ_61.setText("Aide en ligne");
      OBJ_61.setName("OBJ_61");
      OBJ_61.addActionListener(e -> OBJ_37ActionPerformed(e));
      BTD4.add(OBJ_61);
    }
    
    // ======== riSousMenu14 ========
    {
      riSousMenu14.setName("riSousMenu14");
      
      // ---- riSousMenu_bt14 ----
      riSousMenu_bt14.setText("Options");
      riSousMenu_bt14.setName("riSousMenu_bt14");
      riSousMenu_bt14.addActionListener(e -> riSousMenu_bt14ActionPerformed(e));
      riSousMenu14.add(riSousMenu_bt14);
    }
    
    // ======== arguments ========
    {
      arguments.setName("arguments");
      
      // ---- menuItem2 ----
      menuItem2.setText("Recherche");
      menuItem2.setFont(menuItem2.getFont().deriveFont(Font.BOLD));
      menuItem2.setBackground(new Color(204, 204, 204));
      menuItem2.setOpaque(true);
      menuItem2.setRequestFocusEnabled(false);
      menuItem2.setName("menuItem2");
      arguments.add(menuItem2);
      
      // ---- arg_article ----
      arg_article.setText("par code article");
      arg_article.setName("arg_article");
      arg_article.addActionListener(e -> arg_articleActionPerformed(e));
      arguments.add(arg_article);
      
      // ---- arg_generique ----
      arg_generique.setText("par code g\u00e9n\u00e9rique");
      arg_generique.setActionCommand("par code g\u00e9n\u00e9rique");
      arg_generique.setName("arg_generique");
      arg_generique.addActionListener(e -> arg_generiqueActionPerformed(e));
      arguments.add(arg_generique);
      
      // ---- arg_famille ----
      arg_famille.setText("par famille");
      arg_famille.setName("arg_famille");
      arg_famille.addActionListener(e -> arg_familleActionPerformed(e));
      arguments.add(arg_famille);
      
      // ---- arg_classement ----
      arg_classement.setText("par @TMC1@");
      arg_classement.setName("arg_classement");
      arg_classement.addActionListener(e -> arg_classementActionPerformed(e));
      arguments.add(arg_classement);
      
      // ---- arg_classement2 ----
      arg_classement2.setText("par @TMC2@");
      arg_classement2.setName("arg_classement2");
      arg_classement2.addActionListener(e -> arg_classement2ActionPerformed(e));
      arguments.add(arg_classement2);
      
      // ---- arg_gencod ----
      arg_gencod.setText("par code \u00e0 barres");
      arg_gencod.setName("arg_gencod");
      arg_gencod.addActionListener(e -> arg_gencodActionPerformed(e));
      arguments.add(arg_gencod);
      
      // ---- arg_art_comm ----
      arg_art_comm.setText("Article commentaire");
      arg_art_comm.setToolTipText("Recherche d'article commentaires par le code");
      arg_art_comm.setFont(arg_art_comm.getFont().deriveFont(arg_art_comm.getFont().getStyle() | Font.ITALIC));
      arg_art_comm.setName("arg_art_comm");
      arg_art_comm.addActionListener(e -> arg_art_commActionPerformed(e));
      arguments.add(arg_art_comm);
      
      // ---- arg_comm ----
      arg_comm.setText("Saisie commentaire");
      arg_comm.setBackground(new Color(217, 217, 217));
      arg_comm.setOpaque(true);
      arg_comm.setToolTipText("saisie commentaire libre");
      arg_comm.setFont(arg_comm.getFont().deriveFont(arg_comm.getFont().getStyle() | Font.ITALIC));
      arg_comm.setName("arg_comm");
      arg_comm.addActionListener(e -> arg_commActionPerformed(e));
      arguments.add(arg_comm);
    }
    
    // ---- OBJ_93 ----
    OBJ_93.setText("@WCHPA@");
    OBJ_93.setName("OBJ_93");
    
    // ======== navig_valid3 ========
    {
      navig_valid3.setName("navig_valid3");
      
      // ---- button_ok ----
      button_ok.setText("Valider");
      button_ok.setToolTipText("Valider");
      button_ok.setName("button_ok");
      button_ok.addActionListener(e -> button_okActionPerformed(e));
      navig_valid3.add(button_ok);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private XRiTextField EAETB;
  private XRiTextField WNUM;
  private XRiTextField WSUF;
  private RiZoneSortie OBJ_72;
  private RiZoneSortie OBJ_73;
  private RiZoneSortie OBJ_74;
  private JLabel WDATEX;
  private JLabel OBJ_51;
  private JLabel OBJ_52;
  private JLabel OBJ_53;
  private JPanel p_tete_droite;
  private JLabel label_etat;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiSousMenu riSousMenu13;
  private RiSousMenu_bt riSousMenu_bt13;
  private RiSousMenu riSousMenu16;
  private RiSousMenu_bt riSousMenu_bt17;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private RiMenu riMenu5;
  private RiMenu_bt riMenu_bt5;
  private RiSousMenu riSousMenu23;
  private JPanel panelNav;
  private JLabel labelNavBt1;
  private JLabel labelNavBt2;
  private JLabel labelNavBt3;
  private JLabel labelNav;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPanel panel2;
  private XRiTextField WARTT;
  private RiZoneSortie OBJ_117;
  private XRiTextField WART21;
  private RiZoneSortie WTHTX;
  private RiZoneSortie WTVAX;
  private RiZoneSortie WTTCX;
  private JLabel OBJ_110;
  private JLabel OBJ_107;
  private JLabel OBJ_96;
  private XRiTextField W20NAT;
  private JLabel OBJ_92;
  private JLabel OBJ_112;
  private XRiTextField WART22;
  private XRiTextField WART23;
  private JLabel OBJ_116;
  private XRiTextField WSAN;
  private XRiTextField WACT;
  private JLabel OBJ_115;
  private XRiTextField WNLI;
  private JLabel OBJ_114;
  private XRiTextField WMAG;
  private XRiTextField WNAT;
  private XRiTextField WAVR;
  private XRiTextField WCOD;
  private JLabel OBJ_94;
  private JLabel OBJ_95;
  private JLabel label1;
  private SNBoutonLeger bt_argument;
  private SNBoutonDetail riBoutonDetail1;
  private JCheckBox ck_saisie;
  private RiZoneSortie WPDSX;
  private JPopupMenu BTD;
  private JMenuItem OBJ_25;
  private JMenuItem OBJ_26;
  private JMenuItem OBJ_27;
  private JMenuItem OBJ_29;
  private JMenuItem OBJ_28;
  private JMenuItem menuItem1;
  private JMenuItem OBJ_37;
  private JPopupMenu BTD2;
  private JMenuItem OBJ_40;
  private JSeparator separator1;
  private JMenuItem OBJ_42;
  private JMenuItem OBJ_41;
  private JPopupMenu BTD3;
  private JMenuItem OBJ_45;
  private JMenuItem OBJ_44;
  private JPopupMenu FCT1;
  private JMenuItem OBJ_47;
  private JLabel OBJ_81;
  private JLabel TIT;
  private JPopupMenu BTD4;
  private JMenuItem OBJ_49;
  private JMenuItem OBJ_50;
  private JMenuItem OBJ_57;
  private JMenuItem OBJ_58;
  private JMenuItem OBJ_55;
  private JMenuItem OBJ_56;
  private JMenuItem OBJ_54;
  private JMenuItem OBJ_59;
  private JMenuItem OBJ_61;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private JPopupMenu arguments;
  private JMenuItem menuItem2;
  private JMenuItem arg_article;
  private JMenuItem arg_generique;
  private JMenuItem arg_famille;
  private JMenuItem arg_classement;
  private JMenuItem arg_classement2;
  private JMenuItem arg_gencod;
  private JMenuItem arg_art_comm;
  private JMenuItem arg_comm;
  private JLabel OBJ_93;
  private RiMenu navig_valid3;
  private RiMenu_bt button_ok;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
