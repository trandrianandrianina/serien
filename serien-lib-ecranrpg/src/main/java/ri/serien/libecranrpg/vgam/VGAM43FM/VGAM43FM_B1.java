
package ri.serien.libecranrpg.vgam.VGAM43FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGAM43FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGAM43FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP1@")).trim());
    OBJ_21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP2@")).trim());
    OBJ_23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP3@")).trim());
    OBJ_27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP5@")).trim());
    OBJ_25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP4@")).trim());
    OBJ_29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP6@")).trim());
    OBJ_31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP7@")).trim());
    OBJ_33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP8@")).trim());
    OBJ_35.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP9@")).trim());
    OBJ_37.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP10@")).trim());
    OBJ_39.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP11@")).trim());
    OBJ_41.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP12@")).trim());
    OBJ_43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP13@")).trim());
    OBJ_45.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP14@")).trim());
    OBJ_47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP15@")).trim());
    OBJ_49.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP16@")).trim());
    OBJ_51.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP17@")).trim());
    OBJ_53.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP18@")).trim());
    OBJ_20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP1@")).trim());
    OBJ_22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP2@")).trim());
    OBJ_24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP3@")).trim());
    OBJ_26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP4@")).trim());
    OBJ_28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP5@")).trim());
    OBJ_30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP6@")).trim());
    OBJ_32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP7@")).trim());
    OBJ_34.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP8@")).trim());
    OBJ_36.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP9@")).trim());
    OBJ_38.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP10@")).trim());
    OBJ_40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP11@")).trim());
    OBJ_42.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP12@")).trim());
    OBJ_44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP13@")).trim());
    OBJ_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP14@")).trim());
    OBJ_48.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP15@")).trim());
    OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP16@")).trim());
    OBJ_52.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP17@")).trim());
    OBJ_54.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP18@")).trim());
    OBJ_115.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRNOM@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    int hauteurPop = 530;
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    OBJ_54.setVisible(!lexique.HostFieldGetData("ERZP18").trim().equalsIgnoreCase(""));
    OBJ_52.setVisible(!lexique.HostFieldGetData("ERZP17").trim().equalsIgnoreCase(""));
    OBJ_50.setVisible(!lexique.HostFieldGetData("ERZP16").trim().equalsIgnoreCase(""));
    OBJ_48.setVisible(!lexique.HostFieldGetData("ERZP15").trim().equalsIgnoreCase(""));
    OBJ_46.setVisible(!lexique.HostFieldGetData("ERZP14").trim().equalsIgnoreCase(""));
    OBJ_44.setVisible(!lexique.HostFieldGetData("ERZP13").trim().equalsIgnoreCase(""));
    OBJ_42.setVisible(!lexique.HostFieldGetData("ERZP12").trim().equalsIgnoreCase(""));
    OBJ_40.setVisible(!lexique.HostFieldGetData("ERZP11").trim().equalsIgnoreCase(""));
    OBJ_38.setVisible(!lexique.HostFieldGetData("ERZP10").trim().equalsIgnoreCase(""));
    OBJ_36.setVisible(!lexique.HostFieldGetData("ERZP9").trim().equalsIgnoreCase(""));
    OBJ_34.setVisible(!lexique.HostFieldGetData("ERZP8").trim().equalsIgnoreCase(""));
    OBJ_32.setVisible(!lexique.HostFieldGetData("ERZP7").trim().equalsIgnoreCase(""));
    OBJ_30.setVisible(!lexique.HostFieldGetData("ERZP6").trim().equalsIgnoreCase(""));
    OBJ_28.setVisible(!lexique.HostFieldGetData("ERZP5").trim().equalsIgnoreCase(""));
    OBJ_26.setVisible(!lexique.HostFieldGetData("ERZP4").trim().equalsIgnoreCase(""));
    OBJ_24.setVisible(!lexique.HostFieldGetData("ERZP3").trim().equalsIgnoreCase(""));
    OBJ_22.setVisible(!lexique.HostFieldGetData("ERZP2").trim().equalsIgnoreCase(""));
    OBJ_20.setVisible(!lexique.HostFieldGetData("ERZP1").trim().equalsIgnoreCase(""));
    
    OBJ_53.setVisible(!lexique.HostFieldGetData("WZP18").trim().equalsIgnoreCase("B"));
    OBJ_51.setVisible(!lexique.HostFieldGetData("WZP17").trim().equalsIgnoreCase("B"));
    OBJ_49.setVisible(!lexique.HostFieldGetData("WZP16").trim().equalsIgnoreCase("B"));
    OBJ_47.setVisible(!lexique.HostFieldGetData("WZP15").trim().equalsIgnoreCase("B"));
    OBJ_45.setVisible(!lexique.HostFieldGetData("WZP14").trim().equalsIgnoreCase("B"));
    OBJ_43.setVisible(!lexique.HostFieldGetData("WZP13").trim().equalsIgnoreCase("B"));
    OBJ_41.setVisible(!lexique.HostFieldGetData("WZP12").trim().equalsIgnoreCase("B"));
    OBJ_39.setVisible(!lexique.HostFieldGetData("WZP11").trim().equalsIgnoreCase("B"));
    OBJ_37.setVisible(!lexique.HostFieldGetData("WZP10").trim().equalsIgnoreCase("B"));
    OBJ_35.setVisible(!lexique.HostFieldGetData("WZP9").trim().equalsIgnoreCase("B"));
    OBJ_33.setVisible(!lexique.HostFieldGetData("WZP8").trim().equalsIgnoreCase("B"));
    OBJ_31.setVisible(!lexique.HostFieldGetData("WZP7").trim().equalsIgnoreCase("B"));
    OBJ_29.setVisible(!lexique.HostFieldGetData("WZP6").trim().equalsIgnoreCase("B"));
    OBJ_27.setVisible(!lexique.HostFieldGetData("WZP5").trim().equalsIgnoreCase("B"));
    OBJ_25.setVisible(!lexique.HostFieldGetData("WZP4").trim().equalsIgnoreCase("B"));
    OBJ_23.setVisible(!lexique.HostFieldGetData("WZP3").trim().equalsIgnoreCase("B"));
    OBJ_21.setVisible(!lexique.HostFieldGetData("WZP2").trim().equalsIgnoreCase("B"));
    OBJ_19.setVisible(!lexique.HostFieldGetData("WZP1").trim().equalsIgnoreCase("B"));
    OBJ_115.setVisible(lexique.isPresent("FRNOM"));
    
    boolean isFond = false;
    
    if (lexique.HostFieldGetData("WZP18").trim().equalsIgnoreCase("")) {
      EZZP18.setVisible(false);
      hauteurPop -= 28;
    }
    else {
      EZZP18.setVisible(true);
      isFond = true;
    }
    if (lexique.HostFieldGetData("WZP17").trim().equalsIgnoreCase("")) {
      EZZP17.setVisible(false);
      if (!isFond) {
        hauteurPop -= 28;
      }
    }
    else {
      EZZP17.setVisible(true);
      isFond = true;
    }
    if (lexique.HostFieldGetData("WZP16").trim().equalsIgnoreCase("")) {
      EZZP16.setVisible(false);
      if (!isFond) {
        hauteurPop -= 28;
      }
    }
    else {
      EZZP16.setVisible(true);
      isFond = true;
    }
    if (lexique.HostFieldGetData("WZP15").trim().equalsIgnoreCase("")) {
      EZZP15.setVisible(false);
      if (!isFond) {
        hauteurPop -= 28;
      }
    }
    else {
      EZZP15.setVisible(true);
      isFond = true;
    }
    if (lexique.HostFieldGetData("WZP14").trim().equalsIgnoreCase("")) {
      EZZP14.setVisible(false);
      if (!isFond) {
        hauteurPop -= 28;
      }
    }
    else {
      EZZP14.setVisible(true);
      isFond = true;
    }
    if (lexique.HostFieldGetData("WZP13").trim().equalsIgnoreCase("")) {
      EZZP13.setVisible(false);
      if (!isFond) {
        hauteurPop -= 28;
      }
    }
    else {
      EZZP13.setVisible(true);
      isFond = true;
    }
    if (lexique.HostFieldGetData("WZP12").trim().equalsIgnoreCase("")) {
      EZZP12.setVisible(false);
      if (!isFond) {
        hauteurPop -= 28;
      }
    }
    else {
      EZZP12.setVisible(true);
      isFond = true;
    }
    if (lexique.HostFieldGetData("WZP11").trim().equalsIgnoreCase("")) {
      EZZP11.setVisible(false);
      if (!isFond) {
        hauteurPop -= 28;
      }
    }
    else {
      EZZP11.setVisible(true);
      isFond = true;
    }
    if (lexique.HostFieldGetData("WZP10").trim().equalsIgnoreCase("")) {
      EZZP10.setVisible(false);
      if (!isFond) {
        hauteurPop -= 28;
      }
    }
    else {
      EZZP10.setVisible(true);
      isFond = true;
    }
    if (lexique.HostFieldGetData("WZP9").trim().equalsIgnoreCase("")) {
      EZZP9.setVisible(false);
      if (!isFond) {
        hauteurPop -= 28;
      }
    }
    else {
      EZZP9.setVisible(true);
      isFond = true;
    }
    if (lexique.HostFieldGetData("WZP8").trim().equalsIgnoreCase("")) {
      EZZP8.setVisible(false);
      if (!isFond) {
        hauteurPop -= 28;
      }
    }
    else {
      EZZP8.setVisible(true);
      isFond = true;
    }
    if (lexique.HostFieldGetData("WZP7").trim().equalsIgnoreCase("")) {
      EZZP7.setVisible(false);
      if (!isFond) {
        hauteurPop -= 28;
      }
    }
    else {
      EZZP7.setVisible(true);
      isFond = true;
    }
    if (lexique.HostFieldGetData("WZP6").trim().equalsIgnoreCase("")) {
      EZZP6.setVisible(false);
      if (!isFond) {
        hauteurPop -= 28;
      }
    }
    else {
      EZZP6.setVisible(true);
      isFond = true;
    }
    if (lexique.HostFieldGetData("WZP5").trim().equalsIgnoreCase("")) {
      EZZP5.setVisible(false);
      if (!isFond) {
        hauteurPop -= 28;
      }
    }
    else {
      EZZP5.setVisible(true);
      isFond = true;
    }
    if (lexique.HostFieldGetData("WZP4").trim().equalsIgnoreCase("")) {
      EZZP4.setVisible(false);
      if (!isFond) {
        hauteurPop -= 28;
      }
    }
    else {
      EZZP4.setVisible(true);
      isFond = true;
    }
    if (lexique.HostFieldGetData("WZP3").trim().equalsIgnoreCase("")) {
      EZZP3.setVisible(false);
      if (!isFond) {
        hauteurPop -= 28;
      }
    }
    else {
      EZZP3.setVisible(true);
      isFond = true;
    }
    if (lexique.HostFieldGetData("WZP2").trim().equalsIgnoreCase("")) {
      EZZP2.setVisible(false);
      if (!isFond) {
        hauteurPop -= 28;
      }
    }
    else {
      EZZP2.setVisible(true);
      isFond = true;
    }
    if (lexique.HostFieldGetData("WZP1").trim().equalsIgnoreCase("")) {
      EZZP1.setVisible(false);
      if (!isFond) {
        hauteurPop -= 28;
      }
    }
    else {
      EZZP1.setVisible(true);
      isFond = true;
    }
    
    panel1.setSize(680, hauteurPop);
    this.setPreferredSize(new Dimension(870, hauteurPop + 85));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Extension fiche fournisseur"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_19 = new JLabel();
    OBJ_21 = new JLabel();
    OBJ_23 = new JLabel();
    OBJ_27 = new JLabel();
    OBJ_25 = new JLabel();
    OBJ_29 = new JLabel();
    OBJ_31 = new JLabel();
    OBJ_33 = new JLabel();
    OBJ_35 = new JLabel();
    OBJ_37 = new JLabel();
    OBJ_39 = new JLabel();
    OBJ_41 = new JLabel();
    OBJ_43 = new JLabel();
    OBJ_45 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_49 = new JLabel();
    OBJ_51 = new JLabel();
    OBJ_53 = new JLabel();
    OBJ_20 = new RiZoneSortie();
    OBJ_22 = new RiZoneSortie();
    OBJ_24 = new RiZoneSortie();
    OBJ_26 = new RiZoneSortie();
    OBJ_28 = new RiZoneSortie();
    OBJ_30 = new RiZoneSortie();
    OBJ_32 = new RiZoneSortie();
    OBJ_34 = new RiZoneSortie();
    OBJ_36 = new RiZoneSortie();
    OBJ_38 = new RiZoneSortie();
    OBJ_40 = new RiZoneSortie();
    OBJ_42 = new RiZoneSortie();
    OBJ_44 = new RiZoneSortie();
    OBJ_46 = new RiZoneSortie();
    OBJ_48 = new RiZoneSortie();
    OBJ_50 = new RiZoneSortie();
    OBJ_52 = new RiZoneSortie();
    OBJ_54 = new RiZoneSortie();
    EZZP1 = new XRiTextField();
    EZZP2 = new XRiTextField();
    EZZP3 = new XRiTextField();
    EZZP4 = new XRiTextField();
    EZZP5 = new XRiTextField();
    EZZP6 = new XRiTextField();
    EZZP7 = new XRiTextField();
    EZZP8 = new XRiTextField();
    EZZP9 = new XRiTextField();
    EZZP10 = new XRiTextField();
    EZZP11 = new XRiTextField();
    EZZP12 = new XRiTextField();
    EZZP13 = new XRiTextField();
    EZZP14 = new XRiTextField();
    EZZP15 = new XRiTextField();
    EZZP16 = new XRiTextField();
    EZZP17 = new XRiTextField();
    EZZP18 = new XRiTextField();
    barre_tete = new JMenuBar();
    panel2 = new JPanel();
    FRCOL = new XRiTextField();
    FRFRS = new XRiTextField();
    OBJ_56 = new JLabel();
    OBJ_115 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(870, 595));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 260));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_19 ----
          OBJ_19.setText("@WZP1@");
          OBJ_19.setName("OBJ_19");
          panel1.add(OBJ_19);
          OBJ_19.setBounds(18, 14, 207, 20);

          //---- OBJ_21 ----
          OBJ_21.setText("@WZP2@");
          OBJ_21.setName("OBJ_21");
          panel1.add(OBJ_21);
          OBJ_21.setBounds(18, 42, 207, 20);

          //---- OBJ_23 ----
          OBJ_23.setText("@WZP3@");
          OBJ_23.setName("OBJ_23");
          panel1.add(OBJ_23);
          OBJ_23.setBounds(18, 70, 207, 20);

          //---- OBJ_27 ----
          OBJ_27.setText("@WZP5@");
          OBJ_27.setName("OBJ_27");
          panel1.add(OBJ_27);
          OBJ_27.setBounds(18, 126, 207, 20);

          //---- OBJ_25 ----
          OBJ_25.setText("@WZP4@");
          OBJ_25.setName("OBJ_25");
          panel1.add(OBJ_25);
          OBJ_25.setBounds(18, 98, 207, 20);

          //---- OBJ_29 ----
          OBJ_29.setText("@WZP6@");
          OBJ_29.setName("OBJ_29");
          panel1.add(OBJ_29);
          OBJ_29.setBounds(18, 154, 207, 20);

          //---- OBJ_31 ----
          OBJ_31.setText("@WZP7@");
          OBJ_31.setName("OBJ_31");
          panel1.add(OBJ_31);
          OBJ_31.setBounds(18, 182, 207, 20);

          //---- OBJ_33 ----
          OBJ_33.setText("@WZP8@");
          OBJ_33.setName("OBJ_33");
          panel1.add(OBJ_33);
          OBJ_33.setBounds(18, 210, 207, 20);

          //---- OBJ_35 ----
          OBJ_35.setText("@WZP9@");
          OBJ_35.setName("OBJ_35");
          panel1.add(OBJ_35);
          OBJ_35.setBounds(18, 238, 207, 20);

          //---- OBJ_37 ----
          OBJ_37.setText("@WZP10@");
          OBJ_37.setName("OBJ_37");
          panel1.add(OBJ_37);
          OBJ_37.setBounds(18, 266, 207, 20);

          //---- OBJ_39 ----
          OBJ_39.setText("@WZP11@");
          OBJ_39.setName("OBJ_39");
          panel1.add(OBJ_39);
          OBJ_39.setBounds(18, 294, 207, 20);

          //---- OBJ_41 ----
          OBJ_41.setText("@WZP12@");
          OBJ_41.setName("OBJ_41");
          panel1.add(OBJ_41);
          OBJ_41.setBounds(18, 322, 207, 20);

          //---- OBJ_43 ----
          OBJ_43.setText("@WZP13@");
          OBJ_43.setName("OBJ_43");
          panel1.add(OBJ_43);
          OBJ_43.setBounds(18, 350, 207, 20);

          //---- OBJ_45 ----
          OBJ_45.setText("@WZP14@");
          OBJ_45.setName("OBJ_45");
          panel1.add(OBJ_45);
          OBJ_45.setBounds(18, 378, 207, 20);

          //---- OBJ_47 ----
          OBJ_47.setText("@WZP15@");
          OBJ_47.setName("OBJ_47");
          panel1.add(OBJ_47);
          OBJ_47.setBounds(18, 406, 207, 20);

          //---- OBJ_49 ----
          OBJ_49.setText("@WZP16@");
          OBJ_49.setName("OBJ_49");
          panel1.add(OBJ_49);
          OBJ_49.setBounds(18, 434, 207, 20);

          //---- OBJ_51 ----
          OBJ_51.setText("@WZP17@");
          OBJ_51.setName("OBJ_51");
          panel1.add(OBJ_51);
          OBJ_51.setBounds(18, 462, 207, 20);

          //---- OBJ_53 ----
          OBJ_53.setText("@WZP18@");
          OBJ_53.setName("OBJ_53");
          panel1.add(OBJ_53);
          OBJ_53.setBounds(18, 490, 207, 20);

          //---- OBJ_20 ----
          OBJ_20.setText("@ERZP1@");
          OBJ_20.setName("OBJ_20");
          panel1.add(OBJ_20);
          OBJ_20.setBounds(500, 12, 155, OBJ_20.getPreferredSize().height);

          //---- OBJ_22 ----
          OBJ_22.setText("@ERZP2@");
          OBJ_22.setName("OBJ_22");
          panel1.add(OBJ_22);
          OBJ_22.setBounds(500, 40, 155, OBJ_22.getPreferredSize().height);

          //---- OBJ_24 ----
          OBJ_24.setText("@ERZP3@");
          OBJ_24.setName("OBJ_24");
          panel1.add(OBJ_24);
          OBJ_24.setBounds(500, 68, 155, OBJ_24.getPreferredSize().height);

          //---- OBJ_26 ----
          OBJ_26.setText("@ERZP4@");
          OBJ_26.setName("OBJ_26");
          panel1.add(OBJ_26);
          OBJ_26.setBounds(500, 96, 155, OBJ_26.getPreferredSize().height);

          //---- OBJ_28 ----
          OBJ_28.setText("@ERZP5@");
          OBJ_28.setName("OBJ_28");
          panel1.add(OBJ_28);
          OBJ_28.setBounds(500, 124, 155, OBJ_28.getPreferredSize().height);

          //---- OBJ_30 ----
          OBJ_30.setText("@ERZP6@");
          OBJ_30.setName("OBJ_30");
          panel1.add(OBJ_30);
          OBJ_30.setBounds(500, 152, 155, OBJ_30.getPreferredSize().height);

          //---- OBJ_32 ----
          OBJ_32.setText("@ERZP7@");
          OBJ_32.setName("OBJ_32");
          panel1.add(OBJ_32);
          OBJ_32.setBounds(500, 180, 155, OBJ_32.getPreferredSize().height);

          //---- OBJ_34 ----
          OBJ_34.setText("@ERZP8@");
          OBJ_34.setName("OBJ_34");
          panel1.add(OBJ_34);
          OBJ_34.setBounds(500, 208, 155, OBJ_34.getPreferredSize().height);

          //---- OBJ_36 ----
          OBJ_36.setText("@ERZP9@");
          OBJ_36.setName("OBJ_36");
          panel1.add(OBJ_36);
          OBJ_36.setBounds(500, 236, 155, OBJ_36.getPreferredSize().height);

          //---- OBJ_38 ----
          OBJ_38.setText("@ERZP10@");
          OBJ_38.setName("OBJ_38");
          panel1.add(OBJ_38);
          OBJ_38.setBounds(500, 264, 155, OBJ_38.getPreferredSize().height);

          //---- OBJ_40 ----
          OBJ_40.setText("@ERZP11@");
          OBJ_40.setName("OBJ_40");
          panel1.add(OBJ_40);
          OBJ_40.setBounds(500, 292, 155, OBJ_40.getPreferredSize().height);

          //---- OBJ_42 ----
          OBJ_42.setText("@ERZP12@");
          OBJ_42.setName("OBJ_42");
          panel1.add(OBJ_42);
          OBJ_42.setBounds(500, 320, 155, OBJ_42.getPreferredSize().height);

          //---- OBJ_44 ----
          OBJ_44.setText("@ERZP13@");
          OBJ_44.setName("OBJ_44");
          panel1.add(OBJ_44);
          OBJ_44.setBounds(500, 348, 155, OBJ_44.getPreferredSize().height);

          //---- OBJ_46 ----
          OBJ_46.setText("@ERZP14@");
          OBJ_46.setName("OBJ_46");
          panel1.add(OBJ_46);
          OBJ_46.setBounds(500, 376, 155, OBJ_46.getPreferredSize().height);

          //---- OBJ_48 ----
          OBJ_48.setText("@ERZP15@");
          OBJ_48.setName("OBJ_48");
          panel1.add(OBJ_48);
          OBJ_48.setBounds(500, 404, 155, OBJ_48.getPreferredSize().height);

          //---- OBJ_50 ----
          OBJ_50.setText("@ERZP16@");
          OBJ_50.setName("OBJ_50");
          panel1.add(OBJ_50);
          OBJ_50.setBounds(500, 432, 155, OBJ_50.getPreferredSize().height);

          //---- OBJ_52 ----
          OBJ_52.setText("@ERZP17@");
          OBJ_52.setName("OBJ_52");
          panel1.add(OBJ_52);
          OBJ_52.setBounds(500, 460, 155, OBJ_52.getPreferredSize().height);

          //---- OBJ_54 ----
          OBJ_54.setText("@ERZP18@");
          OBJ_54.setName("OBJ_54");
          panel1.add(OBJ_54);
          OBJ_54.setBounds(500, 488, 155, OBJ_54.getPreferredSize().height);

          //---- EZZP1 ----
          EZZP1.setComponentPopupMenu(BTD);
          EZZP1.setName("EZZP1");
          panel1.add(EZZP1);
          EZZP1.setBounds(240, 10, 252, 28);

          //---- EZZP2 ----
          EZZP2.setComponentPopupMenu(BTD);
          EZZP2.setName("EZZP2");
          panel1.add(EZZP2);
          EZZP2.setBounds(240, 38, 252, 28);

          //---- EZZP3 ----
          EZZP3.setComponentPopupMenu(BTD);
          EZZP3.setName("EZZP3");
          panel1.add(EZZP3);
          EZZP3.setBounds(240, 66, 252, 28);

          //---- EZZP4 ----
          EZZP4.setComponentPopupMenu(BTD);
          EZZP4.setName("EZZP4");
          panel1.add(EZZP4);
          EZZP4.setBounds(240, 94, 252, 28);

          //---- EZZP5 ----
          EZZP5.setComponentPopupMenu(BTD);
          EZZP5.setName("EZZP5");
          panel1.add(EZZP5);
          EZZP5.setBounds(240, 122, 252, 28);

          //---- EZZP6 ----
          EZZP6.setComponentPopupMenu(BTD);
          EZZP6.setName("EZZP6");
          panel1.add(EZZP6);
          EZZP6.setBounds(240, 150, 252, 28);

          //---- EZZP7 ----
          EZZP7.setComponentPopupMenu(BTD);
          EZZP7.setName("EZZP7");
          panel1.add(EZZP7);
          EZZP7.setBounds(240, 178, 252, 28);

          //---- EZZP8 ----
          EZZP8.setComponentPopupMenu(BTD);
          EZZP8.setName("EZZP8");
          panel1.add(EZZP8);
          EZZP8.setBounds(240, 206, 252, 28);

          //---- EZZP9 ----
          EZZP9.setComponentPopupMenu(BTD);
          EZZP9.setName("EZZP9");
          panel1.add(EZZP9);
          EZZP9.setBounds(240, 234, 252, 28);

          //---- EZZP10 ----
          EZZP10.setComponentPopupMenu(BTD);
          EZZP10.setName("EZZP10");
          panel1.add(EZZP10);
          EZZP10.setBounds(240, 262, 252, 28);

          //---- EZZP11 ----
          EZZP11.setComponentPopupMenu(BTD);
          EZZP11.setName("EZZP11");
          panel1.add(EZZP11);
          EZZP11.setBounds(240, 290, 252, 28);

          //---- EZZP12 ----
          EZZP12.setComponentPopupMenu(BTD);
          EZZP12.setName("EZZP12");
          panel1.add(EZZP12);
          EZZP12.setBounds(240, 318, 252, 28);

          //---- EZZP13 ----
          EZZP13.setComponentPopupMenu(BTD);
          EZZP13.setName("EZZP13");
          panel1.add(EZZP13);
          EZZP13.setBounds(240, 346, 252, 28);

          //---- EZZP14 ----
          EZZP14.setComponentPopupMenu(BTD);
          EZZP14.setName("EZZP14");
          panel1.add(EZZP14);
          EZZP14.setBounds(240, 374, 252, 28);

          //---- EZZP15 ----
          EZZP15.setComponentPopupMenu(BTD);
          EZZP15.setName("EZZP15");
          panel1.add(EZZP15);
          EZZP15.setBounds(240, 402, 252, 28);

          //---- EZZP16 ----
          EZZP16.setComponentPopupMenu(BTD);
          EZZP16.setName("EZZP16");
          panel1.add(EZZP16);
          EZZP16.setBounds(240, 430, 252, 28);

          //---- EZZP17 ----
          EZZP17.setComponentPopupMenu(BTD);
          EZZP17.setName("EZZP17");
          panel1.add(EZZP17);
          EZZP17.setBounds(240, 458, 252, 28);

          //---- EZZP18 ----
          EZZP18.setComponentPopupMenu(BTD);
          EZZP18.setName("EZZP18");
          panel1.add(EZZP18);
          EZZP18.setBounds(240, 486, 252, 28);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 20, 680, 530);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== barre_tete ========
    {
      barre_tete.setPreferredSize(new Dimension(111, 30));
      barre_tete.setName("barre_tete");

      //======== panel2 ========
      {
        panel2.setPreferredSize(new Dimension(665, 30));
        panel2.setBorder(null);
        panel2.setOpaque(false);
        panel2.setName("panel2");
        panel2.setLayout(null);

        //---- FRCOL ----
        FRCOL.setName("FRCOL");
        panel2.add(FRCOL);
        FRCOL.setBounds(80, 1, 20, 26);

        //---- FRFRS ----
        FRFRS.setName("FRFRS");
        panel2.add(FRFRS);
        FRFRS.setBounds(105, 1, 70, 26);

        //---- OBJ_56 ----
        OBJ_56.setText("Num\u00e9ro");
        OBJ_56.setName("OBJ_56");
        panel2.add(OBJ_56);
        OBJ_56.setBounds(8, 5, 62, 18);

        //---- OBJ_115 ----
        OBJ_115.setText("@FRNOM@");
        OBJ_115.setName("OBJ_115");
        panel2.add(OBJ_115);
        OBJ_115.setBounds(193, 5, 232, 18);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel2.getComponentCount(); i++) {
            Rectangle bounds = panel2.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel2.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel2.setMinimumSize(preferredSize);
          panel2.setPreferredSize(preferredSize);
        }
      }
      barre_tete.add(panel2);
    }
    add(barre_tete, BorderLayout.NORTH);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_19;
  private JLabel OBJ_21;
  private JLabel OBJ_23;
  private JLabel OBJ_27;
  private JLabel OBJ_25;
  private JLabel OBJ_29;
  private JLabel OBJ_31;
  private JLabel OBJ_33;
  private JLabel OBJ_35;
  private JLabel OBJ_37;
  private JLabel OBJ_39;
  private JLabel OBJ_41;
  private JLabel OBJ_43;
  private JLabel OBJ_45;
  private JLabel OBJ_47;
  private JLabel OBJ_49;
  private JLabel OBJ_51;
  private JLabel OBJ_53;
  private RiZoneSortie OBJ_20;
  private RiZoneSortie OBJ_22;
  private RiZoneSortie OBJ_24;
  private RiZoneSortie OBJ_26;
  private RiZoneSortie OBJ_28;
  private RiZoneSortie OBJ_30;
  private RiZoneSortie OBJ_32;
  private RiZoneSortie OBJ_34;
  private RiZoneSortie OBJ_36;
  private RiZoneSortie OBJ_38;
  private RiZoneSortie OBJ_40;
  private RiZoneSortie OBJ_42;
  private RiZoneSortie OBJ_44;
  private RiZoneSortie OBJ_46;
  private RiZoneSortie OBJ_48;
  private RiZoneSortie OBJ_50;
  private RiZoneSortie OBJ_52;
  private RiZoneSortie OBJ_54;
  private XRiTextField EZZP1;
  private XRiTextField EZZP2;
  private XRiTextField EZZP3;
  private XRiTextField EZZP4;
  private XRiTextField EZZP5;
  private XRiTextField EZZP6;
  private XRiTextField EZZP7;
  private XRiTextField EZZP8;
  private XRiTextField EZZP9;
  private XRiTextField EZZP10;
  private XRiTextField EZZP11;
  private XRiTextField EZZP12;
  private XRiTextField EZZP13;
  private XRiTextField EZZP14;
  private XRiTextField EZZP15;
  private XRiTextField EZZP16;
  private XRiTextField EZZP17;
  private XRiTextField EZZP18;
  private JMenuBar barre_tete;
  private JPanel panel2;
  private XRiTextField FRCOL;
  private XRiTextField FRFRS;
  private JLabel OBJ_56;
  private JLabel OBJ_115;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
