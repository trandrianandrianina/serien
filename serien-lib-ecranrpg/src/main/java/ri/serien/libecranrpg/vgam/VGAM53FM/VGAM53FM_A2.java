
package ri.serien.libecranrpg.vgam.VGAM53FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGAM53FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] tops = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11", "WTP12",
      "WTP13", "WTP14", "WTP15", "WTP16", "WTP17", "WTP18" };
  private String[] modifs = { "LD301", "LD302", "LD303", "LD304", "LD305", "LD306", "LD307", "LD308", "LD309", "LD310", "LD311", "LD312",
      "LD313", "LD314", "LD315", "LD316", "LD317", "LD318" };
  
  public VGAM53FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    LIBETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBETB@")).trim());
    ARTRCH.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ARTRCH@")).trim());
    GFARCH.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@GFARCH@")).trim());
    SFARCH.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SFARCH@")).trim());
    FRSRCH.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRSRCH@")).trim());
    LD101.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD101@")).trim());
    LD102.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD102@")).trim());
    LD103.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD103@")).trim());
    LD104.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD104@")).trim());
    LD105.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD105@")).trim());
    LD106.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD106@")).trim());
    LD107.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD107@")).trim());
    LD108.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD108@")).trim());
    LD109.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD109@")).trim());
    LD110.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD110@")).trim());
    LD111.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD111@")).trim());
    LD112.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD112@")).trim());
    LD113.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD113@")).trim());
    LD114.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD114@")).trim());
    LD115.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD115@")).trim());
    LD116.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD116@")).trim());
    LD117.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD117@")).trim());
    LD118.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD118@")).trim());
    LD201.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD201@")).trim());
    LD202.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD202@")).trim());
    LD203.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD203@")).trim());
    LD204.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD204@")).trim());
    LD205.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD205@")).trim());
    LD206.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD206@")).trim());
    LD207.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD207@")).trim());
    LD208.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD208@")).trim());
    LD209.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD209@")).trim());
    LD210.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD210@")).trim());
    LD211.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD211@")).trim());
    LD212.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD212@")).trim());
    LD213.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD213@")).trim());
    LD214.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD214@")).trim());
    LD215.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD215@")).trim());
    LD216.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD216@")).trim());
    LD217.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD217@")).trim());
    LD218.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD218@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    
    RiZoneSortie[] libellesArticles =
        { LD101, LD102, LD103, LD104, LD105, LD106, LD107, LD108, LD109, LD110, LD111, LD112, LD113, LD114, LD115, LD116, LD117, LD118 };
    RiZoneSortie[] libellesUA = { UA1, UA2, UA3, UA4, UA5, UA6, UA7, UA8, UA9, UA10, UA11, UA12, UA13, UA14, UA15, UA16, UA17, UA18 };
    RiZoneSortie[] libellesPRV =
        { LD201, LD202, LD203, LD204, LD205, LD206, LD207, LD208, LD209, LD210, LD211, LD212, LD213, LD214, LD215, LD216, LD217, LD218 };
    RiZoneSortie[] libellesUPRV = { UPRV1, UPRV2, UPRV3, UPRV4, UPRV5, UPRV6, UPRV7, UPRV8, UPRV9, UPRV10, UPRV11, UPRV12, UPRV13, UPRV14,
        UPRV15, UPRV16, UPRV17, UPRV18 };
    JLabel[] slash = { lbSlash1, lbSlash2, lbSlash3, lbSlash4, lbSlash5, lbSlash6, lbSlash7, lbSlash8, lbSlash9, lbSlash10, lbSlash11,
        lbSlash12, lbSlash13, lbSlash14, lbSlash15, lbSlash16, lbSlash17, lbSlash18, lbSlash19, lbSlash20, lbSlash21, lbSlash22, lbSlash23,
        lbSlash24, lbSlash25, lbSlash26, lbSlash27, lbSlash28, lbSlash29, lbSlash30, lbSlash31, lbSlash32, lbSlash33, lbSlash34, lbSlash35,
        lbSlash36 };
    
    for (int i = 0; i < libellesPRV.length; i++) {
      String depart = lexique.HostFieldGetData(libellesPRV[i].getName()).trim();
      if (!depart.trim().isEmpty()) {
        libellesUA[i].setText(depart.substring(0, depart.indexOf(' ')).trim());
        libellesPRV[i].setText(depart.substring(depart.indexOf(' '), depart.lastIndexOf(' ')));
        libellesUPRV[i].setText(depart.substring(depart.lastIndexOf(' ')).trim());
      }
      libellesArticles[i].setVisible(!depart.trim().isEmpty());
      libellesUA[i].setVisible(!depart.trim().isEmpty());
      libellesPRV[i].setVisible(!depart.trim().isEmpty());
      libellesUPRV[i].setVisible(!depart.trim().isEmpty());
      slash[i].setVisible(!depart.trim().isEmpty());
      slash[i + libellesPRV.length].setVisible(!depart.trim().isEmpty());
    }
    
    XRiTextField[] pourcentages =
        { PRC01, PRC02, PRC03, PRC04, PRC05, PRC06, PRC07, PRC08, PRC09, PRC10, PRC11, PRC12, PRC13, PRC14, PRC15, PRC16, PRC17, PRC18 };
    for (int i = 0; i < modifs.length; i++) {
      pourcentages[i].setFocusTraversalKeysEnabled(false);
      if (!lexique.HostFieldGetData(modifs[i]).trim().equals("")) {
        pourcentages[i].setForeground(Constantes.COULEUR_LISTE_ERREUR);
        pourcentages[i].setToolTipText("Evolution tarifaire entrée manuellement");
        if (pourcentages[i].getText().trim().equals("")) {
          pourcentages[i].setText("0");
        }
      }
      else {
        pourcentages[i].setForeground(Constantes.CL_TEXT_SORTIE);
        pourcentages[i].setToolTipText("");
      }
    }
    
    XRiTextField[] valeurs =
        { PRA01, PRA02, PRA03, PRA04, PRA05, PRA06, PRA07, PRA08, PRA09, PRA10, PRA11, PRA12, PRA13, PRA14, PRA15, PRA16, PRA17, PRA18 };
    for (int i = 0; i < valeurs.length; i++) {
      valeurs[i].setFocusTraversalKeysEnabled(false);
    }
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void jmFicheArticleActionPerformed(ActionEvent e) {
    String origine = BTD.getInvoker().getName();
    origine = origine.substring(origine.length() - 2);
    int indiceTops = Integer.parseInt(origine) - 1;
    if (indiceTops >= 0 && indiceTops <= tops.length) {
      lexique.HostFieldPutData(tops[indiceTops], 0, "1");
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void jmEvolutionActionPerformed(ActionEvent e) {
    String origine = BTD.getInvoker().getName();
    origine = origine.substring(origine.length() - 2);
    int indiceTops = Integer.parseInt(origine) - 1;
    if (indiceTops >= 0 && indiceTops <= tops.length) {
      lexique.HostFieldPutData(tops[indiceTops], 0, "2");
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  
  private void ZonesKeyReleased(KeyEvent e) {
    String code = "";
    switch (e.getKeyCode()) {
      case 9:
        code = ">";
        if (e.getModifiers() > 0) {
          code = "<";
        }
        break;
      case 37:
        code = "<";
        break;
      case 38:
        code = "^";
        break;
      case 39:
        code = ">";
        break;
      case 40:
        code = "V";
        break;
      
      default:
        code = "";
        break;
    }
    String nomChampsOrigine = ((XRiTextField)e.getSource()).getName();
    lexique.HostFieldPutData("XXLIG", 0, String.valueOf(lexique.getHostField(nomChampsOrigine).getLigne()));
    lexique.HostFieldPutData("XXCOL", 0, String.valueOf(lexique.getHostField(nomChampsOrigine).getColonne()));    
    
    lexique.HostFieldPutData("ACTION", 0, code);
    if (!code.trim().isEmpty()) {
      lexique.HostScreenSendKey(this, "F6");
    }
  }

  private void champsMouseReleased(MouseEvent e) {
    String nomChampsOrigine = ((XRiTextField)e.getSource()).getName();
    lexique.HostFieldPutData("XXLIG", 0, String.valueOf(lexique.getHostField(nomChampsOrigine).getLigne()));
    lexique.HostFieldPutData("XXCOL", 0, String.valueOf(lexique.getHostField(nomChampsOrigine).getColonne()));  
    
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    DEMETB = new XRiTextField();
    OBJ_67 = new JLabel();
    LIBETB = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_34 = new JLabel();
    ARTRCH = new RiZoneSortie();
    OBJ_38 = new JLabel();
    GFARCH = new RiZoneSortie();
    OBJ_41 = new JLabel();
    SFARCH = new RiZoneSortie();
    OBJ_45 = new JLabel();
    FRSRCH = new RiZoneSortie();
    DATAPP = new XRiCalendrier();
    OBJ_46 = new JLabel();
    panel2 = new JPanel();
    LD101 = new RiZoneSortie();
    LD102 = new RiZoneSortie();
    LD103 = new RiZoneSortie();
    LD104 = new RiZoneSortie();
    LD105 = new RiZoneSortie();
    LD106 = new RiZoneSortie();
    LD107 = new RiZoneSortie();
    LD108 = new RiZoneSortie();
    LD109 = new RiZoneSortie();
    LD110 = new RiZoneSortie();
    LD111 = new RiZoneSortie();
    LD112 = new RiZoneSortie();
    LD113 = new RiZoneSortie();
    LD114 = new RiZoneSortie();
    LD115 = new RiZoneSortie();
    LD116 = new RiZoneSortie();
    LD117 = new RiZoneSortie();
    LD118 = new RiZoneSortie();
    PRA01 = new XRiTextField();
    PRA02 = new XRiTextField();
    PRA03 = new XRiTextField();
    PRA04 = new XRiTextField();
    PRA05 = new XRiTextField();
    PRA06 = new XRiTextField();
    PRA07 = new XRiTextField();
    PRA08 = new XRiTextField();
    PRA09 = new XRiTextField();
    PRA10 = new XRiTextField();
    PRA11 = new XRiTextField();
    PRA12 = new XRiTextField();
    PRA13 = new XRiTextField();
    PRA14 = new XRiTextField();
    PRA15 = new XRiTextField();
    PRA16 = new XRiTextField();
    PRA17 = new XRiTextField();
    PRA18 = new XRiTextField();
    LD201 = new RiZoneSortie();
    LD202 = new RiZoneSortie();
    LD203 = new RiZoneSortie();
    LD204 = new RiZoneSortie();
    LD205 = new RiZoneSortie();
    LD206 = new RiZoneSortie();
    LD207 = new RiZoneSortie();
    LD208 = new RiZoneSortie();
    LD209 = new RiZoneSortie();
    LD210 = new RiZoneSortie();
    LD211 = new RiZoneSortie();
    LD212 = new RiZoneSortie();
    LD213 = new RiZoneSortie();
    LD214 = new RiZoneSortie();
    LD215 = new RiZoneSortie();
    LD216 = new RiZoneSortie();
    LD217 = new RiZoneSortie();
    LD218 = new RiZoneSortie();
    PRC01 = new XRiTextField();
    PRC02 = new XRiTextField();
    PRC03 = new XRiTextField();
    PRC04 = new XRiTextField();
    PRC05 = new XRiTextField();
    PRC06 = new XRiTextField();
    PRC07 = new XRiTextField();
    PRC08 = new XRiTextField();
    PRC09 = new XRiTextField();
    PRC10 = new XRiTextField();
    PRC11 = new XRiTextField();
    PRC12 = new XRiTextField();
    PRC13 = new XRiTextField();
    PRC14 = new XRiTextField();
    PRC15 = new XRiTextField();
    PRC16 = new XRiTextField();
    PRC17 = new XRiTextField();
    PRC18 = new XRiTextField();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    UA1 = new RiZoneSortie();
    UA2 = new RiZoneSortie();
    UA3 = new RiZoneSortie();
    UA4 = new RiZoneSortie();
    UA5 = new RiZoneSortie();
    UA6 = new RiZoneSortie();
    UA7 = new RiZoneSortie();
    UA8 = new RiZoneSortie();
    UA9 = new RiZoneSortie();
    UA10 = new RiZoneSortie();
    UA11 = new RiZoneSortie();
    UA12 = new RiZoneSortie();
    UA13 = new RiZoneSortie();
    UA14 = new RiZoneSortie();
    UA15 = new RiZoneSortie();
    UA16 = new RiZoneSortie();
    UA17 = new RiZoneSortie();
    UA18 = new RiZoneSortie();
    lbSlash1 = new JLabel();
    lbSlash2 = new JLabel();
    lbSlash3 = new JLabel();
    lbSlash4 = new JLabel();
    lbSlash5 = new JLabel();
    lbSlash6 = new JLabel();
    lbSlash7 = new JLabel();
    lbSlash8 = new JLabel();
    lbSlash9 = new JLabel();
    lbSlash10 = new JLabel();
    lbSlash11 = new JLabel();
    lbSlash12 = new JLabel();
    lbSlash13 = new JLabel();
    lbSlash14 = new JLabel();
    lbSlash15 = new JLabel();
    lbSlash16 = new JLabel();
    lbSlash17 = new JLabel();
    lbSlash18 = new JLabel();
    UPRV1 = new RiZoneSortie();
    UPRV2 = new RiZoneSortie();
    UPRV3 = new RiZoneSortie();
    UPRV4 = new RiZoneSortie();
    UPRV5 = new RiZoneSortie();
    UPRV6 = new RiZoneSortie();
    UPRV7 = new RiZoneSortie();
    UPRV8 = new RiZoneSortie();
    UPRV9 = new RiZoneSortie();
    UPRV10 = new RiZoneSortie();
    UPRV11 = new RiZoneSortie();
    UPRV12 = new RiZoneSortie();
    UPRV13 = new RiZoneSortie();
    UPRV14 = new RiZoneSortie();
    UPRV15 = new RiZoneSortie();
    UPRV16 = new RiZoneSortie();
    UPRV17 = new RiZoneSortie();
    UPRV18 = new RiZoneSortie();
    lbSlash19 = new JLabel();
    lbSlash20 = new JLabel();
    lbSlash21 = new JLabel();
    lbSlash22 = new JLabel();
    lbSlash23 = new JLabel();
    lbSlash24 = new JLabel();
    lbSlash25 = new JLabel();
    lbSlash26 = new JLabel();
    lbSlash27 = new JLabel();
    lbSlash28 = new JLabel();
    lbSlash29 = new JLabel();
    lbSlash30 = new JLabel();
    lbSlash31 = new JLabel();
    lbSlash32 = new JLabel();
    lbSlash33 = new JLabel();
    lbSlash34 = new JLabel();
    lbSlash35 = new JLabel();
    lbSlash36 = new JLabel();
    BTD = new JPopupMenu();
    jmFicheArticle = new JMenuItem();
    jmEvolution = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Multiples conditions d'achat");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 40));
          p_tete_gauche.setMinimumSize(new Dimension(900, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- DEMETB ----
          DEMETB.setComponentPopupMenu(BTD);
          DEMETB.setName("DEMETB");

          //---- OBJ_67 ----
          OBJ_67.setText("Etablissement");
          OBJ_67.setName("OBJ_67");

          //---- LIBETB ----
          LIBETB.setText("@LIBETB@");
          LIBETB.setName("LIBETB");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(DEMETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LIBETB, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(DEMETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(LIBETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1000, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(1000, 600));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_34 ----
            OBJ_34.setText("Argument de recherche");
            OBJ_34.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_34.setName("OBJ_34");
            panel1.add(OBJ_34);
            OBJ_34.setBounds(20, 15, 180, 20);

            //---- ARTRCH ----
            ARTRCH.setComponentPopupMenu(BTD);
            ARTRCH.setText("@ARTRCH@");
            ARTRCH.setName("ARTRCH");
            panel1.add(ARTRCH);
            ARTRCH.setBounds(210, 10, 310, ARTRCH.getPreferredSize().height);

            //---- OBJ_38 ----
            OBJ_38.setText("Code famille");
            OBJ_38.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_38.setName("OBJ_38");
            panel1.add(OBJ_38);
            OBJ_38.setBounds(20, 47, 180, 20);

            //---- GFARCH ----
            GFARCH.setComponentPopupMenu(BTD);
            GFARCH.setText("@GFARCH@");
            GFARCH.setName("GFARCH");
            panel1.add(GFARCH);
            GFARCH.setBounds(210, 45, 44, GFARCH.getPreferredSize().height);

            //---- OBJ_41 ----
            OBJ_41.setText("Code sous-famille");
            OBJ_41.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_41.setName("OBJ_41");
            panel1.add(OBJ_41);
            OBJ_41.setBounds(270, 47, 180, 20);

            //---- SFARCH ----
            SFARCH.setComponentPopupMenu(BTD);
            SFARCH.setText("@SFARCH@");
            SFARCH.setName("SFARCH");
            panel1.add(SFARCH);
            SFARCH.setBounds(460, 45, 60, SFARCH.getPreferredSize().height);

            //---- OBJ_45 ----
            OBJ_45.setText("Code fournisseur");
            OBJ_45.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_45.setName("OBJ_45");
            panel1.add(OBJ_45);
            OBJ_45.setBounds(590, 47, 180, 20);

            //---- FRSRCH ----
            FRSRCH.setComponentPopupMenu(BTD);
            FRSRCH.setText("@FRSRCH@");
            FRSRCH.setHorizontalAlignment(SwingConstants.RIGHT);
            FRSRCH.setName("FRSRCH");
            panel1.add(FRSRCH);
            FRSRCH.setBounds(780, 45, 80, FRSRCH.getPreferredSize().height);

            //---- DATAPP ----
            DATAPP.setName("DATAPP");
            panel1.add(DATAPP);
            DATAPP.setBounds(780, 8, 105, DATAPP.getPreferredSize().height);

            //---- OBJ_46 ----
            OBJ_46.setText("Date d'application");
            OBJ_46.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_46.setName("OBJ_46");
            panel1.add(OBJ_46);
            OBJ_46.setBounds(590, 12, 180, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel1);
          panel1.setBounds(10, 10, 980, 86);

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder(""));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- LD101 ----
            LD101.setText("@LD101@");
            LD101.setComponentPopupMenu(BTD);
            LD101.setName("LD101");
            panel2.add(LD101);
            LD101.setBounds(10, 25, 620, LD101.getPreferredSize().height);

            //---- LD102 ----
            LD102.setText("@LD102@");
            LD102.setComponentPopupMenu(BTD);
            LD102.setName("LD102");
            panel2.add(LD102);
            LD102.setBounds(10, 50, 620, LD102.getPreferredSize().height);

            //---- LD103 ----
            LD103.setText("@LD103@");
            LD103.setComponentPopupMenu(BTD);
            LD103.setName("LD103");
            panel2.add(LD103);
            LD103.setBounds(10, 75, 620, LD103.getPreferredSize().height);

            //---- LD104 ----
            LD104.setText("@LD104@");
            LD104.setComponentPopupMenu(BTD);
            LD104.setName("LD104");
            panel2.add(LD104);
            LD104.setBounds(10, 100, 620, LD104.getPreferredSize().height);

            //---- LD105 ----
            LD105.setText("@LD105@");
            LD105.setComponentPopupMenu(BTD);
            LD105.setName("LD105");
            panel2.add(LD105);
            LD105.setBounds(10, 125, 620, LD105.getPreferredSize().height);

            //---- LD106 ----
            LD106.setText("@LD106@");
            LD106.setComponentPopupMenu(BTD);
            LD106.setName("LD106");
            panel2.add(LD106);
            LD106.setBounds(10, 150, 620, LD106.getPreferredSize().height);

            //---- LD107 ----
            LD107.setText("@LD107@");
            LD107.setComponentPopupMenu(BTD);
            LD107.setName("LD107");
            panel2.add(LD107);
            LD107.setBounds(10, 175, 620, LD107.getPreferredSize().height);

            //---- LD108 ----
            LD108.setText("@LD108@");
            LD108.setComponentPopupMenu(BTD);
            LD108.setName("LD108");
            panel2.add(LD108);
            LD108.setBounds(10, 200, 620, LD108.getPreferredSize().height);

            //---- LD109 ----
            LD109.setText("@LD109@");
            LD109.setComponentPopupMenu(BTD);
            LD109.setName("LD109");
            panel2.add(LD109);
            LD109.setBounds(10, 225, 620, LD109.getPreferredSize().height);

            //---- LD110 ----
            LD110.setText("@LD110@");
            LD110.setComponentPopupMenu(BTD);
            LD110.setName("LD110");
            panel2.add(LD110);
            LD110.setBounds(10, 250, 620, LD110.getPreferredSize().height);

            //---- LD111 ----
            LD111.setText("@LD111@");
            LD111.setComponentPopupMenu(BTD);
            LD111.setName("LD111");
            panel2.add(LD111);
            LD111.setBounds(10, 275, 620, LD111.getPreferredSize().height);

            //---- LD112 ----
            LD112.setText("@LD112@");
            LD112.setComponentPopupMenu(BTD);
            LD112.setName("LD112");
            panel2.add(LD112);
            LD112.setBounds(10, 300, 620, LD112.getPreferredSize().height);

            //---- LD113 ----
            LD113.setText("@LD113@");
            LD113.setComponentPopupMenu(BTD);
            LD113.setName("LD113");
            panel2.add(LD113);
            LD113.setBounds(10, 325, 620, LD113.getPreferredSize().height);

            //---- LD114 ----
            LD114.setText("@LD114@");
            LD114.setComponentPopupMenu(BTD);
            LD114.setName("LD114");
            panel2.add(LD114);
            LD114.setBounds(10, 350, 620, LD114.getPreferredSize().height);

            //---- LD115 ----
            LD115.setText("@LD115@");
            LD115.setComponentPopupMenu(BTD);
            LD115.setName("LD115");
            panel2.add(LD115);
            LD115.setBounds(10, 375, 620, LD115.getPreferredSize().height);

            //---- LD116 ----
            LD116.setText("@LD116@");
            LD116.setComponentPopupMenu(BTD);
            LD116.setName("LD116");
            panel2.add(LD116);
            LD116.setBounds(10, 400, 620, LD116.getPreferredSize().height);

            //---- LD117 ----
            LD117.setText("@LD117@");
            LD117.setComponentPopupMenu(BTD);
            LD117.setName("LD117");
            panel2.add(LD117);
            LD117.setBounds(10, 425, 620, LD117.getPreferredSize().height);

            //---- LD118 ----
            LD118.setText("@LD118@");
            LD118.setComponentPopupMenu(BTD);
            LD118.setName("LD118");
            panel2.add(LD118);
            LD118.setBounds(10, 450, 620, LD118.getPreferredSize().height);

            //---- PRA01 ----
            PRA01.setComponentPopupMenu(BTD);
            PRA01.setNextFocusableComponent(PRC01);
            PRA01.setName("PRA01");
            PRA01.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRA01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRA01);
            PRA01.setBounds(630, 23, 84, PRA01.getPreferredSize().height);

            //---- PRA02 ----
            PRA02.setComponentPopupMenu(BTD);
            PRA02.setNextFocusableComponent(PRC02);
            PRA02.setName("PRA02");
            PRA02.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRA02.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRA02);
            PRA02.setBounds(630, 48, 84, PRA02.getPreferredSize().height);

            //---- PRA03 ----
            PRA03.setComponentPopupMenu(BTD);
            PRA03.setName("PRA03");
            PRA03.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRA03.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRA03);
            PRA03.setBounds(630, 73, 84, PRA03.getPreferredSize().height);

            //---- PRA04 ----
            PRA04.setComponentPopupMenu(BTD);
            PRA04.setName("PRA04");
            PRA04.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRA04.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRA04);
            PRA04.setBounds(630, 98, 84, PRA04.getPreferredSize().height);

            //---- PRA05 ----
            PRA05.setComponentPopupMenu(BTD);
            PRA05.setName("PRA05");
            PRA05.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRA05.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRA05);
            PRA05.setBounds(630, 123, 84, PRA05.getPreferredSize().height);

            //---- PRA06 ----
            PRA06.setComponentPopupMenu(BTD);
            PRA06.setName("PRA06");
            PRA06.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRA06.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRA06);
            PRA06.setBounds(630, 148, 84, PRA06.getPreferredSize().height);

            //---- PRA07 ----
            PRA07.setComponentPopupMenu(BTD);
            PRA07.setName("PRA07");
            PRA07.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRA07.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRA07);
            PRA07.setBounds(630, 173, 84, PRA07.getPreferredSize().height);

            //---- PRA08 ----
            PRA08.setComponentPopupMenu(BTD);
            PRA08.setName("PRA08");
            PRA08.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRA08.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRA08);
            PRA08.setBounds(630, 198, 84, PRA08.getPreferredSize().height);

            //---- PRA09 ----
            PRA09.setComponentPopupMenu(BTD);
            PRA09.setName("PRA09");
            PRA09.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRA09.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRA09);
            PRA09.setBounds(630, 223, 84, PRA09.getPreferredSize().height);

            //---- PRA10 ----
            PRA10.setComponentPopupMenu(BTD);
            PRA10.setName("PRA10");
            PRA10.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRA10.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRA10);
            PRA10.setBounds(630, 248, 84, PRA10.getPreferredSize().height);

            //---- PRA11 ----
            PRA11.setComponentPopupMenu(BTD);
            PRA11.setName("PRA11");
            PRA11.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRA11.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRA11);
            PRA11.setBounds(630, 273, 84, PRA11.getPreferredSize().height);

            //---- PRA12 ----
            PRA12.setComponentPopupMenu(BTD);
            PRA12.setName("PRA12");
            PRA12.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRA12.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRA12);
            PRA12.setBounds(630, 298, 84, PRA12.getPreferredSize().height);

            //---- PRA13 ----
            PRA13.setComponentPopupMenu(BTD);
            PRA13.setName("PRA13");
            PRA13.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRA13.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRA13);
            PRA13.setBounds(630, 323, 84, PRA13.getPreferredSize().height);

            //---- PRA14 ----
            PRA14.setComponentPopupMenu(BTD);
            PRA14.setName("PRA14");
            PRA14.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRA14.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRA14);
            PRA14.setBounds(630, 348, 84, PRA14.getPreferredSize().height);

            //---- PRA15 ----
            PRA15.setComponentPopupMenu(BTD);
            PRA15.setName("PRA15");
            PRA15.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRA15.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRA15);
            PRA15.setBounds(630, 373, 84, PRA15.getPreferredSize().height);

            //---- PRA16 ----
            PRA16.setComponentPopupMenu(BTD);
            PRA16.setName("PRA16");
            PRA16.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRA16.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRA16);
            PRA16.setBounds(630, 398, 84, PRA16.getPreferredSize().height);

            //---- PRA17 ----
            PRA17.setComponentPopupMenu(BTD);
            PRA17.setName("PRA17");
            PRA17.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRA17.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRA17);
            PRA17.setBounds(630, 423, 84, PRA17.getPreferredSize().height);

            //---- PRA18 ----
            PRA18.setComponentPopupMenu(BTD);
            PRA18.setName("PRA18");
            PRA18.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRA18.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRA18);
            PRA18.setBounds(630, 448, 84, PRA18.getPreferredSize().height);

            //---- LD201 ----
            LD201.setText("@LD201@");
            LD201.setComponentPopupMenu(BTD);
            LD201.setHorizontalAlignment(SwingConstants.RIGHT);
            LD201.setName("LD201");
            panel2.add(LD201);
            LD201.setBounds(760, 25, 80, LD201.getPreferredSize().height);

            //---- LD202 ----
            LD202.setText("@LD202@");
            LD202.setComponentPopupMenu(BTD);
            LD202.setHorizontalAlignment(SwingConstants.RIGHT);
            LD202.setName("LD202");
            panel2.add(LD202);
            LD202.setBounds(760, 50, 80, LD202.getPreferredSize().height);

            //---- LD203 ----
            LD203.setText("@LD203@");
            LD203.setComponentPopupMenu(BTD);
            LD203.setHorizontalAlignment(SwingConstants.RIGHT);
            LD203.setName("LD203");
            panel2.add(LD203);
            LD203.setBounds(760, 75, 80, LD203.getPreferredSize().height);

            //---- LD204 ----
            LD204.setText("@LD204@");
            LD204.setComponentPopupMenu(BTD);
            LD204.setHorizontalAlignment(SwingConstants.RIGHT);
            LD204.setName("LD204");
            panel2.add(LD204);
            LD204.setBounds(760, 100, 80, LD204.getPreferredSize().height);

            //---- LD205 ----
            LD205.setText("@LD205@");
            LD205.setComponentPopupMenu(BTD);
            LD205.setHorizontalAlignment(SwingConstants.RIGHT);
            LD205.setName("LD205");
            panel2.add(LD205);
            LD205.setBounds(760, 125, 80, LD205.getPreferredSize().height);

            //---- LD206 ----
            LD206.setText("@LD206@");
            LD206.setComponentPopupMenu(BTD);
            LD206.setHorizontalAlignment(SwingConstants.RIGHT);
            LD206.setName("LD206");
            panel2.add(LD206);
            LD206.setBounds(760, 150, 80, LD206.getPreferredSize().height);

            //---- LD207 ----
            LD207.setText("@LD207@");
            LD207.setComponentPopupMenu(BTD);
            LD207.setHorizontalAlignment(SwingConstants.RIGHT);
            LD207.setName("LD207");
            panel2.add(LD207);
            LD207.setBounds(760, 175, 80, LD207.getPreferredSize().height);

            //---- LD208 ----
            LD208.setText("@LD208@");
            LD208.setComponentPopupMenu(BTD);
            LD208.setHorizontalAlignment(SwingConstants.RIGHT);
            LD208.setName("LD208");
            panel2.add(LD208);
            LD208.setBounds(760, 200, 80, LD208.getPreferredSize().height);

            //---- LD209 ----
            LD209.setText("@LD209@");
            LD209.setComponentPopupMenu(BTD);
            LD209.setHorizontalAlignment(SwingConstants.RIGHT);
            LD209.setName("LD209");
            panel2.add(LD209);
            LD209.setBounds(760, 225, 80, LD209.getPreferredSize().height);

            //---- LD210 ----
            LD210.setText("@LD210@");
            LD210.setComponentPopupMenu(BTD);
            LD210.setHorizontalAlignment(SwingConstants.RIGHT);
            LD210.setName("LD210");
            panel2.add(LD210);
            LD210.setBounds(760, 250, 80, LD210.getPreferredSize().height);

            //---- LD211 ----
            LD211.setText("@LD211@");
            LD211.setComponentPopupMenu(BTD);
            LD211.setHorizontalAlignment(SwingConstants.RIGHT);
            LD211.setName("LD211");
            panel2.add(LD211);
            LD211.setBounds(760, 275, 80, LD211.getPreferredSize().height);

            //---- LD212 ----
            LD212.setText("@LD212@");
            LD212.setComponentPopupMenu(BTD);
            LD212.setHorizontalAlignment(SwingConstants.RIGHT);
            LD212.setName("LD212");
            panel2.add(LD212);
            LD212.setBounds(760, 300, 80, LD212.getPreferredSize().height);

            //---- LD213 ----
            LD213.setText("@LD213@");
            LD213.setComponentPopupMenu(BTD);
            LD213.setHorizontalAlignment(SwingConstants.RIGHT);
            LD213.setName("LD213");
            panel2.add(LD213);
            LD213.setBounds(760, 325, 80, LD213.getPreferredSize().height);

            //---- LD214 ----
            LD214.setText("@LD214@");
            LD214.setComponentPopupMenu(BTD);
            LD214.setHorizontalAlignment(SwingConstants.RIGHT);
            LD214.setName("LD214");
            panel2.add(LD214);
            LD214.setBounds(760, 350, 80, LD214.getPreferredSize().height);

            //---- LD215 ----
            LD215.setText("@LD215@");
            LD215.setComponentPopupMenu(BTD);
            LD215.setHorizontalAlignment(SwingConstants.RIGHT);
            LD215.setName("LD215");
            panel2.add(LD215);
            LD215.setBounds(760, 375, 80, LD215.getPreferredSize().height);

            //---- LD216 ----
            LD216.setText("@LD216@");
            LD216.setComponentPopupMenu(BTD);
            LD216.setHorizontalAlignment(SwingConstants.RIGHT);
            LD216.setName("LD216");
            panel2.add(LD216);
            LD216.setBounds(760, 400, 80, LD216.getPreferredSize().height);

            //---- LD217 ----
            LD217.setText("@LD217@");
            LD217.setComponentPopupMenu(BTD);
            LD217.setHorizontalAlignment(SwingConstants.RIGHT);
            LD217.setName("LD217");
            panel2.add(LD217);
            LD217.setBounds(760, 425, 80, LD217.getPreferredSize().height);

            //---- LD218 ----
            LD218.setText("@LD218@");
            LD218.setComponentPopupMenu(BTD);
            LD218.setHorizontalAlignment(SwingConstants.RIGHT);
            LD218.setName("LD218");
            panel2.add(LD218);
            LD218.setBounds(760, 450, 80, LD218.getPreferredSize().height);

            //---- PRC01 ----
            PRC01.setComponentPopupMenu(BTD);
            PRC01.setNextFocusableComponent(PRA02);
            PRC01.setName("PRC01");
            PRC01.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRC01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRC01);
            PRC01.setBounds(890, 23, 60, PRC01.getPreferredSize().height);

            //---- PRC02 ----
            PRC02.setComponentPopupMenu(BTD);
            PRC02.setNextFocusableComponent(PRA03);
            PRC02.setName("PRC02");
            PRC02.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRC02.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRC02);
            PRC02.setBounds(890, 48, 60, PRC02.getPreferredSize().height);

            //---- PRC03 ----
            PRC03.setComponentPopupMenu(BTD);
            PRC03.setName("PRC03");
            PRC03.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRC03.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRC03);
            PRC03.setBounds(890, 73, 60, PRC03.getPreferredSize().height);

            //---- PRC04 ----
            PRC04.setComponentPopupMenu(BTD);
            PRC04.setName("PRC04");
            PRC04.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRC04.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRC04);
            PRC04.setBounds(890, 98, 60, PRC04.getPreferredSize().height);

            //---- PRC05 ----
            PRC05.setComponentPopupMenu(BTD);
            PRC05.setName("PRC05");
            PRC05.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRC05.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRC05);
            PRC05.setBounds(890, 123, 60, PRC05.getPreferredSize().height);

            //---- PRC06 ----
            PRC06.setComponentPopupMenu(BTD);
            PRC06.setName("PRC06");
            PRC06.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRC06.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRC06);
            PRC06.setBounds(890, 148, 60, PRC06.getPreferredSize().height);

            //---- PRC07 ----
            PRC07.setComponentPopupMenu(BTD);
            PRC07.setName("PRC07");
            PRC07.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRC07.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRC07);
            PRC07.setBounds(890, 173, 60, PRC07.getPreferredSize().height);

            //---- PRC08 ----
            PRC08.setComponentPopupMenu(BTD);
            PRC08.setName("PRC08");
            PRC08.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRC08.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRC08);
            PRC08.setBounds(890, 198, 60, PRC08.getPreferredSize().height);

            //---- PRC09 ----
            PRC09.setComponentPopupMenu(BTD);
            PRC09.setName("PRC09");
            PRC09.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRC09.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRC09);
            PRC09.setBounds(890, 223, 60, PRC09.getPreferredSize().height);

            //---- PRC10 ----
            PRC10.setComponentPopupMenu(BTD);
            PRC10.setName("PRC10");
            PRC10.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRC10.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRC10);
            PRC10.setBounds(890, 248, 60, PRC10.getPreferredSize().height);

            //---- PRC11 ----
            PRC11.setComponentPopupMenu(BTD);
            PRC11.setName("PRC11");
            PRC11.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRC11.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRC11);
            PRC11.setBounds(890, 273, 60, PRC11.getPreferredSize().height);

            //---- PRC12 ----
            PRC12.setComponentPopupMenu(BTD);
            PRC12.setName("PRC12");
            PRC12.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRC12.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRC12);
            PRC12.setBounds(890, 298, 60, PRC12.getPreferredSize().height);

            //---- PRC13 ----
            PRC13.setComponentPopupMenu(BTD);
            PRC13.setName("PRC13");
            PRC13.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRC13.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRC13);
            PRC13.setBounds(890, 323, 60, PRC13.getPreferredSize().height);

            //---- PRC14 ----
            PRC14.setComponentPopupMenu(BTD);
            PRC14.setName("PRC14");
            PRC14.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRC14.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRC14);
            PRC14.setBounds(890, 348, 60, PRC14.getPreferredSize().height);

            //---- PRC15 ----
            PRC15.setComponentPopupMenu(BTD);
            PRC15.setName("PRC15");
            PRC15.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRC15.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRC15);
            PRC15.setBounds(890, 373, 60, PRC15.getPreferredSize().height);

            //---- PRC16 ----
            PRC16.setComponentPopupMenu(BTD);
            PRC16.setName("PRC16");
            PRC16.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRC16.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRC16);
            PRC16.setBounds(890, 398, 60, PRC16.getPreferredSize().height);

            //---- PRC17 ----
            PRC17.setComponentPopupMenu(BTD);
            PRC17.setName("PRC17");
            PRC17.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRC17.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRC17);
            PRC17.setBounds(890, 423, 60, PRC17.getPreferredSize().height);

            //---- PRC18 ----
            PRC18.setComponentPopupMenu(BTD);
            PRC18.setName("PRC18");
            PRC18.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                ZonesKeyReleased(e);
              }
            });
            PRC18.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseReleased(MouseEvent e) {
                champsMouseReleased(e);
              }
            });
            panel2.add(PRC18);
            PRC18.setBounds(890, 448, 60, PRC18.getPreferredSize().height);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel2.add(BT_PGUP);
            BT_PGUP.setBounds(950, 23, 25, 207);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel2.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(950, 266, 25, 210);

            //---- label1 ----
            label1.setText("Code article");
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
            label1.setName("label1");
            panel2.add(label1);
            label1.setBounds(10, 5, 90, 20);

            //---- label2 ----
            label2.setText("Libell\u00e9 article");
            label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
            label2.setName("label2");
            panel2.add(label2);
            label2.setBounds(185, 5, 90, 20);

            //---- label3 ----
            label3.setText("Prix d'achat");
            label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
            label3.setName("label3");
            panel2.add(label3);
            label3.setBounds(630, 5, 84, 20);

            //---- label4 ----
            label4.setText("Prix de revient");
            label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
            label4.setName("label4");
            panel2.add(label4);
            label4.setBounds(760, 5, 84, 20);

            //---- label5 ----
            label5.setText("Variation");
            label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
            label5.setName("label5");
            panel2.add(label5);
            label5.setBounds(890, 5, 60, 20);

            //---- UA1 ----
            UA1.setName("UA1");
            panel2.add(UA1);
            UA1.setBounds(720, 25, 34, 24);

            //---- UA2 ----
            UA2.setName("UA2");
            panel2.add(UA2);
            UA2.setBounds(720, 50, 34, UA2.getPreferredSize().height);

            //---- UA3 ----
            UA3.setName("UA3");
            panel2.add(UA3);
            UA3.setBounds(720, 75, 34, UA3.getPreferredSize().height);

            //---- UA4 ----
            UA4.setName("UA4");
            panel2.add(UA4);
            UA4.setBounds(720, 100, 34, UA4.getPreferredSize().height);

            //---- UA5 ----
            UA5.setName("UA5");
            panel2.add(UA5);
            UA5.setBounds(720, 125, 34, UA5.getPreferredSize().height);

            //---- UA6 ----
            UA6.setName("UA6");
            panel2.add(UA6);
            UA6.setBounds(720, 150, 34, UA6.getPreferredSize().height);

            //---- UA7 ----
            UA7.setName("UA7");
            panel2.add(UA7);
            UA7.setBounds(720, 175, 34, UA7.getPreferredSize().height);

            //---- UA8 ----
            UA8.setName("UA8");
            panel2.add(UA8);
            UA8.setBounds(720, 200, 34, UA8.getPreferredSize().height);

            //---- UA9 ----
            UA9.setName("UA9");
            panel2.add(UA9);
            UA9.setBounds(720, 225, 34, UA9.getPreferredSize().height);

            //---- UA10 ----
            UA10.setName("UA10");
            panel2.add(UA10);
            UA10.setBounds(720, 250, 34, UA10.getPreferredSize().height);

            //---- UA11 ----
            UA11.setName("UA11");
            panel2.add(UA11);
            UA11.setBounds(720, 275, 34, UA11.getPreferredSize().height);

            //---- UA12 ----
            UA12.setName("UA12");
            panel2.add(UA12);
            UA12.setBounds(720, 300, 34, UA12.getPreferredSize().height);

            //---- UA13 ----
            UA13.setName("UA13");
            panel2.add(UA13);
            UA13.setBounds(720, 325, 34, UA13.getPreferredSize().height);

            //---- UA14 ----
            UA14.setName("UA14");
            panel2.add(UA14);
            UA14.setBounds(720, 350, 34, UA14.getPreferredSize().height);

            //---- UA15 ----
            UA15.setName("UA15");
            panel2.add(UA15);
            UA15.setBounds(720, 375, 34, UA15.getPreferredSize().height);

            //---- UA16 ----
            UA16.setName("UA16");
            panel2.add(UA16);
            UA16.setBounds(720, 400, 34, UA16.getPreferredSize().height);

            //---- UA17 ----
            UA17.setName("UA17");
            panel2.add(UA17);
            UA17.setBounds(720, 425, 34, UA17.getPreferredSize().height);

            //---- UA18 ----
            UA18.setName("UA18");
            panel2.add(UA18);
            UA18.setBounds(720, 450, 34, UA18.getPreferredSize().height);

            //---- lbSlash1 ----
            lbSlash1.setText("/");
            lbSlash1.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash1.setFont(lbSlash1.getFont().deriveFont(lbSlash1.getFont().getSize() + 2f));
            lbSlash1.setName("lbSlash1");
            panel2.add(lbSlash1);
            lbSlash1.setBounds(710, 23, 15, 28);

            //---- lbSlash2 ----
            lbSlash2.setText("/");
            lbSlash2.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash2.setFont(lbSlash2.getFont().deriveFont(lbSlash2.getFont().getSize() + 2f));
            lbSlash2.setName("lbSlash2");
            panel2.add(lbSlash2);
            lbSlash2.setBounds(710, 48, 15, 28);

            //---- lbSlash3 ----
            lbSlash3.setText("/");
            lbSlash3.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash3.setFont(lbSlash3.getFont().deriveFont(lbSlash3.getFont().getSize() + 2f));
            lbSlash3.setName("lbSlash3");
            panel2.add(lbSlash3);
            lbSlash3.setBounds(710, 73, 15, 28);

            //---- lbSlash4 ----
            lbSlash4.setText("/");
            lbSlash4.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash4.setFont(lbSlash4.getFont().deriveFont(lbSlash4.getFont().getSize() + 2f));
            lbSlash4.setName("lbSlash4");
            panel2.add(lbSlash4);
            lbSlash4.setBounds(710, 98, 15, 28);

            //---- lbSlash5 ----
            lbSlash5.setText("/");
            lbSlash5.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash5.setFont(lbSlash5.getFont().deriveFont(lbSlash5.getFont().getSize() + 2f));
            lbSlash5.setName("lbSlash5");
            panel2.add(lbSlash5);
            lbSlash5.setBounds(710, 123, 15, 28);

            //---- lbSlash6 ----
            lbSlash6.setText("/");
            lbSlash6.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash6.setFont(lbSlash6.getFont().deriveFont(lbSlash6.getFont().getSize() + 2f));
            lbSlash6.setName("lbSlash6");
            panel2.add(lbSlash6);
            lbSlash6.setBounds(710, 148, 15, 28);

            //---- lbSlash7 ----
            lbSlash7.setText("/");
            lbSlash7.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash7.setFont(lbSlash7.getFont().deriveFont(lbSlash7.getFont().getSize() + 2f));
            lbSlash7.setName("lbSlash7");
            panel2.add(lbSlash7);
            lbSlash7.setBounds(710, 173, 15, 28);

            //---- lbSlash8 ----
            lbSlash8.setText("/");
            lbSlash8.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash8.setFont(lbSlash8.getFont().deriveFont(lbSlash8.getFont().getSize() + 2f));
            lbSlash8.setName("lbSlash8");
            panel2.add(lbSlash8);
            lbSlash8.setBounds(710, 198, 15, 28);

            //---- lbSlash9 ----
            lbSlash9.setText("/");
            lbSlash9.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash9.setFont(lbSlash9.getFont().deriveFont(lbSlash9.getFont().getSize() + 2f));
            lbSlash9.setName("lbSlash9");
            panel2.add(lbSlash9);
            lbSlash9.setBounds(710, 223, 15, 28);

            //---- lbSlash10 ----
            lbSlash10.setText("/");
            lbSlash10.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash10.setFont(lbSlash10.getFont().deriveFont(lbSlash10.getFont().getSize() + 2f));
            lbSlash10.setName("lbSlash10");
            panel2.add(lbSlash10);
            lbSlash10.setBounds(710, 248, 15, 28);

            //---- lbSlash11 ----
            lbSlash11.setText("/");
            lbSlash11.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash11.setFont(lbSlash11.getFont().deriveFont(lbSlash11.getFont().getSize() + 2f));
            lbSlash11.setName("lbSlash11");
            panel2.add(lbSlash11);
            lbSlash11.setBounds(710, 273, 15, 28);

            //---- lbSlash12 ----
            lbSlash12.setText("/");
            lbSlash12.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash12.setFont(lbSlash12.getFont().deriveFont(lbSlash12.getFont().getSize() + 2f));
            lbSlash12.setName("lbSlash12");
            panel2.add(lbSlash12);
            lbSlash12.setBounds(710, 298, 15, 28);

            //---- lbSlash13 ----
            lbSlash13.setText("/");
            lbSlash13.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash13.setFont(lbSlash13.getFont().deriveFont(lbSlash13.getFont().getSize() + 2f));
            lbSlash13.setName("lbSlash13");
            panel2.add(lbSlash13);
            lbSlash13.setBounds(710, 323, 15, 28);

            //---- lbSlash14 ----
            lbSlash14.setText("/");
            lbSlash14.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash14.setFont(lbSlash14.getFont().deriveFont(lbSlash14.getFont().getSize() + 2f));
            lbSlash14.setName("lbSlash14");
            panel2.add(lbSlash14);
            lbSlash14.setBounds(710, 348, 15, 28);

            //---- lbSlash15 ----
            lbSlash15.setText("/");
            lbSlash15.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash15.setFont(lbSlash15.getFont().deriveFont(lbSlash15.getFont().getSize() + 2f));
            lbSlash15.setName("lbSlash15");
            panel2.add(lbSlash15);
            lbSlash15.setBounds(710, 373, 15, 28);

            //---- lbSlash16 ----
            lbSlash16.setText("/");
            lbSlash16.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash16.setFont(lbSlash16.getFont().deriveFont(lbSlash16.getFont().getSize() + 2f));
            lbSlash16.setName("lbSlash16");
            panel2.add(lbSlash16);
            lbSlash16.setBounds(710, 398, 15, 28);

            //---- lbSlash17 ----
            lbSlash17.setText("/");
            lbSlash17.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash17.setFont(lbSlash17.getFont().deriveFont(lbSlash17.getFont().getSize() + 2f));
            lbSlash17.setName("lbSlash17");
            panel2.add(lbSlash17);
            lbSlash17.setBounds(710, 423, 15, 28);

            //---- lbSlash18 ----
            lbSlash18.setText("/");
            lbSlash18.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash18.setFont(lbSlash18.getFont().deriveFont(lbSlash18.getFont().getSize() + 2f));
            lbSlash18.setName("lbSlash18");
            panel2.add(lbSlash18);
            lbSlash18.setBounds(710, 448, 15, 28);

            //---- UPRV1 ----
            UPRV1.setName("UPRV1");
            panel2.add(UPRV1);
            UPRV1.setBounds(850, 25, 34, UPRV1.getPreferredSize().height);

            //---- UPRV2 ----
            UPRV2.setName("UPRV2");
            panel2.add(UPRV2);
            UPRV2.setBounds(850, 50, 34, UPRV2.getPreferredSize().height);

            //---- UPRV3 ----
            UPRV3.setName("UPRV3");
            panel2.add(UPRV3);
            UPRV3.setBounds(850, 75, 34, UPRV3.getPreferredSize().height);

            //---- UPRV4 ----
            UPRV4.setName("UPRV4");
            panel2.add(UPRV4);
            UPRV4.setBounds(850, 100, 34, UPRV4.getPreferredSize().height);

            //---- UPRV5 ----
            UPRV5.setName("UPRV5");
            panel2.add(UPRV5);
            UPRV5.setBounds(850, 125, 34, UPRV5.getPreferredSize().height);

            //---- UPRV6 ----
            UPRV6.setName("UPRV6");
            panel2.add(UPRV6);
            UPRV6.setBounds(850, 150, 34, UPRV6.getPreferredSize().height);

            //---- UPRV7 ----
            UPRV7.setName("UPRV7");
            panel2.add(UPRV7);
            UPRV7.setBounds(850, 175, 34, UPRV7.getPreferredSize().height);

            //---- UPRV8 ----
            UPRV8.setName("UPRV8");
            panel2.add(UPRV8);
            UPRV8.setBounds(850, 200, 34, UPRV8.getPreferredSize().height);

            //---- UPRV9 ----
            UPRV9.setName("UPRV9");
            panel2.add(UPRV9);
            UPRV9.setBounds(850, 225, 34, UPRV9.getPreferredSize().height);

            //---- UPRV10 ----
            UPRV10.setName("UPRV10");
            panel2.add(UPRV10);
            UPRV10.setBounds(850, 250, 34, UPRV10.getPreferredSize().height);

            //---- UPRV11 ----
            UPRV11.setName("UPRV11");
            panel2.add(UPRV11);
            UPRV11.setBounds(850, 275, 34, UPRV11.getPreferredSize().height);

            //---- UPRV12 ----
            UPRV12.setName("UPRV12");
            panel2.add(UPRV12);
            UPRV12.setBounds(850, 300, 34, UPRV12.getPreferredSize().height);

            //---- UPRV13 ----
            UPRV13.setName("UPRV13");
            panel2.add(UPRV13);
            UPRV13.setBounds(850, 325, 34, UPRV13.getPreferredSize().height);

            //---- UPRV14 ----
            UPRV14.setName("UPRV14");
            panel2.add(UPRV14);
            UPRV14.setBounds(850, 350, 34, UPRV14.getPreferredSize().height);

            //---- UPRV15 ----
            UPRV15.setName("UPRV15");
            panel2.add(UPRV15);
            UPRV15.setBounds(850, 375, 34, UPRV15.getPreferredSize().height);

            //---- UPRV16 ----
            UPRV16.setName("UPRV16");
            panel2.add(UPRV16);
            UPRV16.setBounds(850, 400, 34, UPRV16.getPreferredSize().height);

            //---- UPRV17 ----
            UPRV17.setName("UPRV17");
            panel2.add(UPRV17);
            UPRV17.setBounds(850, 425, 34, UPRV17.getPreferredSize().height);

            //---- UPRV18 ----
            UPRV18.setName("UPRV18");
            panel2.add(UPRV18);
            UPRV18.setBounds(850, 450, 34, UPRV18.getPreferredSize().height);

            //---- lbSlash19 ----
            lbSlash19.setText("/");
            lbSlash19.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash19.setFont(lbSlash19.getFont().deriveFont(lbSlash19.getFont().getSize() + 2f));
            lbSlash19.setName("lbSlash19");
            panel2.add(lbSlash19);
            lbSlash19.setBounds(840, 23, 10, 28);

            //---- lbSlash20 ----
            lbSlash20.setText("/");
            lbSlash20.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash20.setFont(lbSlash20.getFont().deriveFont(lbSlash20.getFont().getSize() + 2f));
            lbSlash20.setName("lbSlash20");
            panel2.add(lbSlash20);
            lbSlash20.setBounds(840, 48, 10, 28);

            //---- lbSlash21 ----
            lbSlash21.setText("/");
            lbSlash21.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash21.setFont(lbSlash21.getFont().deriveFont(lbSlash21.getFont().getSize() + 2f));
            lbSlash21.setName("lbSlash21");
            panel2.add(lbSlash21);
            lbSlash21.setBounds(840, 73, 10, 28);

            //---- lbSlash22 ----
            lbSlash22.setText("/");
            lbSlash22.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash22.setFont(lbSlash22.getFont().deriveFont(lbSlash22.getFont().getSize() + 2f));
            lbSlash22.setName("lbSlash22");
            panel2.add(lbSlash22);
            lbSlash22.setBounds(840, 98, 10, 28);

            //---- lbSlash23 ----
            lbSlash23.setText("/");
            lbSlash23.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash23.setFont(lbSlash23.getFont().deriveFont(lbSlash23.getFont().getSize() + 2f));
            lbSlash23.setName("lbSlash23");
            panel2.add(lbSlash23);
            lbSlash23.setBounds(840, 123, 10, 28);

            //---- lbSlash24 ----
            lbSlash24.setText("/");
            lbSlash24.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash24.setFont(lbSlash24.getFont().deriveFont(lbSlash24.getFont().getSize() + 2f));
            lbSlash24.setName("lbSlash24");
            panel2.add(lbSlash24);
            lbSlash24.setBounds(840, 148, 10, 28);

            //---- lbSlash25 ----
            lbSlash25.setText("/");
            lbSlash25.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash25.setFont(lbSlash25.getFont().deriveFont(lbSlash25.getFont().getSize() + 2f));
            lbSlash25.setName("lbSlash25");
            panel2.add(lbSlash25);
            lbSlash25.setBounds(840, 173, 10, 28);

            //---- lbSlash26 ----
            lbSlash26.setText("/");
            lbSlash26.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash26.setFont(lbSlash26.getFont().deriveFont(lbSlash26.getFont().getSize() + 2f));
            lbSlash26.setName("lbSlash26");
            panel2.add(lbSlash26);
            lbSlash26.setBounds(840, 198, 10, 28);

            //---- lbSlash27 ----
            lbSlash27.setText("/");
            lbSlash27.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash27.setFont(lbSlash27.getFont().deriveFont(lbSlash27.getFont().getSize() + 2f));
            lbSlash27.setName("lbSlash27");
            panel2.add(lbSlash27);
            lbSlash27.setBounds(840, 223, 10, 28);

            //---- lbSlash28 ----
            lbSlash28.setText("/");
            lbSlash28.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash28.setFont(lbSlash28.getFont().deriveFont(lbSlash28.getFont().getSize() + 2f));
            lbSlash28.setName("lbSlash28");
            panel2.add(lbSlash28);
            lbSlash28.setBounds(840, 248, 10, 28);

            //---- lbSlash29 ----
            lbSlash29.setText("/");
            lbSlash29.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash29.setFont(lbSlash29.getFont().deriveFont(lbSlash29.getFont().getSize() + 2f));
            lbSlash29.setName("lbSlash29");
            panel2.add(lbSlash29);
            lbSlash29.setBounds(840, 273, 10, 28);

            //---- lbSlash30 ----
            lbSlash30.setText("/");
            lbSlash30.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash30.setFont(lbSlash30.getFont().deriveFont(lbSlash30.getFont().getSize() + 2f));
            lbSlash30.setName("lbSlash30");
            panel2.add(lbSlash30);
            lbSlash30.setBounds(840, 298, 10, 28);

            //---- lbSlash31 ----
            lbSlash31.setText("/");
            lbSlash31.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash31.setFont(lbSlash31.getFont().deriveFont(lbSlash31.getFont().getSize() + 2f));
            lbSlash31.setName("lbSlash31");
            panel2.add(lbSlash31);
            lbSlash31.setBounds(840, 323, 10, 28);

            //---- lbSlash32 ----
            lbSlash32.setText("/");
            lbSlash32.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash32.setFont(lbSlash32.getFont().deriveFont(lbSlash32.getFont().getSize() + 2f));
            lbSlash32.setName("lbSlash32");
            panel2.add(lbSlash32);
            lbSlash32.setBounds(840, 348, 10, 28);

            //---- lbSlash33 ----
            lbSlash33.setText("/");
            lbSlash33.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash33.setFont(lbSlash33.getFont().deriveFont(lbSlash33.getFont().getSize() + 2f));
            lbSlash33.setName("lbSlash33");
            panel2.add(lbSlash33);
            lbSlash33.setBounds(840, 373, 10, 28);

            //---- lbSlash34 ----
            lbSlash34.setText("/");
            lbSlash34.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash34.setFont(lbSlash34.getFont().deriveFont(lbSlash34.getFont().getSize() + 2f));
            lbSlash34.setName("lbSlash34");
            panel2.add(lbSlash34);
            lbSlash34.setBounds(840, 398, 10, 28);

            //---- lbSlash35 ----
            lbSlash35.setText("/");
            lbSlash35.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash35.setFont(lbSlash35.getFont().deriveFont(lbSlash35.getFont().getSize() + 2f));
            lbSlash35.setName("lbSlash35");
            panel2.add(lbSlash35);
            lbSlash35.setBounds(840, 423, 10, 28);

            //---- lbSlash36 ----
            lbSlash36.setText("/");
            lbSlash36.setHorizontalAlignment(SwingConstants.CENTER);
            lbSlash36.setFont(lbSlash36.getFont().deriveFont(lbSlash36.getFont().getSize() + 2f));
            lbSlash36.setName("lbSlash36");
            panel2.add(lbSlash36);
            lbSlash36.setBounds(840, 448, 10, 28);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel2);
          panel2.setBounds(10, 100, 980, 485);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- jmFicheArticle ----
      jmFicheArticle.setText("Fiche article");
      jmFicheArticle.setName("jmFicheArticle");
      jmFicheArticle.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          jmFicheArticleActionPerformed(e);
        }
      });
      BTD.add(jmFicheArticle);

      //---- jmEvolution ----
      jmEvolution.setText("D\u00e9tail \u00e9volution prix de vente");
      jmEvolution.setName("jmEvolution");
      jmEvolution.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          jmEvolutionActionPerformed(e);
        }
      });
      BTD.add(jmEvolution);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private XRiTextField DEMETB;
  private JLabel OBJ_67;
  private RiZoneSortie LIBETB;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_34;
  private RiZoneSortie ARTRCH;
  private JLabel OBJ_38;
  private RiZoneSortie GFARCH;
  private JLabel OBJ_41;
  private RiZoneSortie SFARCH;
  private JLabel OBJ_45;
  private RiZoneSortie FRSRCH;
  private XRiCalendrier DATAPP;
  private JLabel OBJ_46;
  private JPanel panel2;
  private RiZoneSortie LD101;
  private RiZoneSortie LD102;
  private RiZoneSortie LD103;
  private RiZoneSortie LD104;
  private RiZoneSortie LD105;
  private RiZoneSortie LD106;
  private RiZoneSortie LD107;
  private RiZoneSortie LD108;
  private RiZoneSortie LD109;
  private RiZoneSortie LD110;
  private RiZoneSortie LD111;
  private RiZoneSortie LD112;
  private RiZoneSortie LD113;
  private RiZoneSortie LD114;
  private RiZoneSortie LD115;
  private RiZoneSortie LD116;
  private RiZoneSortie LD117;
  private RiZoneSortie LD118;
  private XRiTextField PRA01;
  private XRiTextField PRA02;
  private XRiTextField PRA03;
  private XRiTextField PRA04;
  private XRiTextField PRA05;
  private XRiTextField PRA06;
  private XRiTextField PRA07;
  private XRiTextField PRA08;
  private XRiTextField PRA09;
  private XRiTextField PRA10;
  private XRiTextField PRA11;
  private XRiTextField PRA12;
  private XRiTextField PRA13;
  private XRiTextField PRA14;
  private XRiTextField PRA15;
  private XRiTextField PRA16;
  private XRiTextField PRA17;
  private XRiTextField PRA18;
  private RiZoneSortie LD201;
  private RiZoneSortie LD202;
  private RiZoneSortie LD203;
  private RiZoneSortie LD204;
  private RiZoneSortie LD205;
  private RiZoneSortie LD206;
  private RiZoneSortie LD207;
  private RiZoneSortie LD208;
  private RiZoneSortie LD209;
  private RiZoneSortie LD210;
  private RiZoneSortie LD211;
  private RiZoneSortie LD212;
  private RiZoneSortie LD213;
  private RiZoneSortie LD214;
  private RiZoneSortie LD215;
  private RiZoneSortie LD216;
  private RiZoneSortie LD217;
  private RiZoneSortie LD218;
  private XRiTextField PRC01;
  private XRiTextField PRC02;
  private XRiTextField PRC03;
  private XRiTextField PRC04;
  private XRiTextField PRC05;
  private XRiTextField PRC06;
  private XRiTextField PRC07;
  private XRiTextField PRC08;
  private XRiTextField PRC09;
  private XRiTextField PRC10;
  private XRiTextField PRC11;
  private XRiTextField PRC12;
  private XRiTextField PRC13;
  private XRiTextField PRC14;
  private XRiTextField PRC15;
  private XRiTextField PRC16;
  private XRiTextField PRC17;
  private XRiTextField PRC18;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private RiZoneSortie UA1;
  private RiZoneSortie UA2;
  private RiZoneSortie UA3;
  private RiZoneSortie UA4;
  private RiZoneSortie UA5;
  private RiZoneSortie UA6;
  private RiZoneSortie UA7;
  private RiZoneSortie UA8;
  private RiZoneSortie UA9;
  private RiZoneSortie UA10;
  private RiZoneSortie UA11;
  private RiZoneSortie UA12;
  private RiZoneSortie UA13;
  private RiZoneSortie UA14;
  private RiZoneSortie UA15;
  private RiZoneSortie UA16;
  private RiZoneSortie UA17;
  private RiZoneSortie UA18;
  private JLabel lbSlash1;
  private JLabel lbSlash2;
  private JLabel lbSlash3;
  private JLabel lbSlash4;
  private JLabel lbSlash5;
  private JLabel lbSlash6;
  private JLabel lbSlash7;
  private JLabel lbSlash8;
  private JLabel lbSlash9;
  private JLabel lbSlash10;
  private JLabel lbSlash11;
  private JLabel lbSlash12;
  private JLabel lbSlash13;
  private JLabel lbSlash14;
  private JLabel lbSlash15;
  private JLabel lbSlash16;
  private JLabel lbSlash17;
  private JLabel lbSlash18;
  private RiZoneSortie UPRV1;
  private RiZoneSortie UPRV2;
  private RiZoneSortie UPRV3;
  private RiZoneSortie UPRV4;
  private RiZoneSortie UPRV5;
  private RiZoneSortie UPRV6;
  private RiZoneSortie UPRV7;
  private RiZoneSortie UPRV8;
  private RiZoneSortie UPRV9;
  private RiZoneSortie UPRV10;
  private RiZoneSortie UPRV11;
  private RiZoneSortie UPRV12;
  private RiZoneSortie UPRV13;
  private RiZoneSortie UPRV14;
  private RiZoneSortie UPRV15;
  private RiZoneSortie UPRV16;
  private RiZoneSortie UPRV17;
  private RiZoneSortie UPRV18;
  private JLabel lbSlash19;
  private JLabel lbSlash20;
  private JLabel lbSlash21;
  private JLabel lbSlash22;
  private JLabel lbSlash23;
  private JLabel lbSlash24;
  private JLabel lbSlash25;
  private JLabel lbSlash26;
  private JLabel lbSlash27;
  private JLabel lbSlash28;
  private JLabel lbSlash29;
  private JLabel lbSlash30;
  private JLabel lbSlash31;
  private JLabel lbSlash32;
  private JLabel lbSlash33;
  private JLabel lbSlash34;
  private JLabel lbSlash35;
  private JLabel lbSlash36;
  private JPopupMenu BTD;
  private JMenuItem jmFicheArticle;
  private JMenuItem jmEvolution;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
