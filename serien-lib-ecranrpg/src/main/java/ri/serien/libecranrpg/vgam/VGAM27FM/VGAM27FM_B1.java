
package ri.serien.libecranrpg.vgam.VGAM27FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGAM27FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] T_Value = { "", "B", "N", "+", "-", "G" };
  
  public VGAM27FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    T1.setValeurs(T_Value, null);
    T2.setValeurs(T_Value, null);
    T3.setValeurs(T_Value, null);
    T4.setValeurs(T_Value, null);
    T5.setValeurs(T_Value, null);
    T6.setValeurs(T_Value, null);
    T7.setValeurs(T_Value, null);
    T8.setValeurs(T_Value, null);
    T9.setValeurs(T_Value, null);
    T10.setValeurs(T_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    Q1 = new XRiTextField();
    T1 = new XRiComboBox();
    V1 = new XRiTextField();
    R11 = new XRiTextField();
    C1 = new XRiTextField();
    Q2 = new XRiTextField();
    V2 = new XRiTextField();
    R21 = new XRiTextField();
    C2 = new XRiTextField();
    Q3 = new XRiTextField();
    V3 = new XRiTextField();
    R31 = new XRiTextField();
    C3 = new XRiTextField();
    Q4 = new XRiTextField();
    V4 = new XRiTextField();
    R41 = new XRiTextField();
    C4 = new XRiTextField();
    Q5 = new XRiTextField();
    V5 = new XRiTextField();
    R51 = new XRiTextField();
    C5 = new XRiTextField();
    Q6 = new XRiTextField();
    V6 = new XRiTextField();
    R61 = new XRiTextField();
    C6 = new XRiTextField();
    Q7 = new XRiTextField();
    V7 = new XRiTextField();
    R71 = new XRiTextField();
    C7 = new XRiTextField();
    Q8 = new XRiTextField();
    V8 = new XRiTextField();
    R81 = new XRiTextField();
    C8 = new XRiTextField();
    Q9 = new XRiTextField();
    V9 = new XRiTextField();
    R91 = new XRiTextField();
    C9 = new XRiTextField();
    Q10 = new XRiTextField();
    V10 = new XRiTextField();
    R101 = new XRiTextField();
    C10 = new XRiTextField();
    V14 = new XRiTextField();
    V24 = new XRiTextField();
    V34 = new XRiTextField();
    V44 = new XRiTextField();
    V54 = new XRiTextField();
    V64 = new XRiTextField();
    V74 = new XRiTextField();
    V84 = new XRiTextField();
    V94 = new XRiTextField();
    V104 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    BT_PGDOWN = new JButton();
    BT_PGUP = new JButton();
    T2 = new XRiComboBox();
    T3 = new XRiComboBox();
    T4 = new XRiComboBox();
    T5 = new XRiComboBox();
    T6 = new XRiComboBox();
    T7 = new XRiComboBox();
    T8 = new XRiComboBox();
    T9 = new XRiComboBox();
    T10 = new XRiComboBox();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(720, 370));
    setPreferredSize(new Dimension(720, 370));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Conditions quantitatives"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- Q1 ----
          Q1.setName("Q1");
          panel1.add(Q1);
          Q1.setBounds(28, 67, 80, Q1.getPreferredSize().height);

          //---- T1 ----
          T1.setModel(new DefaultComboBoxModel(new String[] {
            " ",
            "Base",
            "Net",
            "+",
            "-",
            "Gratuit"
          }));
          T1.setName("T1");
          panel1.add(T1);
          T1.setBounds(118, 68, 92, T1.getPreferredSize().height);

          //---- V1 ----
          V1.setName("V1");
          panel1.add(V1);
          V1.setBounds(220, 65, 120, V1.getPreferredSize().height);

          //---- R11 ----
          R11.setHorizontalAlignment(SwingConstants.RIGHT);
          R11.setName("R11");
          panel1.add(R11);
          R11.setBounds(350, 65, 50, R11.getPreferredSize().height);

          //---- C1 ----
          C1.setName("C1");
          panel1.add(C1);
          C1.setBounds(410, 65, 60, C1.getPreferredSize().height);

          //---- Q2 ----
          Q2.setName("Q2");
          panel1.add(Q2);
          Q2.setBounds(28, 92, 80, Q2.getPreferredSize().height);

          //---- V2 ----
          V2.setName("V2");
          panel1.add(V2);
          V2.setBounds(220, 90, 120, V2.getPreferredSize().height);

          //---- R21 ----
          R21.setHorizontalAlignment(SwingConstants.RIGHT);
          R21.setName("R21");
          panel1.add(R21);
          R21.setBounds(350, 90, 50, R21.getPreferredSize().height);

          //---- C2 ----
          C2.setName("C2");
          panel1.add(C2);
          C2.setBounds(410, 90, 60, C2.getPreferredSize().height);

          //---- Q3 ----
          Q3.setName("Q3");
          panel1.add(Q3);
          Q3.setBounds(28, 117, 80, Q3.getPreferredSize().height);

          //---- V3 ----
          V3.setName("V3");
          panel1.add(V3);
          V3.setBounds(220, 115, 120, V3.getPreferredSize().height);

          //---- R31 ----
          R31.setHorizontalAlignment(SwingConstants.RIGHT);
          R31.setName("R31");
          panel1.add(R31);
          R31.setBounds(350, 115, 50, R31.getPreferredSize().height);

          //---- C3 ----
          C3.setName("C3");
          panel1.add(C3);
          C3.setBounds(410, 115, 60, C3.getPreferredSize().height);

          //---- Q4 ----
          Q4.setName("Q4");
          panel1.add(Q4);
          Q4.setBounds(28, 142, 80, Q4.getPreferredSize().height);

          //---- V4 ----
          V4.setName("V4");
          panel1.add(V4);
          V4.setBounds(220, 140, 120, V4.getPreferredSize().height);

          //---- R41 ----
          R41.setHorizontalAlignment(SwingConstants.RIGHT);
          R41.setName("R41");
          panel1.add(R41);
          R41.setBounds(350, 140, 50, R41.getPreferredSize().height);

          //---- C4 ----
          C4.setName("C4");
          panel1.add(C4);
          C4.setBounds(410, 140, 60, C4.getPreferredSize().height);

          //---- Q5 ----
          Q5.setName("Q5");
          panel1.add(Q5);
          Q5.setBounds(28, 167, 80, Q5.getPreferredSize().height);

          //---- V5 ----
          V5.setName("V5");
          panel1.add(V5);
          V5.setBounds(220, 165, 120, V5.getPreferredSize().height);

          //---- R51 ----
          R51.setHorizontalAlignment(SwingConstants.RIGHT);
          R51.setName("R51");
          panel1.add(R51);
          R51.setBounds(350, 165, 50, R51.getPreferredSize().height);

          //---- C5 ----
          C5.setName("C5");
          panel1.add(C5);
          C5.setBounds(410, 165, 60, C5.getPreferredSize().height);

          //---- Q6 ----
          Q6.setName("Q6");
          panel1.add(Q6);
          Q6.setBounds(28, 192, 80, Q6.getPreferredSize().height);

          //---- V6 ----
          V6.setName("V6");
          panel1.add(V6);
          V6.setBounds(220, 190, 120, V6.getPreferredSize().height);

          //---- R61 ----
          R61.setHorizontalAlignment(SwingConstants.RIGHT);
          R61.setName("R61");
          panel1.add(R61);
          R61.setBounds(350, 190, 50, R61.getPreferredSize().height);

          //---- C6 ----
          C6.setName("C6");
          panel1.add(C6);
          C6.setBounds(410, 190, 60, C6.getPreferredSize().height);

          //---- Q7 ----
          Q7.setName("Q7");
          panel1.add(Q7);
          Q7.setBounds(28, 217, 80, Q7.getPreferredSize().height);

          //---- V7 ----
          V7.setName("V7");
          panel1.add(V7);
          V7.setBounds(220, 215, 120, V7.getPreferredSize().height);

          //---- R71 ----
          R71.setHorizontalAlignment(SwingConstants.RIGHT);
          R71.setName("R71");
          panel1.add(R71);
          R71.setBounds(350, 215, 50, R71.getPreferredSize().height);

          //---- C7 ----
          C7.setName("C7");
          panel1.add(C7);
          C7.setBounds(410, 215, 60, C7.getPreferredSize().height);

          //---- Q8 ----
          Q8.setName("Q8");
          panel1.add(Q8);
          Q8.setBounds(28, 242, 80, Q8.getPreferredSize().height);

          //---- V8 ----
          V8.setName("V8");
          panel1.add(V8);
          V8.setBounds(220, 240, 120, V8.getPreferredSize().height);

          //---- R81 ----
          R81.setHorizontalAlignment(SwingConstants.RIGHT);
          R81.setName("R81");
          panel1.add(R81);
          R81.setBounds(350, 240, 50, R81.getPreferredSize().height);

          //---- C8 ----
          C8.setName("C8");
          panel1.add(C8);
          C8.setBounds(410, 240, 60, C8.getPreferredSize().height);

          //---- Q9 ----
          Q9.setName("Q9");
          panel1.add(Q9);
          Q9.setBounds(28, 267, 80, Q9.getPreferredSize().height);

          //---- V9 ----
          V9.setName("V9");
          panel1.add(V9);
          V9.setBounds(220, 265, 120, V9.getPreferredSize().height);

          //---- R91 ----
          R91.setHorizontalAlignment(SwingConstants.RIGHT);
          R91.setName("R91");
          panel1.add(R91);
          R91.setBounds(350, 265, 50, R91.getPreferredSize().height);

          //---- C9 ----
          C9.setName("C9");
          panel1.add(C9);
          C9.setBounds(410, 265, 60, C9.getPreferredSize().height);

          //---- Q10 ----
          Q10.setName("Q10");
          panel1.add(Q10);
          Q10.setBounds(28, 292, 80, Q10.getPreferredSize().height);

          //---- V10 ----
          V10.setName("V10");
          panel1.add(V10);
          V10.setBounds(220, 290, 120, V10.getPreferredSize().height);

          //---- R101 ----
          R101.setHorizontalAlignment(SwingConstants.RIGHT);
          R101.setName("R101");
          panel1.add(R101);
          R101.setBounds(350, 290, 50, R101.getPreferredSize().height);

          //---- C10 ----
          C10.setName("C10");
          panel1.add(C10);
          C10.setBounds(410, 290, 60, C10.getPreferredSize().height);

          //---- V14 ----
          V14.setName("V14");
          panel1.add(V14);
          V14.setBounds(220, 65, 120, V14.getPreferredSize().height);

          //---- V24 ----
          V24.setName("V24");
          panel1.add(V24);
          V24.setBounds(220, 90, 120, V24.getPreferredSize().height);

          //---- V34 ----
          V34.setName("V34");
          panel1.add(V34);
          V34.setBounds(220, 115, 120, V34.getPreferredSize().height);

          //---- V44 ----
          V44.setName("V44");
          panel1.add(V44);
          V44.setBounds(220, 140, 120, V44.getPreferredSize().height);

          //---- V54 ----
          V54.setName("V54");
          panel1.add(V54);
          V54.setBounds(220, 165, 120, V54.getPreferredSize().height);

          //---- V64 ----
          V64.setName("V64");
          panel1.add(V64);
          V64.setBounds(220, 190, 120, V64.getPreferredSize().height);

          //---- V74 ----
          V74.setName("V74");
          panel1.add(V74);
          V74.setBounds(220, 215, 120, V74.getPreferredSize().height);

          //---- V84 ----
          V84.setName("V84");
          panel1.add(V84);
          V84.setBounds(220, 240, 120, V84.getPreferredSize().height);

          //---- V94 ----
          V94.setName("V94");
          panel1.add(V94);
          V94.setBounds(220, 265, 120, V94.getPreferredSize().height);

          //---- V104 ----
          V104.setName("V104");
          panel1.add(V104);
          V104.setBounds(220, 290, 120, V104.getPreferredSize().height);

          //---- label1 ----
          label1.setText("Quantit\u00e9/Unit\u00e9");
          label1.setHorizontalAlignment(SwingConstants.CENTER);
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(28, 45, 80, 23);

          //---- label2 ----
          label2.setText("Type valeur");
          label2.setHorizontalAlignment(SwingConstants.CENTER);
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(118, 45, 92, 23);

          //---- label3 ----
          label3.setText("Valeur");
          label3.setHorizontalAlignment(SwingConstants.CENTER);
          label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
          label3.setName("label3");
          panel1.add(label3);
          label3.setBounds(220, 45, 120, 23);

          //---- label4 ----
          label4.setText("Remise");
          label4.setHorizontalAlignment(SwingConstants.CENTER);
          label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
          label4.setName("label4");
          panel1.add(label4);
          label4.setBounds(350, 45, 50, 23);

          //---- label5 ----
          label5.setText("Coef.");
          label5.setHorizontalAlignment(SwingConstants.CENTER);
          label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
          label5.setName("label5");
          panel1.add(label5);
          label5.setBounds(410, 45, 60, 23);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setToolTipText("Page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          panel1.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(475, 65, 25, 115);

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          panel1.add(BT_PGUP);
          BT_PGUP.setBounds(475, 205, 25, 115);

          //---- T2 ----
          T2.setModel(new DefaultComboBoxModel(new String[] {
            " ",
            "Base",
            "Net",
            "+",
            "-",
            "Gratuit"
          }));
          T2.setName("T2");
          panel1.add(T2);
          T2.setBounds(118, 93, 92, T2.getPreferredSize().height);

          //---- T3 ----
          T3.setModel(new DefaultComboBoxModel(new String[] {
            " ",
            "Base",
            "Net",
            "+",
            "-",
            "Gratuit"
          }));
          T3.setName("T3");
          panel1.add(T3);
          T3.setBounds(118, 118, 92, T3.getPreferredSize().height);

          //---- T4 ----
          T4.setModel(new DefaultComboBoxModel(new String[] {
            " ",
            "Base",
            "Net",
            "+",
            "-",
            "Gratuit"
          }));
          T4.setName("T4");
          panel1.add(T4);
          T4.setBounds(118, 143, 92, T4.getPreferredSize().height);

          //---- T5 ----
          T5.setModel(new DefaultComboBoxModel(new String[] {
            " ",
            "Base",
            "Net",
            "+",
            "-",
            "Gratuit"
          }));
          T5.setName("T5");
          panel1.add(T5);
          T5.setBounds(118, 168, 92, T5.getPreferredSize().height);

          //---- T6 ----
          T6.setModel(new DefaultComboBoxModel(new String[] {
            " ",
            "Base",
            "Net",
            "+",
            "-",
            "Gratuit"
          }));
          T6.setName("T6");
          panel1.add(T6);
          T6.setBounds(118, 193, 92, T6.getPreferredSize().height);

          //---- T7 ----
          T7.setModel(new DefaultComboBoxModel(new String[] {
            " ",
            "Base",
            "Net",
            "+",
            "-",
            "Gratuit"
          }));
          T7.setName("T7");
          panel1.add(T7);
          T7.setBounds(118, 218, 92, T7.getPreferredSize().height);

          //---- T8 ----
          T8.setModel(new DefaultComboBoxModel(new String[] {
            " ",
            "Base",
            "Net",
            "+",
            "-",
            "Gratuit"
          }));
          T8.setName("T8");
          panel1.add(T8);
          T8.setBounds(118, 243, 92, T8.getPreferredSize().height);

          //---- T9 ----
          T9.setModel(new DefaultComboBoxModel(new String[] {
            " ",
            "Base",
            "Net",
            "+",
            "-",
            "Gratuit"
          }));
          T9.setName("T9");
          panel1.add(T9);
          T9.setBounds(118, 268, 92, T9.getPreferredSize().height);

          //---- T10 ----
          T10.setModel(new DefaultComboBoxModel(new String[] {
            " ",
            "Base",
            "Net",
            "+",
            "-",
            "Gratuit"
          }));
          T10.setName("T10");
          panel1.add(T10);
          T10.setBounds(118, 293, 92, T10.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 15, 525, 340);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField Q1;
  private XRiComboBox T1;
  private XRiTextField V1;
  private XRiTextField R11;
  private XRiTextField C1;
  private XRiTextField Q2;
  private XRiTextField V2;
  private XRiTextField R21;
  private XRiTextField C2;
  private XRiTextField Q3;
  private XRiTextField V3;
  private XRiTextField R31;
  private XRiTextField C3;
  private XRiTextField Q4;
  private XRiTextField V4;
  private XRiTextField R41;
  private XRiTextField C4;
  private XRiTextField Q5;
  private XRiTextField V5;
  private XRiTextField R51;
  private XRiTextField C5;
  private XRiTextField Q6;
  private XRiTextField V6;
  private XRiTextField R61;
  private XRiTextField C6;
  private XRiTextField Q7;
  private XRiTextField V7;
  private XRiTextField R71;
  private XRiTextField C7;
  private XRiTextField Q8;
  private XRiTextField V8;
  private XRiTextField R81;
  private XRiTextField C8;
  private XRiTextField Q9;
  private XRiTextField V9;
  private XRiTextField R91;
  private XRiTextField C9;
  private XRiTextField Q10;
  private XRiTextField V10;
  private XRiTextField R101;
  private XRiTextField C10;
  private XRiTextField V14;
  private XRiTextField V24;
  private XRiTextField V34;
  private XRiTextField V44;
  private XRiTextField V54;
  private XRiTextField V64;
  private XRiTextField V74;
  private XRiTextField V84;
  private XRiTextField V94;
  private XRiTextField V104;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JButton BT_PGDOWN;
  private JButton BT_PGUP;
  private XRiComboBox T2;
  private XRiComboBox T3;
  private XRiComboBox T4;
  private XRiComboBox T5;
  private XRiComboBox T6;
  private XRiComboBox T7;
  private XRiComboBox T8;
  private XRiComboBox T9;
  private XRiComboBox T10;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
