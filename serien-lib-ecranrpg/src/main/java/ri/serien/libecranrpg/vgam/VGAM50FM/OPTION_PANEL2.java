
package ri.serien.libecranrpg.vgam.VGAM50FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class OPTION_PANEL2 extends SNPanelEcranRPG implements ioFrame {
  
  
  public OPTION_PANEL2(SNPanelEcranRPG parent) {
    super(parent);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Bouton par défaut
    setDefaultButton(bt_valider);
    
    bt_valider.setIcon(lexique.chargerImage("images/OK_p.png", true));
    bt_retour.setIcon(lexique.chargerImage("images/retour_p.png", true));
    setTitle("Calcul prix de vente minimum");
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    // super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    OBJ_158.setVisible(A1PFB.isVisible());
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  @Override
  public void dispose() {
    getJDialog().dispose();
  }
  
  private void bt_validerActionPerformed(ActionEvent e) {
    getData();
    lexique.HostScreenSendKey(this, "ENTER", false);
    closePopupLinkWithBuffer(true);
  }
  
  private void bt_retourActionPerformed(ActionEvent e) {
    closePopupLinkWithBuffer(true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    pnl_menus = new JPanel();
    panel1 = new JPanel();
    riMenu1 = new RiMenu();
    bt_valider = new RiMenu_bt();
    riMenu2 = new RiMenu();
    bt_retour = new RiMenu_bt();
    pnl_contenu = new JPanel();
    panel11 = new JPanel();
    OBJ_149 = new JLabel();
    CAREX3 = new XRiTextField();
    CAREX4 = new XRiTextField();
    CAREX5 = new XRiTextField();
    CAREX6 = new XRiTextField();
    OBJ_158 = new JLabel();
    A1PFB = new XRiTextField();
    OBJ_156 = new JLabel();
    CAKPR = new XRiTextField();

    //======== this ========
    setPreferredSize(new Dimension(570, 115));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnl_menus ========
    {
      pnl_menus.setPreferredSize(new Dimension(170, 0));
      pnl_menus.setMinimumSize(new Dimension(130, 0));
      pnl_menus.setBorder(new LineBorder(Color.lightGray));
      pnl_menus.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
      pnl_menus.setBackground(new Color(238, 239, 241));
      pnl_menus.setName("pnl_menus");
      pnl_menus.setLayout(new BorderLayout());

      //======== panel1 ========
      {
        panel1.setName("panel1");
        panel1.setLayout(new VerticalLayout());

        //======== riMenu1 ========
        {
          riMenu1.setName("riMenu1");

          //---- bt_valider ----
          bt_valider.setText("Valider");
          bt_valider.setName("bt_valider");
          bt_valider.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bt_validerActionPerformed(e);
            }
          });
          riMenu1.add(bt_valider);
        }
        panel1.add(riMenu1);

        //======== riMenu2 ========
        {
          riMenu2.setName("riMenu2");

          //---- bt_retour ----
          bt_retour.setText("Retour");
          bt_retour.setName("bt_retour");
          bt_retour.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bt_retourActionPerformed(e);
            }
          });
          riMenu2.add(bt_retour);
        }
        panel1.add(riMenu2);
      }
      pnl_menus.add(panel1, BorderLayout.SOUTH);
    }
    add(pnl_menus, BorderLayout.EAST);

    //======== pnl_contenu ========
    {
      pnl_contenu.setPreferredSize(new Dimension(400, 120));
      pnl_contenu.setBackground(new Color(238, 238, 210));
      pnl_contenu.setName("pnl_contenu");

      //======== panel11 ========
      {
        panel11.setOpaque(false);
        panel11.setBorder(new LineBorder(new Color(204, 204, 204)));
        panel11.setName("panel11");
        panel11.setLayout(null);

        //---- OBJ_149 ----
        OBJ_149.setText("Remise(s)");
        OBJ_149.setName("OBJ_149");
        panel11.add(OBJ_149);
        OBJ_149.setBounds(15, 15, 66, 20);

        //---- CAREX3 ----
        CAREX3.setComponentPopupMenu(null);
        CAREX3.setHorizontalAlignment(SwingConstants.RIGHT);
        CAREX3.setName("CAREX3");
        panel11.add(CAREX3);
        CAREX3.setBounds(120, 10, 50, CAREX3.getPreferredSize().height);

        //---- CAREX4 ----
        CAREX4.setComponentPopupMenu(null);
        CAREX4.setHorizontalAlignment(SwingConstants.RIGHT);
        CAREX4.setName("CAREX4");
        panel11.add(CAREX4);
        CAREX4.setBounds(177, 10, 50, CAREX4.getPreferredSize().height);

        //---- CAREX5 ----
        CAREX5.setComponentPopupMenu(null);
        CAREX5.setHorizontalAlignment(SwingConstants.RIGHT);
        CAREX5.setName("CAREX5");
        panel11.add(CAREX5);
        CAREX5.setBounds(234, 10, 50, CAREX5.getPreferredSize().height);

        //---- CAREX6 ----
        CAREX6.setComponentPopupMenu(null);
        CAREX6.setHorizontalAlignment(SwingConstants.RIGHT);
        CAREX6.setName("CAREX6");
        panel11.add(CAREX6);
        CAREX6.setBounds(293, 10, 50, CAREX6.getPreferredSize().height);

        //---- OBJ_158 ----
        OBJ_158.setText("Prix de vente mini");
        OBJ_158.setName("OBJ_158");
        panel11.add(OBJ_158);
        OBJ_158.setBounds(15, 48, 105, 20);

        //---- A1PFB ----
        A1PFB.setComponentPopupMenu(null);
        A1PFB.setHorizontalAlignment(SwingConstants.RIGHT);
        A1PFB.setName("A1PFB");
        panel11.add(A1PFB);
        A1PFB.setBounds(120, 44, 84, A1PFB.getPreferredSize().height);

        //---- OBJ_156 ----
        OBJ_156.setText("Coefficient");
        OBJ_156.setName("OBJ_156");
        panel11.add(OBJ_156);
        OBJ_156.setBounds(225, 48, 70, 20);

        //---- CAKPR ----
        CAKPR.setComponentPopupMenu(null);
        CAKPR.setHorizontalAlignment(SwingConstants.RIGHT);
        CAKPR.setName("CAKPR");
        panel11.add(CAKPR);
        CAKPR.setBounds(293, 44, 58, CAKPR.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel11.getComponentCount(); i++) {
            Rectangle bounds = panel11.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel11.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel11.setMinimumSize(preferredSize);
          panel11.setPreferredSize(preferredSize);
        }
      }

      GroupLayout pnl_contenuLayout = new GroupLayout(pnl_contenu);
      pnl_contenu.setLayout(pnl_contenuLayout);
      pnl_contenuLayout.setHorizontalGroup(
        pnl_contenuLayout.createParallelGroup()
          .addGroup(pnl_contenuLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(panel11, GroupLayout.DEFAULT_SIZE, 376, Short.MAX_VALUE)
            .addContainerGap())
      );
      pnl_contenuLayout.setVerticalGroup(
        pnl_contenuLayout.createParallelGroup()
          .addGroup(pnl_contenuLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(panel11, GroupLayout.DEFAULT_SIZE, 89, Short.MAX_VALUE)
            .addContainerGap())
      );
    }
    add(pnl_contenu, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel pnl_menus;
  private JPanel panel1;
  private RiMenu riMenu1;
  private RiMenu_bt bt_valider;
  private RiMenu riMenu2;
  private RiMenu_bt bt_retour;
  private JPanel pnl_contenu;
  private JPanel panel11;
  private JLabel OBJ_149;
  private XRiTextField CAREX3;
  private XRiTextField CAREX4;
  private XRiTextField CAREX5;
  private XRiTextField CAREX6;
  private JLabel OBJ_158;
  private XRiTextField A1PFB;
  private JLabel OBJ_156;
  private XRiTextField CAKPR;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
