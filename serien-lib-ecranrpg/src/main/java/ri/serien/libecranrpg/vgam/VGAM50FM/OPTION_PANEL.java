
package ri.serien.libecranrpg.vgam.VGAM50FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;

/**
 * @author Stéphane Vénéri
 */
public class OPTION_PANEL extends SNPanelEcranRPG implements ioFrame {
  
  
  public OPTION_PANEL(SNPanelEcranRPG parent) {
    super(parent);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Bouton par défaut
    setDefaultButton(bt_valider);
    
    bt_valider.setIcon(lexique.chargerImage("images/OK_p.png", true));
    bt_retour.setIcon(lexique.chargerImage("images/retour_p.png", true));
    setTitle("Calcul prix de vente");
    
    initDiverses();
    CACPV.setValeurs("V", CACPV_GRP);
    CACPV_00.setValeurs("");
    CACPV_01.setValeurs("C");
    CACPV_02.setValeurs("W");
    CACPV_03.setValeurs("N");
    CACPV_04.setValeurs("M");
    CACPV_05.setValeurs("A");
    CACPV_06.setValeurs("1");
    CACPV_07.setValeurs("2");
    CACPV_08.setValeurs("3");
    CACPV_09.setValeurs("4");
    CACPV_10.setValeurs("P");
    CACPV_11.setValeurs("Q");
    CACPV_12.setValeurs("R");
    CACPV_13.setValeurs("D");
    CACPV_14.setValeurs("S");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  @Override
  public void dispose() {
    getJDialog().dispose();
  }
  
  private void bt_validerActionPerformed(ActionEvent e) {
    getData();
    lexique.HostScreenSendKey(this, "ENTER");
    closePopupLinkWithBuffer(true);
  }
  
  private void bt_retourActionPerformed(ActionEvent e) {
    closePopupLinkWithBuffer(true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnl_menus = new JPanel();
    panel1 = new JPanel();
    riMenu1 = new RiMenu();
    bt_valider = new RiMenu_bt();
    riMenu2 = new RiMenu();
    bt_retour = new RiMenu_bt();
    pnl_contenu = new JPanel();
    panel2 = new JPanel();
    CACPV = new XRiRadioButton();
    CACPV_01 = new XRiRadioButton();
    CACPV_02 = new XRiRadioButton();
    CACPV_03 = new XRiRadioButton();
    CACPV_04 = new XRiRadioButton();
    CACPV_05 = new XRiRadioButton();
    CACPV_06 = new XRiRadioButton();
    CACPV_07 = new XRiRadioButton();
    CACPV_08 = new XRiRadioButton();
    CACPV_09 = new XRiRadioButton();
    CACPV_14 = new XRiRadioButton();
    panel3 = new JPanel();
    CACPV_10 = new XRiRadioButton();
    CACPV_11 = new XRiRadioButton();
    CACPV_12 = new XRiRadioButton();
    CACPV_13 = new XRiRadioButton();
    CACPV_00 = new XRiRadioButton();
    CACPV_GRP = new ButtonGroup();

    //======== this ========
    setPreferredSize(new Dimension(505, 510));
    setMinimumSize(new Dimension(505, 510));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnl_menus ========
    {
      pnl_menus.setPreferredSize(new Dimension(170, 0));
      pnl_menus.setMinimumSize(new Dimension(130, 0));
      pnl_menus.setBorder(new LineBorder(Color.lightGray));
      pnl_menus.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
      pnl_menus.setBackground(new Color(238, 239, 241));
      pnl_menus.setName("pnl_menus");
      pnl_menus.setLayout(new BorderLayout());

      //======== panel1 ========
      {
        panel1.setName("panel1");
        panel1.setLayout(new VerticalLayout());

        //======== riMenu1 ========
        {
          riMenu1.setName("riMenu1");

          //---- bt_valider ----
          bt_valider.setText("Valider");
          bt_valider.setName("bt_valider");
          bt_valider.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bt_validerActionPerformed(e);
            }
          });
          riMenu1.add(bt_valider);
        }
        panel1.add(riMenu1);

        //======== riMenu2 ========
        {
          riMenu2.setName("riMenu2");

          //---- bt_retour ----
          bt_retour.setText("Retour");
          bt_retour.setName("bt_retour");
          bt_retour.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bt_retourActionPerformed(e);
            }
          });
          riMenu2.add(bt_retour);
        }
        panel1.add(riMenu2);
      }
      pnl_menus.add(panel1, BorderLayout.SOUTH);
    }
    add(pnl_menus, BorderLayout.EAST);

    //======== pnl_contenu ========
    {
      pnl_contenu.setBackground(new Color(238, 238, 210));
      pnl_contenu.setName("pnl_contenu");

      //======== panel2 ========
      {
        panel2.setOpaque(false);
        panel2.setBorder(new TitledBorder("Mise \u00e0 jour depuis condition d'achat"));
        panel2.setName("panel2");
        panel2.setLayout(null);

        //---- CACPV ----
        CACPV.setText("Prix d'achat brut HT x coeff.");
        CACPV.setName("CACPV");
        panel2.add(CACPV);
        CACPV.setBounds(25, 35, 265, CACPV.getPreferredSize().height);

        //---- CACPV_01 ----
        CACPV_01.setText("Prix d'achat brut HT seul");
        CACPV_01.setName("CACPV_01");
        panel2.add(CACPV_01);
        CACPV_01.setBounds(25, 58, 265, 18);

        //---- CACPV_02 ----
        CACPV_02.setText("Prix d'achat brut HT x coeff. d'approche x coeff.");
        CACPV_02.setName("CACPV_02");
        panel2.add(CACPV_02);
        CACPV_02.setBounds(25, 81, 265, 18);

        //---- CACPV_03 ----
        CACPV_03.setText("Prix net d'achat x coeff.");
        CACPV_03.setName("CACPV_03");
        panel2.add(CACPV_03);
        CACPV_03.setBounds(25, 104, 265, 18);

        //---- CACPV_04 ----
        CACPV_04.setText("Prix net x coeff. d'approche moyen x coeff.");
        CACPV_04.setName("CACPV_04");
        panel2.add(CACPV_04);
        CACPV_04.setBounds(25, 127, 265, 18);

        //---- CACPV_05 ----
        CACPV_05.setText("Prix net x Coeff. d'approche saisi x coeff.");
        CACPV_05.setName("CACPV_05");
        panel2.add(CACPV_05);
        CACPV_05.setBounds(25, 150, 265, 18);

        //---- CACPV_06 ----
        CACPV_06.setText("1er prix quantitatif");
        CACPV_06.setName("CACPV_06");
        panel2.add(CACPV_06);
        CACPV_06.setBounds(25, 173, 265, 18);

        //---- CACPV_07 ----
        CACPV_07.setText("2\u00e8me prix quantitatif");
        CACPV_07.setName("CACPV_07");
        panel2.add(CACPV_07);
        CACPV_07.setBounds(25, 196, 265, 18);

        //---- CACPV_08 ----
        CACPV_08.setText("3\u00e8me prix quantitatif");
        CACPV_08.setName("CACPV_08");
        panel2.add(CACPV_08);
        CACPV_08.setBounds(25, 219, 265, 18);

        //---- CACPV_09 ----
        CACPV_09.setText("4\u00e8me prix quantitatif");
        CACPV_09.setName("CACPV_09");
        panel2.add(CACPV_09);
        CACPV_09.setBounds(25, 242, 265, 18);

        //---- CACPV_14 ----
        CACPV_14.setText("Variation du PRV standard sur prix de vente");
        CACPV_14.setName("CACPV_14");
        panel2.add(CACPV_14);
        CACPV_14.setBounds(25, 265, 265, CACPV_14.getPreferredSize().height);
      }

      //======== panel3 ========
      {
        panel3.setBorder(new TitledBorder("Mise \u00e0 jour li\u00e9e aux achats"));
        panel3.setOpaque(false);
        panel3.setName("panel3");
        panel3.setLayout(null);

        //---- CACPV_10 ----
        CACPV_10.setText("PUMP d\u00e8s la r\u00e9ception d'achat x coeff.");
        CACPV_10.setName("CACPV_10");
        panel3.add(CACPV_10);
        CACPV_10.setBounds(25, 35, 265, 18);

        //---- CACPV_11 ----
        CACPV_11.setText("PUMP \u00e0 la demande (GAM 561) x coeff.");
        CACPV_11.setName("CACPV_11");
        panel3.add(CACPV_11);
        CACPV_11.setBounds(25, 58, 265, 18);

        //---- CACPV_12 ----
        CACPV_12.setText("Prix de revient x coeff.");
        CACPV_12.setName("CACPV_12");
        panel3.add(CACPV_12);
        CACPV_12.setBounds(25, 81, 265, 18);

        //---- CACPV_13 ----
        CACPV_13.setText("Prix de revient r\u00e9el du dernier achat x coeff.");
        CACPV_13.setName("CACPV_13");
        panel3.add(CACPV_13);
        CACPV_13.setBounds(25, 104, 265, 18);
      }

      //---- CACPV_00 ----
      CACPV_00.setText("Pas de mise \u00e0 jour");
      CACPV_00.setFont(CACPV_00.getFont().deriveFont(CACPV_00.getFont().getStyle() | Font.BOLD));
      CACPV_00.setName("CACPV_00");

      GroupLayout pnl_contenuLayout = new GroupLayout(pnl_contenu);
      pnl_contenu.setLayout(pnl_contenuLayout);
      pnl_contenuLayout.setHorizontalGroup(
        pnl_contenuLayout.createParallelGroup()
          .addGroup(pnl_contenuLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnl_contenuLayout.createParallelGroup()
              .addComponent(CACPV_00, GroupLayout.PREFERRED_SIZE, 265, GroupLayout.PREFERRED_SIZE)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
              .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
            .addContainerGap(13, Short.MAX_VALUE))
      );
      pnl_contenuLayout.setVerticalGroup(
        pnl_contenuLayout.createParallelGroup()
          .addGroup(pnl_contenuLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(CACPV_00, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 304, GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(panel3, GroupLayout.DEFAULT_SIZE, 148, Short.MAX_VALUE)
            .addContainerGap())
      );
    }
    add(pnl_contenu, BorderLayout.CENTER);

    //---- CACPV_GRP ----
    CACPV_GRP.add(CACPV);
    CACPV_GRP.add(CACPV_01);
    CACPV_GRP.add(CACPV_02);
    CACPV_GRP.add(CACPV_03);
    CACPV_GRP.add(CACPV_04);
    CACPV_GRP.add(CACPV_05);
    CACPV_GRP.add(CACPV_06);
    CACPV_GRP.add(CACPV_07);
    CACPV_GRP.add(CACPV_08);
    CACPV_GRP.add(CACPV_09);
    CACPV_GRP.add(CACPV_14);
    CACPV_GRP.add(CACPV_10);
    CACPV_GRP.add(CACPV_11);
    CACPV_GRP.add(CACPV_12);
    CACPV_GRP.add(CACPV_13);
    CACPV_GRP.add(CACPV_00);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnl_menus;
  private JPanel panel1;
  private RiMenu riMenu1;
  private RiMenu_bt bt_valider;
  private RiMenu riMenu2;
  private RiMenu_bt bt_retour;
  private JPanel pnl_contenu;
  private JPanel panel2;
  private XRiRadioButton CACPV;
  private XRiRadioButton CACPV_01;
  private XRiRadioButton CACPV_02;
  private XRiRadioButton CACPV_03;
  private XRiRadioButton CACPV_04;
  private XRiRadioButton CACPV_05;
  private XRiRadioButton CACPV_06;
  private XRiRadioButton CACPV_07;
  private XRiRadioButton CACPV_08;
  private XRiRadioButton CACPV_09;
  private XRiRadioButton CACPV_14;
  private JPanel panel3;
  private XRiRadioButton CACPV_10;
  private XRiRadioButton CACPV_11;
  private XRiRadioButton CACPV_12;
  private XRiRadioButton CACPV_13;
  private XRiRadioButton CACPV_00;
  private ButtonGroup CACPV_GRP;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
